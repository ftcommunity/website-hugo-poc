---
title: "Rund um die Community"
weight: 80
date: 2019-04-04T00:00:00
uploadBy:
- "Esther"
---
Hier gibt es Informationen rund um die Menschen in der ft-community.

* Ihr trefft andere Fans auf [Veranstaltungen](veranstaltungen).
* Ihr könnt mit anderen Fans [chatten](chat).
* Ihr könnt euch über den [Verein](verein) informieren.

