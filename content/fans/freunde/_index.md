---
title: "Freunde und Nachbarn"
weight: 50
date: 2019-04-04T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /links.html
- /links0851.html
- /links2790.html
- /linksf0e2.html
- /links3524.html

---

### fischertechnikclub in den Niederlanden

* [fischertechnikclub.nl](http://www.fischertechnikclub.nl)

### Seiten von Fans

* [der fischertechnik-blog, … damit aus fischertechnikern Ingenieure werden](https://fischertechnikblog.wordpress.com)
* [Mathematik verstehen mit fischertechnik](http://mathematik-mit-fischertechnik.de/)
* [fischertechnik-Roboter mit Arduino](https://fischertechnik-roboter-mit-arduino.de/)
* [Technikgeschichte mit fischertechnik](https://technikgeschichte-mit-fischertechnik.de/)
* [technika | Karlsruher Technik-Initiative](https://karlsruher-technik-initiative.de/)
* "fischertechnik Museum in der Schweiz" hat für immer geschlossen
* [Joachim Jacobi](http://www.jojos-homepage.de)
* [virtuelles fischertechnikmuseum](http://www.cc-c.de/fischertechnik/homepage.htm)
* [Fan Archiv](http://www.ft-fanarchiv.de/)
* [Stef Dijkstra (NL)](http://stef.fthobby.nl/)
* [Dave, Jay und Tyrone Gabeler (NL)](http://www.dgabeler.nl/ft/index.htm)
* [fischertechnik & mehr....](https://hobby-endlich.weebly.com/index.html)
* [MINT Schul-Labor](https://www.mint-schul-labor.de/)

### Kommerzielle Angebote

* [Offizielle Website der fischertechnik GmbH](https://www.fischertechnik.de/de-de)
* [Fischerfriendsman Ihr unabhängiger fischertechnik Handel](http://www.fischerfriendsman.de/)
* [fischertechnik Shop santjohanser](https://santjohanser.de/)
* [Offizieller ftDuino-Shop (MINTronics Peter Habermehl)](https://mintronics.de)
* [Shop für Zubehör wie Sensoren, Aktuatoren und Elektronik](https://gundermann-software.de/shop/)
* [Fischertechnik-Teile.de](https://www.fischertechnik-teile.de/)
* [whizzbizz (3D-Druck-Teile, 1:10-Figuren und Elektronik)](https://www.whizzbizz.com/de/)
* [fischertechnik designer](http://www.3dprofi.de/de/)
* [fischertechnik Education Baukästen für Schulen | Christiani](https://www.christiani.de/schule/fischertechnik-education/)
* [fischertechnik Webshop NL](https://www.fischertechnikwebshop.com)
* [Entwicklung und Produktion - Knobloch GmbH](https://knobloch-gmbh.de)
