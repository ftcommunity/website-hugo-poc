---
title: "FAQ: Bilderpool"
weight: 80
date: 2020-11-06T00:00:00
uploadBy:
- "EstherM"
---

### Worum geht es?

Der Bilderpool ist unsere seit 2003 gewachsene Sammlung von Bildern rund ums Thema fischertechnik. 
Weil die Sammlung ordentlich aufgebaut und ist und die Bilder nicht einfach so in einem großen Becken herumschwimmen,
heißt der Menüpunkt jetzt "Bildersammlung".


### Wie kann ich Bilder in den Bilderpool einstellen?

Damit deine Bilder veröffentlicht werden, bitten wir dich um ein paar Vorarbeiten.

Wir benötigen von dir Folgendes:
* Eine Text-Datei „titel.txt“, die nur den Titel deiner Galerie enthält.
* Eine Datei „description.txt“, in der du dein fischertechnik-Modell oder dein fischertechnik-Projekt insgesamt beschreibst. Dieser Text steht dann unterhalb des Titels auf der Seite mit der Galerieübersicht deiner Bilder.

* Eine Datei „liste.csv“. Diese Datei kann mit einer beliebiegen Tabellenkalkulation (z. B. Excel) erstellt werden und ist im csv-Format mit dem Trennzeichen "," zu speichern. Sie enthält alle Informationen zu den einzelnen Bildern, die du in der Galerie zeigen möchtest. Der Inhalt der Datei ist wie folgt aufgebaut:
Spaltennamen: und ihr Inhalt:
  *	Titel		 : Titel für Seiten der einzelnen Bilder
  *	Description : Beschreibungstext für die einzelnen Bilder	
  *	Photographer	 : Wer hat das jeweilige Foto gemacht?
  *	Constructer: Wer hat das Modell konstruiert? Mehrere Personen sind jeweils durch ein Komma "," voneinander zu trennen
  *	Keywords	 : Keywords oder Schlagworte sind jeweils durch ein Komma "," voneinander zu trennen	
  *	Weight	     : Reihenfolge, in der die Bilder in der Gallerie erscheinen sollen
  *	Picture      : Dateiname des jeweiligen Bilds. Die Schreibweise/Groß-Kleinschreibung muss exakt mit dem Dateinamen des Bilds übereinstimmen. Auch die Dateiererweiterung (z.B. „.jpg“) ist genauso anzugeben (Groß-/Kleinschreibung), wie sie im Dateinamen des Bilds angegeben ist.

Die Beschreibungstexte, sowohl für die gesamte Galerie in der description.txt als auch für die einzelnen Bilder in der list.csv, können [Markdown-Formatierungen](https://www.heise.de/mac-and-i/downloads/65/1/1/6/7/1/0/3/Markdown-CheatSheet-Deutsch.pdf) z.B. für fett und kursiv enthalten. Wenn der Text ein Markdown-Zeichen, z.B ein "\*" enthält, dieses aber nicht als solches funktionieren soll, muss es durch einen vorangestellen Schrägstrich ("\\") als normales Zeichen gekennzeichnet sein (Escape-Sequenz), also z.B. '\\*'.
Wenn vorhanden, sollten die Namen von Fotografen und Konstrukteuren dem passenden Eintrag in der [Liste der Konstrukteure](https://www.ftcommunity.de/konstrukteure/) entsprechen.

Diese Dateien legst du dann zusammen mit den Bildern in eine zip-Datei und sendest diese dann bitte per Mail an website-team@ftcommunity.de. In der Mail benötigen wir dann noch die Information, wo du deine Galerie abgelegt haben möchtest. Dazu kannst du in der Bildersammlung über das Menü den entsprechenden Bereich auswählen und die URL aus der Adressleiste deines Internet-Browsers herauskopieren. Eine Beispiel-zip-Datei mit allen erforderlichen Dateien und ein paar Beispielbildern findest du [hier](./TestUpload.zip). 

Bitte beachtet die Hinweise zum Fotografieren von Modellen, die in mehreren ft:pedia-Artikeln gegeben wurden. Außerdem sind aussagekräftige Titel und Beschreibungen hilfreich.

### Geht das auch anders?

Der direkte Upload neuer Bilder mit dem ft Community Publisher (https://www.ct-systeme.com/Seiten/go-publisher.aspx) steht noch aus. Dennoch können Publisher-User den Publisher nutzen, um Bilder hochzuladen. Das geht so:

1. Das Publisher-Projekt "wie gewohnt" erzeugen, mit Bildern und Texten versehen. Wer Microsoft Word installiert hat, kann auch die Rechtschreibkorrektur nutzen, bevor veröffentlicht wird.

2. "Veröffentlichen", aber nicht zur ft Community-Website, sondern ins Dateisystem (das ist eines der wählbaren Veröffentlichungs-Ziele). Das ergibt standardmäßig einen Unterordner "published" unterhalb der Projektdatei, und in dem landen die gerenderten Bilder. Ihr könnt aber auch jeden anderen Ordner als Ziel angeben. Die so gespeicherten Bilder sind genau dieselben, die auch beim automatischen Hochladen verwenden würden. Da sind also alle Bildbearbeitungen wie Drehen, Beschneiden, Schärfen, Helligkeit, Kontrast, Gammakorrektur abgearbeitet.

3. **Alle Bilder dieses Ordners** plus die Publisher-Datei (die **.ftcpub-Datei**, da sind die erfassten Texte und die Bildreihenfolge drin) an website-team@ftcommunity.de mailen. Falls ihr Microsoft Outlook benutzt, könnt ihr den ganzen "published"-Ordner plus die Publisher-Datei einfach vom Explorer auf den Posteingang von Outlook ziehen und wegschicken. Rechte Maustaste, "Senden an", "Mailempfänger" sollte auch gehen, wenn irgend ein Mailprogramm installiert ist.

Für die Generierung der Galerie aus den Publisher-Dateien oder aus einer Tabelle nach dem oben angegebenen Format haben wir Skripte. Wir können uns prinzipiell die Daten auch zusammen sammeln, wenn sie in einem anderen Format vorliegen. Das ist aber sehr viel mehr Aufwand und es wird dann deutlich länger dauern, bis die Bilder auf der Webseite erscheinen. Bitte liefert uns daher die Bildbeschreibungen als Tabelle oder als ftpub-Datei.

### Nutzungsbedingungen

Die Nutzungsbedingungen ergeben sich aus dem üblichen Vorgehen und daraus, dass jeder Hochlader weiß, was mit den Bildern oder sonstigen Dateien passiert.

Mit dem Einschicken der Bilder gibst du uns (also dem Betreiber der Seite) das Recht, die Bilder und die dazugehörigen Daten unbefristet auf unserer Website darzustellen. 
Wir dürfen sie für diesen Zweck auch bearbeiten, z.B. Thumbnails erstellen.
Wir versprechen nicht, dass die Bilder und sonstigen Dateien für immer auf unserem Server liegen und zugänglich sind.

Mit dem Einsenden sicherst du zu, dass du die Rechte an den Bildern und sonstigen Daten hast, und dass du die Rechte anderer (z.B. die Urheberrechte des Konstrukteurs und des Fotografen, 
das Recht auf informationelle Selbstbestimmung von genannten oder abgebildeten Personen, usw.) nicht verletzt.

Wir löschen lediglich auf Anforderung mit begründetem Rechtsanspruch hin. Es gibt keinen Anspruch auf nachträgliche Bild- oder Text-Änderungen oder -Austausch, weil wir dazu nicht die Kapazität haben.

Für Dritte gilt automatisch "Alle Rechte vorbehalten", dh. wenn jemand die Bilder und Dateien anders nutzen will als sie anzugucken, muss er den Rechteinhaber fragen.

Dass die Namen von Konstrukteuren und Fotografen und damit personenbezogene Daten gespeichert werden, versteht sich von selbst.

### Kann ich auch Dritten eine Nutzungslizenz einräumen?

Es ist möglich, eine Lizenz für Bilder oder andere Dateien auf der Webseite anzugeben, und diese wird auch dargestellt.
Eine Lizenz betrifft die Nutzung der Bilder oder sonstigen Dateien durch Dritte. Wenn beim Hochladen nichts anderes angegeben wird, gilt weiterhin automatisch "Alle Rechte vorbehalten".

Der Rechteinhaber ist bei Bildern der Fotograf, bei Modellen der Konstrukteur und bei Texten und Programmen der Autor, also nicht der Nutzer, der die Bilder hochgeladen hat, und nicht der Betreiber der Webseite. Der Betreiber der Webseite hat nur das Recht, die Dateien auf der Webseite darzustellen und sie für diesen Zweck wenn nötig zu bearbeiten. Wenn jemand anderes die Bilder oder Dateien verkaufen oder auf seiner eigenen Webseite zeigen willen, dann muss er den jeweiligen Fotografen, Konstrukteur oder Autor fragen. Wenn jemand eine Lizenz angibt, dann gibt er damit anderen die Erlaubnis das Werk so zu nutzen, wie es in der Lizenz steht. Der Fotograf, Konstrukteur oder Autor verliert damit selbstverständlich nicht die Rechte an seinem Werk.

Die Lizenz hat für euch und für spätere Nutzer den Vorteil, dass sie euer Werk nutzen können, ohne nochmal nachzufragen (wohlgemerkt, nach den Regeln der jeweiligen Lizenz). Wenn ihr also anderen erlauben wollt, ein Bild oder eine Datei, die ihr hier hochladet, zu nutzen, dann sucht euch eine Lizenz aus, am einfachsten hier: https://creativecommons.org/choose/?lang=de . Diese Lizenz gebt ihr beim Einschicken an. Wenn ihr das nicht wollt, oder euch das egal ist, dann braucht ihr nichts machen. Natürlich könnt ihr das für jede einzelne Datei anders machen oder ganz bleiben lassen.

Die Vergabe einer Lizenz hat nichts damit zu tun, dass ihr selbst die Rechte an eurem Werk haben müsst und selbst sicherstellen müsst, dass ihr die Rechte anderer (z.B. evtl. abgebildeter Personen) nicht verletzt.

### Gibt es eine Größenbeschränkung für die Bilder?

Eigentlich nicht. Wenn ihr sehr viel Platz braucht, könnte ihr die Daten bei einem Webhoster zwischenlagern und uns die Adresse nennen. 
Das ist auch eine Möglichkeit, wenn ihr aus irgendwelchen Gründen keine E-Mail senden wollt.

### Können auch Videos veröffentlicht werden?

Leider nein. Videos, wenn sie denn aussagekräftig sein sollen, werden schnell so groß, dass wir beim Server noch in einer ganz anderen Liga spielen müssten, leider.

### Früher ging das doch alles viel einfacher?

Früher gab es tatsächlich die Möglichkeit, als angemeldeter Nutzer Bilder direkt hochzuladen. Dieser alte Bilderpool war unwartbar und wurde nie aktualisiert. Daher wurde er gehackt.
Nach der Entdeckung dieses Vorfalls wurde das verwendete Programm abgeschaltet, so dass der alte Bilderpool nur noch für Lesezugriffe zur Verfügung stand.
Die dazugehörigen Benutzerdaten können nicht mehr verwendet werden.

Seit Weihnachten 2019 ist jetzt unsere neue Website verfügbar. 
Die Entwickler haben viel Wert auf ein sicheres und mit wenig Aufwand wartbares System gelegt. 
Eine Benutzerschnittstelle zum Hochladen von Bildern existiert leider noch nicht.

