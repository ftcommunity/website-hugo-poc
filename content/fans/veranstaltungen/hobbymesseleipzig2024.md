---
title: Hobbymesse Leipzig
layout: "event"
date: 2024-08-18T10:45:52+01:00
uploadBy: EstherM
location: "Leipziger Messegelände"
termin: 2024-09-20T10:00:00+01:00
letztertag: 2024-09-22T18:00:00+01:00
flyer: logo-hobbymesseleipzig-rgb-farbe.jpg
---


Die [Hobbymesse Leipzig](https://www.hobbymesse.de/de/) (ehemals modell-hobby-spiel) findet vom 20. bis 22. September 2024 auf dem Messegelände Leipzig statt.

Unser Stand ("[fischertechik community](https://www.hobbymesse.de/aussteller-produkte/aussteller/hobbymesse/fischertechnik-community-christian-wiechmann/426178)") befindet sich in Halle 5, Stand D07.

Es erwarten euch wieder verschiedenste Eigenkonstruktionen von einfachen Kleinmodellen bis zu naturgetreuen Modellen von Baumaschinen.



### Öffnungszeiten für Besucher

- Freitag 10-18 Uhr
- Samstag 10-18 Uhr
- Sonntag 10-17 Uhr



[![Logo Hobby Leipzig](../logo-hobbymesseleipzig-rgb-farbe.jpg)](../logo-hobbymesseleipzig-rgb-farbe.jpg)
