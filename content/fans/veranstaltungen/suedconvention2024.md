---
title: ftc:süd:con:24
layout: "event"
date: 2024-807-06T10:00:00+01:00
uploadBy: Website-Team
location: "Fördertechnik Museum Sinsheim"
termin: 2024-10-12T10:00:00+01:00
letztertag: 2024-10-13T17:00:00+01:00
flyer: plakat2024v1_2DE.png

---
## Südconvention 2024 - ftc:süd:con:24
mit Modellschau

**12\. - 13. Oktober 2024**
 
[Fördertechnik Museum Sinsheim](https://www.foerdertechnik-museum.de/)  
Untere Au 4  
74889 Sinsheim

Diesmal wird auch die Firma fischertechnik wieder mit dabei sein.
Anmeldung für Aussteller bitte bis zum 15.09.2024 über das [Online-Formular](https://forms.gle/46RT8tBybzyFXqrL6).
Bitte frühzeitig anmelden, da wir dieses Mal deutlich früher planen müssen.
Das Orga-Team ist erreichbar unter suedconvention@ftcommunity.de .

Anmeldung verlängert bis **5. Oktober.**

[![ftc:süd:con:24](../plakat2024v1_2DE.png)](../plakat2024v1_2DE.pdf)


EN: Special guest: fischertechnik company. To register, please use the [online form (available in English)](https://forms.gle/46RT8tBybzyFXqrL6)
Registration is open until September 15. Please register as soon as possible to allow for early preparation. Contact: suedconvention@ftcommunity.de

Registration extended **October 5.**

[![ftc:süd:con:24](../plakat2024v1_2EN.png)](../plakat2024v1_2EN.pdf)
