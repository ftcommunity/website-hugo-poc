﻿---
title: Maker Faire Ruhr 2024
layout: "event"
date: 2024-02-14T10:45:52+01:00
uploadBy: EstherM
location: "DASA Dortmund"
termin: 2024-03-16T00:00:00+01:00
flyer: MFR_24_Ichbindabei.png
---
Da es im letzten Jahr einfach nur genial war...

Der ftc Modellbau e.V ist wieder bei der [Maker Faire Ruhr](https://makerfaire-ruhr.com) dabei.

Termin: 16. und 17.03.24, 10-18 Uhr

Ort: [DASA Arbeitswelt Ausstellung](https://www.dasa-dortmund.de/), Friedrich-Henkel-Weg 1-25, 44149 Dortmund


[![Maker Faire Ruhr - Ich bin dabei](../MFR_24_Ichbindabei.png)](../MFR24_Ichbindabei.png)
