---
title: Modellschau 2025
layout: "event"
date: 2024-12-02T15:45:52+01:00
uploadBy: Website-Team
location: "im Kardinal-von-Galen-Gymnasium Münster-Hiltrup"
termin: 2025-01-19T00:00:00+01:00
letztertag: 2025-01-19T17:00:00+01:00
flyer: Plakat_ft_2025.png
---
fischertechnik - Große Modellschau

**Sonntag, 19. Januar 2025**  
10:00 - 17:00 Uhr  

Mensa des Kardinal-von-Galen-Gymnasiums  
Zum Roten Berge 25  
48164 Münster-Hiltrup  

**Eintritt frei**

Es werden kostenlose Lötkurse (kleine elektronischen Schaltungen) für Kinder von den Funkamateuren aus Münster angeboten.
Es gibt eine Fischertechnik-Spielecke für Kinder.

Es gibt Waffeln, Kaffee und Würstchen.

Personen, Schulen, usw., die auf der Modellschau ausstellen möchten, können sich gerne bei Roland Keßelmann melden (kesselmann@bistum-muenster.de).


[![Plakat Modellschau 2025](../Plakat_ft_2025.png)](../Plakat_ft_2025.pdf)


