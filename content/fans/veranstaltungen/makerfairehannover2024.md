﻿---
title: Maker Faire Hannover 2024
layout: "event"
date: 2023-08-09T10:45:52+01:00
uploadBy: EstherM
location: "Hannover Congress Centrum"
termin: 2024-08-17T10:00:00+01:00
letztertag: 2024-08-18T17:00:00+01:00
flyer: MFH24_Jubiläum.png
---
Die ftc ist wieder mit einem Stand auf der Maker Faire in Hannover vertreten.

Die [Maker Faire](https://maker-faire.de/hannover) findet vom 17. bis 18. August 2024 statt.

Unseren Stand ("[Konstruktionen mit fischertechnik](https://maker-faire.de/maker/konstruktionen-mit-fischertechnik-3/)") findet Ihr unter der Nummer 169 in der Eilenriedhalle. Direkt daneben steht Thomas Püttmann mit seinem Angebot [math-meets-machines](https://maker-faire.de/maker/math-meets-machines/).



### Öffnungszeiten für Besucher

- Samstag 10-18 Uhr
- Sonntag 10-17 Uhr

Achtung: Die Besuchertickets entweder vorher über die Internetseite https://maker-faire.de/hannover/tickets buchen oder, für den selben Preis, an der Tageskasse kaufen. Aber (nochmal) Achtung: An der Tageskasse wird kein Bargeld angenommen. Dort ist nur eine Zahlung mit Karte möglich.


[![Logo Maker Faire Hannover](../MFH24_Jubiläum.png)](../MFH24_Jubiläum.png)
