---
title: "Veranstaltungen"
weight: 1
date: 2025-01-30T00:00:00
uploadBy:
- "Svefisch"
---

Die fischertechnik-Community trifft sich regelmäßig zu Stammtischen und
Conventions sowie auf Modellbau-Messen und Maker Faires. Alle sind
herzlich eingeladen.

### Termine 

*	19. Januar 2025 [Modellschau Münster](modellschau2025)
*	1. bis 2. März 2025 [Modellbau Neumünster 2025](modellbauSH_2025)


### Veranstaltungsübersicht der fischertechnik GmbH

[fischertechnik GmbH](https://www.fischertechnik.de/de-de/ueber-uns/termine)


