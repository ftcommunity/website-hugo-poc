﻿---
title: fischertechnik Nordconvention IdeA
layout: "event"
date: 2024-03-29T13:45:52+01:00
uploadBy: EstherM
location: "Mellendorf/Wedemark"
termin: 2024-04-20T00:00:00+01:00
flyer: IdeA2024.jpg
---

**Samstag, 20. April 2024**

im Schulzentrum in Mellendorf/Wedemark bei Hannover

[![Poster Nordconvention 2024](../IdeA2024.jpg)](../IdeA2024.pdf)

Für Besucher ist die Nordconvention am Samstag, den 20. April von 10 bis 17 Uhr geöffnet.

Ausrichter ist der **Verein zur Förderung des Richard-Brandt-Heimatmuseums Wedemark e.V.** in Kooperation mit dem **ftc Modellbau e.V.**

Es werden wieder belegte Brötchen und diverse Kuchen gegen Spende abgegeben.

## Aussteller-Anmeldung

Eine Anmeldung als Aussteller ist unter nordconvention@ftcommunity.de noch bis zum 15. April möglich.

## Anreise
 
Die Convention findet statt im Forum Campus W Schulzentrum Mellendorf  
Fritz-Sennheiser-Platz 2-3  
30900 Wedemark

Wir freuen uns auf eure Anmeldung.  
Ralf Geerken und Christian Wiechmann
