---
title: Modellbau Neumünster 2025
layout: "event"
date: 2025-01-30T15:45:52+01:00
uploadBy: Website-Team
location: "Holstenhallen 1-4, Neumünster"
termin: 2025-03-01T00:10:00+01:00
letztertag: 2025-03-02T17:00:00+01:00
flyer: modellbauSH_2025.jpg
---
Modellbau-Messe Neumünster

**Samstag, 01. März 2025**, 10:00 - 18:00 Uhr  
**Sonntag, 02. März 2025**  10:00 - 17:00 Uhr  


Holstenhallen 1-4  
Justus-Liebig-Str. 4  
Neumünster  

Auch dieses Jahr sind wir mit einer kleinen Gruppe auf der Modellbaumesse in Neumünster.

Wer Zeit und Lust hat, kann dort neben unserem fischertechnik-Stand mit vielen unterschiedlichen Modellen auch vieles Anderes, was mit Modellbau zu tun hat, bestaunen.


[![Plakat Modellbau Neumünster 2025](../modellbauSH_2025.jpg)](../modellbauSH_2025.pdf)


