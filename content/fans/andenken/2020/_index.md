---
title: "2020"
date: 2021-01-02T00:00:00
weight: 90
layout: "memento"
uploadBy:
- "EstherM"
---

Wegen der Corona-Pandemie war 2020 leider ganz anders als andere Jahre.
Die Nordconvention musste kurzfristig abgesagt werden.
Einige Modelle finden sich in der [Virtuellen Nordconvention](../../../bilderpool/veranstaltungen/nordconvention-2020/).

Zu Ostern gab es ein kleines [Gewinnspiel](../../../bilderpool/modelle/kleinmodelle-zum-nachbauen/ostergewinnspiel-2020/),
bei dem es einen kleinen Kasten aus dem 3D-Drucker zu gewinnen gab.

Für die Südconvention wurde zuerst ein späterer Termin festgelegt, dann wurde sie ganz abgesagt.

In der Vorweihnachtszeit konnten wir uns über einen [Adventskalender](./advent), ohne Schokolade und ohne Gewinne, freuen.

