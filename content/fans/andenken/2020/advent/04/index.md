---
title: "4. Dezember"
linkTitle: "4. Dezember"
publishDate: 2020-12-04T00:00:00+0100
---

In diesem Jahr erinnern wir uns mal an die schönsten Modelle aus den 80ern.

Heute: der Turmdrehkran.

![Turmdrehkran Packung](Turmdrehkran.png).

Die [Anleitung zum Download](39218_1984_BA_MB·Turm-Drehkran.pdf) aus der ft-datenbank.
