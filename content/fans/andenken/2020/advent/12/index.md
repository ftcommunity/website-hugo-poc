---
title: "12. Dezember"
linkTitle: "12. Dezember"
publishDate: 2020-12-12T00:00:00+0100
hidden: true
---
In unserer kleinen Serie "Erinnerungen an die 80er" haben wir heute das Abenteuer-Bau-Buch hervorgekramt.

![Titelbild Abenteuer-Bau-Buch](39020_1985_EB_DasAbenteuerBauBuch.jpg)


[PDF zum Herunterladen](39020_1985_EB_DasAbenteuerBauBuch.pdf)
