---
title: "15. Dezember"
linkTitle: "15. Dezember"
publishDate: 2020-12-15T00:00:00+0100
---

Aus unserer kleinen Serie "Erinnerungen an die 80er" eine der ersten Anleitungen, wie ein Zauberwürfel gelöst werden kann.

![Zauberwürfel](P1050243.jpg)

[Scan zum Herunterladen](scan0014.pdf)

Es handelt sich um den Scan einer Kopie aus den 80ern, die sich bei Aufräumarbeiten in einer Kiste fand. Daher ist die Qualität leider nicht ganz so gut.

Quelle: bild der Wissenschaft, Heft 12, 1980.

Auch nicht mehr ganz neu, aber noch lange nicht so alt, ist das Modell, das auf unserem [Youtube-Kanal](https://www.youtube.com/watch?v=IpsK0KbQJoI) zu sehen ist.


     
