---
title: "2. Dezember"
linkTitle: "2. Dezember"
publishDate: 2020-12-02T00:00:00+0100
---

Heute können wir uns über eine [neue Funktion](/schlagworte) auf unserer Website freuen.

[![Screenshot Schlagwort Advent](Screenshot.png)](/schlagworte/advent)

     
