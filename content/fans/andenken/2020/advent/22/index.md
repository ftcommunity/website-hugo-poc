---
title: "22. Dezember"
linkTitle: "22. Dezember"
publishDate: 2020-12-22T00:00:00+0100
---

Heute gibt es noch mal ein Rätsel. Die Fragen stehen übrigens in dem Dokument, das vom Bild aus verlinkt ist. 

[![Kreuzworträtsel](Kreuzwort.jpg)](Kreuzwort.pdf)

Die Lösung gibt es am 24. Dezember.

Danke an Peter Habermehl.



