---
title: "8. Dezember"
linkTitle: "8. Dezember"
publishDate: 2020-12-08T00:00:00+0100
---
Liebe fischertechnik-Freunde,

heute suchen wir die Farbe eines Bauteils. Löst dazu das Sudoku. 
Die farblich markierten Felder ergeben
– in üblicher Leserichtung kombiniert – die Nummer eines kleinen, recht seltenen Bauteils.
So etwas wie eine Blaue Mauritius der ft-Teile. Aber blau ist das Teil nicht...


[![Sudoku-Vorschau](Sudoku.png)](Sudoku-Rätsel.pdf)

Danke an Peter Habermehl.

Die Auflösung gibt es am 24.Dezember.





     
