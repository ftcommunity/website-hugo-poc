---
title: "9. Dezember"
linkTitle: "9. Dezember"
publishDate: 2020-12-09T00:00:00+0100
---

Wir haben für euch im Bilderpool gekramt und dieses [Puzzle](../../bilderpool/modelle/sonstiges/spiele-spielzeug/puzzle/15957/) gefunden:

[![Puzzle1](thumbnail1.jpg)](https://ftcommunity.de/bilderpool/modelle/sonstiges/spiele-spielzeug/puzzle/15957/)

[![Puzzle2](thumbnail2.jpg)](https://ftcommunity.de/bilderpool/modelle/sonstiges/spiele-spielzeug/puzzle/15957/)

Mit 25 der Teile kann man einen großen Würfel bauen. Bestimmt geht das auch mit andersfarbigen Bausteinen.

Der Konstrukteur ([HLGR](https://ftcommunity.de/konstrukteure/hlgr)) möge sich bitte melden, damit wir ihm danken können.






     
