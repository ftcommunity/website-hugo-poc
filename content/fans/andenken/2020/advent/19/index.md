---
title: "19. Dezember"
linkTitle: "19. Dezember"
publishDate: 2020-12-19T00:00:00+0100
---
### Großer Strohstern

![Großer Strohstern](GroßerStrohstern.png)

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|3|35060|I Strebe 120 gelb|oder
|3|146533|I Strebe 120 grün|
|15|36323|S-Riegel 4|

Danke an Peter Habermehl.

