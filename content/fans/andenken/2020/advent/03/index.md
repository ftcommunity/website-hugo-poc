---
title: "3. Dezember"
linkTitle: "3. Dezember"
publishDate: 2020-12-03T00:00:00+0100
---
### Tannenbaum

![Tannenbaum](Tannenbaum.png)

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|3|146529|I Strebe 15 grün|
|7|146531|I Strebe 45 grün|
|1|156502|I Strebe 75 grün|
|1|146532|I Strebe 60 grün|
|15|36323|S-Riegel 4|

Danke an Peter Habermehl.

