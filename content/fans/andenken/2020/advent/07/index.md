---
title: "7. Dezember"
linkTitle: "7. Dezember"
publishDate: 2020-12-07T00:00:00+0100
---
### Christbaumkugel

![Christbaumkugel](Christbaumkugel.png)

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|4|124864|Flachträger 120 rot|
|2|36323|S-Riegel 4|

oder 

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|4|35054|Flachträger 120 gelb|
|2|36323|S-Riegel 4|


Danke an Peter Habermehl.





     
