---
title: "1. Dezember"
linkTitle: "1. Dezember"
publishDate: 2020-12-01T00:00:00+0100
---

![Advent](Advent.jpg)


Unser Adventskalender beginnt wieder mit einem kleinen Modell.

Vielen Dank an Rüdiger Riedel für diesen Adventsgruß!
