---
title: "16. Dezember"
linkTitle: "16. Dezember"
publishDate: 2020-12-16T00:00:00+0100
hidden: true
---

![Fünfstern](Fünfstern.png)


|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|10|31765|I Strebe 90 gelb|
|5|36328|I Strebe 45 gelb|
|5|36457|S-Riegel 8|
|5|36323|S-Riegel 4|

oder ohne Stabilisierung in der Mitte	

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|10|31765|I Strebe 90 gelb|
|10|36323|S-Riegel 4|
  
