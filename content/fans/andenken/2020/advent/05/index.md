---
title: "5. Dezember"
linkTitle: "5. Dezember - Kleinteile"
publishDate: 2020-12-05T00:00:00+0100
---
Vielleicht braucht jemand kurz vor dem 2. Advent noch einen Adventskranz?

![Adventskranz](Adventskranz1.jpg)

Danke an Rüdiger Riedel.
