---
title: "11. Dezember"
linkTitle: "11. Dezember"
publishDate: 2020-12-11T00:00:00+0100
---
### Strebenstern

![Stebenstern](Strebenstern.png)

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|1|31673|Sternlasche 6-armig|
|6|139648|X-Strebe 169,6 rot|
|24|139646|I Strebe 60 rot|
|12|31668|Lasche 21,2|
|6|36324|S-Riegel 6|
|36|36323|S-Riegel 4|

Danke an Peter Habermehl.





     
