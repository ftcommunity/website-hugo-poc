---
title: "6. Dezember"
linkTitle: "6. Dezember - Straßenwalze"
publishDate: 2020-12-06T00:00:00+0100
---
Auch in diesem Jahr bringt uns der Nikolaus ein geniales Kleinmodell zum Nachbauen.

![Seiltänzer](Seiltänzer.png)

[Anleitung](fischertechnikbalanciert.pdf)


Danke an Rüdiger Riedel.



     
