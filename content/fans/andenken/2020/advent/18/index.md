---
title: "18. Dezember"
linkTitle: "18. Dezember"
publishDate: 2020-12-18T00:00:00+0100
---
### Klapper-Rentier

Heute haben wir mal wieder im Bilderpool gekramt und ein [feines kleines Modell](../../bilderpool/modelle/sonstiges/festliches/klapper-rentier/) gefunden.

[![Klapper-Rentier](thumbnail.jpg)](../../bilderpool/modelle/sonstiges/festliches/klapper-rentier/)
     
Danke an den Konstrukteur (thomas004).
