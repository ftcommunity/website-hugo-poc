---
title: "14. Dezember"
linkTitle: "14. Dezember"
publishDate: 2020-12-14T00:00:00+0100
---
### Statikstern

![Statikstern](Statikstern.png)

|Anzahl|Art.-Nr.|Bezeichnung|
|---:|-------:|:-------:|
|16|31011|Winkelstein 30 Grad|
|16|31765|I Strebe 90 gelb|
|16|31982|Federnocken|
|8|32316|Verbindungsstopfen|
|8|35052|Winkelträger V 7,5 Grad, gelb|
|8|36323|S-Riegel 4|
|8|38428|Bauplatte 5 (15x30) mit 3 Teilnuten|


Danke an Peter Habermehl.





     
