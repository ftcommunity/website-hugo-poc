---
title: "24. Dezember"
linkTitle: "24. Dezember"
publishDate: 2020-12-24T00:00:00+0100
hidden: true
---

Die ft:pedia 2020-4 ist erschienen. Vielen Dank an die ft:pedia-Herausgeber, die uns auch in diesem Jahr an Heiligabend wieder ein spannendes Heft bereitstellen.

[![../../ftpedia/2020/2020-4/](../../ftpedia/2020/2020-4/titelseite.png)](../../ftpedia/2020/2020-4/ftpedia-2020-4.pdf)

Außerdem gibt es:
* die [Lösung des Sudokus](Sudoku-RätselLösung.pdf),
* die [Lösung des Bilderrätsels](unterschiede.pdf),
* und die [Lösung des Kreuzworträtsels](Kreuzwort-Loesung.png).







     
