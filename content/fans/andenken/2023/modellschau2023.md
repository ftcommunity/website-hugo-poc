---
title: Modellschau 2023
layout: "event"
date: 2023-01-02T15:45:52+01:00
uploadBy: Website-Team
location: "im Kardinal-von-Galen-Gymnasium Münster-Hiltrup"
termin: 2023-01-29T00:00:00+01:00
flyer: ModellschauMünster2023-3.jpg
---
fischertechnik - Große Modellschau

**Sonntag, 29. Januar 2023**  
10:00 - 17:00 Uhr  

Pausenhalle des Kardinal-von-Galen-Gymnasiums  
Zum Roten Berge 25  
48164 Münster-Hiltrup  

**Eintritt frei**

Es werden kostenlose Lötkurse (kleine elektronischen Schaltungen) für Kinder von den Funkamateuren aus Münster angeboten.
Es gibt eine Fischertechnik-Spielecke für Kinder.

Es gibt Waffeln, Kaffee und Würstchen.

Personen, Schulen, usw., die auf der Modellschau ausstellen möchten, können sich gerne bei Roland Keßelmann melden (kesselmann@bistum-muenster.de).

[![Plakat Modellschau 2023](../ModellschauMünster2023-3.jpg)](../ModellschauMünster2023-3.pdf)


