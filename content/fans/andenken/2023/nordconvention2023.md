﻿---
title: Nordconvention 2023
layout: "event"
date: 2023-01-29T13:45:52+01:00
uploadBy: EstherM
location: "Schulzentrum in Mellendorf/Wedemark bei Hannover"
termin: 2023-04-15T00:00:00+01:00
flyer: NC_2023_Postkarte.jpg
---
Die Nordconvention 2023 findet am

**Samstag, 15. April 2023**

im Schulzentrum in Mellendorf/Wedemark bei Hannover als 

**IdeenAusstellung mit fischertechnik** 

statt.

Für Besucher ist die Nordconvention am Samstag, den 15. April von 10 bis 17 Uhr geöffnet.

--------

Ausrichter ist der **Verein zur Förderung des Richard-Brandt-Heimatmuseums Wedemark e.V.** in Kooperation mit dem **ftc Modellbau e.V.**

[![Poster Nordconvention 2023](../NC_2023_Postkarte.jpg)](../NC_2023_Postkarte.jpg)

Es werden wieder belegte Brötchen und diverse Kuchen gegen Spende abgegeben.

----

## Aussteller-Anmeldung

Anmeldung bitte an [nordconvention@ftcommunity.de](mailto:nordconvention@ftcommunity.de).

Aufgebaut werden kann am Freitag, den 14. April von 17 Uhr bis 21 Uhr und am Samstag ab 7 Uhr. 

----

## Anreise
 
Die Convention findet statt im Forum Campus W Schulzentrum Mellendorf  
Fritz-Sennheiser-Platz 2-3  
30900 Wedemark

Wir freuen uns auf eure Anmeldung.  
Holger Bernhardt und Ralf Geerken
