---
title: "16. Dezember"
linkTitle: "16. Dezember"
publishDate: 2023-12-16T00:00:00+0100
hidden: true
---
Zu Weihnachten gibt es Kerzen und damit auch Leuchter.

Aus fischertechnik-Teilen lässt sich auch ein realistischer Kerzenständer mit Kerzen und fröhlichen Lichtern basteln.

In diesem Fall mit roten und gelben Grundsteinen und Regenbogen-LEDs

Entwurf und Ausführung: Jeroen Regtien

![Kerzenhalter](kerzenhalter.jpg)
  
