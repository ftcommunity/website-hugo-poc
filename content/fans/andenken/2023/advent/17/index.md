---
title: "17. Dezember"
linkTitle: "17. Dezember"
publishDate: 2023-12-17T00:00:00+0100
---


Mit geometrischen fischertechnik-Teilen lässt sich ganz einfach ein beleuchteter Weihnachtsstern basteln. Die Arbeit an den Kabeln ist eine ganz andere Geschichte ...

Entwurf und Ausführung: Jeroen Regtien

![Stern](stern1.jpg)

![Stern](stern2.jpg)

![Stern](stern3.jpg)

     
