---
title: "8. Dezember"
linkTitle: "8. Dezember"
publishDate: 2023-12-08T00:00:00+0100
---
Dezember ist auch der Monat der Weihnachtsbäume, sei es ein geschmückter Weihnachtsbaum im Garten oder im Haus.

Es gibt viele Varianten, die mit fischertechnik gefertigt werden können. Hier einer mit 60 Grad Winkelsteinen und der andere mit einem Teil aus der neuen Bio-Box mit Lichtern aus Statik-Proberiegeln.

Entwurf und Ausführung: Jeroen Regtien

![Tannenbaum](tannenbaum.jpg)




     
