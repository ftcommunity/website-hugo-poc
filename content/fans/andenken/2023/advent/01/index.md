---
title: "1. Dezember"
linkTitle: "1. Dezember"
publishDate: 2023-12-01T00:00:00+0100
---

Heute zeigen wir euch in unserem virtuellen Adventskalender, wie ein ein Adventskalender aus fischertechnik im echten Leben aussehen kann.

Ein Adventskalender von Jörg Heitkamp


![ft-Kalender](ft-Kalender1.jpg)

![ft-Kalender](ft-Kalender2.jpg) …
