---
title: "14. Dezember"
linkTitle: "14. Dezember"
publishDate: 2023-12-14T00:00:00+0100
---
# Rätsel

Das nächste Rätsel fällt in die Kategorie "unmögliche Konstruktionen", ist aber für einen erfahrenen fischertechnik-Enthusiasten gar nicht so schwer.

Nimm vier graue oder schwarze Scharniersteine und schiebe sie zusammen, bis die Figur auf dem Foto entsteht.

Entwurf und Ausführung: Marianne van Oostenbrugge

![Rätsel](ratsel.jpg)





     
