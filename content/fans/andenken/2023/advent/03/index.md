---
title: "3. Dezember"
linkTitle: "3. Dezember"
publishDate: 2023-12-03T00:00:00+0100
---

In den dunklen Dezembertagen ist es drinnen warm und gemütlich. Zeit, ein Modell zu bauen oder eines der Adventsrätsel zu lösen!

Entwurf und Ausführung: Thomas Habig (Triceratops)

![Vorschau](Advent2023A1.jpg)

[PDF Zum Ausdrucken](Advent2023A1.pdf)

![Vorschau](Advent2023A2.jpg)

[PDF Zum Ausdrucken](Advent2023A2.pdf)


