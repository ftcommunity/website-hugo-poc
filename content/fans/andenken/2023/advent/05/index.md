---
title: "5. Dezember"
linkTitle: "5. Dezember"
publishDate: 2023-12-05T00:00:00+0100
---

# ‘Sinterklaas’ – Sankt Nikolaus

Sinterklaas besucht jedes Jahr im Dezember die Niederlande, um am 5. Dezember mit Kindern seinen Geburtstag zu feiern. Er verteilt Geschenke und liest aus seinem großen Buch, in dem steht, ob sich jedes Kind seit letztem Jahr brav oder frech benommen hat.

Der fischertechnik Sinterklaas besteht aus einer weiß/roten Figur und 3D-gedruckten Accessoires.

Ab heute sind die entsprechenden Dateien auf Thingiverse verfügbar (Suchbegriff “fischertechnik Sinterklaas“).

Entwurf und Ausführung: Hans Wijnsouw

![Sinterklaas](sint1.jpg)

![Sinterklaas](sint2.jpg)

![Sinterklaas](sint3.jpg)
