---
title: "21. Dezember"
linkTitle: "21. Dezember"
publishDate: 2023-12-21T00:00:00+0100
hidden: true
---

Diesmal ein Weihnachtsbaum aus Kugelbahnen, zusammengesetzt mit verschiedenen Winkelblöcken (15, 30, 60) auf eine Drehscheibe 60 und einer langen Achse. Die Lichter sind aus LEDs.

Entwurf und Ausführung: Jeroen Regtien

![Weihnachtsbaum](tannenbaum.jpg)





     
