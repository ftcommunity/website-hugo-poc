---
title: "11. Dezember"
linkTitle: "11. Dezember"
publishDate: 2023-12-11T00:00:00+0100
---
# Kirmes

Es gibt zwar immer noch Weihnachtsmärkte, aber das ist vor allem Nostalgie.

Zum Glück gibt es fischertechnik-Enthusiasten, die sich auf Kirmesmodelle spezialisiert haben und schöne Dauermodelle bauen. Zum Beispiel eine Zuckerwattehütte und ein Karussell.

Entwurf und Ausführung: Jan Willem Dekker

![Kirmes](kirmes1.jpg)

![Kirmes](kirmes2.jpg)

![Kirmes](kirmes3.jpg)

![Kirmes](kirmes4.jpg)
