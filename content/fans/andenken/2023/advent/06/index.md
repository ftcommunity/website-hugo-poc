---
title: "6. Dezember"
linkTitle: "6. Dezember"
publishDate: 2023-12-06T00:00:00+0100
---
# Mondriaan

Piet Mondriaans abstrakte Gemälde mit einer starren geometrischen Flächenstruktur mit schwarzen Linien und farbigen Flächen sind weltberühmt, aber was ist oben oder unten?

Sogar Kunstmuseen gehen manchmal in die falsche Richtung ...

Mit schwarzen 500er-Grundplatten und einem großen Vorrat an fischertechnik-Platten lässt sich eine farbenfrohe Mondrian-Imitation erstellen.

Entwurf und Ausführung: Arjen Neijsen


![Mondriaan](Mondriaan.jpg)




     
