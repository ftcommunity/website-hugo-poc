---
title: "2. Dezember"
linkTitle: "2. Dezember"
publishDate: 2023-12-02T00:00:00+0100
---
Die Herstellung von Minimodellen ist eine interessante Herausforderung. Wie baut man mit möglichst wenigen Teilen ein attraktives und realistisch aussehendes Modell?

Viele Modelle wurden bereits veröffentlicht und hier sind einige Ergänzungen und Variationen.

Wir hoffen, dass die Fotos klar genug sind, um einen Bau zu ermöglichen.

Entwurf und Ausführung: Marc Petit und Jeroen Regtien

![Mini-Modell 1](mini-modelle1.jpg)

![Mini-Modell 2](mini-modelle2.jpg)

![Mini-Modell 3](mini-modelle3.jpg)

![Mini-Modell 4](mini-modelle4.jpg)

![Mini-Modell 5](mini-modelle5.jpg)






     
