---
title: "12. Dezember"
linkTitle: "12. Dezember"
publishDate: 2023-12-12T00:00:00+0100
hidden: true
---

# Tümmler-Männchen

Als ich dieses Modell zum ersten Mal sah, war ich sofort von seiner Kreativität und Originalität beeindruckt.
Sanft rollt der Tumbler-Mann, nur unterstützt durch die Schwerkraft, den Hang hinunter und nutzt dabei vorhandene fischertechnik-Teile auf neuartige Weise.

Entwurf und Ausführung: Heinz Jansen

![Tümmmler-Männchen](tummler1.jpg)

![Tümmmler-Männchen](tummler2.jpg)

![Tümmmler-Männchen](tummler3.jpg)

![Tümmmler-Männchen](tummler4.jpg)

![Tümmler-Männchen](tummler2a.jpeg)

![Tümmler-Männchen](tummler3a.jpeg)