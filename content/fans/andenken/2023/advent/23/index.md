---
title: "23. Dezember"
linkTitle: "23. Dezember"
publishDate: 2023-12-23T00:00:00+0100
hidden: true
---
# Weihnachtskrippe

With Christmas a nativity scene can be seen in many housholds and churches. Fischertechnik is not the most suitable tool to make a nativity scene but it was tried anyway. With some of the earlier presented 3D printed assessories and a few new ones the result is surprising.

Zu Weihnachten ist in vielen Haushalten und Kirchen eine Krippe zu sehen. Fischertechnik ist nicht das geeignetste Werkzeug für die Herstellung einer Krippe, wurde aber trotzdem ausprobiert. Mit einigen der früher vorgestellten und einigen neuen Zubehörteilen aus dem 3D-Drucker kommt man zu einem überraschenden Ergebnis.

Ab heute sind die neuen entsprechenden Dateien auf Thingiverse verfügbar (Suchbegriff “fischertechnik nativity“).

Entwurf und Ausführung: Jeroen Regtien, Hans Wijnsouw, Eric Bernhard

![Krippe](nativity1.jpg)

![Krippe](nativity2.jpg)

![Krippe](nativity3.jpg)




     
