---
title: "20. Dezember"
linkTitle: "20. Dezember"
publishDate: 2023-12-20T00:00:00+0100
hidden: true
---

Aufgrund angelsächsischer Einflüsse findet der Weihnachtsmann in Westeuropa immer mehr Verbreitung und auch fischertechnik kann eine Rolle spielen.

Mit ein paar kleinen 3D-Druck-Anpassungen wird aus einer fischertechnik-Figur ein echter Weihnachtsmann mit Bart, Mütze, Glöckchen und dickem Bauch...

Ab heute sind die entsprechenden Dateien auf Thingiverse verfügbar (Suchbegriff “fischertechnik santa“)

Entwurf und Ausführung: Jeroen Regtien

![Santa](santa1.jpg)

![Santa](santa2.jpg)

![Santa](santa3.jpg)
     
