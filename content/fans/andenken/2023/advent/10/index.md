---
title: "10. Dezember"
linkTitle: "10. Dezember"
publishDate: 2023-12-10T00:00:00+0100
---

Der zweite Weihnachtsbaum besteht aus einer Rückseite aus Öko-Statikteilen und grünen Streben, die mit Prüfriegeln verbunden sind.

Entwurf und Ausführung: Eric Bernhard

![Tannenbaum](tannenbaum10.jpg)


