---
title: "7. Dezember"
linkTitle: "7. Dezember"
publishDate: 2023-12-07T00:00:00+0100
---
# Baubot

Im Jahr 2023 brachte fischertechnik anlässlich der Einführung eines industriellen Bauroboters das neue Modell Baubot auf den Markt, einen beeindruckend beweglichen Roboterarm, der im Bauwesen eingesetzt werden kann.

Das fischertechnik-Modell war zunächst ein Werbegeschenk und nur sehr begrenzt verfügbar, später wurde es für jedermann zugänglich gemacht.
Das Modell ist statisch und verdient unter Kennern keinen Schönheitspreis, sondern ist für sie eher ein Must-Have.

Auf den Bildern sind die „echten“ und Bausatzmodelle zu sehen.

Fotos:  Eric Bernhard


![Baubot](baubot1.jpg)

![Baubot](baubot2.jpg)

![Baubot](baubot3.jpg)

![Baubot](baubot4.jpg)

[Anleitung](baubot.pdf)




     
