---
title: "22. Dezember"
linkTitle: "22. Dezember"
publishDate: 2023-12-22T00:00:00+0100
---
# Rentiere mit Schlitten

Es ist unvermeidlich, dass sich der Weihnachtsmann und der Nikolaus im Dezember treffen und der Weihnachtsmann den Nikolaus zu einer Schlittenfahrt mit Rentieren einlädt.

Entwurf und Ausführung: Jeroen Regtien und Hans Wijnsouw

![Schlitten](schlitte1.jpg)

![Schlitten](schlitte2.jpg)

![Schlitten](schlitte3.jpg)

![Schlitten](schlitte4.jpg)
