---
title: "18. Dezember"
linkTitle: "18. Dezember"
publishDate: 2023-12-18T00:00:00+0100
---

Auch auf dem nostalgischen Weihnachtsmarkt ist die dekorierte Dampfmaschine zu sehen und treibt zahlreiche Kirmesattraktionen an. 

Dieses Modell basiert auf der Dampfmaschine der Classic Line.

Entwurf und Ausführung: Jan Willem Dekker

![Dampfmaschine](dampfmachine.jpg)
