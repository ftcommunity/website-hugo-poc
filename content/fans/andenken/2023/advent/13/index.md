---
title: "13. Dezember"
linkTitle: "13. Dezember"
publishDate: 2023-12-13T00:00:00+0100
---
Zu Weihnachten gibt es viele Geschenke und als Kind habe ich mich immer gefreut, wenn eine fischertechnik-Box unter dem Baum stand.

Diese Geschenke enthalten nicht nur fischertechnik, die Verpackung besteht auch aus fischertechnik-Teilen, nämlich Platten und Antriebsketten.

Entwurf und Ausführung: Arjen Neijsen

![Paket](pakete1.jpg)

![Paket](pakete2.jpg)
     
![Paket](pakete3.jpg)
     
![Paket](pakete4.jpg)
