---
title: "9. Dezember"
linkTitle: "9. Dezember"
publishDate: 2023-12-09T00:00:00+0100
---

Das zweite Triceratops-Rätsel besteht aus drei kleineren Aufgaben, die sowohl sprachliche als auch arithmetische Fähigkeiten auf die Probe stellen.

Entwurf und Ausführung: Thomas Habig (Triceratops)

![Vorschau](Advent2023B.jpg)

[PDF zum Ausdrucken](Advent2023B.pdf)








     
