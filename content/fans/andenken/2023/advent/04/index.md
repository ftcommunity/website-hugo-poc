---
title: "4. Dezember"
linkTitle: "4. Dezember"
publishDate: 2023-12-04T00:00:00+0100
---

Im Dezember sind Outdoor-Sportarten wetterbedingt eingeschränkt und viele Outdoor-Aktivitäten werden ins Fitnessstudio verlegt.
Mit fischertechnik-Teilen lässt sich nahezu ein komplettes Sportgerät herstellen.

Entwurf: Primoz Cebulj  and Jeroen Regtien
Ausführung: Jeroen Regtien

![Sport](sport1.jpg)

![Sport](sport2.jpg)

![Sport](sport3.jpg)
