---
title: "15. Dezember"
linkTitle: "15. Dezember"
publishDate: 2023-12-15T00:00:00+0100
---
Aus den alten 90x180 und 90x90 Grundplatten in Kombination mit Kugelschienen und Verbindern können weitere Pakete zusammengestellt werden.

Entwurf und Ausführung: Jeroen Regtien

![Paket](pakete1.jpeg)

![Paket](pakete2.jpeg)

![Paket](pakete3.jpeg)

     
