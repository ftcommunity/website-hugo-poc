---
title: "24. Dezember"
linkTitle: "24. Dezember"
publishDate: 2023-12-24T00:00:00+0100
hidden: true
---

Die letzte ft:pedia des Jahres 2023 ist erschienen: Ausgabe 2023-4
Vielen Dank an die ft:pedia-Herausgeber!

[![Titelseite](../../../../../ftpedia/2023/2023-4/titelseite.png)](../../../../../ftpedia/2023/2023-4/ftpedia-2023-4.pdf)

Heute gibt es auch die Lösungen für die Rätsel [als PDF zum Download](Advent2023Lösungen.pdf).

An diesem letzten Tag des Adventskalenders 2023 ist es außerdem Zeit, auf die Erfolge der ft-Community in diesem Jahr zurückzublicken.
Ein besonderer und großer Dank geht an die Freiwilligen, die viele Stunden spenden und weiterhin zum Erfolg der FT-Community beitragen:

* Web-Team: Esther, Richard, Holger und Zapfenkiller 
* Moderatoren: DirkW, Holger, Ralf und Robin 
* Südconvention: Tilo und Raphael 
* Nordconvention: Ralf und Holger 
* Vorstand: Ralf, Holger, Stefan, Richard, Esther, Peter
* Ft-Datenbank: Ulrich und Pudy
* ft:pedia: Dirk und Stefan

Ohne diese Freiwilligen wären wir größtenteils auf uns allein gestellt und hätten viel weniger Wissen und Ideen.

![Thankyou](thankyou.jpg)










     
