﻿---
title: Maker Faire Hannover 2023
layout: "event"
date: 2023-07-15T10:45:52+01:00
uploadBy: EstherM
location: "Hannover Congress Centrum"
termin: 2023-08-19T00:00:00+01:00
flyer: icon_Hannover_dt_2023.png
---
Die ftc wird mit einem Stand auf der Maker Faire in Hannover vertreten sein.

Die [Maker Faire](https://maker-faire.de/hannover) findet vom 19. bis 20. August 2023 statt.

Unseren Stand ("Konstruktionen mit fischertechnik") findet Ihr unter der Nummer 125 mitten in der Eilenriedhalle.


[![Logo Maker Faire Hannover](../icon_Hannover_dt_2023.png)](../icon_Hannover_dt_2023.png)
