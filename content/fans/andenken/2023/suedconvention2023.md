﻿---
title: ftc:süd:con:2023
layout: "event"
date: 2023-08-25T10:00:00+01:00
uploadBy: Website-Team
location: "Fördertechnik Museum Sinsheim / Untere Au 4 / 74889 Sinsheim / Germany"
termin: 2023-10-21T10:00:00+01:00
flyer: plakat2023v1_1EN.png

---
Aktuelle Informationen im Forum: https://forum.ftcommunity.de/viewtopic.php?f=39&t=7843

Anmeldung: https://forms.gle/yEToPeLiNUWEQ3hm8


[![ftc:süd:con:2023](../plakat2023v1_1EN.png)](../plakat2023v1_1EN.png)
