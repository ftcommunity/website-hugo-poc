---
title: "22. Dezember"
linkTitle: "22. Dezember"
publishDate: 2021-12-22T00:00:00+0100
---
Heute stellen wir unser Adventsmodell fertig.

![Baustufe4](Bauschritt4.png)

Eine Endloskugelbahn!

Die Inspiration zu diesem Modell war 2012 in Erbes-Büdesheim zu sehen:
https://ftcommunity.de/bilderpool/veranstaltungen/erbes-budesheim/erbes-budesheim-2012/modelle-fredy/35546/
Auch auf der Südconvention 2021 gab es ein ähnliches Modell.

Vielen Dank an Peter Habermehl, der die Anleitung erstellt hat!

[Vollständige Anleitung zum Herunterladen](Weihnachtskugelbahn.pdf)

[ft-Designer-Datei zum Herunterladen](Weihnachtskugelbahn.ftm)
