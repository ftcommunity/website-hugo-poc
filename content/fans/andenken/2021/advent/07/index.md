---
title: "7. Dezember"
linkTitle: "7. Dezember"
publishDate: 2021-12-07T00:00:00+0100
---
Für alle, denen das Warten auf das Sonntagsmodell zu lange dauert, hier die ersten Einzelteile eines anderen mehrstufigen Modells:

![Seitenteil](Modell2-1a.jpg)

![Seitenteile](Modell2-1b.jpg)

![Deckel](Modell2-1c.jpg)


Danke an Rüdiger Riedel.





     
