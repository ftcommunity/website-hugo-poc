---
title: "2. Dezember"
linkTitle: "2. Dezember"
publishDate: 2021-12-02T00:00:00+0100
---

Was kann man alles aus den Bauteilen des Beutelchens "Oldtimer" bauen?
Informationen zum Original gibt es in der ft-datenbank: https://ft-datenbank.de/ft-article/1922 .

Hier und jetzt ein Raumschiff:

![Raumschiff](Raumschiff2.bmp)


Danke an Holger Howey!







     
