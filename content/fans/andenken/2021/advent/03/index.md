---
title: "3. Dezember"
linkTitle: "3. Dezember"
publishDate: 2021-12-03T00:00:00+0100
---

Heute gibt es eine neue kleine Funktion bei der Suche: die [Sortierung nach Datum](../../search/).

[![Screenshot Suche](ScreenshotSuche.png)](../../search/)
