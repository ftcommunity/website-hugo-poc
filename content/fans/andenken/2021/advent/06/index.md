---
title: "6. Dezember"
linkTitle: "6. Dezember"
publishDate: 2021-12-06T00:00:00+0100
---
Für alle Nikoläuse, die auf ihren Einsatz warten, und für alle anderen gibt es heute ein richtiges Kreuzworträtsel.


![Vorschau](Advent2021a.jpg)

[PDF Zum Ausdrucken](Advent2021a.pdf)


Danke an Thomas Habig.

Die Lösung seht ihr am 24.12..



     
