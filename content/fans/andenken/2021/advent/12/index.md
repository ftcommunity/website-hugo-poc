---
title: "12. Dezember"
linkTitle: "12. Dezember"
publishDate: 2021-12-12T00:00:00+0100
hidden: true
---
Am 2. Adventssonntag können wir an unserem Modell weiterbauen.

![Baustufe2](Bauschritt2.png)


[Anleitungsseite zum Herunterladen](Bauschritt2.pdf)
