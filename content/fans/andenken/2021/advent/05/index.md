---
title: "5. Dezember"
linkTitle: "5. Dezember"
publishDate: 2021-12-05T00:00:00+0100
---
An den Adventssonntagen möchten wir in diesem Jahr ein Modell nachbauen.
Was es wird, sehen wir später.

Heute: die erste Baustufe.

![1. Baustufe](Bauschritt1.png)

[Anleitungsseite zum Herunterladen](Bauschritt1.pdf)
