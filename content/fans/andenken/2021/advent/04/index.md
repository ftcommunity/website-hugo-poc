---
title: "4. Dezember"
linkTitle: "4. Dezember"
publishDate: 2021-12-04T00:00:00+0100
---

Eine kleine Sonne, mal eher scheu, mal voll erstrahlt:


![Sonne](sonneneu.jpg)


Danke an Rüdiger Riedel.
