---
title: "19. Dezember"
linkTitle: "19. Dezember"
publishDate: 2021-12-19T00:00:00+0100
---
Auch am 4. Adventssonntag können wir an unserem Modell weiterbauen.
Den letzten Bauschritt schaffen wir in der letzten Woche vor Weihnachten auch noch.

![Baustufe3](Bauschritt3.png)


[Anleitungsseite zum Herunterladen](Bauschritt3.pdf)


