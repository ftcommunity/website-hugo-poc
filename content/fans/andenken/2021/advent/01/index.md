---
title: "1. Dezember"
linkTitle: "1. Dezember"
publishDate: 2021-12-01T00:00:00+0100
---

![Stern](Stern.JPG)


Unser Adventskalender beginnt auch in diesem Jahr wieder mit einem kleinen Modell.

Vielen Dank an Rüdiger Riedel für diesen Adventsgruß!
