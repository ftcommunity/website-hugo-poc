---
title: "24. Dezember"
linkTitle: "24. Dezember"
publishDate: 2021-12-24T00:00:00+0100
hidden: true
---

Auch in diesem Jahr gibt es an Heiligabend ganz viel zu lesen.
Die ft:pedia 2021-4 ist erschienen. Vielen Dank an die ft:pedia-Herausgeber und an alle Autoren!

[![Titelbild](../../../../../ftpedia/2021/2021-4/titelseite.png)](../../../../../ftpedia/2021/2021-4/ftpedia-2021-4.pdf)

Und hier noch die Lösung für das Kreuzworträtsel, das am 6.12. hinter dem Türchen war:

[![Lösung](Lösung-Advent21.png)](Lösung-Advent21.png)









     
