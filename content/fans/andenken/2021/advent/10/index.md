---
title: "10. Dezember"
linkTitle: "10. Dezember"
publishDate: 2021-12-10T00:00:00+0100
---
Aus dem Oldtimer-Beutelchen: ein Mini-Stapler.

![Stapler](Stapler2.bmp)

Danke an Holger Howey.
