---
title: "2021"
date: 2021-12-29T00:00:00
weight: 80
layout: "memento"
uploadBy:
- "EstherM"
---

Auch 2021 war durch die Corona-Pandemie stark beeinträchtigt.

Die Südconvention konnte trotzdem stattfinden und hat allen Beteiligten viel Freude gemacht.

Außerdem hatten wir einen Stand bei der Digital Edition der Maker Faire Hannover.

In der Vorweihnachtszeit hatten wir wieder einen [Adventskalender](./advent).
