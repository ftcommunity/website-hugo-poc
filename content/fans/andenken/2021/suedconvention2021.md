 
---
title: Südconvention 2021
layout: "event"
date: 2021-08-25T08:50.00:00+01:00
uploadBy: Website-Team
location: "im Erlebnismuseum Fördertechnik in Sinsheim"
termin: 2021-10-30T00:00:00+01:00
flyer: plakat2021-v1_5-1.png
---

Am 30. Oktober 2021 findet die fischertechnik Südconvention zum zweiten Mal im Fördertechnik Museum Sinsheim statt. Der Eintritt für das Fördertechnik Museum mit Indoor Spielplatz + den Erlebnistag + Modellausstellung beträgt 11€ für Erwachsene bzw. 8€ für Kinder zwischen 5 und 18 Jahren (Kinder bis einschließlich 4 Jahren sind frei). Für das leibliche Wohl ist gesorgt. Weitere Informationen zu den Attraktionen des Fördertechnik Museums gibt es hier: [https://www.erlebnismuseum-fördertechnik.de/](https://www.erlebnismuseum-fördertechnik.de/)

## Termin:

30\. Oktober 2021 von 10-18 Uhr

## Anfahrt:

Die Convention findet statt im:

Erlebnismuseum Fördertechnik \
Untere Au 4 \
74889 Sinsheim

Das Museum befindet sich direkt neben dem Technik Museum Sinsheim. Parkplätze sind vorhanden.

## Hygienekonzept

Der Besuch des Musuems sowie der Südconvention ist an das Hygienekonzept des Museums gekoppelt. Dazu zählt (Stand 28.08.2021):

- "3G"-Nachweis: geimpft, genesen, getestet
- Tragen eines Mund-Nasenschutzes für alle Personen ab 6 Jahren
- regelmäßiges Desinfizieren der Hände
- Einhaltung des Mindestabstandes von 1.5m

Aktuelle Informationen findet ihr auf der Homepage des Museums: [https://www.erlebnismuseum-fördertechnik.de/](https://www.erlebnismuseum-fördertechnik.de/)

## Poster Südconvention 2021

[![Plakat Südconvention 2021](../plakat2021-v1_5-1.png)](../plakat2021-v1_5.pdf)

## Anmeldung für Aussteller

Wie jedes Jahr erheben wir keine Standgebühr für Aussteller. Aussteller erhalten zudem kostenfreien Eintritt ins Museum. Wir heißen alle Modellbauer von jung bis alt als Aussteller willkommen! Insbesondere freuen wir uns auch über Großmodelle, eine Ausstellungsfläche im Freien ist vorhanden.

Wenn ihr als Aussteller dabei sein wollt, befolgt bitte das Anmeldeverfahren:

- Zur Voranmeldung sendet ihr uns bitte unverbindlich eine kurze, formlose E-Mail mit eurem Namen an suedconvention@ftcommunity.de.
- Wir antworten Anfang Oktober auf eure Mail und wir werden euch bitten, einen Online-Fragebogen auszufüllen, sodass wir mit eurem Besuch der Convention besser planen können.
- Etwa eine Woche vor der Convention werden wir alle Aussteller nochmals per Mail über die wichtigsten organisatorischen Abläufe informieren.

Hinweis: Modelle können mit Fahrzeugen bis direkt an die Halle gebracht werden. Der Auf- bzw. Abbau ist auch mehrere Tage vor bzw. nach der Convention möglich (z.B. für Großmodelle oder Gemeinschaftsprojekte).

Anmeldeschluss ist der 10. Oktober 2021. Wir freuen uns auf eure Modelle!

## Anmeldung zum Flohmarkt / Tauschbörse

Ihr möchtet euch von (einem Teil) eurer fischertechnik Sammlung trennen? Oder habt ihr beispielsweise in den letzten Monaten an einem Elektronikprojekt gearbeitet und möchtet einige übrige Exemplare an Interessierte aus der Community verkaufen? Oder habt ihr einzigartige 3D-Drucke angefertigt und möchtet diese Raritäten mit der Community teilen? Für diese und weitere Fällen möchten wir den Flohmarkt bzw. die Tauschbörse einrichten. Wir erheben auch hierfür keine Standgebühr. Ob ihr verkaufen, tauschen, verschenken oder versteigern wollt obliegt eurer Entschiedung. Bei Fragen oder zur Anmeldung bitte eine Mail an suedconvention@ftcommunity.de schreiben. Für diejenigen, die sich auch als Aussteller anmelden, reicht es aus, dies im Fragebogen anzugeben.

Ansprechpartner: Tilo Rust (ClassicMan) und David Holtz (davidrpf)

Euer Organisationsteam

