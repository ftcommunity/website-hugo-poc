---
title: "Vorträge Südconvention 2019"
date: 2019-10-09T17:38:47+02:00
draft: false
---
# Vortragsreihe

Im Rahmen der Vortragsreihe wurden Besucherinnen und Besucher der Südconvention fünf spannende Fachvorträge geboten. Freundlicherweise haben die Vortragenden eine Kopie ihrer Präsentationen zum Download zur Verfügung gestellt.

#### Willkommen: Neustart in Sinsheim  
##### 10:30 – 10:40 *Organisationsteam* 

#### Die neue ftcommunity-Website: Anforderungen, Umsetzung, Mitmachen 
##### 10:45 – 11:15 *Esther Mietzsch*    ([PDF](../Esther-ft-20190921.pdf))

####  Vorstellung Elektronikmodule 
##### 11:30 – 12:15 *Hans-Christian Funke* ([PDF](../Präsentation-Elektronikmodule-SüdConvention.pdf), [Fotoalbum](../FotoalbumElektronikmodule.pdf))

#### nanoFramework – Softwaretechnik, Mikrocontroller und fischertechnik
##### 13:00 – 13:30 *Stefan Falk* ([PDF](../nanoFramework.pdf))

#### Schrägseilbrücke & ft:express - Konstruktion und Entwicklung mit Fischertechnik 
##### 14:00 – 14:30 *Tilo Rust* ([PDF](../TiloVortrag2019.pdf))

#### Vorstellung des ftPwrDrive: Schrittmotoren & fischertechnik 
##### 15:00 – 15:30 *Stefan Fuss* ([PDF](../Vortrag-ftPwrDrive-Convention19.pdf))

