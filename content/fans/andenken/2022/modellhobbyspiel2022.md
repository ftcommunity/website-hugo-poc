 
---
title: Modell-Hobby-Spiel
layout: "event"
date: 2022-07-13T08:50.00:00+01:00
uploadBy: Website-Team
location: "Leipziger Messe"
termin: 2022-10-02T00:00:00+01:00
flyer: mhs22.jpg
---

Die ftc wird mit einem Stand auf der "Modell-Hobby-Spiel" in Leipzig vertreten sein.

Die Messe findet vom 30.09. - 02.10.2022 statt.

Leipziger Messe \
Messe-Allee 1 \
04356 Leipzig

Franz Santjohanser ist wohl ebenfalls mit einem Stand vertreten.

Es werden noch Mitstreiter gesucht, welche Lust und Zeit haben dort auszustellen.
Die Veranstaltung ist für Teilnehmer kostenlos.
Tische und Stühle sowie Stromanschlüsse werden gestellt.

Meldet Euch bitte bei Christian (https://forum.ftcommunity.de/viewtopic.php?f=39&t=7343#p56579)


[![Logo Modell Hobby Spiel](../mhs22.jpg)](../mhs22.jpg)



