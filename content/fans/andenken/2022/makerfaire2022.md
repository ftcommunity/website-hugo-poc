 
---
title: Maker Faire
layout: "event"
date: 2022-08-13T08:50.00:00+01:00
uploadBy: Website-Team
location: "Hannover Congress Centrum"
termin: 2022-09-10T00:00:00+01:00
flyer: MFH_22_Icon.png
---

Die ftc wird mit einem Stand auf der Maker Faire in Hannover vertreten sein.

Die [Maker Faire](https://maker-faire.de/hannover) findet vom 10.09. - 11.09.2022 statt.


[![Logo Maker Faire Hannover](../MFH_22_Icon.png)](../MFH_22_Icon.png)



