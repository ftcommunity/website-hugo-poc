 
---
title: "Südconvention 2022"
layout: "event"
date: 2022-09-20T15:50.00:00+01:00
uploadBy: Website-Team
location: "im Erlebnismuseum Fördertechnik in Sinsheim"
termin: 2022-10-02T00:00:00+01:00
flyer: plakat2022-v1_1-DE-klein.png
---

[![Plakat Südconvention 2022](../plakat2022-v1_1-DE-klein.png)](../plakat2022-v1_1-DE.png)

Modellschau der ftc:süd:con:2022 im Fördertechnik Museum Sinsheim (Untere Au 4, D-74889 Sinsheim).
Dieses Jahr erstmals 2-tägig und mit "Come-Together Party" am Samstag.
Aussteller in jedem Alter willkommen - bringt eure Modelle! Anmeldung erforderlich bis 27.09.2022: https://forms.office.com/r/nAW31XdcZe
Mehr Infos im Forum: https://forum.ftcommunity.de/viewtopic.php?f=39&t=7394

---------
English Version:
---------
We strongly believe that our 2-day Convention is a great opportunity to join us at the Fördertechnik Museum Sinsheim (Untere Au 4, D-74889 Sinsheim, Baden-Württemberg). Saturday: Come-Together-Party with BBQ.
If you would like to participate as an exhibitor, please use this form. https://forms.office.com/r/nAW31XdcZe
For updates see forum thread: https://forum.ftcommunity.de/viewtopic.php?f=39&t=7394


