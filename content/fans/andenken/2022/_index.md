---
title: "2022"
date: 2022-12-31T00:00:00
weight: 75
layout: "memento"
uploadBy:
- "EstherM"
---

2022 gab es wieder die Südconvention in Sinsheim, sogar zweitägig.

Zeitgleich hat ein kleines Team die Community auf der Modell-Hobby-Spiel in Leipzig vertreten.

Auch auf der Maker-Faire in Hannover waren wir aktiv.

Und wir hatten einen [Adventskalender](./advent).
