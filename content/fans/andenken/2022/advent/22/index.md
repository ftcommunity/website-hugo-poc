---
title: "22. Dezember"
linkTitle: "22. Dezember"
publishDate: 2022-12-22T00:00:00+0100
---
Kurz vor Heiligabend gibt es noch mal ein technisches Modell.

![Drehteller](Drehteller.jpg)

Vielen Dank an Stefan Falk.

[Vollständige Anleitung zum Herunterladen](Drehteller.pdf)

