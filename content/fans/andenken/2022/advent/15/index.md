---
title: "15. Dezember"
linkTitle: "15. Dezember"
publishDate: 2022-12-15T00:00:00+0100
---
Um mal über den Tellerrand zu schauen, können wir uns heute [einen Bericht](https://www.arte.tv/de/videos/101944-014-A/geschichte-schreiben/) über eine Mechanische Ente aus dem 18. Jahrhundert ansehen.

![Digesting Duck](Digesting_Duck.jpg)

Das "Modell" passt auch gut zu dem mehrjährigen Thema "Früher war alles besser".




     
