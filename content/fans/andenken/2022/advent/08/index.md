---
title: "8. Dezember"
linkTitle: "8. Dezember"
publishDate: 2022-12-08T00:00:00+0100
---
Der Bilderpool bietet noch mehr Tiere.
Hier die [Elektronische Henne aus dem Clubheft 1973](../../../../../bilderpool/modelle/clubmodelle/elektronische-henne-clubheft-1973-3/gallery-index/), die 2010 wiederbelebt wurde.

![Elektronische Henne](elektronischehenne06.jpg)



Konstruiert von Stefan Falk und Volker-James Münchhof (qincym). 



     
