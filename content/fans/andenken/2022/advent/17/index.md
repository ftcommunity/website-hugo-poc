---
title: "17. Dezember"
linkTitle: "17. Dezember"
publishDate: 2022-12-17T00:00:00+0100
---

Und noch mal eine praktische Übersicht, heute über Sensoren und Lampen bzw. LEDs.

![Übersicht Elektrik](ÜbersichtElektrik.jpg)

[PDF zum Download](ÜbersichtElektrik.pdf)

Danke an Holger Howey.



     
