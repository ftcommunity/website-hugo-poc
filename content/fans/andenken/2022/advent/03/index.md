---
title: "3. Dezember"
linkTitle: "3. Dezember"
publishDate: 2022-12-03T00:00:00+0100
---

Wenn es bei fischertechnik um Tiere geht, sollte es auch um Fische gehen.

Hier also jetzt: der bissige Hecht


![Der bissige Hecht](DerbissigeHecht.JPG)


Danke an Rüdiger Riedel.

