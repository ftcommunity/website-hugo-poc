---
title: "11. Dezember"
linkTitle: "11. Dezember"
publishDate: 2022-12-11T00:00:00+0100
---

Heute wieder eine Bauanleitung für eine Freundin der "Animal Friends", diesmal "Carla the Crab".

![Carla the Crab](CarlaTheCrab.png)

[Bauanleitung als PDF](CarlaTheCrab.pdf)

Danke an Holger Howey.
