---
title: "10. Dezember"
linkTitle: "10. Dezember"
publishDate: 2022-10-10T00:00:00+0100
---
Heute mal etwas praktisches: eine Übersicht über die fischertechnik-Motoren.

![Übersicht Motoren](ÜbersichtMotoren.jpg)

[PDF zum Download](ÜbersichtMotoren.pdf)

Danke an Holger Howey.
