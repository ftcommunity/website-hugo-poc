---
title: "4. Dezember"
linkTitle: "4. Dezember"
publishDate: 2022-12-04T00:00:00+0100
---

Am Adventssonntag gibt es eine Bauanleitung, und zwar für James und Jenny, Freunde der "Animal Friends".

![James and Jenny the Dragonbirds](JamesandJenny.png)

[Bauanleitung als PDF](JamesandJennytheDragonbirds.pdf)

Danke an Holger Howey.
