---
title: "19. Dezember"
linkTitle: "19. Dezember"
publishDate: 2022-12-19T00:00:00+0100
---
Gegen Ende der Adventszeit jetzt noch einmal ein Fisch: die gemütliche Makrele.

![GemütlicheMakrele](DiegemütlicheMakrele.JPG)

Danke an Rüdiger Riedel.


