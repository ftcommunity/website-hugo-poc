---
title: "24. Dezember"
linkTitle: "24. Dezember"
publishDate: 2022-12-24T00:00:00+0100
hidden: true
---

Die letzte ft:pedia des Jahres 2022 ist erschienen: Ausgabe 2022-4
Vielen Dank an die ft:pedia-Herausgeber!

[![Titelbild](../../../../../ftpedia/2022/2022-4/titelseite.png)](../../../../../ftpedia/2022/2022-4/ftpedia-2022-4.pdf)

Außerdem die [Lösungen für die Rätsel](Lösungen2022.pdf)









     
