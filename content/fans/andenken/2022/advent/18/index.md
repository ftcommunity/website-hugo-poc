---
title: "18. Dezember"
linkTitle: "18. Dezember"
publishDate: 2022-12-18T00:00:00+0100
---

### Zyklisch ungleichförmig übersetzendes Getriebe

Am 4. Adventssonntag gibt es wieder eine Anleitung, diesmal sehr technisch, und zwar für ein zyklisch ungleichförmig übersetzendes Getriebe.
     

![Zyklisch ungleichförmig übersetzendes Getriebe](Getriebe.jpg)


[Bauanleitung als PDF](Getriebe.pdf)

Danke an Stefan Falk für die Konstruktion und Peter Habermehl für die Anleitung.
Die Theorie hinter diesem Modell wird Stefan Falk demnächst ausführlicher darstellen.
