---
title: "12. Dezember"
linkTitle: "12. Dezember"
publishDate: 2022-12-12T00:00:00+0100
hidden: true
---
Und wieder ein paar Tierchen: die fröhlichen Hühner.

![Fröhliche Hühner](DiefröhlichenHühner.JPG)


Danke an Rüdiger Riedel.
