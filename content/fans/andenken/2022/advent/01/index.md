---
title: "1. Dezember"
linkTitle: "1. Dezember"
publishDate: 2022-12-01T00:00:00+0100
---

![Rentier](Weihnachtsschlitten_2.jpg)


Zum Einstieg in den diesjährigen Adventskalender gucken wir uns zunächst in unserer großen Sammlung von Modellbildern um und betrachten dieses nette kleine [Rentier](../../../../../bilderpool/modelle/sonstiges/festliches/weihnachtsgruss/7876).

Der Konstrukteur war thomas004.
