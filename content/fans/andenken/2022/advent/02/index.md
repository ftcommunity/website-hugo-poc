---
title: "2. Dezember"
linkTitle: "2. Dezember"
publishDate: 2022-12-02T00:00:00+0100
---

Heute suchen wir, was es alles in der ft:pedia zum Thema "Tier" gibt:

![Bildschirmfoto](Screenshot-Sucheinderftpedia2.png)


Dies ist nur ein Bildschirmfoto. Die ganz neue Funktion der Volltextsuche in allen ft:pedia-Ausgaben gibt es unter [Suche](../../../../../search/ftpedia-search).







     
