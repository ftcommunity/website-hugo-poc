---
hidden: true
layout: "file"
title: "2017 Süd-Convention"
date: 2017-07-22T00:00:00
file: "suedconvention_2017.pdf"
konstrukteure:
- "Stefan Fuss"
uploadBy:
- "EstherM"
license: "unknown"
---
Plakat zur Convention Dreieich 2017
