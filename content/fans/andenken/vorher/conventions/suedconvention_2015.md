---
hidden: true
layout: "file"
title: "2015 Convention"
date: 2015-07-15T00:00:00
file: "suedconvention_2015.pdf"
konstrukteure:
- "Martin Westphal (Masked)"
uploadBy:
- "Masked"
license: "unknown"
legacy_id:
- /downloads/conventionplakate/plakatconventionklein.pdf
- /data/downloads/conventionplakate/plakatconventiongross.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/conventionplakate/plakatconventionklein.pdf -->
Plakat zur Convention 2015
