---
layout: "file"
hidden: true
title: "Adventslösungen"
date: "2012-01-09T00:00:00"
file: "adventsrtsel.pdf"
konstrukteure: 
- "Thomas Habig (Triceratops)"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/adventsrtsel.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/adventsrtsel.pdf -->
Auflösungen zu den Adventsrätseln 2011
