---
hidden: true
layout: "file"
title: "2003 Fan Club Modell 22 - Mobile Pneumatic Robot"
date: 2007-06-18T00:00:00
file: "fanclubnews_mobilerpneumaticrobot.pdf"
konstrukteure:
- "fischertechnik GmbH"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/fanclubnews/cm200301fcm22mobilerpneumaticrobot.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/fanclubnews/cm200301fcm22mobilerpneumaticrobot.pdf -->
Fan Club Modell 22 - Mobile Pneumatic Robot
