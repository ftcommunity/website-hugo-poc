---
title: "Fan-Club News"
date: 2019-04-11T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /downloadsa2ac.html.html
imported:
- "2019"
---
<!-- http://ftcommunity.de/downloadsa2ac.html?kategorie=Fan+Club+News -->

Seit langem gibt es den fischertechnik FAN-CLUB, dessen Mitglieder per Post News erhalten.  
Redaktion und Urheber: [fischerwerke / fischertechnik GmbH](https://www.fischertechnik.de/de-de/spielen/fan-club/infos-und-anmeldung)  
Einige alte Ausgaben wurden von Sven Engelke und Fabian Seiter gescannt und zur Verfügung gestellt. Vielen Dank!
