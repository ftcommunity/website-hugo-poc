---
title: "Andenken"
date: 2019-04-09T00:00:00
weight: 150
layout: "memento"
uploadBy:
- "Esther"
---

Die ftcommunity ist seit vielen Jahren aktiv. In dieser langen Zeit gab es viele gute Ideen und spannende Conventions,
die ihre Spuren hinterlassen haben. Diese Spuren sollen hier archiviert werden.




