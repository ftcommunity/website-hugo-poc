
---
title: "FAQ: Chat"
weight: 80
date: 2020-01-20T00:00:00
uploadBy:
- "Stefan Falk"
legacy_id:
- /knowhow/umzugsgut/wiki/ex_wiki_faq_chat/
- /wikibf0b.html
- /php/wiki/8
- /ftcomm8cb8.html
- /php/ftcomm/chat
- 
---

### Worum geht es?
Im Chat können wir uns direkt miteinander unterhalten – jedenfalls die, die gerade „da“ sind. 
Der fischertechnik-Chat ist wie alles auf ftcommunity.de kostenlos; jede und jeder sind herzlich eingeladen.

### Wie komme in den Chat?
Der Chat ist wie folgt erreichbar:

Unser [Matrix](https://matrix.org)-Homeserver heißt *chat.ftcommunity.de* und bietet den Raum *#ftcommunity* an. 
Der Raum ist im kompletten Matrix-Netzwerk sichtbar.
Ihr könnt dafür ein Konto von jedem beliebigen Matrix-Server nutzen, der am Matrix-Verbund teilnimmt.
Wenn ihr bereits im Forum angemeldet seid, könnt ihr euch auch mit eurer Forums-Anmeldung bei unserem Homeserver anmelden. 
Beim ersten Anmelden wird dann automatisch ein passendes Matrix-Konto für euch angelegt[^1]. 
In Matrix sind für Benutzernamen nur die Kleinbuchstaben a-z, die Ziffern 0-9 sowie die Sonderzeichen =_-./ zugelassen.
Wenn euer Benutzername andere Zeichen (z.B. Umlaute, Leerzeichen usw.) enthält, könnt ihr den Chat leider nicht mit dem Forums-Login nutzen.
Zum Login einfach den Homeserver  auf *chat.ftcommunity.de* setzen, anschließend auf *ftc Login* drücken. Die Anmeldemaske ist Teil unseres zentralen Authentifizierungsdienstes und fragt euch derzeit nach den euch bekannten Forums-Zugangsdaten.

    Ihr könnt auch einen „Matrix-Client“ nutzen. 
    Ein Beispiel ist <https://app.element.io>. 

### Wo ist der IRC Server hin?

IRC ist ein in die Jahre gekommenes Protokoll und dies neben Matrix zu unterstützen ist relativ aufwendig. Daher haben wir uns entschieden ausschließlich auf Matrix zu setzen. Matrix bietet auch einen großen Vorteil gegenüber IRC: Nachrichten kommen auch dann an, wenn du offline bist und erst viel später die Nachrichten abrufst.

### Was muss ich noch wissen?
Nichts. Wenn etwas unklar ist, fragt einfach im Chat nach, oder bei Bedarf im Forum. 
Im Chat erklären euch die alten Hasen auch, was man da noch so tun kann (z.B. „private“ Chats zwischen zwei Teilnehmern führen).

Die Regeln im Chat sind grundsätzlich dieselben wie im Forum. 
Wir sind höflich zueinander, jeder darf alles fragen, ob nun direkt zu fischertechnik-Themen oder auch zu anderen.

Oft sind den ganzen Tag über bis in den späten Abend hinein Leute im Chat, und sei es nur „passiv“. 
Manchmal sind Leute im Chat, können aber gerade nicht lesen oder schreiben, weil sie anderes zu tun haben. 
Habt also bitte Geduld. Sagt einfach mal nett guten Tag, fragt was ihr fragen möchtet  und fangt ruhig eine Diskussion über etwas Interessantes an.

[^1]: Unser automatisch angelegter "Forums-Chat-Account" ist übrigens ebenfalls ein "vollwertiger" Matrix-Account, d.h. man kann mit dem dann auch auf Chats zugreifen, die bei matrix.org bzw. anderen "federated" Servern gehostet werden.
