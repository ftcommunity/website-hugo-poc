---
title: "Website (Technik)"
layout: "techdoc"
stand: 5. Juni 2020
weight: "99"
---
Hier findet ihr Informationen darüber, wie diese Website selbst aufgebaut ist:
Welche Technik wird verwendet. um die Inhalte zu organisieren und darzustellen,
was haben wir uns für die Darstellung der einzelnen Bereiche gedacht usw.
Die Anforderungen und unsere Gedanken zur Architektur haben wir hier: 
https://gitlab.com/ftcommunity/website gesammelt.

Dieser Bereich ist in erster Linie für diejenigen gedacht, 
die beim Aufbau und der Pflege der ftc-Website helfen wollen.
Er kann aber auch nützliche Informationen für Diejenigen liefern,
die für ihre eigenen Website mit Hugo Anregungen suchen.

Die eigentlichen Inhalte für die Website liegen als [Markdown](https://de.wikipedia.org/wiki/Markdown-Dateien) vor.
Diese Dateien werden mit [Hugo](https://gohugo.io/) nach HTML übersetzt.
Die Inhalte sind hier: https://gitlab.com/ftcommunity/website-hugo-poc zugänglich;
unser Theme für Hugo liegt hier: https://gitlab.com/ftcommunity/website-layout.

Die weiteren Abschnitte in diesem Bereich setzen voraus, dass du bereits 
Grundkenntnisse über Markdown und [HTML](http://selfhtml.org/) hast.

Diese Dokumentation ist nicht vollständig,
aber aktuell und nach bestem Wissen korrekt.
Für den Einstieg empfiehlt es sich, zuerst die Seite [Bau mit Hugo](./allgemeines)
und dann die Abschnitte unter [Know-how](./knowhow) und [ft:pedia](./ftpedia) zu lesen.

