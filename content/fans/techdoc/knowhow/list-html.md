---
title: "Übersichtseiten im Know-How-Bereich"
topic: "list.html"
layout: "techdoc"
weight: 50
stand: "9. Juni 2020"
---

## Inhalte auflisten (list.html)

Außer den Inhalten selbst gibt es auch noch die jeweiligen
Übersichtsseiten, mit dem Prädikat _section_.
So eine Übersichtsseite wird in ihrem Ordner durch die Datei `_index.md`
beschrieben.
Unterhalb der _root section_ "Know-how" kümmert sich das Script `layouts/knowhow/list.html`
um die Darstellung dieser Inhalte.

Das Script beginnt mit dem üblichen "Überbau" für eine Seite und einer
dazwischen eingefädelten Konstantendefinition (`$maxChars`).
````html
{{ define "main" }}

  {{ $maxChars := 320 }}

  <div class="padding highlightable">
    {{ partial "topbar.html" . }}

    <div id="body-inner">
````
Direkt danach kommt auch schon der Titel und der Inhalt aus der jeweiligen
`_index.md`-Datei.
````html
      <h1>{{.Title}}</h1>

      {{ .Content }}
````
Nun arbeitet sich das Script durch alle weiteren Unterseiten (_sections_), um
sie aufzulisten und mit einer kurzen Inhaltsangabe zu versehen.
````html
      {{ range .Sections }}
         <hr>
         <h4>
            <a href = "{{ .RelPermalink }}"> <i class="far fa-folder-open"></i> </a>
            -
            <a href = "{{ .RelPermalink }}" style = "color: Black;"> {{ .Title }} </a>
         </h4>
         <p>{{ .Summary | markdownify | safeHTML | truncate $maxChars }}</p>
         <a href = "{{ .RelPermalink }}">Abschnitt öffnen</a>
      {{ end }}
````
Für jede _section_ gibt es zuerst eine horizontale Trennlinie (`<hr>`),
dann folgt der klickbare Eintrag als Überschrift (`<h4>...</h4>`) mit Icon
aus Fontawesome davor.
Dabei ist es Absicht, nur das Icon in 'Linkfarbe' darzustellen, den Seitentitel
jedoch stattdessen schwarz zu setzen.

Der beschreibende Text der Unter_section_ wird mittels `.Summary` geholt,
durch den Markdown-Prozessor geschleust (`markdownify`), das entstandene HTML
als 'sicher, als solches weitergeben' (`safeHTML`) erklärt und schließlich
auf die eingangs definierte Länge `$maxChars` zurechtgestutzt.
Dabei passt Hugo auf, dass keine Worte mittendrin abgeschnitten werden.
Dann gibt es nochmal einen Link zur _section_.
Der ist für die Leute mit dem ganz kleinen Bildschirm gedacht - so muss man
nach dem Lesen der Einleitung nicht wieder hochscrollen.
````html
         <a href = "{{ .RelPermalink }}">Abschnitt öffnen</a>
````
Sind die _sections_ alle aufgelistet, startet eine zweite Schleife, um die
Liste durch alle Unterseiten des Typs _page_ zu ergänzen.
Dieses Konstrukt ist technisch identisch zur vorgenannten Schleife für die
_sections_.
Die Beschreibung wird daher kurzgefasst.
````html
      {{ range .Pages }}
         {{ if eq .Layout "file" }}
            <hr>
            <h4>
               <a href = "{{ .RelPermalink }}"><i class="fas fa-download"></i></a>
               -
               <a href = "{{ .RelPermalink }}" style = "color: Black;"> {{ .Title }} </a>
            </h4>
            <p>{{ .Summary | markdownify | safeHTML | truncate $maxChars }}</p>
            <a href = "{{ .RelPermalink }}">Zum Download</a>
         {{ else if eq .Layout "wiki" }}
            <hr>
            <h4>
               <a href = "{{ .RelPermalink }}"><i class="fas fa-book-reader"></i></a>
               -
               <a href = "{{ .RelPermalink }}" style = "color: Black;"> {{ .Title }} </a>
            </h4>
            <p>{{ .Summary | markdownify | safeHTML | truncate $maxChars }}</p>
            <a href = "{{ .RelPermalink }}">Weiterlesen</a>
         {{ else }}
            {{ errorf "Oh oh, hier fehlt die Ansichtsdefinition für das Layout '%s' in Seite %q" .Layout .Path }}
         {{ end }}
      {{ end }}
````
Die Unterschiede:

*  `range` läuft hier nicht auf `.Sections`, sondern auf `.Pages`.
*  Die Einträge werden nach `.Layout`-Typ aus dem Frontmatter unterschieden.
   Ersatzweise ginge auch `.Params.layout`.
*  Je nachdem ob `layout: "file"` oder `layout: "wiki"` vorhanden ist, wird
   das Icon für den Eintrag ausgewählt.
*  Je nach Layout wird unterhalb der Summary ein passender Linktext verwendet.
*  Ist das Layout unbekannt, wird sich Hugo weigern die Seite zu bauen.
   Das ist die Zeile mit dem `errorf`.

Das Script findet dann seinen Abschluss durch die nötigen
abschliessenden Tags / Befehle.
````html
      <hr>
      <br />
    </div> 
  </div>
{{ end }}
````

