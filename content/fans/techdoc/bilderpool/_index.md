---
title: "Bildersammlung"
layout: "techdoc"
weight: 50
stand: "28. Dezember 2020"
---

Unsere Bildersammlung ist auch unter dem Namen "Bilderpool" bekannt.
Diese Bezeichnung ist allerdings irreführend, weil die Bilder
nicht in einem Pool herumschwimmen, sondern ordentlich gesammelt sind.

Im Bilderpool gibt es vier Arten von Dateien: 
_Bilder_, _Kommentare_, _Übersichtsseiten_ und _Bilderserien_.

## Bild

Zu einem Bild gehören im Dateisystem drei Dateien: 
Das eigentliche Bild (bei Fotos typischerweise als JPG),
ein verkleinertes Vorschaubild (Thumbnail)
und eine Markdown-Datei mit den zugehörigen Daten.
Die Dateien liegen gemeinsam in einem Verzeichnis.
Der Namen dieses Verzeichnisses ist eine laufende ID (z.B. "48312").
Die Markdown-Datei für Bilder heißt immer "index.md".

Die Markdown-Datei enthält die meisten Daten im "Frontmatter"-Abschnitt.
Neben den üblichen Feldern wie `date`, `title`, `weight` gibt es die 
folgenden speziellen Felder und Bedeutungen:

* `layout`: Muss den Wert "image" haben
* `picture`: Der Dateiname des eigentlichen Bilds
* `konstrukteure`: Namen der Konstrukteure des gezeigten Modells (es können mehrere Konstrukteure angegeben werden)
* `fotografen`: Name der Fotografen (es können mehrere Fotografen angegeben werden)
* `schlagworte`: "Keywords", aktuell nur bei Bildern.
* `uploadBy`: Benutzername desjenigen, der das Bild hochgeladen hat 
* `license`: Lizenz, unter der das Bild verwendet werden darf. Wenn nichts angegeben ist, gilt "Alle Rechte vorbehalten".
* `legacy_id`: Links zur alten Website und zu ihrer statischen Kopie.
* `imported`: für Bilder aus dem alten Bilderpool.

Weitere Informationen zu diesen Frontmatter-Parametern 
finden sich [in der Dokumentation im Know-How-Bereich](../knowhow/download).

Nur bei Bildern gibt es Schlagworte, die das Bild näher beschreiben.
Diese konnten im alten Bilderpool von den Benutzern oder den Moderatoren gesetzt werden.
Schlagworte beschreiben das Bild näher und erlauben Querverweise zu anderen Modellen.
Idealerweise sollten sie nicht den Konstrukteur oder den Bildtitel wiederholen. 
Jedes Schlagwort steht in Anführungszeichen; mehrere Schlagworte werden durch Komma getrennt.

Wichtig sind noch die Werte für `weight` und `date`.
Da die Bilder in den Galerien und Übersichten nach `weight` aufsteigend sortiert werden,
muss jeweils das erste Bild einer Galerie den niedrigsten Wert für `weight` haben.
Wenn dieses Bild als neuestes Bild auf der Startseite und bei Neuheiten angezeigt werden soll,
ist es schlau, beim Hochladen mit den Bildern mit den höheren Werten für `weight` zu beginnen,
so dass das erste Bild automatisch das neueste ist, erkennbar am Wert von `date`.
Beim Import aus der alten Webseite wurden die Bilder so geordnet, 
dass jeweils das älteste Bild den niedrigsten Wert für `weight` hat.

Am Ende des Frontmatters folgen bei den älteren Bildern noch einige Werte, 
die bei dem Import der Bilder aus der 4images-Datenbank angefallen sind.
Im Moment werden sie nicht genutzt, aber vielleicht werden die Informationen noch mal gebraucht.

Das Thumbnail-Bild wird beim Import bzw. beim Hochladen erstellt.
Es ist immer 191 x 191 Pixel groß.
Die Datei hat immer den Namen "thumbnail.jpg".
Das Vorschaubild liegt immer mit in dem Bild-Verzeichnis.
Dadurch wird es dem richtigen Bild zugeordnet.

Der eigentliche Markdown-Text enthält die Bildbeschreibung.

##### Beispiel 
````
---
layout: "image"
title: "Untergestell"
date: "2011-08-30T23:21:42"
picture: "rollenlager6.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Speichenrad"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31731
- /details18e6.html
imported:
- "2019"
_4images_image_id: "31731"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "6"
---
<!-- https://www.ftcommunity.de/details.php?image_id=31731 -->
Dieses Untergestell wird später noch ein wenig stabilisiert.
Erste Tests haben ergeben, dass es noch zuviel schwingt.
````

Die Anzeige eines Bilds auf der Website besteht aus dem Titel als Überschrift, 
gefolgt vom Bild (mit den Namen von Konstrukteur und Fotograf sowie Hochlade-Datum und -User als Bildunterschrift),
und dann die Bildbeschreibung.

Für die Darstelllung des Bildes ist die Datei `layouts/bilderpool/image.html` zuständig.

Unter dem Bild und seinen Daten folgen dann die Kommentare.

## Kommentar

Die Kommentare wurden von der alten Webseite importiert.
Aktuell gibt es sie daher nur bei den importierten Bildern.
Sie haben jeweils eine eigene Datei. 
Sie werden nicht auf einer eigenen Seite angezeigt,
sondern auf der Seite des dazugehörigen Bildes.
Die Kommentar-Dateien liegen immer im gleichen Verzeichnis wie das Bild,
dadurch werden die Kommentare dem Bild zugeordnet.
Der Dateiname ist die laufende Nummer des Kommentars.

Wichtige Parameter im Frontmatter sind:

* `layout`: hat immer den Wert "comment"
* `title`: bei den importierten Kommentaren die ID aus der Datenbank
* `date`: das Datum des Kommentars
* `uploadBy`: der Benutzername des Kommentators


## Übersichtsseite oder Album

Eine Übersichtsseite oder "Album" ist im Dateisystem der Bildersammlung ein Verzeichnis, 
das andere Übersichtsseiten, Bilderserien (Galerien) und Einzelbilder enthält.
Übersichtsseiten enthalten eine `_index.html`-Datei.

Das Layout für `_index.html` muss `overview` sein.

##### Beispiel
```
---
layout: "overview"
title: "Keksdrucker"
date: 2020-02-22T08:40:38+01:00
legacy_id:
- /php/categories/3438
- /categories3c86.html
- /categories63f8.html
- /categories8b6c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3438 --> 
Weihnachten steht vor der Tür. Obwohl 3D Drucker schon in vielen fischertechnikerhaushalten stehen, müssen Kekse immer noch manuell verziert werden. Hier hilft Magic's Cookie Printer - die Fotos zeigen wie er funktioniert.
```

Angezeigt wird für eine Übersichtsseite der Titel aus `_index.html` als Überschrift,
dann der Text (Content, der Teil unter dem Frontmatter), 
dann die ersten 4 Bilder in diesem Album
und danach jedes Unterverzeichnis. 

Die Darstellung der Unterverzeichnisse unterscheidet sich 
je nachdem, ob das jeweilige Unterverzeichnis
eine Bilderserie oder wieder ein Album enthält:

* Für eine Galerie werden die bis zu 4 ersten Bilder als Thumbnails gezeigt.
* Für ein Album wird jeweils das erste Bild als Thumbnails
der bis zu 4 ersten Unterverzeichnisse gezeigt.
Die folgenden Unterverzeichnisse werden namentlich genannt.

Die Reihenfolge der Unterverzeichnisse ist alphabetisch.
Die Bilder sind nach `weight` geordnet.

Die Thumbnails sind jeweils mit Links hinterlegt,
die direkt zum jeweiligen Bild führen.
Die Titel sind bei Alben mit der Übersichtseite des Album verlinkt,
bei Galerien mit der Darstellung der Galerie.

Die Darstellung wird über die Datei `layouts\bilderpool\overview.html` gesteuert.

## Bilderserie oder Galerie

Eine Bilderserie ist eine Gruppe zusammengehöriger Bilder, 
optional mit einem einleitenden Text. 

Die Darstellung einer Galerie wird 
über die Datei `layouts\bilerpool\image-collection.html` gesteuert.

In einer Galerie werden alle enthaltenen Bilder als Thumbnails dargestellt.
Die Bilder sind jeweils mit Links hinterlegt,
die direkt zum jeweiligen Bild führen.
Über dem Bildindex stehen noch der Titel und die Beschreibung.

Für diese Darstellung liegt in jedem Bildverzeichnis zusätzlich zu `_index.html` noch
eine Datei `gallery-index.md` mit dem Layout `image-collection`.

##### Struktur

```
---
layout: "image-collection"
hidden: "true"
title: "Bildindex"
---
```

Diese Datei hat immer den gleichen Namen und den gleichen Inhalt.
Für die Beschreibung und den Titel wird daher auf `_index.md` zurückgegriffen.
Durch diesen Trick ist es möglich, das gleiche Verzeichnis 
je nach Kontext auf verschiedene Arten abbilden zu können.


