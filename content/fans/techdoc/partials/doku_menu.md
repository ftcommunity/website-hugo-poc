---
title: "Menu"
layout: "techdoc"
topic: "layouts/partials/menu.html"
stand: "2. Juli 2020"
---

Dieses Partial ist für den Bau des Seitenmenüs verantwortlich. Es erzeugt
den Kopfteil mit dem Logo und dem Suchfeld, die Auflistung der _sections_,
ein paar spezielle Einträge mit Links außerhalb der von Hugo gebauten Seite und
die Werbung für die aktuelle ft:pedia-Ausgabe.
Dabei werden andere Partials mit `partialCached` aufgerufen.
Dadurch werden diese Partials nur einmal für alle Seiten gebaut, 
was bei der Vielzahl der Seiten eine deutliche Zeitersparnis bedeutet.

Ein paar Kommentarzeilen erleichtern die Übersicht.

````
{{define "menu-nodes"}}
````

Das Kernstück dieses Partials ist das eigene Template `menu-node`.
Dieses ruft sich rekursiv auf (`{{template "menu-nodes" dict "node" . "here" $this "activate" $actNode}}`)
und baut so das Menü zusammen.

````
    {{$entries := .Sections}} 
    {{$entries = where $entries "Params.hidden" "ne" true}}
    {{$entries = where $entries "Layout" "ne" "image"}}
````
Mit `.Sections` werden nur die Übersichtsseiten (`_index.md`) ausgewählt.
Mit den nächsten Abfragen wird sichergestellt,
dass die Seite nicht über einen Eintrag im Frontmatter (`hidden`) auf unsichtbar gestellt ist und
dass es sich nicht um eine Bilderseite handelt.

Ein Menüeintrag ist der aktuelle Eintrag, wenn entweder
* der Menüeintrag zur gerade angezeigten Seite gehört oder
* der Menüeintrag zu einem Vorfahren der gerade angezeigten Seite in der
  Hierarchie gehört, aber es keine Untereinträge unterhalb dieses Eintrags
  mehr gibt (in diesem Fall ist der Eintrag dann der letzte im Menü angezeigte
  Vorfahr der Seite und soll deshalb als "aktiv" markiert sein).

Es werden nur die Teile gerendert, die auch wirklich sichtbar sind.
Ansonsten dauert der Bau des Menüs einfach viel zu lange.
Im Bilderpool-Bereich werden die Menüeinträge alphabetisch nach dem Titel sortiert,
überall sonst nach `weight`, was die Standardeinstellung ist.

Der horizontale Trennstrich `<hr>` vor und nach der Werbung für ft:pedia wird durch etwas
inline-css (`style="...`) gestaltet.
Der eigentliche Bau der Werbung für die ft:pedial ist in ein [eigenes Partial](../docu_menu-ftpedia-youngest) ausgelagert.

Am Ende kommt noch ein Link zu Hugo.

