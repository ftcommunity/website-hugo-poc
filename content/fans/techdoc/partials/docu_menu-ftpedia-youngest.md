---
title: "Menü-Eintrag für neueste ft:pedia"
topic: "layouts/partials/menu-ftpedia-youngest.html"
layout: "techdoc"
stand: "9. Juni 2020"
---

### menu-ftpedia-youngest.html

Zur Präsentation der aktuellen ft:pedia Ausgabe gibt es zwei Möglichkeiten.
Es gibt gerade einen _teaser_ für die bald erscheinende neue Ausgabe oder das
aktuelle Heft wird als klickbarer Download-Link angeboten. Dabei wird dann
noch unterschieden, wie das Veröffentlichungsdatum dargestellt wird.

````html
{{ $date_format_string := "02.01.2006" }}

{{ $ftpSectionAll := where .Site.Pages "Section" "ftpedia" }}
{{ $ftps := where $ftpSectionAll ".Layout" "issue" }}
{{ $ftps = append $ftps (where $ftpSectionAll ".Layout" "teaser") }}
{{ range last 1 $ftps.ByDate }}
   <figure style="margin-left: 16px; width: max-content; text-align: center;">
      <a href="{{- .RelPermalink -}}">
         <img
            src ="{{- .RelPermalink -}} titelseite.png"
            alt = "Aktuelle Ausgabe"
         >
      </a>
      <br/>
      <a href="{{ .RelPermalink }}">
         <small>
               ft:pedia {{ .Title -}}
               {{- if .Params.launchDate -}}
                  <br/>erscheint am<br/>{{dateFormat $date_format_string .Params.launchDate}}
               {{- else }}
                  ist da!
               {{- end -}}
         </small>
      </a>
   </figure>
{{ end }}
````

`$ftpSectionAll := where .Site.Pages "Section" "ftpedia"` weist die Liste
aller Unterseiten in der _section_ `ftpedia/` an `$ftpSectionAll` zu. In
dieser Liste sind die einzelnen Ausgaben (`ftpedia/yyyy/yyyy-n/_index.md`),
zugeordnete Downloads (`ftpedia/yyyy/yyyy-n/einfile.md`), die Jahrgangsseiten
(`ftpedia/yyyy/_index.md`) und die _section_ Wurzel (`ftpedia/_index.md`)
enthalten.

Uns interessieren allerdings nur die Seiten, deren Frontmatter
`layout: "issue"` vorgibt: `$ftps := where $ftpSectionAll ".Layout" "issue"`.
Zusätzlich wird noch der Teaser aufgespürt und, so vorhanden, an die Liste
`$ftps` angefügt:
`$ftps = append $ftps (where $ftpSectionAll ".Layout" "teaser")`.

Rein theoretisch würde es ausreichen, das jüngste Element der Liste
`$ftpSectionAll` auszuwählen. Testweise eingebaute Fehler im Datenbestand
wählen dann allerdings eine Jahrgangsseite anstelle der jüngsten Ausgabe
und bringen so den Aufbau des Menüs durcheinander. Daher wird zusätzlich
auf `issue` oder `teaser` eingeschränkt und `$ftps` erzeugt. Dadurch wird
immer eine Ausgabe oder der Teaser im Menü erscheinen.

Der naheliegende Versuch, `.Params.launchDate` mit `where` abzufragen, ist gescheitert.
Daher wird `layout: ...` für die Identifikation der Ausgabe und des Teasers genutzt.

Aus der Liste `$ftps`, die jetzt nur noch Seiten für die Ausgabe und evtl.
einen Teaser enthält, wird das jüngste Element ausgewählt. Das erledigt
`range last 1 $ftps.ByDate` kurz und bündig. Bei Hugo steht das jüngste
Element immer als letztes in der Liste, wenn `.ByDate` sortiert wird.

Die gefällige Anordnung verwendet `<figure>` und passt diese mit etwas
inline-css an. Die nächsten Zeilen bauen einen Bild-Hyperlink:
````html
      <a href="{{- .RelPermalink -}}">
         <img
            src ="{{- .RelPermalink -}} titelseite.png"
            alt = "Aktuelle Ausgabe"
         >
      </a>
````
`.RelPermalink` steuert den Ort bei (z. B.:
`https://ftcommunity.de/ftpedia/2013/2013-1/`).

In einer vorigen Version zeigte der Link direkt auf die Download-Datei.
<!--Doku zu voriger Version: Gibt es gar keinen Dateinamen, weil es sich um den Teaser handelt, zeigt der
Link (z. B.: `https://ftcommunity.de/ftpedia/2013/2013-1/`) bereits auf die
Einzelseite des Teasers. Das ist ein netter Seiteneffekt der Dateistruktur und
des Bauscripts für die _section_ ft:pedia. Er erlässt uns hier die sonst
fällige Sonderbehandlung des Teasers.-->

Das zugehörige Thumbnail für das Titelbild heißt generell `titelbild.png`
und so zeigt `src ="{{- .RelPermalink -}} titelseite.png"` an die richtige
Stelle.

Ein Zeilenvorschub `<br/>` lässt den noch folgenden Text unter dem Bild
erscheinen. Auch dieser Text wird als Download-Link angelegt (s. o.).
Der Standardanfang `ft:pedia {{ .Title -}}` ("ft:pedia n / yyyy") wird dann
je nach Anwendungsfall entweder um "ist da!" oder "erscheint am" und das
geplante Erscheinungsdatum ergänzt. `if .Params.launchDate` unterscheidet hier
zwischen Teaser und Ausgabe. `if eq .Layout "teaser"` hätte es auch getan. Mit
den notwendigen schließenden Tags endet dieser Teilbereich des Menüs.
