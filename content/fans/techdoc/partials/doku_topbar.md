---
title: "Kopfzeile mit Breadcrumb-Navigation"
layout: "techdoc"
topic: "layouts/partials/topbar.html"
stand: "14. März 2022"
---

Dieses Partial ist für den Bau der obersten Zeile einer Seite zuständig.
Die ursprüngliche Version enthielt noch die Möglichkeit, zum Editieren der Seite zu verlinken.
Da hier die Seiten ohnehin nicht auf diese Weise bearbeitet werden,
ist jetzt diese Abfrage gestrichen.

Außerdem baute die ursprüngliche Version noch ein Symbol für ein Inhaltsverzeichnis der Seite ein.
Da diese Funktion nicht so richtig überzeugte, ist sie ebenfalls gestrichen.

Übrig bleibt also die Breadcrumb-Navigation, 
bei der die einzelnen Bestandteile des Pfades verlinkt werden.
Der Bau dieser Zeile wird in diesem Partial unverändert vom Original übernommen.
