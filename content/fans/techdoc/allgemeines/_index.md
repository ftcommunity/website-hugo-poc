---
title: "Bau mit Hugo"
layout: "techdoc"
weight: 1
stand: "16. Juli 2020"
---
## Über Hugo

[Hugo](https://gohugo.io) ist ein Generator für statische Websites.
Das Programm erstellt aus Textdateien, Bildern und anderen Inhalten
browsertaugliche HTML-Dateien,
die es im Verzeichnis `/public` ablegt.
Von dort werden die Seiten von einem Serverprogramm ausgeliefert.

## Über die Inhalte

Die Dateien mit den Inhalten liegen im Verzeichnis `/content`.
Dort können sie hinzugefügt, verändert, verschoben,
gelöscht oder nach sonstigem Belieben bearbeitet werden.
Änderungen an den Inhalten werden von Hugo umgesetzt und schlagen sich dann
in der produktiven Seite nieder.
Der dargestellte Inhalt liegt in Markdown-Dateien (`.md`).

Jedes Verzeichnis in `/content` stellt 
einen Bereich (`section`) der Website dar.
Im Menü sind das die Hauptpunkte.
Unterverzeichnisse davon ergeben Untermenüs.
   
Im Header in den Markdown-Dateien (= Frontmatter) stehen
wichtige Informationen (Metadaten) zu der Seite, unter anderem der Titel.
Dieses Frontmatter ist im jeweiligen Kapitel zu dem Bereich 
in dieser Dokumentation detailliert beschrieben.

## Über das Layout

Das Aussehen der Website wird von einem Theme bestimmt.
Teile davon
haben wir von einem vorgefertigten "Theme" für Hugo 
-- dem ["Learn"-Theme](https://github.com/matcornic/hugo-theme-learn/) --
übernommen, hier aber grundlegend überarbeitet.

Im Unterschied zum Learn-Theme nutzen wir für den "Rahmen" der Seite die modernere Variante 
mit [Base-Templates und Blocks](https://gohugo.io/templates/base/).
Im Learn-Theme ist das HTML für die Kopfleiste der Seite 
hart in `partials/header.html` kodiert.
Bei uns ist dieser Bereich nach 
`partials/topbar.html` 
ausgelagert.
Wir haben eine eigene, angepasste Navigation für die einzelnen Bereiche. 
Diese ist in den Seitenbau-Skripten für die jeweilige _section_ enthalten.

Die Darstellung der Seiten wird in Template-Dateien beschrieben.
Diese Template-Dateien und andere Skripte für die Website liegen
in dem Verzeichnis für unser Theme (`themes/website-layout`).

Die Aufteilung in die Bereiche `/content` und `/themes` erlaubt es, sehr einfach
an den Inhalten zu arbeiten, ohne sich um deren Darstellung kümmern zu müssen.

## Über die Templates
   
Die Templates für die HTML-Dateien liegen in und unterhalb `/layouts`.
Diese Template-Dateien haben die Dateiendung `.html`.
In diesen Dateien befindet sich neben einem HTML-Grundgerüst auch Go-Code.

Normale Seiten (z.B. `index.md`, `seite.md`), ziehen sich das Layout-Template `single.html`, 
wenn sie selbst _kein_ eigenes Layout besitzen, 
also keinen Eintrag `layout:` im Frontmatter haben.
Wenn sie ein eigenes Layout haben, ziehen sie sich das entsprechende Template, 
z. B. `layout: "file"` zieht `file.html`.
Übersichtsseiten (`_index.md`) ziehen sich das Layout-Template
`list.html`, wenn kein anderes Layout im Frontmatter angegeben ist.

In `/layouts` kann es für jede `section` (jedes Verzeichnis direkt unter
`/content`) ein gleichnamiges Unterverzeichnis geben.
Liegt in diesem Verzeichnis ein passendes Template, so wird das
<u>vor</u> dem Default-Template verwendet.
[Default-Templates](./default_templates/)
finden sich in `/layouts/_default`.

Der Inhalt aus den `.md` wird mit `{{ .Content }}` eingefügt.

Kommentare mit `{{/*   */}}` erscheinen nicht in den fertigen Seiten.

Die Sortierung der Seiten erfolgt standardmäßig nach `weight`.
Bei `weight` ist der niedrigste Wert oben. Er kann auch negativ sein.
Merke: Leichtes schwimmt oben.

Die Sortierung kann in den Templates explizit geändert werden.
Sortieren kann man u. a. nach Titel (`.ByTitle`) oder auch Datum (`.ByDate`).
Die Sortierung kann man mit angehängtem `.Reverse` umdrehen.   

Die verwendeten Templates werden in den entsprechenden Abschnitten 
dieser Dokumentation beschrieben.   
   
## Allgemeine Konfiguration der Website

Allgemeine Einstellungen, die die ganze Website betreffen,
werden in der Datei `config.toml` gemacht.
Dort werden auch einige globale Parameter festgelegt.
   
Auch Menüpunkte mit Links nach extern, also bei uns im Moment zu Forum und cfw,
werden in `config.toml` eingetragen.

Hier werden auch ["Taxonomien"](./doku_taxonomy/) definiert.

In `config.toml` werden auch Einstellungen für den Markdown-Parser "Blackfriday" gemacht. 
   
   ```
   [blackfriday]
        fractions = false
   ```
Diese Einstellung bewirkt, dass Zahlen vor und nach dem Schrägstrich `/` nicht als Bruch dargestellt werden.

