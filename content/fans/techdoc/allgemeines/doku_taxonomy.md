---
title: "Taxonomie"
topic: "Taxonomie"
layout: "techdoc"
stand: "2. Januar 2021"
---

Hugo kann auf relativ einfache Weise Index-Dateien über mehrere oder sogar viele Seiten anlegen,
sogenannten "Taxonomien".
Dazu werden Werte aus dem Frontmatter ausgewertet.
Wir verwenden diese Funktion für drei Parameter:

* legacy_id
* Konstrukteur (konstrukteure)
* Schlagworte (schlagworte)

## Konfiguration

In der Datei `config.toml` wird definiert,
welche Parameter als Taxonomy genutzt werden.
Dies geschieht in dem Format
`Schlüsselbegriff im Singular = "Schlüsselbegriff im Plural"`.
Der Plural ist genau der Parameter aus dem Frontmatter.
Das sieht bei uns dann so aus:

```
[taxonomies]
  konstrukteur = "konstrukteure"
  legacy_id = "legacy_id"
  schlagwort = "schlagworte"
```

## Darstellung

Im Theme gibt es für jede "Taxonmie" zwei Layouts:

* list.html
* "Begriff".html

Diese liegen jeweils in eim Verzeichnis,
das den Schlüsselbegriff im Plural als Namen hat,
also bei uns `konstrukteure`, `schlagworte` und `legacy_id`.

In `list.html` wird die Liste aller Konstrukteure bzw. aller Schlagworte
in alphabetischer Reihenfolge dargestellt. Für legacy_id entfällt diese Darstellung.

`konstrukteur.html`, `schlagwort.html` und `legacy_id.html`
legen fest, wie die Seite für den einzelnen Index-Begriff aussehen soll.

Die Seiten unter `legacy_id` dienen im Wesentlichen dem Webserver, 
der die Seitenaufrufe direkt von der veralteteen URL auf die Zielseite weiter leitet.

