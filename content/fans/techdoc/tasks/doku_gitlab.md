---
title: "Nutzung der Gitlab-Oberfläche"
layout: "techdoc"
topic: "Nutzung der Gitlab-Oberfläche"
stand: "28. Dezember 2020"
---

Wenn du nur schnell eine Kleinigkeit ändern willst,
oder dir aus anderen Gründen nicht das ganze Repo herunterladen willst,
kannst du auch mit der Gitlab-Oberfläche arbeiten.

Dazu musst du einen Account bei Gitlab haben und als Mitglied der ftcommunity registriert sein.
Dann kannst du dich mit Benutzername und Passwort anmelden.

Dann steht dir im Repository die Funktion `Web-IDE` zur Verfügung.

## Datei anlegen

Du kannst eine neue Dateien anlegen, indem du dir eine existierende Datei anguckst 
und die Inhalte in eine neue Datei schreibst.

Beachte dabei, dass das Frontmatter der Datei oben und unten von drei Bindestrichen (`---`) umgeben ist,
die in der fertig gerenderten Datei nicht angezeigt werden.

## Datei hochladen

Über die Web-IDE kannst du auch eine Datei, z.B. ein Download-File hochladen.
Die Größe der Datei ist dabei auf 16 MByte begrenzt.
Diese Größenbeschränkung gilt nur für die Web-IDE.

## Verschieben einer Datei

Um eine Datei aus einem Verzeichnis in ein anderes zu verschieben,
nutzt du die Funktion `Rename/Move` in der Web-IDE.
Mit dieser Funktion kannst du nicht nur den Dateinamen,
sondern auch den Pfad zu der Datei editieren und damit
die Datei verschieben.
