---
title: "FAQ: Forum"
date: 2019-10-15T18:15:45
konstrukteure: 
- "EstherM"
uploadBy:
- "EstherM"
license: "GNU Free Documentation License, Version 1.1, ohne unveränderliche Abschnitte"
legacy_id:
- /wiki44b7.html
imported:
- "2019"
weight: 90
---
<!-- https://ftcommunity.de/wiki44b7.html?action=show&topic_id=6 -->
<!--
Wiki

Thema: FAQ: Forum

ThanksForTheFish 05.03.2018, 22:15:45 Uhr

   Historie:
Version 27 von Fredy am 28.07.2008, 10:45:28 Uhr
Version 26 von Masked am 04.05.2008, 19:32:44 Uhr
Version 25 von Masked am 04.05.2008, 19:20:39 Uhr
...
Version 3 von wahsager am 24.05.2003, 11:14:40 Uhr
Version 2 von wahsager am 24.05.2003, 11:06:47 Uhr
Version 1 von wahsager am 24.05.2003, 11:05:33 Uhr
-->

## Einleitung

FAQ steht kurz für "Frequently Asked Questions". In dieser FAQ sollen die Fragen beantwortet werden, die beim Besuch des Forums entstehen können.

**Bitte beachte die Forenregeln und die [Nutzungsbedingungen für phpBB-Foren](https://forum.ftcommunity.de/ucp.php?mode=terms).**

#### Wie komme ich am einfachsten ins Forum?

Die Adresse des Forums ist <https://forum.ftcommunity.de/>. Über "Schnellzugriff" - "Aktive Themen" werden nur die Themen angezeigt, 
in denen sich in den letzten Tagen etwas getan hat. Den Überblick erreicht man dagegen über "Foren-Übersicht".

#### Muss ich mich anmelden?

Jeder kann das Forum lesen, auch wenn er nicht angemeldet ist. Zum Schreiben ist es allerdings notwendig, sich einen Nickname auszudenken und sich anzumelden. Eine funktionierende E-Mail-Adresse ist erforderlich, weitere Angaben sind immer gern gesehen, aber nicht nötig. Die Anmeldung geschieht kurz und schmerzlos.

#### Wofür sind die Unterforen?

Das Forum ist in einige Unterbereiche (Unterforen) aufgegliedert. Diese helfen, die Themen (Threads) ein bisschen inhaltlich zusammen zu fassen. Die Überschriften versuchen, den inhaltlichen Rahmen abzustecken. Um sicher zu gehen, dass der eigene Beitrag im richtigen Forum gestellt wird, ist es sinnvoll, sich einfach die anderen Beiträge in diesem Unterforum anzugucken.

Ein Thread (Thread, nicht Threat) kommt als nächstes in der Hierarchie. Betritt man ein Teilforum, sieht man zuerst eine große Liste von Threads, also Diskussionsthemen, die einen bestimmten Namen haben und sich genau um dieses Thema drehen. Okay, drehen sollen. Oft genug schweift man ab, das nennt man dann "Off-Topic" oder kurz OT.

Ein Posting ist ein einzelner Beitrag innerhalb eines Threads. Vom kurzen Kommentar bis zu mehrseitigen Ausführungen wurde bereits alles gesehen, aber was in einem Posting geschrieben ist, sollte die Diskussion ein wenig weiterbringen. 

#### Wer soll sich beteiligen?

Jeder! Wir bitten alle, die nur hin und wieder vorbeischauen, etwas lesen und dann wieder verschwinden, obwohl sie eigentlich etwas zu sagen haben, auch selbst zu schreiben. Das Forum besteht nur daraus, dass jeder, der mag, etwas schreiben kann und sich an Diskussionen beteiligen kann. Davon lebt das Forum, je lebhafter es hier zugeht, umso mehr Spaß macht es uns allen.

#### Kann ich in anderen Sprachen posten?

fischertechnik gibt es nicht nur in Deutschland, und von daher wird auch nicht jeder, der das Forum besucht, zwangsläufig auf Deutsch schreiben können. Wir erlauben das Posten in anderen Sprachen. Hierbei wäre es aber sehr hilfreich, wenn ihr euch auf Englisch, Französisch und Niederländisch beschränkt. Damit dann auch jeder mitreden kann, sind alle, die Deutsch und die betroffene Sprache beherrschen, dazu aufgerufen das Posting zu übersetzen - das muss nicht wörtlich sein, eine einzeilige Zusammenfassung eines ganzseitigen Postings ist ok, wenn der Sinn rüberkommt.
Wenn möglich sollte in der Originalsprache geantwortet werden - oder es hilft auch hier jemand zeitnah als Übersetzer aus. Notfalls tut es eine maschinelle Übersetzung.

## Das Forum bedienen

#### Wie schreibe ich Antworten?

Unten auf der Seite gibt es einen Button "Antworten". Vor dem Abschicken des Beitrags kann man mit "Vorschau" noch einmal prüfen, ob der Beitrag richtig aussieht. Außerdem kann man einen vorigen Beitrag zitieren mit "Mit Zitat antworten" (1. Button von rechts in der Titelzeile des Beitrags, der mit den Gänsefüßchen). Es sollte nur dann zitiert werden, wenn es wirklich notwendig ist. 

#### Wie editiere ich Postings? 

Eigene Nachrichten kann man auch nach dem Posten problemlos ändern, dazu gibt es den Button "Beitrag ändern" (1. Button von links in der Titelzeile des Beitrags). Vom Forum aus wird nur angezeigt, ob ein Beitrag bearbeitet worden ist, wenn schon eine Antwort eingegangen ist. Daher ist es nett, die Änderungen mit einem kleinen "Edit:" am Ende bekannt zu geben.

#### Wie kann ich Links integrieren?

Links zu anderen Webseiten sind kein Problem, zumindest nicht mehr als überall sonst. Für den rechtlichen Teil ist der jeweilige Autor verantwortlich.

Zum Einfügen von Links dient der Button "Link einfügen" (das Bild mit dem Kettenglied).

Man kann den Code auch von Hand erzeugen. Beispiel: 
`[url]http://www.fischertechnik.de [/url]`

Tipp: Bei langen urls, wie z.B. Verweisen innerhalb des Forums, wirkt es unschön, diese direkt zu posten. Dann ist es sinnvoll, sich einen kurzen Alternativtext auszudenken, der dann angezeigt wird und trotzdem auf die "lange" Adresse verweist. Man schreibt das im Posting folgendermaßen:
`[url=adresse] Text [/url]`

Beispiel:
`[url=https://ftcommunity.de/fans/andenken/adventskalender2012/]Adventskalender 2012[/url]`

Bitte prüft vor dem Absenden noch einmal mit "Vorschau" nach, ob die Links so auch funktionieren.

#### Wie kann ich Bilder integrieren? 

Man kann Bilder direkt in das Forum einfügen, so dass diese mit dem Text angezeigt werden. Das ist für den Leser übersichtlicher und sicherer als Links auf Bilder an einer anderen Stelle.

Zum Hochladen von Bildern dient die Funktion "Dateianhänge" unter dem Beitrag. Mit "Datei anhängen" kommt man zu einem Auswahldialog, mit dem man die Bilddatei auf dem eigenen Rechner auswählt. Nach Click auf "Im Beitrag anzeigen" und "Absenden" erscheint das Bild im eigenen Beitrag.

Angehängte Dateien dürfen maximal 4 MByte groß sein. Als Datentypen sind gif, jpeg, jpg und png erlaubt.

#### Was ist eine Signatur?

Eine Signatur ist ein Text, der an deine Nachrichten angefügt wird. Sie ist auf 100 Zeichen begrenzt. Smilies und Bilder sind ausgeschaltet. Es sind maximal zwei Links erlaubt.

#### Was bedeuten diese kryptischen Bildchen neben den Themen?

Diese Bildchen werden von der Forensoftware (phpBB) bereitgestellt.
Sie informieren über den Status des Themas.

| Bildchen | Beschreibung |
| --- | --- |
| ![i](icon-i.png) | Bekanntmachung |
| ![Ausrufezeichen](icon-ausrufezeichen.png) | Thema, das ein Moderator wegen seiner Wichtigkeit oben in einem Forum angeheftet hat (Status: sticky) |
| ![ungelesen](icon-ungelesen.png) | Thema, das von dir ungelesene Beiträge enthält. Das rote Sternchen bedeutet, dass du selbst dort einen Beitrag verfasst hast. | 
| ![gelesen](icon-gelesen.png) | Thema, in dem du schon alle Beiträge gelesen hast |
| ![hot](icon-hot.png) | Besonders heftig diskutiertes Thema |
| ![locked](icon-locked.png) | geschlossenes Thema, in dem normale Benutzer keine Beiträge verfassen dürfen |







