---
title: "Impressum"
date: 2019-04-11T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm006c.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/ftcomm006c.html?file=impressum -->

für die Webseite ftcommunity.de inklusive forum.ftcommunity.de, für ft-datenbank.de
und die Weiterleitung www.ft-modellbau.de


#### Server & Hosting, inhaltlich verantwortlich
    
    
ftc Modellbau e.V.     
Guntherweg 1    
23562 Lübeck
  
vorstand@ftcommunity.de  


Vertretungsberechtiger Vorstand: Christian Wiechmann (Vorsitzender), Stefan Falk (Schriftführer), Raphael Jacob (Kassenwart)
  
Eingetragen im Vereinsregister am Amtsgericht Hannover, VR203188

