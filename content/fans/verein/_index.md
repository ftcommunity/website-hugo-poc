---
title: "Verein"
weight: 2
date: 2019-04-09T00:00:00
uploadBy:
- "Esther"
legacy_id:
- /ftcomm28dc.html
imported:
- "2019"
---

<!-- http://ftcommunity.de/ftcomm28dc.html?file=verein -->

## Der Verein ftc Modellbau e.V.

Im Frühjahr 2013 entstand die Idee, einen Trägerverein für den Server der ftcommunity zu gründen, 
um die finanzielle und rechtliche Verantwortung auf mehr Personen zu verteilen. 
Nach vielen langen Nächten mit Satzungsentwürfen und Bürokratie wurde die Idee am 31.08.2013 Wirklichkeit: 
Zehn fischertechniker trafen sich in Dreieich und gründeten den ftc Modellbau e.V.

### Sinn und Zweck des Vereins

Der ftc Modellbau e.V. hat entsprechend seiner Vereinssatzung folgenden Zweck:

* die Pflege des Modellbaus mit dem fischertechnik-Baukastensystem. 
* die Förderung der Kreativität, des technischen Verständnisses und ähnlichen Dingen beim Modellbau mit fischertechnik. 
* die Förderung der Ingenieurs- und Naturwissenschaften und Heranführung von Kindern und Jugendlichen an damit verbundene Thematiken.

Dazu betreiben wir in erster Linie den Server der fischertechnik-Community mit dem Bilderpool und dem Forum sowie die fischertechnik-Datenbank, um Modellideen und technische Kniffe anschaulich darstellen zu können, ständige Diskussionen zu bautechnischen und allgemeinen Fragen zu ermöglichen sowie das Wissen über die Bauteile und die Historie des fischertechnik-Systems zu teilen. 

Auf unseren fischertechnik-Conventions, die wir nach Möglichkeit jeweils einmal jährlich in Nord- und in Süddeutschland veranstalten, ist jeder eingeladen, seine fischertechnik-Modelle und -Projekte persönlich vorzustellen und sich über Baumöglichkeiten mit dem fischertechnik-System zu informieren. Hier besteht die Möglichkeit zum persönlichen Austausch zwischen den Ausstellern sowie Besuchern, damit die Modelle in voller Funktion vorgestellt und diskutiert werden können.

Auch die internationale Zusammenarbeit wird durch diese virtuellen und persönlichen Zusammenkünfte mit anderen fischertechnik-Fans und -Gruppen (z.B. ft-Club Niederlande) gefördert und unterstützt.

Darüber hinaus sind Mitglieder der fischertechnik-Community immer wieder auf anderen Modellbauausstellungen und Messen, wie Maker Faires, vertreten um den Verein und das Bauen mit fischertechnik der breiten Öffentlichkeit vorzustellen.

Selbstverständlich ist der ftc Modellbau e.V. politisch, weltanschaulich und konfessionell neutral.

### Ich möchte Mitglied im ftc Modellbau e.V. werden!
Eine Mitgliedschaft im Forum und im Bilderpool oder eine Mitarbeit im Team der ftc ist niemals an die Vereinsmitgliedschaft gebunden. 
Hilfe in jeder Form (auch finanziell) kann jederzeit völlig unabhängig von der Vereinsmitgliedschaft erfolgen.
Die Vereinsmitgliedschaft ermöglicht es Euch, den Betrieb des Servers und damit aller Webseiten langfristig zu unterstützen und zu sichern. 
Außerdem habt ihr die Möglichkeit, aktiv über die "strategische" Zukunft von Verein und Server mitzureden.

Wer gerne einen finanziellen Beitrag leisten möchte, ohne Vereinsmitglied zu werden, findet unten unsere Bankverbindung.
Der Jahresbeitrag kann in Form einer Mitgliedskategorie ab 10€/Jahr frei gewählt werden. 
Ein Wechsel ist immer zu einem neuen Kalenderjahr möglich, die Bezahlung erfolgt per Überweisung. 
Ihr erhaltet immer zum Ende des Kalenderjahres eine Rechnung für das Folgejahr.

[Beitrittserklärung](Beitrittserklärung.pdf)  
[Beitrittserklärung für Fördermitglieder](Beitrittserklärung-Fördermitglieder.pdf)  
[SEPA-Lastschriftmandat](SEPA-Mandat.pdf)  
[Satzung](Satzung.pdf)  
[Beitrags- und Gebührenordnung](BeitragsGebuehrenordnung.pdf)  

Aus rechtlichen Gründen benötigen wir eine Original-Unterschrift von Euch. 
Bitte füllt deshalb die Beitrittserklärung aus, unterschreibt sie im ausgedruckten Zustand 
und schickt sie uns per Post oder als Scan per Mail an vorstand@ftcommunity.de.
Personen unter 18 Jahren benötigen zusätzlich die Unterschrift eines gesetzlichen Vertreters.

#### Unsere Bankverbindung:  
ftc Modellbau e.V.  
IBAN DE88830654080004190220  
BIC GENODEF1SLR  
Deutsche Skatbank  



