---
layout: "image"
title: "ft:pedia-Kuchen"
date: "2012-09-30T18:55:09"
picture: "ftpediakuchen1.jpg"
weight: "32"
konstrukteure: 
- "Familie Fox"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35562
- /details194c.html
imported:
- "2019"
_4images_image_id: "35562"
_4images_cat_id: "312"
_4images_user_id: "104"
_4images_image_date: "2012-09-30T18:55:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35562 -->
Die Redaktionsbesprechung zur ft:pedia 3/2012 fand bei Dirk Fox zu Hause statt. Und seine ganze Familie hatte heimlich diesen Kuchen gebaut, äh, gebacken, und Dirk und mich damit überrascht. Danke! Lecker war er!
