---
layout: "image"
title: "Motor"
date: "2013-01-13T09:21:11"
picture: "Motor.jpg"
weight: "33"
konstrukteure: 
- "gamma vielleicht!?"
fotografen:
- "gamma"
uploadBy: "gamma"
license: "unknown"
legacy_id:
- /php/details/36460
- /detailsb3e8.html
imported:
- "2019"
_4images_image_id: "36460"
_4images_cat_id: "312"
_4images_user_id: "1607"
_4images_image_date: "2013-01-13T09:21:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36460 -->
Haben diese Motoren denselben Anschluß wie die Powermotoren von ft? 
Welche Teile benötigt man um einen Powermotor im Modell einzubauen?