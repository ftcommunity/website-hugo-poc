---
layout: "image"
title: "Seltsamer Stein"
date: "2007-02-20T19:54:05"
picture: "SeltsamerStein1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9115
- /details19f7.html
imported:
- "2019"
_4images_image_id: "9115"
_4images_cat_id: "829"
_4images_user_id: "456"
_4images_image_date: "2007-02-20T19:54:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9115 -->
Näheres in der Beschreibung der Unterkategorie. Die große Preisfrage: Wer weiß was das für ein Stein ist? Und, ist er was wert?
