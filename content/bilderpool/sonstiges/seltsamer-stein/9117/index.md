---
layout: "image"
title: "Seltsamer Stein"
date: "2007-02-20T19:54:05"
picture: "SeltsamerStein3.jpg"
weight: "3"
konstrukteure: 
- "Wer Weiss das wohl?"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9117
- /details23d7.html
imported:
- "2019"
_4images_image_id: "9117"
_4images_cat_id: "829"
_4images_user_id: "456"
_4images_image_date: "2007-02-20T19:54:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9117 -->
