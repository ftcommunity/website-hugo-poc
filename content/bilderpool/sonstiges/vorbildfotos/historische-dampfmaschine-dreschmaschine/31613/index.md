---
layout: "image"
title: "Beschreibungen (2)"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine24.jpg"
weight: "24"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31613
- /detailsadd2.html
imported:
- "2019"
_4images_image_id: "31613"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31613 -->
Leider nicht scharf, sorry.
