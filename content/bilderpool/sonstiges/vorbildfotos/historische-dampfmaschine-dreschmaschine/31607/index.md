---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine18.jpg"
weight: "18"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31607
- /details6540.html
imported:
- "2019"
_4images_image_id: "31607"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31607 -->
Eine Öffnung weiter oben.
