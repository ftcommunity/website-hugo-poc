---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine11.jpg"
weight: "11"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31600
- /details5d5c-2.html
imported:
- "2019"
_4images_image_id: "31600"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31600 -->
Ein anderer Blick zur vorherigen Aufnahme.
