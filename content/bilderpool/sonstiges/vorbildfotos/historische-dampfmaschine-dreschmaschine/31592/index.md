---
layout: "image"
title: "Rückseite"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine03.jpg"
weight: "3"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31592
- /details4a44.html
imported:
- "2019"
_4images_image_id: "31592"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31592 -->
Die Dampfmaschine von der anderen Seite
