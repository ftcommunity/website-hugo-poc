---
layout: "image"
title: "Kesseleingang"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine06.jpg"
weight: "6"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31595
- /details8f47.html
imported:
- "2019"
_4images_image_id: "31595"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31595 -->
