---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine15.jpg"
weight: "15"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31604
- /details5833.html
imported:
- "2019"
_4images_image_id: "31604"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31604 -->
"Vor Inbetriebnahme Schrauben nachziehen" - Stand Anfang des 20. Jahrhunderts eben.
