---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine17.jpg"
weight: "17"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31606
- /details91ad.html
imported:
- "2019"
_4images_image_id: "31606"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31606 -->
Blick in die untere Öffnung im vorherigen Bild.
