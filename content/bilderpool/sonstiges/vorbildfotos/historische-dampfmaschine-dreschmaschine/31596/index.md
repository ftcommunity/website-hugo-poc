---
layout: "image"
title: "Manometer"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine07.jpg"
weight: "7"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31596
- /details5569.html
imported:
- "2019"
_4images_image_id: "31596"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31596 -->
