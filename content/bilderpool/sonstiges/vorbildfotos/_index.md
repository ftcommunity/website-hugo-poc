---
layout: "overview"
title: "Vorbildfotos"
date: 2020-02-22T09:23:21+01:00
legacy_id:
- /php/categories/2355
- /categories5c09.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2355 --> 
Diverse Bilder von "Originalen", die es wert wären nachgebaut zu werden.