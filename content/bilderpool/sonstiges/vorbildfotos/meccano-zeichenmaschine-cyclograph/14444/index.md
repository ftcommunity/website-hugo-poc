---
layout: "image"
title: "Cyclograph  Mitten"
date: "2008-05-03T15:48:08"
picture: "meccanozeichenmaschinecyclograph1.jpg"
weight: "12"
konstrukteure: 
- "J Weststrate (meccano gilde nederland)"
fotografen:
- "pvd"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/14444
- /details359a.html
imported:
- "2019"
_4images_image_id: "14444"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:48:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14444 -->
Eine ausserst komplizierte Maschine .  Der Herr Weststrate  der diese Maschine gebaut hat ,   erzählte mir das er 7 Jahre lang  daran gebaut / verbessert hat .  Hier oben im Foto  sieht man oben (mitte)  mehrere Achsen  die senkrecht nach oben gehen.  Die formen eine "Saule" die  wobelnd bewegt   wenn man danach hinseht  weilst die Maschine Zeichnet .   Diese Saule  tragt  einen Platform (rote Meccano-GrundpPlatte ) /  Dieser Platform macht bewegungen in ein horizontales Flach , also  X und Y  Bewegungen vom Platform .    Auf diesen Platform befindet sich der drehende Zeichentisch. 
Alle Bewegungen (sowohl X , Y,  als Drehung vom Zeichtentisch ) lassen sich separat mechanisch einstellen und  ändern .   Jedesmal sind auch Intervalle möglich in jeder Bewegung .
