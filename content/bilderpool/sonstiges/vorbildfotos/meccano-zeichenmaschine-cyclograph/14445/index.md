---
layout: "image"
title: "Cyclograph   Platform und Zeichentisch"
date: "2008-05-03T15:48:08"
picture: "meccanozeichenmaschinecyclograph2.jpg"
weight: "13"
konstrukteure: 
- "J Weststrate (meccano gilde nederland)"
fotografen:
- "pvd"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/14445
- /details1f85.html
imported:
- "2019"
_4images_image_id: "14445"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:48:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14445 -->
Unten im Foto sieht man die "Saule"  ,  die besteht  aus 4 Stahlachsen .   Hieroben befindet sich dann die rote Grundplatte  (die also im horizontalen Flach  XY Bewegunge macht )  dirigiert von dieser Saule .  Oben diese rote Grundplatte ist dann die rote Drehscheibe   die verbunden ist mit der eigentliche Zeichentisch (Plexi) .  Hierauf wird das Papier geklemmt .   Es wird  schweres  und glattes weisses Papier verwendet .
