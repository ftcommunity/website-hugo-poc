---
layout: "image"
title: "Ein Quad als Schneemobil 1/5"
date: "2011-02-20T21:26:44"
picture: "a1.jpg"
weight: "1"
konstrukteure: 
- "can-am"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/30096
- /details3562.html
imported:
- "2019"
_4images_image_id: "30096"
_4images_cat_id: "2224"
_4images_user_id: "389"
_4images_image_date: "2011-02-20T21:26:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30096 -->
Das Quad habe ich im Skigebiet von Mayrhofen entdeckt.

Hier der Größenvergleich zu einer Pistenraupe
