---
layout: "image"
title: "Antrieb"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34563
- /details4f41.html
imported:
- "2019"
_4images_image_id: "34563"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34563 -->
Das in der Bildmitte aufgewickelte Stahlseil läuft zum Gewicht - einem Granitblock. Der hier nach rechts laufende silbrige Metallstab mit dem justierbaren Gewicht bewegt sich jede Sekunde ein klein wenig nach unten und wird alle 30 s wieder nach oben befördert. Links greift ein zweiflügliges Metallteil mit einer Nase ins Räderwerk, und alle 30 s wird es freigegeben und macht eine halbe Umdrehung.
