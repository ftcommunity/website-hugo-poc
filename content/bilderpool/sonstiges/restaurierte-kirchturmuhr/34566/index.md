---
layout: "image"
title: "Rückansicht des Getriebes"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34566
- /detailsee10.html
imported:
- "2019"
_4images_image_id: "34566"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34566 -->
Die Flügelwerke beschränken die Drehgeschwindigkeit auf manchen Achsen durch ihren Luftwiderstand.
