---
layout: "image"
title: "Überblick übers Uhrwerk"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34559
- /details4c37.html
imported:
- "2019"
_4images_image_id: "34559"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34559 -->
Auf der nächsten Seite findet sich die Beschreibung.
