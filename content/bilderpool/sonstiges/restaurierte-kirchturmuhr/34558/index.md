---
layout: "image"
title: "Gesamtansicht"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34558
- /detailsc2f3.html
imported:
- "2019"
_4images_image_id: "34558"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34558 -->
Dies ist eine wunderschöne restaurierte Kirchturmuhr, die jetzt im Eingangsbereich des Hotels "Tanne" in Tonbach/Schwarzwald ihren Platz gefunden hat. Man beachte die beiden Zifferblätter. Vielleicht reizt das ja jemanden zum fischertechnik-Nachbau...
