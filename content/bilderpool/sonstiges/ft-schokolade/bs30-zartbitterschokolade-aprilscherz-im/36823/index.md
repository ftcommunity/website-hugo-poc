---
layout: "image"
title: "Schoko-BS30 (5)"
date: "2013-04-02T15:32:14"
picture: "P1070855_verkleinert.jpg"
weight: "5"
konstrukteure: 
- "Bernhard Lehner (bflehner)"
fotografen:
- "Bernhard Lehner (bflehner)"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/36823
- /details8ea4.html
imported:
- "2019"
_4images_image_id: "36823"
_4images_cat_id: "2733"
_4images_user_id: "1028"
_4images_image_date: "2013-04-02T15:32:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36823 -->
...die Seiten werden entfernt...
