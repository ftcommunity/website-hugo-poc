---
layout: "image"
title: "ft tshirt (ft Princess 3)"
date: "2007-12-20T17:36:17"
picture: "tshirt_c.jpg"
weight: "4"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["tshirt", "ft", "princess"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13121
- /detailsa7d5.html
imported:
- "2019"
_4images_image_id: "13121"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2007-12-20T17:36:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13121 -->
This is a t-shirt designed by Laura (and posted on Zazzle.com). It is being modelled by my daughter Amelia. She is a ft princess! (google translation-Dies ist ein T-Shirt entworfen von Laura (und auf Zazzle.com). Es wird nach dem Vorbild meiner Tochter Amelia. Sie ist eine Prinzessin!)