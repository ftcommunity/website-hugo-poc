---
layout: "image"
title: "fischertechnik 200"
date: 2021-04-12T11:15:44+02:00
picture: "ft200plusmot1-3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch toll an den Pralinenpackungen war, dass wirklich mit einem einzigen kurzen Blick kontrollierbar war, ob man alle Teile hatte. Dieser Teilebestand ist Anfänger-geeignet - ganz anders als ich den Teile-Wust in den heutigen Kästen finde, wo selbst in den kleinsten Modellkästen kaum auch nur zwei gleiche Teile vorkommen - wer soll da das fischertechnik-System verstehen und erlernen können?