---
layout: "image"
title: "1970er Grundkästen "
date: 2021-04-12T11:15:45+02:00
picture: "ft200plusmot1-2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist eine Seite der abgebildeten Anleitung - für die, die die Ur-fischertechnik-Kästen nicht kennen. Es gab ft 50, 100, 200, 300 (= 100 + mot.1 in etwa) und den großen 400. Den ft 200 finde ich bis heute einen super Kasten - guter Teilebestand in einem hervorragend ausgewogenen Mix, gut genug für viele wirklich tolle Modelle.