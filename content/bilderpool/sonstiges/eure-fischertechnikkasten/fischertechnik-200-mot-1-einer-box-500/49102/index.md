---
layout: "image"
title: "fischertechnik mot.1"
date: 2021-04-12T11:15:41+02:00
picture: "ft200plusmot1-5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der mot.1 enthielt den Ur-fischertechnik-Motor mit ein paar Zusatzteilen - immerhin auch ein Kardangelenk - und einen Batteriestab für drei Babyzellen mit links/rechts/aus-Schalter.