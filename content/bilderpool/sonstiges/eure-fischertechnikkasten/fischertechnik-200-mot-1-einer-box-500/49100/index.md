---
layout: "image"
title: "Bauplatte herausgenommen"
date: 2021-04-12T11:15:39+02:00
picture: "ft200plusmot1-7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Es sind genau die Teile des Ur-fischertechnik 200 und des Ur-mot.1 enthalten und einfach sortiert. Die Bauplatte hindert auch sehr gut die Kleinteile links unten daran, sich irgendwie aus der Box 500 herauszuarbeiten.

Beim nächsten Besuch der Kinder werde ich testen, wie das ankommt. Und in eine weitere Box 500 will ich noch versuchen einen Ur-fischertechnik-200S Statikkasten (wenn der Platz reicht, mit einem Ur-mot.2) unterzubringen. Das wäre auch ein tolles Set für einen Urlaub oder so.