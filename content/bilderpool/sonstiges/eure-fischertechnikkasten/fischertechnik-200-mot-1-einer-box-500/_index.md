---
layout: "overview"
title: "fischertechnik 200 + mot.1 in einer Box 500"
date: 2021-04-12T11:15:39+02:00
---

Ab und zu sind ein paar Kinder von Freunden hier. Die haben zwar ein wenig eigenes ft, aber nur diese kleinen Mini-Modellkästen. Wenn sie bei uns sind, dürfen sie nach Herzenslust mit allem, was da ist bauen.

Aber es gibt ein Problem: Die Teilevielfalt. Für Anfänger oder Gelegenheitsbauer sind wenige verschiedene Teile geeigneter als viele. Also gab ich den Kindern eine frühe Anleitung zu Grundkästen aus den 1970ern. Damit beschäftigten sie sich stundenlang fast ohne Hilfebedarf.

Zur Vorbereitung auf den nächsten Besuch habe ich versucht, einen Ur-fischertechnik 200 und einen Ur-mot.1-Kasten in eine Box 500 zu bringen Der ft 200 hat nämlich einen super ausgewogenen, aber leicht überschaubaren Teilebestand. Damit man mehr Modelle bauen kann, ist ein Motor natürlich toll. Und weil in der rechteckigen Anleitung zum ft 200 auch zwei interessante Modelle damit abgebildet sind, kam noch eine Segmentscheibe 31037 mit rein.