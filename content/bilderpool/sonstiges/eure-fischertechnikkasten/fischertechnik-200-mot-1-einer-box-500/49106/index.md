---
layout: "image"
title: "Das Endprodukt"
date: 2021-04-12T11:15:46+02:00
picture: "ft200plusmot1-1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Box 500 ist der Teilebestand eines fischertechnik 200 und eines fischertechnik mot.1. Diese und alle Vorgänger-Bauanleitungen (also alle für die 1960er/1970er Grundkästen) passen dazu, und man kann alle Modelle für ft 50, 100, 200, 300 plus die mit "+ mot.1" beim Bild bauen.