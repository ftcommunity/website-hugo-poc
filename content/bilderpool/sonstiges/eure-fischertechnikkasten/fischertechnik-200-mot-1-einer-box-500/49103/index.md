---
layout: "image"
title: "Ausbaukästen"
date: 2021-04-12T11:15:43+02:00
picture: "ft200plusmot1-4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Seite mit den Ausbaukästen (nicht zu verwechseln mit den kleinen Zusatzpackungen). Davon wollte ich den mot.1-Kasten mit drin haben.