---
layout: "image"
title: "ft 200 + mot.1 in einer Box 500"
date: 2021-04-12T11:15:40+02:00
picture: "ft200plusmot1-6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier das Endprodukt. Es passt sogar die gute alte Grundplatte 90 * 180 wunderbar rein. Wenn man nach Anleitung bauen will, ist die Bauplatte 500 der Box 500 zwar nett, aber eben nicht optimal fürs Nachbauen.

Ich habe extra aus meinem Bestand an grauen Baustein handverlesen welche ausgewählt, die noch ganz gut halten und nicht zu ausgeleiert sind. Deren Zapfen habe ich mit der in einer ft:pedia beschriebenen Methode nochmal kräftig reingedrückt.