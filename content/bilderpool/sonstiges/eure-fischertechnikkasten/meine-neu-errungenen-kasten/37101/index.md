---
layout: "image"
title: "Hobbywelt 1 und ein Baukasten der 50er Serie"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten06.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37101
- /detailsaa6f-2.html
imported:
- "2019"
_4images_image_id: "37101"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37101 -->
Da waren in dem Hobbywelt Kasten sogar noch alle Schneiddrähte vorhanden. Nur die Tube UHU Por war eingetrocknet.
