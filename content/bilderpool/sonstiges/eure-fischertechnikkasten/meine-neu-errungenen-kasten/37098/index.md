---
layout: "image"
title: "Da kommen die ersten Kästen zum Vorschein"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten03.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37098
- /detailsd6cb.html
imported:
- "2019"
_4images_image_id: "37098"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37098 -->
