---
layout: "image"
title: "Der Jet und der Statik 200S Kasten"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten10.jpg"
weight: "10"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37105
- /details7696-2.html
imported:
- "2019"
_4images_image_id: "37105"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37105 -->
