---
layout: "image"
title: "Weils so schön war gleich noch welche"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten04.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/37099
- /detailsba32.html
imported:
- "2019"
_4images_image_id: "37099"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37099 -->
