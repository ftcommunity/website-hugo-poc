---
layout: "image"
title: "Das Endprodukt"
date: 2021-04-21T15:18:17+02:00
picture: "ft200SplusZusaetze01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit dem Grundkasten fischertechnik 200 und dem Statikkasten 200S dazu konnte man eine ganze Reihe toller Modelle bauen. Dabei bleibt der Teilebestand noch einigermaßen übersichtlich. Und er passt in eine Box 500.