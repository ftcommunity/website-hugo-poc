---
layout: "image"
title: "Mit mot. 2"
date: 2021-04-21T15:18:10+02:00
picture: "ft200SplusZusaetze06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn man unbedingt will, bekommt man die Teile des Ur-mot.2 (Stufengetriebe) auch noch in den Kasten, aber es wird schon eng. Das würde ich eher nicht empfehlen, denn sonst kommt man nur umständlich an wichtige Teile heran.

(Auch hier fehlen die vier Spurkränze, die wohl noch irgendwie reingezwängt werden könnten, vielleicht in die Kassette, wo sie sich auch im Original ft 200S befanden)