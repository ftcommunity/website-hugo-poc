---
layout: "image"
title: "Mit mini-mot. 1"
date: 2021-04-21T15:18:09+02:00
picture: "ft200SplusZusaetze07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eher würde ich, wie hier vorgeschlagen, einen mini-mot. 1 aufnehmen. Dessen Teile passen prima in das untere kleine Fach, und die Scharniere und Winkellaschen darin finden im mittleren Fach ein akzeptables Plätzchen.

Die Kleinteile des mini-mot. 1, namentlich ein Verbinder 15 und die beiden Kontaktklemmen, packte ich in die Kassette, damit sie nicht verloren gehen können. Die beiden Kontaktklemmen waren fürs Anklemmen an die Kontaktstreifen einer 4,5-V-Flachbatterie gedacht und dürften für die Stromversorgung heutzutage entbehrlich sein.

Ein zweiadriges Kabel befindet sich oberhalb der Kassette.

Und hier habe ich auch endlich an die vier Spurkränze mit ihren Laufgummis gedacht.