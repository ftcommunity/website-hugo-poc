---
layout: "image"
title: "Modell mit mini-mot. 1 und Polwendeschalter"
date: 2021-04-21T15:18:06+02:00
picture: "ft200SplusZusaetze09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn wir zusätzlich einen Polwendeschalter einpacken, lässt sich dieser tolle Radarschirm nachbauen. Der raffinierte Antrieb mit dem von außen angetriebenen U-Getriebe des Minimotors führt zusammen mit dem Schalter zu einem permanenten Hin- und Herschwenken des Radarschirms in einer schön langsamen Geschwindigkeit.

Das war übrigens das erste Modell, dass ich mit meinem vermutlich zum Geburtstag bekommenen 200S gleich ganz früh am nächsten Morgen baute. Als alle anderen noch schliefen und es wohl auch noch dunkel war. :-)