---
layout: "image"
title: "Mit mini-mot. 1 und Polwendeschalter"
date: 2021-04-21T15:18:16+02:00
picture: "ft200SplusZusaetze10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Polwendeschalter findet leicht im Fach mit den X-Streben links unten seinen Platz und erweitert die Möglichkeiten von Motorsteuerungen natürlich erheblich.