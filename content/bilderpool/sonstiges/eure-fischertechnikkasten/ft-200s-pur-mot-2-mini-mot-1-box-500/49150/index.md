---
layout: "image"
title: "Laufschienen herausgenommen"
date: 2021-04-21T15:18:12+02:00
picture: "ft200SplusZusaetze04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ohne die Laufschienen sieht man genauer, wie die Bauelemente verteilt wurden. Im Kästchen befinden sich die 100 S-Riegel 4, 8 S-Riegel 8, 4 Riegelscheiben und 1 Riegelschlüssel.

(Auch hier fehlen die vier Spurkränze)