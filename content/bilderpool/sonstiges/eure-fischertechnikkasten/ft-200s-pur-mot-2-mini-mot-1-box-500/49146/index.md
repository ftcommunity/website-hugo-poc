---
layout: "image"
title: "Modell mit mini-mot. 1"
date: 2021-04-21T15:18:07+02:00
picture: "ft200SplusZusaetze08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit dem Minimotor lässt sich z.B. dieser Portalkran aus der Anleitung aufbauen.