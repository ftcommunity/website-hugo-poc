---
layout: "image"
title: "Lange Streben und obere Lage Statikträger herausgenommen"
date: 2021-04-21T15:18:11+02:00
picture: "ft200SplusZusaetze05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die herausgenommenen Streben I120 und X127,2 geben den Blick auf die Winkelsteine frei. Die obere Lage Statikträger ist ebenfalls herausgenommen, und man sieht, wie die Bogenstücke 60° in hobby-S-Manier in die Statikträger 120 passen.

(Auch hier fehlen die vier Spurkränze)