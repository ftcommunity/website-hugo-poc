---
layout: "image"
title: "Der Inhalt"
date: 2021-04-21T15:18:13+02:00
picture: "ft200SplusZusaetze03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Genau die Teile des 200S passen in eine Box 500. Folgende Anmerkungen:

- Die beiden Statikschienen liegen obenauf. So in der Mitte angeordnet geht der Deckel der Box (die Bauplatte 500) noch hinreichend gut zu.

- Links neben dem Kästchen 30x60x60 passen noch die beiden X-Streben 127,2 und die beiden I-Streben 120 in die Vertiefung der langen Stege.

- Die vier X-Streben 106 sind auch zu lang für die kleinen Fächer und fanden ihren Platz im freien Raum des mittleren rechten Fachs mit den Statikträgern 120.

- Vergessen hatte ich auf diesem Bild die vier Spurkränze nebst ihren Laufgummis. Die fänden aber Platz links oben bei den I-Streben oder rechts oben auf den Statikträgern 15.