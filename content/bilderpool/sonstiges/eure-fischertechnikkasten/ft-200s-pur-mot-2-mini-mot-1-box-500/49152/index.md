---
layout: "image"
title: "Statikkästen"
date: 2021-04-21T15:18:15+02:00
picture: "ft200SplusZusaetze02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist eine Seite der Statik-Anleitung aus den 1970er Jahren. Um den Inhalt des fischertechnik 200 S rechts oben geht es.