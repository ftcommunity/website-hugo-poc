---
layout: "image"
title: "Kabine"
date: "2009-06-15T11:19:44"
picture: "20052009152x.jpg"
weight: "3"
konstrukteure: 
- "OTIS"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/24370
- /details2975.html
imported:
- "2019"
_4images_image_id: "24370"
_4images_cat_id: "1670"
_4images_user_id: "729"
_4images_image_date: "2009-06-15T11:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24370 -->
Links im Bild erkennt man die seitlichen Kabinenführungen.