---
layout: "image"
title: "Schacht"
date: "2009-06-15T22:51:45"
picture: "20052009153x.jpg"
weight: "6"
konstrukteure: 
- "OTIS"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/24387
- /details9330.html
imported:
- "2019"
_4images_image_id: "24387"
_4images_cat_id: "1670"
_4images_user_id: "729"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24387 -->
Blick in den Schacht. In der Bildmitte sieht man das Umlenkhorn. Daran werden die Kabinen beim Umsetzen geführt.