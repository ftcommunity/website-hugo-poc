---
layout: "image"
title: "Obere Umsetzstelle"
date: "2009-06-15T11:19:44"
picture: "20052009150x.jpg"
weight: "1"
konstrukteure: 
- "OTIS"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/24368
- /details1a3f.html
imported:
- "2019"
_4images_image_id: "24368"
_4images_cat_id: "1670"
_4images_user_id: "729"
_4images_image_date: "2009-06-15T11:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24368 -->
Eine Kabine fährt durch die obere Umsetzstelle