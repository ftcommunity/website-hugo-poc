---
layout: "image"
title: "ft Plans on Barnes and Noble's nook"
date: "2011-04-08T00:27:03"
picture: "amelia_nook_b.jpg"
weight: "28"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["nook"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/30432
- /detailsb835.html
imported:
- "2019"
_4images_image_id: "30432"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2011-04-08T00:27:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30432 -->
I have been porting my ft construction plans to the nook ereader. Thought to share.