---
layout: "image"
title: "Das Peltier Element"
date: "2011-01-29T22:57:02"
picture: "peltierelement5.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/29823
- /detailsb779.html
imported:
- "2019"
_4images_image_id: "29823"
_4images_cat_id: "2195"
_4images_user_id: "558"
_4images_image_date: "2011-01-29T22:57:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29823 -->
