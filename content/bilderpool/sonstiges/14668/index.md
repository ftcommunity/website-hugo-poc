---
layout: "image"
title: "Cody's Construction"
date: "2008-06-10T16:51:11"
picture: "ft_cody.jpg"
weight: "7"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Cody", "Construction"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14668
- /detailsf35d.html
imported:
- "2019"
_4images_image_id: "14668"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2008-06-10T16:51:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14668 -->
Cody came in and built this today. Thought to share.