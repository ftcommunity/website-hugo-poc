---
layout: "image"
title: "Leckerer Brotaufstrich"
date: "2006-10-01T14:11:26"
picture: "Mrshausen_017.jpg"
weight: "1"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7045
- /detailsc913.html
imported:
- "2019"
_4images_image_id: "7045"
_4images_cat_id: "312"
_4images_user_id: "130"
_4images_image_date: "2006-10-01T14:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7045 -->
Interessante Verpackung, war aber sehr lecker der Inhalt.