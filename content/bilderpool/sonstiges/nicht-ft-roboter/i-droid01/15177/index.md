---
layout: "image"
title: "ziode / LED Augen"
date: "2008-09-05T14:13:51"
picture: "Immag118.jpg"
weight: "5"
konstrukteure: 
- ".zeuz."
fotografen:
- ".zeuz."
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15177
- /details12dd.html
imported:
- "2019"
_4images_image_id: "15177"
_4images_cat_id: "1106"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T14:13:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15177 -->
Die LED in den Augen, rot, gelb und grün, geben verschiedene Auskünfte, unter anderem sollen sie mal seine Emotive zustände, die er durch sein Neuronales Netz aus Umgebungseinflüsse bildet, ausdrücken.
