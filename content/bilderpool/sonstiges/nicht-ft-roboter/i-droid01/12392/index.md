---
layout: "image"
title: "ziode / Erste Baustufe"
date: "2007-11-04T14:53:58"
picture: "Immag004.jpg"
weight: "1"
konstrukteure: 
- ".zeuz."
fotografen:
- ".zeuz."
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/12392
- /details1640.html
imported:
- "2019"
_4images_image_id: "12392"
_4images_cat_id: "1106"
_4images_user_id: "634"
_4images_image_date: "2007-11-04T14:53:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12392 -->
Das ist er, ich hab ihn ziode getauft.
Er wird von mir lackiert, da mir das Original Silber  überhaupt nicht gefällt.
Im Bild hier zu sehen ist der fast fertige Kopf, der enthält 3 Mikrofone um Geräusche orten zu können, LEDs in Augen und Ohren. Außerdem die Elektronik um den Geräuschen zu „folgen“ (also Kopf links/rechts drehen), und den Motor & Getriebe für die Kopf auf- / ab- Bewegung.
