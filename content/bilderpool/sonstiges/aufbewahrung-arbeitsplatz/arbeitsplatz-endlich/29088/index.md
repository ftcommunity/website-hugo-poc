---
layout: "image"
title: "Koffer"
date: "2010-10-28T15:07:33"
picture: "endlich31.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29088
- /details3dae.html
imported:
- "2019"
_4images_image_id: "29088"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:33"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29088 -->
Koffer von oben
