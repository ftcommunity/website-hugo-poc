---
layout: "image"
title: "Kleinteilemagazin"
date: "2010-10-28T15:07:31"
picture: "endlich01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29058
- /detailsc43e.html
imported:
- "2019"
_4images_image_id: "29058"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29058 -->
So, hier nun mein Arbeitsplatz. 

Hier in diesem Holzregal befinden sich die Einsätze von BTI (BTI.de). Das Holzregal baute mein Vater für mich und hat Platz für insgesamt 24 Einsätze. Man kann das Regal natürlich auch beliebig noch erweitern. 

Das was ihr aber hier seht, ist längst nicht alles, viele Teile sind auch in Modelle verbaut.

Die Einsätze gehören normalerweise in die orangenen Koffer unter dem Tisch, aber dazu später mehr. Aber so ist es besser.
