---
layout: "image"
title: "Ft-Zimmer"
date: "2007-08-29T19:19:07"
picture: "DSC03075.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Aufbewahrung", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/11414
- /details3b41.html
imported:
- "2019"
_4images_image_id: "11414"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-29T19:19:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11414 -->
