---
layout: "image"
title: "Umbau 07"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle7.jpg"
weight: "7"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24897
- /details2633.html
imported:
- "2019"
_4images_image_id: "24897"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24897 -->
Wird die bearbeitete Box 3 von "Profi Oeko Tech" auf die Box vom "Profi Hydro Cell Kit" aufgestapelt, kann die Brennstoffzelle von unten hineinragen.
