---
layout: "comment"
hidden: true
title: "9820"
date: "2009-09-10T08:43:13"
uploadBy:
- "qincym"
license: "unknown"
imported:
- "2019"
---
Hallo Frederik,

Deine Prioritätenliste weicht offensichtlich ganz erheblich von meiner 
Prioritätenliste ab.

Dennoch möchte ich in Zukunft bei meiner Prioritätenliste bleiben und - Dein Einverständnis voraussetzend - genau das veröffentlichen, was ich veröffentlichen möchte.

Um Deinen unbändigen Wissensdurst zu stillen, fallen mir zwei Lösungswege ein.
Entweder
Du erarbeitest Dir die Anworten auf Deine Fragen selbst
oder
Du postest eine freundlich formulierte Anfrage nach einem Erfahrungsbericht
über die Brennstoffzelle im fischertechnik Forum.

Du befindest Dich hier bei
ftCommunity>Bilderpool>Exoten,Zubehör,Schnitzereien>Umbau Box 500 für die Brennstoffzelle.
Würde ich hier einen Erfahrungsbericht über die Brennstoffzelle ablegen, 
würde ich meinen Deutschlehrer aus dem Grabe rufen hören:
"Thema verfehlt! Setzen! Sechs!"

Viele Grüße
Volker-James