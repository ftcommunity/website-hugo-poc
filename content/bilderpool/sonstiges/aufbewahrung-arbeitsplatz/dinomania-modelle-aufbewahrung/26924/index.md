---
layout: "image"
title: "Ansicht von meinen Robo-Explorer"
date: "2010-04-11T23:16:17"
picture: "neuebildervonmeinenmodellen02.jpg"
weight: "2"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26924
- /details4444.html
imported:
- "2019"
_4images_image_id: "26924"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26924 -->
Der Kabelsalat bei diesem Modell
