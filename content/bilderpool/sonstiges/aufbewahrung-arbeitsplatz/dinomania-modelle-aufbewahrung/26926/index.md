---
layout: "image"
title: "Der Fire Truck und eine Rutsche"
date: "2010-04-11T23:16:18"
picture: "neuebildervonmeinenmodellen04.jpg"
weight: "4"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26926
- /details483b.html
imported:
- "2019"
_4images_image_id: "26926"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26926 -->
Die Rutsche ist durch eine Sperre blockierbar
