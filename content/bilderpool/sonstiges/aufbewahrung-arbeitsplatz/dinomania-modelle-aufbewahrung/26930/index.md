---
layout: "image"
title: "Der Schaufelradlader aus dem Kasten PFROF-PNEUMATIC II (2)"
date: "2010-04-11T23:16:18"
picture: "neuebildervonmeinenmodellen08.jpg"
weight: "8"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26930
- /details4359.html
imported:
- "2019"
_4images_image_id: "26930"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26930 -->
Die Schaufel mit dem Hebemechanismus, mit Druckluft gesteuert
