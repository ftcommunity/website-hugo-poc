---
layout: "image"
title: "Minimodelle, hier z. B. der Hafenkran"
date: "2010-04-11T23:16:19"
picture: "neuebildervonmeinenmodellen12.jpg"
weight: "12"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/26934
- /details58f2.html
imported:
- "2019"
_4images_image_id: "26934"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26934 -->
