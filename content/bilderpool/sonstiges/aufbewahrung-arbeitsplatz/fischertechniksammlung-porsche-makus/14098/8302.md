---
layout: "comment"
hidden: true
title: "8302"
date: "2009-01-21T23:27:56"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hier mal die Links zu den PMs (Power Motoren für die Suchfunktion :) bei Conrad weil es grad so schön passt:

1:50
http://www.conrad.de/goto.php?artikel=244023

1:125
http://www.conrad.de/goto.php?artikel=244031

1:312
http://www.conrad.de/goto.php?artikel=244040

Ob die 50'er exakt so schnell wie die von ft sind wäre mal interessant...

Die 125'er passen super als direkt Antrieb von 60'er Reifen, drehen sehr weich hoch.

Gruß,
Thomas