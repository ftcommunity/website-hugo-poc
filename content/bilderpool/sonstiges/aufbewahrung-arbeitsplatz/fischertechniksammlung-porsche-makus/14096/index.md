---
layout: "image"
title: "02"
date: "2008-03-24T21:17:24"
picture: "20080125_Fischertechniksammlung_11.jpg"
weight: "2"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14096
- /details6902.html
imported:
- "2019"
_4images_image_id: "14096"
_4images_cat_id: "1293"
_4images_user_id: "327"
_4images_image_date: "2008-03-24T21:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14096 -->
