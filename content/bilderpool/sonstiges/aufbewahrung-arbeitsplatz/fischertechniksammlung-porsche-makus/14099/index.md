---
layout: "image"
title: "05"
date: "2008-03-24T21:17:24"
picture: "20080317_Fischertechnik_sortieren_09.jpg"
weight: "5"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14099
- /details99b7.html
imported:
- "2019"
_4images_image_id: "14099"
_4images_cat_id: "1293"
_4images_user_id: "327"
_4images_image_date: "2008-03-24T21:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14099 -->
