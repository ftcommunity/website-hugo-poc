---
layout: "image"
title: "Bausteine 02"
date: "2014-05-18T19:01:36"
picture: "meinarbeitsundlagerplatz47.jpg"
weight: "47"
konstrukteure: 
- "MartinB"
fotografen:
- "MartinB"
uploadBy: "MartinB"
license: "unknown"
legacy_id:
- /php/details/38820
- /details43c0.html
imported:
- "2019"
_4images_image_id: "38820"
_4images_cat_id: "2899"
_4images_user_id: "1696"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38820 -->
Bausteine 15 + 30