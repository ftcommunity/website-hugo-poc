---
layout: "image"
title: "seltene Teile + Raritäten + mehr"
date: "2014-05-18T19:01:36"
picture: "meinarbeitsundlagerplatz10.jpg"
weight: "10"
konstrukteure: 
- "MartinB"
fotografen:
- "MartinB"
uploadBy: "MartinB"
license: "unknown"
legacy_id:
- /php/details/38783
- /detailsf64e.html
imported:
- "2019"
_4images_image_id: "38783"
_4images_cat_id: "2899"
_4images_user_id: "1696"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38783 -->
noch eingepackte Teile
in den 1000er Boxen sind Silberlinge und sehr viele Raritäten