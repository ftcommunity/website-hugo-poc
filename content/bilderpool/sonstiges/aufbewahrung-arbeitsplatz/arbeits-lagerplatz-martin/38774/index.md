---
layout: "image"
title: "Gesamtansicht 01"
date: "2014-05-18T19:01:35"
picture: "meinarbeitsundlagerplatz01.jpg"
weight: "1"
konstrukteure: 
- "MartinB"
fotografen:
- "MartinB"
uploadBy: "MartinB"
license: "unknown"
legacy_id:
- /php/details/38774
- /details6461.html
imported:
- "2019"
_4images_image_id: "38774"
_4images_cat_id: "2899"
_4images_user_id: "1696"
_4images_image_date: "2014-05-18T19:01:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38774 -->
Komplette Ansicht meines Arbeits- und Lagerzimmers
die 1000er Boxen links oben sind noch leer