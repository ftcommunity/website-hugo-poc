---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/38247
- /detailsf1bd.html
imported:
- "2019"
_4images_image_id: "38247"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38247 -->
Technik im Spiel...