---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/38251
- /detailscaa2.html
imported:
- "2019"
_4images_image_id: "38251"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38251 -->
FT Geometric 5000