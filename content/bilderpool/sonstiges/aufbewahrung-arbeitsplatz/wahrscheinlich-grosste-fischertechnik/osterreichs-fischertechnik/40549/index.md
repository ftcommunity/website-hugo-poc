---
layout: "image"
title: "Roboter-Armee (schräge Ansicht)"
date: "2015-02-16T17:29:12"
picture: "CIMG9656a.jpg"
weight: "65"
konstrukteure: 
- "Reinhold Schertler"
fotografen:
- "Reinhold Schertler"
uploadBy: "Striker01"
license: "unknown"
legacy_id:
- /php/details/40549
- /details4584-2.html
imported:
- "2019"
_4images_image_id: "40549"
_4images_cat_id: "2848"
_4images_user_id: "1689"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40549 -->
Fischertechnik 30300 Trainings-Roboter 
Fischertechnik 30554 1986 Teach In Roboter 
Fischertechnik 30572 1985 Trainingsroboter