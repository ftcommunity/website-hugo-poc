---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett39.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/38269
- /detailsae37.html
imported:
- "2019"
_4images_image_id: "38269"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38269 -->
FT Grand Prix