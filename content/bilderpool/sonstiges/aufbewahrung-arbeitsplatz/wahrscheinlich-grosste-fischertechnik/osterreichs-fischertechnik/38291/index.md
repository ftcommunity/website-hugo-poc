---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett61.jpg"
weight: "61"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/38291
- /details3400-2.html
imported:
- "2019"
_4images_image_id: "38291"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38291 -->
FT e5