---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/38250
- /details32be.html
imported:
- "2019"
_4images_image_id: "38250"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38250 -->
FT Industrieserie 6000 und Schule Serie5000