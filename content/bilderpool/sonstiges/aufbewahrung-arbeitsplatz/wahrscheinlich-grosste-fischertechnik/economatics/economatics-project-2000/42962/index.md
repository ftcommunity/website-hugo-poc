---
layout: "image"
title: "Economatics Project 2000 Box 3"
date: "2016-03-01T10:07:46"
picture: "economaticsproject05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42962
- /detailsfe60.html
imported:
- "2019"
_4images_image_id: "42962"
_4images_cat_id: "3194"
_4images_user_id: "1688"
_4images_image_date: "2016-03-01T10:07:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42962 -->
