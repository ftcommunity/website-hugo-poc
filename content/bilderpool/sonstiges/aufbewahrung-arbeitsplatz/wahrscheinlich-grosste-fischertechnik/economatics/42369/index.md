---
layout: "image"
title: "Economatics PIC-Logicator"
date: "2015-11-10T11:36:04"
picture: "economatics13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42369
- /detailsfef9.html
imported:
- "2019"
_4images_image_id: "42369"
_4images_cat_id: "3152"
_4images_user_id: "1688"
_4images_image_date: "2015-11-10T11:36:04"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42369 -->
