---
layout: "image"
title: "Economatics PIC-Logicator"
date: "2015-11-10T11:36:03"
picture: "economatics09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42365
- /details7188.html
imported:
- "2019"
_4images_image_id: "42365"
_4images_cat_id: "3152"
_4images_user_id: "1688"
_4images_image_date: "2015-11-10T11:36:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42365 -->
