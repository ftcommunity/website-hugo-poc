---
layout: "image"
title: "PIC Buggy von der Seite"
date: "2016-04-01T21:25:26"
picture: "Buggy_Seite_ss.jpg"
weight: "23"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["PIC", "Buggy"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43221
- /detailsc65b.html
imported:
- "2019"
_4images_image_id: "43221"
_4images_cat_id: "3152"
_4images_user_id: "579"
_4images_image_date: "2016-04-01T21:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43221 -->
siehe auch Video-Link: 

http://youtu.be/rc8h1piBmmw