---
layout: "image"
title: "Fischertechnik PIC Buggy"
date: "2016-04-01T21:25:26"
picture: "Buggy_s.jpg"
weight: "20"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Fischertechnik", "PIC", "Buggy"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43218
- /details1b42.html
imported:
- "2019"
_4images_image_id: "43218"
_4images_cat_id: "3152"
_4images_user_id: "579"
_4images_image_date: "2016-04-01T21:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43218 -->
Von schräg vorne betrachtet.

Link zum Video:

http://youtu.be/rc8h1piBmmw