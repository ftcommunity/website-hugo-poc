---
layout: "image"
title: "Economatics PIC-Logicator"
date: "2015-11-10T11:36:03"
picture: "economatics07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42363
- /details9a1e.html
imported:
- "2019"
_4images_image_id: "42363"
_4images_cat_id: "3152"
_4images_user_id: "1688"
_4images_image_date: "2015-11-10T11:36:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42363 -->
