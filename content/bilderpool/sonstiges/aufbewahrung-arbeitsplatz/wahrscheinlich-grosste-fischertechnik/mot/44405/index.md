---
layout: "image"
title: "mot _erste Serie"
date: "2016-09-25T21:53:05"
picture: "mot09.jpg"
weight: "9"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44405
- /detailsc16a.html
imported:
- "2019"
_4images_image_id: "44405"
_4images_cat_id: "3283"
_4images_user_id: "1688"
_4images_image_date: "2016-09-25T21:53:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44405 -->
