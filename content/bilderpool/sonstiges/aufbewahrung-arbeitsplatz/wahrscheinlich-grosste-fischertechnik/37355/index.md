---
layout: "image"
title: "Geometric 5000"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung40.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37355
- /details5c5a.html
imported:
- "2019"
_4images_image_id: "37355"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37355 -->
Geometric 5000