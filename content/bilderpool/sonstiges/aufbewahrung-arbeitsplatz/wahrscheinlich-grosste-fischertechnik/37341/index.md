---
layout: "image"
title: "1000 er Boxen"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung26.jpg"
weight: "26"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37341
- /details3f92.html
imported:
- "2019"
_4images_image_id: "37341"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37341 -->
Modell "Ducati" :-)
