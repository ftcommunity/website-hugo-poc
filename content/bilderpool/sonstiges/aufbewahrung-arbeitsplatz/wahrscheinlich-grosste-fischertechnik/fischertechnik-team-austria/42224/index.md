---
layout: "image"
title: "Anlieferung aus Frankreich"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett07.jpg"
weight: "7"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42224
- /detailsfc10.html
imported:
- "2019"
_4images_image_id: "42224"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42224 -->
