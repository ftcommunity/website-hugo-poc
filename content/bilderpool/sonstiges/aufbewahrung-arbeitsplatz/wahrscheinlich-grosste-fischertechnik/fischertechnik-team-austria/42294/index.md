---
layout: "image"
title: "Schwerlastkran"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett77.jpg"
weight: "77"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42294
- /details43e7.html
imported:
- "2019"
_4images_image_id: "42294"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42294 -->
