---
layout: "image"
title: "minimot 2"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett65.jpg"
weight: "65"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42282
- /detailsac27.html
imported:
- "2019"
_4images_image_id: "42282"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42282 -->
