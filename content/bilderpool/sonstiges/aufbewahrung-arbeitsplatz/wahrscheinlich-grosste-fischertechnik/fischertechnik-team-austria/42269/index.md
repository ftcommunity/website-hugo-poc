---
layout: "image"
title: "Der Hafenkran aus 1966"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett52.jpg"
weight: "52"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42269
- /details0b99.html
imported:
- "2019"
_4images_image_id: "42269"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42269 -->
