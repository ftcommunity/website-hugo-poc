---
layout: "comment"
hidden: true
title: "21226"
date: "2015-11-07T15:13:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die linke Version hat keine Klemmkontakte aufgedruckt, die rechte zusätzlich wohl ein Entstör-Zeichen. Ansonsten sehe ich auch nur leichte Variationen im Aufdruck, z.B. auf den Seiten der beiden mittleren.

Gruß,
Stefan