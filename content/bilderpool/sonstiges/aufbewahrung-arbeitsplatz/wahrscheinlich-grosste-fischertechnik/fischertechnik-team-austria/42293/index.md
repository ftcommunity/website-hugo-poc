---
layout: "image"
title: "Teleskop Mobilkran"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett76.jpg"
weight: "76"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42293
- /details6883-2.html
imported:
- "2019"
_4images_image_id: "42293"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42293 -->
