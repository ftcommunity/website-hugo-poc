---
layout: "image"
title: "FT Modelle"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle08.jpg"
weight: "8"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40217
- /detailsf832-2.html
imported:
- "2019"
_4images_image_id: "40217"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40217 -->
Flugzeuge 1966
