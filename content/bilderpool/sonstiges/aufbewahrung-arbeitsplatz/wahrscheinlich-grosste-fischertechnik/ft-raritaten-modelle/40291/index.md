---
layout: "image"
title: "FT l-e"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle82.jpg"
weight: "82"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40291
- /details666a.html
imported:
- "2019"
_4images_image_id: "40291"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40291 -->
l-e 1 aus 1972....
