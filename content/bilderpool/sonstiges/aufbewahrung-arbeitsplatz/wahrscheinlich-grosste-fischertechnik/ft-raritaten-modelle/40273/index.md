---
layout: "image"
title: "FT mot 4"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle64.jpg"
weight: "64"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40273
- /detailsbad9-2.html
imported:
- "2019"
_4images_image_id: "40273"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40273 -->
FT Ur Trafo mit "Bügeleisenschalter" und Nachfolger...
