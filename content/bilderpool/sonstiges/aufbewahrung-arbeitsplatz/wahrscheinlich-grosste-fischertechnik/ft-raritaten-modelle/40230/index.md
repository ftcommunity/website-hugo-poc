---
layout: "image"
title: "FT Computing"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle21.jpg"
weight: "21"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40230
- /detailsdc14-2.html
imported:
- "2019"
_4images_image_id: "40230"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40230 -->
Plotter Scanner mit Schneider 6128
