---
layout: "image"
title: "FT u-t"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle81.jpg"
weight: "81"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40290
- /detailsfe83-2.html
imported:
- "2019"
_4images_image_id: "40290"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40290 -->
u-t 3-2 und u-t 4-2...
