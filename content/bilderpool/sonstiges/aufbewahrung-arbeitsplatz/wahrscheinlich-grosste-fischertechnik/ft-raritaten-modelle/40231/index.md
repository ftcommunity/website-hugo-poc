---
layout: "image"
title: "FT Computing"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle22.jpg"
weight: "22"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40231
- /detailse4a9.html
imported:
- "2019"
_4images_image_id: "40231"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40231 -->
Trainingsroboter... Schule
