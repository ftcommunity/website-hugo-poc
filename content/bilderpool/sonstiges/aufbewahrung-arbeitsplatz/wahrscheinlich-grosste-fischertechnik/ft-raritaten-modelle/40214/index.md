---
layout: "image"
title: "FT Modell"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle05.jpg"
weight: "5"
konstrukteure: 
- "NN"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/40214
- /detailse41c.html
imported:
- "2019"
_4images_image_id: "40214"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40214 -->
Ducati
