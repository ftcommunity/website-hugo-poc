---
layout: "image"
title: "Lehr- und Demonstrationsprogramm 5000 Grundkasten und Ergänzung"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung34.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37349
- /details226a.html
imported:
- "2019"
_4images_image_id: "37349"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37349 -->
Lehr- und Demonstrationsprogramm 5000 Grundkasten und Ergänzung