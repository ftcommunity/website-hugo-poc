---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:50"
picture: "fischertechnikmoebel088.jpg"
weight: "88"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44284
- /detailse7ba.html
imported:
- "2019"
_4images_image_id: "44284"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:50"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44284 -->
Bausteine....
