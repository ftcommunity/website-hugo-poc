---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:49"
picture: "fischertechnikmoebel005.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44201
- /details68f9.html
imported:
- "2019"
_4images_image_id: "44201"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44201 -->
