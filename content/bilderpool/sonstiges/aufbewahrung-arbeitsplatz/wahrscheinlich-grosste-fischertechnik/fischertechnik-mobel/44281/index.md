---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:50"
picture: "fischertechnikmoebel085.jpg"
weight: "85"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44281
- /details375d.html
imported:
- "2019"
_4images_image_id: "44281"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:50"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44281 -->
Sonderteile
