---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:50"
picture: "fischertechnikmoebel075.jpg"
weight: "75"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44271
- /details15c4.html
imported:
- "2019"
_4images_image_id: "44271"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:50"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44271 -->
