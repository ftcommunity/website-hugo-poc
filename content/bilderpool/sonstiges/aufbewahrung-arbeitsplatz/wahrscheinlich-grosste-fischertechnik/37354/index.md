---
layout: "image"
title: "FT Panoramabild Schule und Hobby :-)"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung39.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37354
- /details879a-2.html
imported:
- "2019"
_4images_image_id: "37354"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37354 -->
Schul- und Hobbyprogramm