---
layout: "image"
title: "Fischertechnik im Fernsehn"
date: "2014-03-22T10:01:51"
picture: "fischerbeintv2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38477
- /details1dd9.html
imported:
- "2019"
_4images_image_id: "38477"
_4images_cat_id: "2871"
_4images_user_id: "968"
_4images_image_date: "2014-03-22T10:01:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38477 -->
Ist auf jeden Fall mal eine tolle Erfahrung, vor der Kamera zu stehen. Es wurden alle meine derzeitigen großen und kleinen
Modelle gefilmt und ich wurde ausgiebig zum Thema FISCHER und FISCHERTECHNIK interviewt.