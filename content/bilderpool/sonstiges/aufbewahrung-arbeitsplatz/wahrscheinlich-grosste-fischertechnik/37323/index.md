---
layout: "image"
title: "Nostalgie FT ab 1965"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37323
- /detailsfe5f.html
imported:
- "2019"
_4images_image_id: "37323"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37323 -->
Nostalgie pur FT 1965-1970 viele davon unbespielt