---
layout: "image"
title: "Lehr- und Demonstrationsprogramm 5000  Ergänzung"
date: "2013-09-08T11:13:32"
picture: "ftmodellsammlung38.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/37353
- /details31c9.html
imported:
- "2019"
_4images_image_id: "37353"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:32"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37353 -->
Lehr- und Demonstrationsprogramm 5000  Ergänzung