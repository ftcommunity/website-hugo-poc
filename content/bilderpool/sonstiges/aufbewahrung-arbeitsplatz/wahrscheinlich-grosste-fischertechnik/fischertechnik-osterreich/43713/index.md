---
layout: "image"
title: "Fischertechnik Sammlung von Heinz aus Graz"
date: "2016-06-09T11:17:03"
picture: "fischertechnikausoesterreich03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43713
- /details230d-2.html
imported:
- "2019"
_4images_image_id: "43713"
_4images_cat_id: "3238"
_4images_user_id: "1688"
_4images_image_date: "2016-06-09T11:17:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43713 -->
Pneumatik, Elektronik... für Fischertechniker