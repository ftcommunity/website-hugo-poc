---
layout: "image"
title: "8. Energieversorgung, Pneumatik"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish11.jpg"
weight: "11"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- /php/details/39690
- /details113d.html
imported:
- "2019"
_4images_image_id: "39690"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39690 -->
Schublade 8