---
layout: "image"
title: "1. Bausteine"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish04.jpg"
weight: "4"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- /php/details/39683
- /details29e2.html
imported:
- "2019"
_4images_image_id: "39683"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39683 -->
Schublade 1