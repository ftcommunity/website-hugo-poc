---
layout: "image"
title: "2. Bausteine, Motoren, Getriebe"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish05.jpg"
weight: "5"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- /php/details/39684
- /detailsd8ee-2.html
imported:
- "2019"
_4images_image_id: "39684"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39684 -->
Schublade 2