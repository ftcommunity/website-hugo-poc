---
layout: "image"
title: "4. Verkleidung, Seilwinden"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish07.jpg"
weight: "7"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- /php/details/39686
- /detailsc4a4.html
imported:
- "2019"
_4images_image_id: "39686"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39686 -->
Schublade 4