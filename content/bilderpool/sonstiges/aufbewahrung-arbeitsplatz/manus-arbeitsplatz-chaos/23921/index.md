---
layout: "image"
title: "weitere Wühl-Kiste"
date: "2009-05-08T23:33:18"
picture: "manusarbeitsplatz4.jpg"
weight: "4"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23921
- /details25ce.html
imported:
- "2019"
_4images_image_id: "23921"
_4images_cat_id: "1640"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23921 -->
eine von meinen Wühl-Kisten
