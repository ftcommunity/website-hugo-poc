---
layout: "image"
title: "[2/3] Etiketten Neuteile 2008"
date: "2010-08-16T10:35:34"
picture: "neuteileetiketten2.jpg"
weight: "9"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/27822
- /detailsd200.html
imported:
- "2019"
_4images_image_id: "27822"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2010-08-16T10:35:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27822 -->
Die Etiketten wurden zunächst in "erster Lesung" erarbeitet. Hier ist z.B. bei 132292K, 133053 und 503146 das Schwarz nicht richtig rübergekommen.
