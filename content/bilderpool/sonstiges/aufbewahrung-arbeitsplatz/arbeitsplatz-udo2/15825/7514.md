---
layout: "comment"
hidden: true
title: "7514"
date: "2008-10-07T23:48:02"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
@Udo2:
Habe Deinen obigen Text vor dem Posting schon gelesen, und meinte es eher als ein zusätzliches Element (aber Du hast ja kein Platz mehr...).
Ordnest Du die Sortierbehälter immer an der selben Stelle ein bzw. hat jedes seinen bestimmten Platz?
Gruß
Thomas