---
layout: "comment"
hidden: true
title: "7509"
date: "2008-10-07T22:13:57"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
Hallo,
das nenne ich mal ordentlich und den Platz gut ausgenutzt! Um nicht so oft aufstehen zu müssen, würde ich mir da noch so Kleinteilemagazine (die mit den kleinen durchsichtigen Fächern zum herausziehen) zulegen, um die meist gebrauchten Teile griffbereit zu haben.
Gruß
Thomas