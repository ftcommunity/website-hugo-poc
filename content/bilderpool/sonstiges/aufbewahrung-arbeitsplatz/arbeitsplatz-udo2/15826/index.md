---
layout: "image"
title: "[3/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:25"
picture: "arbeitsplatzvonudo3.jpg"
weight: "5"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15826
- /detailsfff0.html
imported:
- "2019"
_4images_image_id: "15826"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15826 -->
Teileregal:
Ausschnitt aus dem "Bereich alte Teile".
