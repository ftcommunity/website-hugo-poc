---
layout: "image"
title: "[5/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:25"
picture: "arbeitsplatzvonudo5.jpg"
weight: "7"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15828
- /detailsc7fe.html
imported:
- "2019"
_4images_image_id: "15828"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15828 -->
Modellboden:
Ganz oben die Modellablage mit einer Fläche von 1,20m x 0,30m. Klein meint ihr, nun wer das sagt schenke mir ein Haus damit ich auch ein ft-Studio einrichten kann! Immerhin sind hier trotz beengtem Platz z.Zt. abgestellt die ft-Modelle Trainingsroboter, 3-Säulen-Roboter und Pneumatischer Bagger sowie von mir die Bohr- und Fräsmaschine BF1 und meine beiden doch ziemlich großen 3D-XYZ.
