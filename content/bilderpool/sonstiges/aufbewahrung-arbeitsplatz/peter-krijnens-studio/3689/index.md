---
layout: "image"
title: "Studio von PK-9"
date: "2005-03-04T14:32:00"
picture: "PICT0012.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3689
- /detailse528.html
imported:
- "2019"
_4images_image_id: "3689"
_4images_cat_id: "442"
_4images_user_id: "144"
_4images_image_date: "2005-03-04T14:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3689 -->
Von rechts nach links:
Zahnräder, achsen, winkelsteinen, bausteinen, bauplatten.
Oben sind die drei ausleger er Kroll K10000 zu sehen.