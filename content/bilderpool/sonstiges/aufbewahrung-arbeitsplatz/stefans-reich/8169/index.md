---
layout: "image"
title: "Elektronik"
date: "2006-12-29T15:44:44"
picture: "stefansreich5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8169
- /details68e0.html
imported:
- "2019"
_4images_image_id: "8169"
_4images_cat_id: "754"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T15:44:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8169 -->
