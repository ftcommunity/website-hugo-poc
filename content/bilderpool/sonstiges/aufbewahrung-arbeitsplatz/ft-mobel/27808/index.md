---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel11.jpg"
weight: "11"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27808
- /details920c.html
imported:
- "2019"
_4images_image_id: "27808"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27808 -->
Schublade 7 , Bauplatten,große Teile , Kleinteilemagazine.
