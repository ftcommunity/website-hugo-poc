---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel05.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/27802
- /details6622.html
imported:
- "2019"
_4images_image_id: "27802"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27802 -->
Schublade 1 , Bausteine ,Winkelsteine
