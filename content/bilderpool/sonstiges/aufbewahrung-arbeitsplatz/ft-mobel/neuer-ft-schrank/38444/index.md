---
layout: "image"
title: "Neuer FT Schubladenschrank"
date: "2014-03-09T17:20:54"
picture: "neuerftschrank1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38444
- /details870c.html
imported:
- "2019"
_4images_image_id: "38444"
_4images_cat_id: "2866"
_4images_user_id: "968"
_4images_image_date: "2014-03-09T17:20:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38444 -->
Hier der neue Schrank mit den 12 Schubladen

Ich habe ca 8 Std für Zuschnitt ,verleimen,bohren gebraucht. Mein Vatter hat dann ca 10 Std geschliffen,lackiert, Schubladenböden und Grife montiert und ich habe dannach noch mal ca 6 Std. für die Montage der Schubladenschienen, Schubladen und der Rollen gebraucht. Also ca 24 Arbeitsstunden. Dazu 140 Euronen für die Schubladenschienen und 160 Euros für Holz,Lack,Rollen und Werkstattnutzung.
Kein billiges Vergnügen aber komfortabel und hochwertig :)
