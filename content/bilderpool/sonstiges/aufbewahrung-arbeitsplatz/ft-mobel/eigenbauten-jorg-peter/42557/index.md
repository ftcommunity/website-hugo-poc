---
layout: "image"
title: "Einfaches Regal 2"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_02.jpg"
weight: "1"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- /php/details/42557
- /details549f.html
imported:
- "2019"
_4images_image_id: "42557"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42557 -->
Eine weitere Ansicht des ganz einfachen Regals, in das 14 Sortierboxen eingesetzt werden können. Es kann mit wenigen Werkzeugen nachgebaut werden und ist im Materialaufwand ausgesprochen preiswert. Für die Stabilität hat es eine Rückwand. Der Entwurf kann in Höhe und Breite (eine Reihe, zwei Reihen, drei Reihen) beliebig variiert werden.