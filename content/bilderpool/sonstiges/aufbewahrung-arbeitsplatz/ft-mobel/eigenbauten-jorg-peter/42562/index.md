---
layout: "image"
title: "Regal 7"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_07.jpg"
weight: "6"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- /php/details/42562
- /details7255.html
imported:
- "2019"
_4images_image_id: "42562"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42562 -->
So sehen die Kästen geschlossen aus. Durch den Deckel kann man den Inhalt sehr gut erkennen, dennoch ist er prima vor Staub geschützt. Und vorne gibt es eine sckicke Lasche für die Beschriftung auf einem Streifen Papier - keine Klebeetiketten nötig!