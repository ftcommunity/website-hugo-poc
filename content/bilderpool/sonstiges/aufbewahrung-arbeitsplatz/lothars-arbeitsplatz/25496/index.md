---
layout: "image"
title: "Arbeitsplatz 3"
date: "2009-10-06T18:48:33"
picture: "AP3.jpg"
weight: "3"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/25496
- /details0853.html
imported:
- "2019"
_4images_image_id: "25496"
_4images_cat_id: "1784"
_4images_user_id: "10"
_4images_image_date: "2009-10-06T18:48:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25496 -->
Und noch mal aus der anderen Richtung
