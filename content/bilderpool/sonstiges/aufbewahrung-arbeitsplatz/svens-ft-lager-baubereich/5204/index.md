---
layout: "image"
title: "sven bastelecke 020"
date: "2005-11-05T16:00:38"
picture: "sven_bastelecke_020.jpg"
weight: "20"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/5204
- /detailsc222.html
imported:
- "2019"
_4images_image_id: "5204"
_4images_cat_id: "435"
_4images_user_id: "1"
_4images_image_date: "2005-11-05T16:00:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5204 -->
