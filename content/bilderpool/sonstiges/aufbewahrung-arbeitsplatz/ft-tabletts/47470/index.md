---
layout: "image"
title: "FT Tabletts"
date: "2018-04-25T21:47:01"
picture: "fttabletts3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/47470
- /detailsb87e.html
imported:
- "2019"
_4images_image_id: "47470"
_4images_cat_id: "3507"
_4images_user_id: "968"
_4images_image_date: "2018-04-25T21:47:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47470 -->
Jedes Tablett faßt 9 FT Sortiewannen.