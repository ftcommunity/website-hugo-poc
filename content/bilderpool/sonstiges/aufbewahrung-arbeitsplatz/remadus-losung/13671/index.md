---
layout: "image"
title: "Schublade"
date: "2008-02-17T15:39:32"
picture: "Schublade.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13671
- /details1a6d.html
imported:
- "2019"
_4images_image_id: "13671"
_4images_cat_id: "1257"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13671 -->
Die Innenhöhe der Schublade ist etwas mehr als 90 mm, weshalb sie sich so prima eignen. Außerdem passen gerade eben drei Sortiereinsätze hintereinander in die Schublade.

Selbst der große Zahnkranz kann jetzt aufrecht stehend untergebracht werden. Die Aufbewahrungsdichte des Materials nimmt enorm zu.

Teilweise ist es gelungen, den Inhalt einer gesamten Box 1000 in einer Schublade unterzubringen.
