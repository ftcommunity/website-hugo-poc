---
layout: "image"
title: "Kabelhalter 1"
date: "2011-08-20T14:58:02"
picture: "Kabelhalter_1.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31634
- /details1826.html
imported:
- "2019"
_4images_image_id: "31634"
_4images_cat_id: "2358"
_4images_user_id: "328"
_4images_image_date: "2011-08-20T14:58:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31634 -->
Ich habe mittlerweile dutzende FT-Kabel, vor allem von den kürzeren. Immer lagen die irgendwie auf einem Haufen, waren verknäult, und auch die Längen waren so schlecht zu erkennen. Ich habe schon einiges probiert, z.B. die Kabel an Haken aufzuhängen, aber das hält auch nicht sicher.

Es musste also eine dauerhafte Lösung her,

1. die nur aus FT-Teilen besteht,

2. die nur FT-Teile nutzt, von denen ich viel zu viel habe und selten/nie nutze (viel Statik),

3. an der Kabel nicht nur aufgehängt, sondern sicher geklemmt werden,

4. an der ich die Kabellängen sortieren und jederzeit problemlos erkennen kann,

5. die Platz für Elektro-Kleinteile (z.B. Stecker) und einen Schraubenzieher bietet und

6. die kompakt ist und seitlich an meinem Bauplatz aufgestellt werden kann.

Die Lösung hat unten einen sicheren Platz für den Kasten aus dem Lights-Baukasten, daneben 2 Schalen für Kleinteile.

Darüber die Kabelleiste mit Klemm-Haken aus Statikstreben, die sich sehr leicht nach vorn biegen lassen, um die Kabel zu verstauen. Die Kabel klemmen sehr fest und fallen nicht runter.

Jeweils eine Farbe der Statikstreben markiert eine Kabellänge, so dass ich diese gut und schnell erkennen kann.

Bei Bedarf lässt sich die Leiste an beiden Seiten erweitern bzw. mittels unterschiedlichen Farben der Statikstreben anders sortieren.

Der Schraubenzieher wird auch sicher verstaut und ist jederzeit gut erreichbar.