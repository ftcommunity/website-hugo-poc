---
layout: "image"
title: "Verkleidung"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14254
- /details254b.html
imported:
- "2019"
_4images_image_id: "14254"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14254 -->
Verkleidungsplatten , das meiste noch aus den 60'ern