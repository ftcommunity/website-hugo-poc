---
layout: "image"
title: "Bausteine"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14249
- /details3f99.html
imported:
- "2019"
_4images_image_id: "14249"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14249 -->
Bausteine, ein Großteil aus den 60'ern