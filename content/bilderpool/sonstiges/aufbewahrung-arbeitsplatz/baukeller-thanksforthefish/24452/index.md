---
layout: "image"
title: "Ansicht 1"
date: "2009-06-24T22:42:40"
picture: "P6170059.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/24452
- /details7c77.html
imported:
- "2019"
_4images_image_id: "24452"
_4images_cat_id: "1677"
_4images_user_id: "381"
_4images_image_date: "2009-06-24T22:42:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24452 -->
Spieltisch mit Bautisch
