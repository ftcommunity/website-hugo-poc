---
layout: "image"
title: "Arbeitsplatz 1 nah"
date: "2007-02-19T21:26:18"
picture: "Bild7.jpg"
weight: "6"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9071
- /detailsf8b1.html
imported:
- "2019"
_4images_image_id: "9071"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9071 -->
Hier noch mal das Interface, im hintergrund der weiße Kasten ist eine selbstbau lösung um mit dem Interface andere Spannungen zu schalten, es sind vier Relais 9V eingebaut die 12V oder jede beliebige andere Spannung über die Relais schalten Lemo Kompressor läuft bei mir mit 12V!