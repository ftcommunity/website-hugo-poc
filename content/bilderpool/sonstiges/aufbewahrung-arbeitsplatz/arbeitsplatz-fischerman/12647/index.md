---
layout: "image"
title: "Hier meine kleine Bauteile Sammlung"
date: "2007-11-11T08:33:13"
picture: "Bauteilesammlung_unter_FT-Tisch.jpg"
weight: "32"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12647
- /details09b7.html
imported:
- "2019"
_4images_image_id: "12647"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12647 -->
Kleine Bauteilesammlung,
Transistoren,Widerstände,Kondensatoren,Taster,Schalter,Steckverbindungen u.s.w. u.s.w.