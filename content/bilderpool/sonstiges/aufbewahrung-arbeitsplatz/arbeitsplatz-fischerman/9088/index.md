---
layout: "image"
title: "Rundblick der Werkstatt Bild 4"
date: "2007-02-19T21:26:40"
picture: "Rundblick22.jpg"
weight: "23"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9088
- /details0792.html
imported:
- "2019"
_4images_image_id: "9088"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9088 -->
