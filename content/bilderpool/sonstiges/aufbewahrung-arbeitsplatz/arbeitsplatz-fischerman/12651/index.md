---
layout: "image"
title: "Koffer Komplett"
date: "2007-11-11T08:33:13"
picture: "Komplettaufnahme_Bauteilesammlung.jpg"
weight: "36"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12651
- /details0577.html
imported:
- "2019"
_4images_image_id: "12651"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12651 -->
