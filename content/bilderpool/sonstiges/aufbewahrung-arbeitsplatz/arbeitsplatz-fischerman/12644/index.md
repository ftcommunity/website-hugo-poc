---
layout: "image"
title: "Intelligent+Extension Nahaufnahme"
date: "2007-11-11T08:33:13"
picture: "Nahaufnahme_IntelligentExtension.jpg"
weight: "29"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12644
- /details45f8.html
imported:
- "2019"
_4images_image_id: "12644"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12644 -->
