---
layout: "image"
title: "Rundblick der Werkstatt Bild 3"
date: "2007-02-19T21:26:40"
picture: "Rundblick20.jpg"
weight: "21"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9086
- /detailsa04e.html
imported:
- "2019"
_4images_image_id: "9086"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9086 -->
