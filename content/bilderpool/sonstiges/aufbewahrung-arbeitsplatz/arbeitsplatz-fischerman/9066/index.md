---
layout: "image"
title: "Arbeitsplatz 1"
date: "2007-02-19T21:26:18"
picture: "Bild1.jpg"
weight: "1"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9066
- /details9606.html
imported:
- "2019"
_4images_image_id: "9066"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9066 -->
Arbeitsplatz mit Pentium II 350 Mhz mit Windows 98 für Fischertechnik Soft und Hardware!