---
layout: "image"
title: "Testkofferunten nah"
date: "2007-11-11T08:33:14"
picture: "Testkoffer_unten_nah.jpg"
weight: "40"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/12655
- /details0120.html
imported:
- "2019"
_4images_image_id: "12655"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12655 -->
