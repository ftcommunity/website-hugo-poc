---
layout: "image"
title: "Rundblick der Werkstatt Bild 1"
date: "2007-02-19T21:26:33"
picture: "Rundblick18.jpg"
weight: "19"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9084
- /detailsbde5.html
imported:
- "2019"
_4images_image_id: "9084"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9084 -->
