---
layout: "image"
title: "Arbeitsplatz 1 rechts"
date: "2007-02-19T21:26:33"
picture: "Bild11.jpg"
weight: "12"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9077
- /details98ea.html
imported:
- "2019"
_4images_image_id: "9077"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9077 -->
und Kette und Zahnstangen ohne Ende.