---
layout: "image"
title: "Arberitsplatz 1 Schubladen"
date: "2007-02-19T21:26:18"
picture: "Bild5.jpg"
weight: "3"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/9068
- /details0478.html
imported:
- "2019"
_4images_image_id: "9068"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9068 -->
Schubladen für besondere Teile + Hefte