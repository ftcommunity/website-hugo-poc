---
layout: "image"
title: "Arbeisplatz (1)"
date: "2008-08-27T21:17:53"
picture: "Arbeitsplatz_1.jpg"
weight: "2"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
schlagworte: ["Box", "Kleinteile"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15112
- /details93e0.html
imported:
- "2019"
_4images_image_id: "15112"
_4images_cat_id: "1377"
_4images_user_id: "820"
_4images_image_date: "2008-08-27T21:17:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15112 -->
Kleinteile Box