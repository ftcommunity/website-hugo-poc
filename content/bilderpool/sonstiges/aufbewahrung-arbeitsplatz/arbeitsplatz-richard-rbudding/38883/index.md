---
layout: "image"
title: "EC1, Hobby 4 etc...."
date: "2014-05-29T22:53:45"
picture: "FTc2014_030.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/38883
- /details6d7d.html
imported:
- "2019"
_4images_image_id: "38883"
_4images_cat_id: "2905"
_4images_user_id: "371"
_4images_image_date: "2014-05-29T22:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38883 -->
