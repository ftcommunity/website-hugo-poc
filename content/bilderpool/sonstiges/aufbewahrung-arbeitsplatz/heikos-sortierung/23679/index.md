---
layout: "image"
title: "IKEA Hacking - Rückwand"
date: "2009-04-13T00:33:54"
picture: "Heikos_Sortierung-016.jpg"
weight: "12"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23679
- /detailsea3e-2.html
imported:
- "2019"
_4images_image_id: "23679"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:33:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23679 -->
Die Schubladen sind geringfügig kürzer als man es brauchen könnte. Was remadus also "geringfügige Modifikation" nennt, erfordert den Einsatz von fachmännischem Werkzeug.

In meinem Fall, wenn man keine Blechschere hat: Kabelbinder. Man kann das Rückteil falschherum einsetzen, und das mit vier Kabelbindern befestigen. Hält ausreichend gut.

Erst dadurch kann man drei Box-1000-Einsätze quer hintereinander unterbringen.