---
layout: "image"
title: "Schublade 6"
date: "2009-04-13T00:28:11"
picture: "Heikos_Sortierung-013.jpg"
weight: "11"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["anbauwinkel"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23678
- /details8235.html
imported:
- "2019"
_4images_image_id: "23678"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:28:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23678 -->
Links: Räder. 

Rechts: Bauplatten, Drehkränze, U-Träger, Anbauwinkel. In der blauen Box 500 sind Kisten und Tonnen aus dem 3-6-Programm etc..