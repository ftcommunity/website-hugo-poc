---
layout: "image"
title: "IKEA Hacking 2 - Schubladenstop"
date: "2009-04-13T00:37:33"
picture: "Heikos_Sortierung-017.jpg"
weight: "13"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23680
- /detailsecfc-2.html
imported:
- "2019"
_4images_image_id: "23680"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:37:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23680 -->
Unten laufen die Kabelbinder unter der Unterseite der Schublade entlang. Sie rutschen also auf der Schiene und heben den Kasten etwas an.

Wenn die Schublade ganz geschlossen wird, sackt sie ab, weil die Schiene nicht so weit zurückreicht und der Kabelbinder dann in der Luft hängt. Also fällt der Kasten ein paar Millimeter runter.

Also braucht es jetzt etwas mehr Gefühl beim Herausziehen. Positiv formuliert: Die Schublade hält deutlich besser zu und fühlt sich an wie eine selbstschließende.