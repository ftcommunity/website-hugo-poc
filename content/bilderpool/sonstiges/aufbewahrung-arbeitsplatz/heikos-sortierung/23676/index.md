---
layout: "image"
title: "Schublade 5 oben"
date: "2009-04-13T00:25:31"
picture: "Heikos_Sortierung-011.jpg"
weight: "9"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23676
- /detailse4e4.html
imported:
- "2019"
_4images_image_id: "23676"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:25:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23676 -->
Links: Statik 2. Sämtliche Streben, gelbe Bauplatten. 

Rechts: Große Zahnräder, komische rote Bauteile, U-Träger (nicht farblich sortiert).