---
layout: "image"
title: "Schublade 2 unten"
date: "2009-04-13T00:18:30"
picture: "Heikos_Sortierung-006.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23671
- /detailse1fe.html
imported:
- "2019"
_4images_image_id: "23671"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23671 -->
Links: Motor und Getriebe.

Rechts: Rote Kleinteile. Hinten werden Winkelsteine, Bausteine 5, Bauplatten 15*30*5 usw. archiviert. Vorne auch nochmal. Ein paar Spalten sind noch frei.