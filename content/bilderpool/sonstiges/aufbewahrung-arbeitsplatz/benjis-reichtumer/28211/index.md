---
layout: "image"
title: "Mini Antriebe"
date: "2010-09-25T12:15:36"
picture: "100_0151A.jpg"
weight: "7"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/28211
- /details89aa.html
imported:
- "2019"
_4images_image_id: "28211"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28211 -->
