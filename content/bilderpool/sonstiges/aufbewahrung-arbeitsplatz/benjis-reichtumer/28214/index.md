---
layout: "image"
title: "Aufbewarung"
date: "2010-09-25T12:15:36"
picture: "100_0155A.jpg"
weight: "10"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/28214
- /details3ea1.html
imported:
- "2019"
_4images_image_id: "28214"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28214 -->
In dies Ecke steht nicht alles.