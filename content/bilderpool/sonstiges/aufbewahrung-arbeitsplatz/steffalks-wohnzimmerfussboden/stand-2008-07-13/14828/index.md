---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 8"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14828
- /details4253.html
imported:
- "2019"
_4images_image_id: "14828"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14828 -->
Statik, Platten. (Ja, auch von den guten alten Flachsteinen 30 und 60 zeigen die auf der Unterseite befindlichen "Fisch"-Symbole in dieselbe Richtung.)
