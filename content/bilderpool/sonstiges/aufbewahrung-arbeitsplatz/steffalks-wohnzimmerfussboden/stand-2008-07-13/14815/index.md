---
layout: "image"
title: "Gesamtansicht"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14815
- /details44a0.html
imported:
- "2019"
_4images_image_id: "14815"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14815 -->
My fischertechnik-U is my castle. ;-)
