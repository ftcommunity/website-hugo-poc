---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 4"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14822
- /details19cb.html
imported:
- "2019"
_4images_image_id: "14822"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14822 -->
Grundbausteine, die guten alten kleinen Elektromechanik-Relais aus dem em-5, ein paar der wunderbaren Silberlinge. Um ein Beispiel zu geben, was es heißt, sein fischertechnik zu lieben: Die Bausteine 30 hier stehen alle senkrecht in den Sortiereinsätzen. Deren Nut ist also unsichtbar am Boden. Die Nuten sind *alle* identisch in Längsrichtung der Sortiereinsätze ausgerichtet. Solche Details könnte ich noch eine ganze Reihe aufzählen. ;-)
