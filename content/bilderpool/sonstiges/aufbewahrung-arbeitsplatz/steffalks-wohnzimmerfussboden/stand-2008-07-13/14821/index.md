---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 3"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14821
- /detailsbe43.html
imported:
- "2019"
_4images_image_id: "14821"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14821 -->
Neben Standardteilen die älteren Kardangelenke, 80-Jahre-Elektronikbausteine und die drei freundlicherweise von Dirk Haizmann (fischertechnik) auf der Convention 2007 anlässlich eines Gesprächs, wie man die Digitaluhr etwas weniger geheimschriftlastig machen könnte, gesponserten Pneumatik-Magnetventile (Danke Dirk! Extrem nett!).
