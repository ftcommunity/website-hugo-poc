---
layout: "overview"
title: "Stand 2008-07-13"
date: 2020-02-22T09:20:27+01:00
legacy_id:
- /php/categories/1354
- /categories7bcd.html
- /categories400f.html
- /categoriese684.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1354 --> 
So sieht\'s aus, wenn ich auf dem Boden rumkrabbeln muss. In absehbarer Zeit kann ich hoffentlich einen ständig aufgebauten Arbeitsplatz einrichten, bei dem ich auf einem Stuhl sitzen kann.