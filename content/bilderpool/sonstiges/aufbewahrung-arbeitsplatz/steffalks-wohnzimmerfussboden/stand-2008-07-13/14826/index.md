---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 6"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14826
- /detailsf8dc.html
imported:
- "2019"
_4images_image_id: "14826"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14826 -->
Räder, Reifen und die guten alten Gummiringe, die über die alten Reifen 45 passen.
