---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 10"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14830
- /details80b9.html
imported:
- "2019"
_4images_image_id: "14830"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14830 -->
Immer noch Statik, Motore mit Zubehör, Akkus und ein paar der guten alten Antriebsfedern. Links unten im Bild sieht man die Abdeckplatten der ft 1000 und unter der grauen Bauplatte 500 (vom Digitalpraktikum) kann man 5 schwarze Bauplatten 500 erahnen.
