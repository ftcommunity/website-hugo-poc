---
layout: "image"
title: "Rechte Seite"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7159
- /details9624.html
imported:
- "2019"
_4images_image_id: "7159"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7159 -->
nebst den 10 ft 1000 und 5 500er Bauplatten
