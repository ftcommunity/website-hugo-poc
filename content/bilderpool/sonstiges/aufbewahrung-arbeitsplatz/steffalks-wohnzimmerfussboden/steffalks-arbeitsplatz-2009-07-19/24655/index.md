---
layout: "image"
title: "13 - Schublade rechts 1"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24655
- /details2572.html
imported:
- "2019"
_4images_image_id: "24655"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24655 -->
Motoren, Getriebeaufsätze, Zahnstangen.
