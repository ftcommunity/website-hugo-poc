---
layout: "image"
title: "15 - Schublade rechts 3"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24657
- /detailsc017.html
imported:
- "2019"
_4images_image_id: "24657"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24657 -->
Statikstreben, Schienen und Schiffskörperteile.
