---
layout: "image"
title: "06 - Schublade links 4 sichtbare Lage"
date: "2009-07-19T20:07:11"
picture: "steffalksarbeitsplatz06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24648
- /details2230.html
imported:
- "2019"
_4images_image_id: "24648"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24648 -->
Die Schubladen 4 bis 6 jedes Schrankes (also die unteren 3 von 6) sind doppelt hoch und deshalb zumindest teilweise zweilagig bestückt. Fotografiert ist immer die sichtbare Lage. An Stellen, die nur einlagig bestückt sind, ist das zwar die untere, aber so sieht man es eben beim Schublade Aufziehen.

Hier also die sichtbare Lage der Schublade 4: Zahnräder, Drehscheiben, Ketten, Differentiale, Gelenksteine mit schwarzen (mittig links) und roten (mittig rechts daneben) Zapfen, Federgelenksteine (links oben die beiden Reihen), Seilrollen (schwarz, rot mit Innenrille, rot ohne Innenrille).

Links nebendran Elektromechanik-Metallachsen, die man aneinander stecken und damit als lange Stromschienen einsetzen kann.
