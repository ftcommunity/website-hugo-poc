---
layout: "image"
title: "03 - Schublade links 1"
date: "2009-07-19T20:07:11"
picture: "steffalksarbeitsplatz03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24645
- /detailsd90e.html
imported:
- "2019"
_4images_image_id: "24645"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24645 -->
Die folgenden Bilder zeigen die Schubladen des linken Schrankes von oben nach unten. Hier die oberste Schublade: Im Wesentlichen Grundbausteine.

Ganz hinten in fast jeder Schublade sitzen die 60 * 60 * 30-Kästchen mit Deckel. Die haben nämlich genau die richtige Dicke, um den Rest der Schublade auszufüllen. Auf diese Weise rutschen die Sortierkästen nicht nach hinten. Links nebendran längere Alustäbe.
