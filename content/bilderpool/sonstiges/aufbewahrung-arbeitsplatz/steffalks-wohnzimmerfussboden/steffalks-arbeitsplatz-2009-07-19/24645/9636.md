---
layout: "comment"
hidden: true
title: "9636"
date: "2009-07-23T03:27:49"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Das ist ja auch immer das, was ich bei der Einführung von GELB befürchtet
habe: irgendwann kommt (wohl leider) der Punkt, an dem Fischertechnik in
allen erdenklichen Farben produziert wird. Ich selbst habe meinen Bestand
mittlerweile so sortiert, daß wenn ich z. B. nur in grau bauen möchte, dann
alle gelben Teile in Extrakästen/ Schubladen verstaut bleiben können. Und
bei gelben Modellen bleibt dann eben grau nahezu komplett verstaut. Mit
schwarz ist das einfacher, da diese Farbe immer harmoniert.

Mit roten und gelben Bausteinen (und gar roten Winkelträgern) und was weiß
noch so für Farben sähe es wahrscheinlich ähnlich aus. Funktionsmodelle
sind, eben weil hier die Funktion im Fordergrund steht, eher einfarbig. Das
kann dann auch mal "quietschgelb" sein und einen Baukran originaler aus-
sehen lassen. Aber ein "martialischer" Getriebeblock in knallbunten Farben
sähe m. E. doch eher lächerlich aus.

Andererseits ist es jedoch auch so, daß wenn ein x-beliebiges Modell explizit
in nur einer Farbe gebaut würde (also auch gelbe S-Riegel, gelbe Laschen,
gelbe Winkelsteine und normale - eben ALLE Bautelie), sähe ein Modell ja
auch irgendwie langweilig aus.

Gruß, Thomas