---
layout: "image"
title: "22 - Sortierkästen"
date: "2009-07-19T20:07:13"
picture: "steffalksarbeitsplatz23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24665
- /detailsdf66-2.html
imported:
- "2019"
_4images_image_id: "24665"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:13"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24665 -->
In den Sortierkästen lagern noch die Originalwannen der hobby-, ut- und Digital-Praktikums-Kästen. Evtl. kommt da auch mal das gedruckte Material (Prospekte, Anleitungen, Clubhefte) rein.
