---
layout: "comment"
hidden: true
title: "22246"
date: "2016-07-22T15:36:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Zufällig stoße ich gerade darauf: Im Clubheft 1972-1 (von dem es aber wohl zwei leicht unterschiedliche Varianten gab) auf Seite 5 ist besagtes Schleifring-Großmodell mit Artur Fischer davor abgebildet. Auf demselben Bild sieht man auch das Riesen-LKW-Fahrwerk, was wir schon mal wo anders ansprachen.

Gruß,
Stefan