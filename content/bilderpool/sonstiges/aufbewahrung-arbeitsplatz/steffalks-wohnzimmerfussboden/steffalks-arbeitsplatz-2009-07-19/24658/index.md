---
layout: "image"
title: "16 - Schublade rechts 4 sichtbare Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24658
- /details9f2e-2.html
imported:
- "2019"
_4images_image_id: "24658"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24658 -->
Alle Statikträger.
