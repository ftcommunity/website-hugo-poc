---
layout: "image"
title: "10 - Schublade links 6 sichtbare Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24652
- /details7a74.html
imported:
- "2019"
_4images_image_id: "24652"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24652 -->
In der sichtbaren Lage der untersten Schublade befinden sich Verkleidungsplatten, Seilrollen, Lenkungsteile und ganz selten (wenn überhaupt) benötigte Achsformen.
