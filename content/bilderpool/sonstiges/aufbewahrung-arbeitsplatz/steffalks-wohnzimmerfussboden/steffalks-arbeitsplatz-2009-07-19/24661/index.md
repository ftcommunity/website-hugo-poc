---
layout: "image"
title: "19 - Schublade rechts 5 verdeckte Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24661
- /detailsd1ac-2.html
imported:
- "2019"
_4images_image_id: "24661"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24661 -->
Untendrunter liegen die hobby-Elektronikbausteine ("Silberlinge"), optische Blenden, Linsen, 80er-Jahre-Elektronikbausteine, Zählwerke und einige der kleinen em-5-Relais.
