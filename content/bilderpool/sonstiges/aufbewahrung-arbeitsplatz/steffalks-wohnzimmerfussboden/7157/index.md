---
layout: "image"
title: "Linke Seite"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7157
- /details4e83.html
imported:
- "2019"
_4images_image_id: "7157"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7157 -->
