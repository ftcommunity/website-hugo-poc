---
layout: "image"
title: "hobby-Kästen"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7158
- /details4c04.html
imported:
- "2019"
_4images_image_id: "7158"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7158 -->
hobby 1, 2, S, 3, 4 aus den 70er Jahren
