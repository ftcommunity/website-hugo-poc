---
layout: "image"
title: "Schrank 1 Schublade 4 sichtbare Lage"
date: 2022-01-15T19:17:17+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist die erste zweilagig bestückte Schublade. Die obere sichtbare Lage enthält Grundbausteine, die komischen "Design"-Teile, Federn, Reedkontakt-/Kabelhalter.