---
layout: "image"
title: "Schrank 3 Schublade 5 sichtbare Lage"
date: 2022-01-15T19:16:51+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Statikträger 120, Flachträger, Flach- und Bogenstücke 30° und 60°.