---
layout: "image"
title: "Schrank 3 Schublade 1"
date: 2022-01-15T19:16:58+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Statik-Kleinteile, hobby-S/ut-S Statikträger-Aufnahmen, Achsaufnahmen, Schaufeln, Greifer, Seiltrommeln, Haken, Seile, Baggerschaufeln, Kraftmesser.