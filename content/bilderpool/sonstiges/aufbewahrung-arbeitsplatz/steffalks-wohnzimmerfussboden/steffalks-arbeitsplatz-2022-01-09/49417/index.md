---
layout: "image"
title: "Kabel (2)"
date: 2022-01-15T19:16:56+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Lange und zweiadrige Kabel, Kabel für die 1980er-Jahre-Elektronikbausteine.