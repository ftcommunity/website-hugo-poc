---
layout: "image"
title: "Schrank 4 Schublade 5 sichtbare Lage"
date: 2022-01-15T19:16:40+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Akkus, Batteriehalter, Fernsteuerungen, Ladegerät, Robo- und TXT-Interface, Anleitungen zu den Fernbedienungen (zum schnellen Nachschlagen der DIP-Schalter und Anschlüsse), USB-Kabel, das RF Data Link (Funkschnittstelle fürs Robo Interface) und ein netterweise von thkais selbst gebautes Seriell/USB-Interface.