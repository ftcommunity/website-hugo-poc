---
layout: "image"
title: "Schrank 1 Schublade 5 verdeckte Lage"
date: 2022-01-15T19:17:14+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Verdeckt sind andersfarbige Vorstuferäder und weitere alte Differenziale.