---
layout: "image"
title: "Schrank 4 Schublade 2"
date: 2022-01-15T19:16:45+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Pneumatik-Zylinder, Einfach- und Doppelbetätiger, Festo-Ventile in rot (Schließer) und blau (Öffner), Pneumatikzubehör, Kompressorteile, Drosseln, Handventile, Manometer, Elektroventile, Drucktanks, eine Lemo-Solar-Pumpe, Solarzellen und -motoren, Propellerflügel. Im mittleren Einsatz rechts eine freundlicherweise geschenkte selbstgebaute Pneumatik-Drehdurchführung.