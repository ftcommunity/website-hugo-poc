---
layout: "image"
title: "Schrank 2 Schublade 1"
date: 2022-01-15T19:17:10+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Verschiedene Ketten, Kettenschaufeln, Kettenbeläge, Haushaltsgummis, Ur-Antriebsfedern, Ersatz-Trennstege für die 1000er-Einsätze, Ur-Raupenbänder, Stecker, le-1/ut-4 Zentrierplatten für optische Bänke, Spiralschlauch für schöne Verkabelungen, Litze und Flachbandkabel.