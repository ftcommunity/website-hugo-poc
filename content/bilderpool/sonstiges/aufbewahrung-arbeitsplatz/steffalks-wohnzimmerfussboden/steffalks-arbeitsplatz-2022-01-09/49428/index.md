---
layout: "image"
title: "Kabel (1)"
date: 2022-01-15T19:17:09+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Normale Kabel von kurz bis länger.