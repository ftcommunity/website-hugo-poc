---
layout: "image"
title: "Schrank 1 Schublade 3"
date: 2022-01-15T19:17:19+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Naben (gleich auseinandergenommen, weil man sie ja eh so braucht), Freilaufnaben, K-, Rast- und Metallachsen, Seilrollen, Kurven- und Nockenscheiben, sowie ein weißes Döschen mit Vaseline zum Schmieren von Achsen. Links nebendran und vorne lange Achsen. Die ganz langen vorne sind 50-cm-Achsen von Conrad.