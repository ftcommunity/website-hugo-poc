---
layout: "image"
title: "Schrank 4 Schublade 5 verdeckte Lage"
date: 2022-01-15T19:16:39+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Ur-Parallel-Interface, die Robo Connect Box dafür, ein Akku, Servo, IR-Empfänger, Extensions fürs Robo Interface, Schrittmotoren, USB-Kabel, Parallel-Interface-Anschlusskabel.