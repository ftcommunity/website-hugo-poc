---
layout: "image"
title: "Schrank 2 Schublade 3"
date: 2022-01-15T19:17:06+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

1969er l-e1/ut-4 Lichtelektronik-Schaltstäbe, 1970er-Jahre-Silberlinge, em-5-Relais, 1980er-Jahre-IC-Elektronikbausteine, Lautsprecher, ICs, em-6-Zählwerke.