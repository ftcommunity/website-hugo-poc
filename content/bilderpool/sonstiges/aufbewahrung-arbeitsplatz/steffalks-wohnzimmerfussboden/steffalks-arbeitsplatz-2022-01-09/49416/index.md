---
layout: "image"
title: "Schrank 3 Schublade 3"
date: 2022-01-15T19:16:54+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alle I-Streben, Flexschienen und Teile dazu, Gewichte, selten gebraute Lenkungsteile sowie die Ur-ft-Pappstücke aus dem fischertechnik 200 (Trommel, Radarschirm, Betonmischer). Nebendran ältere Statikschienen.