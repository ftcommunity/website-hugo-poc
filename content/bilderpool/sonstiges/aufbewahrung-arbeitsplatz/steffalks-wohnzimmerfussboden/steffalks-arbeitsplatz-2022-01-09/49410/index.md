---
layout: "image"
title: "Schrank 3 Schublade 6 verdeckte Lage"
date: 2022-01-15T19:16:47+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Andersfarbige Verkleidungsplatten, Klipsplatten-Verbinder, Türen, Giebelteile und weil sie dazu passen hobby-S-/ut-S-Platten, mit denen Knotenplatten 30 x 45 verbunden waren. Weitere Bauplatten 60 x 120.