---
layout: "image"
title: "Schrank 4: Motoren, Pneumatik, Stromversorgung, Fernsteuerungen, Computing"
date: 2022-01-15T19:16:30+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts hinter einem gibt es Motoren, Zahnstangen, Pneumatik, Solarteile, Stromversorgungen, Akkus, Fernsteuerungen, ft-Computing-Interfaces, Werkzeug, NetDuinos fürs .net nanoFramework.