---
layout: "image"
title: "Schrank 2 Schublade 5"
date: 2022-01-15T19:17:04+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Bauplatten 1000 schwarz und grau mit Platte an der Unterseite, Bauplatten 500 schwarz und eine in grau.