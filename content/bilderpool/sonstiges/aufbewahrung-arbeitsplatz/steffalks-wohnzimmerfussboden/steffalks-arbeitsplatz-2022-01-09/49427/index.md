---
layout: "image"
title: "Schrank 2 Schublade 2"
date: 2022-01-15T19:17:08+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Lampen, LEDs, Leuchtkappen, Kontaktstücke, Schalter, Taster, Verteilerplatten, Kugeln, 128 Neodym-Magnete, ft-Dauer- und Elektromagnete, Rückschlussplatten, Schwingfedern, Lampensockel, Schraubendreher, Summer, Federn, Federstäbe, Schleifringe und zugehörige Unterbrecher, Schaltscheiben.