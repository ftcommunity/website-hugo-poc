---
layout: "image"
title: "Schrank 2: Ketten, Elektromechanik, Elektronik, Bauplatten"
date: 2022-01-15T19:16:32+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Links hinter dem Stuhl findet sich Ketten, Raupengummis, Litze, Elektromechanik, Elektronik aller Jahrgänge, Sensoren, Optik und Bauplatten 500 und 1000.