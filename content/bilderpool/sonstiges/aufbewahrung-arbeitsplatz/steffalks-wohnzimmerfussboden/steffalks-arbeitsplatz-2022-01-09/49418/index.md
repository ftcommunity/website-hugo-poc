---
layout: "image"
title: "Schrank 3 Schublade 2"
date: 2022-01-15T19:16:57+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alle I-Streben, seltener genutzte Statikteile. Nebendran neuere Statikschienen.