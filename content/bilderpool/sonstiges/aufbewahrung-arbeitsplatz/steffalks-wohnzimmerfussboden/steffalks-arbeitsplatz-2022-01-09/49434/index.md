---
layout: "image"
title: "Schrank 1 Schublade 4 verdeckte Lage"
date: 2022-01-15T19:17:16+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Lage tiefer finden sich weitere Grundbausteine.