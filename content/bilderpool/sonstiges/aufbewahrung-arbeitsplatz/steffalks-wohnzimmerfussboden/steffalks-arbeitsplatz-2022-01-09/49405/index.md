---
layout: "image"
title: "Schrank 4 Schublade 4"
date: 2022-01-15T19:16:41+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Netzteile von ft und anderen, ft-Netzschaltgerät, Spritzguss-Bahnteile von einem Werksbesuch.