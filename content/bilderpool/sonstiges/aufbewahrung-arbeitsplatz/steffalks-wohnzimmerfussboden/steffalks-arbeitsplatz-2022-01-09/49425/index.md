---
layout: "image"
title: "Schrank 2 Schublade 4"
date: 2022-01-15T19:17:05+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

E-Tec-, Electronics- und MiniBot-Module, die selbsthaftende blaue Platte, Farb-/Spur-/Entfernungs-/Universal-Sensoren, Fotowiderstände, Fototransistoren, Störlichtkappen, Elektronik-Einzelteile (Kondensatoren, Widerstände, Dioden, Transistoren), Silberling-Stromversorgungsstecker, Potentiometer, Linsen, Hohl- und Planspiegel, Spiegelbänder, Strich-/Kreuz-/Lochblenden, Bimetallstreifen, 1980er-Jahre-Elektronikteile, Lichtleiter, Silikonschlauch und ein paar selten genutzte Teile links oben.