---
layout: "image"
title: "Schrank 1 Schublade 5 sichtbare Lage"
date: 2022-01-15T19:17:15+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier gibt es Zahnräder, Drehscheiben, Vorstuferäder mit Achsen und Gummireifen, Drehkränze, Feldengräder, Rastdifferenziale zusammengesetzt und auseinandergenommen, Kurbeln, Schwungscheiben, die ersten drei alten Differenziale und die Raritäten: Malteserräder und von Esther (Danke!) zwei ungleichförmige Zahnräder aus dem 3D-Drucker. In der Mitte ganz hinten finden sich aus kaputten Bausteinen herausgenommene Zapfen, die man mitunter gut gebrauchen kann.