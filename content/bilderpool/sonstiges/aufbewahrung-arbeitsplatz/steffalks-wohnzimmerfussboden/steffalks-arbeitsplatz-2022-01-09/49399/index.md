---
layout: "image"
title: "Schrank 1: Standardteile, Achsen, Räder"
date: 2022-01-15T19:16:34+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Direkt links vorne neben dem Stuhl in den sechs Schubladen des Alex (3 einfach und 3 doppelt hohe) finden sich hier Standardteile, Achsen, Naben, Zahnräder, Reifen und leere ft-Kassetten.