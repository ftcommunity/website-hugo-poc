---
layout: "image"
title: "Schrank 1 Schublade 2"
date: 2022-01-15T19:17:20+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weitere rote Kleinteile, Rastachsenzubehör, Gelenke, Kardans, große Gelenk- und Federgelenksteine, Schnecken. Links nebendran passen kürzere Alus.