---
layout: "image"
title: "Schrank 3: Statik, Verkleidungsplatten"
date: 2022-01-15T19:16:31+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts vorne neben dem Stuhl ist alles voll mit Statik und allen Bauplatten kleiner oder gleich 180 x 90.