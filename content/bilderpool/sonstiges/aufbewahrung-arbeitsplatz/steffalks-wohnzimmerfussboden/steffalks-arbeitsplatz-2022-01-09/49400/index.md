---
layout: "image"
title: "Utensilien (2)"
date: 2022-01-15T19:16:35+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Etwas fischer Tip (noch nie benutzt), Gummis, Schreibpapier und eine weiße Tischdecke für Fotos.