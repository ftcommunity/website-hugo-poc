---
layout: "image"
title: "Schrank 1 Schublade 1"
date: 2022-01-15T19:17:21+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Lauter rote Kleinteile, Verbinder, Winkelbausteine. Links nebendran passen in die Schubladen noch die zwei langen Alus.