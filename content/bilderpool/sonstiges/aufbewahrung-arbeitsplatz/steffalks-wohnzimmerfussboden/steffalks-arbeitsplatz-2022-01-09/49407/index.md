---
layout: "image"
title: "Schrank 4 Schublade 3"
date: 2022-01-15T19:16:43+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Schläuche, Kompressoren, Sitze, Männchen und selten benutzte Führerhausteile.