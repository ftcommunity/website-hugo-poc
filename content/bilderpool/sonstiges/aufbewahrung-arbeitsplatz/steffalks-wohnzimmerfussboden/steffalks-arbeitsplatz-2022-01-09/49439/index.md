---
layout: "image"
title: "Überblick"
date: 2022-01-15T19:17:22+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein tiefer Biertisch ist die Haupt-Arbeitsfläche. Das fischertechnik sitzt in vier Ikea "Alex"-Schränken. Wenn man am Tisch sitzt, ist

- Schrank 1 links vorne (Standardteile, Achsen, Räder)

- Schrank 2 links hinten (Ketten, Elektromechanik, Elektronik, Bauplatten)

- Schrank 3 rechts vorne (Statik, Verkleidungsplatten)

- Schrank 4 rechts hinten (Motoren, Pneumatik, Stromversorgung, Fernsteuerungen, Computing)

Die Kabel hängen in einer von einem Kollegen und Freund gebauten perfekten Holzaufnahme.