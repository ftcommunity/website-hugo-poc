---
layout: "image"
title: "Box 500 mit fischertechnik 200"
date: 2022-01-15T19:17:00+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist der Inhalt der oberen Box 500 (siehe Schrank 2 Schublade 6 sichtbare Lage), die sich für Besuchskinder bestens bewährt.