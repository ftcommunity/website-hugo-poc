---
layout: "image"
title: "Schrank 3 Schublade 6 sichtbare Lage"
date: 2022-01-15T19:16:48+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Verkleidungsplatten mit Zapfen und zum Klipsen, Fensterplatten, Bauplatten 180 x 90, 90 x 90, Statikplatten 180 x 90, 90, 90, Bauplatten 90 x 45 und 45 x 45 sowie 30 x 90, schwarze Bauplatten 60 x 120, Muldenteile.