---
layout: "image"
title: "Schrank 1 Schublade 6 sichtbare Lage"
date: 2022-01-15T19:17:13+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alles, was Reifen heißt, von ganz alt bis neu, Statik-Spurkränze mit und ohne Laufgummi, sowie leere 60x60-Kassetten incl. Bootskörper.