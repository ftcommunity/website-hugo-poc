---
layout: "image"
title: "Utensilien (1)"
date: 2022-01-15T19:16:42+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mein altes Windows 10-Phone für gescheite Taschenrechner, meine ft Electronics App zum Nachschlagen von DIP-Switches von ft-Elektronik - und für Musik aus dem Bluetooth-Lautsprecher, der bei der Wand hängt. Der Rechenschieber ist Nostalgie. Schreibzeugs, Telefon, Akku-Ladegerät und vor allem diese Dokumentation (das Bild zeigt noch die vorherige), damit ich nachschlagen kann: a) "Wo habe ich dieses selten gebrauchte Teil?" und b) "Was kommt in diese vielen nebeneinander liegenden leergebauten Fächer?".