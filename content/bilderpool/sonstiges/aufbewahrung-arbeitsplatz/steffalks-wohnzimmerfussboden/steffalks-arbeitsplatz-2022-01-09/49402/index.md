---
layout: "image"
title: "Schrank 5 Schublade 6 sichtbare Lage"
date: 2022-01-15T19:16:37+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz43.jpg"
weight: "43"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

NetDuino 3 (normal und WiFi), Adafruit Motor Shields v2, Convention-Teile, ein von meiner Tochter hergestellter Schraubstock, Werkzeuge und Lötzubehör, Multimeter, Kabel für Microcontroller, Sensoren, Widerstände. Rechts oben drei eingepackte, mit schwarzer Farbe bemalte "Straßen" für das Clubmodell "Autotrainer" (siehe Clubheft 1972-4 und siehe https://ftcommunity.de/bilderpool/modelle/sonstiges/kompakter-autotrainer/gallery-index/). In der Dose von Harald geschenkte 3D-Druck-Teile für Hypozykloidgetriebe.