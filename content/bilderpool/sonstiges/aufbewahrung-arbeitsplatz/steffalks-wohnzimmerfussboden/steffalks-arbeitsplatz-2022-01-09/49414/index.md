---
layout: "image"
title: "Schrank 3 Schublade 4 verdeckte Lage"
date: 2022-01-15T19:16:52+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weitere Statikträger 30 und 60 sowie U-Träger.