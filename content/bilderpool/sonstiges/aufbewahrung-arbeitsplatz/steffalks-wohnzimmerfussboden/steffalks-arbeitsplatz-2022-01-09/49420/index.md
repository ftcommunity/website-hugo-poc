---
layout: "image"
title: "ft:pedia Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten"
date: 2022-01-15T19:16:59+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der ft:pedia Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten mitsamt Stückliste und Modellvorschlägen als Staubschutzhülle (man suche auf https://ftcommunity.de/ftpedia/overview/ im Browser nach "urlaubs").