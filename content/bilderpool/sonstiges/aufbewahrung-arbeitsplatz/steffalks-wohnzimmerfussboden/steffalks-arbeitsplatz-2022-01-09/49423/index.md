---
layout: "image"
title: "Schrank 2 Schublade 6 sichtbare Lage"
date: 2022-01-15T19:17:03+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Bauplatten 1000 ohne Doppelwand, eine weitere Bauplatte 500 (die in Schublade 5 nicht mehr passte), Bootskörper, Box 500 bestückt mit Teilen des Ur-fischertechnik 200 plus mot.1 plus Segmentscheibe (siehe https://ftcommunity.de/bilderpool/sonstiges/eure-fischertechnikkasten/fischertechnik-200-mot-1-einer-box-500/gallery-index/).