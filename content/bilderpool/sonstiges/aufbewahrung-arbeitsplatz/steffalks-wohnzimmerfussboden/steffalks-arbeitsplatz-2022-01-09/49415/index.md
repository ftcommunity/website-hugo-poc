---
layout: "image"
title: "Schrank 3 Schublade 4 sichtbare Lage"
date: 2022-01-15T19:16:53+01:00
picture: "2022-01-09_steffalks_Arbeitsplatz31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Statikträger 15, 30 und 60 sowie U-Träger.