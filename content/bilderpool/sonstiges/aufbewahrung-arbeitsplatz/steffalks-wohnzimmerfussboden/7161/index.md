---
layout: "image"
title: "Einzelteile 2"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7161
- /details0191.html
imported:
- "2019"
_4images_image_id: "7161"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7161 -->
Der gute alte Kraftmesser und die guten alten Gummiraupen, Kurbeln und Seiltrommeln.
