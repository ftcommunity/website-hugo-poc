---
layout: "image"
title: "Einzelteile 8"
date: "2006-10-10T19:04:26"
picture: "steffalkswohnzimmerfussboden14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7169
- /details31a4.html
imported:
- "2019"
_4images_image_id: "7169"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:26"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7169 -->
Statik, Platten, alte Batteriestäbe
