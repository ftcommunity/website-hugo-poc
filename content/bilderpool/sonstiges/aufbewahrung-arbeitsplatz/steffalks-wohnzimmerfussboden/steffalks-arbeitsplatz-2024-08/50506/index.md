---
layout: "image"
title: "Schrank links hinten Schublade 6 sichtbare Lage"
date: 2024-09-06T15:02:58+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_48.jpg"
weight: "48"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zu guter Letzt noch ein paar leere Sortiereinsätze, Trennstege dafür und alle möglichen Federn aus alten Festplattenlaufwerken - noch hab ich mich nicht überwunden, die wegzuwerfen.