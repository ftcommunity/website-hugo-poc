---
layout: "image"
title: "Telefon, Kabel Ladegerät, Schreibzeugs, Doku"
date: 2024-09-06T15:03:36+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Telefon, Ladegerät, Kleinkram und diese Bilder (hier noch auf dem vorherigen Stand von 2022) ausgedruckt zum Nachschlagen, wo ein selten gebrauchtes Teil stecken müsste bzw. was in die vielen leergebauten Fächer in welcher Reihenfolge rein muss.