---
layout: "image"
title: "Schrank links mittig Schublade 3"
date: 2024-09-06T15:03:12+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

1970er Silberlinge und 1980er IC-Elektronik, zwischendrin mit em5-Relais ausgestopft. Links vorne Silikonschlauch (damals für Wasserpumpen), Lichtleitwinkel und links außen lange Lichtleitstäbe.