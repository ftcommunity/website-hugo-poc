---
layout: "image"
title: "Schrank links hinten Schublade 2"
date: 2024-09-06T15:03:03+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_44.jpg"
weight: "44"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Microcontroller (genauer NetDuinos und Adafruit Motor Shields), ein kleines Seedstudio DS2 Nano Oszilloskop, zwei Multimeter, Kabel und rechts oben Kabelspiralen.