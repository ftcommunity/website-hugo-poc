---
layout: "image"
title: "Schrank rechts hinten Schublade 6 geöffnet"
date: 2024-09-06T15:03:15+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der Urlaubskasten und der ft 200 mit mot.1 und Segmentscheibe in der Innenansicht.