---
layout: "image"
title: "Schrank links hinten Schublade 1"
date: 2024-09-06T15:03:04+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_43.jpg"
weight: "43"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz hinten links wirds exotischer: Ungeliebte Teile wie fertige Führerhäuser und solches Zeug, aber auch aus den Ur-ft 200 und 400 die Pappeinlagen für Betonmischtrommel, Radarschirm, Lostrommel und die ft-400-Rakete (von denen habe ich leider nur noch zwei anstatt der für die Rakete benötigten drei). Rechts oben alle losen Aufkleber, eine ft-originale adhäsiv klebende Scheibe sowie Spritzgussteile von kleinen Bahnen von einem Fanclubtag. In der Mitte sieht man mit Wasserfarbe bemalte Papierrollen für Rechenmaschinen für das Modell "Autotrainer" aus den 1970er Clubheften.