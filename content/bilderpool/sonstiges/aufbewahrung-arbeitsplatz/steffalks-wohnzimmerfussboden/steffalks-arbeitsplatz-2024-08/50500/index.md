---
layout: "image"
title: "Schrank links hinten - Fertigteile, Microcontroller, Computing, Netzteile, Leerkästen"
date: 2024-09-06T15:02:51+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Links ganz hinten kommen selten gebrauchte Teile wie fertige Führerhäuser, Microcontroller, ft-Computing, Netzteile, Werkstattausrüstung und leere 1000er-Einsätze als Reserve.