---
layout: "image"
title: "Schrank rechts vorne Schublade 3"
date: 2024-09-06T15:03:33+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alle X-Streben, original-ft-Kugeln (bis auf die schwarze, die stammt von einer Maus), Flexschienen, Baggerschaufeln, Kraftmesser. Außerhalb ältere (glänzende) Statikschienen.