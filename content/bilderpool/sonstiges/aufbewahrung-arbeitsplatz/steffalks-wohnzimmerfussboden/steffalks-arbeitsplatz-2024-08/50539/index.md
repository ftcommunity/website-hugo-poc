---
layout: "image"
title: "Schrank links vorne Schublade 6 verdeckte Lage"
date: 2024-09-06T15:03:38+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch hier gibt es eine untere Lage mit weiteren Reifen, Vorstuferädern, Gummiringen dazu, Ur-Raupen und Meccanum-Rädern.