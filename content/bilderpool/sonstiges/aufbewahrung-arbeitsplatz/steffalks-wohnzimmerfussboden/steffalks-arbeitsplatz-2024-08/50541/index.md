---
layout: "image"
title: "Schrank links vorne Schublade 5 verdeckte Lage"
date: 2024-09-06T15:03:40+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der unteren Lage finden sich die älteren glänzenden Nabenmuttern, weitere Zahnräder, Freilaufnaben, Kurven- und Segmentscheiben. Die weißen längeren Zahnräder links mittig sind ovale 3D-Druck-Zahnräder für ungleichförmige Übersetzungen von Esther Mietzsch.