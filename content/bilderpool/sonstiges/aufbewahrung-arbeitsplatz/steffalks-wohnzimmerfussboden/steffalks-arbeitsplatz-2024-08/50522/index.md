---
layout: "image"
title: "Schrank rechts hinten Schublade 5"
date: 2024-09-06T15:03:18+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Fast ganz unten finden sich Akkus, Batteriehalter, Fernbedienungen (mit der Anleitung gleich daneben) und ft-Kassetten.