---
layout: "image"
title: "Schrank rechts hinten - Platten, Pneumatik, Motoren, Akkus, 500er-Kästen"
date: 2024-09-06T15:02:52+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der hintere Schrank auf der rechten Seite enthält Verkleidungsplatten, Pneumatik, Motoren, Akkus, Fernbedienungen, Leerkassetten sowie den ft:pedia Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten und einen Ur-ft 200 in einer Box 500.