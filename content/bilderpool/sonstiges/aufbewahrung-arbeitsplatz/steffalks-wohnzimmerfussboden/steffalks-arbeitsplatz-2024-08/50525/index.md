---
layout: "image"
title: "Schrank rechts hinten Schublade 3"
date: 2024-09-06T15:03:21+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Pneumatik-Tanks, Schläuche, Kompressoren, Manometer.