---
layout: "image"
title: "Schrank links vorne Schublade 2"
date: 2024-09-06T15:03:46+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

eins drunter Winkelsteine und Gelenke. Die Winkelsteine 60°, aber vor allem die tatsächlich unterschiedlichen Ur- und aktuellen Fassungen der Winkelsteine 30° sind getrennt. Und weitere Aluprofile außerhalb.