---
layout: "overview"
title: "steffalks Arbeitsplatz 2024-08"
date: 2024-09-06T15:02:51+02:00
---

Nach diversen mehr oder weniger ungeplantem Neuzugang wurde mal wieder eine Neusortierung notwendig, weil alle möglichen Fächer zu klein wurden. Teil des Neuzugangs war ein fünfter Ikea "Alex", den ich nie haben wollte, weil das die Sache unübersichtlicher macht, und nun halt doch habe. Dafür ist jetzt hier und da noch Raum für Erweiterungen.