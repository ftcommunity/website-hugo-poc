---
layout: "image"
title: "Schrank links mittig Schublade 6"
date: 2024-09-06T15:03:06+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weitere Bauplatten 500  sowie Zapfenkiller-1000er, Bootskörper.