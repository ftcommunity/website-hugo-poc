---
layout: "image"
title: "Schrank links vorne Schublade 4 sichtbare Lage"
date: 2024-09-06T15:03:44+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die erste doppelt hohe Schublade enthält Grundbausteine. Die Schwarzen links oben sind solche mit Bohrung. Dies ist die obere Lage der komplett zweilagig gefüllten Schublade.