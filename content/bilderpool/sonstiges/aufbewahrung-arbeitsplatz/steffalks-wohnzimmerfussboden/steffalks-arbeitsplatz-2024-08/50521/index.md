---
layout: "image"
title: "Schrank rechts hinten Schublade 6"
date: 2024-09-06T15:03:16+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz unten: Hinten eine leere Box 500, vorne links der ft:pedia Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten und rechts das Material eines Ur-ft 200 Kastens plus mot.1 plus Segmentscheibe, mit dem man also alle Modelle des ft 50, 100, 200 aus den 1960er bis Anfang 1970er Jahre bauen kann - das hat sich für Besuchskinder ohne ft-Erfahrung glänzend bewährt, weil die Teilevielfalt übersichtlich ist - hört ihr mich, ft?