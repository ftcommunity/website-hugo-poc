---
layout: "image"
title: "Schrank links hinten Schublade 3"
date: 2024-09-06T15:03:02+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_45.jpg"
weight: "45"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

ft-Computing: Links Robo Interface mit Funkmodul, darüber 3 I/O-Extensions, mittig unten ein TXT, Knoblochs Robo Connect Box, rechts daneben ein IBM-PC-Parallel-Interface. Ganz oben außer den USB-Kabeln ft-Schrittmotoren. Das gelbe Kästchen in der Mitte ist ein USB-nach-Seriell-Wandler von Thomas Kaiser.