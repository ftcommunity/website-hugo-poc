---
layout: "image"
title: "Schrank links mittig Schublade 4 sichtbare Lage"
date: 2024-09-06T15:03:10+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alle möglichen Sensoren, Bimetallstreifen, Spiegel, Linsen, Blenden, Potentiometer, Reedkontakte, E-Tec-, MiniBots- und Electronics-Module. Ganz hinten 1969er Licht-Elektronik-Stäbe, plus ein Nachbau eines lieben Schulfreundes neben den Ur-Batteriestäben. Rechts davon die l-e1-Flächen für optische Bänke sowie ft-Stecker.