---
layout: "image"
title: "Schrank rechts hinten Schublade 1"
date: 2024-09-06T15:03:25+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts hinter dem Stuhl geht es weiter mit Verkleidungsplatten etc., incl. links oben farbige und beklebte.