---
layout: "image"
title: "Schrank links hinten Schublade 5"
date: 2024-09-06T15:03:00+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_47.jpg"
weight: "47"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Werkstatt: Lupe, dritte Hand, Lötzubehör, Scheren, Messer, ein von meiner Tochter hergestellter Schraubstock, im welligen Kästchen Teile für DIY-Schleifringe aus dem 3D-Drucker von Harald Steinhaus. Der Kasten links oben enthält jede Menge Sensoren für Microcontroller, fast alle 1Wire (sprich: die funktionieren mit nur einer einzigen Zuleitung).