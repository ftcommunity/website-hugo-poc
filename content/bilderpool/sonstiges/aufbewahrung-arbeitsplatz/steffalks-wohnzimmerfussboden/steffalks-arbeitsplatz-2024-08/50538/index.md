---
layout: "image"
title: "Schrank rechts vorne Schublade 1"
date: 2024-09-06T15:03:37+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wir gehen weiter zum rechten vorderen Schrank: Statik-Kleinteile und alles, was mit Seilrollen zu tun hat.