---
layout: "image"
title: "Großer Kabelhalter"
date: 2024-09-06T15:03:09+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Stege sind wunderbar parallel und stabil, ganz prima zum schnellen Einhängen von ft-Kabeln.