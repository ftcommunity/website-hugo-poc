---
layout: "image"
title: "Schrank rechts hinten Schublade 4 verdeckte Lage"
date: 2024-09-06T15:03:19+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der zweiten Lage gibt es noch ein paar Motoren und Zubehör.