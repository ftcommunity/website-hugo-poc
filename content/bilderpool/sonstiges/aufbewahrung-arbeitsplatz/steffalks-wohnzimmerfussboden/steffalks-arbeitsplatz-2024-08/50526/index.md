---
layout: "image"
title: "Kleiner Kabelhalter"
date: 2024-09-06T15:03:22+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kabelhalter wurden von einem Kollegen von mir gebaut. Der ist in Sachen Holz der absolute Perfektionist, und das merkt man den Haltern auch an.