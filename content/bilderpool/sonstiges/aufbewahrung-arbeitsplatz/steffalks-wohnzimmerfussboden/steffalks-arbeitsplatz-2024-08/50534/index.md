---
layout: "image"
title: "Schrank rechts vorne Schublade 4 sichtbare Lage"
date: 2024-09-06T15:03:32+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Doppelt hohe Schublade mit Statikträgern 15, 30, 60, Riegelsteinen, Statiksteinen und U-Trägern. Außerhalb eine fischerform-Leiter.