---
layout: "image"
title: "Schrank links mittig Schublade 5"
date: 2024-09-06T15:03:07+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

1000er- und 500er-Bauplatten. Die 1000er in grau sind die alten, unten geschlossenen. Die schwarzen darunter sind die nicht-Zapfen-Killer-Versionen.