---
layout: "image"
title: "Schrank rechts vorne Schublade 4 verdeckte Lage"
date: 2024-09-06T15:03:31+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der verdeckten Lage weitere kleine Statikträger.