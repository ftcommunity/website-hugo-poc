---
layout: "image"
title: "Schrank links hinten Schublade 4"
date: 2024-09-06T15:03:01+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_46.jpg"
weight: "46"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

ft- und nicht-ft-Netzteile sowie ein 1969er ft-Netzschaltgerät.