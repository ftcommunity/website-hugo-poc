---
layout: "image"
title: "Schrank links vorne - Standardteile"
date: 2024-09-06T15:02:56+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Schrank direkt links vorne neben dem Stuhl enthält Standardteile, Grundbausteine, Achsen, Zahnräder, Reifen und dergleichen.