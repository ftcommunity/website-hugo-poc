---
layout: "image"
title: "Schrank rechts hinten Schublade 2"
date: 2024-09-06T15:03:24+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Darunter Platten 30x90, Flachträger, Fensterscheiben und jede Menge Pneumatik incl. Festo-Ventilen und Betätigern.