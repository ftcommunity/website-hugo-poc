---
layout: "image"
title: "Schrank rechts vorne - Statik"
date: 2024-09-06T15:02:54+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts vorne geht's weiter mit Statik und Kugelbahn-Teilen.