---
layout: "image"
title: "Schrank links mittig - Ketten, Elektromechanik, Elektronik, Bauplatten"
date: 2024-09-06T15:02:55+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eins weiter hinten auf der linken Seite folgen Ketten/Raupen etc., Elektromechanik, Elektronik und Bauplatten.