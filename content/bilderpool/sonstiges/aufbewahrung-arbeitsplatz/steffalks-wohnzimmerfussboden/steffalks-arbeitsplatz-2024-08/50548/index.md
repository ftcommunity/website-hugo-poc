---
layout: "image"
title: "steffalks Arbeitsplatz 2024-08"
date: 2024-09-06T15:03:49+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Arbeitsfläche ist nach wie vor ein breiter Biertisch. Links vom Stuhl gibt es drei Schränke (von rechts nach links im Bild "vorne", "mittig", "hinten" in den folgenden Beschreibungen) und rechts zwei ("vorne" beim Fenster und "hinten").