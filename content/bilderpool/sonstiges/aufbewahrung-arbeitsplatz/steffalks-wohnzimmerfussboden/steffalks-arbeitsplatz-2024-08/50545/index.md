---
layout: "image"
title: "Schrank links vorne Schublade 3"
date: 2024-09-06T15:03:45+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eins tiefer liegen Achsen, Klemmringe, Rastgetriebeteile, Rollen, Kardangelenke und drumrum lange Achsen (vorne 50-cm-Achsen von Conrad)