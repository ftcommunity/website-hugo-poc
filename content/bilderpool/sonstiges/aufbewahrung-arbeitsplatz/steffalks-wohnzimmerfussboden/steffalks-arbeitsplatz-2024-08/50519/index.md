---
layout: "image"
title: "Schrank links mittig Schublade 1"
date: 2024-09-06T15:03:14+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der linke mittlere Schrank beginnt noch mit ein paar Bausteinen, für die im linken vorderen Schrank kein Platz mehr war. Vor allem enthält der aber Ketten, Raupenbeläge, Federn, Ur-Antriebsfedern und ein paar Gummis sowie Lenkungsteile, die ich eh kaum brauche.