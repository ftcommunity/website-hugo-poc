---
layout: "image"
title: "Schrank rechts vorne Schublade 5 sichtbare Lage"
date: 2024-09-06T15:03:30+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Darunter folgen graue und schwarze Statikträger 120, Flachträger, Bogenstücke.