---
layout: "image"
title: "Schrank rechts hinten Schublade 4 sichtbare Lage"
date: 2024-09-06T15:03:20+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der doppelt hohen Schublade alles, was mit Motoren zu tun hat, Zahnstangen, Differenziale, Solarzellen und -motoren - und drei netterweise von Roland Enzenhofer geschenkte kompletten Malteserräder.