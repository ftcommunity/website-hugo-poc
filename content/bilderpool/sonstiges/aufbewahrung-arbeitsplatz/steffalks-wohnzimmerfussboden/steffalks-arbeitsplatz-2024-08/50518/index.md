---
layout: "image"
title: "Schrank links mittig Schublade 2"
date: 2024-09-06T15:03:13+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Elektromechanik: Taster, Schalter, Lampen, Leuchtkappen, Dauermagnete, Elektromagnete, Schwingfedern, Schleifringe, Summer und längere Stromschienen (die ineinander steckbar sind).