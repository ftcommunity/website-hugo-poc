---
layout: "image"
title: "Schrank links mittig Schublade 4 verdeckte Lage"
date: 2024-09-06T15:03:08+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der unteren Lage Kabel und Elektronik für die 1980er IC-Elektronikbausteine sowie ganz rechts aus dem Kasten "Elektronik-Praktikum" von 1977.