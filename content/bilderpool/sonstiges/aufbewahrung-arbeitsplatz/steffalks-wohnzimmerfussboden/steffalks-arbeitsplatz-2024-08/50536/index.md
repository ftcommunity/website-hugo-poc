---
layout: "image"
title: "Schrank rechts vorne Schublade 2"
date: 2024-09-06T15:03:34+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alles I-Streben und (neuere, matte) Statikschienen.