---
layout: "image"
title: "Schrank links vorne Schublade 5 sichtbare Lage"
date: 2024-09-06T15:03:42+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die zweitunterste Schublade ist ebenfalls zweilagig bestückt und enthält Naben, Zahnräder, Drehscheiben, Drehkränze, Schnecken. Die hier sichtbaren Nabenmuttern sind neuere, matte. In der Mitte sieht man graue und schwarze Zahnräder Z25 - ein Geschenk von Roland Enzenhofer.