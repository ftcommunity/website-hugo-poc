---
layout: "image"
title: "Schrank rechts vorne Schublade 6 verdeckte Lage"
date: 2024-09-06T15:03:26+02:00
picture: "steffalks-arbeitsplatz-2024-08-27_27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Noch ein paar Träger und Platten 60x120 in der unteren Lage.