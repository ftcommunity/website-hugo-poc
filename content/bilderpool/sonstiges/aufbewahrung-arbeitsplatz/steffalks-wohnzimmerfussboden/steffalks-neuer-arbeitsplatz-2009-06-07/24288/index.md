---
layout: "image"
title: "Hohe Schubladen"
date: "2009-06-07T17:22:06"
picture: "steffalk3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24288
- /details627f.html
imported:
- "2019"
_4images_image_id: "24288"
_4images_cat_id: "1662"
_4images_user_id: "104"
_4images_image_date: "2009-06-07T17:22:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24288 -->
In die hohen Schubladen passen zwei Lagen von ft 1000-Sortiereinsätzen (oder hobby-Kästen). Diese Schublade enthält hinten in der unteren Lage drei längs eingesetzte hobby-Kästen, eine Lage darüber wieder normale Sortiereinsätze. Bei dieser Schublade ist die vordere Reihe nur einlagig bestückt.
