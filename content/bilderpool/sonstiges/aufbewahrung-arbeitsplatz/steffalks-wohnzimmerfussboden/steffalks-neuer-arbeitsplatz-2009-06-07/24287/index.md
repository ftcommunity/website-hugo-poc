---
layout: "image"
title: "Flache Schubladen"
date: "2009-06-07T17:22:06"
picture: "steffalk2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24287
- /details1acd.html
imported:
- "2019"
_4images_image_id: "24287"
_4images_cat_id: "1662"
_4images_user_id: "104"
_4images_image_date: "2009-06-07T17:22:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24287 -->
In die flachen Schubladen passen 3 * 3 ft 1000-Sortiereinsätze. Etwas Luft für lange Achsen, Alus u.dgl. bleibt noch, und auch in der Höhe ist noch etwas Luft für höhere Teile wie Pneumatiktanks usw.
