---
layout: "comment"
hidden: true
title: "9382"
date: "2009-06-08T22:05:46"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Bei IKEA steht alles zu dem hübschen Teil.
Hier mal die Maße:
Produktmaße:
Breite: 67 cm
Tiefe: 48 cm
Höhe: 66 cm
Kostenpunkt:99€/Stück

Würde mir auch gefallen aber der Unterschrank den ich aus der Firma, wo meine Frau arbeitet, bekommen habe reicht mir mit meinen vorhandenen Vorratsbehältern vollkommen aus, erstmal.