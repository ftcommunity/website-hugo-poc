---
layout: "image"
title: "Schrank 2 Schublade 2"
date: "2018-05-07T22:44:05"
picture: "einsortierung17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47613
- /detailsb5cf.html
imported:
- "2019"
_4images_image_id: "47613"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47613 -->
Elektromechanik: Leuchtkappen, Kontaktstifte, Drehschalter, Taster, Polwendeschalter, Linsen- und normale Lampen, LEDs, Kontaktplatten, Klemmkontakte, Federn, Stahlkugeln, Neodym-Magnete, Magnetbausteine, Dauermagnete, eckige und runde Rückschlussplatten, Elektromagnete, Schwingfedern, Ersatzbirnchen, Kontakt-Abdeckplättchen, Leuchtensockel, Schraubendreher, Summer, verschiedene Stahlfedern, Federfüße, einfache und doppelte Unterbrecherstücke für die Schleifringe, Schaltscheiben.
