---
layout: "overview"
title: "Einsortierung 2018-05-07"
date: 2020-02-22T09:20:35+01:00
legacy_id:
- /php/categories/3511
- /categoriesca07.html
- /categories9876.html
- /categoriesb30a.html
- /categoriescb8a.html
- /categories4e3c.html
- /categoriesc095.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3511 --> 
Der letzte bei mir dokumentierte Stand (wurde nie auf die ftc hochgeladen) stammte von 2015. Seitdem kam mehr dazu, was eine fast komplett neue Sortierung erforderlich machte.