---
layout: "image"
title: "Schrank 4 Schublade 3"
date: "2018-05-07T22:44:05"
picture: "einsortierung34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47630
- /detailsfc6a.html
imported:
- "2019"
_4images_image_id: "47630"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47630 -->
Varianten von Pneumatikschläuchen, Sitze, Männchen, selten gebrauchte fertige Führerhäuser.
