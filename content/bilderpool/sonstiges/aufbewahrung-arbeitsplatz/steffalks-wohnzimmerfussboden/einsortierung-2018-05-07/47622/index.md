---
layout: "image"
title: "Schrank 2 Schublade 4 sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47622
- /details9672.html
imported:
- "2019"
_4images_image_id: "47622"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47622 -->
Statikträger 15, 30 und 60, U-Träger 150.
