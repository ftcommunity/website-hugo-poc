---
layout: "image"
title: "Schrank 4 Schublade 6 (unten) sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47634
- /details94bb.html
imported:
- "2019"
_4images_image_id: "47634"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47634 -->
Microcontroller und Werkzeug: Je ein Netduino 3 und Netduino 3 WiFi, vier Adafruit Motor Shields V2.3, Kabel dafür, ein Kasten mit Sensoren (darunter viele OneWire-Sensoren), Multimeter, ein kleines Oszilloskop (in der Tasche), Breadboard, Dritte Hand, Küchenmesser, Pinzette, Entlötpumpe, Lötkolben, Metermaß. Oben rechts die Rollen mit aufgemalter Bahn für das Auto-Trainer-Modell und ein 3D-Druck-Getriebeteil (im Kästchen) von Harald (Danke!).
