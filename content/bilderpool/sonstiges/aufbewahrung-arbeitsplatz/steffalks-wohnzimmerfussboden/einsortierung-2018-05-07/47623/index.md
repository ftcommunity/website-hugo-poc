---
layout: "image"
title: "Schrank 3 Schublade 4 verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47623
- /detailsf45a.html
imported:
- "2019"
_4images_image_id: "47623"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47623 -->
Weitere Statikträger 60 und U-Träger 150.
