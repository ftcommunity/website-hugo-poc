---
layout: "image"
title: "Schrank 3 Schublade 6 (unten) sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47626
- /details3619.html
imported:
- "2019"
_4images_image_id: "47626"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47626 -->
Verkleidungsplatten mit Zapfen und als Clipsplatten, durchsichtige Flachbausteine, Flachsteine 30 und 60, Grundbauplatten 180x90 und 90x90, daneben 90x45 und 45x45, Statikplatten 180x90 und 90x90, Bodenplatten in verschiedenen Farben (zwei blaue durch Absprung beschädigt), Bauplatten 120x60, Muldenteile. Um Platz zu sparen, stehen viele Platten senkrecht in den Sortiereinsätzen.
