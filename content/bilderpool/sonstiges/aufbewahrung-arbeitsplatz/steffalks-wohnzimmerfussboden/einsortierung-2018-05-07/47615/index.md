---
layout: "image"
title: "Schrank 2 Schublade 4"
date: "2018-05-07T22:44:05"
picture: "einsortierung19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47615
- /detailsb0e4-2.html
imported:
- "2019"
_4images_image_id: "47615"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47615 -->
Electronics- und MiniBots-Module, E-Tecs, Spur-, Farb- und Abstandssensoren, Störlichttubusse für die Fotozellen, kleine Fotowiderstände, Fototransistoren, Störlichtkappen, Transistoren, Dioden, Kondensatoren, Temperaturfühler, Reed-Kontakte, Feuchtigkeitssensor, Lämpchen und Zwischenstecker für Silberlinge, Potentiometer, Linsen, Hohl- und Flachspiegel, Spiegelbänder, Blenden, Bimetallstreifen, Elektronikteile und Kabel für die 80er-Jahre-Elektronikbausteine, Lichtleitwinkel, Silikonschlauch, runde Zylinderbausteine, Holzteile für Farbsortierer. Links daneben lange Lichtleitstäbe.
