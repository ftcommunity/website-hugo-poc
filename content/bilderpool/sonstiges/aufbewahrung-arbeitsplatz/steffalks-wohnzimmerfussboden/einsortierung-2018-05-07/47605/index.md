---
layout: "image"
title: "Schrank 1 Schublade 3"
date: "2018-05-07T22:44:05"
picture: "einsortierung09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47605
- /detailsa472.html
imported:
- "2019"
_4images_image_id: "47605"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47605 -->
Naben, Freilaufnaben, Metall- und Kunststoffachsen, Seilrollen, Nocken- und Kurvenscheiben sowie (in dem weißen Döschen) Vaseline zum Schmieren von Achsen (von meiner Blockflöte aus Kindheitstagen). Davor und links daneben lange Achsen und Elektromechanik-Steckachsen (die vorderen sind 50-cm-Achsen von Conrad).
