---
layout: "image"
title: "Schrank 2 - Ketten, Elektromechanik, Elektronik, Bauplatten"
date: "2018-05-07T22:44:05"
picture: "einsortierung03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47599
- /details127e.html
imported:
- "2019"
_4images_image_id: "47599"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47599 -->
Links hinten (vom Sessel aus gesehen) befinden sich Ketten, Raupen, Litze, Elektromechanik, Elektronik, Sensoren, Bauplatten 1000 und 500, der Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten, eine weitere Box 500 sowie Bootskörper
