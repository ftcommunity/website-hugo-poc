---
layout: "image"
title: "Schrank 3 Schublade 3"
date: "2018-05-07T22:44:05"
picture: "einsortierung25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47621
- /details6626.html
imported:
- "2019"
_4images_image_id: "47621"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47621 -->
Alle X-Streben von 42,4 - 169,6, Kassetten mit Metallgewichten, Flexschienen, selten gebrauchte Lenkungsteile, Aufkleber und Papp-Platten aus den Ur-ft-200/400-Kästen. Links daneben ältere (glänzende) Statikschienen aus meinen ur-ft-200S/400S.
