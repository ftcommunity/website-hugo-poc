---
layout: "image"
title: "Schrank 1 - Standardteile"
date: "2018-05-07T22:44:05"
picture: "einsortierung02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47598
- /detailsd8e7.html
imported:
- "2019"
_4images_image_id: "47598"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47598 -->
Links vorne (vom Sessel aus gesehen) befindet sich der Schrank mit Bausteinen, Achsen, Rädern.
