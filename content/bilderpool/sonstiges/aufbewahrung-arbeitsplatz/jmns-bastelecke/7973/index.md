---
layout: "image"
title: "Ubersicht"
date: "2006-12-20T00:23:02"
picture: "jmnsbastelecke1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/7973
- /details561b.html
imported:
- "2019"
_4images_image_id: "7973"
_4images_cat_id: "744"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:23:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7973 -->
Hier seht mann meine Bastel Ecke. An eine Seite dieses Zimmer mein fischertechnik. An der andere Seite den Computer.
