---
layout: "image"
title: "Dokumente"
date: "2006-12-20T00:23:03"
picture: "jmnsbastelecke8.jpg"
weight: "8"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/7980
- /details8663.html
imported:
- "2019"
_4images_image_id: "7980"
_4images_cat_id: "744"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:23:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7980 -->
Viele Bauanleitungen und dokumente von fischertechnik (5 Schubladen voll)
