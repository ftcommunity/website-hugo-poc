---
layout: "image"
title: "bastel_2074.JPG"
date: "2010-01-28T21:31:58"
picture: "bastel_2074.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26180
- /detailsf971.html
imported:
- "2019"
_4images_image_id: "26180"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2010-01-28T21:31:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26180 -->
Die Arbeitsplatte hat jetzt einen umlaufenden Teile-Runterfall-Schutz. Das besondere Feature ist die Fahrrad-Aufhängung unter der Decke. Da passt natürlich der Flieger genauso gut hin. Dem hat's beim Umzug die Heckklappe etwas demoliert.
