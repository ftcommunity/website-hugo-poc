---
layout: "comment"
hidden: true
title: "5527"
date: "2008-03-11T19:30:24"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,

Die Fa. Rotwerk hatte ich damals auch in der engeren Auswahl. Den Fräsvorschub hatten sie aber seinerzeit noch nicht, sonst wär's vielleicht eine Rotwerk geworden. Der deutschtümelnde Name ist genauso Blendwerk wie "Optimum", d.h. China-Importware ist beides.

Gruß,
Harald