---
layout: "image"
title: "Bastelzimmer 2009-06.JPG"
date: "2009-06-27T20:21:22"
picture: "Bastelzimmer_verpackt_11.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/24466
- /detailsb2f1.html
imported:
- "2019"
_4images_image_id: "24466"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2009-06-27T20:21:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24466 -->
Juni 2009. Die Magazine sind verpackt, die Schränke leer. Die Arbeitsplatte hinten war früher mal Küche, daher der Ausschnitt fürs Spülbecken. Das fehlende Teil (sowas wirft man ja nicht weg) war nur lose eingelegt. 
Den Tisch brauche ich noch für Reparaturen am Fahrrad, sonst wär er auch schon zerlegt.
