---
layout: "image"
title: "Diverses - 2. Lage"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich35.jpg"
weight: "35"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33789
- /detailsb705.html
imported:
- "2019"
_4images_image_id: "33789"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33789 -->
