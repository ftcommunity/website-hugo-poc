---
layout: "image"
title: "Das hintere Lager"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich02.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33756
- /detailsdc4e.html
imported:
- "2019"
_4images_image_id: "33756"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33756 -->
Hier befinden sich noch verpackte fischertechnik Kästen, die ich mehrfach habe.
Und auch die eine und andere Rarität.
