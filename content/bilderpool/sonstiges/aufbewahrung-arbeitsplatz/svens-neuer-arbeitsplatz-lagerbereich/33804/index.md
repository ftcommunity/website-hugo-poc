---
layout: "image"
title: "Kleinteile 4"
date: "2011-12-25T14:16:52"
picture: "ftbaubereich50.jpg"
weight: "50"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33804
- /details27cd.html
imported:
- "2019"
_4images_image_id: "33804"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:52"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33804 -->
