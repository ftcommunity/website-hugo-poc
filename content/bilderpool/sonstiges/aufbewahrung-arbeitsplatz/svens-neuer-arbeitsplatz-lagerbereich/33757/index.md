---
layout: "image"
title: "Das hintere Lager"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich03.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33757
- /detailsa4ec.html
imported:
- "2019"
_4images_image_id: "33757"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33757 -->
Hier befinden sich noch verpackte fischertechnik Kästen, die ich mehrfach habe.
Und auch die eine und andere Rarität.
