---
layout: "image"
title: "Vorrat 1"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich27.jpg"
weight: "27"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33781
- /detailsad72.html
imported:
- "2019"
_4images_image_id: "33781"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33781 -->
Ein Vorrat an verschweißten Tütchen für alle Fälle.
