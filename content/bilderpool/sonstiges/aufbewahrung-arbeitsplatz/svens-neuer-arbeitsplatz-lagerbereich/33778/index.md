---
layout: "image"
title: "Diverses"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich24.jpg"
weight: "24"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33778
- /details5533.html
imported:
- "2019"
_4images_image_id: "33778"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33778 -->
Cassetten mit und ohne Deckel in 3 Farben.
Links Powerblockgehäuse ohne Inhalt zu bastelzwecken.
