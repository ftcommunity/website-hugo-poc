---
layout: "image"
title: "Verschiedene Bausteine"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich15.jpg"
weight: "15"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33769
- /details4a58-2.html
imported:
- "2019"
_4images_image_id: "33769"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33769 -->
z.B. Statik
