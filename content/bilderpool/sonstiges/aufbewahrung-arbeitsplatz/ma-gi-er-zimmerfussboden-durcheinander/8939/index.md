---
layout: "image"
title: "RF Datalink"
date: "2007-02-11T15:17:16"
picture: "magier1.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8939
- /detailsad38.html
imported:
- "2019"
_4images_image_id: "8939"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:17:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8939 -->
Auf meinem Gestell hoch gelagert.