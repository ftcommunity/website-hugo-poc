---
layout: "image"
title: "Anleitungen"
date: "2007-02-11T15:18:39"
picture: "magier4.jpg"
weight: "9"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8942
- /detailsee0c.html
imported:
- "2019"
_4images_image_id: "8942"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8942 -->
und andere Ft-Dokumente.