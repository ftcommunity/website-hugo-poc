---
layout: "comment"
hidden: true
title: "3594"
date: "2007-07-05T14:37:52"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Kenn ich auch von mir.
Jedenfalls solange ich noch auf dem Boden gebaut habe. Jetzt baue ich immer auf einem Tisch, da wird man "gezwungen" aufzuräumen, wenn nicht alle Teile runterfallen sollen.