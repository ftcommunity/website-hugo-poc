---
layout: "image"
title: "Ecke"
date: "2006-12-31T18:19:29"
picture: "magierzimmerfussbodendurcheinander3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8238
- /details00dc.html
imported:
- "2019"
_4images_image_id: "8238"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2006-12-31T18:19:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8238 -->
In der mitte die Krimskramsschachtel.