---
layout: "image"
title: "Weiteres"
date: "2007-02-11T15:17:16"
picture: "magier3.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8941
- /details6a2b.html
imported:
- "2019"
_4images_image_id: "8941"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:17:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8941 -->
Links die Ft's meiner Schwester. Rechts leere Schachteln.