---
layout: "overview"
title: "Werkstatt von Detlef Ottmann"
date: 2023-04-28T17:29:36+02:00
---

Der Raum (34 Quadratmeter) sollte eigentlich mal Büro und Bastelstube werden, ist aber inzwischen fast nur noch fischertechnik-Raum.
Die Büromöbel und die für fischertechnik habe ich alles selbst gebaut. Ich habe auch eine Holzwerkstatt mit Formatkreissäge und allem.
Die Schubladen-Schränke habe ich von Bisley gekauft und dort alles in Reichweite vom Konstruktionstisch untergebracht. 
In die Schubladen passen die V-Kästen rein und sind gut für die Aufbewahrung von Kleinteilen und Platten. 
