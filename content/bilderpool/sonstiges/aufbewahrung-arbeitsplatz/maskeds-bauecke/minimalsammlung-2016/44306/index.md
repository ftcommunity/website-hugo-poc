---
layout: "image"
title: "Erste Lage Box 1"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44306
- /details89f0.html
imported:
- "2019"
_4images_image_id: "44306"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44306 -->
Details auf den nächsten Bildern, jetzt bezeichnet als Box 1.1