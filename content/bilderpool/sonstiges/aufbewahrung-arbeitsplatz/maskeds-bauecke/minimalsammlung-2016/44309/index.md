---
layout: "image"
title: "Box 1.1 oben rechts"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44309
- /details7b80.html
imported:
- "2019"
_4images_image_id: "44309"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44309 -->
32 Baustein 15 rot mit Loch, 16 Baustein 5 mit 2 Zapfen

32 Baustein 5 mit 1 Zapfen

12 Bauplatten 5 (15x30) mit 3 Nuten (38428)
12 Bauplatten 5 (15x20) mit Zapfen und Nut (35049)
8 Baustein 30 mit Bohrung
32 Baustein 30
