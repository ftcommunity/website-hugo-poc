---
layout: "image"
title: "Box 1.1 oben links"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44308
- /detailsb5de.html
imported:
- "2019"
_4images_image_id: "44308"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44308 -->
Rast-Kardangelenke, Klemmstift 4,1mm, Rastachse mit Platte
Unter den Ketten und in den folgenden Fächern:
Rast- und Metallachsen in allen Längen
Hülse 15
Klemmverbinder für Metallachsen
Im rechten Fach: Rast-Z20, Rast-Z10, 1 Rastschnecke, 4 Z10, 2 Z20, 2 Z30, 4 Schraubnaben, 2 Freilaufnaben
