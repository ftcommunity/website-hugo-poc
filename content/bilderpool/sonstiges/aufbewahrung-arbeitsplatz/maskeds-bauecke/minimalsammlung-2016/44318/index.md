---
layout: "image"
title: "Box 2 unterer Sortereinsatz"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44318
- /details3484.html
imported:
- "2019"
_4images_image_id: "44318"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44318 -->
die "diversen Reste", die ich vergessen hatte:

diverse Kleinteile (u.A. Puffer, Abstandsring)

Lenkrad, Roboter-Greifzangen, diverse Teile für Lenkungen

inzwischen geändert: S-Riegel 4 statt U-Träger-Adapter

Clipsachse 34, Rastachse 20 rot

4 Federn schwarz, 4 kleine Federn d=4mm, Stebenadapter

Werkstücke, Bauplatte 30x30, 1 Seiltrommel mit Rast-Anschluss

TST-Spezialteile
