---
layout: "image"
title: "Box 1.2 unten rechts"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44315
- /detailse085.html
imported:
- "2019"
_4images_image_id: "44315"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44315 -->
Kabel, Seile, Anschlussbox

Schraubendreher, diverse Sensoren (Reedkontakt, Farbsensor, Spursensor), Elektromagnet neu

Kabelhalter

Lampen, Linsenlampen, LEDs, Leuchtkappen, Fotowiderstände und Fototransistoren
