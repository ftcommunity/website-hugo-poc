---
layout: "image"
title: "unter den Flexschienen..."
date: "2016-08-20T23:05:38"
picture: "minimalsammlung16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44319
- /detailsb912-2.html
imported:
- "2019"
_4images_image_id: "44319"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44319 -->
Liegen insgesamt 24 Winkelträger 120
