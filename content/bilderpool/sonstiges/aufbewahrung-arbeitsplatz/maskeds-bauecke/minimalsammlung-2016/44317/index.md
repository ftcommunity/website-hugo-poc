---
layout: "image"
title: "Box 2 Übersicht"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44317
- /details1fdc.html
imported:
- "2019"
_4images_image_id: "44317"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44317 -->
"diverse Reste", obere Lage.
Unten links Pneumatikteile. In den beiden Kassetten mit Deckel:
Kassette 1: Verbinder 15, Rastachsenverbinder, Rastadapter
Kassette 2: Federnocken, Klemmbuchsen

Eine Tüte mit Schläuchen, außerdem ein TX und ein ft-Akku
