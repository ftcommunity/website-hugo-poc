---
layout: "image"
title: "Box 1.2 oben links"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44313
- /details6bd9-2.html
imported:
- "2019"
_4images_image_id: "44313"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44313 -->
Verbinder 30 und 45

Winkelsteine 10 und Eckstein (38240)

Winkelsteine 15 und 7,5

Winkelsteine 30

Baustein 5 mit 2 Zapfen

Baustein 5 mit 1 Zapfen, Baustein 7,5

Dynamics-Kleinteile

Dynamics-Kugelhalter und Kugeln
