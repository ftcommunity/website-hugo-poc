---
layout: "image"
title: "Untere Box Nr. 1"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44305
- /detailsf992.html
imported:
- "2019"
_4images_image_id: "44305"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44305 -->
Abgedeckt von insgesamt 4 Bauplatten 500, die auch gleichzeitig das verrutschen von Kleinteilen verhindern.