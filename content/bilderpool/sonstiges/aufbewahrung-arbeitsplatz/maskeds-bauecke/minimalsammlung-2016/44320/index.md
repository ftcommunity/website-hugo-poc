---
layout: "image"
title: "Box 2 rechte Seite unten"
date: "2016-08-20T23:05:38"
picture: "minimalsammlung17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/44320
- /detailsa890.html
imported:
- "2019"
_4images_image_id: "44320"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:38"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44320 -->
Unter den Schläuchen, dem Akku und TX:
4 Bauplatten 30x90
4 Bauplatten 60x120
4 U-Träger schwarz
Fernsteuerung
Schraubendreher für TST-Madenschrauben
1 Drehkranz TST
1 Drehkranz ft
2A-Netzteil
Ladegerät für ft-Akkus
hier nicht zu sehen: 1 ft-Akku
