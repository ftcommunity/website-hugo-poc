---
layout: "image"
title: "Maskeds Bauecke"
date: "2008-07-22T20:51:14"
picture: "masked1.jpg"
weight: "1"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/14942
- /details44ca.html
imported:
- "2019"
_4images_image_id: "14942"
_4images_cat_id: "1364"
_4images_user_id: "373"
_4images_image_date: "2008-07-22T20:51:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14942 -->
Übersicht
