---
layout: "image"
title: "Bautisch"
date: "2008-07-22T20:51:14"
picture: "masked3.jpg"
weight: "3"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/14944
- /details8cf6.html
imported:
- "2019"
_4images_image_id: "14944"
_4images_cat_id: "1364"
_4images_user_id: "373"
_4images_image_date: "2008-07-22T20:51:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14944 -->
Hinten links sämtliche Grundbausteine, in der Etage darunter diverses Zeug. Davor Boxen 500 mit Statik und Zanhrädern (da hat mein Opa mal aus den alten ut-Kästen die Zwischenstege rausgebrochen...arrrrgggg), nochmal davor ein Sortierkasten mit Statik-Kleinteilen.
Die Beiden großen Sortimentskästen sind von Aldi, gibts nächste Woche (31.Juli) wieder bei Aldi Süd!
Außerdem gibts noch ein Regal, in dem alle Teile rumliegen, die ich so gut wie nie brauche oder für die man einfach keinen gescheiten Platz finde...
