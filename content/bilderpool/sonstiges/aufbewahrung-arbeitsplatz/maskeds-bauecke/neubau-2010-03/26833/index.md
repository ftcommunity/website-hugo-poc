---
layout: "image"
title: "rechte Seite"
date: "2010-03-28T14:49:50"
picture: "bauecke4.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26833
- /detailseaec.html
imported:
- "2019"
_4images_image_id: "26833"
_4images_cat_id: "1918"
_4images_user_id: "373"
_4images_image_date: "2010-03-28T14:49:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26833 -->
Hier hat sich nicht viel geändert.
