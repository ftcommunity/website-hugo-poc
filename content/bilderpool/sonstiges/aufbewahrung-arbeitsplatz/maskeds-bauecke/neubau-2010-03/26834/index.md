---
layout: "image"
title: "Schrank"
date: "2010-03-28T14:49:50"
picture: "bauecke5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26834
- /details3173.html
imported:
- "2019"
_4images_image_id: "26834"
_4images_cat_id: "1918"
_4images_user_id: "373"
_4images_image_date: "2010-03-28T14:49:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26834 -->
Mit einem der beiden "Nebenlager" für Alles, was in größeren Mengen vorhanden ist. In diesem Fall hauptsächlich Statik, Statik und nochmal Statik.
Für die Statik-Streben habe ich inzwischen auch eine ordentliche Aufbewahrungsmöglichkeit gefunden. Sie sind nach Länge, Farbe und mit/ohne Löcher in Druckverschlussbeuteln sortiert. Auf denen steht jeweils groß drauf was drin ist, außerdem sind sie durchsichtig und somit sieht mans auch sehr gut. Selbst größte Mengen lassen sich in entsprechenden Beutel-Größen (ebay) sehr gut unterbringen.
