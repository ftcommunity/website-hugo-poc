---
layout: "image"
title: "PC"
date: "2008-08-24T16:00:37"
picture: "dragonsarbeitsplatz02.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15098
- /detailse8b6.html
imported:
- "2019"
_4images_image_id: "15098"
_4images_cat_id: "1376"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15098 -->
Ein durchschnittlicher PC mit Surround System.