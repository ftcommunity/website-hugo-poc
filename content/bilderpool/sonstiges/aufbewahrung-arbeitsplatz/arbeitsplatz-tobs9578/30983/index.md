---
layout: "image"
title: "4er Einsatz"
date: "2011-07-04T11:32:23"
picture: "einsaetze1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30983
- /detailsaa36.html
imported:
- "2019"
_4images_image_id: "30983"
_4images_cat_id: "2315"
_4images_user_id: "1007"
_4images_image_date: "2011-07-04T11:32:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30983 -->
Hallo ich habe mir auch die Kästen angeschafft von remadus um bei mir Ordnung reinzubringen(http://www.ftcommunity.de/details.php?image_id=13670).
Nun habe ich mir gedacht Einsätze aus Sperrholz dafür zu bauen.
Meine ersten Ideen seht ihr hier.Mit einem kostenlosem 3D programm von Google skizziert und dann als Bild exportiert.
Ich werde euch natürlich dann auch die Realität zeigen,wenn sie fertig ist.

Gruß tobs9578
