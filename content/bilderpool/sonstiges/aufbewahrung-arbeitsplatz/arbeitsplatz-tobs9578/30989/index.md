---
layout: "image"
title: "6er Einsatz"
date: "2011-07-05T21:10:09"
picture: "einsatz2.jpg"
weight: "5"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30989
- /details6d60.html
imported:
- "2019"
_4images_image_id: "30989"
_4images_cat_id: "2315"
_4images_user_id: "1007"
_4images_image_date: "2011-07-05T21:10:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30989 -->
