---
layout: "image"
title: "4er Einsatz"
date: "2011-07-05T21:10:09"
picture: "einsatz1.jpg"
weight: "4"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30988
- /detailsd0dc.html
imported:
- "2019"
_4images_image_id: "30988"
_4images_cat_id: "2315"
_4images_user_id: "1007"
_4images_image_date: "2011-07-05T21:10:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30988 -->
