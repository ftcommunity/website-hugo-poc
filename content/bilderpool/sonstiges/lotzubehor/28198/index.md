---
layout: "image"
title: "Typenschild"
date: "2010-09-19T18:19:47"
picture: "loetzubehoer7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28198
- /detailsbc24.html
imported:
- "2019"
_4images_image_id: "28198"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28198 -->
Ich glaube, die waren von Reichelt. Mit den wie hier beschrieben gelöteten Buchsen kann ich damit nun auch normale ft-Modelle ohne Computing betreiben.
