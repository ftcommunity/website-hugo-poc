---
layout: "image"
title: "Stehend geht auch"
date: "2010-09-19T18:19:46"
picture: "loetzubehoer3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28194
- /detailsb2ad.html
imported:
- "2019"
_4images_image_id: "28194"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28194 -->
Auch so kann man es aufstellen, um von allen Seiten bequem dran zu kommen.
