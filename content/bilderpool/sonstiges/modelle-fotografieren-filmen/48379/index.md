---
layout: "image"
title: "Plotterpapier Befestigung mit Flacheisen"
date: "2018-11-06T11:03:10"
picture: "fotografieren09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48379
- /details0604.html
imported:
- "2019"
_4images_image_id: "48379"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48379 -->
