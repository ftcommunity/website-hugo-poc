---
layout: "image"
title: "Fotostudio Leuchte Set in Tragetasche"
date: "2018-11-06T11:03:21"
picture: "fotografieren17.jpg"
weight: "17"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48387
- /detailsd367.html
imported:
- "2019"
_4images_image_id: "48387"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48387 -->
Fotostudio-Komplett-Set bestehend aus:

3 Stativen, 1 Galgenstativ, 3 Softboxen, 3 Diffusoren, 3 Leuchtmitteln und großer Tragetasche
Galgenstativ mit Ballasttasche: Höhenverstellbar 78 - 350 cm und schwenkbar für mehr Flexibilität bei Aufnahmen
3 leichte und stabile Lampenstative aus Aluminium mit verstellbarer Höhe
3 verstellbare Softboxen inkl. entferntbaren Diffusoren: 40 x 40 cm für das weiche Licht und unscharfe Schattenkonturen
Fotolampen: 55 W, Farbtemperatur 5500 Kelvin; E27 Sockel, Spannung: 230 V - 50/60 Hz
Lichtstative: höhenverstellbar von ca. 90 - 215 cm
Galgenstativ: höhenverstellbar mit Galgenstativ von ca. 78 - 350 cm
Softboxen inkl. entfernbaren Diffusoren: ca. 40 x 40 cm
Leuchtmittel: 55 W, Farbtemperatur 5500 Kelvin
Kabellänge: ca. 240 cm
