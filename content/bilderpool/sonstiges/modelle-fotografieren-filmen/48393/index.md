---
layout: "image"
title: "Kameratasche Inhalt"
date: "2018-11-06T11:03:28"
picture: "fotografieren23.jpg"
weight: "23"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48393
- /details7979-2.html
imported:
- "2019"
_4images_image_id: "48393"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:28"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48393 -->
Ich habe die Kamera gleich als Komplettpaket mit Kameratasche gekauft.
Dazu noch einen Ersatzakku.
