---
layout: "image"
title: "Softbox Set"
date: "2018-11-06T11:03:21"
picture: "fotografieren16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48386
- /details2847.html
imported:
- "2019"
_4images_image_id: "48386"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48386 -->
Stativ, Leuchtmittel, Softbox, Diffusor.

