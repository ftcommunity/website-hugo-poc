---
layout: "image"
title: "Hama Star 63 Stativ für Kamera"
date: "2018-11-06T11:03:21"
picture: "fotografieren18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48388
- /details909b.html
imported:
- "2019"
_4images_image_id: "48388"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48388 -->
Dies ist das Stativ der Firma Hama mit Tragetasche.
Dieses Modell kann von 66cm bis 166 cm ausgefahren werden.

