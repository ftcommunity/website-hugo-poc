---
layout: "image"
title: "Stativ für Kamera"
date: "2018-11-06T11:03:21"
picture: "fotografieren19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48389
- /details84b5.html
imported:
- "2019"
_4images_image_id: "48389"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48389 -->
Das Stativ ist nicht teuer und wird mit Hilfe der kleinen Libellen ausgerichtet.Es kann 
in allen Positionen geschwenkt und dann fixiert werden. Unten kann noch ein Gewicht eingehängt werden.
