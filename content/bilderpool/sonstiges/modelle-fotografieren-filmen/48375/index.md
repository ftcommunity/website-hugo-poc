---
layout: "image"
title: "Kamera"
date: "2018-11-06T11:03:10"
picture: "fotografieren05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48375
- /details1199.html
imported:
- "2019"
_4images_image_id: "48375"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48375 -->
