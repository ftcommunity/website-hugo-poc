---
layout: "image"
title: "Bildbeispiel 4 Fräse"
date: "2018-11-06T11:03:28"
picture: "fotografieren29.jpg"
weight: "29"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/48399
- /detailsb034.html
imported:
- "2019"
_4images_image_id: "48399"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:28"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48399 -->
