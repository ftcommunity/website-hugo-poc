---
layout: "comment"
hidden: true
title: "24295"
date: "2018-11-11T12:19:19"
uploadBy:
- "EstherM"
license: "unknown"
imported:
- "2019"
---
Das heißt, Du hast für die stationäre Fotoecke ungefähr 150 Euro ausgegeben. Die Kamera baust Du immer neu auf, und nutzt sie auch für alle anderen Anlässe. Das ist ja jetzt nicht so viel Geld. Um den Platz, den Du dafür nutzen konntest, beneide ich Dich allerdings!
Gruß
Esther