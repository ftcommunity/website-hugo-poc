---
layout: "image"
title: "Blinder Roboter klein"
date: "2007-04-02T20:48:16"
picture: "ForumDSC06678Forum.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9888
- /detailsa4a2.html
imported:
- "2019"
_4images_image_id: "9888"
_4images_cat_id: "843"
_4images_user_id: "445"
_4images_image_date: "2007-04-02T20:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9888 -->
Dieses Bild bitte anstelle des anderen einstellen, umd den alten Komentar verwenden, Danke!
