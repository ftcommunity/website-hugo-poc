---
layout: "image"
title: "Koffer mit herausnehmbarer Polsterung"
date: "2017-06-12T17:05:49"
picture: "Koffer.jpg"
weight: "98"
konstrukteure: 
- "wahrscheinlich Allit"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/45936
- /detailsaeeb.html
imported:
- "2019"
_4images_image_id: "45936"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2017-06-12T17:05:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45936 -->
Koffer 400*300*120
