---
layout: "image"
title: "cfw BenoiTxt - Bild 2"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt04.jpeg"
weight: "4"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45252
- /details6a39.html
imported:
- "2019"
_4images_image_id: "45252"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45252 -->
...solche Bilder zu erzeugen...
