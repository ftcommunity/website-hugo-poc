---
layout: "image"
title: "MODELL 12: Bezeichnung: US Truck mit Tiefbettauflieger"
date: "2015-08-16T18:51:31"
picture: "Nr._12_Bild_5.jpg"
weight: "47"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41799
- /detailsa4af.html
imported:
- "2019"
_4images_image_id: "41799"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41799 -->
