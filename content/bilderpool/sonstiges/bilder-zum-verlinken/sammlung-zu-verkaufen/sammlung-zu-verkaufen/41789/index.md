---
layout: "image"
title: "MODELL 10: Bezeichnung: 2 Achs Anhänger für MB Trac"
date: "2015-08-16T18:51:31"
picture: "Nr._10_Bild_1.jpg"
weight: "37"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41789
- /details58c1.html
imported:
- "2019"
_4images_image_id: "41789"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41789 -->
MODELL 10: Bezeichnung: 2 Achs Anhänger für MB Trac // Länge: ca. 33 cm Breite: ca. 11 cm Höhe: ca. 11 cm Besonderes: Kipperaufbau / Kennzeichen // -