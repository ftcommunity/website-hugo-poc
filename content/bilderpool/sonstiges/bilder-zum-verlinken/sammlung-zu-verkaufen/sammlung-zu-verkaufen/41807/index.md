---
layout: "image"
title: "MODELL 15: Bezeichnung: MAN Feuerwehr Drehleiterwagen"
date: "2015-08-16T18:51:31"
picture: "Nr._15_Bild_1.jpg"
weight: "55"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41807
- /details7801.html
imported:
- "2019"
_4images_image_id: "41807"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41807 -->
MODELL 15: Bezeichnung: MAN Feuerwehr Drehleiterwagen // Länge: ca. 48 cm mit eingefahrener Leiter Breite: ca. 12,5 cm Höhe: ca. 20 cm mit eingefahrener Leiter Besonderes: gelenkte Nachlaufachse durch automatische Ansteuerung bei Einschlag des Lenkrades  / Drehleiter mit Korb (aus Holz) / Wassertank mit Handpumpe / Wasserschlauch / Besatzung / elektrische blaue Rundumleuchten / Kennzeichen // -