---
layout: "image"
title: "MODELL 13 mit MODELL 19: Bezeichnung: Wechselaufbaukranaggregat z.B. für US Truck &#8222;Peterbilt&#8220;"
date: "2015-08-16T18:51:31"
picture: "Nr._19_Bild_1.jpg"
weight: "74"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41826
- /details857f.html
imported:
- "2019"
_4images_image_id: "41826"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41826 -->
MODELL 19: Bezeichnung: Wechselaufbaukranaggregat z.B. für US Truck &#8222;Peterbilt&#8220; // Länge: ca. 20 cm Breite: ca. 11 cm Höhe: ca. 18 cm Besonderes:         - /-