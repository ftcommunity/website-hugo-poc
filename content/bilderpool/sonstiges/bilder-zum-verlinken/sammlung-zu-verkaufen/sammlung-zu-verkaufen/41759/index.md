---
layout: "image"
title: "MODELL 1: Bezeichnung: 3 Achser &#8222;Scania&#8220; mit 2 Achs Anhänger /  Planenzug"
date: "2015-08-16T18:51:31"
picture: "Nr.1_Bild_7.jpg"
weight: "7"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41759
- /detailse8aa.html
imported:
- "2019"
_4images_image_id: "41759"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41759 -->
