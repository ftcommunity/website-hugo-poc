---
layout: "image"
title: "MODELL 9: Bezeichnung: 2 Achs Anhänger für MB Trac"
date: "2015-08-16T18:51:31"
picture: "Nr._9_Bild_2.jpg"
weight: "36"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41788
- /details3085.html
imported:
- "2019"
_4images_image_id: "41788"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41788 -->
