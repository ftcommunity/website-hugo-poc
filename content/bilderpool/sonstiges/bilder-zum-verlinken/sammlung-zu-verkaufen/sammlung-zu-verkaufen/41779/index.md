---
layout: "image"
title: "MODELL 6: Bezeichnung: Kleiner 2 Achs Kranwagen"
date: "2015-08-16T18:51:31"
picture: "Nr._6_Bild_1.jpg"
weight: "27"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41779
- /details91f2.html
imported:
- "2019"
_4images_image_id: "41779"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41779 -->
MODELL 6: Bezeichnung: Kleiner 2 Achs Kranwagen // Länge: ca. 40 cm Breite: ca. 12,5 cm Höhe: ca. 20 Besonderes:       - /-