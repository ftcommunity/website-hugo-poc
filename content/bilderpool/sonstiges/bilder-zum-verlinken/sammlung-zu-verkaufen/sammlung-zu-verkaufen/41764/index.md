---
layout: "image"
title: "MODELL 2:  VERKAUFT!!!"
date: "2015-08-16T18:51:31"
picture: "Nr._2_Bild_5.jpg"
weight: "12"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41764
- /detailsc752.html
imported:
- "2019"
_4images_image_id: "41764"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41764 -->
MODELL 2: VERKAUFT!!!