---
layout: "image"
title: "MODELL 4: Bezeichnung: Mercedes Benz Schwerlastsattelzugmaschine mit Flachbettauflieger"
date: "2015-08-16T18:51:31"
picture: "Nr._4_Bild_5.jpg"
weight: "22"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41774
- /detailsc0a8.html
imported:
- "2019"
_4images_image_id: "41774"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41774 -->
