---
layout: "image"
title: "MODELL 4: Bezeichnung: Mercedes Benz Schwerlastsattelzugmaschine mit Flachbettauflieger"
date: "2015-08-16T18:51:31"
picture: "Nr._4_Bild_1.jpg"
weight: "18"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41770
- /details1d45-2.html
imported:
- "2019"
_4images_image_id: "41770"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41770 -->
MODELL 4: Bezeichnung: Mercedes Benz Schwerlastsattelzugmaschine mit Flachbettauflieger // Länge: ca. 134 cm Breite: ca. 15 cm Höhe: ca. 18 cm Besonderes: 4 Achs Sattelzugmaschine mit über Lenkrad angesteuerter Doppellenkachse / elektrischer Arbeitsscheinwerfer / gelbe elektrische Rundumleuchten / Falk Stadtplan und Cola Flasche in Fahrertüre / 5 Achs Auflieger mit automatischem Lenkeinschlag der Räder bei Ansteuerung / Kennzeichen // -