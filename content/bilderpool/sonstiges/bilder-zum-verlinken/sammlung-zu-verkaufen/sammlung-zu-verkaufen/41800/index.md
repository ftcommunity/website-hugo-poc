---
layout: "image"
title: "MODELL 13: Bezeichnung: US Truck 3 Achser Zugmaschine &#8222;Peterbilt&#8220;"
date: "2015-08-16T18:51:31"
picture: "Nr._13_Bild_1.jpg"
weight: "48"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- /php/details/41800
- /detailsc218.html
imported:
- "2019"
_4images_image_id: "41800"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41800 -->
MODELL 13: Bezeichnung: US Truck 3 Achser Zugmaschine &#8222;Peterbilt&#8220; // Länge: ca. 45 cm Breite: ca. 12 cm Höhe: ca. 18,5 cm Besonderes: Ladekran am Heck / bedruckte Plane / elektrischer Arbeitsschweinwerfer / gelbe elektrische Rundumleuchten / bei Lenkungseinschlag automatisch                    angesteuerte Nachlaufachse / Seilwinde vorn / Kennzeichen // -