---
layout: "image"
title: "ballmaschine"
date: "2008-10-26T11:12:07"
picture: "fischertechnik_004.jpg"
weight: "41"
konstrukteure: 
- "tim"
fotografen:
- "tim"
uploadBy: "t@tim"
license: "unknown"
legacy_id:
- /php/details/16080
- /detailsa8f4.html
imported:
- "2019"
_4images_image_id: "16080"
_4images_cat_id: "843"
_4images_user_id: "705"
_4images_image_date: "2008-10-26T11:12:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16080 -->
