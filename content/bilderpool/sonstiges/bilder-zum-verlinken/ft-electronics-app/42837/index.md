---
layout: "image"
title: "Windows Phone 8.1-App - MiniBots"
date: "2016-01-30T01:23:15"
picture: "ftelectronicsapp3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42837
- /details3f37.html
imported:
- "2019"
_4images_image_id: "42837"
_4images_cat_id: "3184"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T01:23:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42837 -->
Und hier schließlich die Spezialfunktionen des MiniBots-Moduls. Einige Standardprogramme sind da noch nicht enthalten, weil ich noch keine Doku darüber habe.
