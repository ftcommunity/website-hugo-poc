---
layout: "image"
title: "Windows Phone 8.1-App - E-Tec"
date: "2016-01-30T01:23:15"
picture: "ftelectronicsapp1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42835
- /detailsaad2.html
imported:
- "2019"
_4images_image_id: "42835"
_4images_cat_id: "3184"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T01:23:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42835 -->
So sieht die Phone-App aktuell aus, wenn man sie startet. Man wischt nach links/rechts zu den drei Moduln und nach oben/unten zu den Funktionen.

Wichtig: Die App ist noch nicht veröffentlicht.
