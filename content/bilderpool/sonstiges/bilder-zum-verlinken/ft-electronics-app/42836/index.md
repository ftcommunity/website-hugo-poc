---
layout: "image"
title: "Windows Phone 8.1-App - Electronics"
date: "2016-01-30T01:23:15"
picture: "ftelectronicsapp2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42836
- /detailsf6b3.html
imported:
- "2019"
_4images_image_id: "42836"
_4images_cat_id: "3184"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T01:23:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42836 -->
Bei vielen Funktionen sind auch gleich Schaltbilder sichtbar. Hier die Übersicht über das Electronics-Modul.
