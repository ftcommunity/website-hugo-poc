---
layout: "image"
title: "Abstandsmessung mit dem I2C Sensor VCNL4010"
date: "2016-01-24T12:16:29"
picture: "VCNL4010_Abstandsmessung.png"
weight: "80"
konstrukteure: 
- "C. Hehr"
fotografen:
- "C. Hehr"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/42728
- /details61d3.html
imported:
- "2019"
_4images_image_id: "42728"
_4images_cat_id: "843"
_4images_user_id: "2374"
_4images_image_date: "2016-01-24T12:16:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42728 -->
Eine Fischtechnikplatte wurde mehrmals zwischen 65 mm und 58 mm Endanschlag  per Hand bewegt. Dabei wurden die VCNL4010 Sensordaten ausgelesen.