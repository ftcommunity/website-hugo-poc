---
layout: "comment"
hidden: true
title: "7664"
date: "2008-10-27T17:50:02"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Hallo Tim,
die dargestellte Sperre ist sicherlich die einfachste und sicherste Lösung den Ball unter Kontrolle zu halten. 
Eine Heausforderung an euch Roboter-Experten wäre es auf diese Sperre zu verzichten und statt dessen über mehrere Sensoren die Ballpositions zu erkennen und dementsprechend den Vortrieb bzw. Lenkung zu steuern und zu regeln.

Gruß jw