---
layout: "image"
title: "Zeitmessgerät"
date: "2015-01-18T16:07:01"
picture: "ft_Zeitmess.jpg"
weight: "78"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/40383
- /details50d1.html
imported:
- "2019"
_4images_image_id: "40383"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2015-01-18T16:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40383 -->
Hallo,

ich habe da diesen Schaltplan. Kennt jemand vielleicht das Modell dazu???
liebe Grüße ludger
