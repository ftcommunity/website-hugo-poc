---
layout: "image"
title: "Keine Verzweigung bei den Tastern"
date: "2012-03-20T09:42:18"
picture: "robopro_problem.jpg"
weight: "67"
konstrukteure: 
- "Reiner Stüven"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/34664
- /details153a.html
imported:
- "2019"
_4images_image_id: "34664"
_4images_cat_id: "843"
_4images_user_id: "109"
_4images_image_date: "2012-03-20T09:42:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34664 -->
Programm verzweigt nicht wie erwartet bei gedrückten Taster