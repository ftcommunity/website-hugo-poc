---
layout: "image"
title: "Blinder Roboter klein"
date: "2007-04-01T17:49:09"
picture: "DSC06678K.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9871
- /details710a.html
imported:
- "2019"
_4images_image_id: "9871"
_4images_cat_id: "843"
_4images_user_id: "445"
_4images_image_date: "2007-04-01T17:49:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9871 -->
Dieses Bild wird unten an meinen Nachrichten angezeigt.
