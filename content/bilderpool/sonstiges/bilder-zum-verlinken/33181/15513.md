---
layout: "comment"
hidden: true
title: "15513"
date: "2011-10-22T12:16:21"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Die Reifen sind echt intreressant aufgebaut. Soweit ich das erkennen kann sind in das in der Mitte die roten Drehscheiben und um die ist dann mit Winkelsteinen und normalen Bausteinen der eigentlich Reifen bzw Felge aufgebaut. Durch kurze Stangen sind die Drehscheiben mit den Steinen verbunden. Später sind dann die Raupengummis drumherum gewickelt worden. Wie genau kann man leider nicht erkennen. Die Reifen müssen aber ganz schön dick und groß gewesen sein. 
Mich würde mal interessieren ob das Modell noch existiert oder schon zerlegt/verkauft worden ist. 

Viele Grüße
Stephan