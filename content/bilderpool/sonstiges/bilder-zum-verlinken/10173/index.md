---
layout: "image"
title: "Schwarze Kette"
date: "2007-04-26T15:36:23"
picture: "DSC07862.jpg"
weight: "10"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10173
- /detailscfd2.html
imported:
- "2019"
_4images_image_id: "10173"
_4images_cat_id: "843"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10173 -->
Diese Kette gleicht der Kette von ft, nur ist sie schwarz. Die Kette ist vom Brio Builder System, Das ist eine mischung aus Lego Technik, Bilofix und Matador, ein kleines bisschen ft ist auch eingebaut.
