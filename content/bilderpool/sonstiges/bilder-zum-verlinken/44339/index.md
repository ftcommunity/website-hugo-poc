---
layout: "image"
title: "IR Empfänger aus alten FT-Elektronik-Bausteinen + Attiny84"
date: "2016-09-06T20:55:28"
picture: "20160906_185550.jpg"
weight: "86"
konstrukteure: 
- "thomasm"
fotografen:
- "thomasm"
uploadBy: "thomasm"
license: "unknown"
legacy_id:
- /php/details/44339
- /detailscb5b-2.html
imported:
- "2019"
_4images_image_id: "44339"
_4images_cat_id: "843"
_4images_user_id: "2632"
_4images_image_date: "2016-09-06T20:55:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44339 -->
Zu sehen ist das Fahrzeug von oben mit 4 Elektronik-Bausteinen. Die Hauptarbeit verrichtet der Attiny84.