---
layout: "image"
title: "Mosaik-Plakat"
date: "2011-06-24T21:27:23"
picture: "mosaikjahre1.jpg"
weight: "60"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30907
- /detailscf8a.html
imported:
- "2019"
_4images_image_id: "30907"
_4images_cat_id: "843"
_4images_user_id: "373"
_4images_image_date: "2011-06-24T21:27:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30907 -->
Siehe http://forum.ftcommunity.de/viewtopic.php?f=23&t=714
Kommentare bitte auch dort, sonst entstehen zwei Plätze an denen diskutiert wird.
