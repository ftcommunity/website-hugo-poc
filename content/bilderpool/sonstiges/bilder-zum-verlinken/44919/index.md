---
layout: "image"
title: "Brickly mit Funktionsdefinition"
date: "2016-12-20T15:00:30"
picture: "Funktion.jpg"
weight: "89"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/44919
- /detailsf34b.html
imported:
- "2019"
_4images_image_id: "44919"
_4images_cat_id: "843"
_4images_user_id: "2488"
_4images_image_date: "2016-12-20T15:00:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44919 -->
