---
layout: "image"
title: "Thales01"
date: "2008-10-20T08:42:13"
picture: "Thales01.jpg"
weight: "6"
konstrukteure: 
- "Thales"
fotografen:
- "HLGR"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/16014
- /details59e6.html
imported:
- "2019"
_4images_image_id: "16014"
_4images_cat_id: "1456"
_4images_user_id: "832"
_4images_image_date: "2008-10-20T08:42:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16014 -->
