---
layout: "image"
title: "Facit04"
date: "2008-10-20T08:42:13"
picture: "Facit04.jpg"
weight: "10"
konstrukteure: 
- "Facit"
fotografen:
- "HLGR"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/16018
- /details25ba.html
imported:
- "2019"
_4images_image_id: "16018"
_4images_cat_id: "1456"
_4images_user_id: "832"
_4images_image_date: "2008-10-20T08:42:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16018 -->
