---
layout: "image"
title: "ballmaschine"
date: "2008-10-26T10:31:17"
picture: "fischertechnik_002.jpg"
weight: "39"
konstrukteure: 
- "tim"
fotografen:
- "tim"
uploadBy: "t@tim"
license: "unknown"
legacy_id:
- /php/details/16078
- /details3dca.html
imported:
- "2019"
_4images_image_id: "16078"
_4images_cat_id: "843"
_4images_user_id: "705"
_4images_image_date: "2008-10-26T10:31:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16078 -->
