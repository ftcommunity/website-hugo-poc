---
layout: "image"
title: "Stift-Revolver"
date: "2007-06-03T19:07:01"
picture: "plotter2.jpg"
weight: "16"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/10679
- /details4984.html
imported:
- "2019"
_4images_image_id: "10679"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10679 -->
Der Revolver wird über eine sehr trickreiche Mechanik gedreht - ich muss unbedingt mal ein Video machen. Rechts sieht man die Andruckleiste, auf der ein Kugellager läuft.
