---
layout: "image"
title: "30493_I'm walking_Bauanleitung_Seite15_Ausschnitt"
date: "2009-05-30T09:12:56"
picture: "imwalking05.jpg"
weight: "5"
konstrukteure: 
- "ft"
fotografen:
- "Ed"
uploadBy: "Ed"
license: "unknown"
legacy_id:
- /php/details/24141
- /details3d05.html
imported:
- "2019"
_4images_image_id: "24141"
_4images_cat_id: "1657"
_4images_user_id: "43"
_4images_image_date: "2009-05-30T09:12:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24141 -->
Erläuterung, deutsch