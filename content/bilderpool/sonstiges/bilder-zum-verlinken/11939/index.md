---
layout: "image"
title: "Gemetzel in Rot"
date: "2007-09-23T20:53:12"
picture: "Gemetzel_in_Rot.JPG"
weight: "28"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11939
- /detailsfb3f.html
imported:
- "2019"
_4images_image_id: "11939"
_4images_cat_id: "843"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T20:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11939 -->
Tatort: Deutsches Museum, München, Abteilung "Technisches Spielzeug", 23.09.2007
