---
layout: "image"
title: "ft Roboter"
date: "2009-06-20T13:34:49"
picture: "fischertechnikreferat2.jpg"
weight: "2"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24417
- /details92d4.html
imported:
- "2019"
_4images_image_id: "24417"
_4images_cat_id: "1673"
_4images_user_id: "430"
_4images_image_date: "2009-06-20T13:34:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24417 -->
Der Roboter stand während des Referates neben, bzw. vor mir. 
Der Bewegungsablauf ist denkbar einfach. 
Wenn man hinten auf den Taster drückt, schwenkt der Robo seinen Kopf erst nach rechts und winkt mit dem rechten Arm.
Anschließend schwenkt der Kopf nach links und er winkt mit dem linken Arm.
Während er das macht, leuchten seine Augen und jeweils abwechselnd drei von sechs Lampen die den Mund bilden.