---
layout: "image"
title: "Buzzer"
date: "2009-06-20T13:34:49"
picture: "fischertechnikreferat3.jpg"
weight: "3"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24418
- /detailsc281.html
imported:
- "2019"
_4images_image_id: "24418"
_4images_cat_id: "1673"
_4images_user_id: "430"
_4images_image_date: "2009-06-20T13:34:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24418 -->
Mit diesem Taster konnte man den Roboter auslösen. 
Trotz des einfachen Aufbaus, macht es richtig Spaß da drauf zu hauen :)