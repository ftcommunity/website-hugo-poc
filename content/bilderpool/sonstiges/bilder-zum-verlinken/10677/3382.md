---
layout: "comment"
hidden: true
title: "3382"
date: "2007-06-07T09:39:11"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Huch, baust du deinen C64 (endlich) wieder auf? Willkommen im Retro Club ;)

Der wird ja über den seriellen Commodore Bus angesteuert. Du willst doch nicht etwa das Protokoll in deinem Basic umsetzen und ihn an die Serielle hängen :o

Gruß,
Thomas

PS: Hab auch einen im Schrank, meiner geht aber (ähm zumindest vor ein paar Jahren...)