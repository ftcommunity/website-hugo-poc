---
layout: "image"
title: "zug06.jpg"
date: "2014-10-10T19:10:48"
picture: "zug06.jpg"
weight: "6"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39698
- /detailscc7a.html
imported:
- "2019"
_4images_image_id: "39698"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39698 -->
