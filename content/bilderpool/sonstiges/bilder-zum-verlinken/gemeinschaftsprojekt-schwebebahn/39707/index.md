---
layout: "image"
title: "zug15.jpg"
date: "2014-10-10T19:10:48"
picture: "zug15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39707
- /detailsc21e.html
imported:
- "2019"
_4images_image_id: "39707"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39707 -->
