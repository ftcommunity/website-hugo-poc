---
layout: "image"
title: "zug01.jpg"
date: "2014-10-10T19:10:48"
picture: "zug01.jpg"
weight: "1"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39693
- /detailsa2c5.html
imported:
- "2019"
_4images_image_id: "39693"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39693 -->
Der Zug für das Gemeinschaftsprojekt Schwebebahn