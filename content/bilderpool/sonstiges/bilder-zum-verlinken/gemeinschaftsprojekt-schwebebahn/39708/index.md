---
layout: "image"
title: "zug16.jpg"
date: "2014-10-10T19:10:48"
picture: "zug16.jpg"
weight: "16"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39708
- /details750b.html
imported:
- "2019"
_4images_image_id: "39708"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39708 -->
Ein unten verbauter xs Motor öffnet und schließt die Türen.