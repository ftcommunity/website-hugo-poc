---
layout: "image"
title: "Ostern statt Weihnachten - TXT Discovery Set - Software RoboPro und Introduction CD fehlen"
date: "2014-12-25T00:05:46"
picture: "osternstattweihnachten12.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "fisch"
uploadBy: "fisch"
license: "unknown"
legacy_id:
- /php/details/39976
- /detailsc0fe.html
imported:
- "2019"
_4images_image_id: "39976"
_4images_cat_id: "3004"
_4images_user_id: "2227"
_4images_image_date: "2014-12-25T00:05:46"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39976 -->
Bein TXT Discovery Set - 
Fehlt die Software RoboPro und Introduction CD