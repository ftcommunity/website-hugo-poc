---
layout: "image"
title: "Ostern statt Weihnachten - TXT Discovery Set - Software RoboPro und Introduction CD fehlen"
date: "2014-12-25T00:05:46"
picture: "osternstattweihnachten01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "fisch"
uploadBy: "fisch"
license: "unknown"
legacy_id:
- /php/details/39965
- /details2049-2.html
imported:
- "2019"
_4images_image_id: "39965"
_4images_cat_id: "3004"
_4images_user_id: "2227"
_4images_image_date: "2014-12-25T00:05:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39965 -->
