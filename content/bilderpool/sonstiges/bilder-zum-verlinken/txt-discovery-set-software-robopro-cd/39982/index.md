---
layout: "image"
title: "Ostern statt Weihnachten - TXT Discovery Set - Software RoboPro und Introduction CD fehlen"
date: "2014-12-25T00:05:46"
picture: "osternstattweihnachten18.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "fisch"
uploadBy: "fisch"
license: "unknown"
legacy_id:
- /php/details/39982
- /details6060.html
imported:
- "2019"
_4images_image_id: "39982"
_4images_cat_id: "3004"
_4images_user_id: "2227"
_4images_image_date: "2014-12-25T00:05:46"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39982 -->
Plan B