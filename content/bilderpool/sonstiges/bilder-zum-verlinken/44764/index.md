---
layout: "image"
title: "Misumi Alu Profil mit Statik Verbinder"
date: "2016-11-14T17:07:00"
picture: "IMG_7680.jpg"
weight: "87"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Alu", "Misumi", "Profile"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/44764
- /detailseafd.html
imported:
- "2019"
_4images_image_id: "44764"
_4images_cat_id: "843"
_4images_user_id: "579"
_4images_image_date: "2016-11-14T17:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44764 -->
Man kann den Stein durch vorsichtiges Eindrehen befestigen.