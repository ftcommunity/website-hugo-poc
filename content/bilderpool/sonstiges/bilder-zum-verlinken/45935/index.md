---
layout: "image"
title: "Allit Sortimentskasten"
date: "2017-06-12T17:05:49"
picture: "Allit.jpg"
weight: "97"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/45935
- /details6d70.html
imported:
- "2019"
_4images_image_id: "45935"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2017-06-12T17:05:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45935 -->
die roten, gelben und blauen Einsätze habe ich durch 3 graue getauscht.
Eine genaue Beschreibung folgt später.
