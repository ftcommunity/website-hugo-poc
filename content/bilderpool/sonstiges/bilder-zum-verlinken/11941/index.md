---
layout: "image"
title: "Arbeitstier02.JPG"
date: "2007-09-24T18:22:04"
picture: "Arbeitstier02.JPG"
weight: "30"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11941
- /details7b78.html
imported:
- "2019"
_4images_image_id: "11941"
_4images_cat_id: "843"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T18:22:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11941 -->
Was dem Dagobert Duck sein erster selbstverdienter Taler ist, das ist mir das erste ft-Getriebe. Man sieht es ihm an, dass es bald 40 Jahre auf dem Buckel hat, verschiedene Experimente zur Schmierung hinter sich hat und manchmal auch grausam gequält wurde.
