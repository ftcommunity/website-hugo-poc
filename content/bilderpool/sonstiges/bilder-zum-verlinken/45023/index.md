---
layout: "image"
title: "Economatics_ Alu Grundplatte für Pneumatikmodelle"
date: "2017-01-08T19:57:18"
picture: "Econaomatics_ALU_Grundplatte.jpg"
weight: "95"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/45023
- /details78ca.html
imported:
- "2019"
_4images_image_id: "45023"
_4images_cat_id: "843"
_4images_user_id: "1688"
_4images_image_date: "2017-01-08T19:57:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45023 -->
Die Pneumatikserie von Economatics war sehr interessant...
