---
layout: "image"
title: "Überblick"
date: "2013-12-01T18:42:43"
picture: "DSC00304_publish.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
schlagworte: ["Überblick"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37861
- /details55af.html
imported:
- "2019"
_4images_image_id: "37861"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37861 -->
Ein Überblick über den Inhalt des Pakets.
Es fehlen allderings drei oder vier Kabel,
zwei Schnüre und ein Reed-Sensor,
die auch noch enthalten waren.