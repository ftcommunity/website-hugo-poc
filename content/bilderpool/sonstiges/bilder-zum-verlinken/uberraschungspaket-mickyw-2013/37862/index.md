---
layout: "image"
title: "Paket_Teil1"
date: "2013-12-01T18:42:43"
picture: "DSC00311_publish.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
schlagworte: ["Kugelbahn"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37862
- /detailsa95e.html
imported:
- "2019"
_4images_image_id: "37862"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37862 -->
Wohl Teile von der Kugelbahn.
Die "Leitern" aber kann ich garnicht zuordnen.