---
layout: "image"
title: "Paket_Teil2"
date: "2013-12-01T18:42:43"
picture: "DSC00313_publish.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
schlagworte: ["Statik", "Achsenklemmen", "Achsen", "Räder"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37863
- /details457c.html
imported:
- "2019"
_4images_image_id: "37863"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37863 -->
Statikteile, Achsenklemmen, Räder.
Das meiste aus dem unteren, rechten Bidlviertel kann ich garnicht zuordnen.