---
layout: "comment"
hidden: true
title: "18491"
date: "2013-12-01T18:47:40"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Die "Druckspender" sind Pumpen aus den Feuerwehr-Kästen. die konnten mit Hilfe eines Flachsteins 30x30 mit Borhung montiert werden. Oben und unten ein Schlauch und eine Düse dran und fertig ist die Feuerwehrspritze. Dafür dienen auch die Kassettendeckel mit Loch, durch das Loch wird der Schlauch geführt, in der Kassette ist der Wasservorrat enthalten.