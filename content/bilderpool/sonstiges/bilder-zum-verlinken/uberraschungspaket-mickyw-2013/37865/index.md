---
layout: "image"
title: "Paket_Teil5"
date: "2013-12-01T18:42:43"
picture: "DSC00317_publish.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
schlagworte: ["Platten", "Deckel"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37865
- /detailsbf82.html
imported:
- "2019"
_4images_image_id: "37865"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37865 -->
Abdeckplatten weiss, rot, schwarz
Druckspender (?) Die kann ich überhaupt nicht zuordnen.
Klare Kistendeckel aber mit Loch! Wozu braucht man die?