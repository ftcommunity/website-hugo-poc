---
layout: "comment"
hidden: true
title: "22243"
date: "2016-07-19T17:01:51"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hallo David,

die Dateien liegen Hr. Samek (ftdesigner) seit Februar vor. Darunter z, B. der TX Controller, TXT Controller, das Sound-Modul etc. Leider ist wenig bis gar nichts aktualisiert worden. Daher habe ich mich entschlossen auf diesem Wege die Dateien zu veröffentlichen. 

Gruß
Dirk