---
layout: "image"
title: "Schrittmotor"
date: "2007-05-25T21:23:34"
picture: "Schrittmotor1.jpg"
weight: "11"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10508
- /detailseefe.html
imported:
- "2019"
_4images_image_id: "10508"
_4images_cat_id: "843"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10508 -->
Das ist der Scrittmotor.
