---
layout: "image"
title: "ballmaschine"
date: "2008-10-26T11:12:07"
picture: "fischertechnik_009.jpg"
weight: "46"
konstrukteure: 
- "tim"
fotografen:
- "tim"
uploadBy: "t@tim"
license: "unknown"
legacy_id:
- /php/details/16085
- /detailse0dd-2.html
imported:
- "2019"
_4images_image_id: "16085"
_4images_cat_id: "843"
_4images_user_id: "705"
_4images_image_date: "2008-10-26T11:12:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16085 -->
