---
layout: "image"
title: "Robo TXT Controller"
date: "2016-07-19T15:45:56"
picture: "522429_Robo_TXT_Controller.jpg"
weight: "84"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
schlagworte: ["ft designer", "Robo", "TXT", "Controller"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43910
- /details8b0b.html
imported:
- "2019"
_4images_image_id: "43910"
_4images_cat_id: "843"
_4images_user_id: "2303"
_4images_image_date: "2016-07-19T15:45:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43910 -->
Bauteil für ftdesigner aus Downloaddatei ftdesigner_bauteile_teil_2

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_2.zip
