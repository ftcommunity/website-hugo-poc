---
layout: "comment"
hidden: true
title: "17282"
date: "2012-09-24T14:45:21"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nachtrag: Der Kasten gehörte wohl eher zum ft-Schulprogramm. Ich habe den nie in einem Laden oder einem normalen ft-Prospekt gesehen, und auch in den Clubheften wurde er meiner Erinnerung nach nie erwähnt.

Gruß,
Stefan