---
layout: "comment"
hidden: true
title: "17284"
date: "2012-09-24T21:21:18"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

Stefan hat natürlich recht mit dem „informic“, da hab‘ ich mich voll verhauen. Sorry!!!

Auch mit dem ft-Schulprogramm dürfte er goldrichtig liegen. Ich kann mich an eine Abbildung des Kastens, aufgrund der vier markant roten Taster erinnern.
Dieser Kasten war Anfang/Mitte der 70er-Jahre, in Verbindung mit den u-t- und den geometric-Kästen, in einem Prospekt abgebildet.

Gruß

Lurchi