---
layout: "image"
title: "TXTShow Webinterface II - Bilder verwalten"
date: "2017-02-03T20:02:53"
picture: "txtshow-webint2.png"
weight: "8"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
schlagworte: ["TXTShow", "cfw"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45120
- /detailsa504.html
imported:
- "2019"
_4images_image_id: "45120"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45120 -->
Hier können die Bilder gelöscht oder Bilder auf den TXT hochgeladen werden.
Außerdem kann man hier das Album umbenennen.

Wenn man das Vorschaubild anklickt, wird das Original angezeigt.
Mit Rechtsklick-"Ziel speichern unter..." kann man das Bild vom TXT herunterladen.
