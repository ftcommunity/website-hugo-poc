---
layout: "image"
title: "TXTShow Menu III"
date: "2017-02-03T20:02:53"
picture: "txtshow04.png"
weight: "5"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45117
- /detailsce69.html
imported:
- "2019"
_4images_image_id: "45117"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45117 -->
Der dritte Menuscreen:

Aktuelles Album auswählen (Album-button), neues Album anlegen (Ordner +), Album löschen (Ordner -) und Album umbenennen (Eingabefeld)
