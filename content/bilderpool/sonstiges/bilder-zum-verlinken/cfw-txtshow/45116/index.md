---
layout: "image"
title: "TXTShow Menu II"
date: "2017-02-03T20:02:53"
picture: "txtshow03.png"
weight: "4"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
schlagworte: ["TXTShow", "cfw"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45116
- /details2c6e.html
imported:
- "2019"
_4images_image_id: "45116"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45116 -->
Der zweite Menuscreen:
Bild in ein anderes Album kopieren (weiße Blätter), Bild in ein anderes Album verschieben (Schere), Bild umbenennen (Eingabefeld) und Bild löschen (Mülleimer)
