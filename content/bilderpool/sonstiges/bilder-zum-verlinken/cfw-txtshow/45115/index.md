---
layout: "image"
title: "TXTShow Menu I"
date: "2017-02-03T20:02:53"
picture: "txtshow02.png"
weight: "3"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
schlagworte: ["TXTShow", "cfw"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/45115
- /details5c2c.html
imported:
- "2019"
_4images_image_id: "45115"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45115 -->
Menuscreen eins:

Schnellauswahl des aktuellen Bilds - über den Drehsteller und die grünen Buttons kann man schnell durch das aktuelle Fotoalbum springen - praktisch, wenn viele Bilder darin sind.

Mit der Uhr kann man die Bildwechselverzögerung für die automatische Diashow  einstellen, die Return-Taste beendet den Menuscreen, mit der Kamera kommt man zum Fotos machen, mit dem blauen Pfeil auf Menuscreen II.
