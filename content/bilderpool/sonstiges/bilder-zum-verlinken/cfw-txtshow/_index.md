---
layout: "overview"
title: "cfw TXTShow"
date: 2020-02-22T09:22:50+01:00
legacy_id:
- /php/categories/3361
- /categories2f84.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3361 --> 
Eine kleine App zum Verwalten von Fotos auf dem TXT.
Es können mit der USB-Kamera Fotos gemacht werden, die übers Webinterface der App auch heruntergeladen werden können.