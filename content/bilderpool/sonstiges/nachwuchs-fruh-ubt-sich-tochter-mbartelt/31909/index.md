---
layout: "image"
title: "Nachwuchs 1"
date: "2011-09-23T20:11:38"
picture: "nachwuchs1.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31909
- /details9f2b.html
imported:
- "2019"
_4images_image_id: "31909"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-23T20:11:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31909 -->
Hier wird est mal geschaut was der Papa in der Hand hat.