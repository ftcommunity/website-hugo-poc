---
layout: "image"
title: "Nachwuchs 4"
date: "2011-09-23T20:11:38"
picture: "nachwuchs4.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31912
- /details52e5.html
imported:
- "2019"
_4images_image_id: "31912"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-23T20:11:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31912 -->
Und was ist da da oben ?