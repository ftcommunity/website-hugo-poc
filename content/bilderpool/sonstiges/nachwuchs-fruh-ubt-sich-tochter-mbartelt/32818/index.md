---
layout: "image"
title: "Preisausschreiben - Gewinn 4"
date: "2011-09-27T19:44:27"
picture: "helena4.jpg"
weight: "9"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/32818
- /detailsd811.html
imported:
- "2019"
_4images_image_id: "32818"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-27T19:44:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32818 -->
Selbst unsere Hundedame inspiziert das Modell.