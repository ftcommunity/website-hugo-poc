---
layout: "comment"
hidden: true
title: "13159"
date: "2011-01-12T15:09:56"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Im aktuellen Kasten "Technical Revolutions" muss der neue Magnet Magnet 15*15*3 passgenau mittels eines Klebestreifens auf eine Bauplatte 15*15 geklebt werden. Und wisst Ihr was? Ich habe mir wieder eine Art Schablone nach diesem Vorbild hier gebaut, und der Magnet sitzt superexakt af der Platte! ;o)

Wirklich sinnvoll, so etwas zu basteln!

Gruß, Thomas