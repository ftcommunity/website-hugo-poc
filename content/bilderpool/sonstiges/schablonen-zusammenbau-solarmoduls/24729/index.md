---
layout: "image"
title: "Schablone 07"
date: "2009-08-09T23:39:21"
picture: "klebe7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24729
- /detailsc886.html
imported:
- "2019"
_4images_image_id: "24729"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24729 -->
Die Abbildung zeigt die fertige Klebeschablone.

Nach dem Entfernen der beiden schmalen Abdeckstreifen
des Doppel-Klebestreifens ist die Klebeschablone bereit 
zur Aufnahme des Solarmodules.