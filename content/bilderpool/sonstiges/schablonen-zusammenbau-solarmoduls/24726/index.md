---
layout: "image"
title: "Schablone 04"
date: "2009-08-09T23:39:21"
picture: "klebe4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24726
- /detailse5d0.html
imported:
- "2019"
_4images_image_id: "24726"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24726 -->
Auf die Bausteine 5 werden die Bauplatten 15x60 mit 4 Zapfen geschoben.

Die Bausteine 5 erleichtern des spätere Abschieben des fertigen, 
beklebten Solarmudules.