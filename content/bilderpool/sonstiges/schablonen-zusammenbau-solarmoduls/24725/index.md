---
layout: "image"
title: "Schablone 03"
date: "2009-08-09T23:39:21"
picture: "klebe3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/24725
- /detailsfef0.html
imported:
- "2019"
_4images_image_id: "24725"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24725 -->
Jeweils zwei Bausteine 30 werden zusammengefügt und mit 4 Bausteinen 5
bestückt, wie es dem Bild zu entnehmen ist.