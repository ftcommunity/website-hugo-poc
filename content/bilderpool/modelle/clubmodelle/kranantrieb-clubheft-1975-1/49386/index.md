---
layout: "image"
title: "Drehantrieb"
date: 2022-01-15T13:55:34+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-106.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die beiden Achsen für den Seilantrieb (die gleichzeitig die Drehachse des Krans ist) und für die Drehung des Innenzahnrads.