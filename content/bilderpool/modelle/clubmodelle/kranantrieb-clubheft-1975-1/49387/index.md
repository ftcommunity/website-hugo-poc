---
layout: "image"
title: "Schwenken des Arms"
date: 2022-01-15T13:55:35+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-105.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Kran selbst sitzt auf der Drehscheibe, die wiederum ein Innenzahnrad trägt. Die lange senkrechte Achse ist sowohl die Drehachse des Kranarms (angetrieben von einem Z10 auf der Unterseite des Innenzahnrads) als auch die Antriebsachse der Seilrolle des Krans. Deshalb sitzt die Drehscheibe auf einer Freilaufnabe.