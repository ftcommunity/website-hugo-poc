---
layout: "image"
title: "Rückseite"
date: 2022-01-15T13:55:36+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-104.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dann ist dies hier die Rückseite. Die nach rechts herausstehende Achse ist ein Hebel, mit dem man den Block von Z15 anheben kann.