---
layout: "image"
title: "Konstruktionsänderung"
date: 2022-01-15T13:55:40+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-113.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Insgesamt finde ich die Konstruktion so - na ja. Die beiden Federgelenksteine für den Hebel würden an normalen Bausteinen dazwischen schleifen und deshalb klemmen. Vielleicht war das aber auch Absicht. Ich habe die Steine durch drei Bausteine 7,5 ersetzt. Die Mechanik muss gut justiert werden, damit alles sauber fluchtet und flutscht. Da ist auch die Lagerung der senkrechten Achsen ein Problem.

Man könnte mal versuchen, sowas auf aktuellem Stand besser zu bauen. Wie im Clubheft abgebildet, bietet das Modell wohl reichlich Spielraum zum Tüfteln, bis es wirklich funktioniert, und Ansporn zur Verbesserung - was ja durchaus in bester Lehrabsicht gewollt gewesen sein kann!