---
layout: "image"
title: "Wirkungsweise"
date: 2022-01-15T13:55:32+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-108.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Es gibt nur einen Motor. Der treibt das Paket von stapelbaren Z15 (die mit zusätzlicher feiner Verzahnung) an. Dieses Paket kann per Hebelkraft nach oben oder per Federkraft zurück nach unten gedrückt werden.

Steht das Paket unten, greift die Sperrklinke wie hier gezeigt ins Z10. Das Kranseil ist somit blockiert und kann nicht auf noch ab. Dafür wird das Z20 unten gedreht und bewirkt eine Drehung des Krans.

Betätigt man den Hebel, wird das Z15-Paket angehoben. Dadurch wird die Sperrklinke oben vom Z10 wegbewegt, das dann vom hoch stehenden Z15-Block angetrieben wird - der Motor betätigt nun das Kranseil. Dafür hebt sich die Sperrklinke unten (der herausstehende Verbinder) und blockiert das Z20, sodass sich der Kran selbst nicht unerwünscht dreht.