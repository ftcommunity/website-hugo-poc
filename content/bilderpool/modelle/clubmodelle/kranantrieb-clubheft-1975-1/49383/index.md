---
layout: "image"
title: "Mechanik in abgesenktem Zustand"
date: 2022-01-15T13:55:30+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-109.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Z15-Paket ist unten und greift ins Dreh-Z20 ein. Das Seil-Z10 ist blockiert.