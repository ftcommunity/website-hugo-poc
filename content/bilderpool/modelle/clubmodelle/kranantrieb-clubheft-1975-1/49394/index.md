---
layout: "image"
title: "Mechanik in angehobenem Zustand"
date: 2022-01-15T13:55:43+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-110.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Hebel ist gedrückt. Da Z15-Paket sitzt nun oben und greift ins Seil-Z10 ein. Die Sperrklinke des Z10 ist nach hinten oben weggedrückt. Das Dreh-Z20 unten ist vom ebenfalls mit angehobenem Verbinder blockiert.