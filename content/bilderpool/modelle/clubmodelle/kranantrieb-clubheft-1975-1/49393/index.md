---
layout: "image"
title: "Hebelmechanik"
date: 2022-01-15T13:55:42+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-111.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der von rechts ankommende Hebel hat einen Verbinder, der unterhalb der Z15 endet. Damit kann der Z15-Block mitsamt des Federgelenksteins darüber angehoben werden. Außer dass die Z15 dann anstatt ins Dreh-Z20 ins Seil-Z10 eingreifen, wird beim Anheben die Metallachse, die links im Federgelenkbaustein steckt, durch die schräg angebrachten Bausteine nach außen gedrückt. Das bewirkt, dass die Sperrklinke aus dem Z10 ausgeklinkt wird.

Sofern nichts verkantet ist, drückt die Spiralfeder oben das Ganze wieder herunter, wenn man den Hebel loslässt.