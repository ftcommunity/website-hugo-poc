---
layout: "image"
title: "Das Original"
date: 2022-01-15T13:55:45+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-101.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Modell aus dem Clubheft 1975-1 wollte ich schon immer mal verstehen und baute es also nach.