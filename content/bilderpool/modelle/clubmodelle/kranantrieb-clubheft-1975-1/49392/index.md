---
layout: "image"
title: "Rückseite bei gedrücktem Hebel"
date: 2022-01-15T13:55:41+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-112.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hebel gedrückt, Z15 angehoben, Sperrklinke rausgedrückt.