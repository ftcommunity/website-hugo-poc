---
layout: "image"
title: "Frontseite"
date: 2022-01-15T13:55:37+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-103.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese Seite nenne ich mal die Front.