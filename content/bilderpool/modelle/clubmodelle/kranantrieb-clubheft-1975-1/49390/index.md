---
layout: "image"
title: "Der Nachbau"
date: 2022-01-15T13:55:39+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-102.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Nachbau richtet sich nach der Bauanleitung und den Bildern, soweit man alles erkennen konnte. An wenigen Stellen musste ich etwas ändern, damit's nicht klemmt.