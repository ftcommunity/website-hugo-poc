---
layout: "image"
title: "Seilrollenantrieb"
date: 2022-01-15T13:55:33+01:00
picture: "2022-01-07_Kranantrieb_aus_dem_Clubheft_1975-107.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die lange Achse kommt also von unten und dreht am Z40, was das Anheben oder Absenken des Kranseils bewirkt.