---
layout: "image"
title: "Heck"
date: "2009-04-13T11:49:11"
picture: "trikemitanhaenger2.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23689
- /detailsb118.html
imported:
- "2019"
_4images_image_id: "23689"
_4images_cat_id: "1635"
_4images_user_id: "845"
_4images_image_date: "2009-04-13T11:49:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23689 -->
An dem Gelenk-Baustein wird der Anhänger drangehängt.