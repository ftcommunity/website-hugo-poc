---
layout: "image"
title: "Lenker"
date: "2009-04-13T11:49:11"
picture: "trikemitanhaenger3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23690
- /detailsfe48.html
imported:
- "2019"
_4images_image_id: "23690"
_4images_cat_id: "1635"
_4images_user_id: "845"
_4images_image_date: "2009-04-13T11:49:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23690 -->
Das Trike lenkt mir einem Gelenk-Baustein.