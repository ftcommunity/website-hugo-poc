---
layout: "image"
title: "Gesamtansicht"
date: "2009-04-13T11:49:11"
picture: "trikemitanhaenger1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23688
- /detailse8c7.html
imported:
- "2019"
_4images_image_id: "23688"
_4images_cat_id: "1635"
_4images_user_id: "845"
_4images_image_date: "2009-04-13T11:49:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23688 -->
