---
layout: "image"
title: "Das was mann nicht in die bauanleitung findet..."
date: "2016-07-14T18:10:27"
picture: "image_15.jpeg"
weight: "13"
konstrukteure: 
- "Fred Spies"
fotografen:
- "Fred Spies"
uploadBy: "Fredspies"
license: "unknown"
legacy_id:
- /php/details/43893
- /details8abd-2.html
imported:
- "2019"
_4images_image_id: "43893"
_4images_cat_id: "3250"
_4images_user_id: "2620"
_4images_image_date: "2016-07-14T18:10:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43893 -->
