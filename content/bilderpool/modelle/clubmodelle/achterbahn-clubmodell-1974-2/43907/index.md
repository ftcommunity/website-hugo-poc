---
layout: "image"
title: "Aus der sendung 'De gouden jaren 1974' von 'Ons'"
date: "2016-07-14T18:10:27"
picture: "image_29.jpeg"
weight: "9"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "fischertechnik GmbH"
uploadBy: "Fredspies"
license: "unknown"
legacy_id:
- /php/details/43907
- /detailsa329.html
imported:
- "2019"
_4images_image_id: "43907"
_4images_cat_id: "3253"
_4images_user_id: "2620"
_4images_image_date: "2016-07-14T18:10:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43907 -->
http://www.kijkbijons.nl/de-gouden-jaren-1974/tvgemist/item?b8wNqLf07xD4Ci3tuI33HA==
