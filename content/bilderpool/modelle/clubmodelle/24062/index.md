---
layout: "image"
title: "Magnet Motoren"
date: "2009-05-21T15:50:23"
picture: "magnet1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/24062
- /details0373.html
imported:
- "2019"
_4images_image_id: "24062"
_4images_cat_id: "1617"
_4images_user_id: "453"
_4images_image_date: "2009-05-21T15:50:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24062 -->
Links ist ein ganz normaler 50 Hertz Motor zu sehen.
Auf der rechten Seite ein Magnet Motor, er wird über den Schleifring, mit hilfe des Relais in der Mitte gesteuert. Der  Schaltstab im Hintergrund wird über eine Lichtschranke ausgelöst und lässt das Zählwerk zählen, wie viele Umdrehungen der Motor gemacht hat.

Der Rechte Motor ist aus einem Clubheft, der Linke ist ein Nachbau, wie man ihn schon öfters auf der ftc findet kann.
