---
layout: "image"
title: "Ausgabe"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus9.jpg"
weight: "9"
konstrukteure: 
- "Frank Hanke (franky)"
fotografen:
- "franky"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13118
- /detailse558.html
imported:
- "2019"
_4images_image_id: "13118"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13118 -->
Hier wurde eine Packung ausgeworfen.
