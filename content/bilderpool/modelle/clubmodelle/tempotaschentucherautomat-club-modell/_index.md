---
layout: "overview"
title: "Tempotaschentücher-automat"
date: 2020-02-22T08:39:38+01:00
legacy_id:
- /php/categories/1188
- /categories1e9b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1188 --> 
### Tempotaschentücherautomat (Club-Modell aus 2004)

Zunächst zeigt die grüne LED/Lampe an, dass sich der Automat im Status \"bereit\" befindet. Wirft man nun eine Münze ein, wechselt der Status nach \"in Betrieb\" (rote LED/Lampe), der Schieber geht nach vorne und schmeißt eine Tempopackung aus. Anschließend fährt der Schieber wieder zurück und der Status springt wieder auf \"bereit\". Mein Sohn (5) findet den Automaten total toll. Er hat sogar rausgefunden, dass man die Maschine mit einem Papierstreifen betuppen kann, sprich Tempopackung ohne Münze.
