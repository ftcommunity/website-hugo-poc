---
layout: "image"
title: "Münzeinwurf"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus4.jpg"
weight: "4"
konstrukteure: 
- "Frank Hanke (franky)"
fotografen:
- "franky"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13113
- /details1753.html
imported:
- "2019"
_4images_image_id: "13113"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13113 -->
hier wirft man die Münze rein. Eine Echheitsprüfung findet nicht statt. Zur Not tut es auch ein Knopf.
