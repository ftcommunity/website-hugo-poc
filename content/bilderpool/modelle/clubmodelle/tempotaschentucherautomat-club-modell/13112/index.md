---
layout: "image"
title: "Antrieb (Vogeperspektive)"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus3.jpg"
weight: "3"
konstrukteure: 
- "Frank Hanke (franky)"
fotografen:
- "franky"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/13112
- /details4654.html
imported:
- "2019"
_4images_image_id: "13112"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13112 -->
Der Antrieb von oben
