---
layout: "image"
title: "Clubmodell"
date: "2010-04-04T17:28:01"
picture: "clubmodell1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26871
- /details16b8.html
imported:
- "2019"
_4images_image_id: "26871"
_4images_cat_id: "1924"
_4images_user_id: "453"
_4images_image_date: "2010-04-04T17:28:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26871 -->
Nachbau eines Clubmodells http://www.fischertechnik-museum.ch/doc/FanClub/Club_10_1971.pdf
Ab Seite 19
