---
layout: "image"
title: "Vorschub"
date: "2012-02-23T21:35:28"
picture: "kopiereri12.jpg"
weight: "14"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34390
- /details61de-2.html
imported:
- "2019"
_4images_image_id: "34390"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2012-02-23T21:35:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34390 -->
