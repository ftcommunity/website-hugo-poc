---
layout: "image"
title: "Stift"
date: "2012-02-23T21:35:27"
picture: "kopiereri03.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34381
- /detailseb67.html
imported:
- "2019"
_4images_image_id: "34381"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2012-02-23T21:35:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34381 -->
