---
layout: "overview"
title: "Clubmodell 04/1975"
date: 2020-02-22T08:39:46+01:00
legacy_id:
- /php/categories/2002
- /categoriesd702.html
- /categories7ee2.html
- /categories3423.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2002 --> 
Nachbau eines Clubmodells http://www.fischertechnik-museum.ch/doc/FanClub/Club_25_1975.pdf
Ab Seite 7