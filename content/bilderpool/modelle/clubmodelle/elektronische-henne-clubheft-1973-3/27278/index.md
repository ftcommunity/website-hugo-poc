---
layout: "image"
title: "3/4-Ansicht"
date: "2010-05-16T21:24:02"
picture: "elektronischehenne09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
- "Volker-James Münchhof (qincym)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27278
- /details1edb.html
imported:
- "2019"
_4images_image_id: "27278"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:02"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27278 -->
fischergeometric hat ein 10-mm-Raster, was sich mit Bausteinen 5 auch ganz gut in fischertechnik einklemmen lässt. Bausteine 5 gab es 1973 noch nicht, aber so ist die Geschichte weniger wackelig.
