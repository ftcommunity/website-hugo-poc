---
layout: "image"
title: "Taster und Schalter"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46825
- /detailsc720.html
imported:
- "2019"
_4images_image_id: "46825"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46825 -->
Der linke Taster stößt die Talfahrt an (und sonst nichts), der rechte die Bergfahrt (und sonst nichts).

Der Stufenschalter dient zur Überbrückung keines, eines bestimmten oder beider Taster für einen Endlosbetrieb (das ist eine Ergänzung von mir, die im Originalfoto nicht vorhanden war).
