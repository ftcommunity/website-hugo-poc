---
layout: "image"
title: "Maschinenhaus"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46820
- /detailsd5e0.html
imported:
- "2019"
_4images_image_id: "46820"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46820 -->
Der Kern des Modells befindet sich hier: Ein recht ungewöhnlicher Antrieb über eine aufgewickelte Kette - das hatte ich sonst noch nirgends bei ft gesehen, eine Elektronik mit nichts außer Gleichrichter und 2 h4-Relais, und zwei Taster. Mehr war nicht. Den Stufenschalter habe ich ergänzt.
