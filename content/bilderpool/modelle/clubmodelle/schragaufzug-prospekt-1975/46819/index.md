---
layout: "image"
title: "Lichtschranke am Fuß"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46819
- /details9614.html
imported:
- "2019"
_4images_image_id: "46819"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46819 -->
Oben wie unten gibt es je eine Lichtschranke. Ich habe noch Blink-LEDs als Warnlichter ergänzt.
