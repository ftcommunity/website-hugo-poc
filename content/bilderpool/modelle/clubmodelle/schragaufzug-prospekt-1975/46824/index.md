---
layout: "image"
title: "Elektronik"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46824
- /details4103.html
imported:
- "2019"
_4images_image_id: "46824"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46824 -->
Über die Schaltung konnten wir ja nur mutmaßen, aber diese hier hat zumindest die Steckerbelegung der h4-Silberlinge passend zum Originalfoto.

Ich habe folgende Schaltung gewählt: Jedes Relais wird von einer der beiden Endlagen-Lichtschranken angesteuert, fällt also ab, wenn diese unterbrochen ist. Die Relais haben eine Selbsthalteschaltung (untere Anschlüsse). Die oberen Anschlüsse der Relais polen je einen Anschluss des Motors zwischen + (angezogen) und - (abgefallen) um. Sind beide Relais also abgefallen oder beide angezogen, steht der Motor (weil er auf beiden Anschlüssen + bzw. auf beiden - bekommt). Ist nur eines angezogen, dreht er sich, und die Drehrichtung wird dadurch bestimmt, welches der beiden Relais angezogen wird.

Das Relais, bei dessen Anzug der Wagen hochfährt, wird durch die obere Lichtschranke
