---
layout: "comment"
hidden: true
title: "23738"
date: "2017-10-23T11:46:44"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ob Linsenlampen mehr Strom ziehen (höhere Leistung haben), weiß ich nicht. Der Gedanke mit der Belastung ist nicht verkehrt. Sie bündeln das Licht allerdings stärker und belichten damit den LDR deutlich besser. Der LDR wird niederohmiger und läßt mehr Steuerstrom vom "E" nach GND fließen. Der Innenwiderstand im Silberling hat 3300 Ohm und begrenzt den Steuerstrom ordentlich. An der Stelle mußt Du Dir keine Gedanken machen.

Du kannst ja mal die Widerstände der LDRs messen: Belichtet, beschattet. Und zum Vergleich mit Linsenlampen.