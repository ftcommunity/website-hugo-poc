---
layout: "image"
title: "Verdrahtungsplan"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46827
- /details0b03.html
imported:
- "2019"
_4images_image_id: "46827"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46827 -->
So hatte ich das Modell auf der Convention verdrahtet. Man kann Steckerbelegungen erreichen, die, soweit ich das erkennen konnte, mit den auf dem Originalfoto übereinstimmen.
