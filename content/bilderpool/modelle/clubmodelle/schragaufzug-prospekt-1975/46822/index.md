---
layout: "image"
title: "Kettenantrieb (2)"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46822
- /details8ae6.html
imported:
- "2019"
_4images_image_id: "46822"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46822 -->
Hier ein Detailblick auf den Antrieb. Die beiden senkrecht stehenden Achsen sind auch meine Ergänzung, damit sich die Kette gleichmäßiger aufwickelt.
