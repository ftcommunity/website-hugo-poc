---
layout: "image"
title: "Gesamtansicht"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46818
- /detailse9ca.html
imported:
- "2019"
_4images_image_id: "46818"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46818 -->
In diesem ft-Katalog von 1975 https://ft-datenbank.de/details.php?ArticleVariantId=41a702c9-38f5-4617-8456-365d9c261403 auf Seite 15 und diesem von 1977 https://ft-datenbank.de/details.php?ArticleVariantId=c1c0a835-e1a0-448c-a432-75bd023c6bd8 auf Seite 13 findet sich das Original dieses Modells. Die Forumsanfrage in https://forum.ftcommunity.de/viewtopic.php?f=6&t=4367, in der die Frage nach der vermutlichen elektrischen Schaltung gestellt wurde, brachte mich dazu, das Modell ebenfalls nachzubauen. Auf der Convention 2017 stand das Modell; dies hier ist die etwas genauere Dokumentation.
