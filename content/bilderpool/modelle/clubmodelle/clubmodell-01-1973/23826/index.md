---
layout: "image"
title: "Elektronisch gesteuerte Uhr"
date: "2009-04-29T17:24:19"
picture: "clubmodell4.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23826
- /details7b6a.html
imported:
- "2019"
_4images_image_id: "23826"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23826 -->
Uhrwerk von rechter Seite betrachtet.