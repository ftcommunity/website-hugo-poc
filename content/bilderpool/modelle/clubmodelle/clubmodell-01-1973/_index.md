---
layout: "overview"
title: "Clubmodell 01/1973"
date: 2020-02-22T08:39:40+01:00
legacy_id:
- /php/categories/1628
- /categories1186.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1628 --> 
Im Clubmagazin 01/1973 wird eine elektronisch gesteuerte Uhr beschrieben. Bei der Schaltung habe ich allerdings den 22 KOhm Widerstand durch eine Drahtbrücke ersetzt und einen 2200 uF/16V Elko genommen. Seit der Justage des Taktebers läuft die Uhr nun seit 2 Stunden ohne Abweichung. Wollen wir mal sehen wie es mit der Genauigkeit weitergeht.