---
layout: "image"
title: "Elektronisch gesteuerte Uhr"
date: "2009-04-29T17:24:20"
picture: "clubmodell5.jpg"
weight: "5"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23827
- /details54a0.html
imported:
- "2019"
_4images_image_id: "23827"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23827 -->
Uhrwerk von linker Seite betrachtet.