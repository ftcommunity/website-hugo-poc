---
layout: "image"
title: "Blick ins Innere"
date: 2020-05-09T17:28:13+02:00
picture: "Spielautomat von 1975 4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht den Trafo, die beiden Motoren für die Räder mitsamt den Schleifringen mit je einem Unterbrecher, die die Gewinn-Taster drücken, sowie unter dem Einwurfschlitz die Lichtschranke und den Münzvorrats-Turm.
