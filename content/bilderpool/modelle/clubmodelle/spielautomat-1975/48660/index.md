---
layout: "image"
title: "Die Auswurfmechanik"
date: 2020-05-09T17:28:12+02:00
picture: "Spielautomat von 1975 5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Mini-Mot mit Hubgetriebe schiebt bei einem Vor-/Zurück-Hub genau eine Münze heraus. Er verfügt über zwei Endlagen-Taster. Das muss sehr genau justiert werden, damit nichts klemmt und nicht mehr als eine Münze pro Hub ausgeworfen wird.
