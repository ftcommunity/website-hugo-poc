---
layout: "image"
title: "Clubmodell"
date: "2010-04-02T23:27:45"
picture: "clubmodell4.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26864
- /details112f.html
imported:
- "2019"
_4images_image_id: "26864"
_4images_cat_id: "1923"
_4images_user_id: "453"
_4images_image_date: "2010-04-02T23:27:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26864 -->
