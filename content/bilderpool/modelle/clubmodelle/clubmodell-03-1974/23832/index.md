---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:15"
picture: "clubmodell3.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23832
- /detailsa1aa.html
imported:
- "2019"
_4images_image_id: "23832"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23832 -->
Radarschirm