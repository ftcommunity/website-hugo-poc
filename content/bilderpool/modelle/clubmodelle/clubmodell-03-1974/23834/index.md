---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:15"
picture: "clubmodell5.jpg"
weight: "5"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23834
- /detailsbb86.html
imported:
- "2019"
_4images_image_id: "23834"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23834 -->
Seitenansicht; inklusive kleiner Erfrischung und dem Modell Fire Fighter.