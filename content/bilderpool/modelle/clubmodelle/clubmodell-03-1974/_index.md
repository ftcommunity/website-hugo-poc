---
layout: "overview"
title: "Clubmodell 03/1974"
date: 2020-02-22T08:39:41+01:00
legacy_id:
- /php/categories/1630
- /categories09da.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1630 --> 
Dies ist das Radargerät aus dem Clubhelft 03/1974. Im Gegesatz zu einem richtigen Radargerät reagiert es auf Licht. Dieses Modell zeigt aber prima die Wirkungsweise.