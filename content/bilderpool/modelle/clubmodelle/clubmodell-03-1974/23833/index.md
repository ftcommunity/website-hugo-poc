---
layout: "image"
title: "Radar"
date: "2009-05-04T09:37:15"
picture: "clubmodell4.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23833
- /details44fa.html
imported:
- "2019"
_4images_image_id: "23833"
_4images_cat_id: "1630"
_4images_user_id: "936"
_4images_image_date: "2009-05-04T09:37:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23833 -->
Radarschirm; nun ohne Blitz, man sieht die Lampe mit der grünen Leuchtkappe besser.