---
layout: "image"
title: "Iphone 5 Halterung 2"
date: "2013-09-03T22:01:42"
picture: "IMG_2453.jpg"
weight: "2"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
schlagworte: ["Iphone", "5", "Stativ", "Bennik", "Halterung"]
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- /php/details/37297
- /detailsbd0c.html
imported:
- "2019"
_4images_image_id: "37297"
_4images_cat_id: "2776"
_4images_user_id: "1549"
_4images_image_date: "2013-09-03T22:01:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37297 -->
Halterung fürs Iphone 5