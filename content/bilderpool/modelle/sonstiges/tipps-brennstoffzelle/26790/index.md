---
layout: "image"
title: "Batteriehalter"
date: "2010-03-21T18:38:02"
picture: "brennstoffzelle1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26790
- /detailsfdf4.html
imported:
- "2019"
_4images_image_id: "26790"
_4images_cat_id: "1912"
_4images_user_id: "453"
_4images_image_date: "2010-03-21T18:38:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26790 -->
Batteriehalter für eine 1,5 V Zelle. Der Strom kann an dem Lampenbaustein abgenommen werden.
