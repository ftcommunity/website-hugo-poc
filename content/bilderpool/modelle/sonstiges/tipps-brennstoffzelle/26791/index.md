---
layout: "image"
title: "Batteriehalter und Brennstoffzelle"
date: "2010-03-21T18:38:02"
picture: "brennstoffzelle2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/26791
- /detailsa876.html
imported:
- "2019"
_4images_image_id: "26791"
_4images_cat_id: "1912"
_4images_user_id: "453"
_4images_image_date: "2010-03-21T18:38:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26791 -->
Hier nutze ich den Batteriehalter um die Brennstoffzelle auch ohne Sonne aufzuladen, sprich Wasserstoff zu produzieren.
Beide Solarmotoren sind im Versuchsaufbau vorhanden.
