---
layout: "image"
title: "Plakathalter aufgeklappt"
date: "2011-11-05T20:17:25"
picture: "coneb3.jpg"
weight: "3"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/33421
- /detailsf1fe.html
imported:
- "2019"
_4images_image_id: "33421"
_4images_cat_id: "2476"
_4images_user_id: "430"
_4images_image_date: "2011-11-05T20:17:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33421 -->
Und hier der Plakathalter komplett aufgeklappt.