---
layout: "image"
title: "ft Medal"
date: "2010-03-03T20:05:24"
picture: "ft-medal.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "Medal"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26577
- /details367b.html
imported:
- "2019"
_4images_image_id: "26577"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-03T20:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26577 -->
Experimenting with medal designs