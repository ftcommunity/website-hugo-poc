---
layout: "image"
title: "Trophy"
date: "2010-03-02T21:52:19"
picture: "ft-trophy2.jpg"
weight: "3"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Trophy"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26574
- /detailsa04a.html
imported:
- "2019"
_4images_image_id: "26574"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-02T21:52:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26574 -->
My trophy model -version 1