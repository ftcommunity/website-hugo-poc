---
layout: "image"
title: "Thunderbird"
date: "2008-07-13T16:36:15"
picture: "ft-thunderbird.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Yellow", "Thunderbird"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14814
- /details47d3-2.html
imported:
- "2019"
_4images_image_id: "14814"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14814 -->
I am stilling fiddling with the yellow blocks.