---
layout: "image"
title: "Lagerung weiterer Bahnen"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46778
- /detailsc316.html
imported:
- "2019"
_4images_image_id: "46778"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46778 -->
Das hier dient dazu, die anderen Bahnen ordentlich aufzubewahren.
