---
layout: "overview"
title: "Kompakter Autotrainer"
date: 2020-02-22T08:32:22+01:00
legacy_id:
- /php/categories/3463
- /categoriesf955.html
- /categories9807.html
- /categories17a8.html
- /categoriesb1e5.html
- /categoriesa67c.html
- /categoriesfd30-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3463 --> 
Ein kompakter Fahrsimulator nach einer Idee aus dem Clubheft 1972-4