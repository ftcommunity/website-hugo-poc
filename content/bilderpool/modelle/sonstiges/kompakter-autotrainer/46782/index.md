---
layout: "image"
title: "Antrieb"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46782
- /details3e37-2.html
imported:
- "2019"
_4images_image_id: "46782"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46782 -->
Der kleine XS-Motor genügt. Über die beiden Reibräder treibt er die großen Reifen an, die die Achse tragen, auf denen die Bahn aufgewickelt ist.
