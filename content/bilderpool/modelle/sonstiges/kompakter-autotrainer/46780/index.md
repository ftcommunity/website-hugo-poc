---
layout: "image"
title: "Aufbau der Wickelachsen"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46780
- /details7fbb.html
imported:
- "2019"
_4images_image_id: "46780"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46780 -->
Je drei Platten 15x60 halten die BS30 zusammen, mit den Clipsplatten (Ur-ft aus den 1960er/1970er Jahren, gab's auch mit zwei kleineren anstatt einem durchgehenden Clip) werden die Enden der Bahn eingeklemmt.
