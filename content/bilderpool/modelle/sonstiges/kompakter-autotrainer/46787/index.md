---
layout: "image"
title: "Fahrbahnbeleuchtung"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46787
- /detailsd65a.html
imported:
- "2019"
_4images_image_id: "46787"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46787 -->
Die LED auf dem beweglichen Träger leuchtet von unten durchs Papier.
