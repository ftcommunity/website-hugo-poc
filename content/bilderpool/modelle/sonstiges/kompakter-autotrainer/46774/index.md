---
layout: "image"
title: "Rechte Seite"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46774
- /details4afd.html
imported:
- "2019"
_4images_image_id: "46774"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46774 -->
Diese Variante sollte kompakter und ohne eine Grundplatte gebaut werden. Außerdem wollte ich das Zurückspulen der Bahn zwar immer noch manuell lassen (damals hatten wir natürlich auch mit einem zweiten Motor dafür gesorgt), aber es sollte, genauso wie der Fahrbahnwechsel, erleichtert werden.
