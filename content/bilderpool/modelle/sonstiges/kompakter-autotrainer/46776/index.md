---
layout: "image"
title: "Bahn-Tausch"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46776
- /detailsdce1.html
imported:
- "2019"
_4images_image_id: "46776"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46776 -->
Zum Tausch der Bahn gegen eine andere kann man die Fotodiode und die beiden Metallachsen hochklappen. Die Bahnen sind in standardisierten Aufnehmern gelagert.
