---
layout: "image"
title: "Helpdesk auf"
date: "2010-10-02T16:06:56"
picture: "Helpdesk2.jpg"
weight: "1"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/28801
- /details4987.html
imported:
- "2019"
_4images_image_id: "28801"
_4images_cat_id: "2098"
_4images_user_id: "1177"
_4images_image_date: "2010-10-02T16:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28801 -->
...und aufgeklappt
