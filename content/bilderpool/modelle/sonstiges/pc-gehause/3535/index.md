---
layout: "image"
title: "ft-PC 002"
date: "2005-02-08T16:21:04"
picture: "ft-PC_002.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3535
- /details9fe8.html
imported:
- "2019"
_4images_image_id: "3535"
_4images_cat_id: "324"
_4images_user_id: "5"
_4images_image_date: "2005-02-08T16:21:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3535 -->
Das Netzteil ist links an der Seite angebracht, da es sonst nirgends mehr hingepasst hätte.