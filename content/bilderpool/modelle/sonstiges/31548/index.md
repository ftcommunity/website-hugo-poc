---
layout: "image"
title: "Chefsessel40"
date: "2011-08-08T21:07:30"
picture: "Chefsessel40.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31548
- /details3625.html
imported:
- "2019"
_4images_image_id: "31548"
_4images_cat_id: "323"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:07:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31548 -->
Das ist der Sessel vom Chef. In edlem Schwarz bezogen, mit elastischen Elementen (das sind die weichen Kettenglieder), Armlehnen und gefederter Rückenlehne.
