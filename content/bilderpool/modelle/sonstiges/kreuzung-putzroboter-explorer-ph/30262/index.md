---
layout: "image"
title: "Gesamtansicht"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph01.jpg"
weight: "1"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30262
- /details9e24.html
imported:
- "2019"
_4images_image_id: "30262"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30262 -->
Eine kreuzung von Explorer und Putzmaschiene
2 TX
1 IF
2 EX 0/1

Video:
http://www.youtube.com/watch?v=VxJp9Ky5kqQ