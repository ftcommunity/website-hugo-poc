---
layout: "image"
title: "Anzeige+Steuerpult"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph16.jpg"
weight: "16"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30277
- /details888f-3.html
imported:
- "2019"
_4images_image_id: "30277"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30277 -->
Licht,
Hupe,
Kamera und
Wischlappen