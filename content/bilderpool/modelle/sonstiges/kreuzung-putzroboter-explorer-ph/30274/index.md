---
layout: "image"
title: "Die Kamera von Oben"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph13.jpg"
weight: "13"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30274
- /details16a3.html
imported:
- "2019"
_4images_image_id: "30274"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30274 -->
Der Antrieb erfolgt auf beiden Seiten