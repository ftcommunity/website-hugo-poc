---
layout: "image"
title: "Von hinten"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph08.jpg"
weight: "8"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30269
- /detailsc42b.html
imported:
- "2019"
_4images_image_id: "30269"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30269 -->
Hier sieht man auch die rote LED