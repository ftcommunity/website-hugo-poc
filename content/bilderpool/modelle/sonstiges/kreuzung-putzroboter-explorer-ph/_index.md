---
layout: "overview"
title: "Eine Kreuzung von Putzroboter und Explorer von PH"
date: 2020-02-22T08:30:24+01:00
legacy_id:
- /php/categories/2250
- /categories9424.html
- /categoriesb41e.html
- /categories3f08.html
- /categoriesd7f3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2250 --> 
Eine kreuzung von Explorer und Putzmaschiene
Mit:

2 TX,
1 IF,
2 EX 0/1,
Joystick,
Anzeigen,
und natürlich Putzlappen.