---
layout: "image"
title: "Der Antrieb für den Wischlappen"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph07.jpg"
weight: "7"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30268
- /details7785.html
imported:
- "2019"
_4images_image_id: "30268"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30268 -->
Ein Powermotor treibt eine Welle an die den Wischlappen vor und zurück zieht.