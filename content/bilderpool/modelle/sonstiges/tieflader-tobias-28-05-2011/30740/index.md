---
layout: "image"
title: "Tieflader"
date: "2011-05-30T17:12:07"
picture: "tieflader4.jpg"
weight: "4"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30740
- /details970e.html
imported:
- "2019"
_4images_image_id: "30740"
_4images_cat_id: "2292"
_4images_user_id: "1162"
_4images_image_date: "2011-05-30T17:12:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30740 -->
Die Rampe.
