---
layout: "image"
title: "Schon wieder ein Goldenes Schaukelpferd fur fischertechnik!"
date: "2014-08-31T15:58:23"
picture: "dasgoldeneschaukelpferd1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/39325
- /detailsa409.html
imported:
- "2019"
_4images_image_id: "39325"
_4images_cat_id: "2944"
_4images_user_id: "162"
_4images_image_date: "2014-08-31T15:58:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39325 -->
nur mahl gebaut um die Goldene Platten zu verwenden die es gab beim fischertechnik fanclubtag 2013
