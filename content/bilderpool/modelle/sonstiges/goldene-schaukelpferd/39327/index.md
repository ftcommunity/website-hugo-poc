---
layout: "image"
title: "Schon wieder ein Goldenes Schaukelpferd fur fischertechnik!"
date: "2014-08-31T15:58:24"
picture: "dasgoldeneschaukelpferd3.jpg"
weight: "3"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/39327
- /detailsfe4e.html
imported:
- "2019"
_4images_image_id: "39327"
_4images_cat_id: "2944"
_4images_user_id: "162"
_4images_image_date: "2014-08-31T15:58:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39327 -->
