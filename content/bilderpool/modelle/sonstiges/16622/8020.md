---
layout: "comment"
hidden: true
title: "8020"
date: "2008-12-18T13:25:15"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Welche Werte bekommst du vom Motor(3) geliefert? Sehe leider nichts zur Bestimmung der Drehzahl sondern nur eine Messung der Spannung für den Motor? Da die Motoren auch bei gleicher Spannung nie 100% gleich schnell sind reicht das reine Messen der Spannung meiner Meinung nach nicht aus...