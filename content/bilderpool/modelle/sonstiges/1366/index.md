---
layout: "image"
title: "Expreßseilwinde"
date: "2003-09-06T09:49:27"
picture: "Expreseilwinde.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Seilwinde"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1366
- /detailsba89.html
imported:
- "2019"
_4images_image_id: "1366"
_4images_cat_id: "323"
_4images_user_id: "46"
_4images_image_date: "2003-09-06T09:49:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1366 -->
Vier Motore für Seilgeschwindigkeiten bis 1,5 m/s bei 2 N Zugkraft
