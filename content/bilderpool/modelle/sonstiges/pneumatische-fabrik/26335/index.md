---
layout: "image"
title: "Kompressor und NOTAUS"
date: "2010-02-11T18:38:50"
picture: "pnaumatischefabrik7.jpg"
weight: "7"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26335
- /detailsfd25.html
imported:
- "2019"
_4images_image_id: "26335"
_4images_cat_id: "1873"
_4images_user_id: "1057"
_4images_image_date: "2010-02-11T18:38:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26335 -->
Die Überschrift sagt alles