---
layout: "image"
title: "Zusatzförderband"
date: "2010-02-11T18:38:50"
picture: "pnaumatischefabrik8.jpg"
weight: "8"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26336
- /details6104-2.html
imported:
- "2019"
_4images_image_id: "26336"
_4images_cat_id: "1873"
_4images_user_id: "1057"
_4images_image_date: "2010-02-11T18:38:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26336 -->
Einfach weil das breite Förderband verbaut war