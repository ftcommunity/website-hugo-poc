---
layout: "image"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:28+01:00
picture: "Garagentorantrieb-02.JPG"
weight: "2"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Garagentorantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Tor geschlossen. Der Taster rechts löst die Schließbewegung aus (der gehört eigentlich in die Garage)