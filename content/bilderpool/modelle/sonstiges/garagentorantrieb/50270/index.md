---
layout: "image"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:26+01:00
picture: "Garagentorantrieb-03.JPG"
weight: "3"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Garagentorantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Ansicht von oben. Die umlaufende Kette zieht einen Schlitten an dem das Tor mit Statikstreben angelenkt ist. Der 60° Baustein betätigt die Abschaltung in den Endlagen.