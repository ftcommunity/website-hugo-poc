---
layout: "image"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:25+01:00
picture: "Garagentorantrieb-04.JPG"
weight: "4"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Garagentorantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Seitenansicht. Das Tor ist oben beidseits mit Seilrollen in den Schienen geführt