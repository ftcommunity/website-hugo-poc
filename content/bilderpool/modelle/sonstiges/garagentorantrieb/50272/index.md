---
layout: "image"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:29+01:00
picture: "Garagentorantrieb-01.JPG"
weight: "1"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Garagentorantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Vorne links der Fotowiderstand mit Lochstein als Blende - quasi Empfänger der Licht-Fernbedienung. Ohne die Gummi-Federn funktioniert der Schwingtor-Mechanismus nicht.