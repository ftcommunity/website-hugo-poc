---
layout: "image"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:22+01:00
picture: "Garagentorantrieb-06.JPG"
weight: "6"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Garagentorantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Schaltschrank. Den darf man fast nicht herzeigen - so wüst verdrahtet ist der. Da kommen die Silberlinge nicht zur Geltung.