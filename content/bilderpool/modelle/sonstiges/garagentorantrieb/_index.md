---
layout: "overview"
title: "Garagentorantrieb"
date: 2024-01-30T09:36:21+01:00
---

Zu meinem 50sten wurde ich stolzer Besitzer eines gleich alten hobby4 Kastens - endlich meine ersten Silberlinge!
Erster Einsatz: Ein Garagentorantrieb. Die Kinematik des Schwingtores interessierte mich schon länger,
jetzt die Umsetzung gleich mit Antrieb und minimal "Fernbedienung", inspiriert aus einem Hobby-Begleitheft. 

Öffnen mittels Lampe von außen, schließen per Taster.
Die Ansteuerung des Motors erfolgt zum Öffnen über den Fotowiderstand und Relais mit Vorverstärker und Selbsthaltung.
Schließen über einfaches, selbsthaltendes Relais, angesteuert mit Taster.
Abschaltung durch Taster an jeder Endlage.
