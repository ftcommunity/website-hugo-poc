---
layout: "image"
title: "Überwachungsvehikel"
date: "2016-10-30T20:05:42"
picture: "DSC00428_sc01.jpg"
weight: "6"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/44715
- /detailsb8de.html
imported:
- "2019"
_4images_image_id: "44715"
_4images_cat_id: "3330"
_4images_user_id: "2488"
_4images_image_date: "2016-10-30T20:05:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44715 -->
Das Kommunikations- und Überwachungsmodul
