---
layout: "image"
title: "Lösung zum Geschicklichkeitsspiel"
date: "2014-12-28T16:55:41"
picture: "loesung4.jpg"
weight: "9"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/40010
- /detailsfc70.html
imported:
- "2019"
_4images_image_id: "40010"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-28T16:55:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40010 -->
Bild 4
