---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Teil 4"
date: "2014-12-21T09:02:59"
picture: "geschicklichkeitsspiel4.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39949
- /details22ba.html
imported:
- "2019"
_4images_image_id: "39949"
_4images_cat_id: "3001"
_4images_user_id: "182"
_4images_image_date: "2014-12-21T09:02:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39949 -->
Die Aufgabe besteht darin die verbliebenen 11 Achsen zusammen auf dem Zahnrad auszubalancieren ohne das sie den Boden / Tisch berühren.
Es dürfen keine weiteren Hilfsmittel wie Kleber etc. benutzt werden.

Ich wünsche Euch viel Spaß bei der Aufgabe.
Wer die Lösung hat möge sie hier in Form eines Bildes präsentieren.

Also nun viel Spaß........

http://forum.ftcommunity.de/viewtopic.php?f=4&t=2667
