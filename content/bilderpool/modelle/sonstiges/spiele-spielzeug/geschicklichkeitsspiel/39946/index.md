---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Teil 1"
date: "2014-12-21T09:02:59"
picture: "geschicklichkeitsspiel1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/39946
- /details1e0d.html
imported:
- "2019"
_4images_image_id: "39946"
_4images_cat_id: "3001"
_4images_user_id: "182"
_4images_image_date: "2014-12-21T09:02:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39946 -->
Hallo zusammen
Ich möchte hier der ft Gemeinde für die langen Weihnachtstage mal ein Rätsel aufgeben.
Ihr benötigt dafür 12 Stück Rastachsen 90 (35066), 12 Stück Rastkegelzahnrad (35062) und 1 Stück Rad 23 (36581).
