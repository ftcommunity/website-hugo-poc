---
layout: "image"
title: "5-Würfel-Puzzle 2"
date: "2008-10-11T22:48:04"
picture: "Puzzle2.jpg"
weight: "2"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15958
- /details67da.html
imported:
- "2019"
_4images_image_id: "15958"
_4images_cat_id: "1449"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15958 -->
Mit 25 der 29 Teile kann man einen großen Würfel bauen.