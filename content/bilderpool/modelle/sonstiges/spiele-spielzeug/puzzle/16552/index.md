---
layout: "image"
title: "7-Dreiecke-Puzzle"
date: "2008-12-06T12:02:39"
picture: "7Dreiecke.jpg"
weight: "5"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
schlagworte: ["Puzzle"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/16552
- /details3093-2.html
imported:
- "2019"
_4images_image_id: "16552"
_4images_cat_id: "1449"
_4images_user_id: "832"
_4images_image_date: "2008-12-06T12:02:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16552 -->
Es gibt genau 24 Möglichkeiten 7 Dreiecke ganzseitig aneinander zu legen.
Diese lassen sich mit 72 Winkelsteinen 31010 und 96 Winkelsteinen 31918 aufbauen.
(Oder aus Pappe, wenn man gerade nicht genügend dieser Steine hat.)
Es sollte möglich sein, sie zu einem Dreieck mit der Kantenlänge 13 und einem Loch in der Mitte zusammen zu legen.
(24 x 7 ergibt 168 kleine Dreiecke und das große Dreieck würde aus 169 Dreiecken bestehen.)

Außerdem kann man sich an Rauten mit den Kantenlängen 12 x 7, 14 x 6, 21 x 4 und 28 x 3 versuchen.
Natürlich sind auch zahllose andere Formen denkbar.

Wem das zu schwer ist, der kann auch die 12 Möglichkeiten mit 6 Dreiecken basteln, und damit Figuren legen.
Allerdings funktioniert das nicht 100%ig mit Fischertechnik, da es keinen 60 Grad Winkelstein mit einem Zapfen gibt.

Auf jeden Fall sollte damit die Wartezeit auf den Weihnachtsmann (der hoffentlich viel Fischertechnik bringt) zu verkürzen sein.