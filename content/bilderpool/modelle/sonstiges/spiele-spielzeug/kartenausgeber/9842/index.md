---
layout: "image"
title: "Kartenausgeber 11"
date: "2007-03-28T15:09:58"
picture: "kartenausgeber11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9842
- /details4bc2.html
imported:
- "2019"
_4images_image_id: "9842"
_4images_cat_id: "886"
_4images_user_id: "502"
_4images_image_date: "2007-03-28T15:09:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9842 -->
Das ist der Schalter der die Reifen unten abstellt damit keine weitere Karten nach oben geschoben werden.
