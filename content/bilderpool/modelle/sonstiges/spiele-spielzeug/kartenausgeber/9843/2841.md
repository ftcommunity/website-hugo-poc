---
layout: "comment"
hidden: true
title: "2841"
date: "2007-03-28T23:16:18"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

sieht super aus! Eine wirklich gelungene Konstruktion!
Aber leiden da nicht die Karten drunter, wenn sie immer so "um die Ecke" gebogen werden?

Gruß,
Franz