---
layout: "image"
title: "Sicht von oben"
date: "2012-04-14T18:08:46"
picture: "minitischkicker04.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34795
- /detailsc2b3.html
imported:
- "2019"
_4images_image_id: "34795"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34795 -->
-