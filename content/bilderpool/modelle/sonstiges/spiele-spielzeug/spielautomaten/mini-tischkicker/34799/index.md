---
layout: "image"
title: "Griffe zum Drehen der Spieler"
date: "2012-04-14T18:08:47"
picture: "minitischkicker08.jpg"
weight: "8"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34799
- /detailse56f.html
imported:
- "2019"
_4images_image_id: "34799"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34799 -->
-