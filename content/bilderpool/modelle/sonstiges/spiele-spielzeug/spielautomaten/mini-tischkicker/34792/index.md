---
layout: "image"
title: "Gesamtansicht"
date: "2012-04-14T18:08:46"
picture: "minitischkicker01.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34792
- /detailsf374-2.html
imported:
- "2019"
_4images_image_id: "34792"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34792 -->
-