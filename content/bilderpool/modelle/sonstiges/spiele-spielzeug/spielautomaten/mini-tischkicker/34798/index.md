---
layout: "image"
title: "Auffangbehälter für den Ball"
date: "2012-04-14T18:08:47"
picture: "minitischkicker07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34798
- /detailsd4ee.html
imported:
- "2019"
_4images_image_id: "34798"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34798 -->
-