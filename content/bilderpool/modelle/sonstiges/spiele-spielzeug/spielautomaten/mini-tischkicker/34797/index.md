---
layout: "image"
title: "Der 'Ball'"
date: "2012-04-14T18:08:47"
picture: "minitischkicker06.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34797
- /details6ccb.html
imported:
- "2019"
_4images_image_id: "34797"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34797 -->
Der Ball ist eine etwa 1cm große Metallkugel.