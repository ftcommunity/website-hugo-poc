---
layout: "overview"
title: "Joystick steuert das Labyrinth"
date: 2020-02-22T08:31:54+01:00
legacy_id:
- /php/categories/3216
- /categories453c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3216 --> 
Hier kann man mit einem Joystick den Ball durch ein Labyrinth lotsen.