---
layout: "image"
title: "Labyrinth von Seite"
date: "2016-04-26T13:19:44"
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
schlagworte: ["Labyrinth"]
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/43316
- /detailsf47c.html
imported:
- "2019"
_4images_image_id: "43316"
_4images_cat_id: "3216"
_4images_user_id: "891"
_4images_image_date: "2016-04-26T13:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43316 -->
Ich habe den nicht motorisierten Teil des Labyrinths schon seit Jahren hier rumliegen. (Hab ihn seinerzeit für eine ft-Convention gebaut.) Inzwischen gefällt er mir eigentlich nicht mehr. Ich hatte jedoch von Anfang an die Idee, das Labyrinth motorisiert zu steuern. So habe ich die Motorisierung nun nachgerüstet, damit die Sache abgeschlossen ist und ich das Ganze auseinander nehmen kann. ;-) Die Joystick-Steuerung macht (so glaube ich jedenfalls) tatsächlich etwas her.