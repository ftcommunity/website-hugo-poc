---
layout: "image"
title: "Flipper PD-Holland"
date: "2012-08-10T20:35:09"
picture: "flipperpdholland06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35298
- /detailse846.html
imported:
- "2019"
_4images_image_id: "35298"
_4images_cat_id: "2616"
_4images_user_id: "22"
_4images_image_date: "2012-08-10T20:35:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35298 -->
- Rad-Kugellift +
- Schnecken-Kugellift  (= vijzel-lift)  +
- Auto-Flipper
