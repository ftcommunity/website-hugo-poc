---
layout: "image"
title: "Flipper PD-Holland"
date: "2012-08-10T20:35:09"
picture: "flipperpdholland08.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35300
- /detailsd583.html
imported:
- "2019"
_4images_image_id: "35300"
_4images_cat_id: "2616"
_4images_user_id: "22"
_4images_image_date: "2012-08-10T20:35:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35300 -->
- Rad-Kugellift +
- Schnecken-Kugellift  (= vijzel-lift)  +
- Auto-Flipper
