---
layout: "image"
title: "Flipper PD-Holland"
date: "2012-08-10T20:35:09"
picture: "flipperpdholland04.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35296
- /details4a1b.html
imported:
- "2019"
_4images_image_id: "35296"
_4images_cat_id: "2616"
_4images_user_id: "22"
_4images_image_date: "2012-08-10T20:35:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35296 -->
Score bei:

- Auto-flipper
- Farbsensor (Näherung)
- Drehflugel
