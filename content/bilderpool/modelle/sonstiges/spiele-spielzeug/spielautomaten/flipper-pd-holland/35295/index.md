---
layout: "image"
title: "Flipper PD-Holland"
date: "2012-08-10T20:35:09"
picture: "flipperpdholland03.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35295
- /details6050-2.html
imported:
- "2019"
_4images_image_id: "35295"
_4images_cat_id: "2616"
_4images_user_id: "22"
_4images_image_date: "2012-08-10T20:35:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35295 -->
Flipper funtioniert jetzt mit 2x I2C-Bus LED Display (Conrad # 198344)       +     1x LCD Anzeige (Conrad # 198330)  

LED Display- nr2 mit Jumper 2 wird im Robo Pro Programm mit Adresse 0x39 ansprochen.
