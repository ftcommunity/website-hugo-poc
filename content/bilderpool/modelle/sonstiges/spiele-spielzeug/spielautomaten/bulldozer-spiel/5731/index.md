---
layout: "image"
title: "Details"
date: "2006-02-01T14:22:07"
picture: "ein_Schacht_im_Detail_1.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5731
- /detailscebf.html
imported:
- "2019"
_4images_image_id: "5731"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5731 -->
