---
layout: "image"
title: "Ansicht (4)"
date: "2006-02-01T14:21:41"
picture: "DSCN0651.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5704
- /details3cc1.html
imported:
- "2019"
_4images_image_id: "5704"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5704 -->
Im Vordergrund erkennt man einen seitlichen Schacht.
