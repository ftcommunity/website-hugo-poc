---
layout: "image"
title: "Raupenantrieb (3)"
date: "2006-02-01T14:21:51"
picture: "Antrieb_fr_die_Raupe_2.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5716
- /details735f.html
imported:
- "2019"
_4images_image_id: "5716"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5716 -->
Die vordere Metallstange geht unter dem ganzen Modell her und treibt beide Förderbänder an.
