---
layout: "image"
title: "Ansicht (2)"
date: "2006-02-01T14:21:41"
picture: "DSCN0645.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5701
- /detailsf711.html
imported:
- "2019"
_4images_image_id: "5701"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5701 -->
Der Bulldozer schiebt die Spielsteine nach vorn. Viele fallen in die 2 seitlichen und 2 äußeren Schächte. Diese werden mit einem Förderband nach hinten transportiert.
