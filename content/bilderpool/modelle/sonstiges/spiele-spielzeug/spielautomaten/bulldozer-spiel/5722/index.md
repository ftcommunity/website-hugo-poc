---
layout: "image"
title: "Bulldozer (3)"
date: "2006-02-01T14:22:01"
picture: "DSCN0664.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5722
- /detailse892-3.html
imported:
- "2019"
_4images_image_id: "5722"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5722 -->
