---
layout: "image"
title: "Details"
date: "2006-02-01T14:21:51"
picture: "DSCN0660.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5719
- /detailsf00e.html
imported:
- "2019"
_4images_image_id: "5719"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5719 -->
Weil der blaue Pneumatikschlauch nicht flexilbel genug ist musste ich mir eine andere Lösung einfallen lassen. Ich habe Ventilgummi genutzt.
Das Spielfeld besteht aus 8 Bauplatten 500. Davon habe ich jeweils 2 Stück mittels 6-8 Federnocken zusammengesteckt. Somit hatte ich eine große Bauplatte die von beiden Seiten bebaubar ist.
Über die vier Messinghülsen gibst den "Saft". Dazu habe ich ein 4mm Messingrohr gekürzt und darin eine der Rohrnieten gesteckt.
