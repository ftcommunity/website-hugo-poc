---
layout: "image"
title: "Spielautomat Münzeinwurf"
date: "2010-04-07T12:40:32"
picture: "spielautomat3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26899
- /details648f-2.html
imported:
- "2019"
_4images_image_id: "26899"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26899 -->
Hier können echte 2 Cent Münzen eingeworfen werden.