---
layout: "overview"
title: "Spielautomat"
date: 2020-02-22T08:31:23+01:00
legacy_id:
- /php/categories/1928
- /categoriese6d1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1928 --> 
Früher hatten meine Eltern uns einen Spielautomaten für den Keller gekauft. Es war immer mein Traum ein solches Modell mal aus FT nachzubauen.