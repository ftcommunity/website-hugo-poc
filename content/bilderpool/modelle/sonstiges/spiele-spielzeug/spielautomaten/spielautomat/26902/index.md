---
layout: "image"
title: "Spielautomat Gesamtrückansicht"
date: "2010-04-07T12:40:32"
picture: "spielautomat6.jpg"
weight: "6"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26902
- /detailsfd7c.html
imported:
- "2019"
_4images_image_id: "26902"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26902 -->
