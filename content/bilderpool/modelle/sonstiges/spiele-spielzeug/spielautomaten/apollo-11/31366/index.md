---
layout: "image"
title: "P3020072"
date: "2011-07-24T16:39:18"
picture: "apollo27.jpg"
weight: "27"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31366
- /details60b5.html
imported:
- "2019"
_4images_image_id: "31366"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31366 -->
The startbutton after initialisation.