---
layout: "image"
title: "P3020041"
date: "2011-07-24T16:39:18"
picture: "apollo04.jpg"
weight: "4"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31343
- /details0061.html
imported:
- "2019"
_4images_image_id: "31343"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31343 -->
On this picture is the remote control removed.