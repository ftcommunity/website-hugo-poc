---
layout: "image"
title: "P3020047"
date: "2011-07-24T16:39:18"
picture: "apollo10.jpg"
weight: "10"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31349
- /details0818.html
imported:
- "2019"
_4images_image_id: "31349"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31349 -->
Another view of the mechanism, up and down, left and right.
