---
layout: "image"
title: "P3020055"
date: "2011-07-24T16:39:18"
picture: "apollo16.jpg"
weight: "16"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31355
- /detailsede9.html
imported:
- "2019"
_4images_image_id: "31355"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31355 -->
A closeup of the pulse generator which control the movements of the lunar lander.
