---
layout: "image"
title: "P3020045"
date: "2011-07-24T16:39:18"
picture: "apollo08.jpg"
weight: "8"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31347
- /details7f80.html
imported:
- "2019"
_4images_image_id: "31347"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31347 -->
Closeup of the single remote control.