---
layout: "image"
title: "P3020050"
date: "2011-07-24T16:39:18"
picture: "apollo12.jpg"
weight: "12"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31351
- /details4cdd-3.html
imported:
- "2019"
_4images_image_id: "31351"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31351 -->
70 km above the lunar surface.