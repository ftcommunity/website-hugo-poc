---
layout: "image"
title: "P3020065"
date: "2011-07-24T16:39:18"
picture: "apollo21.jpg"
weight: "21"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31360
- /detailsd4fd-2.html
imported:
- "2019"
_4images_image_id: "31360"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31360 -->
Closeup of the Columbia mechanics.