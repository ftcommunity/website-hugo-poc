---
layout: "image"
title: "P3020054"
date: "2011-07-24T16:39:18"
picture: "apollo15.jpg"
weight: "15"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31354
- /details3407.html
imported:
- "2019"
_4images_image_id: "31354"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31354 -->
Detail of the movement of the command module forwards and backwards.