---
layout: "image"
title: "P3020046"
date: "2011-07-24T16:39:18"
picture: "apollo09.jpg"
weight: "9"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31348
- /details4dfa.html
imported:
- "2019"
_4images_image_id: "31348"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31348 -->
View of the mechanisms for the three movements of the command module.
Left below the start button.
