---
layout: "image"
title: "P3020062"
date: "2011-07-24T16:39:18"
picture: "apollo19.jpg"
weight: "19"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31358
- /detailsbc5a.html
imported:
- "2019"
_4images_image_id: "31358"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31358 -->
A picture of the remote mounted; above the fuel / time 
indicator.
