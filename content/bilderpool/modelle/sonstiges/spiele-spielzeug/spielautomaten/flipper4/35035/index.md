---
layout: "image"
title: "Flipper4 - Bumper"
date: "2012-06-06T22:26:53"
picture: "flipper12.jpg"
weight: "12"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35035
- /details8c30-2.html
imported:
- "2019"
_4images_image_id: "35035"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35035 -->
Die Kugel stößt an die Platte und betätigt den Taster.