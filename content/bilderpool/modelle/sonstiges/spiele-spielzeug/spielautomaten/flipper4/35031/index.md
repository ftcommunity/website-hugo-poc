---
layout: "image"
title: "Flipper4 - Drehflügel"
date: "2012-06-06T22:26:53"
picture: "flipper08.jpg"
weight: "8"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35031
- /details2650.html
imported:
- "2019"
_4images_image_id: "35031"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35031 -->
Die Kugel versetzt die unteren Flügel in Drehung. Die oberen Flügel auf der selben Achse unterbrechen die Lichtschranke.