---
layout: "image"
title: "Flipper4 - Abschuss"
date: "2012-06-06T22:26:53"
picture: "flipper04.jpg"
weight: "4"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35027
- /details8229.html
imported:
- "2019"
_4images_image_id: "35027"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35027 -->
Links die beiden Flipper, unten der Taster (gelb) für den rechten Flipper, rechts der Abschuss.
