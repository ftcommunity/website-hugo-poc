---
layout: "image"
title: "Flipper4 - Schacht"
date: "2012-06-06T22:26:53"
picture: "flipper05.jpg"
weight: "5"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35028
- /details5d7e.html
imported:
- "2019"
_4images_image_id: "35028"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35028 -->
Wenn eine Kugel in den Schacht läuft, wird die Lichtschranke unterbrochen, der Zylinder wird aktiviert und die Kugel wird wieder ausgestoßen.
