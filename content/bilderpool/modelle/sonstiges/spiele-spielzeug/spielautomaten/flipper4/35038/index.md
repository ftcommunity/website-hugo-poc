---
layout: "image"
title: "Flipper4 - Hauptprogramm"
date: "2012-06-06T22:26:53"
picture: "flipper15.jpg"
weight: "15"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/35038
- /details8d32-2.html
imported:
- "2019"
_4images_image_id: "35038"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35038 -->
Die parallel laufenden Unterprogramme behandeln jeweils ein Hindernis, erhöhen die Punktzahl, schalten die Lichteffekte und steuern das Bedienfeld.