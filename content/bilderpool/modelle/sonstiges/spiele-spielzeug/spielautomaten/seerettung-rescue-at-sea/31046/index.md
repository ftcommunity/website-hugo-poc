---
layout: "image"
title: "turntable"
date: "2011-07-12T17:33:43"
picture: "turntable.jpg"
weight: "23"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31046
- /detailse93a.html
imported:
- "2019"
_4images_image_id: "31046"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T17:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31046 -->
