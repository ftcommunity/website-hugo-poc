---
layout: "image"
title: "tackel3"
date: "2011-07-12T10:37:57"
picture: "tackle3.jpg"
weight: "19"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31042
- /details822f.html
imported:
- "2019"
_4images_image_id: "31042"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T10:37:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31042 -->
