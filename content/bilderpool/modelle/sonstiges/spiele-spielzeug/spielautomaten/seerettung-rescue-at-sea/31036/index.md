---
layout: "image"
title: "overview"
date: "2011-07-12T10:37:57"
picture: "overview3.jpg"
weight: "13"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31036
- /details02c6.html
imported:
- "2019"
_4images_image_id: "31036"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T10:37:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31036 -->
