---
layout: "image"
title: "wind"
date: "2011-07-12T17:33:43"
picture: "wind.jpg"
weight: "24"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31047
- /details779b.html
imported:
- "2019"
_4images_image_id: "31047"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T17:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31047 -->
