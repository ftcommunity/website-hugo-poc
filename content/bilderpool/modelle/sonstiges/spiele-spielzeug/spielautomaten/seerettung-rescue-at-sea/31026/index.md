---
layout: "image"
title: "back2"
date: "2011-07-12T10:37:49"
picture: "back2.jpg"
weight: "3"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/31026
- /detailsfbb7.html
imported:
- "2019"
_4images_image_id: "31026"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T10:37:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31026 -->
