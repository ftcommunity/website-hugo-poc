---
layout: "image"
title: "Alternative"
date: "2013-02-17T23:43:44"
picture: "DSCN4977.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36638
- /detailsb116-2.html
imported:
- "2019"
_4images_image_id: "36638"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36638 -->
Hier kann man gut sehen wie die Propeller aneinander vorbeilaufen.
