---
layout: "image"
title: "Da sollen sie hinein."
date: "2012-05-21T17:24:21"
picture: "09-DSCN4771.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34986
- /detailsbca3.html
imported:
- "2019"
_4images_image_id: "34986"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34986 -->
Beim Test habe ich bemerkt das nur ein Ball rein passt......
Da müsste ich vielleicht noch etwas ändern.
