---
layout: "image"
title: "Ansicht von unten"
date: "2012-05-21T17:24:21"
picture: "11-DSCN4837.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34988
- /details6ab1-2.html
imported:
- "2019"
_4images_image_id: "34988"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34988 -->
Hier mal ein Blick unter die Spielfläche. Links unten und Rechts oben sieht man die
Anschlüsse der Kabelfernbedienungen. Jeweils in der Mitte der Anschlussleiste ist
die Zugentlastung zu sehen. Diese ist notwendig damit beim spielen nicht die Stecker
gezogen werden.
Leider ist beim Transport der untere Hintere Teil abgebrochen.....
Kann man gut daran erkennen das das gelbe Segment schief steht.
