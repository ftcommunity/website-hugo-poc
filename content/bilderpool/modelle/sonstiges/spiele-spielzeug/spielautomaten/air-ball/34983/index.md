---
layout: "image"
title: "Ein Ventilator - Rückansicht"
date: "2012-05-21T17:24:21"
picture: "06-DSCN4753.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34983
- /detailsae2b.html
imported:
- "2019"
_4images_image_id: "34983"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34983 -->
