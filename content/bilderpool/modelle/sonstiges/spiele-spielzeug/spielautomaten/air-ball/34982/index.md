---
layout: "image"
title: "Ein Ventilator -  Ansicht"
date: "2012-05-21T17:24:21"
picture: "05-DSCN4751.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34982
- /details198c.html
imported:
- "2019"
_4images_image_id: "34982"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34982 -->
