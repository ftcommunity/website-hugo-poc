---
layout: "image"
title: "Airball neu"
date: "2013-02-14T13:45:40"
picture: "Airball_neu.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36627
- /details0eb1-3.html
imported:
- "2019"
_4images_image_id: "36627"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36627 -->
Auch hier hat sich etwas getan.
Ich habe die roten Schienen gegen die Flexschienen getauscht.
