---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "029.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15894
- /detailsf88b-4.html
imported:
- "2019"
_4images_image_id: "15894"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15894 -->
So siehts´von unten aus.
Die drei schwarzen Bauplatten greifen in einen Spalt am Gewinn-Schacht und verhindern somit ein herausfallen der Spielsteine nach hinten.
