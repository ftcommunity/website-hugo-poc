---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:08"
picture: "010.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15875
- /detailsaa95-2.html
imported:
- "2019"
_4images_image_id: "15875"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15875 -->
erste Schritte.
Förderbänder aufgebaut, einfachen Rahmen konstruiert, Verkleidungen angebracht und Antriebsmotor eingebaut.
