---
layout: "image"
title: "Detail Rampe"
date: "2013-02-14T13:45:40"
picture: "Bausteionspender_rot_Rampe.jpg"
weight: "67"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36624
- /details5672.html
imported:
- "2019"
_4images_image_id: "36624"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36624 -->
Diese habe ich auch neu gebaut. Mit den kleinen Seilrollen läuft das super.
