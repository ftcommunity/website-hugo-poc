---
layout: "image"
title: "rechte Seite (Blick von hinten)"
date: "2013-02-14T13:45:40"
picture: "DSCN4960.jpg"
weight: "61"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36618
- /details98a5-2.html
imported:
- "2019"
_4images_image_id: "36618"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36618 -->
Hier gut zu erkennen der dreieckige Querschnitt und die Konstruktion der Kardane.
