---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "034.jpg"
weight: "34"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15899
- /details3fce-2.html
imported:
- "2019"
_4images_image_id: "15899"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15899 -->
Hier eine Förderbandkonstruktion im Detail.
Dazu habe ich Rastachsen durch die Löcher der U-Träger gesteckt und die roten Hülsen aufgeschoben.
