---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:08"
picture: "014.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15879
- /details6eb7.html
imported:
- "2019"
_4images_image_id: "15879"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15879 -->
Ein Förderband mit Antriebsmotor
