---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "026.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15891
- /details0ce7.html
imported:
- "2019"
_4images_image_id: "15891"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15891 -->
Die Arbeitsplattform liegt auf der Statikkonstruktion.
Sie ist so gebaut das sie unter die seitlich montierten geln Bauplatten 16x90 passt.
