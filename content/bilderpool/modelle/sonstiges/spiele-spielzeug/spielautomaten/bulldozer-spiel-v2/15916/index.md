---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:10"
picture: "051.jpg"
weight: "51"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15916
- /details1acb-2.html
imported:
- "2019"
_4images_image_id: "15916"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15916 -->
Anschlüsse des Bulldozers.
Luft, Antrieb, Magnetventile und Licht.
