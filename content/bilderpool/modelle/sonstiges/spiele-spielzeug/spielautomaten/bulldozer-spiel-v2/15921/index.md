---
layout: "image"
title: "Bausteinspender"
date: "2008-10-10T13:44:10"
picture: "056.jpg"
weight: "56"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15921
- /details2e7e-2.html
imported:
- "2019"
_4images_image_id: "15921"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15921 -->
Der hier setzt alles in Bewegung
