---
layout: "image"
title: "Blich auf die Rückseite"
date: "2013-02-14T13:45:40"
picture: "DSCN4963.jpg"
weight: "62"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36619
- /detailse616.html
imported:
- "2019"
_4images_image_id: "36619"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36619 -->
Hinten hat sich auch etwas getan. Hier steht nun eine Sortieranlage.
Im gelben Kasten links ist der Antrieb. Auf der rechten Seite sind der pneumatische Auswurf und die Auffangkörbe. Weiße Teile werden rausgeworfen, rote laufen durch.
Gesteuert wird das Ganze mit Silberlingen.
