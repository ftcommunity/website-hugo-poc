---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "040.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15905
- /details5045.html
imported:
- "2019"
_4images_image_id: "15905"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15905 -->
Eine früherere Version des Bausteinauswerfers.
Die hat mir aber nicht gefallen und ich habe sie später umgebaut.
