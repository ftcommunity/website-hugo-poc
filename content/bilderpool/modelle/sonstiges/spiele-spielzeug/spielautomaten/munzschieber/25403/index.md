---
layout: "image"
title: "(1) Gesamtansicht"
date: "2009-09-28T21:32:23"
picture: "Mnzschieber_201.jpg"
weight: "28"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25403
- /details9121.html
imported:
- "2019"
_4images_image_id: "25403"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25403 -->
So war mein Münzschieber auf der Convention 2009 in Erbes – Büdesheim zu sehen
