---
layout: "image"
title: "(14) Münzprüfer"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_214.jpg"
weight: "41"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25416
- /detailsc5b7-2.html
imported:
- "2019"
_4images_image_id: "25416"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25416 -->
Wenn zu kleine Münzen eingeworfen werden, sorgt der Münzprüfer zuverlässig dafür, daß diese durch die schwarze Rohrleitung direkt in die Kasse geleitet werden. Zu große Münzen können nicht eingeworfen werden, da der Schlitz maximal 20-Cent Münzen passieren läßt.
