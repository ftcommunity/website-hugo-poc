---
layout: "image"
title: "(24) Münzprüfer"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_024.jpg"
weight: "22"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17102
- /details9613.html
imported:
- "2019"
_4images_image_id: "17102"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17102 -->
Nach dem Münzprüfer ist an dem kardanischen Gelenk die Schiene aufgehängt. (teilweise vom Flachträger verdeckt)
