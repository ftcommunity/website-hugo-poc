---
layout: "image"
title: "(18) Antrieb"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_018.jpg"
weight: "17"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17097
- /details699c.html
imported:
- "2019"
_4images_image_id: "17097"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17097 -->
