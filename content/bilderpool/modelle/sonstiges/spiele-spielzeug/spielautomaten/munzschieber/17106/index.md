---
layout: "image"
title: "(28) Betrieb"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_028.jpg"
weight: "26"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17106
- /details90ce.html
imported:
- "2019"
_4images_image_id: "17106"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17106 -->
Blick in den Münzschieber, während des Betriebs.

Zum Abschluß noch die Gewinnchancen: von 100 eingeworfenen Münzen werden 60 – 80 Münzen wieder ausgeworfen. Die restlichen Münzen verbleiben auf den  Spielfeldern oder fallen in die zwei Fächer für den Aufsteller, denn der will ja auch was verdienen.

Natürlich kann es passieren, daß plötzlich, ohne Zutun, eine Münze (oder mehrere) in das Gewinnfach fallen.
Ganz streng verboten ist natürlich das „zufällige“ Anstoßen am Gerät, damit ein Gewinn herauskommt… Dann sollte eine Hupe ertönen, die das Aufstellpersonal herbeiruft, und den Übeltäter… (Das wurde (noch) nicht realisiert.)
Außerdem fehlen noch Lichteffekte, ansprechendes Äußeres…
