---
layout: "image"
title: "(3) Münzeinwurf"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_203.jpg"
weight: "30"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25405
- /details9042.html
imported:
- "2019"
_4images_image_id: "25405"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25405 -->
