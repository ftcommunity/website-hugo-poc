---
layout: "comment"
hidden: true
title: "9994"
date: "2009-10-02T15:29:28"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo schnaggels,

ja, die rechte untere Schaltung ist die Alarmanlage.
Zur Funktion: der NE 555 ist als retriggerable Monoflop geschaltet.
Wenn der Stab den Ring berührt (durch Kippen oder heftigem Stoß), wird der Kondensator entladen, der Summer ertönt.
Anschließend wird der Kondensator, vorausgesetzt der Stab berührt den Ring nicht mehr, über den 10 kOhm Widerstand wieder geladen, was etwa 7 Sekunden dauert. Dann erst ertönt der Summer nicht mehr. Mit dem Taster und dem 1kOhm Widerstand wird der Kondensator schneller geladen. Leider ertönt der Summer auch beim Einschalten der Schaltung.

Viele Grüße

mirose