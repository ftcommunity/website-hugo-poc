---
layout: "image"
title: "(25) Schaltung"
date: "2009-09-30T17:28:35"
picture: "Mnzschieber_225.jpg"
weight: "52"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25442
- /detailsd8c7-2.html
imported:
- "2019"
_4images_image_id: "25442"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-30T17:28:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25442 -->
Zum Abschluß noch die Schaltung.
Der IC wurde mit dem Kondensator und den 2 Widerständen auf einer Lochrasterplatine montiert.
