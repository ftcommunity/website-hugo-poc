---
layout: "image"
title: "(4) Blick auf das Spielfeld"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_004.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17083
- /details9583.html
imported:
- "2019"
_4images_image_id: "17083"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17083 -->
Unten in der Mitte seht ihr die Schale, aus dem man den Gewinn entnehmen kann. Links die schwarze Röhre, in der zu kleine Münzen direkt in den Untergrund befördert werden, denn es sollen nur 20-Cent Münzen eingeworfen werden.
