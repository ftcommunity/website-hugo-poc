---
layout: "image"
title: "(7) von hinten, Blick auf den Polwendeschalter"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_207.jpg"
weight: "34"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25409
- /details7bb4-2.html
imported:
- "2019"
_4images_image_id: "25409"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25409 -->
