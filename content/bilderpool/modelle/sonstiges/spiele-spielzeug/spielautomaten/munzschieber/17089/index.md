---
layout: "image"
title: "(10) Schiene"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_010.jpg"
weight: "9"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17089
- /details2f15.html
imported:
- "2019"
_4images_image_id: "17089"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17089 -->
Noch einmal die Schiene, von der die Münzen auf das Prallblech fallen. Während des Betriebs wird diese Schiene hin- und herbewegt.
