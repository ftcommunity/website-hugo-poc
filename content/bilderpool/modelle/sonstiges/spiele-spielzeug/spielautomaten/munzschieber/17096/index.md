---
layout: "image"
title: "(17) Antrieb"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_017.jpg"
weight: "16"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17096
- /details9d8a.html
imported:
- "2019"
_4images_image_id: "17096"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17096 -->
Noch einmal der Antrieb, diesmal direkt von hinten.
Unter der unteren Spielebene sieht man die 2 weißen Kunststoffbehälter, in die die Münzen für den Aussteller fallen, aber auch die ausgeschiedenen Münzen.
