---
layout: "image"
title: "(17) Münzprüfer"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_217.jpg"
weight: "44"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/25419
- /details1785.html
imported:
- "2019"
_4images_image_id: "25419"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25419 -->
ohne Motor
