---
layout: "image"
title: "09 Geldhaus ohne Dach"
date: "2010-05-31T21:14:39"
picture: "m09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27326
- /details5385.html
imported:
- "2019"
_4images_image_id: "27326"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27326 -->
Hier sieht man jetzt einen der "Arme" zum Teil rausgefahren. Normalerweise fährt er raus bis auch die zweite Münze rausgefallen ist, habe das nur zum Verdeutlichen so gelassen ;-)
Der andere Arm kann natürlich auch rausgefahren werden, wenn man 40ct gewinnt fahren beide ein Stück raus. 
Nebendran sieht man den Münzeinwurf und das Interface.