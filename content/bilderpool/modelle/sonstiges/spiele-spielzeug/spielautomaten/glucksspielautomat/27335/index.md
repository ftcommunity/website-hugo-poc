---
layout: "image"
title: "18 Der Impulszähler Z40"
date: "2010-05-31T21:14:39"
picture: "m18.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27335
- /details0a64.html
imported:
- "2019"
_4images_image_id: "27335"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27335 -->
Die drei Noppen des Z40 drücken auf auf den Taster, es dient als Ersatz eines Impulszählers, da ich nur zwei habe. 
Das Z40 wied von einem Z10 angetrieben, das untendrunter, also hinter dem Taster, sitzt.
Auf dem Bild sind nirgenswo Kabel zu erkennen, da ich sie schon alle weggemacht habe.