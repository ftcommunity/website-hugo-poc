---
layout: "image"
title: "08 Geldhaus ohne Dach"
date: "2010-05-31T21:14:39"
picture: "m08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27325
- /detailsa9c9.html
imported:
- "2019"
_4images_image_id: "27325"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27325 -->
Nahaufnahme des Speichers. 
Insgesamt sind sind 2x5x2 Münzen enthalten, wenn man also immer 40ct gewinnt, kann man nur 5 mal spielen, bis die Rote Lampe angeht und wartet, bis man wieder aufgefüllt hat bzw. den Knopf gedrückt hat ;-)