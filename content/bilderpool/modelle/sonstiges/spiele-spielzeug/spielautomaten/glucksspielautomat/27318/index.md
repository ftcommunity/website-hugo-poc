---
layout: "image"
title: "01 Glücksspielautomat - Gesamtansicht"
date: "2010-05-31T21:14:37"
picture: "m01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27318
- /details946c.html
imported:
- "2019"
_4images_image_id: "27318"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27318 -->
Hier habe ich einen Glücksspielautomat gebaut.
Ein Spiel kostet 10 Cent, mit diesen kann man dann bis zu 40 Cent gewinnen.
Die Funktionsweise ist im nächsten Bild erklärt.

Das Modell ist schon ein paar Monate alt und auch schon auseinander gebaut, die meisten Bilder sind während des Abbaus oder kurz davor entstanden. 


ach ja, hier ist ein Video: http://www.youtube.com/watch?v=GGeC0pjrObE
es ist zwar nicht sonderlich gut, aber es reicht, um das Spielprinzip zu verstehen.

Das Programm funktioniert einwandfrei, nur manchmal hängt das Förderband oder eine Münze bleibt im Einwurf stecken (beides im Video zu sehen)