---
layout: "image"
title: "06 Geldeinwurf"
date: "2010-05-31T21:14:39"
picture: "m06.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27323
- /details243e.html
imported:
- "2019"
_4images_image_id: "27323"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27323 -->
Hier ist der Geldeinwurf zu sehen. Alles was größer aus die 10ct Münze ist, passt nicht durch, was kleiner ist fällt einfach runter in den Tresor. Wenn der voll ist, muss man das ganze Ding nach vorne schieben, um das eingeworfene Geld entnehmen zu können. 
Hinten dran ist das Interface und links daneben sieht man das Erweiterungsmodul. Diese steuern den ganzen Automaten.