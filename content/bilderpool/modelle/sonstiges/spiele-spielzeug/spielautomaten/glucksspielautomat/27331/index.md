---
layout: "image"
title: "14 Abschussrampe"
date: "2010-05-31T21:14:39"
picture: "m14.jpg"
weight: "14"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27331
- /detailsb548-2.html
imported:
- "2019"
_4images_image_id: "27331"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27331 -->
Die Abschussrampe alleine ausgebaut. Unten hängt das Gummi.
(etwas unscharf, da Handykamera)