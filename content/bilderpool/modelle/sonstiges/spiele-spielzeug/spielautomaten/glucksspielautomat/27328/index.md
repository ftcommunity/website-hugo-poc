---
layout: "image"
title: "11 Drehbereich von oben"
date: "2010-05-31T21:14:39"
picture: "m11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27328
- /detailseef1.html
imported:
- "2019"
_4images_image_id: "27328"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27328 -->
Das ganze von oben.
Besser erkennbar ist hier die Rampe, auf die die Kugel nach dem Transport auf der Kette fällt. Danach rollt sie runter zur Abschussrampe.
Die Gondel auf der Kette und die Kugel kann man auch sehen.