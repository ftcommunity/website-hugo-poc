---
layout: "image"
title: "20 Interface"
date: "2010-05-31T21:14:40"
picture: "m20.jpg"
weight: "20"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27337
- /details2f2f-2.html
imported:
- "2019"
_4images_image_id: "27337"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27337 -->
Das Interface und sein Erweiterungsmodul, das Geldhaus und der Geldeinwurf. 
Beide Arme sind zum Teil rausgefahren, man kann gut erkennen, wie sie angetrieben werden. 
Die beiden Motoren der Arme sind übrigens abmontiert und die Kabel fehlen auch alle. Auch die Enttaster und die Taster beim Impulszähler sind weg.
Die beiden Impulszähler sind hinter dem Baustein 15 mit Loch, durch den die Stange für das Bewegen des Armes geht.  (ganz am Rand links)