---
layout: "image"
title: "27 Kabel"
date: "2010-05-31T21:14:40"
picture: "m27.jpg"
weight: "27"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27344
- /detailsb3a0.html
imported:
- "2019"
_4images_image_id: "27344"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27344 -->
Das sind die Kabel, die ich für das Modell benutzt habe. 
Am oberen Rand sieht man den Powermoter und das Interface.