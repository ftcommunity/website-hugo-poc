---
layout: "image"
title: "ft-RR4-09: Handtaster"
date: "2013-08-21T21:30:38"
picture: "l09.jpg"
weight: "9"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37248
- /details5b5f.html
imported:
- "2019"
_4images_image_id: "37248"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37248 -->
Die Taster oben auf dem Handgriff sind aus der Elektronik-Grabbelkiste.