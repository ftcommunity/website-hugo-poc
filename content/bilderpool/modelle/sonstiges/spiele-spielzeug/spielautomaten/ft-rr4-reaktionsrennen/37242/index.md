---
layout: "image"
title: "ft-RR4-03: kurz vor dem Ziel"
date: "2013-08-21T21:30:38"
picture: "l03.jpg"
weight: "3"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- /php/details/37242
- /detailsa86e.html
imported:
- "2019"
_4images_image_id: "37242"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37242 -->
Die roten Bausteine betätigen die Zielschalter.