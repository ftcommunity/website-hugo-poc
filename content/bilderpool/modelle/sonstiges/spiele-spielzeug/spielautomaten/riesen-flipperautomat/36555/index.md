---
layout: "image"
title: "Auto-Abschuss (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild13.jpg"
weight: "14"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36555
- /detailsb443.html
imported:
- "2019"
_4images_image_id: "36555"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36555 -->
Mein leider noch nicht funktionierender Auto-Abschuss des Flippers. Der Hubmagnet ist einer von Conrad.de, den ich (ich gestehs...) mit einem Kompressorgummi befestigt habe.
Der Hubmagnet ist für 12 Volt ausgelegt, mit 14V ist er immer noch zu schwach. Vielleicht sollte ich es mal mit 24V versuchen? Ich hab nur keinen passenden Trafo :-(

(Übrigens: das Thema habe ich auch schon im Forum angesprochen)