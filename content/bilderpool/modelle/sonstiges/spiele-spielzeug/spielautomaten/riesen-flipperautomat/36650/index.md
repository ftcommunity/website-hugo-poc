---
layout: "image"
title: "Stabilität ist wichtig 2 (noch im Bau)"
date: "2013-02-19T18:03:38"
picture: "bild4_3.jpg"
weight: "35"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36650
- /detailsbd5c.html
imported:
- "2019"
_4images_image_id: "36650"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36650 -->
Früher verwendete ich auch hier bei der mittleren Stütze BS 30. Auf die Idee mit den Alu's bin ich durch meine BS30-Not gekommen... Die schwarzen "Eckbausteine" sind mit S-Riegeln mit den Winkelträgern befestigt.