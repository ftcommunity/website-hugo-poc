---
layout: "image"
title: "Beleuchtung Innenbahn 1 (noch im Bau)"
date: "2013-04-18T20:27:44"
picture: "2013-04-182.jpg"
weight: "76"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
schlagworte: ["Flipper", "Flipperautomat", "Pinball", "Beleuchtung", "Lampe"]
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36849
- /details8926-2.html
imported:
- "2019"
_4images_image_id: "36849"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36849 -->
Beleuchtung unter der Platte in der rechten Innenbahn.