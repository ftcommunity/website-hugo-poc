---
layout: "image"
title: "Tilt-Pendel (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild19.jpg"
weight: "70"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36763
- /detailsd8f6.html
imported:
- "2019"
_4images_image_id: "36763"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36763 -->
Mit dem am Ring angelötetem Kabel. Das andere Kabel ist mit der Achse im Kardangelenk eingeschraubt.