---
layout: "image"
title: "Rechte Rampe"
date: "2013-10-03T09:29:05"
picture: "bild06_4.jpg"
weight: "95"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37500
- /details512b.html
imported:
- "2019"
_4images_image_id: "37500"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37500 -->
Der Eingangsbereich der rechten Rampe ist gleich geblieben, die Kugel wird jetzt durch die Rohrhülsen laufen und dann in die rechte Innenbahn fallen.