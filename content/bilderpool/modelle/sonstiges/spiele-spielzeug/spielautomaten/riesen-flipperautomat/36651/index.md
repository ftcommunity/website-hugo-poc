---
layout: "image"
title: "Stabilität ist wichtig 3 (noch im Bau)"
date: "2013-02-19T18:03:38"
picture: "bild5_3.jpg"
weight: "36"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36651
- /detailsab89.html
imported:
- "2019"
_4images_image_id: "36651"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36651 -->
Hier handelt es sich um die vordere rechte Stütze. Alle anderen Stützen bestehen (noch) aus Winkelträgern und BS5.