---
layout: "comment"
hidden: true
title: "21053"
date: "2015-09-05T09:05:51"
uploadBy:
- "Phil"
license: "unknown"
imported:
- "2019"
---
Danke! Ja, manchmal braucht man wirklich Geduld, wenn manche Sachen einfach nicht funktionieren wollen wie man sich das vorstellt. Da hilft es auch, mal ne Weile abstand vom Modell zu nehmen, quasi eine "Verschnaufpause" zu machen. Oft kommt danach dann die richtige Idee.

Schöne Grüße
Philip