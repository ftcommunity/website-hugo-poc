---
layout: "comment"
hidden: true
title: "17870"
date: "2013-03-12T18:44:12"
uploadBy:
- "da-kid"
license: "unknown"
imported:
- "2019"
---
@Phil:
Es gibt Acryl- oder Plexiglas in verschiedenen Stärken, aber so ab 1mm aufwärts ist es sinnvoll, ich würde mindestens 1,5 mm dicke Scheiben empfehlen.