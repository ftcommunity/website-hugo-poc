---
layout: "image"
title: "Neuer Kugelrücklauf 2"
date: "2014-08-20T17:27:31"
picture: "bild4_7.jpg"
weight: "108"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39258
- /detailsf768-2.html
imported:
- "2019"
_4images_image_id: "39258"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39258 -->
Hier sieht man ganz gut die Spule (die rechte von den beiden), die die Kugeln in den Abschuss schleudert. Die linke Spule, waagerecht eingebaut, ist für den rechten Flipperfinger zuständig.
Die Sensoren, die hier noch reinkommen, werde ich wohl in die Löcher der Winkelträger einbauen müssen, oder in den kleinen freien Spalt zwischen Flexschiene und Winkelträger.