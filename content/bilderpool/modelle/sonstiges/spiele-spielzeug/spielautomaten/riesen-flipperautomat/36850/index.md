---
layout: "image"
title: "Beleuchtung Innenbahn 2 (noch im Bau)"
date: "2013-04-18T20:27:44"
picture: "2013-04-183.jpg"
weight: "77"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
schlagworte: ["Flipper", "Flipperautomat", "Pinball", "Beleuchtung", "Lampe"]
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36850
- /details704d-2.html
imported:
- "2019"
_4images_image_id: "36850"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36850 -->
Und so sieht's unter der Platte aus. Mit dem normalen Leuchtstein passt die Lampe da nicht rein, deshalb kam die Birne aus dem Sockel raus (für mehr LEDs!), Stecker dran und der Draht wurde so gebogen, dass die Lampe quer liegt.