---
layout: "image"
title: "Fallziele 1 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild02_2.jpg"
weight: "41"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36733
- /details94ef-2.html
imported:
- "2019"
_4images_image_id: "36733"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36733 -->
Die Fallziele, auch "Drop Targets" genannt, sind eine der besten Tischkomponenten des ft-Flippers. Bei einem Treffer, wie der Name schon sagt, versinken diese Ziele im Spielfeld. Wurden alle drei Ziele getroffen, werden sie wieder an die Spielfeldoberfläche befördert.