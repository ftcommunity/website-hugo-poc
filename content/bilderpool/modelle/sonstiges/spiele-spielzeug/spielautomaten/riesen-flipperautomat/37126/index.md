---
layout: "image"
title: "Gesamt - Spielbereit (noch im Bau)"
date: "2013-07-01T09:24:41"
picture: "bild1_6.jpg"
weight: "82"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37126
- /details4e19.html
imported:
- "2019"
_4images_image_id: "37126"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37126 -->
Wie die überschrift schon sagt, ist der Flipper so schon spielbereit. Die Flipperfinger, Innen- und Außenbahnen, die Slingshots und der Kugelrücklauf funktionieren. Wenn man die Kugel den linken oder rechten Loop stark genug hinauf schießt, kommt sie auf der anderen Seite wieder herunter. Wenn die Kugel den Weg nicht ganz schafft, rollt sie durch eine der drei oberen Bahnen und rollt zu den Schlagtürmen, die noch nicht in Betrieb sind. Diesen Bereich verlässt sie in den linken Loop, wo sie dann auf den Spieler zurollt.

An der linken Rampe muss ich noch etwas arbeiten, sie ist sehr steil, weil direkt dahinter einer der drei Schlagtürme ist. Außerdem müssen die Flipperfinger stärker werden - zwei Kompressoren und sechs Magnetventile (also drei pro Flipperfinger) reichen noch nicht aus... :-(
Die Fallziele haben es leider nicht geschafft, sie sind zu groß und nehmen zuviel Platz weg.

Auf diesem Bild ist übrigens auch der Kran zu sehen, aber es folgt noch ein Detailbild.