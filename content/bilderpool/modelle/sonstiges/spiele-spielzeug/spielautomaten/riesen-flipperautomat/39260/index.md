---
layout: "image"
title: "Stromversorgung"
date: "2014-08-20T17:27:31"
picture: "bild6_4.jpg"
weight: "110"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39260
- /details570c-3.html
imported:
- "2019"
_4images_image_id: "39260"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39260 -->
Das ist die Stromversorgung, die erstmal für die Spulen alleine dient. Später soll die mal den ganzen Flipper mit Strom versorgen. Also- Spulen, LEDs, GI und den Mikrocontroller. Den Trafo, 12V, hab ich mal zusammen mit einem gleichen anderen, irgendwo in unserer Werkstatt gefunden, und dann gleich meine Methode angewendet: "Den kann ich gebrauchen, der gehört jetzt mir". :-))) Naja, er lag halt einfach rum. Aber die BS15-Loch, die mit Schrauben in den Löchern des Trafos befestigt sind, sind - überraschenderweise - genau im ft-Raster. Das macht das Einbauen erheblich einfacher.
Der Wechselstrom, der aus der Sekundärspule des Trafos heraus kommt, wird dann erst mal gleichgerichtet, und dann mit zwei "Riesenkondensatoren", einer 10.000µF, der andere 6.800µF, geglättet. Das ergibt so ca. 17V (gemessen), die dann an den Spulen anliegen. Die haben dadurch genügend Power, um die Kugel über den ganzen Tisch zu fegen.