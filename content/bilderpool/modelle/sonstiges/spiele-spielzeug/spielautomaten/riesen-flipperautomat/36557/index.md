---
layout: "image"
title: "Unterseite von rechts (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild15.jpg"
weight: "16"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36557
- /details6b13-2.html
imported:
- "2019"
_4images_image_id: "36557"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36557 -->
Hier mal ein kleiner Blick unter das Spielfeld.