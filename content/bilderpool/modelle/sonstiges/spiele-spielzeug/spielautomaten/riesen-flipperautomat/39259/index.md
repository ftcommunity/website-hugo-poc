---
layout: "image"
title: "Flipperfinger-Mechanik"
date: "2014-08-20T17:27:31"
picture: "bild5_5.jpg"
weight: "109"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/39259
- /details6ab2-2.html
imported:
- "2019"
_4images_image_id: "39259"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39259 -->
Hier sieht man deutlich die überarbeitete Version des Flipperfingerantriebs. Ganz perfekt ist es nicht, z. B. ist es an nur 2 Stellen aufgehängt und manche Teile verrutschen auf Dauer, bei den starken Kräften, die hier wirken.
Ich muss sagen, ich bin echt dankbar, dass es einen Kleber Marke "KLEBEN STATT BOHREN" gibt (Das soll hier jetzt aber keine Werbung sein). Diesen Kleber habe ich mit etwas frickelei in die Messinghülse rein gemacht, und dann die Metall-Winkelachsen reingesteckt und festgeschraubt. Festschrauben allein reicht nicht, deshalb dieser Kleber. Und das hält. Und wie!

Sorry, das Bild ist nicht das beste. Ich habe versucht es etwas nachzubearbeiten, ich hoffe man erkennt alles.