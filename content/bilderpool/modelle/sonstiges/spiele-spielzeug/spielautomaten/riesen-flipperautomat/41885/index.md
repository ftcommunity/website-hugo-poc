---
layout: "image"
title: "Auslöser für die Schlagtürme"
date: "2015-09-02T20:01:59"
picture: "bild09_5.jpg"
weight: "120"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41885
- /detailsd840.html
imported:
- "2019"
_4images_image_id: "41885"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41885 -->
Hier sieht man gut die beiden Metallachsen, die zur Auslösung aneinander gedrückt werden. der Spalt zwischendrin ist kleiner als 1mm, das ist wichtig, damit der Schlagturm gut auslöst. Insgesamt muss alles sehr leichtgängig sein, damit der Schlagturm gut arbeitet.