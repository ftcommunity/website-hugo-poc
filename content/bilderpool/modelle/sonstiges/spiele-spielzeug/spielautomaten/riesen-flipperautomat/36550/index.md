---
layout: "image"
title: "Rechte Innen- & Außenbahnen (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild08.jpg"
weight: "9"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36550
- /details1353-2.html
imported:
- "2019"
_4images_image_id: "36550"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36550 -->
Hier sieht man die rechte Innen- und Außenbahn. Die gelben Lampen zeigen an, ob man hier schon mal getroffen hat oder nicht. Das Gummi hat die Aufgabe, die Kugeln am ins-Aus-gehen zu hindern. Manchmal sollte sie sber trotzdem da raus gehen...