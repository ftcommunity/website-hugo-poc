---
layout: "image"
title: "Zentrum 2 (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild6_3.jpg"
weight: "87"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37131
- /details243e-2.html
imported:
- "2019"
_4images_image_id: "37131"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37131 -->
Nochmal das gleiche Bild mit abgesenkten Zielen und getroffenem Fallziel. Vor das Fallziel kommt noch eine Lampe hin.