---
layout: "image"
title: "Fallziele 9 (noch im Bau)"
date: "2013-03-10T17:46:49"
picture: "bild10_2.jpg"
weight: "49"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36741
- /details0d49-3.html
imported:
- "2019"
_4images_image_id: "36741"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:49"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36741 -->
Hier sieht man die Gummis und wie sie befestigt sind. Alle drei Ziele wurden "nicht getroffen".