---
layout: "image"
title: "Abschusslicht"
date: "2015-09-02T20:01:59"
picture: "bild07_5.jpg"
weight: "118"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41883
- /detailsaecb.html
imported:
- "2019"
_4images_image_id: "41883"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41883 -->
In der Öffnung, aus der die Kugeln herauskommen befindet sich eine LED, welche den hier sichtbaren Bereich ausleuchtet. Muss ja nicht sein, sieht aber ganz nett aus ;)