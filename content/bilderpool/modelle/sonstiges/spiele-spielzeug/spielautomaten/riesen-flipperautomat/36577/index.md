---
layout: "image"
title: "Rechte Rampe 2 (noch im Bau)"
date: "2013-02-04T10:32:23"
picture: "bild2.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36577
- /detailse444.html
imported:
- "2019"
_4images_image_id: "36577"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36577 -->
Das ist die Kurve, durch die die Kugel von der eigentlichen Rampe auf die Flexschienen geleitet wird.