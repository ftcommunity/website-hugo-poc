---
layout: "image"
title: "Auswurfloch von links (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild7.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36582
- /detailsd1bf.html
imported:
- "2019"
_4images_image_id: "36582"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36582 -->
Hier seht ihr den Auswurfmechanismus. Die Kugel rollt auf der Flexschiene nach unten, wird von einer noch nicht eingebauten Lichtschranke erkannt und dann vom P-Zylinder wieder ausgestoßen.