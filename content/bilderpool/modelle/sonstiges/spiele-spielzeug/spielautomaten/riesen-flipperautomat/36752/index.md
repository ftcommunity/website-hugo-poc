---
layout: "image"
title: "Lichter in den Bahnen 1 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild08_3.jpg"
weight: "59"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36752
- /details40bd.html
imported:
- "2019"
_4images_image_id: "36752"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36752 -->
Rollt die Kugel durch eine der Innen- oder oder Außenbahnen, blinkt das entsprechende Licht kurz sehr schnell und bleibt dann an, bis alle vier Bahnen durchlaufen wurden.