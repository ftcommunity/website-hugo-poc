---
layout: "comment"
hidden: true
title: "18124"
date: "2013-07-01T10:58:39"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Gratuliere! Da hat sich offenbar die Strategie "Durchbeißen, bis auch die letzten 3 % funktionieren" wiedermal bewährt!

Gruß,
Stefan