---
layout: "image"
title: "Platte 4"
date: "2014-06-22T18:49:30"
picture: "fliperplatte1.jpg"
weight: "104"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/38971
- /details60d0.html
imported:
- "2019"
_4images_image_id: "38971"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-06-22T18:49:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38971 -->
Der größte Teil der Platte ist jetzt herausgesägt. Man erkennt rechts die Aussparungen für die Slingshots, die Innenbahnen und die Flipperfinger. Links ist auch schon fast alles nicht benötigte grob entfernt, demnächst mache ich mich an die Feinarbeit, wie zum Beispiel die Aussparungen für die Schlagtürme, die sehr genau sein müssen, damit die Kugel nicht hängen bleibt. Die Löcher für die Wände der Oberen Bahnen und zwischen dem linken Schlagturm (hier der untere) und dem linken Loop fehlen noch ganz. Wohl deshalb, weil da noch die Führung für die Kugel fehlt...
Mittlerweile mache ich mir schon Gedanken, was ich unter die Plexiglasplatte tue. Entweder gar nichts, dann sieht man das Gitter und die Technik, allerdings kommen die Lichter, die unter der Platte sind, nicht so gut (sollten verschiedenfarbig sein, beschriftet werden und ich verwende LEDs). Oder ich lege ein großes Blatt Papier drunter, auf dem dann Bilder und Motive zum Thema ft zu sehen sind (z.B. Teile, Männchen, ft-Logo). Ich bin eher für letzteres, da man die Technik auch von der Seite sogar besser als von oben sehen kann. Wer möchte, kann ja kurz kommentieren, was er besser findet.