---
layout: "image"
title: "Tilt-Pendel (noch im Bau)"
date: "2013-02-17T23:43:44"
picture: "bild5_2.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36643
- /details9575.html
imported:
- "2019"
_4images_image_id: "36643"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36643 -->
So wie bei den großen Flippern.

(Oh oh... da darf ich auch nicht mehr so viel rütteln, wie ich sonst tue...)