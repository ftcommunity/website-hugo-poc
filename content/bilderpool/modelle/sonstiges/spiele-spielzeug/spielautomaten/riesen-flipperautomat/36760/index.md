---
layout: "image"
title: "Kugelrücklauf, überarbeitet 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild16_2.jpg"
weight: "67"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36760
- /details0e85-2.html
imported:
- "2019"
_4images_image_id: "36760"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36760 -->
Drei Kugel warten im Moment auf ihren Einsatz. Man erkennt hier auch den BS 7,5, and dem der Gelenkwürfel des Hebels befestigt ist. Damit ist dieser Hebel anti-längsverschiebbar.