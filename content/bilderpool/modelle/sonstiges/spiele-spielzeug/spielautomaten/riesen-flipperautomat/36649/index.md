---
layout: "image"
title: "Stabilität ist wichtig... 1 (noch im Bau)"
date: "2013-02-19T18:03:37"
picture: "bild3_3.jpg"
weight: "34"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36649
- /details777e-2.html
imported:
- "2019"
_4images_image_id: "36649"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36649 -->
...damit man Tilt nicht so leicht auslösen kann (man hat doch Mitleid :-) ). Dies ist übrigens die vorder linke Stütze.