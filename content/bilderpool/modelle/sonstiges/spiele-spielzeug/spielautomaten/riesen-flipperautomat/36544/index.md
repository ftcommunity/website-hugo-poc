---
layout: "image"
title: "Test TX Controller (noch im Bau)"
date: "2013-02-03T10:19:44"
picture: "bild02.jpg"
weight: "3"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36544
- /detailse9f9.html
imported:
- "2019"
_4images_image_id: "36544"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36544 -->
Den "Test TX Controller" hab ich einfach mal links am Flipper befestigt. In der fertigen Version, so stell ich mir das vor, wird es wohl an einer Seite eine Klappe geben, die man öffnen und dann etwas an einem der TX Controller machen kann.