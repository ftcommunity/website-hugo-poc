---
layout: "image"
title: "Kugelrücklauf, überarbeitet 1 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild15_2.jpg"
weight: "66"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36759
- /detailsd059-4.html
imported:
- "2019"
_4images_image_id: "36759"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36759 -->
So gebaut ist's besser. Der Kugelrücklauf braucht weniger BS 30 und ist etwas niedriger der vorige.