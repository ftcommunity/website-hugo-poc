---
layout: "image"
title: "Standart-Anzeige (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild01_3.jpg"
weight: "52"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36745
- /detailsfe02.html
imported:
- "2019"
_4images_image_id: "36745"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36745 -->
So sieht's auf dem Bildschirm aus, wenn gerade nichts los ist und man 80.260.000 Punkte hat und bei der zweiten Kugel ist.