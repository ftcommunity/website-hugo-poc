---
layout: "image"
title: "Auswurfloch im Detail (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild8.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36583
- /details80ac.html
imported:
- "2019"
_4images_image_id: "36583"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36583 -->
Durch den Hebelmechanismus ist es mir gelungen, die Kugel schön stark herauszuschleudern. Das Loch kann bis zu drei Kugeln halten und schmeißt die alle auf einmal heraus.

Das wär doch was: eine Kugel gelangt hinein, dann erscheint eine neue im Abzug. Die schießt man auch ins Loch. Dann kommt eine Dritte Kugel in den Abzug, und nach dem Einschießen werden die zwei gefangenen Kugeln ausgeworfen.