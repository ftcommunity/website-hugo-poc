---
layout: "image"
title: "Die GI & der rechte Teil des Spielfelds"
date: "2015-09-02T20:01:59"
picture: "bild03_5.jpg"
weight: "114"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/41879
- /details3e80.html
imported:
- "2019"
_4images_image_id: "41879"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41879 -->
Man müsste den Flipper mal im Dunkeln sehen. Durch die Lampen sieht der richtig gut aus.
Man sieht hier auch das Drehziel, den "Spinner". An den Fallzielen macht sich die Stabilität bemerkbar. Sowohl der hin- als auch der Rücktransport zum ft-Jubi 2015 hat dem Flipper ganz schön zugesetzt.