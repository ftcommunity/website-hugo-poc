---
layout: "image"
title: "Auswurfloch oben von vorne (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild9.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36584
- /detailsd814.html
imported:
- "2019"
_4images_image_id: "36584"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36584 -->
nur dieser kleine Ausschnitt ist der sichtbare des Lochs...