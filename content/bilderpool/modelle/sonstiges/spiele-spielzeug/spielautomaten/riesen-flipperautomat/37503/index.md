---
layout: "image"
title: "Backbox 2"
date: "2013-10-03T09:29:05"
picture: "bild09_4.jpg"
weight: "98"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37503
- /detailsd944-2.html
imported:
- "2019"
_4images_image_id: "37503"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37503 -->
Hier ist die Backbox heruntergeklappt. Dadurch spart man beim Transport ein bisschen an Höhe ein.