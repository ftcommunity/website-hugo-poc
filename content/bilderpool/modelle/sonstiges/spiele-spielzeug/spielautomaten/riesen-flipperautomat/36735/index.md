---
layout: "image"
title: "Fallziele 3 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild04_2.jpg"
weight: "43"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36735
- /details7b21-2.html
imported:
- "2019"
_4images_image_id: "36735"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36735 -->
Das ist die Rückseite der Fallziele. Der Balken ganz unten resettet die Ziele wieder.