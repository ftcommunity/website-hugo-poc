---
layout: "image"
title: "Lichter in den Bahnen 3 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild10_3.jpg"
weight: "61"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36754
- /detailsdd85.html
imported:
- "2019"
_4images_image_id: "36754"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36754 -->
Nein, diese zwei Lichter leuchten nicht, sie blinken. Und zwar schnell. Und gleichzeitig. Das bedeutet, dass der Spieler die vier Bahnen komplettiert hat (und 10 Millionen oder so kriegt).