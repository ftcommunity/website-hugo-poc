---
layout: "image"
title: "Elektronik 1"
date: "2009-01-03T19:45:49"
picture: "flipper5.jpg"
weight: "5"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16851
- /details6aee.html
imported:
- "2019"
_4images_image_id: "16851"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16851 -->
Zur Ansteuerung der kleinen Solenoide bietet sich die Leistungsstufe aus dem alten Elektromechanik-Kasten an.