---
layout: "image"
title: "Unterbau"
date: "2009-01-03T19:45:48"
picture: "flipper2.jpg"
weight: "2"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16848
- /detailsd4c5.html
imported:
- "2019"
_4images_image_id: "16848"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16848 -->
Die Spielfläche ist 7,5 Grad geneigt, das ist etwas mehr als bei einem echten Flipper.