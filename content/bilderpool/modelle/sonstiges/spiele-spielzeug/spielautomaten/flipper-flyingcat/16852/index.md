---
layout: "image"
title: "Elektronik 2"
date: "2009-01-03T19:45:49"
picture: "flipper6.jpg"
weight: "6"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16852
- /details3ed6.html
imported:
- "2019"
_4images_image_id: "16852"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16852 -->
Die großen Solenoide werden im Moment über Relais angesteuert - hier ist die Relaisleiste im Bild, die linken 3 von 5 sind noch nicht in Benutzung. Herr Tremba entwickelt eine Solenoid-Steuerung, sobald die fertig ist, kann ich die Relais wahrscheinlich durch eine lautlose und wesentlich schnellere elektronische Steuerung ersetzen. Der Rampen- und Abschuss-Solenoid sind 3 Volt-Typen; sie werden stark übersteuert (15,75 Volt, 235 Watt) - allerdings pro "Schuss" nur für etwa 50 ms. Die Solenoide können das ab - bis jetzt gab's keine Probleme.