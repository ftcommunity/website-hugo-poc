---
layout: "image"
title: "Kugelvereinzelung"
date: "2009-01-03T19:45:49"
picture: "flipper7.jpg"
weight: "7"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16853
- /detailsb0d6.html
imported:
- "2019"
_4images_image_id: "16853"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:49"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16853 -->
Hier einer der kleinen Magnete zur Vereinzelung der Kugeln auf der Rampe nach unten - der zweite sitzt verdeckt fast gegenüber. Wenn er unter Strom ist, ragt der Metallstift so weit in die Bahn, dass keine Kugel mehr vorbeikommt.