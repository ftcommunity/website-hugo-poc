---
layout: "image"
title: "Lange Rampe 2"
date: "2010-06-20T12:18:05"
picture: "flipper08.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/27540
- /details61f2.html
imported:
- "2019"
_4images_image_id: "27540"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27540 -->
Er bringt die meisten Punkte im Spiel.