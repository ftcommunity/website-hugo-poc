---
layout: "image"
title: "Lange Rampe 1"
date: "2010-06-20T12:18:05"
picture: "flipper07.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/27539
- /details33cf.html
imported:
- "2019"
_4images_image_id: "27539"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27539 -->
Am Ende der Rampe befindet sich ein Taster.