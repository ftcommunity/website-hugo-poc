---
layout: "image"
title: "Startrampe"
date: "2010-06-20T12:18:05"
picture: "flipper05.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/27537
- /detailse6fe.html
imported:
- "2019"
_4images_image_id: "27537"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27537 -->
Über die Startrampe wird eine neue Kugel ins Spiel gebracht.