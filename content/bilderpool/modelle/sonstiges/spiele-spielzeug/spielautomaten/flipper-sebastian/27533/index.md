---
layout: "image"
title: "Gesamtansicht 1"
date: "2010-06-20T12:18:04"
picture: "flipper01.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/27533
- /details59af-2.html
imported:
- "2019"
_4images_image_id: "27533"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27533 -->
In der mitte der Flipper. Am linken Rand befindet sich die Technik wie z.B. Interface, Kompressor und Münzsortierer.