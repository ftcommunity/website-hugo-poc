---
layout: "image"
title: "Fußhalter 3D-Druck"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46046
- /detailsd512.html
imported:
- "2019"
_4images_image_id: "46046"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46046 -->
Hiermit kann der Flipper auch in der Höhe verstellt werden.
