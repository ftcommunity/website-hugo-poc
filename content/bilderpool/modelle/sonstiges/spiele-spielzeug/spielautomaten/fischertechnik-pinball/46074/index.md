---
layout: "image"
title: "Elektronik von oben"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian47.jpg"
weight: "46"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46074
- /details596d.html
imported:
- "2019"
_4images_image_id: "46074"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46074 -->
Links ist der Luftspeicher zu sehen. Daneben der Antrieb, um die Kugel über das Zahnrad hochzuholen. Mittig die Multiplikatoranzeige. Dann ein Relais zum ansteuern. Rechts die Magenetventile und der I2C-Output 5- 24 V 8-bit.
