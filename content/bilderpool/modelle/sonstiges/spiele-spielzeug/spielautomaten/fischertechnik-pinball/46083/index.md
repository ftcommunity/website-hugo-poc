---
layout: "image"
title: "Flipper komplett."
date: "2017-07-10T19:44:56"
picture: "piratesofthecaribbian56.jpg"
weight: "55"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46083
- /details67a3.html
imported:
- "2019"
_4images_image_id: "46083"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:56"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46083 -->
