---
layout: "image"
title: "7-Segmentanzeige mit Halter"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian42.jpg"
weight: "41"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46069
- /details093d-2.html
imported:
- "2019"
_4images_image_id: "46069"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46069 -->
Halter unter:
https://ftcommunity.de/data/downloads/3ddruckdateien/icserial7segmetdisplaysparkfunhalter.zip
