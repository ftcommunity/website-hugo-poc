---
layout: "image"
title: "Kopfaufbau links"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46036
- /details8e96-2.html
imported:
- "2019"
_4images_image_id: "46036"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46036 -->
