---
layout: "image"
title: "Rückseite hinten Elektronik"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian44.jpg"
weight: "43"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46071
- /details252d.html
imported:
- "2019"
_4images_image_id: "46071"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46071 -->
Auf der Rückseite seht ihr die Ansteuerung des Flippers über zwei ROBO TX Controller. Die Ausgänge der Controller haben nicht für die komplette Ansteuerung des Flippers ausgereicht, daher hatte ich mich für die Verwendung von I2C-Komponenten entschieden.
