---
layout: "image"
title: "Kopfaufbau und Frontscheibe"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian38.jpg"
weight: "37"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46065
- /details33a1.html
imported:
- "2019"
_4images_image_id: "46065"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46065 -->
