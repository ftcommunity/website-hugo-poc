---
layout: "image"
title: "Maker Beam XL Endstück"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian17.jpg"
weight: "17"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46044
- /detailsaaaa-3.html
imported:
- "2019"
_4images_image_id: "46044"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46044 -->
Die Endstücke für die Maker Beam XL Profile habe ich in Inventor konstruiert und mit dem fischertechnik 3D Printer gedruckt.

3D Druck + Inventor Datei:
https://ftcommunity.de/data/downloads/3ddruckdateien/makerbeamxlendstueck.zip
