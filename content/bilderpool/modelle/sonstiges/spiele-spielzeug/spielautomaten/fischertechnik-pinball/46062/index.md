---
layout: "image"
title: "Der Kopfaufbau"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian35.jpg"
weight: "34"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46062
- /detailsab8d.html
imported:
- "2019"
_4images_image_id: "46062"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46062 -->
