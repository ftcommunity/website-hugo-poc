---
layout: "image"
title: "Maker Beam XL Fuß"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46045
- /detailsd95a.html
imported:
- "2019"
_4images_image_id: "46045"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46045 -->
Als Fuß brauchte ich eine neue Lösung.
