---
layout: "image"
title: "Flipper 'Pirates of the Caribbian'"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46028
- /details6542.html
imported:
- "2019"
_4images_image_id: "46028"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46028 -->
Den Aufbau habe ich bewußt erhöht gebaut. Der Vorteil ist, man bekommt alles unter, was Elektrik, Beleuchtung etc. angeht.
