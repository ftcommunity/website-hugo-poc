---
layout: "image"
title: "Flipper vorne"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46037
- /details11ac.html
imported:
- "2019"
_4images_image_id: "46037"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46037 -->
Vorne seht ihr den Startknopf. Damit wird das Spiel gestartet. Über den rechten schwarzen Flipperknopf wird die Anzahl der Spieler eingegeben.
