---
layout: "image"
title: "Fußhalter Detail"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46047
- /detailsb8f0.html
imported:
- "2019"
_4images_image_id: "46047"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46047 -->
Fußhalter mit Gelenkfuß und M3-Innensechskantschrauben.

3D Druck + Inventor Datei:
https://ftcommunity.de/data/downloads/3ddruckdateien/makerbeamxl3erfu.zip
