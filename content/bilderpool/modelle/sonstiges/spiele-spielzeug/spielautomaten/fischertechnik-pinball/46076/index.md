---
layout: "image"
title: "I2C-Output 5- 24 V 8-bit"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian49.jpg"
weight: "48"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46076
- /details3093-5.html
imported:
- "2019"
_4images_image_id: "46076"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46076 -->
Damit ich mehr Lampen ansteuern kann.
