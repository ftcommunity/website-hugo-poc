---
layout: "image"
title: "Mini-Kreisel (4)"
date: "2013-02-23T18:00:41"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36671
- /detailsfc99.html
imported:
- "2019"
_4images_image_id: "36671"
_4images_cat_id: "2719"
_4images_user_id: "1624"
_4images_image_date: "2013-02-23T18:00:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36671 -->
Das sind die dre Einzelteile - ein Widerstand, eine Flachnabe und eine Nabenmutter.