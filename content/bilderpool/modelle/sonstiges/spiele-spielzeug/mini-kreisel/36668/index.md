---
layout: "image"
title: "Mini-Kreisel (1)"
date: "2013-02-23T18:00:41"
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36668
- /detailsc690.html
imported:
- "2019"
_4images_image_id: "36668"
_4images_cat_id: "2719"
_4images_user_id: "1624"
_4images_image_date: "2013-02-23T18:00:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36668 -->
Ein kleiner Kreisel. Die Idee ist mir gekommen, als ich in der Schule einen Widerstand gefunden habe, bei dem an einer Seite der Draht fehlte. Beim herumspielen mit dem ft Arm, den ich für die Schule gebaut habe, ist mir aufgefallen, dass der Widerstand durch die 4mm-Löcher passt, und so ist dann dieser Kreisel entstanden.