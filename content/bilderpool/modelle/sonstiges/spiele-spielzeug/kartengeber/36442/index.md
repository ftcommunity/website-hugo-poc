---
layout: "image"
title: "Kartengeber (Einzug)"
date: "2013-01-06T17:46:54"
picture: "kartengeber4.jpg"
weight: "4"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/36442
- /detailscad2.html
imported:
- "2019"
_4images_image_id: "36442"
_4images_cat_id: "2705"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T17:46:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36442 -->
Hier werden die Karten eingezogen.