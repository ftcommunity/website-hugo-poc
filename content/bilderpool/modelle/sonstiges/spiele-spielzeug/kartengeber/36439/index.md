---
layout: "image"
title: "Kartengeber"
date: "2013-01-06T17:46:54"
picture: "kartengeber1.jpg"
weight: "1"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/36439
- /details924e.html
imported:
- "2019"
_4images_image_id: "36439"
_4images_cat_id: "2705"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T17:46:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36439 -->
Gesamtbild des Kartengebers