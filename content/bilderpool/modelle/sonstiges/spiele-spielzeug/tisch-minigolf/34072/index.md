---
layout: "image"
title: "Tisch-Minigolfbahn - Prototyp"
date: "2012-01-30T18:31:31"
picture: "tischminigolfprototyp1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/34072
- /details9576.html
imported:
- "2019"
_4images_image_id: "34072"
_4images_cat_id: "2521"
_4images_user_id: "1"
_4images_image_date: "2012-01-30T18:31:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34072 -->
Hier der erste Prototyp einer Bahn für eine kleine Tisch-Minigolfanlage aus fischertechnik.
Bis zur Convention möchte ich noch einige weitere Bahnen erstellen so das man dort ein wenig Tischminigolf spielen kann.

Vorne liegt der Queue mit dem die Kugeln angestoßen werden müssen.
