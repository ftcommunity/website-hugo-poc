---
layout: "image"
title: "Die Zahnstange"
date: "2012-11-24T19:00:27"
picture: "die_Zahnstange.jpg"
weight: "10"
konstrukteure: 
- "Zahnrädchen 001"
fotografen:
- "Zahnrädchen 001"
uploadBy: "Zahnrädchen001"
license: "unknown"
legacy_id:
- /php/details/36173
- /details7ae7-2.html
imported:
- "2019"
_4images_image_id: "36173"
_4images_cat_id: "776"
_4images_user_id: "1565"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36173 -->
Hier habe ich den Spielsteinbehälter enfent, wodurch man die 3 Schalter für die Positionierung sehen kann.