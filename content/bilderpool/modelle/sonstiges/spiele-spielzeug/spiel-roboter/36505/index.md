---
layout: "image"
title: "Untere Seite des flippers"
date: "2013-01-22T17:34:38"
picture: "Bild_4.jpg"
weight: "14"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
schlagworte: ["Flipper"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36505
- /details53bc-2.html
imported:
- "2019"
_4images_image_id: "36505"
_4images_cat_id: "776"
_4images_user_id: "1608"
_4images_image_date: "2013-01-22T17:34:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36505 -->
Hier ist die untere Seite des Flippers und der manuelle Abschuss.
Ich habe versucht den Abschuss zu automatisieren (mit Pneumatic) doch dann war die reibung zu groß. :(