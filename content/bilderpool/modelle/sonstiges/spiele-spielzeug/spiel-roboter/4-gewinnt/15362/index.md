---
layout: "image"
title: "Auswerfer"
date: "2008-09-21T19:29:43"
picture: "gewinnt5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15362
- /details3c84-2.html
imported:
- "2019"
_4images_image_id: "15362"
_4images_cat_id: "1401"
_4images_user_id: "521"
_4images_image_date: "2008-09-21T19:29:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15362 -->
Der Minimotor schiebt den Stein in das Spielfeld.
Er wird warscheinlich nicht mehr lange leben - quitscht laut und rasselt wenn man ihn schüttelt.