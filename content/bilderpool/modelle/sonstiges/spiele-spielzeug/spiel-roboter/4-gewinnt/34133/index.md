---
layout: "image"
title: "'Vier gewinnt'-Maschine (1)"
date: "2012-02-12T12:01:19"
picture: "VG1.jpg"
weight: "6"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
uploadBy: "ulib"
license: "unknown"
legacy_id:
- /php/details/34133
- /details1b80.html
imported:
- "2019"
_4images_image_id: "34133"
_4images_cat_id: "1401"
_4images_user_id: "1330"
_4images_image_date: "2012-02-12T12:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34133 -->
Hallo, das hier ist eine Maschine, gegen die ein Mensch "Vier gewinnt" spielen kann.
Und so funktionierts:
- Schlitten fährt nach links, Chip-Transport startet
- der vorderste Chip fällt runter durch die Lichtschranke, Chip-Transport stoppt
- Greifer schließt und hält Chip fest, Schlitten fährt gewünschte Position an, Greifer öffnet, ...
- Controller läuft im Online-Modus
- Programmierung in C (Eclipse, MinGW)

Folgende Möglichkeiten stehen für den Spieler zur Eingabe eines Zuges bereit:
- PC-Tastatur
- Android-Handy
- automatische Erkennung über webcam