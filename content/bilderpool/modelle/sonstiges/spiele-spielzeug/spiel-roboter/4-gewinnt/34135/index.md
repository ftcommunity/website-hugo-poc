---
layout: "image"
title: "'Vier gewinnt'-Maschine (3)"
date: "2012-02-12T12:01:20"
picture: "VG3.jpg"
weight: "8"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
uploadBy: "ulib"
license: "unknown"
legacy_id:
- /php/details/34135
- /details90cb-2.html
imported:
- "2019"
_4images_image_id: "34135"
_4images_cat_id: "1401"
_4images_user_id: "1330"
_4images_image_date: "2012-02-12T12:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34135 -->
