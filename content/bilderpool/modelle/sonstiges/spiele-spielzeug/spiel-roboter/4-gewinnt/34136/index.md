---
layout: "image"
title: "'Vier gewinnt'-Maschine (4)"
date: "2012-02-12T12:01:20"
picture: "VG4.jpg"
weight: "9"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
uploadBy: "ulib"
license: "unknown"
legacy_id:
- /php/details/34136
- /detailsd39f.html
imported:
- "2019"
_4images_image_id: "34136"
_4images_cat_id: "1401"
_4images_user_id: "1330"
_4images_image_date: "2012-02-12T12:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34136 -->
