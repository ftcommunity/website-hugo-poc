---
layout: "comment"
hidden: true
title: "16381"
date: "2012-02-13T22:36:15"
uploadBy:
- "ulib"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
die Drehscheibe ist fest aufgeschraubt, jedoch ist das Getriebe defekt. Zum Festhalten des Chips dreht sich der S-Motor einen Tick länger als zum Öffnen, aber nicht die ganze Zeit bis zum Abwurf. Der Chip hält, da auf der feststehenden Seite ein selbstklebender Filz angebracht ist.
Viele Grüße,
Uli.