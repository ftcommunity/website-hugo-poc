---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-21T19:29:42"
picture: "gewinnt1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15358
- /details31d2.html
imported:
- "2019"
_4images_image_id: "15358"
_4images_cat_id: "1401"
_4images_user_id: "521"
_4images_image_date: "2008-09-21T19:29:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15358 -->
Mit der Drehscheibe entscheidet der Spieler wo sein Stein(grau) hingelegt wird, die schwarzen Steine legt das Interface selbstständig.