---
layout: "image"
title: "Das Spielfeld"
date: "2012-11-24T19:00:27"
picture: "das_spielfeld.jpg"
weight: "6"
konstrukteure: 
- "Zahnrädchen 001"
fotografen:
- "Zahnrädchen 001"
uploadBy: "Zahnrädchen001"
license: "unknown"
legacy_id:
- /php/details/36169
- /details9d8f-2.html
imported:
- "2019"
_4images_image_id: "36169"
_4images_cat_id: "776"
_4images_user_id: "1565"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36169 -->
Hier sieht man das Spielfeld des Roboters.