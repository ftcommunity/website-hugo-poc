---
layout: "image"
title: "Flipper_Bild3"
date: "2012-04-27T21:14:06"
picture: "Flipper_Bild3.jpg"
weight: "3"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/34824
- /details0145.html
imported:
- "2019"
_4images_image_id: "34824"
_4images_cat_id: "2576"
_4images_user_id: "-1"
_4images_image_date: "2012-04-27T21:14:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34824 -->
