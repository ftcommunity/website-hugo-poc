---
layout: "comment"
hidden: true
title: "803"
date: "2005-11-22T21:22:56"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Zu viel des GutenDas erinnert mich doch ein wenig an meinen Roboterarm, was mich natürlich freut – selten hab ich so gute Ideen, dass andere sie klauen (oder zumindest die gleichen haben).

Aber: Springen die Kegelräder nicht beiseite, wenn die Belastung auf den Greifer zu groß wird? Und muss das Handgelenk überhaupt angetrieben werden, wenn doch der Greifer immer nur nach unten zeigen soll? 

Ist die Positionierung zuverlässig? Wenn du mit den gleichen gemessenen Impulsen nicht immer die gleiche Position erreichst, musst du den Arm steifer aufbauen.

Natürlich ist es noch leichter, einen Portalroboter aufzubauen, wenn du die Teile dafür hast. Trotzdem, viel Erfolg weiterhin.