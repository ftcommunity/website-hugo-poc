---
layout: "image"
title: "Greifer4"
date: "2005-11-29T20:12:06"
picture: "Brettspielroboter_004.jpg"
weight: "5"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5427
- /details4fa3.html
imported:
- "2019"
_4images_image_id: "5427"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2005-11-29T20:12:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5427 -->
