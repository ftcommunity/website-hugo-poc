---
layout: "image"
title: "Greifer6"
date: "2005-11-29T20:12:06"
picture: "Brettspielroboter_006.jpg"
weight: "7"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5429
- /detailsb033.html
imported:
- "2019"
_4images_image_id: "5429"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2005-11-29T20:12:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5429 -->
