---
layout: "image"
title: "Detailansicht Auswurfmechansimus"
date: "2011-12-23T19:30:21"
picture: "cac9.jpg"
weight: "8"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33727
- /details21ce.html
imported:
- "2019"
_4images_image_id: "33727"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33727 -->
Detailansicht des Auswurfmechansimus´
Eine Karte wird gerade nach vorne hinausgeschoben.