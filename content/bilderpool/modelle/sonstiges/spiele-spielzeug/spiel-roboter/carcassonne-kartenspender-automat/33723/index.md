---
layout: "image"
title: "Auswurfmechanismus Hinten"
date: "2011-12-23T19:30:20"
picture: "cac4.jpg"
weight: "4"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33723
- /details0379.html
imported:
- "2019"
_4images_image_id: "33723"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33723 -->
Für beide Schächte gibt es einen Auswurfmechanismus, welcher immer zum jeweiligen Kartenschacht fährt, und dort eine Karte herausschiebt. Nach dem Auswurf fährt er in seine Warteposition zwischen beiden Schächten zurück. Es sein denn die Schachtfüllungsüberpüfung mittels Lichtschranke ergibt nach erfolgtem Auswurf, dass ein Schacht leer ist. In diesem Fall wartet der Auswerfer hinter dem noch befüllten Schacht, bis dieser auch leer ist, oder der andere Schacht wieder befüllt wurde.