---
layout: "image"
title: "Befüllter Kartenschacht"
date: "2011-12-23T19:30:21"
picture: "cac7.jpg"
weight: "6"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33725
- /details7c8b.html
imported:
- "2019"
_4images_image_id: "33725"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33725 -->
Befüllter Kartenschacht während der Befüllungsüberprüfung (Licht an).