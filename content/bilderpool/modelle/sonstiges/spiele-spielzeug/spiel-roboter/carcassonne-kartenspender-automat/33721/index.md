---
layout: "image"
title: "Draufsicht"
date: "2011-12-23T14:18:31"
picture: "cac2.jpg"
weight: "2"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/33721
- /detailsf8aa.html
imported:
- "2019"
_4images_image_id: "33721"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T14:18:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33721 -->
Hinten ist der Auswurfmechanismus für die Stapel zusehen.