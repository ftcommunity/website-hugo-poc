---
layout: "overview"
title: "Carcassonne Kartenspender Automat"
date: 2020-02-22T08:31:00+01:00
legacy_id:
- /php/categories/2494
- /categories5353.html
- /categories536f.html
- /categories6cc7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2494 --> 
Nachdem die Bilder nun seit fast einem Jahr auf meiner HD warten, komme ich nun endlich zum Upload! Vorgestellt wird ein Automat, welcher nach dem Zufallsprinzip Carcassonnespielkarten aus einem von zwei Stapeln auswirft. Näheres steht unter den Bildern.