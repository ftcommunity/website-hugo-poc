---
layout: "image"
title: "Flipper Version 2"
date: "2007-03-11T14:19:05"
picture: "IMG_0141.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9406
- /detailsed61.html
imported:
- "2019"
_4images_image_id: "9406"
_4images_cat_id: "776"
_4images_user_id: "557"
_4images_image_date: "2007-03-11T14:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9406 -->
Software siehe Downloads(11.03.07)
Details:Detektion verlorene Bälle und Flipper