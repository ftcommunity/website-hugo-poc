---
layout: "image"
title: "Der Resetschalter"
date: "2012-11-24T19:00:27"
picture: "der_Endschalter.jpg"
weight: "9"
konstrukteure: 
- "Zahnrädchen 001"
fotografen:
- "Zahnrädchen 001"
uploadBy: "Zahnrädchen001"
license: "unknown"
legacy_id:
- /php/details/36172
- /detailsc90a.html
imported:
- "2019"
_4images_image_id: "36172"
_4images_cat_id: "776"
_4images_user_id: "1565"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36172 -->
