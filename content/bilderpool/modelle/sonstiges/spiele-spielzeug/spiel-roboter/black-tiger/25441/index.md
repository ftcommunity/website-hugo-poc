---
layout: "image"
title: "Gewinntafel"
date: "2009-09-30T15:09:00"
picture: "blacktiger02_2.jpg"
weight: "13"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/25441
- /detailsa51c.html
imported:
- "2019"
_4images_image_id: "25441"
_4images_cat_id: "1780"
_4images_user_id: "791"
_4images_image_date: "2009-09-30T15:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25441 -->
