---
layout: "image"
title: "ohne Gehäuse"
date: "2009-09-28T21:32:25"
picture: "blacktiger10.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebstian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/25436
- /details9f93-2.html
imported:
- "2019"
_4images_image_id: "25436"
_4images_cat_id: "1780"
_4images_user_id: "791"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25436 -->
