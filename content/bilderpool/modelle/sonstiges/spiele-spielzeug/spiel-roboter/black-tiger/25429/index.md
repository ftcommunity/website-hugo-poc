---
layout: "image"
title: "Chipeinwurf"
date: "2009-09-28T21:32:25"
picture: "blacktiger03.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebstian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/25429
- /details04ae.html
imported:
- "2019"
_4images_image_id: "25429"
_4images_cat_id: "1780"
_4images_user_id: "791"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25429 -->
