---
layout: "image"
title: "August  Pneumatik  ( =Bruder Eucalypta )"
date: "2010-12-26T10:55:44"
picture: "August-Maske-Robot_001.jpg"
weight: "79"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29537
- /details99cf.html
imported:
- "2019"
_4images_image_id: "29537"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2010-12-26T10:55:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29537 -->
Maske-Robot  August
