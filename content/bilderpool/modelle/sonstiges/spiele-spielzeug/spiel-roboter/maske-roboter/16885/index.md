---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T19:02:45"
picture: "Fischertechnik__Kalmthoutse-Heide_044.jpg"
weight: "61"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16885
- /detailsb71a.html
imported:
- "2019"
_4images_image_id: "16885"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T19:02:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16885 -->
Elefant mit Vakuum-Rüssel
