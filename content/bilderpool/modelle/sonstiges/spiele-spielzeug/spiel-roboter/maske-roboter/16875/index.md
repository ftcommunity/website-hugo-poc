---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T14:10:38"
picture: "Fischertechnik__Kalmthoutse-Heide_017.jpg"
weight: "51"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16875
- /detailscd2f.html
imported:
- "2019"
_4images_image_id: "16875"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T14:10:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16875 -->
Elefant mit Vakuum-Rüssel
