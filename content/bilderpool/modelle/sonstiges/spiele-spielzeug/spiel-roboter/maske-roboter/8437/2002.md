---
layout: "comment"
hidden: true
title: "2002"
date: "2007-01-14T19:36:58"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Paul en andere FT-Freunden,

Ik werk via 2 instelbare reduceerventielen met een hoge druk (1,6bar)t.b.v. de lange profesionele Mecman cylinder met een slag van 320mm en een lage druk (0,7 bar)t.b.v. de Fischertechnik-cylindertjes.

De "kopopbouw" schat ik op enkele kilo's, temeer omdat ik het achterhoofd extra heb moeten ballasten met loodplaatje (in FT-casette (60x60x30mm). Met de beschikbare druk van 1,6 bar is dit geen enkel probleem. Voor de hoge druk gebruik ik overigens niet de FT-magneet-ventielen: deze zijn beslist ongeschikt !
Ik gebruik een magneet-ventiel van Lemo-solar welke geschikt is tot 7 bar en relatief een grote doorlaat heeft.

Sensor-Technics heeft overigens ook interessante magneetventielen. Deze moet ik nog uitproberen.........