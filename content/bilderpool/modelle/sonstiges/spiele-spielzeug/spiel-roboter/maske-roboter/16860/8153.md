---
layout: "comment"
hidden: true
title: "8153"
date: "2009-01-05T19:33:08"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Freunden,

De slang-aandrijving moet gezien worden als een test-studie om op een eenvoudige manier een wervelkolom te kunnen laten bewegen.
Het aantrekken van een koord aan de ene zijde, en het tegelijkertijd ontspannen aan de andere zijde, komt overeen met de spier- bewegingen en -krachten rond een wervelkolom. Of het nu een slang, paling, hond of een mens betreft, het principe van de aanwezige wervelkolom-beweging is bij allen hetzelfde.

Lering trekkend uit de ervaringen van de slang-aandrijving ben ik verder gaan zoeken en experimenteren om een olifanten-slurf te kunnen laten bewegen. 
http://www.ftcommunity.de/details.php?image_id=16902

Gruss,

Peter Damen
Poederoyen NL