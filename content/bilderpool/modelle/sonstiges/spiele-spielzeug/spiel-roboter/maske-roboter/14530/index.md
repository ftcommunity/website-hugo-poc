---
layout: "image"
title: "'Bor de Wolf ' noch ohne Rotkäppchen"
date: "2008-05-16T21:40:32"
picture: "Bor_de_Wolf_007.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/14530
- /detailsffbf.html
imported:
- "2019"
_4images_image_id: "14530"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2008-05-16T21:40:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14530 -->
