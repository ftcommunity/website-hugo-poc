---
layout: "comment"
hidden: true
title: "1999"
date: "2007-01-14T18:58:58"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Markus und Ulrich Müller,
 
Sehr vielen Dank !  ....endlich wieder wichtige neue Information für "Robopro-Anfänger" und Leute die sich nicht Beruflich mit Programieren beschäftigen. Ich bin Wasserbau-Ingenieur und baue verschiedene FT-Modellen die ich automatisieren möchte.
Das Programmieren mit Robopro ist Interessant aber für "Robopro-Anfänger" manchmal doch Kompliziert.
 
Ihren Tutorial hat mir den letzten Jahren sehr viel gelernt, vorallem wegen die verschiedene (einfache) Beispielen mit Unterprogrammen die man selbst leicht ändern kann. Eine grosse Anerkennung.......sehr vielen Dank...
 
Gruss,
 
Peter Damen
Poederoyen, Holland