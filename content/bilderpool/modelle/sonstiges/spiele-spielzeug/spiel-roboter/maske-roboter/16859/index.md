---
layout: "image"
title: "Schlange-Antrieb"
date: "2009-01-04T14:10:37"
picture: "Fischertechnik__Kalmthoutse-Heide_035.jpg"
weight: "35"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16859
- /details3cda.html
imported:
- "2019"
_4images_image_id: "16859"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T14:10:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16859 -->
Schlange-Antrieb
