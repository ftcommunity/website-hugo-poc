---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T19:02:46"
picture: "Fischertechnik__Kalmthoutse-Heide_107.jpg"
weight: "70"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16894
- /details12b8-2.html
imported:
- "2019"
_4images_image_id: "16894"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T19:02:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16894 -->
Elefant mit Vakuum-Rüssel
