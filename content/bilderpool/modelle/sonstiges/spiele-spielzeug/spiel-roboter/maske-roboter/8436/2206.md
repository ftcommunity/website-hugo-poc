---
layout: "comment"
hidden: true
title: "2206"
date: "2007-01-29T20:22:26"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
FT-Freunden,

Bei SensorTechnics ( www.sensortechnics.com ) gibt es 5 und 12V Pneumatik Magnetventiele, passend für Fischertechnik.

Sensortechnics GmbH
Boschstr. 10
82178 Puchheim 
Deutschland 
Telefon: +49 (89) 80083-0 
Fax: +49 (89) 80083-33 


V2-valves: V2-13-3-PV-12-F-8-8 á 22,30 Euro/st 
eignen sich sehr gut für Fischertechnik. 
Weil es 3 Anschlusse gibt statt nur 2 wie beim öriginal FT-Magnetventilen gibt es mehrere möglichkeiten. Positionierung der Zylinder ist dann leichter.




Gruss,

Peter Damen
Poederoyen NL