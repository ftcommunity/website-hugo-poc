---
layout: "image"
title: "Eucalypta Hexe mit Kleider und Voice-modulen"
date: "2007-02-12T17:46:56"
picture: "Explorer-Eucalypta_004.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/8985
- /detailsa840.html
imported:
- "2019"
_4images_image_id: "8985"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2007-02-12T17:46:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8985 -->
Eucalypta Hexe mit Kleider und Voice-modulen
