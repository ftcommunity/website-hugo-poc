---
layout: "image"
title: "Auswurf"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter5.jpg"
weight: "5"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/8772
- /detailsbe14.html
imported:
- "2019"
_4images_image_id: "8772"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8772 -->
Das Rad wird gedreht und scleudert die Bälle auf die Platte. Indem nur oben ein Rad ist werden sie auch angeschnitten.
