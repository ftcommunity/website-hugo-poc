---
layout: "image"
title: "Aufbau 6"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum12.jpg"
weight: "12"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44932
- /details9af4.html
imported:
- "2019"
_4images_image_id: "44932"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44932 -->
Die Standfüße sind einfach in 30er Bausteine mit Loch gesteckt so dass sie leicht an und abbaubar sind.