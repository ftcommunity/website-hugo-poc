---
layout: "image"
title: "Aufbau 5"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum11.jpg"
weight: "11"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44931
- /details5598-4.html
imported:
- "2019"
_4images_image_id: "44931"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44931 -->
An dem Lichtmast in der Mitte wird später die Plane befestigt. 
Die Klapptreppe links wird während der Fahrt einfach unter den Anhänger geschoben und bei gebrauch herausgezogen.