---
layout: "image"
title: "Aufbau 8"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum14.jpg"
weight: "14"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/44934
- /details9807.html
imported:
- "2019"
_4images_image_id: "44934"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44934 -->
Komplett Aufgebaut