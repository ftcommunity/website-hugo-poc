---
layout: "image"
title: "Skizze Programm"
date: "2007-02-11T15:18:39"
picture: "bettklimaanlage17.jpg"
weight: "17"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8961
- /detailsdda8.html
imported:
- "2019"
_4images_image_id: "8961"
_4images_cat_id: "812"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8961 -->
