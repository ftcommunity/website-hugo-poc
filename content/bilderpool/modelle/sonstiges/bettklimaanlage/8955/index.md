---
layout: "image"
title: "Kabel2"
date: "2007-02-11T15:18:39"
picture: "bettklimaanlage11.jpg"
weight: "11"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8955
- /detailsf508.html
imported:
- "2019"
_4images_image_id: "8955"
_4images_cat_id: "812"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8955 -->
