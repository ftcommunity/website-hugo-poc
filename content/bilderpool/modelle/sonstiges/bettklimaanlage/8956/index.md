---
layout: "image"
title: "Kabel3"
date: "2007-02-11T15:18:39"
picture: "bettklimaanlage12.jpg"
weight: "12"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8956
- /detailsf6f4.html
imported:
- "2019"
_4images_image_id: "8956"
_4images_cat_id: "812"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8956 -->
