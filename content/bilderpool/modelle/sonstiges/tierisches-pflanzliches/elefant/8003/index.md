---
layout: "image"
title: "Elefant schräg von hinten"
date: "2006-12-20T19:22:17"
picture: "Elefant_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8003
- /details3c55.html
imported:
- "2019"
_4images_image_id: "8003"
_4images_cat_id: "746"
_4images_user_id: "328"
_4images_image_date: "2006-12-20T19:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8003 -->
Sicher nicht die Schokoladenseite, aber die Schwanz-Kurbel ist gut zu sehen.