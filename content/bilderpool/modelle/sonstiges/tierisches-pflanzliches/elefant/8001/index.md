---
layout: "image"
title: "Elefant mit Rüssel oben"
date: "2006-12-20T19:22:17"
picture: "Elefant_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8001
- /detailsddf6.html
imported:
- "2019"
_4images_image_id: "8001"
_4images_cat_id: "746"
_4images_user_id: "328"
_4images_image_date: "2006-12-20T19:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8001 -->
Hier mein Elefant mit maximal gehobenem Rüssel, der über die Schwanz-Kurbel betätigt wird.