---
layout: "image"
title: "Elefant schräg von vorn"
date: "2006-12-20T19:22:17"
picture: "Elefant_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8002
- /detailsa274.html
imported:
- "2019"
_4images_image_id: "8002"
_4images_cat_id: "746"
_4images_user_id: "328"
_4images_image_date: "2006-12-20T19:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8002 -->
Speziell in dieser Ansicht zeigt sich, dass FT für biologische Darstellungen weniger geeignet ist, da so komplexe Formen wie ein Kopf nur schwer darzustellen sind.

Wichtig war mir, dass das Rüssel-Gelenk nicht offen liegt. Der obere Teil des Rüssels taucht quasi in die Stirn ein, was in Bewegung recht realistisch aussieht.