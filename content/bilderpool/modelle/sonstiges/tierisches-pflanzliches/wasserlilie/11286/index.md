---
layout: "image"
title: "FT-Waterlelie   Poederoyen NL"
date: "2007-08-03T16:12:02"
picture: "Efteling-2007_084.jpg"
weight: "22"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11286
- /details08b4.html
imported:
- "2019"
_4images_image_id: "11286"
_4images_cat_id: "1028"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11286 -->
Links en verdere info:

Efteling Indische Waterlelies:

http://www.efteling.com/Page.aspx?PageId=1121&ParentName=Park&LanguageId=1
