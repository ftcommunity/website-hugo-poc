---
layout: "image"
title: "FT-Waterlelie   Poederoyen NL"
date: "2007-08-03T16:12:02"
picture: "FT-Diafragma-dak-constructie_015.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11266
- /details4d8e.html
imported:
- "2019"
_4images_image_id: "11266"
_4images_cat_id: "1028"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11266 -->
