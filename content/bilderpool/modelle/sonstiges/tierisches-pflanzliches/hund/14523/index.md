---
layout: "image"
title: "Yellow Dog"
date: "2008-05-16T21:40:31"
picture: "yellow_dog.jpg"
weight: "3"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Yellow", "Dog", "gelb", "blöcke"]
schlagworte: ["Yellow", "Dog", "gelb", "blöcke"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14523
- /details3cb2.html
imported:
- "2019"
_4images_image_id: "14523"
_4images_cat_id: "1235"
_4images_user_id: "585"
_4images_image_date: "2008-05-16T21:40:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14523 -->
I purchased the special yellow blocks last December. This Yellow Dog is all I have built with them! Did anyone else purchase the yellow blocks? 

***google translation: 
Ich habe die spezielle gelbe Blöcke im Dezember letzten Jahres. Dies Yellow Dog ist alles, was ich haben mit ihnen! Hat jemand Kauf der gelben Blöcke?