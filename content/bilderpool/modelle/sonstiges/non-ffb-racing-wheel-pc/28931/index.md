---
layout: "image"
title: "(Update 05.10.10) Lenkrad von Vorne"
date: "2010-10-05T16:42:50"
picture: "IMG_9391.jpg"
weight: "10"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/28931
- /details2448.html
imported:
- "2019"
_4images_image_id: "28931"
_4images_cat_id: "2033"
_4images_user_id: "986"
_4images_image_date: "2010-10-05T16:42:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28931 -->
