---
layout: "image"
title: "von hinten"
date: "2010-08-31T13:05:25"
picture: "IMG_7319_.jpg"
weight: "1"
konstrukteure: 
- "ich"
fotografen:
- "ich"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/28023
- /details642f.html
imported:
- "2019"
_4images_image_id: "28023"
_4images_cat_id: "2033"
_4images_user_id: "986"
_4images_image_date: "2010-08-31T13:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28023 -->
hier ist einmal das getriebe für die "digitalisierung" und auf der anderen seite der pneumatikschlauch zu sehen, der für mehr wiederstand sorgt.