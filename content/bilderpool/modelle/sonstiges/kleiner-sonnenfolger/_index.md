---
layout: "overview"
title: "Kleiner Sonnenfolger"
date: 2021-03-28T21:36:18+02:00
---

Das Modell stellt eine einfache Solarzellennachführung dar. Es entspricht im wesentlichen dem Modell aus dem Baukasten oeco-Energy.
Der Zeiger wird Richtung Sonne gedreht. Die Solarzellen sind antiparallel geschaltet, sind sie gleich stark beleuchtet, so heben sich ihre Spannungen auf und der Motor steht still. Wandert die Sonne nun und bestrahlt eine Zelle stärker als die andere, so treibt die resultierende Spannung den Motor an und dreht den Zeiger in die entsprechende Richtung.
