---
layout: "image"
title: "Detail1"
date: 2021-03-28T21:36:23+02:00
picture: "Sonnenfolger_3.jpeg"
weight: "4"
konstrukteure: 
- "bertbln"
fotografen:
- "bertbln"
schlagworte: ["Solar","Sonne"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Anschluss der Solarzellen-Ausgänge an den Motor.
