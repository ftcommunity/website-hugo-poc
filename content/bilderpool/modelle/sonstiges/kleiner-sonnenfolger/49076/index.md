---
layout: "image"
title: "Schaltplan"
date: 2021-03-28T21:36:18+02:00
picture: "schaltplan.png"
weight: "6"
konstrukteure: 
- "bertbln"
fotografen:
- "bertbln"
uploadBy: "Website-Team"
license: "unknown"
---

Der Schaltplan zeigt, wie die Solarzellen mit dem Motor verbunden sind.