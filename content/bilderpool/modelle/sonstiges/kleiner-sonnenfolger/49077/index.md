---
layout: "image"
title: "Detail2"
date: 2021-03-28T21:36:20+02:00
picture: "Sonnenfolger_4.jpeg"
weight: "5"
konstrukteure: 
- "bertbln"
fotografen:
- "bertbln"
schlagworte: ["Solar","Sonne"]
uploadBy: "Website-Team"
license: "unknown"
---

Der mechanische Aufbau in Detailansicht.
