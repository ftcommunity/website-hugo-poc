---
layout: "image"
title: "Trommelmännle"
date: "2011-02-25T13:51:09"
picture: "trommelmaennle1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30120
- /details868c.html
imported:
- "2019"
_4images_image_id: "30120"
_4images_cat_id: "2226"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:51:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30120 -->
Hier ein sehr beliebtes Spielzeug unseres Sohnes (2 1/2)
