---
layout: "image"
title: "Lichtbogen"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne05.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11786
- /detailse131.html
imported:
- "2019"
_4images_image_id: "11786"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11786 -->
Der schafft etwas Stimmung...