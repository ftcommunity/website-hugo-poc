---
layout: "image"
title: "Warm/Helle Beleuchtung"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne07.jpg"
weight: "7"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11788
- /details9ed9.html
imported:
- "2019"
_4images_image_id: "11788"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11788 -->
Sehr wirkungsvoll!