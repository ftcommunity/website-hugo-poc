---
layout: "image"
title: "Richtscheinwerfer"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne02.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11783
- /details4dd4.html
imported:
- "2019"
_4images_image_id: "11783"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11783 -->
Das ist eine Linsenlampe, von der ich die Höhe mit der IR Fehrnsteuerung bewegen. Diese Lampe ist direckt an der Stromquelle.