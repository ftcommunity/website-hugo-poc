---
layout: "image"
title: "UV Scheinwerfer"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne12.jpg"
weight: "12"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11793
- /detailsb0fa.html
imported:
- "2019"
_4images_image_id: "11793"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11793 -->
Wäre so n' billiger UV-Pointer, hat aber Power!