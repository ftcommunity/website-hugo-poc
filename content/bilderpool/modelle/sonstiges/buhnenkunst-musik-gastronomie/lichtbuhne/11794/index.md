---
layout: "image"
title: "Steuerung am IF"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne13.jpg"
weight: "13"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11794
- /details4207-2.html
imported:
- "2019"
_4images_image_id: "11794"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11794 -->
Einer, der ganz links ist für den Blitzer und ist darum nicht am IF.