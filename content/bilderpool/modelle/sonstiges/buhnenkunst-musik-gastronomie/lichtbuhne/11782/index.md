---
layout: "image"
title: "Gesamtansicht"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11782
- /detailscd91-2.html
imported:
- "2019"
_4images_image_id: "11782"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11782 -->
Das ist die Bühne bei "Tag".