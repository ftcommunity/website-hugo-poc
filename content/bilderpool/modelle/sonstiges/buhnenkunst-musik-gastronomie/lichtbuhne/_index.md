---
layout: "overview"
title: "Lichtbühne"
date: 2020-02-22T08:30:04+01:00
legacy_id:
- /php/categories/1055
- /categories0e56.html
- /categoriesf8f7.html
- /categories8fbd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1055 --> 
Eine Bühne die mit Scheinwerfern und co ausgestattet ist.