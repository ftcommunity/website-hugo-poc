---
layout: "image"
title: "Piano (Rückansicht)"
date: "2011-02-15T21:14:52"
picture: "piano3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/29989
- /details37de.html
imported:
- "2019"
_4images_image_id: "29989"
_4images_cat_id: "2211"
_4images_user_id: "1112"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29989 -->
