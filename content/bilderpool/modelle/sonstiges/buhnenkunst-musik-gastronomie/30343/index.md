---
layout: "image"
title: "Sommer_39"
date: "2011-03-31T13:21:39"
picture: "Sommer_4839.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30343
- /detailsacac.html
imported:
- "2019"
_4images_image_id: "30343"
_4images_cat_id: "2121"
_4images_user_id: "4"
_4images_image_date: "2011-03-31T13:21:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30343 -->
Das Bier wurde in Holland gebraut: http://www.ftcommunity.de/details.php?image_id=29218

Mein erstes ft-Avatar-Bildchen ist dabei auch herausgesprungen :-)
