---
layout: "image"
title: "Sommer_38"
date: "2011-03-31T13:20:57"
picture: "Sommer_4838.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30342
- /details682b.html
imported:
- "2019"
_4images_image_id: "30342"
_4images_cat_id: "2121"
_4images_user_id: "4"
_4images_image_date: "2011-03-31T13:20:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30342 -->
Die Liege von der Rückseite her.
