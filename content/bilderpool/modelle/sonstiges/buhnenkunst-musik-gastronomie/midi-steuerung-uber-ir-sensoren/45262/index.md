---
layout: "image"
title: "Sensorhalter von oben"
date: "2017-02-19T14:09:35"
picture: "P1050693_800.jpg"
weight: "1"
konstrukteure: 
- "axel"
fotografen:
- "axel"
schlagworte: ["MIDI", "Tinkerforge", "java"]
uploadBy: "axel"
license: "unknown"
legacy_id:
- /php/details/45262
- /detailsfd5c-2.html
imported:
- "2019"
_4images_image_id: "45262"
_4images_cat_id: "3372"
_4images_user_id: "2056"
_4images_image_date: "2017-02-19T14:09:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45262 -->
Midi Steuerung über Entfernungsmessung
 
https://de.wikipedia.org/wiki/Musical_Instrument_Digital_Interface

Tinkerforge Teile:
Master Brick (1x)
Step-Down Power Supply (z.Zt. nicht angeschlossen) (1x)
Distance IR-Bricklet mit Sharp IR Sensor (2x)

Abhängig von der gemessenen Entfernung wird eine andere Note gespielt. Der rechte Sensor ist für die Auswahl der Note zuständig, der linke Sensor zur Auswahl von Oktave und Stärke. Das zugehörige kleine Java Progrämmchen kommt mit weniger als 200 Zeilen Code aus.