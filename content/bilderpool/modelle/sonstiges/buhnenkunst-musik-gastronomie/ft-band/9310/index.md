---
layout: "image"
title: "Mikrophonhalter"
date: "2007-03-06T18:08:46"
picture: "ftband06.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9310
- /detailsb7da.html
imported:
- "2019"
_4images_image_id: "9310"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9310 -->
Damit auch an der Gitare gesungen werden kann. hier habe ich einen 7.5° Winkelstein verwendet.