---
layout: "image"
title: "hänge Toms und base Drum"
date: "2007-03-06T18:08:46"
picture: "ftband08.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9312
- /details84ad.html
imported:
- "2019"
_4images_image_id: "9312"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9312 -->
Das Fusspedal folgt. Den Winkel der Toms muss jeder ft-Mann selbst bestimmen.