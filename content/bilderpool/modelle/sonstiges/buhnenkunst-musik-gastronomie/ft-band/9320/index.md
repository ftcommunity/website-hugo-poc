---
layout: "image"
title: "Ride Becken"
date: "2007-03-06T18:15:49"
picture: "ftband16.jpg"
weight: "16"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9320
- /detailsed95-3.html
imported:
- "2019"
_4images_image_id: "9320"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:15:49"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9320 -->
Mit einem Eisenbahn Spurkranz verlängert.