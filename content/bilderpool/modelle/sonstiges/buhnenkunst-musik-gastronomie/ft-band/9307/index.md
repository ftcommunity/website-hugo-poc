---
layout: "image"
title: "E-Gitare/Bass"
date: "2007-03-06T18:08:46"
picture: "ftband03.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9307
- /details97d1.html
imported:
- "2019"
_4images_image_id: "9307"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9307 -->
Nochmal das von vorher, einfach ohne ft-Mensch.