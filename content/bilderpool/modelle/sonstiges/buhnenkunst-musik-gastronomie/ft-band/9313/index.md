---
layout: "image"
title: "Von vorne"
date: "2007-03-06T18:08:46"
picture: "ftband09.jpg"
weight: "9"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9313
- /details5bcd.html
imported:
- "2019"
_4images_image_id: "9313"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9313 -->
