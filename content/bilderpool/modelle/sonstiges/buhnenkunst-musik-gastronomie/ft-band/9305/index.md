---
layout: "image"
title: "Keyboard"
date: "2007-03-06T18:08:46"
picture: "ftband01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9305
- /details9e1a.html
imported:
- "2019"
_4images_image_id: "9305"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9305 -->
Ein wichtiger bestandteil ein Keyboard, natürlich mit Synthesycer ;-) .