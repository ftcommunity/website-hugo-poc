---
layout: "image"
title: "Fusspedal"
date: "2007-03-06T18:15:49"
picture: "ftband11.jpg"
weight: "11"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9315
- /details4c5a.html
imported:
- "2019"
_4images_image_id: "9315"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:15:49"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9315 -->
Ein 45° Winkelbaustein.