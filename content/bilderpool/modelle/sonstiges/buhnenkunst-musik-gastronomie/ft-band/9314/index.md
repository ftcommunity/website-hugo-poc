---
layout: "image"
title: "Befestigung der Toms"
date: "2007-03-06T18:08:46"
picture: "ftband10.jpg"
weight: "10"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9314
- /details305b-2.html
imported:
- "2019"
_4images_image_id: "9314"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:08:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9314 -->
Ist nicht sehr Teile sparend, aber es soll doch möglichst nah am Orginal sein.