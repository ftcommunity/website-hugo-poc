---
layout: "image"
title: "Kopplungsstutzen am Bug Typ APAS-89"
date: "2009-09-07T21:17:19"
picture: "sarja2.jpg"
weight: "2"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24886
- /details8f71-2.html
imported:
- "2019"
_4images_image_id: "24886"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24886 -->
Hier gibt es eine Luke die einmal pneumatisch geöffnet werden soll.

An diesem Kopplungsknoten wird Unity andocken. Unity ist ein Verbindungsknoten und Koppelungsadapter für den Anschluss weiterer Module an der noch überschaubaren ISS.