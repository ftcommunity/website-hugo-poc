---
layout: "image"
title: "Flügelmotor"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate11.jpg"
weight: "17"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25907
- /details7e01.html
imported:
- "2019"
_4images_image_id: "25907"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25907 -->
Das ist der Motor, der später den Flügel drehen soll. Über ein Z30 werden Impulse abgenommen, damit man eine halbwegs genaue Positionierung hat (12° pro Zahn ist nicht gerade viel).
In Hintergrund sieht man den Luftspeicher