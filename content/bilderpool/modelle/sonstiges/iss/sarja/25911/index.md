---
layout: "image"
title: "Standbein"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate15.jpg"
weight: "21"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25911
- /detailsb87d.html
imported:
- "2019"
_4images_image_id: "25911"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25911 -->
Runde Dinge rollen...
Also musste für den stabilen Stand ein Standbein her. Damit auch auf leicht geneigtem Boden (welches Haus ist schon exakt gerade) ein stabiler Stand gewährleistet werden kann, habe ich diese LKW hydraulik Stützen verwendet