---
layout: "image"
title: "Ein Flügel"
date: "2009-12-06T19:38:52"
picture: "sarjaupdate03.jpg"
weight: "9"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25899
- /details224f-2.html
imported:
- "2019"
_4images_image_id: "25899"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25899 -->
Im Gegensatz zum echten Modul, hat mein Modell leider nur 3 Abschnitte die als Solarfläche dienen können.
In Wirklichkeit sind es 4 (das vierte ist halb so groß wie die anderen 3).
Leider habe ich nicht genug Solarpanele, dass ich mein Modell tatsächlich mit Strom versorgen konnte, somt mussten Statik-Streben herhalten.
Ein problem bei Solarversorgung wäre auch hier wieder die Gravitation, denn Selbst mit den neuen Panelen wären das immerhin ca. 220 g mit denen jeder Flügel zusätzlich belastet würde.