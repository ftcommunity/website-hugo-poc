---
layout: "image"
title: "Sicherung"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate10.jpg"
weight: "16"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25906
- /detailse5b2-2.html
imported:
- "2019"
_4images_image_id: "25906"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25906 -->
Damit die Belastung der Teile nicht zu hoch wird (der Deckel wiegt ca. 1 kg und ist nur mit drei Teilen am Rest befestigt), habe ich ein Seil gespannt, dass den Deckel festhält.