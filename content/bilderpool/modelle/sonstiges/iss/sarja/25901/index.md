---
layout: "image"
title: "Sarja mit Außentanks"
date: "2009-12-06T19:38:52"
picture: "sarjaupdate05.jpg"
weight: "11"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25901
- /detailsed67-2.html
imported:
- "2019"
_4images_image_id: "25901"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25901 -->
Hier sieht man mal 2 Tanks. Die Flachträger kann man leicht abnehmen. Der Mechanismus wird beim nächsten Bild erklärt.