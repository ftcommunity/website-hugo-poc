---
layout: "image"
title: "Luftversorgung"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate07.jpg"
weight: "13"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25903
- /details7000.html
imported:
- "2019"
_4images_image_id: "25903"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25903 -->
Um pneumatisch Dinge in der ISS bewegen zu können, brauch es auch einen eingebauten Kompressor.
Hier habe ich einen Powermoter genommen, da später 2 Kolben betrieben werden sollen (z.Z. ist leider eines meiner Rückschlagventile kaputt, sodass nur einer möglich ist).