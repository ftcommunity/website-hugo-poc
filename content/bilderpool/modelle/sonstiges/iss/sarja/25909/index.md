---
layout: "image"
title: "Der Kern der Pneumatik"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate13.jpg"
weight: "19"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25909
- /detailsedf4.html
imported:
- "2019"
_4images_image_id: "25909"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25909 -->
Luftspeicher und automatische Druck-Abschaltung.