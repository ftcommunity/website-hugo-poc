---
layout: "image"
title: "Standfuß"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate16.jpg"
weight: "22"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/25912
- /detailsf445.html
imported:
- "2019"
_4images_image_id: "25912"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25912 -->
Auch vorne und hinten brauch es Füße, die stehen bleiben. Damit die Belastung möglichst gut verteilt ist, hat Sarja insgesamt 2 Füße und 2 Beine.