---
layout: "image"
title: "Schwäbischer Adventskranz"
date: "2012-11-24T19:00:27"
picture: "Schwbischer_Adventskranz_01_Upload.jpg"
weight: "69"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36174
- /details6777.html
imported:
- "2019"
_4images_image_id: "36174"
_4images_cat_id: "1180"
_4images_user_id: "1126"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36174 -->
Ökologisch und ökonomisch korrekter Adventskranz - minimaler Ressourcenbedarf, schnell aufgebaut, immergrün, 100% wiederverwendbar, keine CO2-Emissionen.