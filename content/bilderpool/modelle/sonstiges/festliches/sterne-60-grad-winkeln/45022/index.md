---
layout: "image"
title: "3 kaskadierte Sterne"
date: "2017-01-07T20:37:39"
picture: "IMG_1055.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45022
- /details2e25.html
imported:
- "2019"
_4images_image_id: "45022"
_4images_cat_id: "3350"
_4images_user_id: "1359"
_4images_image_date: "2017-01-07T20:37:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45022 -->
