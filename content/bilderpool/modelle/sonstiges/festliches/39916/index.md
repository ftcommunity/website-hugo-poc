---
layout: "image"
title: "Schneemann"
date: "2014-12-11T17:18:30"
picture: "Weihnachtsmann_small.jpg"
weight: "70"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39916
- /detailsca0f.html
imported:
- "2019"
_4images_image_id: "39916"
_4images_cat_id: "1180"
_4images_user_id: "502"
_4images_image_date: "2014-12-11T17:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39916 -->
Inspiriert vom "Snowman" von Richard Mussler-Wright.
