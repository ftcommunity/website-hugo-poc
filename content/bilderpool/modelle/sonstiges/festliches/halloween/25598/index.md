---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-30T21:05:05"
picture: "sm_skull_6.jpg"
weight: "17"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["2009", "Skull"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25598
- /detailscc0d.html
imported:
- "2019"
_4images_image_id: "25598"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25598 -->
This is my 2009 fischertechnik Skull