---
layout: "image"
title: "Jack-o-Lantern"
date: "2008-10-20T21:35:33"
picture: "jol_08_final_fsm.jpg"
weight: "10"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Halloween", "Jack-o-Lantern"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16019
- /detailsed04.html
imported:
- "2019"
_4images_image_id: "16019"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2008-10-20T21:35:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16019 -->
This is one of my entries for the Instructables.com 2008 Halloween contest. The ft jack-o-lantern!