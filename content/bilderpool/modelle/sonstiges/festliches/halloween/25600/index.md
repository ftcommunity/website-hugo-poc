---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-31T14:15:29"
picture: "sm_skull_4_2.jpg"
weight: "19"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["2009", "skull", "halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25600
- /details6793.html
imported:
- "2019"
_4images_image_id: "25600"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-31T14:15:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25600 -->
This is my 2009 fischertechnik Skull