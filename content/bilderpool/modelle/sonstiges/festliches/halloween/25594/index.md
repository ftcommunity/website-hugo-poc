---
layout: "image"
title: "2009 Big Skull"
date: "2009-10-30T21:05:05"
picture: "sm_skull_1.jpg"
weight: "13"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["2009", "skull"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25594
- /details6c27-2.html
imported:
- "2019"
_4images_image_id: "25594"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2009-10-30T21:05:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25594 -->
This is my 2009 fischertechnik Skull