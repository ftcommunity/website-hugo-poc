---
layout: "image"
title: "ft Halloween Bat (Halloween Theme)"
date: "2007-10-25T21:23:01"
picture: "ft-Bat_3.jpg"
weight: "5"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Halloween", "Bat"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12314
- /detailsd1d3.html
imported:
- "2019"
_4images_image_id: "12314"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-25T21:23:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12314 -->
A ft Bat for Halloween!