---
layout: "image"
title: "Halloween Monster"
date: "2007-10-20T12:39:32"
picture: "monster_2b.jpg"
weight: "4"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Halloween", "Monster", "face"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12266
- /details9f1e.html
imported:
- "2019"
_4images_image_id: "12266"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-20T12:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12266 -->
A Monster for Halloween! (Ein Monster für Halloween!-google translation)