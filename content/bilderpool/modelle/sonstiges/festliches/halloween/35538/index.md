---
layout: "image"
title: "Sound Suit"
date: "2012-09-24T21:55:38"
picture: "ft_august_small.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Sound", "Suit", "Halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/35538
- /details25cd.html
imported:
- "2019"
_4images_image_id: "35538"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2012-09-24T21:55:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35538 -->
August is wearing a sound suit (created in spirit of Nick Cave's Sound Suits) as part of the Boise Art Museum parade!