---
layout: "image"
title: "ft Halloween Skull structure"
date: "2007-10-30T18:02:56"
picture: "S_Skull_sm2_2.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/12371
- /details5a88-2.html
imported:
- "2019"
_4images_image_id: "12371"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-30T18:02:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12371 -->
This is a rendering of the fischertechnik structure used for the Halloween Skull model. 


(Dies ist eine Darstellung aus dem fischertechnik Struktur für die Halloween Skull Modell.-google translation)