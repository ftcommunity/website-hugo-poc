---
layout: "image"
title: "Rentier"
date: "2006-12-12T08:54:27"
picture: "Weihnachtsschlitten_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7876
- /detailsbd60.html
imported:
- "2019"
_4images_image_id: "7876"
_4images_cat_id: "739"
_4images_user_id: "328"
_4images_image_date: "2006-12-12T08:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7876 -->
