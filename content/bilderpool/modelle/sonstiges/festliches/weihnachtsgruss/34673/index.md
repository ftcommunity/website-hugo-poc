---
layout: "image"
title: "Santa"
date: "2012-03-23T00:54:35"
picture: "ft_santa.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Santa"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/34673
- /details08a9.html
imported:
- "2019"
_4images_image_id: "34673"
_4images_cat_id: "739"
_4images_user_id: "585"
_4images_image_date: "2012-03-23T00:54:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34673 -->
A few months late. My cubist take on Santa.