---
layout: "image"
title: "Aufzug mit Schneckenantrieb"
date: "2007-12-07T18:24:23"
picture: "aufzug.jpg"
weight: "12"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13017
- /detailsaba0.html
imported:
- "2019"
_4images_image_id: "13017"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-07T18:24:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13017 -->
