---
layout: "image"
title: "Taschenlampe"
date: "2007-12-09T19:43:45"
picture: "taschenlampe.jpg"
weight: "13"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13027
- /detailsabff.html
imported:
- "2019"
_4images_image_id: "13027"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-09T19:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13027 -->
Simpel. Wenn man die Teile für die Fussgängerampel schon hat, geht damit auch fast diese Taschenlampe (bzw. andersrum).