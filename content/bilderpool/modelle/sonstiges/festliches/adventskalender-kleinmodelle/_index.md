---
layout: "overview"
title: "Adventskalender - Kleinmodelle für die Vorweihnachtszeit"
date: 2020-02-22T08:29:15+01:00
legacy_id:
- /php/categories/1179
- /categories4c1a.html
- /categories2d30.html
- /categories3230.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1179 --> 
Die Idee zu einem ft-Adventskalender entstand im November 2007 in den fischertechnik-fanclub-foren. Zielsetzung: Aus ft viele Klein(st)modelle zu konstruieren, damit Kind jeden Tag etwas zu spielen hat. Je kleiner und einfacher, desto besser!



Ich stelle mal ein paar Kleinmodelle ein, die mir dazu auf die Schnelle eingefallen sind. Wäre schön, wenn andere auch ein paar Ideen hätten und die Ergebnisse zeigen würden - damit der Adventskalender 2008 für jeden Tag etwas zu bieten hat ;-)



Viele Grüße, 

Stefan