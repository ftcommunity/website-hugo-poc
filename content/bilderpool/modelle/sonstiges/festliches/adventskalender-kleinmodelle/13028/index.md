---
layout: "image"
title: "Taschenlampe mit Blinklicht"
date: "2007-12-09T19:43:45"
picture: "Taschenlampe_mit_Blinklicht.jpg"
weight: "14"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13028
- /details004f.html
imported:
- "2019"
_4images_image_id: "13028"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-09T19:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13028 -->
Zwischen der Taschenlampe und diesem Modell liegen nur ein paar Bauteile (leider auch die teure Blinkelektronik aus dem 'Lights'-Set). In einer Stellung des Polwendeschalters gibt es Dauerlicht weiss, in der anderen Blinklicht orange.