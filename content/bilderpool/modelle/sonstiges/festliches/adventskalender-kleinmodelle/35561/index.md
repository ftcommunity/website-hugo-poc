---
layout: "image"
title: "Segelflieger mit V-Leitwerk von unten"
date: "2012-09-30T18:09:59"
picture: "segelfliegermitvleitwerk2.jpg"
weight: "17"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35561
- /details6f3b.html
imported:
- "2019"
_4images_image_id: "35561"
_4images_cat_id: "1179"
_4images_user_id: "1196"
_4images_image_date: "2012-09-30T18:09:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35561 -->
Modifikation aus Start 100
