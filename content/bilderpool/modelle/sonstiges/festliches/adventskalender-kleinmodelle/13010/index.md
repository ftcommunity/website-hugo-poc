---
layout: "image"
title: "Roller mit Lenkung"
date: "2007-12-06T19:07:02"
picture: "adv7.jpg"
weight: "7"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13010
- /detailsd993-2.html
imported:
- "2019"
_4images_image_id: "13010"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13010 -->
