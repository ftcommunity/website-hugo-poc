---
layout: "image"
title: "Wechselblinker - manuell mit Handkurbel"
date: "2007-12-12T07:32:25"
picture: "wechselblinker.jpg"
weight: "15"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13034
- /details1307.html
imported:
- "2019"
_4images_image_id: "13034"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-12T07:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13034 -->
Blinken geht auch ohne Elektronik-Blink-Baustein... nämlich wie hier per Handkurbel.