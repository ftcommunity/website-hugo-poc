---
layout: "image"
title: "Advent Sled"
date: "2010-12-15T00:29:52"
picture: "sm_sled.jpg"
weight: "53"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["snow", "sled", "advent", "calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29460
- /details434c.html
imported:
- "2019"
_4images_image_id: "29460"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-15T00:29:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29460 -->
This is a micro build of a sled for the advent calendar.