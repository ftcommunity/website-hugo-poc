---
layout: "overview"
title: "FT Tannenbaum mit Kerzen zum auspusten"
date: 2020-02-22T08:29:24+01:00
legacy_id:
- /php/categories/2822
- /categoriesa8c2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2822 --> 
Am FT-Tannenbaum brennen 6 Kerzen. Schaffst du es, sie alle aus zu pusten???
Wenn ja, dann erwartet dich eine Überraschung.
