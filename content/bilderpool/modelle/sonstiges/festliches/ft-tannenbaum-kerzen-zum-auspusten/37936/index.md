---
layout: "image"
title: "PappKerze-PusteSensor von Seite"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten5.jpg"
weight: "5"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37936
- /details0e4e.html
imported:
- "2019"
_4images_image_id: "37936"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37936 -->
Der "PappKerze-PusteSensor" besteht aus bunter Pappe, 3 Fahrwerksfeder, Flachstein 30, V-Riegel, Kabel und Alufolie.
Am Flachstein haben wir eine Aluflolienkugel befestigt, diese wurde zusammen mit den  Fahrwerksfedern mit Alufolie umwickelt und an einem Kabelende befestigt.
