---
layout: "image"
title: "Advent Ship"
date: "2010-12-14T19:54:34"
picture: "ship_advent_P.jpg"
weight: "48"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["ship", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29455
- /detailsc661.html
imported:
- "2019"
_4images_image_id: "29455"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-14T19:54:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29455 -->
This is a micro build ship for an ft advent calendar