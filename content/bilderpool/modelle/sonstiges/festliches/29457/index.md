---
layout: "image"
title: "Advent Rocking Horse"
date: "2010-12-14T19:54:34"
picture: "rockinghorseA.jpg"
weight: "50"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Advent", "Rocking", "Horse"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29457
- /details100c-2.html
imported:
- "2019"
_4images_image_id: "29457"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-14T19:54:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29457 -->
This isn't my model, but I liked it so much I had to render it! Thanks!