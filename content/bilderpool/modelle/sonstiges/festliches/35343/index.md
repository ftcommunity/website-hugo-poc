---
layout: "image"
title: "Sound Suit"
date: "2012-08-16T13:26:36"
picture: "ft_sound_suit.jpg"
weight: "68"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["sound", "suit"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/35343
- /detailsfce1.html
imported:
- "2019"
_4images_image_id: "35343"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2012-08-16T13:26:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35343 -->
This is a sound suit piece inspired by Nick Cave's "Meet Me at the Center of the Earth" series. This is version one, and I want to fully enclose the headpiece. My son is modelling it.