---
layout: "image"
title: "Scorpion"
date: "2010-07-09T08:53:48"
picture: "scorpion_pic.jpg"
weight: "19"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27736
- /details4065.html
imported:
- "2019"
_4images_image_id: "27736"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-07-09T08:53:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27736 -->
This is a model I created for the Instructables Gift Exchange!