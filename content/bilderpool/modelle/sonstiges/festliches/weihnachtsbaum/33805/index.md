---
layout: "image"
title: "Weihnachten"
date: "2011-12-25T14:29:38"
picture: "schmuck1.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33805
- /detailsab55.html
imported:
- "2019"
_4images_image_id: "33805"
_4images_cat_id: "1507"
_4images_user_id: "453"
_4images_image_date: "2011-12-25T14:29:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33805 -->
