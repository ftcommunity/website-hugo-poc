---
layout: "image"
title: "Klapper-Rentier 3"
date: "2010-12-18T14:56:51"
picture: "Klapper-Rentier_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29487
- /details033e.html
imported:
- "2019"
_4images_image_id: "29487"
_4images_cat_id: "2146"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29487 -->
Ich wünsche allen FT-Freunden und ihren Familien ein frohes und gesundes Weihnachtsfest!!!

Gruß, Thomas