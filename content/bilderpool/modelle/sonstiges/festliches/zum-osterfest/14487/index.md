---
layout: "image"
title: "Easter Egg Robot"
date: "2008-05-08T17:07:59"
picture: "ft-egg_vb_sm_wires_2.jpg"
weight: "12"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["programmable", "Easter", "Egg", "Robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14487
- /details0033.html
imported:
- "2019"
_4images_image_id: "14487"
_4images_cat_id: "1508"
_4images_user_id: "585"
_4images_image_date: "2008-05-08T17:07:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14487 -->
Howdy,

I built a programmable Easter Egg Robot, and posted the building instructions for it on www.instructables.com.
The instructions are for a contest. Please visit the page and vote for me!
http://www.instructables.com/id/fischertechnik--Easter-Egg--Robot/

Richard

***google translation

Ich baute eine programmierbare Easter Egg Robot, und entsandte die Bauanleitung für ihn auf www.instructables.com.
Die Anweisungen sind für einen Wettbewerb. Bitte besuchen Sie die Seite und stimmen Sie für mich!
http://www.instructables.com/id/fischertechnik--Easter-Egg--Robot/

Richard