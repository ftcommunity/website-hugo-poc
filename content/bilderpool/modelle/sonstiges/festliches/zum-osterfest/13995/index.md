---
layout: "image"
title: "Easter Rabbit -3"
date: "2008-03-22T07:45:09"
picture: "ft-rabbit_fr.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Easter", "Rabbit", "egg", "basket"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13995
- /details756a.html
imported:
- "2019"
_4images_image_id: "13995"
_4images_cat_id: "1508"
_4images_user_id: "585"
_4images_image_date: "2008-03-22T07:45:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13995 -->
The Easter Rabbit with egg basket!

(google translation: Die Oster-Ei-Hase mit Korb!)