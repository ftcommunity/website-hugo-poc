---
layout: "image"
title: "Eierausblasvorrichtung3"
date: "2008-03-20T10:00:56"
picture: "Bild5.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/13967
- /details24e5-2.html
imported:
- "2019"
_4images_image_id: "13967"
_4images_cat_id: "1508"
_4images_user_id: "182"
_4images_image_date: "2008-03-20T10:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13967 -->
Hier das Detail der Einspannung
