---
layout: "image"
title: "Osterei"
date: "2008-03-19T16:32:47"
picture: "osterei1.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13963
- /detailsdbb9-3.html
imported:
- "2019"
_4images_image_id: "13963"
_4images_cat_id: "1508"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:32:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13963 -->
