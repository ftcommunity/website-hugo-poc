---
layout: "image"
title: "Advent Snowmobile"
date: "2010-12-16T16:39:46"
picture: "sm_snowmobile.jpg"
weight: "55"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Advent", "snowmobile"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29462
- /detailsa82e.html
imported:
- "2019"
_4images_image_id: "29462"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-16T16:39:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29462 -->
This is a micro snowmobile for the advent calendar. Might be appropriate for all the snow you are receiving!