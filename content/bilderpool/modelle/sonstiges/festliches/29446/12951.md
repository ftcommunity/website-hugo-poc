---
layout: "comment"
hidden: true
title: "12951"
date: "2010-12-16T17:23:08"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Wie mein Vorgänger schon schreibt: sechseckig

Allerdings nur secheckig, denn eine Schneeflocke entsteht - egal wie groß oder komplex - immer als Hexagon. Die Sternlasche ist hier also der richtige "Knotenpunkt"; die äußeren Arme müßten also nur um zwei weitere erweitert werden.

Gruß, Thomas