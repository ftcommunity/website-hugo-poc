---
layout: "image"
title: "Birthday Cake"
date: "2009-05-06T16:03:30"
picture: "b_day_cake.jpg"
weight: "15"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23885
- /details7294.html
imported:
- "2019"
_4images_image_id: "23885"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2009-05-06T16:03:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23885 -->
I made a ft model birthday cake with 5 different kinds of elements.