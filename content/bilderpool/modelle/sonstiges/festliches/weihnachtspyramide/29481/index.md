---
layout: "image"
title: "Weihnachtspyramide 6"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_6.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29481
- /details1ff0-2.html
imported:
- "2019"
_4images_image_id: "29481"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29481 -->
Auf der anderen Seite der Drehscheibe ist ein Schlitten mit dem Weihnachtsmann. Im Grunde soll das Ganze einen drehbaren Schlitten mit vorgespanntem Rentier darstellen.