---
layout: "image"
title: "Weihnachtspyramide 1"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29476
- /details7337.html
imported:
- "2019"
_4images_image_id: "29476"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29476 -->
Mein Modell zeigt eine klassische Weihnachtspyramide im erzgebirger Stil. Natürlich können die Kerzen leuchten und die Pyramide dreht sich.