---
layout: "image"
title: "Weihnachtspyramide 4"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_3.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29479
- /details0d1f.html
imported:
- "2019"
_4images_image_id: "29479"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29479 -->
Detailansicht.