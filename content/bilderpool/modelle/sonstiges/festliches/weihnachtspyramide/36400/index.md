---
layout: "image"
title: "Weihnachtspyramide anderer Antrieb 2"
date: "2013-01-04T12:50:57"
picture: "weihnachtspyramideandererantrieb2.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36400
- /details8d98-2.html
imported:
- "2019"
_4images_image_id: "36400"
_4images_cat_id: "2145"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T12:50:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36400 -->
Dieser Antrieb für die Weihnachtspyramide von thomas004 läuft etwas langsamer als der Antrieb durch zwei Zahnräder in einer - wie ich finde - realistischeren Geschwindigkeit
