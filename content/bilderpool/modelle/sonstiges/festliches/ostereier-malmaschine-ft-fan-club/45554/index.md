---
layout: "image"
title: "Ostereiermalmaschine"
date: "2017-03-18T13:33:45"
picture: "IMG_1109.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45554
- /detailsc243.html
imported:
- "2019"
_4images_image_id: "45554"
_4images_cat_id: "3384"
_4images_user_id: "1359"
_4images_image_date: "2017-03-18T13:33:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45554 -->
