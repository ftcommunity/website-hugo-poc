---
layout: "image"
title: "Advent Rocking Horse"
date: "2010-12-14T20:54:07"
picture: "rockinghorseD.jpg"
weight: "52"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["rocking", "horse", "advent", "calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29459
- /details094a.html
imported:
- "2019"
_4images_image_id: "29459"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-14T20:54:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29459 -->
This is not my model...but I loved it so much I had to render it!