---
layout: "image"
title: "Yule Tree"
date: "2008-12-06T12:02:38"
picture: "sm_xmas_tree3.jpg"
weight: "13"
konstrukteure: 
- "Richard Mussler-Wright, Laura Baran, David Chase"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Christmas", "Tree"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16551
- /details91bc-2.html
imported:
- "2019"
_4images_image_id: "16551"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2008-12-06T12:02:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16551 -->
Another image of the tree!