---
layout: "image"
title: "Jack-O-Lantern"
date: "2008-10-22T21:48:36"
picture: "JOL_08_pic1.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Jack-O-Lantern"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16035
- /details8f71.html
imported:
- "2019"
_4images_image_id: "16035"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2008-10-22T21:48:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16035 -->
This is a picture of my entry into the 2008 Instructables DIY Halloween contest.