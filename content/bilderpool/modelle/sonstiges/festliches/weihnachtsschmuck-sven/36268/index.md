---
layout: "image"
title: "Kugel grün"
date: "2012-12-13T19:31:22"
picture: "kugel3.jpg"
weight: "14"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36268
- /detailsd81f-3.html
imported:
- "2019"
_4images_image_id: "36268"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36268 -->
Die grüne Kugel ist ähnlich der blauen, aber hier ist kein Drehkranz verbaut worde.
Grund und Deckel bestehen aus Winkelsteinen 60° + BS 7,5.
