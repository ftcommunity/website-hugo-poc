---
layout: "image"
title: "Kugeln grün & blau"
date: "2012-12-13T19:31:22"
picture: "kugel6.jpg"
weight: "17"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36271
- /detailsb7ef-3.html
imported:
- "2019"
_4images_image_id: "36271"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36271 -->
Hier beide Kugeln zusammen.
