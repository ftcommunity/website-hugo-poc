---
layout: "image"
title: "Kugel grün"
date: "2012-12-13T19:31:22"
picture: "kugel4.jpg"
weight: "15"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36269
- /detailsc5c5-2.html
imported:
- "2019"
_4images_image_id: "36269"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36269 -->
Hier sieht man die BS 7,5 + Winkelsteine 60°
