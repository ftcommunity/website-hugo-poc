---
layout: "image"
title: "Weihnachtskugel"
date: "2012-12-12T15:41:00"
picture: "weihnachten1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36255
- /detailse2ad-3.html
imported:
- "2019"
_4images_image_id: "36255"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T15:41:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36255 -->
Im original ft Weihnachtsschmuck den es vor ein paar Jahren mal gab waren Kugeln aus Flachträgern enthalten.
Da der Weihnachtsschmuck von ft letztes und auch dieses Jahr mit den Flexschinen umgesetzt wurde (die Glocken) kam mir die Idee eine Kugel mit den Flexschinen zu bauen.
Das Ergebnis sieht man nun hier.
