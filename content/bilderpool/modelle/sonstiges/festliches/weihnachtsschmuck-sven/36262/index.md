---
layout: "image"
title: "großer Stern"
date: "2012-12-12T16:16:39"
picture: "weihnachstsschmuck2.jpg"
weight: "8"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/36262
- /detailsd382-2.html
imported:
- "2019"
_4images_image_id: "36262"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T16:16:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36262 -->
