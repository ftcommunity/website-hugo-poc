---
layout: "image"
title: "Christmas Fireplace with Yeloow Stockings"
date: "2010-12-05T15:10:58"
picture: "ft_fireplace.jpg"
weight: "33"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["fireplace", "advent", "calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29413
- /details1903.html
imported:
- "2019"
_4images_image_id: "29413"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-05T15:10:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29413 -->
A Christmastime fireplace. This is a model for an Advent Calendar. (Note-I will render these models when I get a chance).