---
layout: "image"
title: "Advent Elf"
date: "2010-12-09T21:46:33"
picture: "elf_1.jpg"
weight: "42"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Advent", "Elf"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29443
- /details6c5d.html
imported:
- "2019"
_4images_image_id: "29443"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-09T21:46:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29443 -->
This is my first attempt at an elf for the Advent Calendar.