---
layout: "image"
title: "Advent Plane"
date: "2010-12-17T18:09:16"
picture: "advent_plane.jpg"
weight: "58"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Advent", "airplane"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29465
- /detailsc545.html
imported:
- "2019"
_4images_image_id: "29465"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-17T18:09:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29465 -->
A small airplane for advent calendar.