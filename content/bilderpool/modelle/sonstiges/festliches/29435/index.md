---
layout: "image"
title: "Yule Reindeer"
date: "2010-12-08T12:29:16"
picture: "flying-raindeerA_2.jpg"
weight: "39"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Yule", "Reindeer", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/29435
- /detailse94e-4.html
imported:
- "2019"
_4images_image_id: "29435"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-08T12:29:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29435 -->
A Yule Reindeer for the Advent Calendar