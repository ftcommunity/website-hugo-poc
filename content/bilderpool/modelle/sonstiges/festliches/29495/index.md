---
layout: "image"
title: "Schwibbogen Verkabelung"
date: "2010-12-20T19:29:25"
picture: "schwibbogen1_2.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/29495
- /details0422-2.html
imported:
- "2019"
_4images_image_id: "29495"
_4images_cat_id: "1180"
_4images_user_id: "791"
_4images_image_date: "2010-12-20T19:29:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29495 -->
Hier sieht man die Verkabelung.