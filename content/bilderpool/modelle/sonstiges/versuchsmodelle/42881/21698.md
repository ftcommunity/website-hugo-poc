---
layout: "comment"
hidden: true
title: "21698"
date: "2016-02-16T22:20:42"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Lurchi!
Meine Güte, auch deine anderen Modelle, du willst ja hoch hinaus! Mit irgendwelchem Klein-Klein gibst du dich eher nicht ab, was? Wirklich sehr beeindruckend.
Und der Raum, ist das ein extra Fischertechnik-Präsentationsraum? Der bringt das supergut zur Geltung.
Martin