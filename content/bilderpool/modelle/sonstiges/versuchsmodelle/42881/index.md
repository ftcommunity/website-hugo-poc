---
layout: "image"
title: "U-Träger-Tragwerk 5"
date: "2016-02-15T22:41:37"
picture: "IMG_3279.jpg"
weight: "5"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42881
- /details29bb.html
imported:
- "2019"
_4images_image_id: "42881"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42881 -->
Ansicht der wuchtigen Gesamtkonstruktion von unten nach oben.