---
layout: "image"
title: "U-Träger-Brückentragwerk 4"
date: "2016-02-15T22:41:37"
picture: "IMG_3295.jpg"
weight: "11"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42887
- /details37d2-2.html
imported:
- "2019"
_4images_image_id: "42887"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42887 -->
Einblick in die innere Tragwerkkonstruktion.