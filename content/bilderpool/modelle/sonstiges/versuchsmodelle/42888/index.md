---
layout: "image"
title: "U-Träger-Tragwerk 2"
date: "2016-02-15T22:41:37"
picture: "IMG_3314.jpg"
weight: "12"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42888
- /detailsb873.html
imported:
- "2019"
_4images_image_id: "42888"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42888 -->
Ansicht eines Hauptträgers mit dem Maß 30 x 30 mm

Zur besseren Stabilität wurden die U-Träger-Bündel fortlaufend immer um 90° versetzt.