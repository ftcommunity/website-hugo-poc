---
layout: "image"
title: "Schwenkrollenwagen 1"
date: "2016-02-15T22:41:37"
picture: "IMG_3320.jpg"
weight: "13"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42889
- /details196a.html
imported:
- "2019"
_4images_image_id: "42889"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42889 -->
Modifizierte Konstruktion eines Schwenkrollenwagens.

Das Originalmodell stammt aus der Bauanleitung des Grundkastens 50/2 (30141, mit kleiner Wanne) von 1975.