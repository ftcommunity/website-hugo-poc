---
layout: "image"
title: "U-Träger-Brückentragwerk 2"
date: "2016-02-15T22:41:37"
picture: "IMG_3309.jpg"
weight: "9"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42885
- /details6f9f.html
imported:
- "2019"
_4images_image_id: "42885"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42885 -->
Die Gesamtlänge des Gesamttragwerks ergibt sich aus der Summe der Winkelträger:

16 x WT 120 (= 1920 mm) + 1 x WT 60 (60 mm) + 1 WT 30 (30 mm) = 2010 mm