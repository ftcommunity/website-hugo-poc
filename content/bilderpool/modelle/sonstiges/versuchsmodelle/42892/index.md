---
layout: "image"
title: "Träger/Kabelkanal 2"
date: "2016-02-15T22:41:37"
picture: "IMG_3328.jpg"
weight: "16"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/42892
- /detailsb059-2.html
imported:
- "2019"
_4images_image_id: "42892"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42892 -->
Einblick in das Innere des Trägers.

Durch einen geschickten Versatz der Winkelträger kann der innere Hohlraum durchgehend als Kabelkanal genutzt werden.