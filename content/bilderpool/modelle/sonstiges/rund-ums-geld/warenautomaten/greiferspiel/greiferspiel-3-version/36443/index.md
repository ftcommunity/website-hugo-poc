---
layout: "image"
title: "Greiferspiel (Gesamt)"
date: "2013-01-06T18:56:57"
picture: "greiferspielversion1.jpg"
weight: "1"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/36443
- /details53d0.html
imported:
- "2019"
_4images_image_id: "36443"
_4images_cat_id: "2706"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T18:56:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36443 -->
Hier sieht man das komplette Spiel. Am Hubgetriebe hängen Motor und Kompressor.