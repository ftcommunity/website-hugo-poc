---
layout: "overview"
title: "Greiferspiel 3. Version"
date: 2020-02-22T08:28:31+01:00
legacy_id:
- /php/categories/2706
- /categories91f6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2706 --> 
Hier ist nochmal eine Neuauflage meines Greiferspiels. Die Bauzeit war diesmal noch viel länger, vor allem die Entwicklung des Greifers. Ein Video kann man hier sehen: http://henkel.homedns.org/Fischertechnik/greiferspiel.mp4