---
layout: "image"
title: "Greiferspiel Mechanik 2"
date: "2010-04-07T12:40:32"
picture: "greiferspiel3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26891
- /detailsde4b.html
imported:
- "2019"
_4images_image_id: "26891"
_4images_cat_id: "1927"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26891 -->
