---
layout: "image"
title: "Greiferspiel Greifermechanik"
date: "2010-04-07T12:40:32"
picture: "greiferspiel4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26892
- /details58e5.html
imported:
- "2019"
_4images_image_id: "26892"
_4images_cat_id: "1927"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26892 -->
