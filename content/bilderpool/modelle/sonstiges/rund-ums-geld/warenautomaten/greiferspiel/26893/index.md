---
layout: "image"
title: "Greiferspiel"
date: "2010-04-07T12:40:32"
picture: "greiferspiel5.jpg"
weight: "5"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26893
- /details03f3.html
imported:
- "2019"
_4images_image_id: "26893"
_4images_cat_id: "1927"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26893 -->
