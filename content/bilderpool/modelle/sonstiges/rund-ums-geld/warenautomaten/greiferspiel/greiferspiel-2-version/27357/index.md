---
layout: "image"
title: "Greiferspiel 2.Version Gesamtansicht"
date: "2010-06-04T10:54:30"
picture: "greiferspielversion1.jpg"
weight: "1"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27357
- /details477b.html
imported:
- "2019"
_4images_image_id: "27357"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27357 -->
Diesmal auch mit schöner Verkabelung.