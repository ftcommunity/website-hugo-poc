---
layout: "image"
title: "Greiferspiel 2.Version Antrieb"
date: "2010-06-04T10:54:31"
picture: "greiferspielversion4.jpg"
weight: "4"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27360
- /details6a7b.html
imported:
- "2019"
_4images_image_id: "27360"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27360 -->
Diese Übersetzung ist genau passend für ein schnelles, aber trotzdem präzises Spiel. Natürlich sind überall Endtaster montiert, damit die Endpositionen nicht überschritten werden können.