---
layout: "image"
title: "Greiferspiel 2.Version Senkfunktion"
date: "2010-06-04T10:54:30"
picture: "greiferspielversion2.jpg"
weight: "2"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27358
- /details6793-3.html
imported:
- "2019"
_4images_image_id: "27358"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27358 -->
Der Greifer wird über die FT-Kette hinunter gelassen. Dadurch schwankt und dreht er sich nur noch minimal. Über die Lichtschranke erfährt das Robo-Interface, wann der Greifer unten angekommen ist. Das ist genau eine Umdrehung des Zahnrades. Ich habe allerdings die anderen zwei Löcher des Zahnrades mit Isolierband abgeklebt.