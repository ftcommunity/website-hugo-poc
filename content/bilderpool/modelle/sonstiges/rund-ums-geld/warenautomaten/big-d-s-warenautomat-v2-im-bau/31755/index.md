---
layout: "image"
title: "Komplettansicht"
date: "2011-09-09T07:41:48"
picture: "bigdswarenautomat01.jpg"
weight: "1"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31755
- /details6ebc.html
imported:
- "2019"
_4images_image_id: "31755"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31755 -->
Hier sieht man die ganze Front.