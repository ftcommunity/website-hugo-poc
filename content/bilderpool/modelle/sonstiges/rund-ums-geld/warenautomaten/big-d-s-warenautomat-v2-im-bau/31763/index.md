---
layout: "image"
title: "Noch ein Schlüsselschalter..."
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat09.jpg"
weight: "9"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/31763
- /detailsa0de.html
imported:
- "2019"
_4images_image_id: "31763"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31763 -->
dahinter: dreifach (!!!) alarmgesichert das Geldfach.