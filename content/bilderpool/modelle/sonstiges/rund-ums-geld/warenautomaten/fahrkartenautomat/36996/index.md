---
layout: "image"
title: "Fahrkartenautomat 2 mit Touchscreen"
date: "2013-05-27T15:45:22"
picture: "Automat1.jpg"
weight: "8"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/36996
- /detailsc990.html
imported:
- "2019"
_4images_image_id: "36996"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36996 -->
Hier seht ihr den verbesserten Fahrkartenautomat mit meinem Handy als eingebauten Touchscreen. Dies habe ich mit einer App realisiert, die ich selbst programmiert und designt habe. Leider habe ich es nicht geschafft Befehle zwischen Handy und Interface kommunizieren zu lassen, daher ist das Handy auf einer Wippe gelagert, die nachgibt, wenn man rechts oder links draufdrückt und der Befehl wird dann an einen Taster weitergegeben.
Der Geldschacht ist unten, der Rest erklärt sich eigentlich von selbst.