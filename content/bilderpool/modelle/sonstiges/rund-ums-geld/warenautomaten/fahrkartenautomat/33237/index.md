---
layout: "image"
title: "Münzrückgabe"
date: "2011-10-19T16:49:06"
picture: "DSCF7746.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33237
- /detailsfc4e.html
imported:
- "2019"
_4images_image_id: "33237"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33237 -->
Der auf dem Bild grüne Pfeil beschreibt den Auswurf der Münze durch die Rohrleitung.
Der orangene Pfeil zeigt das hinauf und hinunterfahren, welches für jede Münze wiederholt werden muss.