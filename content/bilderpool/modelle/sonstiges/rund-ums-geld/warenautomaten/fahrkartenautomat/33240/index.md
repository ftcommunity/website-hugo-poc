---
layout: "image"
title: "Bildschirm von hinten"
date: "2011-10-19T16:49:06"
picture: "DSCF7752.jpg"
weight: "6"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33240
- /details9219.html
imported:
- "2019"
_4images_image_id: "33240"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33240 -->
Der Bildschirm wird über eine Schnecke (rechts) hoch und runtergefahren. (Er ist das weiße Objekt in der Mitte) Rechts neben der Schnecke befinden sich die Tasteranschläge um die Stellung des Bildschirmes zu überwachen.