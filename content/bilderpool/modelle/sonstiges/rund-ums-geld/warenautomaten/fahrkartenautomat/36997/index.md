---
layout: "image"
title: "Detailansicht des Touchscreens."
date: "2013-05-27T15:45:22"
picture: "Automat2.jpg"
weight: "9"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/36997
- /details38a2-2.html
imported:
- "2019"
_4images_image_id: "36997"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36997 -->
Die Detailansicht mit der selbst programmierten App. Man sieht auch gut die Lagerung des ganzen Handys...