---
layout: "overview"
title: "Warenautomaten"
date: 2020-02-22T08:28:30+01:00
legacy_id:
- /php/categories/2009
- /categoriesc0eb.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2009 --> 
Geräte, die gegen Geldeinwurf eine Ware ausgeben.