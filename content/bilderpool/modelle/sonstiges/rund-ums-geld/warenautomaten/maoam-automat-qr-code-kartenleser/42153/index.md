---
layout: "image"
title: "Die Steuereinheit"
date: "2015-10-26T14:47:12"
picture: "DSC08337_1.jpg"
weight: "4"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42153
- /detailsa753.html
imported:
- "2019"
_4images_image_id: "42153"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42153 -->
Robo Extention für die Steuerung, Kompressor und kleiner Druckspeicher (aus Profi Pneumatic Power) und Magnetventil (von eBay)

Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168
