---
layout: "image"
title: "Gesamtansicht von schräg oben, mit Gehäuse"
date: "2015-11-07T18:07:35"
picture: "DSC08440_sc01.jpg"
weight: "10"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42297
- /detailsadb4.html
imported:
- "2019"
_4images_image_id: "42297"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-11-07T18:07:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42297 -->
