---
layout: "image"
title: "Die Warenschächte"
date: "2015-10-26T14:47:12"
picture: "DSC08336_1.jpg"
weight: "3"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42152
- /detailsbc3b.html
imported:
- "2019"
_4images_image_id: "42152"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42152 -->
Zum Auffüllen wird der Haltebügel hochgeklappt und gibt dann den Zugriff auf alle fünf Warenschächte frei

Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168
