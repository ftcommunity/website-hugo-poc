---
layout: "image"
title: "Gesamtansicht von schräg oben"
date: "2015-10-26T17:17:22"
picture: "DSC08340_1.jpg"
weight: "7"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42156
- /detailsf9b0.html
imported:
- "2019"
_4images_image_id: "42156"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T17:17:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42156 -->
Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168
