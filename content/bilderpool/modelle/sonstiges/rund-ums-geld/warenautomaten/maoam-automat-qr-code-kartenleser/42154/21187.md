---
layout: "comment"
hidden: true
title: "21187"
date: "2015-11-02T21:33:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist der Grund, warum ich immer gegen schwarz als Bausteinfarbe zetere: Es ist so besch...eiden zu fotografieren. Etwas Abhilfe gibt's per Bildbearbeitung (und falls Du Windows verwenden solltest, über den ft Community Publisher) mit der "Gamma-Korrektur", die dunkle Stellen aufhellt, helle aber nicht überstrahlen lässt.

Gruß,
Stefan