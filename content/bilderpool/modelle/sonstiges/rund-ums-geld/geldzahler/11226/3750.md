---
layout: "comment"
hidden: true
title: "3750"
date: "2007-07-30T09:52:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
>> Vereinzelug? Dass man alle Scheine einlegt und der jeweils nur einen zieht?

Ja, genau das. Ich denke Harald meinte dasselbe. Das wäre eine nette Ergänzung und bestimmt auch eine ganz schöne Herausforderung.

Gruß,
Stefan