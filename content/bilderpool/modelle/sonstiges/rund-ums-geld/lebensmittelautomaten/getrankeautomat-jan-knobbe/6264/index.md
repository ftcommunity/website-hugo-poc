---
layout: "image"
title: "Getränkeautomat von oben"
date: "2006-05-11T20:00:14"
picture: "getraenkeautomat_von_oben.jpg"
weight: "5"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6264
- /detailsdcc9.html
imported:
- "2019"
_4images_image_id: "6264"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T20:00:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6264 -->
Blick hinein. Die 2 Sorten Brause werden durch Tabletten erzeugt, von denen jeweils ein kleiner Vorrat vorhanden ist. Nicht ganz leicht: Beide Tablettensorten müssen am Beginn des Programmablaufs in den gleichen Becher fallen können.