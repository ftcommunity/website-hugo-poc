---
layout: "image"
title: "Getränkeautomat von vorn"
date: "2006-05-11T19:57:16"
picture: "getraenkeautomat_von_vorn.jpg"
weight: "1"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6260
- /details10d1-3.html
imported:
- "2019"
_4images_image_id: "6260"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T19:57:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6260 -->
Ganz rechts der Münzeinwurf (Lichtschranke), daneben ein Schalter, mit dem man zwischen 2 Geschmacksrichtungen wählen kann.

Programmablauf:

1. Wahl zwischen zwei Brausesorten

2. Geldeinwurf, sofort wird das betreffende Förderband mit der gewünschten Brausetablette in Bewegung gesetzt.

3. Liegt die Tablette im Becher, wird dieser nach vorn geschoben.

4. Ist der Becher vorn, läuft die Pumpe an. Die Zahl der Zähne auf dem Zahnrad, das von der Motorschnecke angetriben wird entspricht genau der zahl der Kettenglieder zwischen zwei bechern. So ist gewährleistet, dass nach genau einer Zahnrad- bzw. Achsenumdrehung alle becher wieder richtig stehen. - Gleichzeitig werden zwei kurze Achsen in den Becher abgesenkt (Fühler), damit er nicht überläuft.

5. Sobald die beiden Achsen mit Flüssigkeit in Berührung kommen, wird die Pumpe ausgeschaltet und der Fühler wird hochgezogen. Fertig!