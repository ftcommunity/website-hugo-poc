---
layout: "image"
title: "Koch-Temperaturregler"
date: "2006-05-18T16:57:11"
picture: "ft_3_005.jpg"
weight: "5"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/6276
- /detailsf32f-2.html
imported:
- "2019"
_4images_image_id: "6276"
_4images_cat_id: "546"
_4images_user_id: "420"
_4images_image_date: "2006-05-18T16:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6276 -->
