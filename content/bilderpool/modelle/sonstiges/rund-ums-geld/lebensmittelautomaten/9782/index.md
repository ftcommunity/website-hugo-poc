---
layout: "image"
title: "Schokoladenspender"
date: "2007-03-25T10:30:47"
picture: "jpg3.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9782
- /details9e7c-2.html
imported:
- "2019"
_4images_image_id: "9782"
_4images_cat_id: "2332"
_4images_user_id: "557"
_4images_image_date: "2007-03-25T10:30:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9782 -->
Bei Münzeinwurf wird ein schokoladenstück ausgegeben(Vorgangsleuchte)