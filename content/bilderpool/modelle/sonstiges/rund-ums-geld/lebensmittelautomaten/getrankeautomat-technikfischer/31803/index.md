---
layout: "image"
title: "Antrieb für die Kakaoschnecke"
date: "2011-09-14T19:23:08"
picture: "technikfischer10.jpg"
weight: "10"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31803
- /details3c72.html
imported:
- "2019"
_4images_image_id: "31803"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31803 -->
Wenn die Schnecke gedreht wird, wird das Pulver durch ein Loch in den Becher befördert