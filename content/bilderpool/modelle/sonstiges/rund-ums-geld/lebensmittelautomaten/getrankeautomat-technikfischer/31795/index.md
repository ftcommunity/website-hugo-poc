---
layout: "image"
title: "Von der Seite"
date: "2011-09-14T19:23:07"
picture: "technikfischer02.jpg"
weight: "2"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31795
- /details33af.html
imported:
- "2019"
_4images_image_id: "31795"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31795 -->
Man sieht ganz rechts den Einfüller und links auf dem Gerüst den Mixer

Programmm : http://www.ftcommunity.de/data/downloads/robopro/getraenkeautomat.rpp