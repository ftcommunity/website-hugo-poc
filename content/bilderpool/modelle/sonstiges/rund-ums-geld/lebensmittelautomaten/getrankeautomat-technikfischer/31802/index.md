---
layout: "image"
title: "Kakaobehälter"
date: "2011-09-14T19:23:08"
picture: "technikfischer09.jpg"
weight: "9"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31802
- /details82bf-2.html
imported:
- "2019"
_4images_image_id: "31802"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31802 -->
An dieser Stelle möchte ich herzlich bei scripter1 bedanken, der mir die Idee gebracht hat und mich mit Plänen zum Kakaobehälter unterstüzt hat