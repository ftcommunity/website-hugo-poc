---
layout: "image"
title: "Halterung für Schraubendreher"
date: "2010-09-28T16:46:06"
picture: "ag11.jpg"
weight: "11"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28696
- /details8087-2.html
imported:
- "2019"
_4images_image_id: "28696"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28696 -->
Hier könnt ihr meine Halterung für den Schraubendreher sehen. Wenn man nämlich mit dem Modell irgendwo hingeht, und man hat seinen Schraubendreher vergessen, dann steht man ganz schön dumm da, deswegen eine extra Halterung für ihn.
