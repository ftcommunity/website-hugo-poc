---
layout: "image"
title: "Becher"
date: "2010-09-28T16:46:06"
picture: "ag15.jpg"
weight: "15"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28700
- /details4dc0.html
imported:
- "2019"
_4images_image_id: "28700"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:06"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28700 -->
Hier könnt ihr eine handelüblichen Einweg-Becher sehen, der hinter gefahren wurde, und gerade befüllt wird.

Ich hoffe ich habe euch ein paar Eindrücke von meiner Wasserspender gezeigt, und gute Bilder gemacht. Schreibt doch Kommentare, wenns euch gefällt, oder ihr Verbesserungsvorschläge habt. 

MfG
Endlich

Hier der Wasserspender auf der Convention mit kleinen Veränderungen: http://www.ftcommunity.de/details.php?image_id=28399#col3
