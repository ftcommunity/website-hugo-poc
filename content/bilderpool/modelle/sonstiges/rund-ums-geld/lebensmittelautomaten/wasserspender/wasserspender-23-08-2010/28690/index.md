---
layout: "image"
title: "Einwurf in den Geldschlitz"
date: "2010-09-28T16:46:05"
picture: "ag05.jpg"
weight: "5"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28690
- /details855c.html
imported:
- "2019"
_4images_image_id: "28690"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28690 -->
Hier kann man meine Finger sehen, die gerade eine 50ct Münze in den Geldschlitz schmeißen.
