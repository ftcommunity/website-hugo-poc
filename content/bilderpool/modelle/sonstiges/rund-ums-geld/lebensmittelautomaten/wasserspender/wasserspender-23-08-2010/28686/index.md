---
layout: "image"
title: "Wasserspender"
date: "2010-09-28T16:46:03"
picture: "ag01.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28686
- /details578e-2.html
imported:
- "2019"
_4images_image_id: "28686"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28686 -->
Jetzt endlich, nach der Convention stelle ich die Bilder hoch, sie sind zwar schon ein bisschen älter, aber im Prinzip gleich. 

Hier könnt ihr meinen Wasserspender sehen (von 23.08.2010) man kann sehen, er ist jetzt voll automatisiert und er hat jetzt zwei Bauplatten 500, der TX ist eingebaut, und es sind jetzt zwei Flaschen. 

Ich hoffe euch gefällt mein Wasserspender, und die Bilder.

MfG
Endlich

PS: der Wasserspender auf der Convention, mit kleinen Veränderungen: http://www.ftcommunity.de/details.php?image_id=28399
