---
layout: "image"
title: "Bedienfeld"
date: "2010-09-28T16:46:04"
picture: "ag03.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28688
- /detailsee8e.html
imported:
- "2019"
_4images_image_id: "28688"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28688 -->
Hier sieht man das Bedienfeld man kann zwieschen zwei Sorten Wasser wählen. Die Statuslampen zeigen den Status der Maschine an. Rechts sieht man den Geldschlitz. Eine Münzprüfung, ob man 1€, 50ct,.. hineinschmeißt findet nicht statt.
