---
layout: "image"
title: "Die Tage des Wasserspenders sind gezählt"
date: "2011-12-31T13:13:32"
picture: "wasserspender1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33822
- /details1df9-2.html
imported:
- "2019"
_4images_image_id: "33822"
_4images_cat_id: "2500"
_4images_user_id: "1162"
_4images_image_date: "2011-12-31T13:13:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33822 -->
