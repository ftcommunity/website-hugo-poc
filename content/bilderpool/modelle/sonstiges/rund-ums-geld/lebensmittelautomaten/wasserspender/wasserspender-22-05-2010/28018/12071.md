---
layout: "comment"
hidden: true
title: "12071"
date: "2010-08-31T07:02:00"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Doch, auf langfristige Zeit schon, aber damals habe ich nicht drüber nachgedacht. Jetzt, wenn die Zylinder ein weilchen unten muss mon sie vom Boden "wegreißen" aber es hält sich noch in Grenzen. Ich nehme jetzt auch nicht mehr die Zylinder zum Füllen des Tanks, sondern ich habe eine Wasserflasche, bei der man nur den Deckel aufschrauben muss und schneller füllen kann. Siehe hier:   http://www.ftcommunity.de/details.php?image_id=27662

MfG
Endlich