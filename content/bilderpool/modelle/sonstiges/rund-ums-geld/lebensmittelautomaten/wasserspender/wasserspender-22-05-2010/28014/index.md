---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T16:04:38"
picture: "wasserspender1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28014
- /detailscba7.html
imported:
- "2019"
_4images_image_id: "28014"
_4images_cat_id: "2031"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T16:04:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28014 -->
Hier könnt ihr meine FT-Wasserspender sehen. Er hat jetzt schon einen kleinen Fortschrit. Und zwar ist die Kompressoreinheit jetzt geschützt.

Weitere Bilder folgen noch über die anderen Versionen. 

MfG
Endlich
