---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T16:04:39"
picture: "wasserspender4.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28017
- /detailsbf66.html
imported:
- "2019"
_4images_image_id: "28017"
_4images_cat_id: "2031"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T16:04:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28017 -->
Hier seht ihr die Pumpe, mit der man Wasser in den Tank bekommt. Man hält den Schlauch an den Zylindern ins Wasser, zieht, hält dann den Schlauch an den Tank und drückt, so kommt immer ein bisschen Wasser in den Tank.
