---
layout: "image"
title: "Rückseite"
date: "2015-12-07T17:49:00"
picture: "schorobot08.jpg"
weight: "8"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42486
- /detailsb94d.html
imported:
- "2019"
_4images_image_id: "42486"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42486 -->
Auf diesem Bild kann man die Rückseite des Automatens sehen, gut zu erkennen ist vor allem die Laufkatze oben im Bild, die sich leider in Ermangelung eines weiteren Aluminiumprofils teilweise sehr stark durchgebogen hat. Unten Links im Bild sieht man auch den Pneumatikzylinder, der die fünfte Schokoladensorte ausgibt.