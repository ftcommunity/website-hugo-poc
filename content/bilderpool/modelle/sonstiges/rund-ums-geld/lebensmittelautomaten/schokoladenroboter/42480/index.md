---
layout: "image"
title: "Die Kartusche (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot02.jpg"
weight: "2"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42480
- /details4563.html
imported:
- "2019"
_4images_image_id: "42480"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42480 -->
Hier ist die Kartusche noch mal zu sehen, diesmal von oben.