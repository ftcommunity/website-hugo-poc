---
layout: "image"
title: "Tasterleiste"
date: "2015-12-07T17:49:00"
picture: "schorobot11.jpg"
weight: "11"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42489
- /detailsce88-2.html
imported:
- "2019"
_4images_image_id: "42489"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42489 -->
Hier sieht man die Tasterleiste, die zur genauen Positionierung des Saughebers nötig sind. Diese Taster sind sehr einfach zu verschieben, was eine spätere Feinjustierung sehr einfach macht