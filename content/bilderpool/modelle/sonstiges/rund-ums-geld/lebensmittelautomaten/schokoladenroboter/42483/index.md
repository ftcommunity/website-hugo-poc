---
layout: "image"
title: "Saugheber (3)"
date: "2015-12-07T17:49:00"
picture: "schorobot05.jpg"
weight: "5"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42483
- /detailsf377-2.html
imported:
- "2019"
_4images_image_id: "42483"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42483 -->
Hier ist der Saugheber mit einem Teil der Laufkatze zu sehen, gut erkennbar an den beiden roten Zahnrädern.