---
layout: "overview"
title: "Schokoladenroboter"
date: 2020-02-22T08:28:56+01:00
legacy_id:
- /php/categories/3158
- /categoriesb545.html
- /categories9185.html
- /categories2d08.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3158 --> 
Dieser Automat liefert - natürlich erst nach Geldeinwurf - die Gewünschte Schokoladensorte, indem die Schokolade per Vakuumsauger angehoben und auf ein Ausgabeförderband gegeben wird. Der Kunde hat dabei die Wahl zwischen vier beziehungsweise fünf Sorten. Vier Sorten werden nämlich über den Sauger transportiert, die fünfte am hinteren Ende des Förderbandes. Dort wird die Schokolade über einen Pneumatikzylinder portioniert und ebenfalls über das Transportband zum Kunde gebracht.