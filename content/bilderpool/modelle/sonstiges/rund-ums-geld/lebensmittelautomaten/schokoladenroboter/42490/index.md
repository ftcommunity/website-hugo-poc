---
layout: "image"
title: "Tasterleiste (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot12.jpg"
weight: "12"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42490
- /details4c9a-3.html
imported:
- "2019"
_4images_image_id: "42490"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42490 -->
Diesmal die Tasterleiste von oben. Gut zu erkennen ist hier der Kabelbaum.