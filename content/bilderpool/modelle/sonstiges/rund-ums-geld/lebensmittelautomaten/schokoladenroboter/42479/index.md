---
layout: "image"
title: "Die Kartusche"
date: "2015-12-07T17:49:00"
picture: "schorobot01.jpg"
weight: "1"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42479
- /detailsc50c.html
imported:
- "2019"
_4images_image_id: "42479"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42479 -->
In dieser Kartusche werden die ersten vier Schokoladensorten aufbewahrt.