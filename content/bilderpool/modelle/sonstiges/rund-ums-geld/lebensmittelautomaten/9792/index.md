---
layout: "image"
title: "Schokoladenspender_"
date: "2007-03-27T12:30:44"
picture: "_opr08O8L_2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9792
- /details7bba-2.html
imported:
- "2019"
_4images_image_id: "9792"
_4images_cat_id: "2332"
_4images_user_id: "557"
_4images_image_date: "2007-03-27T12:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9792 -->
Bei Münzeinwurf wird ein schokoladenstück ausgegeben(Einwurfschacht)