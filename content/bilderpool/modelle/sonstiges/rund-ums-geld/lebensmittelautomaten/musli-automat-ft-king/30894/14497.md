---
layout: "comment"
hidden: true
title: "14497"
date: "2011-06-23T18:12:14"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Mir gefällt die Anlage auch, allerdings hättest du sie nicht zerlegen dürfen, sondern noch am Aussehen und an der Technik feilen sollen, denn so wie ich das sehe, sehe ich relativ wenig Sensoren. Vielleicht hast du ja mal Lust und Zeit deinen Müsliautomaten wieder aufzubauen und zu perfektionieren.

MfG
Endlich