---
layout: "image"
title: "Deckel mit Schläuchen"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat14.jpg"
weight: "14"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13917
- /detailsb21f-2.html
imported:
- "2019"
_4images_image_id: "13917"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13917 -->
Durch den Deckel wurden vier Löcher gebohrt. Zwei sind für die Kabel, eins für den Wasserschlauch und das letzte für den Luftschlauch.
