---
layout: "image"
title: "Münz-Einwurf"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat23.jpg"
weight: "23"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13926
- /detailsbd37-2.html
imported:
- "2019"
_4images_image_id: "13926"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13926 -->
Hier sieht man den Münz-Einwurf. Er ist für 10ct Stücke gebaut.
