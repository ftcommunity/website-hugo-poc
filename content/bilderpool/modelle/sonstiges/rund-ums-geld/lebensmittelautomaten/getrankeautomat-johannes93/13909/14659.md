---
layout: "comment"
hidden: true
title: "14659"
date: "2011-07-16T22:26:20"
uploadBy:
- "scripter1"
license: "unknown"
imported:
- "2019"
---
Das ist die leicht abgeänderte Version des Taschentuchspender-Münzprüfers. Ich kann nur die Lichtschranke erkennen, jedoch keinen Taster...

Der Münzprüfer aus dem Taschentuchspender ist (fast) perfekt, da ich getestet habe, dass er mit Taster+Lichtschranke auch 5 Cent-münzen als gültig erkennt, obwohl er für 20 Cent-Münzen vorgesehen ist.

Ich verwende diesen Münzprüfer für jedes meiner Modelle - schon mindestens 10 mal gebaut...