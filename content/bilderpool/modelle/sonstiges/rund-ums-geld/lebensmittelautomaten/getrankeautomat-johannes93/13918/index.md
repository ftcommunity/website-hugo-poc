---
layout: "image"
title: "Seitenansicht rechts"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat15.jpg"
weight: "15"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13918
- /detailsb57d.html
imported:
- "2019"
_4images_image_id: "13918"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13918 -->
