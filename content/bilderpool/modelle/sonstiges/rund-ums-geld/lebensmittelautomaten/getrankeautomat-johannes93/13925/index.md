---
layout: "image"
title: "Getränkwahltasten"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat22.jpg"
weight: "22"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13925
- /detailsc576-2.html
imported:
- "2019"
_4images_image_id: "13925"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13925 -->
Mit den Tasten wählt man das gewünschte Getränk. Die 1-Taste dient gleichzeitig als Reset-Taste.
