---
layout: "image"
title: "Detailansicht Rüttler"
date: "2010-05-27T12:28:06"
picture: "IMG_9075kl.jpg"
weight: "4"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
schlagworte: ["Rüttler"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/27312
- /detailse723-2.html
imported:
- "2019"
_4images_image_id: "27312"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27312 -->
Hier die Detailansicht der zur Vereinzelung der Münzen dienenden Rüttlers. Links rutschen die Münzen durch den Schlitz. Hin und wieder "verstopft" der Rüttler leider, doch meist verrichtet er zuverlässig seinen Dienst. Gut zu erkennen ist auch der Schmutz den die Münzen hinterlassen. Nach ca. 2000 gezählten Centstücken musste ich den Rüttler weitestgehend zerlegen um ihn zu reinigen, da die Münzen immer stecken, bzw. kleben blieben.