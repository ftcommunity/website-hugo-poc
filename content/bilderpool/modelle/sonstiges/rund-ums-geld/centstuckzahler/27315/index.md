---
layout: "image"
title: "Detailansicht TX-Display"
date: "2010-05-27T12:28:06"
picture: "IMG_9083kl.jpg"
weight: "7"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
schlagworte: ["Display"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/27315
- /details9dc1.html
imported:
- "2019"
_4images_image_id: "27315"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27315 -->
Oben werden die vollen Eurobeträge und unten die Centbeträge angezeigt.