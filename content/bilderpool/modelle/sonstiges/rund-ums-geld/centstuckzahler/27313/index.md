---
layout: "image"
title: "Detailansicht Rüttlerantrieb"
date: "2010-05-27T12:28:06"
picture: "IMG_9073kl.jpg"
weight: "5"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
schlagworte: ["Rüttler"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/27313
- /details4090.html
imported:
- "2019"
_4images_image_id: "27313"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27313 -->
Der Rüttlerantrieb erfolgt über einen S-Motor. Die Konstruktion ist dem Antrieb der Kompressoren aus den aktuellen Fischertechnikkästen sehr ähnlich. Anstatt eines Zylinders wird hier der Rüttler über eine Strebe angetrieben.