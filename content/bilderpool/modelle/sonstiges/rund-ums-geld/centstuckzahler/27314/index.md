---
layout: "image"
title: "Detailansicht Münzrutsche"
date: "2010-05-27T12:28:06"
picture: "IMG_9077kl.jpg"
weight: "6"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
schlagworte: ["Münzrutsche", "Zähler", "Rüttler"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/27314
- /details1b93.html
imported:
- "2019"
_4images_image_id: "27314"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27314 -->
Rutsche, welche die Münzen vom Rüttler zum Zähler führt.