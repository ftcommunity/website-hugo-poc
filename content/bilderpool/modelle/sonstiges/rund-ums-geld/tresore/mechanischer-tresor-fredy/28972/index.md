---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:10"
picture: "mechanischertresor02.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28972
- /detailseb68.html
imported:
- "2019"
_4images_image_id: "28972"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28972 -->
Hier könnt ihr die Schaltwalzen erkennen, in diesem zustand fallen die Hebel, zwischen die Scheiben und verriegeln die Tür.
