---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:11"
picture: "mechanischertresor08.jpg"
weight: "8"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28978
- /details6305-3.html
imported:
- "2019"
_4images_image_id: "28978"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28978 -->
So sieht es aus wenn die Hebel die Tür verriegeln.
Die anderen drei natürlich auch, die fehlen fürs Foto.
