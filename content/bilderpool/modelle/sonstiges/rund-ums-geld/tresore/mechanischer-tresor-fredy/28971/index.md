---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:10"
picture: "mechanischertresor01.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28971
- /details6844.html
imported:
- "2019"
_4images_image_id: "28971"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28971 -->
Die Hebel aus den BS30 halten die Tür zu oder geben sie frei.
