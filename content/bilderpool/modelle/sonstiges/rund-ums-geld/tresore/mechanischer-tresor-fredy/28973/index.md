---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:10"
picture: "mechanischertresor03.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28973
- /details9b74.html
imported:
- "2019"
_4images_image_id: "28973"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28973 -->
Wenn alle Winkel oben sind werden die Hebel hoch gedrückt und die Tür ist nicht mehr verriegelt.
