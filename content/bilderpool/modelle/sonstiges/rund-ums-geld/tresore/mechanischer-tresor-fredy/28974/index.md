---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:11"
picture: "mechanischertresor04.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28974
- /details1fac.html
imported:
- "2019"
_4images_image_id: "28974"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28974 -->
Die Schaltwalzen, auf ihnen könnt ihr die Winkel und die Mitnehmer erkennen.
Öffnen des Schlosses:
Als erstes wird die hintere(Rechts) Walze in die Position gebracht. Dafür muss das Schloss ersteinmal 4x nach Rechts drehen.
Damit sich alle mitnehmer richtig einstellen.
Nun können wir die Zahlenscheibe(außen am Tresor) Rechts herrum auf die erste Geheimzahl drehen, in diesem Fall die 10.
Jetzt wird die Zahlenscheibe 2x Links herrum gedreht, nun kann die nächste Zahl eingestellt werden, nach Links drehen bis zur 55.
Jetzt stehen die Winkel der hinteren beiden Walzen oben.
1x Rechts herrum drehen, nächste Zahl einstellen. Rechts herrum bis zur 25.
Jetzt wieder Links herrum bis zur 50.
Alle Winkel sind oben und die Hebel geben die Tür frei.
