---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:11"
picture: "mechanischertresor06.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28976
- /details3998.html
imported:
- "2019"
_4images_image_id: "28976"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28976 -->
Die Tür.
