---
layout: "image"
title: "Die Kontrolleinheit ohne Abdechung"
date: "2014-05-26T20:08:07"
picture: "unbenannt-5241193.jpg"
weight: "12"
konstrukteure: 
- "AKtobi47"
fotografen:
- "AKtobi47"
schlagworte: ["Codeschloss", "Tresor", "Kontrolleinheit", "Verkabelung"]
schlagworte: ["Codeschloss", "Tresor", "Kontrolleinheit", "Verkabelung"]
uploadBy: "AKtobi47"
license: "unknown"
legacy_id:
- /php/details/38860
- /detailse8f8.html
imported:
- "2019"
_4images_image_id: "38860"
_4images_cat_id: "2903"
_4images_user_id: "2183"
_4images_image_date: "2014-05-26T20:08:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38860 -->
Hier sieht man das "Licht und Sound" Modul, zwei Lampen, den Kompressor, ein Magnetventil, den Summer, den Controller und die Kabel.

Hier der Downloadlink: http://www.file-upload.net/download-10268862/Codeschloss-Tresor.zip.html