---
layout: "image"
title: "Der Magnetschlüssel"
date: "2014-05-26T20:08:07"
picture: "unbenannt-5241200.jpg"
weight: "13"
konstrukteure: 
- "AKtobi47"
fotografen:
- "AKtobi47"
schlagworte: ["Codeschloss", "Tresor"]
schlagworte: ["Codeschloss", "Tresor"]
uploadBy: "AKtobi47"
license: "unknown"
legacy_id:
- /php/details/38861
- /details0cf4.html
imported:
- "2019"
_4images_image_id: "38861"
_4images_cat_id: "2903"
_4images_user_id: "2183"
_4images_image_date: "2014-05-26T20:08:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38861 -->
Wird für das Fehlerprotokoll mit dem Reedkontakt gebraucht

Hier der Downloadlink: http://www.file-upload.net/download-10268862/Codeschloss-Tresor.zip.html