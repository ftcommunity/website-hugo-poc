---
layout: "image"
title: "Tresor"
date: "2007-04-21T14:21:45"
picture: "tresor1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10140
- /detailse717.html
imported:
- "2019"
_4images_image_id: "10140"
_4images_cat_id: "917"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10140 -->
Gesamtansicht