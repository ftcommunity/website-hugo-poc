---
layout: "image"
title: "Geldspeicher 9"
date: "2012-09-01T11:10:54"
picture: "DSC02444.jpg"
weight: "9"
konstrukteure: 
- "robbi2011"
fotografen:
- "robbi2011"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- /php/details/35422
- /detailsbda0.html
imported:
- "2019"
_4images_image_id: "35422"
_4images_cat_id: "2628"
_4images_user_id: "1442"
_4images_image_date: "2012-09-01T11:10:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35422 -->
Der Mechanismus des "Fallhammers"