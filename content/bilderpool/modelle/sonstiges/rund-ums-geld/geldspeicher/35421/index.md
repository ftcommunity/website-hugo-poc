---
layout: "image"
title: "Geldspeicher 8"
date: "2012-09-01T11:10:54"
picture: "DSC02443.jpg"
weight: "8"
konstrukteure: 
- "robbi2011"
fotografen:
- "robbi2011"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- /php/details/35421
- /details0c84.html
imported:
- "2019"
_4images_image_id: "35421"
_4images_cat_id: "2628"
_4images_user_id: "1442"
_4images_image_date: "2012-09-01T11:10:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35421 -->
Der Ultraschallsensor löst den "Fallhammer" aus