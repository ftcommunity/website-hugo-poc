---
layout: "image"
title: "Geldspeicher 6"
date: "2012-09-01T11:10:54"
picture: "DSC02441.jpg"
weight: "6"
konstrukteure: 
- "robbi2011"
fotografen:
- "robbi2011"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- /php/details/35419
- /details5924.html
imported:
- "2019"
_4images_image_id: "35419"
_4images_cat_id: "2628"
_4images_user_id: "1442"
_4images_image_date: "2012-09-01T11:10:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35419 -->
Das Sicherheitsfenster  (fast)geschlossen.
Um es zu öffnen, muss man zuerst die inneren (oder die äußeren) Klappen öffnen und dann die anderen beiden.
Wenn man nur dagegendrückt bleibt  das Fenster geschlossen.