---
layout: "image"
title: "Geldautomat"
date: "2006-02-24T20:15:50"
picture: "Wale_006.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5789
- /detailsb7fb.html
imported:
- "2019"
_4images_image_id: "5789"
_4images_cat_id: "496"
_4images_user_id: "420"
_4images_image_date: "2006-02-24T20:15:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5789 -->
Man wählt an den Schiebeschaltern wie 
viel Geld der Automat ausgeben soll (1 oder 2 Euro).  Danach schiebt man die
Karte in den Schlitz. Nun gibt der Geldautomat den gewählten Geldetrag aus.