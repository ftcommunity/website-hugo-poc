---
layout: "image"
title: "Geldautomat (noch nicht ganz fertig)"
date: "2006-02-24T15:28:03"
picture: "Geldautomat.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5788
- /detailsde71-2.html
imported:
- "2019"
_4images_image_id: "5788"
_4images_cat_id: "496"
_4images_user_id: "420"
_4images_image_date: "2006-02-24T15:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5788 -->
