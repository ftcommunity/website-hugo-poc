---
layout: "image"
title: "Kartenschlitz"
date: "2011-12-28T22:55:31"
picture: "geldautomat3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33816
- /details9b52-2.html
imported:
- "2019"
_4images_image_id: "33816"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33816 -->
Hier nun der Kartenschlitz , indem momentan die FanClubKarte steckt, also die "Bankkarte".
