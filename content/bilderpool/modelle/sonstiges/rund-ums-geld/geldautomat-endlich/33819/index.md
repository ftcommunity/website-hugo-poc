---
layout: "image"
title: "Der Kompressor"
date: "2011-12-28T22:55:31"
picture: "geldautomat6.jpg"
weight: "6"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33819
- /details254d-2.html
imported:
- "2019"
_4images_image_id: "33819"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33819 -->
