---
layout: "image"
title: "Handventil mit Beschriftung"
date: "2011-12-28T22:55:31"
picture: "geldautomat2.jpg"
weight: "2"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33815
- /detailse225.html
imported:
- "2019"
_4images_image_id: "33815"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33815 -->
Auf diesem Bild kann man das Handventil sehen, mit dem man die Zylinder ein-  bzw. ausfahren kann. Darüber sieht man die Beschriftung.
