---
layout: "image"
title: "Magazine"
date: "2011-12-28T22:55:31"
picture: "geldautomat7.jpg"
weight: "7"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33820
- /details9e5b.html
imported:
- "2019"
_4images_image_id: "33820"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33820 -->
rechts: Fach für die 1 Euro Münzen
links: Fach für die 2 Euro Münzen
