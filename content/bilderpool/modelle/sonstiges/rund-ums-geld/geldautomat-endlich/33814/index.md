---
layout: "image"
title: "Geldautomat - Gesamtansicht"
date: "2011-12-28T22:55:31"
picture: "geldautomat1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33814
- /details061c-2.html
imported:
- "2019"
_4images_image_id: "33814"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33814 -->
Hallo,
hier könnt ihr meinen Geldautomaten sehen, der bereits auf dem FanClubTag 2011, in Tumlingen gezeigt und auch  in der ft:pedia veröffentlicht und beschrieben wurde. 
Nun zeige ich euch hier mehr Bilder meines Geldautomaten.

Ich hoffe euch gefallen die Bilder :)

MfG
Endlich
Achso, euch noch nen guten Rutsch ins neue Jahr!!!
