---
layout: "image"
title: "Diebstahlsicherung"
date: "2007-04-03T17:33:55"
picture: "antidieb1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9900
- /details5b79.html
imported:
- "2019"
_4images_image_id: "9900"
_4images_cat_id: "895"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9900 -->
Schläger zum abwehren( mit Powermotor)