---
layout: "image"
title: "Diebstahlsicherung"
date: "2007-04-03T17:33:55"
picture: "antidieb3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9902
- /detailscd5a.html
imported:
- "2019"
_4images_image_id: "9902"
_4images_cat_id: "895"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9902 -->
Öffnungsmechanismus