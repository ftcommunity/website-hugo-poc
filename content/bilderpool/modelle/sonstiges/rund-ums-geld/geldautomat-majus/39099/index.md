---
layout: "image"
title: "Seitenansicht von rechts"
date: "2014-07-29T17:26:54"
picture: "Bild2.jpg"
weight: "2"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/39099
- /details9102.html
imported:
- "2019"
_4images_image_id: "39099"
_4images_cat_id: "2923"
_4images_user_id: "1239"
_4images_image_date: "2014-07-29T17:26:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39099 -->
Hier sieht man die Geldkassette auf der rechten Seite. Leider ist der Automat mit Monopolygeld bestückt, da mir die echten Scheine leider fehlen :D :D
Insgesamt sind "1040¤" im Automat gebunkert, die über einen Schieber und Rollen zum Kunden befördert werden.
Die Metallstangen des Schiebers gucken oben rechts heraus, die passenden Rollen im Anschluss sieht man oben links.

Die Rollen unten ziehen die Bankkarte ein oder spucken sie wieder aus.

Ein Video des Automaten gibt&#180;s hier: http://youtu.be/_ULOComTNBo