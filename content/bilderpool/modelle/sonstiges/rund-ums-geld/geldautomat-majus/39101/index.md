---
layout: "image"
title: "Vogelperspektive des Geldautomaten"
date: "2014-07-29T17:26:54"
picture: "Bild4.jpg"
weight: "4"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/39101
- /details8e11.html
imported:
- "2019"
_4images_image_id: "39101"
_4images_cat_id: "2923"
_4images_user_id: "1239"
_4images_image_date: "2014-07-29T17:26:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39101 -->
Direkt hinter dem Tablet seht ihr zwei QR Codes, die über einen Motor und eine Schnecke hoch und runter und damit vor die Kamera gefahren werden können.
Der eine Code beinhaltet eine "1" und der andere Code beinhaltet eine "0". 
Damit übergibt RoboPro Daten an die App.
Umgekehrt funktioniert das System über einen Fotowiderstand oben am Bildschirm, dessen Kabel man in der Bildmitte erkennen kann.
Ist der Bildschirm schwarz, so meldet der AX Eingang des Interface 1023, ist der Bildschirm weiß, so liegt der Wert unter 800. 

Dieser Effekt ist durch die mangelnde Helligkeit nur durch einen dazwischen geschalteten Transistor möglich. An seinem Kollektor ist der Pluspol des Widerstands angeschlossen, der Minuspol wurde an der Basis verkabelt. Dies verstärkt den Strom zwischen Kollektor und Emitter und erlaubt es, das Signal mit dem Interface AX Eingang überhaupt zu messen. (funktioniert prima!)
Würde man das nicht tun, würde das Licht des LED Bildschirms gar nicht ausreichen, um einen messbaren Unterschied zwischen weiß und schwarz zu erkennen.

Ein Video des Automaten gibt&#180;s hier: http://youtu.be/_ULOComTNBo