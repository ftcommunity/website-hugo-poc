---
layout: "image"
title: "Frontansicht mit Touchscreen"
date: "2014-07-29T17:26:54"
picture: "Bild1.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/39098
- /details80b5.html
imported:
- "2019"
_4images_image_id: "39098"
_4images_cat_id: "2923"
_4images_user_id: "1239"
_4images_image_date: "2014-07-29T17:26:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39098 -->
Hier sieht man die Vorderseite meines Geldautomaten.
Die Steuerung funktioniert über eine selbstprogrammierte App und mein Tablet. (siehe dazu mehr bei einigen späteren Bildern)
Oben rechts werden die Scheine ausgespuckt und unten rechts kann man den Kartenschacht erkennen.

Die Pineingabe sowie die Auswahl der Geldsumme läuft über den Touchscreen.

Ein Video des Automaten gibt&#180;s hier: http://youtu.be/_ULOComTNBo