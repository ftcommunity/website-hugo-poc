---
layout: "image"
title: "06 Kurbel"
date: "2012-04-02T19:39:17"
picture: "geldwechsler6.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34747
- /detailsa25a.html
imported:
- "2019"
_4images_image_id: "34747"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34747 -->
das ist die Kurbel