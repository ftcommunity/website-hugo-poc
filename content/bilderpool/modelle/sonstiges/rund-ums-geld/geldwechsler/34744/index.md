---
layout: "image"
title: "03 Geldeinwurf"
date: "2012-04-02T19:39:17"
picture: "geldwechsler3.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34744
- /detailsc4d0.html
imported:
- "2019"
_4images_image_id: "34744"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34744 -->
selbes Prinzip wie bei einem Münzsortierer, die Münzen fallen aufgrund ihrer verschiedenen Größen durch die einzelnen Spalte und werden von der Lichtschranke erfasst. 

(Da sind übrigens Infrarotdioden drin, keine ft-Lampen; siehe hier: http://www.ftcommunity.de/details.php?image_id=29414)