---
layout: "comment"
hidden: true
title: "15241"
date: "2011-09-27T20:56:50"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Kieseleck,

das mit dem Münzprüfer ist nicht so einfach...
Folgender Vorschlag (größtenteils beim Glücksspielautomaten [ Video Münzschieber](http://www.youtube.com/watch?v=-sq1BoAZ1pM) zu sehen): Die Münzöffnung ist nur so groß wie die gewünschte Münze, d.h. eine größere Münze paßt nicht in den Schlitz.
Anschließend wird die Größe geprüft, eine zu kleine Münze fällt. (wie beim Münzsortierer, aber natürlich nur einmal) Die Münze mit einer definierten Geschwindigkeit am Prüfer vorbeiführen, damit die Münze nicht durchgeschossen werden kann.
Und dann sollte man noch prüfen, ob die Münze metallisch ist...
Wenn die Münze alle Prüfungen erfolgreich überstandenn hat, könnte man über eine Lichtschranke den Einwurf registrieren (also kein Taster).

Ich hoffe, Dir damit geholfen zu haben,
viele Grüße
Mirose