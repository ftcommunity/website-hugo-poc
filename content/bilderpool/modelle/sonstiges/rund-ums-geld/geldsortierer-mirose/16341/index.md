---
layout: "image"
title: "Geldsortierer 16"
date: "2008-11-18T16:59:54"
picture: "Geldsortierer_16.jpg"
weight: "15"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16341
- /detailsee21-2.html
imported:
- "2019"
_4images_image_id: "16341"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16341 -->
Die Münzen kurz vor dem Fall zum Vereinzelner. – Und eine Münze hat soeben den Vereinzelner verlassen.
