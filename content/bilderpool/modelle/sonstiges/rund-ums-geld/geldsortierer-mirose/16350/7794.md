---
layout: "comment"
hidden: true
title: "7794"
date: "2008-11-19T14:55:08"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan!

Danke für Dein Lob und Deinen Vorschlag.
Problem mit der Lichtschranke:
1) 8 Lichtschranken, für jedes Körbchen eines, notwendig.
2) Die Münzen kommen "irgendwie" ins Körbchen; daher müßte man sie wieder zwangsweise in eine Stellung bringen, was wiederum viel Platz (und Höhe) benötigt.

Die einfachste Möglichkeit ist sicher, die Münzen auf einer entsprechenden Waage abzuwiegen.

Vielen Dank

Mirose