---
layout: "image"
title: "Geldsortierer 15"
date: "2008-11-18T16:59:01"
picture: "Geldsortierer_15.jpg"
weight: "14"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16340
- /details2950.html
imported:
- "2019"
_4images_image_id: "16340"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16340 -->
Und jetzt das Ganze mit Münzen.
