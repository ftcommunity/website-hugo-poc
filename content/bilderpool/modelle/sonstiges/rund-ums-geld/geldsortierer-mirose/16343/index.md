---
layout: "image"
title: "Geldsortierer 18"
date: "2008-11-18T17:00:36"
picture: "Geldsortierer_18.jpg"
weight: "17"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16343
- /detailse713.html
imported:
- "2019"
_4images_image_id: "16343"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:00:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16343 -->
Die ersten Münzen wurden schon sortiert…
