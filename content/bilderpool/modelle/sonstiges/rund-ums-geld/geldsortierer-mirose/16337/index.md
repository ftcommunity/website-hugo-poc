---
layout: "image"
title: "Geldsortierer 12"
date: "2008-11-18T16:59:01"
picture: "Geldsortierer_12.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/16337
- /detailsa35a-2.html
imported:
- "2019"
_4images_image_id: "16337"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16337 -->
Der rechte Teil etwas von unten. Zu erkennen ist der Antrieb des Rüttlers, der vom Vereinzelner kommt.
