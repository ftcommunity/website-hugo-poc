---
layout: "image"
title: "Doppelbereifung"
date: "2011-01-15T20:46:47"
picture: "doppelreifen1.jpg"
weight: "17"
konstrukteure: 
- "Tobias Tacke"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29701
- /details4258.html
imported:
- "2019"
_4images_image_id: "29701"
_4images_cat_id: "323"
_4images_user_id: "182"
_4images_image_date: "2011-01-15T20:46:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29701 -->
Tobias ( mein Sohn ) war heute sehr kreativ.

Er hat eine Zwillingsbereifung aus den Traktorrädern 80mm und den 43er Felgen konstriert.
Jeder Reifensatz besteht aus 2 Reifen und 3 Felgen.
Ich finde die sehen echt stark aus.
