---
layout: "image"
title: "Chefsessel46"
date: "2011-08-08T21:11:47"
picture: "Chefsessel46.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31550
- /details348b.html
imported:
- "2019"
_4images_image_id: "31550"
_4images_cat_id: "323"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:11:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31550 -->
Was soll ich sagen? Man sitzt ganz ordentlich drin :-)
