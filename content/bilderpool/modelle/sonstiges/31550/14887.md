---
layout: "comment"
hidden: true
title: "14887"
date: "2011-08-18T23:10:56"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hach, eine Limousine... vielleicht gar ein Aston Martin - z.B. Bonds DB5 (http://de.wikipedia.org/wiki/Aston_Martin_DB5), natürlich mit Schleudersitz, ausfahrbarem schusssicheren Rückfensterschutz, automatischen Wechselnummernschildern, Rammen in den Stoßstangen und diesen schnuckeligen Reifenschlitzern in den Naben (http://www.hammerperformance.com/wp-content/uploads/2011/02/Aston-Martin-DB5.jpg?84cd58) - träum...
Vielleicht ja zur Convention 2012...

Gruß, Dirk