---
layout: "image"
title: "Elektromagnet zum Starten"
date: "2016-01-13T14:57:44"
picture: "vbse4.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42724
- /details9e1b-2.html
imported:
- "2019"
_4images_image_id: "42724"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42724 -->
Der Elektromagnet lässt die Kugel los. Die erste Laserlichtschranke bestimmt über die Verdunklungszeit und den Durchmesser der Kugel (12,5mm) annäherungsweise eine Momentangeschwindigkeit
