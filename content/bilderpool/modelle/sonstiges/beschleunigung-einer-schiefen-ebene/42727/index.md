---
layout: "image"
title: "Programm"
date: "2016-01-13T14:57:44"
picture: "vbse7.jpg"
weight: "7"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42727
- /details43c2.html
imported:
- "2019"
_4images_image_id: "42727"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42727 -->
Hier ist ein Ausschnitt aus dem UP Verdunklungszeit:
Dabei wird eine Timervariable auf 1 Sekunde gesetzt, nachdem die Kugel die erste Lichtschranke unterbrochen hat. In 1ms Schritten zählt sie runter, bis ein Ereignis (die Kugel verlässt die Lichtschranke wieder) eintritt. Das Programm speichert die Restzeit und zieht diese (in einem anderen UP) von einer Sekunde ab. Man erhält die Verdunklungszeit.

Aus dieser kann man die Momentangeschwindigkeit v0 berechnen (v0 = Durchmesser der Kugel (12.5 mm ) / Verdunklungszeit). Physikalisch korrekt wäre dies eine mittlere Geschwindigkeit in einerm sehr kleinen Intervall, praktisch die Momentangeschwindigkeit.
Anschließend wird die Zeit t gestoppt, wie lange die Kugel bis zur zweiten Laserlichtschranke braucht.

Es gilt: s = 0,5 * a * t² + v0 * t. Da man den Weg s zwischen den Lichtschranken kennt, kann man die Formel nach a umstellen, um die Beschleunigung zu berechen: a = -2*v0 / t + 2s / t²

Mit der Beschleunigung lässt sich die Endgeschwindigkeit v1 berechnen: v1 = a * t + v0. Dabei kann man sich auf die Bewegungsgesezt der Physik verlassen um eine ziemlich genaue Endgeschwindigkeit zu erhalten, ohne sehr kleine Zeiten messen zu müssen.
