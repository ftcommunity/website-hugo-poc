---
layout: "image"
title: "Kugel in Bewegung"
date: "2016-01-13T14:57:44"
picture: "vbse5.jpg"
weight: "5"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42725
- /details5aac-2.html
imported:
- "2019"
_4images_image_id: "42725"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42725 -->
Slo-Motion-Aufnahme der Kugel
