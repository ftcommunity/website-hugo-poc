---
layout: "image"
title: "Laserlichtschranke"
date: "2016-01-13T14:57:44"
picture: "vbse3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42723
- /details339e.html
imported:
- "2019"
_4images_image_id: "42723"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42723 -->
Eine Laserdiode als Lichtschranke eignet sich perfekt zum Messen der Zeiten
