---
layout: "comment"
hidden: true
title: "21580"
date: "2016-01-17T10:22:15"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
@ ft fritze: Das geht, ich verwende momentan nur die ft Winkelsteine 7,5° 15° (auf dem Foto) und 30° dafür, weil ich dann einen definierten Winkel habe. Man könnte die Winkelsteine aber auch durch einen Gelenkstein ersetzen, sodass man die Steigung der schiefen Ebene beliebig einstellen kann