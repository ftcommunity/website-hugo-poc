---
layout: "overview"
title: "Beschleunigung auf einer schiefen Ebene"
date: 2020-02-22T08:32:06+01:00
legacy_id:
- /php/categories/3181
- /categories188c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3181 --> 
Anlässlich des Themas "Beschleunigung einer Kugel auf einer schiefen Ebene" http://forum.ftcommunity.de/viewtopic.php?f=8&t=3333 habe ich einen Versuchsaufbau erstellt, mit dem man die Beschleunigung einer Kugel und deren Endgeschwindigkeit berechnen kann. Der Versuchsaufbau nutzt die Bewegungsgleichungen, um die Endgeschwindigkeit zu berechnen, ohne sehr kleine Zeiten messen zu müssen.