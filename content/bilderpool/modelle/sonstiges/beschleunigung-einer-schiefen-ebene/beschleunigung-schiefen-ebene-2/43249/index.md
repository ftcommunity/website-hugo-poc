---
layout: "image"
title: "Kugel in Bewegung"
date: "2016-04-04T21:54:43"
picture: "badsema4.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43249
- /detailsd0ea.html
imported:
- "2019"
_4images_image_id: "43249"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43249 -->
Slow-motion Aufnahme der Kugel in Bewegung
