---
layout: "image"
title: "TX Display"
date: "2016-04-04T21:54:43"
picture: "badsema2.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43247
- /details2ce1.html
imported:
- "2019"
_4images_image_id: "43247"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43247 -->
Der TX erwies sich aufgrund der zu geringen zeitlichen Auflösung bei der Sensorabfrage als eher ungeeignet für eine präzise Messung. Als Alternative bietet sich der Arduino an.
