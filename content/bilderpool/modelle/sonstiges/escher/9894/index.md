---
layout: "image"
title: "Escher's Relativity"
date: "2007-04-02T22:04:21"
picture: "relativity.jpg"
weight: "4"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
schlagworte: ["Escher", "Relativity"]
uploadBy: "martijn"
license: "unknown"
legacy_id:
- /php/details/9894
- /details2571.html
imported:
- "2019"
_4images_image_id: "9894"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-04-02T22:04:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9894 -->
Escher's Relativity