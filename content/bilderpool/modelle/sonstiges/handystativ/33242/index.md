---
layout: "image"
title: "StativNew1"
date: "2011-10-19T16:49:06"
picture: "DSC01607.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33242
- /detailsfeab.html
imported:
- "2019"
_4images_image_id: "33242"
_4images_cat_id: "2459"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33242 -->
Hallo!
Hier seht ihr die überarbeitete Version des Stativs, neu sind: die Räder unten, die einen Dollyeinsatz ermöglichen, und der Drehkranz.

LG
Lukas