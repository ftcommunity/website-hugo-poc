---
layout: "image"
title: "Stativ3"
date: "2011-10-19T14:25:45"
picture: "Stativ4.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
schlagworte: ["Handykamerahalterung", "Stativ"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33234
- /details2226.html
imported:
- "2019"
_4images_image_id: "33234"
_4images_cat_id: "2459"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T14:25:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33234 -->
Hier sieht man die Halterung mit Gegengewicht (das Gegengewicht macht das Neigen leichter und dient der Schonung d. Materials).