---
layout: "image"
title: "Igarashi Power-Motor in Hülse 01"
date: "2008-03-23T21:08:21"
picture: "25.jpg"
weight: "7"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
schlagworte: ["Igarashi", "Powermotor", "Power", "Motor", "Hülse", "Zylinder-Kessel", "Rohrhülse", "31613"]
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14040
- /details8806.html
imported:
- "2019"
_4images_image_id: "14040"
_4images_cat_id: "323"
_4images_user_id: "327"
_4images_image_date: "2008-03-23T21:08:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14040 -->
Hallo zusammen,

die schwarze Hülse [31613] passt vom Innendurchmesser her gerade so NICHT über den Power-Motor. Ich dachte mir, das wäre aber trotzdem eine feine Art der Befestigung des Motors.

Ich habe also deshalb die Hülse mit einem Heißluftfön ein wenig erwärmt, so daß sie dehnbar ist und dann mit viel Gefühl über den Motor geschoben. Dabei muß man auf darauf achten, daß die seitlichen "Anbaufedern" möglichst parallel zur Getriebeausgangswelle des Motors stehen.

Hinten hat man dann noch etwas Platz, z.B. für einen Entstörkondensator oder sonstwas. Dann noch einen Deckel drauf, evtl. sogar mit Buchsen und aus die Maus.

Obwohl der Durchmesser der Hülse dadurch etwas mehr als 30 mm beträgt, lässt sich das Teil so recht gut integrieren. Auf den nächsten Fotos sind ein paar Anbaubeispiele dazu.