---
layout: "comment"
hidden: true
title: "6040"
date: "2008-03-23T21:39:04"
uploadBy:
- "Porsche-Makus"
license: "unknown"
imported:
- "2019"
---
was mir grade noch dazu eingefallen ist:

kritiker werden bemängeln, daß dadurch die seitlichen ventilationsöffnungen des motors komplett verschlossen werden und das ist auch richtig.

man wird sehen, ob es auch ohne geht, aber ich schätze, das hängt stark davon ab, wie lange der motor am stück laufen muss und welche last er zu bewälltigen hat. zur not könnte man ja auch in die hülse passende öffnungen bohren/schneiden.