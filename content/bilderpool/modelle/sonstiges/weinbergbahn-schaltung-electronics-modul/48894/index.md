---
layout: "image"
title: "Draufsicht"
date: 2020-11-15T19:47:16+01:00
picture: "Schaltung mit Electronics-Modul2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Schaltung von oben gesehen. Ich hoffe, man kann alle Kabelführungen gut erkennen. Das Doppelkabel vom Starttasten unten in der Mitte geht mit beiden Adern zu I2 vom Modul. Das andere rote Kabel ist das vom Modusschalter (der sich rechts unten befindet) zum rechten Pol des I3-Eingangs des Moduls.
