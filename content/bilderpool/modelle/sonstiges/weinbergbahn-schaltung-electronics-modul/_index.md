---
layout: "overview"
title: "Weinbergbahn-Schaltung mit Electronics-Modul"
date: 2020-11-15T19:47:16+01:00
---

Hier ist die Schaltung der Weinbergbahn aus ft:pedia 3/2018 anstatt mit einem E-Tec-Modul mit einem Electronics-Modul aufgebaut. Die Schaltung ist exakt gleich der mit dem E-Tec-Modul, wird hier aber der Klarheit wegen aufgebaut gezeigt.

Die Schaltung wurde zur Erleichterung der Diskussion in https://forum.ftcommunity.de/viewtopic.php?f=6&t=6438 aufgebaut.