---
layout: "image"
title: "Schrägansicht"
date: 2020-11-15T19:47:17+01:00
picture: "Schaltung mit Electronics-Modul1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Motor steht hier rechts. Das ist die Ruheposition.

Oben sieht man die beiden Endlagentaster:

- Der linke (am "oberen Ende der Weinbergbahn") ist mit I1 des Moduls verbunden.

- Der rechte verbindet den linken Pol von I3 des Moduls über das grüne Kabel mit einem Pol des Modus-Schalters rechts. Der wiederum verbindet in der "Einzelfahrt"-Stellung (nach links, wie im Bild gezeigt) über das rote Kabel zum rechten Pol des I3-Eingangs. In "Endlosfahrt"-Stellung (nach rechts) verbindet er über das blaue Kabel mit demselben Eingang wie der Start-Taster. Sprich: In Endlos-Fahrt wirkt der rechte Endlagentaster (in der Talstation der Weinbergbahn nämlich) gerade so wie der Starttaster: Er lässt den Motor über I2 nach links ("aufwärts") fahren anstatt über I3 anhalten.

Unten finden sich von links nach rechts:

- Der Ein/Aus-Schalter, der einen Pol der Stromversorgung des Electronics-Moduls unterbricht (ausschaltet; Schalter nach links) oder schließt (einschaltet, Schalter nach rechts wie im Bild gezeigt).

- Der Start-Taster verbindet die beiden Anschlüsse von I2 und lässt den Motor nach links ("Richtung Bergstation der Weinbergbahn") losfahren.

- Der Modus-Schalter: Beschaltet wie im Bild steht die linke (im Bild gezeigte) Schalterstellung für eine Einzelfahrt (Start lässt den Motor nach links fahren, dort umkehren und rechts wieder anhalten). Schalter nach rechts bedeutet Endlosfahrt: Der Motor fährt ständig zwischen den beiden Endlagentastern hin und her, weil der rechte wie der Starttaster wirkt.
