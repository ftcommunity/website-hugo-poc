---
layout: "image"
title: "Feuerwerfer 08"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36835
- /details74f1.html
imported:
- "2019"
_4images_image_id: "36835"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36835 -->
Hier sieht man den Mechanismus, der die Dose öffnet.
Wenn man an das Gas möchte, muss man Öffnung in die Dose reindrücken (gleiches Prinzip wie bei Deo-/ Spraydosen).

Die beiden Zahnräder drücken beim Drehen mit ihrer Narbe die kleine rote Platte nach hinten, die mit einem BS15 mit Loch verbunden ist und die Öffnung in die Dose drückt. Wie der BS15 an der Dose hängt, kann man zwei Bilder vorher sehen.
Der Tastet dient zur Positionserkennung.