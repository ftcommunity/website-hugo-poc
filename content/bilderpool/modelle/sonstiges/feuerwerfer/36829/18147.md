---
layout: "comment"
hidden: true
title: "18147"
date: "2013-07-08T14:48:43"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Wenn ich das richtig sehe, ist da ein Magnetventil vom Elektronik-Restposten aus Pförring.
Paß mit den Dingern auf, denn sie dichten nicht 100%ig ab.
Insbesondere sind die Teile ausdrücklich nur für Luft gebaut und haben durchaus auch schon eine Mißhandlung mit Wasser hinter sich.

Also ich würde mich nicht trauen brennbare Gase damit zu schalten.