---
layout: "image"
title: "Feuerwerfer 01"
date: "2013-04-18T11:36:21"
picture: "feuerwerfer01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36828
- /detailsc8c8.html
imported:
- "2019"
_4images_image_id: "36828"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36828 -->
Dieses Fahrzeug stellt quasi das Pendant zu den beiden Feuerwehrautos da, die ich vor einigen Jahren mal gebaut hatte.

Es dürfte wohl klar sein, was es macht bzw. was vorne aus der Düse kommt ;-D

kurzes Video: http://www.youtube.com/watch?v=-nuXwW-KDRo