---
layout: "image"
title: "Feuerwerfer 10"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36837
- /details1b0b-2.html
imported:
- "2019"
_4images_image_id: "36837"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36837 -->
von unten. 

Der Antrieb erfolgt über ein Gleichlaufgetriebe, wie es an anderer Stelle hier schonmal vorgestellt wurde. (http://www.ftcommunity.de/details.php?image_id=6763)