---
layout: "image"
title: "Produkte der Langeweile"
date: "2007-07-22T14:02:00"
picture: "Produkte_der_Langweile_002.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11189
- /detailsc8bd.html
imported:
- "2019"
_4images_image_id: "11189"
_4images_cat_id: "1011"
_4images_user_id: "453"
_4images_image_date: "2007-07-22T14:02:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11189 -->
