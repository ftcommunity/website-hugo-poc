---
layout: "image"
title: "Achter-Bahn mit Flexschienen"
date: "2013-05-26T21:03:37"
picture: "achterbahnmitflexschienen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36990
- /details6b2f-2.html
imported:
- "2019"
_4images_image_id: "36990"
_4images_cat_id: "2750"
_4images_user_id: "104"
_4images_image_date: "2013-05-26T21:03:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36990 -->
Es hätte so schön sein sollen, wurde aber nichts: Autos sollten von etwas, was von einer Kette durch Flexschienen gezogen wurde, mitgenommen werden und schöne Bahnen fahren.

Vergleiche das funktionierende Modell unter http://www.ftcommunity.de/categories.php?cat_id=2742 und die Grundidee unter http://www.ftcommunity.de/categories.php?cat_id=2646

So wie hier gezeigt funktioniert es mindestens aus folgenden Gründen NICHT:

- Das Z10 zum Antrieb der Kette dreht einfach nur durch. Gegenhalten wird knifflig, denn irgendwann kommt ja das Auto vorbei.

- Obwohl hier nur 3 BS7,5 in der Kette eingebaut sind, die die Mitnehmer fürs Auto tragen (2 Verbinder 45), spannt sich die Kette bis zum Stillstand durch Reibung und Klemmung, weil die Kette in Kurven zu weit nach außen gedrängt wird (die 3 BS7,5 sind ja eine starre 45-mm-Strecke und schmiegen sich also nicht an die Kurven an).

Ich kann nur hoffen, dass irgendjemand eine pfiffige Lösung dafür einfällt und doch noch etwas aus der Idee wird.
