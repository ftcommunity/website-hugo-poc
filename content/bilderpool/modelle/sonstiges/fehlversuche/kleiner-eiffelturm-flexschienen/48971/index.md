---
layout: "image"
title: "Fundament und Fuß"
date: 2021-01-19T14:57:01+01:00
picture: "EiffelturmFlexschienenFehlschlag2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Fundament aus vier U-Trägern 150 mit ein paar Achsen hindert die Füße daran, sich nach außen zu spreizen und fast flach auf dem Boden zu liegen.