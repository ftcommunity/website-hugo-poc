---
layout: "image"
title: "Obere Plattform"
date: 2021-01-19T14:56:58+01:00
picture: "EiffelturmFlexschienenFehlschlag4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Entschuldigung für das unscharfe Bild, es fiel mir erst auf, nachdem das Modell schon zerlegt war. Aber es lohnte ja eh nicht ;-)