---
layout: "image"
title: "Cadi03"
date: "2004-10-21T19:39:52"
picture: "Cadi03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2734
- /details9291.html
imported:
- "2019"
_4images_image_id: "2734"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2734 -->
Vorne ist noch reichlich Platz für Motor, Licht, IR-Empfänger und was man so alles braucht.