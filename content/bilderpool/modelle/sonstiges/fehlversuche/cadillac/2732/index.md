---
layout: "image"
title: "Cadi01"
date: "2004-10-21T19:39:51"
picture: "Cadi01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2732
- /detailsf36d-2.html
imported:
- "2019"
_4images_image_id: "2732"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2732 -->
Das ist eins von den Projekten, die eigentlich rechtzeitig zur Convention 2004 fertig werden sollten: ein Auto Marke 'Ami-Straßenschlitten'. Jetzt wird es auf Eis gelegt, damit die anderen vorankommen. Zum Wegwerfen ist es aber doch zu schade.

Features:
- Lenkung per Motor (Hubgetriebe unter Beifahrersitz)
- Achsen vorn und hinten gefedert
- Vordere Türscharniere genau dort, wo sie hingehören
