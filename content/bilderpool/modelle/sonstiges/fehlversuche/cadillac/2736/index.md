---
layout: "image"
title: "Cadi05"
date: "2004-10-21T19:39:52"
picture: "Cadi05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2736
- /detailse813.html
imported:
- "2019"
_4images_image_id: "2736"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2736 -->
Die Vorderachse ist gekröpft, damit sie unter den Längsträgern hindurchpaßt. Mehr als die drei S-Streben und die (hier nicht eingebauten) Kunststoff-Federn ist zur Befestigung nicht nötig.