---
layout: "image"
title: "Traktor 13"
date: "2007-01-23T16:01:39"
picture: "traktor13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8617
- /detailsc3e6.html
imported:
- "2019"
_4images_image_id: "8617"
_4images_cat_id: "793"
_4images_user_id: "502"
_4images_image_date: "2007-01-23T16:01:39"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8617 -->
