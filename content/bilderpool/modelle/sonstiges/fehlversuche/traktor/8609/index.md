---
layout: "image"
title: "Traktor 5"
date: "2007-01-23T16:01:39"
picture: "traktor05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8609
- /detailsf298.html
imported:
- "2019"
_4images_image_id: "8609"
_4images_cat_id: "793"
_4images_user_id: "502"
_4images_image_date: "2007-01-23T16:01:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8609 -->
