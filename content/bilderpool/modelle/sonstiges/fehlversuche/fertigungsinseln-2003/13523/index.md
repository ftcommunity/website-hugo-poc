---
layout: "image"
title: "Teil"
date: "2008-02-04T13:59:56"
picture: "olli3.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13523
- /details0290.html
imported:
- "2019"
_4images_image_id: "13523"
_4images_cat_id: "1240"
_4images_user_id: "504"
_4images_image_date: "2008-02-04T13:59:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13523 -->
Dies hier ist ein Fehlversuch!!!
Fahrbarer Teil, der die Werkstücke transportiert hat.