---
layout: "overview"
title: "Fehlversuche..."
date: 2020-02-22T08:27:48+01:00
legacy_id:
- /php/categories/716
- /categories45a9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=716 --> 
Alles, was nicht so recht was geworden ist, landet hier. Vielleicht hat ja jemand anderes eine Idee, wie man daraus noch etwas sinnvolles machen kann...