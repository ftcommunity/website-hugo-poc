---
layout: "image"
title: "Getriebe - Kurbelseite"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst07.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Glasmurmelbahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41675
- /details8d45-2.html
imported:
- "2019"
_4images_image_id: "41675"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41675 -->
Die Hubstangen werden von der oberen Kurbel angetrieben. Jede Umdrehung erzeugt einen vollständigen Hub beider Stangen. Leider ist die Kurbelkonstruktion aufwendig und nicht stabil genug für einen dauerhaften Betrieb.

Die untere Kurbel treibt über eine ausknickende Pleuelstange einen Federzylinder 133027 an. Der Zylinder wird von der Kurbelbewegung ausgezogen, zurückgestellt wird er nur per Federkraft, bis zu einem bestimmten Punkt. Die Pleuelstange knickt dann aus und die Kurbelbewegung ist entkoppelt. Da die Zylinderbewegung synchron zu den Hubstangen erfolgt, werden die angeschlossenen P-Betätiger der Murmelschleusen ebenfalls synchron zu den Hubstangen ein- und ausgestülpt.

Eine zusätzliche Hilfe zum Verständnis ist die Beschreibung beim Pneumatikschaltplan: http://www.ftcommunity.de/details.php?image_id=41680

---------------

Two cranks form the mechanism. The first one drives the connection rod to the top. The second smaller one drives a special connection rod to the pneumatic cylinder. Here the pressure is buld and released to synchronously drive the marble entries.

And this is where this first attempt fails. The cranks are not stable enough to deliver a reliable service. Wear is really heavy the way I did it here.