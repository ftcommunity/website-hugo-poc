---
layout: "image"
title: "Murmelschleusen - Detailaufnahme"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst03.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41671
- /details76b4.html
imported:
- "2019"
_4images_image_id: "41671"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41671 -->
Jede Schleuse besteht einfach aus einem P-Betätiger 36075. Ist die Membran vorgestülpt, wird der Weg versperrt.

-------------

Just a close up to the special fischertechnik pneumatic part used (36075). This has been in production only for a few years in the mid of the 1980s.