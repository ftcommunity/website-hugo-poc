---
layout: "image"
title: "Stromlaufplan - Gesamtanlage"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst10.jpg"
weight: "10"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41678
- /details9c92.html
imported:
- "2019"
_4images_image_id: "41678"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41678 -->
S5 ist der erwähnte Endschalter, der die Hebekunst gezielt anhält sobald S3 geöffnet wird. Aus sehr speziellen Gründen kann noch mit S4 die Hebekunst notabgeschaltet werden - falls S5 bei geöffnetem S3 partout nicht öffnen will.

---------

A schematic of the complete marble run.