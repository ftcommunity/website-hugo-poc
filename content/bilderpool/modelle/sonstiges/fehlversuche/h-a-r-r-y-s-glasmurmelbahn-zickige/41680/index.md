---
layout: "image"
title: "Pneumatikschaltplan"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst12.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41680
- /details8214-2.html
imported:
- "2019"
_4images_image_id: "41680"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41680 -->
Fast ein Fall für die ft:pedia, aber dafür ist es denn doch zu einfach, oder?

Der Federzylinder 133027 wird von einer der beiden Motorkurbeln angetrieben und der Kolben synchron zur Hubstangenbewegung herausgezogen und wieder losgelassen. Während des Kolbenhubes verhindert das Rückschlagventil 36970 (32061 geht auch), daß sich ein Vakuum bildet. Ein klitzekleinwenig Unterdruck entsteht dann doch und hilft auch dem widerborstigsten P-Betätiger 36075 dabei die Membran einzustülpen. Wenn der Kolben durch die Feder zurückgedrückt wird, entsteht ein geringer Druck, der die Membran der P-Betätiger ausstülpt. An einem bestimmten Punkt bleibt der Kolben stehen, da die Druckkraft auf die Kolbenfläche der Rückstellkraft der Feder entspricht - die Details mit der Reibung des Kolbens an der Zylinderwand sind natürlich auch noch im Spiel. An diesem Punkt knickt dann die Pleuelstange des Kolbens aus - siehe auch die Getriebebeschreibung.

Allfällige geringste Undichtigkeiten sorgen dafür, daß sich Schwankungen des äußeren Luftdrucks nicht auswirken können. Steht das Pneumatiksystem unter Druck entweicht die enthaltene Luft spätestens nach ein paar Minuten von alleine. Über das Rückschlagventil wird der Arbeitspunkt immer automatisch richtig eingestellt sobald der Antrieb anläuft.

--------------

The pneumatic circuit.