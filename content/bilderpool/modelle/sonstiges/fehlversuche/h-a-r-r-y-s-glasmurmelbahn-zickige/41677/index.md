---
layout: "image"
title: "Endlageschalter"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst09.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41677
- /detailsfbaf.html
imported:
- "2019"
_4images_image_id: "41677"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41677 -->
Für den eigentlichen Betrieb war der nicht nötig, aber während der Experimente und Probeläufe war es erforderlich die Hubstangen immer an einer bestimmten Position anzuhalten. Wie beim Scheibenwischerantrieb war dafür dieser Taster als Endschalter vorgesehen. Am richtigen Punkt wird die Stromzufuhr zum Motor unterbrochen und der Antrieb mit den Hubstangen bleibt genau an der richtigen Position stehen. Der Stromanschlußkasten sitzt praktischerweise gleich in der Nähe.

------------

The 'power distribution'. And there is a special switch that detects the optimum position to stop the motor. The machine works without it but so it is more convenient when truning off: the lifting platforms run to their home position. It is like the windshield wiper in your car.