---
layout: "comment"
hidden: true
title: "21000"
date: "2015-08-06T14:33:31"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
... außerdem hat der Lochkreis der Drehscheibe einen ungegeignet kleinen Durchmesser - leider. Hinter mir steht aber schon ein Prototyp, bei dem auch dieses eher periphere Problem sauber gelöst ist.

Grüße
H.A.R.R.Y.