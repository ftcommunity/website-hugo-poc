---
layout: "image"
title: "Die Murmelausleitungen und die Krone"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst04.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["marble", "marbles", "rolling", "ball", "track"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41672
- /detailsbf4b-2.html
imported:
- "2019"
_4images_image_id: "41672"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41672 -->
An den oberen Enden der Hubstangen werden die Murmeln mit leichtem zeitlichem Versatz auf die Schienen ausgeleitet. Zwei enge 180°-Kehren leiten zur gemeinsamen abgehenden Strecke über.

Die Hubstangen sind an einer gemeinsamen Kette aufgehängt, die um das Z40 in der Kronenkonstruktion geschlungen ist. Auf diese Art gleichen sich die Gewichtskräfte der Hubstangen aus und der Antrieb wird hauptsächlich mit den Gewichtskräften der Murmeln belastet. Das Z40 überträgt den Antrieb auf die Ketten und bewegt dadurch die Hubstangen.