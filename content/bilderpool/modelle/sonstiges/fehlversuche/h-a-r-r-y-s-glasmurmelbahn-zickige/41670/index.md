---
layout: "image"
title: "Einlässe und Murmelschleusen"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst02.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Murmelschleuse", "Einstieg"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/41670
- /detailsd33d.html
imported:
- "2019"
_4images_image_id: "41670"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41670 -->
Am unteren Ende der Hubstangen bleiben die Murmeln an den pneumatischen Einlaßschleusen liegen. Nur wenn die Schleusen den Weg freigeben, kann jeweils eine Murmel vorne und hinten auf die erste Hubplattform (linke Hubstange) einsteigen. Sobald sich die Hubstange wieder hebt, blockieren die Schleusen den Weg. Auf diese Art werden nachrollende Murmeln daran gehindert in den "Schachtsumpf" zu fallen.

-------------------

This sepcial kind of lifter needs something to synchronize the marbles to the movement of the lifting platforms. Here it is done by a pneumatic system. Without this synchronization marbles fall into the "shaft sump" and jam the mechanism.