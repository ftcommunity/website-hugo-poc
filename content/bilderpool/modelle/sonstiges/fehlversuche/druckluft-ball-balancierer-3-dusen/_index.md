---
layout: "overview"
title: "Druckluft-Ball-Balancierer mit 3 Düsen"
date: 2020-02-22T08:28:11+01:00
legacy_id:
- /php/categories/2974
- /categoriesdf41.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2974 --> 
Analog zum Druckluft-Ball-Balancierer mit einer Düse und einem Ball sollte dies hier einer mit drei Düsen und zwei Bällen werden - der aber leider an zu wenig Druckluft scheiterte. Allerdings nicht am Prinzip - das konnte erfolgreich getestet werden.