---
layout: "image"
title: "Erste Idee zur Drehmechanik"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/39674
- /detailsdb76-2.html
imported:
- "2019"
_4images_image_id: "39674"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39674 -->
Eine ft-Segmentscheibe (hier grau, mit 12 Zähnen auf der Unterseite) soll nacheinander die drei Z30 um den richtigen Winkel drehen. Hier fehlen noch die Rückstell-Gummis.
