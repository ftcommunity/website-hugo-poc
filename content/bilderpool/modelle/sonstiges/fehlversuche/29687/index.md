---
layout: "image"
title: "Bowdenzug"
date: "2011-01-15T20:46:32"
picture: "Bowdenzug.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29687
- /details6cba.html
imported:
- "2019"
_4images_image_id: "29687"
_4images_cat_id: "716"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29687 -->
Die Idee war ein Bowdenzug ausschließlich aus unmodifizierten Original-FT-Teilen. Ich könnte mir einen Bowdenzug sinnvoll vorstellen, um irgendwo an schwer zugänglichen Stellen etwas zu schalten, dessen Antrieb (z.B. Mini-Motor mit Hubgetriebe) woanders im Modell positioniert ist.

Ich habe dazu ein FT-Seil durch einen Pneumatikschlauch gefädelt, der mittels Pneumatik-T-Stücken an beiden Enden fest fixiert ist. Ziehe ich am Ende vom Seil (unten im Bild), wird das andere Ende bewegt. Es ist dort natürlich ein Feder-Mechanismus notwendig, um das Seil wieder zurückzuziehen.

Ist der Bowdenzug gerade verlegt oder nur GANZ leicht gebogen, klappt das ganz gut. Aber ist die Krümmung größer oder sind die Radien enger, wird die Reibung im Bowdenzug sehr groß, und das Seil lässt sich gar nicht mehr richtig ziehen. Der Pneumatikschlauch ist dann auch viel zu instabil und zieht sich zusammen. Und die Feder schafft es dann auch nicht mehr, das Seil wieder zurückzuziehen ...

Viele Gründe also, die mich bewegt haben, das Projekt Bowdenzug nicht weiter zu verfolgen. Daher ist es auch in der Kategorie "Fehlversuche" gelandet.