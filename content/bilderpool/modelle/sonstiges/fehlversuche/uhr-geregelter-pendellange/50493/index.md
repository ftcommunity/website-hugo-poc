---
layout: "image"
title: "Blick von oben"
date: 2024-08-21T09:38:09+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Bildmitte sieht man eine Freilaufkupplung zwischen Pendelantrieb und Uhrwerk. Die ist nötig, weil man die Uhr ja auch stellen können muss. Dazu stellt man sie einfach durch Drehen direkt am Sekundenzeiger vorwärts (und nur vorwärts). Ggf. kann man ja auch den Stunden- oder Minutenzeiger versetzen, um nicht 24 h mit 1 Umdrehung je Minute langwierig vorstellen muss.