---
layout: "image"
title: "Pendelhemmung (2)"
date: 2024-08-21T09:37:45+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein etwas höherer Blickwinkel auf die Hemmung.