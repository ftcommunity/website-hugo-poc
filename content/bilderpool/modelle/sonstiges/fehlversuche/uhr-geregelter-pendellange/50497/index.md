---
layout: "image"
title: "Rückseite des Gewichts"
date: 2024-08-21T09:38:14+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nichts Besonderes, aber stabil muss es sein.