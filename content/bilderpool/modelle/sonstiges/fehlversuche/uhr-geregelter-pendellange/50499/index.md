---
layout: "image"
title: "Pendeluhr mit geregelter Pendellänge"
date: 2024-08-21T09:38:17+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Idee ist eine Kombination aus folgendem:

a) Das schon mehrfach eingesetzte und auch in der ft:pedia 2023-1 beschriebene kompakte Uhrwerk mit Sekundenzeiger. Es braucht am Eingang einfach etwas, was sich ein Mal pro Minute dreht.

b) Eine Pendelmechanik mit Aufziehen per Gewicht über einen Huygensscher Aufzug. Hier ist das eine endlos umlaufende Kette. Eine Seite zieht und treibt das Pendel an, auf der anderen kann es motorisch wieder aufgezogen werden, und zwar *während* die Pendeluhr weiterläuft. Siehe auch http://www.uhrenlexikon.de/begriff.php?begr=Huygensscher%20Aufzug

c) Ein Pendel mit motorisch veränderbarer Länge. Die Uhr soll von einem TXT-Controller gesteuert werden. Der misst per Reedkontakt, wie lange die Uhr tatsächlich für eine angebliche Minute brauchte und soll die Pendellänge dann geeignet nachjustieren (kürzer, wenn es zu lange dauerte, länger, wenn die Minute zu schnell um war). Das Justieren geschieht per Seilzug an einem Gewicht im Fuß des Pendels. Die Regelung und der Algorithmus ist praktisch derselbe wie schon in https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/uhren/mikro-uhr-kontinuierlich-geregeltem/gallery-index/ realisiert und beschrieben.

Allerdings habe ich das so eng gebaut, dass ich an die Pendeljustierung nicht mehr rankomme... Es stellte sich leider aber - erst nach dem Zusammenbau - heraus, dass ich da justieren muss, weil sich das verstellen kann. Insofern ist das also leider ein Fehlschlag und muss etwas umkonstruiert werden. Also zerlegt, aber erstmal dokumentiert.