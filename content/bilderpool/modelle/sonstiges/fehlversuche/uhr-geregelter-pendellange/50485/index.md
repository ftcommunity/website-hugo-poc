---
layout: "image"
title: "Nur das Pendel"
date: 2024-08-21T09:38:00+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ab hier sind es Bilder vom Abbau. Das Pendel muss ja nur in Pendelrichtung steif sein, nach vorne und hinten nicht so sehr.