---
layout: "image"
title: "Gewicht von hinten"
date: 2024-08-21T09:37:29+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der Rückseite hat auch dieses Gewicht einen Neodym-Magneten. Der sagt, beim Aufziehen oben ankommend, einem Reedkontakt, er möge doch bitte den Aufzugsmotor ausschalten.