---
layout: "image"
title: "Freilaufkupplung"
date: 2024-08-21T09:37:54+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse des schwarzen Z30 links im Bild endet im schwarzen BS30 mit Bohrung. Nach vorne geht ebenfalls in ihm die Achse zur Sekundenwelle weiter. Die federnd eingehängten Kombinationen aus BS7,5 und Platte 15x15 greifen dann in die feine Verzahnung des Z10 ein. Zum Thema Freilauf siehe auch https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/getriebe/freilaufgetriebe/ bzw. für die hier verbaute Variante in "Ein einfach zu bauendes Uhrenpendel" in ft:pedia 2022-2.

Weiter unten sieht man das Z20 mit einem Arm der Pendelhemmung (endend im Winkelstein 60!).