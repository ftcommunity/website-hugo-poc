---
layout: "image"
title: "Uhrwerk"
date: 2024-08-21T09:37:36+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Uhrwerk selbst ist schon bekannt aus https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/uhren/gerauschlose-50-hz-uhr-neodym/ und https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/uhren/50-hz-pendeluhr-sekundenzeiger/gallery-index/. Es ist in ft:pedia 2023-1 ausführlich zum Nachbau beschrieben.

Hinzugekommen ist nur die Mimik mit dem Pendel. Links sieht man den M-Motor, der das Gewicht möglichst leise wieder hochziehen soll. Links unterhalb von ihm sieht man einen Reedkontakt, der von dem darüber sichtbaren (und normalerweise darunter befindlichen) kleinen Gewicht ausgelöst wird. Das ist dann, wenn das schwere Gewicht auf der anderen Seite fast ganz unten angekommen ist. Der Aufziehvorgang muss dann beginnen.