---
layout: "image"
title: "Zeitmessung"
date: 2024-08-21T09:37:52+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Außer dem Neodym-Magneten im schwarzen Z30 sieht man dahinter auch den (hier noch leeren) Reedkontakthalter, in den der Reedkontakt für die Zeitmessung hinein sollte.