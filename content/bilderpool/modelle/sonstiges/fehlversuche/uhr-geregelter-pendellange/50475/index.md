---
layout: "image"
title: "Pendelgetriebe und Freilauf ohne Uhrengetriebe"
date: 2024-08-21T09:37:47+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist der Uhrwerks-Getriebeblock herausgenommen und gibt etwas mehr Blick frei.