---
layout: "image"
title: "Kettenaufhängung des Gewichts"
date: 2024-08-21T09:38:13+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

... mitsamt dem Neodym-Magneten für den Reedkontakt "Aufziehen stoppen".