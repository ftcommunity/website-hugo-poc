---
layout: "image"
title: "Pendel (1)"
date: 2024-08-21T09:38:03+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die beiden waagerechten Achsen 110 können auf den beiden senkrechten nach oben oder unten gleiten. An ihnen soll ein Seil befestigt sein, dass oben von einem Motor mehr oder weniger aufgewickelt werden kann.