---
layout: "image"
title: "Kleines Gegengewicht"
date: 2024-08-21T09:37:33+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man das Gegengewicht in einer typischen Position