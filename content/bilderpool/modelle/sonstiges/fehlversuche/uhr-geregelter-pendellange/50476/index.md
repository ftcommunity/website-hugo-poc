---
layout: "image"
title: "Uhrengetriebe"
date: 2024-08-21T09:37:49+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist der Getriebeblock für das 3-zeigrige Uhrengetriebe - der Nachbau ist in ft:pedia 2023-1 genau beschrieben, wer's mag.