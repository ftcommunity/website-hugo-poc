---
layout: "overview"
title: "Uhr mit geregelter Pendellänge"
date: 2024-08-21T09:37:29+02:00
---

Es hätte so schön werden sollen, aber ich habe mich selber ausgetrickst: Die Justierung der Pendelhemmung geht nicht, weil sie so zugebaut ist, dass man nicht mit den Fingern rankommt... Da ist eine Neukonstruktion fällig.