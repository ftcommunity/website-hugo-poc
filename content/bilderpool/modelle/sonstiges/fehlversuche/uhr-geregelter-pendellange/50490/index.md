---
layout: "image"
title: "Blick von hinten auf die Pendelaufhängung"
date: 2024-08-21T09:38:06+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sitzt das Pendel richtig. Ich wiederhole mich: Aber man kommt halt an die Hemmung nicht ran, um sie nach einmaligem Verrutschen wieder richtig zu justieren.