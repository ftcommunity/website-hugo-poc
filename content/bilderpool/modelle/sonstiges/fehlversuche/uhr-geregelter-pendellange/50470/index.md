---
layout: "image"
title: "Pendelhemmung (5)"
date: 2024-08-21T09:37:41+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Hemmung von der Vorderseite der Uhr aus gesehen. Das Z20 kommt zwischen den beiden WS60° zu liegen.