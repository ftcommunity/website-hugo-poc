---
layout: "image"
title: "Noch ein Blick von oben"
date: 2024-08-21T09:38:08+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

... aus einem etwas anderen Winkel. Rechts von der Freilaufkupplung, direkt über (im Bild) der Kette, kann man einen Arm der Pendelhemmung sehen, der in ein Z20 eingreift. Da müsste man mit den Fingern hinkommen, um ihn zu justieren...