---
layout: "image"
title: "Pendelübersetzung und Freilaufkupplung"
date: 2024-08-21T09:37:55+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Über das Z30 oben im Bild läuft die Kette. Die zieht mit dem großen Gewicht sehr kräftig. Deshalb ist das Z30 auch mit einer zusätzlichen Metallachse 30 mit dem Z40 daneben verbunden, damit die Naben nicht durchrutschen. Es gibt also insgesamt zwei Mal eine Übersetzung Z40:Z10

Das schwarze Z30 in Bildmitte sitzt direkt vor der Freilaufkupplung zur Welle mit dem Sekundenzeiger. Das Z30 trägt einen Neodym-Magneten, mit dem über einen dritten Reed-Kontakt festgestellt wird, wann die Uhr eine Minute vergangen sieht. Mit diesem Messwert kann dann die laufende Regelung der Pendellänge berechnet werden.