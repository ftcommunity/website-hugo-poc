---
layout: "image"
title: "Nochmal der Freilauf"
date: 2024-08-21T09:37:51+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

... und untendrunter das Z20 der Pendelhemmung.