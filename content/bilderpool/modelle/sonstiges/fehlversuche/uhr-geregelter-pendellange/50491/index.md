---
layout: "image"
title: "Erahnbare Pendelhemmung"
date: 2024-08-21T09:38:07+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wieder ein Blick von unten: Man sieht unscharf ein Z20, in das die Pendelhemmung eingreift.