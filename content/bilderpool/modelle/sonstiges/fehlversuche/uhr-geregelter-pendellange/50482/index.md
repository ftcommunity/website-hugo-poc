---
layout: "image"
title: "Blick von unten beim Abbau (3)"
date: 2024-08-21T09:37:56+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unten im Bild sieht man das Z20 der Pendelhemmung.