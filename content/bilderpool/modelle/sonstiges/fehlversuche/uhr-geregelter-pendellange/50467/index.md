---
layout: "image"
title: "Testaufbau fürs Aufziehen (1)"
date: 2024-08-21T09:37:38+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zum Austesten, wieviel Gewicht ich brauche und ob das automatische Aufziehen während des Pendelns funktioniert, diente dieses Gestell.