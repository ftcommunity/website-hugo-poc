---
layout: "image"
title: "Pendelhemmung (4)"
date: 2024-08-21T09:37:43+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier drücke ich auf den von hinten gesehen linken Hemmungs-Arm, um den rechten ins Z20 eingreifen zu lassen.