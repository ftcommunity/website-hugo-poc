---
layout: "image"
title: "Pendelhemmung (6)"
date: 2024-08-21T09:37:40+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Anderer Blickwinkel