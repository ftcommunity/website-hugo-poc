---
layout: "image"
title: "Pendelhemmung (7)"
date: 2024-08-21T09:37:39+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von hinten aus gesehen. Leider können die Teile bei größeren Schlägen (z.B. wenn man mit der Hand ans Pendel kommt) verrutschen, und ab dann hat sich’s was mit sauberem Pendellauf.