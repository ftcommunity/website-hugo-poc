---
layout: "image"
title: "Abgenommenes Pendel"
date: 2024-08-21T09:38:04+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Pendel abgezogen und gibt den Blick zumindest auf alles von unten frei.