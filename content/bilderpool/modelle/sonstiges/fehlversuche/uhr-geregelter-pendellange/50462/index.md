---
layout: "image"
title: "Reedkontakt zum Starten des Aufziehens"
date: 2024-08-21T09:37:32+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Verkabelung ist nur provisorisch an einem Electronics-Modul, das als Flipflop geschaltet ist. Damit wurde das Aufziehen getestet.

Der oben verlaufende Kettenstrang zieht auf, der untere treibt das Pendel an. Es ist aber nur eine einzige endlos umlaufende Kette.