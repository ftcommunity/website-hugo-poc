---
layout: "image"
title: "Blick von unten beim Abbau (2)"
date: 2024-08-21T09:37:57+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man blickt hier auch von unten aufs Uhrwerk selbst.