---
layout: "image"
title: "Blick von rechts hinten"
date: 2024-08-21T09:38:02+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man auch in der Mitte das Z20 für die Pendelhemmung, die von unten in es eingreift. Keine Chance für eine Justage.