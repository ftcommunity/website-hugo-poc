---
layout: "image"
title: "Details zum Gewicht"
date: 2024-08-21T09:38:15+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Detailblick aufs Gewicht mit seiner Knautschzone.