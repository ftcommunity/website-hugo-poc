---
layout: "image"
title: "Blick von unten auf die Pendelaufhängung"
date: 2024-08-21T09:38:12+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Pendel pendelt auf der Metallachse, die da von hinten (im Bild rechts) ankommt. Es sitzt gerade etwas außermittig, wie man an den BS7,5 sieht, die genau unter dem BS15 mit Loch sitzen sollten.

Der XS-Motor mit Impulstaster soll direkt auf seiner Achse ein Seil per Klemmring befestigt bekommen, dass über die kleine Umlenkrolle (vor dem BS15 mit Loch) hinunter zum Pendelgewicht (den verschiebbaren Achsen 110) geht. Damit soll die wirksame Pendellänge in Impulstaster-Schritten verändert werden können, um die Laufgeschwindigkeit der Uhr im Betrieb ständig nachregeln zu können.

Man sieht es hier nicht, aber der obere der beiden BS7,5 steckt mit einem Strebenadapter (31848 bzw. 111881) in der Statikschiene. Das Pendel besteht ist zwei Schienen lang.