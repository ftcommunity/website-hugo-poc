---
layout: "image"
title: "Pendelhemmung (3)"
date: 2024-08-21T09:37:44+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dasselbe von der Rückseite der Uhr aus gesehen.