---
layout: "image"
title: "Blick von der Seite"
date: 2024-08-21T09:37:34+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das schwere Gewicht (auf der anderen Seite) steht gerade wirklich auf dem Boden, weil ich die Kette nicht monatelang belasten wollte. Deshalb ist das kleine Gewicht hier (schwarzes Kettenzahnrad Z20 mit den 3 BS7,5 und den zwei BS5) hier zu hoch. Sobald es (von unten nämlich) am Reedkontakt vorbeikommt, löst der an seiner linken Seite sichtbare Neodym-Magnet das Aufziehen aus.