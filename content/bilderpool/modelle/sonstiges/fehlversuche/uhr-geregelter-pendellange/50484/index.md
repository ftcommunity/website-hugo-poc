---
layout: "image"
title: "Blick von unten beim Abbau (1)"
date: 2024-08-21T09:37:58+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Pendelaufhängung, Justiermotor mit Umlenkrolle fürs Justierseil.