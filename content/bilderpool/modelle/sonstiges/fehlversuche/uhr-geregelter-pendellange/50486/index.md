---
layout: "image"
title: "Blick von rechts vorne"
date: 2024-08-21T09:38:01+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht einen Teil der Übersetzung (Z10/Z40) fürs Pendel, links davon die Freilaufkupplung - und wieder fast nichts von der Pendelhemmung.

Deshalb gibts Bilder von der während der Zerlegung.