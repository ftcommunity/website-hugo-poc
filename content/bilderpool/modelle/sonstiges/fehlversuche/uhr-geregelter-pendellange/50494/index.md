---
layout: "image"
title: "Blick von hinten"
date: 2024-08-21T09:38:11+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht die Zahnräder für die Aufzugskette sowie einige Zahnräder für die Übersetzung von dieser Kette auf die Pendelhemmung. Irgendwo da drinnen versteckt ist die Pendelhemmung, die justiert werden muss - und an die man nicht mehr rankommt... Grmpf.