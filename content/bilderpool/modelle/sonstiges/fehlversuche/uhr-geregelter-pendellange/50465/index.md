---
layout: "image"
title: "Testaufbau fürs Aufziehen (2)"
date: 2024-08-21T09:37:35+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Pendelei und Aufzieherei funktionierte gut. Nur hatte ich nicht damit gerechnet, dass ich die Hemmungsarme nachjustieren muss, wenn schon alles aufgebaut und zu-gebaut ist. Deshalb muss ich das nochmal anders entwerfen, sodass die Pendelhemmung entweder sich gar nicht verjustieren kann oder sie leicht zugänglich ist.