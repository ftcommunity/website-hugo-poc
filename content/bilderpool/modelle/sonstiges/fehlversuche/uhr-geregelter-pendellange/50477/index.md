---
layout: "image"
title: "Pendel (2)"
date: 2024-08-21T09:37:50+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Pendel von der rechten Seite aus gesehen.