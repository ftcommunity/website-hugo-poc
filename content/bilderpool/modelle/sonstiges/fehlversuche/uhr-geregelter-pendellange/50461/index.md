---
layout: "image"
title: "Pendelgewicht"
date: 2024-08-21T09:37:30+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Gewicht - echt schwer, voll mit Metall - sorgt für den Antrieb des Pendels.

Für den Fall, dass mal ein Kettenglied reißen sollte (auf einer Seite sind mir dabei tatsächlich schon zwei kaputt gegangen) und das Gewicht auf den Boden knallen sollte, hat es unten eine "Knautschzone". Das Gewicht - die beiden metallgefüllten Kästchen - kann mit den Befestigungszapfen in den Nuten der beiden BS30-Reihen mit viel Kraft verschoben werden. Das sollte die Aufprallenergie dämpfen, sodass hoffentlich kein Baustein und auch die nicht die 1000er-Platte am Boden beschädigt werden sollten. Es kam bisher aber zum Glück nicht dazu.