---
layout: "image"
title: "Pendelhemmung (1)"
date: 2024-08-21T09:37:46+02:00
picture: "2023-01-07_uhr_mit_geregelter_pendellaenge_32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Endlich ist der Blick aufs Pendel-Z20 mal frei. Links der höher stehende Arm der Pendelhemmung, rechts sieht man direkt hinter dem schwarzen Baustein den WS60° des anderen Arms gerade ins Z20 eingreifen.