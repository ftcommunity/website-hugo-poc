---
layout: "image"
title: "Hebe-/Senkantrieb Waschbürste"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39001
- /details292c.html
imported:
- "2019"
_4images_image_id: "39001"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39001 -->
Die Gegengewichte in den roten 9V-Boxen rechts sind so austariert, das der Arm nahezu im Gleichgewicht ist.
Leider ist das größte Problem, dass die Motoren natürlich viel zu schnell und ungenau sind. Da müsste was her mit ganz wenigen Umdrehungen pro Minute.
