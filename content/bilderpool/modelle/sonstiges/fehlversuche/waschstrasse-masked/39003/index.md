---
layout: "image"
title: "Rollenende angetrieben"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked7.jpg"
weight: "7"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39003
- /details27c2.html
imported:
- "2019"
_4images_image_id: "39003"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39003 -->
Das angetriebene Ende mit den zwei Linsenlampen mit zusätzlichen Reflektoren (von zwei kaputten 24V-Birnchen aus dem Industrieprogramm).
