---
layout: "image"
title: "Zapu_6629.jpg"
date: "2011-11-01T16:51:48"
picture: "Zapu_6629_Chevyfahrer.jpg"
weight: "6"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33372
- /detailsb2c2-2.html
imported:
- "2019"
_4images_image_id: "33372"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T16:51:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33372 -->
Das ist eine Zahnradpumpe, die auch wirklich pumpt. Höhe ca. 45 mm (entspricht also 3x BS15). Der Motor käme von der Rückseite dran.

Vielen Dank an Chevyfahrer, der mir dieses Teil überlassen hat!

Gruß,
Harald
