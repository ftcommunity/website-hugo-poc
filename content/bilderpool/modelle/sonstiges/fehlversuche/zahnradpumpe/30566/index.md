---
layout: "image"
title: "Zapu42.JPG"
date: "2011-05-14T22:18:56"
picture: "Zapu42.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30566
- /detailsec71.html
imported:
- "2019"
_4images_image_id: "30566"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-05-14T22:18:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30566 -->
Ich hatte gehofft, dass die 'arbeitende' Bauhöhe der ft-Differenziale die schlechte Abdichtung an den Enden wettmachen würde. Selbst mit dem nachträglich eingedrückten gelben Gummiring war keine Verbesserung zu sehen.
