---
layout: "comment"
hidden: true
title: "14259"
date: "2011-05-15T13:32:57"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
ich habe in dieser Hinsicht auch schon viele Experimente gemacht. Wichtig ist, dass allseitig um die Zahnräder eine enge Toleranz eingehalten wird- es darf einerseits nichts schleifen, andererseits nur einige 1/100mm Spiel haben, auch in der Dicke.
Ich hatte damals sogar eine CNC-Maschine zur Verfügung und habe die Gehäuse aus Messing gefräst. Ziel war ein Druck von 2...5 bar, aber das habe ich nie erreicht.
Ein anderer Schwachpunkt sind die Zahnräder. Obwohl die ft-Zahnräder relativ präzise sind und eine Elvolventenverzahnung haben, wird es in der Verzahnung auch nicht genügend dicht.
Die besten Ergebnisse hatte ich mit einer Kraftstoffpumpe von Conrad (selbst die bringt aber keine 2 bar), ich habe auch schon einiges Equipment im Auge, wie z.B. Schnellverbinder von Festo für 2mm-Schlauch (oder waren es 3mm?). Liegt alles in einer Kiste und wartet darauf, zur Anwendung zu kommen... Aber bevor die Pumpenfrage nicht gelöst ist, machen die anderen Komponenten wenig Sinn.
Leider habe ich momentan keinen Zugriff mehr auf Dreh- /Fräsmaschinen.