---
layout: "image"
title: "Zapu36.jpg"
date: "2011-05-14T22:13:09"
picture: "Zapu36.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30564
- /detailsa70a-2.html
imported:
- "2019"
_4images_image_id: "30564"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-05-14T22:13:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30564 -->
Die Animation von Wikipedia:
http://upload.wikimedia.org/wikipedia/commons/f/f8/Gear_pump_animation.gif
sieht ja soo simpel und einfach aus, und reizt zum Nachbauen. Ein sehr müder Strahl ist aber alles, was das Gerät zu Stande bringt, und das auch nur bei voller Drehzahl des Motors.
