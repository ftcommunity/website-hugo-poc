---
layout: "comment"
hidden: true
title: "14395"
date: "2011-06-05T14:03:39"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das Foto ohne die Antriebsachsen täuscht ein wenig. Mit den Achsen haben die Zahnräder ein wenig Druck nach außen, und dort hatte ich gehofft, das Kammervolumen halbwegs dicht halten zu können. Es wird auch nur ein Zahnrad angetrieben, so dass die Zähne zwangsläufig Kontakt haben. Nutzt aber alles nichts. Unterm Strich ist halt die notwendige Präzision mit Bastelmitteln einfach nicht hin zu kriegen.

Gruß,
Harald