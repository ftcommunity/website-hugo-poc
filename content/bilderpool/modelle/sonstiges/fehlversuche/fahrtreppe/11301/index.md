---
layout: "image"
title: "Rolltreppe04.JPG"
date: "2007-08-05T17:07:10"
picture: "Rolltreppe04.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11301
- /detailsc293.html
imported:
- "2019"
_4images_image_id: "11301"
_4images_cat_id: "1016"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:07:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11301 -->
