---
layout: "image"
title: "Riesen Kompressor 04"
date: "2008-03-23T22:30:50"
picture: "20050224_Fischertechnik_Kompressor_08.jpg"
weight: "4"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14048
- /details5fc2.html
imported:
- "2019"
_4images_image_id: "14048"
_4images_cat_id: "1287"
_4images_user_id: "327"
_4images_image_date: "2008-03-23T22:30:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14048 -->
