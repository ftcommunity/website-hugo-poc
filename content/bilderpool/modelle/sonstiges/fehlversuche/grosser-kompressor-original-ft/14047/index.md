---
layout: "image"
title: "Riesen Kompressor 03"
date: "2008-03-23T22:30:50"
picture: "20050224_Fischertechnik_Kompressor_05.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14047
- /details3188-2.html
imported:
- "2019"
_4images_image_id: "14047"
_4images_cat_id: "1287"
_4images_user_id: "327"
_4images_image_date: "2008-03-23T22:30:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14047 -->
