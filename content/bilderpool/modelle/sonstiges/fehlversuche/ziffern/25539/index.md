---
layout: "image"
title: "Ziffern 6 und 9 mit jüngeren WS30-Varianten"
date: "2009-10-11T18:13:03"
picture: "mitjuengerenwsvarianten1.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25539
- /detailsb6a2-2.html
imported:
- "2019"
_4images_image_id: "25539"
_4images_cat_id: "1785"
_4images_user_id: "104"
_4images_image_date: "2009-10-11T18:13:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25539 -->
Links liegt die "6" mit den älteren, kleineren WS30 gebaut, rechts eine Alternative mit den neueren, etwas größeren. Um die 9 zu sehen,...

- mache man einfach einen Handstand,
- drehe man seinen Monitor um 180°,
- drehe man den Monitor um 90° und seinen Kopf um 90° in die entgegengesetzte Richtung,
- halte man einen Spiegel flach unter oder über den Monitor und blicke hinein,
- erstelle man ein Bildschirmfoto und drehe es auf beliebige Weise um 180°,
- stelle man sich die Ziffern einfach gedreht vor, oder
- baue die Teile nach und setze sich auf die andere Seite des Tisches.

:-)
