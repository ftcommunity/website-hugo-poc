---
layout: "image"
title: "Gesamtansicht von oben"
date: "2007-01-20T19:32:30"
picture: "Laufroboter1-02b.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8575
- /detailsc6e6.html
imported:
- "2019"
_4images_image_id: "8575"
_4images_cat_id: "790"
_4images_user_id: "488"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8575 -->
Habe dann versucht, die Achsen mittels Hülsen gegeneinander zu stützen, aber das hat auch nicht geholfen. Die Hülsen hätten viel länger sein müssen, um eine angemessene Wirkung zu erzielen.