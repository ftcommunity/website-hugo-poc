---
layout: "image"
title: "Ansicht von schräg hinten"
date: "2007-01-20T19:32:30"
picture: "Laufroboter1-03b.jpg"
weight: "3"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8576
- /detailsbd97.html
imported:
- "2019"
_4images_image_id: "8576"
_4images_cat_id: "790"
_4images_user_id: "488"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8576 -->
Mit Differentialen wäre es vielleicht gegangen, aber auch dann wären die Kräfte bedingt durch die langen Hebel zu groß gewesen. Außerdem haben die Z20 ziemlich fest auf den Z10 der Motoren geklemmt.