---
layout: "image"
title: "1. Gelenk"
date: "2011-08-30T19:15:00"
picture: "bagger21.jpg"
weight: "21"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31722
- /details27f0.html
imported:
- "2019"
_4images_image_id: "31722"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31722 -->
