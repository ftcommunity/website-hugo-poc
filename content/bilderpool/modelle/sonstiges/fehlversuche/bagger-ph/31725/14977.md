---
layout: "comment"
hidden: true
title: "14977"
date: "2011-08-30T21:44:01"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das sieht alles so sauber gebaut (und fotografiert!) aus - das ist aber schade, dass Du das zerlegt hast! Gabs da einen realen Vorbild-Bagger?

(Du weißt schon, dass Du Fotos im Publisher mit einem Mausklick drehen kannst?)

Gruß,
Stefan