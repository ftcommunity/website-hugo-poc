---
layout: "image"
title: "Bagger"
date: "2011-08-30T19:15:00"
picture: "bagger15.jpg"
weight: "15"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/31716
- /details84ae-3.html
imported:
- "2019"
_4images_image_id: "31716"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31716 -->
Von vorne