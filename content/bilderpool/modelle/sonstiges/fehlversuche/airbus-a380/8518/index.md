---
layout: "image"
title: "BugFW50.JPG"
date: "2007-01-19T13:33:13"
picture: "BugFW50.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8518
- /detailsfb40-2.html
imported:
- "2019"
_4images_image_id: "8518"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T13:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8518 -->
Bugfahrwerk eingefahren. Die Kamera schaut von oben, Flugrichtung ist nach links unten.
