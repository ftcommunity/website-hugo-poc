---
layout: "image"
title: "A380-Erlkönig15.JPG"
date: "2007-01-19T16:06:14"
picture: "A380-Erlknig15.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8527
- /detailsdb86.html
imported:
- "2019"
_4images_image_id: "8527"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T16:06:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8527 -->
Das Hauptfahrwerk ist voll ausgefahren und (mangels Riegel oder Kniegelenk-Mechanik) nicht verriegelt.

Zum Ausgleich von Bodenunebenheiten können die Fahrwerksträger mit den 6 Rädern dran vor- und zurückpendeln. Das geschieht über ein Trapez mit 4 Gelenkwürfeln je Seite. Dessen lange Seite liegt unten zwischen den Rädern; die kurze Seite liegt hinter der roten Platte 15x30 in Bildmitte unten.
