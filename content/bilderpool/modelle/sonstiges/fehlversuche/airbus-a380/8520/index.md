---
layout: "image"
title: "Kabinenmodul26.JPG"
date: "2007-01-19T13:45:13"
picture: "Kabinenmodul26.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8520
- /details34cb.html
imported:
- "2019"
_4images_image_id: "8520"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T13:45:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8520 -->
Die Kabine soll aus einzelnen Modulen bestehen. Jedes davon fasst 12 Sitze (3 Reihen, je zwei Sitze links und rechts). 

Der Mittelgang ist dabei noch üppig breit, weil der Boden aus Statikplatten 180x90 besteht, die quer auf die gelben Winkelträger (links+rechts unten) gelegt werden. Alles andere führt zu Stückwerk und mehr Gewicht. 

Im Gegenzug wird die Zelle einfach riesig und macht es unmöglich, einen Maßstab für das ganze Modell durchzuhalten. Vom Maßstab-halten habe ich mich sowieso verabschiedet, denn dann müsste ich in der Grundversion schlappe 550 ft-Figuren unterbringen.
