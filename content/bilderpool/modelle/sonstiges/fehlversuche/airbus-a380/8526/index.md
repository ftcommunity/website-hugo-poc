---
layout: "image"
title: "A380-Erlkönig09.JPG"
date: "2007-01-19T15:58:47"
picture: "A380-Erlknig09.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8526
- /details2093-2.html
imported:
- "2019"
_4images_image_id: "8526"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T15:58:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8526 -->
Ganz links am Rand: der Minimot, der die äußeren Fahrwerke einfährt. Die Kegelräder am U-Träger sorgen dafür, dass die beiden Achsen gegenläufig drehen. Die Achsen treiben über Klemmschnecken, 1x Z20, dann 1x Z30 die Viergelenkhebel an (eine Schnecke sieht man hier: http://www.ftcommunity.de/details.php?image_id=8525 )

Unter dem Minimot sitzt der Power-Mot fürs große Fahrwerk. Der Antrieb geht über eine Schnecke 35977 von unten aufs Differenzial, dann über die Z10 und Z20 auf die Z40, die das Viergelenk antreiben. Das Differenzial sitzt hier nur drinnen, weil man es mit der Schnecke antreiben kann und es zwei Ausgänge hat. Auszugleichen gibt es nichts, denn linke und rechte Seite sind über die Metallachse (mitten im Bild) starr gekoppelt.

Das Hauptfahrwerk wird derzeit noch nicht verriegelt: wenn die Viergelenkhebel beiderseits der Messingachse (die gerade den schräg im Raum liegenden Taster betätigt) einknicken, dann sackt der Flieger ab.

Die Luken fürs Hauptfahrwerk sind schon da, nur ihre hintere Lagerung fehlt noch.

Ein paar Regentropfen haben fischertechnik noch nie geschadet.
