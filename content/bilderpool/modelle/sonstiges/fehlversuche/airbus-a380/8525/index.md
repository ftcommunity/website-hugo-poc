---
layout: "image"
title: "A380-Erlkönig02.JPG"
date: "2007-01-19T14:11:58"
picture: "A30-Erlknig02.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8525
- /details4eb6-2.html
imported:
- "2019"
_4images_image_id: "8525"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T14:11:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8525 -->
Der besteht noch aus mehr Löchern als Materie, aber irgendwie muss es ja anfangen.

Die K-Achse im Bugfahrwerk wird beim Einrasten wirklich gebogen, aber hier hat die Kamera etwas übertrieben.


Um im Maßstab (ca. 1:30) zu bleiben, müssten die Räder doppelt so groß werden (dann passen sie aber nicht mehr in die Schächte), die Zelle müsste so ca. 2,50 m lang werden (dann passt sie nicht mehr ins Auto) und der Flieger müsste ca. 2,30 m Spannweite haben (dann wird der Eingang vom DGH in Mörshausen zu eng).
