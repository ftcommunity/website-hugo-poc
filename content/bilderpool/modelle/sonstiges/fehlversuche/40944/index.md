---
layout: "image"
title: "Ostereier Färbe Maschine - ohne motorisiertem Antrieb"
date: "2015-05-03T19:52:12"
picture: "OstereierFaerbeMaschine.jpg"
weight: "3"
konstrukteure: 
- "pinkepunk"
fotografen:
- "pinkepunk"
schlagworte: ["Küche", "Mischgerät", "Mixer", "Ostereier", "färben"]
uploadBy: "pinkepunk"
license: "unknown"
legacy_id:
- /php/details/40944
- /details97e8.html
imported:
- "2019"
_4images_image_id: "40944"
_4images_cat_id: "716"
_4images_user_id: "2431"
_4images_image_date: "2015-05-03T19:52:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40944 -->
Vor einigen Jahren hatten wir zu Ostern ein neuartiges Färbeset für Ostereier gekauft. In einem mitgelieferten Becher wurde Reis eingefärbt und das hartgekochte Ei darin geschüttelt. Die Eier erhielten durch das Verfahren eine interessante Färbung/Musterung. Das Schütteln hatte allerdings auch seine Tücken.
Die hier vorgestellte Maschine sollte ähnliches leisten: Das Ei wird zusammen mit gefärbtem Reis in ein rotierendes Behältnis (aus dem Drogeriemarkt) gelegt. Durch einen moderaten Motorantrieb sollte das Ei gleichmäßig umgewälzt und schonend eingefärbt werden.  Allerdings war das Ergebnis mit den heuer verwendeten Kaltfarben in Reis eher blass und ernüchternd. Insofern bezieht sich die Einordnung in die Kategorie Fehlversuche nicht in erster Linie auf FT. Wer hat weitere Ideen für den konkreten Einsatz von Fischertechnik in der Küche?
Ich habe in der Abbildung den nur halbwegs küchentauglichen Motorantrieb mit Batterieunterstützung  weggelassen.