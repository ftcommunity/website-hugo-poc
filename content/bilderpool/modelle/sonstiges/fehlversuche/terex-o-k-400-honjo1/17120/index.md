---
layout: "image"
title: "Kranhaken"
date: "2009-01-21T16:56:56"
picture: "honjo_raupenkran_11.jpg"
weight: "4"
konstrukteure: 
- "honjo1"
fotografen:
- "honjo1"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/17120
- /details0c2f.html
imported:
- "2019"
_4images_image_id: "17120"
_4images_cat_id: "1535"
_4images_user_id: "14"
_4images_image_date: "2009-01-21T16:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17120 -->
mit gewichten zum seilauszug