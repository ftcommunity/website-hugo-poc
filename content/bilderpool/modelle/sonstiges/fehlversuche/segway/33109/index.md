---
layout: "image"
title: "Erste Version des Programms"
date: "2011-10-05T19:20:26"
picture: "segway2_2.jpg"
weight: "9"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
schlagworte: ["Segway", "Programm"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/33109
- /detailsebe8.html
imported:
- "2019"
_4images_image_id: "33109"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T19:20:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33109 -->
Dies ist die erste Version des Programms.