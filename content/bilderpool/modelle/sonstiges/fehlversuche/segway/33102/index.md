---
layout: "image"
title: "Von oben/Interface"
date: "2011-10-05T11:04:35"
picture: "segway2.jpg"
weight: "2"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
schlagworte: ["Segway"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/33102
- /details490f-2.html
imported:
- "2019"
_4images_image_id: "33102"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T11:04:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33102 -->
Ansicht von oben.
Als Interface habe ich das ROBO Interface verwendet. Dieses sollte eigentlich schnell genug sein.