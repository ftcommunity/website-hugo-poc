---
layout: "overview"
title: "Segway"
date: 2020-02-22T08:28:08+01:00
legacy_id:
- /php/categories/2438
- /categories7333.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2438 --> 
Dies ist ein erster fehlgeschlagener Versuch eines Segways. Er konnte sich zwar mal für 2 Sekunden halten, aber mehr auch nicht. Auch die Verlagerung des Schwerpunktes nach oben half nichts.