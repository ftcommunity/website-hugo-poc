---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7693
- /details3589.html
imported:
- "2019"
_4images_image_id: "7693"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7693 -->
Man sieht auch einige unglaublich präzise ;-) Zeichnungen. Die Seile drehten natürlich an den Reibzahnrädern trotz Klemmung durch. So etwas wie "Wiederholgenauigkeit" gab es nicht. Mit Ketten hätte es eher geklappt.
