---
layout: "image"
title: "Stiftaufnahme (1)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7665
- /detailsd42f-2.html
imported:
- "2019"
_4images_image_id: "7665"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7665 -->
Der Plotter sollte Stifte wechseln können. Dies hier hätte wie eine Kugelschreibermechanik wirken sollen. Hat es aber natürlich nicht.
