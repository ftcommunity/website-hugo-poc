---
layout: "image"
title: "Antrieb (4)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7674
- /detailsdd88-2.html
imported:
- "2019"
_4images_image_id: "7674"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7674 -->
Beide Klemm-Zahnrädchen wurden von unten gegenläufig angetrieben.
