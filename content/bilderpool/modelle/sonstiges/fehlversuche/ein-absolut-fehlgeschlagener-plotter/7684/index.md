---
layout: "image"
title: "Stiftheber (3)"
date: "2006-12-03T21:28:39"
picture: "plotterfehlschlag21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7684
- /detailsc2fe.html
imported:
- "2019"
_4images_image_id: "7684"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:39"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7684 -->
Von Oben. Die grüne Rolle wird per Federgelenkstein nach unten gedrückt. Wenn der Stift oben angekommen ist, kann er nicht mehr weiter. Dann wird die Rolle nach oben gezogen und betätigt den oberen Endtaster. Auch die Taster sind also außerhalb des eigentlichen Plotters angebracht.
