---
layout: "image"
title: "Stiftheber (1)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7682
- /detailsb480-2.html
imported:
- "2019"
_4images_image_id: "7682"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7682 -->
Dies hier ist der Antrieb für das Seil, was den Stift anhebt oder senkt. Auch der ist fest auf der Grundplatte montiert.
