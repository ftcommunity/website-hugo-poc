---
layout: "image"
title: "Stiftheber (4)"
date: "2006-12-03T21:28:39"
picture: "plotterfehlschlag22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7685
- /details2eae.html
imported:
- "2019"
_4images_image_id: "7685"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:39"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7685 -->
Das blaue ft-Seil wird also von außen gezogen oder losgelassen und hebt bzw. senkt über diese Umlenkrollen den eigentlichen Stiftträger, der wiederum an zwei Stahlachsen auf und ab gleiten kann.
