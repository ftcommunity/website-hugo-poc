---
layout: "image"
title: "Drucker"
date: "2009-05-26T13:53:28"
picture: "druckkopf-fischertechnik.jpg"
weight: "14"
konstrukteure: 
- "mccabe.peter@hotmail.com"
fotografen:
- "mccabe.peter@hotmail.com"
schlagworte: ["Drucker"]
uploadBy: "mccabe.peter@hotmail.com"
license: "unknown"
legacy_id:
- /php/details/24104
- /detailse49a.html
imported:
- "2019"
_4images_image_id: "24104"
_4images_cat_id: "323"
_4images_user_id: "965"
_4images_image_date: "2009-05-26T13:53:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24104 -->
Drucker aus Fischertechnik welcher über ein Interface mit .NET angesteuert wird.
Mehr Bilder und Video gibts unter http://flyer-vulkan.de/fischertechnik

Gruß