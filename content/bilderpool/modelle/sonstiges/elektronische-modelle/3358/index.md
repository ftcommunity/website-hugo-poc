---
layout: "image"
title: "16Bit.jpg"
date: "2004-11-28T15:06:46"
picture: "ft16bit_resize.jpg"
weight: "1"
konstrukteure: 
- "hildegunde"
fotografen:
- "-?-"
schlagworte: ["ff", "flipflop", "silberlinge", "16"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3358
- /details358b-2.html
imported:
- "2019"
_4images_image_id: "3358"
_4images_cat_id: "283"
_4images_user_id: "120"
_4images_image_date: "2004-11-28T15:06:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3358 -->
16Bit-Zähler aus FF-Silberlingen von hildegunde