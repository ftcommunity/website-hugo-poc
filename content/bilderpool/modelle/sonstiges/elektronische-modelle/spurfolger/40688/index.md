---
layout: "image"
title: "schräge Draufsicht"
date: "2015-03-29T20:33:31"
picture: "spurfolger1.jpg"
weight: "1"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40688
- /details908f.html
imported:
- "2019"
_4images_image_id: "40688"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40688 -->
Der Akku sitzt an der Vorderseite des Fahrzeugs.