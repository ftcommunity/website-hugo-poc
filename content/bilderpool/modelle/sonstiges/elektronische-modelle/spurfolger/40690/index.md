---
layout: "image"
title: "Linke Seite"
date: "2015-03-29T20:33:31"
picture: "spurfolger3.jpg"
weight: "3"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40690
- /details9c0b.html
imported:
- "2019"
_4images_image_id: "40690"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40690 -->
Die Strebe und der graue Mini-taster bilden den etwas improvisierten Ein/Aus Schalter. In der Mitte ist der 2te S-Motor zu sehen.