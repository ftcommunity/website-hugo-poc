---
layout: "image"
title: "Ohne Akku"
date: "2015-03-29T20:33:31"
picture: "spurfolger8.jpg"
weight: "8"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40695
- /details79b4.html
imported:
- "2019"
_4images_image_id: "40695"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40695 -->
Hier nochmal ein Bild mit ausgebautem Akku.