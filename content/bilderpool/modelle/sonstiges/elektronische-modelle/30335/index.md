---
layout: "image"
title: "LCD 02"
date: "2011-03-30T16:49:54"
picture: "LCD02.jpg"
weight: "4"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/30335
- /detailse83d.html
imported:
- "2019"
_4images_image_id: "30335"
_4images_cat_id: "283"
_4images_user_id: "1177"
_4images_image_date: "2011-03-30T16:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30335 -->
hier ein 16*1 LCD
links der poti zur Reglung des Kontrastes
als Controller habe ich einen atmega88 verwendet
das LCD läuft im 4 bit modus
