---
layout: "image"
title: "Ein Tisch"
date: "2009-09-20T19:18:07"
picture: "verschiedenemodelle2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/24997
- /detailse262.html
imported:
- "2019"
_4images_image_id: "24997"
_4images_cat_id: "1768"
_4images_user_id: "374"
_4images_image_date: "2009-09-20T19:18:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24997 -->
Bau eines Tisches mit den neuen roten Statik-Winkelträgern