---
layout: "image"
title: "4"
date: "2010-07-09T08:53:32"
picture: "solarventilator04.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27727
- /details1641.html
imported:
- "2019"
_4images_image_id: "27727"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27727 -->
Der Ventilator von etwas dichter dran.