---
layout: "image"
title: "10"
date: "2010-07-09T08:53:32"
picture: "solarventilator10.jpg"
weight: "10"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27733
- /details80f7.html
imported:
- "2019"
_4images_image_id: "27733"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27733 -->
Hier ist die Bohrung zu sehen.