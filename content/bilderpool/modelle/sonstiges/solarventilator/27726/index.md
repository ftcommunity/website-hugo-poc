---
layout: "image"
title: "3"
date: "2010-07-09T08:53:32"
picture: "solarventilator03.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27726
- /detailse1db.html
imported:
- "2019"
_4images_image_id: "27726"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27726 -->
Aus dem Kofferraum fotgrafiert: Der Ventilator
Er ist so ausgerichtet das er mich genau anpustet, wenn ich hinten in der mitte sitze. Da er aber relativ schwach ist, stört er auch nicht, es weht einem halt ein laues Lüftchen entgegen. Vor allem an sehr heißen Tagen ist das sehr angenehm.