---
layout: "image"
title: "Bagger 03"
date: "2011-01-22T19:45:39"
picture: "pneumodelle5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29758
- /details9be1.html
imported:
- "2019"
_4images_image_id: "29758"
_4images_cat_id: "2188"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29758 -->
