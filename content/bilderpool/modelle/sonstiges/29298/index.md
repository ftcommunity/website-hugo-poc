---
layout: "image"
title: "Zerfressene Rastachse"
date: "2010-11-20T21:59:42"
picture: "zerfressene_Rastachse_45.jpg"
weight: "15"
konstrukteure: 
- "Verusacher: Zahnrad"
fotografen:
- "WERNER"
schlagworte: ["zerfressen", "hungrig", "45", "Rastachse"]
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/29298
- /details96d7-2.html
imported:
- "2019"
_4images_image_id: "29298"
_4images_cat_id: "323"
_4images_user_id: "1196"
_4images_image_date: "2010-11-20T21:59:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29298 -->
Ich habe ein Zahnrad (31022) auf die abgebildete Achse geschraubt.
Nach kurzer Zeit hat die Narbe (35031) des Rades die Stange zerfressen!