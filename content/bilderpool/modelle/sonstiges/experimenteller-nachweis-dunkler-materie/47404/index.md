---
layout: "image"
title: "Waage belastet - Zeiger"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie6.jpg"
weight: "6"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47404
- /details0b73-2.html
imported:
- "2019"
_4images_image_id: "47404"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47404 -->
Der dunklere Probekörper befindet sich auf der linken Waagschale. Bauartbedingt schlägt der Zeiger der Waage allerdings in Richtung des leichteren Probestücks aus. Der Ausschlag ist jedoch sehr deutlich zu erkennen.