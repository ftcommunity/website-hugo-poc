---
layout: "image"
title: "Waage unbelastet - Zeiger"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie4.jpg"
weight: "4"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47402
- /details8429.html
imported:
- "2019"
_4images_image_id: "47402"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47402 -->
Damit man auch die Feinheiten erkennt, ist hier der Zeiger im Fokus. Die Nullstellung der unbelasteten Waage ist klar zu erkennen.