---
layout: "overview"
title: "Experimenteller Nachweis Dunkler Materie"
date: 2020-02-22T08:32:24+01:00
legacy_id:
- /php/categories/3502
- /categories6262.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3502 --> 
An dieser Stelle einen herzlichen Dank an Frau Dr. Dr. N. Hell-Seeker, die mir die hochauflösenden Bilder der Versuchsapparatur zur Verfügung gestellt hat.