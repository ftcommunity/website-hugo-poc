---
layout: "image"
title: "Probekörper (2)"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie2.jpg"
weight: "2"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47400
- /detailsafb1.html
imported:
- "2019"
_4images_image_id: "47400"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47400 -->
Um sämtliche Einflüsse unterschiedlichen Lichteinfalles auf die Helligkeit auszuschliessen, sind die Probekörper in diesem Bild nochmals vertauscht angeordnet. Man sieht, dass die Probestücke tatsächlich eine unterschiedliche Färbung aufweisen.