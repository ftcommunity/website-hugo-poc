---
layout: "image"
title: "Nahaufnahme"
date: "2007-01-28T17:07:10"
picture: "Nahaufnahme.jpg"
weight: "3"
konstrukteure: 
- "jko"
fotografen:
- "jko"
uploadBy: "jko"
license: "unknown"
legacy_id:
- /php/details/8716
- /detailsc38b.html
imported:
- "2019"
_4images_image_id: "8716"
_4images_cat_id: "798"
_4images_user_id: "540"
_4images_image_date: "2007-01-28T17:07:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8716 -->
