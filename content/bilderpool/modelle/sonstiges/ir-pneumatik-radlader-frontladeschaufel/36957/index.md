---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - DETAIL  Dioden 2"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon7.jpg"
weight: "7"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36957
- /detailsd596-2.html
imported:
- "2019"
_4images_image_id: "36957"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36957 -->
