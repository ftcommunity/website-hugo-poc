---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - DETAIL  Dioden 3"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon8.jpg"
weight: "8"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36958
- /detailsaff9.html
imported:
- "2019"
_4images_image_id: "36958"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36958 -->
