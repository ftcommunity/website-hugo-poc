---
layout: "image"
title: "Der fertige Kopf"
date: "2016-12-15T17:20:57"
picture: "movinghead12.jpg"
weight: "12"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44910
- /details0893-2.html
imported:
- "2019"
_4images_image_id: "44910"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44910 -->
Der fertige Kopf