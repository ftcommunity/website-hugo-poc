---
layout: "image"
title: "Das Interface"
date: "2016-12-15T17:20:57"
picture: "movinghead15.jpg"
weight: "15"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44913
- /detailsa3d5-2.html
imported:
- "2019"
_4images_image_id: "44913"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44913 -->
Das Interface wurde auf dem Drehkranz befestigt, da ich keine Schleifkontakte besitze und sich das Modell somit trotzdem frei Drehen kann