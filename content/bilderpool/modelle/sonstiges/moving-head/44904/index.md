---
layout: "image"
title: "Linse mit Verkleidung"
date: "2016-12-15T17:20:56"
picture: "movinghead06.jpg"
weight: "6"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44904
- /details080e.html
imported:
- "2019"
_4images_image_id: "44904"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44904 -->
Durch die Verkleidung wird verhindert, dass das Licht auch "nebendran" vorbeigeht. Allerdings wird die Helligkeit durch die teilweise Abdeckung leicht verringert.