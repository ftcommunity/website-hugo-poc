---
layout: "image"
title: "Oberer Schrittzähler"
date: "2016-12-15T17:20:57"
picture: "movinghead17.jpg"
weight: "17"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44915
- /details29ad.html
imported:
- "2019"
_4images_image_id: "44915"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44915 -->
Oberer Schrittzähler