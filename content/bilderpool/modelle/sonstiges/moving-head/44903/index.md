---
layout: "image"
title: "Abstand zur Linse"
date: "2016-12-15T17:20:56"
picture: "movinghead05.jpg"
weight: "5"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44903
- /detailsa06e.html
imported:
- "2019"
_4images_image_id: "44903"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44903 -->
Hier wurde der Abstand zur Linse experimentell eingestellt. Als Linse dient eine abgesägte Lupe.