---
layout: "image"
title: "LED-Modul"
date: "2016-12-15T17:20:56"
picture: "movinghead03.jpg"
weight: "3"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44901
- /details4ff2.html
imported:
- "2019"
_4images_image_id: "44901"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44901 -->
Das LED-Modul in zwei Statikbausteine eingesetzt. Da das Modul nur für 4,5V ausgelegt ist, wurde noch ein Vorwiderstand angelötet