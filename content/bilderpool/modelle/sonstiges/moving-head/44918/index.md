---
layout: "image"
title: "Draufsicht"
date: "2016-12-15T17:20:57"
picture: "movinghead20.jpg"
weight: "20"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/44918
- /details650d.html
imported:
- "2019"
_4images_image_id: "44918"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44918 -->
Der verdrillte Draht kommt von den Kontakten an der Achse und führt dann zum LED-Modul