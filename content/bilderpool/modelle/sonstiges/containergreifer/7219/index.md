---
layout: "image"
title: "containergreifer04.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7219
- /details5f69.html
imported:
- "2019"
_4images_image_id: "7219"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7219 -->
