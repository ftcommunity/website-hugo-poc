---
layout: "comment"
hidden: true
title: "1487"
date: "2006-10-26T12:14:08"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Geniale Mechanik zum Zentrieren und Verriegeln!

Wie willst du die Motoren steuern? Per Kabel kann sich das ja immer so leicht mit dem Kranseil verheddern und per FT-IR ist zu gefährlich wenn der Container in 3m Höhe ausgeklinkt wird :)

Klasse,
Thomas