---
layout: "image"
title: "containergreifer09.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7224
- /detailsefb6.html
imported:
- "2019"
_4images_image_id: "7224"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7224 -->
