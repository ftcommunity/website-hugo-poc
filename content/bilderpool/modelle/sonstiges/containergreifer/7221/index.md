---
layout: "image"
title: "containergreifer06.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7221
- /details5902.html
imported:
- "2019"
_4images_image_id: "7221"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7221 -->
