---
layout: "image"
title: "Der Trockenbalken"
date: "2014-04-27T16:09:00"
picture: "waschstrasse16.jpg"
weight: "16"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38698
- /detailsda34-3.html
imported:
- "2019"
_4images_image_id: "38698"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38698 -->
...wird von einem ENC-Motor und einer Lichtschranke gesteuert.