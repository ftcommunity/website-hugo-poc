---
layout: "image"
title: "Das Förderband"
date: "2014-04-27T16:09:00"
picture: "waschstrasse15.jpg"
weight: "15"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38697
- /detailsce65.html
imported:
- "2019"
_4images_image_id: "38697"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38697 -->
Durch das Fehlen der Gummiglieder wird das Auto gezogen.