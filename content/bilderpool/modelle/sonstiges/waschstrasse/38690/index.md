---
layout: "image"
title: "Bürstenmotor"
date: "2014-04-27T16:09:00"
picture: "waschstrasse08.jpg"
weight: "8"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38690
- /details77e3-2.html
imported:
- "2019"
_4images_image_id: "38690"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38690 -->
Leider unscharf... Der linke Mot dreht die Bürste.