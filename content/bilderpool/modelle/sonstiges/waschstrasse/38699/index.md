---
layout: "image"
title: "Trockenbalken"
date: "2014-04-27T16:09:00"
picture: "waschstrasse17.jpg"
weight: "17"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38699
- /details4b3c-3.html
imported:
- "2019"
_4images_image_id: "38699"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38699 -->
