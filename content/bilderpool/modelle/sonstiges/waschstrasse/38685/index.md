---
layout: "image"
title: "Autoperspekitve"
date: "2014-04-27T16:09:00"
picture: "waschstrasse03.jpg"
weight: "3"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38685
- /detailsa014.html
imported:
- "2019"
_4images_image_id: "38685"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38685 -->
Blick auf die Bürsten.