---
layout: "image"
title: "Tuchtrocknen"
date: "2014-04-27T16:09:00"
picture: "waschstrasse13.jpg"
weight: "13"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38695
- /details3cc0.html
imported:
- "2019"
_4images_image_id: "38695"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38695 -->
Hier wird das Auto "getrocknet"