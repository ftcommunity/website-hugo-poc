---
layout: "image"
title: "Kasse"
date: "2014-04-27T16:09:00"
picture: "waschstrasse05.jpg"
weight: "5"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38687
- /detailsc1f8.html
imported:
- "2019"
_4images_image_id: "38687"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38687 -->
Die Lampe zeigt an, wann das nächste Auto auf das Förderband fahren muss. Mit dem linken Taster schaltet man die Unterbodenwäsche zu, 
mit dem Rechten den Trockenbalken.