---
layout: "image"
title: "offene Tür"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13884
- /detailsf7a4.html
imported:
- "2019"
_4images_image_id: "13884"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13884 -->
Hier ist die Tür des Korbes geöffnet.