---
layout: "image"
title: "Bewässerungsanlage mit Pflanzen"
date: "2011-05-21T12:12:17"
picture: "anlage1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30589
- /detailsfa28.html
imported:
- "2019"
_4images_image_id: "30589"
_4images_cat_id: "2280"
_4images_user_id: "1162"
_4images_image_date: "2011-05-21T12:12:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30589 -->
Hier nun der Beweis, die Bewässerungsanlage mit Pflanzen. Kresse und Buschbohnen werden hier jeden Tag von der Anlage zweimal gegossen. Die Buschbohnen sieht man zwar noch nicht aber zumindest die Kresse, die schon erntereif ist.
