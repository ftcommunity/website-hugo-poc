---
layout: "comment"
hidden: true
title: "12033"
date: "2010-08-26T22:19:32"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich find's gut, weil eine funktionierende Wasserpumpe zu bauen schon ein schönes Projekt ist. Man kann es ja auch noch nach Belieben z. B. um eine Trockenheitserkennung ergänzen.

Gruß,
Stefan