---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage9.jpg"
weight: "9"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27955
- /details2e11.html
imported:
- "2019"
_4images_image_id: "27955"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27955 -->
Hier könnt ihr die "eingesperrte" Flasche sehen, die in den "Käfig" ziemlich gut steht und nicht umfällt.

Mann/Frau sieht auch den Schlauch der in die Flasche ragt und das Wasser hochbringt, das die Luft verträngt.
