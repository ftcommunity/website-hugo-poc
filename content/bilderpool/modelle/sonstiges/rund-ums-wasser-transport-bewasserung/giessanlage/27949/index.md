---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27949
- /details344c.html
imported:
- "2019"
_4images_image_id: "27949"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27949 -->
Hier sieht man den Kompressor. Der Excenter ist von TST (alias Andreas Tacke) genauso wie die angepasste Bauplatte. Ich muss sagen, dass der Excenter echt klasse ist, und das das Wasser ganz fix herauskommt. Vielen Dank an Andreas, hier an dieser Stelle. 

Der Kompressor wird duch den Taster ein-/ bzw. ausgeschalten.
