---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage4.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27950
- /details4995.html
imported:
- "2019"
_4images_image_id: "27950"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27950 -->
Hier sieht man die Gießanlage von oben. Man sieht die kompakte Bauweise. und die Schläuche, die man dann an die gewünschte Pflanze hinhalten kann.
