---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage6.jpg"
weight: "6"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27952
- /details2541.html
imported:
- "2019"
_4images_image_id: "27952"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27952 -->
Hier seht ihr den Taster zum ein-/ausschalten und den Kompressor von der Seite
