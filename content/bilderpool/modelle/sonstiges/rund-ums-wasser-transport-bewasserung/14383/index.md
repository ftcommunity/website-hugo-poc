---
layout: "image"
title: "Wasserpumpe"
date: "2008-04-24T23:06:05"
picture: "PICT3949.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14383
- /details3287.html
imported:
- "2019"
_4images_image_id: "14383"
_4images_cat_id: "2155"
_4images_user_id: "729"
_4images_image_date: "2008-04-24T23:06:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14383 -->
Wasserpumpe von unten.