---
layout: "image"
title: "Gießmaschine"
date: "2015-07-01T18:05:08"
picture: "091_2.jpg"
weight: "2"
konstrukteure: 
- "Bauteil"
fotografen:
- "Bauteil"
uploadBy: "Bauteil"
license: "unknown"
legacy_id:
- /php/details/41369
- /detailsa64b.html
imported:
- "2019"
_4images_image_id: "41369"
_4images_cat_id: "3091"
_4images_user_id: "2452"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41369 -->
Gießmaschine