---
layout: "image"
title: "Bewässerungsanlage- Schalter"
date: "2011-01-01T17:41:04"
picture: "ssfsf4.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29585
- /detailsdb1b.html
imported:
- "2019"
_4images_image_id: "29585"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29585 -->
Hier sind die Schalter zu sehen. Es ist bis jetzt nur ein Schalter programmiert und zwar der Linke. Mit ihm kann man die Anlage einschalten.
