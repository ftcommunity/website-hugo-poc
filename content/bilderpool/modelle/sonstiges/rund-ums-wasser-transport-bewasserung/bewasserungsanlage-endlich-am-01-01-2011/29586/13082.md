---
layout: "comment"
hidden: true
title: "13082"
date: "2011-01-02T08:17:15"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Ja, die Schläuche sind etwas verbogen, aber später werden die an einen Stecken, der im Blumentopf steckt, mit Kabelbindern befestigt, dann sehen die nicht mehr so verbogen aus.

MfG
Endlich