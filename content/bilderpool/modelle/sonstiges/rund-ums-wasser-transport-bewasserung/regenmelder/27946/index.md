---
layout: "image"
title: "Regenmelder"
date: "2010-08-25T17:55:06"
picture: "regenmelder6.jpg"
weight: "6"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27946
- /details1c86.html
imported:
- "2019"
_4images_image_id: "27946"
_4images_cat_id: "2023"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27946 -->
Hier könnt ihr sehen, das die "Wanne" mit den  Kontakten direkt unterhalb vom Dachfenster befestig ist. Und dass das Kabel direkt ins Haus hineinführen.

Der Regenmelder hängt bestimmt schon 1 1/2 Monate draußen.

Ich hoffe euch hats gefallen.
