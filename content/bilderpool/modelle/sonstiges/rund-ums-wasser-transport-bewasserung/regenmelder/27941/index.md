---
layout: "image"
title: "Regenmelder"
date: "2010-08-25T17:55:05"
picture: "regenmelder1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27941
- /details7588.html
imported:
- "2019"
_4images_image_id: "27941"
_4images_cat_id: "2023"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27941 -->
Hier bei diesen Bildern, seht ihr meinen Regenmelder. Auf die Idee kam ich als es immer in mein Dachfenster hinein regnete, und ich immer vergass das Fenster zu schließen :) . 
Der Regenmelder funktioniert so: Es hängen zwei Kontakte außen auf dem Dach, und wenn es regnet leiten das Wasser den Strom zu den Kontakten. 

Hier bei diesem ersten Bild sehr ihr das Hauptmodul vom Melder. den Batteriehalter mit Schalter, und das Soundmodul. Wenn es regnet, dann bekommt das Soundmodul den "Hinweis" das es warnen muss das es regnet. Auf dem Soundmodul ist ein selbst aufgenommener Text. Das Hauptmodul wird durch einen Magnetbaustein von fischertechnik auf meiner Magnettafel gehalten. 

ich hoffe euch gefällt der Melder, und ihr schreibt fleißig Kommi´s :)

MfG
Endlich

