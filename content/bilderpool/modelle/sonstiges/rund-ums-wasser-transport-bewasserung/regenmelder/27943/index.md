---
layout: "image"
title: "Regenmelder"
date: "2010-08-25T17:55:06"
picture: "regenmelder3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27943
- /detailsd217.html
imported:
- "2019"
_4images_image_id: "27943"
_4images_cat_id: "2023"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27943 -->
Hier sieht man den Regenmelder von hinten. Man sieht auch den Magnetbaustein von FT. Ich finde persönlich bei diesem Modell auch die wenigen Bauteile.
