---
layout: "image"
title: "Wasserpumpe"
date: "2008-04-24T23:06:05"
picture: "PICT3948.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14382
- /detailsc26d.html
imported:
- "2019"
_4images_image_id: "14382"
_4images_cat_id: "2155"
_4images_user_id: "729"
_4images_image_date: "2008-04-24T23:06:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14382 -->
Wasserpumpe von hinten.