---
layout: "image"
title: "Bewässerungsanlage V2 - Schläuche"
date: "2011-02-15T21:14:59"
picture: "bwv11.jpg"
weight: "11"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30001
- /detailse780.html
imported:
- "2019"
_4images_image_id: "30001"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:59"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30001 -->
Hier kann man sehr gut die Schläuche für das Wasser sehen, sowie auch den PowerMotor 20:1 der für den Antrieb der Schnecke zuständig ist.
