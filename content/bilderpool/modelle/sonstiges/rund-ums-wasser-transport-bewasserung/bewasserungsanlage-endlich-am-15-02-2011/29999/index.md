---
layout: "image"
title: "Bewässerungsanlage V2 - Motor"
date: "2011-02-15T21:14:59"
picture: "bwv09.jpg"
weight: "9"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29999
- /detailscc79.html
imported:
- "2019"
_4images_image_id: "29999"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29999 -->
Hier kann man den Motor für das Ventil sehen. Man kann erkennen, das es nur noch wenige Zentimeter Luft zum Interface gibt.
