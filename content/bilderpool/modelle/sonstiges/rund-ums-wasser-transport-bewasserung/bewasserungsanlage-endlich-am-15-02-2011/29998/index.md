---
layout: "image"
title: "Bewässerungsanlage V2 - Ventilsteuerung"
date: "2011-02-15T21:14:59"
picture: "bwv08.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29998
- /details675b.html
imported:
- "2019"
_4images_image_id: "29998"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29998 -->
Das hier ist die Ventilsteuerung. Die Idee hab ich von Frank übernommen, da ich sie einfach nur toll fand. Sie funktioniert auch übrigends super und der Motor tut sich nicht mehr schwer.
