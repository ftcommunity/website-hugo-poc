---
layout: "image"
title: "Glückwunschdisplay"
date: "2011-04-03T14:44:52"
picture: "glueckwunschdisplay1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30424
- /details7eed-2.html
imported:
- "2019"
_4images_image_id: "30424"
_4images_cat_id: "2261"
_4images_user_id: "968"
_4images_image_date: "2011-04-03T14:44:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30424 -->
Gestern morgen hatte ich dann endlich die fehlende Idee fürs 50ste Geburtstagsfoto für meine weit weg lebende Schwester.
Schnell die Buchstaben gebaut und auf eine Platte montiert.