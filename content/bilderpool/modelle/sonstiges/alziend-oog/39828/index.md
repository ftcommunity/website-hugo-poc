---
layout: "image"
title: "Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar."
date: "2014-11-16T15:34:20"
picture: "alziendoog03.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39828
- /details318b-2.html
imported:
- "2019"
_4images_image_id: "39828"
_4images_cat_id: "2988"
_4images_user_id: "22"
_4images_image_date: "2014-11-16T15:34:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39828 -->
Beneden-positie ver weg  kijkend..........
