---
layout: "image"
title: "Technikgeschichte mit Alientechnologie"
date: "2015-11-29T16:51:41"
picture: "technikgeschichtemitalientechnologie1.jpg"
weight: "1"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42475
- /details52fa.html
imported:
- "2019"
_4images_image_id: "42475"
_4images_cat_id: "3157"
_4images_user_id: "1677"
_4images_image_date: "2015-11-29T16:51:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42475 -->

Nachdem ich zu St. Martin von ungewöhnlichen Sichtungen hörte, wollte ich es unbeding mit eigenen Augen sehen.
So buchte ich über ftAirlines einen 48h Nonstop-Langstreckenflug von Düsseldorf nach Münchhausen. Schon der Flug ließ micht staunen,
denn das Flugzeug wurde nicht ein einziges mal aufgetankt. Dazu später mehr.

In Münchhausen wollte ich sofort zu dem Platz, wo die wahrscheinlichkeit am größten war eine Sichtung zu erleben.
Ein gewisser Baron den ich zufällig traf führte mich zum Ort des Geschehens. Es Dauerte eine ganz "lange Weile".
In der Zwischenzeit erzählte der Baron mir seine Lebensgeschicht 
(die auch auf YouTube zu hören ist[https://www.youtube.com/watch?v=ef5JD5dnNvY])bis wir etwas entdecken.-
----> https://youtu.be/Zz3X65whT4I <----

Zu erst war ich überrascht & fassungslos aber je länger ich darüber nachdachte manifestierten sich zwei Thesen:

1. Artur Fischer ist ein Alien in Menschengestalt!  
	"Artur Fischer hatte bis Ende 2013 insgesamt 1136 Patente und Gebrauchsmuster angemeldet und gilt als einer der erfolgreichsten Erfinder weltweit."
	(Quelle: https://de.wikipedia.org/wiki/Artur_Fischer)
	Wie kann ein Mensch SO VIELE Erfindungen machen??? Haben all diese Erfindungen, nicht unser aller Leben positiv beeinflusst??? 

2. Artur Fischer hat eins dieser UFO´s (siehe Bild & Video) gefunden und im geheimen die Alientechnologie ausgeschlachtet!
Als bestes Beispiel dient mir ein angebliches Kinderspielzeug, diese Fischertechnik Baukästen. Wundert ihr euch nicht auch, wie Vielfälltig sich die merkwürdigen Bausteine verwenden lassen??? Und sogar Erwachsene spielen damit, unfassbar!!!
Und jetzt auch noch das Foto von H.a.r.r.y - die Fischerwerke gehen in den Flugzeugbau? (als wenn der Absatzmarkt mit Airbus, Boing und Andere nicht schon gesättigt wäre) Warum trauen sie sich in diesen heiß umkämpften Markt - Doch nur wenn sie durch Alientechnologie einen klaren Vorteil haben. Ein 48 Stunden Nonstop-Langstreckenflug rund um die Erde ohne Nachtanken??? Alientechnologie !!!

Jetzt bin ich mal gespannt ob ihr weitere Thesen habt und ob ich mit meinen Vermutungen alleine da stehe.

Willkommen zu den VerschwörungsTherorien rund um ft :)

DANKE FÜR DEN IMPULS, HARRY ! http://www.ftcommunity.de/details.php?image_id=42173

Und Sorry Dirk & Thomas, wenn ich eueren Buchtitel für meine Geschichte abgewandelt habe. :)

