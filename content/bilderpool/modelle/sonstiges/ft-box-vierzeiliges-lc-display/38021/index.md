---
layout: "image"
title: "LCD-Box (leer, von vorne)"
date: "2014-01-06T08:31:03"
picture: "ftboxfuervierzeiligeslcdisplay4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38021
- /details0edc.html
imported:
- "2019"
_4images_image_id: "38021"
_4images_cat_id: "2828"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38021 -->
Front aus transparenten Flachsteinen 30 x 30 (31556).