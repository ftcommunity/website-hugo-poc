---
layout: "image"
title: "LCD-Box (leer, von oben)"
date: "2014-01-06T08:31:03"
picture: "ftboxfuervierzeiligeslcdisplay3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38020
- /detailsc243-2.html
imported:
- "2019"
_4images_image_id: "38020"
_4images_cat_id: "2828"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38020 -->
Ansicht von oben. Das Display wird zwischen Winkelsteinen 60° und BS 5 festgeklemmt.