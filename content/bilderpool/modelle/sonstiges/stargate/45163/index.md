---
layout: "image"
title: "Gate"
date: "2017-02-11T21:15:13"
picture: "stargate21.jpg"
weight: "21"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45163
- /detailsac18.html
imported:
- "2019"
_4images_image_id: "45163"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45163 -->
Es ist im Boden eingebaut und hat 5 Chevrons(Die Pfeile), mit welchen die Adresse bestimmt wird. In der Mitte befindet sich eine runde Plexiglasscheibe, welch durch LEDs beleuchtet wird. 
Zum Anwählen gibt es 4 Taster mit welchen eine 5 stellige Adresse eingegeben werden muss um das entsprechende Gate zu aktivieren.
Die Chevrons leuchten nach erfolgter Eingabe in der Farbe des Zielgates auf und das Gate leuchtet dann blau.
Wählt man eine nicht vorhandene- oder erreichbare Adresse leuchten die Chevrons kurz rot auf und man muss neu wählen.
Jedes Gate hat seine individuelle Adresse.