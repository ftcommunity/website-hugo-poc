---
layout: "image"
title: "Aufenthaltsraum"
date: "2017-02-11T21:15:13"
picture: "stargate32.jpg"
weight: "32"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45174
- /details0bc5.html
imported:
- "2019"
_4images_image_id: "45174"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45174 -->
