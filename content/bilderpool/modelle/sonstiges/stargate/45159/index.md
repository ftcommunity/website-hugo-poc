---
layout: "image"
title: "Die Elektronik"
date: "2017-02-11T21:15:13"
picture: "stargate17.jpg"
weight: "17"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45159
- /detailsfe3b.html
imported:
- "2019"
_4images_image_id: "45159"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45159 -->
Eigenentwicklung doppelseitige Platine mit :

Atmel XMEGA 256
WiFi Modul ESP8266
2 Motor Controller TB6612 für 4 Motoren
8 analog Eingänge
8 Schalt Ausgänge
16 Ein- / Ausgänge

Die Gates kommunizieren über WiFi miteinander.