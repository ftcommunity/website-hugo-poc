---
layout: "image"
title: "Obere Tür"
date: "2017-02-11T21:15:13"
picture: "stargate10.jpg"
weight: "10"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45152
- /details6ace.html
imported:
- "2019"
_4images_image_id: "45152"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45152 -->
Unten in der Türe ist ein Magnet verbaut welcher den Reedkontakt in der Türschwelle betätigt, dadurch wird die Stellung der Tür erfasst.