---
layout: "image"
title: "Steuerpult"
date: "2017-02-11T21:15:12"
picture: "stargate04.jpg"
weight: "4"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45146
- /detailsd0ff-2.html
imported:
- "2019"
_4images_image_id: "45146"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45146 -->
Das Pult dient zum öffnen, schließen und verriegeln der Türen, sowie zur Kontrolle des Alarms