---
layout: "image"
title: "Gate Kontrollpult"
date: "2017-02-11T21:15:12"
picture: "stargate06.jpg"
weight: "6"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45148
- /details77c5.html
imported:
- "2019"
_4images_image_id: "45148"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45148 -->
Die grüne LED ist die Statusanzeige des Schilds, 
die rote Led blinkt sobald man den Alarm aktiviert.