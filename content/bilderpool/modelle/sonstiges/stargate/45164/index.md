---
layout: "image"
title: "Gate"
date: "2017-02-11T21:15:13"
picture: "stargate22.jpg"
weight: "22"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45164
- /detailsf660-2.html
imported:
- "2019"
_4images_image_id: "45164"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45164 -->
In aktiviertem Zustand.
Die Chevrons leuchten gerade Rosa und das Gate ist bereit um durchquert zu werden.