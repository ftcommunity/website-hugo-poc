---
layout: "image"
title: "Krankenstation"
date: "2017-02-11T21:15:13"
picture: "stargate40.jpg"
weight: "40"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45182
- /details3a95-2.html
imported:
- "2019"
_4images_image_id: "45182"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45182 -->
