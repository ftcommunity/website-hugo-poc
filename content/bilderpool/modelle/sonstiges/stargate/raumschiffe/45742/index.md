---
layout: "image"
title: "Innenansicht"
date: "2017-04-14T22:49:00"
picture: "raumschiff02.jpg"
weight: "2"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45742
- /details84dd.html
imported:
- "2019"
_4images_image_id: "45742"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45742 -->
Zwei Sitzplätze für Piloten und etwas Stauraum