---
layout: "image"
title: "Innenraum"
date: "2017-04-14T22:49:00"
picture: "raumschiff11.jpg"
weight: "11"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45751
- /details2712.html
imported:
- "2019"
_4images_image_id: "45751"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45751 -->
Der hintere Teil ist der Frachtraum und vorne die Piloten