---
layout: "image"
title: "Das kleine Schattel"
date: "2017-04-14T22:49:00"
picture: "raumschiff01.jpg"
weight: "1"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45741
- /details8837-2.html
imported:
- "2019"
_4images_image_id: "45741"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45741 -->
Kleines Transport Raumschiff