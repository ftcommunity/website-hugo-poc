---
layout: "image"
title: "Die Elektronik"
date: "2017-02-11T21:15:13"
picture: "stargate47.jpg"
weight: "47"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45189
- /detailsc38c.html
imported:
- "2019"
_4images_image_id: "45189"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45189 -->
Hier ist die gleiche Elektronik verbaut wie im Gateraum.
Eigenentwicklung doppelseitige Platine mit :

Atmel XMEGA 256
WiFi Modul ESP8266
2 Motor Controller TB6612 für 4 Motoren
8 analog Eingänge
8 Schalt Ausgänge
16 Ein- / Ausgänge

Die Gates kommunizieren über WiFi miteinander.