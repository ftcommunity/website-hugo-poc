---
layout: "image"
title: "Nasenbohrer3"
date: "2007-04-30T18:47:23"
picture: "Nasenbohrerm3.jpg"
weight: "3"
konstrukteure: 
- "Ft"
fotografen:
- "Kurt"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- /php/details/10238
- /details8f24.html
imported:
- "2019"
_4images_image_id: "10238"
_4images_cat_id: "927"
_4images_user_id: "71"
_4images_image_date: "2007-04-30T18:47:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10238 -->
