---
layout: "image"
title: "Nasenbohrer"
date: "2007-04-30T09:00:18"
picture: "Nasenbohrer1..jpg"
weight: "1"
konstrukteure: 
- "FT"
fotografen:
- "Kurt"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- /php/details/10231
- /detailsf303.html
imported:
- "2019"
_4images_image_id: "10231"
_4images_cat_id: "927"
_4images_user_id: "71"
_4images_image_date: "2007-04-30T09:00:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10231 -->
war wohl mal als Trostplaster gedacht