---
layout: "image"
title: "Nasenbohrer2"
date: "2007-04-30T09:00:18"
picture: "Nasenbohrer2.jpg"
weight: "2"
konstrukteure: 
- "FT"
fotografen:
- "Kurt"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- /php/details/10232
- /details879a.html
imported:
- "2019"
_4images_image_id: "10232"
_4images_cat_id: "927"
_4images_user_id: "71"
_4images_image_date: "2007-04-30T09:00:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10232 -->
