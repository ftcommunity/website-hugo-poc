---
layout: "image"
title: "Advent, Licht zum Weihnachten"
date: "2015-11-29T16:51:42"
picture: "Advent_licht.jpg"
weight: "22"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- /php/details/42477
- /details6681.html
imported:
- "2019"
_4images_image_id: "42477"
_4images_cat_id: "323"
_4images_user_id: "1295"
_4images_image_date: "2015-11-29T16:51:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42477 -->
1ste Advent, 4 Wochen vor Weihnachten. In die Box 500 4 E-Tec modulen. Programmierung: 4 D-flipflops mit taktsteuerung. 

Spezialprogramma 2.3: inhttp://www.fischertechnik.de/PortalData/1/Resources/downloads/documents/etecdigitaltechnik.pdf