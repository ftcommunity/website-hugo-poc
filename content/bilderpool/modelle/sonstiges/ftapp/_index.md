---
layout: "overview"
title: "FtApp"
date: 2020-02-22T08:32:10+01:00
legacy_id:
- /php/categories/3225
- /categoriesf6c9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3225 --> 
Ich habe mich in den Ferien mal hingesetzt und eine kleine Android App geschrieben. Es ist nur die erste (beta-) Version aber ich wollte sie euch nicht vorenthalten :D. Wenn ihr daran interessiert seid, könnt ihr diese Version unter diesem Link herunterladen: https://github.com/Bennik2000/FtApp/blob/master/de.bennik2000.ftapp.apk?raw=true

Falls sich jemand den Source Code und das TCP/IP Protokoll anschauen will, hier ist auch dieser Link: https://github.com/Bennik2000/FtApp