---
layout: "image"
title: "(2) von vorne"
date: "2009-03-23T07:37:09"
picture: "2_von_vorne.jpg"
weight: "2"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/23485
- /detailsa025-2.html
imported:
- "2019"
_4images_image_id: "23485"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23485 -->
