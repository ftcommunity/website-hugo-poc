---
layout: "image"
title: "(4) von hinten, links"
date: "2009-03-23T07:37:10"
picture: "4_von_hinten_links.jpg"
weight: "4"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/23487
- /detailse23e-2.html
imported:
- "2019"
_4images_image_id: "23487"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23487 -->
