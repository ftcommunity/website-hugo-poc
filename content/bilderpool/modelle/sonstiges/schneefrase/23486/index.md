---
layout: "image"
title: "(3) von links"
date: "2009-03-23T07:37:09"
picture: "3_von_links.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/23486
- /detailsffdb.html
imported:
- "2019"
_4images_image_id: "23486"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23486 -->
Antrieb durch Powermotor 8:1
