---
layout: "image"
title: "(8) Fräsrad ausgebaut"
date: "2009-03-23T07:37:10"
picture: "8_Frsrad_ausgebaut.jpg"
weight: "8"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/23491
- /details1c8e.html
imported:
- "2019"
_4images_image_id: "23491"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23491 -->
Das Fräsrad im ausgebauten Zustand
