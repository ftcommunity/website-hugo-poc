---
layout: "image"
title: "Radar +  4-polige-Jackplug-3,5mm (statt Schleifringen)"
date: "2008-01-11T21:09:58"
picture: "Radar-4-polige-Jackplug-35mm_003.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13304
- /details326e.html
imported:
- "2019"
_4images_image_id: "13304"
_4images_cat_id: "1230"
_4images_user_id: "22"
_4images_image_date: "2008-01-11T21:09:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13304 -->
Radar +  4-polige-Jackplug-3,5mm (statt Schleifringen)
