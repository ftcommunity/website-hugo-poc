---
layout: "image"
title: "Radar_001"
date: "2003-10-14T11:30:49"
picture: "Radar001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Radar"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1830
- /detailsc968.html
imported:
- "2019"
_4images_image_id: "1830"
_4images_cat_id: "198"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:30:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1830 -->
Ein frühes Werk (ca.1972)