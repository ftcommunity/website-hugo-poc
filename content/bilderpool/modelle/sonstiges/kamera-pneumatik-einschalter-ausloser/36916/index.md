---
layout: "image"
title: "Pneumatische Kamerasteuerung - vorne"
date: "2013-05-14T21:23:13"
picture: "kameramitpneumatikeinschalterausloeser1.jpg"
weight: "1"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36916
- /detailsae95.html
imported:
- "2019"
_4images_image_id: "36916"
_4images_cat_id: "2743"
_4images_user_id: "1677"
_4images_image_date: "2013-05-14T21:23:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36916 -->
Die Kamera wird über einen Pneumatikzylinder eingeschaltet und über einen anderen Zylinder wir der Auslöser betätigt.
Für ein Fischertechnik-Gewächshaus wollte ich periodische Fotoaufnahmen tätigen (Zeitraffer). Anfangs machte ich mir Gedanken über einen SD-Kartenadapter auf USB.
Damit wollte ich die Fotos von der Kamera direkt auf dem PC Speichern. Um das ganze mit unterbrechungsfreien Stromversorgung auszustatten hatte ich schon ein Akku-Gehäuse umfunktioniert.
Allerding war der Geistesblitz noch viel einfacher zu realisieren.
Das Gewächshaus wird per RoboTX Controller gesteuert. Da liegt es doch nah, auch die Steuerung der Kamera  per TX zu erledigen. Der Akku hält lange, lange, lange genung durch, da nach jedem Foto die Kamera wieder per pneumatik ausgeschaltet wird.

