---
layout: "image"
title: "Die Vogelspinne"
date: "2013-03-07T18:13:18"
picture: "P1200678.jpg"
weight: "2"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/36730
- /detailsaff5.html
imported:
- "2019"
_4images_image_id: "36730"
_4images_cat_id: "2725"
_4images_user_id: "1635"
_4images_image_date: "2013-03-07T18:13:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36730 -->
Gesamtansicht der Vogelspinne