---
layout: "image"
title: "fischertechnik-Spin met Encodermotor"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45762
- /detailsb09f.html
imported:
- "2019"
_4images_image_id: "45762"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45762 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017
