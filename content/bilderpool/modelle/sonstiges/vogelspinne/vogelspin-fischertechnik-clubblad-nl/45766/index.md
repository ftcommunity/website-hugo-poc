---
layout: "image"
title: "fischertechnik-Spin met Powermotor 50-1"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl10.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45766
- /details2917.html
imported:
- "2019"
_4images_image_id: "45766"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45766 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017
