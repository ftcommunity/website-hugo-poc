---
layout: "image"
title: "FT-Vogelspin + Powermotor 50-1"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45773
- /detailsd231-2.html
imported:
- "2019"
_4images_image_id: "45773"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45773 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017

FT Vogelspin met Powermotor 50 : 1 
https://www.youtube.com/watch?v=GxA4DuOwgYo
