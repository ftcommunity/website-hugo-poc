---
layout: "image"
title: "FT-Vogelspinnen -Encodermotor + Powermotor (R)"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl18.jpg"
weight: "18"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/45774
- /detailscf16.html
imported:
- "2019"
_4images_image_id: "45774"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45774 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017  (NL + De ):
http://www.fischertechnikclub.nl/index.php?option=com_content&view=article&id=697:clubblad-april-2017&catid=31&Itemid=101

FT Vogelspin met Powermotor 50 : 1 
https://www.youtube.com/watch?v=GxA4DuOwgYo
