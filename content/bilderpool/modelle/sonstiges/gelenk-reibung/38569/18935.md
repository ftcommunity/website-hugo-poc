---
layout: "comment"
hidden: true
title: "18935"
date: "2014-04-18T13:47:45"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
da hast du wieder mal erfolgreich zugeschlagen :o) Die Reibung ist hier nicht von den Toleranzen der Nennmassgeometrie oder dem Schrumpfverzug zufällig abhängig, sondern von der radial aussermittigen Lage des damit vorgespannten Federnockens 31982 im Pneumatik-Achsadapter 31422
Gruss, Udo2