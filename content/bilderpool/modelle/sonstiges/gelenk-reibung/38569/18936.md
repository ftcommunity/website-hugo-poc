---
layout: "comment"
hidden: true
title: "18936"
date: "2014-04-18T13:57:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo,

na, ich glaube, das reibt auch einfach deshalb, weil der Federnocken ja dafür entworfen ist, in einer "4-mm-Nut" zu klemmen. Und so klemmt er eben auch in einem 4-mm-Loch.

Gruß,
Stefan