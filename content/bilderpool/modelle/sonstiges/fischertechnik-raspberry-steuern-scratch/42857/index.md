---
layout: "image"
title: "Scratch Programm"
date: "2016-02-02T13:11:13"
picture: "fischertechnik_parallel_IF_mit_Scratch_bearbeitet3.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/42857
- /detailsea9b.html
imported:
- "2019"
_4images_image_id: "42857"
_4images_cat_id: "3187"
_4images_user_id: "34"
_4images_image_date: "2016-02-02T13:11:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42857 -->
Hintergrund-Objekt zum ansteuern der GPIOs eines Raspberry B+ mit dem aktuellem Sratch.
Das Programm sendet die Befehle des selbst geschriebenen Scratch-Programms an ein 
parallel fischertechnik-Interface. 
Es ist damit auch möglich mehrere Programme parallel laufen zu lassen. Dieses Grundprogramm kann leicht abgeändert werden um auch mehrere (- viele :-)  ) ft-parallel Interfaces anzusteuern.

Grenzen: 
Das Programm nutzt eine feste Zeit als Verzögerung. Dadurch und weil Scratch selbst die eigenen Programme erst in eine vom Computer lesbare Form umwandeln muss - die auch Zeit braucht, Werden z.B. Endschalter weiter überfahren als wie man es von RoboPro gewohnt ist.

Achtung! Der Raspberry arbeitet nur mit 3,3 V an den Ausgängen! 
Es muss eine Trennung zu den "5V"  des ft-Interfaces erfolgen. Ich habe hierzu ein Gertboard benutzt. Man kann sich die Schaltung auch selber nachbauen (ca. 5¤).
Wenn man das Gertboard und einen alten grauen ft-Trafo benutzt und man ein (!) ft-parallel Interface benutzt darf man den Trafo nicht ganz aufdrehen da das Interface sonst nicht mehr die Daten vom Gertbord erkennt (die ICs auf dem Gertboard sind vom Typ HC und erkennen die TTL-Pegel nicht). Mit zwei und mehr ft-Interfaces kann man den Trafo ganz aufdrehen.

Weite Info, siehe im Forum :
http://forum.ftcommunity.de/viewtopic.php?f=8&t=3299
