---
layout: "image"
title: "FT Entscheidungsfinder"
date: "2018-05-12T10:30:23"
picture: "Fischertechnik_Entscheidungsfinder_-_3_of_7.jpg"
weight: "28"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47643
- /details9212.html
imported:
- "2019"
_4images_image_id: "47643"
_4images_cat_id: "323"
_4images_user_id: "2739"
_4images_image_date: "2018-05-12T10:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47643 -->
