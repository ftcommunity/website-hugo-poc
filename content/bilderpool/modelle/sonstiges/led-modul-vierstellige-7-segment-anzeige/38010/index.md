---
layout: "image"
title: "LED-Modul: Frontseite (von innen)"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38010
- /detailsed54-2.html
imported:
- "2019"
_4images_image_id: "38010"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38010 -->
Die Frontseite mit zwei transparenten Flachsteinen 30 x 30 (31556) von innen ...