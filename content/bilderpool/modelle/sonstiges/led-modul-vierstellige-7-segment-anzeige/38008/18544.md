---
layout: "comment"
hidden: true
title: "18544"
date: "2014-01-06T20:12:29"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Ixi,

pardon, ja: die Conrad-Bestellnummer lautet 198344 (http://www.conrad.de/ce/de/product/198344/C-Control-IC-LED-Display-Modul). Näheres zur Nutzung/Programmierung in ft:pedia 4/2012 (http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-4.pdf).

Beste Grüße,
Dirk