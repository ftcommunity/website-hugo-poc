---
layout: "image"
title: "LED-Modul: Montage auf Grundplatte"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38008
- /details2a4a.html
imported:
- "2019"
_4images_image_id: "38008"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38008 -->
Mit kleinen Winkeln und passenden Schrauben lässt sich das LED-Modul von Conrad mittig auf einer Grundplatte 90 x 45 (36576) befestigen.