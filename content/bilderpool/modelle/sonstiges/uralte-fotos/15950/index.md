---
layout: "image"
title: "Roboter 1"
date: "2008-10-11T22:48:04"
picture: "OLD08.jpg"
weight: "8"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15950
- /detailsd397.html
imported:
- "2019"
_4images_image_id: "15950"
_4images_cat_id: "1448"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15950 -->
Alter ca. 30 Jahre