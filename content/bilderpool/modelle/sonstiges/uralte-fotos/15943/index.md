---
layout: "image"
title: "Panzer"
date: "2008-10-11T22:48:04"
picture: "OLD01.jpg"
weight: "1"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
schlagworte: ["Panzer"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15943
- /detailsa340.html
imported:
- "2019"
_4images_image_id: "15943"
_4images_cat_id: "1448"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15943 -->
Panzer
Alter ca. 30 Jahre