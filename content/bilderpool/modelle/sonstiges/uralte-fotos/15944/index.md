---
layout: "image"
title: "Fahrsimulator"
date: "2008-10-11T22:48:04"
picture: "OLD02.jpg"
weight: "2"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
schlagworte: ["Fahrsimulator"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15944
- /details8faf-2.html
imported:
- "2019"
_4images_image_id: "15944"
_4images_cat_id: "1448"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15944 -->
Alter ca. 30 Jahre