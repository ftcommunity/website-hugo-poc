---
layout: "comment"
hidden: true
title: "7554"
date: "2008-10-12T15:19:12"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Die Taste sind mit einem Mini-Motor verbunden, der die Vorderachse dreht.
Da es kein Servo ist, hat man gut zu tun, das Auto auf der Straße zu halten.