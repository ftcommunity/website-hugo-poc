---
layout: "image"
title: "Geschwindigkeitsmessanlage- erste Lichtschranke"
date: "2011-01-01T17:32:48"
picture: "sdfsfs8.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29581
- /details5c69-2.html
imported:
- "2019"
_4images_image_id: "29581"
_4images_cat_id: "2154"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:32:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29581 -->
Die erste Lichtschranke im Detail. Beide Lichtschranken sind gleich aufgebaut. 

Ein Video von der Anlage gibt es hier: http://www.youtube.com/watch?v=hTprAwfvemI
