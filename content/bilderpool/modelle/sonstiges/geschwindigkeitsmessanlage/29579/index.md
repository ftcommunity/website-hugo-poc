---
layout: "image"
title: "Geschwindigkeitsmessanlage- erste Lichtschranke"
date: "2011-01-01T17:32:48"
picture: "sdfsfs6.jpg"
weight: "6"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29579
- /detailsefb5-2.html
imported:
- "2019"
_4images_image_id: "29579"
_4images_cat_id: "2154"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:32:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29579 -->
Hier sieht man die zweite Lichtschranke, durch die der Zug als erstes fährt.
