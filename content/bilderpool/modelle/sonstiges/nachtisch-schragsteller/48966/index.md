---
layout: "image"
title: "Das Resultat"
date: 2021-01-05T16:25:05+01:00
picture: "NachtischSchraegsteller3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Letztlich hat das Prozedere eben den hier gezeigten Effekt. Das Rezept sagte, das wäre ganz arg doll. Geschmeckt hat's jedenfalls auch in Schräglage.
