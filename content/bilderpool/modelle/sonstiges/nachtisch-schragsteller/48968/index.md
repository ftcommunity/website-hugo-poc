---
layout: "image"
title: "Die Anforderung"
date: 2021-01-05T16:25:07+01:00
picture: "NachtischSchraegsteller1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Nachtisch-Rezept lautet grob so: Himbeermasse schräg in ein Glas füllen, das Glas schräg eine Nacht im Kühlschrank halten und vor dem Servieren auf die so fest gewordene Masse die Sahne geben.