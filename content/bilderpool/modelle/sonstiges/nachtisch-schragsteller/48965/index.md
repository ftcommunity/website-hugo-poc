---
layout: "image"
title: "Der Aufbau"
date: 2021-01-05T16:25:04+01:00
picture: "NachtischSchraegsteller4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell ist hochkomplex, wurde aber immerhin für den realen Einsatz gebaut.