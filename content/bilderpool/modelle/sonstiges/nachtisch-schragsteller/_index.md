---
layout: "overview"
title: "Nachtisch-Schrägsteller"
date: 2021-01-05T16:25:04+01:00
---

Für ein bestimmtes Rezept für einen Nachtisch war gefordert, Gläser schräg in den Kühlschrank zu stellen. Wie gut, dass die beste aller Ehefrauen einen fischertechniker ihr Eigen nennt.