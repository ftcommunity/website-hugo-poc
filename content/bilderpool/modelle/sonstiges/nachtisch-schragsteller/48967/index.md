---
layout: "image"
title: "Schräggestellte Gläser"
date: 2021-01-05T16:25:06+01:00
picture: "NachtischSchraegsteller2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Kühlschrankkälte macht den fischertechnik-Teilen wohl nichts aus. Als flugs ein paar davon zusammengesteckt und die Küchenchefin glücklich gemacht.