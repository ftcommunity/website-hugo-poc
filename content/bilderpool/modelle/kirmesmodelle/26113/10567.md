---
layout: "comment"
hidden: true
title: "10567"
date: "2010-01-19T22:45:41"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
Hallo Fredy,
nicht schlecht, so kann man also die Solarenergie speichern. Wie hoch sind denn die Verluste - Wirkungsgrad?
Am besten mal 50 min nur den Solarmotor laufen lassen und die Anzahl der Umdrehungen Zählen (Zählen lassen!). Und dann die möglichen Umdrehungen nur mit gefülltem Wasserstofftank zählen...
Gruß
Thomas