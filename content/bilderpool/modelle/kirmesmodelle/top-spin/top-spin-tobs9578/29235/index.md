---
layout: "image"
title: "Die Technik-Teil 2"
date: "2010-11-13T21:55:48"
picture: "topspin5.jpg"
weight: "5"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29235
- /detailsfc00-2.html
imported:
- "2019"
_4images_image_id: "29235"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-13T21:55:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29235 -->
Hier ist die vorläufige Steuerung über das Interface zu erkennen.
