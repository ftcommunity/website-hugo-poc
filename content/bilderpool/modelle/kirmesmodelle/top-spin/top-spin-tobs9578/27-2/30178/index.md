---
layout: "image"
title: "Das Kassenhäuschen von oben"
date: "2011-02-28T19:23:02"
picture: "top1_3.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30178
- /details2126-3.html
imported:
- "2019"
_4images_image_id: "30178"
_4images_cat_id: "2230"
_4images_user_id: "1007"
_4images_image_date: "2011-02-28T19:23:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30178 -->
In der Mitte sieht man die zugerecht gepfeilten Bausteine, da die blauen Platten ja fecherförmig auseinander gehen.