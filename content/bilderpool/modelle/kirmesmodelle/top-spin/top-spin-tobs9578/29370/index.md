---
layout: "image"
title: "Positionsanzeige"
date: "2010-11-28T09:38:02"
picture: "topspin2_3.jpg"
weight: "14"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29370
- /details60d7-3.html
imported:
- "2019"
_4images_image_id: "29370"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-28T09:38:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29370 -->
Hier sieht man die Positionsanzeige mit den Anzahl der Fahrten  und ob das Gerät betreibsbereit ist oder nicht.
