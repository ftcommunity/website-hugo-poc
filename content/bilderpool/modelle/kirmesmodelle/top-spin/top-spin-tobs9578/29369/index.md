---
layout: "image"
title: "Die Positionabfrage"
date: "2010-11-28T09:38:01"
picture: "topspin1_4.jpg"
weight: "13"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29369
- /details5202.html
imported:
- "2019"
_4images_image_id: "29369"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-28T09:38:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29369 -->
