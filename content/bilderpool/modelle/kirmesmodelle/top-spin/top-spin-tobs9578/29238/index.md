---
layout: "image"
title: "Einsteigebrücke"
date: "2010-11-14T17:58:25"
picture: "topspin3_2.jpg"
weight: "8"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29238
- /detailsc8d6.html
imported:
- "2019"
_4images_image_id: "29238"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-14T17:58:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29238 -->
Die Brücke wir mit einem Pneumatikzylinder gehoben und soll vielleicht später die Bügel öffnen.
