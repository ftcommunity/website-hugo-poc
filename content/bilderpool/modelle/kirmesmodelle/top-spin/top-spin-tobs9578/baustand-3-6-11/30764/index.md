---
layout: "image"
title: "Das Stroboskop"
date: "2011-06-03T09:38:22"
picture: "topspin3.jpg"
weight: "3"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30764
- /detailsb4e2-2.html
imported:
- "2019"
_4images_image_id: "30764"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T09:38:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30764 -->
Habe mir ein kleines Stroboskop gebaut, damit es echter wirkt.
