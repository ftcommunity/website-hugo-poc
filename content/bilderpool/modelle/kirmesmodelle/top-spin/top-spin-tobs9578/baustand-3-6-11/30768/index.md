---
layout: "image"
title: "Die Steuerung"
date: "2011-06-03T10:16:44"
picture: "topspin2_2.jpg"
weight: "7"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30768
- /detailsfdf8-2.html
imported:
- "2019"
_4images_image_id: "30768"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T10:16:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30768 -->
Mit der Steuerung kann man die die Extension mit Strom versorgen ( 1 Controller und ein Kippschalter), 2 Controller und Kippschalter für das Top Spin Schild.
Rechts der Polwendeschalter für das Stroboskop. Unter der Abdeckung verbirgt sich eine kleine Verteilerbox.
