---
layout: "image"
title: "Das Stroboskop (1)"
date: "2011-06-03T09:38:22"
picture: "topspin4.jpg"
weight: "4"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30765
- /details52d5.html
imported:
- "2019"
_4images_image_id: "30765"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T09:38:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30765 -->
