---
layout: "image"
title: "Drehkranz"
date: "2007-11-08T23:07:49"
picture: "Bild_1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Drehkranz", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12555
- /details8580.html
imported:
- "2019"
_4images_image_id: "12555"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:07:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12555 -->
Drehkranz - Seitenteil
