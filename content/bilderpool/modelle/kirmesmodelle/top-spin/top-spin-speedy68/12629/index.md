---
layout: "image"
title: "Gesamtansicht"
date: "2007-11-11T08:33:13"
picture: "Bild_12.jpg"
weight: "12"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Kirmesmodell"]
schlagworte: ["Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12629
- /detailsb20a-2.html
imported:
- "2019"
_4images_image_id: "12629"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12629 -->
Gesamtansicht von der Seite
