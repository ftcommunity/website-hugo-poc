---
layout: "image"
title: "Drehkranz"
date: "2007-11-08T23:08:51"
picture: "Bild_8.jpg"
weight: "8"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Drehkranz", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12562
- /details39a4.html
imported:
- "2019"
_4images_image_id: "12562"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:08:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12562 -->
habe 135mm Alu`s genommen, da ich von den anderen (150mm)nicht genügend hatte.
