---
layout: "image"
title: "Mittlere Ansicht - Seite"
date: "2007-11-11T08:33:13"
picture: "Bild_15.jpg"
weight: "15"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12632
- /detailsad3e.html
imported:
- "2019"
_4images_image_id: "12632"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12632 -->
Beide Drehkränze inclusive Achse wiegen gut 5 kg.
