---
layout: "image"
title: "Antrieb Version 1"
date: "2007-11-08T23:08:51"
picture: "Bild_9.jpg"
weight: "9"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Drehkranz", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12563
- /details686b.html
imported:
- "2019"
_4images_image_id: "12563"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:08:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12563 -->
Bei Version 1 wird es nicht bleiben, da die Kette mir etwas zu "Schwach" wirkt.
