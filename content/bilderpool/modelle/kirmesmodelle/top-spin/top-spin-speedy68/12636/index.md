---
layout: "image"
title: "Gondel offen"
date: "2007-11-11T08:33:13"
picture: "Bild_19.jpg"
weight: "19"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Kirmesmodell", "Gondel"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12636
- /details33f3.html
imported:
- "2019"
_4images_image_id: "12636"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12636 -->
Ist während der Fahrt verriegelt - geht nur auf, wenn Brücke zum Ein/Aussteigen unten ist
