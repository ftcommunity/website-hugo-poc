---
layout: "comment"
hidden: true
title: "5456"
date: "2008-03-03T00:53:18"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
Hallo,
danke für das Lob und die Kritik.
@Harald:
Es sind ja immer mindestens 6 Zahnräder Z20 mit der Kette „verbunden“.  Selbst, wenn die Kette locker wäre hoppelt sie nicht drüber (Schwerkraftmäßig). Den Schwachpunkt hast Du aber richtig erkannt, die äußeren 2 Zahnräder Z30, da muss ich evtl. noch Andruckrollen von außen anbringen. 
@jw:
Sinn der Kette ist, dass sie größere Kräfte aufnehmen kann, als eine normale Kette und nicht reißt.
Aus jetziger Sicht werde ich keinen größeren Umbau des Antriebs mehr vornehmen. 
@Udo2:
Die Z15 Zahnräder hatte ich in den Versionen mit den normalen ft Ketten benutzt. In dieser Version habe ich Z20 Zahnräder benutzt, die sich aber selbst nicht drehen können. Sonst hast Du alles richtig erkannt und ich bin voll Deiner Meinung. Da es mit den Z20 problemlos funktioniert spare ich mir den aufwendigen Umbau des kompletten Antriebs (wegen den Z30).
Gruß
Thomas