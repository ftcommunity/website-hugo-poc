---
layout: "comment"
hidden: true
title: "5447"
date: "2008-03-02T18:28:19"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Sinn und Zweck dieser Kette verstehe ich auch nicht. Sie ist teuer, träge und kann bestimmt kein größeres Momente übertragen als eine ft Kette. 
Würde es nicht ausreichen den zuvor gebauten Antrieb mit zwei Kettenpaaren auszustatten? Wenn ich mir den Antrieb mit den vielen Zahnrädern ansehe ist der Wirkungsgrad der Drehmomentenübertagung bezogen auf Harald´s Frage sicherlich nicht besonders hoch.

Gruß Jürgen