---
layout: "image"
title: "Versuchsantriebskette"
date: "2007-11-11T08:33:13"
picture: "Bild_23.jpg"
weight: "23"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Kirmesmodell", "Kette", "Antrieb"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/12640
- /detailsac98.html
imported:
- "2019"
_4images_image_id: "12640"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12640 -->
Habe hier mal ein Stück einer Antriebskette (Version 2).
Ist auf alle Fälle stabiler, hoffentlich klappt es damit.
