---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:31+01:00
picture: "wellenflieger-12-drehantrieb.jpg"
weight: "12"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Drehantrieb Dachstuhl mit Lagerung