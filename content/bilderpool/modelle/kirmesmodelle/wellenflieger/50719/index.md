---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:19+01:00
picture: "wellenflieger-08-ausgleichsrolle-mit-umlenkung.jpg"
weight: "8"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Detail Flaschenzug: Die obere Seilrolle gleicht die Last zwischen linker und rechter Aufhängung aus