---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:30+01:00
picture: "wellenflieger-13-detail-reibradantrieb.jpg"
weight: "13"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Detail Reibradantrieb \- Fremdteil Gummiring in der kleinen Seilrolle