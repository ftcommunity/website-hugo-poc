---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:34+01:00
picture: "wellenflieger-10-innenkorb-fuehrung.jpg"
weight: "10"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Hubkorb mit Lagerung des Dachstuhls