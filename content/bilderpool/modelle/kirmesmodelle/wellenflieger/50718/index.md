---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:18+01:00
picture: "wellenflieger-09-umlenkung-hubseil.jpg"
weight: "9"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Hubkorb ist am Mast mit 4 pendelnd gelagerten Rollenpaaren geführt\. Seilumlenkung direkt auf der Achse \- das musste so kompakt sein 