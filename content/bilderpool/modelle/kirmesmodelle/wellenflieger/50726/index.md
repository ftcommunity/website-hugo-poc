---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:29+01:00
picture: "wellenflieger-14-innenkorb-ohne-dachstuhl.jpg"
weight: "14"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Hubkorb \- Dachstuhl abgenommen