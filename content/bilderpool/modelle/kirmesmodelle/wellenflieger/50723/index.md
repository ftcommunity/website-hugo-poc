---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:25+01:00
picture: "wellenflieger-04-von-oben.jpg"
weight: "4"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Dachstuhl und Hubkorb von oben