---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:33+01:00
picture: "wellenflieger-11-mast-innenkorb-dachstuhl.jpg"
weight: "11"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Mast mit Bogenstück