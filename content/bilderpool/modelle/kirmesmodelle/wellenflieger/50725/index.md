---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:27+01:00
picture: "wellenflieger-02-gesamtansicht-in-fahrt.jpg"
weight: "2"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

In Fahrt \- Dachstuhl angehoben und geneigt