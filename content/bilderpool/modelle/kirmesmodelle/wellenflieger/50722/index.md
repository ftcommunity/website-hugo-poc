---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:23+01:00
picture: "wellenflieger-05-mastfuss.jpg"
weight: "5"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Mastfuss mit Schleifringen für Hubmotor und Drehantrieb Dachstuhl