---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:21+01:00
picture: "wellenflieger-07-hubantrieb-flaschenzug.jpg"
weight: "7"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Mast mit innenliegendem Seilzug zum Heben des Dachstuhls