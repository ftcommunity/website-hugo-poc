---
layout: "overview"
title: "Wellenflieger"
date: 2024-12-17T19:24:18+01:00
---

Der Wellenflieger aus dem Hause ZIERER \- schon seit Jahrzehnten auf Volksfesten oder in Vergnügungsparks zu sehen.
Die wellenartige Taumelbewegung kommt zustande durch die Kombination zweier Drehbewegungen: 
Den drehenden Mast, der am oberen Ende geneigt ist und dazu entgegengesetzt, um eine geneigte Achse drehend, der Aufbau, der die Gondeln trägt \- 
in Fachkreisen "Dachstuhl" genannt\. 
 
Zu Beginn der Fahrt ist der Dachstuhl mit den Gondeln noch abgesenkt, um Ein- und Aussteigen zu ermöglichen.
Sobald der Dachstuhl mittels Winde und Flaschenzug angehoben wird, drehen Mast \(langsam, rückwärts\) und Gondeln \(schneller, vorwärts\) 
entgegengesetzt, zuerst noch auf parallelen Achsen.
Sobald sich der Dachstuhl mit Gondeln am oberen Mastende schräg stellt, setzt die wellenförmige Taumelbewegung ein\.

Reizvoll am Nachbau waren vor allem die konzentrischen Drehachsen von Mast und Dachstuhl\. 
Der Drehantrieb des Mastes ist mittels Drehkranz gelöst\.
Der drehende Dachstuhl läuft auf einer Eigenbau Lagerung, bestehend aus 3 Laufrollenpaaren, Antrieb über ein Reibrad\.
