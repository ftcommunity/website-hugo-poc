---
layout: "image"
title: "Wellenflieger"
date: 2024-12-17T19:24:35+01:00
picture: "wellenflieger-01-gesamtansicht-abgesenkt.jpg"
weight: "1"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Wellenflieger"]
uploadBy: "Website-Team"
license: "unknown"
---

Stillstand \- Dachstuhl abgesenkt