---
layout: "image"
title: "Sylvias Riesenrad (1)"
date: "2004-12-05T17:48:59"
picture: "Sylvias_Riesenrad_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3418
- /detailsc459.html
imported:
- "2019"
_4images_image_id: "3418"
_4images_cat_id: "343"
_4images_user_id: "104"
_4images_image_date: "2004-12-05T17:48:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3418 -->
Ein Gemeinschaftsprojekt von Tochter und Vater ausschließlich mit Sylvias Teilen gebaut.