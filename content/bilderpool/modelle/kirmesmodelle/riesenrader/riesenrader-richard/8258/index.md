---
layout: "image"
title: "Now in Gray"
date: "2007-01-02T14:58:38"
picture: "fischertechnik_027.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/8258
- /details12db.html
imported:
- "2019"
_4images_image_id: "8258"
_4images_cat_id: "762"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8258 -->
