---
layout: "image"
title: "Riesenrad 1m"
date: "2003-11-24T21:01:24"
picture: "Riesenrad_klein.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- /php/details/2006
- /details68bb.html
imported:
- "2019"
_4images_image_id: "2006"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-24T21:01:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2006 -->
Riesenrad von burgerman