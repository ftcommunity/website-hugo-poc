---
layout: "image"
title: "Riesenrad 1m  Antrieb2"
date: "2003-11-26T15:00:56"
picture: "Riesenrad_Antrieb2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- /php/details/2009
- /details369f-2.html
imported:
- "2019"
_4images_image_id: "2009"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-26T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2009 -->
Der Antrieb von vorn bei abgenommener Verkleidungsplatte.