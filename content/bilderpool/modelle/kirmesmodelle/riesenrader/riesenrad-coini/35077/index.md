---
layout: "image"
title: "Riesenrad Komplettansicht 2"
date: "2012-06-17T14:59:38"
picture: "riesenrad02.jpg"
weight: "2"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/35077
- /details811b.html
imported:
- "2019"
_4images_image_id: "35077"
_4images_cat_id: "2597"
_4images_user_id: "1476"
_4images_image_date: "2012-06-17T14:59:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35077 -->
Inspiriert von dem Riesenrad aus dem Baukasten Master Plus Adventure Park und aus dem Baukasten Advanced Fun Park habe ich dieses Riesenrad gebaut. Das Riesenrad wird mit einem Mini-Motor über einen Gummi-Riemen angetrieben und ist beleuchtet.