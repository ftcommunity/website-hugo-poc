---
layout: "image"
title: "Riesenrad"
date: "2012-06-17T14:59:56"
picture: "riesenrad08.jpg"
weight: "8"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/35083
- /details37ee.html
imported:
- "2019"
_4images_image_id: "35083"
_4images_cat_id: "2597"
_4images_user_id: "1476"
_4images_image_date: "2012-06-17T14:59:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35083 -->
Inspiriert von dem Riesenrad aus dem Baukasten Master Plus Adventure Park und aus dem Baukasten Advanced Fun Park habe ich dieses Riesenrad gebaut. Das Riesenrad wird mit einem Mini-Motor über einen Gummi-Riemen angetrieben und ist beleuchtet.