---
layout: "overview"
title: "Riesenrad von rito"
date: 2020-02-22T07:58:12+01:00
legacy_id:
- /php/categories/3040
- /categories65b8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3040 --> 
Hallo Leute,
soeben habe ich mein erstes "Wiedereinstiegsprojekt" beendet. Mein Riesenrad 1.0 ist nun soweit, dass alle Grundfunktionen implementiert wurden und so arbeiten wie ich es mir wünsche. Nun wird es wohl noch an die Feinheiten gehen. Ich möchte es euch nun nicht vorenthalten und bin gespannt, was ihr davon haltet.