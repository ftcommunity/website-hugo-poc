---
layout: "image"
title: "Gesamtsicht"
date: "2015-02-16T17:29:12"
picture: "riesenrad01.jpg"
weight: "1"
konstrukteure: 
- "rito"
fotografen:
- "rito"
schlagworte: ["Riesenrad", "Kirmes", "TXT"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40550
- /detailsc57d.html
imported:
- "2019"
_4images_image_id: "40550"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40550 -->
Ich habe hier das Rad bestimmt nicht neu erfunden und einige Details habt ihr in alten Bauplänen bestimmt schon einmal gesehen. Aber im Großen und Ganzen habe ich mich einfach im Sinne von trial and error an das Thema Riesenrad herangepirscht!

Hier die Gesamtansicht

Programmiert wurden unterschiedliche Arten der Drehbewegung und der Blinkrhythmus der Lämpchen!