---
layout: "image"
title: "Gondel"
date: "2015-02-16T17:29:12"
picture: "Gondel.jpg"
weight: "6"
konstrukteure: 
- "rito"
fotografen:
- "rito"
schlagworte: ["Riesenrad", "Kirmes"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40555
- /detailsc0a9.html
imported:
- "2019"
_4images_image_id: "40555"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40555 -->
Ich glaube diese Art von Gondel wurde in ähnlicher Weise schön öfter gebaut.