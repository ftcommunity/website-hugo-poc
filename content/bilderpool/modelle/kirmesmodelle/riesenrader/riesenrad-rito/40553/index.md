---
layout: "image"
title: "Achsenklammer zum Abnehmen des Stromes an der Achse"
date: "2015-02-16T17:29:12"
picture: "Achsenklammer.jpg"
weight: "4"
konstrukteure: 
- "rito"
fotografen:
- "rito"
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/40553
- /detailsdff1.html
imported:
- "2019"
_4images_image_id: "40553"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40553 -->
Die Stromkreise aller Lämpchen werden über eine Klammer mit der Mittelachse verbunden und dann auf der Rückseite per Metallklammer mit der Erde des TXT verbunden.