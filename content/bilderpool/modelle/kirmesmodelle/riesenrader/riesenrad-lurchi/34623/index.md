---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_6499.jpg"
weight: "10"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34623
- /details6187.html
imported:
- "2019"
_4images_image_id: "34623"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34623 -->
Die "Strahlen" und ihre Verstrebungen.