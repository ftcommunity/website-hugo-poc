---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2428.jpg"
weight: "5"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34618
- /details5982.html
imported:
- "2019"
_4images_image_id: "34618"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34618 -->
Einblick auf die Innennabe.
Wie zu erkennen ist, besteht die Innennabe im Kern aus einer serienmäßigen ft-Metall-Steckachse (Art.-Nr. 31315) mit den Maßen 4 x 235mm.
Auf ihr wurden mittels Flachnaben vier Drehscheiben montiert, die über sechs eingeschobene und je 125 mm langen Metall-Achesen verstärkt wurden.
Bombenstabil und somit nur minimale Durchbiegung der Hauptachse.