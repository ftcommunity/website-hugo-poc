---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2447.jpg"
weight: "19"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34632
- /detailse06c.html
imported:
- "2019"
_4images_image_id: "34632"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34632 -->
Einblick auf Hauptachse (Seriensteckachse 235) und Lager.
Gut zu erkennen, faktisch keine Durchbiegung trotz des hohen Gewichtes des Rades.