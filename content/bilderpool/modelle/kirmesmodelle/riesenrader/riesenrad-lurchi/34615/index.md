---
layout: "image"
title: "Riesenrad-Innennabe"
date: "2012-03-10T23:15:59"
picture: "IMG_2420.jpg"
weight: "2"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/34615
- /detailsbd29.html
imported:
- "2019"
_4images_image_id: "34615"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34615 -->
Im Mittelpunkt zum Bau des Riesenradmodellls stand die Konstruktion einer geeigneten Nabe.

Ein Wunsch meinerseits war es zudem diese hochbelastbare Nabe ausschließlich aus unveränderten Serienbauteilen zu erstellen, was bei ähnlich großen Modellen dieser Art bisher nicht der Fall war.