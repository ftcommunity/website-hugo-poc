---
layout: "image"
title: "Selbstdrehende Sitze"
date: 2022-01-07T16:22:53+01:00
picture: "2021-12-31_Riesenrad10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Über das Winkelgetriebe führt das Mitdrehen der waagerechten Achse mit dem Riesenrad dazu, dass sich der Sitz und das Männchen um seine senkrechte Achse dreht. Das Männchen guckt also immer mal in jede Richtung.

Die Idee stammt nicht von mir; die gab es schon in früheren Riesenradmodellen.