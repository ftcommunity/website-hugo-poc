---
layout: "image"
title: "Stütze auf Antriebsseite"
date: 2022-01-07T16:22:48+01:00
picture: "2021-12-31_Riesenrad04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach und unspektakulär.