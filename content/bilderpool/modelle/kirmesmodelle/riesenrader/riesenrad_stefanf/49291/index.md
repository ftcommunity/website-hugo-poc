---
layout: "image"
title: "Starre Befestigung der Sitzträger-Achse"
date: 2022-01-07T16:22:42+01:00
picture: "2021-12-31_Riesenrad09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf den Achsen, die die Sitze tragen, steckt je ein S-Mitnehmer (hier das schwarze Teil), damit sich die Achse genauso dreht, wie das Rad. Das ist wichtig für die sich um sich selbst drehenden Sitze.