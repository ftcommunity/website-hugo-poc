---
layout: "image"
title: "Verbindung zwischen Strebe und Rad"
date: 2022-01-07T16:22:43+01:00
picture: "2021-12-31_Riesenrad08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Länge der Statikträger passt wie dafür gemacht.