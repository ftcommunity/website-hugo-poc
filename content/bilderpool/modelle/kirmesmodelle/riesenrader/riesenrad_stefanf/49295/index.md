---
layout: "image"
title: "Stütze auf der Rückseite"
date: 2022-01-07T16:22:47+01:00
picture: "2021-12-31_Riesenrad05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist hier genauso aufgebaut wie auf der anderen Seite, nur ohne den Motor.