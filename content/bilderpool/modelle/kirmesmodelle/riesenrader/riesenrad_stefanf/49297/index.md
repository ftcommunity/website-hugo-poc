---
layout: "image"
title: "Achslagerung"
date: 2022-01-07T16:22:49+01:00
picture: "2021-12-31_Riesenrad03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die eine einzige lange durchgehende Achse ist vierfach, aber leichtgängig gelagert.