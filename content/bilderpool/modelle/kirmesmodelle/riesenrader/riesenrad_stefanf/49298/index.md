---
layout: "image"
title: "Motor"
date: 2022-01-07T16:22:51+01:00
picture: "2021-12-31_Riesenrad02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Motor ist mit wenigen Grundbausteinen, zwei Riegelsteinen und vier Platten 15x30 an der Stütze befestigt. Die fischertechnik-Antriebsfeder sorgt für eine gewissen Entkopplung des Motors vom Rad, sodass der Anlauf und Stopp das Material nur wenig belasten. Auch das schnelle Drehen des Riesenrads von Hand (wenn's dem Kind nicht schnell genug geht) ist damit kein Problem.