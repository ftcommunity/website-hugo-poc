---
layout: "image"
title: "Befestigung der Streben am Rad"
date: 2022-01-07T16:22:45+01:00
picture: "2021-12-31_Riesenrad07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Je zwei I-Streben 15 bringen die Strebe mittig zwischen zwei Rad-Löchern. Das Rad besteht aus Bogenstücken 30°.