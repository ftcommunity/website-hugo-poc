---
layout: "image"
title: "Gesamtsicht auf den Sitz"
date: 2022-01-07T16:22:52+01:00
picture: "2021-12-31_Riesenrad11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Sitz selbst ist aus wenigen Teilen zusammengesetzt.