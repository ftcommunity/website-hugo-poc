---
layout: "image"
title: "Das Rad-Innere"
date: 2022-01-07T16:22:46+01:00
picture: "2021-12-31_Riesenrad06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die I-Streben 90 treffen das einzuhaltende Maß (man beachte den BS7,5) auf 0,3 mm genau.