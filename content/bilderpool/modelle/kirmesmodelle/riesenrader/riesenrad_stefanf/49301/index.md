---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-07T16:22:54+01:00
picture: "2021-12-31_Riesenrad01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein mit nur 6 V betriebener M-Motor betreibt das in einer ganz angenehmen Geschwindigkeit.

Ein Video gibt's unter https://youtu.be/VOh1srePFE4