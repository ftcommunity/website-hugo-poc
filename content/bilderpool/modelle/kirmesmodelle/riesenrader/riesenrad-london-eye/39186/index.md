---
layout: "image"
title: "Einstiegshilfe"
date: "2014-08-08T21:21:23"
picture: "DSC00098.jpg"
weight: "7"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["bauFischertechnik", "Fischertechnik", "automatisch", "Beleuchtung", "computergesteuert", "Robo", "TX", "Controller", "RoboPro", "Reisenrad", "London", "Eye", "Roboter", "Kirmes", "Folksfest", "Oktoberfest", "Ferris", "wheel"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39186
- /detailsea89.html
imported:
- "2019"
_4images_image_id: "39186"
_4images_cat_id: "2930"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39186 -->
Diese klappt durch die Pneumatikzylinder hoch.