---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination6.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48190
- /details01d1-2.html
imported:
- "2019"
_4images_image_id: "48190"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48190 -->
Zur Stromversorgung habe ich Segmentweise zwei Kupferdrähte gezogen und die Led`s angelötet
