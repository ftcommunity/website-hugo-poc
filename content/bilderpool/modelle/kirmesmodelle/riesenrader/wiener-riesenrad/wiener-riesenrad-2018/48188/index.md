---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48188
- /details2a28.html
imported:
- "2019"
_4images_image_id: "48188"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48188 -->
Hier eine leuchtende Röhre. Ursprünglich wollte ich mal jede Röhre mit zwei Led`s bestücken.
Da hätte ich 240 Led`s verbauen müssen und das war mir zuviel. Um den "Hotspot" Effekt an der Leuchtmittel-Seite zu mildern,
habe  ich an der hier noch offenen Seite eine 4 mm weiß beschichtete , 1,5 mm dicke Pappe ausgehauen und als "Reflektor"
in die Röhre geklebt.Den Schwarzen Ring habe ich dann noch mit grauem Iso Tape gecovert und das ganz dann in das Alu Profil gedrückt.
