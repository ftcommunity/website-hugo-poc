---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination7.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48191
- /details708d.html
imported:
- "2019"
_4images_image_id: "48191"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48191 -->
Hier das Ergebniss. Etwas schwierig zu fotografieren bei Gegenlicht.....
