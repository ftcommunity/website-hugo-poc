---
layout: "image"
title: "Wiener Riesenrad"
date: "2011-12-27T11:34:57"
picture: "wienerriesenrad3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33810
- /detailsf276.html
imported:
- "2019"
_4images_image_id: "33810"
_4images_cat_id: "2498"
_4images_user_id: "968"
_4images_image_date: "2011-12-27T11:34:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33810 -->
Die Achse besteht aus einer 10mm Edelstahlwelle.
Auf der Welle stecken 2 Sperrholzscheiben die straff in einem 80mm Alurohr sitzen.
Die Aluohre haben jeweils 48 Bohrungen für die Speichen.
