---
layout: "image"
title: "Wiener Riesenrad"
date: "2011-12-27T11:34:57"
picture: "wienerriesenrad1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33808
- /detailsd896.html
imported:
- "2019"
_4images_image_id: "33808"
_4images_cat_id: "2498"
_4images_user_id: "968"
_4images_image_date: "2011-12-27T11:34:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33808 -->
Hier der Radreifen des Riesenrad´s mit einer Musterkabine.
