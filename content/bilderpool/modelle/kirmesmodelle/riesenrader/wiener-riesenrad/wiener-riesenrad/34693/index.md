---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad6.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34693
- /details17c4.html
imported:
- "2019"
_4images_image_id: "34693"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34693 -->
Da paßt es so grade.
