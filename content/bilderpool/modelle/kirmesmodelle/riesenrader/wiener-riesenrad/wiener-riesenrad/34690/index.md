---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34690
- /details0632.html
imported:
- "2019"
_4images_image_id: "34690"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34690 -->
