---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad5.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/34692
- /details94a7.html
imported:
- "2019"
_4images_image_id: "34692"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34692 -->
Der Kollege Zivi mit Rollstuhl.
