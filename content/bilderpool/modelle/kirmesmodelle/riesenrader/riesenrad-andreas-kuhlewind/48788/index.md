---
layout: "image"
title: "Gondeln"
date: 2020-08-09T19:40:22+02:00
picture: "rrad0109.jpg"
weight: "9"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

...eine eher langweilige Massenproduktion