---
layout: "overview"
title: "Riesenrad von Andreas Kuhlewind
"
date: 2020-08-09T19:40:22+02:00
---

### Ein Großmodell

Nach zahlreichen kleineren Modellen, wurde es Zeit für eine wirklich große Eigenkreation.

25.000 Einzelteile und 3 Monate später stand ein weitestgehend fertiges Riesenrad im Wohnzimmer.

Die ersten Entwürfe bestanden aus einer Mischung aus grauen, roten, gelben und schwarzen Teilen. Das war mir aber zu bunt, und ich hatte mich daher für ein etwas edleres rot-schwarzes Design für die finale Version entschieden.

Da ich kurz zuvor London besucht hatte und dort mit dem London Eye gefahren bin, kam mir die Idee, die Gondeln in einem Laufkäfig unterzubringen.

Das Riesenrad sollte auf einer Ausstellung gezeigt werden, daher musste es für den Transport einfach in Einzelkomponenten zerlegt werden können - eine weitere Herausforderung. Für die Höhe waren 2.50m angepeilt, da es dann noch grad so unter die Decke des Wohnzimmers passt.

Der Plan war, mit den Erfahrungen des ersten Riesenrades noch zwei weitere zu bauen, eines in grauer Farbe und ein gelbes. Das wäre dann relativ schnell machbar gewesen, da nur der erste Prototyp aufwendig ist, die Nachbauten gehen dann schnell von der Hand. Es wäre sicher ein spektakulärer Anblick gewesen...

Leider wurde die Convention wegen Corona abgesagt.

Daher habe ich am Ende auf den Bau der Einstiegszone zu den Gondeln sowie die beiden weiteren Modelle verzichtet. Die Einstiegszone wäre nur kosmetischer Natur gewesen, und ich hatte einfach keine Motivation mehr, dieses nunmehr sinnlos gewordene Detail zu bauen. Stattdessen habe ich das Modell sang- und klanglos wieder abgebaut, da ich es nicht auf ungewisse Zeit im Wohnzimmer haben wollte.

Ansonsten war das Modell jedoch fertig und funktionierte trotz des hohen Gewichtes und der verbauten Massen erstaunlich gut und zuverlässig.

Weitere Infos und Videos finden sich
hier: http://kuhlewind.net/index.php/fischertechnik8/grossmodelle/riesenrad
und hier: https://www.youtube.com/watch?v=TI40rOdf2Qs

Viel Spaß mit diesem Modell :-)
