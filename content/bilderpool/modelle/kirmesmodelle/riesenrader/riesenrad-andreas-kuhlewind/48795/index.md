---
layout: "image"
title: "Modularer Aufbau"
date: 2020-08-09T19:40:30+02:00
picture: "rrad0102.jpg"
weight: "2"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Für den Transport kann das Modell leicht in große Einzelkomponenten zerlegt werden.