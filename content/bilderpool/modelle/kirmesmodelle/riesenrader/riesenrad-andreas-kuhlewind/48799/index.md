---
layout: "image"
title: "Module"
date: 2020-08-09T19:40:35+02:00
picture: "rrad0112.jpg"
weight: "12"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

In die Einzelkomponenten zerlegt, passt das Rad in einen größeren Kombi.