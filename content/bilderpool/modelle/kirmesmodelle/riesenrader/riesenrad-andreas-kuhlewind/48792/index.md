---
layout: "image"
title: "Kassenhäuschen "
date: 2020-08-09T19:40:27+02:00
picture: "rrad0105.jpg"
weight: "5"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Ein Kassenhäuschen darf natürlich nicht fehlen