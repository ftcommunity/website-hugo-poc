---
layout: "image"
title: "Kettenantrieb"
date: 2020-08-09T19:40:25+02:00
picture: "rrad0107.jpg"
weight: "7"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Zwei Antriebsketten laufen über das nach außen erweiterte Innenrad.