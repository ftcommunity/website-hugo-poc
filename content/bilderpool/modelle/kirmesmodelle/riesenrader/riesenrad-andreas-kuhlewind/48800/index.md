---
layout: "image"
title: "Eine Gondel"
date: 2020-08-09T19:40:36+02:00
picture: "rrad0111.jpg"
weight: "11"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Nach vielen Versuchen zeigte es sich, dass die Gondeln auf 4 Spurkranzrädern ohne Gummis mit einem Gegengewicht versehen und die Achsen mit etwas Vaseline beschmiert recht gut in den Laufkäfigen rotieren. Maximal vier Figuren können so pro Gondel fahren, bei fünf Figuren reicht dann das Gegengewicht nicht mehr aus.