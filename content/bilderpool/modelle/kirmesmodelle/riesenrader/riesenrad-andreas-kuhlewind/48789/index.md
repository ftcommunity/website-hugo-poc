---
layout: "image"
title: "Motoren"
date: 2020-08-09T19:40:24+02:00
picture: "rrad0108.jpg"
weight: "8"
konstrukteure: 
- "Andreas Kuhlewind"
fotografen:
- "Andreas Kuhlewind"
schlagworte: ["Nordconvention-2020"]
uploadBy: "Website-Team"
license: "unknown"
---

Zwei 50:1 Powermotoren treiben jeweils eine Kette an.

Durch die hohe Übersetzung (50:1 * z10/Innerad) dauert eine komplette Umdrehung des Riesenrades 4-5 Minuten. Das ergibt ein schönes Fahrbild.

Die Motoren können sich auf Metallstangen vertikal bewegen und werden durch stabile Gummis nach unten gezogen, sodass eine vernünftige Kettenspannung gewährleistet ist.