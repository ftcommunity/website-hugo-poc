---
layout: "image"
title: "Ansicht von oben"
date: "2003-07-27T12:02:36"
picture: "Draufsicht.jpg"
weight: "13"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1272
- /detailsffb0.html
imported:
- "2019"
_4images_image_id: "1272"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-07-27T12:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1272 -->
So sieht's von oben aus. Bei den etwas scmalerenGondeln ist anstatt der Platte 30x90 eine Platte 15x90