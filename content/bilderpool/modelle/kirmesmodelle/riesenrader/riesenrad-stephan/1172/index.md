---
layout: "image"
title: "Details vom Riesenrad 2"
date: "2003-06-03T22:56:59"
picture: "CNXT0033_1.jpg"
weight: "3"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1172
- /detailscc23.html
imported:
- "2019"
_4images_image_id: "1172"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T22:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1172 -->
Das ist die Aufhängung der Strebe vom Rad.
