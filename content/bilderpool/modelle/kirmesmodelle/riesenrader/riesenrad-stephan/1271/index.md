---
layout: "image"
title: "Gondel von einer anderen Seite aus gesehen"
date: "2003-07-27T12:02:36"
picture: "Seitenansicht_von_der_Gondel.jpg"
weight: "12"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1271
- /details9b28.html
imported:
- "2019"
_4images_image_id: "1271"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-07-27T12:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1271 -->
Hier mal ein Bild von der anderen Seite. Verbunden habe ich die Verkleidungsplatten mit dem Zwischenstück (Nr:38428)