---
layout: "image"
title: "Das erste Bild vom neuen Riesenrad"
date: "2003-07-08T17:12:53"
picture: "Mein_RR_im_Rohbau_2.jpg"
weight: "10"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1224
- /details5430.html
imported:
- "2019"
_4images_image_id: "1224"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1224 -->
Weil mich immer wieder welche fragen wie mein Riesenrad aussieht nun ein Bild vom demselbigen im "Rohbau" . Inzwischen habe ich es ganz geschlossen und die Stützen noch um 1 Bauplatte 90x 180 erhöht. Sonst paßten die Gondeln nicht dran. Bis zur Zimmerdecke sind es ,wenn die Gondeln dran sind, noch knapp 12cm.