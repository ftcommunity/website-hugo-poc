---
layout: "image"
title: "Riesenrad"
date: "2003-05-22T16:07:11"
picture: "R2.jpg"
weight: "1"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1134
- /detailsc136-2.html
imported:
- "2019"
_4images_image_id: "1134"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-05-22T16:07:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1134 -->
Hier ist mein "altes" Riesenrad zu sehen. Hatte ne Höhe von ca.1,50m. Ist leider nicht fertig geworden.Baue aber zZt. an einem neuen das noch etwas größer werden soll.Höhe ca.180 cm.