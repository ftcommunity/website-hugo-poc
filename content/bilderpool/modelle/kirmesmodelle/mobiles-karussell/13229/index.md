---
layout: "image"
title: "Draufsicht"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel07.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13229
- /detailsc3c1.html
imported:
- "2019"
_4images_image_id: "13229"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13229 -->
