---
layout: "image"
title: "Draufsicht"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel19.jpg"
weight: "19"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13241
- /detailsa6be-2.html
imported:
- "2019"
_4images_image_id: "13241"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13241 -->
