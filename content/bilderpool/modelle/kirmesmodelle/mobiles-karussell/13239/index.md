---
layout: "image"
title: "Von Vorne"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel17.jpg"
weight: "17"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13239
- /detailsb6f8.html
imported:
- "2019"
_4images_image_id: "13239"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13239 -->
