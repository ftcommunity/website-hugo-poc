---
layout: "image"
title: "Kabelsalat"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion32.jpg"
weight: "32"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29949
- /details994d-2.html
imported:
- "2019"
_4images_image_id: "29949"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29949 -->
Hier ohne Bodenplatten. Die Kabel sind strukturiert durch Kabelhalter unter der Bodenplatte verlegt. Links-Oben gehen die Kabelstränge in die Turmspitze ab.
