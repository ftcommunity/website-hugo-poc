---
layout: "image"
title: "Detail-Gipfelstation"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion09.jpg"
weight: "9"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29926
- /details4455.html
imported:
- "2019"
_4images_image_id: "29926"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29926 -->
Links: Der Hochzieher
Mitte-Rechts: Zahnräder des Abkoppelmechanismus
