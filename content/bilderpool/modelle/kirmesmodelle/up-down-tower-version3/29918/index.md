---
layout: "image"
title: "Gesamtansicht"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion01.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29918
- /detailsbeb3.html
imported:
- "2019"
_4images_image_id: "29918"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29918 -->
Eine Gesamtansicht meines "UP & DOWN Tower"
Daten:
    - Maße: 3,00*1,00*0,60m (H*B*T)
    - Kapazität: 12 Personen pro Fahrt
    - Teileanzahl: 8882 (ohne Kabel und Stecker ;-)
    - Steuerung: Interface, Extension, Relaiseinheit, 5 PMs, 3 MMs (, 32 LEDs), 12 Taster, 3 Reedkontakte
    - Fahrzeit: ca. 4min (Mittelstation), ca. 5min (Gipfelstation)
    - Funktion: 1. Hochzieher fährt runter und hakt sich am Zug ein
                      2. Hochzieher zieht den Zug bis zu einer der Stationen
                      3. Der Zug wird ausgehakt und fällt im "freien Fall"
                      4. Der Zug pendelt sich aus und kommt in der Station zum stehen ;-)
                      Außerdem: - Schranken auf / zu
                                        - Stationsbeleuchtung, Gesamtbeleuchtung
    - Bauzeit: ca. 6 Monate (es gab mehrere Zwischenstufen, die immer wieder umgebaut / ab- und neugebaut wurden)
