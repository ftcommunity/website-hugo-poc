---
layout: "image"
title: "Ausgang"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion30.jpg"
weight: "30"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29947
- /details1fda-2.html
imported:
- "2019"
_4images_image_id: "29947"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29947 -->
Auch der Ausgang kann mit dem kleinen Stecker an die Station angesteckt werden.
