---
layout: "image"
title: "Zug3"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion26.jpg"
weight: "26"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29943
- /details0e65.html
imported:
- "2019"
_4images_image_id: "29943"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29943 -->
Hier gut zusehen: Die Räder sind mit 7,5° + 7,5° (Oberen) und mit 30° (Unteren) befestigt. 
Außerdem: Der Haken, in den der Hochzieher einhakt und der Magnet der die Reedkontakte aktiviert.
