---
layout: "image"
title: "Detail-Seilwinde3"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion21.jpg"
weight: "21"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29938
- /details6429.html
imported:
- "2019"
_4images_image_id: "29938"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29938 -->
3 Power Motoren (20:1) treiben die Seilwinde (Z30) über eine Schnecke an. Die Motoren werden über das PC-Netzteil betrieben, da sie über 1A Strom brauchen.
