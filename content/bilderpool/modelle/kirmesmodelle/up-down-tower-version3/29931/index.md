---
layout: "image"
title: "Pferdekopf2"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion14.jpg"
weight: "14"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29931
- /detailsa98e.html
imported:
- "2019"
_4images_image_id: "29931"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29931 -->
Die Stange an der Nabe (Flachnabenzange + Nabenmutter) kann heraus gezogen werden. Nun kann der mit Gelenken montierte Vorbau hoch geklappt und der Zug herunter genommen werden.
