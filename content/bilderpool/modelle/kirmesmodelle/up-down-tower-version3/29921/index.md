---
layout: "image"
title: "Turmspitze-Nebenturm"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion04.jpg"
weight: "4"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29921
- /details4a05.html
imported:
- "2019"
_4images_image_id: "29921"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29921 -->
Der Ausschwing-Turm auch "Pferdekopf" genannt, wegen der oberen Form. Dieser kann hochgeklappt werden um den Zug von der Schiene zunehmen.
