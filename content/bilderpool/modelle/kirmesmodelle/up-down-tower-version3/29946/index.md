---
layout: "image"
title: "Eingang mit 2 Kassenhäuschen"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion29.jpg"
weight: "29"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/29946
- /details06a8.html
imported:
- "2019"
_4images_image_id: "29946"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29946 -->
Die Rampen sind beweglich gelagert, um Höhenunterschiede auszugleichen.
Außerdem kann der Eingang mit den abstehenden Stangen und einem weiteren Stecker an die Station angesteckt werden.
