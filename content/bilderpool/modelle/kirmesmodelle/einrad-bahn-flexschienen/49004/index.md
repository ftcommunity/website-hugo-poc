---
layout: "image"
title: "Antrieb der waagerechten Weiche"
date: 2021-02-26T21:31:32+01:00
picture: "einradbahnflexschienen13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts sieht man das Hubgetriebe und die beiden mit Dioden versehenen Endlagentaster. Die Schaltung ist die aus "Motorsteuerungen (Teil 3)" in ft:pedia 2011-3. Die Grundbausteine sind wesentlich steifer als Statikträger, deshalb wurden die überall verwendet, wo es auf Steifigkeit ankommt.