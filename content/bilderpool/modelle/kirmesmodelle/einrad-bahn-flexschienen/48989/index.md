---
layout: "image"
title: "Rückkehr-Kurve der langen Strecke"
date: 2021-02-26T21:31:15+01:00
picture: "einradbahnflexschienen27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier führt die lange Strecke zurück. Der Wagen kommt von rechts unten im Bild und fährt in Bildmitte nach rechts wieder heraus.