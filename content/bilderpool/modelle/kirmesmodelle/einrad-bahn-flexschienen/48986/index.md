---
layout: "image"
title: "Der Wagen (1)"
date: 2021-02-26T21:31:11+01:00
picture: "einradbahnflexschienen03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Wagen ist simpel: 9-V-Batteriehalter, XS-Motor, Kette, Vorstufe-Rad 23 mit Gummireifen.