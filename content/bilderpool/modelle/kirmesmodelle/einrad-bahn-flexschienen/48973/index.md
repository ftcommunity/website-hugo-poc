---
layout: "image"
title: "90°-Kurve"
date: 2021-02-26T21:30:56+01:00
picture: "einradbahnflexschienen09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine 90°-Kurve besteht nach den Bausteinen zur Befestigung der Flexschienen aus Statikbaustein 15, Winkelstein 30° (die neue, leicht größere Version, nicht die Ur-Version), BS30, BS15, BS5, WS30°, BS5, BS15, BS30, WS30°, BS15 mit zwei Zapfen.
