---
layout: "image"
title: "Die Bahn"
date: 2021-02-26T21:30:57+01:00
picture: "einradbahnflexschienen08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Los geht's: Der Wagen hängt auf der Flexschiene und fährt Richtung rechts hinten los, in die erste 90°-Kurve.