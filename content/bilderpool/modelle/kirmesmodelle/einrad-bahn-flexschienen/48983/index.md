---
layout: "image"
title: "Endlagentaster der senkrechten Weiche"
date: 2021-02-26T21:31:07+01:00
picture: "einradbahnflexschienen32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der obere Taster wird gedrückt, wenn der Motor am Faden zieht und das waagerechte Streckenstück, das in diesem Bild unten liegt und dem Talfahrt-Stück Platz macht, nach oben in die Bahn gedreht hat. Der untere Endlagentaster hat seinen Stößel nach hinten im Bild und ist gerade betätig.