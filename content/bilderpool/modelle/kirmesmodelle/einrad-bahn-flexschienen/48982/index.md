---
layout: "image"
title: "Die letzte S-Kurve"
date: 2021-02-26T21:31:06+01:00
picture: "einradbahnflexschienen33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese kleine S-Kurve führt die Bahn wieder an ihren Anfang zurück.