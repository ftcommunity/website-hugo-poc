---
layout: "image"
title: "Blick von unten auf den Antrieb"
date: 2021-02-26T21:31:25+01:00
picture: "einradbahnflexschienen19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Motor und das Hubgetriebe müssen nur schieben, aber keine nennenswerten Tragkräfte aufnehmen.