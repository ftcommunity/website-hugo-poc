---
layout: "image"
title: "Alu-Profil für die kurze Strecke"
date: 2021-02-26T21:31:30+01:00
picture: "einradbahnflexschienen15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die kurze Strecke führt über das Aluprofil. Des sitzt hier deshalb, weil es nur links hinten gelagert werden kann. Vorne muss der Wagen vorbei können, und hinnten auch (wie der Fahrt um die Kurve für die lange Strecke nämlich).