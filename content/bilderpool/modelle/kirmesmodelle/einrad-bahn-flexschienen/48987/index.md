---
layout: "image"
title: "Senkrechte Weiche (1)"
date: 2021-02-26T21:31:12+01:00
picture: "einradbahnflexschienen29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die zweite Weiche ist eine senkrechte. Hier sieht man die Stellung für die kurze Fahrt: Der Wagen kommt von links, fährt über das hier sichtbare Brückenstück und dann nach rechts weiter.