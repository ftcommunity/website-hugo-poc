---
layout: "image"
title: "Senkrechte Weiche (2)"
date: 2021-02-26T21:31:10+01:00
picture: "einradbahnflexschienen30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die senkrechte Weiche auf die lange Fahrt eingestellt: Der Wagen kommt von links oben ins Bild, fährt die Abfahrt hinunter und nach rechts weiter.

Links oben sieht man eine Platte 15x15 und ein etwas versetztes Schienenstück ankommen. Das führt dazu, dass das Rad des Wagens sicher auf die Talfahrt-Schiene gelangt.

Das Geheimnis ist die Drehachse. Unten sieht man nämlich die waagerechte Brücke vom vorherigen Bild. Die beiden Teile sind um 90° verdreht auf der drehbaren Mechanik angeordnet. So kann wahlweise eine der beiden Streckenstücke in die Bahn gebracht werden, und die andere macht Platz.