---
layout: "image"
title: "S-Kurve vor der zweiten Weiche"
date: 2021-02-26T21:31:13+01:00
picture: "einradbahnflexschienen28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch hier gibt es eine S-Kurve, damit die Bahn korrekt an der zweiten Weiche ankommt.