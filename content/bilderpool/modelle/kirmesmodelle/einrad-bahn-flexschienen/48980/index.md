---
layout: "image"
title: "Ein-/Ausschalter"
date: 2021-02-26T21:31:04+01:00
picture: "einradbahnflexschienen35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Als nachträgliche Verbesserung bekam die Bahnelektronik noch einen Ein-Ausschalter, der auf den anderen Bildern und auch auf dem Video noch nicht angebracht war.