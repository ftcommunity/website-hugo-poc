---
layout: "image"
title: "Die waagerechte Weiche"
date: 2021-02-26T21:31:33+01:00
picture: "einradbahnflexschienen12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Wagen kommt von rechts an. In der hier gezeigten Stellung führt die Strecke zum kurzen Rundweg, hinten ginge es zum langen Außenweg. Die Weiche wird von einem S-Motor mit Hubgetriebe verschoben. Da sich dabei die Länge der Weichenstrecke ändert, stecken in den BS30 in der Mitte zwei Metallachsen als Teleskopauszug.