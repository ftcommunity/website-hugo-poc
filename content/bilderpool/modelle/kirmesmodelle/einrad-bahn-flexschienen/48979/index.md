---
layout: "image"
title: "Elektronik"
date: 2021-02-26T21:31:03+01:00
picture: "einradbahnflexschienen36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das linke E-Tec-Modul im Bild hat seine DIP-Schalter auf 0-0-1-1 stehen und ist ein Monoflop. Es wird vom Fototransistor der Lichtscrhanke an I1 angesteuert. Sobald der Wagen die Lichtschranke passiert und sie wieder freigegeben hat, schaltet das Monoflop, so wie hier beschaltet, den Ausgang für ca. 2 Sekunden auf "1". Das ist sozusagen die Entprellung, denn der Wagen kann durch Wackeln die Lichtschranke bei einer Durchfahrt mehrmals unterbrechen und wieder freigeben. Gebraucht wird aber zuverlässig genau ein Impuls pro Durchfahrt.

Der Ausgang des Monoflops geht auf den Trigger-Eingang des zweiten, als D-Flipflop geschalteten E-Tecs (DIP-Schalter auf 1-0-1-1, den zweiten Pol des Ausgangs mit I1 verbunden). Jeder Impuls des Monoflops schaltet das Flipflop also um.

Die so erreichte Polumkehr bei jedem Durchgang geht an beide Weichen. Deren Motoren versetzen die Weichen dadurch in die jeweils gewünschte Lage. Die Endlagentaster der Weichen sorgen fürs punktgenaue Anhalten. Die Dioden auf den Tastern erlauben aber den Strumdurchfluss in die andere Richtung (zur Diodenschaltung siehe "Motorsteuerungen (Teil 3)" in ft:pedia 2011-3).