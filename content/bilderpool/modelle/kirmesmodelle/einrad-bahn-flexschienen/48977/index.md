---
layout: "image"
title: "Der Wagen (3)"
date: 2021-02-26T21:31:00+01:00
picture: "einradbahnflexschienen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Batteriehalter hängt mittels zweier Federnocken an einem einzigen Baustein 15x30 mit 1 Zapfen, der über einen BS15 mit dem Rest verbunden ist.