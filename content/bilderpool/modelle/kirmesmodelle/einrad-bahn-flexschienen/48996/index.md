---
layout: "image"
title: "Weichenstellung für kurze Fahrt"
date: 2021-02-26T21:31:23+01:00
picture: "einradbahnflexschienen20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Blick von oben auf die Weiche, eingestellt auf die kurze Rundfahrt.