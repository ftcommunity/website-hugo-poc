---
layout: "image"
title: "Endlagentaster"
date: 2021-02-26T21:31:26+01:00
picture: "einradbahnflexschienen18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der linke Taster stoppt den Motor, wenn er gedrückt wird. Der rechte ist andersherum verschaltet und stoppt ihn, wenn er von der nach rechts fahrenden Schubstange losgelassen wird.