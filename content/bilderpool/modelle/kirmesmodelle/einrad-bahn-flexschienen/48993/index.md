---
layout: "image"
title: "Verkürzung der kurzen Strecke (1)"
date: 2021-02-26T21:31:19+01:00
picture: "einradbahnflexschienen23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Damit alles aufgeht, muss die kurze Strecke etwas verkürzt werden. Deshalb gibt es auf dem Rückweg eine S-Kurve. Der schwarze Statikträger 60, der da so unvermittelt herumliegt, stützt die Konstruktion, die ja das Gewicht des Wagens tragen muss.