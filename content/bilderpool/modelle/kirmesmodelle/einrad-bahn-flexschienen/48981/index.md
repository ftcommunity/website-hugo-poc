---
layout: "image"
title: "Lichtschranke"
date: 2021-02-26T21:31:05+01:00
picture: "einradbahnflexschienen34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Linsenlampe beleuchtet eine Fotozelle. Der Batteriehalter des Wagens unterbricht diese Lichtschranke bei jeder Durchfahrt. Das wird ausgenutzt, um die Weichen umzuschalten. Deshalb fährt der Wagen immer abwechselnd die kurze oder die lange Strecke.