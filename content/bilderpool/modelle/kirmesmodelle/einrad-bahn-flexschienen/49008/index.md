---
layout: "image"
title: "Gesamtansicht (1)"
date: 2021-02-26T21:31:37+01:00
picture: "einradbahnflexschienen01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der rechts vorne hängende Wagen fährt die Strecke ab. Es gibt einen kurzen Rundweg und einen langen Weg außenherum, zwei Brücken und zwei unterschiedlich gebaute Weichen.

Ein Video gibt's unter https://www.youtube.com/watch?v=ebOAilHBnCk