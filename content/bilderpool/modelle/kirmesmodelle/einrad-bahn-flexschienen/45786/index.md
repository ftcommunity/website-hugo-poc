﻿---
layout: "image"
title: "Schaltplan"
date: 2023-01-18T18:56:46+01:00
picture: "Einrad-Bahn-auf-Flexschienen_Schaltplan1.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf Nachfrage eines Fans hier der Schaltplan der Anlage. Man beachte, dass der Endlagentaster für die kurze Strecke bei der waagerechten Weiche als Öffner angebracht ist, alle anderen als Schließer. Deshalb sind die Anschlüsse und die Diodenrichtung bei diesem Taster vertauscht. Siehe die Bilder mit genauem Hinschauen.
