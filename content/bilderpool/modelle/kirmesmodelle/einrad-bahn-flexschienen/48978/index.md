---
layout: "image"
title: "Der Wagen (2)"
date: 2021-02-26T21:31:02+01:00
picture: "einradbahnflexschienen04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die vielen kleinen roten Teile werden gebraucht, damit der Wagen sicher durch alle Kurven und Weichen kommt und damit der Wagen schön senkrecht im Gleichgewicht hängt.