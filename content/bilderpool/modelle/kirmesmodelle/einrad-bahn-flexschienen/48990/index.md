---
layout: "image"
title: "Kurzes Schienenstück"
date: 2021-02-26T21:31:16+01:00
picture: "einradbahnflexschienen26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der langen Strecke sitzt das kurze blaue Schienenstück, damit alles aufgeht.