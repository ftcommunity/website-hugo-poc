---
layout: "image"
title: "Bergfahrt einer Brücke"
date: 2021-02-26T21:31:36+01:00
picture: "einradbahnflexschienen10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Über Flachträger mit Bogenstücken 30° darin geht es weiter auf die erste Überführung.