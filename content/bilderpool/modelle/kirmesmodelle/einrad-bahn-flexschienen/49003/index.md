---
layout: "image"
title: "Stellung zur langen Strecke"
date: 2021-02-26T21:31:31+01:00
picture: "einradbahnflexschienen14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Stellung, die den Wagen auf die lange Außenstrecke führt.