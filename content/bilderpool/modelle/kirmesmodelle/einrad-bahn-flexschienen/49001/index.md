---
layout: "image"
title: "Befestigung des Aluprofils"
date: 2021-02-26T21:31:29+01:00
picture: "einradbahnflexschienen16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier wimmelt es von Grundbausteinen, denn das Aluprofil nimmt große Kräfte auf, wenn der Wagen an seinem vorderen Ende drauffährt.