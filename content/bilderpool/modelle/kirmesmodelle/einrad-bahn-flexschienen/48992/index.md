---
layout: "image"
title: "Verkürzung der kurzen Strecke (2)"
date: 2021-02-26T21:31:18+01:00
picture: "einradbahnflexschienen24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der zweite Teil der S-Kurve, der in die zweite Weiche führt.