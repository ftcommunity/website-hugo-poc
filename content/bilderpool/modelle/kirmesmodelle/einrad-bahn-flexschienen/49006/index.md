---
layout: "image"
title: "Talfahrt nach der ersten Brücke"
date: 2021-02-26T21:31:35+01:00
picture: "einradbahnflexschienen11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier geht's wieder runter. Im Vordergrund sieht man die beiden E-Tecs - rechts das Monoflop (das von der Lichtschranke angesteuert wird), links das Flipflop (das die Brückenmotoren umpolt).