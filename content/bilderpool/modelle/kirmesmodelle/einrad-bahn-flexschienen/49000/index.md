---
layout: "image"
title: "Antrieb der waagerechten Weiche"
date: 2021-02-26T21:31:28+01:00
picture: "einradbahnflexschienen17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vier Vorstuferäder 23 gibt es hier: Eines unten (links zu sehen), eines oben (hinten), eines links und eines rechts. So wird die lange Stange in allen Richtungen von Rädern geführt, in die Kräfte auftreten. Der XS-Motor sitzt auf einem BS15 mit zwei Zapfen zwischen den senkrechten Bausteingruppen. Der Winkelstein am rechten Endlagentaster sorgt dafür, dass sich die Schubstange nicht verhakt.