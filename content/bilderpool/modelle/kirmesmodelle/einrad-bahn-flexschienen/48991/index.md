---
layout: "image"
title: "Kurve der langen Strecke"
date: 2021-02-26T21:31:17+01:00
picture: "einradbahnflexschienen25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der langen Strecke kommt der Wagen von rechts aus dem Hintergrund den Hügel hoch und biegt ab. Unten sieht man die Lichtschranke für die Weichensteuerung.