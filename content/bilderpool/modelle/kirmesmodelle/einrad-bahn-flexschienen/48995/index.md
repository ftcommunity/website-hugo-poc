---
layout: "image"
title: "Kurve hinter der Weiche"
date: 2021-02-26T21:31:22+01:00
picture: "einradbahnflexschienen21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die 45°-Verstärkung hier geht genau auf: WS15°, WS30°, BS5, BS15, BS30, WS30°, WS15°.