---
layout: "image"
title: "Gesamtansicht (2)"
date: 2021-02-26T21:31:24+01:00
picture: "einradbahnflexschienen02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Jede Durchfahrt wird von einer Lichtschranke detektiert. Die geht sicherheitshalber auf ein Mono-Flop und das auf ein Flip-Flop (beides E-Tec-Module). Das Flip-Flop schaltet beide Weichen auf eine passende Position, wobei die Endlagentaster mit Dioden versehen wurden, damit jeweils zwei Zuleitungen je Weiche genügen.