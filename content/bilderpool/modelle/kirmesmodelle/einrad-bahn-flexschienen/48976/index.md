---
layout: "image"
title: "Der Wagen (4)"
date: 2021-02-26T21:30:59+01:00
picture: "einradbahnflexschienen06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man ganz gut, wieviel Platz für die unter dem Rad laufende Schiene ist.