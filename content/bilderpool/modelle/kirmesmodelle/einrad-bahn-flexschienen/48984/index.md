---
layout: "image"
title: "Antrieb der senkrechten Weiche"
date: 2021-02-26T21:31:09+01:00
picture: "einradbahnflexschienen31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser XS-Motor hat einfach einen Faden auf seiner Abtriebsachse eingeklemmt. Der wird an dem oben sichtbaren BS5 fixiert.