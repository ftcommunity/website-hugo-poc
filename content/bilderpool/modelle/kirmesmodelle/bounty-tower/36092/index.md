---
layout: "image"
title: "06 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower06.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36092
- /detailsf7d5.html
imported:
- "2019"
_4images_image_id: "36092"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36092 -->
Ja, ein 50:1 Powermotor ist in der Tat zu schwach, die Gondel in einem akzeptablen Tempo nach oben zu ziehen. Zwei Motoren müssen es schon sein.

Trotzdem dauert es durch die (notwendigen!) Untersetzungen und Flaschenzüge immer noch sehr lange, bis die Gondel oben ist.