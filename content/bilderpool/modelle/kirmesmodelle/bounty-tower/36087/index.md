---
layout: "image"
title: "01 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36087
- /detailsbf4f.html
imported:
- "2019"
_4images_image_id: "36087"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36087 -->
Das ist mein Versuch, das Fahrgeschäft "Bounty Tower" aus dem Holiday Park bei Haßloch nachzubauen. Das Modell kommt zwar nicht ganz an das Original ran, ich glaube aber dafür sind mir meine Teile auch dankbar, denn die Belastung ist durch das Gewicht schon sehr hoch.

Einfach mal nach Bounty Tower googlen, um sich ein Bild vom Original zu verschaffen.

Video: http://www.youtube.com/watch?v=vbnUDZFNeU4