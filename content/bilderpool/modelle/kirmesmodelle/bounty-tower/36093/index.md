---
layout: "image"
title: "07 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36093
- /details4887.html
imported:
- "2019"
_4images_image_id: "36093"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36093 -->
Im Original zwar nicht so, doch auch ich musste im Modell die Motoren der Gondel mit Strom versorgen. 

Ich habe mich für ein Kabel entschieden, das voll automatisch auf einer Trommel aufgewickelt wird, wenn die Gondel nach oben fährt.
Kann man gut im Video sehen (am Ende): http://www.youtube.com/watch?v=vbnUDZFNeU4

Links das Steuerpult.