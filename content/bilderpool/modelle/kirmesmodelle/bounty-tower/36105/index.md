---
layout: "image"
title: "19 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower19.jpg"
weight: "19"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36105
- /details5ddf.html
imported:
- "2019"
_4images_image_id: "36105"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36105 -->
So wird der Strom auf den Drehkranz übertragen, damit sich die Gondeln auch drehen können. 

Funktioniert leider nicht zu 100%, es ruckelt des Öfteren.