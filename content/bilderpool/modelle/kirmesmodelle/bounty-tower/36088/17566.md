---
layout: "comment"
hidden: true
title: "17566"
date: "2012-11-21T15:11:26"
uploadBy:
- "tobs9578"
license: "unknown"
imported:
- "2019"
---
Hallo,

der Vorschlag mit den Minimotoren ist denke ich schon umsetzbar.
In meinem letzten Modell wurde eine Gondel nur von einem Minimotor gedreht. http://www.ftcommunity.de/details.php?image_id=33368
Hatte keine Probleme.

Gruß Tobias