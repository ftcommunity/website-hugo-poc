---
layout: "image"
title: "11 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36097
- /details6990.html
imported:
- "2019"
_4images_image_id: "36097"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36097 -->
Das hier ist ein Teil des Seilmanagment. Das blaue Seil wird von den Motoren aufgewickelt, wodurch dieses Teil nach unten fährt. Dadurch wird das weiße Seil in den Turm gezogen und der Gondel bleibt nichts anderes übrig, als sich nach oben zu bewegen. 

Außer die Gondel überlegt es sich anders und das Seil reißt, wie es auch während den Aufnahmen passiert ist :(
Leider ist auch das blaue ft-Seil nicht unreißbar...