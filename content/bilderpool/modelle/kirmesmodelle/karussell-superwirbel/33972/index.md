---
layout: "image"
title: "Karussell SuperWirbel"
date: "2012-01-17T19:05:36"
picture: "karussellsuperwirbel2.jpg"
weight: "2"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33972
- /details751e-2.html
imported:
- "2019"
_4images_image_id: "33972"
_4images_cat_id: "2514"
_4images_user_id: "1361"
_4images_image_date: "2012-01-17T19:05:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33972 -->
Durch einen Exzenter werden die einzelnen "Wirbel" bei jeder Umdrehung einmal schräggestellt und in die Höhe gefahren.