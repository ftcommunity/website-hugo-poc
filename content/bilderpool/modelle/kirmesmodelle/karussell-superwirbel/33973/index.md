---
layout: "image"
title: "Karussell SuperWirbel"
date: "2012-01-17T19:05:36"
picture: "karussellsuperwirbel3.jpg"
weight: "3"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33973
- /details95fe.html
imported:
- "2019"
_4images_image_id: "33973"
_4images_cat_id: "2514"
_4images_user_id: "1361"
_4images_image_date: "2012-01-17T19:05:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33973 -->
Blick auf einen der vier "Wirbel".