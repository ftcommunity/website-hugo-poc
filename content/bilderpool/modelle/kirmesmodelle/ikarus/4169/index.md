---
layout: "image"
title: "Eine Seitenstütze"
date: "2005-05-20T20:10:10"
picture: "Modell_Ikarus_38.jpg"
weight: "20"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4169
- /detailsa791.html
imported:
- "2019"
_4images_image_id: "4169"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4169 -->
Das Bild zeigt eine von zwei Seitenstützen. Die roten Drehplatten werden noch gegen eckige Stützen(Pyramiden) ausgewechselt. Die Stützen selber muss ich aber doch noch verstärken.