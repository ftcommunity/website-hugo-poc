---
layout: "image"
title: "Modell Ikarus"
date: "2005-04-25T12:45:25"
picture: "Modell_Ikarus_34.jpg"
weight: "16"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4077
- /detailsea82-2.html
imported:
- "2019"
_4images_image_id: "4077"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-25T12:45:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4077 -->
Der Ring wird erst dann stabil wenn er ganz geschlossen ist. Im Moment ist er noch sehr wackelig.