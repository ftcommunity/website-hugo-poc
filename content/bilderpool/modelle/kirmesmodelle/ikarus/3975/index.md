---
layout: "image"
title: "Der Drehkranz vom Ikarusmodell von unten gesehen"
date: "2005-04-16T19:04:32"
picture: "Modell_Ikarus16.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/3975
- /details088e.html
imported:
- "2019"
_4images_image_id: "3975"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T19:04:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3975 -->
Hier nunmal der eigentliche Drehkranz. Das drehbare Teil ist auf diesem Bild schlecht zu erkennen. Hier sieht man nur die Unterseite auf der von innen die Rollen laufen.