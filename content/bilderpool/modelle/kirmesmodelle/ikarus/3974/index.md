---
layout: "image"
title: "Die Säule vom Ikarusmodell"
date: "2005-04-16T19:00:51"
picture: "Modell_Ikarus13.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/3974
- /detailsaa6f-3.html
imported:
- "2019"
_4images_image_id: "3974"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T19:00:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3974 -->
Das ist schonmal die Säule von meinem Ikarusmodell. An ihr soll dann der Drehkranz mit den vier Auslegern und den daran befestigten Gondelkreis rauf- bzw runterfahren und sich dabei noch drehen. Evtl wird sie noch etwas höher weil der Drehkranz ziemlich mächtig geworden ist.