---
layout: "image"
title: "Der Drehkranz von der Seite"
date: "2005-04-24T00:01:47"
picture: "Modell_Ikarus_26.jpg"
weight: "9"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4070
- /detailsb589.html
imported:
- "2019"
_4images_image_id: "4070"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-24T00:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4070 -->
Hier nun noch eine Seitenansicht! Es geht langsam weiter aufwärts.