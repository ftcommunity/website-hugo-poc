---
layout: "image"
title: "Modell Ikarus"
date: "2005-04-25T10:58:12"
picture: "Modell_Ikarus_31.jpg"
weight: "13"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4074
- /details4310-2.html
imported:
- "2019"
_4images_image_id: "4074"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-25T10:58:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4074 -->
Nochmal das gleiche Bild nur aus einer anderen Position.