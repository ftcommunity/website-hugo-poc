---
layout: "image"
title: "Die Stütze auf einer Pyramide"
date: "2005-05-21T18:42:07"
picture: "Modell_Ikarus_44.jpg"
weight: "24"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/4173
- /details90ed-2.html
imported:
- "2019"
_4images_image_id: "4173"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-21T18:42:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4173 -->
Die verstärkte Stütze auf einer Pyramide.