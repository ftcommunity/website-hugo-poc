---
layout: "image"
title: "Draufsicht auf eine Säule"
date: "2006-04-03T20:05:10"
picture: "Sule_vom_ME.jpg"
weight: "4"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6018
- /details6a16.html
imported:
- "2019"
_4images_image_id: "6018"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-03T20:05:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6018 -->
