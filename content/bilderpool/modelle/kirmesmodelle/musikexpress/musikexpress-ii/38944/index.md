---
layout: "image"
title: "Gondel Prototyp"
date: "2014-06-12T13:24:35"
picture: "musikexpressii09.jpg"
weight: "9"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38944
- /details3a61.html
imported:
- "2019"
_4images_image_id: "38944"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38944 -->
Das war einfach mal ein Versuch eine Gondel bzw. Chaise zu basteln die in etwa wie das Original aussieht. Ob ich die später verwende weiß ich noch nicht.