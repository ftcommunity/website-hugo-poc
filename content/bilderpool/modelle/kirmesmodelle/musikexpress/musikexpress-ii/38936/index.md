---
layout: "image"
title: "Mittelbau"
date: "2014-06-12T13:24:35"
picture: "musikexpressii01.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38936
- /details255f.html
imported:
- "2019"
_4images_image_id: "38936"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38936 -->
Die Kiste mit S-Riegeln wird nicht mit eingebaut. :-)