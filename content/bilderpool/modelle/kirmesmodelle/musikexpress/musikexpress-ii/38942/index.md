---
layout: "image"
title: "Musikexpress II Blick von rechts aufs Modell"
date: "2014-06-12T13:24:35"
picture: "musikexpressii07.jpg"
weight: "7"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38942
- /detailsf9ec.html
imported:
- "2019"
_4images_image_id: "38942"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38942 -->
Rechts kommen auch noch oben Platten dran. Muss den Dachrahmen aber vorne doch wohl abstützhen. Er verdreht sich nämlich, das kann man auf nem anderen Foto gut erkennen.