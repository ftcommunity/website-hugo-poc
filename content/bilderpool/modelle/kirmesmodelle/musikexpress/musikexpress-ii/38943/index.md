---
layout: "image"
title: "Gondel Draufsicht"
date: "2014-06-12T13:24:35"
picture: "musikexpressii08.jpg"
weight: "8"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38943
- /detailsa1b6.html
imported:
- "2019"
_4images_image_id: "38943"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38943 -->
