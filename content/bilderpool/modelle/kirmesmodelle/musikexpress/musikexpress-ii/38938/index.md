---
layout: "image"
title: "Drehkranz"
date: "2014-06-12T13:24:35"
picture: "musikexpressii03.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38938
- /detailsb8e7.html
imported:
- "2019"
_4images_image_id: "38938"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38938 -->
An dem Drehkranz kommen später die Ausleger dran. Wollte die erst mit Achsen machen aber das funktioniert nicht weil die Achsen steif sind und keine gescheite Berg- und Talfahrt möglich ist, höchstens mit nem Gelenk dazwischen.