---
layout: "image"
title: "Der Antrieb von schräg hinten"
date: "2014-07-03T22:30:20"
picture: "meii2.jpg"
weight: "13"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38993
- /detailsa1d5.html
imported:
- "2019"
_4images_image_id: "38993"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-07-03T22:30:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38993 -->
Weil der Drehkranz geneigt ist muss der Antrieb natürlich auch geneigt sein.