---
layout: "image"
title: "Der Antrieb von der Seite"
date: "2014-07-03T22:30:20"
picture: "meii3.jpg"
weight: "14"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/38994
- /details6f36.html
imported:
- "2019"
_4images_image_id: "38994"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-07-03T22:30:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38994 -->
Unter dem Z40 liegen 4 von den kleinen Zahnscheiben um die Höhe anzupassen. Die Kette geht ganz knapp über die Zapfen drüber weg. Anders herum hätte das nicht gepasst. Die wär dann unten angestossen.