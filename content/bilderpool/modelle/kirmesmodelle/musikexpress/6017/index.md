---
layout: "image"
title: "rechte innere Säule"
date: "2006-04-03T20:05:10"
picture: "Rechte_innere_Sule_vom_ME.jpg"
weight: "3"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6017
- /detailsbb6d.html
imported:
- "2019"
_4images_image_id: "6017"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-03T20:05:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6017 -->
Die innere Säule hatte ich zuerst auch aus den roten Schienen gebaut aber das war dann doch zu wackelig.