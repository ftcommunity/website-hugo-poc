---
layout: "comment"
hidden: true
title: "14948"
date: "2011-08-27T10:57:46"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Wollte den Dachstuhl erst zusammen klappbar machen aber das wäre dann zuviel Aufwand beim Aufbau gewesen. Hab mich dann doch für die feste Version entschieden. Die runden "Pfosten" hab ich später durch eckige ersetzt, aus Stabilitätsgründen. Das Dach ist schwerer geworden als erst angenommen und die runde Variante war zu instabil.
Leider ist das Modell damals beim einladen ins Auto, wegen Umzug, in mehrere Teile zerbröselt und ich hätte es hier komplett neu aufbauen müssen, wozu mir damals die Zeit fehlte. Könnte mir aber jetzt gut vorstellen den ME nochmal aufzubauen. Hab ja inzwischen noch ein paar Teile dazu bekommen. Größer würde ich den nicht bauen. Wird sonst zu wuchtig und zu schwer.