---
layout: "image"
title: "Fast fertig, aber nur fast."
date: "2006-09-14T23:22:17"
picture: "ME_08.jpg"
weight: "12"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/6798
- /detailsfcf8-2.html
imported:
- "2019"
_4images_image_id: "6798"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-09-14T23:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6798 -->
Jetzt muss ich nur noch die Dachteile verkleiden. Das hängt ganz schön nach vorne runter.