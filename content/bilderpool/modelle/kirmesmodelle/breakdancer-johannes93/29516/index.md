---
layout: "image"
title: "Kassenhäuschen"
date: "2010-12-23T15:37:40"
picture: "breakdancer10.jpg"
weight: "10"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/29516
- /details0320.html
imported:
- "2019"
_4images_image_id: "29516"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29516 -->
