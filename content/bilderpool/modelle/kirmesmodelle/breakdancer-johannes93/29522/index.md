---
layout: "image"
title: "Blick unter den Aufbau"
date: "2010-12-23T15:37:40"
picture: "breakdancer16.jpg"
weight: "16"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/29522
- /details80e8-2.html
imported:
- "2019"
_4images_image_id: "29522"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29522 -->
