---
layout: "image"
title: "Sitzplätze"
date: "2010-12-23T15:37:40"
picture: "breakdancer07.jpg"
weight: "7"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/29513
- /details530c.html
imported:
- "2019"
_4images_image_id: "29513"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29513 -->
