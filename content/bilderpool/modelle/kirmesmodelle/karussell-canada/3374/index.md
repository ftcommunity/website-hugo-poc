---
layout: "image"
title: "Karussell 007"
date: "2004-12-05T10:21:16"
picture: "Karussell_007.JPG"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3374
- /detailsf402.html
imported:
- "2019"
_4images_image_id: "3374"
_4images_cat_id: "293"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:21:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3374 -->
