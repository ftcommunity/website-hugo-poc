---
layout: "image"
title: "Karussell 014"
date: "2004-12-05T10:21:16"
picture: "Karussell_014.JPG"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3381
- /details74d8.html
imported:
- "2019"
_4images_image_id: "3381"
_4images_cat_id: "293"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:21:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3381 -->
