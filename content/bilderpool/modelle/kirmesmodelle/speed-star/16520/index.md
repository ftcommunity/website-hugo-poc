---
layout: "image"
title: "Speed - Star"
date: "2008-11-28T23:04:42"
picture: "BILD0420.jpg"
weight: "2"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
schlagworte: ["Speed-Star"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16520
- /detailsf07f-2.html
imported:
- "2019"
_4images_image_id: "16520"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:04:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16520 -->
Hier seht ihr das komplette modell.
