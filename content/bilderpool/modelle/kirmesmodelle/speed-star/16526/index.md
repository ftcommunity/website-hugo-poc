---
layout: "image"
title: "Motor"
date: "2008-11-28T23:04:42"
picture: "BILD0428.jpg"
weight: "8"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
schlagworte: ["Speed-Star"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16526
- /details022d-2.html
imported:
- "2019"
_4images_image_id: "16526"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:04:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16526 -->
