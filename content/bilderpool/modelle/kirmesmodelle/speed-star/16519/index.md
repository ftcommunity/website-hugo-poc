---
layout: "image"
title: "v"
date: "2008-11-28T23:04:42"
picture: "BILD0419.jpg"
weight: "1"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16519
- /detailsa61d.html
imported:
- "2019"
_4images_image_id: "16519"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:04:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16519 -->
