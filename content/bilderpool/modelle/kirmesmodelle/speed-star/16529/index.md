---
layout: "image"
title: "Speed - Star in Action"
date: "2008-11-28T23:12:36"
picture: "BILD0431.jpg"
weight: "11"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
schlagworte: ["Speed-Star"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- /php/details/16529
- /detailsf713-2.html
imported:
- "2019"
_4images_image_id: "16529"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:12:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16529 -->
Die Seile stabilisieren den Ausleger zusätzlich
