---
layout: "image"
title: "Die ersten Masten"
date: "2012-04-25T12:02:58"
picture: "modellartistico1.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34820
- /details03dd.html
imported:
- "2019"
_4images_image_id: "34820"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-04-25T12:02:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34820 -->
Die waren etwas zu instabil und ich hatte auch nicht genug von den U - Trägern. 
Das etwas kürzere rechteckige Teil ist der Schwingarm an dem später unten die Ausleger mit den Gondeln dran kommt.