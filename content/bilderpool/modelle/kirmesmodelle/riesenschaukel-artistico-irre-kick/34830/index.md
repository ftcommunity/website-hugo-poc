---
layout: "image"
title: "Die ersten beiden neuen Masten sind fertig"
date: "2012-04-29T13:34:18"
picture: "IMG_5369a.jpg"
weight: "5"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34830
- /details4bb5-2.html
imported:
- "2019"
_4images_image_id: "34830"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-04-29T13:34:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34830 -->
Das untere Ende eines Mastens. Werde den Aufnahmehalter auf der Platte wohl etwas verbreitern damit auf der einen Seite noch ein "Lagerbock" (BS15 mit Loch) hinpasst.