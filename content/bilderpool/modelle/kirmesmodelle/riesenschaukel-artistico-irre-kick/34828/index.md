---
layout: "image"
title: "Die ersten beiden neuen Masten sind fertig"
date: "2012-04-29T13:34:18"
picture: "IMG_5366a.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34828
- /detailsc078.html
imported:
- "2019"
_4images_image_id: "34828"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-04-29T13:34:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34828 -->
Diese Ausführung ist wesentlich stabiler als die erste Version. Auf der einen Seite hab ich die Bauplatten 90x180 verbaut und gegenüber dann die Statikplatten in der gleichen Größe. Oben werden sie jetzt verbunden und dann kommt da der Antrieb für den Schwingarm hin. Das einzige was mich noch nervt ist das die normalen Grundbausteine immer aus den Winkelsteinen bzw. Statikscharnieren rausrutschen. Irgendwie mus ich das noch ändern. Weiß zwar noch nicht genau wie aber da fällt mir schon noch was zu ein. Für Tipps bin ich aber immer dankbar.