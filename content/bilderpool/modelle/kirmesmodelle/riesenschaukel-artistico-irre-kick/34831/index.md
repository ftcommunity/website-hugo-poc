---
layout: "image"
title: "Die ersten beiden neuen Masten sind fertig"
date: "2012-04-29T13:34:18"
picture: "IMG_5371a.jpg"
weight: "6"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34831
- /detailsa43a.html
imported:
- "2019"
_4images_image_id: "34831"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-04-29T13:34:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34831 -->
So sehen die aus wenn die transportiert werden müssen. Beim Original klappen die Masten an einer Seite in der Mitte zusammen und liegen dann unter dem einen langen Mast.