---
layout: "image"
title: "Das Innere eines Mastens"
date: "2012-12-17T08:28:09"
picture: "artistico6.jpg"
weight: "12"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/36322
- /details7abd.html
imported:
- "2019"
_4images_image_id: "36322"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-12-17T08:28:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36322 -->
So sieht es im innern eines Mastens aus.