---
layout: "image"
title: "Gesamtansicht"
date: "2010-09-27T23:33:13"
picture: "updowntowerversion01.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28644
- /details1a10.html
imported:
- "2019"
_4images_image_id: "28644"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28644 -->
Hier seht ihr eine Gesamtansicht meines neuen Modells. In diesem Zustand exestiert noch keine Elektronik, doch es ist stabiel gebaut. Die Türme sind leider etwas dünn und deswegen besteht Umsturzgefahr!
