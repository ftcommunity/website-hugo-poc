---
layout: "image"
title: "Station2"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion15.jpg"
weight: "12"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28655
- /detailse892-2.html
imported:
- "2019"
_4images_image_id: "28655"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28655 -->
Hier ist die Station von vorne.
