---
layout: "image"
title: "Turmende"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion12.jpg"
weight: "10"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28653
- /details9b25.html
imported:
- "2019"
_4images_image_id: "28653"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28653 -->
Hier sieht man das Ende des kurzen Turms. Falls der Wagen zu hoch kommt verhindern die Federn den Aufprall.
