---
layout: "overview"
title: "UP & DOWN Tower Version1"
date: 2020-02-22T07:59:57+01:00
legacy_id:
- /php/categories/2071
- /categoriesda02.html
- /categories3fc2-2.html
- /categories5a54.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2071 --> 
Der kleine Zug fällt von oben runter und schwingt dann unten hin und her.
Es sind zwar nicht die schönsten Bilder, doch Ich hoffe sie gefallen euch trotzdem.

WICHTIG: Das ist die alte Version nicht die von der Convention! Bilder der aktuellen Version folgen in kürze!