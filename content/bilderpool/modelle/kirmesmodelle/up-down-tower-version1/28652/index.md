---
layout: "image"
title: "Verstrebung3"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion09.jpg"
weight: "9"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28652
- /details3812.html
imported:
- "2019"
_4images_image_id: "28652"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28652 -->
Hier ist eine Detail-Aufnahme der Verstrebung. Im oberen Teil ist auch die Abmontage-Plattform zuerkennen.
