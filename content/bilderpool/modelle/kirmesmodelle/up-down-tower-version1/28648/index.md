---
layout: "image"
title: "Hauptteil3"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion05.jpg"
weight: "5"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28648
- /detailsbb24.html
imported:
- "2019"
_4images_image_id: "28648"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28648 -->
Hier ist der Hauptteil meines Turms. Dieser Teil ist noch relativ stabiel gebaut mit vielen Verstrebungen. Diese seht ihr auch noch in den nächsten Bildern.
