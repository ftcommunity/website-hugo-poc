---
layout: "image"
title: "Verstrebung"
date: "2010-09-27T23:33:14"
picture: "updowntowerversion07.jpg"
weight: "7"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/28650
- /detailsf374.html
imported:
- "2019"
_4images_image_id: "28650"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28650 -->
Hier ist eine Detail-Aufnahme der Verstrebung.
