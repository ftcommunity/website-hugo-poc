---
layout: "image"
title: "Die KRAKE - Gondel im Detail"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster24.jpg"
weight: "24"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42920
- /details620b.html
imported:
- "2019"
_4images_image_id: "42920"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42920 -->
Inzwischen wurden die Sitze durch Schalensitze ausgetauscht und schon Sicherungsbügel angebracht
