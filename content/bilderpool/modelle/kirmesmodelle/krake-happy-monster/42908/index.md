---
layout: "image"
title: "Die KRAKE - Aufzugsmotor an der Banane"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster12.jpg"
weight: "12"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42908
- /details19ef-2.html
imported:
- "2019"
_4images_image_id: "42908"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42908 -->
