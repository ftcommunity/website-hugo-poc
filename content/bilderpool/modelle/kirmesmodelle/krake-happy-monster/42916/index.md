---
layout: "image"
title: "Die KRAKE - Aufzug Mitte + Aufhängung der Arme"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster20.jpg"
weight: "20"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42916
- /details2849.html
imported:
- "2019"
_4images_image_id: "42916"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42916 -->
