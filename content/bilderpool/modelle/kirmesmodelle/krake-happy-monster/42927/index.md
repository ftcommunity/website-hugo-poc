---
layout: "image"
title: "DIE KRAKE - von schräg oben - 'Happy Monster'"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster31.jpg"
weight: "31"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42927
- /details503f.html
imported:
- "2019"
_4images_image_id: "42927"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42927 -->
hier schonmal die ersten Bilder meines aktuellen Bauprojektes - voll funktionsfähiges Modell der bekannten "DIE KRAKE", auch unterwegs als "Polyp" oder "Happy Monster".
ein vielleicht etwas "betagtes" Fahrgeschäft, welches immer noch oft zu sehen ist und auch gut besucht wird. Mechanisch ist das Ganze schon teilweise kniffelig in ft umzusetzen - definitiv sind noch nicht alle Probleme gelöst, aber ich denke, bis zur convention sollte es im großen und Ganzen "rund" laufen. Vieles wurde schon gebaut aber nicht fotografiert - man darf gespannt sein ;-)
