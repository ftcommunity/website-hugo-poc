---
layout: "image"
title: "Die KRAKE - Banane - Aufzug"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster09.jpg"
weight: "9"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42905
- /details3503.html
imported:
- "2019"
_4images_image_id: "42905"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42905 -->
hier sieht man die Seilführung, um den Schlitten bzw Drehkranz an der Banane hinaufzuziehen
