---
layout: "image"
title: "Die KRAKE - Drehkreuz ohne Gondeln"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster19.jpg"
weight: "19"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42915
- /details9156.html
imported:
- "2019"
_4images_image_id: "42915"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42915 -->
