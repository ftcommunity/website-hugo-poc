---
layout: "image"
title: "Die KRAKE die Mitte von der Seite"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster05.jpg"
weight: "5"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42901
- /details65d0.html
imported:
- "2019"
_4images_image_id: "42901"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42901 -->
