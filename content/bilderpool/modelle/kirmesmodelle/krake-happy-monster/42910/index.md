---
layout: "image"
title: "Die KRAKE - Aufzug seitlich"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster14.jpg"
weight: "14"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42910
- /details871e-2.html
imported:
- "2019"
_4images_image_id: "42910"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42910 -->
