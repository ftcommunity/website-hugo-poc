---
layout: "image"
title: "Die KRAKE - Lager der Drehkreuze"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster25.jpg"
weight: "25"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42921
- /detailsab54-2.html
imported:
- "2019"
_4images_image_id: "42921"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42921 -->
hier werden ggf. noch Kugellager eingesetzt
