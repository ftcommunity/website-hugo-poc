---
layout: "image"
title: "die KRAKE von weitem"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster28.jpg"
weight: "28"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42924
- /detailsf18d-2.html
imported:
- "2019"
_4images_image_id: "42924"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42924 -->
