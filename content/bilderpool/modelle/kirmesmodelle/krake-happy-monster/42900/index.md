---
layout: "image"
title: "Die KRAKE Mittelbau von der Seite"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster04.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42900
- /details4959.html
imported:
- "2019"
_4images_image_id: "42900"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42900 -->
