---
layout: "image"
title: "Transportwagen und Kran"
date: "2008-08-05T13:42:49"
picture: "evolution01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14995
- /details9cd8.html
imported:
- "2019"
_4images_image_id: "14995"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14995 -->
Auf dem Transportwagen befindet sich Evolution.
