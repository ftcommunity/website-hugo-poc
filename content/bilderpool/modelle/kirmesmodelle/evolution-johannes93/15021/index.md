---
layout: "image"
title: "Kassenhäuschen"
date: "2008-08-05T13:42:51"
picture: "evolution27.jpg"
weight: "27"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15021
- /details3d5f.html
imported:
- "2019"
_4images_image_id: "15021"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:51"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15021 -->
Im Kassenhäuschen befindet sich das Netzteil.
