---
layout: "image"
title: "Montage der Drehscheibe"
date: "2008-08-05T13:42:50"
picture: "evolution20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15014
- /detailse57f-2.html
imported:
- "2019"
_4images_image_id: "15014"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15014 -->
