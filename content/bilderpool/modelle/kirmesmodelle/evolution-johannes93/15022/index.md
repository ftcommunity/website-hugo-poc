---
layout: "image"
title: "Kassenhäuschen am Kranhaken"
date: "2008-08-05T13:42:51"
picture: "evolution28.jpg"
weight: "28"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15022
- /details7efa.html
imported:
- "2019"
_4images_image_id: "15022"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:51"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15022 -->
