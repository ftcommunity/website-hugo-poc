---
layout: "image"
title: "Antrieb Drehscheibe"
date: "2008-08-05T13:42:51"
picture: "evolution39.jpg"
weight: "39"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15033
- /detailsc991.html
imported:
- "2019"
_4images_image_id: "15033"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:51"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15033 -->
Ein Powermotor mit schwarzer Kappe treibt die Drehscheibe an.
