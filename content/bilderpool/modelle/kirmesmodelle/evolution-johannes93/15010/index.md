---
layout: "image"
title: "Verbindung der beiden Stützen"
date: "2008-08-05T13:42:50"
picture: "evolution16.jpg"
weight: "16"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15010
- /details00fc.html
imported:
- "2019"
_4images_image_id: "15010"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15010 -->
