---
layout: "image"
title: "Transportwagen ohne Dach"
date: "2008-08-05T13:42:50"
picture: "evolution08.jpg"
weight: "8"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15002
- /detailscdb7-2.html
imported:
- "2019"
_4images_image_id: "15002"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15002 -->
