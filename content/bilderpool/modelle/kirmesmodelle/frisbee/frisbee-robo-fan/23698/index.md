---
layout: "image"
title: "Das Steuerpult"
date: "2009-04-13T14:50:49"
picture: "frisbee07.jpg"
weight: "7"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23698
- /details627e.html
imported:
- "2019"
_4images_image_id: "23698"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23698 -->
Am Anfang leuchtet die rote Lampe. Dann kann man uf die grüne Lampe drücken(darunter befindet sich ein Taster) Die roteLampe geht dabei aus und die grüne schaltet sich mit dem Modell ein. Für den Notstopp kann manauf die rote Lampe drücken(unter der befindet sich auch ein Taster) Dabei schaltet sich die grüne aus und die rote ein.