---
layout: "image"
title: "Polwendeschalter"
date: "2009-04-13T14:50:49"
picture: "frisbee08.jpg"
weight: "8"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23699
- /detailsfbf7.html
imported:
- "2019"
_4images_image_id: "23699"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23699 -->
Schaltet das Interface ein. Links unten befindet sich noch ein Schalter für die Bodenbeleuchtung.