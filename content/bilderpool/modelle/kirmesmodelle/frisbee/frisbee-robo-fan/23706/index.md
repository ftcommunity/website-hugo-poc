---
layout: "image"
title: "Blick von oben links"
date: "2009-04-13T14:50:56"
picture: "frisbee15.jpg"
weight: "15"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23706
- /detailse285.html
imported:
- "2019"
_4images_image_id: "23706"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:56"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23706 -->
