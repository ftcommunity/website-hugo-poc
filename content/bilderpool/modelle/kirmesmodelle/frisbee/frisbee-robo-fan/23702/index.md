---
layout: "image"
title: "Die Bodenbeleuchtung"
date: "2009-04-13T14:50:56"
picture: "frisbee11.jpg"
weight: "11"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23702
- /details1af6-2.html
imported:
- "2019"
_4images_image_id: "23702"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23702 -->
Bestehend aus zwei Linsenlampen.