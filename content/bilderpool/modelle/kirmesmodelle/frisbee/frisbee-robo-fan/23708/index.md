---
layout: "image"
title: "Blick von hinten oben"
date: "2009-04-13T14:50:56"
picture: "frisbee17.jpg"
weight: "17"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23708
- /details4d8f.html
imported:
- "2019"
_4images_image_id: "23708"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:56"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23708 -->
