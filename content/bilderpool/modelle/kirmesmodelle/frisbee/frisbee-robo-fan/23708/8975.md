---
layout: "comment"
hidden: true
title: "8975"
date: "2009-04-13T19:09:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr schönes Modell, aber ich glaube, der Motor, der das ganze Teil dreht (hier der nicht sichtbare) täte sich ganz erheblich leichter, wenn Du dem ganzen Arm ein geeignetes Gegengewicht spendieren würdest.

Gruß,
Stefan