---
layout: "image"
title: "Antrieb der Gondel"
date: "2009-04-13T14:50:49"
picture: "frisbee04.jpg"
weight: "4"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/23695
- /details503e.html
imported:
- "2019"
_4images_image_id: "23695"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23695 -->
Vom motor aus geht eine Stange durch den Arm runter zur Gondel