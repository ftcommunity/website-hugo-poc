---
layout: "image"
title: "Kabelgewirr im Inneren"
date: "2011-10-19T19:22:33"
picture: "frisbee6.jpg"
weight: "13"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33256
- /details0ce5.html
imported:
- "2019"
_4images_image_id: "33256"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:33"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33256 -->
Das Kabelgewirr werdet ihr später nicht mehr sehen,wo dieser Teil eingebaut ist.
