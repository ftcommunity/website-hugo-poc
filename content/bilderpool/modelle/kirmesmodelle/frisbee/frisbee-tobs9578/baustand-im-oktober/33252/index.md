---
layout: "image"
title: "frisbee2.jpg"
date: "2011-10-19T19:22:33"
picture: "frisbee2_3.jpg"
weight: "9"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33252
- /details4e84.html
imported:
- "2019"
_4images_image_id: "33252"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33252 -->
