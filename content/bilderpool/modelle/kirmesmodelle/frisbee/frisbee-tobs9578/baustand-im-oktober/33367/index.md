---
layout: "image"
title: "Stuhl 3"
date: "2011-10-30T11:32:57"
picture: "fri3_2.jpg"
weight: "19"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33367
- /detailsd464-2.html
imported:
- "2019"
_4images_image_id: "33367"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-30T11:32:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33367 -->
