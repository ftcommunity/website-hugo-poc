---
layout: "image"
title: "frisbee4.jpg"
date: "2011-10-19T19:22:33"
picture: "frisbee4_2.jpg"
weight: "11"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33254
- /detailsd8c0-2.html
imported:
- "2019"
_4images_image_id: "33254"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33254 -->
