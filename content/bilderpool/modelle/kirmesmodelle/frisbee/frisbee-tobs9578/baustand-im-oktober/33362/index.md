---
layout: "image"
title: "Ledleiste eingebaut"
date: "2011-10-29T19:13:33"
picture: "fri1.jpg"
weight: "14"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33362
- /details1131-3.html
imported:
- "2019"
_4images_image_id: "33362"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-29T19:13:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33362 -->
Hier die Led Leiste im eingebauten Zustand.
