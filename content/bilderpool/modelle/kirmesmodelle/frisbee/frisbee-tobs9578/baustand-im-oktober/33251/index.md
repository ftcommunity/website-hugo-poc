---
layout: "image"
title: "frisbee1.jpg"
date: "2011-10-19T19:22:32"
picture: "frisbee1_3.jpg"
weight: "8"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33251
- /details14f4.html
imported:
- "2019"
_4images_image_id: "33251"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33251 -->
