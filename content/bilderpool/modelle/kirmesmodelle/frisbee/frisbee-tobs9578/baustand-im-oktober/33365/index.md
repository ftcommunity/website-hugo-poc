---
layout: "image"
title: "Stuhl für Kassierer"
date: "2011-10-30T11:32:57"
picture: "fri1_2.jpg"
weight: "17"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33365
- /detailsac7d-2.html
imported:
- "2019"
_4images_image_id: "33365"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-30T11:32:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33365 -->
Kleiner Stuhl mit Armlene für Kassierer
