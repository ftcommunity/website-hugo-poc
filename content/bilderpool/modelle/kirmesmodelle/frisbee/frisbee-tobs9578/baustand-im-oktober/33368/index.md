---
layout: "image"
title: "Gesamtansicht"
date: "2011-10-30T15:10:41"
picture: "fri1_3.jpg"
weight: "20"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33368
- /detailsb5dd.html
imported:
- "2019"
_4images_image_id: "33368"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-30T15:10:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33368 -->
Hier mal eine Gesamtansicht zur besseren Vorstellung
