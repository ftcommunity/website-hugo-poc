---
layout: "image"
title: "Die Fahrgastbrücke"
date: "2011-10-19T14:25:45"
picture: "frisbee5.jpg"
weight: "7"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33231
- /detailsa1cd.html
imported:
- "2019"
_4images_image_id: "33231"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T14:25:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33231 -->
Kleine Brücke die mit Hilfe eines kleinen Zylinders hochgehoben wird, damit die Ft Männchen einsteigen können.
