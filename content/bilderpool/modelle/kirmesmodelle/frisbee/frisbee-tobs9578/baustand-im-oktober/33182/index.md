---
layout: "image"
title: "Fahrgasttreppe"
date: "2011-10-16T17:01:17"
picture: "frisbee1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33182
- /details1b99.html
imported:
- "2019"
_4images_image_id: "33182"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-16T17:01:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33182 -->
In der Fahrgasttreppe befinden sich bis jetzt 16 Leds. 8 rot und 8 gelb.
Auf der anderen Seite soll nochmal das Gleiche entstehen.
