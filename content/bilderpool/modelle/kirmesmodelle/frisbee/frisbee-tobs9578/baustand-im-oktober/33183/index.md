---
layout: "image"
title: "Fahrgasttreppe 2"
date: "2011-10-16T17:01:17"
picture: "frisbee2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33183
- /detailsfcfa.html
imported:
- "2019"
_4images_image_id: "33183"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-16T17:01:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33183 -->
Hier sieht man nun die gelben Leds.
