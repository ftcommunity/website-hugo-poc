---
layout: "image"
title: "frisbee3.jpg"
date: "2011-10-19T19:22:33"
picture: "frisbee3_2.jpg"
weight: "10"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/33253
- /details1733-2.html
imported:
- "2019"
_4images_image_id: "33253"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-19T19:22:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33253 -->
