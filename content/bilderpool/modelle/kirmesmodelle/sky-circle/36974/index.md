---
layout: "image"
title: "Steuerpult"
date: "2013-05-26T11:54:50"
picture: "IMG_4728.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/36974
- /detailsdaac.html
imported:
- "2019"
_4images_image_id: "36974"
_4images_cat_id: "2744"
_4images_user_id: "1631"
_4images_image_date: "2013-05-26T11:54:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36974 -->
Taster1. Licht An/Aus (efekte)
Taster2. Start der fahrt
Taster3. Treppe Heben/Senken
Taster4. Notaus
Übrigends habe ich für das Modell ein Programm mit robo-pro programiert, wo mir alles angezeigt wird.