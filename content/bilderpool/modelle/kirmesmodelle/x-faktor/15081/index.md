---
layout: "image"
title: "Großer Ring drehend"
date: "2008-08-24T16:00:36"
picture: "xfaktor03.jpg"
weight: "3"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15081
- /details5723.html
imported:
- "2019"
_4images_image_id: "15081"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15081 -->
Hier sieht man den großen Ring sich bei voller Geschwindigkeit drehen.