---
layout: "image"
title: "Kleiner Ring Schräg"
date: "2008-08-24T16:00:37"
picture: "xfaktor17.jpg"
weight: "17"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15095
- /detailsf35b.html
imported:
- "2019"
_4images_image_id: "15095"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15095 -->
