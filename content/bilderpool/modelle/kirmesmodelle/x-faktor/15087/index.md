---
layout: "image"
title: "Ring Senkrecht"
date: "2008-08-24T16:00:37"
picture: "xfaktor09.jpg"
weight: "9"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15087
- /details935f.html
imported:
- "2019"
_4images_image_id: "15087"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15087 -->
