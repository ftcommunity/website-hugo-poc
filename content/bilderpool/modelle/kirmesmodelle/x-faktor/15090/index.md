---
layout: "image"
title: "Ausgangsposition"
date: "2008-08-24T16:00:37"
picture: "xfaktor12.jpg"
weight: "12"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15090
- /detailsba87.html
imported:
- "2019"
_4images_image_id: "15090"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15090 -->
Waagerecht,die sicherste Position für die Figur ;-)