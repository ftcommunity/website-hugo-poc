---
layout: "image"
title: "Ring groß"
date: "2008-08-24T16:00:36"
picture: "xfaktor05.jpg"
weight: "5"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15083
- /details1623.html
imported:
- "2019"
_4images_image_id: "15083"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15083 -->
