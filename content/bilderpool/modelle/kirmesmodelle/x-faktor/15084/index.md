---
layout: "image"
title: "Antrieb Schwenkarm"
date: "2008-08-24T16:00:36"
picture: "xfaktor06.jpg"
weight: "6"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/15084
- /details6e01.html
imported:
- "2019"
_4images_image_id: "15084"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15084 -->
