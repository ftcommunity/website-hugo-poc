---
layout: "image"
title: "Antrieb des Oberteils"
date: "2008-07-15T22:17:21"
picture: "hullygully19.jpg"
weight: "19"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14857
- /details335d.html
imported:
- "2019"
_4images_image_id: "14857"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14857 -->
Der Antrieb des Oberteils erfolgt über einen Minimotor.
