---
layout: "image"
title: "Antrieb des Unterteils"
date: "2008-07-15T22:17:21"
picture: "hullygully18.jpg"
weight: "18"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14856
- /detailsc0e7.html
imported:
- "2019"
_4images_image_id: "14856"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14856 -->
Das Unterteil wird von einem Powermotor mit schwarzer Kappe über eine Schnecke angetrieben.
