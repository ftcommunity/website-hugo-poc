---
layout: "image"
title: "Karussell mit Fahrgästen"
date: "2008-07-15T22:17:06"
picture: "hullygully12.jpg"
weight: "12"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14850
- /details6626-2.html
imported:
- "2019"
_4images_image_id: "14850"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14850 -->
