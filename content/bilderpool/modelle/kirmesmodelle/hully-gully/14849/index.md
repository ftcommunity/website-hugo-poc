---
layout: "image"
title: "Kabine mit Fahrgast"
date: "2008-07-15T22:17:06"
picture: "hullygully11.jpg"
weight: "11"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14849
- /details6864-2.html
imported:
- "2019"
_4images_image_id: "14849"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14849 -->
Die Fahrgäste werden mit Bügeln vor dem Herausfallen geschützt.
