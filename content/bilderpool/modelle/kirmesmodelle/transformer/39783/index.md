---
layout: "image"
title: "Schleifring Brücke"
date: "2014-11-09T17:21:24"
picture: "IMG_0007.jpg"
weight: "19"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39783
- /detailse1f5-2.html
imported:
- "2019"
_4images_image_id: "39783"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39783 -->
es sind auf jeder Seite Antrieb und Schleifring vorhanden, damit steht genug Antriebsleistung zur Verfügung, weniger Torsion auf der Brücke und Beleuchtung und Motor der Zentralen Rotation sind damit unabhängig voneinander mit Strom zu versorgen.
