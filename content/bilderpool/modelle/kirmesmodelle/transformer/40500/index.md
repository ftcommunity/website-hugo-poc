---
layout: "image"
title: "ca 90 grad gedrehte Gesamtansicht"
date: "2015-02-08T12:54:27"
picture: "IMG_0056.jpg"
weight: "33"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40500
- /detailsfa36-2.html
imported:
- "2019"
_4images_image_id: "40500"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40500 -->
