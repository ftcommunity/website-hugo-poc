---
layout: "image"
title: "Drehgestell Antrieb"
date: "2014-12-31T17:27:48"
picture: "IMG_0070.jpg"
weight: "25"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40090
- /details93f2.html
imported:
- "2019"
_4images_image_id: "40090"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-12-31T17:27:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40090 -->
auf diesem bereits getetsteten Drehgestell wird der Transformer später komplett rotieren. ein Lasttest wurde erfolgreich bestanden. Der Kettenaufbau für den Antrieb am Statik-Kreis wurde in Anlehnung an den FTpedia-Beitrag 4/2014 / schreiender Wecker S.25ff von Andreas Gail weiter entwickelt.
