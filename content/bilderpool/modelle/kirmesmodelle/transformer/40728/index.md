---
layout: "image"
title: "Sockel mit Schleifring"
date: "2015-04-06T19:15:40"
picture: "IMG_0042.jpg"
weight: "64"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40728
- /details1ca8.html
imported:
- "2019"
_4images_image_id: "40728"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-04-06T19:15:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40728 -->
natürlich ist der schleifring selbst NICHT verklebt! - ganz normal verschraubt..
