---
layout: "image"
title: "Montage Schleifring"
date: "2015-04-06T19:15:40"
picture: "IMG_0039.jpg"
weight: "61"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40725
- /details15fc-2.html
imported:
- "2019"
_4images_image_id: "40725"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-04-06T19:15:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40725 -->
Ich musste leider diese 3 Teile mit 2k-Kleber fix verbinden, da ich immer wieder den zentralen Schleifring bzw. dessen Funktion verloren habe - ständig drehte er sich mit und damit wickelten sich die Kabel auf bzw. einer der beiden Stecker rutschte raus..
