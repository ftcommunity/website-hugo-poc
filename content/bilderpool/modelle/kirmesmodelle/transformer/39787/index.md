---
layout: "image"
title: "Antrieb und Schleifring von Oben"
date: "2014-11-09T17:21:24"
picture: "IMG_0015_2.jpg"
weight: "23"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39787
- /details5280.html
imported:
- "2019"
_4images_image_id: "39787"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39787 -->
