---
layout: "image"
title: "jetzt KUGELGELAGERT"
date: "2015-02-08T12:54:27"
picture: "IMG_0053.jpg"
weight: "34"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40501
- /details0344.html
imported:
- "2019"
_4images_image_id: "40501"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40501 -->
entegen der ursprünglichen Lösung habe ich jetzt den Drehkranz auf Basis des Kugellager-Prinzips gebaut. Ergebnis ist mehr Lauf-Ruhe, bessere Gewichts - Aufteilung, Stabilität
