---
layout: "image"
title: "Kugellager geöffnet ohe Drehkranz"
date: "2015-02-08T12:54:27"
picture: "IMG_0064_2.jpg"
weight: "39"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40506
- /details1acc-2.html
imported:
- "2019"
_4images_image_id: "40506"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40506 -->
ja, das sind VIELE Kugeln. ich habe einen günstigen Lieferanten gefunden. 400 Kugeln zu knapp 21¤ inkl Versand (die Kugeln haben Original Durchmesser 1/2" bzw. 12,7mm und sind von gleicher Qualität
