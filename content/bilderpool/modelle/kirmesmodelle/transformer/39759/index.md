---
layout: "image"
title: "Transformer Machbarkeitstest 1"
date: "2014-11-02T15:23:34"
picture: "IMG_0203.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Karussell", "Transformer"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39759
- /detailsffcc-2.html
imported:
- "2019"
_4images_image_id: "39759"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-02T15:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39759 -->
hier meine ersten Versuche, zahlreiche Details werden noch geändert, die 4. Achse ist noch nicht gebaut. (Gesamtrotation des Bügels)
