---
layout: "image"
title: "Spindelantrieb 'Brücke' heben und senken"
date: "2014-11-09T17:21:24"
picture: "IMG_0019.jpg"
weight: "8"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39772
- /details3afc.html
imported:
- "2019"
_4images_image_id: "39772"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39772 -->
Dieser Antrieb ist identisch auf beiden Seiten vorhanden. Die Endlageschalter "Unten" sind schon montiert, aber nicht angeschlossen. Die Endlageschalter "Oben" folgen noch.
