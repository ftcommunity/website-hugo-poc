---
layout: "image"
title: "Sleeveblock von hinten, neue Variante"
date: "2014-12-31T17:27:48"
picture: "IMG_0071.jpg"
weight: "24"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40089
- /details8480.html
imported:
- "2019"
_4images_image_id: "40089"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-12-31T17:27:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40089 -->
Ich musste die Sleeves nochmals anpassen, damit sie nicht kippeln und in sich stabiler sind.
