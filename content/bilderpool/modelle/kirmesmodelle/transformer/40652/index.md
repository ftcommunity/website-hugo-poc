---
layout: "image"
title: "halbe Plattform fertig gebaut"
date: "2015-03-14T12:36:48"
picture: "IMG_0051_copy.jpg"
weight: "54"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40652
- /detailsef67.html
imported:
- "2019"
_4images_image_id: "40652"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40652 -->
