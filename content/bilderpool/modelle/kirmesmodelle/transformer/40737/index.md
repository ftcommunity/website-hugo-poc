---
layout: "image"
title: "Portal / Brücke im abgesenkten Zustand"
date: "2015-04-06T19:15:40"
picture: "IMG_0055_2.jpg"
weight: "73"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40737
- /details4581.html
imported:
- "2019"
_4images_image_id: "40737"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-04-06T19:15:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40737 -->
