---
layout: "image"
title: "Schleiferabgriff Mittelstange Drehscheibe"
date: "2015-03-14T12:36:48"
picture: "IMG_0031.jpg"
weight: "41"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40639
- /details0923.html
imported:
- "2019"
_4images_image_id: "40639"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40639 -->
hier sieht man die Federstifte, wie sie normalerweise im alten Drehschalter aus em2 / 1975 verwendet wurden.
