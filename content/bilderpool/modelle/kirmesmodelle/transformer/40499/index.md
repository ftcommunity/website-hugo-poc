---
layout: "image"
title: "Gesamtansicht"
date: "2015-02-08T12:54:27"
picture: "IMG_0055.jpg"
weight: "32"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40499
- /details7d21.html
imported:
- "2019"
_4images_image_id: "40499"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40499 -->
hier mal in Längsrichtung der Brücke "gepeilt" . wie man sieht, steht der gesamte Brückenaufbau nun auf dem Drehkranz . allerdings noch nicht befestigt. es wackelt auch im laufenden Betrieb nichts, d.h. ingesamt ist alles schon ganz gut ausgewuchtet.
