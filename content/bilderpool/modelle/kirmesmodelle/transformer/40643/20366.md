---
layout: "comment"
hidden: true
title: "20366"
date: "2015-03-14T20:37:55"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
Optimierungsmöglichkeiten: Kabel einlöten, und die Zwischenstecker noch etwas kürzen, dann kann man glatt noch etwas Platzsparen