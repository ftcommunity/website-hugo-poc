---
layout: "image"
title: "doppelter Abgriff und Lager der Stange"
date: "2015-03-14T12:36:48"
picture: "IMG_0008_2.jpg"
weight: "43"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40641
- /detailscdec.html
imported:
- "2019"
_4images_image_id: "40641"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40641 -->
Ich führe den Abgriff mit den Federstiften doppelt aus, um zum einen die Stromdichte niedrig zu halten und einen unterbrechungsfreieen Stromfluss sicherzustellen. Über die Stange wird später die Masse für alle Motoren und Lampen gleichzeitig geführt.
