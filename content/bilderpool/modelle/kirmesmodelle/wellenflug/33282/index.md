---
layout: "image"
title: "WF6853.JPG"
date: "2011-10-21T16:17:12"
picture: "WF6853.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33282
- /details40c8.html
imported:
- "2019"
_4images_image_id: "33282"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:17:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33282 -->
Der Gummi-Antrieb hat nicht getaugt und wurde durch einen Kettenantrieb ersetzt. Komplett zerlegen wollte ich das Teil aber nicht, deshalb hat der Gondelträger jetzt ein Speichenrad zu viel. Dadurch ist er aber auch zu lang und kommt nicht mehr um die Kurve, sprich: er klemmt im oberen Bahnteil ... wenn man kein Gelenk einbaut. Am oberen Ende gibt es das gleiche Problem nochmal, deswegen konnte ich dort keinen Original-Schleifring verbauen.

An den Übergängen zwischen den Flachträgern wird es eng, denn Kabelbinder, Stromkabel und der Seilzug passen nur gerade eben so durch die Nabenlöcher der Drehscheiben und Speichenräder.
