---
layout: "image"
title: "Wellenflug419"
date: "2011-10-21T15:42:21"
picture: "KG_419.JPG"
weight: "2"
konstrukteure: 
- "Fa. Zierer"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33275
- /details5922.html
imported:
- "2019"
_4images_image_id: "33275"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T15:42:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33275 -->
Dieser ist ein wenig größer und fährt auf vier Achsen. Das Prinzip ist aber gleich.

München, Oktoberfest 2004
