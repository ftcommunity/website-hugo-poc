---
layout: "image"
title: "WF6871.JPG"
date: "2011-10-21T17:31:16"
picture: "WF6871.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33291
- /detailsb757-2.html
imported:
- "2019"
_4images_image_id: "33291"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T17:31:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33291 -->
Fertig. So war der Welllenflug in Erbes-Büdesheim zu sehen.
