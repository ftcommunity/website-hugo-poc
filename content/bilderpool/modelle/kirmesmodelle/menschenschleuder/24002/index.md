---
layout: "image"
title: "Gesamtansicht_vorne"
date: "2009-05-13T17:01:38"
picture: "menschenschleuder02.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24002
- /details693d.html
imported:
- "2019"
_4images_image_id: "24002"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:01:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24002 -->
