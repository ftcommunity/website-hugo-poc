---
layout: "image"
title: "Schleifkontakt"
date: "2009-05-13T17:02:00"
picture: "menschenschleuder11.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24011
- /details7231.html
imported:
- "2019"
_4images_image_id: "24011"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:02:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24011 -->
