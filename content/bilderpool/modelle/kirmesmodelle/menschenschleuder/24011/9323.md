---
layout: "comment"
hidden: true
title: "9323"
date: "2009-05-20T20:57:16"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

@ manuMFfilms: Die originalen FT-Schleifringe sind absolut in Ordnung, was
die Verwendung an sich betrifft. Ich habe auch zwei davon, verbaue die aber
recht selten. Die sind sind nämlich leider extrem klobig und daher nur schwer
zu verbauen. Aus dem Grund habe ich mir ja selber noch welche wie oben be-
schrieben angefertigt; es ist dann halt nicht mehr original...

Gruß, Thomas