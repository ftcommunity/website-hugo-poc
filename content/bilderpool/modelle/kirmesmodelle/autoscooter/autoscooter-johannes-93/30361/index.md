---
layout: "image"
title: "Fahrzeuge auf der Fahrbahn"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes18.jpg"
weight: "18"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/30361
- /detailsec1a-3.html
imported:
- "2019"
_4images_image_id: "30361"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30361 -->
Auf der Fahrbahn fahren zwei Fahrzeuge, die sich gelegentlich auch überholen.
