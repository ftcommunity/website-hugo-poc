---
layout: "image"
title: "Auf der Modellbau Schleswig Holstein"
date: 2020-10-19T14:00:35+02:00
picture: "Fast fertiges Modell 2.jpg"
weight: "5"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Fast wie in echt - mit Zuschauern