---
layout: "image"
title: "Das rohe Grundgestell"
date: 2020-10-19T14:00:39+02:00
picture: "Das Grundgestell 2.jpg"
weight: "2"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Der seitliche Durchblick