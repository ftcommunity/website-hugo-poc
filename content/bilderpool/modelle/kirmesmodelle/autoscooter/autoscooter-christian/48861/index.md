---
layout: "image"
title: "Grundgestell mit Beleuchtung"
date: 2020-10-19T14:00:38+02:00
picture: "Erste Beleuchtungsprobe.jpg"
weight: "3"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Baustelle mit Beleuchtung