---
layout: "image"
title: "Auf der Modellbau Schleswig Holstein"
date: 2020-10-19T14:00:34+02:00
picture: "Fast fertiges Modell 3.jpg"
weight: "6"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Fast wie in echt - ohne Nebel