---
layout: "image"
title: "Das Kassenhäuschen"
date: 2020-10-19T14:00:48+02:00
picture: "Kassenhäuschen beleuchtet.jpg"
weight: "10"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Bei Nacht