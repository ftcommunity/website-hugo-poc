---
layout: "image"
title: "Auf der Modellbau Schleswig Holstein"
date: 2020-10-19T14:00:36+02:00
picture: "Fast fertiges Modell 1.jpg"
weight: "4"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Christian Wiechmann"
uploadBy: "Website-Team"
license: "unknown"
---

Fast wie in echt - mit Nebel