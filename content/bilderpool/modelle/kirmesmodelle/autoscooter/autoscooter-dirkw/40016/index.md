---
layout: "image"
title: "Autoscooter in Aktion"
date: "2014-12-28T22:12:40"
picture: "autoscooter05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40016
- /details553c.html
imported:
- "2019"
_4images_image_id: "40016"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40016 -->
Hier sind die Fahrzeuge in Aktion. Sie fahren selbsständig mit einem S-Motor und Getriebe.
Über eine bewegliche gelagerte Frontachse weichen sich beim Zusammenstoß aus.
