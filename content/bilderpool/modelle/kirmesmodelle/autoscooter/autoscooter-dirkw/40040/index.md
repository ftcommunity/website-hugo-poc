---
layout: "image"
title: "Scooter vorn"
date: "2014-12-28T22:12:40"
picture: "autoscooter29.jpg"
weight: "29"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40040
- /details5047-2.html
imported:
- "2019"
_4images_image_id: "40040"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40040 -->
Der Scooter sollte möglichst klein werden, daher ist  die Front platzmäßig sehr eng gebaut.
