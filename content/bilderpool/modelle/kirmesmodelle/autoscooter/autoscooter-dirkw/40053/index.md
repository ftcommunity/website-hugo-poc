---
layout: "image"
title: "Stromversorgung"
date: "2014-12-28T22:12:40"
picture: "autoscooter42.jpg"
weight: "42"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40053
- /details46b1.html
imported:
- "2019"
_4images_image_id: "40053"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40053 -->
Für den Strombügel habe ich Draht benutzt.
