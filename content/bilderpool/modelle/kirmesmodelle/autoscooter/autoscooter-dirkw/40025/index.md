---
layout: "image"
title: "Stromversorgung"
date: "2014-12-28T22:12:40"
picture: "autoscooter14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40025
- /details2c47.html
imported:
- "2019"
_4images_image_id: "40025"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40025 -->
Das Alu-Gitter vom Baumarkt  (Plus +) habe ich mit Draht an den Winkelträgern befestigt.
