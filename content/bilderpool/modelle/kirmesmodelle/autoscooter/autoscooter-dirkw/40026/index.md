---
layout: "image"
title: "Autoscooter Boden"
date: "2014-12-28T22:12:40"
picture: "autoscooter15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40026
- /details757c.html
imported:
- "2019"
_4images_image_id: "40026"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40026 -->
Hier seht ihr die Alu Platte. Sie ist aus 1 mm Alu-Blech ebenfalls vom Baumarkt.

