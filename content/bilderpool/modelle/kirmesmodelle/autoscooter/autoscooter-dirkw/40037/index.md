---
layout: "image"
title: "Scooter Achse"
date: "2014-12-28T22:12:40"
picture: "autoscooter26.jpg"
weight: "26"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40037
- /detailsfe1f-2.html
imported:
- "2019"
_4images_image_id: "40037"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40037 -->
Hier seht ihr die vordere bewegliche Achse. Sie knickt weg, wenn der Scooter auf ein Hindernis fährt.


