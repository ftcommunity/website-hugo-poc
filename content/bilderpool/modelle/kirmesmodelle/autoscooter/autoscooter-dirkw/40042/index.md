---
layout: "image"
title: "Scooter oben"
date: "2014-12-28T22:12:40"
picture: "autoscooter31.jpg"
weight: "31"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40042
- /detailsd49e.html
imported:
- "2019"
_4images_image_id: "40042"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40042 -->
