---
layout: "image"
title: "Autoscooter"
date: "2014-12-28T22:12:40"
picture: "autoscooter01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40012
- /detailsa233.html
imported:
- "2019"
_4images_image_id: "40012"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40012 -->
