---
layout: "image"
title: "Stromversorgung"
date: "2014-12-28T22:12:40"
picture: "autoscooter17.jpg"
weight: "17"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40028
- /details70dc.html
imported:
- "2019"
_4images_image_id: "40028"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40028 -->
Hier seht ihr den Stromanschluß der Alu-Platte (- Minus).

Die Stromversorgung kommt aus einem Labornetzteil und beträgt  ca. 13 Volt und 1,7 Ampere bei 4 Scootern mit Beleuchung.
