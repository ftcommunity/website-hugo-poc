---
layout: "image"
title: "Scooter ohne Fahrer"
date: "2014-12-28T22:12:40"
picture: "autoscooter28.jpg"
weight: "28"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40039
- /detailsd9d0.html
imported:
- "2019"
_4images_image_id: "40039"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40039 -->
