---
layout: "image"
title: "Gesamtansicht von Schräg oben"
date: "2011-01-02T17:21:29"
picture: "Bild_315.jpg"
weight: "1"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29591
- /details2985.html
imported:
- "2019"
_4images_image_id: "29591"
_4images_cat_id: "2157"
_4images_user_id: "1164"
_4images_image_date: "2011-01-02T17:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29591 -->
Die Becke und der Boden sind aus Alu-Folie