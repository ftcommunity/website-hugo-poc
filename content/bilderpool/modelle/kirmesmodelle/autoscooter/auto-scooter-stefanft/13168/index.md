---
layout: "image"
title: "Stromversorgung"
date: "2007-12-26T18:26:58"
picture: "autoscooter2.jpg"
weight: "2"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/13168
- /detailsc9f8.html
imported:
- "2019"
_4images_image_id: "13168"
_4images_cat_id: "1189"
_4images_user_id: "672"
_4images_image_date: "2007-12-26T18:26:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13168 -->
Ebenfalls völlig simpel. Ein Pol per Draht an den Fahrboden, der andere per Draht an das Gitter im Dach. Und ein Taster zum Ein-/Ausschalten.