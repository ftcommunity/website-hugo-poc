---
layout: "image"
title: "Autoscooter Auto"
date: "2010-04-07T12:40:32"
picture: "autoscooter3_2.jpg"
weight: "7"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26896
- /detailsbf4c.html
imported:
- "2019"
_4images_image_id: "26896"
_4images_cat_id: "1189"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26896 -->
