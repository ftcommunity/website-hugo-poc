---
layout: "image"
title: "gesamte Unterkonstruktion"
date: "2014-05-25T17:49:46"
picture: "IMG_0106.jpg"
weight: "38"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38845
- /detailsfd1b.html
imported:
- "2019"
_4images_image_id: "38845"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38845 -->
Die Unterkonstruktion unter den Flexschienen wurde maximal verstrebt, da doch eine gewisse Steifigkeit der gesamten Bahn erforderlich ist. auch die Schienensäulen mit den Bahnträgern wurden nochmals mittels Streben zu den Unterkonstruktionskreisen abgesichert
