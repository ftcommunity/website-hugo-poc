---
layout: "image"
title: "Aufgänge vorn"
date: "2014-07-27T22:00:08"
picture: "IMG_0083.jpg"
weight: "39"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39088
- /detailsc22a-2.html
imported:
- "2019"
_4images_image_id: "39088"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-07-27T22:00:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39088 -->
