---
layout: "image"
title: "Fussraum"
date: "2014-07-27T22:00:08"
picture: "IMG_0086.jpg"
weight: "42"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39091
- /details1a19.html
imported:
- "2019"
_4images_image_id: "39091"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-07-27T22:00:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39091 -->
Hier sieht man ganz gut, dass unten alles "zu" ist ..
