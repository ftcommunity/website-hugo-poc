---
layout: "image"
title: "Zahnrad Mittelkranz"
date: "2014-02-23T12:10:16"
picture: "IMG_0009.jpg"
weight: "23"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38338
- /details6f15.html
imported:
- "2019"
_4images_image_id: "38338"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T12:10:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38338 -->
