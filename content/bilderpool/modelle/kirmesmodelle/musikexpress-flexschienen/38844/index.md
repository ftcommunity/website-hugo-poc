---
layout: "image"
title: "Wagen komplett"
date: "2014-05-25T17:49:46"
picture: "IMG_0095.jpg"
weight: "37"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38844
- /details21c5.html
imported:
- "2019"
_4images_image_id: "38844"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38844 -->
Alle Wagen heben jetzt auch einen Boden, der zum Einen aus dem jew. Wagenträger nach vorn gebaut ist, und dann aus dem Vorderwagen komplettiert wird
