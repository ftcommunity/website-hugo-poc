---
layout: "image"
title: "alles von oben"
date: "2014-08-10T21:11:11"
picture: "IMG_0021_2.jpg"
weight: "45"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/39231
- /details6a32.html
imported:
- "2019"
_4images_image_id: "39231"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-08-10T21:11:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39231 -->
Dachkgiebel und Hintergrund+ Umlauf sind fast fertig
