---
layout: "image"
title: "Antrieb gesamt"
date: "2014-02-23T12:10:16"
picture: "IMG_0007.jpg"
weight: "21"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38336
- /detailsc000-2.html
imported:
- "2019"
_4images_image_id: "38336"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T12:10:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38336 -->
ist ein erster Versuch - läuft sauber und kräftig, Geschwindigkeit passt zur Modellgröße.
Die Unterkonstruktion und Lager werden noch verfeinert, es ist aber schonmal ein Ansatz ;-)
