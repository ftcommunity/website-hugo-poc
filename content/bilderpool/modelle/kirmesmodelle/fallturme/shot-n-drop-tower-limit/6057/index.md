---
layout: "image"
title: "tower3"
date: "2006-04-09T21:18:58"
picture: "tower3.jpg"
weight: "3"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/6057
- /details6630.html
imported:
- "2019"
_4images_image_id: "6057"
_4images_cat_id: "525"
_4images_user_id: "430"
_4images_image_date: "2006-04-09T21:18:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6057 -->
der 55cm hohe Turm
