---
layout: "image"
title: "Passagiergondel"
date: "2008-08-30T13:50:44"
picture: "Free-Fall-Tower_11.jpg"
weight: "1"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15126
- /details1ee4.html
imported:
- "2019"
_4images_image_id: "15126"
_4images_cat_id: "1385"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15126 -->
