---
layout: "image"
title: "Elektronische Steuerung und Kompressor"
date: "2009-08-14T21:37:27"
picture: "freefalltower5.jpg"
weight: "5"
konstrukteure: 
- "Armin Jacob"
fotografen:
- "Armin Jacob"
uploadBy: "armin.jacob"
license: "unknown"
legacy_id:
- /php/details/24790
- /details81e7.html
imported:
- "2019"
_4images_image_id: "24790"
_4images_cat_id: "1705"
_4images_user_id: "988"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24790 -->
Hier die beiden Grundplatten mit Interface, Extension, Kompressor, den Magnetvetilen und dem Soundmodul.