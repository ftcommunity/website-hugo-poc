---
layout: "comment"
hidden: true
title: "9762"
date: "2009-08-24T18:09:03"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach Armin,

dann können wir Dir als superspannende (versprochen!) Lektüre die fischertechnik hobby-Begleitbücher allerhöchstwärmstens empfehlen, die Du z. B. von http://www.ft-fanpage.de/FT/hobby_.htm aller herunterladen kannst. Die von hobby-1 und hobby-2 sind wohl zunächst was für Dich. Viel Spaß damit!

Gruß,
Stefan