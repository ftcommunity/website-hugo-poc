---
layout: "image"
title: "Oberteil"
date: "2008-06-14T13:25:33"
picture: "freefalltower20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14697
- /detailsa549.html
imported:
- "2019"
_4images_image_id: "14697"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14697 -->
