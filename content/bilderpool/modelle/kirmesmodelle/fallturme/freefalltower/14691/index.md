---
layout: "image"
title: "Blick unter das Podest"
date: "2008-06-14T13:25:33"
picture: "freefalltower14.jpg"
weight: "14"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14691
- /details4a42.html
imported:
- "2019"
_4images_image_id: "14691"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14691 -->
Unterdem Podest befindet sich das Interface mit der kompletten Verkabelung.
