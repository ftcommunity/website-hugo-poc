---
layout: "image"
title: "Gondel rechts"
date: "2008-06-14T13:25:32"
picture: "freefalltower03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14680
- /details0411-2.html
imported:
- "2019"
_4images_image_id: "14680"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14680 -->
