---
layout: "image"
title: "Gondel oben"
date: "2008-06-14T13:25:33"
picture: "freefalltower23.jpg"
weight: "23"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14700
- /details39d2.html
imported:
- "2019"
_4images_image_id: "14700"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14700 -->
