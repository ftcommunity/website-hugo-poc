---
layout: "image"
title: "Plattform"
date: "2008-06-14T13:25:33"
picture: "freefalltower28.jpg"
weight: "28"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14705
- /details9cde.html
imported:
- "2019"
_4images_image_id: "14705"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14705 -->
Auf die Plattform führen zwei Treppen, sodass die Fahrgäste von beiden Seiten einsteigen können.
