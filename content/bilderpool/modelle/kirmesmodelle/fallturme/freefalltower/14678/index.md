---
layout: "image"
title: "Gesamtansicht inks"
date: "2008-06-14T13:25:32"
picture: "freefalltower01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14678
- /details0e69.html
imported:
- "2019"
_4images_image_id: "14678"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14678 -->
Die Fallhöhe des Towers beträgt genau einen Meter.
