---
layout: "image"
title: "Lichtsschranke unten"
date: "2008-06-14T13:25:33"
picture: "freefalltower17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14694
- /details6ceb-2.html
imported:
- "2019"
_4images_image_id: "14694"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14694 -->
Die Lichtschranke dient zum Stoppen der Gondel.
