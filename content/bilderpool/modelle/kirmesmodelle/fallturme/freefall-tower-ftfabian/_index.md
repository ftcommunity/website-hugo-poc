---
layout: "overview"
title: "Freefall-Tower von ftFabian"
date: 2020-02-22T07:58:46+01:00
legacy_id:
- /php/categories/2493
- /categories4196.html
- /categories5a95-2.html
- /categories8763.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2493 --> 
Bei meinem Freefall-Tower wollte ich zwei Auslösemechanismmen realisieren. Der erste Auslösemechnismus kuppelt den Antrieb während des Hochfahrens aus. Damit geht das Hochfahren direkt in den Freefall über. Der zweite Auslösemechanismus blockiert zuerst die Kabine am höchsten Punkt des Towers. Der Hochziehmechanismus wird ausgekoppelt. Wenn die Blockierung gelöst wird, startet schlagartig der Freefall. Durch diese Maßnahmen ist es mir gelungen, einen "echten" freien Fall zu haben. Die Bremsung wird mittels eines Fototransistors ausgelöst. Die Bremse ist wie eine Backenbremse aufgebaut, wobei die Bremswirkung erstaunlich gut ist. 

The basic idea for my freefall-tower was to implement two escapements. The first escapement couples the actuation out while the carriage is carried up. Moving up instantly turns into a free fall. The second escapement blocks the cabin when it reached the highest point and later couples out. When the blockage is removed, the free fall starts. So I managed to realize a "real" free fall. The brakes are actioned by a phototransistor. It is build like a shoe brake. The breaking action is very good.