---
layout: "image"
title: "Kassenhäuschen / the ticket office"
date: "2011-12-18T21:03:16"
picture: "ftfabian14.jpg"
weight: "14"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33715
- /details1a2a.html
imported:
- "2019"
_4images_image_id: "33715"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33715 -->
Die Kasse mit auffahrbarer Tür.

The ticket office with a movable door.