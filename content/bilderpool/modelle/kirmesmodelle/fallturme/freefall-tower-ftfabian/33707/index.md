---
layout: "image"
title: "Seilrolle / the rope roll"
date: "2011-12-18T21:02:57"
picture: "ftfabian06.jpg"
weight: "6"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- /php/details/33707
- /detailsc27b.html
imported:
- "2019"
_4images_image_id: "33707"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33707 -->
Als Seilrolle sollte man eine ähnliche Konstruktion benutzen, da die normalen Fischertechnik Seilrollen nicht stabil genug sind.

You should use a similar construction for a rope roll because the normal Fischertechnik rope rolls are not rugged enough.