---
layout: "image"
title: "Motorenhaus"
date: "2010-02-14T14:00:25"
picture: "s03.jpg"
weight: "3"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26403
- /details7728.html
imported:
- "2019"
_4images_image_id: "26403"
_4images_cat_id: "1862"
_4images_user_id: "999"
_4images_image_date: "2010-02-14T14:00:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26403 -->
Hier ist die Starke übersetzung zu sehen und die 2 Powermotoren die, die Gondel bewegen