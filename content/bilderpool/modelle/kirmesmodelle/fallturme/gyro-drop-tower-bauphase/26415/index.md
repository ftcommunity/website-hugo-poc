---
layout: "image"
title: "Gyro Drop Tower"
date: "2010-02-14T14:01:02"
picture: "s15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26415
- /details7a9a-2.html
imported:
- "2019"
_4images_image_id: "26415"
_4images_cat_id: "1862"
_4images_user_id: "999"
_4images_image_date: "2010-02-14T14:01:02"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26415 -->
etwas sehr überbelichtete nahaufnahme von den 2 Powermotoren