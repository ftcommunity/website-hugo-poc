---
layout: "image"
title: "Motorenverkleidung-28.3.2010"
date: "2010-03-29T19:03:26"
picture: "gyrodroptowerbauphasestandderdinge3.jpg"
weight: "3"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26842
- /detailsa868.html
imported:
- "2019"
_4images_image_id: "26842"
_4images_cat_id: "1920"
_4images_user_id: "999"
_4images_image_date: "2010-03-29T19:03:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26842 -->
Die Verkleidung habe ich aus den Grundplatten gebaut, die auch den Boden und die Decke machen