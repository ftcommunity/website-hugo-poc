---
layout: "image"
title: "Gesamtansicht-28.3.2010"
date: "2010-03-29T19:03:25"
picture: "gyrodroptowerbauphasestandderdinge1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- /php/details/26840
- /details0668.html
imported:
- "2019"
_4images_image_id: "26840"
_4images_cat_id: "1920"
_4images_user_id: "999"
_4images_image_date: "2010-03-29T19:03:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26840 -->
So sieht er aus. Gut zu erkennen: ich habe mal die erste seite des Motorenhauses verkleidet und unten sind die Alu Schienen für die Wirbelstrom Bremse. im Moment ist nicht der fahrgastträger montiert, weil er sehr schwer ist und die Bremse noch nicht genügend Bremskraft hat.