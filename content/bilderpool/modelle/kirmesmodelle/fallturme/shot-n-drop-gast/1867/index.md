---
layout: "image"
title: "Shot´n Drop"
date: "2003-11-06T00:42:24"
picture: "11.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1867
- /details19db-2.html
imported:
- "2019"
_4images_image_id: "1867"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-11-06T00:42:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1867 -->
Das Innenleben des Turm. (von oben)