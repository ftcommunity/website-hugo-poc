---
layout: "image"
title: "Shot´n Drop"
date: "2003-11-05T01:53:51"
picture: "6.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1861
- /details3dd4.html
imported:
- "2019"
_4images_image_id: "1861"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-11-05T01:53:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1861 -->
Der untere Teil des Turmes mit den Pneumatikventilen. (von der Seite)