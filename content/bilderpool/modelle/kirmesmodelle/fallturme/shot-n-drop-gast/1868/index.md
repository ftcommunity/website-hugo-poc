---
layout: "image"
title: "Shot´n Drop"
date: "2003-11-06T00:42:25"
picture: "11_2.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1868
- /details5b85.html
imported:
- "2019"
_4images_image_id: "1868"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-11-06T00:42:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1868 -->
Ein Blick ins Innere des Turms. (von oben)