---
layout: "image"
title: "Zylinder"
date: "2003-10-25T13:20:24"
picture: "Zylinder.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1844
- /details8ccd.html
imported:
- "2019"
_4images_image_id: "1844"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-10-25T13:20:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1844 -->
Der Zylinder mit den Laufrollen für den Seilantrieb.