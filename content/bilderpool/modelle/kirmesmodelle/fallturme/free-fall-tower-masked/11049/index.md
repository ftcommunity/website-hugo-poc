---
layout: "image"
title: "Unterteil"
date: "2007-07-13T17:46:54"
picture: "freefalltower2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11049
- /details345f.html
imported:
- "2019"
_4images_image_id: "11049"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11049 -->
Im Vordergrund die Mechanik, dahinter Plattform und Turmansatz
