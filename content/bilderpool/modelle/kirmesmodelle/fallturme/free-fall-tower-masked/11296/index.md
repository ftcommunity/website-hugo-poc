---
layout: "image"
title: "Beleuchtung bei 'Tag'"
date: "2007-08-05T15:30:46"
picture: "freefallumbauten5.jpg"
weight: "13"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11296
- /detailse5b5.html
imported:
- "2019"
_4images_image_id: "11296"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11296 -->
