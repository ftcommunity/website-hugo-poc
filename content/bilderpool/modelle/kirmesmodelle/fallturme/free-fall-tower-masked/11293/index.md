---
layout: "image"
title: "Kassenhäuschen"
date: "2007-08-05T15:30:45"
picture: "freefallumbauten2.jpg"
weight: "10"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11293
- /details8142.html
imported:
- "2019"
_4images_image_id: "11293"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11293 -->
Gebaut nach einem Vorbild irgendwo hier auf der ftc, fragt mich nicht wo.
