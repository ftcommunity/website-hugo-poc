---
layout: "image"
title: "Innenleben"
date: "2007-08-05T15:30:46"
picture: "freefallumbauten7.jpg"
weight: "15"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11298
- /details2441.html
imported:
- "2019"
_4images_image_id: "11298"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11298 -->
Das Innenleben der Treppe. Ich hatte grad nix Anderes als Kupferlackdraht und war zu faul in den Keller zu gehen, also hab ich die Leds mit eben Diesem verbunden. Der Verteiler ist von Busch (Modellbahnzubehör),  Art.Nr.2592.
