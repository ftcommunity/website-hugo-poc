---
layout: "image"
title: "Mechanik 1"
date: "2007-07-13T17:46:54"
picture: "freefalltower3.jpg"
weight: "3"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11050
- /detailsd944.html
imported:
- "2019"
_4images_image_id: "11050"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11050 -->
Der PowerMotor auf der linken Seite (in Moosgummi eingepackt) sorgt für den nötigen Antrieb. Rechts sieht man den MiniMotor zur Öffnung der Kupplung
