---
layout: "image"
title: "Unterteil 2"
date: "2007-07-13T17:46:54"
picture: "freefalltower5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11052
- /detailsc148.html
imported:
- "2019"
_4images_image_id: "11052"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11052 -->
Nochmal die Plattform, man erkennt die Steuerungstaster. Mit dem rechten Schiebeschalter schaltet man die Turmbeleuchtung durch die 4 LEDs an (Siehe Bild weiter hinten).
