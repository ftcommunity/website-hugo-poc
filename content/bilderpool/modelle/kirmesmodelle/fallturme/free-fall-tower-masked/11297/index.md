---
layout: "image"
title: "Schaltpult"
date: "2007-08-05T15:30:46"
picture: "freefallumbauten6.jpg"
weight: "14"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/11297
- /detailsde33.html
imported:
- "2019"
_4images_image_id: "11297"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-08-05T15:30:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11297 -->
Links der Reset-Taster, rechts die Start-Taste und in der Mitte der Schalter für das komplette Licht. Das Schaltpult ist über ein altes 25-Pol-Sub-D-Kabel mit dem Turm verbunden. Bleibt also noch viel Platz für Erweiterungen. Selbstverständlich hat das ganze eine Steckverbindung.
