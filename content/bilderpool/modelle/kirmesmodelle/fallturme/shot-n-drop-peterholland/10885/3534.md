---
layout: "comment"
hidden: true
title: "3534"
date: "2007-06-20T21:31:26"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Mit 15 Umdrehungen/sec. der Laufrolle mit 1 weisser Markierung und ein CNY-70 (9,1V Spannung, 100 Ohm Vorderwiederstand) funktioniert es jetzt schon gut. Ich habe Heute andere Magnet-ventilen (grossere Durchlass) per Post empfangen, damit ich die Zylinderkolben-geschindigkeit (=Drehgeschwindigkeit) erhöhen kann. Dann muss es auch noch funktionieren. 

Bei meiner Eucalypta-hexe habe ich beim positionieren der Zunge schon bemerkt das bei eine niedrige Spannung oder zu hohe Vorderwiederstand einer CNY-70, das Signal niedriger und schnell unzuverlässig wird.
http://www.ftcommunity.de/details.php?image_id=8438

Beim Versuch mit das 8-Locher-Rad habe ich eine Vorderwiederstand von 470 Ohm genutzt. Mit 8 kleine Pulsen/Umdrehung mit ein meccano-Rad gibt es 8x15= 120 oder 16x15= 240 Pulsen/sec. 
Bei einer Abtastrate von 10ms (=100 / sec.) kann es mit 240 Impulsen / sec. nicht funktionieren. Die Anzahl der Impulse / sec. ist schlichtweg zu hoch.


Gruss,

Peter 
Poederoyen NL