---
layout: "image"
title: "FT-Shot 'n Drop"
date: "2007-06-18T21:56:52"
picture: "FT-_Shot_n_Drop_005.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/10887
- /details12be.html
imported:
- "2019"
_4images_image_id: "10887"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2007-06-18T21:56:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10887 -->
Pneumatik Shot 'n Drop mit CNY-70 & Laufrolle zum Zylinder-Positionierung.
