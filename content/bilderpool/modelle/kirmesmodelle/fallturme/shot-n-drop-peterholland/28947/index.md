---
layout: "image"
title: "Freifallturm mit wirbelstrombremsen"
date: "2010-10-09T13:47:32"
picture: "Freifallturmwirbelstrombremsen_004.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28947
- /details7537.html
imported:
- "2019"
_4images_image_id: "28947"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-09T13:47:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28947 -->
Freifallturm mit wirbelstrombremsen
