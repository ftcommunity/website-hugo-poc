---
layout: "image"
title: "Freifallturm mit Wirbelstrombremsen:"
date: "2010-10-16T13:39:02"
picture: "Freifallturmwirbelstrombremsen_019.jpg"
weight: "42"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29004
- /detailsae36.html
imported:
- "2019"
_4images_image_id: "29004"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-16T13:39:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29004 -->
Die Ober-Gondel hat eines Verriegelungsystem zum Verbindung an die Unter-Gondel mit Sessel;  genau wie beim Adrenalin-Freifallturm.  Es folgt noch ein Video.
An 2 Seiten 10 st Magneten :
http://www.supermagnete.de/Q-15-15-08-N 
Q-15-15-08-N 
15 x 15 x 8 mm 
Gewicht 14 g 
vernickelt (Ni-Cu-Ni) 
Magnetisierung: N42 
Haftkraft: ca. 7,6 kg/st 

und ein Alu-Profil U20x20x2mm  Länge: 0,6m
Die Gondel Bremmst sehr gut.
