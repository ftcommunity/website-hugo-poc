---
layout: "image"
title: "Freifallturm mit wirbelstrombremsen"
date: "2010-10-09T13:47:31"
picture: "Freifallturmwirbelstrombremsen_011.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Freifallturm", "Wirbelstrombremsen"]
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28944
- /detailsc90a-2.html
imported:
- "2019"
_4images_image_id: "28944"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-09T13:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28944 -->
Dieses Wochenende habe ich nochmals einige Versuchen gemacht mit an 2 Seiten 10 st Magneten 
http://www.supermagnete.de/Q-15-15-08-N 
Q-15-15-08-N 
15 x 15 x 8 mm 
Gewicht 14 g 
vernickelt (Ni-Cu-Ni) 
Magnetisierung: N42 
Haftkraft: ca. 7,6 kg/st 

und ein Alu-Profil U20x20x2mm 

Die Gondel Bremmst jetzt schon sehr gut wegen die richtige Polung !

Grüss, 

Peter 
Poederoyen NL
