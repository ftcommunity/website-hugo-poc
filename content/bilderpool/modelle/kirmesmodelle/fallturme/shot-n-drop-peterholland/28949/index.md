---
layout: "image"
title: "Freifallturm mit wirbelstrombremsen"
date: "2010-10-09T13:47:32"
picture: "Freifallturmwirbelstrombremsen_006.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28949
- /details0eac-2.html
imported:
- "2019"
_4images_image_id: "28949"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2010-10-09T13:47:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28949 -->
Freifallturm mit wirbelstrombremsen

Wie man in Video sehen kann, bremmst die Wirbelstrombremse nicht echt abrupt.
https://www.youtube.com/watch?v=Fz71mrg-mSo

Die Neodym-Magneten habe ich gekleb.
