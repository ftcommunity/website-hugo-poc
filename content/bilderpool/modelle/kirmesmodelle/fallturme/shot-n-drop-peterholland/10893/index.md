---
layout: "image"
title: "FT-Shot 'n Drop"
date: "2007-06-18T21:57:08"
picture: "FT-_Shot_n_Drop_014.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/10893
- /detailsf77e.html
imported:
- "2019"
_4images_image_id: "10893"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2007-06-18T21:57:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10893 -->
Pneumatik Shot 'n Drop mit CNY-70 & Laufrolle zum Zylinder-Positionierung.
