---
layout: "comment"
hidden: true
title: "6526"
date: "2008-05-24T18:24:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist die Elektronik, die den Free Fall Tower steuerte. Sie besteht aus den 1970er-Jahre-Elektronikbausteinen von fischertechnik, den "Silberlingen" (rat mal warum die so genannt wurden ;-) ). Die drei gleichen Elektroniksteine mit dem Einstellrad sind MonoFlops (die machen eine Zeitverzögerung), darunter vier Flip-Flops (die können 1 Bit speichern), darunter vier Relais-Bausteine (die steuern dann tatsächlich die Motoren). Ganz ohne Computer :-)

Gruß,
Stefan