---
layout: "image"
title: "Die Gondel"
date: "2010-04-16T23:38:57"
picture: "freefalltower3_2.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/26950
- /details4a6a.html
imported:
- "2019"
_4images_image_id: "26950"
_4images_cat_id: "1917"
_4images_user_id: "1007"
_4images_image_date: "2010-04-16T23:38:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26950 -->
Die Bügel öffnen sich wenn die Gondel unten aufsetzt und schließen sich automatisch wieder.
