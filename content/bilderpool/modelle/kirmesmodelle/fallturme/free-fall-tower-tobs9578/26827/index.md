---
layout: "image"
title: "Getriebe"
date: "2010-03-28T11:37:22"
picture: "freefalltower6.jpg"
weight: "6"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/26827
- /detailsd5ad.html
imported:
- "2019"
_4images_image_id: "26827"
_4images_cat_id: "1917"
_4images_user_id: "1007"
_4images_image_date: "2010-03-28T11:37:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26827 -->
Hier sieht man das Getriebe mit der Kupplung die ich von Masked übernommen habe.
