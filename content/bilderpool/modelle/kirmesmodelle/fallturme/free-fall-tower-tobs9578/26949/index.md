---
layout: "image"
title: "Kassenhäuschen von Oben"
date: "2010-04-16T23:38:57"
picture: "freefalltower2_2.jpg"
weight: "8"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/26949
- /details8ea9.html
imported:
- "2019"
_4images_image_id: "26949"
_4images_cat_id: "1917"
_4images_user_id: "1007"
_4images_image_date: "2010-04-16T23:38:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26949 -->
Obendrauf in der Mitte sieht man sehr deutlich das Dach ,welches durch einen Boxenverschluss dargestellt wird.
