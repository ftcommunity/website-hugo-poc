---
layout: "image"
title: "FreeFallTurm_Spitze"
date: "2010-07-06T20:03:35"
picture: "freefallturm07.jpg"
weight: "7"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27715
- /detailsd31e.html
imported:
- "2019"
_4images_image_id: "27715"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27715 -->
Noch ein Bild der Spitze nur mit Fassade.
