---
layout: "image"
title: "FreeFallTurm_Antrieb genau"
date: "2010-07-06T20:03:35"
picture: "freefallturm05.jpg"
weight: "5"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27713
- /detailsd4c3.html
imported:
- "2019"
_4images_image_id: "27713"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27713 -->
Hier seht ihr in der Mitte den Antrieb.Er besteht aus 2 Powermots, die synchron drehen. Links sind noch 2 Magnetventile für die Bremse.
