---
layout: "image"
title: "FreeFallTurm_Wagen"
date: "2010-07-06T20:03:35"
picture: "freefallturm08.jpg"
weight: "8"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27716
- /detailsa5fa.html
imported:
- "2019"
_4images_image_id: "27716"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27716 -->
Hier seht ihr den Passagier- und den Hochziehwagen. Der untere Teil mit den Sitzen bis zum zweiten (von unten), roten Rahmen ist der Passagierwagen. An ihm sind Haken in die sich die Andockklappen ein- und durch die Abstoßwippe wieder aushaken können. Diese sind am oberen Wagen angebaut, dem Hochziehwagen.
