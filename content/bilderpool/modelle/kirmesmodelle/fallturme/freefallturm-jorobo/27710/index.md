---
layout: "image"
title: "FreeFallTurm_Mit Beleuchtung"
date: "2010-07-06T20:03:35"
picture: "freefallturm02.jpg"
weight: "2"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27710
- /details26ac.html
imported:
- "2019"
_4images_image_id: "27710"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27710 -->
Hier seht ihr meinen FreeFallTurm mit Beleuchtung. Es sind ca. 52 LEDs in drei verschiedenen Farben verbaut. Jede vierte Etage mit LEDs ist parallel geschaltet. Die LEDs auf einer Etage sind in Reihe geschaltet (so spar ich den Widerstand ;-)
