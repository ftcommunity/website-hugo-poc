---
layout: "image"
title: "FreeFallTurm_Haken"
date: "2010-07-06T20:03:36"
picture: "freefallturm10.jpg"
weight: "10"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27718
- /details7a08.html
imported:
- "2019"
_4images_image_id: "27718"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27718 -->
Hier ist noch eine Nahaufnahme von einem der beiden Haken. Im Hintergrund ist eine der Bremsklappen zu sehen.
