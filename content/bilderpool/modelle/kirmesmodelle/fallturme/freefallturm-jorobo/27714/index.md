---
layout: "image"
title: "FreeFallTurm_Umlenkrollen, Abstoßwippe"
date: "2010-07-06T20:03:35"
picture: "freefallturm06.jpg"
weight: "6"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/27714
- /details4d9a.html
imported:
- "2019"
_4images_image_id: "27714"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27714 -->
Die Spitze des Turms besteht aus:
- den Umlenkrollen: lenken die Seile von unten (Seiltrommel) nach unten (Fahrmodul)
- der Abstoßwippe: löst die Andockklappen
- oberste Reihe LEDs (rot)
