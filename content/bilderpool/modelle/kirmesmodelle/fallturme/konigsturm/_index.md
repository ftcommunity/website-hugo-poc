---
layout: "overview"
title: "Königsturm"
date: 2020-02-22T07:58:27+01:00
legacy_id:
- /php/categories/1210
- /categories2561.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1210 --> 
Dieser Free Fall Tower stellt die vollendete Version des "Free Fall Tower von Sebo" dar. Durch die aufgesetzte Krone erhält er seinen Namen.