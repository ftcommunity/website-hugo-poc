---
layout: "image"
title: "Königsturm Stationsansicht (2)"
date: "2008-01-13T22:29:28"
picture: "free_7.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
schlagworte: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/13325
- /detailsd0fa.html
imported:
- "2019"
_4images_image_id: "13325"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13325 -->
Lediglich durch die etwas schlechte Aufnahme entgeht dem Betrachter die briliant weiße Ausleuchtung der Plattform. 
Ungetrübt davon sticht das mit einer Glühlampe hell erleuchtete Kassenwärterhäusschen ins Auge.