---
layout: "image"
title: "Freefall ohne Bodenplatte"
date: "2010-04-07T12:40:31"
picture: "freefall4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26878
- /details762c.html
imported:
- "2019"
_4images_image_id: "26878"
_4images_cat_id: "1925"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26878 -->
