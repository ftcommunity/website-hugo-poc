---
layout: "image"
title: "Freefall Mechanik 4"
date: "2010-04-07T12:40:31"
picture: "freefall8.jpg"
weight: "8"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26882
- /details8329.html
imported:
- "2019"
_4images_image_id: "26882"
_4images_cat_id: "1925"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26882 -->
Die Verkabelung ist ein wenig krautig.

Beim Hochziehen ist das Zahnrad auf das Differenzial gedrückt. Die Sitzbänke werden am Seil hochgezogen und zwischen den Zahnrädern  wird das Seil  aufgewickelt. Wenn die Sitzbänke oben sind, wird das Zahnrad mit Hilfe von zwei Pneumatikzylindern angehoben. Nun geht es abwärts. Nach einer festgelegten Zeit, fährt der andere Pneumatikzylinder aus und drückt mit einer Stange gegen das Rad mit den Steckverbindern. Die Bremsung ist zwar ein wenig hart, aber wirksam.