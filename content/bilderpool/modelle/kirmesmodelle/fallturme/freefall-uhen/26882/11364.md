---
layout: "comment"
hidden: true
title: "11364"
date: "2010-04-16T15:08:28"
uploadBy:
- "uhen"
license: "unknown"
imported:
- "2019"
---
...also ich sage mal so, ich hatte zuerst nur die Stange, die gegen das Rad ohne Verbinder beim Bremsen drückt. Nur hast Du dann einen Bremsweg von ca. 20cm oder das Sitzgestell schlägt unten auf. Deswegen nun die harte Methode, aber auf der Kirmes sieht die Bremsung auch nicht gerade weich aus bei den Geräten. ;-)