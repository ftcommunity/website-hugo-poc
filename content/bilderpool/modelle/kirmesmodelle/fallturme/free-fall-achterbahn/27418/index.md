---
layout: "image"
title: "39 Tore"
date: "2010-06-07T21:41:45"
picture: "freefalltower13.jpg"
weight: "41"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27418
- /details4fbc-2.html
imported:
- "2019"
_4images_image_id: "27418"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27418 -->
Die Tore geschlossen. 
Scheint, als wollte keiner mitfahren ;-)