---
layout: "image"
title: "21 Sperre"
date: "2010-06-05T13:59:46"
picture: "freefallachterbahn18.jpg"
weight: "22"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27385
- /details34d9.html
imported:
- "2019"
_4images_image_id: "27385"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:46"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27385 -->
Diese Sperre fährt dann raus und das Wägelchen setzt den Wagen darauf ab.