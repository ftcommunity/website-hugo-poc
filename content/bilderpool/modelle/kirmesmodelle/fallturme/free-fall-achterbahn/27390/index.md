---
layout: "image"
title: "26 Oben"
date: "2010-06-05T13:59:46"
picture: "freefallachterbahn23.jpg"
weight: "27"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27390
- /details4703.html
imported:
- "2019"
_4images_image_id: "27390"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:46"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27390 -->
Der gesamte obere Teil.