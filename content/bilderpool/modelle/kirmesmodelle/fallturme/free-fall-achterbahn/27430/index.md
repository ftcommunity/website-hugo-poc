---
layout: "image"
title: "51 Verteilerkasten"
date: "2010-06-07T21:41:46"
picture: "freefalltower25.jpg"
weight: "53"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27430
- /details9a24-2.html
imported:
- "2019"
_4images_image_id: "27430"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:46"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27430 -->
Nochmal nah ran. Man kann den Verteilerkasten einfach nach oben abnehmen, da nur zwei Zapfen in den zwei Löchern stecken.