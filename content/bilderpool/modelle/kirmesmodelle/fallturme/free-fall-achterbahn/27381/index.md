---
layout: "image"
title: "17 Blick von oben"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn14.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27381
- /detailsd321.html
imported:
- "2019"
_4images_image_id: "27381"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27381 -->
So ähnlich sehen das auch die Ft-Männchen, wenn sie kurz von herrunterfallen sind. Natürlich ohne den Wagen in der Station und ohne das Hoch-Zieh-Wägelchen hier links.