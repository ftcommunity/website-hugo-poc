---
layout: "image"
title: "82 Speere"
date: "2010-06-13T11:39:51"
picture: "freefallachterbahn12_2.jpg"
weight: "70"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27482
- /details29fb.html
imported:
- "2019"
_4images_image_id: "27482"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:51"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27482 -->
Ich habe nochmal ein Bild von der Sperre gemacht, die den Wagen oben festhält bzw. loslässt.
