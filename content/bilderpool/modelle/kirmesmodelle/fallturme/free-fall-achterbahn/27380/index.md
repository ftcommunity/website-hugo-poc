---
layout: "image"
title: "16 Lichtschranke"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn13.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27380
- /details5956.html
imported:
- "2019"
_4images_image_id: "27380"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27380 -->
Von der anderen Seite.