---
layout: "image"
title: "14 Reedkontakt"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn11.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27378
- /details05e2.html
imported:
- "2019"
_4images_image_id: "27378"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27378 -->
Im Wagen ist ein Magnet eingabaut. Wenn der Magnet durch das Schieben des Förderbandes hier vorbei kommt, hält er an und die Bühne (rechts im Bild) fährt herraus. Auch hier wird der Wagen beim Vorbeirasen nicht erkannt.