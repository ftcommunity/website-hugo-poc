---
layout: "image"
title: "05 Unterer Teil"
date: "2010-06-05T13:59:43"
picture: "freefallachterbahn01.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27368
- /detailsca69-2.html
imported:
- "2019"
_4images_image_id: "27368"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27368 -->
Noch mal unten.
Hinten sieht man das Kabel, das eigentlich zur Stromversorguung da ist.