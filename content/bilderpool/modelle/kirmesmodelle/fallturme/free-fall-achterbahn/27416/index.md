---
layout: "image"
title: "37 Bühne"
date: "2010-06-07T21:41:45"
picture: "freefalltower11.jpg"
weight: "39"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27416
- /details04d9.html
imported:
- "2019"
_4images_image_id: "27416"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27416 -->
Über diese Schnecke wird die Bühne nach vorn und nach hinten gefahren. Die Schnecke ist mit einem Z10-Ritzel auf der Stange befestigt, das in ein Ritzel am Minimotor greift. Das ist leider von dem Baustein verdeckt.
Unten links ist der Reed-Kontakt zu sehen, der auf den Magnet am Wagen reagiert.