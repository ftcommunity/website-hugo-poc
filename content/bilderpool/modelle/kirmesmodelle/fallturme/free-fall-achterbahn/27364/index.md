---
layout: "image"
title: "01 Free Fall Achterbahn - Gesamtansicht"
date: "2010-06-04T10:54:32"
picture: "freefallachterbahn1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27364
- /detailsd01f.html
imported:
- "2019"
_4images_image_id: "27364"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-04T10:54:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27364 -->
Meine Free Fall Achterbahn in der Gesamtansicht.

Mein Ziel war es eigentlich, beide Türme gleich hoch zu bauen und den Wagen dann auf beiden Seiten immer wieder hoch zu ziehen, das habe ich dann aber doch gelassen, da es zu schwer war das zu bauen und ich nicht genug Teile dazu habe. So wird der Wagen nur auf einer Seite hochgezogen, dafür dann aber in eine Höhe von 1,5 Metern. 


Weitere Bilder werde ich demnächst hochladen.


Video: http://www.youtube.com/watch?v=MLfpjPjFTFk