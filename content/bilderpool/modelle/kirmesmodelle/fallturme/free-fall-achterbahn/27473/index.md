---
layout: "image"
title: "70 Wagen"
date: "2010-06-13T11:39:50"
picture: "freefallachterbahn03_2.jpg"
weight: "61"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27473
- /details1f48-2.html
imported:
- "2019"
_4images_image_id: "27473"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27473 -->
Das Gelenk vom Wagen