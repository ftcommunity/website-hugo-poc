---
layout: "image"
title: "33 Kurve"
date: "2010-06-07T21:41:44"
picture: "freefalltower07.jpg"
weight: "35"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27412
- /detailsb5b3.html
imported:
- "2019"
_4images_image_id: "27412"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27412 -->
Die Kurve zum kleinen Turm hoch. Hintendran ist die Treppe und die Bühne.