---
layout: "image"
title: "52 Loch"
date: "2010-06-07T21:41:46"
picture: "freefalltower26.jpg"
weight: "54"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27431
- /details5b80-2.html
imported:
- "2019"
_4images_image_id: "27431"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:46"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27431 -->
In diesem Loch steht eines der sechs Beine von der Plattform, auf der auch der Wartebereich ist.