---
layout: "image"
title: "81 Wägelchen"
date: "2010-06-13T11:39:51"
picture: "freefallachterbahn11_2.jpg"
weight: "69"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27481
- /detailsc9cf-2.html
imported:
- "2019"
_4images_image_id: "27481"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:51"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27481 -->
von unten. 
Es hat nur 4 Räder, von hinten greifft es die Schiene mit den herrausragenden Noppen