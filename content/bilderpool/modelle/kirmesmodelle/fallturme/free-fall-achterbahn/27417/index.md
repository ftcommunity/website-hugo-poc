---
layout: "image"
title: "38 Tore"
date: "2010-06-07T21:41:45"
picture: "freefalltower12.jpg"
weight: "40"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27417
- /details21ca.html
imported:
- "2019"
_4images_image_id: "27417"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27417 -->
Die Tore, die sich öffnen, nachdem die Bühne wieder zurückgefahren ist. Hinten ist der Wagen zu sehen, in den dann die Ft-Menschen einsteigen.