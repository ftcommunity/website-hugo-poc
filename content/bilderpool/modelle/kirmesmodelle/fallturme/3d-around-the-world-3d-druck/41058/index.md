---
layout: "image"
title: "3D-Around-the-World"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld15.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41058
- /detailsf6f9.html
imported:
- "2019"
_4images_image_id: "41058"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41058 -->
