---
layout: "image"
title: "6st   3D-Druck  Innenzahnkranz  in grau Polyamid zum '3D-Around-the-World'"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld28.jpg"
weight: "28"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41071
- /detailsde0c.html
imported:
- "2019"
_4images_image_id: "41071"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41071 -->
Johannes Visser hat im ft:pedia-2015-1 ein 3D-Druck  Innenzahnkranz  Entwurfen.
Ich habe bei  Trinckle 3D GmbH  12st in grau Polyamid bestellt.   
6 Stuck für die "3D-Around-the-World" + 6 Stuck für andere Modellen.

Web:      www.trinckle.com         Tel: +49 (0)3302 2094740
Email: info@trinckle.com

Der Druck von 6 Modellen in ABS würde ca. 88,20 EUR zzgl. Versand kosten.  Das verwendete Material ABS wird in der Farbe Elfenbein angeboten.  
Alternativ könnten Sie Polyamid,  inklusive graue Färbung kosten 6 Stück 132,84 EUR zzgl. Versand.
