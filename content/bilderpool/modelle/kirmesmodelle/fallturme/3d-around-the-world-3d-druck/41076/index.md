---
layout: "image"
title: "Einbau der 6st Polyamid 3D-Druck  Innenzahnkranz    -unten"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld33.jpg"
weight: "33"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41076
- /detailse270.html
imported:
- "2019"
_4images_image_id: "41076"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41076 -->
