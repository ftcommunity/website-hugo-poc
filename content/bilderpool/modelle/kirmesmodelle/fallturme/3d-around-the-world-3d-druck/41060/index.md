---
layout: "image"
title: "3D-Around-the-World  mit Geschwindigkeit"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41060
- /detailsab0b.html
imported:
- "2019"
_4images_image_id: "41060"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41060 -->
