---
layout: "image"
title: "3D-Around-the-World mit einer 3D-Druck Innenzahnkranz   -Detail   2st  4mm Messing Schleifringe"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld26.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41069
- /detailse24b.html
imported:
- "2019"
_4images_image_id: "41069"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41069 -->
4mm Messing Schleifringe
