---
layout: "comment"
hidden: true
title: "20680"
date: "2015-05-28T19:39:06"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
Auch von mir großes Lob für dieses und auch die anderen Modelle aus deiner "ft-Werkstatt"!
(es scheint mir, der Raum könnte noch viel höher sein)
Martin