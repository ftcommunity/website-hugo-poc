---
layout: "image"
title: "3D-Around-the-World"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld14.jpg"
weight: "14"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41057
- /details2940.html
imported:
- "2019"
_4images_image_id: "41057"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41057 -->
