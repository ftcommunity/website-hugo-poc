---
layout: "image"
title: "Einbau der 6st Polyamid 3D-Druck  Innenzahnkranz + Federschleifer oderSchleifkontakten"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld39.jpg"
weight: "39"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41082
- /details6135-2.html
imported:
- "2019"
_4images_image_id: "41082"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41082 -->
Sehr wichtig beim Aluminium-Profilen ist die Entfernung der Anodisierung an die Ecken zum Electrischen Kontakten !
Anodiseerlagen zijn elektrisch isolerend. Middels schuurpapier is deze anodiseer-laag  effectief te verwijderen met blijvende goede geleiding voor de sleepcontacten.
