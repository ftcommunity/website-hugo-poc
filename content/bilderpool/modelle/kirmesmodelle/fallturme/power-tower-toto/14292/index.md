---
layout: "image"
title: "Seilwinde"
date: "2008-04-19T08:42:56"
picture: "powertower03.jpg"
weight: "3"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/14292
- /details590a-2.html
imported:
- "2019"
_4images_image_id: "14292"
_4images_cat_id: "1323"
_4images_user_id: "762"
_4images_image_date: "2008-04-19T08:42:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14292 -->
Wenn das große Gewicht hoch gezogen wird kommt die Gondel in den Freifall.