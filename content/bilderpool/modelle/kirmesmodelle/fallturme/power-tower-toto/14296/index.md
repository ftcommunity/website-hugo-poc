---
layout: "image"
title: "Spitze"
date: "2008-04-19T08:42:56"
picture: "powertower07.jpg"
weight: "7"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/14296
- /detailscc62.html
imported:
- "2019"
_4images_image_id: "14296"
_4images_cat_id: "1323"
_4images_user_id: "762"
_4images_image_date: "2008-04-19T08:42:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14296 -->
Spitze