---
layout: "image"
title: "Gondel mit Einstieg"
date: "2014-08-07T10:27:25"
picture: "DSC00057.jpg"
weight: "8"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["bauFischertechnik", "Looping", "Karussell", "Fischertechnik", "Schleifkontakt", "Robo", "TX", "Controller", "RoboPro"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39156
- /details94e5.html
imported:
- "2019"
_4images_image_id: "39156"
_4images_cat_id: "2927"
_4images_user_id: "2086"
_4images_image_date: "2014-08-07T10:27:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39156 -->
Der Steg zur gondel kann auf und ab gefahren werden.