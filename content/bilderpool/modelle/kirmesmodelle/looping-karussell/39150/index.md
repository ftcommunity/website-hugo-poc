---
layout: "image"
title: "Sockel"
date: "2014-08-07T10:27:25"
picture: "DSC00050.jpg"
weight: "2"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["bauFischertechnik", "Looping", "Karussell", "Fischertechnik", "Schleifkontakt", "Robo", "TX", "Controller", "RoboPro"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39150
- /details1134.html
imported:
- "2019"
_4images_image_id: "39150"
_4images_cat_id: "2927"
_4images_user_id: "2086"
_4images_image_date: "2014-08-07T10:27:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39150 -->
Hier sieht man den Sockel des Looping-Karussells.