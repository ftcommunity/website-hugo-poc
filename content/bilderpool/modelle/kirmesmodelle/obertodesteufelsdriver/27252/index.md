---
layout: "image"
title: "Gesamtansicht"
date: "2010-05-16T15:52:19"
picture: "obertodesteufelsdriver01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27252
- /detailsf375-2.html
imported:
- "2019"
_4images_image_id: "27252"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27252 -->
Dies ist eine ganz einfache Bahn, auf der ein Obertodesteufelsdriver ;-) einen Looping und eine Sprungschanze zu überstehen hat. Am Ende wird er von Puffern einigermaßen weich abgefangen.

Vielleicht wird da mal so etwas wie eine Darda-Bahn aus fischertechnik draus. Mir gingen allerdings die Flachträger aus, die gerade alle in den Achter-Bahnen (http://www.ftcommunity.de/categories.php?cat_id=1884) stecken, so dass ich keine Kurven mehr probieren konnte. Außerdem müsste man noch einen Wagen konstruieren, der einen Aufzieh-Motor hat.

Video: http://www.youtube.com/watch?v=J_R8n6fCnVo
