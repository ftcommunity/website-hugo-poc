---
layout: "image"
title: "Ausfahrt aus dem Looping und Ansatz zur Sprungschanze"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27256
- /details482e-2.html
imported:
- "2019"
_4images_image_id: "27256"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27256 -->
Auch hier wieder eine stabile Befestigung. Links beginnt die Sprungschanze.
