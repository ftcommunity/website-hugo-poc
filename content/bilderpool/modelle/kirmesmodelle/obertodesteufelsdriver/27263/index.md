---
layout: "image"
title: "Der Wagen - Ohne Sitz"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27263
- /detailsd951-2.html
imported:
- "2019"
_4images_image_id: "27263"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27263 -->
Hier sieht man, wo und wie der Sitz befestigt wird. Das seltsame Teil mit dem Knick ist vermutlich für eine Schaufel gedacht und stammt aus dem Bestand meiner Tochter.
