---
layout: "image"
title: "Alternativer, aber zu breiter Wagen - Ohne Sitz"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27268
- /details22a9.html
imported:
- "2019"
_4images_image_id: "27268"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27268 -->
Man erkennt, wie der Sitz befestigt wird. Der Mittelteil besteht aus (in dieser Reihenfolge) BS15, BS5, BS7,5, BS15 mit zwei Zapfen, und wieder BS7,5, BS5 und BS15. Mit Platten 15x75 ist alles gegen Verrutschen nach oben oder unten gesichert.
