---
layout: "image"
title: "Alternativer, aber zu breiter Wagen - Unterseite"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27269
- /details94d1-2.html
imported:
- "2019"
_4images_image_id: "27269"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27269 -->
Hier die Unterseite, ebenfalls mit einem Blick auf die Sitzbefestigung.
