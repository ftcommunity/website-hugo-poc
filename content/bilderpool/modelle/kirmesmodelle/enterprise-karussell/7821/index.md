---
layout: "image"
title: "Sprekende en spelende kermisdraaimolem"
date: "2006-12-10T17:51:48"
picture: "Voice-mudule_002.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7821
- /detailsf986.html
imported:
- "2019"
_4images_image_id: "7821"
_4images_cat_id: "1578"
_4images_user_id: "22"
_4images_image_date: "2006-12-10T17:51:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7821 -->
