---
layout: "image"
title: "sp"
date: "2006-12-10T17:51:48"
picture: "Voice-mudule_009.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7826
- /details6a6e.html
imported:
- "2019"
_4images_image_id: "7826"
_4images_cat_id: "1578"
_4images_user_id: "22"
_4images_image_date: "2006-12-10T17:51:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7826 -->
