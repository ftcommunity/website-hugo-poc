---
layout: "image"
title: "Sprekende en spelende kermisdraaimolem"
date: "2006-12-10T17:51:48"
picture: "Voice-mudule_005.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7824
- /detailsa609-2.html
imported:
- "2019"
_4images_image_id: "7824"
_4images_cat_id: "1578"
_4images_user_id: "22"
_4images_image_date: "2006-12-10T17:51:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7824 -->
