---
layout: "image"
title: "02 Gesamtansicht"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen2.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27752
- /details88a3.html
imported:
- "2019"
_4images_image_id: "27752"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27752 -->
Von der Seite sieht man die Räder und deren Befestigung. Es greift pro Wagen jeweils ein Rad von oben, von unten und von der Seite an jeder Ecke in die Schiene. Zwischen zwei Wägen befindet sich ein Kardangelenk.