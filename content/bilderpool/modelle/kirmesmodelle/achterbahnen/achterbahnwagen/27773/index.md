---
layout: "image"
title: "10 neuer Wagen"
date: "2010-07-20T15:10:42"
picture: "achterbahnwagen2_2.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27773
- /detailsb95c-2.html
imported:
- "2019"
_4images_image_id: "27773"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-20T15:10:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27773 -->
Hier von der Seite. Verrutschen kann hier nichts mehr. Jetzt muss ich nur noch eine Lösung für das große Loch in der Mitte finden, denn da fällt das Licht der Lichtschranke durch.