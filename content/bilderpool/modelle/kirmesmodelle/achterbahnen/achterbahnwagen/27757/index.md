---
layout: "image"
title: "07 unten"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen7.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27757
- /details17e5.html
imported:
- "2019"
_4images_image_id: "27757"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27757 -->
So sieht er von unten aus. Nochmal zu sehen sind die zwei Noppen. 
Bei genauem Hinsehen erkennt man auf der linken Seite etwas Tesafilm. Ich habe etwas Watte in die Zwischenräume gesteckt und festgeklebt, damit da das Licht der Lichtschranke nicht durchfällt. Von außen ist das aber nicht zu sehen, oder hat jemand was auf dem vorherigen Bild gesehen? ;-)