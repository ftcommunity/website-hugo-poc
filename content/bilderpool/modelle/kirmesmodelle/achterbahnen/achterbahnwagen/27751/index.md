---
layout: "image"
title: "01 Achterbahnwagen - Gesamtansicht"
date: "2010-07-14T22:09:09"
picture: "achterbahnwagen1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27751
- /details17b2.html
imported:
- "2019"
_4images_image_id: "27751"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27751 -->
Dies ist der Wagen, den ich für meine neue Achterbahn verwenden wollte.
Nun muss ich einen neuen Wagen entwerfen, der den Kräften eines Loopings standhält. Diesen hier reist es nämlich bei zu hoher Geschwindigkeit auseinander. Die Räder biegen sich zur Seite weg und das führt dann im Looping zu bösen Unfällen ;-) Bei einem Gewicht von 650 Gramm ist das aber  kein Wunder. 
Der Vorteil dieses Wagens ist, dass er trotz seines Gewichts gut und leichtgängig auf der Schiene fährt und das er beliebig erweiterbar ist, solange man genug Teile hat. Denn materialsparend ist er nicht. Weiterhin ist das Schienenmaß einfach gehalten, es ist 4 Bausteine oder 6cm breit. 
Wem er also gefällt kann ihn gerne für seine nächste Achterbahn verwenden, ich mach mich auch die Suche nach einer Alternative ;-)