---
layout: "comment"
hidden: true
title: "13124"
date: "2011-01-06T21:30:05"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Ein Problem, das sich leider oft nicht vermeiden läßt ist, daß Kameras vor allem mit ausschließlich (!) automatischer Schärfeeinstellung filigrane Gitterstrukturen (bei FT ja unvermeidlich) nicht "erkennen" und dann den Hintergrund fokussieren. Auch hierbei entstehen zumeist unscharfe Bilder in bezug aufs Modell selbst. Dann hilft nur probieren - und zwar möglichst aus verschiedenen Höhen und/ oder Blickwinkeln. Irgendwann klappt's dann mit dem Bild.

Gruß, Thomas