---
layout: "image"
title: "Helix (Kurve)"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase06.jpg"
weight: "6"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25607
- /details472f.html
imported:
- "2019"
_4images_image_id: "25607"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25607 -->
Um die Kurve fahren zu können, musste die Schiene 90° gedreht werden. Dabei kommt meine Steckverbindung zum Einsatz (http://www.ftcommunity.de/categories.php?cat_id=1795)

[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)
