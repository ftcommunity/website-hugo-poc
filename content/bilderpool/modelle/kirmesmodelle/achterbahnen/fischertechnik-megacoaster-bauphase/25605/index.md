---
layout: "image"
title: "Bremse"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase04.jpg"
weight: "4"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
schlagworte: ["modding"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/25605
- /details8337.html
imported:
- "2019"
_4images_image_id: "25605"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25605 -->
Dies ist die vorläufige Version der Bremse vor der Transferbühne.
Durch die Reibung an den mit Isolierband beschichteten Bremsplatten, kommt der Wagen zum stehen. dann werden die Bremsplatten pneumatisch weggeklappt, damit der Wagen auf die Transferbühne fahren kann.

[Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version](http://www.youtube.com/user/ftmegacoaster)
