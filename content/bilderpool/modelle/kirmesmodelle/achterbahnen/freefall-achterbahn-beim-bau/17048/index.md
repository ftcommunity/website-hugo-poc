---
layout: "image"
title: "Rechter Turm - Bahnhof"
date: "2009-01-17T13:23:39"
picture: "freefallachterbahnbeimbau08.jpg"
weight: "8"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17048
- /detailsa872.html
imported:
- "2019"
_4images_image_id: "17048"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17048 -->
Hier sieht man 4 Fahrwägen (1kg!), die dann mit Hilfe der Ketten bis zur Übergabe am Turmaufzug hochgezogen werden. Bei Betrieb kommen die Ketten noch unter die schwarzen Zahnräder (rechter oberer Kettenteil) hin.
