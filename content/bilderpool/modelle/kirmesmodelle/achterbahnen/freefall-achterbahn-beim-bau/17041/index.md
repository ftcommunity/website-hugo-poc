---
layout: "image"
title: "Fahrwagen Version7"
date: "2009-01-17T13:23:39"
picture: "freefallachterbahnbeimbau01.jpg"
weight: "1"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17041
- /details01f0.html
imported:
- "2019"
_4images_image_id: "17041"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17041 -->
Bild wurde zum besseren Verständnis um 180° gedreht. Diese Version war optisch mein Favorit, leider hat sie sich trotz anfänglichem Optimismus als zu "bremsend" erwiesen .Die Sitzbank wäre noch unten hingekommen.
