---
layout: "image"
title: "Rechter Turm - Übergabe Turmaufzug"
date: "2009-01-17T14:45:22"
picture: "freefallachterbahnbeimbau10.jpg"
weight: "10"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17050
- /details01d7-2.html
imported:
- "2019"
_4images_image_id: "17050"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T14:45:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17050 -->
Da der Turm wegen dem späteren Transport im Kfz getrennt transportiert werden muss, musste ich den Aufzug in 2 Teilen machen. Auf dem Alu läuft später die Kette, deren Mitnehmer mit Hilfe der Streben sich hinter den obersten Fahrwagen einklinken muss, um ihn hochzuziehen.
