---
layout: "image"
title: "Linker und rechter Turm"
date: "2009-01-17T14:45:22"
picture: "freefallachterbahnbeimbau11.jpg"
weight: "11"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/17051
- /detailse8d4.html
imported:
- "2019"
_4images_image_id: "17051"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T14:45:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17051 -->
Den Looping habe ich sicherheitshalber zusätzlich mit Alus stabilisiert.
