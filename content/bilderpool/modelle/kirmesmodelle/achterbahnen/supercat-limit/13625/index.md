---
layout: "image"
title: "Supercat neue Wippe"
date: "2008-02-09T22:49:05"
picture: "sc03.jpg"
weight: "14"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13625
- /detailsf8b3.html
imported:
- "2019"
_4images_image_id: "13625"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13625 -->
Die Abfahrt ist sehr sehr steil :)
Unten die Kurve zur Geraden werde ich vielleicht noch überarbeiten. Aber erst mal muss ich schaun, dass der Stoß besser wird, damit sich der Wagen nicht verkeil.
