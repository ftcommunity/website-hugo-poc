---
layout: "image"
title: "Supercat Wagen auf der Strecke"
date: "2008-01-19T17:53:54"
picture: "sc7.jpg"
weight: "7"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13352
- /details21d7.html
imported:
- "2019"
_4images_image_id: "13352"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13352 -->
Hier sieht man den Wagen gerade in der Mulde.
Der Wagen ist ausgestattet mit Kugellagern und fährt auch recht schnell:)
Des weiteren hat er auch Bügel, die auch wirklich zuhalten (ganz einfaches System)
