---
layout: "image"
title: "Wagen auf der Wippe"
date: "2008-01-24T21:53:21"
picture: "supercat1.jpg"
weight: "10"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13380
- /detailsb6d0-2.html
imported:
- "2019"
_4images_image_id: "13380"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-24T21:53:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13380 -->
Hier sieht man den Wagen auf der Wippe von oben.
