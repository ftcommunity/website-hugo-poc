---
layout: "image"
title: "Supercat Überblick"
date: "2008-02-09T22:49:06"
picture: "sc10.jpg"
weight: "21"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13632
- /details4cfe.html
imported:
- "2019"
_4images_image_id: "13632"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13632 -->
Hier noch mal ein aktueller Überblick über Supercat. 
Keine Panik die ganzen Kabel, die da rumhängen werd ich später irgendwie verstauen. Jetzt brauch ich sie noch, weil ich noch alles manuell steuere.
