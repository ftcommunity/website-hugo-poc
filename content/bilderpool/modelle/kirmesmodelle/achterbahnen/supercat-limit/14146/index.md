---
layout: "image"
title: "Supercat Wippe"
date: "2008-03-30T20:29:26"
picture: "sc2_2.jpg"
weight: "25"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/14146
- /details4379.html
imported:
- "2019"
_4images_image_id: "14146"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-03-30T20:29:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14146 -->
Hier sieht man die Schnecke, die dafür sorgt, dass die Wippe sich nach oben oder nach unten neigt. Bei genauerem Betrachten merkt man, dass sich das ganze etwas wölbt. Kann das vielleicht daran liegen, dass es nicht eine durchgehende Achse ist auf der die Schnecke gesteckt ist?
