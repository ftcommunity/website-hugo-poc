---
layout: "image"
title: "Supercat untere Strecke"
date: "2008-02-09T22:49:06"
picture: "sc07.jpg"
weight: "18"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13629
- /details42f1-2.html
imported:
- "2019"
_4images_image_id: "13629"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13629 -->
Das Bild wurde aus der Aufzugskabine geschossen.
Man sieht vorne die Reibräder (Station (evtl. links), Bremse)
Dann die Bremschwerter. Im Anschluss daran eine Gabellichtschranke (die die Fotomaschine auslösen soll).
Und ganz hinten den Wagen auf der Wippe.
