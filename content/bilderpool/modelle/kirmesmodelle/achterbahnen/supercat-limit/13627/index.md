---
layout: "image"
title: "Supercat Wagen"
date: "2008-02-09T22:49:06"
picture: "sc05.jpg"
weight: "16"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13627
- /details5754-2.html
imported:
- "2019"
_4images_image_id: "13627"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13627 -->
Und weil wir schon beim Thema Sicherheit sind.
Hier ein Bild, dass die Sicherheitsbügel zeig, welche wirklich halten und sich nur unter einem bestimmten Faktor öffnen lassen.
Des Weiteren soll vor der Abfahrt aus der Station ein Reed-Kontakt erfragen ob der Bügel wirklich geschlossen ist. (Darum die Magnetbausteine)
