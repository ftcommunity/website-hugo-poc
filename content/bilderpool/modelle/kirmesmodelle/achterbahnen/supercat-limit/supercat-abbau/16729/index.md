---
layout: "image"
title: "Supercat Abbau - Letztes Bild vom Aufzug"
date: "2008-12-24T12:16:41"
picture: "supercatabbau19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16729
- /detailsb6d7.html
imported:
- "2019"
_4images_image_id: "16729"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16729 -->
Der Aufzug in seiner vollen Größe