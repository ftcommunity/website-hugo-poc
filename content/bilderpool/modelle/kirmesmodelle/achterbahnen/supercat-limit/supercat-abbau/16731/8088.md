---
layout: "comment"
hidden: true
title: "8088"
date: "2008-12-27T17:39:48"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Den Baustein 7,5 hatte ich auch schon in Erwägung. Jedoch war der Überstand des Leuchtsteins über den Bauplatten nicht sicher auszumachen ... So wie von dir angegeben, ist die Lösung wegen der Verzapfung natürlich die einzig richtige.
Gruß, Udo2