---
layout: "image"
title: "Supercat Abbau - Fehlende obere Etage"
date: "2008-12-24T12:16:40"
picture: "supercatabbau06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16716
- /details78fc-2.html
imported:
- "2019"
_4images_image_id: "16716"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16716 -->
Hier oben war die Wippe. Dahinter befand sich die Welle und ganz im Hintergrund sieht man noch den Aufzug.