---
layout: "image"
title: "Supercat Abbau - Technik der Schienen"
date: "2008-12-24T12:16:40"
picture: "supercatabbau13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16723
- /details1bc7-2.html
imported:
- "2019"
_4images_image_id: "16723"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16723 -->
Hier sieht man nochmal die Reibräder und das Bremsschwert. Unter der Verkleidung der Station sind die Reed-Kontakte, die die Bügel überprüft haben.