---
layout: "image"
title: "Supercat Abbau - Reibrad Antrieb"
date: "2008-12-24T12:16:41"
picture: "supercatabbau32.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16742
- /details1516-2.html
imported:
- "2019"
_4images_image_id: "16742"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16742 -->
So waren die Reibräder aufgebaut.