---
layout: "image"
title: "Supercat Abbau - Aufzug ohne Aufbauten"
date: "2008-12-24T12:16:41"
picture: "supercatabbau27.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16737
- /detailsfd9e-2.html
imported:
- "2019"
_4images_image_id: "16737"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16737 -->
Oben wurden statischen Gitter entfernt sowie das Gefahrenfeuer, das sich in der Dunkelheit einschaltete.