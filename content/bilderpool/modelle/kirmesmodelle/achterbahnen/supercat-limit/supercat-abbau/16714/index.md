---
layout: "image"
title: "Supercat Abbau - Konstruktion der Stützen"
date: "2008-12-24T12:16:40"
picture: "supercatabbau04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16714
- /details9d34.html
imported:
- "2019"
_4images_image_id: "16714"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16714 -->
Das Wippsegment wurde einfach von den beiden Drehkränzen abgenommen und vom Seil befreit.