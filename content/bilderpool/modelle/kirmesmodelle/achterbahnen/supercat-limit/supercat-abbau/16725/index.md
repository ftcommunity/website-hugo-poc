---
layout: "image"
title: "Supercat Abbau - Technik der Plattform"
date: "2008-12-24T12:16:40"
picture: "supercatabbau15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/16725
- /details18e7.html
imported:
- "2019"
_4images_image_id: "16725"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16725 -->
Auch hier wieder ein Blick auf die Reed-Kontakte und auf die Zahnräder, die die Gates betrieben haben.