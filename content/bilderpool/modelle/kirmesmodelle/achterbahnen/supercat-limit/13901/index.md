---
layout: "image"
title: "Wagen auf der Wippe"
date: "2008-03-14T07:06:11"
picture: "supercat2_2.jpg"
weight: "23"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13901
- /details1c37-3.html
imported:
- "2019"
_4images_image_id: "13901"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-03-14T07:06:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13901 -->
Hier sieht man den Sinn und Zweck der Anti Entgleisungsräder :)
