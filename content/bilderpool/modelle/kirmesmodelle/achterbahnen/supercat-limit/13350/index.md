---
layout: "image"
title: "Supercat Aufzug Flaschenzug"
date: "2008-01-19T17:53:54"
picture: "sc5.jpg"
weight: "5"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13350
- /detailsd4c6.html
imported:
- "2019"
_4images_image_id: "13350"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13350 -->
Hier sieht man wunderbar wie oft das Seil umgelent wird, damit die Kabine nach oben gezogen werden kann.
