---
layout: "image"
title: "Supercat-Wippe-Neue Version im gekippten Zustand"
date: "2008-04-18T21:08:55"
picture: "sc2_3.jpg"
weight: "28"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/14289
- /detailsb9a8.html
imported:
- "2019"
_4images_image_id: "14289"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-04-18T21:08:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14289 -->
Hier ist die Wippe im gekippten Zustand zu sehen. Nun würde hier das hintere Rad vom Wagen den Winkelträger berühren.
Ob ich die Wippe unten noch zentriere weiss ich noch nicht. Das lässt sich erst nach mehreren manuellen Testfahrten sagen.
