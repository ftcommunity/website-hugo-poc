---
layout: "image"
title: "Supercat - vorläufiges Steuerpult"
date: "2008-06-21T18:28:24"
picture: "sc1_4.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/14749
- /details8f2c.html
imported:
- "2019"
_4images_image_id: "14749"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14749 -->
Das ist das vorläufige Steuerpult von Supercat. 
Die vier weisen Lichter (links oben) zeigen an ob alle Bügel geschlossen sind.. 
Die Lampe unten drunter ist gleichzietig noch ein Taster. Mit diesem kann man die Tore in der Station steuern.
Unten links und rechts befindet sich geweils eine grüne Leuchte, die man auch drücken kann. Werden diese beiden Lampen gleichzeitig gedrückt startet die Achterbahn. 
Rechts außen mittig befindet sich ein Pohlwender. mit diesem kann man den Betriebsmodus ändern. (Automatisch und Halbautomatisch).
Rechts oben befindet sich der Not aus.
Und die zwei Lampen (rot und grün) sind die Statusleuchten.
Ich hoffe, dass das auch mal wirklich so funktionieren wird :)