---
layout: "image"
title: "Supercat Aufzug Gesammt"
date: "2008-01-19T17:53:54"
picture: "sc2.jpg"
weight: "2"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13347
- /details3a7a.html
imported:
- "2019"
_4images_image_id: "13347"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13347 -->
Durch einen Flaschenzug kommt die Kabnine mitsamt Wagen nach oben.
Das Seil ist an beiden Enden mit Seiltrommeln verbunden, die das Seil aufziehen.
