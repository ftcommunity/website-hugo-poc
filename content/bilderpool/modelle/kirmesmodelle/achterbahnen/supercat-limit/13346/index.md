---
layout: "image"
title: "Supercat Gesammt"
date: "2008-01-19T17:53:53"
picture: "sc1.jpg"
weight: "1"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/13346
- /details38e5.html
imported:
- "2019"
_4images_image_id: "13346"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13346 -->
So sieht Supercat im Moment aus.
Von Links nach Rechts:
Der große Turm ist der Aufzug
Dann kommt die Strecke (Die Mulde oben wird Rückwärts durchfahren
und ein bisschen Abseits kommt die Wippe (die muss ich noch mal überholen)
