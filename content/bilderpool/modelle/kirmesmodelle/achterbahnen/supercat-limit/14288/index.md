---
layout: "image"
title: "Supercat-Wippe-Neue Version"
date: "2008-04-18T21:08:55"
picture: "sc1_3.jpg"
weight: "27"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/14288
- /details249a.html
imported:
- "2019"
_4images_image_id: "14288"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-04-18T21:08:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14288 -->
Hier sieht man die neue Wippe von Supercat. Vieles hat sich verändert gegenüber der alten Version. Der "Schneckentrurm"  ist z.b. Verschwunden. Nun wickeln zwei 8:1 Motoren das Seil auf und ab.
Dessweiteren wurden die U-Träger mit gelben Winkelträgern abgestützt. Ein Problem was durch die neue Konstruktion dabei entsteht ist, dass der Wagen aufliegt.
Aber das ist nicht so schlimm wie momentan die anderen Probleme.
