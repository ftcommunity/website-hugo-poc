---
layout: "image"
title: "Wagen"
date: "2007-02-04T12:35:30"
picture: "achterbahn2.jpg"
weight: "2"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8872
- /details85e8.html
imported:
- "2019"
_4images_image_id: "8872"
_4images_cat_id: "805"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8872 -->
Das ist der Wagen. Die Gelenke werden bei Kurven oder  Steigungen benötigt.