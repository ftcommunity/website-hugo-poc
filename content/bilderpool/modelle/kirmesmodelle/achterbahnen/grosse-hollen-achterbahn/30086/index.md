---
layout: "image"
title: "Achterbahn-Wagen Weiß 2"
date: "2011-02-19T19:14:04"
picture: "grossehoellenachterbahn2.jpg"
weight: "2"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Jonas Rupp (jorobo)"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/30086
- /detailse84b-2.html
imported:
- "2019"
_4images_image_id: "30086"
_4images_cat_id: "2217"
_4images_user_id: "1030"
_4images_image_date: "2011-02-19T19:14:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30086 -->
Hier seht ihr drei verschiedene Achterbahn-Wagen meiner neuen "Höllen-Achterbahn" ;-)
Sie unterscheiden sich in den verschieden Farben der Räder (Rot, Schwarz, Weiß). Details folgen später.

Welche Version gefällt euch am besten??
(Bitte posten, Danke)

Die Grundidee stammt von Stefan Falk, doch diese Wagen sind noch etwas verbessert!
