---
layout: "image"
title: "Mechanik"
date: "2006-07-08T23:04:53"
picture: "FT_004.jpg"
weight: "2"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/6604
- /detailsc13a.html
imported:
- "2019"
_4images_image_id: "6604"
_4images_cat_id: "569"
_4images_user_id: "430"
_4images_image_date: "2006-07-08T23:04:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6604 -->
Hier sieht man die Mechanik bzw. den Antrieb der Anlage.