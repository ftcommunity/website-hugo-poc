---
layout: "image"
title: "Das vorläufige Steuerpult"
date: "2006-12-10T13:25:12"
picture: "steuerpult1.jpg"
weight: "19"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7805
- /detailsa4e6-3.html
imported:
- "2019"
_4images_image_id: "7805"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-12-10T13:25:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7805 -->
So hier ist ein kleiner Einduck vom Steuerpult. Das Pult ist noch nicht fertig da mir leider noch ein paar Teile fehlen. Außerdem hab ich noch kein Programm geschrieben. Das heißt ich erkläre es wie es funktionieren soll :)

So die grünen Punkte (inkl. Zahlen) hab ich gemacht damit ichs besser erklären kann.

1 Start Schalter - Die Achterbahn startet nur wenn man beide Schalter gleichzeitig drückt. (sollte sie zumindest)
2 Gates Steuerung - Mit den Schaltern soll man die Gates öffnen und wieder schließen können.
3 Der Schlüssel - Bevor irgendein Befehl (z.b. "Gate auf") ausgeführt wird soll das Interface überprüfen ob der Schlüssel steckt. (Falls der Schlüssel nicht stecken sollte, sollte das Interface das Programm unterbrechen)
4 Kontrollleuchten - Die Lampen zeigen an ob der Wagen (bzw. Die Strecke) bereit ist.
5 Bremse Steuerung - Mit den Schaltern kann man die Bremse öffnen und schließen. (Aber da soll es auch Sicherheitsmechamismen geben das man das nicht wärend der Fahrt machen kann)