---
layout: "image"
title: "Der Wagen 3.0"
date: "2006-11-01T21:51:43"
picture: "wagen2.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7296
- /detailsbd4a-2.html
imported:
- "2019"
_4images_image_id: "7296"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-01T21:51:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7296 -->
Die Schrägansicht des neuen Wagens