---
layout: "image"
title: "Die Technik der Geats"
date: "2006-11-25T19:00:05"
picture: "bremseundstation4.jpg"
weight: "15"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7612
- /details5794.html
imported:
- "2019"
_4images_image_id: "7612"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7612 -->
Hier ist die Technik der Geats schön sichtbar.