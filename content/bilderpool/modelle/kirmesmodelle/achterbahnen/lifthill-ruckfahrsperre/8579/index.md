---
layout: "image"
title: "Der Aufzug von der Seite"
date: "2007-01-20T19:32:30"
picture: "wagenundlift2.jpg"
weight: "24"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/8579
- /details4980.html
imported:
- "2019"
_4images_image_id: "8579"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8579 -->
Hier sieht man den Aufzug von der Seite. Die gelben Träger sind nur Übergangsweiße da