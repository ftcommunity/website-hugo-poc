---
layout: "image"
title: "Der Wagen 4.0"
date: "2007-01-14T20:57:12"
picture: "wagen2_2.jpg"
weight: "22"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/8464
- /details02af.html
imported:
- "2019"
_4images_image_id: "8464"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2007-01-14T20:57:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8464 -->
Das ist auch der Wagen 4.0 (unfertig). Die gelben Schienen dienen nur als demonstration, dass er auch auf den normalen schienen drauf passt ohne große Bauarbeiten anzufangen.