---
layout: "image"
title: "Die Station von vorne"
date: "2006-11-25T19:00:05"
picture: "bremseundstation1.jpg"
weight: "12"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/7609
- /details5c46-3.html
imported:
- "2019"
_4images_image_id: "7609"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7609 -->
Hier ist die neue Station für die neue Achterbahn. Die Station besitzt Gates damit keine FT Männchen vor den Zug springen könnte.
Der Taster dient nur zur Befestigung des Wagens.