---
layout: "image"
title: "Der Wagen 4.0"
date: "2007-01-14T20:57:12"
picture: "wagen1_3.jpg"
weight: "21"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/8463
- /details4aca-2.html
imported:
- "2019"
_4images_image_id: "8463"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2007-01-14T20:57:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8463 -->
So sieht der Wagen 4.0 in der Realität aus (noch nicht fertig)