---
layout: "overview"
title: "Achterbahn nach Dr. Dabert
"
date: 2020-04-06T16:49:31+02:00
---

Die ganz und gar wunderbaren Modelle und Videos von "Dabert Konstruktorius" https://www.youtube.com/channel/UCJYth5ANTpbcRy7Lg_MiJdA und die sich darüber entspannende Diskussion im fischertechnik-Forum unter https://forum.ftcommunity.de/viewtopic.php?f=6&amp;t=5772 steckten mich an, auch eine Version einer Einschienen-Achterbahn mit 15 mm Schienenbreite zu bauen.
