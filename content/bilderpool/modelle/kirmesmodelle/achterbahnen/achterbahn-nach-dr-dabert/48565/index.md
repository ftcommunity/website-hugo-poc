---
layout: "image"
title: "Gesamtansicht (1)"
date: 2020-04-06T16:49:58+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Schöne an der minimalen Schiene ist, dass man Kurven, Berge und Täler realisieren kann. Hier ein Blick auf die Bahn von der Seite.

Ein Video gibt es unter https://youtu.be/DiUJX_UQIRk
