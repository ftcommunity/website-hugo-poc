---
layout: "image"
title: "Streckenführung (7)"
date: 2020-04-06T16:49:32+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick in die Schussfahrt hinein, entgegengesetzt zur Fahrtrichtung.
