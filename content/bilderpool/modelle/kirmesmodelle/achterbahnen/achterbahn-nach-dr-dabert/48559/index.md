---
layout: "image"
title: "Wagen von unten"
date: 2020-04-06T16:49:51+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der mittlere Querträger ist vorne mit zwei und hinten mit einem Federnocken gegen Verschieben gesichert. Seine beiden BS7,5 sind mittig mit einem Verbinder 15 und seitlich mit je einem Verbinder 30 verstärkt.
