---
layout: "image"
title: "Streckenführung (6)"
date: 2020-04-06T16:49:33+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Schwung geht es zum letzten Berg vor der letzten Linkskurve.
