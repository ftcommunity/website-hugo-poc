---
layout: "image"
title: "Streckenführung (1)"
date: 2020-04-06T16:49:39+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nach der ersten großen Kurve geht's schräg in die erste Schikane, gefolgt von einer scharfen Kurve zurück.
