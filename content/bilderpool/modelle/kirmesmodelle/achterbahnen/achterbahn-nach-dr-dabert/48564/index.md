---
layout: "image"
title: "Gesamtansicht (2)"
date: 2020-04-06T16:49:57+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vier Wagen durchlaufen eine Reihe Kurven und Schikanen. Alles ist unverändertes fischertechnik ohne Fremdteile.
