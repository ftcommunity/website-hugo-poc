---
layout: "image"
title: "Streckenführung (4)"
date: 2020-04-06T16:49:35+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch die ist nicht symmetrisch, sondern besteht genau genommen aus zwei Teilkurven. Der Wagen fährt dort knapp an den Verstrebungen vorbei. Und ab geht's ins nächste Tal, wiederum knapp am obendrüber liegenden vorbei und unter einem Querbalken durch.
