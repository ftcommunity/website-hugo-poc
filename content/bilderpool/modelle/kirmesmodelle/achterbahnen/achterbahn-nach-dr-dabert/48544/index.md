---
layout: "image"
title: "Streckenführung (5)"
date: 2020-04-06T16:49:34+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Es folgt die zweite Steilkurve, bevor es zur Schussfahrt geht.
