---
layout: "image"
title: "Mitnehmer"
date: 2020-04-06T16:49:47+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Als Mitnehmer für die Wagen wirkt die Kombination aus senkrecht auf der Kette stehenden BS7,5 und obenauf steckendem 32455 E-Magnet-Führungsplatte 15x20 (https://ft-datenbank.de/ft-article/1265).

Alle Achsen sind zum leichten Lauf mit Vaseline geschmiert.
