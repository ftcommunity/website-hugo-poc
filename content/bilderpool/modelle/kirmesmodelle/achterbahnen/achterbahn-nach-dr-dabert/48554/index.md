---
layout: "image"
title: "Wagen in Startposition"
date: 2020-04-06T16:49:46+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man einen Wagen kurz vor der Aufnahme durch den Mitnehmer.
