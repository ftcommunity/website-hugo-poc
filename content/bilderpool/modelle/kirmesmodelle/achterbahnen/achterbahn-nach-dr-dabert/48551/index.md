---
layout: "image"
title: "Wagenauslass"
date: 2020-04-06T16:49:42+02:00
picture: "2020-03-31 Achterbahn nach Vorbild Dr. Dabert15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ober werden die Wagen über diesen Buckel geführt und dann dem freien Lauf überlassen.
