---
layout: "image"
title: "05 Zwischenteil"
date: "2010-09-07T18:06:06"
picture: "achterbahn05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28036
- /details06fe.html
imported:
- "2019"
_4images_image_id: "28036"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28036 -->
Hier sieht man die Befstigung der Schiene am Boden. Der 50:1 Powermotor treibt die Kette an.