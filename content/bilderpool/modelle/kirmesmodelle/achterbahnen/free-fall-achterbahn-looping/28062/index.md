---
layout: "image"
title: "31 Schiene"
date: "2010-09-07T18:06:07"
picture: "achterbahn31.jpg"
weight: "31"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28062
- /details96c8.html
imported:
- "2019"
_4images_image_id: "28062"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28062 -->
Blick entlang der langen Kette. Rechts nebendran sind zwei Lichtschranken zu sehen. Bei der ersten sollte der Wagen haten, damit die Fahrgäste aussteigen können, bei der zweiten sollen wieder welche einsteigen.