---
layout: "image"
title: "14 Sperre"
date: "2010-09-07T18:06:07"
picture: "achterbahn14.jpg"
weight: "14"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28045
- /details8d88.html
imported:
- "2019"
_4images_image_id: "28045"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28045 -->
So sieht sie von unten aus. 
Endtaster gibt es keine, das funktioniert alles über Zeitsteuerung.