---
layout: "image"
title: "35 Schiene"
date: "2010-09-07T18:06:07"
picture: "achterbahn35.jpg"
weight: "35"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28066
- /details99d4.html
imported:
- "2019"
_4images_image_id: "28066"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28066 -->
Hier wird der Wagen hochgezogen. Die Schiene ist tatsächlich so gekrümmt, da sie nur oben und unten befestigt ist. Das ist aber nicht weiter schlimm und fällt beim normalen Betrachten kaum auf.