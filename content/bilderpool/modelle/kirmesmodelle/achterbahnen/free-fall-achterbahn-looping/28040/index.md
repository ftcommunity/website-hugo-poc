---
layout: "image"
title: "09 Turm"
date: "2010-09-07T18:06:07"
picture: "achterbahn09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28040
- /detailsd5a6-2.html
imported:
- "2019"
_4images_image_id: "28040"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28040 -->
Durch den gesamten Turm verläuft diese Säule aus U-Trägern.Sie soll dem Turm zusätzlichen Halt geben.