---
layout: "image"
title: "22 Drehkranz"
date: "2010-09-07T18:06:07"
picture: "achterbahn22.jpg"
weight: "22"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28053
- /details3067-2.html
imported:
- "2019"
_4images_image_id: "28053"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28053 -->
Nochmal der Drehkranz. Die zwei Zahnräder werden vom 20:1 Motor angetrieben und sind für das Seil des Wagens da.