---
layout: "image"
title: "15 Sperre"
date: "2010-09-07T18:06:07"
picture: "achterbahn15.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28046
- /detailsf9bf-3.html
imported:
- "2019"
_4images_image_id: "28046"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28046 -->
Der Vergleich macht deutlich, wie weit sie aus der Schiene herrausragen kann.