---
layout: "image"
title: "03 unten / Looping"
date: "2010-09-07T18:06:05"
picture: "achterbahn03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28034
- /detailsa906.html
imported:
- "2019"
_4images_image_id: "28034"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28034 -->
Das ist der Looping. Nur in den seltesten Fällen kommt der Wagen hier durch. Ich habe lange nach der Ursache gesucht, konnte sie aber leider nicht ausfindig machen.
Auf der Schiene hinten sieht man die Kette, mit der der Wagen zum Anfang geschoben wird. Eine vernünftige Station habe ich dann auch nicht mehr gebaut.