---
layout: "image"
title: "13 Sperre"
date: "2010-09-07T18:06:07"
picture: "achterbahn13.jpg"
weight: "13"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28044
- /details0fa6.html
imported:
- "2019"
_4images_image_id: "28044"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28044 -->
Wenn die Welle sich dreht, drückt sie die anderen Teile raus, an denen der Wagen nicht vorbei kommt.