---
layout: "image"
title: "08 Motor"
date: "2010-09-07T18:06:06"
picture: "achterbahn08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
schlagworte: ["modding"]
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28039
- /detailsbf68.html
imported:
- "2019"
_4images_image_id: "28039"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28039 -->
Der Motor für die Kette. Das Schutzblech von Autos über dem Motor ist etwas bearbeitet. Zwei der drei Schlitze an der Unterseite habe ich weggemacht, damit es draufpasst.