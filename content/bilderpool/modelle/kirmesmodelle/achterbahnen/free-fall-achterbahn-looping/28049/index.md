---
layout: "image"
title: "18 Drehkranz"
date: "2010-09-07T18:06:07"
picture: "achterbahn18.jpg"
weight: "18"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28049
- /detailsa89d.html
imported:
- "2019"
_4images_image_id: "28049"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28049 -->
Der Drehkranz, an dem die Schiene befestigt ist. 
Von rechts unten kommt übrigens das Kabel.