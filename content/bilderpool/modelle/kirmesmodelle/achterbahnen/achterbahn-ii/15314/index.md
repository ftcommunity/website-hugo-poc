---
layout: "image"
title: "Wagen am höchsten Punkt"
date: "2008-09-20T19:20:47"
picture: "achterbahn20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15314
- /detailscf15.html
imported:
- "2019"
_4images_image_id: "15314"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15314 -->
