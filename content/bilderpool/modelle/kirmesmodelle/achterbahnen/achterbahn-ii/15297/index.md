---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-20T19:20:46"
picture: "achterbahn03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15297
- /detailsb7dc.html
imported:
- "2019"
_4images_image_id: "15297"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15297 -->
