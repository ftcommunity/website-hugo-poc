---
layout: "image"
title: "Verstrebtes Grundgerüst"
date: "2008-09-20T19:20:47"
picture: "achterbahn30.jpg"
weight: "30"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15324
- /details6f9b-2.html
imported:
- "2019"
_4images_image_id: "15324"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15324 -->
Die gesamte Konstruktion ist sehr stabil und kann leicht transportiert werden.
