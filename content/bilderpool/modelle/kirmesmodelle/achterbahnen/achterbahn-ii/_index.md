---
layout: "overview"
title: "Achterbahn II"
date: 2020-02-22T07:59:12+01:00
legacy_id:
- /php/categories/1397
- /categories9ed7.html
- /categories3609.html
- /categories997c.html
- /categories1858.html
- /categories641f.html
- /categories4373.html
- /categoriesf45c-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1397 --> 
von Johannes Westphal



Die Achterbahn II ist meine Zweite Achterbahn aus Fischertechnik. Sie hat ein komplett neues Schienen- und Stützensystem, die Wagen können nun nicht mehr entgleisen. Außerdem hat sie viele weitere Verbesserungen...