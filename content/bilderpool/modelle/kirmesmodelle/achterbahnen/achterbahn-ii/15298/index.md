---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-20T19:20:46"
picture: "achterbahn04.jpg"
weight: "4"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15298
- /details1bd2-2.html
imported:
- "2019"
_4images_image_id: "15298"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15298 -->
