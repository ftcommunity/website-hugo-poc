---
layout: "image"
title: "Wagen von hinten"
date: "2008-09-20T19:20:48"
picture: "achterbahn42.jpg"
weight: "42"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15336
- /details0f17.html
imported:
- "2019"
_4images_image_id: "15336"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15336 -->
