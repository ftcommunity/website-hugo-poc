---
layout: "image"
title: "Stützkonstruktion"
date: "2008-09-20T19:20:48"
picture: "achterbahn35.jpg"
weight: "35"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15329
- /detailse8e4-2.html
imported:
- "2019"
_4images_image_id: "15329"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15329 -->
