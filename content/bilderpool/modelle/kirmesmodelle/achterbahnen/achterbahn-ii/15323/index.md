---
layout: "image"
title: "Wagen kurz vor der Station"
date: "2008-09-20T19:20:47"
picture: "achterbahn29.jpg"
weight: "29"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/15323
- /details6e85-2.html
imported:
- "2019"
_4images_image_id: "15323"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15323 -->
