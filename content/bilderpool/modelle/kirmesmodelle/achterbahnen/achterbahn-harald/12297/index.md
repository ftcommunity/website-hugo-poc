---
layout: "image"
title: "Achterbahn06.JPG"
date: "2007-10-23T19:59:35"
picture: "Achterbahn06.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12297
- /details6a04.html
imported:
- "2019"
_4images_image_id: "12297"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:59:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12297 -->
So läuft der Wagen im Gleis. Es muss noch etwas Spiel zwischen den Rädern und den Schwertern bleiben, sonst wird die Fahrt gebremst.
