---
layout: "image"
title: "Achterbahn10.JPG"
date: "2007-10-23T20:04:11"
picture: "Achterbahn10.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12299
- /details2bcb-2.html
imported:
- "2019"
_4images_image_id: "12299"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T20:04:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12299 -->
Der Wagen von der Unterseite. Die Ballastkörper stammen aus einer ft-BSB-Lokomotive.
