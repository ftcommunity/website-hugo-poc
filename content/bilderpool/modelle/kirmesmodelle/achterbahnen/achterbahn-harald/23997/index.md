---
layout: "image"
title: "Lifthill59.JPG"
date: "2009-05-10T16:28:13"
picture: "Lifthill59.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23997
- /details38c3-2.html
imported:
- "2019"
_4images_image_id: "23997"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2009-05-10T16:28:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23997 -->
Das Gespann ist fertig zur Abfahrt. Die Seilführung ist noch nicht drinnen, und da steckt sicher noch die eine oder andere (böse) Überraschung drin. Das Seil muss nämlich unter dem Gleis und über/zwischen den Fahrgästen verlaufen, denn nur dort kann es mitsamt Gleis weggeschwenkt werden. Der Schubwagen wird aber hier zur Seite verfahren, so dass das Seil bei Einfahrt des Fahrgastwagens im Weg sein wird. Ohne Seil und nur mit Reibrädern ist die Steigung nicht sicher zu bewältigen.
