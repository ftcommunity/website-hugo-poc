---
layout: "image"
title: "Achterbahn17.jpg"
date: "2008-12-29T16:45:34"
picture: "Achterbahn17.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16758
- /details0313-2.html
imported:
- "2019"
_4images_image_id: "16758"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2008-12-29T16:45:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16758 -->
Das obere Ende des Lifthill.

Die beiden Wagen haben sich auf verschiedenen Gleisen voneinander getrennt, nachdem der obere Wagen auf die breitere Bahn mit den V-Platten und den U-Trägern aufgefahren ist und nun auf seinene oberen Rollenpaaren fährt.
 
Die langen Metallachsen besorgen den letzten Schubser über die Kuppe hinweg. Danach kann der obere Wagen wieder zur Talstation fahren, um den nächsten Fahrgastwagen zu übernehmen.

Der Lifthill wird auf ca. 1,5 m Länge freitragend aufgehängt, weil er nur ganz oben und ganz unten (eben außerhalb der Bahn des oberen Wagens) überhaupt befestigt werden kann. Die gelbe Stütze links unten ist nur fürs Foto drunter.
