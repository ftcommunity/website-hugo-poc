---
layout: "image"
title: "Achterbahn18.jpg"
date: "2008-12-29T16:30:23"
picture: "Achterbahn18.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16756
- /details237c.html
imported:
- "2019"
_4images_image_id: "16756"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2008-12-29T16:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16756 -->
Kaum ist ein Jahr vergangen, schon (!) gibt es was Neues.

Der Wagen fährt jetzt mit den gelben Freilaufnaben. Das könnte noch ein Quentchen Reibung einsparen.

Das Gleis wird mit einem spiegelverkehrt aufgesetzten Flachträger *absolut* entgleisungssicher. Zwischen Ober- und Unterträger kommt Material in Stärke von zwei S-Streben. Wenn man das Gleisstück hier um seine Längsachse dreht, bleibt der Wagen drinnen und dreht mit. Damit ist also auch die "Schraube" als Bahnfigur möglich.
