---
layout: "image"
title: "Wagen bei der Abfahrt"
date: "2008-06-21T18:28:24"
picture: "achterbahn23.jpg"
weight: "23"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14741
- /details0072.html
imported:
- "2019"
_4images_image_id: "14741"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14741 -->
