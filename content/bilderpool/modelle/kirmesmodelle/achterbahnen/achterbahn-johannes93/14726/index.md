---
layout: "image"
title: "Lange Gerade"
date: "2008-06-21T18:28:23"
picture: "achterbahn08.jpg"
weight: "8"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14726
- /detailsfe09-2.html
imported:
- "2019"
_4images_image_id: "14726"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14726 -->
Auf der langen Gerade erreicht der Wagen seine Höchstgeschwindigkeit.
