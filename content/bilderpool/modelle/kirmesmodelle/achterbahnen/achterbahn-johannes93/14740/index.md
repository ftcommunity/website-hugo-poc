---
layout: "image"
title: "Wagen auf der Spitze"
date: "2008-06-21T18:28:24"
picture: "achterbahn22.jpg"
weight: "22"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14740
- /detailsb9ae.html
imported:
- "2019"
_4images_image_id: "14740"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14740 -->
