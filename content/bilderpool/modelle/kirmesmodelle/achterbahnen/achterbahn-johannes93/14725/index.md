---
layout: "image"
title: "Kurve 2"
date: "2008-06-21T18:28:23"
picture: "achterbahn07.jpg"
weight: "7"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/14725
- /details3b29.html
imported:
- "2019"
_4images_image_id: "14725"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14725 -->
