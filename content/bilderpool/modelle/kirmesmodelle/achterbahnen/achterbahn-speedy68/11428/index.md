---
layout: "image"
title: "Achterbahn Bild 1"
date: "2007-09-01T15:33:19"
picture: "Achterbahn_Bild_1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["ft", "Fischertechnik", "Achterbahn"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/11428
- /details70af.html
imported:
- "2019"
_4images_image_id: "11428"
_4images_cat_id: "1023"
_4images_user_id: "409"
_4images_image_date: "2007-09-01T15:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11428 -->
Wegen der Nachfrage - habe leider nur eines meiner Modelle fotografiert, ca. 3700 Teile wurden benötigt. Damals gab es leider keine Digitalkameras, die besten Bilder hatte ich an ft geschickt.
