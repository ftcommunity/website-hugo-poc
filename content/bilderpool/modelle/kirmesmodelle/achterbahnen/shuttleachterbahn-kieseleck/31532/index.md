---
layout: "image"
title: "Magnetwagen"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck12.jpg"
weight: "12"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31532
- /details923d-4.html
imported:
- "2019"
_4images_image_id: "31532"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31532 -->
Das ist der Magnetwagen der den Wagen hochzieht.