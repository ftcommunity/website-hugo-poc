---
layout: "image"
title: "Federbalken"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck07.jpg"
weight: "7"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31527
- /details08be.html
imported:
- "2019"
_4images_image_id: "31527"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31527 -->
Das ist der Federbalken, der den Wagen abfängt.