---
layout: "overview"
title: "Shuttleachterbahn (Kieseleck)"
date: 2020-02-22T07:59:36+01:00
legacy_id:
- /php/categories/2346
- /categories01ad.html
- /categories7824.html
- /categoriesa85f.html
- /categories49df.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2346 --> 
Diese Achterbahn hat eine gewisse Ähnlichkeit mit einer Free Fall Achterbahn: Ein Wagen wird eine Steigung hochgezogen, schließlich losgelassen und saust dann den Berg hinab. Ein Shuttlecoaster hat ein offenes Gleissystem, das zweimal durchfahren wird (vorwärts und rückwärts). Meine Achterbahn ist möglichst einfach gehalten so verzichte ich z.B. auf Lichtschranken.