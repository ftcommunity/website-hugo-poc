---
layout: "image"
title: "Station"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck04.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31524
- /details230f-2.html
imported:
- "2019"
_4images_image_id: "31524"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31524 -->
Hier sieht man die Station mit der Gate- (bzw. Schaltpult-) Ampel.