---
layout: "image"
title: "Catwalk"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck11.jpg"
weight: "11"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31531
- /detailsdbd5.html
imported:
- "2019"
_4images_image_id: "31531"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31531 -->
Der Catwalk für Notfälle