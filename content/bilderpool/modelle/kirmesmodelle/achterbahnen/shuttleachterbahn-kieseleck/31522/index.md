---
layout: "image"
title: "Schienensystem"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck02.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/31522
- /details1e89-2.html
imported:
- "2019"
_4images_image_id: "31522"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31522 -->
Die Schienen bestehen ausschließlich aus 36333, "Laufschiene 240 mm".