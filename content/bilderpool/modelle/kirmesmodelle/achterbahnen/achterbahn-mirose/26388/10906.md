---
layout: "comment"
hidden: true
title: "10906"
date: "2010-02-15T17:41:01"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Udo2,

die Winkelträger 15 sind auf Bogenspannung. Urspünglich waren da Flachträger eingebaut, aber der herunterfahrende Wagen (dürfte somit etwa 20 km/h) daherkommen) hat sie derart verformt, daß er sich verspießt hat und den Schwung nicht optimal mitgenommen hat.

Viele Grüße

Mirose