---
layout: "image"
title: "untere Rollen von unten"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_44.jpg"
weight: "36"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26440
- /detailsc19d.html
imported:
- "2019"
_4images_image_id: "26440"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26440 -->
