---
layout: "image"
title: "Damit die Kette..."
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_30.jpg"
weight: "30"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["modding"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26398
- /detailsd212.html
imported:
- "2019"
_4images_image_id: "26398"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26398 -->
Damit die Kette nicht nach oben schwingt, habe ich die Bausteine 7,5 'etwas' bearbeitet.
