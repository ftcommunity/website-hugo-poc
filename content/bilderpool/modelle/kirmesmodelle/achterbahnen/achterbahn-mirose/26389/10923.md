---
layout: "comment"
hidden: true
title: "10923"
date: "2010-02-16T11:14:28"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hab ichs doch fast übersehen. Ohne Klothoide ist so ein Looping eigentlich kein "richtiger" Looping. Das kompensiert zweifellos zwischen den dynamischen und statischen Kräften des Systems vor Ort.