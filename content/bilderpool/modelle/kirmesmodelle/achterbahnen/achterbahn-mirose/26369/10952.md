---
layout: "comment"
hidden: true
title: "10952"
date: "2010-02-18T14:21:48"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo,

Da hast recht, ft benützt fleißig die flexible Geometrie. Jüngstes Beispiel ist der Super Fun Park: Lt. Packshot wird das Rad mit einer Strebe 120, einer Strebe 90 und einer Lasche 15 mit der Nabe verbunden, ergibt einen Abstand von 225 mm.
Wenn man jedoch das Ganze genau zeichnet, ergibt sich ein Abstand von 225,923 mm.
Es mag der Einwand gelten, daß das nicht sehr viel mehr ist, sieht man sich jedoch den Packshot an, sieht man, daß der Winkelträger 120 und der Winkelträger 15 des Rades etwas verschoben ist.
Die bessere Lösung wäre die Verwendung folgender Streben: 84,8 + Lasche 21,2 + Strebe 120, das ergibt dann einen Abstand von 226 mm.

Ich zeichne mit AutoCAD.

Unter dem Teppich liegt ein Parkettboden.
Der Vorschlag mit dem Doppelklebeband klingt gut. Damit werde ich in E.-B. das Modell auf den Tisch kleben, damit es nicht verrutscht. - Danke für den Vorschlag. Dann kann ich mit den Transport der Holzplatten ersparen.

Viele Grüße

Mirose

P.S.: das Video findest Du auf

[URL=http://www.youtube.com/watch?v=sXhuZxIf68g/URL]