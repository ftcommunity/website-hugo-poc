---
layout: "comment"
hidden: true
title: "10858"
date: "2010-02-14T00:10:38"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

schön, daß Dir meine Hochschaubahn gefällt.
Ich habe die ganze Bahn mit ACAD durchkonstruiert. Die Einlaufkurve in und die Auslaufkurve aus dem Looping entspricht annähernd einer Klothoide (Übergangsbogen). (d.h. die Kurve wird allmählich immer enger; das Material wird nicht so hoch beansprucht, der Schwung des Wagens wird besser ausgenutzt)

Viele Grüße

Mirose