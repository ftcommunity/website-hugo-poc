---
layout: "image"
title: "Gesamtansicht"
date: "2010-02-13T22:21:36"
picture: "Hochschaubahn_01.jpg"
weight: "1"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Achterbahn", "Looping"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26369
- /detailsee8b.html
imported:
- "2019"
_4images_image_id: "26369"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26369 -->
Gesamtansicht meiner Hochschaubahn mit Looping.
Einige Daten:
Platzbedarf: ca. 2,9 x 1,0 m
Max. Höhe: 1,7 m
Spurweite: 65 mm
Gleislänge: 13,7 m
Kettenlänge Aufzug: ca. 6,0 m

Das Video:
http://www.youtube.com/watch?v=sXhuZxIf68g
_
