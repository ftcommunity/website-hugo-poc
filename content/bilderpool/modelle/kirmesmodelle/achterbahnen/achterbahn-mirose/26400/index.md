---
layout: "image"
title: "Einfahrt Ein- / Ausstiegsstelle"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_32.jpg"
weight: "32"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26400
- /detailsd5be-2.html
imported:
- "2019"
_4images_image_id: "26400"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26400 -->
Im Baustein 30 zwischen der Stiege und dem Gleis befindet sich ein Reedkontakt (auch zu erkennen am grünen und roten Anschlußdraht)

Das Video:

http://www.youtube.com/watch?v=sXhuZxIf68g
_
