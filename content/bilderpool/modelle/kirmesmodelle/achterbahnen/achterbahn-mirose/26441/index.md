---
layout: "image"
title: "Wagenunterseite"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_45.jpg"
weight: "37"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["modding"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26441
- /detailsfcc3.html
imported:
- "2019"
_4images_image_id: "26441"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26441 -->
linkes oben der Möbelmagnet;
in der Mitte der kurze rote Stift ist der bearbeitete S-Riegel 4mm = Mitnehmer, der in die Kette eingreift.
