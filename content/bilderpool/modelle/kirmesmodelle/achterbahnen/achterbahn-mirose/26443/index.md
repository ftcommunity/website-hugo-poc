---
layout: "image"
title: "Vorderansicht Wagen"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_47.jpg"
weight: "39"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26443
- /details2d30.html
imported:
- "2019"
_4images_image_id: "26443"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26443 -->
Das Video:

http://www.youtube.com/watch?v=sXhuZxIf68g
_
