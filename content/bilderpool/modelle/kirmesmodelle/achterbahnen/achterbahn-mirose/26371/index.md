---
layout: "image"
title: "Looping"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_03.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26371
- /details9062.html
imported:
- "2019"
_4images_image_id: "26371"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26371 -->
