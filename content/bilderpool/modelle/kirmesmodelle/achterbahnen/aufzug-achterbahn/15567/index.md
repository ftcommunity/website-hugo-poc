---
layout: "image"
title: "Aufzug für die Achterbahn(7)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn07.jpg"
weight: "7"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15567
- /details5078-2.html
imported:
- "2019"
_4images_image_id: "15567"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15567 -->
Von Oben kann man alles sehen.