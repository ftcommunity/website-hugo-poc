---
layout: "image"
title: "Aufzug für die Achterbahn(6)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn06.jpg"
weight: "6"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- /php/details/15566
- /details01be.html
imported:
- "2019"
_4images_image_id: "15566"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15566 -->
Von Oben kann man alles sehen.