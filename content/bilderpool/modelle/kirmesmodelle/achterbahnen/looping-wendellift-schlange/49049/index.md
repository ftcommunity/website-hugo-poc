---
layout: "image"
title: "Looping und Wendelsteigung"
date: 2021-03-09T15:56:41+01:00
picture: "looping19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nach eineinhalb Bahnen auf der Innenseite der Wendelstrecke kommt der Ball auf der gegenüberliegenden Seite oben heraus.