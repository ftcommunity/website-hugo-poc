---
layout: "image"
title: "Die erste Steigung"
date: 2021-03-09T15:56:47+01:00
picture: "looping14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die hier linke Bahn ist der Fuß des großen Loopings mit Bogenstücken 30°. Rechts daneben sieht man den Übergang zum kleinen Looping.