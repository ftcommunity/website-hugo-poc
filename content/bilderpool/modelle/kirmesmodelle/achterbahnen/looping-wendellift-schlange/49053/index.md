---
layout: "image"
title: "Großer und kleiner Looping trennen sich"
date: 2021-03-09T15:56:45+01:00
picture: "looping15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ab hier besteht der kleine Looping - rechts - aus Bogenstücken 60°.