---
layout: "image"
title: "Start"
date: 2021-03-09T15:56:51+01:00
picture: "looping10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In dem Kraftmesser ist die stärkere der beiden mitgelieferten Federn eingehängt. Wenn man die rote Zunge ganz herauszieht und dann zurückschnalzen lässt, schlägt die rote Achsaufnahme gegen den bewusst ganz leicht gebauten Hebel, der auf der anderen Seite den Stoß an den Tischtennisball weitergibt.