---
layout: "image"
title: "Gesamtansicht 2"
date: 2021-03-09T15:56:35+01:00
picture: "looping03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Ball rast mit einer überraschend hohen Geschwindigkeit in Null Komma Nichts durch den großen Looping, an den sich gleich danach der kleine anschließt.