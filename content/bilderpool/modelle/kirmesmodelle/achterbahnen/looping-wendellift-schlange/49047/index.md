---
layout: "image"
title: "Übergang zur Schlange"
date: 2021-03-09T15:56:38+01:00
picture: "looping20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kommt der Ball aus der senkrechten "Wand" wieder auf eine waagerechte Bahn, die sich nochmal durch den großen Looping bis zurück zum Start schlängelt. Ab hier muss der Ball seine Drehrichtung wieder in die waagerechte ändern.