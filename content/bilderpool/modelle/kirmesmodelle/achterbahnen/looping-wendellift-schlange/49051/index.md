---
layout: "image"
title: "Übergang in die Wendelsteigung"
date: 2021-03-09T15:56:43+01:00
picture: "looping17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man, wo der Ball aus dem kleinen Looping einfach in die senkrechte "Wand" der Wendelsteigung gefeuert wird.

In der senkrechten Bahn muss der Ball seine Drehrichtung aus der horizontalen in die vertikale Achse ändern. Das sieht und hört man schleifend sehr schön im Zeitlupenteil des Videos.