---
layout: "image"
title: "Gesamtansicht 1"
date: 2021-03-09T15:56:40+01:00
picture: "looping02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bahn ist ganz reizvoll, aber der Reihe nach: Unten sieht man einen alten fischertechnik-Kraftmesser (aus der Zusatzpackung ft-25 aus den 1970er Jahren). Die rote Zunge ist mit der starken der beiden mitgelieferten Federn eingehängt. Man zieht sie heraus und lässt sie zurückschnalzen. Das gibt dem Tischtennisball einen heftigen Kick.

Ein Video gibt es unter https://www.youtube.com/watch?v=Mk9mcYzyHsE&t=165s