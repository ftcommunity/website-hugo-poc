---
layout: "image"
title: "Loopings und Wendelsteigung"
date: 2021-03-09T15:56:42+01:00
picture: "looping18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ineinander verschlungen sieht man hier die beiden Loopings und die Wendel, in der der Ball wie ein Obertodesteufelsdriver auf der Innenseite nach oben (!) rast.