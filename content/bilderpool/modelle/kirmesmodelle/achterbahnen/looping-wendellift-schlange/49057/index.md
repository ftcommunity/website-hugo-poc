---
layout: "image"
title: "Der Kraftmesser-Killer 1"
date: 2021-03-09T15:56:50+01:00
picture: "looping11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man, warum man das Modell so wie ich es gebaut habe KEINESFALLS NACHBAUEN SOLLTE. Nach ca. 20 Stößen fing die Achsaufnahme nämlich an, zu reißen. Die ist normalerweise kreisrund geschlossen und kann eine Achse tragen, an der irgendwas befestigt wird.