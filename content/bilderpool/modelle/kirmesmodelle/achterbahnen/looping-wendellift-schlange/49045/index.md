---
layout: "image"
title: "Ansicht von unten"
date: 2021-03-09T15:56:36+01:00
picture: "looping22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der Aufbau von der Unterseite gesehen.