---
layout: "image"
title: "Der Kraftmesser-Killer 2"
date: 2021-03-09T15:56:49+01:00
picture: "looping12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier mal ran gezoomt. Der Riss setzt sich von der Achsaufnahme an der dem Stoß entfernten Seite komplett durchs rote Plastik fort. Entsetzen machte sich also breit!