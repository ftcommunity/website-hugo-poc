---
layout: "image"
title: "Das Original"
date: 2021-03-09T15:56:53+01:00
picture: "looping01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ursprünglich wollte ich dieses Original aus dem fischertechnik-Clubheft 1976-2 nachbauen. Anlässlich der geringen Auflösung der PDF entschloss ich mich aber dann doch für einen Eigenbau, der auch gleich etwas größer ausfallen durfte.