---
layout: "image"
title: "Wo alles beginnt"
date: 2021-03-09T15:56:27+01:00
picture: "looping09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist ein Ur-fischertechnik-Kraftmesser aus dem ehemaligen Zusatzkasten ft-25. Der kam 1970 heraus. 1975 gab es eine neuere Fassung mit mehr Nuten und mehr Anbaumöglichkeiten an der roten Zunge. Hier vorne wird er von der schwarzen Achse 40 unten fixiert, die, weil es sonst zu stramm ist, unten an Prüfriegeln (die durchsichtigen etwas weichen aus dem hobby-S) gehalten wird.