---
layout: "image"
title: "Kugelabschluss"
date: 2021-03-09T15:56:48+01:00
picture: "looping13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man, wo der Tischtennisball einen heftigen Impuls bekommt und geradezu fortgeschossen wird. Man sieht's im Video sehr gut, wie der durch die Bahn rast.