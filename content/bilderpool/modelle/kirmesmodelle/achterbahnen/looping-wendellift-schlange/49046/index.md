---
layout: "image"
title: "Schlange"
date: 2021-03-09T15:56:37+01:00
picture: "looping21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

An verschiedenen Stellen sorgen solche Abstandshalter für die richtige Bahnbreite. Hier ist die Schlangenbahn auch noch mit dem Looping fest verbunden.