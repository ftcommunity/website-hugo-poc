---
layout: "image"
title: "Loopings"
date: 2021-03-09T15:56:44+01:00
picture: "looping16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Vordergrund der Rest vom großen Looping vor dem Übergang zum kleinen. Im Hintergrund der Start und der Beginn des großen Loopings.