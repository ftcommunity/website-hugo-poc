---
layout: "image"
title: "Inferno-Detail30.JPG"
date: "2005-04-22T11:17:24"
picture: "Inferno-Detail30.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4068
- /details3d70.html
imported:
- "2019"
_4images_image_id: "4068"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-22T11:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4068 -->
Ein Blick in den "Kopf". 

Von links kommt die Antriebswelle aus dem Gegengewicht. Die Kraftübertragung erfolgt über 4xZ10, von den hier nur die beiden links zu sehen sind. Die Rastkegel werden nur eingeklipst, damit sich im Betrieb nichts mehr verziehen kann.

Der Schleifring hat irgendwo noch einen Wackler, deswegen sind hier immer zwei Bahnen parallelgeschaltet.