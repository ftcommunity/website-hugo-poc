---
layout: "image"
title: "Inferno52.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno52.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4004
- /details4bc8.html
imported:
- "2019"
_4images_image_id: "4004"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4004 -->
Die seitlichen Stützen sind ausgeklappt und alle Stützen heruntergekurbelt. Noch wird der Arm von zwei Doppelstreben (mit Haken) festgehalten.