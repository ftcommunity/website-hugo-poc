---
layout: "image"
title: "Inferno-Gondel12.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno-Gondel12.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4007
- /detailsfd03.html
imported:
- "2019"
_4images_image_id: "4007"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4007 -->
Meine Gondel fasst nur 2x8 Personen, dafür hat sie aber einen eigenen Antrieb bekommen.