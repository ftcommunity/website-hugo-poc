---
layout: "image"
title: "Inferno-Detail28.JPG"
date: "2005-04-22T11:17:24"
picture: "Inferno-Detail28.jpg"
weight: "25"
konstrukteure: 
- " "
fotografen:
- " "
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4069
- /details5e7b.html
imported:
- "2019"
_4images_image_id: "4069"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-22T11:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4069 -->
Hinten drin ist noch reichlich Platz für Seilwinde und Interface.