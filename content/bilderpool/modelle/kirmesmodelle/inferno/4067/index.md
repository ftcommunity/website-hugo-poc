---
layout: "image"
title: "Inferno-Detail22.JPG"
date: "2005-04-22T11:17:24"
picture: "Inferno-Detail22.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4067
- /detailsc4d7.html
imported:
- "2019"
_4images_image_id: "4067"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-22T11:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4067 -->
Als Antrieb für den Hauptarm dienen zwei Motoren, die über ein Differenzial zusammgeschaltet sind. Das Robo-Interface im Fahrzeugheck soll sie so ansteuern, dass sie beim Anfahren gegensinnig und mit leicht unterschiedlicher Geschwindigkeit arbeiten, und danach allmählich auf volles Tempo in einer Richtung übergehen.

Die beiden BS15 mit Loch sind auf 10,5 mm Stärke heruntergedreht und -gefräst worden. Dadurch rasten die Kupplungsstücke jetzt sauber und das Differenzial fügt sich ins Rastsystem ein, wie es sich gehört.

Im Drehkranz arbeitet ein Schleifring (Eigenbau Modell F, für Details siehe http://www.ftcommunity.de/categories.php?cat_id=346 ).

Das Z10 links in Bildmitte gehört zu einer kurzen Welle, die quer über den Drehkranz hinwegführt. Wenn die Arme für Gegengewicht und Gondel eingerastet sind, kämmen auf beiden Seiten der Welle Paare von Z10 ineinander.
