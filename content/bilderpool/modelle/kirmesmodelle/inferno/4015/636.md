---
layout: "comment"
hidden: true
title: "636"
date: "2005-07-28T00:20:11"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nu hätt' ich aber doch zwei FragenTach Harald!

a) Wie hast Du denn bitte die Gelenke hinten an der gerade waagerecht liegenden Bodenplatte befestigt?

b) Was sind das für kurze Teile vorne, mit zwei Zapfen scheinbar, mit denen Du die Streben (4 Paare) miteinander verbunden hast?

Ist das Standard ft oder was eigengefrickeltes?

Gruß und nochmals Kompliment,
Stefan