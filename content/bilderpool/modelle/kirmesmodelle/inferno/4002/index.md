---
layout: "image"
title: "Inferno44.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno44.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4002
- /details6f0c.html
imported:
- "2019"
_4images_image_id: "4002"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4002 -->
Und so sieht das Teil in ft aus. Hubzylinder gibt es allerdings nicht (zu schwer und zu sperrig), dafür habe ich hinten eine Seilwinde drin. Das Gegengewicht liegt hier obenauf und wird nicht abmontiert, denn das wäre wegen der Elektrik darin zu umständlich.
