---
layout: "image"
title: "Inferno50.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno50.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4003
- /detailsffff.html
imported:
- "2019"
_4images_image_id: "4003"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4003 -->
Wenn das Radgestell vorne weg ist, kann der Arm durch Umstecken der Stahlachse auf dem Boden abgestützt werden. Das gibts beim Original nicht und hat sich eigentlich "nur so" ergeben.
