---
layout: "image"
title: "Inferno_804.JPG"
date: "2005-04-19T11:25:10"
picture: "Inferno_804.jpg"
weight: "1"
konstrukteure: 
- "Vorbild: Fa. Mondial"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4000
- /details80a0.html
imported:
- "2019"
_4images_image_id: "4000"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4000 -->
Das ist das "Inferno" von Mondial. Der Betreiber hier hat sein Gerät "Night Fly" getauft, es gibt in Deutschland noch zwei andere, und "Night Fly" ist inzwischen wieder weiterverkauft. Na egal, jedenfalls macht das gute Stück ordentlich was her.

Die Fahrgäste sitzen zu 2x19 in zwei Halbgondeln links und rechts von der vertikalen Drehachse, die im linken Bildviertel zu sehen ist. Die Halbgondeln rotieren frei (bis auf eine Bremse) um eine waagerechte Achse. Die dritte Drehachse ist diejenige, um die der ganze Arm rotiert.

Wenn das Gerät einmal aufgebaut ist, bleibt der vertikale Träger unverändert stehen (bei anderen Fahrgeschäften kann der Hauptträger gesenkt werden). Beim Inferno wird die Plattform unter der Gondel mit kombinierter Hydraulik und Pneumatik gehoben und gesenkt (wieso da beide Sorten Zylinder drunter sind, -- ??), im Moment ist die Plattform, auch "Podium" genannt, hochgefahren, damit die Fahrgäste zusteigen können.
