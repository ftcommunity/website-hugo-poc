---
layout: "image"
title: "Gesamtansicht von vorn"
date: "2008-08-30T13:50:53"
picture: "Schiffschaukel_33.jpg"
weight: "3"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15140
- /detailsb892.html
imported:
- "2019"
_4images_image_id: "15140"
_4images_cat_id: "1386"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15140 -->
