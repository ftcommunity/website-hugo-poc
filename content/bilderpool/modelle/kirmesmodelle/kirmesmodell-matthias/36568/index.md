---
layout: "image"
title: "10 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell10.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36568
- /details0857.html
imported:
- "2019"
_4images_image_id: "36568"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36568 -->
Die Kompressoren laufen unabhängig vom Interface, die Druckabschaltung rechts unterbricht die Stromzufuhr, wenn der Tank voll ist.