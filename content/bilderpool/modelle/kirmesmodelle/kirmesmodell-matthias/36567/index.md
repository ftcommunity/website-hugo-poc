---
layout: "image"
title: "09 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell09.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36567
- /details5acb.html
imported:
- "2019"
_4images_image_id: "36567"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36567 -->
Ein Tank, zwei Pollin-Kompressoren und 4 Magnetventile. Diese hatte ich schon an anderer Stelle vorgestellt (http://www.ftcommunity.de/details.php?image_id=36325)