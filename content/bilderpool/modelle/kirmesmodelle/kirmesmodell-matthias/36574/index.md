---
layout: "image"
title: "16 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell16.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36574
- /details4bbd.html
imported:
- "2019"
_4images_image_id: "36574"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36574 -->
Zwischen den beiden Z10 ist der Adapter zu sehen, der zwischen zwei BS7,5 geklemmt ist und so mitdreht. Funktioniert wunderbar.