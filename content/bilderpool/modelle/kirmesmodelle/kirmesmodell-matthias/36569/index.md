---
layout: "image"
title: "11 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36569
- /details799c.html
imported:
- "2019"
_4images_image_id: "36569"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36569 -->
Der Zylinder ist für die linke Passergiertreppe zuständig.