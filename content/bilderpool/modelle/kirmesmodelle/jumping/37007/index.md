---
layout: "image"
title: "Jumping6157"
date: "2013-06-01T23:37:48"
picture: "IMG_6157.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37007
- /details1377.html
imported:
- "2019"
_4images_image_id: "37007"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:37:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37007 -->
Das ist der Zwischenstand von der Konvention 2011. Die Grundfunktionen für den Spielbetrieb sind drin: Heben/Senken, Drehen des Ganzen, Rotieren der Gondeln. Die seitlichen Stützen können eingeklappt werden, der Mast kann von Hand flach gelegt werden;
