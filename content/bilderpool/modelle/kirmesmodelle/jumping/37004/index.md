---
layout: "image"
title: "Jumping906.jpg"
date: "2013-06-01T23:12:26"
picture: "JP906_mit.JPG"
weight: "3"
konstrukteure: 
- "Fa. Huss"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37004
- /details0c2a.html
imported:
- "2019"
_4images_image_id: "37004"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:12:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37004 -->
Der Jumping bei Nacht. Mit Strahlern, LIcht und Nebel haben auch diejenigen etwas davon, die nicht in den Gondeln mit fahren.


München, Oktoberfest 2004
