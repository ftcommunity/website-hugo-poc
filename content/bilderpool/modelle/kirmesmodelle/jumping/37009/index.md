---
layout: "image"
title: "Jumping8607"
date: "2013-06-01T23:49:27"
picture: "IMG_8607.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37009
- /details55fe-2.html
imported:
- "2019"
_4images_image_id: "37009"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:49:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37009 -->
Hinten links und rechts befinden sich ausklappbare Stützen. Der Motor dazwischen betätigt ein Nylonseil, das die Stützen zum Aufrichten des Mastes in ihre Positionen lenkt.
