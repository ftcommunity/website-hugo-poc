---
layout: "image"
title: "Jumping9558.JPG"
date: "2013-09-30T18:25:40"
picture: "Jumping9558.JPG"
weight: "30"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37461
- /details4a23-2.html
imported:
- "2019"
_4images_image_id: "37461"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:25:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37461 -->
Der "Jumping" mit allem, was dazu gehört, wird auf drei Wagen transportiert. Der Mittelbau (oben) trägt schon ein paar Platten fürs Podium an seinen Seiten. Der Rest und die Gondeln fahren auf den anderen Wagen. Der untere Wagen enthält einen Kompressor, damit ein Teil des Podiums (zwei Elemente vorn und hinten in der Mitte vom fertig aufgebauten Gerät) abgesenkt werden können.
