---
layout: "image"
title: "Jumping8624"
date: "2013-06-02T10:11:28"
picture: "IMG_8624.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37015
- /details4949-2.html
imported:
- "2019"
_4images_image_id: "37015"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T10:11:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37015 -->
Hier hat der kleine Hilfsmast seine Arbeit schon getan und den Mast auf seiner Schlittenfahrt nach hinten durch Abstützen an den beiden Winkeln 60° soweit aufgerichtet, dass der vordere Hilfsmast in seinen Abstützpunkt (die Metallachse, auf der die rote Nabe sitzt) eingefädelt ist und die weitere Arbeit übernehmen kann.
