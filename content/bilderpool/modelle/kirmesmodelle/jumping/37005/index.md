---
layout: "image"
title: "Jumping204"
date: "2013-06-01T23:20:25"
picture: "JP204_mit.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37005
- /details93c2.html
imported:
- "2019"
_4images_image_id: "37005"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:20:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37005 -->
Aufbau: der Mast steht, jetzt schweben die Gondeln am Kranseil herein. Hier ist der Bolzen gerade gesetzt worden, der die Gondel mit dem Trägerarm verbindet.
