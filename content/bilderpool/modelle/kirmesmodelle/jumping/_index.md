---
layout: "overview"
title: "Jumping"
date: 2020-02-22T08:00:32+01:00
legacy_id:
- /php/categories/2752
- /categoriese8e6.html
- /categoriesa845.html
- /categories143b.html
- /categories5e6e.html
- /categoriesfff0-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2752 --> 
Ein Fahrgeschäft mit fünf rotierenden Fahrgastgondeln, die auf und ab geschwenkt werden und sich um einen zentralen Mast drehen.