---
layout: "comment"
hidden: true
title: "18100"
date: "2013-06-16T20:42:37"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, sie tun es. Beim Transport liegt ja auch der Mast nach vorne bündig, da kommt hinten nur ein Teil vom Gewicht an. Außerdem sind es 8 Federn. Wenn aber die hinterste Achse einfedert, drückt sie die Hilfsmasten nach oben.

Gruß,
Harald