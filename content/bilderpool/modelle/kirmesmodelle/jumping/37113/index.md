---
layout: "image"
title: "Jumping8701.jpg"
date: "2013-06-14T07:32:06"
picture: "IMG_8701.JPG"
weight: "29"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37113
- /details7979-3.html
imported:
- "2019"
_4images_image_id: "37113"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T07:32:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37113 -->
Der Mast, aus dem Wagen heraus gezogen und frei stehend (mit ein bisschen Stütze von hinten, weil die Elektronik dort den Schwerpunkt hin zieht).
