---
layout: "image"
title: "Jumping8699.jpg"
date: "2013-06-14T07:28:09"
picture: "IMG_8699.JPG"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37112
- /details4d0d.html
imported:
- "2019"
_4images_image_id: "37112"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T07:28:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37112 -->
Vorderteil von unten. Die IR-Fernbedienung am Dolly hat seitdem ein besseres Plätzchen gefunden. Das Nylonseil ist gestrafft, weil der Mast komplett aus dem Wagen heraus gefahren ist und jetzt daneben liegt.
