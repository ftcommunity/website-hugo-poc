---
layout: "image"
title: "Jumping6117"
date: "2013-06-02T09:53:25"
picture: "IMG_6117.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37013
- /detailsed74.html
imported:
- "2019"
_4images_image_id: "37013"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T09:53:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37013 -->
Die Schleifringe kommen nicht ganz ohne Fummelei aus: die Federn sind ja schön und gut, aber irgendwie muss da noch "Strom dran", ohne dass es einen Kurzschluss gibt. Dazu wurden zwei Stücke Kupferdraht in die Nuten der BS7,5 geklemmt und außen mit Steckern versehen.
