---
layout: "image"
title: "Jumping9553.JPG"
date: "2013-09-30T18:31:26"
picture: "Jumping9553.JPG"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37463
- /detailsc2b2.html
imported:
- "2019"
_4images_image_id: "37463"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:31:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37463 -->
Wagen Nummer 2, mit Kompressor und zwei pneumatisch absenkbaren Podiumselementen oben drauf. Die Treppe am Heck wird zum Transport einfach hoch geklappt.
