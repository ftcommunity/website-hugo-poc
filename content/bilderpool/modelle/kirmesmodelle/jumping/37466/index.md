---
layout: "image"
title: "Jumping9710.JPG"
date: "2013-09-30T18:40:33"
picture: "Jumping9710.JPG"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37466
- /details9b55.html
imported:
- "2019"
_4images_image_id: "37466"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:40:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37466 -->
Der Aufbau ist fertig. Wagen Nummer 2 wird Teil des Unterbaus, das Podium ist vorn und hinten in der Mitte abgesenkt.
