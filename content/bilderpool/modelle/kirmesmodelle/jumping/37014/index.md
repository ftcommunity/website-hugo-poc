---
layout: "image"
title: "Jumping8633"
date: "2013-06-02T10:04:04"
picture: "IMG_8633.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37014
- /detailsf338.html
imported:
- "2019"
_4images_image_id: "37014"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T10:04:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37014 -->
Zum Aufrichten aus eigener Kraft dienen gleich zwei Hilfsmasten, die bei Nichtgebrauch im Fahrwerk verschwinden. Dazu wird der Schlitten mit dem Mast per Schneckenantrieb nach hinten geschoben und über zwei Hilfsmasten aufgerichtet.

Der kleine, hintere Hilfsmast (mit roten BS30+BS15) liegt auf dem vorderen auf. Der vordere (mit U-Träger-Adapter an der Spitze) enthält einen Teleskopschub aus einem ft-Alu.

Die Spinnfäden sind keine Spinnfäden, sondern sind ein Nylonseil, das die Hilfsmasten aufrichtet und exakt in ihre Zielstellen am Hauptmast einfädelt. Dazu führt es von unten durch die Spitze des hinteren Mastes, dann frei hinauf zu zwei Winkeln 60° am Hauptmast (hier nicht sichtbar), und vom Stützpunkt für den vorderen Mast hinunter durch dessen Spitze (U-Träger-Adapter) zurück nach ganz hinten auf die Winde.
