---
layout: "image"
title: "Jumping8605.jpg"
date: "2013-06-01T23:45:24"
picture: "IMG_8605.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37008
- /details81a0.html
imported:
- "2019"
_4images_image_id: "37008"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-01T23:45:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37008 -->
Das ist der Mittelbau mit Stand von heute: Als Rückgrat sind zwei Alu-Träger über die ganze Länge des Fahrzeugs drin. Darunter hängt das Fahrwerk mit 2+4 Achsen, und darin gleitet ein Schlitten mit dem klappbaren Mast vor und zurück. Bodenplatten fürs Podium sind links und rechts angebracht und werden heruntergeklappt, wenn der Mast steht und seine richtige Position mit dem Fuß hinter dem Vorderachs-Dolly eingenommen hat.
