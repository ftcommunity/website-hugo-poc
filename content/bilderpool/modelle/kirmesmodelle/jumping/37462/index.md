---
layout: "image"
title: "Jumping9552.jpg"
date: "2013-09-30T18:28:48"
picture: "Jumping9552.JPG"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37462
- /details900d.html
imported:
- "2019"
_4images_image_id: "37462"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:28:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37462 -->
Der Mittelbau mit Robo-TX und abgeklapptem Mast. Die Zugmaschine ist der "Büssing"-LKW von hier: http://www.ftcommunity.de/categories.php?cat_id=2762
