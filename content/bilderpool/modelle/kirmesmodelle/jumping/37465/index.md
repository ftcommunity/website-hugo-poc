---
layout: "image"
title: "Jumping9684.JPG"
date: "2013-09-30T18:36:37"
picture: "Jumping9684.JPG"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37465
- /details97d8.html
imported:
- "2019"
_4images_image_id: "37465"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T18:36:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37465 -->
Der Aufbau hat begonnen: der Mast ist aufgerichtet, ein Teil der Gondeln ist schon dran, die Podiumsplatten sind flach geklappt, hinten wird die obere Lage noch auseinander geklappt.
