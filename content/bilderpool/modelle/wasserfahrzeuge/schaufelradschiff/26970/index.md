---
layout: "image"
title: "Schaufelradschiff"
date: "2010-04-20T21:29:28"
picture: "schaufelradschiff1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/26970
- /detailscb8e.html
imported:
- "2019"
_4images_image_id: "26970"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26970 -->
Schiff von der Seite. Im Beutel ist das Interface.