---
layout: "image"
title: "Schaufelradschiff"
date: "2010-04-20T21:29:28"
picture: "schaufelradschiff2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/26971
- /detailsd598.html
imported:
- "2019"
_4images_image_id: "26971"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26971 -->
Schiff von hinten.