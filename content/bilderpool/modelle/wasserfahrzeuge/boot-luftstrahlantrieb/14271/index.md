---
layout: "image"
title: "Boot mit Luftstrahlantrieb"
date: "2008-04-17T17:56:48"
picture: "bootmitluftstrahlantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14271
- /details13f3.html
imported:
- "2019"
_4images_image_id: "14271"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14271 -->
Das ist ein Gesamtbild von meinem Boot.