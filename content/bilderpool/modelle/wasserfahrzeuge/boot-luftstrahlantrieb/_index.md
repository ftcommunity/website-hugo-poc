---
layout: "overview"
title: "Boot mit Luftstrahlantrieb"
date: 2020-02-22T08:27:07+01:00
legacy_id:
- /php/categories/1321
- /categoriesf031.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1321 --> 
Das ist ein Boot, das von einem pneumatisch gesteuerten Luftstrahl angetrieben wird.