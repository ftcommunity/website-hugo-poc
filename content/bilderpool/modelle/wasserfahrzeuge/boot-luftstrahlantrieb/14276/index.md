---
layout: "image"
title: "Luftstrahl"
date: "2008-04-17T17:56:49"
picture: "bootmitluftstrahlantrieb6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14276
- /details2403-2.html
imported:
- "2019"
_4images_image_id: "14276"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14276 -->
Auf diesem Bild sieht man den Luftstrahl.