---
layout: "image"
title: "Bootsrumpf-3"
date: "2011-03-23T17:40:33"
picture: "boot_4742.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30310
- /detailsb995.html
imported:
- "2019"
_4images_image_id: "30310"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:40:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30310 -->
Der Rumpf wird in eine Plastiktüte eingewickelt, aber so, dass alle Kanten oberhalb der Wasserlinie liegen. Das Ganze mit Klebeband fixieren.

Die Bauart erweist sich als ziemlich schwergewichtig. Mit dem großen Akku, Empfänger und Außenborder liegt der Pott verdammt tief im Wasser. Es sollte möglich sein, das Gerippe rein aus S-Streben und zugehörigen Teilen zu bauen.
