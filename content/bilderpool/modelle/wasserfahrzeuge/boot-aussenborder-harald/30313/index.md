---
layout: "image"
title: "Erlkönig_4736.JPG"
date: "2011-03-24T18:49:31"
picture: "Kreuzer_4736.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/30313
- /details4f63.html
imported:
- "2019"
_4images_image_id: "30313"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-24T18:49:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30313 -->
Dieser Erlkönig ist schon nicht mehr ganz Stand der Dinge. Es soll mal ein Seenotrettungskreuzer werden. Die XM-Motoren arbeiten 1:1 auf Graupner 3-Blattschrauben, aber die brauchen deutlich mehr Drehzahl, um ordentlich Schub zu liefern. Da fehlt noch eine Übersetzung von mindestens 1:3.

Aktuell sind zwei Power-Motoren 1:10 verbaut, mit Übersetzung 1:2 (ins schnellere), und zwei 5-Blattschrauben hinten dran. Die Ruder sollen mit Seilen oder P-Schläuchen angetrieben werden, und auf die Heckrampe kommt das Außenborder-Schiffchen als Beiboot drauf. Deswegen ist die, ähh, Mülltüte, die die Außenhaut abgibt, nach innen hineingewickelt, d.h. die Rampe zählt als "außerhalb" des Schiffsrumpfes.
