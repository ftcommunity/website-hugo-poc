---
layout: "image"
title: "Motorboot"
date: "2008-04-07T07:56:04"
picture: "motorboot01.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14194
- /detailsd78c.html
imported:
- "2019"
_4images_image_id: "14194"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14194 -->
Das ist ein Gesamtbild von meinem Motorboot.