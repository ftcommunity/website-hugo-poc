---
layout: "image"
title: "Fernsteuerung"
date: "2008-04-07T07:56:04"
picture: "motorboot05.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14198
- /details8934.html
imported:
- "2019"
_4images_image_id: "14198"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14198 -->
Hier sieht man den Empfänger von dem IR Control Set.