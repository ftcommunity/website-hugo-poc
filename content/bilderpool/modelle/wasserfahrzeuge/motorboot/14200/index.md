---
layout: "image"
title: "Funktion des Ruders"
date: "2008-04-07T07:56:04"
picture: "motorboot07.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14200
- /details80f3.html
imported:
- "2019"
_4images_image_id: "14200"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14200 -->
Hier sieht man den Motor, der das Ruder steuert und wie es funktioniert.