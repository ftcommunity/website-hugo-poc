---
layout: "image"
title: "Rennboot56.JPG"
date: "2012-05-17T14:14:22"
picture: "Rennboot56.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34964
- /detailsbc1e-3.html
imported:
- "2019"
_4images_image_id: "34964"
_4images_cat_id: "2587"
_4images_user_id: "4"
_4images_image_date: "2012-05-17T14:14:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34964 -->
Der Außenborder wurde unverändert übernommen. Das Boot wiegt komplett 498 g.
