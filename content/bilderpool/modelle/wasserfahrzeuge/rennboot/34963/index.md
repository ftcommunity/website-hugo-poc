---
layout: "image"
title: "Rennboot55.jpt"
date: "2012-05-17T14:12:32"
picture: "Rennboot55.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34963
- /detailscf0a-2.html
imported:
- "2019"
_4images_image_id: "34963"
_4images_cat_id: "2587"
_4images_user_id: "4"
_4images_image_date: "2012-05-17T14:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34963 -->
Der Rumpf ist aus einer Hartschaumplatte mittels Laubsäge und scharfem Messer entstanden. Ein bisschen Nachbearbeitung mit Schleifpapier würde die Optik noch aufbessern. Die Aufbauten sind per ft-hobbywelt-Verbinder am Rumpf befestigt.
