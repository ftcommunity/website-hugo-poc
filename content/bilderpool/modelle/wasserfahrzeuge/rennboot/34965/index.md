---
layout: "image"
title: "Rennboot58.JPG"
date: "2012-05-17T14:20:10"
picture: "Rennboot58.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "ft"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34965
- /details0d9b.html
imported:
- "2019"
_4images_image_id: "34965"
_4images_cat_id: "2587"
_4images_user_id: "4"
_4images_image_date: "2012-05-17T14:20:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34965 -->
Hobbywelt-Verbinder, reloaded. Der rechte gehört natürlich bis zur Kante versenkt, bevor man ihn mit dem "Schwert" fixiert.

Es gibt ja vielleicht unter den Roboter- und 3D-Spritzmaschinen- und insbesondere den ft-Plotterbauern mal so einen oder zwei, die dem hobbywelt-Styroporschneidedraht mal ein bisschen CNC-Geist einhauchen. Mit sowas könnte man Schiffsrümpfe (und natürlich Flugzeugtragflächen) vom Feinsten auf die Beine stellen.
