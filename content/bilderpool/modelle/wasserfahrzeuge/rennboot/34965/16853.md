---
layout: "comment"
hidden: true
title: "16853"
date: "2012-05-18T11:20:07"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
eine 2D-Styroporsäge aus ft gab es immerhin schon - auf der letztjährigen Modellausstellung in Münster: http://www.ftcommunity.de/details.php?image_id=33492
Vielleicht liefern die Erbauer ja noch Detailfotos für die ft:c?
Gruß, Dirk