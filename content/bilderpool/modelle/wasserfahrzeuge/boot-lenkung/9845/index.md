---
layout: "image"
title: "Boot mit Antrieb und Lenkung"
date: "2007-03-29T17:34:25"
picture: "bootmitlenkung1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9845
- /detailsbe53.html
imported:
- "2019"
_4images_image_id: "9845"
_4images_cat_id: "888"
_4images_user_id: "557"
_4images_image_date: "2007-03-29T17:34:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9845 -->
Ansicht von oben.Als Schwimmkörper dient aufbewahrungsbox