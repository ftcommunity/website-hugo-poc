---
layout: "image"
title: "Boot mit Antrieb und Lenkung"
date: "2007-03-29T17:34:25"
picture: "bootmitlenkung6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9850
- /detailsd067.html
imported:
- "2019"
_4images_image_id: "9850"
_4images_cat_id: "888"
_4images_user_id: "557"
_4images_image_date: "2007-03-29T17:34:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9850 -->
Gesamtansicht von vorne