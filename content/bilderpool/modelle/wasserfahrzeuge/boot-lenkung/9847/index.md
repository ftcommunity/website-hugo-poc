---
layout: "image"
title: "Boot mit Antrieb und Lenkung"
date: "2007-03-29T17:34:25"
picture: "bootmitlenkung3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9847
- /details0da3-2.html
imported:
- "2019"
_4images_image_id: "9847"
_4images_cat_id: "888"
_4images_user_id: "557"
_4images_image_date: "2007-03-29T17:34:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9847 -->
Antriebsmodul, Luftschraube zur Schiffschraube umfunktioniert