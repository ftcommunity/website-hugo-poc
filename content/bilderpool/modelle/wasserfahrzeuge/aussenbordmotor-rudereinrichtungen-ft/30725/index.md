---
layout: "image"
title: "Außenbord19"
date: "2011-05-29T20:45:13"
picture: "aussenbord19.jpg"
weight: "19"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30725
- /details5cb5-3.html
imported:
- "2019"
_4images_image_id: "30725"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30725 -->
Die Abbildung zeigt den fertig montierten Außenbordantrieb mit der Seilzug-Rudersteuerung (noch ohne aufgelegten Seilzug) und aufgeclipster Bauplatte auf der Mittelsäule als Abdeckung.

Deutlich zu sehen ist die Abstützung unter dem S-Motor.
