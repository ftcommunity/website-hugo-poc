---
layout: "overview"
title: "Außenbordmotor mit Rudereinrichtungen für Schiffsrumpf ft# 126927"
date: 2020-02-22T08:27:25+01:00
legacy_id:
- /php/categories/2290
- /categoriesdc9d.html
- /categories63df.html
- /categoriesb3c8.html
- /categories0125.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2290 --> 
Die Beschreibung eines Außenbordantriebes in ftpedia # 1 war der Anlass, nach einer baulichen Lösung mit gleichzeitiger Möglichkeit der Richtungssteuerung für den Schiffsrumpf ft# 126927 zu suchen.