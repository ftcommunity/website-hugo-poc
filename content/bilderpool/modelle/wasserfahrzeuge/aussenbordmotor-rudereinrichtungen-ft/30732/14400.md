---
layout: "comment"
hidden: true
title: "14400"
date: "2011-06-06T09:14:04"
uploadBy:
- "qincym"
license: "unknown"
imported:
- "2019"
---
Der Vorschlag mit dem Video ist angekommen.

Ich warte jetzt auf die angekündigten IR-Empfänger ohne Strombegrenzung. Es gibt doch hinundwieder Aussetzer mit den Anlaufströmen und dann ist das Boot mitten auf dem See und ein Reset ist nicht möglich. Also bitte etwas Geduld. Oder spätesens bei der Convention.

Viele Grüße
Volker-James