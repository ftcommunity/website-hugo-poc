---
layout: "image"
title: "Außenbord11"
date: "2011-05-29T20:45:13"
picture: "aussenbord11.jpg"
weight: "11"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30717
- /detailsbabe.html
imported:
- "2019"
_4images_image_id: "30717"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30717 -->
Der weiche Silikonschlauch (Innendurchmesser 4 mm, Wandstärke 0,5 mm) wird über das "dicke Ende" der Schubstange geschoben und dient als kardanische Verbindung zwischen Schubstange und Schnecke des S-Motors. Je besser diese Teile in achsialer Richtung ausgerichtet sind, desto geringer ist die Walkarbeit des Silikonschlauches, die ja der S-Motor aufbringen muss, und die an der Schiffsschraube zum Vortrieb des Schiffes fehlt.

Die linke Seite des Bildes zeigt den auf das "dicke Ende" der Schubstange aufgeschobene Silikonschlauch. Die rechte Seite des Bildes zeigt, wie das noch offene Ende des Silikonschlauches auf die kleine Schnecke des S-Motors aufgeschoben wurde.
