---
layout: "image"
title: "Außenbord17"
date: "2011-05-29T20:45:13"
picture: "aussenbord17.jpg"
weight: "17"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30723
- /detailsa86b.html
imported:
- "2019"
_4images_image_id: "30723"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30723 -->
Eine Traverse als Träger für den unteren Aufhängungspunkt des unteren Gelenkwürfels des Außenbordmotors wird aus folgenden Teilen: 9 Baustein 7,5 rot ft# 37468, 3 Verbindungsstücke 45 rot ft# 31330, 2 Verbindungsstücke 30 rot ft# 31061, 5 Federnocken rot ft# 31982, 1 Baustein 15 grau ft# 31005, 1 Bauplatte 15x30x3,75 rot ft# 32330 und 1 Bauplatte 15*30 rot ft# 38241 angefertigt. Die rechte Bildseite zeigt den fertigen unteren unteren Aufhängungspunkt des unteren Gelenkwürfels des Außenbordmotors. 
Die Traverse wird links und rechts mit 4 Bausteinen 5 rot ft# 37237 an den am Heck des Schiffsrumpfes angeklebten Bauplatten 15*30 am bugseitigen Zapfen quer unter dem Schiffsrumpf befestigt.
