---
layout: "image"
title: "Außenbord13"
date: "2011-05-29T20:45:13"
picture: "aussenbord13.jpg"
weight: "13"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
schlagworte: ["Silikonkupplung"]
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30719
- /detailsab1d-2.html
imported:
- "2019"
_4images_image_id: "30719"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30719 -->
Jetzt kann mit dem Zusammenbau von Schiffschraubenantrieb und der Schiffsschraubeneinheit begonnen werden. Dazu werden die beiden an den Wangen besfestigten Bauplatten in die seitlichen Nuten des S-Motors geschoben. Beim weiteren Zusammenschieben der beiden Einheiten ist darauf zu achten, daß die Metallachse durch das Gelenk der Schiffsschraubeneinheit und die Getriebeachse in die 2. Nut des 2:1-Getriebebausteins gelangt.
