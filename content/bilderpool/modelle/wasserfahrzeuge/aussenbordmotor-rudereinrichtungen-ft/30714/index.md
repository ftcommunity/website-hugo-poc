---
layout: "image"
title: "Außenbord08"
date: "2011-05-29T20:45:13"
picture: "aussenbord08.jpg"
weight: "8"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30714
- /details55e8.html
imported:
- "2019"
_4images_image_id: "30714"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30714 -->
An den Wangen des schon fertiggestellten Trägers werden 2 Bauplatten 15x90 rot mit Zapfen ft# 38245, wie das Bild zeigt, befestigt. Diese stellen die Verbindung zum S-Motor her.
Auf den vordersten Baustein 15 des schon fertiggestellten Trägers, den Schiffsschraubenträger, wird die so eben fertiggestellte Schiffsschraubeneinheit aufgeschoben.
