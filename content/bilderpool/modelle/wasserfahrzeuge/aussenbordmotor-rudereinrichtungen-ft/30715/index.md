---
layout: "image"
title: "Außenbord09"
date: "2011-05-29T20:45:13"
picture: "aussenbord09.jpg"
weight: "9"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30715
- /detailsd641.html
imported:
- "2019"
_4images_image_id: "30715"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30715 -->
Aus den Teilen:

1 S-Motor 6-9 V schwarz (mit kleiner Metallschnecke) ft# 32293, 1 Federnocken rot ft# 31982, 1 Drehscheibe 60 rot ft# 31019, 1 Nabenmutter rot ft# 310581, 1 Flachnabenzange rot ft# 350311, 1 Achse 125 Metall (d=4mm) ft# 31036, 1 Abstandsring rot ft# 31597, 1 Gelenkwürfel-Zunge rot ft# 31426, 2 Gelenkwürfel-Klauen rot ft# 31436, 1 Lagerhülse schwarz ft# 36819 und 2 Bausteine 5 rot ft# 37237

wird der Schiffschraubenantrieb mit der "Ruderscheibe" angefertigt.

Die linke Bildseite zeigt die einzelnen Teile, die rechte Bildseite zeigt den komplett zusammengebauten Schiffsschraubenantrieb.

Die Achse 125 Metall (d=4mm) ft# 31036 erleichtert später die Ausrichtung des Schiffsschraubenträgers zum Schiffsschraubenantrieb ungemein.
