---
layout: "image"
title: "Außenbord04"
date: "2011-05-29T20:45:13"
picture: "aussenbord04.jpg"
weight: "4"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30710
- /details6ca5.html
imported:
- "2019"
_4images_image_id: "30710"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30710 -->
Jetzt ist der Träger mit der vorderen Wange geschlossen und ganz links mit einer Bauplatte 15x15 rot (zum Clipsen) ft# 31506 und ganz rechts mit einem Gelenk bestehend aus einer Gelenkwürfel-Klaue rot ft# 31436 und einer Gelenkwürfel-Zunge rot ft# 31426 versehen.
