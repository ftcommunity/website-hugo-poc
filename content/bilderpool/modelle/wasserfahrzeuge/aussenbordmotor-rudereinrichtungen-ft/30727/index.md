---
layout: "image"
title: "Außenbord21"
date: "2011-05-29T20:45:13"
picture: "aussenbord21.jpg"
weight: "21"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30727
- /detailsd475.html
imported:
- "2019"
_4images_image_id: "30727"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30727 -->
Die zweite Variante des Ruderantriebes erfolgt mit einem Zahnrad.

Dazu wird 1 Zahnrad Z40/32, m 1,5 schwarz ft# 31022 auf die Ruderscheibe aufgedrückt und mit 3 Verbindungsstopfen schwarz ft# 32316 an dieser unverdrehbar befestigt. Die rechte Bildseite zeigt das fertig montierte Modul.

Der obere Aufhängungspunkt des oberen Drehpunktes des Außenbordmotors wird mit den Teilen 1 Baustein 30 grau ft# 31004, 1 Baustein 30 mit Bohrung 30 grau ft# 31004, 2 Winkelstein 10*15*15 rot ft# 38423, 4 Federnocken rot ft# 31982 und 1 Verbindungsstück 30 rot ft# 31061 erstellt. Die Abbildung zeigt links im Hintergrund das fertige Modul.

Der untere Aufhängungspunkt des unteren Drehpunktes des Außenbordmotors wurde bereits beschrieben.
