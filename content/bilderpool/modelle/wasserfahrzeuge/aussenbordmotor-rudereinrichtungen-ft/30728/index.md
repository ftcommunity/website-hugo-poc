---
layout: "image"
title: "Außenbord22"
date: "2011-05-29T20:45:13"
picture: "aussenbord22.jpg"
weight: "22"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/30728
- /detailsc3f2-2.html
imported:
- "2019"
_4images_image_id: "30728"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30728 -->
Aus den teilen 1 S-Motor 6-9 V (mit kleiner Metallschnecke) schwarz ft# 32293, 1 U-Getriebe mini schwarz ft# 31078, 1 U-Achse 40 mit Zahnrad Z28, m 0,5 schwarz ft# 31064, 1 Ritzel Z 10, m 1,5 schwarz ft# 35112 und 1 Spannzange rot ft# 35113 wird der Antrieb für das Ruder gebaut.

Auf der Unterseite des S-Motors wird 1 Baustein 7,5,rot ft# 37468 mit 1 Federnocken rot ft# 31982 als "Abstandshalter" befestigt. Die rechte Bildseite zeigt das fertige Modul.
