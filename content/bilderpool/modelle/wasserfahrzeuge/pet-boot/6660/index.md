---
layout: "image"
title: "Batterie und Empfänger"
date: "2006-08-06T13:02:30"
picture: "Hovercraft_BatterieEmpfnger.jpg"
weight: "5"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6660
- /details73fe.html
imported:
- "2019"
_4images_image_id: "6660"
_4images_cat_id: "575"
_4images_user_id: "426"
_4images_image_date: "2006-08-06T13:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6660 -->
Und hier der schöne alte Batteriekasten und der Empfänger!