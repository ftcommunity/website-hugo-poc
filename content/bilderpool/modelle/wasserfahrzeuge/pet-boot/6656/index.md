---
layout: "image"
title: "Hovercraft von der Seite"
date: "2006-08-06T13:02:30"
picture: "Hovercraft_seitenansicht.jpg"
weight: "1"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6656
- /details89c2-3.html
imported:
- "2019"
_4images_image_id: "6656"
_4images_cat_id: "575"
_4images_user_id: "426"
_4images_image_date: "2006-08-06T13:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6656 -->
So habe mich mal hergesetz und da ein Wassermodell gesehen hier im Forum. Nun habe ich mir gedacht bei dem Wetter ist das blöd im Freibad mit dem Ferngesteuerten Auto rum zu fahren also habe ich ein PET Hovercraft-Boot entworfen.

Wie schon gesagt befindet sich noch im Aufbau hier mal der Prototyp!