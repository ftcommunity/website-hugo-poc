---
layout: "image"
title: "Draufsicht"
date: "2006-08-06T13:02:30"
picture: "Hovercraft_draufsicht.jpg"
weight: "2"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6657
- /details4ad1.html
imported:
- "2019"
_4images_image_id: "6657"
_4images_cat_id: "575"
_4images_user_id: "426"
_4images_image_date: "2006-08-06T13:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6657 -->
