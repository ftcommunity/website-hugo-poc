---
layout: "image"
title: "Seitenansicht"
date: "2006-08-06T13:02:30"
picture: "Hovercraft_seitenansicht_2.jpg"
weight: "6"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6661
- /detailsd4bc.html
imported:
- "2019"
_4images_image_id: "6661"
_4images_cat_id: "575"
_4images_user_id: "426"
_4images_image_date: "2006-08-06T13:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6661 -->
Also hier mein Prototyp eines Hovercraft-Bootes in Verbindung mit der Fischertechnik Funkfernsteuerung. Ein Motorausgang steuert die beiden Motore mit Luftschraube. Der Servo steuert die richtung der Motore dadurch wird das Modell gelenkt!