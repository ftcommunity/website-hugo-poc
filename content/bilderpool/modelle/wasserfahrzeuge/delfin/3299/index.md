---
layout: "image"
title: "Delfin07.JPG"
date: "2004-11-21T13:37:35"
picture: "Delfin07.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Boot", "Jacht"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3299
- /details4847-2.html
imported:
- "2019"
_4images_image_id: "3299"
_4images_cat_id: "313"
_4images_user_id: "4"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3299 -->
Die Kajüte von unten gesehen.