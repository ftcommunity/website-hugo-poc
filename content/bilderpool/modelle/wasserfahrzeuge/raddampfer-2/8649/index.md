---
layout: "image"
title: "noch mal von nahem"
date: "2007-01-23T21:08:02"
picture: "raddampfer6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8649
- /detailsee6c.html
imported:
- "2019"
_4images_image_id: "8649"
_4images_cat_id: "795"
_4images_user_id: "453"
_4images_image_date: "2007-01-23T21:08:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8649 -->
