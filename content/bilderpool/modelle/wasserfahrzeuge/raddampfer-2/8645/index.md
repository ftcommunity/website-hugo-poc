---
layout: "image"
title: "Antrieb der Schaufelräder"
date: "2007-01-23T21:08:02"
picture: "raddampfer2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8645
- /detailse8c2.html
imported:
- "2019"
_4images_image_id: "8645"
_4images_cat_id: "795"
_4images_user_id: "453"
_4images_image_date: "2007-01-23T21:08:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8645 -->
