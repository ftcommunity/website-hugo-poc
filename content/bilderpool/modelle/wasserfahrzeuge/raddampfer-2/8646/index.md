---
layout: "image"
title: "Gesamtansicht"
date: "2007-01-23T21:08:02"
picture: "raddampfer3.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8646
- /details1d2f.html
imported:
- "2019"
_4images_image_id: "8646"
_4images_cat_id: "795"
_4images_user_id: "453"
_4images_image_date: "2007-01-23T21:08:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8646 -->
