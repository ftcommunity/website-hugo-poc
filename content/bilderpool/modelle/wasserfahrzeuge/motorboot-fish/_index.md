---
layout: "overview"
title: "Motorboot (fish)"
date: 2020-02-22T08:27:19+01:00
legacy_id:
- /php/categories/2135
- /categories16b9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2135 --> 
Ein schnelles Motorboot, welches von zwei Luftschrauben angetrieben wird.