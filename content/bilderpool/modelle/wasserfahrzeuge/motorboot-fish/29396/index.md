---
layout: "image"
title: "Schiff Übersicht"
date: "2010-12-01T22:17:02"
picture: "motorbootfish2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/29396
- /detailsd2f2.html
imported:
- "2019"
_4images_image_id: "29396"
_4images_cat_id: "2135"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29396 -->
Man sieht die Akkus, das IR-Control-Set, die beiden Powermotoren mit Getriebe und Schiffsschrauben.