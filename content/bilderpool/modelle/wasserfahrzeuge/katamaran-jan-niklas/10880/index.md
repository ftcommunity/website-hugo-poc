---
layout: "image"
title: "Katamaran"
date: "2007-06-18T09:17:43"
picture: "kata3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10880
- /details266b.html
imported:
- "2019"
_4images_image_id: "10880"
_4images_cat_id: "985"
_4images_user_id: "557"
_4images_image_date: "2007-06-18T09:17:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10880 -->
hier die lenkung:40:1 (schraube auf rad 10:1, rad auf rad 4:1)