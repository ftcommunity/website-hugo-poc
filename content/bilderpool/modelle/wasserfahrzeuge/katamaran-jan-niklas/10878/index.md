---
layout: "image"
title: "Katamaran"
date: "2007-06-18T09:17:43"
picture: "kata1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10878
- /detailsc6d0.html
imported:
- "2019"
_4images_image_id: "10878"
_4images_cat_id: "985"
_4images_user_id: "557"
_4images_image_date: "2007-06-18T09:17:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10878 -->
Die antriebsschraube