---
layout: "image"
title: "baggerschiff"
date: "2007-05-07T18:38:28"
picture: "baggerschiff1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10347
- /detailse2e0.html
imported:
- "2019"
_4images_image_id: "10347"
_4images_cat_id: "942"
_4images_user_id: "557"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10347 -->
bagerarm