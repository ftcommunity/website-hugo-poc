---
layout: "image"
title: "IMG_6622.JPG"
date: "2011-11-01T17:29:06"
picture: "IMG_6622_mit.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Leiter", "130925"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33376
- /details5238-2.html
imported:
- "2019"
_4images_image_id: "33376"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:29:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33376 -->
Die Rumpfteile bestehen aus einem Gerippe aus ft-Statik und den Verkleidungsplatten 15x90. Darüber kommt eine Lage (1) Schaumpolsterfolie, darüber (2) eine graue Plastikfolie (Mülltüte) und darüber (3) eine Lage grüne Dampfsperrenfolie (im Baumarkt unter "Dach-Ausbau").

Lage 1 soll etwas mehr Auftrieb verschaffen. Der Rumpf ist etwas zu klein ausgefallen für die Aufbauten, die dann später drauf gekommen sind.

Lage 2 macht den Rumpf wasserdicht. Lage 1 kann das nicht, und Lage 3 tut es nicht: die Dampfsperren-Klebefolie hält in der Fläche schon dicht, aber wenn beim Überlappen nur ein kleines Fältchen entsteht, sickert dort Wasser durch.

Lage 3 hält die graue Mülltütenfolie von Lage 2 an Ort und Stelle, und sorgt für die Optik. Wasserdicht ist sie nicht.
