---
layout: "image"
title: "Kata5254.JPG"
date: "2011-11-01T17:57:46"
picture: "IMG_5254_mit.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33383
- /detailsbf44.html
imported:
- "2019"
_4images_image_id: "33383"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:57:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33383 -->
Der erste Versuch, mit "Panzerband" als Rumpfhülle, ist gründlich daneben gegangen. Dieses Klebeband mag wohl Spritzwasser abhalten, aber drückendes Wasser kann man damit nicht aufhalten. Das hätte man in einem Vorversuch klären können. Ich habe es erst mit dem "fertigen" Boot in der Wanne erfahren.
