---
layout: "image"
title: "Kata_6343.JPG"
date: "2011-11-01T17:20:27"
picture: "IMG_6343_mit.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33375
- /detailsb821.html
imported:
- "2019"
_4images_image_id: "33375"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33375 -->
Das Doppelrumpf-Feuerlöschboot, im Pool in Erbes-Büdesheim.
