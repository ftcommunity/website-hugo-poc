---
layout: "image"
title: "Kata4828.JPG"
date: "2011-11-01T17:45:53"
picture: "IMG_4828_mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Leiter", "130925"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33380
- /details472a.html
imported:
- "2019"
_4images_image_id: "33380"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:45:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33380 -->
Ein Rumpfbogen von oben, noch ohne Beplankung. Vom BS7,5 sind ein paar Dutzend verbaut worden.
