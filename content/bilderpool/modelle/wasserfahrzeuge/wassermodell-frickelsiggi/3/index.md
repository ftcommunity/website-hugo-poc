---
layout: "image"
title: "3"
date: "2003-04-21T15:28:22"
picture: "3.jpg"
weight: "3"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/3
- /details3ce6.html
imported:
- "2019"
_4images_image_id: "3"
_4images_cat_id: "15"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T15:28:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3 -->
