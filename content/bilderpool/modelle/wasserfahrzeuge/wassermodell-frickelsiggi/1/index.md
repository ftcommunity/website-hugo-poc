---
layout: "image"
title: "1"
date: "2003-04-21T15:28:22"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Siegfried Kloster (frickelsiggi)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1
- /details2a35.html
imported:
- "2019"
_4images_image_id: "1"
_4images_cat_id: "15"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T15:28:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1 -->
