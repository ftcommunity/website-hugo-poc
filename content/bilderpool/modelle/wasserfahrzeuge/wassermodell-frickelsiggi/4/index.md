---
layout: "image"
title: "4"
date: "2003-04-21T15:28:22"
picture: "4.jpg"
weight: "4"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4
- /details6ff7-2.html
imported:
- "2019"
_4images_image_id: "4"
_4images_cat_id: "15"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T15:28:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4 -->
