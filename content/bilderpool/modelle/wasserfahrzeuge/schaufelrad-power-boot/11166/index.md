---
layout: "image"
title: "nächster Versuch - Bild 2"
date: "2007-07-20T22:01:02"
picture: "schaufelradpowerboot1_4.jpg"
weight: "6"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11166
- /detailsfa87.html
imported:
- "2019"
_4images_image_id: "11166"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:01:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11166 -->
ein Zusatzschwimmer aus 2 der alten Bootskörper ist trotzdem nötig, bei Rückwärtsfahrt schwappt sonst immer noch Wasser rein
