---
layout: "image"
title: "erster Versuch - Bild 3"
date: "2007-07-20T21:25:11"
picture: "schaufelradpowerboot3.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11163
- /detailsc8e5.html
imported:
- "2019"
_4images_image_id: "11163"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T21:25:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11163 -->
Detail einer Schaufel, es gehen natürlich auch die normalen Drehscheiben aber mit den Malteser Schaltrad Prototypen sieht es einfach besser aus :)
