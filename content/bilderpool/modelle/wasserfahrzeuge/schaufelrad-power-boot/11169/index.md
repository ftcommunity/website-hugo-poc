---
layout: "image"
title: "nächster Versuch - Bild 5"
date: "2007-07-20T22:20:41"
picture: "schaufelradpowerboot1_7.jpg"
weight: "9"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11169
- /detailsd04f.html
imported:
- "2019"
_4images_image_id: "11169"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:20:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11169 -->
die schnelle Rückwärtsfahrt ist immer noch heikel, ab in's Tuning-Center zur Demontange!
