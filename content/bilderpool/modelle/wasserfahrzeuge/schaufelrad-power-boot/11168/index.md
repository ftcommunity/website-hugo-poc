---
layout: "image"
title: "nächster Versuch - Bild 4"
date: "2007-07-20T22:15:10"
picture: "schaufelradpowerboot1_6.jpg"
weight: "8"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11168
- /details2c4a.html
imported:
- "2019"
_4images_image_id: "11168"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:15:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11168 -->
und es schwimmt doch :)
