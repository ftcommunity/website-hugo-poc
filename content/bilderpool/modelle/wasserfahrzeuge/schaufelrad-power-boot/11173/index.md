---
layout: "image"
title: "Rahmen - Bild 4"
date: "2007-07-20T22:49:12"
picture: "schaufelradpowerboot1_10.jpg"
weight: "13"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11173
- /detailsfb74.html
imported:
- "2019"
_4images_image_id: "11173"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:49:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11173 -->
mit Boot ist alles gerade, der Akku liegt im Boot vorn auf und verursacht leider kleine Abdrücke im Rumpf...
