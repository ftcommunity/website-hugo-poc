---
layout: "image"
title: "nächster Versuch - Bild 3"
date: "2007-07-20T22:08:25"
picture: "schaufelradpowerboot1_5.jpg"
weight: "7"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11167
- /details1e63-2.html
imported:
- "2019"
_4images_image_id: "11167"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:08:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11167 -->
mein erster Versuch übrigens, alles so weit wie möglich in Rot zu bauen :)
