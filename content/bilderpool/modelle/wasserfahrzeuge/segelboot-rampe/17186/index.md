---
layout: "image"
title: "segelbootmirrampe03.jpg"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe03.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17186
- /detailsdfe0.html
imported:
- "2019"
_4images_image_id: "17186"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17186 -->
