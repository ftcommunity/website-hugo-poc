---
layout: "image"
title: "Gesamtansicht"
date: "2009-01-30T19:38:21"
picture: "segelbootmirrampe01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17184
- /details93a5-2.html
imported:
- "2019"
_4images_image_id: "17184"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17184 -->
