---
layout: "comment"
hidden: true
title: "8435"
date: "2009-02-03T20:23:02"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
... das Metadatum wird dann sichtbar, wenn das Dateiverzeichnis in der Detailansicht entsprechend erweitert ist oder mit der Maus auf das Thumbnail der Datei-Ausgabe einer Bildverwaltung gezeigt wird.