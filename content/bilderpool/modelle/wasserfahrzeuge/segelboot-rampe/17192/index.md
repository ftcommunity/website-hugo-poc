---
layout: "image"
title: "Ankerseilwinde und Haken"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe09.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17192
- /detailsd555.html
imported:
- "2019"
_4images_image_id: "17192"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17192 -->
