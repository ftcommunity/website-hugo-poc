---
layout: "image"
title: "Männchen mit Sitz und Steuer"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17187
- /detailscb93.html
imported:
- "2019"
_4images_image_id: "17187"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17187 -->
