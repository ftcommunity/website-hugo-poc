---
layout: "image"
title: "Anker"
date: "2009-01-30T19:38:47"
picture: "segelbootmirrampe23.jpg"
weight: "23"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17206
- /details474d.html
imported:
- "2019"
_4images_image_id: "17206"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:47"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17206 -->
Das ist der Anker.