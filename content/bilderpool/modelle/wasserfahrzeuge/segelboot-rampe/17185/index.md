---
layout: "image"
title: "Gesamtansicht_2"
date: "2009-01-30T19:38:22"
picture: "segelbootmirrampe02.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17185
- /details2a37.html
imported:
- "2019"
_4images_image_id: "17185"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17185 -->
