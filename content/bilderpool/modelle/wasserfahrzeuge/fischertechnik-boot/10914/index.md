---
layout: "image"
title: "Dampferle 2"
date: "2007-06-25T00:32:31"
picture: "GrennderungDSC03058.jpg"
weight: "4"
konstrukteure: 
- "charly"
fotografen:
- "charly"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/10914
- /details1402.html
imported:
- "2019"
_4images_image_id: "10914"
_4images_cat_id: "948"
_4images_user_id: "115"
_4images_image_date: "2007-06-25T00:32:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10914 -->
Schade, daß man im Schwobaländle keinen so schönen Schiffchenteich am Ort hat.