---
layout: "image"
title: "Testmodell"
date: "2009-05-29T15:30:25"
picture: "airboot1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24121
- /details8ee8.html
imported:
- "2019"
_4images_image_id: "24121"
_4images_cat_id: "1654"
_4images_user_id: "845"
_4images_image_date: "2009-05-29T15:30:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24121 -->
Das ist nur der Testlauf in der Badewanne. Weitere Bilder kommen noch!