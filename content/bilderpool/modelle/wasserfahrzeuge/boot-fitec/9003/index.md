---
layout: "image"
title: "Boot"
date: "2007-02-12T17:47:11"
picture: "Boot6.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9003
- /details2169.html
imported:
- "2019"
_4images_image_id: "9003"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9003 -->
Hier lasse ich es immer fahren. Das ist das Schwimmbad in unserem Garten. Es ist 4x8 Meter groß. Wie tief es ist weiß ich nicht. ich schätze man 1,30.
