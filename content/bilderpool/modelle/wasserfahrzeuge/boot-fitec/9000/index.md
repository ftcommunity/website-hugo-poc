---
layout: "image"
title: "Boot"
date: "2007-02-12T17:47:07"
picture: "Boot3.jpg"
weight: "3"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9000
- /details9669.html
imported:
- "2019"
_4images_image_id: "9000"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9000 -->
Antrieb des rechten Schaufelrads.
