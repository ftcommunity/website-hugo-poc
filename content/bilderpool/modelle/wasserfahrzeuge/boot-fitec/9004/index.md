---
layout: "image"
title: "Boot"
date: "2007-02-12T18:45:40"
picture: "Boot7.jpg"
weight: "7"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9004
- /details9fe3.html
imported:
- "2019"
_4images_image_id: "9004"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T18:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9004 -->
Ich hab es jetzt auf Ruder und Schiffsschraube umgebaut. es fährt so viel besser und schneller.
