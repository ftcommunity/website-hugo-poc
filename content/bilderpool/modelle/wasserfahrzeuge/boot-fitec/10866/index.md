---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-14T20:34:31"
picture: "Boot22.jpg"
weight: "22"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10866
- /details2017-2.html
imported:
- "2019"
_4images_image_id: "10866"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10866 -->
Gesamtansicht. Links sind ein paar Gewichte damit es nich umkippt weil es rechts etwas schwerer ist.
