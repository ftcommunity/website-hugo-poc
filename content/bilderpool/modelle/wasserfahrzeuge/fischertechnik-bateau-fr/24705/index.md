---
layout: "image"
title: "FT-Bateau En détail 01"
date: "2009-08-04T18:13:30"
picture: "ftbateau05.jpg"
weight: "5"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/24705
- /details34f5.html
imported:
- "2019"
_4images_image_id: "24705"
_4images_cat_id: "1697"
_4images_user_id: "136"
_4images_image_date: "2009-08-04T18:13:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24705 -->
