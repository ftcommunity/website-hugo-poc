---
layout: "image"
title: "FT-Bateau 03"
date: "2009-08-04T18:13:30"
picture: "ftbateau04.jpg"
weight: "4"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/24704
- /details0366-2.html
imported:
- "2019"
_4images_image_id: "24704"
_4images_cat_id: "1697"
_4images_user_id: "136"
_4images_image_date: "2009-08-04T18:13:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24704 -->
