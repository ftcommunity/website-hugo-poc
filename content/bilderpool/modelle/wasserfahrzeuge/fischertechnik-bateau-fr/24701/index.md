---
layout: "image"
title: "FT-Bateau 00"
date: "2009-08-04T18:13:30"
picture: "ftbateau01.jpg"
weight: "1"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/24701
- /details1a72.html
imported:
- "2019"
_4images_image_id: "24701"
_4images_cat_id: "1697"
_4images_user_id: "136"
_4images_image_date: "2009-08-04T18:13:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24701 -->
Sur le Cere (Cantal FR)