---
layout: "comment"
hidden: true
title: "2555"
date: "2007-03-01T20:10:40"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Hast du mal ausprobiert, ob die Schnecke wirklich Vortrieb erzeugt? Sieht für mich so aus, als wäre die ziemlich im Schatten der Aufhängung. Vielleicht eher ne Luftschraube probieren und die so antreiben, dass sie langsam dreht.