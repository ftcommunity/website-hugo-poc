---
layout: "image"
title: "Außenborder Angebaut"
date: "2009-11-11T20:20:47"
picture: "ausssenborder1.jpg"
weight: "2"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25764
- /detailsf421.html
imported:
- "2019"
_4images_image_id: "25764"
_4images_cat_id: "1807"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25764 -->
Der Außenborder dient zum Antrieb meines Bootes aus der Kiste der Universalbox. Er wird einfach an der Wand der Box angeklemmt.