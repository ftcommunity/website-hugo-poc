---
layout: "image"
title: "VSP-B03.JPG"
date: "2007-08-09T19:18:32"
picture: "VSP-B03.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11316
- /detailsd1c9-2.html
imported:
- "2019"
_4images_image_id: "11316"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:18:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11316 -->
Das ist Modell "B" von unten.
