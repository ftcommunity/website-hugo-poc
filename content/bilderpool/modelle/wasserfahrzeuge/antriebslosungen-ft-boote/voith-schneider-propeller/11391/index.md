---
layout: "image"
title: "VSP-F01.JPG"
date: "2007-08-18T10:13:29"
picture: "VSP-F01.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Speichenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11391
- /details2b27.html
imported:
- "2019"
_4images_image_id: "11391"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-18T10:13:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11391 -->
Modell "F".

Die Getriebeklaue 35467 hat die richtige Öffnung, um darin eine S-Strebe zu führen. Die Drähte sorgen nur dafür, dass die Anordnung beim Umdrehen zwecks Bastelei auf der Unterseite zusammen bleibt. Die grauen BS15-Loch gibt es mittlerweile in schwarz; sie haben auf der einen Seite eine größere Bohrung, z.B. um einen Dauermagneten aufzunehmen. Hier steckt aber eine K-Achse mit Vierkant 78237 darin, um die Drehbewegung von der Getriebeklaue auf das Propellerblatt zu übertragen.

Zwischen Speichenrad und Kette ist ein Raupenband 31057 aufgezogen. Dieses Eigenbau-Zahnrad wird von einem Z10 rechts oben unterhalb des Z20 angetrieben. Das mit der Kette ums Speichenrad haben unsere Freunde in Holland erfunden: http://ftcommunity.de/details.php?image_id=2464
