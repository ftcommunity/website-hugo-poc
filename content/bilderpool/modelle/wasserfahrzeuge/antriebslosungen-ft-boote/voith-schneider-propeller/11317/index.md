---
layout: "image"
title: "VSP-C04.JPG"
date: "2007-08-09T19:24:41"
picture: "VSP-C04.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11317
- /details4427-2.html
imported:
- "2019"
_4images_image_id: "11317"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:24:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11317 -->
Das ist ein Blatt von Modell "C". Die ft-Streben sind 8 mm breit und gar nicht so einfach in eine Gleitführung einzupassen. 

Dieser Aufbau ist insgesamt zu schwabbelig und löst sich nach einigen Umdrehungen in seine Bestandteile auf. 

In den Worten von Martin (Remadus): man muss viele Kröten küssen, bis man auf einen Prinzen stößt. Das hier ist eine Kröte.
