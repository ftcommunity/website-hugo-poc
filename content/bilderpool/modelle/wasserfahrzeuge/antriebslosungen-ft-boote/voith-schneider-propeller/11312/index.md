---
layout: "image"
title: "VSP-0x03.JPG"
date: "2007-08-09T18:51:42"
picture: "VSP-0x03.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11312
- /detailsd552-2.html
imported:
- "2019"
_4images_image_id: "11312"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:51:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11312 -->
Noch eine Variante, die etwas weniger wackelig ist als die in Foto VSP-0x02.JPG gezeigte.

Den BS15 gibt es auch ohne Zapfen, und genau so einer steckt hier mittendrin.

Das Spezialteil unten ist Original-ft und stammt aus einem geplatzten BS15 mit Doppelzapfen. Sowas wirft man ja nicht weg.
