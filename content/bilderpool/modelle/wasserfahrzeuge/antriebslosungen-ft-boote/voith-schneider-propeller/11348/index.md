---
layout: "image"
title: "VSP-E28.JPG"
date: "2007-08-11T07:51:58"
picture: "VSP-E28.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11348
- /details3ecd.html
imported:
- "2019"
_4images_image_id: "11348"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-11T07:51:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11348 -->
Modell "E" mit zweitem Speichenrad. Das wurde notwendig, weil der Propeller ja mit den Blättern im Wasser stecken muss und das untere Speichenrad deshalb nur knapp über Wasser bleibt. Jetzt kommt der Motor weiter vom Wasserspiegel und dem Spritzwasser (der Propeller dürfte ziemlich stark planschen, schätze ich) weg.

Der Nachteil ist allerdings, dass der Steuerknüppel mittendrin jetzt einen ziemlich eingeschränkten Bewegungsraum hat.
