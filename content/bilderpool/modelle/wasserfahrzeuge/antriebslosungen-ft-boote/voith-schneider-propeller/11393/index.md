---
layout: "image"
title: "VSP-F03.JPG"
date: "2007-08-18T10:16:30"
picture: "VSP-F03.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11393
- /details0559-2.html
imported:
- "2019"
_4images_image_id: "11393"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-18T10:16:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11393 -->
Modell "F" im Versuchsträger eingebaut.
