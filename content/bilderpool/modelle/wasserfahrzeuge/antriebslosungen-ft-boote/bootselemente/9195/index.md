---
layout: "image"
title: "Auch anders brauchbar..."
date: "2007-03-01T16:56:00"
picture: "bootselemente2.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9195
- /details01df.html
imported:
- "2019"
_4images_image_id: "9195"
_4images_cat_id: "850"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9195 -->
Wenn man sie nicht zuklebt kann man darin Sachen aufbewahren.