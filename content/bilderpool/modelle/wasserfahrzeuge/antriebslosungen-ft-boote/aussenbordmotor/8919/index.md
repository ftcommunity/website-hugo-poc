---
layout: "image"
title: "Gesamtansicht 2"
date: "2007-02-10T15:13:34"
picture: "Auenborder02b.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8919
- /details0fff.html
imported:
- "2019"
_4images_image_id: "8919"
_4images_cat_id: "809"
_4images_user_id: "488"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8919 -->
Das ganze nochmal von oben betrachtet. Der Motor bleibt bei dieser Konstruktion deutlich oberhalb der Wasserlinie und ist somit geschützt.