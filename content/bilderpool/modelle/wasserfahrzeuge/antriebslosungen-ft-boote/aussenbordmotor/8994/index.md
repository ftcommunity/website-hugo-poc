---
layout: "image"
title: "Ausenborder"
date: "2007-02-12T17:47:07"
picture: "Ausenborder1.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8994
- /details238e-2.html
imported:
- "2019"
_4images_image_id: "8994"
_4images_cat_id: "809"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8994 -->
Das ist meine Konstruktion von einem Ausenborder. Es ist eine kompakte Lösung wo Antrieb und Lenkung dabei sind.
Gelenkt wird über einen Mini-Mot und über das Kardangelenk. Für den Antrieb ist der Power.Mot.
