---
layout: "image"
title: "modellevonclauswludwig64.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig64.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12724
- /details89bf.html
imported:
- "2019"
_4images_image_id: "12724"
_4images_cat_id: "1148"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12724 -->
