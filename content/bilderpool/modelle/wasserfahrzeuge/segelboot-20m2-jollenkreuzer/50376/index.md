---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (13) (Large)"
date: 2024-03-27T09:47:08+01:00
picture: "20m2Jollenkreuer_1_Rumpf (13) (Large).JPG"
weight: "23"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf