---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (3) (Large)"
date: 2024-03-27T09:46:49+01:00
picture: "20m2Jollenkreuer_1_Rumpf (3) (Large).JPG"
weight: "37"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf