---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (1) (Large)"
date: 2024-03-27T09:47:14+01:00
picture: "20m2Jollenkreuer_1_Rumpf (1) (Large).JPG"
weight: "19"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf