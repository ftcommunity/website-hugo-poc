---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (23) (Large)"
date: 2024-03-27T09:46:53+01:00
picture: "20m2Jollenkreuer_1_Rumpf (23) (Large).JPG"
weight: "34"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf