---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (18) (Large)"
date: 2024-03-27T09:47:02+01:00
picture: "20m2Jollenkreuer_1_Rumpf (18) (Large).JPG"
weight: "28"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf