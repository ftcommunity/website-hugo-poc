---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (17) (Large)"
date: 2024-03-27T09:47:03+01:00
picture: "20m2Jollenkreuer_1_Rumpf (17) (Large).JPG"
weight: "27"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf