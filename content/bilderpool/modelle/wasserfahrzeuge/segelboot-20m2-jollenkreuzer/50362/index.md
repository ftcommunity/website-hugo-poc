---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (25) (Large)"
date: 2024-03-27T09:46:50+01:00
picture: "20m2Jollenkreuer_1_Rumpf (25) (Large).JPG"
weight: "36"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf