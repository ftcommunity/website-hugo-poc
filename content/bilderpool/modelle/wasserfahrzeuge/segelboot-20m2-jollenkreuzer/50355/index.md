---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (8) (Large)"
date: 2024-03-27T09:46:41+01:00
picture: "20m2Jollenkreuer_1_Rumpf (8) (Large).JPG"
weight: "42"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf