---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (16) (Large)"
date: 2024-03-27T09:47:04+01:00
picture: "20m2Jollenkreuer_1_Rumpf (16) (Large).JPG"
weight: "26"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf