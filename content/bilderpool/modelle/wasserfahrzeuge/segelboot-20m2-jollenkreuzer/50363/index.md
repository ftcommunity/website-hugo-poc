---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (24) (Large)"
date: 2024-03-27T09:46:52+01:00
picture: "20m2Jollenkreuer_1_Rumpf (24) (Large).JPG"
weight: "35"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf