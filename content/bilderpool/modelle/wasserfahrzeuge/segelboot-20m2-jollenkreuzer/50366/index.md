---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (21) (Large)"
date: 2024-03-27T09:46:55+01:00
picture: "20m2Jollenkreuer_1_Rumpf (21) (Large).JPG"
weight: "32"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf