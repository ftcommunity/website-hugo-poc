---
layout: "overview"
title: "Segelboot 20m2 Jollenkreuzer"
date: 2024-03-27T09:46:01+01:00
---

Der 20m2 Jollenkreuzer wird auf den Seen in Deutschland, Österreich und der Schweiz sehr gerne gesegelt. Er zeichnet sich durch hohe Geschwindigkeit, wunderbares Segelerlebnis und eine optisch äußerst attraktive Erscheinung aus. Mehr über das Boot selbst: https://www.20r.at/boot/jollenkreuzer.pdfin Zeitschrift "Palsteak" sowie ein Folder https://www.20r.at/boot/20er_Folder_V3_240213.pdf

In Natur sind die Schiffe 7,75 m lang. Ich habe einen solchen Jollenkreuzer aus Fischer Technik nachgebaut, wobei mein Modell knapp 1 Meter lang ist.
Das Bootsmodell zeigt dem Rumpf, den Kajütaufbau ohne Verschalung, sowie das Rigg. Die Besonderheit am Bau dieses Modells war es einen möglichst harmonischen Verlauf der Bootsspanten hinzubekommen, wobei man nur 2 verschiedene Biegungen der Statistik-Kurven-Elemente hat.
Das Boot hat eine Rollfock, ein verstellbares Schwert, ein Ruder mit drehbaren Ausleger auf der Pinne. Moderne Boote haben zwar kaum mehr einen Traveller für den Großschotblock, mit hat es aber Spaß gemacht das Getriebe des Mini-Motor dafür zu verwenden.
(Das Rigg ist überdurchschnittlich schwer und der Mast bleibt zwar stehen, biegt sich auf lange Sicht aber durch. Ich habe ihn als das Boot längere Zeit gestanden ist auf der Zimmerdecke befestigt.) In natura würde der Mast genau über einen Spant stehen, aber das hat sich im Bau dann so ergeben, dass ich den Schwertkasten nicht mehr verändern wollte.