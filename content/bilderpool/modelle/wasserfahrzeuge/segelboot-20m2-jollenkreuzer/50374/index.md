---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (15) (Large)"
date: 2024-03-27T09:47:06+01:00
picture: "20m2Jollenkreuer_1_Rumpf (15) (Large).JPG"
weight: "25"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf