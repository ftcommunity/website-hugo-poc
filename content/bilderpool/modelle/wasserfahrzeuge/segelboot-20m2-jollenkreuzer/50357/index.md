---
layout: "image"
title: "20m2Jollenkreuer_1_Rumpf (6) (Large)"
date: 2024-03-27T09:46:44+01:00
picture: "20m2Jollenkreuer_1_Rumpf (6) (Large).JPG"
weight: "40"
konstrukteure: 
- "Gerhard"
fotografen:
- "Gerhard"
uploadBy: "Website-Team"
license: "unknown"
---

Details zum Rumpf