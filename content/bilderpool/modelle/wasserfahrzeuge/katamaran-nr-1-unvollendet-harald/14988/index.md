---
layout: "image"
title: "Rumpfzuschnitt"
date: "2008-08-03T10:52:43"
picture: "P7300002_Zuschnitt.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14988
- /detailsbda4-2.html
imported:
- "2019"
_4images_image_id: "14988"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T10:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14988 -->
Ring frei für den zweiten Anlauf. Die Schalung besteht aus einzelnen Scheiben, die aus einer Hartschaumplatte ausgeschnitten werden.
Die Methode ist von den "Türmen aus Hanoi" abgeguckt, einschließlich der Dorne, auf die die Scheiben nachher aufgespießt werden. Zum Fixieren dienen hier zwei angespitzte Achsen 50, auf denen die beiden Z10 aufgeklemmt sind. Durch diese Löcher hindurch kommen später zwei Messingstäbe.
