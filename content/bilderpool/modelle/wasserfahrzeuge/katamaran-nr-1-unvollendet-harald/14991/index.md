---
layout: "image"
title: "Pappmaché"
date: "2008-08-03T11:08:44"
picture: "P8010011_beklebt.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14991
- /details995d-2.html
imported:
- "2019"
_4images_image_id: "14991"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T11:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14991 -->
Das Ganze wird mit Frischhaltefolie umwickelt und dann mit eingekleisterten Papierstreifen belegt. Es wird sich noch zeigen, ob die Schalung hinterher wieder heil herauszuholen ist.
