---
layout: "comment"
hidden: true
title: "4115"
date: "2007-09-24T20:15:15"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tja, mit dem Auflegen der Folie ist es ja nicht getan. sie muss am Rand umgebördelt und angetackert werden. In den Feldern zwischen den Spanten beult sie sich unter Druck nach innen. Bis da etwas draus wird, fehlt noch eine ganze Menge Fummelei, fürchte ich.


Gruß,
Harald