---
layout: "image"
title: "Katamaran07.JPG"
date: "2007-09-24T19:05:27"
picture: "Katamaran07.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11947
- /details8fe0-2.html
imported:
- "2019"
_4images_image_id: "11947"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T19:05:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11947 -->
Ein Versuch, den Rumpf aus Pappmaché zu fertigen. Ich hab's dann bleiben gelassen. Es ist eine Kleisterschmierpamperei ohne Ende, und das Zeug wird einfach nicht glatt und nicht fest. Dann will ich doch lieber Glasfasermatten aus dem PKW-Reparatursatz auflegen und stinkendes Kunstharz drüber pinseln.
