---
layout: "image"
title: "Feinarbeit"
date: "2008-08-03T10:59:40"
picture: "P7300004_Frsen.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14989
- /detailsd477.html
imported:
- "2019"
_4images_image_id: "14989"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T10:59:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14989 -->
Die Platten lassen sich mit einem scharfen Messer prima schneiden. Den Rest erledigt die Fräse. Der Fräser hat den gleichen Durchmesser wie die ft-Klemmbuchse, die genau darunter montiert ist (im Bild etwas verdeckt vom Abfall). 
Mit der zweistöckigen Anordnung wird die Kontur der ft-Vorlage (unten, ft-Rumpf wird an der Klemmbuchse entlang gezogen) auf die Hartschaumplatte (oben, Fräser zieht durchs Material) kopiert.
