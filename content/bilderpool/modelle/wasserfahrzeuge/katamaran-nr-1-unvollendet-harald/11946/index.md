---
layout: "image"
title: "Katamaran05.JPG"
date: "2007-09-24T19:01:51"
picture: "Katamaran05.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11946
- /detailsda9e.html
imported:
- "2019"
_4images_image_id: "11946"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T19:01:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11946 -->
So sollte es funktionieren,  Antriebswelle und Ruderwelle oberhalb der Wasserlinie durch die Rumpfwand zu bringen, und die ganze Anordnung auch unter Wasser dicht zu halten.

In den Löchern der Drehscheibe stecken Powermagnete mit 5 mm Länge und 4 mm Durchmesser. Auf der Innenseite des Rumpfes gibt es das gleiche noch einmal, und voilà, der Antrieb sitzt :-)
