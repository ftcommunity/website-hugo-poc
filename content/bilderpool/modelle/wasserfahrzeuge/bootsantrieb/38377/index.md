---
layout: "image"
title: "Schaufelrad (1)"
date: "2014-02-23T18:04:18"
picture: "bootsantrieb4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38377
- /details105f.html
imported:
- "2019"
_4images_image_id: "38377"
_4images_cat_id: "2853"
_4images_user_id: "104"
_4images_image_date: "2014-02-23T18:04:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38377 -->
Die drei BS7,5 sind hier von der 130593 Rastaufnahmeachse 22,5 abgeschoben, damit man den sieht. Ein hier kaum sichtbarer Klemmring zwischen ihm und der Freilaufnabe, auf der die Drehscheibe sitzt, dient als Abstandshalter.
