---
layout: "overview"
title: "Bootsantrieb"
date: 2020-02-22T08:27:31+01:00
legacy_id:
- /php/categories/2853
- /categoriesfc51.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2853 --> 
Dies ist ein einfacher Bootsantrieb mit möglichst tiefliegenden Motoren. Die damit erreichten Geschwindigkeiten passen gut zu den Platzverhältnissen in einer Badewanne.