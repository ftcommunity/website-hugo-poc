---
layout: "image"
title: "Schaufelrad (2)"
date: "2014-02-23T18:04:18"
picture: "bootsantrieb5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38378
- /details4ddc.html
imported:
- "2019"
_4images_image_id: "38378"
_4images_cat_id: "2853"
_4images_user_id: "104"
_4images_image_date: "2014-02-23T18:04:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38378 -->
Auf der anderen Seite wird das Schaufelrad von einem weiteren Klemmring gehalten. Es rotiert per Freilaufnabe locker und wird nur über das Z40 angetrieben.
