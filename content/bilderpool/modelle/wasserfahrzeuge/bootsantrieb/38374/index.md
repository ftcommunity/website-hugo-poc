---
layout: "image"
title: "Gesamtansicht"
date: "2014-02-23T18:04:06"
picture: "bootsantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38374
- /details6f5b-2.html
imported:
- "2019"
_4images_image_id: "38374"
_4images_cat_id: "2853"
_4images_user_id: "104"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38374 -->
Zwei S-Motoren treiben über ein Winkelgetriebe und eine Z10/Z40-Kombination die Schaufelräder an. Das ergibt für Badewannengrößen gerade die richtigen Geschwindigkeiten. Der Schwerpunkt sitzt tief, sodass man da noch gut Aufbauten anbringen kann. Die beiden Schaufelräder sind an einer Reihe BS7,5 mit Verbindern 15 aufgehängt und lassen sich einfach nach vorne abschieben - siehe das hinter dem Boot liegende Schaufelrad.
