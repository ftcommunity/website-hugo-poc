---
layout: "image"
title: "Boot"
date: "2007-05-07T16:05:15"
picture: "PICT0018.jpg"
weight: "2"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10341
- /details1009.html
imported:
- "2019"
_4images_image_id: "10341"
_4images_cat_id: "937"
_4images_user_id: "590"
_4images_image_date: "2007-05-07T16:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10341 -->
Hier sieht man die Elecktronik und den Motor zum lenken und den Motor zum fahren.
