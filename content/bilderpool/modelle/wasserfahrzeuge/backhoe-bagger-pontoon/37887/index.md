---
layout: "image"
title: "Kran gestartet"
date: "2013-12-02T12:57:36"
picture: "backhoe17.jpg"
weight: "17"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37887
- /details91f4.html
imported:
- "2019"
_4images_image_id: "37887"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37887 -->
Damit der Kran die Spuds nich Anstoßen kann, werden Ausleger und Baggerlöffel nach innen bewogen.
