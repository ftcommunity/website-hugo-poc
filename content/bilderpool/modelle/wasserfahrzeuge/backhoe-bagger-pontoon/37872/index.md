---
layout: "image"
title: "Spud Aufrechten"
date: "2013-12-02T12:57:36"
picture: "backhoe02.jpg"
weight: "2"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37872
- /details55ed-2.html
imported:
- "2019"
_4images_image_id: "37872"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37872 -->
Hier wird der erste Spud aufgerichtet. Als erste wird diese Spud mit ein Winde nach seine Gleichgewchtspunkt verschoben. Dann Kippt der Spudkast sich mittels ein Zylinder (Zylinder ist hier nur Optisch eingebaut). Das Kippen geht mittels eine Schnecke mit Motor
