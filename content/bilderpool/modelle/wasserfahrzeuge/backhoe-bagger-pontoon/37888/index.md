---
layout: "image"
title: "Baggern"
date: "2013-12-02T12:57:36"
picture: "backhoe18.jpg"
weight: "18"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37888
- /details11c4.html
imported:
- "2019"
_4images_image_id: "37888"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37888 -->
Wann der Kran nach außen gedreht ist, kann das Baggern anfangen.
