---
layout: "image"
title: "Spud Aufrechten 3"
date: "2013-12-02T12:57:36"
picture: "backhoe04.jpg"
weight: "4"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37874
- /details0523.html
imported:
- "2019"
_4images_image_id: "37874"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37874 -->
Aufrechten des Spuds kostet hier ca 90 sekunden.
