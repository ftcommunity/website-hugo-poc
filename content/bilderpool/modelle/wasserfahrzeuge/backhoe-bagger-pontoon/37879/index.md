---
layout: "image"
title: "Spud Aufrechten 8"
date: "2013-12-02T12:57:36"
picture: "backhoe09.jpg"
weight: "9"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37879
- /details0c60.html
imported:
- "2019"
_4images_image_id: "37879"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37879 -->
