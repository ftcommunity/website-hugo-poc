---
layout: "image"
title: "'Lauf Spud' Aufrechten 3"
date: "2013-12-02T12:57:36"
picture: "backhoe12.jpg"
weight: "12"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37882
- /detailsd76f-3.html
imported:
- "2019"
_4images_image_id: "37882"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37882 -->
Spud steht hier Senkrecht. Der Spudwagen werd jetzt nach hinten verschoben damit es auf die Richtige Stelle kommt.
