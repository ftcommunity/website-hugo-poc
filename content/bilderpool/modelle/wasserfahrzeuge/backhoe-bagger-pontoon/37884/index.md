---
layout: "image"
title: "Laufen 2"
date: "2013-12-02T12:57:36"
picture: "backhoe14.jpg"
weight: "14"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37884
- /detailsa5b1.html
imported:
- "2019"
_4images_image_id: "37884"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37884 -->
Hier ist der Spud am Ende die Laufschiene gekommen. Wann am Ende wird der Spud in die Grund gedruckt.
