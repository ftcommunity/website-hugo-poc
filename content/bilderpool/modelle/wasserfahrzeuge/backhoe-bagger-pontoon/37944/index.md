---
layout: "image"
title: "Innenleben Backhoe"
date: "2013-12-29T19:23:03"
picture: "backhoe1.jpg"
weight: "19"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37944
- /detailse058.html
imported:
- "2019"
_4images_image_id: "37944"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-29T19:23:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37944 -->
Hier ein Bild wobei alle Abdeckplatten entfernt sind, damit man besser das Innenleben sehen kann.
Links unten: 2x Kompressor mit 2 mahl Pneumatik Ventil (1x ausleger Stick, 1x Ausleger Boom)
rechts unten: 1x XM motor und getriebe fur die neigung Spud
Mitte: 1x Robo Interface mit 2x Extension
Mitte oben: 1x Pneumatik Ventil (bucket)
