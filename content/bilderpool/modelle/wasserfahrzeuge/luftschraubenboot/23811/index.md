---
layout: "image"
title: "Luftschraubenboot (1)"
date: "2009-04-26T19:08:25"
picture: "luftschraubenboot1.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/23811
- /detailsf311.html
imported:
- "2019"
_4images_image_id: "23811"
_4images_cat_id: "1626"
_4images_user_id: "592"
_4images_image_date: "2009-04-26T19:08:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23811 -->
Die Idee, ein Luftschraubenboot zu bauen, ist dem Luftschraubenauto (http://www.ftcommunity.de/categories.php?cat_id=1595) entsprungen. Vom Antrieb her funktioniert es genau gleich.
Im Wasser wird das Boot sehr schnell. Zugleich ist es sehr wendig.