---
layout: "image"
title: "Boot- Treppe"
date: "2011-02-18T14:12:43"
picture: "boot04.jpg"
weight: "4"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30058
- /detailscdd2.html
imported:
- "2019"
_4images_image_id: "30058"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30058 -->
Hier kann man sehr schön die Treppe sehen, die aus Bausteinen 7,5 und Verbindungselemten 30 besteht.
