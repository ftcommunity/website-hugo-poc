---
layout: "image"
title: "Boot- von unten"
date: "2011-02-18T14:12:43"
picture: "boot11.jpg"
weight: "10"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30065
- /detailsa107.html
imported:
- "2019"
_4images_image_id: "30065"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30065 -->
Hier kann man sehr schön sehen, wie das Boot aufgedockt ist und auf dem Trockenen steht.
