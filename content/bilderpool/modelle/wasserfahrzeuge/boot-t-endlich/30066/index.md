---
layout: "image"
title: "Boot- Unter Deck"
date: "2011-02-18T16:56:40"
picture: "sdf1.jpg"
weight: "11"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30066
- /detailscef3.html
imported:
- "2019"
_4images_image_id: "30066"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T16:56:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30066 -->
Unter Deck. Hier sieht man den Empfänger und den Akku.
