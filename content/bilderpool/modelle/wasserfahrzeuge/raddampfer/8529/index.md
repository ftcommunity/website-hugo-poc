---
layout: "image"
title: "Antrieb der Schaufelräder"
date: "2007-01-20T16:45:44"
picture: "schiff02.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8529
- /detailsbd62-2.html
imported:
- "2019"
_4images_image_id: "8529"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8529 -->
