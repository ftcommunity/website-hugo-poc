---
layout: "image"
title: "Gesamt"
date: "2007-01-20T16:45:44"
picture: "schiff09.jpg"
weight: "9"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8536
- /details28a4.html
imported:
- "2019"
_4images_image_id: "8536"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8536 -->
Das Modell ist nur aus Fischertechnik gebaut, nur die Akkus sind kein ft.
