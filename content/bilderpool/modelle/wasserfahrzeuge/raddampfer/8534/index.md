---
layout: "image"
title: "Vorder Teil"
date: "2007-01-20T16:45:44"
picture: "schiff07.jpg"
weight: "7"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8534
- /detailsd76f-2.html
imported:
- "2019"
_4images_image_id: "8534"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8534 -->
Die grünen Akkus haben 7,2 Volt. Es wird nur einer gebraucht der andere ist nur als gewicht da.
Gesteuert wird es mit der Infrarot Fernsteuerung von Fischertechnik
