---
layout: "image"
title: "Schaufelräder"
date: "2007-01-20T16:45:44"
picture: "schiff01.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8528
- /detailsa93d.html
imported:
- "2019"
_4images_image_id: "8528"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8528 -->
Das Schiff fährt nach vorne/hinten wenn sich beide Räder gleich rum drehen, nach links und recht  fährt es wenn sich die Räder in Unterschiedliche richtungen drehen.
