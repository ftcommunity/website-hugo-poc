---
layout: "image"
title: "Gesamt im Wasser"
date: "2007-01-20T16:46:18"
picture: "schiff11.jpg"
weight: "11"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8538
- /detailsea93.html
imported:
- "2019"
_4images_image_id: "8538"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8538 -->
