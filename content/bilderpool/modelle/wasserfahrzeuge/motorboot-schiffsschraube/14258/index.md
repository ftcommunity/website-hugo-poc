---
layout: "image"
title: "Motorboot"
date: "2008-04-13T16:49:29"
picture: "motorbootmitschiffsschraube1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14258
- /detailsa8d6.html
imported:
- "2019"
_4images_image_id: "14258"
_4images_cat_id: "1319"
_4images_user_id: "747"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14258 -->
Das ist ein Gesamtbild des Motorboots.