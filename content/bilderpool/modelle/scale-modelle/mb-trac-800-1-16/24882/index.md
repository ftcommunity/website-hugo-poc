---
layout: "image"
title: "MB trac 800 22"
date: "2009-09-07T21:14:52"
picture: "MB_trac_31.jpg"
weight: "22"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24882
- /details7ded.html
imported:
- "2019"
_4images_image_id: "24882"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-09-07T21:14:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24882 -->
Detailansicht der Front. Die Vorderachse schafft es haarscharf, unter dem Powermotor zu pendeln. Den Baustein 5 unten rechts gibt es auch auf der anderen Seite - er dient als Anschlag der Pendelachse. Sonst würden die Räder am Aufbau schleifen.

Gut zu sehen die Blockierung der Zapfwelle vorn mittels Klemmbuchsen. Dadurch wird vermieden, dass die Zapfwelle beim Abbau von Zusatzgeräten versehentlich aus dem Fahrzeug gezogen wird.