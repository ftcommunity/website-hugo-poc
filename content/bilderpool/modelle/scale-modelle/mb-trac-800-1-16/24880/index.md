---
layout: "image"
title: "MB trac 800 20"
date: "2009-09-07T21:14:52"
picture: "MB_trac_29.jpg"
weight: "20"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24880
- /details97a7.html
imported:
- "2019"
_4images_image_id: "24880"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-09-07T21:14:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24880 -->
Der Powermotor hat mit Müh und Not unter die Fronthaube gepasst, die - aufgrund der Maßstabstreue - keinen Millimeter höher hätte sein dürfen.