---
layout: "image"
title: "MB trac 800 18"
date: "2009-09-07T21:14:51"
picture: "MB_trac_27.jpg"
weight: "18"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24878
- /detailsba6d.html
imported:
- "2019"
_4images_image_id: "24878"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-09-07T21:14:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24878 -->
Fronthaube, Führerhaus und Ladefläche entfernt.