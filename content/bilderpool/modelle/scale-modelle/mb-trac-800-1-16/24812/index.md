---
layout: "image"
title: "MB trac 800 14"
date: "2009-08-18T18:59:11"
picture: "MB_trac_22.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24812
- /details97c8-2.html
imported:
- "2019"
_4images_image_id: "24812"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24812 -->
Wie alle meinen neueren Fahrzeugmodelle hat auch mein MB trac einen Starthebel im Führerhaus. Die Statikstrebe ist gelenkig gelagert und aktiviert beim Bewegen nach vorn den Empfänger über einen Mini-Taster. Sehr realistisch, oder? ;o)

Im Grunde gehört aber an den Empfänger ab Werk ein kleiner Schalter zum An- und Ausschalten! Da hat FT wirklich was versäumt! Der Empfänger muss ja, da er optische Signale empfangen soll, sowieso immer sichtbar sein, also wäre ein kleiner Schalter auf der Oberseite auch immer zugänglich. Wirklich blöd, dass der fehlt...