---
layout: "image"
title: "MB trac 800 15"
date: "2009-08-20T19:03:04"
picture: "MB_trac_26.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24813
- /detailsa695.html
imported:
- "2019"
_4images_image_id: "24813"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-20T19:03:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24813 -->
Detailansicht vom Unterboden. Man erkennt gut das in den Rahmen eingearbeitete Servo und die dazugehörige Lenkstrebe. Die komplette Verkabelung des Fahrzeugs liegt gut verkleidet und kaum sichtbar im Inneren bzw. im Führerhaus.