---
layout: "image"
title: "MB trac 800 16"
date: "2009-08-20T19:03:04"
picture: "MB_trac_25.jpg"
weight: "16"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24814
- /detailse244.html
imported:
- "2019"
_4images_image_id: "24814"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-20T19:03:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24814 -->
Detailansicht der Hinterachse. Genau wie an der Vorderachse treibt eine Rastschnecke das Z20 auf der Radachse an.