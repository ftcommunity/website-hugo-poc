---
layout: "image"
title: "MB trac 800 1"
date: "2009-08-18T18:44:45"
picture: "MB_trac_05.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24799
- /details876e.html
imported:
- "2019"
_4images_image_id: "24799"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:44:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24799 -->
Hier stelle ich Euch mein ferngesteuertes Modell des MB trac 800 im Maßstab 1:16 vor. Im Gegensatz zu den meisten meiner anderen Modelle lag die höchste Priorität hier auf Optik und Maßstabstreue. Natürlich fährt und lenkt er und hat Allrad und andere technische Details, aber trotzdem waren mir das Aussehen und eine perfekte Nachbildung des Originals am wichtigsten.

Ich habe lange gerechnet und gemessen und fand im Maßstab 1:16 die am besten passende Größe zu den Traktorreifen D80 und der minimal möglichen Spurweite der gelenkten und angetriebenen Vorderachse. Somit ist das Modell ein MB trac 800 mit der Serienbereifung 14.9R24 auf der im Original maximal möglichen Spurweite von 1990 mm. Alle wichtigen Fahrzeugmaße (Länge, Breite, Höhe) sowie die Proportionen passen auf wenige Millimeter genau – die Optik ist aus allen Blickwinken sehr realistisch und originalgetreu.

Im Original hat ein MB trac Hinterradantrieb mit starr zuschaltbarem Vorderradantrieb. Beide Achsen enthalten sperrbare Differenziale. Mein Modell ist im Allradmodus dargestellt (Vorderachse starr mit Hinterachse verbunden), und die beiden Achsdifferenziale sind gesperrt dargestellt (also nicht vorhanden).

Im Original ist die Hinterachse nahezu starr am Rahmen befestigt, die Vorderachse ist pendelnd gelagert. Auch das habe ich im Modell nachgebildet.

Die horizontale Antriebswelle, die längs komplett durchs Fahrzeug geht, endet vorn und hinten in Zapfwellenanschlüssen. Diese Rastachsen sind gegen versehentliches Herausziehen beim Anschließen und Lösen von Zubehör mit Klemmbuchsen gesichert.

Das Modell ist bis in die letzte Ritze mit Technik vollgepackt, so dass keinerlei Bauraum für zusätzliche Funktionen, wie z.B. einen funktionierenden Heckkraftheber, mehr vorhanden war. Da aber der MB trac in Realität in der Basisausstattung sehr „nackt“ war, ist dieser Aufbauzustand durchaus realistisch. Der MB trac wurde ja nicht nur als klassischer Traktor eingesetzt, sondern oftmals auch als reiner Schlepper in Industrie, Forst- und Bauwirtschaft, wo Front- und Heckkraftheber nicht unbedingt notwendig sind.

Ausstattung des Modells:

- Antrieb durch Powermotor unter der Fronthaube
- starrer Allradantrieb (entspricht Hinterradantrieb mit starr zugeschalteter Vorderachse und gesperrten Achsdifferenzialen)
- Lenkung per Servo im Unterboden
- 2 Zapfwellen vorn und hinten (vom Fahrmotor mit angetrieben)
- Pendelachse vorn
- Spannungsversorgung durch 2 9V-Blockakkus über der Hinterachse (für den Standard-Akkupack war leider kein Platz)
- Empfänger im Führerhaus
- Starthebel im Führerhaus (schaltet den Empfänger an)

Technisch ist mein Modell leider nicht so optimal: Er fährt ganz passabel geradeaus und wühlt sich auch ganz ordentlich durch unwegsames Gelände. Der Lenkausschlag ist aber sehr gering, und dem Servo fehlt oftmals die Kraft, die Vorderachse unter Last und im Gelände zu lenken. Der Antrieb des vollständig starren Allrads verspannt dann naturgemäß... :o(

Das Modell ist daher statt fürs Gelände eher für die Vitrine geeignet. Dort macht es aber dann eine wirklich gute Figur!