---
layout: "image"
title: "MB trac 800 24"
date: "2009-09-07T21:14:52"
picture: "MB_trac_34.jpg"
weight: "24"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/24884
- /detailse689.html
imported:
- "2019"
_4images_image_id: "24884"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-09-07T21:14:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24884 -->
Auch hinten ist die Zapfwelle gegen versehentliches Herausziehen mit Klemmbuchsen gesichert.