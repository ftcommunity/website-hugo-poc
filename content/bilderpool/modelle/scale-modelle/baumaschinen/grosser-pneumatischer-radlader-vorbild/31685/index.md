---
layout: "image"
title: "Antrieb"
date: "2011-08-29T10:16:37"
picture: "catg15.jpg"
weight: "15"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31685
- /details1cde.html
imported:
- "2019"
_4images_image_id: "31685"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31685 -->
Diese 2 Powermots bewegen den Radlader. Dunkel kann man zwischen den Motoren ein Z20 erkennen. Es wird von beiden seiten von einem Z10 den Powermots angetrieben. Das Z20 sitzt auf einer Achse die durch das ganze Fahrzeug führt und mit einer Schnecke die Differntiale antreibt.