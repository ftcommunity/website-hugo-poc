---
layout: "image"
title: "Knicklenkung von der anderen Seite"
date: "2011-08-29T10:16:38"
picture: "catg19.jpg"
weight: "19"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31689
- /details51c3.html
imported:
- "2019"
_4images_image_id: "31689"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31689 -->
Hinter der Kolbenstange sieht man die Kardanwelle der Antriebsachse.