---
layout: "image"
title: "Radlader seite"
date: "2011-08-29T10:16:37"
picture: "catg05.jpg"
weight: "5"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31675
- /details1eb8.html
imported:
- "2019"
_4images_image_id: "31675"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31675 -->
wenn die Schaufel leer war konnte sie etwa auf die Höhe des Führerhäuschens gehoben werden.