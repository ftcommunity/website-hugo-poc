---
layout: "image"
title: "hinten Rechts"
date: "2011-08-29T10:16:38"
picture: "catg18.jpg"
weight: "18"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31688
- /details6812.html
imported:
- "2019"
_4images_image_id: "31688"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31688 -->
die Ecke im größeren Zusammenhang, der Polwendeschalter ist fürs vorwärts- bzw. rückwärts Fahren.