---
layout: "image"
title: "Knicklenkung Deteil"
date: "2011-08-29T10:16:38"
picture: "catg16.jpg"
weight: "16"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31686
- /detailsf26f.html
imported:
- "2019"
_4images_image_id: "31686"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31686 -->
Hier kann man die Zylinder zur lenkung sehen. Die entgegengesetzt angeschlossenen Zylinder hatten keine Probelm den Bagger zu lenken.
In der MItte läuft auch die Kardanwelle der Antriebsachse, leider kann man sie hir nicht sehen.