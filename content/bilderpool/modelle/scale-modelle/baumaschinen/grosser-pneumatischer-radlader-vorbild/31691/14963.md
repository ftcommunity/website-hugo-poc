---
layout: "comment"
hidden: true
title: "14963"
date: "2011-08-29T19:18:23"
uploadBy:
- "Thilo"
license: "unknown"
imported:
- "2019"
---
Also das ganze Modell wird ja nicht von einem FT Kompressor unter druck gesetzt, sondern von einem modellbau kompressor, der hat laut hersteller etwa 1 bar druck wenn er mit Luft läuft. Wenn ich dann die Schaufel in einer Position habe und kurzzeitig irgenwo hinfahren will, passiert es sehr schnell, dass die Pumpe das System so voll pumpt, dass sie den Druck nicht mehr erhöhen kann, das hat zur Folge, dass der Pumpmotor merklich angestrengt ist und langsamer wird, er kann sogar stehen bleiben. Um diese Phänomen zu unterbinden kann ich die Schaufelregler einfach auf die MItte stellen, so wird das Schaufelsystem dicht gemacht und dann den entlüftungsschalter unmstellen und die Pumoe pumpt dann praktisch umsonst. Die Luft geht einfach wieder raus. KLa könnte man den Kompressor auch einfach aus machen aber dann hat man nicht das schöne Baustellengeräusch. :) Hoffe es ist verständlich!
lG Thilo