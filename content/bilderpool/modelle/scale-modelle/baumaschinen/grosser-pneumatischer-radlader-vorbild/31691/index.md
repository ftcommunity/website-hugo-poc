---
layout: "image"
title: "Pneumatik"
date: "2011-08-29T10:16:38"
picture: "catg21.jpg"
weight: "21"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31691
- /details5b6c.html
imported:
- "2019"
_4images_image_id: "31691"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31691 -->
Drei manuelle Pneumatik Regler. von rechts nach links: Schaufel, Hubarm, Entlüftung.