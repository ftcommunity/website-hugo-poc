---
layout: "image"
title: "Schaufel Seite"
date: "2011-08-29T10:16:38"
picture: "catg29.jpg"
weight: "29"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31699
- /detailsb42c.html
imported:
- "2019"
_4images_image_id: "31699"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31699 -->
man sieht im Hintergrund den Zylinder der die Schaufel bewegt. Ich habe eine parallel Kinematik gebaut wie auch das Vorbild von Caterpillar hat. Eine Z Kinematik hätte bei der Bauweise auch vom PLatz nicht gepasst.