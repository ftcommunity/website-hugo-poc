---
layout: "image"
title: "Schaufel wird entladen"
date: "2011-08-29T10:16:37"
picture: "catg06.jpg"
weight: "6"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31676
- /detailsf915.html
imported:
- "2019"
_4images_image_id: "31676"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31676 -->
in dieser Position kann man die hintere Pendelachse sehr gut sehen.