---
layout: "comment"
hidden: true
title: "14967"
date: "2011-08-29T23:28:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
... da hilft nur eine Untersetzung direkt am Rad/Reifen. Vielleicht passt da ja ein Z30 auf die Radachse, mit Achsen 30 mit den Drehscheiben gekoppelt, die die Reifen tragen, und das Z30 dann von einem Z10 angetrieben?

Sehr schön ist er geworden!

Gruß,
Stefan