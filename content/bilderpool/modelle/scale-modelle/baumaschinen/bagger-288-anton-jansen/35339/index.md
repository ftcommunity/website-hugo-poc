---
layout: "image"
title: "bagger10"
date: "2012-08-15T17:21:05"
picture: "bagger288-10.jpg"
weight: "9"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/35339
- /details1b55.html
imported:
- "2019"
_4images_image_id: "35339"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-15T17:21:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35339 -->
