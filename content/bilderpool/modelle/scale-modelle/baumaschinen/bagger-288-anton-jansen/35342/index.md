---
layout: "image"
title: "bagger13"
date: "2012-08-15T17:21:06"
picture: "bagger288-13.jpg"
weight: "12"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/35342
- /details2b14-2.html
imported:
- "2019"
_4images_image_id: "35342"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-15T17:21:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35342 -->
