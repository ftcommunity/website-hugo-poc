---
layout: "image"
title: "bagger288.4"
date: "2013-01-26T22:29:42"
picture: "bagger-288.4_2.jpg"
weight: "16"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/36523
- /detailsc119-2.html
imported:
- "2019"
_4images_image_id: "36523"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2013-01-26T22:29:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36523 -->
