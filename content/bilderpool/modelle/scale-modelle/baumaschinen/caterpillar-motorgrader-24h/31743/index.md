---
layout: "image"
title: "Innenleben Ausleger Cat 24H"
date: "2011-09-03T15:55:27"
picture: "motorgraderh1_2.jpg"
weight: "22"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/31743
- /details6916.html
imported:
- "2019"
_4images_image_id: "31743"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-09-03T15:55:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31743 -->
Weil mir den Ausleger zu schwach geworden war, habe ich mir ein 15x15mm Aluminium Stab biegen lassen. Dieser Stab habe ich dan ins Ausleger hinein gebaut damit es jetzt sehr Stabil geworden ist.
