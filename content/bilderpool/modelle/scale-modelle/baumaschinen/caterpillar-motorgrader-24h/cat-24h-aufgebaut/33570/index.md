---
layout: "image"
title: "Caterpillar motorgrader 24H, Lenkanlage (von unten)"
date: "2011-11-27T00:13:34"
picture: "cath07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/33570
- /details16c9.html
imported:
- "2019"
_4images_image_id: "33570"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33570 -->
Hier kann man sehen wie das Modell lenkt. Eingebaut ist ein 1:50 powermotor der mit eine Kette ein Schnecke antriebt.
Es sind mehrere 90 grad Metall achsen gebraucht der durch den Aluminium Profil gleiten.