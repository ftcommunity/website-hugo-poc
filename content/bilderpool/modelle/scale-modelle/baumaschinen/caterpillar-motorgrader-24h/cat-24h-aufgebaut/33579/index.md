---
layout: "image"
title: "Caterpillar motorgrader 24H, Steuerung"
date: "2011-11-27T00:13:35"
picture: "cath16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/33579
- /detailsd84e-3.html
imported:
- "2019"
_4images_image_id: "33579"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33579 -->
Steuerung von der Caterpillar. Steuerung und Cat sind mit ein Bandkabel (40 adrig) verbunden.
Weil ich nicht genugend polwenderschalter habe (31340) , habe ich die mit den anderen Polwenderschalter (36707) und eine Taster gemacht.