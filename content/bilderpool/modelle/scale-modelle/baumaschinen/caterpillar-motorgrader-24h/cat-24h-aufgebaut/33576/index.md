---
layout: "image"
title: "Caterpillar motorgrader 24H, Pflug"
date: "2011-11-27T00:13:35"
picture: "cath13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/33576
- /details279e-3.html
imported:
- "2019"
_4images_image_id: "33576"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33576 -->
