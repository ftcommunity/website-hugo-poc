---
layout: "image"
title: "Caterpillar Motorgrade 24H cylinder"
date: "2011-04-02T23:50:37"
picture: "caterpillarmotorgradeh12.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30386
- /details12a0.html
imported:
- "2019"
_4images_image_id: "30386"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30386 -->
Hier seht mann wie die Cylinder angetrieben werd. Habe auf dit U-Getriebeachse 40 (31064) ein circlips eingebaut (gleich wie Achse aus Supertrucks #31285) damit die Achse nicht raus kann.