---
layout: "image"
title: "Caterpillar Motorgrader 24H aft side"
date: "2011-04-02T13:57:28"
picture: "caterpillarmotorgradeh04.jpg"
weight: "6"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30378
- /details5f4b.html
imported:
- "2019"
_4images_image_id: "30378"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T13:57:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30378 -->
