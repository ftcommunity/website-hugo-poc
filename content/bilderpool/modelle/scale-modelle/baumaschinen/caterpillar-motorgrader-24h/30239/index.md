---
layout: "image"
title: "Caterpillar Motorgrader 24H (noch lange nicht fertig)"
date: "2011-03-08T18:28:51"
picture: "motorgraderh1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30239
- /detailse882.html
imported:
- "2019"
_4images_image_id: "30239"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-03-08T18:28:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30239 -->
Lange = ca 130cm
Breite = ca 450mm
Hohe = ca 450mm
