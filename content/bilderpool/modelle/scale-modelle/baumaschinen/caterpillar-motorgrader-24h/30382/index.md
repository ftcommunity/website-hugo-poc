---
layout: "image"
title: "Caterpillar Motorgrade 24H turntable"
date: "2011-04-02T13:57:28"
picture: "caterpillarmotorgradeh08.jpg"
weight: "10"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30382
- /detailsd54d-2.html
imported:
- "2019"
_4images_image_id: "30382"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T13:57:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30382 -->
