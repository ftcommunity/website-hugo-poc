---
layout: "image"
title: "Caterpillar Motorgrade 24H Blade out"
date: "2011-04-02T23:50:37"
picture: "caterpillarmotorgradeh15.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/30389
- /detailsd822-2.html
imported:
- "2019"
_4images_image_id: "30389"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30389 -->
