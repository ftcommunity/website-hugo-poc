---
layout: "image"
title: "Antrieb Cat 24H"
date: "2011-07-28T21:44:14"
picture: "cath1.jpg"
weight: "18"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/31401
- /details18c3.html
imported:
- "2019"
_4images_image_id: "31401"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-07-28T21:44:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31401 -->
Hier ein Bild wie den Caterpillar Grader 24H angetrieben wird.
Gebraucht werden 2 Igarashi Motore. Differential ist nicht eingebaut weil die Standard ft differentiele sind zu klein (nicht stark genug) und ein Eigenbau differential nicht rein passte.
