---
layout: "image"
title: "CAT D11R CD (Carrydozer) Vorderseite"
date: "2013-03-19T22:15:53"
picture: "drcd03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/36774
- /detailsd31a-2.html
imported:
- "2019"
_4images_image_id: "36774"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36774 -->
Fast alles mit Orginale Teile gebaut. Naturlich einige "nicht" fischertechnik Teile gebraucht wie einige Madenschraube Achsen verbindungen. Madenschraube Zahnrader, Achsen und Gewindestangen.