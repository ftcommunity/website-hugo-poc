---
layout: "image"
title: "Smart Fortwo 4"
date: "2007-01-07T17:53:02"
picture: "Smart_Fortwo_06.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8325
- /detailsfc34.html
imported:
- "2019"
_4images_image_id: "8325"
_4images_cat_id: "766"
_4images_user_id: "328"
_4images_image_date: "2007-01-07T17:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8325 -->
