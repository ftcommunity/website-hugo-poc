---
layout: "image"
title: "TEREX_60"
date: "2007-02-03T16:32:37"
picture: "TEREX_60.jpg"
weight: "71"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/8826
- /details2fc4.html
imported:
- "2019"
_4images_image_id: "8826"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2007-02-03T16:32:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8826 -->
