---
layout: "image"
title: "terex_20"
date: "2005-03-26T19:25:32"
picture: "terex_20.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3875
- /details5744-2.html
imported:
- "2019"
_4images_image_id: "3875"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-26T19:25:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3875 -->
Kabine mit zwei Fahrer und der schaltcentrale.