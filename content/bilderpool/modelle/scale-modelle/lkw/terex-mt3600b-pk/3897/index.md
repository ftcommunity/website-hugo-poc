---
layout: "image"
title: "terex_47"
date: "2005-03-26T19:25:58"
picture: "terex_47.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3897
- /detailsfe08.html
imported:
- "2019"
_4images_image_id: "3897"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-26T19:25:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3897 -->
Das gleige für die vorderwand der Mulde.