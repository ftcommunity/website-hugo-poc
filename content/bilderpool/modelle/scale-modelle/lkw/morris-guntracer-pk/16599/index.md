---
layout: "image"
title: "MORRIS C.8.Mkll _1"
date: "2008-12-14T10:53:09"
picture: "MORIS_GUNTRACER_1_1.jpg"
weight: "1"
konstrukteure: 
- "peter krijnen"
fotografen:
- "peter krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/16599
- /detailsfb76-2.html
imported:
- "2019"
_4images_image_id: "16599"
_4images_cat_id: "1503"
_4images_user_id: "144"
_4images_image_date: "2008-12-14T10:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16599 -->
Der erste versuch aus der 80er jaren.

Obwol ich in gedächtnis hatte das diese kleine Lafette von der marke MORIS hergesteld wurde, hab ich, nachdem ich der bauanleitung wieder gefunden hab, feststellen mussen das der von MORRIS hergesteld wurde und als Typenbezeichnung C.8.Mkll mit auf den weg bekommen hat.