---
layout: "image"
title: "Unimog U1300L 02"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_01.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33393
- /details2376.html
imported:
- "2019"
_4images_image_id: "33393"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33393 -->
Wie es sich für einen Mercedes-Benz gehört, hat er vorn einen Stern. Nur mit etwas mehr Zacken ... ;o)