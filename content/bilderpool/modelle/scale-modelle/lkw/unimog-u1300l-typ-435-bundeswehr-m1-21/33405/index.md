---
layout: "image"
title: "Unimog U1300L 14"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_14.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33405
- /detailsec8c.html
imported:
- "2019"
_4images_image_id: "33405"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33405 -->
Die Pendelachse hinten. Gut zu sehen sind die Statikstreben vor der Achse, die als Blattfedern dienen und sich am Rahmen abstützen. Einfach, aber es funktioniert!

Die Aufhängung der Achse hinten sieht etwas seltsam und kompliziert aus, war aber notwendig, um die im Lastenheft geforderten 45° Rampenwinkel zu erreichen. Die Achse stützt sich hinten quasi direkt an der Kardanwelle ab.