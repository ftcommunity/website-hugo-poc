---
layout: "image"
title: "Unimog U1300L 04"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33395
- /details1d09.html
imported:
- "2019"
_4images_image_id: "33395"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33395 -->
Im Hintergrund die Maßzeichnung, die ich als Vorlage verwendet habe. Zusätzlich natürlich noch viele andere relevante Maße, die ich mir im Internet besorgt habe.