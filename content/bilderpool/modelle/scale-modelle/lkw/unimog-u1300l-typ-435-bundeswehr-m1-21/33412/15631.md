---
layout: "comment"
hidden: true
title: "15631"
date: "2011-11-06T11:40:16"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Jaja, die Rampe ... :o/ Ich habe beschlossen, meinen maßstabsgetreuen Unimog erstmal so zu belassen, wie er hier gezeigt ist. Der Antrieb ist schon sehr sehr stabil dargestellt, und es geht im Gelände wirklich super voran. Die einzige Schwachstelle ist derzeit die Übertragung über das Zahnrad vom Motor auf das Zentraldifferenzial. Da knackt es bei schwiergien Situationen gern unangenehm ... ;o) Aber bei der durch den Maßstab vorgegebenen Länge ließ sich das nicht noch stabiler darstellen.

Nachteilig für die Steigfähigkeit an der Rampe ist auch das hohe Gewicht von Führerhaus, Planen-Aufbau und anderen optischen Details, die für eine möglichst originalgetreue Darstellung notwendig waren.

Trotzdem: Dieser Unimog ist das stabilste und geländegängigste Fahrzeug, was ich je gebaut habe!

Das Chassis mit Achsen und Antrieb (evtl. etwas länger) wird aber die Basis sein für einen leichten simplen Allradler, mit dem ich die Rampenversuche durchführen und dokumentieren werden.

Gruß, Thomas