---
layout: "image"
title: "Unimog U1300L 21"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_21.jpg"
weight: "21"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33412
- /detailsa3bb.html
imported:
- "2019"
_4images_image_id: "33412"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33412 -->
Die Plane von vorn.