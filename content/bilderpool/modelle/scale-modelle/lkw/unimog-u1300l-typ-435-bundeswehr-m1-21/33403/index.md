---
layout: "image"
title: "Unimog U1300L 12"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_12.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/33403
- /detailsde3c.html
imported:
- "2019"
_4images_image_id: "33403"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33403 -->
Der Radstand ergibt rechnerisch 15,5 cm, was zufällig genau das Rastermaß ist, um die Länge der zu tief im Zentraldifferenzial steckenden Rastachsen auszugleichen, super! Ich musste nur vor dem Differenzial im Rahmen einen Baustein 5 einfügen, dann passte der Radstand und die Rastachsen steckten satt im Differenzial, so wie es sein soll.