---
layout: "image"
title: "Die Lenkung"
date: "2014-06-19T19:13:42"
picture: "Bild_025.jpg"
weight: "8"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38968
- /details4dfc.html
imported:
- "2019"
_4images_image_id: "38968"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38968 -->
Der Lenkungsmotor treibt ein Schneckengetriebe an. Dieses leitet die Bewegung über ein Z10 an eine M4-Gewindestange weiter. Der Gewindebaustein auf dieser bewegt dann die Lenkstange.