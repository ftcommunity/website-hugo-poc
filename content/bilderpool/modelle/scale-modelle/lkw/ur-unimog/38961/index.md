---
layout: "image"
title: "Ur-Unimog"
date: "2014-06-19T19:13:42"
picture: "Bild_019.jpg"
weight: "1"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38961
- /detailsed03.html
imported:
- "2019"
_4images_image_id: "38961"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38961 -->
Da das Model bereits zerlegt ist, gibt es keine weiteren Fotos mehr.