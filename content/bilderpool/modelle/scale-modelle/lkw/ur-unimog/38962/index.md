---
layout: "image"
title: "Seitenansicht"
date: "2014-06-19T19:13:42"
picture: "Bild_015.jpg"
weight: "2"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38962
- /details6ffe.html
imported:
- "2019"
_4images_image_id: "38962"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38962 -->
