---
layout: "comment"
hidden: true
title: "17016"
date: "2012-07-22T20:25:55"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Bilder folgen.
Ich benutze nur zwei Geschwindigkeiten. Die schnelle hat eine hervorragende Geschwindigkeit, sowohl um den Oberwagen nur zu schwenken als auch um die Last sicher aber schnell zu transportieren. Die niedrigere Geschwindigkeit eignet sich besonders gut um die Last auf den Millimeter abzusetzen. Sie eignet sich aber trotzdem auch um den Oberwagen in akzeptabler Zeit zu drehen.
Funktion: Beide Motore treiben die Wellen des Diffenzialgetriebes an. Das Zahnrad auf den Diff.Gehäuse ist der Abtrieb. Meist bleibt also eine Welle des Diff.Getriebe stehen.
Insgesamt ist dieser Antrieb mit seinen Geschwindigkeiten das beste was mir bisher im Kranbau gelungen ist