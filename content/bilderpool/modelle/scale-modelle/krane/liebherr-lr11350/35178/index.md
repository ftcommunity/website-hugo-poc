---
layout: "image"
title: "-9.1_2"
date: "2012-07-16T20:37:42"
picture: "-9.1_2.jpg"
weight: "19"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/35178
- /details6f09.html
imported:
- "2019"
_4images_image_id: "35178"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-07-16T20:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35178 -->
