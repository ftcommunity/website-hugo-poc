---
layout: "image"
title: "Ansicht von oben"
date: "2012-06-15T21:09:19"
picture: "LR_11350_Ansicht_von_oben.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/35053
- /details93d6-2.html
imported:
- "2019"
_4images_image_id: "35053"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-06-15T21:09:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35053 -->
