---
layout: "comment"
hidden: true
title: "16962"
date: "2012-07-07T16:23:14"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
Der Drehkranz im Hintergrund gehört zu einem LR11350. Der Kontakt entstand vor vielen Jahren. Auch die anderen Modellbauer (Lego, Stahl, Alu) sind langjährig bekannte und freuten sich über diese Ehre dabei sein zu dürfen. 

Ich sag dir - das war das Paradies im Kranbau...

Gruß Jürgen