---
layout: "image"
title: "gottwald 13"
date: "2010-03-06T19:20:54"
picture: "gottwald_13.jpg"
weight: "11"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26610
- /details3bfc.html
imported:
- "2019"
_4images_image_id: "26610"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26610 -->
