---
layout: "image"
title: "gottwald 25"
date: "2010-03-07T10:12:48"
picture: "gottwald_25.jpg"
weight: "25"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26642
- /details95ed.html
imported:
- "2019"
_4images_image_id: "26642"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26642 -->
