---
layout: "image"
title: "gottwald 5"
date: "2010-03-06T19:20:54"
picture: "gottwald_5.jpg"
weight: "19"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26618
- /detailscc8a-2.html
imported:
- "2019"
_4images_image_id: "26618"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26618 -->
