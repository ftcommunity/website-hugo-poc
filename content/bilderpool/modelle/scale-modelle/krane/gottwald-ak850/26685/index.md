---
layout: "image"
title: "gottwald 35"
date: "2010-03-13T17:40:46"
picture: "gottwald_35.jpg"
weight: "35"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26685
- /detailsb46f.html
imported:
- "2019"
_4images_image_id: "26685"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26685 -->
