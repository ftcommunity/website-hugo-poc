---
layout: "image"
title: "gottwald 11"
date: "2010-03-06T19:20:54"
picture: "gottwald_11.jpg"
weight: "13"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26612
- /details036f-2.html
imported:
- "2019"
_4images_image_id: "26612"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26612 -->
