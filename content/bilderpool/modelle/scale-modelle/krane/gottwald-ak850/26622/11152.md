---
layout: "comment"
hidden: true
title: "11152"
date: "2010-03-06T22:23:30"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Gewaltig. Meisterhaft in jedem Detail, einfach Spitzenklasse. Das ist ja schon nicht mehr Modellbau, das ist richtiger Maschinenbau. Riesenkompliment!

Gruß,
Stefan