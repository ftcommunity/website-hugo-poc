---
layout: "image"
title: "gottwald 31"
date: "2010-03-07T22:41:44"
picture: "gottwald_31.jpg"
weight: "31"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/26663
- /details490e-2.html
imported:
- "2019"
_4images_image_id: "26663"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-07T22:41:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26663 -->
