---
layout: "image"
title: "MK500-88 van Twist_19"
date: "2016-10-17T17:40:21"
picture: "mkvantwist8.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44599
- /details1888.html
imported:
- "2019"
_4images_image_id: "44599"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44599 -->
Da der Drehkranz nur 58 Zähnen hat, mus der Motor auserhalb das 15er ft Raster eingebaut worden.