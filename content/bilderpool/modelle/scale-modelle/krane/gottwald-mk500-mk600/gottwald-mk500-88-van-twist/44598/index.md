---
layout: "image"
title: "MK500-88 van Twist_18"
date: "2016-10-17T17:40:21"
picture: "mkvantwist7.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44598
- /details8dd8.html
imported:
- "2019"
_4images_image_id: "44598"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44598 -->
Zur drehen der Kran gibts auch ein "Micro Metal Gear" motor. Jetzt aber mit ein Überzetsung von 1:1000