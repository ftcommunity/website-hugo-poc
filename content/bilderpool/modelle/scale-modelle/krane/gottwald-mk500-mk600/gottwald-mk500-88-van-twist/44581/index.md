---
layout: "image"
title: "MK500-88 van Twist_5"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44581
- /details21b5-2.html
imported:
- "2019"
_4images_image_id: "44581"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44581 -->
Der MK500-88 konte mittels 4 angetriebene Achsen auf den Bauplatz verfahren werden.
Auf der Strasse war er aber auf ein Zugmaschiene angewiesen.