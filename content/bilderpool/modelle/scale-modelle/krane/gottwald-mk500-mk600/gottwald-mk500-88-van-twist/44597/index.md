---
layout: "image"
title: "MK500-88 van Twist_17"
date: "2016-10-17T17:40:21"
picture: "mkvantwist6.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44597
- /details768a.html
imported:
- "2019"
_4images_image_id: "44597"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44597 -->
Zur Montage hab ich 2 "Bausteinen 15, mit Loch" auf 12mm ausgebohrt.