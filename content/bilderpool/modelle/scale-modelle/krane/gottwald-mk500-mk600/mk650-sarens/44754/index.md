---
layout: "image"
title: "MK650 Sarens_6"
date: "2016-11-13T15:14:19"
picture: "mksarens6.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44754
- /details05b4.html
imported:
- "2019"
_4images_image_id: "44754"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44754 -->
Die Hausfarbe von Sarens ist blau. Da es von ft keinen blauen teilen (mehr) gibt hab ich diesen Kran mal im ft Grau/rot gebaut,