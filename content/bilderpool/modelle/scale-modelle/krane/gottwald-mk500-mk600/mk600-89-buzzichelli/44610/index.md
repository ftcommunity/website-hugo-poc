---
layout: "image"
title: "MK600-89 Buzzichelli_1"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44610
- /detailsffd6.html
imported:
- "2019"
_4images_image_id: "44610"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44610 -->
Gegenüber der MK500-88, war der MK600-89 um etwa 250mm länger.