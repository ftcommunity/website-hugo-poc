---
layout: "image"
title: "MK600-89 Buzzichelli_10"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44619
- /details0926-4.html
imported:
- "2019"
_4images_image_id: "44619"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44619 -->
Zur montage der Abstützarme benutze ich 2x Gelenkstein 22,5 und 2x Rollenlager 15.
Natürlich auch ein Metalachse 50.