---
layout: "image"
title: "MK600-89 Buzzichelli_7"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44616
- /details31a7-2.html
imported:
- "2019"
_4images_image_id: "44616"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44616 -->
Zicht von oben auf der zusätliche Kabine.