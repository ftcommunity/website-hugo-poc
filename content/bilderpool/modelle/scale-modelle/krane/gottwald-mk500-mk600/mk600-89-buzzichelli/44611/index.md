---
layout: "image"
title: "MK600-89 Buzzichelli_2"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44611
- /details3a91.html
imported:
- "2019"
_4images_image_id: "44611"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44611 -->
Auf grund der eingeschränkten Sichtverhältnisse wurde bei die MK600 ein zusätzliche Kabine montiert.