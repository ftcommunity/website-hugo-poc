---
layout: "image"
title: "MK600-89 Buzzichelli_6"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44615
- /detailsf812.html
imported:
- "2019"
_4images_image_id: "44615"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44615 -->
Von oben.