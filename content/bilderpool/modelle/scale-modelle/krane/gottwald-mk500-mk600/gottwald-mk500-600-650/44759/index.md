---
layout: "image"
title: "Gottwald MK500/600/650_4"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk4.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44759
- /detailsb97f.html
imported:
- "2019"
_4images_image_id: "44759"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44759 -->
Die steuerung solte natürlich auf alle Kränen genutzt werden.
Darzu hab ich auf die Modellen eine Platine mit 8 kontkflächen eingebaut.
Als Kontakt hab ich 4 Feder halbiert. Die hab ich auf 8 V-radachsen 36586, gesteckt. Die Feder werden mit 8 Klemmkontakten 31338, in 2 Winkelträger 60 gesteckt und mit Klemmbuchse 5 fest gehalten, Die Klemmkontakten werden dan mit Stecker und draht mit der IR-Emfänger verbunden