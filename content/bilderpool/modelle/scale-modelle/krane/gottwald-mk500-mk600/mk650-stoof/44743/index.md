---
layout: "image"
title: "MK650 Stoof_1"
date: "2016-11-13T15:14:18"
picture: "mkstoof1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44743
- /details27a1.html
imported:
- "2019"
_4images_image_id: "44743"
_4images_cat_id: "3333"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44743 -->
Modell der in 1972 an die niederländische Firma Stoof gelieferte MK650 in Maßstab 1:27.