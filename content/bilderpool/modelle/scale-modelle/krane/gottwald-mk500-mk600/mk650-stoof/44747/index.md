---
layout: "image"
title: "MK650 Stoof_5"
date: "2016-11-13T15:14:18"
picture: "mkstoof5.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44747
- /details34d8.html
imported:
- "2019"
_4images_image_id: "44747"
_4images_cat_id: "3333"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44747 -->
Modell diesen Kran ist gleich an die beide Kränen von Sparrow und van Driessen.
Nur die Unterwagen sind unterschiedlich.