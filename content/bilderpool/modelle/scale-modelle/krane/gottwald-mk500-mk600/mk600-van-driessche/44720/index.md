---
layout: "image"
title: "MK600 van Driessche_5"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44720
- /details81d8.html
imported:
- "2019"
_4images_image_id: "44720"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44720 -->
Das vordere Fahrgestell wurde mit einem langen Schwanenhals auf die Zugmaschine aufgesattelt.