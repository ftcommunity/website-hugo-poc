---
layout: "image"
title: "MK600 van Driessche_6"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44721
- /details7166.html
imported:
- "2019"
_4images_image_id: "44721"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44721 -->
Von der linke Seite.
Leider mus ich bekennen das ich der zusätzlichem Sockelballast vergessen bin.
In betrieb wurden die Fahrgestellen meistens entfernt.
Um das fehlen eines Fahrgestell auszugleichen, wurde die MK600 mit ingesamt 50t Ballast zwischen die Abstützarmen, ausgestattet.