---
layout: "overview"
title: "MK600 van Driessche"
date: 2020-02-22T08:23:47+01:00
legacy_id:
- /php/categories/3331
- /categoriesfb2f.html
- /categories4af5.html
- /categories440e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3331 --> 
Modell der in 1969 an die belgische Firma van Driessche gelieferte MK600 in Maßstab 1:27.