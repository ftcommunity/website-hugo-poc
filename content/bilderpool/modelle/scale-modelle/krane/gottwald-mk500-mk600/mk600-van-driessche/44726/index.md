---
layout: "image"
title: "MK600 van Driessche_11"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche11.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44726
- /details4abf-2.html
imported:
- "2019"
_4images_image_id: "44726"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44726 -->
In transport aufstellung.
Problem mit dieses Modell sind die Reifen/Räder.
Die Reifen der Mack solten 38mm sein und die von die beide Fahrgestellen 34mm.
38mm gibts von ft nicht und von der 34er hab ich nur 12 Stück. Und auch keine Gelbe 23er V-Räder.