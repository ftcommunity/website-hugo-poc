---
layout: "comment"
hidden: true
title: "22729"
date: "2016-11-02T15:40:50"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr gelungen! Interessant, wie man so einen großen Kran auf einem Fahrzeug mit doch relativ kleiner Spurweite (auch im ft-Modell) bauen kann. Das Führerhaus gefällt mir auch super - wer brauchte nochmal diese komischen Fertig-Führerhäuschen? :-)

Gruß,
Stefan