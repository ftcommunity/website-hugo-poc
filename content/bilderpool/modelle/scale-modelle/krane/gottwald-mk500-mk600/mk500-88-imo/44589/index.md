---
layout: "image"
title: "MK500-88 IMO_2"
date: "2016-10-16T16:02:29"
picture: "mkimo2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44589
- /detailsc1bb.html
imported:
- "2019"
_4images_image_id: "44589"
_4images_cat_id: "3319"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44589 -->
Durchblick durch der Auslegerfus.