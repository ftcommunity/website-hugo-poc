---
layout: "overview"
title: "MK500-88 IMO"
date: 2020-02-22T08:23:45+01:00
legacy_id:
- /php/categories/3319
- /categories9ead.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3319 --> 
Modell der in 1971 an die Firma IMO Merseburg gelieferte MK500-88 in Maßstab 1:27.