---
layout: "image"
title: "MK650 Schmidbauer_4"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44732
- /details2838-2.html
imported:
- "2019"
_4images_image_id: "44732"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44732 -->
Zur besseren gewichtsverteilung: Abstützschwingen
