---
layout: "image"
title: "MK650 Schmidbauer_8"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44736
- /details9620.html
imported:
- "2019"
_4images_image_id: "44736"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44736 -->
Die beide Haken etwas näer