---
layout: "image"
title: "MK650 Schmidbauer_9"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44737
- /detailsffac.html
imported:
- "2019"
_4images_image_id: "44737"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44737 -->
Nur noch die Abstützarme entfernen, dan gehts ab zur nächte baustelle.