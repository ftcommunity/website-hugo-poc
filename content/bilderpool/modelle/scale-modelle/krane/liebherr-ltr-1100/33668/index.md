---
layout: "image"
title: "Heckballast"
date: "2011-12-13T23:22:52"
picture: "liebherrltr25.jpg"
weight: "25"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33668
- /detailscf17-2.html
imported:
- "2019"
_4images_image_id: "33668"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33668 -->
Erinnert doch an das Vorbild :)