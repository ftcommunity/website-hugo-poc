---
layout: "image"
title: "Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr11.jpg"
weight: "11"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33654
- /detailse3c9-2.html
imported:
- "2019"
_4images_image_id: "33654"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33654 -->
Durch das Licht wirkt der Kran lebendig :)
Der Kran richtet sich auf und schon so wirkt er ziemlich mächtig.