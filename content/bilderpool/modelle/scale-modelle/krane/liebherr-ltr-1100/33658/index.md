---
layout: "image"
title: "Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr15.jpg"
weight: "15"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33658
- /details26da-2.html
imported:
- "2019"
_4images_image_id: "33658"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33658 -->
Das ist die Maximale Länge das Auslegers. Wenn der Ausleger ganz steil steht, ist der Kran etwa 2 m hoch, an der Oberen Bildkante erkennt man die Decke.