---
layout: "image"
title: "LIEBHERR"
date: "2011-12-13T23:22:51"
picture: "liebherrltr09.jpg"
weight: "9"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33652
- /detailsd2c4.html
imported:
- "2019"
_4images_image_id: "33652"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33652 -->
Auf den lackierten Ausleger habe ich mit einer Schablone den Firmennamen gesprüht.
Unter dem Ausleger sieht man den Zylinder, der den Auslger bewegt. (Funktion wird später erklärt)