---
layout: "image"
title: "Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr02.jpg"
weight: "2"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33645
- /detailsa875-2.html
imported:
- "2019"
_4images_image_id: "33645"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33645 -->
damit man weiß woran ich mich orientiert habe, das ist ein Bild des Vorbilds