---
layout: "comment"
hidden: true
title: "15871"
date: "2011-12-14T10:00:20"
uploadBy:
- "Thilo"
license: "unknown"
imported:
- "2019"
---
Ja ich habe mir das natürlich auch aus der realität abgeguckt. Hebebühnen fahren oft so aus. Also insgesammt fahren die Teleskope gut aus, am anfang rattert es manchmal ziemlich weil die Gewindestange dann von innen an die Wände des Teleskops schlägt. Das hört aber nach ca. 1/4 auf wenn die Mutter weiter hochgeschraubt ist und die Stange zu stabilisiert. Steif und belastbar ist die Konstruktion allemal. Der große Vorteil vom Holz ist die einfache Verarbeitung der geringe Preis und dies sehr geringe Gewicht.