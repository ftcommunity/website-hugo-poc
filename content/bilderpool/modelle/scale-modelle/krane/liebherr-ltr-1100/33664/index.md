---
layout: "image"
title: "Empfänger"
date: "2011-12-13T23:22:51"
picture: "liebherrltr21.jpg"
weight: "21"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/33664
- /detailsec3c-2.html
imported:
- "2019"
_4images_image_id: "33664"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33664 -->
Zu sehen sind die IR-Empfänger, links davon sieht man etwas helles. Das ist der Pneumatikkompressor und darüber befinden sich die Ventile.