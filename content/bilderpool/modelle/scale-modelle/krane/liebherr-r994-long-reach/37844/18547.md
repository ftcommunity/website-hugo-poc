---
layout: "comment"
hidden: true
title: "18547"
date: "2014-01-07T21:50:03"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Mann-o-Mann! 11 kg hat nicht mal meine "Antonov" gewogen. Die hohen Drücke sind das eine, aber auf der Oberseite gibt es ja genau so hohe Zugkräfte. Hast du da noch etwas eingebaut, damit keine ft-Zapfen herausgezogen werden?

Gruß,
Harald