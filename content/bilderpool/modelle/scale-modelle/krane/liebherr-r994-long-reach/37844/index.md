---
layout: "image"
title: "Liebherr R994"
date: "2013-11-27T14:54:23"
picture: "PB260353.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/37844
- /detailsb989.html
imported:
- "2019"
_4images_image_id: "37844"
_4images_cat_id: "2812"
_4images_user_id: "838"
_4images_image_date: "2013-11-27T14:54:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37844 -->
Mijn nieuwste model een Liebherr R994 in een Long Reach uitvoering
Volledig functioneel weegt 10 a 11 kg. Alleen het contra gewicht is al 3,8 kg!!
Reikwijdte van de arm is 83 cm van uit het centum van het draaipunt.