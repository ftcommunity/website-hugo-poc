---
layout: "image"
title: "Gesamtansicht 5"
date: "2006-04-14T22:36:45"
picture: "IMG_4416.jpg"
weight: "30"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6116
- /details6f87.html
imported:
- "2019"
_4images_image_id: "6116"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6116 -->
