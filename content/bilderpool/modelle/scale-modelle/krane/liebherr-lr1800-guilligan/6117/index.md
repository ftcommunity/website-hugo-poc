---
layout: "image"
title: "Vorderansicht"
date: "2006-04-14T22:37:04"
picture: "IMG_4420.jpg"
weight: "31"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6117
- /details8f12.html
imported:
- "2019"
_4images_image_id: "6117"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6117 -->
leider ist in dem Hubseil ein Drall drin...
