---
layout: "image"
title: "Aufbau"
date: "2006-04-14T22:36:45"
picture: "IMG_4400.jpg"
weight: "24"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6109
- /details9174.html
imported:
- "2019"
_4images_image_id: "6109"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6109 -->
