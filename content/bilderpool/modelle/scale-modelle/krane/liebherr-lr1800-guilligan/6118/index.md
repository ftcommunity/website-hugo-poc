---
layout: "image"
title: "Seitenansicht"
date: "2006-04-14T22:37:04"
picture: "IMG_4432.jpg"
weight: "32"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6118
- /details016c-4.html
imported:
- "2019"
_4images_image_id: "6118"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6118 -->
Der Kran ist schon stabil gebaut aber zur Herbstzeit an der Nordsee würde ich ihn nicht aufbauen...
