---
layout: "image"
title: "Gesamtansicht 3"
date: "2006-04-14T22:36:45"
picture: "IMG_4408.jpg"
weight: "28"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6114
- /detailsf090.html
imported:
- "2019"
_4images_image_id: "6114"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6114 -->
