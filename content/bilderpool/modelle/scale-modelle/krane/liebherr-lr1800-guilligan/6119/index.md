---
layout: "image"
title: "von oben"
date: "2006-04-14T22:37:04"
picture: "IMG_4441.jpg"
weight: "33"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6119
- /detailsfbc5.html
imported:
- "2019"
_4images_image_id: "6119"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6119 -->
