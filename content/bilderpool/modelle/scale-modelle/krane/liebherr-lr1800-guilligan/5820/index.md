---
layout: "image"
title: "LR1800 009"
date: "2006-03-05T12:34:12"
picture: "LR1800_009.JPG"
weight: "8"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/5820
- /details2ff9.html
imported:
- "2019"
_4images_image_id: "5820"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-03-05T12:34:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5820 -->
