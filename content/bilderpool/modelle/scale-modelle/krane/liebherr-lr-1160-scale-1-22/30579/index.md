---
layout: "image"
title: "lr1160-2"
date: "2011-05-18T21:37:51"
picture: "lr1160-2.jpg"
weight: "3"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30579
- /detailsb23f.html
imported:
- "2019"
_4images_image_id: "30579"
_4images_cat_id: "2279"
_4images_user_id: "541"
_4images_image_date: "2011-05-18T21:37:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30579 -->
