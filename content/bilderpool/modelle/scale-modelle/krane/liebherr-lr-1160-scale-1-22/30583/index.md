---
layout: "image"
title: "lr1160-6"
date: "2011-05-18T21:37:51"
picture: "lr1160-6.jpg"
weight: "7"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30583
- /details7a76.html
imported:
- "2019"
_4images_image_id: "30583"
_4images_cat_id: "2279"
_4images_user_id: "541"
_4images_image_date: "2011-05-18T21:37:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30583 -->
