---
layout: "image"
title: "Liebherr-FT-1-Poederoyen-NL"
date: "2016-07-01T21:07:15"
picture: "liebherrftpoederoyen17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/43828
- /detailscc52.html
imported:
- "2019"
_4images_image_id: "43828"
_4images_cat_id: "3246"
_4images_user_id: "22"
_4images_image_date: "2016-07-01T21:07:15"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43828 -->
Drehkrans: SKF Naaldset AXK 7095 + SKF Taatsring AS 7095
