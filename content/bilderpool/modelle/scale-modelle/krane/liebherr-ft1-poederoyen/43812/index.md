---
layout: "image"
title: "Liebherr-FT-1-Poederoyen-NL"
date: "2016-07-01T21:07:15"
picture: "liebherrftpoederoyen01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/43812
- /details309d.html
imported:
- "2019"
_4images_image_id: "43812"
_4images_cat_id: "3246"
_4images_user_id: "22"
_4images_image_date: "2016-07-01T21:07:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43812 -->
Insgesamm 6st micro-getriebe-motoren 110 rpm zum:
Antrieb Tracks links
Antrieb Tracks rechts
Antrieb Tracks -in + out
Antrieb Drehkranz

Antrieb Kabel 2x
Antrieb Kabel 1x mit Servo-360-grad
Antrieb Kabel 1x mit S-motor
micromotoren gibt es bei 
https://www.pololu.com/category/60/micro-metal-gearmotors 

oder noch billiger : 
http://nl.aliexpress.com

https://nl.aliexpress.com/item/New-DC-12V-100RPM-Mini-Metal-Gear-Motor-with-Gearwheel-N20-3mm-Shaft-Diameter/32767629574.html?spm=2114.010108.3.1.88E2S7&ws_ab_test=searchweb0_0,searchweb201602_5_10152_10208_10065_10151_10068_5330012_10304_10301_10136_10137_10060_10155_10062_437_10154_10056_10055_10054_10059_303_5340014_100031_10099_10103_5320014_10102_10096_5350014_10052_10053_10142_10107_10050_10051_10084_10083_10080_10082_10081_10177_10110_519_10111_10112_10113_10114_10182_10078_10079_10073_5260013_10123_10189_142,searchweb201603_9,ppcSwitch_3&btsid=2a3c4872-e8af-469a-8bbd-1b7aaee6d7d0&algo_expid=2c7005ad-6719-41e4-9b1c-804f5dad5e00-0&algo_pvid=2c7005ad-6719-41e4-9b1c-804f5dad5e00
