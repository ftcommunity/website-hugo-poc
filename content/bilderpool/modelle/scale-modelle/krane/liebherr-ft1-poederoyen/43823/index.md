---
layout: "image"
title: "Liebherr-FT-1-Poederoyen-NL"
date: "2016-07-01T21:07:15"
picture: "liebherrftpoederoyen12.jpg"
weight: "12"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/43823
- /detailsec22-2.html
imported:
- "2019"
_4images_image_id: "43823"
_4images_cat_id: "3246"
_4images_user_id: "22"
_4images_image_date: "2016-07-01T21:07:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43823 -->
Vorne....

Drehkrans:  SKF Naaldset AXK 7095   +  SKF Taatsring AS 7095

Neue Makerbeam -XL :  

https://www.ftcommunity.de/categories.php?cat_id=3277

Deze past nog beter bij de ft-Bausteine; zowel qua passing als uiterlijk ! 

Deze zal eind oktober worden aangeboden in de lengtes van 50mm/100mm/150mm/200mm/300mm/ 360mm(kossel)/500mm/750mm/750mm(kossel)/1000mm en 2000mm. 
In eerste instantie alleen in Black, Clear waarschijnlijk in later stadium
