---
layout: "image"
title: "Liebherr-FT-1-Poederoyen-NL"
date: "2016-07-01T21:07:15"
picture: "liebherrftpoederoyen04.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/43815
- /details91ca.html
imported:
- "2019"
_4images_image_id: "43815"
_4images_cat_id: "3246"
_4images_user_id: "22"
_4images_image_date: "2016-07-01T21:07:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43815 -->
Antrieb Tracks -in -out mit Drehzylinder + micromotoren 100 rpm
https://www.pololu.com/category/60/micro-metal-gearmotors 

oder billiger bei : 
http://nl.aliexpress.com/item/N20-DC12V-100RPM-Gear-Motor-High-Torque-Electric-Gear-Box-Motor/32524083373.html?spm=2114.13010608.0.65.NMlkHy 


