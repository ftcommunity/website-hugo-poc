---
layout: "image"
title: "Führerhäuschen"
date: "2011-07-14T10:50:29"
picture: "grovegtk042.jpg"
weight: "41"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31140
- /details607f-2.html
imported:
- "2019"
_4images_image_id: "31140"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31140 -->
