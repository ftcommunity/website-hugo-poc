---
layout: "image"
title: "Oberwagen"
date: "2011-07-14T10:50:29"
picture: "grovegtk062.jpg"
weight: "61"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31160
- /details9cc3-2.html
imported:
- "2019"
_4images_image_id: "31160"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31160 -->
Aus dem Ausleger guckt am ende ein Powermotor haraus. Er bewegt das erste Teleskop des Auslegers.