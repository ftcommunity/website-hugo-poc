---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk031.jpg"
weight: "30"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31129
- /details5ee7.html
imported:
- "2019"
_4images_image_id: "31129"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31129 -->
Der Mast hat die Schrägstellung erreicht.