---
layout: "image"
title: "Hauptmasthänger"
date: "2011-07-14T10:50:29"
picture: "grovegtk023.jpg"
weight: "23"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31121
- /detailsd8b9-2.html
imported:
- "2019"
_4images_image_id: "31121"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31121 -->
Hier kann man die entgegengesetzt eingelenkten Räder am Hauptmasthänger sehen.