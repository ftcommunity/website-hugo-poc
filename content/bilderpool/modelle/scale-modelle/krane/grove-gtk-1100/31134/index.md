---
layout: "image"
title: "Hänger 1"
date: "2011-07-14T10:50:29"
picture: "grovegtk036.jpg"
weight: "35"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31134
- /detailsa6c1.html
imported:
- "2019"
_4images_image_id: "31134"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31134 -->
Auf dem Hänger liegt auch noch das Führerhäuschen was später am Oberwagen angebracht wird. Das Führerhäuschen ist mien Idee, da der orginialkran nur per Fernbedienung bewegt wird. Aber so ist auch noch ein bisschen "Spielspaß" dabei. ;-)