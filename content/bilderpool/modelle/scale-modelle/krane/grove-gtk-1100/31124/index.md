---
layout: "image"
title: "Lenkung"
date: "2011-07-14T10:50:29"
picture: "grovegtk026.jpg"
weight: "25"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31124
- /details74ef-3.html
imported:
- "2019"
_4images_image_id: "31124"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31124 -->
Die Streben an der rechten Fahrzeugseite bewirken den entgegengesetzten Lenkeinschlag der Räder.