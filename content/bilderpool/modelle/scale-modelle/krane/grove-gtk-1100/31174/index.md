---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk076.jpg"
weight: "75"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31174
- /details21c7.html
imported:
- "2019"
_4images_image_id: "31174"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31174 -->
Der Oberwagen ist auf dem Mast fixiert.