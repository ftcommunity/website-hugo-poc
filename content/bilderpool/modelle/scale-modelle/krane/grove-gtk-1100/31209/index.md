---
layout: "image"
title: "Grove GTK 1100 aus Fischertechnik"
date: "2011-07-14T10:50:29"
picture: "grovegtk111.jpg"
weight: "110"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31209
- /detailsecc4.html
imported:
- "2019"
_4images_image_id: "31209"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "111"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31209 -->
