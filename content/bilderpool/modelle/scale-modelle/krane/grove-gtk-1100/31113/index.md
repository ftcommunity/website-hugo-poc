---
layout: "image"
title: "grovegtk015.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk015.jpg"
weight: "15"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31113
- /details7c06.html
imported:
- "2019"
_4images_image_id: "31113"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31113 -->
