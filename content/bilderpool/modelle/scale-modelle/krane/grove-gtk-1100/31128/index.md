---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk030.jpg"
weight: "29"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31128
- /detailsef11-2.html
imported:
- "2019"
_4images_image_id: "31128"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31128 -->
