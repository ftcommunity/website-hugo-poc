---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk033.jpg"
weight: "32"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31131
- /details5334.html
imported:
- "2019"
_4images_image_id: "31131"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31131 -->
Der Hauptmast hat seine Einsatzstellung erreicht.