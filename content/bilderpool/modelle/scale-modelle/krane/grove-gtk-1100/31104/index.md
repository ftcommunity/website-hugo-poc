---
layout: "image"
title: "grovegtk006.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk006.jpg"
weight: "6"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31104
- /details166b.html
imported:
- "2019"
_4images_image_id: "31104"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31104 -->
