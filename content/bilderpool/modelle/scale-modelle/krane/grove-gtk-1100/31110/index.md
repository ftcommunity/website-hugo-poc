---
layout: "image"
title: "grovegtk012.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk012.jpg"
weight: "12"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31110
- /details70b1.html
imported:
- "2019"
_4images_image_id: "31110"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31110 -->
