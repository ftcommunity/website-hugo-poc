---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk085.jpg"
weight: "84"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31183
- /details5274.html
imported:
- "2019"
_4images_image_id: "31183"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31183 -->
Der Ausleger wurde angehoben. Wie beim Original baut der Kran sich nun selbstständig weiter auf.