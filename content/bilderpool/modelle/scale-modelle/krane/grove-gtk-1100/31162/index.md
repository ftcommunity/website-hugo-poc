---
layout: "image"
title: "Ausleger, spitze"
date: "2011-07-14T10:50:29"
picture: "grovegtk064.jpg"
weight: "63"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31162
- /detailsd7bc-2.html
imported:
- "2019"
_4images_image_id: "31162"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31162 -->
