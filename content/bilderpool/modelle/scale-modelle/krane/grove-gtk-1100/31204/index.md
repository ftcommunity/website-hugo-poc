---
layout: "image"
title: "Gesammtbild"
date: "2011-07-14T10:50:29"
picture: "grovegtk106.jpg"
weight: "105"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31204
- /details93f6.html
imported:
- "2019"
_4images_image_id: "31204"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31204 -->
Nun muss nur noch der Ausleger teleskopiert werden, dafür kann er leider nicht besonders steil stehen, weil sonst die Zimmerdecke im Weg ist.