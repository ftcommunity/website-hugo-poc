---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk095.jpg"
weight: "94"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31193
- /detailsd03c-3.html
imported:
- "2019"
_4images_image_id: "31193"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31193 -->
