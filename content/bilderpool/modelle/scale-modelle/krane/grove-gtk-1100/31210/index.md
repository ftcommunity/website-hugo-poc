---
layout: "image"
title: "Oberwagen in der Höhe"
date: "2011-07-14T10:50:29"
picture: "grovegtk112.jpg"
weight: "111"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31210
- /detailsc2d1.html
imported:
- "2019"
_4images_image_id: "31210"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "112"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31210 -->
