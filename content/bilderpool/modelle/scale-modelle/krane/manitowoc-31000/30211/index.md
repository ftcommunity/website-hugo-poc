---
layout: "image"
title: "manitowoc2"
date: "2011-03-06T18:12:43"
picture: "man2.jpg"
weight: "9"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30211
- /detailsd25e.html
imported:
- "2019"
_4images_image_id: "30211"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-03-06T18:12:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30211 -->
