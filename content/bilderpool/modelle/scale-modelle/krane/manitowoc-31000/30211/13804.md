---
layout: "comment"
hidden: true
title: "13804"
date: "2011-03-06T19:27:13"
uploadBy:
- "robvanbaal"
license: "unknown"
imported:
- "2019"
---
Het lijkt op het eerste oog een klein model... maar dat zal in de werkelijkheid wel weer meevallen. In ieder geval gefeliciteerd met het resultaat. Het is weer typisch Jansen-gedetailleerd!