---
layout: "image"
title: "manitowoc1"
date: "2011-03-06T18:12:43"
picture: "man1.jpg"
weight: "8"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30210
- /details2491.html
imported:
- "2019"
_4images_image_id: "30210"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-03-06T18:12:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30210 -->
