---
layout: "image"
title: "manitowoc 31000"
date: "2011-02-27T13:24:21"
picture: "manitowoc4.jpg"
weight: "4"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30140
- /detailsf665.html
imported:
- "2019"
_4images_image_id: "30140"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-02-27T13:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30140 -->
