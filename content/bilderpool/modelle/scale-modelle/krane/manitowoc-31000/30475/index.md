---
layout: "image"
title: "manitow 31000-6"
date: "2011-04-25T20:23:38"
picture: "man6_2.jpg"
weight: "20"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30475
- /detailse40f-2.html
imported:
- "2019"
_4images_image_id: "30475"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-04-25T20:23:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30475 -->
