---
layout: "image"
title: "manitowoc 31000-13"
date: "2011-04-25T20:23:38"
picture: "man13.jpg"
weight: "27"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/30482
- /detailse916.html
imported:
- "2019"
_4images_image_id: "30482"
_4images_cat_id: "2229"
_4images_user_id: "541"
_4images_image_date: "2011-04-25T20:23:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30482 -->
