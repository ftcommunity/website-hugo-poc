---
layout: "image"
title: "kroll k 10000 maßstab 1:50 long jib"
date: "2004-04-25T20:47:03"
picture: "K_10000_010.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/2390
- /details6e2c-2.html
imported:
- "2019"
_4images_image_id: "2390"
_4images_cat_id: "222"
_4images_user_id: "14"
_4images_image_date: "2004-04-25T20:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2390 -->
2m hackenhöhe 3,30m bis zur spitze, von vorne bis hinten 3,45m,  0,4m spurweite des unterwagens