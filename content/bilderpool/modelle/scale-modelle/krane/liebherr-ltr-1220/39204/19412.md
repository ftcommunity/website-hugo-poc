---
layout: "comment"
hidden: true
title: "19412"
date: "2014-08-14T21:16:41"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Hallo Peter, 
Ik vermoed dat deze constructie enkel symmetrisch wordt in zijn uiterste standen (volledig in- en uitgeschoven). Een van de delen schuift tot deze niet meer verder kan, waarna het andere deel zich hiertegen afzet, totdat ook het tweede deel zijn uiterste stand bereikt heeft.  
Dirks oplossing is natuurlijk weer van grootse schoonheid maar daarmee ook ingewikkelder.

Hallo Peter, 

Ich vermüte dass diese Konstruktion nur in die Endlagen symmetrisch ist. Ein der Teilen schiebt bis zur Anschlag, danach drückt das andere Teil sich gegen das erste Teil ab, bis zur Anschlag.

Dirks Lösung ist naturlich wieder grosse Klasse, aber auch umständiger. 

Mfrgr Marten