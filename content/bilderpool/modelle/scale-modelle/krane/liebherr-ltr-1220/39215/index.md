---
layout: "image"
title: "Liertrommel"
date: "2014-08-09T22:30:57"
picture: "P8080050.jpg"
weight: "16"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39215
- /detailsf877-2.html
imported:
- "2019"
_4images_image_id: "39215"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39215 -->
Touw moet ook een plekje hebben