---
layout: "image"
title: "Aandrijving telescoopmast"
date: "2014-08-14T20:14:12"
picture: "P6220002.jpg"
weight: "19"
konstrukteure: 
- "chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39241
- /detailsa7e1-2.html
imported:
- "2019"
_4images_image_id: "39241"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-14T20:14:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39241 -->
Beide lagerblokken. Zijn wel nodig omdat het een nogal gesloten mast is. Wanneer er tijdens het inschuiven wat klemt trekt hij zichzelf stuk. Maar na wat aanpassingen is dat ook klaar/ opgelost