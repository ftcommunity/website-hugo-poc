---
layout: "overview"
title: "Mobiler Schwerlastkran"
date: 2020-02-22T08:23:57+01:00
legacy_id:
- /php/categories/3411
- /categoriesb63b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3411 --> 
Vierachsiger mobiler Schwerlastkran, voll ferngesteuert, mit Stützen und Teleskop-Schwenkarm.