---
layout: "image"
title: "Draufsicht Stütze eingefahren"
date: "2017-06-11T21:52:18"
picture: "20170531_104344.jpg"
weight: "2"
konstrukteure: 
- "The Rob"
fotografen:
- "The Rob"
uploadBy: "The Rob"
license: "unknown"
legacy_id:
- /php/details/45932
- /details33c8.html
imported:
- "2019"
_4images_image_id: "45932"
_4images_cat_id: "3411"
_4images_user_id: "2745"
_4images_image_date: "2017-06-11T21:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45932 -->
Über die Zahnstangen werden die Stützen synchron seitlich ausgefahren.