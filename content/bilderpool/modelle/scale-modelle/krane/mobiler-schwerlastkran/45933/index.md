---
layout: "image"
title: "Draufsicht Stützen ausgefahren"
date: "2017-06-11T21:52:18"
picture: "20170531_104403_edit.jpg"
weight: "3"
konstrukteure: 
- "The Rob"
fotografen:
- "The Rob"
uploadBy: "The Rob"
license: "unknown"
legacy_id:
- /php/details/45933
- /details3406.html
imported:
- "2019"
_4images_image_id: "45933"
_4images_cat_id: "3411"
_4images_user_id: "2745"
_4images_image_date: "2017-06-11T21:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45933 -->
So sind die Stützen maximal ausgefahren.
Video zum Ausfahren per Hand: https://www.youtube.com/watch?v=NoThs1lorkw&feature=youtu.be&pxtry=2
Man kann recht gut erkennen, dass ich die Z10 teilweise bearbeitet habe, damit überall Platz ist und die Zahnräder schön in die Endposition gleiten. Bessere Lösung mit 3D-Druck ist in Planung.