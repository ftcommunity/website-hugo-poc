---
layout: "image"
title: "Ballastwagen ISO 3"
date: "2004-11-23T21:59:56"
picture: "Ballastwagen_von_oben_1.jpg"
weight: "34"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3336
- /details0706-2.html
imported:
- "2019"
_4images_image_id: "3336"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T21:59:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3336 -->
