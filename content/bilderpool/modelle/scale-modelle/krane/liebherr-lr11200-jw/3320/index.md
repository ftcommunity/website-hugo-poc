---
layout: "image"
title: "Vorderansicht Drehgestell 1"
date: "2004-11-23T19:48:21"
picture: "Vorderansicht_LR11200_1.jpg"
weight: "18"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/3320
- /detailse642-2.html
imported:
- "2019"
_4images_image_id: "3320"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3320 -->
