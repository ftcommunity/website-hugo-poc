---
layout: "image"
title: "Gesamtansicht 2"
date: "2004-06-06T10:40:03"
picture: "LR11200_Gesamtansicht_2_Ostern_03.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/2483
- /details4a0c.html
imported:
- "2019"
_4images_image_id: "2483"
_4images_cat_id: "230"
_4images_user_id: "1"
_4images_image_date: "2004-06-06T10:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2483 -->
