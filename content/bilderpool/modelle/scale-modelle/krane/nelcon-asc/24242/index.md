---
layout: "image"
title: "asc_17"
date: "2009-06-07T14:43:34"
picture: "nelconasc20.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24242
- /details52f4.html
imported:
- "2019"
_4images_image_id: "24242"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24242 -->
Diesen konstruktion, zur heben und Spannungsversorgung der Spreader (und Container), hat mir vielen kopfschmerzen besorgt.
20:1 Power-motor auf achse eines alten Differential #31043.
Auf die andere achse is ein Klemzahnrad Z15 die ein weiteren Z15, die auf ein Differential Antriebrad Z14 #31414
geklemmt ist, ein Rastdifferential antreibt. An beide ausgehenden achsen des Differentials sind Seiltrommel für die stromdrahten.
Am Deffirentialkäfig des #31043 werden zwei sets von je 2 Seiltrommel, über Z15 und 2x Z10 angetrieben.
