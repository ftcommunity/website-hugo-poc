---
layout: "image"
title: "asc_1"
date: "2009-06-07T14:43:33"
picture: "nelconasc04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24227
- /detailscf2c-2.html
imported:
- "2019"
_4images_image_id: "24227"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24227 -->
Das Modell:
Länge: 101cm
Breite: 64cm
Höhe: 67cm