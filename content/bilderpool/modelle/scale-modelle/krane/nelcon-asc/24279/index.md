---
layout: "image"
title: "asc_containers_1"
date: "2009-06-07T14:43:36"
picture: "nelconasc57.jpg"
weight: "56"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24279
- /detailsef61.html
imported:
- "2019"
_4images_image_id: "24279"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24279 -->
Container:
45 Fus
40 Fus
20 Fus