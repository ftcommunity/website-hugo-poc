---
layout: "comment"
hidden: true
title: "9401"
date: "2009-06-12T10:26:38"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Halten die Container etwa nur durch das Drehen der Rastachsen auf eine andere Position im Statikloch des Containers?