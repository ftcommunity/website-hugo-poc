---
layout: "image"
title: "asc_spreader V2_13"
date: "2009-06-07T14:43:36"
picture: "nelconasc47.jpg"
weight: "46"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24269
- /details2b4e-2.html
imported:
- "2019"
_4images_image_id: "24269"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24269 -->
Spreader in der 40 Fus position.