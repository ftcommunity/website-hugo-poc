---
layout: "image"
title: "asc_20"
date: "2009-06-07T14:43:34"
picture: "nelconasc23.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24245
- /detailsece2.html
imported:
- "2019"
_4images_image_id: "24245"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24245 -->
Schleifringen, Das Rezept:
Man neme:
Kunststoff rohr 3x2  20mm,
Kunststoff rohr 4x3  2x 4mm 
Messing rohr 4x3mm  2x 6mm
2 x Klemmkontakt #31338
Seiltrommel #35070

Bohre das kleine loch (nicht zu tief, sonst bohrt man der "Rast" weg) von den Seiltrommel #35070 auf 4mm.
Bohre ein loch in den Seiltrommel so das das loch groß und tief genug ist um das draht durch das gerade auf 4mmm aufgebohrten loch heraus zu füren.
Mach an einen ende des 20mm langen rohr einen schlits von 6mm lang und an das anderen ende einen schlits von 10mm lang.
Schiebe über das zu verwendende draht erst ein 4mm Kunststoff rohr, dan ein 6mm Messing rohr, das zweite stück 4mm rohr schieb man über das 20mm rohr und dan über das draht.
Löte eines der beide Adern an das zweite stück Messing rohr und schieb das über das 20mm rohr.
Der zweite Ader Lötet man an das erste stück Messing rohr und schieb das auf das 20mm rohr.
Als letstes klebt man das erste stück Kunststoff rohr auf das 20mm rohr.
Das ganse steckt man dan in das loch des Seiltrommels.