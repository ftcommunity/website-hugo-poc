---
layout: "image"
title: "asc_4"
date: "2009-06-07T14:43:33"
picture: "nelconasc07.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24229
- /details74be.html
imported:
- "2019"
_4images_image_id: "24229"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24229 -->
Der Fahrantrieb besteht aus 4 x 8:1 Power-motoren.