---
layout: "image"
title: "asc_11"
date: "2009-06-07T14:43:34"
picture: "nelconasc14.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24236
- /detailsb2ff.html
imported:
- "2019"
_4images_image_id: "24236"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24236 -->
Ein 14 Aderiges kabel werd von Reedkontakt-halter #35969 an das ALU-profil geleitet.
Innerhalb der kranbalken werd diesen kabel mittels einen 15pin Sub D connector angeschlossen.