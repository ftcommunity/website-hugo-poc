---
layout: "image"
title: "NELCON ASC_1 (orginal)"
date: "2009-06-07T14:43:33"
picture: "nelconasc01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24224
- /detailsa275.html
imported:
- "2019"
_4images_image_id: "24224"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24224 -->
Diese foto hab ich ein gescand aus einen Werbeprospect der Firma Nelcon.