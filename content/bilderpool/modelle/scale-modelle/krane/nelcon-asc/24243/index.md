---
layout: "image"
title: "asc_18"
date: "2009-06-07T14:43:34"
picture: "nelconasc21.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24243
- /detailsfb42.html
imported:
- "2019"
_4images_image_id: "24243"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24243 -->
Um beim heben der Spreader horizontal zu halten hab ich die Seiltrommel halbschalen #32973 verwendet.
Auf jeder Seiltrommel kan zomit etwas über 1m draht gewikelt werden.