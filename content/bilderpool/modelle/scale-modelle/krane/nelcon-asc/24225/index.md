---
layout: "image"
title: "NELCON ASC_2 (original)"
date: "2009-06-07T14:43:33"
picture: "nelconasc02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24225
- /details5ece.html
imported:
- "2019"
_4images_image_id: "24225"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24225 -->
Diese foto hab ich ein gescand aus einen Werbeprospect der Firma Nelcon.