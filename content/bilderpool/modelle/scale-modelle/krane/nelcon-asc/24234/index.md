---
layout: "image"
title: "asc_9"
date: "2009-06-07T14:43:34"
picture: "nelconasc12.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/24234
- /detailsdf69.html
imported:
- "2019"
_4images_image_id: "24234"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24234 -->
Spannungsverzorgung mittel 9,6v Accu.