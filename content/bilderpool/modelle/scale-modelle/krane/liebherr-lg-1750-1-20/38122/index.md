---
layout: "image"
title: "bsic truck"
date: "2014-01-24T01:01:20"
picture: "LG10.jpg"
weight: "6"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Anton Jansen"
uploadBy: "anton"
license: "unknown"
legacy_id:
- /php/details/38122
- /details8b51.html
imported:
- "2019"
_4images_image_id: "38122"
_4images_cat_id: "2836"
_4images_user_id: "541"
_4images_image_date: "2014-01-24T01:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38122 -->
