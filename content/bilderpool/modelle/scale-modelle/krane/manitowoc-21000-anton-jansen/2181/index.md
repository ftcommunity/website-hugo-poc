---
layout: "image"
title: "Mani01.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2181
- /details69b7.html
imported:
- "2019"
_4images_image_id: "2181"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2181 -->
Veghel 2004:
Manitowac 21000 ist ein 'Mordstrum' von Kran. Den auch noch nachzubauen, ist eine nicht minder bärenstarke Leistung.
