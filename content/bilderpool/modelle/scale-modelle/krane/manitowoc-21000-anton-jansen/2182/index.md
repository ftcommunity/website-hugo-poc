---
layout: "image"
title: "Mani02.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani02.jpg"
weight: "2"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2182
- /details9d2e.html
imported:
- "2019"
_4images_image_id: "2182"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2182 -->
Das Radfahrgestell hinten war auf dem Prospektbild nicht drauf, aber soviel dichterische Freiheit ist wohl erlaubt.
