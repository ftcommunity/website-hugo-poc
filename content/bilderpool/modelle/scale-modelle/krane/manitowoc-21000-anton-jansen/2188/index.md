---
layout: "image"
title: "Mani07.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani07.jpg"
weight: "7"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2188
- /detailsa1c7.html
imported:
- "2019"
_4images_image_id: "2188"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2188 -->
Die 5 Powermots im Bild machen nicht mal die Hälfte der Powermots aus.
