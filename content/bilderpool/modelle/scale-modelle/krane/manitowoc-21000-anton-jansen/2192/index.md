---
layout: "image"
title: "Mani12.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani12.jpg"
weight: "11"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2192
- /detailsf342.html
imported:
- "2019"
_4images_image_id: "2192"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2192 -->
