---
layout: "image"
title: "Mani06.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Monsterreifen", "Großreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2187
- /detailsbe09.html
imported:
- "2019"
_4images_image_id: "2187"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2187 -->
Die Räder sind noch zwei Nummern größer als die Monsterreifen von Conrad. Hier sind sie in der Position zum Herumschwenken des Krans zu sehen.
