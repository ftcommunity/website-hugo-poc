---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250013.jpg"
weight: "12"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/41476
- /details7620-2.html
imported:
- "2019"
_4images_image_id: "41476"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41476 -->
Contra gewicht is ongeveer 7 a 8 kg met maar een afstand van 20 cm naar het hoofdschanier. Dus de krachten zijn best groot op dat korte armpje.