---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250019.jpg"
weight: "16"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/41480
- /details963c.html
imported:
- "2019"
_4images_image_id: "41480"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41480 -->
De hoofdstaander. Zitten 4 Alu's in en ik heb hem zo klein mogelijk rond gekregen om de Alu's heen.