---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250001.jpg"
weight: "1"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/41465
- /details4549.html
imported:
- "2019"
_4images_image_id: "41465"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41465 -->
Weegt maar 14.1kg ;)