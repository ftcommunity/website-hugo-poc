---
layout: "image"
title: "Portalkatze 6"
date: "2006-04-29T18:58:51"
picture: "CTA_-_Portalkatze_5.jpg"
weight: "18"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6184
- /details0ec2-2.html
imported:
- "2019"
_4images_image_id: "6184"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6184 -->
