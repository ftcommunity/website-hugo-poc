---
layout: "image"
title: "Laschplattform 5"
date: "2006-04-29T18:58:43"
picture: "CTA_-_Laschplattform_5.jpg"
weight: "15"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6181
- /details3ef7-2.html
imported:
- "2019"
_4images_image_id: "6181"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6181 -->
