---
layout: "image"
title: "CTA Containerbrücke im Maßstab 1:50 (Ostern 06)"
date: "2006-04-28T11:27:59"
picture: "CTA_7.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6161
- /details39f9-2.html
imported:
- "2019"
_4images_image_id: "6161"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-28T11:27:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6161 -->
Nach rund ein Jahr Bauzeit und rund 1 1/2 Jahren Recherche im Internet ist es mir doch gelungen dieses Modell zu bauen !!! Basis war vor allem das Patent der HHLA bzgl. der Laschplattform. Ohne dieses Patent und der enthaltenen Skizzen hätte ich das Projekt, mangels Unterstützung, nicht durchführen können.
Das Modell ist zur Modelshow Europe 06 im Bemmel (NL) nicht ganz fertig geworden. Es fehlen Geländer, Details wie den Laschkorb zum entriegeln der Container beim entladen, ein Schiffsrumpf, mehr Container und vorallem eine bessere Software (LLWin reicht nicht aus)
