---
layout: "image"
title: "Ausleger vorne seite"
date: "2007-04-21T01:15:43"
picture: "tadanogre22.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/10123
- /details9cd1.html
imported:
- "2019"
_4images_image_id: "10123"
_4images_cat_id: "912"
_4images_user_id: "162"
_4images_image_date: "2007-04-21T01:15:43"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10123 -->
Hier seht mann die Zapfen von die Muttern der in die Grundplatte von der 3. Stufe geklemmt werden.