---
layout: "image"
title: "Vollig ausgefahren"
date: "2007-04-21T01:15:29"
picture: "tadanogre08.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/10109
- /detailsb321-2.html
imported:
- "2019"
_4images_image_id: "10109"
_4images_cat_id: "912"
_4images_user_id: "162"
_4images_image_date: "2007-04-21T01:15:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10109 -->
Insgesammt 1,50 meter lang ist der Ausleger geworden