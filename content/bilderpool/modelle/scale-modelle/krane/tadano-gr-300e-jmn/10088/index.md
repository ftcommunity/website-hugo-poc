---
layout: "image"
title: "Tadano GR-300E"
date: "2007-04-14T07:58:50"
picture: "tadanogre1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/10088
- /detailsb2bc.html
imported:
- "2019"
_4images_image_id: "10088"
_4images_cat_id: "912"
_4images_user_id: "162"
_4images_image_date: "2007-04-14T07:58:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10088 -->
Erstes Bild mein neues Projekt. Dies ist das erste Bild. Jetzt ist es schon wieder verbessert. Auch warte ich noch auf einige Teile damit ich es fertig machen kann. Der Ausleger hat 3 Stufen. Der Ausleger wird angetrieben durch ein Conrad Igarashi motor 1:125. Das heben des Auslegers auch.
Antrieb von die Reifen ist mit 2 1:50 Igarashi Motoren. Die Reifen sind 4x4 angetrieben und konnen alle lenken. Lenkung wird mit ein Mini mot angetrieben.
Drehkranz wird mit einen Powermot 1:50 angetrieben.
Die Stutzen werden durch 2 M-motoren angetrieben. 1 vorne und 1 hinter.

Wann es ganz fertig ist, werde ich mehrere detail Bilder reinstellen. Noch ein wenig gedult.
