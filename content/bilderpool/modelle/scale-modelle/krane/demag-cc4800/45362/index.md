---
layout: "image"
title: "DEMAG CC4800_24"
date: "2017-03-01T15:57:19"
picture: "demagcc24.jpg"
weight: "24"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45362
- /detailsf7e4.html
imported:
- "2019"
_4images_image_id: "45362"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45362 -->
Die beide Abstützarmen 1 und 2 sind entfernt.
Beim Vorbild werden die Abstützarmen an das zwischeteil angeboltzt. Das wahr bei mein Modell aber nicht gans möglich.
Bei mein Modell stecken die Abstützarmen durch das zwischenteil und teilweise auch durch der gegenüber liegende Abstützarm.