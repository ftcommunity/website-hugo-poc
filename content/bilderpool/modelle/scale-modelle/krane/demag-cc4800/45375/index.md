---
layout: "image"
title: "DEMAG CC4800_37"
date: "2017-03-01T15:57:19"
picture: "demagcc37.jpg"
weight: "37"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45375
- /details3e9a.html
imported:
- "2019"
_4images_image_id: "45375"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45375 -->
Erst wen die Raupe gespannen ist Schaltet der Mini-taster der Strom zu die beide Fahrmotoren frei.