---
layout: "image"
title: "DEMAG CC4800_42"
date: "2017-03-01T15:57:19"
picture: "demagcc42.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45380
- /detailsb09f-2.html
imported:
- "2019"
_4images_image_id: "45380"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45380 -->
Antrieb der 4 innere Radsatzen: Faulhaber 2619S012SR mit 112:1 Untersetsung.
Die 111x31mm Reifen kommen von Metallus, sind aber "NML"