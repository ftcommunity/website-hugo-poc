---
layout: "image"
title: "DEMAG CC4800_32"
date: "2017-03-01T15:57:19"
picture: "demagcc32.jpg"
weight: "32"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45370
- /detailsc9c1-2.html
imported:
- "2019"
_4images_image_id: "45370"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45370 -->
Linke Taster geschlossen: Raupe ist gespant.