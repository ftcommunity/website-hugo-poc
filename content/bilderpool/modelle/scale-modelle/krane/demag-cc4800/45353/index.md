---
layout: "image"
title: "DEMAG CC4800_15"
date: "2017-03-01T15:57:19"
picture: "demagcc15.jpg"
weight: "15"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45353
- /detailscad9.html
imported:
- "2019"
_4images_image_id: "45353"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45353 -->
Das obere ende der A-bock: die 2 Rote Lochplatten und die 4 Schwartze Lochplatten zwischen die Seilscheiben, habe ich bei Metallus gekauft.