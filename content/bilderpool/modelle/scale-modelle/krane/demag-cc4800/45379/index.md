---
layout: "image"
title: "DEMAG CC4800_41"
date: "2017-03-01T15:57:19"
picture: "demagcc41.jpg"
weight: "41"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45379
- /details39d7.html
imported:
- "2019"
_4images_image_id: "45379"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45379 -->
Sicht auf das Frame und einer der angetriebene Radsatzen.