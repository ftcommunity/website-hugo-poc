---
layout: "image"
title: "DEMAG CC4800_28"
date: "2017-03-01T15:57:19"
picture: "demagcc28.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45366
- /details2401.html
imported:
- "2019"
_4images_image_id: "45366"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45366 -->
Abstützplatte mit offenen Abstützarm.