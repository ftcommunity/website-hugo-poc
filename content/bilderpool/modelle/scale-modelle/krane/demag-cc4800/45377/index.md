---
layout: "image"
title: "DEMAG CC4800_39"
date: "2017-03-01T15:57:19"
picture: "demagcc39.jpg"
weight: "39"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45377
- /detailse071.html
imported:
- "2019"
_4images_image_id: "45377"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45377 -->
Ballastwagen von die Rechte Seite. Zwischen die Räder ist einer der beide Abstützplatten zu sehen die von ein mini-motor angetrieben werd.