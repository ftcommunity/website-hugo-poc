---
layout: "image"
title: "DEMAG CC4800_12"
date: "2017-03-01T15:57:19"
picture: "demagcc12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45350
- /detailsd0c5-2.html
imported:
- "2019"
_4images_image_id: "45350"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45350 -->
Untere Hakenflasche: 2x 7 Seilscheiben 45,5mm.