---
layout: "image"
title: "DEMAG CC4800_35"
date: "2017-03-01T15:57:19"
picture: "demagcc35.jpg"
weight: "35"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45373
- /detailsaf32.html
imported:
- "2019"
_4images_image_id: "45373"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45373 -->
Raupe gespannen.