---
layout: "overview"
title: "Demag CC4800"
date: 2020-02-22T08:23:52+01:00
legacy_id:
- /php/categories/3377
- /categories0ea9.html
- /categoriesd3b9.html
- /categories3950.html
- /categoriesae87.html
- /categories7791.html
- /categories204f.html
- /categories658d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3377 --> 
In 1993 bin ich der Demag CC4800 zum ersten mal begegnet.
Da ich damals noch nicht zo viele Teilen hatte, wahr es noch nicht möglich diesen Kran zu bauen.
Nachdem ich der Demag CC12000 wieder zerlegt hatte konte ich erst der Kran in der Twinring ausfürung in Maßstab 1:45 aufbauen.
Seit ende 2003 weis ich wie ich 90mm breite Raupen bauen kan. Aber erst in 2010 hab ich angefangen mit der bau.
So groß wie möglich, was bedeutete das der Maßstab auf 1:16 kam. Und mit ein RC anlage Ferngesteuert.

Die Daten:
Maßstab: 1:16 mit 16 Kanal RC fernsteuerung
Raupen Länge: 880mm
Raupen Breite: 90mm
Raupen Höhe: 140mm
Breite über Raupen: 750mm
Oberbau Länge: 845mm
Oberbau Breite: 245mm
Oberbau Höhe: 307mm
A-Bock Länge: 875mm
Ausleger Länge: 3450mm
Ausleger Breite: 240mm
Ausleger Höhe: 186mm
Dereckausleger Länge: 2650mm
Dereckausleger Breite: 195mm
Dereckausleger Höhe: 150mm
Ballastwagen Länge: 360mm
Ballastwagen Breite: 630mm
Mitte Ballastwagen zu Mitte Kran: 1395mm
Footprint: 2015 x 750mm