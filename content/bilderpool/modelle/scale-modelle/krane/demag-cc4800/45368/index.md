---
layout: "image"
title: "DEMAG CC4800_30"
date: "2017-03-01T15:57:19"
picture: "demagcc30.jpg"
weight: "30"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45368
- /details8745.html
imported:
- "2019"
_4images_image_id: "45368"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45368 -->
Ein halbierter Verbinder 15 verhindert mittels ein in der Drahtstange eingefrästen Schlitzt das drehen.