---
layout: "image"
title: "DEMAG CC4800_36"
date: "2017-03-01T15:57:19"
picture: "demagcc36.jpg"
weight: "36"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45374
- /details7e55-2.html
imported:
- "2019"
_4images_image_id: "45374"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45374 -->
Antriebrad und Raupe.
Wie auf der Aufkleber zu lezen ist, werden die Raupen von je 2x 781:1 Getriebemotoren angetrieben, Zusätzlich werd mit hilfe von ein Zahnrad  z10 und der Innenverzahnung z30, noch ein Untersetsung von 3:1 erreicht.