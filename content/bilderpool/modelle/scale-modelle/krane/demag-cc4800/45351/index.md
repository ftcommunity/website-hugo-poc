---
layout: "image"
title: "DEMAG CC4800_13"
date: "2017-03-01T15:57:19"
picture: "demagcc13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45351
- /detailsfdb1.html
imported:
- "2019"
_4images_image_id: "45351"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45351 -->
Untere Hakenflasche von der Seite.