---
layout: "image"
title: "DEMAG CC4800_8"
date: "2017-03-01T15:57:19"
picture: "demagcc08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45346
- /details649a.html
imported:
- "2019"
_4images_image_id: "45346"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45346 -->
Rückfalsicherung: Kardan-klaue, Achsen 110 und 80, Klemmhülze, drei Feder und ABS Röhren. Drei Metalscheiben sorgen dafür das die Feder nicht in einander geraten.