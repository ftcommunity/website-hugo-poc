---
layout: "image"
title: "DEMAG CC4800_18"
date: "2017-03-01T15:57:19"
picture: "demagcc18.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45356
- /detailsc94a-2.html
imported:
- "2019"
_4images_image_id: "45356"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45356 -->
Satelitansicht auf der ganse oberwagen der Kran, durch der Aufrichtebock hindurch.