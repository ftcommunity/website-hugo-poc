---
layout: "image"
title: "DEMAG CC4800_14"
date: "2017-03-01T15:57:19"
picture: "demagcc14.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45352
- /detailsec6b-2.html
imported:
- "2019"
_4images_image_id: "45352"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45352 -->
Das obere ende der A-bock: 2x 8 Seilscheiben 45,5mm.