---
layout: "image"
title: "DEMAG CC4800_29"
date: "2017-03-01T15:57:19"
picture: "demagcc29.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45367
- /detailsad79.html
imported:
- "2019"
_4images_image_id: "45367"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45367 -->
Die Quadratisch gefrästen M10 Mutter ist fest in das Differentialgehause eingebaut. Durch drehen von das Differentialgehause werd der M10 Drahtstange verschoben.