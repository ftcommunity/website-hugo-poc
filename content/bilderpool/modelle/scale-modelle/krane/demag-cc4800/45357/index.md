---
layout: "image"
title: "DEMAG CC4800_19"
date: "2017-03-01T15:57:19"
picture: "demagcc19.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45357
- /detailsab1d-3.html
imported:
- "2019"
_4images_image_id: "45357"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45357 -->
Sicht auf der doppelten Auspuff, Treppe und Oberwagenballast.