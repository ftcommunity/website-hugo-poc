---
layout: "image"
title: "DEMAG CC4800_21"
date: "2017-03-01T15:57:19"
picture: "demagcc21.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/45359
- /details3aa1.html
imported:
- "2019"
_4images_image_id: "45359"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45359 -->
Abstützplatten auf der Boden.