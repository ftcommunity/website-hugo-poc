---
layout: "image"
title: "Steunpoten"
date: "2012-02-26T12:53:07"
picture: "terex_018.jpg"
weight: "16"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34412
- /detailsbcd4-2.html
imported:
- "2019"
_4images_image_id: "34412"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34412 -->
De steunpoten schuiven in en uit met een katrol plus kabelsysteem beide (voor en achter) aangedreven door een xs motor