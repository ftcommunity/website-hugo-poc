---
layout: "image"
title: "Giek"
date: "2012-02-26T12:53:07"
picture: "terex_011.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34405
- /detailsdee3.html
imported:
- "2019"
_4images_image_id: "34405"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34405 -->
Zicht op de powermotor voor het uitschuiven en inschuiven van de 3 delige mast