---
layout: "image"
title: "Overzicht"
date: "2012-02-26T12:53:07"
picture: "terex_016.jpg"
weight: "14"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34410
- /details4a44-2.html
imported:
- "2019"
_4images_image_id: "34410"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34410 -->
Mast weer omhoog met jib en gewicht