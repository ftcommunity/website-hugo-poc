---
layout: "image"
title: "Bovenwagen"
date: "2012-02-26T12:53:07"
picture: "terex_006.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34400
- /details4a25.html
imported:
- "2019"
_4images_image_id: "34400"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34400 -->
Bovenwagen cabine met motorkap voor de hef motor