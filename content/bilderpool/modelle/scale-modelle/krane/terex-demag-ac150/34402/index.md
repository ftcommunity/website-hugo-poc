---
layout: "image"
title: "Hefmotor"
date: "2012-02-26T12:53:07"
picture: "terex_008.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34402
- /details3522.html
imported:
- "2019"
_4images_image_id: "34402"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34402 -->
Tijdens het heffen komt de motor via de motorkap naar buiten