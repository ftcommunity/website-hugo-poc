---
layout: "image"
title: "Spindel"
date: "2012-02-26T12:53:07"
picture: "terex_023.jpg"
weight: "20"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34416
- /details6742-3.html
imported:
- "2019"
_4images_image_id: "34416"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34416 -->
Zicht op de spindel die het eerste deel van de mast uitschuift het tweede deel gaat mee door middel van touwen.