---
layout: "image"
title: "Terex Demag AC150"
date: "2012-03-12T17:26:59"
picture: "terex_029.jpg"
weight: "27"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34645
- /details1edc-2.html
imported:
- "2019"
_4images_image_id: "34645"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-03-12T17:26:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34645 -->
De mast houdt het. Ik druk met mijn hand op de achterzijde want ik ben bang dat de draaikrans afbreekt. De mast is echt sterk (zie foto) buigt bijna niet door.