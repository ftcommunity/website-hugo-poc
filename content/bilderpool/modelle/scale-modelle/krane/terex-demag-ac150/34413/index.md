---
layout: "image"
title: "Schakelaar"
date: "2012-02-26T12:53:07"
picture: "terex_019.jpg"
weight: "17"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34413
- /details96c9.html
imported:
- "2019"
_4images_image_id: "34413"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34413 -->
Schakelaar om boven wagen spanningsloos te kunnen maken. Beide IR systemen storen elkaar soms ?????????