---
layout: "image"
title: "Derrickwagen CC8800 Twin 12/12"
date: "2009-03-08T18:35:01"
picture: "cctwin12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
schlagworte: ["Monsterreifen"]
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23433
- /detailsbb3d.html
imported:
- "2019"
_4images_image_id: "23433"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:35:01"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23433 -->
Ein Detailbild der Stütze und wieder ein Größenvergleich
