---
layout: "image"
title: "CC8800 Twin 2/6"
date: "2009-04-26T19:08:26"
picture: "twin2.jpg"
weight: "29"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23815
- /detailsbd43.html
imported:
- "2019"
_4images_image_id: "23815"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23815 -->
Dieser Raupenträger passt leider nicht zum Unterwagen.
