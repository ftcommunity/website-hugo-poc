---
layout: "image"
title: "Kranhaken 3/4"
date: "2009-03-24T06:43:31"
picture: "kranhaken2.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23504
- /detailse04e-2.html
imported:
- "2019"
_4images_image_id: "23504"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23504 -->
