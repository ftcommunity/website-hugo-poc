---
layout: "image"
title: "CC8800 Twin 14/28"
date: "2009-05-04T21:14:30"
picture: "cctwin14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23850
- /details2556.html
imported:
- "2019"
_4images_image_id: "23850"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23850 -->
Der Kran nimmt langsam Formen an.
