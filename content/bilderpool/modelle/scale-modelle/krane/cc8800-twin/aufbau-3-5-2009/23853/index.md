---
layout: "image"
title: "CC8800 Twin 17/28"
date: "2009-05-04T21:14:30"
picture: "cctwin17.jpg"
weight: "17"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23853
- /details6da9.html
imported:
- "2019"
_4images_image_id: "23853"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23853 -->
