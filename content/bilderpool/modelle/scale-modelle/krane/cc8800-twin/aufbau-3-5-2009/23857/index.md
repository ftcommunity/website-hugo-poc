---
layout: "image"
title: "CC8800 Twin 21/28"
date: "2009-05-04T21:14:30"
picture: "cctwin21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23857
- /detailsd17c.html
imported:
- "2019"
_4images_image_id: "23857"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23857 -->
Aber irgendwann war auch die passive Hilfe überflüssig und der Kran bewegte sich von ganz alleine Richtung Decke.
