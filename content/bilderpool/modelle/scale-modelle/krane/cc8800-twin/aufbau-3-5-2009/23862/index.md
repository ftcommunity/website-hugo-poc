---
layout: "image"
title: "CC8800 Twin 26/28"
date: "2009-05-04T21:14:30"
picture: "cctwin26.jpg"
weight: "26"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23862
- /details5484.html
imported:
- "2019"
_4images_image_id: "23862"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23862 -->
