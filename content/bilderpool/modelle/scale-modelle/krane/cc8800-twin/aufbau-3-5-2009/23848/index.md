---
layout: "image"
title: "CC8800 Twin 12/28"
date: "2009-05-04T21:14:30"
picture: "cctwin12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23848
- /details58f3-2.html
imported:
- "2019"
_4images_image_id: "23848"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23848 -->
