---
layout: "image"
title: "CC8800 Twin 27/28"
date: "2009-05-04T21:14:30"
picture: "cctwin27.jpg"
weight: "27"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23863
- /details4c1a.html
imported:
- "2019"
_4images_image_id: "23863"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23863 -->
Und der Moment ist gekommen.
Der Wippausleger schwebt in der Luft.
Nun könnte es eigentlich ganz entspannt bis nach ganz oben gehen aber jedes Material hat seine physikalischen Grenzen und so auch Nylon... auf dem nächsten Bild ist die Auflösung zu sehen.
Und deswegen habe ich den Aufbau abgebrochen.
