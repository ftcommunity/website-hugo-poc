---
layout: "image"
title: "Derrickwagen CC8800 Twin 1/12"
date: "2009-03-08T18:34:55"
picture: "cctwin01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23422
- /details0535.html
imported:
- "2019"
_4images_image_id: "23422"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23422 -->
Gesamtansicht des neuen Derickwagen
Maße ca.1m breit und 0,40m lang 
Das Modell entsteht in Maßstab 1:22
Als Größenvergleich steht eine ft Figur vor dem Modell.
Bei dem CC8800 Twin heißt das ganze Superlift Gegengewicht
Es besteht aus einem Derrickwagen + und Schwebeballast die mit
einer Hydraulik(Orginal) verbunden sind. An allen 4 Ecken sind Stützen
angebracht um den Wagen anzuheben um danach die beiden Drehgestelle
zu drehen. An jedem Gestell sind 4 Räder montiert.
