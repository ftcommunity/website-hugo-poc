---
layout: "image"
title: "Ansicht Oberwagen 3/6"
date: "2009-03-24T06:43:31"
picture: "unteroberwagen03.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23509
- /detailsf51e.html
imported:
- "2019"
_4images_image_id: "23509"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23509 -->
