---
layout: "image"
title: "Derrickwagen CC8800 Twin 7/12"
date: "2009-03-08T18:34:56"
picture: "cctwin07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23428
- /detailsa077.html
imported:
- "2019"
_4images_image_id: "23428"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23428 -->
