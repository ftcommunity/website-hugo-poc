---
layout: "image"
title: "Kranhaken 4/4"
date: "2009-03-24T06:43:31"
picture: "kranhaken4.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23506
- /details61e7-2.html
imported:
- "2019"
_4images_image_id: "23506"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23506 -->
Hier sieht man noch einmal den Größenunterschied.