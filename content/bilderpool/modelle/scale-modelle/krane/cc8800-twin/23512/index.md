---
layout: "image"
title: "Draufsicht Oberwagen 6/6"
date: "2009-03-24T06:43:31"
picture: "unteroberwagen06.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23512
- /details1b9e-3.html
imported:
- "2019"
_4images_image_id: "23512"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23512 -->
