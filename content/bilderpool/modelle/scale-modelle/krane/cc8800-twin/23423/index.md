---
layout: "image"
title: "Derrickwagen CC8800 Twin 2/12"
date: "2009-03-08T18:34:56"
picture: "cctwin02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23423
- /details9789.html
imported:
- "2019"
_4images_image_id: "23423"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23423 -->
Hier sieht man das Modell von oben. Gut zu erkennen ist in der Mitte ein Aluprofil
in der sich eine ft Schnecke befindet um den Wagen vom Kran weg zu schieben oder
heran zu holen.
