---
layout: "image"
title: "CC8800 Twin 1/6"
date: "2009-04-26T19:08:26"
picture: "twin1.jpg"
weight: "28"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23814
- /details88b7.html
imported:
- "2019"
_4images_image_id: "23814"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23814 -->
Komplettansicht
Stand der Dinge(26.04.2009)
Es fehlen noch beide Raupenträger
und die Verdrahtung und dann ist er endlich fertig.
