---
layout: "image"
title: "Bedienpult CC8800 Twin - 2/8"
date: "2011-01-09T17:32:27"
picture: "a2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/29646
- /details4e04.html
imported:
- "2019"
_4images_image_id: "29646"
_4images_cat_id: "2170"
_4images_user_id: "389"
_4images_image_date: "2011-01-09T17:32:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29646 -->
