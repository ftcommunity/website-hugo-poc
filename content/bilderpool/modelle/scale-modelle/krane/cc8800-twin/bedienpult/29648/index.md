---
layout: "image"
title: "Bedienpult CC8800 Twin - 4/8"
date: "2011-01-09T17:32:27"
picture: "a4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/29648
- /details3d39-2.html
imported:
- "2019"
_4images_image_id: "29648"
_4images_cat_id: "2170"
_4images_user_id: "389"
_4images_image_date: "2011-01-09T17:32:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29648 -->
