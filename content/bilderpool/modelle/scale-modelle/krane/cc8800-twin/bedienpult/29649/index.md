---
layout: "image"
title: "Bedienpult CC8800 Twin - 5/8"
date: "2011-01-09T17:32:27"
picture: "a5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/29649
- /details7a27.html
imported:
- "2019"
_4images_image_id: "29649"
_4images_cat_id: "2170"
_4images_user_id: "389"
_4images_image_date: "2011-01-09T17:32:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29649 -->
