---
layout: "image"
title: "CC8800 Twin 6/6"
date: "2009-04-26T19:08:26"
picture: "twin6.jpg"
weight: "33"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23819
- /detailsa7fa-2.html
imported:
- "2019"
_4images_image_id: "23819"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23819 -->
Vorderansicht
