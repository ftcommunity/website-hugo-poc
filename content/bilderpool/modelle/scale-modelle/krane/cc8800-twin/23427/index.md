---
layout: "image"
title: "Derrickwagen CC8800 Twin 6/12"
date: "2009-03-08T18:34:56"
picture: "cctwin06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23427
- /detailseb2e.html
imported:
- "2019"
_4images_image_id: "23427"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23427 -->
