---
layout: "image"
title: "Derrickwagen CC8800 Twin 9/12"
date: "2009-03-08T18:34:57"
picture: "cctwin09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/23430
- /detailse723.html
imported:
- "2019"
_4images_image_id: "23430"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23430 -->
Hier nochmal einen Gesamtansicht von oben.
