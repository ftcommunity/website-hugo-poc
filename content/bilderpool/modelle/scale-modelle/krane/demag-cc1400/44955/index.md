---
layout: "image"
title: "Demag CC1400_9"
date: "2016-12-29T19:33:43"
picture: "demagcc09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44955
- /details16e6.html
imported:
- "2019"
_4images_image_id: "44955"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44955 -->
Der Derrickausleger von hinten.