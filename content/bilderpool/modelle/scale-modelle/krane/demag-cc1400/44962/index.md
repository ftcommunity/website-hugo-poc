---
layout: "image"
title: "Demag CC1400_16"
date: "2016-12-29T19:33:43"
picture: "demagcc16.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44962
- /details61c8-2.html
imported:
- "2019"
_4images_image_id: "44962"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44962 -->
Hauptauslegerspitze und Wippspitzenfuß von vorne gesehen.
Alle eingebauten Seilrollen sind Marke eingenbau.