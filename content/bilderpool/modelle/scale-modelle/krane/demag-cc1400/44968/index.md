---
layout: "image"
title: "Demag CC1400_22"
date: "2016-12-29T19:33:43"
picture: "demagcc22.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44968
- /details44ed.html
imported:
- "2019"
_4images_image_id: "44968"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44968 -->
Wippspitzenauslegerspitze von unten gesehen.