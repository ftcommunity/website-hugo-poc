---
layout: "image"
title: "Demag CC1400_7"
date: "2016-12-29T19:33:43"
picture: "demagcc07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44953
- /details0308.html
imported:
- "2019"
_4images_image_id: "44953"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44953 -->
Schwebende Ballast. 2x Metalstab 10x30x120mm.