---
layout: "image"
title: "Demag CC1400_27"
date: "2016-12-29T19:33:43"
picture: "demagcc27.jpg"
weight: "27"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44973
- /details61e1.html
imported:
- "2019"
_4images_image_id: "44973"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44973 -->
Sicht auf Drehkranz und die kleine zusatz Seilwinde.
Um umkippen der Kran zu verhindern werd zwischen die Abstützarmen noch 2x 15t Zentralballast angebaut. In mein Modell hab is das mit 4x Metalstab 10x30x120mm erreicht.
Die Stützen werden nur beim auf- und abbau benutzt.