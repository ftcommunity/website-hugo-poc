---
layout: "image"
title: "Demag CC1400_20"
date: "2016-12-29T19:33:43"
picture: "demagcc20.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44966
- /details3692.html
imported:
- "2019"
_4images_image_id: "44966"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44966 -->
Um die Seilen durch zu füren sind mehrere Seilrollen eingebaut.