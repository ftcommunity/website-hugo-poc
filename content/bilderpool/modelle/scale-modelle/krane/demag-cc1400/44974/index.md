---
layout: "image"
title: "Demag CC1400_28"
date: "2016-12-29T19:33:43"
picture: "demagcc28.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44974
- /details854c.html
imported:
- "2019"
_4images_image_id: "44974"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44974 -->
Drehantrieb der Kran: "Micro Metal Gear Motor" mit 1:1000 übersetzung.