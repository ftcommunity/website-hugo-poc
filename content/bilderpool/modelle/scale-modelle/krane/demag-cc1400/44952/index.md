---
layout: "image"
title: "Demag CC1400_6"
date: "2016-12-29T19:33:43"
picture: "demagcc06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44952
- /details2560.html
imported:
- "2019"
_4images_image_id: "44952"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44952 -->
Von rechts hinten.