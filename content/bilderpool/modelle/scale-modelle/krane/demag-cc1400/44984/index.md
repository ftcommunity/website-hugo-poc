---
layout: "image"
title: "Demag CC1400_38"
date: "2016-12-29T19:33:43"
picture: "demagcc38.jpg"
weight: "38"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44984
- /details738a.html
imported:
- "2019"
_4images_image_id: "44984"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44984 -->
Alu-platte und der obere Axial-Kugellager.
Das loch in die Alu-platte ist 20mm.
Conrad - 185298: Axial-Rillenkugellager FAG 51111 Bohrungs-Ø 55 mm Außen-Durchmesser 78mm