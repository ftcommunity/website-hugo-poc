---
layout: "image"
title: "Demag CC1400_13"
date: "2016-12-29T19:33:43"
picture: "demagcc13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44959
- /details01d3-3.html
imported:
- "2019"
_4images_image_id: "44959"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44959 -->
Seilwinde der Hauptausleger.