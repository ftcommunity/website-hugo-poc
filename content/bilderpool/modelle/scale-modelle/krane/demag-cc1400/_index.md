---
layout: "overview"
title: "Demag CC1400"
date: 2020-02-22T08:23:39+01:00
legacy_id:
- /php/categories/3343
- /categories5e23.html
- /categoriesb9bc-2.html
- /categoriesb4e6.html
- /categories82a7.html
- /categories2dcb.html
- /categories3583.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3343 --> 
Nachdem Anton Jansen ein Modell einer Liebherr LR1160 in Maßstab 1:22 gebaut hatte, wolte unbedingt auch ein Kran in 1:22 bauen. Der anlas für mein Modell war dan aber der von Anton benutzten 45er Raupenplatten.
Da ich mehr ein Demag fan bin, wolte ich also ein Demag bauen.
Das es letztentlich der CC1400 wurde hat damit zu tun das diesen Kran eben 1m breiten Raupen besitzt.
Da Anton bei fischerfriendswomen schon alle 45er Raupenplatten gekauft hatte, hab ich mein CC1400 erstmals mit der alte 15er aufbauen müssen. Später konte ich dan doch noch 200 Stück kaufen.