---
layout: "image"
title: "MHC700_13"
date: "2005-03-05T16:13:52"
picture: "MHC700_13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/3721
- /details0a7a.html
imported:
- "2019"
_4images_image_id: "3721"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T16:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3721 -->
Der Container greifer.