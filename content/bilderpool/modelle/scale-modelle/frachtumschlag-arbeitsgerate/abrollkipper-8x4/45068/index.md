---
layout: "image"
title: "Mulde 5"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx22.jpg"
weight: "23"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45068
- /details027a.html
imported:
- "2019"
_4images_image_id: "45068"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45068 -->
Klappe geöffnet.
