---
layout: "image"
title: "Abrollkipper Mulde hub"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx08.jpg"
weight: "9"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45054
- /details0012-2.html
imported:
- "2019"
_4images_image_id: "45054"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45054 -->
In normalen Stand ist der Mulde nah an die Kabine
