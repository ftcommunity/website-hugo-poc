---
layout: "image"
title: "Abrollkipper Mulde hub"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx11.jpg"
weight: "12"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45057
- /detailsa4b4.html
imported:
- "2019"
_4images_image_id: "45057"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45057 -->
Am diesen Moment ziehen die 2 Power Motoren circa 2,5 Ampere (und gebe ich noch ein kleines Power Boost nach ca. 15 Volt!
