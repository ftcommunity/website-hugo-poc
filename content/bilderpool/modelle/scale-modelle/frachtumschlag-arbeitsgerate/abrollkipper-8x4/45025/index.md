---
layout: "image"
title: "Rollkipper [1]"
date: "2017-01-11T07:42:21"
picture: "abrollkipper1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45025
- /details0181.html
imported:
- "2019"
_4images_image_id: "45025"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-11T07:42:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45025 -->
Rollkipper 8x4 allrad angetrieben.
Lange: ca 110cm
Breite: ca 32 cm
Hohe: ca 41 cm
