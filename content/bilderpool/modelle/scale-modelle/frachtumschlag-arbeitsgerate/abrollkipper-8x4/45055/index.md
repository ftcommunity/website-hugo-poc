---
layout: "image"
title: "Abrollkipper Mulde"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx09.jpg"
weight: "10"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45055
- /detailsedf6-2.html
imported:
- "2019"
_4images_image_id: "45055"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45055 -->
Als erste wird die Mulde ein stuck nach hinten geschoben.
