---
layout: "image"
title: "Abrollkipper Mulde"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx17.jpg"
weight: "18"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45063
- /details4def.html
imported:
- "2019"
_4images_image_id: "45063"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45063 -->
Hier ein blick über den LKW mit die Zylinder völlig ausgeschoben.

Die Zylinder sind aus Kupferrohr gemacht (15mm x 1.5mm). Innenseite ist aus Aluminium Rohr (12mm x 1mm). An einem Seitz habe ich da ein (kein Idee wie das auf Deutsch heißt) "klinknagel moer" ein geklebt und denn außen Diameter ein wenig abgedreht damit es auch Durchmesser 12mm hat. Im innen Rohr lauft ein Drahtstange M8. Die Stange habe ich abgedreht über eine Länge von 15mm am Ende das es 4mm ist. Im Kupferrohr habe ich an die Antrieb Seite eine Buchse gedreht und in das Rohr gepresst. Am Ende habe ich dann ein Gaffel gefräst (ähnlich wie das hier: http://tb-onderdelen.nl/gaffel-met-borgclip-m10). In der Gaffel kommt dann ein Kardangelenk damit ich die M8 Achse antreiben kann
