---
layout: "image"
title: "Mulde 4"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx21.jpg"
weight: "22"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45067
- /detailscfe8.html
imported:
- "2019"
_4images_image_id: "45067"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45067 -->
Auf die hinterste Seite gibt es 2 Rollen, damit die Mulde über die Boden rollt wann der Kipper die Mulde hebt oder runter legt. Am Heck gibt es 2 Verriegelachsen womit die Klappe verriegelt werden kann.
