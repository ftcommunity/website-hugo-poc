---
layout: "image"
title: "Abrollkipper Mulde hub"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx13.jpg"
weight: "14"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45059
- /detailscedf-2.html
imported:
- "2019"
_4images_image_id: "45059"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45059 -->
Die Mulde kommt mit der Hinterseite auf dem Grund. Die Mulde hat dort 2 rollen damit es über die Grund rollt.
