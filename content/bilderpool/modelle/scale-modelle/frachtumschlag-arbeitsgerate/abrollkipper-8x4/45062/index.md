---
layout: "image"
title: "Abrollkipper Mulde hub"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx16.jpg"
weight: "17"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/45062
- /details3190.html
imported:
- "2019"
_4images_image_id: "45062"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45062 -->
Endlich, Mulde ist auf die Grund. Einem guten Haken bekomme ich noch nicht gebaut aus fischertechnik. So der an und ab Kupplung braucht noch ein wenig Hilfe.
