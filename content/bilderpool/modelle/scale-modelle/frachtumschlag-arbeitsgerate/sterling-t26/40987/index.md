---
layout: "image"
title: "Sterling T26"
date: "2015-05-16T19:01:51"
picture: "P5160132.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/40987
- /details3b83.html
imported:
- "2019"
_4images_image_id: "40987"
_4images_cat_id: "3078"
_4images_user_id: "838"
_4images_image_date: "2015-05-16T19:01:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40987 -->
Aandrijving van de wielen