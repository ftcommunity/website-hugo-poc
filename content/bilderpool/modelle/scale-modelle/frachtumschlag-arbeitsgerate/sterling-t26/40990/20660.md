---
layout: "comment"
hidden: true
title: "20660"
date: "2015-05-17T20:20:37"
uploadBy:
- "chef8"
license: "unknown"
imported:
- "2019"
---
Het totale gewicht op de draaikrans is 4,3 kg. De rubberen banden hebben teveel wrijving op de grond om zijdelings te kunnen schuiven / roteren.
Tijdens het draaien van de draaikrans roteert eerder de complete machine om de vooras!