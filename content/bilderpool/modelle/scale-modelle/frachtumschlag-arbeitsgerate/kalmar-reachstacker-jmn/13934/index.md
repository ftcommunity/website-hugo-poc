---
layout: "image"
title: "detail container handler [2]"
date: "2008-03-17T07:24:29"
picture: "kalmarreackstacker4.jpg"
weight: "4"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/13934
- /details3740.html
imported:
- "2019"
_4images_image_id: "13934"
_4images_cat_id: "1279"
_4images_user_id: "162"
_4images_image_date: "2008-03-17T07:24:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13934 -->
