---
layout: "image"
title: "Besturing"
date: "2011-03-07T19:27:09"
picture: "pivot_014.jpg"
weight: "15"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30231
- /details0d4d-2.html
imported:
- "2019"
_4images_image_id: "30231"
_4images_cat_id: "2245"
_4images_user_id: "838"
_4images_image_date: "2011-03-07T19:27:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30231 -->
Stuuruitslag andere kant uit