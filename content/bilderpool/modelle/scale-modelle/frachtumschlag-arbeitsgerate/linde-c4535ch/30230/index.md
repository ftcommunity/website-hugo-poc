---
layout: "image"
title: "Besturing"
date: "2011-03-07T19:27:09"
picture: "pivot_013.jpg"
weight: "14"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30230
- /detailse01f-2.html
imported:
- "2019"
_4images_image_id: "30230"
_4images_cat_id: "2245"
_4images_user_id: "838"
_4images_image_date: "2011-03-07T19:27:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30230 -->
Bovenaanzicht besturing