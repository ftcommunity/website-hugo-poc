---
layout: "image"
title: "Uitschuifbare arm"
date: "2011-03-07T19:27:09"
picture: "pivot_003.jpg"
weight: "5"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30221
- /detailscf26.html
imported:
- "2019"
_4images_image_id: "30221"
_4images_cat_id: "2245"
_4images_user_id: "838"
_4images_image_date: "2011-03-07T19:27:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30221 -->
Achter in de arm zit een xm motor die rechtstreeks een 4mm draadeind laat draaien voor het uischuiven van de arm