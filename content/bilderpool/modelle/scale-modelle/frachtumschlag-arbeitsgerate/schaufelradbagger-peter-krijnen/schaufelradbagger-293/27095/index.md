---
layout: "image"
title: "Gegengewichtausleger_3"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger036.jpg"
weight: "36"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27095
- /details6c58.html
imported:
- "2019"
_4images_image_id: "27095"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27095 -->
