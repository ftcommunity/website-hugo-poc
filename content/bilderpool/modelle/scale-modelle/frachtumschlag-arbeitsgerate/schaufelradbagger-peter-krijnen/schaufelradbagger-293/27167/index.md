---
layout: "image"
title: "Raupenschiffen 5"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger108.jpg"
weight: "108"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27167
- /details06a6.html
imported:
- "2019"
_4images_image_id: "27167"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "108"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27167 -->
Seiten anzicht mit antrieb