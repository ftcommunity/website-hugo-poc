---
layout: "image"
title: "Raupenschiffen 9"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger112.jpg"
weight: "112"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27171
- /details2c02.html
imported:
- "2019"
_4images_image_id: "27171"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "112"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27171 -->
Antriebrad, Umlenkrad und die Laufräder.