---
layout: "image"
title: "Rote Bausteinen 1"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger121.jpg"
weight: "121"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27180
- /detailsa5f1.html
imported:
- "2019"
_4images_image_id: "27180"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "121"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27180 -->
37237 - 273x
37238 - 86x
37468 - 456x