---
layout: "image"
title: "Hauptgerät_8"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger012.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27071
- /details1736-5.html
imported:
- "2019"
_4images_image_id: "27071"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27071 -->
