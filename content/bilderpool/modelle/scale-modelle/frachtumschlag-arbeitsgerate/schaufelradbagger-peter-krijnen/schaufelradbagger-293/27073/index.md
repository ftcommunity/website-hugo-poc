---
layout: "image"
title: "Kran 2_2"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger014.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27073
- /details21ee-3.html
imported:
- "2019"
_4images_image_id: "27073"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27073 -->
