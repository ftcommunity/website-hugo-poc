---
layout: "image"
title: "Graue Bausteinen"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger117.jpg"
weight: "117"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27176
- /details1557-2.html
imported:
- "2019"
_4images_image_id: "27176"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "117"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27176 -->
31003 - 873x
31004 - 24x
31005 - 823x
31006 - 367x
31007 - 8x
31059 - 27x