---
layout: "image"
title: "Frame der Bandschleife 4_1"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger072.jpg"
weight: "72"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27131
- /detailsc36d.html
imported:
- "2019"
_4images_image_id: "27131"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27131 -->
