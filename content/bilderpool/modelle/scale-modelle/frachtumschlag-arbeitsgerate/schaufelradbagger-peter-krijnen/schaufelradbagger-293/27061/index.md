---
layout: "image"
title: "Schaufelradbagger 293_2"
date: "2010-05-08T20:05:16"
picture: "schaufelradbagger002.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27061
- /detailsed8d.html
imported:
- "2019"
_4images_image_id: "27061"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27061 -->
Hier das Modell auf meine arbeitsplats.