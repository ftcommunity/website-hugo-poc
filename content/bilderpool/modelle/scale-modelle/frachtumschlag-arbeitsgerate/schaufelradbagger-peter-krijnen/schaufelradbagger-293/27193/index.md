---
layout: "image"
title: "Ausleger_4"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger134.jpg"
weight: "134"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27193
- /details4d49.html
imported:
- "2019"
_4images_image_id: "27193"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "134"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27193 -->
