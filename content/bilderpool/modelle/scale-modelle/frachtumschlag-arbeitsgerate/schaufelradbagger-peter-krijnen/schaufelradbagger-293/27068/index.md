---
layout: "image"
title: "Hauptgerät_5"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger009.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27068
- /detailsf7c2.html
imported:
- "2019"
_4images_image_id: "27068"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27068 -->
