---
layout: "image"
title: "Gegengewichtausleger_1"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger034.jpg"
weight: "34"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27093
- /details5774-2.html
imported:
- "2019"
_4images_image_id: "27093"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27093 -->
Neben das Gegengewicht beinhaltet diesen ausleger auch die 4 Seilwinden, ein Wartungkran und der Steuercentrale.