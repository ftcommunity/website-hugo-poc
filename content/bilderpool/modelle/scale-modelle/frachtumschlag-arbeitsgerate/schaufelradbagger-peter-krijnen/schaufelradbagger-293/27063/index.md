---
layout: "image"
title: "Frame 2"
date: "2010-05-08T20:05:16"
picture: "schaufelradbagger004.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27063
- /detailsf026.html
imported:
- "2019"
_4images_image_id: "27063"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27063 -->
Das Frame: Raupen mit Raupengruppenträger und deren Lagerpunkten.