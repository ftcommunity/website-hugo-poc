---
layout: "image"
title: "Kran 2_4"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger016.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27075
- /detailsba5b-3.html
imported:
- "2019"
_4images_image_id: "27075"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27075 -->
Antrieb und lagerung sind mit Kugellager ausgestatet, damit das drehen gleichmäsich und rückelfrei verlauft.
Damit die vier drahten zu die beide motoren kommen, ist die achse der Kran hohl.