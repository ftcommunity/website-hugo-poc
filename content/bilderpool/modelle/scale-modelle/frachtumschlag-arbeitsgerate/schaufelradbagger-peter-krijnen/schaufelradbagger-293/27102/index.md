---
layout: "image"
title: "Gegengewichtausleger_10"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger043.jpg"
weight: "43"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27102
- /detailsa88b.html
imported:
- "2019"
_4images_image_id: "27102"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27102 -->
