---
layout: "image"
title: "Baggerbrücke unterseite 1"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger079.jpg"
weight: "79"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27138
- /details610c-2.html
imported:
- "2019"
_4images_image_id: "27138"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27138 -->
#31263 Aufliegerkupplung unterseite werd hier als Oberteil benutst.
Die hier noch sichtbaren Seilrolle 12, mus das band in horizontaler lage ausrichten