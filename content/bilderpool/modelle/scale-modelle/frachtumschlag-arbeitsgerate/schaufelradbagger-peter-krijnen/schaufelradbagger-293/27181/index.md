---
layout: "image"
title: "Rote Bausteinen 2"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger122.jpg"
weight: "122"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27181
- /details4c56-2.html
imported:
- "2019"
_4images_image_id: "27181"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "122"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27181 -->
32064 - 153x
32315 - 2x
32330 - 101x
35049 - 44x
38424 - 1x
38428 - 35x
