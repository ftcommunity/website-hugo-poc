---
layout: "image"
title: "Kran 3-1"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger020.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27079
- /details2b4f-3.html
imported:
- "2019"
_4images_image_id: "27079"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27079 -->
