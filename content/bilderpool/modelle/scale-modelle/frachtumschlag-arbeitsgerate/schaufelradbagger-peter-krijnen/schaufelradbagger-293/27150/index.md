---
layout: "image"
title: "Abgabegerät_10"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger091.jpg"
weight: "91"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27150
- /detailseef0.html
imported:
- "2019"
_4images_image_id: "27150"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27150 -->
