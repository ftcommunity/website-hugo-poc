---
layout: "image"
title: "Kabinen_3"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger139.jpg"
weight: "139"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27198
- /details2a13.html
imported:
- "2019"
_4images_image_id: "27198"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "139"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27198 -->
In verhältnis sind die beide steuerkabinen zu groß.