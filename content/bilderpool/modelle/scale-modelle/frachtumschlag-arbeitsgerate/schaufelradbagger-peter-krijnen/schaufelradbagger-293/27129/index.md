---
layout: "image"
title: "Baggerbrücke_22"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger070.jpg"
weight: "70"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27129
- /details5d80-2.html
imported:
- "2019"
_4images_image_id: "27129"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27129 -->
