---
layout: "image"
title: "Kabel"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger130.jpg"
weight: "130"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27189
- /details9476-2.html
imported:
- "2019"
_4images_image_id: "27189"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27189 -->
Das braucht man um alle Motoren, Schalter und Interfaces zu verbinden