---
layout: "image"
title: "Kran 3-4"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger023.jpg"
weight: "23"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27082
- /details1843.html
imported:
- "2019"
_4images_image_id: "27082"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27082 -->
