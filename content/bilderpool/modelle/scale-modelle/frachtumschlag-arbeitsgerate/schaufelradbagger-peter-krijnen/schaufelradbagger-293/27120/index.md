---
layout: "image"
title: "Baggerbrücke_13"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger061.jpg"
weight: "61"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27120
- /details0eb2-2.html
imported:
- "2019"
_4images_image_id: "27120"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27120 -->
