---
layout: "image"
title: "Raupenschiffen 11"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger114.jpg"
weight: "114"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27173
- /detailsf548.html
imported:
- "2019"
_4images_image_id: "27173"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "114"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27173 -->
