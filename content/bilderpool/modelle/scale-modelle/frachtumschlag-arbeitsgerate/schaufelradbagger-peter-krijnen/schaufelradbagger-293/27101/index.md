---
layout: "image"
title: "Gegengewichtausleger_9"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger042.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27101
- /detailsfc4f-3.html
imported:
- "2019"
_4images_image_id: "27101"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27101 -->
