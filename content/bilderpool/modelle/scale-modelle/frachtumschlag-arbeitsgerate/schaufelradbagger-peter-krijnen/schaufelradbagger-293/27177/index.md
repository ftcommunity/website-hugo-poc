---
layout: "image"
title: "Lagersteinen, Räder, Seilrollen"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger118.jpg"
weight: "118"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27177
- /details039c.html
imported:
- "2019"
_4images_image_id: "27177"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "118"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27177 -->
35795 - 18x
35797 - 53x
35969 - 50x
36573 - 137x
36581 - 28x
36586 - 36x
37636 - 151x
38252 - 2x
38253 - 8x
38258 - 15x
38260 - 30x