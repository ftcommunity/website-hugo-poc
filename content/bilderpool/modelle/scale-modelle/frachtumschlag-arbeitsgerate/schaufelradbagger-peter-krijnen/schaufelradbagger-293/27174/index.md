---
layout: "image"
title: "Raupenschiffen 12"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger115.jpg"
weight: "115"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27174
- /details6581-2.html
imported:
- "2019"
_4images_image_id: "27174"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "115"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27174 -->
