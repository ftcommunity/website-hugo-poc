---
layout: "image"
title: "Kabinen_1"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger137.jpg"
weight: "137"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27196
- /details9e35.html
imported:
- "2019"
_4images_image_id: "27196"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "137"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27196 -->
