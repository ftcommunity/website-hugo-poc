---
layout: "image"
title: "Steuerung der Raupengruppen 2"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger105.jpg"
weight: "105"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27164
- /detailsed81-3.html
imported:
- "2019"
_4images_image_id: "27164"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27164 -->
M-Motor und getriebe zur steuerung der vorderste Raupengruppe