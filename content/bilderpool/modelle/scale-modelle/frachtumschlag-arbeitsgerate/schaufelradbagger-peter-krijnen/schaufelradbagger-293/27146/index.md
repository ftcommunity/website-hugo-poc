---
layout: "image"
title: "Abgabegerät_6"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger087.jpg"
weight: "87"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27146
- /detailsc8a0-4.html
imported:
- "2019"
_4images_image_id: "27146"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27146 -->
