---
layout: "image"
title: "Steuercentrale_3"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger032.jpg"
weight: "32"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27091
- /detailsda24.html
imported:
- "2019"
_4images_image_id: "27091"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27091 -->
In der mitte, der Spannungsstabilisator für die 5Volt der Gabellichtschranken.