---
layout: "image"
title: "Hauptgerät_1"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger005.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27064
- /details2abf.html
imported:
- "2019"
_4images_image_id: "27064"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27064 -->
