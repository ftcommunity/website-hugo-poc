---
layout: "image"
title: "Drehkranze 2_3"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger078.jpg"
weight: "78"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27137
- /detailsf039-3.html
imported:
- "2019"
_4images_image_id: "27137"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27137 -->
