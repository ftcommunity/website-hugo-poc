---
layout: "image"
title: "Winkelsteinen"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger119.jpg"
weight: "119"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27178
- /details1d02-2.html
imported:
- "2019"
_4images_image_id: "27178"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "119"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27178 -->
31010 - 305x
31011 - 116x
31012 - 12x
31918 - 44x
31981 - 340x
32071 - 192x