---
layout: "image"
title: "Steuercentrale_1"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger030.jpg"
weight: "30"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27089
- /details3825-2.html
imported:
- "2019"
_4images_image_id: "27089"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27089 -->
Centrale steuercentrale besteht aus:
2 x ROBO pro mit RF und 4 Extentions.
2 Weitere Ext. sind in das Abgabegerät eingebaut.