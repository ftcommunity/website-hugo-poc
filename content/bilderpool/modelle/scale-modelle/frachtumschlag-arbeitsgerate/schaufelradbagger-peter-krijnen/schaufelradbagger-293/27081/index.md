---
layout: "image"
title: "Kran 3-3"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger022.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27081
- /details5b67-3.html
imported:
- "2019"
_4images_image_id: "27081"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27081 -->
