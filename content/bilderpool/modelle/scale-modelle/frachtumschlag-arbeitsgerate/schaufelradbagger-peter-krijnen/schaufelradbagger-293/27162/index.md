---
layout: "image"
title: "Steuerung der Raupengruppen 1"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger103.jpg"
weight: "103"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27162
- /details64b7.html
imported:
- "2019"
_4images_image_id: "27162"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27162 -->
Hier blickt noch das ende einer der Schnecken zur steuerung der beide hinterste Raupengruppen.