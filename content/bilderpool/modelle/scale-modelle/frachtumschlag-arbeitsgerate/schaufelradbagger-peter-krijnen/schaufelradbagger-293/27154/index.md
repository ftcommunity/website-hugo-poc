---
layout: "image"
title: "Bandschleive 5"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger095.jpg"
weight: "95"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27154
- /details6045.html
imported:
- "2019"
_4images_image_id: "27154"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27154 -->
Wie alle Bändern Ist auch dieses Band aus 4cm breiten elastischen Gewebe.
Zo etwas findet man auch in sein pyjamahose. Nur etwas breiter.