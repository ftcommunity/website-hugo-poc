---
layout: "image"
title: "Baggerbrücke_21"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger069.jpg"
weight: "69"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27128
- /details4914.html
imported:
- "2019"
_4images_image_id: "27128"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27128 -->
