---
layout: "image"
title: "Frame des Abgabegarät_3"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger099.jpg"
weight: "99"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27158
- /details6861.html
imported:
- "2019"
_4images_image_id: "27158"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27158 -->
