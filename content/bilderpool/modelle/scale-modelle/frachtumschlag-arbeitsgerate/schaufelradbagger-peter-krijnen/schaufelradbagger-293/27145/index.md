---
layout: "image"
title: "Abgabegerät_5"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger086.jpg"
weight: "86"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/27145
- /details0ec8-2.html
imported:
- "2019"
_4images_image_id: "27145"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27145 -->
