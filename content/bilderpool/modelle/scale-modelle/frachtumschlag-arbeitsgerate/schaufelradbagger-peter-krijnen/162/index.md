---
layout: "image"
title: "details1"
date: "2003-04-21T19:26:58"
picture: "details1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/162
- /details990b.html
imported:
- "2019"
_4images_image_id: "162"
_4images_cat_id: "11"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=162 -->
