---
layout: "image"
title: "Schaufelradbagger (versuchter nachbau des ft originals)"
date: "2004-03-04T17:45:40"
picture: "Img_0008.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/2263
- /detailsa1d8-3.html
imported:
- "2019"
_4images_image_id: "2263"
_4images_cat_id: "11"
_4images_user_id: "14"
_4images_image_date: "2004-03-04T17:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2263 -->
