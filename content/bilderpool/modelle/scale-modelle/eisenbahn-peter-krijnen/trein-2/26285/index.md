---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 7"
date: "2010-02-10T15:59:14"
picture: "trein35.jpg"
weight: "35"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26285
- /detailscea6.html
imported:
- "2019"
_4images_image_id: "26285"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26285 -->
