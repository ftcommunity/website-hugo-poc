---
layout: "image"
title: "Trein 2: Lok, Drehgestell von oben"
date: "2010-02-10T15:59:14"
picture: "trein25.jpg"
weight: "25"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26275
- /details048d.html
imported:
- "2019"
_4images_image_id: "26275"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26275 -->
