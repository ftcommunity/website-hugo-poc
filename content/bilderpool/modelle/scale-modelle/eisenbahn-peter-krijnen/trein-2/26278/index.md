---
layout: "image"
title: "Trein 2: Lok, unterseite frame"
date: "2010-02-10T15:59:14"
picture: "trein28.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26278
- /details119d-2.html
imported:
- "2019"
_4images_image_id: "26278"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26278 -->
Das drehgestell wird mit ein Clipsachse #32870 und eine Scheibe #31647 an das frame bevestigd.
