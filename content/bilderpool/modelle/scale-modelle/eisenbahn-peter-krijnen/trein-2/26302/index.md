---
layout: "image"
title: "Trein 2: Großen Kran 3"
date: "2010-02-10T15:59:15"
picture: "trein52.jpg"
weight: "52"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26302
- /details3751-3.html
imported:
- "2019"
_4images_image_id: "26302"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26302 -->
...und ein dritte ist zur drehen der Kran.