---
layout: "image"
title: "Trein 2: der gegengewichtswagen 4"
date: "2010-02-10T15:59:15"
picture: "trein42.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26292
- /detailsae8c.html
imported:
- "2019"
_4images_image_id: "26292"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26292 -->
