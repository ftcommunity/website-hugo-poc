---
layout: "image"
title: "Trein 2: ...Wagon für gegengewicht und matratzen..."
date: "2010-02-10T15:59:13"
picture: "trein08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26258
- /details743a.html
imported:
- "2019"
_4images_image_id: "26258"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26258 -->
Zur vorbild stand das LGB Modell #42610