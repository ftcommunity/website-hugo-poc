---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 1"
date: "2010-02-10T15:59:14"
picture: "trein29.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26279
- /details410b.html
imported:
- "2019"
_4images_image_id: "26279"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26279 -->
Dies ist ein nachbau der kleine LGB kran. Leider ohne ausfahrbaren ausleger.