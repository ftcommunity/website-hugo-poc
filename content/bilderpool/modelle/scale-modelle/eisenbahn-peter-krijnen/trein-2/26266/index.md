---
layout: "image"
title: "Trein 2: Lok, antrieb"
date: "2010-02-10T15:59:14"
picture: "trein16.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26266
- /details9578.html
imported:
- "2019"
_4images_image_id: "26266"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26266 -->
...die mittels eine Kette 2 Schnecken, die auch mit ein z10 auf einen Achse montiert is, antreibt, ....