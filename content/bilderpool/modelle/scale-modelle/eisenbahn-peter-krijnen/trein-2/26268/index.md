---
layout: "image"
title: "Trein 2: Lok, lagerung der Achsen"
date: "2010-02-10T15:59:14"
picture: "trein18.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
schlagworte: ["modding"]
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26268
- /detailscf17.html
imported:
- "2019"
_4images_image_id: "26268"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26268 -->
Ein Kugellager 4x8x3 (Conrad # 215724 - 89 oder 295604 - 89) ist in ein halbierten Riegelstein #32850 eingelassen....