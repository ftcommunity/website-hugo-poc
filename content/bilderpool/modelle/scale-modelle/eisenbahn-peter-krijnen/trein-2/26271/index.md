---
layout: "image"
title: "Trein 2: Lok, was braucht man für ein rad?"
date: "2010-02-10T15:59:14"
picture: "trein21.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26271
- /details67e8.html
imported:
- "2019"
_4images_image_id: "26271"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26271 -->
neben den Spurkranz braucht man ein Gummiring 24x2mm, ein Nabenmutter und ein Flachnabe.