---
layout: "image"
title: "Trein 2: Kleine Kran 'MATRA' 4"
date: "2010-02-10T15:59:14"
picture: "trein32.jpg"
weight: "32"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26282
- /details1fb7.html
imported:
- "2019"
_4images_image_id: "26282"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26282 -->
Der Hydraulik aus der gelben Radlader kasten #30466