---
layout: "image"
title: "Trein 2: ...Eisenbahnkran..."
date: "2010-02-10T15:59:13"
picture: "trein07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26257
- /details972e.html
imported:
- "2019"
_4images_image_id: "26257"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26257 -->
Zur vorbild stand ein Eisenbahnkran der Rhätische Bahn