---
layout: "image"
title: "Trein 2: Ausleger-auflagewagen 2"
date: "2010-02-10T15:59:15"
picture: "trein45.jpg"
weight: "45"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26295
- /details63b7.html
imported:
- "2019"
_4images_image_id: "26295"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26295 -->
Das es an diesen Wagen kein Luftdruckbremse gibt, mus ein mitarbeiter der Eisenbahn der Bremse met der Hand drehen.