---
layout: "image"
title: "Trein 2: Lok, antrieb und Stromentname"
date: "2010-02-10T15:59:14"
picture: "trein17.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/26267
- /details0b5c-2.html
imported:
- "2019"
_4images_image_id: "26267"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26267 -->
...die je einen achse mit ein z10 antreibt.
Hier ist auch einer der beide Stromabnehmer zu sehen.
Dies ist ein bearbeiteten LGB #63193, die zu Zweit verkauft werden.