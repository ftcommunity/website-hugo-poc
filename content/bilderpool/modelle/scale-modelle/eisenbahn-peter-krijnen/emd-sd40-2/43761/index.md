---
layout: "image"
title: "EMD SD40-2. 22"
date: "2016-06-12T19:48:23"
picture: "emdsd22.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43761
- /details4d81.html
imported:
- "2019"
_4images_image_id: "43761"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43761 -->
Da die #36333, Laufschien 240, nicht leitend sind, musten 2 Mesingrohren als Stromschiene benutst werden. Mit 4x 2 Federkontakt #31306 ist die stromabname aber gesichert.