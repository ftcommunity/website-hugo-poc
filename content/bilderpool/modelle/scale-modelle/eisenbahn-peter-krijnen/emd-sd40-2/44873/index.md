---
layout: "image"
title: "EMD SD40-2. 49 CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd10_2.jpg"
weight: "49"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44873
- /details023e.html
imported:
- "2019"
_4images_image_id: "44873"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44873 -->
Kleine Platine große wirkung: 2x 4 Weiße SMD LED's und 2x 47Ohm SMD Widerständen.