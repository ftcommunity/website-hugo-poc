---
layout: "image"
title: "EMD SD40-2. 27"
date: "2016-06-12T19:48:23"
picture: "emdsd27.jpg"
weight: "27"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43766
- /details4d75.html
imported:
- "2019"
_4images_image_id: "43766"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43766 -->
#35019 Gelenkmutter schwarz: damit die LED's + platinen fest sitzen, hab ich das loch aufgebohrt bis 4,8mm für die beide Rote LED's, und bis 4,9mm für die 4 Weise LED's.