---
layout: "image"
title: "EMD SD40-2. 13"
date: "2016-06-12T19:48:23"
picture: "emdsd13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43752
- /detailse377.html
imported:
- "2019"
_4images_image_id: "43752"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43752 -->
Die 3 Ventilatoren für die Dynamische Bremse.

Da die 6 Motoren Mechanisch gekuppeld sind an die Achsen. drehen die immer wen gebremst werd. Die Motoren arbeiten in diesen fal als Generatoren. Motoren verbrauchen Strom, Generatoren generieren Strom. Diesen Strom werd dan durch Widerständen geleitet, wo sie in wärme umgezetst werd.
Diese wärme werd mit Ventilatoren abgesaugt.