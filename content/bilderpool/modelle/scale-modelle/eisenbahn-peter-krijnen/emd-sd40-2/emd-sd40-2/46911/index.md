---
layout: "image"
title: "EMD SD40-2. 55"
date: "2017-11-08T17:20:12"
picture: "emdsd02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46911
- /detailsf1b6.html
imported:
- "2019"
_4images_image_id: "46911"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46911 -->
Gegenüber Bild 43 ist deutlich zu erkennen das ich die Geländer an den beide Fronten geänderd habe. Der kurze Nase ist dabei um 15mm höher geworden. Der Kette komt aus der Schifmodellbau.