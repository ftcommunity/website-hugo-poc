---
layout: "image"
title: "EMD SD40-2. 60"
date: "2017-11-08T17:20:12"
picture: "emdsd07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46916
- /details0860.html
imported:
- "2019"
_4images_image_id: "46916"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46916 -->
Der Distanz zwischen die beide Drehgestellen ist um 45mm verlängerd.