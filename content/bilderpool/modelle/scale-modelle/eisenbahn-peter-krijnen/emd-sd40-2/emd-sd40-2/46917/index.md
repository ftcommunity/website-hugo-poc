---
layout: "image"
title: "EMD SD40-2. 61"
date: "2017-11-08T17:20:12"
picture: "emdsd08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46917
- /details545c-2.html
imported:
- "2019"
_4images_image_id: "46917"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46917 -->
Beide Lufttanks sind natürlich auch länger geworden.
Gegenüber Bild 12 ist zu erkenen das auch die Luftfilterausbau länger geworden ist.