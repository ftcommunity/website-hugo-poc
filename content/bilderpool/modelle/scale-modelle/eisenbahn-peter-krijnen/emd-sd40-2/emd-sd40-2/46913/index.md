---
layout: "image"
title: "EMD SD40-2. 57"
date: "2017-11-08T17:20:12"
picture: "emdsd04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/46913
- /detailsfeff-2.html
imported:
- "2019"
_4images_image_id: "46913"
_4images_cat_id: "3472"
_4images_user_id: "144"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46913 -->
Der Dieseltank ist 45m länger geworden.