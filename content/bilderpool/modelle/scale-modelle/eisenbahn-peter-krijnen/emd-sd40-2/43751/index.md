---
layout: "image"
title: "EMD SD40-2. 12"
date: "2016-06-12T19:48:23"
picture: "emdsd12.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43751
- /detailsb240.html
imported:
- "2019"
_4images_image_id: "43751"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43751 -->
Der Sd40-2 von oben.
Auch in der relität, sehen die 2 Fan's links und die 3 Fan's rechts, anders aus.