---
layout: "image"
title: "EMD SD40-2. 29"
date: "2016-06-12T19:48:23"
picture: "emdsd29.jpg"
weight: "29"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43768
- /detailsfd39.html
imported:
- "2019"
_4images_image_id: "43768"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43768 -->
Die 5 "Propeller 70- 4x bl" #36337, sind auf 50mm gekürzt worden, damit sie frei drehen können.
Hier seht man auch gleich wie ich #35694 Innenzahnrad, angebaut habbe.
Um freiraum für die Propeller zu bekommen, dienen 4x  #37679 Klembuchsen 5 und 4 x #31597 Abstandsring 3, als abstandshalter.