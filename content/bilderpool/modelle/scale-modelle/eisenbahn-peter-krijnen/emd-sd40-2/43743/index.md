---
layout: "image"
title: "EMD SD40-2. 4"
date: "2016-06-12T19:48:23"
picture: "emdsd04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43743
- /details217d.html
imported:
- "2019"
_4images_image_id: "43743"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43743 -->
Blick auf der Vorderseite