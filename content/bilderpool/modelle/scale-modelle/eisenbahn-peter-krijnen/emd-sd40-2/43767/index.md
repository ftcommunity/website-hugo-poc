---
layout: "image"
title: "EMD SD40-2. 28"
date: "2016-06-12T19:48:23"
picture: "emdsd28.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43767
- /details2c21.html
imported:
- "2019"
_4images_image_id: "43767"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43767 -->
Einer von 8 eingebauten SMD-LED-Platinen: SMD LED + Widerstand.