---
layout: "image"
title: "EMD SD40-2. 42  CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd03_2.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/44866
- /details3dcb-2.html
imported:
- "2019"
_4images_image_id: "44866"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44866 -->
Einblick in der Steuercentrale, Beide Kabineleuchten eingeschaltet.