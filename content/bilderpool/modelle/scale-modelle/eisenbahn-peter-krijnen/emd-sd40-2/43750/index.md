---
layout: "image"
title: "EMD SD40-2. 11"
date: "2016-06-12T19:48:23"
picture: "emdsd11.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43750
- /details9d72.html
imported:
- "2019"
_4images_image_id: "43750"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43750 -->
Blick von hinten.
Da die Amerikaner nur mit Kabine voraus fahren, frage ich mir ob die beide "Ditch Lights" hier fehlen durfen. Wen ja, dan werde ich die nicht entfernen. Es seht mir einfach gut aus.