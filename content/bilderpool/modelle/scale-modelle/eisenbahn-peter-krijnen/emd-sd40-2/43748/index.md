---
layout: "image"
title: "EMD SD40-2. 9"
date: "2016-06-12T19:48:23"
picture: "emdsd09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43748
- /details5f0c.html
imported:
- "2019"
_4images_image_id: "43748"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43748 -->
Weshalb ist da ein Steuerrad in der Vorbau eingebaut?
Handbremse.
Und was sol den das kleine Rad da auf der Spitse der Vobau?
Abdekkung der Fülöffnung der Sandkaste.