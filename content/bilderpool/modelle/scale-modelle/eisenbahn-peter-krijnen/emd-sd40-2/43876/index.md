---
layout: "image"
title: "EMD SD40-2. 37  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd7.jpg"
weight: "37"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43876
- /details2ddf.html
imported:
- "2019"
_4images_image_id: "43876"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43876 -->
Klauenkupplung geschlossen