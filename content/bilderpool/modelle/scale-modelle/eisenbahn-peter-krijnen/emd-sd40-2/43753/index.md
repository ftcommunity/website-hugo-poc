---
layout: "image"
title: "EMD SD40-2. 14"
date: "2016-06-12T19:48:23"
picture: "emdsd14.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43753
- /details0e2d-2.html
imported:
- "2019"
_4images_image_id: "43753"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43753 -->
Antrieb der Ventilatoren.
Die beide motoren (Igarachi) habbe ich bei Conrad gekauft. Die getrieben habbe ich aber an ft motoren angebaut. Die 1:8 getrieben sind dan an die Conradmotoren angebaut.
Die augängen von der Decoder hab ich auf 12Volt begrensen können.