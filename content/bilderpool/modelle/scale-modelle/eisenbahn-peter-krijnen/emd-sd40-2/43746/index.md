---
layout: "image"
title: "EMD SD40-2. 7"
date: "2016-06-12T19:48:23"
picture: "emdsd07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/43746
- /detailsb414.html
imported:
- "2019"
_4images_image_id: "43746"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43746 -->
Sicht auf das vodere Drehgestel:
4 Bremscylinder für 6 Bremsen. Die Bremsklotzen, hab ich aber nicht eingebaut.
Rechts neben der Dieseltank ist noch ein Glocke.
Die hab ich bei ein Modellbauladen gefunden bei der abteilung Schifsmodelbau. Mit ein #31024 Klemmkupplung und #31124 Aufnahmeachse, ist die Glocke angebaut.