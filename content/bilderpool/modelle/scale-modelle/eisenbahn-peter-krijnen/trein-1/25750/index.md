---
layout: "image"
title: "Kleine Kran"
date: "2009-11-08T19:43:55"
picture: "trein22.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25750
- /details8ae0-2.html
imported:
- "2019"
_4images_image_id: "25750"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25750 -->
Vorbild LGB #40420: der Welt bekanten kleine Matra