---
layout: "image"
title: "Das innen leben."
date: "2009-11-08T19:43:54"
picture: "trein10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25738
- /details108c-2.html
imported:
- "2019"
_4images_image_id: "25738"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25738 -->
Der 24Volt version der "M"-motor.