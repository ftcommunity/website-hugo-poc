---
layout: "image"
title: "Zwei Achsigen Rungenwagen..."
date: "2009-11-08T19:43:55"
picture: "trein20.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25748
- /details992d.html
imported:
- "2019"
_4images_image_id: "25748"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25748 -->
LGB hat leider keine Zwei Achsigen Rungenwagen. Aber das wahr kein problem für mich.