---
layout: "image"
title: "...und wie zusamen gesteck werden"
date: "2009-11-08T19:43:55"
picture: "trein19.jpg"
weight: "19"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25747
- /detailsdd7f.html
imported:
- "2019"
_4images_image_id: "25747"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25747 -->
