---
layout: "image"
title: ".. von der linken seite...."
date: "2009-11-08T19:43:54"
picture: "trein06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/25734
- /detailsfaf8.html
imported:
- "2019"
_4images_image_id: "25734"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25734 -->
Das model der kleine Köf von LGB (#21900) hat mir als vorbild gedient.
Wen auch ohne Koppelstangen.