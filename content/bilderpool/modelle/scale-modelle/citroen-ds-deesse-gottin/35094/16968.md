---
layout: "comment"
hidden: true
title: "16968"
date: "2012-07-08T11:27:13"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die Lenkgeometrie ist wirklich aus der Not geboren und ist ein Parallelogramm (nix Ackermann-Trapez und so). Unter Zug gehen die Kardans und Rastachsen auf --- aber nur noch solange, bis die Rastklipse mit kleinen schwarzen Plastikkeilen verblockt sind. Aber auf die Stabilität vom Rest lass ich nichts kommen! :-)

Gruß,
Harald