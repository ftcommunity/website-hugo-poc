---
layout: "image"
title: "Deesse12m1.JPG"
date: "2012-07-03T21:35:32"
picture: "Deesse12m1.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35090
- /details4e51.html
imported:
- "2019"
_4images_image_id: "35090"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:35:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35090 -->
Das wichtigste gleich vorne weg: unter der Motorhaube werkelt ein Frontantrieb. Und: der Antrieb funktioniert. Details dazu gibt es hier: http://www.ftcommunity.de/categories.php?cat_id=1613 . Der Motor sitzt zwischen den noch nicht vorhandenen Vordersitzen, längs unter dem E-Tec.
