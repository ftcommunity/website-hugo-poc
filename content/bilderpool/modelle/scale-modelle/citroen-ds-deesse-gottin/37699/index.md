---
layout: "image"
title: "IMG_9890.JPG"
date: "2013-10-08T21:32:51"
picture: "IMG_9890.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37699
- /details387a-3.html
imported:
- "2019"
_4images_image_id: "37699"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T21:32:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37699 -->
Citroen DS, reloaded
Sie hat jetzt einen kompakteren Antrieb (der Motor ist nach vorn unter die Haube gewandert), außerdem ist sie jetzt "länger" übersetzt und kommt schneller vom Fleck.

Gerade habe ich hier http://www.ftcommunity.de/details.php?image_id=37698
noch behauptet, dass der neue Antrieb nimmer kompakter zu machen ginge. Ätsch, geht aber do-ho-ch: man kann den S-Motor mit Hubgetriebe ganz dicht heranrutschen. Dafür wird der Lenkeinschlag aber beeinträchtigt, und dagegen hat dann doch das Modding-Messer wieder zugeschlagen und einen angeschrägten Winkel aus der Sortierkiste mit den "bösen" Teilen hervorgeholt.
