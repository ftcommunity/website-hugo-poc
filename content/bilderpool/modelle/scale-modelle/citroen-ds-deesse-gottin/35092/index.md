---
layout: "image"
title: "Deesse15m1.JPG"
date: "2012-07-03T21:39:59"
picture: "Deesse15m1.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35092
- /details49d8.html
imported:
- "2019"
_4images_image_id: "35092"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:39:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35092 -->
Die vorderen Türen haben Klappverschlüsse, hinten sind es gefederte Schnappverschlüsse. Der P-Zylinder gehört zur pneumatisch gefederten Hinterachse.
