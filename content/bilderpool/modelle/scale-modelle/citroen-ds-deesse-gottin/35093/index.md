---
layout: "image"
title: "Deesse19m1.JPG"
date: "2012-07-03T21:45:09"
picture: "Deesse19m1.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35093
- /detailse7f2.html
imported:
- "2019"
_4images_image_id: "35093"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:45:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35093 -->
Hier ist der Kofferraum geöffnet. Die Nische fürs doppelte hintere Kennzeichen (eins aufrecht, eins liegend) ist auch da. Die rückwärtigen Blinker sitzen da, wo sie hingehören: unterm Dachjuchee. 

Mangels Kompressor (der soll noch vor die Hinterachse) muss die Stütze verhindern, dass die Deesse "Platz macht" und sich mit dem Hinterteil auf den Boden absenkt. Der Federweg reicht dafür voll aus.

Die Seiten gehören natürlich auch noch mit Moosgummi verkleidet.
