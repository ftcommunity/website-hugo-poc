---
layout: "image"
title: "DS-Skizze.jpg"
date: "2012-07-08T11:16:34"
picture: "Deesse45.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35106
- /detailscc15.html
imported:
- "2019"
_4images_image_id: "35106"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-08T11:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35106 -->
Das ist eine von den Skizzen (neben vielen Fotos aus dem Netz), die herangezogen wurden. Ausgangspunkt sind 60 mm Raddurchmesser und 180 mm Breite; der Rest wurde dann so gut wie möglich hinskaliert. Der Winkel der Heckschräge ist das Problemkind geblieben.
