---
layout: "image"
title: "Magirus-Deutz"
date: 2024-01-04T12:19:43+01:00
picture: "magirus-01.jpeg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Das Deutz Emblem ist ein 3D-Druck von Peter Habermehl. Auch viele Kleinteile (Abdeckungen von Winkelsteinen, Baustein 5, abgerundete Grundbausteine) sind von ihm entworfen und gedruckt worden.