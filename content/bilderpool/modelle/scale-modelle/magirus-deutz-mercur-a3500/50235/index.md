---
layout: "image"
title: "Verladekran"
date: 2024-01-04T12:19:31+01:00
picture: "magirus-19.jpeg"
weight: "19"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Verladekran ist durch die Linearantriebe erst möglich geworden. Sie sind einfach zu verbauen und haben eine eingebaute Endabschaltung.