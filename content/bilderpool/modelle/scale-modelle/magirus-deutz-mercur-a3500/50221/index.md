---
layout: "image"
title: "Ansicht von vorne links"
date: 2024-01-04T12:19:15+01:00
picture: "magirus-04.jpeg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Verbaut habe ich: 2 Powermotoren 20:1, 2 Linearantriebe 50mm Aushub, 3 mit 100mm, zwei mit 30 mm und einen M-Motor mit Winkelgetriebe von fischertechnik für den Drehkranz des Krans.