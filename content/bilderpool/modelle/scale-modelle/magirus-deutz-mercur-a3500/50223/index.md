---
layout: "image"
title: "Vorderansicht"
date: 2024-01-04T12:19:17+01:00
picture: "magirus-03.jpeg"
weight: "3"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

