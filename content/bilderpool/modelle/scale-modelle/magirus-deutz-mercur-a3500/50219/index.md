---
layout: "image"
title: "Unter der Motorhaube"
date: 2024-01-04T12:19:12+01:00
picture: "magirus-06.jpeg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Bluetooth-Empfänger"]
uploadBy: "Website-Team"
license: "unknown"
---

Ebenfalls sind zwei Bluetooth-Empfänger unter der Motorhaube verbaut, die über einen blauen Bluetooth-Sender betätigt werden.