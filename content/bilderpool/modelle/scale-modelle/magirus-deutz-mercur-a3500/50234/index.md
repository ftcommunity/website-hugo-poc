---
layout: "image"
title: "Vorderansicht"
date: 2024-01-04T12:19:30+01:00
picture: "magirus-02.jpeg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Auch von vorn einfach ein schönes Fahrzeug: der Eckhauber von Magirus Deutz.