---
layout: "image"
title: "Rad"
date: 2024-01-04T12:19:42+01:00
picture: "magirus-10.jpeg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Bei der Lenkung habe ich einen Linearantrieb eingesetzt. Bei den großen Rädern und einem Gewicht des Fahrzeugs von 4,6 kg ist das die Lösung schlechthin, funktioniert immer.