---
layout: "image"
title: "Akkus"
date: 2024-01-04T12:19:33+01:00
picture: "magirus-18.jpeg"
weight: "18"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Zwei fischertechnik-Akkus haben rechts und links vom Kran ihren Platz gefunden.