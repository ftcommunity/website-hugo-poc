---
layout: "image"
title: "Bedienpult"
date: 2024-01-04T12:19:22+01:00
picture: "magirus-26.jpeg"
weight: "26"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Die hier sichtbaren Stecker sollen die Hebelbedienungen darstellen.