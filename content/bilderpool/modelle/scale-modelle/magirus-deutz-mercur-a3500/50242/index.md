---
layout: "image"
title: "Kipper"
date: 2024-01-04T12:19:40+01:00
picture: "magirus-12.jpeg"
weight: "12"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

