---
layout: "image"
title: "Hebewerkzeug"
date: 2024-01-04T12:19:21+01:00
picture: "magirus-27.jpeg"
weight: "27"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

In dem aufgebauten halben Batterie-Kästchen befindet sich eine handelsübliche Kette, an der ich vier fischertechnik-Haken befestigt habe und die als Hebewerkzeug dient.