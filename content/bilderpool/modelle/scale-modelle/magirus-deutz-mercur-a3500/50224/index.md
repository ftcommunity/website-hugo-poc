---
layout: "image"
title: "Führerhaus"
date: 2024-01-04T12:19:18+01:00
picture: "magirus-29.jpeg"
weight: "29"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Pneumatikschlauch"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Lenkrad ist aus einem Pneumatik-Schlauch schwarz gebaut und an den Enden mit Sekundenkleber verklebt. Da dieser Schlauch 4 mm hat, passt er einwandfrei in die Nut des Baustein 7,5.