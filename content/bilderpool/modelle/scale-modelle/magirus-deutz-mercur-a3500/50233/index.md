---
layout: "image"
title: "Verladekran"
date: 2024-01-04T12:19:29+01:00
picture: "magirus-20.jpeg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

