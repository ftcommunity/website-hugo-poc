---
layout: "overview"
title: "Magirus Deutz Mercur A3500"
date: 2024-01-04T12:19:08+01:00
---

Ein aus fischertechnik erstelltes LKW-Modell: der Magirus Deutz Mercur A3500 mit Kipper und Verladekran, ein Zwei-Achser mit Heckantrieb im Maßstab ca. 1:10. 

Die Farbgebung habe ich wie folgt festgelegt: Untergestell und Fahrgestell rot, Aufbau gelb; eine damals nicht so seltene Zusammenstellung bei LKW allgemein. Die Reifen habe ich im RC Modellbau besorgt und auf Drehscheiben von fischertechnik aufgezogen. Die Drehscheibe ist noch mit verschiedenen fischertechnik-Teilen zu einer alten Stahlfelge optisch verfeinert. Verwendung fanden natürlich die Linearantriebe, die gerade bei diesem Modell ihren Platz haben. 

Der LKW passt nicht nur im Maßstab zu dem schon bekannten Hydraulik-Bagger, wie man im Video und auf Ausstellungen im letzten Jahr sehen konnte. 

Der Produktions-Zeitraum vom Original war ab 1962 bis 1971. Der Mercur war ein oft vertretener Geselle besonders als Feuerwehr-Fahrzeug.
