---
layout: "image"
title: "Abstützung"
date: 2024-01-04T12:19:34+01:00
picture: "magirus-17.jpeg"
weight: "17"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

