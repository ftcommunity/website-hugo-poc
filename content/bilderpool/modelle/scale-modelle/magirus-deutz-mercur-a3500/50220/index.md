---
layout: "image"
title: "Seitenansicht"
date: 2024-01-04T12:19:13+01:00
picture: "magirus-05.jpeg"
weight: "5"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

