---
layout: "image"
title: "Kipper"
date: 2024-01-04T12:19:41+01:00
picture: "magirus-11.jpeg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Kipper wird mit zwei 100 mm Linerantrieben betätigt.