---
layout: "image"
title: "Türen"
date: 2024-01-04T12:19:10+01:00
picture: "magirus-08.jpeg"
weight: "8"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Die Türen kann man öffnen. Schade, dass fischertechnik noch immer keine vernünftigen Türschaniere herstellt.