---
layout: "image"
title: "Abstützung"
date: 2024-01-04T12:19:35+01:00
picture: "magirus-16.jpeg"
weight: "16"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Für die Abstützung habe ich zwei 30 mm Linerantriebe verbaut.