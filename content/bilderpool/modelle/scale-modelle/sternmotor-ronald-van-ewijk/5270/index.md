---
layout: "image"
title: "Freilauf03.JPG"
date: "2005-11-07T19:57:39"
picture: "Freilauf03.JPG"
weight: "10"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5270
- /details05da.html
imported:
- "2019"
_4images_image_id: "5270"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:57:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5270 -->
...Die Kontaktfedern klemmen in der einen Drehrichtung und sperren in der anderen: das Innen-Z30 kann IM Uhrzeigersinn frei gedreht werden, in Gegenrichtung wird die Kurbelwelle mitgenommen...
