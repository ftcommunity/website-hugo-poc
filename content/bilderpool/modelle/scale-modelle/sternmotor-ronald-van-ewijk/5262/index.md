---
layout: "image"
title: "Stern03.JPG"
date: "2005-11-07T19:35:42"
picture: "Stern03.JPG"
weight: "2"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5262
- /detailsfc39.html
imported:
- "2019"
_4images_image_id: "5262"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5262 -->
Der Power-Motor links treibt das ganze Modell an. Nach unten rechts ist ein kompletter Zylinder zu sehen.
