---
layout: "comment"
hidden: true
title: "2836"
date: "2007-03-28T18:45:52"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Da mir die Kosten für die Ketten von Anton und Peter zu teuer sind habe ich die ft Kettenglieder mit Superkleber verbunden. Bedingt durch den ft Kunststofftyp hällt die Verbindung nur bedingt. Die Kettenglieder können jederzeit durch eine gezielte Biegebeanspruchung getrennt werden. Die Teile erleiden dabei keinen Schaden und sehen weitgehend so aus wie vor dem Verkleben!
Was die sieben Achsen angeht, so sind die Achsen ca. 15 mm zu kurz. Ich muss also eine Felge auf der Achse verschrauben, die andere Felge läuft auf der Achse lose und wird durch die Buchse positioniert und gehalten. Bisher habe ich bei 360° Drehungen auf der Stelle kein Verschieben der Buchsen feststellen können.

Gruß Jürgen