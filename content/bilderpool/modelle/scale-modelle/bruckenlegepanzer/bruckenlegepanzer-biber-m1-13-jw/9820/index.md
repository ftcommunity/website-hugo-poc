---
layout: "image"
title: "Optimierung Package autom. Steuerung 001"
date: "2007-03-27T20:35:55"
picture: "Optimierung_Package_autom._Steuerung001.jpg"
weight: "6"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/9820
- /detailsb1c2.html
imported:
- "2019"
_4images_image_id: "9820"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-03-27T20:35:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9820 -->
