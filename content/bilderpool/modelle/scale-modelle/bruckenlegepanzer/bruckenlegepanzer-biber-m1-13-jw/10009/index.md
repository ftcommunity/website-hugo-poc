---
layout: "image"
title: "Stützschild voll ausfahren"
date: "2007-04-06T19:01:59"
picture: "04_Sttzschild_voll_ausfahren_4.jpg"
weight: "15"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10009
- /details8e29.html
imported:
- "2019"
_4images_image_id: "10009"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10009 -->
Stützschild voll ausfahren:
Ist der Panzer positioniert wird das Stützschild voll ausgefahren.
