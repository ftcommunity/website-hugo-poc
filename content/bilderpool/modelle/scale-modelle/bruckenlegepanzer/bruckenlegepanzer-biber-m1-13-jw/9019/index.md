---
layout: "image"
title: "Funktionsprüfung Brücke anheben 02"
date: "2007-02-14T13:21:20"
picture: "Funktion_Bruecke_anheben_4.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/9019
- /detailsfa4f.html
imported:
- "2019"
_4images_image_id: "9019"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-02-14T13:21:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9019 -->
Die Brücke ist etwa 1690 mm lang und wiegt als Tragstruktur NUR 1200 Gramm. Ob sie den Panzer tragen kann konnte ich noch nicht überprüfen. Mit einer noch zu erstellenden Styropur Verkleigung wird sie wohl max. 1400 Gramm wiegen. Sie lässt sich gut verfahren und im Hubmotor ist auch noch genügend Reserve. Leiden muss derzeit nur das Gesamtgewicht des Panzers. So wie im Original übrigens auch.
