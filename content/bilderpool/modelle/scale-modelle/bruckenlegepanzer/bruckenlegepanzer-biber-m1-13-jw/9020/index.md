---
layout: "image"
title: "Funktionsprüfung Brücke anheben 03"
date: "2007-02-14T13:21:20"
picture: "Funktion_Bruecke_anheben_5.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/9020
- /details25ce-2.html
imported:
- "2019"
_4images_image_id: "9020"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-02-14T13:21:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9020 -->
Bedingt durch den absoluten Leichtbau bei größtmöglicher Steifigkeit und Festigkeit musste ich auf Alu Profile ausweichen. Die Aluprohile findet Ihr im Baumarkt (Firma ALFA). Weitere nicht ft Teile sind M4 Gewindespindeln und Senkschrauben zur Befestigung von ft an das Alu-Profil.
