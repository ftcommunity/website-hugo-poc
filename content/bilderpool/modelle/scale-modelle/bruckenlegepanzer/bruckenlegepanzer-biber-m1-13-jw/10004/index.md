---
layout: "image"
title: "Montagebeschreibung"
date: "2007-04-06T19:01:52"
picture: "00_Montagebeschreib.jpg"
weight: "10"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10004
- /details6b34-2.html
imported:
- "2019"
_4images_image_id: "10004"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10004 -->
Montagebeschreibung der beiden Brückenelemente:
Auf Wunsch einer einzelnen Person werde ich auf den nächsten Bildern beschreiben wie die Brückenelemente montiert und abgelegt werden. Die Brückenelemente selbst sind nur symbolisch dargestellt (dünne Linien), da sie im Modell noch nicht fertig sind. Auch fehlt noch eine Feinabstimmung der Hebelarme und Befestigungspunkte an den Brückenelementen. 
Ich bitte um Verständnis das ich nur eine grobe Beschreibung abgebe. Über Details können wir in Mörshausen 07 sprechen. Den automatischen Verbolzvorgang werde ich in dieser Fotofolge nicht beschrieben.
