---
layout: "image"
title: "Brücke ablegen 2"
date: "2007-04-06T19:08:57"
picture: "10_Brcke_ablegen_10_1.jpg"
weight: "22"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10016
- /details74c0.html
imported:
- "2019"
_4images_image_id: "10016"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10016 -->
Brücke ablegen:
Ist die Brücke abgelegt – Panzerfahrer und Modellbauer sind erleichtert, wird das Stützschild angehoben. Auf die Details gehe ich dabei nicht ein. 
Anschließend fährt der Panzer soweit zurück, bis die Tragrollen frei liegen und der Ausleger angehoben werden kann.
