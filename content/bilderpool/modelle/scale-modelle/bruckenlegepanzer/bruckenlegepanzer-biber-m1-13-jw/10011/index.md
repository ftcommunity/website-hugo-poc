---
layout: "image"
title: "Heckausleger absenken"
date: "2007-04-06T19:01:59"
picture: "06_Heckausleger_absenken_6.jpg"
weight: "17"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10011
- /detailse509.html
imported:
- "2019"
_4images_image_id: "10011"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10011 -->
Nun wird der Heckausleger abgesenkt . Das obere Brückenmodul befindet sich in der Verbolzposition
