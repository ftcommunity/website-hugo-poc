---
layout: "image"
title: "Prototyp_1_Schloss"
date: "2007-05-06T14:54:09"
picture: "Brueckenmodule_entkoppeln_2.jpg"
weight: "28"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/10321
- /details6dc6-2.html
imported:
- "2019"
_4images_image_id: "10321"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-05-06T14:54:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10321 -->
