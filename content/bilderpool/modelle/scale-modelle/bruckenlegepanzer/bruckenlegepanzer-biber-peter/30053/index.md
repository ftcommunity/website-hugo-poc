---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber45.jpg"
weight: "47"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30053
- /details12e1.html
imported:
- "2019"
_4images_image_id: "30053"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30053 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught
