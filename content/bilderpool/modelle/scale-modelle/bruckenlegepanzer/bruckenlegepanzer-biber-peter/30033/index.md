---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber25.jpg"
weight: "27"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30033
- /detailsb29f.html
imported:
- "2019"
_4images_image_id: "30033"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30033 -->
FT-Brückenlegepanzer-Biber mit Federung-details
