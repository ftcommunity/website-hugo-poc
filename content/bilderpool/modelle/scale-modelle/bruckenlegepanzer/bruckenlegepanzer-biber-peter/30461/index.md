---
layout: "image"
title: "Meine Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder -unten mit Kugellager"
date: "2011-04-14T21:26:00"
picture: "Brckenlegepanzer-details_018.jpg"
weight: "67"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30461
- /details6b1e-2.html
imported:
- "2019"
_4images_image_id: "30461"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-04-14T21:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30461 -->
Meine Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder -unten mit Kugellager.
Ich hoffe das Andreas T. es schaft eine schöne Löschung für die Brücken-Verbinder  aus Stahl oder Messing zu machen. Jeder Verbinder kleben und mit einer M4-Schraube + Kugellager befestigen an einer Brücketeil wäre gut sein.
Ohne Andreas wichtige Brücken-Verbinder  funktioniert die Brückenlegepanzer nicht !
Das is auch der Grund warum ich die Bruckenteilen noch nicht verklebt habe.
