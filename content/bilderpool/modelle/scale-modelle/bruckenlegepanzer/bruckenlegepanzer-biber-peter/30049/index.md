---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber41.jpg"
weight: "43"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30049
- /detailscb55-2.html
imported:
- "2019"
_4images_image_id: "30049"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30049 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught

7 Laufrollen je Seite und per Drehstab gefedert.  

Die Fischertechnik-Laufrollen haben schwarze Freilaufnaben. Die Radaufhängung und zugleich Federung besteht aus A2-RVS-Achsen die ich pro Achse 2x 90 Grad biegen muss.   
Die Drehstäbe reichen bis zur Gegenseite durch und deshalb sind wie beim Leo hier auf dem Bild auch die Aufstandsflächen der Ketten (links/rechts) unterschiedlich angeordnet - die eine Seite liegt etwas weiter hinten auf als die andere. 

Die FT-Kette hat Kufen die zwischen den Rädern 60 hindurch läuft und damit die Kette in der Spur hält. 
