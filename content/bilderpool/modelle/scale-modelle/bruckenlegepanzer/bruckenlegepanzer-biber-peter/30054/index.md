---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber46.jpg"
weight: "48"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30054
- /detailsee1d-2.html
imported:
- "2019"
_4images_image_id: "30054"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30054 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught

Ich habe selbst auch ein 1:72 Revell-Model Brückenlegepanzer Biber (03135) 
Wenn ich die Abmessungen meiner FT-Brückenlegepanzer Biber umrechne, hat dieser  ein Massstab ca. 1:10. 

Dass heisst bei einer 22m lange Aluminium Panzerschnellbrücke hat ein Fischertechnik-Brücke eine Länge ca.: 2x >1m = >2m. Extreme Kräfte gibt es dann im FT-Modell !.... 

Ich überdenke jetzt die Aluminium/Kunststoff Brücke mit Kopplungen............
