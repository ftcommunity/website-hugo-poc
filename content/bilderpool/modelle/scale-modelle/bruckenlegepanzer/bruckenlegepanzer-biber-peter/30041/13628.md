---
layout: "comment"
hidden: true
title: "13628"
date: "2011-02-18T23:49:29"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Ich überdenke noch die FT-Alu-Profilen mit der orginal-Zahnstangen zu nützen 
(1), die Mädler-Zahnstangen zu kaufen und zu kleben (2), oder eine FT-Kette wie bei deiner Brückenlegepanzer. 
Die 3 möglichkeiten haben alle Vor- und Nachteilen.

- Statt (FT-) Alu-Profilen + Kunststoffplatten überdenke ich aber auch noch: 
(FT-) Alu-Profilen + 0,5mm Aluminiumplatten

.......in die Niederlande sagt man "bezint eer je beginnt".   (Uberdenken bevor man beginnt)