---
layout: "image"
title: "Brückenpanzer Biber - 1/2"
date: "2011-01-04T21:46:43"
picture: "a1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/29606
- /detailseeec.html
imported:
- "2019"
_4images_image_id: "29606"
_4images_cat_id: "2214"
_4images_user_id: "389"
_4images_image_date: "2011-01-04T21:46:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29606 -->
Hier ist das Detailbild vom Antrieb der Kette.
