---
layout: "image"
title: "Brückenlegepanzer-Biber in Entwicklung"
date: "2011-03-13T19:56:35"
picture: "Brckenlegepanzer-Biber_003.jpg"
weight: "52"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30248
- /details2d1c.html
imported:
- "2019"
_4images_image_id: "30248"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-03-13T19:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30248 -->
Brückenlegepanzer-Biber in Entwicklung
