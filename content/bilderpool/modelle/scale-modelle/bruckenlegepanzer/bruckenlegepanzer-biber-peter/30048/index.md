---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber40.jpg"
weight: "42"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30048
- /details09ec-2.html
imported:
- "2019"
_4images_image_id: "30048"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30048 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught
