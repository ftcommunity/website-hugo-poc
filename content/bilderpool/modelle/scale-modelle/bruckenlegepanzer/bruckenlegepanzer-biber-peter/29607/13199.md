---
layout: "comment"
hidden: true
title: "13199"
date: "2011-01-15T17:04:27"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk, 

- Biegst du die neue Drehstäbe wie beim Beton-wehrung selbst ? 
  Ich nutze immer 6m-Länge  4mm A2-Edelstahl

- Wie gefällt dir die Kettenkonstruktion beim Drehen mit umgekehrte Drehrichtung ?
- Verschieben die Verbindungen dann nicht 3 bis 6mm ?

- Reicht ein Powermotor zum Antrieb einer Seite ?

- Nehmst du am Samstag 19 März 2011 deine Brückelegepanzer zum Modelshow-Europe in Ede ?!..
Schau mal unter:     www.modelshow-europe.com
 

Grüss, 

Peter 
Poederoyen NL