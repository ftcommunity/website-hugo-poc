---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber24.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30032
- /details4dee.html
imported:
- "2019"
_4images_image_id: "30032"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30032 -->
FT-Brückenlegepanzer-Biber  mit Federung-details
