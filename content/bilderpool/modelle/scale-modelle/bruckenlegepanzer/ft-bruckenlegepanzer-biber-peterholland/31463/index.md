---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber30.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31463
- /details4dbc.html
imported:
- "2019"
_4images_image_id: "31463"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31463 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Ich habe selbst auch ein 1:72 Revell-Model Brückenlegepanzer Biber (03135) 
Wenn ich die Abmessungen meiner FT-Brückenlegepanzer Biber umrechne, hat dieser  ein Massstab ca. 1:10. 
Die Machbarkeit der FT-Brückenlegepanzer-Biber mit dem 1:10 Maßstab war das Punkt !!!!!!!!............  Wegen die zu grösse Kräfte und Momenten  ist meine Aluminium Panzerschnellbrücke nur 2x 1,0 = 2,0m  

Aluminium Panzerschnellbrücke:
2 x 2,6 kg  = 5,2 kg
2 x 1 m =  2,0 m (statt 2x 1,1 = 2,2m Massstab 1:10).  

Wie im Original:  7 Laufrollen je Seite, und per Drehstab gefedert.  Die Laufrollen haben schwarze Freilaufnaben. Die FT-Kette hat Kufen die zwischen den Rädern 60 hindurch läuft und damit die Kette in der Spur hält. 
Die Radaufhängung und zugleich "Federung" besteht aus A2-RVS-Achsen die ich pro Achse 2x 90 Grad biegen muss.   Bei mir machen die stahlen Schraubenfedern (nicht-FT) die ganze Arbeit. Die Stäben gehen aber wie beim Original bis zur anderen Seite, funktionel nur damit sich die Seitenteile nicht verbiegen und vorallem die ganze FT-Konstrukion stabiler wird.  Die Drehstäbe reichen bis zur Gegenseite durch und deshalb sind wie beim Leo auch die Aufstandsflächen der Ketten (links/rechts) unterschiedlich angeordnet - die eine Seite liegt etwas weiter hinten auf als die andere. 


