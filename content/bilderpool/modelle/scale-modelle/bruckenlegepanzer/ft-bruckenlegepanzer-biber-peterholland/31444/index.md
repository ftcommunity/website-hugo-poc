---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber11.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31444
- /details1699.html
imported:
- "2019"
_4images_image_id: "31444"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31444 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Das "Hirschgeweih" das so wichtig für die Zusammenführung der beiden Segnente ist. 
Es ist so gestaltet, dass es die Laufschiene umgreift und ein Abrutschen unmöglich macht.
