---
layout: "image"
title: "Trockenlager"
date: "2017-09-30T11:52:18"
picture: "keksdrucker10.jpg"
weight: "10"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46540
- /detailsa2ac.html
imported:
- "2019"
_4images_image_id: "46540"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46540 -->
Im Trockenlager sind die Kekse solange, bis der Zuckerguß hart geworden ist. Für die Steuerung ist die Belegung des Lagers egal. Vor dem Einlagern eines neuen Keks in eine Lagerposition wird zunächst die Postion geräumt - ob nun ein Keks in der Position ist oder nicht.