---
layout: "image"
title: "Abräumbagger_03"
date: "2012-04-06T23:17:30"
picture: "abraumbagger03.jpg"
weight: "3"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34752
- /details5165.html
imported:
- "2019"
_4images_image_id: "34752"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34752 -->
Hier der Blick von vorn.