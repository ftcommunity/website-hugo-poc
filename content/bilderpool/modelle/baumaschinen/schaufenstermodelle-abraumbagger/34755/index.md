---
layout: "image"
title: "Abräumbagger_06"
date: "2012-04-06T23:17:30"
picture: "abraumbagger06.jpg"
weight: "6"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34755
- /detailsbad9.html
imported:
- "2019"
_4images_image_id: "34755"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34755 -->
Ein Blick auf den Antrieb.