---
layout: "image"
title: "Abräumbagger_14"
date: "2012-04-06T23:17:42"
picture: "abraumbagger14.jpg"
weight: "14"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34763
- /details90fe.html
imported:
- "2019"
_4images_image_id: "34763"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34763 -->
Ein weiterer Blick auf das Kettengestell und den beiden Verteilerplatten für den Strom.