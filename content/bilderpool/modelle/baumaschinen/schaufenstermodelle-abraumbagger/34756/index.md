---
layout: "image"
title: "Abräumbagger_07"
date: "2012-04-06T23:17:30"
picture: "abraumbagger07.jpg"
weight: "7"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34756
- /details7ab0.html
imported:
- "2019"
_4images_image_id: "34756"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34756 -->
Ein weiterer Blick auf den Antrieb