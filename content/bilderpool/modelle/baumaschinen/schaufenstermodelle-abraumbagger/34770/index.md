---
layout: "image"
title: "Abräumbagger_21"
date: "2012-04-06T23:17:54"
picture: "abraumbagger21.jpg"
weight: "21"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34770
- /detailscd03.html
imported:
- "2019"
_4images_image_id: "34770"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:54"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34770 -->
Ein weiterer Blick auf den Polwendeschalter.