---
layout: "image"
title: "Abräumbagger_12"
date: "2012-04-06T23:17:42"
picture: "abraumbagger12.jpg"
weight: "12"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34761
- /detailse78e.html
imported:
- "2019"
_4images_image_id: "34761"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:42"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34761 -->
Hier ist sehr gut die Statik zu erkennen.