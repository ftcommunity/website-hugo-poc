---
layout: "image"
title: "Abräumbagger_02"
date: "2012-04-06T23:17:29"
picture: "abraumbagger02.jpg"
weight: "2"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34751
- /detailsbc48.html
imported:
- "2019"
_4images_image_id: "34751"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34751 -->
Bei diesem Modell handelt es sich um das Schaufenstermodell "Abraumbagger", das zu Weihnachten 1978 in einigen wenigen Schaufenstern von Spielwarenläden zu finden war. Ich habe das Modell nur anhand von Fotos mit meinen damaligen fischertechnik-Bauteilen originalgetreu wieder aufgebaut. Der eine Minimotor treibt das Schaufelrad an. Der andere Minimotor dreht den Bagger oberhalb des Kettengestells um ca. 180 Grad in beide Richtungen. Der Polwendeschalter ändert bei Erreichen des Zielpunktes jeweils die Drehrichtung. Durch die Seilwinden kann der Ausleger auf- und abgesenkt werden.
