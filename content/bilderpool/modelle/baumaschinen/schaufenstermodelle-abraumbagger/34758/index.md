---
layout: "image"
title: "Abräumbagger_09"
date: "2012-04-06T23:17:30"
picture: "abraumbagger09.jpg"
weight: "9"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34758
- /details7dc7.html
imported:
- "2019"
_4images_image_id: "34758"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34758 -->
Ein Blick auf die rechte Seite.