---
layout: "image"
title: "pneumatischerkipper08.jpg"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25641
- /details5e4a.html
imported:
- "2019"
_4images_image_id: "25641"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25641 -->
