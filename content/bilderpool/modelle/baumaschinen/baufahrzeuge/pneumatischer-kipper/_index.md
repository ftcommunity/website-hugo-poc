---
layout: "overview"
title: "pneumatischer Kipper"
date: 2020-02-22T08:12:59+01:00
legacy_id:
- /php/categories/1801
- /categoriesfeba.html
- /categories6282.html
- /categories6529.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1801 --> 
Die Laderampe kann pneumatisch gekippt werden. Außerdem hat er eine einfache Federung und ist fernsteuerbar. Ich habe die verkleinerte Version meines Powerkompressors genommen.