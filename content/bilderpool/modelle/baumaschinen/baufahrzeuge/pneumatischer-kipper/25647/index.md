---
layout: "image"
title: "Lenkung und Federung"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper14.jpg"
weight: "14"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25647
- /detailsfe09-3.html
imported:
- "2019"
_4images_image_id: "25647"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25647 -->
Das ist eine einfache Federung.