---
layout: "image"
title: "Frontansicht"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper09.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25642
- /detailse8d1.html
imported:
- "2019"
_4images_image_id: "25642"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25642 -->
