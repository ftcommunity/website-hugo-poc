---
layout: "image"
title: "Federung"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper15.jpg"
weight: "15"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25648
- /details5235-3.html
imported:
- "2019"
_4images_image_id: "25648"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25648 -->
Hier die ganze Federung.