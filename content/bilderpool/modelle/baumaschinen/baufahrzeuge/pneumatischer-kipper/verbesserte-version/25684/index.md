---
layout: "image"
title: "Detail"
date: "2009-11-07T20:23:46"
picture: "verbesserteversion03.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25684
- /detailsb4d5.html
imported:
- "2019"
_4images_image_id: "25684"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25684 -->
Die Federung ist jetzt etwas härter,.