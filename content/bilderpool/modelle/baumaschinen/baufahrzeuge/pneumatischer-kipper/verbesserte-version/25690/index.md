---
layout: "image"
title: "Klappe"
date: "2009-11-07T20:23:46"
picture: "verbesserteversion09.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25690
- /details8082.html
imported:
- "2019"
_4images_image_id: "25690"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25690 -->
