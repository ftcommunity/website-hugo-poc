---
layout: "image"
title: "Feder"
date: "2009-11-07T20:23:46"
picture: "verbesserteversion04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25685
- /detailsbf31.html
imported:
- "2019"
_4images_image_id: "25685"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25685 -->
Hier die Feder.