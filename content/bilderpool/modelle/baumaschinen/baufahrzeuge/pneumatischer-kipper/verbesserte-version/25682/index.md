---
layout: "image"
title: "Gesamtansicht"
date: "2009-11-07T20:23:45"
picture: "verbesserteversion01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25682
- /details05f9.html
imported:
- "2019"
_4images_image_id: "25682"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25682 -->
Wie man sieht ist die Vorderachse jetzt weiter hinten.