---
layout: "image"
title: "centraal draaipunt"
date: "2013-02-21T18:56:48"
picture: "P2180005.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/36657
- /details9816.html
imported:
- "2019"
_4images_image_id: "36657"
_4images_cat_id: "2718"
_4images_user_id: "838"
_4images_image_date: "2013-02-21T18:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36657 -->
Model kan volledig rond draaien om het draaipunt