---
layout: "image"
title: "kraan"
date: "2011-06-20T21:57:11"
picture: "TD24_014.jpg"
weight: "14"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30881
- /details7fba.html
imported:
- "2019"
_4images_image_id: "30881"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-20T21:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30881 -->
Bovenkant kraanarm