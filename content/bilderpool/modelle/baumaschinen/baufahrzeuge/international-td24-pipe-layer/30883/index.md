---
layout: "image"
title: "kraan"
date: "2011-06-20T21:57:11"
picture: "TD24_016.jpg"
weight: "16"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30883
- /detailsdb20.html
imported:
- "2019"
_4images_image_id: "30883"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-20T21:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30883 -->
Kraanarm uit en contra gewicht ook