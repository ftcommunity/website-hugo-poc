---
layout: "image"
title: "Mulde von vorne"
date: "2014-08-08T21:21:23"
picture: "dumper11.jpg"
weight: "11"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39177
- /details3b4a-2.html
imported:
- "2019"
_4images_image_id: "39177"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39177 -->
vom Fahrgestell abmontierte Mulde
