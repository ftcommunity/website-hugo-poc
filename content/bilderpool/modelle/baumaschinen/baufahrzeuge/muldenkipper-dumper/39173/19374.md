---
layout: "comment"
hidden: true
title: "19374"
date: "2014-08-10T09:56:15"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Genau, ich habe vier Rastzahnräder 10 benutzt. Die Gelenke halten ziemlich gut, beim Fahren verschieben sie sich nicht.
Die einzige Ausnahme ist das Herumtragen: Hier muss man das Modell am Gelenk stützen, sodass sich die Gelenksteine nicht verschieben.