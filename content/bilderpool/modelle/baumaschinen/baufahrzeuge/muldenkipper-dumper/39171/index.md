---
layout: "image"
title: "Ansicht von oben"
date: "2014-08-08T21:21:23"
picture: "dumper05.jpg"
weight: "5"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39171
- /details7602.html
imported:
- "2019"
_4images_image_id: "39171"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39171 -->
Führerhaus und Mulde von oben
