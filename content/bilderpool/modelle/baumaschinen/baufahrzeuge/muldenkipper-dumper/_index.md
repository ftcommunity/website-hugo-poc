---
layout: "overview"
title: "Muldenkipper 'Dumper'"
date: 2020-02-22T08:13:08+01:00
legacy_id:
- /php/categories/2929
- /categoriesf548.html
- /categoriesb1fc.html
- /categories5e7a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2929 --> 
Dies ist mein Muldenkipper oder auf Englisch mein "Dumper". Der ein oder andere kennt das Modell vielleicht schon von der Modellaustellung des ft-Fanclubtags 2014. Sowohl das Design als auch technische Funktionen wie der Allradantrieb oder die pneumatische (naja in echt hydraulische) Lenkung orientieren sich an dem Volvo Muldenkipper A30F.