---
layout: "image"
title: "Seitenansicht"
date: "2014-08-08T21:21:23"
picture: "dumper02.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39168
- /detailsa0eb.html
imported:
- "2019"
_4images_image_id: "39168"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39168 -->
Mein "Lieblingsdetail" des Modells: die Treppe zum Führerhaus
