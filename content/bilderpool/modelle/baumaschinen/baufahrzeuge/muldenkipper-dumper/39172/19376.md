---
layout: "comment"
hidden: true
title: "19376"
date: "2014-08-10T10:18:37"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ich selbst hatte am Anfang auch Zweifel mit dem Kippmechanismus. Dennoch baut der fischertechnik Kompressor genug Druck auf, um die Mulde (mitsamt ein paar ft-Steinen) anzuheben.

Die Kombination aus einem roten Zylinder mit Federung und einem blaue ohne Federung hat genau den richtigen Widerstand, sodass die Mulde durch ihr Eigengewich nach unten gedrückt wird, aber nich absackt.

Gruß, David