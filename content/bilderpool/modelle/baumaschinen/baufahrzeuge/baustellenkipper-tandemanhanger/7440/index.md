---
layout: "image"
title: "Gesamtansicht"
date: "2006-11-06T21:30:33"
picture: "kipper3.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/7440
- /details5f82.html
imported:
- "2019"
_4images_image_id: "7440"
_4images_cat_id: "140"
_4images_user_id: "6"
_4images_image_date: "2006-11-06T21:30:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7440 -->
Der LKW bekommt eine Kipperwanne die mit einem pneumatischen Teleskopzylinder angehoben wird. Das Getriebe braucht er, da es noch einen Tieflader geben wird, den er ziehen muss,