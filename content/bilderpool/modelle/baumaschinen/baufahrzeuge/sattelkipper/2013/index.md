---
layout: "image"
title: "Zweiter Versuch"
date: "2003-12-07T19:10:29"
picture: "IMG_0449.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2013
- /detailsb7c2.html
imported:
- "2019"
_4images_image_id: "2013"
_4images_cat_id: "145"
_4images_user_id: "6"
_4images_image_date: "2003-12-07T19:10:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2013 -->
So kann man das Getriebe schalten wenn man keinen Servo zur Verfügung hat.