---
layout: "image"
title: "Kipperpumpe unter dem Fahrerhaus"
date: "2003-08-24T14:22:08"
picture: "IMG_0382.jpg"
weight: "10"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/1346
- /details4fdf-2.html
imported:
- "2019"
_4images_image_id: "1346"
_4images_cat_id: "145"
_4images_user_id: "6"
_4images_image_date: "2003-08-24T14:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1346 -->
