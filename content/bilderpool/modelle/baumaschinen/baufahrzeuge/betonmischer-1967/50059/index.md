---
layout: "image"
title: "Der Nachbau"
date: 2023-06-18T23:13:09+02:00
picture: "2023-01-22_Betonmischer_1967_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alles, was oben sichtbar ist, ist offensichtlich und leicht nachzubauen. 
Die Metallachsen im Fahrzeugrahmen habe ich zwecks Stabilität ergänzt - meine grauen Bausteine sind nach so vielen Jahrzehnten recht ausgeleiert. Auf die schicken glänzenden Verkleidungen der Mischtrommeln habe ich verzichtet. Man beachte aber den fehlenden Winkelstein 30° beim Führerhaus - ganz wie im Original ;-)