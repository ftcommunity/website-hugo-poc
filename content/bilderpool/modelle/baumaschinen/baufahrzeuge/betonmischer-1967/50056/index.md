---
layout: "image"
title: "Trommelaufhängung"
date: 2023-06-18T23:13:05+02:00
picture: "2023-01-22_Betonmischer_1967_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mehr Probiererei war bei der Aufhängung der Mischtrommel notwendig. Ich bin sicher, sie war zweifach gelagert. Nur in dem von oben sichtbaren Baustein 15 gelagert hätte sie sich brutal verkantet und wäre enorm schwergängig gewesen.