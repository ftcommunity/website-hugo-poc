---
layout: "image"
title: "Gestell für Ausstellungen"
date: 2023-06-18T23:13:03+02:00
picture: "2023-01-22_Betonmischer_1967_7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Für Ausstellungen habe ich dieses Gestell gebaut. Den Betonmischer kann man oben aufsetzen und er fällt auch beim Wackeln nicht herunter. Die beiden Schalter steuern die Stromzufuhr für Fahrantrieb und Trommeldrehung.