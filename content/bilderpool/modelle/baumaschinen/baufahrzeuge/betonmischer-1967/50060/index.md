---
layout: "image"
title: "Das Original"
date: 2023-06-18T23:13:10+02:00
picture: "2023-01-22_Betonmischer_1967_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses eine Bild gibt es in der Ur-fischertechnik-Anleitung von 1967. Das Modell wollte ich schon immer  mal nachbauen.

Die Frage: Wie ist das unten gemacht, wo man nicht hinsehen kann?