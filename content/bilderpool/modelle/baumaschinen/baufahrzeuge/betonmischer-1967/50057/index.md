---
layout: "image"
title: "Fahrantrieb"
date: 2023-06-18T23:13:06+02:00
picture: "2023-01-22_Betonmischer_1967_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb ist noch einfach und auch mit nur dem Originalfoto gut vorzustellen.