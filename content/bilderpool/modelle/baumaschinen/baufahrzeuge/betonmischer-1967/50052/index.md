---
layout: "image"
title: "Betonmischer auf dem Gestell (2)"
date: 2023-06-18T23:13:00+02:00
picture: "2023-01-22_Betonmischer_1967_9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine weitere Ansicht.