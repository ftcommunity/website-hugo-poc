---
layout: "image"
title: "Die Unterseite"
date: 2023-06-18T23:13:07+02:00
picture: "2023-01-22_Betonmischer_1967_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier liegt das Modell auf dem Kopf und man sieht, wie ich die Unterseite gebaut habe. Da ist natürlich Spekulation und Vermutung drin - evtl. war es auch ganz anders.