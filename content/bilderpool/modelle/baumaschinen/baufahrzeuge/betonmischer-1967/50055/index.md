---
layout: "image"
title: "Stoßstange"
date: 2023-06-18T23:13:04+02:00
picture: "2023-01-22_Betonmischer_1967_6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Stoßstange - das unterste Element beim Führerhaus - war etwas knifflig. Man bedenke:

- Die Ur-Version des Winkelsteins 30° war kleiner als die heutige.
- Es gab damals noch nicht mal einen Verbinder 15 (nur 30), von Federnocken ganz zu schweigen.
- Auch Bausteine 5 und 7,5 gab es natürlich noch nicht.

Mit dieser Bauart sieht es von oben aus wie auf dem Originalfoto, und es passt alles, ohne nennenswert üble Verbiegungen zu benötigen.