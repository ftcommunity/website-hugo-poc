---
layout: "image"
title: "Betonmischer auf dem Gestell"
date: 2023-06-18T23:13:01+02:00
picture: "2023-01-22_Betonmischer_1967_8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Gestell dient dazu, dass man eben auch von unten unters Modell schauen kann, und dass der Fahrmotor laufen kann, ohne dass das Fahrzeug abhaut ;-)