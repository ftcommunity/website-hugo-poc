---
layout: "image"
title: "Losschuif"
date: "2011-02-13T17:51:44"
picture: "pivot_018.jpg"
weight: "17"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29970
- /details8b33.html
imported:
- "2019"
_4images_image_id: "29970"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29970 -->
Losschuif halverwege terug