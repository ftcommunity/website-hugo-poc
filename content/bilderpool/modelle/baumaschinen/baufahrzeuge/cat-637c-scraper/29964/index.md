---
layout: "image"
title: "Losschuif"
date: "2011-02-13T17:51:43"
picture: "pivot_012.jpg"
weight: "11"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29964
- /detailscf94.html
imported:
- "2019"
_4images_image_id: "29964"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29964 -->
XM motor zorgt voor het heen en weer schuiven van de losschuif