---
layout: "image"
title: "Vooraanzicht"
date: "2011-02-14T14:45:47"
picture: "pivot_030.jpg"
weight: "29"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29985
- /details0037-2.html
imported:
- "2019"
_4images_image_id: "29985"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-14T14:45:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29985 -->
Motorkap plus cabine