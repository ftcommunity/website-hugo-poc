---
layout: "comment"
hidden: true
title: "13573"
date: "2011-02-15T21:50:41"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Hallo Ruurd, 

Dank voor je foto's! Ik had niet gedacht dat een minimot deze krachten kon leveren! Hm, interessant en inspirerend.

Ik ben zelf bezig met een shovel met knikbesturing, 4 wiel aandrijving, pendelas achter en een werkende bakconstructie voor, maar gebruik voor de knik aandrijving toch echt de "gewone" grijze motor. De aandrijving moet worden verzorgd door een powermotor, ik ben er nog niet uit of het een 1:50 of 1:20 overbrenging moet worden. 
Proefondervindelijk moet blijken of ik de kunststof aandrijfelementen met "klik bevestiging" ga gebruiken of toch met de metalen assen en klemverbindingen ga werken. In een eerder model slipten de tandwielen over de metalen assen :(
Als het model af is zal ik proberen de foto s hier te posten. Dit kan nog even duren echter.

groet, Marten

Hallo Ruurd,  
Danke fur die Bilder. Ich hatte nie gedacht dass ein Minimot soviel Kraft hätte. Hmm... Interessant und inspirierend.
Ich beschäftige mich jetzt mit der Bau eines Shovels mit Knicklenkung, Allradantrieb, Pendelasche hinten und voll funktionierendes Hebeteil vorne. Ich benutze aber die alte graue "grosse Motor" zum Knicken. Antriebsmotor muss ein Powermotor werden, ich bin noch nicht
sicher ob es ein 1:50 oder 1:20 Getriebe werden muss.
Experimentell muss sich beweisen ob ich diese plastik Rastaschen verwenden werde, oder doch die Metalaschen mit Klemmverbindungen. In einem fruheren Modell  rutshten die Zahnräder uber die Metallaschen :(
Wenn ich das Modell vollendet habe, werde ich versuchen die Bilder hier hoch zu laden, es wird aber noch einige Zeit daueren.
mfrgr Marten