---
layout: "image"
title: "Losschuif"
date: "2011-02-13T17:51:44"
picture: "pivot_017.jpg"
weight: "16"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/29969
- /details19f6.html
imported:
- "2019"
_4images_image_id: "29969"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29969 -->
Losschuif in de voorste stand bak is leeg gedrukt