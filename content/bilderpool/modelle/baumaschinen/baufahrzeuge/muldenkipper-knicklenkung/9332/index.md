---
layout: "image"
title: "Bell_B30D_11.JPG"
date: "2007-03-06T20:05:50"
picture: "Bell_B30D_11.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9332
- /detailscd3a.html
imported:
- "2019"
_4images_image_id: "9332"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T20:05:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9332 -->
Die Hinterachsen haben ein gemeinsames Differenzial mitten drin. Dieses Differenzial stellt auch die Schwenkachse der beiden Träger links/rechts dar. Die Differenzialachse hört nach dem Klemm-Z10, aber noch innerhalb des BS15-Loch auf, der hier zwischen den beiden Z40 und unter dem BS15 verdeckt ist. Von außen ist ein Lagerbock (?, ähnlich dem Radhalter 35668, aber länger) aufgeschoben, dessen "Knubbel" das Gewicht mitträgt.
