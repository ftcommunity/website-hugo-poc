---
layout: "image"
title: "Bell_B30D_01.JPG"
date: "2007-03-06T19:54:32"
picture: "Bell_B30D_01.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad", "Knicklenkung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9328
- /details81b0.html
imported:
- "2019"
_4images_image_id: "9328"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:54:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9328 -->
Das ist mein Versuch, den B30D nachzubilden. Es ist kein "Scale-Modell", weil das eine oder andere wirklich nicht mit dem Original übereinstimmt.
