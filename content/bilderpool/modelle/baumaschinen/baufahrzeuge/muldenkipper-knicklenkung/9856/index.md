---
layout: "image"
title: "Bell_B30D_29.JPG"
date: "2007-03-30T17:20:56"
picture: "Bell_B30D_29.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9856
- /details3d84.html
imported:
- "2019"
_4images_image_id: "9856"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-30T17:20:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9856 -->
