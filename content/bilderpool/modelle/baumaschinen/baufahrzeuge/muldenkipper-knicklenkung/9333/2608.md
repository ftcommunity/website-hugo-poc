---
layout: "comment"
hidden: true
title: "2608"
date: "2007-03-07T09:51:45"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
und oft genug ist eine Kröte dazwischen.

Respekt. Richtig großer Maschinenbau.

Bei derart verzwickten Konstruktionen mache ich den Entwurf oft mehrmals, um die unterschiedlichen Konstruktionen nebeneinander legen zu können und beim Neubau auch den Vorgänger im Auge behalten zu können.

Nur wenn dann ein Teil fehlt, wirds ärgerlich.

Viel Erfolg weiterhin
Remadus