---
layout: "image"
title: "Bell_B30D_03.JPG"
date: "2007-03-06T19:54:50"
picture: "Bell_B30D_03.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9329
- /details58d1.html
imported:
- "2019"
_4images_image_id: "9329"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:54:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9329 -->
