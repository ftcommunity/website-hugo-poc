---
layout: "image"
title: "Bell_B30D_17.JPG"
date: "2007-03-06T20:16:29"
picture: "Bell_B30D_17.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9334
- /details54b6.html
imported:
- "2019"
_4images_image_id: "9334"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T20:16:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9334 -->
Die Haube von innen gesehen.
