---
layout: "image"
title: "Bell-B30D-52.JPG"
date: "2007-03-06T19:52:19"
picture: "Bell-B30D-52.JPG"
weight: "4"
konstrukteure: 
- "Fa. Bell"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9327
- /detailsc943.html
imported:
- "2019"
_4images_image_id: "9327"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:52:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9327 -->
Das gute Stück von schräg hinten. Die rechte Auspufföffnung ist das schwarze Loch in der Versteifungsrippe oberhalb vom Kipplager der Mulde.
