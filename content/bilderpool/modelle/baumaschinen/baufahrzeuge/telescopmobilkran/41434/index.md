---
layout: "image"
title: "Telesc-69-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran37.jpg"
weight: "37"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41434
- /details3c7a-2.html
imported:
- "2019"
_4images_image_id: "41434"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41434 -->
