---
layout: "image"
title: "Telesc-32-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran20.jpg"
weight: "20"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41417
- /detailse5bb.html
imported:
- "2019"
_4images_image_id: "41417"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41417 -->
