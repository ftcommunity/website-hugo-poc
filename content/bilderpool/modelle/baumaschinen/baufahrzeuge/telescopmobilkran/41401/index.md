---
layout: "image"
title: "Telesc-06-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41401
- /details258d.html
imported:
- "2019"
_4images_image_id: "41401"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41401 -->
