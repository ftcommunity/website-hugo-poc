---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0918.jpg"
weight: "17"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/46014
- /details217c.html
imported:
- "2019"
_4images_image_id: "46014"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46014 -->
Draaikrans verwijdert d.m.v. het verwijderen van 4 pennen.