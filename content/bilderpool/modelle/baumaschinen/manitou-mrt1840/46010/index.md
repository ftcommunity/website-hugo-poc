---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0913.jpg"
weight: "13"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/46010
- /details9da6.html
imported:
- "2019"
_4images_image_id: "46010"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46010 -->
De kat is voor de schaal van de manitou.
58 cm lang
33 cm breed
120 cm maximale hoogte