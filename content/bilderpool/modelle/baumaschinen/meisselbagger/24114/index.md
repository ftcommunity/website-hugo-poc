---
layout: "image"
title: "hochgefahrener Arm"
date: "2009-05-27T18:14:03"
picture: "meiselbagger10.jpg"
weight: "10"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24114
- /detailse4a4-3.html
imported:
- "2019"
_4images_image_id: "24114"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24114 -->
