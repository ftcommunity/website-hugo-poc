---
layout: "image"
title: "in Betrieb"
date: "2009-05-27T18:14:03"
picture: "meiselbagger13.jpg"
weight: "13"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24117
- /details93ea.html
imported:
- "2019"
_4images_image_id: "24117"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:03"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24117 -->
Der Meisel kann auch Wände meiseln.