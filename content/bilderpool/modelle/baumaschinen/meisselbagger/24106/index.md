---
layout: "image"
title: "Führerhaus"
date: "2009-05-27T18:14:02"
picture: "meiselbagger02.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24106
- /details2577.html
imported:
- "2019"
_4images_image_id: "24106"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24106 -->
