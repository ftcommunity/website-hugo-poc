---
layout: "image"
title: "in Betrieb"
date: "2009-05-27T18:14:03"
picture: "meiselbagger14.jpg"
weight: "14"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24118
- /detailse381.html
imported:
- "2019"
_4images_image_id: "24118"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:03"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24118 -->
