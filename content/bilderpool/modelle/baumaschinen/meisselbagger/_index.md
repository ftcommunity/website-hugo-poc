---
layout: "overview"
title: "Meißelbagger"
date: 2020-02-22T08:13:27+01:00
legacy_id:
- /php/categories/1653
- /categoriesd305.html
- /categories68ba.html
- /categoriesc86c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1653 --> 
Dieses Gerät ist mit einem riesigen Meißel ausgestattet. Der Arm ist ähnlich wie im Pneumatik-2-Baukasten und pneumatisch gesteuert. Der Meißel wird durch einen Exzenter angetrieben. Mit so einem Gerät werden Straßen aufgebrochen.