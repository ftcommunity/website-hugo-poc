---
layout: "image"
title: "Der Empfänger"
date: "2011-02-03T19:51:17"
picture: "lkwmittiefladerundgabelstabler12.jpg"
weight: "12"
konstrukteure: 
- "Tim_Fischer"
fotografen:
- "Tim_Fischer"
uploadBy: "Tim_Fischer"
license: "unknown"
legacy_id:
- /php/details/29853
- /details2381-2.html
imported:
- "2019"
_4images_image_id: "29853"
_4images_cat_id: "2198"
_4images_user_id: "1244"
_4images_image_date: "2011-02-03T19:51:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29853 -->
