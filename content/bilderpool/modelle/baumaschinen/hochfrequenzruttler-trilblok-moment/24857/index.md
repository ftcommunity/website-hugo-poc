---
layout: "image"
title: "Fischertechnik Trilblok met instelbaar variabel excentrisch moment."
date: "2009-08-30T09:28:51"
picture: "fttrilblokmetinstelbaarvariabelexcentrischmoment09.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24857
- /details7961.html
imported:
- "2019"
_4images_image_id: "24857"
_4images_cat_id: "1708"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24857 -->
Trilblok met instelbaar variabel excentrisch moment.

Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.

Voordelen: 

minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk

Der "Trick des System" des variablen Moments ist ein Diffential.   
Auch mit Fischertechnik: 2x  2st-Kugelgelager an beiden Seiten, sonst funktioniert es nicht !
