---
layout: "image"
title: "Fischertechnik Trilblok met instelbaar variabel excentrisch moment."
date: "2009-08-30T09:28:50"
picture: "fttrilblokmetinstelbaarvariabelexcentrischmoment03.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24851
- /details6172.html
imported:
- "2019"
_4images_image_id: "24851"
_4images_cat_id: "1708"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24851 -->
Trilblok met instelbaar variabel excentrisch moment.

Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.

Voordelen: 

minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk
