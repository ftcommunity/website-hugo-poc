---
layout: "image"
title: "Greifer5"
date: "2006-05-07T15:16:33"
picture: "IMG_4580.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6229
- /details78ea.html
imported:
- "2019"
_4images_image_id: "6229"
_4images_cat_id: "543"
_4images_user_id: "389"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6229 -->
