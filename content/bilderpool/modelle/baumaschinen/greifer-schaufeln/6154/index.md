---
layout: "image"
title: "Greifer"
date: "2006-04-27T13:15:47"
picture: "IMG_4560.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
schlagworte: ["Greifer", "elektrischer", "Seilbagger", "Bagger", "Seilgreifer", "Guilligan"]
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6154
- /details0d31.html
imported:
- "2019"
_4images_image_id: "6154"
_4images_cat_id: "543"
_4images_user_id: "389"
_4images_image_date: "2006-04-27T13:15:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6154 -->
