---
layout: "overview"
title: "Raupenbagger 2. Version"
date: 2020-02-22T08:13:38+01:00
legacy_id:
- /php/categories/2922
- /categories8ec2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2922 --> 
Mein erster Entwurf war ja schon ganz ok, hatte aber noch ein paar Mängel.
Basierend auf den Erfahrungen habe ich fast alles neu konstruiert.
Leider fehlt es momentan an der Zeit, so daß der aktuelle Stand schon ca. 2 Monate im Schrank liegt und auf die Vollendung wartet.
Ich will jetzt trotzdem mal anfangen, die ersten Bilder zu zeigen. Im Detail wird sich noch das Eine oder Andere ändern, Irgendwie ist ein Modell ja nie ganz fertig.