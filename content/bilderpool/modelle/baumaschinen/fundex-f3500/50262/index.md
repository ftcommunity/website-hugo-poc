---
layout: "image"
title: "F3500 Mast"
date: 2024-01-26T18:54:59+01:00
picture: "FundexF3500-12.jpg"
weight: "12"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Teil war siemlich schwer zu bauen. Es musste klein sein, doch musste auch 3 Gewinde drauf gebaut werden und den Makler sollte durch diese Mast gleiten konnen. Das geht nur mit Aluminum damit mann ein Stabilen bauteil machen kann.
Dieser Mast hat viele funktionen:
- Es gibt ein Zylinder um denn Winkel vom Makler zum Bodem/Maschine zu verstellen. Auch ist diese verstellung notwendig um ins Transportmodus zu kommen.
- Winde fur denn Aufzug
- Winde fur dass heben der Makler vom Boden und verschiedene umlaufscheiben fur die Kabel
- Winde fur Kran