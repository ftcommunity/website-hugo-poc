---
layout: "overview"
title: "Fundex F3500"
date: 2024-01-26T18:54:48+01:00
---

Dies ist ein Modell von ein Fundex F3500 Pfahlbohrrmachine (Vollverdrängungsbohrpfahl-System).
Scale 1:10.
24 funktionen sind eingbaut:

1: Fahren Links
2: Fahren Rechts
3: Spurbreiteverstellung
4: Oberwagen drehen
5: Stutz vorne links hoch/runter
6: Stutz vorne rechts hoch/runter
7: Stutz vorne links schwenken
8: Stutz vorne rechts schwenken
9: Stutz hinten rechts hoch/runter
10: Stutz hinte links hoch/runter
11: Oberwagen/mast aufrechten
12: positionierung Mast/Ausleger
13: Aufzug/Lift
14: Kran schwenken
15: Kranarm heben/senken
16: Kran last heben/senken
17: Auxiliary hoist hebe function
18: Mast/Führungssystem vom bodem heben
19: Bohrtischl heben
20: Bohrtisch senken
21: Führungssystem wagerecht ausschieben von Oberwagen
22: Bohrtisch drehen
23: Cabine schwenken
24: Cabine senken/aufrichten

Hohe von dieses model ca 2.5 meter
In wirklichkeit kann diese Machine bis zum 40 meter Hoch gebaut werden.