---
layout: "image"
title: "F3500 Kopf"
date: 2024-01-26T18:54:58+01:00
picture: "FundexF3500-13.jpg"
weight: "13"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Der Kopf kommt oben auf den Makler und hat verschiedene funktionen.
- Oben steht ein kleiner Kran um zum beispiel Bewehrung zu heben.
- An den Linkerseite 2 Umlaufscheiben fur den Aufzug
- Das teil ist hohl um die Kabel durch zu fuhren fur Bohrtisch heben und ein draht fur denn auxiliary Winde