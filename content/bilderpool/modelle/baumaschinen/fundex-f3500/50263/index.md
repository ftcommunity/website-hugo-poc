---
layout: "image"
title: "F3500 Makler"
date: 2024-01-26T18:55:00+01:00
picture: "FundexF3500-11.jpg"
weight: "11"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Der Makler ist gebaut aus 112 stuck U-Träger schwarz, 150mm. Die habe ich in ein Viereck verbaut damit ich ein 75x75mm Makler hatte. 
An der Vorderseite ist ein doppelte Schiene eingebaut fur den Bohrtisch. An der seite ist ein einzele schiene gebaut fur den Aufzug.
An der oberseite kommt Kopf mit ein kleiner Kran und scheiben um die Kabel zum Bohrtisch zu begleiten.
An der unterseite eine Klamme fur den Pfahl, und auch Scheiben wodurch ein Draht kommt um denn kompletten Makler von Boden zu heben (Makler schiebt in der Mast).