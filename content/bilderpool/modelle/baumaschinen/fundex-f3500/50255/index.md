---
layout: "image"
title: "F3500 Raupen"
date: 2024-01-26T18:54:51+01:00
picture: "FundexF3500-06.jpg"
weight: "6"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Raupen sind mit 3D druck gemacht, weil ich keine richtige Kettenglieder aus fischertechnik bauen konnte. Habe mir dan eigentlich das orginale Rollenbock als anfang gebracht und denn verbreitet nach 30mm. 
Antriebzahnrad passt dann ins Raster von den Kettenglieder, und dieser Antriebzahnrad hat ein innen Z15 bekommen damit es uber denn Differenzial Zahnrad passt.