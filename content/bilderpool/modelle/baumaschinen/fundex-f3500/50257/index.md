---
layout: "image"
title: "F3500 spurbreite-verstellung"
date: 2024-01-26T18:54:54+01:00
picture: "FundexF3500-04.jpg"
weight: "4"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb musste so in circa 45mm hohe passen. 
Antrieb durch 2 Motoren. Die motoren treiben jeder eine Achse an. Am ende stecken schnecken die wieder Ritzel Z10 antreiben mit ein Gewindestange. 
Eine Trinkwasserleitung aus Kupfer ist gebraucht als Zylindermantel. Die Zylinder bewegen denn Raupen nach innen oder nach aussen.
Diese Raupen gleiten dan uber die hier Wagerecht abgebilderte Alu-profile.