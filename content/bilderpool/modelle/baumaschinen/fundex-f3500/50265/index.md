---
layout: "image"
title: "F3500 Komplett Bild hinten"
date: 2024-01-26T18:55:03+01:00
picture: "FundexF3500-01.jpg"
weight: "1"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Ca 2.5 meter hoch.
Von Internet wie es funktioniert:
Das Bohrrohr, ein starkwandiges Stahlrohr, wird von unten mit Stahlfußplatte und einer Dichtung wasserdicht verschlossen. Eine hydraulische Bohrung bringt diesen Hohlquerschnitt in den Boden, der seitlich verdrängt wird. Mit Erreichen der Absetztiefe wird ein Bewehrungskorb eingesetzt und das Rohr mit Beton verfüllt, bevor das Bohrrohr gezogen wird.