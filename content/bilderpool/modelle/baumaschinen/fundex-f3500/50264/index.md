---
layout: "image"
title: "F3500 Führungssystem"
date: 2024-01-26T18:55:02+01:00
picture: "FundexF3500-10.jpg"
weight: "10"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Jetzt musste ich die Teile vom Führungssystem bauen. Der besteht aus hauptsachlich 4 grosse Teile:
- Uithouder = Ausleger; damit kann mann die Komplette mast aufrechten und ist naturlich notwendig fur die stabilitat.
- Mast = verbunden mit denn Ausleger und den Makler gleitet durch die Mast.
- Makelaar = Makler; kurze ausfuhrung ca 25m, kann bis zum 40 meter lange gebaut werden
- Boorpaal = Bohrtisch; antreibung von Bohrpfahl.