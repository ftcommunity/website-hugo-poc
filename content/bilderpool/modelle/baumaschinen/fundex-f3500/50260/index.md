---
layout: "image"
title: "F3500 Bohrtisch"
date: 2024-01-26T18:54:57+01:00
picture: "FundexF3500-14.jpg"
weight: "14"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Denn Bohrtisch konnte leider nicht mit orginal Fischertechnik gebaut werden. Hier habe ich 2 Grundplatten mit Loch entwurfen und ein Antriebzahrad gemacht der uber ein Aluminium Rohr 50mm geschoben werden kann.
Dan Antriebzahrad ist gelagert mit 2 SKF 51110 Lager (nur die Ringen mit Kugeln gebraucht um die Einbauhohe zu behalten). 
Das Zahnrad wird mit Schnecke angetrieben durch 2 Motoren.