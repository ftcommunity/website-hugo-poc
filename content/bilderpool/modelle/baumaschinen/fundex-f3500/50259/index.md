---
layout: "image"
title: "F3500 Komplett Bild vorne"
date: 2024-01-26T18:54:56+01:00
picture: "FundexF3500-02.jpg"
weight: "2"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kann man ein bisschen unterscheiden wie es funktioniert.
In die Mast an die Vorderseite sind Schiene auf der ein Bohrtisch sich kann bewegen (hoch/runter). In den Tisch steckt ein Bohrpfahl der sich dreht.
An der Rechterseite sind schiene fur ein Aufzug.
Oben auf der Mast steht ein kleiner Kran, womit leichtere Teile hoch geheben werden konnen.
Auch gibt es noch ein haupt Hebe, womit ein Betonkübel hoch geheben werden kann.
