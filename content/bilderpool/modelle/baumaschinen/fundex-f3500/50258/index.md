---
layout: "image"
title: "F3500 Unterwagen"
date: 2024-01-26T18:54:55+01:00
picture: "FundexF3500-03.jpg"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Unterwagen/raupen hat spurbreiteverstellung damit er von 35cm Breite nach 50cm Breite verstellt werden kann. Beide Raupen werden angetrieben durch ein Pollin motor (1:780 glaube ich).
Dieser Motor treibt eine Achse an nach Vorne und Hinten, Da stecken differenzial Ritzel (alter form der 30mm Breit ist) der angetrieben werden durch Schnecken.
Auf dem Ritzel vom differnzial (der ist ja 30mm Breit) habe ich dan ein Zahnrad entwurfen (3D) der ich auf das Differenzial schieben kann. Damit habe ich auf der einen Seite ein Zahnrad und auf der anderen Seite ein Schneckengetriebe

Das Kettenrad ist so konstruiert, dass der Außendurchmesser genau in das Raster der Raupenkette passt
Drehkranz ist 3D druck und kann mann hier finden: https://www.thingiverse.com/thing:4062440
