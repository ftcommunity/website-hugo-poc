---
layout: "image"
title: "F3500 Raupen [3]"
date: 2024-01-26T18:54:49+01:00
picture: "FundexF3500-08.jpg"
weight: "8"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
uploadBy: "Website-Team"
license: "unknown"
---

CAD design von Kettenglieder und Zahnrad