---
layout: "image"
title: "O&K Bagger R18 scale 1:22 (1)"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr01.jpg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43102
- /details1c3a.html
imported:
- "2019"
_4images_image_id: "43102"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43102 -->
Orginalaufnahme aus dem Buch O&K Seilbagger Heinz - Herbert Cohrs, welches  als Vorlage für die technischen Angaben des Modells scale 1:22 diente.