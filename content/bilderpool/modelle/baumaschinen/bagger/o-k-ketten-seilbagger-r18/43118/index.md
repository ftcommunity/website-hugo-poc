---
layout: "image"
title: "O&K Bagger R18 (17) Seitenansicht 2"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr17.jpg"
weight: "17"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43118
- /detailsa098-2.html
imported:
- "2019"
_4images_image_id: "43118"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43118 -->
Das Modell wird mit dem Control-Set 500881 gesteuert, zwei IR- Empfänger u. ein IR-Sender. Ein Empfänger im Unterwagen und einer  im Aufbau, welche sich durch umschalten des Kanals am Sender recht einfach bedienen lassen. Zudem sind 4 Accu-35537 verbaut, welche  für die 8 Motoren, die Beleuchtung, das " Sound and Light Modul 130589", sowie für die Blinkelektronik 35604 die Energie liefern.