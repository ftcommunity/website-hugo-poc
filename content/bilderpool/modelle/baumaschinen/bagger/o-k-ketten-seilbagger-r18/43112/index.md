---
layout: "image"
title: "O&K Bagger R18 (11) Unterwagen von vorn"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr11.jpg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43112
- /details6390-3.html
imported:
- "2019"
_4images_image_id: "43112"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43112 -->
Hier sieht man das Lenkrad,  mit dem der Kettenspanner verstellt werden kann.