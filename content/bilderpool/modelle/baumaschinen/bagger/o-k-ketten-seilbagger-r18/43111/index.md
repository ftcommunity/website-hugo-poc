---
layout: "image"
title: "O&K Bagger R18 (10) Unterwagen mit Ketten und Teilverkleidung"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr10.jpg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43111
- /detailsf1e9-2.html
imported:
- "2019"
_4images_image_id: "43111"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43111 -->
Um die Orginal Farbgebung von O&K umzusetzen, habe ich Einteiler vom Sortierkasten( immer zwei übereinander in die Nut zwischen den Grundbausteinen Grau) eingeklemmt.