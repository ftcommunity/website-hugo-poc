---
layout: "image"
title: "O&K Bagger R18 (13) Der Drehkranz 2"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr13.jpg"
weight: "13"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43114
- /details9daa-2.html
imported:
- "2019"
_4images_image_id: "43114"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43114 -->
Das Innenzahnrad 30 ist mit zwei Verbindunsstopfen 6592 an der Grundplatte befestigt. Zwischen dem Innenzahnrad 30 und der Lagerschale sind 4 Federnocken eingeklemmt ,was der Justierung und dem Festhalten der Lagerschale dient.