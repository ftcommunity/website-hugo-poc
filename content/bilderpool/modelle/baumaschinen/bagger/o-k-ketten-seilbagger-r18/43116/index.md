---
layout: "image"
title: "O&K Bagger R18 (13) Der Drehkranz 4"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr15.jpg"
weight: "15"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43116
- /details9792.html
imported:
- "2019"
_4images_image_id: "43116"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43116 -->
ohne Beschreibung