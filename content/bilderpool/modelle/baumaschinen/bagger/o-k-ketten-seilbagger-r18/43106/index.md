---
layout: "image"
title: "O&K Bagger R18 (5) Unterwagen 2"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr05.jpg"
weight: "5"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43106
- /detailsa2b4.html
imported:
- "2019"
_4images_image_id: "43106"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43106 -->
Nun der Unterwagen von unten (kopfüber). Hier ist die Antriebsübertragung an die Leiträder zu sehen. Auch die Spannvorichtung der Ketten. Wegen dem hohen Gesamtgewicht (14,3 kg bei dem fertigen Modell )habe ich mich für zwei Zahnräder in der Spurbreite entschieden, was sich im Nachinein auch als richtig erwies.