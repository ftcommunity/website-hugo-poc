---
layout: "image"
title: "O&K Bagger R18 (22) Von Oben"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr22.jpg"
weight: "22"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43123
- /detailsc702.html
imported:
- "2019"
_4images_image_id: "43123"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43123 -->
Die Statikplatten am Heck habe ich mit der Formatkreissäge in die richtige Form gebracht, um der Optik bis ins letzte Detail gerecht zu werden.