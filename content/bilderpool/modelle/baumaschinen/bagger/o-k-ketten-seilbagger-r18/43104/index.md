---
layout: "image"
title: "O&K Bagger R18 (3)"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr03.jpg"
weight: "3"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43104
- /details50ad.html
imported:
- "2019"
_4images_image_id: "43104"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43104 -->
siehe Bild 1