---
layout: "image"
title: "O&K Bagger R18 (4) Unterwagen 1"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr04.jpg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43105
- /detailse389.html
imported:
- "2019"
_4images_image_id: "43105"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43105 -->
Hier gut zu sehen: das Dreieck der Rollenwagen, welche nur am Mittelpunkt des Dreiecks aufgehängt sind.