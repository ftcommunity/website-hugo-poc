---
layout: "image"
title: "O&K Bagger R18 (6) Unterwagen von der seite"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr06.jpg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43107
- /details4f0e-2.html
imported:
- "2019"
_4images_image_id: "43107"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43107 -->
Nun kann man die Rollenwagen in Funktion sehen. Mit dieser Bauweise von O&K verteilt die Kette die Last immer gleichmässig am Boden. Damit wurde die Spannung im Unterwagengestell stark reduziert.