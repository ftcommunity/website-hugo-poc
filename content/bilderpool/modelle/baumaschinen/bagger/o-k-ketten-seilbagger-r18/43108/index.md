---
layout: "image"
title: "O&K Bagger R18 (7) die Einzelteile des Kettengliedes"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr07.jpg"
weight: "7"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43108
- /details23ba.html
imported:
- "2019"
_4images_image_id: "43108"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43108 -->
Um ein sehr breites Kettenglied zu bekommen, wie im Orginal, und um den Maßstab so gut wie es geht einzuhalten, habe  ich zwei Bauplatten 6 x1 verwendet. In der Mitte, als Abstandshalter, sind zwei Winkelsteine 60 Grad verbaut. Somit erhöht sich das Gewicht des Kettengliedes, und es hängt später an den oberen Laufrollen (Zahnräder) richtig durch, wie beim Orginal.