---
layout: "image"
title: "O&K Bagger R18 (12) Der Drehkranz 1"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr12.jpg"
weight: "12"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43113
- /details0150.html
imported:
- "2019"
_4images_image_id: "43113"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43113 -->
Das  Problem" Drehkranz bei Fischertechnik" ist wohl jedem bekannt. Dann noch 14,3 kg, sowie eine möglichts niedrige Bauhöhe.Soetwas lässt sich nicht ohne ein Fremdteil umsetzen. Meine Kostruktion: ein Drucklager, 3- teilig, 2 Lagerschalen und ein Kugelkranz mit Käfig, in dem ich noch ein Planetengetriebe einbauen konnte.