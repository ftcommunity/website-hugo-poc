---
layout: "image"
title: "Die Halle im Original"
date: "2015-05-26T20:18:54"
picture: "WP_20141020_091.jpg"
weight: "6"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41042
- /detailse5db.html
imported:
- "2019"
_4images_image_id: "41042"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T20:18:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41042 -->
Da es im Netz kaum etwas zu finden gibt, hier die Halle im Original. Gebaut wird fullscale nach Originalbauplänen! Gesamtlänge ü.a. 5,50 m. Noch bin ich 46 Jahre alt, und wenn ich mit 63 in Rente gehe, dann ist das Modell fertig...