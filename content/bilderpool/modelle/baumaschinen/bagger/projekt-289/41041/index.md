---
layout: "image"
title: "Gesamtansicht"
date: "2015-05-26T14:41:38"
picture: "WP_20141030_009.jpg"
weight: "5"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41041
- /details47be.html
imported:
- "2019"
_4images_image_id: "41041"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41041 -->
Gesamtansicht vor Einbau der Halle