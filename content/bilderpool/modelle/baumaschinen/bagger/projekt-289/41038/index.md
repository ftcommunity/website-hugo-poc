---
layout: "image"
title: "Ballastausleger nach langer Pause"
date: "2015-05-26T14:41:38"
picture: "WP_20141101_001.jpg"
weight: "2"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41038
- /details6cb8-2.html
imported:
- "2019"
_4images_image_id: "41038"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41038 -->
Ballastausleger von oben (noch liegend im Bau)