---
layout: "image"
title: "Gesamtansicht ohne Halle"
date: "2015-05-26T20:18:54"
picture: "WP_20141130_012.jpg"
weight: "7"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- /php/details/41043
- /details3313.html
imported:
- "2019"
_4images_image_id: "41043"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T20:18:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41043 -->
