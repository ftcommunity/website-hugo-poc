---
layout: "image"
title: "Montageständer"
date: "2004-01-20T20:34:29"
picture: "IMG_0504.jpg"
weight: "30"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "-?-"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2073
- /details7572.html
imported:
- "2019"
_4images_image_id: "2073"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2004-01-20T20:34:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2073 -->
Hier steht der Bagger auf dem Fahrwerkmontageständer. Das ist praktisch wenn man was am Fahrwerk ändern will, z.B. baue ich als Antrieb 2 P-Mots. 50:1 ein. Habe das so aufgebaut um die Schwenkwerksantriebswelle zu wechseln, diese war vorher aus einer Rastachse mit einem Z 10 und jetzt ist es eine Silberstahlwelle. Das scheint ein wenig stabiler zu sein. Auch zu sehen ist die geänderte Heckpartie, die jetzt ein wenig ansprechender wirkt.
