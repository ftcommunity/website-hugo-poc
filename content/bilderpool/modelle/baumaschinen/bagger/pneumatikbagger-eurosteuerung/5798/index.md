---
layout: "image"
title: "Fahrwerk"
date: "2006-02-27T22:04:06"
picture: "Fahrwerk.jpg"
weight: "41"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5798
- /detailsb916.html
imported:
- "2019"
_4images_image_id: "5798"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-02-27T22:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5798 -->
Neues allradangetriebenes Fahrwerk für den Bagger, mit mechanisch heb und senkbarem Schiebeschild
