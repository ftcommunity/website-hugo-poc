---
layout: "image"
title: "Schiebeschild"
date: "2006-08-21T19:42:50"
picture: "IMG_0650.jpg"
weight: "51"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/6708
- /details0bfd.html
imported:
- "2019"
_4images_image_id: "6708"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-08-21T19:42:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6708 -->
Mittels des leicht abgewandelten Krafthebers von Harald wird das Schild auf und ab gesteuert.
