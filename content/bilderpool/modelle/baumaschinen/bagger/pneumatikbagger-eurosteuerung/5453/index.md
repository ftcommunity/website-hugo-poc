---
layout: "image"
title: "Loeffelstiel"
date: "2005-12-01T21:52:48"
picture: "Loeffelstiel.jpg"
weight: "40"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5453
- /detailsf23a-3.html
imported:
- "2019"
_4images_image_id: "5453"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2005-12-01T21:52:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5453 -->
