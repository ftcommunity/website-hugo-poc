---
layout: "image"
title: "Mechanik"
date: "2011-07-14T12:04:42"
picture: "kettenbagger08.jpg"
weight: "8"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31281
- /details0df0-2.html
imported:
- "2019"
_4images_image_id: "31281"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31281 -->
Die Schnecke, die den Arm bewegt. 
Man sieht wie lang die Schnecke ist.