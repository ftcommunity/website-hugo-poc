---
layout: "image"
title: "Gesamtansicht"
date: "2012-07-24T22:12:29"
picture: "liebherrr6.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35213
- /details8e16.html
imported:
- "2019"
_4images_image_id: "35213"
_4images_cat_id: "2610"
_4images_user_id: "1122"
_4images_image_date: "2012-07-24T22:12:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35213 -->
Die Länge beträgt 142 cm.