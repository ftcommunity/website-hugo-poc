---
layout: "image"
title: "Aufstieg zum Fahrerhäuschen - Leiter oben"
date: "2012-08-11T20:38:30"
picture: "liebherrr06.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35312
- /details813d.html
imported:
- "2019"
_4images_image_id: "35312"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35312 -->
Die Leiter wird mithilfe eines Minimotors hoch und runter gefahren.