---
layout: "image"
title: "Getriebe zum Bewegen der Schaufel"
date: "2012-08-11T20:38:30"
picture: "liebherrr22.jpg"
weight: "22"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35328
- /detailsa62f.html
imported:
- "2019"
_4images_image_id: "35328"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35328 -->
-