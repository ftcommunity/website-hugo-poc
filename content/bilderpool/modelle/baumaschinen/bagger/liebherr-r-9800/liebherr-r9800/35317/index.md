---
layout: "image"
title: "Motor zum Drehen des Baggers"
date: "2012-08-11T20:38:30"
picture: "liebherrr11.jpg"
weight: "11"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35317
- /detailse9cf.html
imported:
- "2019"
_4images_image_id: "35317"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35317 -->
-