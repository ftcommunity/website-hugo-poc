---
layout: "image"
title: "Getriebe zum Bewegen des Mittelstücks des Arms"
date: "2012-08-11T20:38:30"
picture: "liebherrr20.jpg"
weight: "20"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35326
- /detailsb040-2.html
imported:
- "2019"
_4images_image_id: "35326"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35326 -->
-