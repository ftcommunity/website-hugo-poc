---
layout: "image"
title: "Drehkranz"
date: "2012-08-11T20:38:30"
picture: "liebherrr10.jpg"
weight: "10"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35316
- /details736b.html
imported:
- "2019"
_4images_image_id: "35316"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35316 -->
-