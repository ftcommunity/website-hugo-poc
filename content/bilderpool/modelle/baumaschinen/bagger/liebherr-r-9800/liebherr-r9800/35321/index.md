---
layout: "image"
title: "Antrieb des Gesamtarms"
date: "2012-08-11T20:38:30"
picture: "liebherrr15.jpg"
weight: "15"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35321
- /details75c4-2.html
imported:
- "2019"
_4images_image_id: "35321"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35321 -->
4 Motoren heben und senken den kompletten Arm. 
Es sind zwei Motoren pro Zylinder erforderlich. Das Foto zeigt eine dieser Antriebe.