---
layout: "image"
title: "Innenleben des Baggers"
date: "2014-05-02T16:40:08"
picture: "Bagger3.jpg"
weight: "3"
konstrukteure: 
- "majus"
fotografen:
- "majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/38740
- /details2d68.html
imported:
- "2019"
_4images_image_id: "38740"
_4images_cat_id: "2893"
_4images_user_id: "1239"
_4images_image_date: "2014-05-02T16:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38740 -->
Wie im ersten Bild beschrieben sieht man hier das Extension rechts und links die Magnetventile für die Zylinder des Baggerarms. Außerdem erkennbar sind die Ausgänge der Motorenkabel für Drehen und Fahren sowie ganz hinten zwei Kabelwicklungen, die die fehlenden Eingänge vom Interface her bilden.