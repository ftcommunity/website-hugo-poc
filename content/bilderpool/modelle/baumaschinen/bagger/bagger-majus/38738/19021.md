---
layout: "comment"
hidden: true
title: "19021"
date: "2014-05-06T10:14:03"
uploadBy:
- "majus"
license: "unknown"
imported:
- "2019"
---
Hallo,

zur Anmerkung von Dirk Fox: Die Idee mit der Drossel hatte ich auch, allerdings bei der Umsetzung Probleme. Da die Magnetventile nur die Stellung "auf" kennen, war es extrem schwierig, den Zylinder in einer Position zu halten. Im Prinzip wäre das nur mit einem Ventil möglich gewesen, das einen entsprechenden Gegendruck aufgebaut hätte. Leider habe ich weder genug Ventile (die sind ja mörderteuer) noch genug Platz im Bagger gehabt, um das Umzusetzen. In diesem Fall reicht als Drossel ein einfaches Handventil direkt nach den Kompressor, da die Zylinder ohnehin nur in zwei Stellungen gehalten werden konnten.

Gruß 

Majus