---
layout: "image"
title: "Onderste stand"
date: "2012-08-07T08:03:14"
picture: "PICT3782.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/35269
- /detailsb88b.html
imported:
- "2019"
_4images_image_id: "35269"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-08-07T08:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35269 -->
Arm in onderste stand