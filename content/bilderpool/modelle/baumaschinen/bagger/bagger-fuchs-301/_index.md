---
layout: "overview"
title: "Bagger Fuchs 301"
date: 2022-08-16T23:39:45+02:00
---

Der Fuchs Bagger 301 war ein oft vertretener Geselle und wurde über 10 000 mal gebaut. Er ist in den 60 und 70er Jahren überall eingesetzt worden. Auf Umschlagplätzen, im Straßenbau, Tiefbau und Hochbau, an Hafenanlagen, auf dem Schrottplatz und in der Landwirschaft. 
Er ist während dieser Zeit sehr weit verbreitet gewesen.  

Der Bagger ist vollfunktionfähig, besitzt einen Grabenlöffel und ein Greifer. Als Kran kann er auch eingesetzt werden. Die zwei Seilwinden werden mit einem Differenzial angetrieben, deren Zahnrad mit dem Servo gesperrt werden kann.
Damit ist das Öffnen und Schließen der Schaufel möglich. Im Oberwagen befinden sich zwei Akku, ein Akku im Unterwagen. Die Empfänger sind unten und oben verbaut und mit einer Fehrsteuerung gekoppelt. 
Die Beleuchtung und das Blinklicht sind funktionsfähig. Das schwierigste an diesem Modell war, das Beschaffen der blauen Teile insbesondere deren Anzahl. Der Nachbau hat mir viel Spaß gemacht, besonders das Beschaffen der seltenen blauen Bauteile.

Viel Spaß beim Anschauen.

Detlef Ottmann