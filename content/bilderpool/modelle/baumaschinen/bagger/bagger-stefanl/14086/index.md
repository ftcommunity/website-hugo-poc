---
layout: "image"
title: "Bagger 18"
date: "2008-03-24T10:35:19"
picture: "baggerstefanl18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14086
- /details8e22.html
imported:
- "2019"
_4images_image_id: "14086"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:19"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14086 -->
Der Linke lässt sich natürlich hochklappen damit der Fahrer besser aussteigen kann.
