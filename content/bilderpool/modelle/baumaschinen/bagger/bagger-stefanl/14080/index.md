---
layout: "image"
title: "Bagger 12"
date: "2008-03-24T10:35:18"
picture: "baggerstefanl12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14080
- /detailsa0b1.html
imported:
- "2019"
_4images_image_id: "14080"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14080 -->
