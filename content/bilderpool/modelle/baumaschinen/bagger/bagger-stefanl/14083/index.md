---
layout: "image"
title: "Bagger 15"
date: "2008-03-24T10:35:19"
picture: "baggerstefanl15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14083
- /details3077-2.html
imported:
- "2019"
_4images_image_id: "14083"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:19"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14083 -->
Neue Kettenhalterung.
