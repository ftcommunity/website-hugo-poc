---
layout: "image"
title: "Bagger 14"
date: "2008-03-24T10:35:19"
picture: "baggerstefanl14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14082
- /detailsb111.html
imported:
- "2019"
_4images_image_id: "14082"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14082 -->
Durch diese Platte wird der Deckel gehalten, sie ersetzt den Gummi.
