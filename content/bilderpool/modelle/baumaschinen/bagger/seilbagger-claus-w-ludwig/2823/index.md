---
layout: "image"
title: "Seilbagger 004"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_004.JPG"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2823
- /detailsa70a.html
imported:
- "2019"
_4images_image_id: "2823"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2823 -->
von Claus-W. Ludwig