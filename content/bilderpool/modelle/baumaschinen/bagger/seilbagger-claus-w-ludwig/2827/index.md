---
layout: "image"
title: "Seilbagger 008"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_008.JPG"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2827
- /detailsfde7.html
imported:
- "2019"
_4images_image_id: "2827"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2827 -->
von Claus-W. Ludwig