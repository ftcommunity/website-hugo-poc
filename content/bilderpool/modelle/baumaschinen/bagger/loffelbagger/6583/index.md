---
layout: "image"
title: "löffelbagger 5"
date: "2006-06-24T19:38:46"
picture: "ft_lffelbagger3_007.jpg"
weight: "8"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/6583
- /details6b03.html
imported:
- "2019"
_4images_image_id: "6583"
_4images_cat_id: "567"
_4images_user_id: "368"
_4images_image_date: "2006-06-24T19:38:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6583 -->
