---
layout: "image"
title: "löffelbagger 3"
date: "2006-06-24T19:38:11"
picture: "ft_lffelbagger_003.jpg"
weight: "3"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/6576
- /details78f4.html
imported:
- "2019"
_4images_image_id: "6576"
_4images_cat_id: "567"
_4images_user_id: "368"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6576 -->
