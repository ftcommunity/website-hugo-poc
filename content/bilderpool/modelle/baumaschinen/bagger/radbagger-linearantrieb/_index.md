---
layout: "overview"
title: "Radbagger mit Linearantrieb"
date: 2023-03-17T17:34:44+01:00
---

Hier stelle ich ein Modell vor, das kein Nachbau ist, sonderm von mir frei konstruiert wurde.
Es handelt sich um einen Radbagger der 15-Tonnen-Klasse. 
Ich habe hier Linearantriebe aus dem freien Handel eingesetzt, die erst seit ca. einem halben Jahr auf dem Markt sind, und habe festgestellt, dass sie mehr als hervorragend zu fischertechnik passen und sogar wie gerade für fischertechnik gemacht sind.
Ich hoffe, dass alle, die mit fischertechnik verbunden sind, damit so viel Freude haben wie ich.
Jetzt ist etwas möglich, was mit fischertechnik noch nie ging, und das so einfach, da die Linearantriebe mit 9 bis 12 Volt laufen. Außerdem haben sie schon die Endabschalter eingebaut und eine Kurzschluss-Sicherung. An den Enden befindet sich eine Aufnahme mit 4 mm-Loch; außerdem passen diese Aufnahmen in alle Teile von fischertechnik. Man kann sie wie einen fischertechnik-Motor einfach mit einem Polwendeschalter oder Taster ansteuern oder man kann sie einfach an die Empfänger anschließen und mit der Fernbedienung sauber steuern.

Video auf Youtube: https://youtu.be/50cDMSyKk5E
