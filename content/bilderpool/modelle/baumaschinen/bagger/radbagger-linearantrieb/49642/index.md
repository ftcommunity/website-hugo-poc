---
layout: "image"
title: "Radbagger mit Linearantrieb Heckansicht mit angehobenen Schild. "
date: 2023-03-17T17:34:47+01:00
picture: "DO-Radbagger-07.jpeg"
weight: "7"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind die 3D-Druck-Steine in blau als Abdeckung gut zu sehen. Danke Peter.
