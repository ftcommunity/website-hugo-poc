---
layout: "image"
title: "Radbagger mit Linearantrieb (10)"
date: 2023-03-17T17:34:59+01:00
picture: "DO-Radbagger-10.jpeg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Vorderachse ist eine Pendelachse, aber nicht angetrieben. Die Lenkung (wie man sehen kann) ein Linearantrieb, zwei Kabel zum Empfänger, fertig. Keine Endabschalter, kein Zahnstangen-Getriebe, das nach der Zeit den Geist aufgibt. Die Kraft der Linearantriebe lassen auch keine Zweifel aufkommen: noch einfacher und besser geht nicht.