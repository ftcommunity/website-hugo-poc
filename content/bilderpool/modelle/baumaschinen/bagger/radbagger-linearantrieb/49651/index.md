---
layout: "image"
title: "Führerhaus Innenansicht"
date: 2023-03-17T17:34:58+01:00
picture: "DO-Radbagger-11.jpeg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Die drei Kippschalter hinter dem Sitz schalten die Stromzuführung von Akku ein, das Blinklicht und das Fahrlicht.