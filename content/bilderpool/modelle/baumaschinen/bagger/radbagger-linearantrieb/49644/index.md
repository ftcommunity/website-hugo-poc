---
layout: "image"
title: "Radbagger mit Linearantrieb (5)"
date: 2023-03-17T17:34:49+01:00
picture: "DO-Radbagger-05.jpeg"
weight: "5"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Linearantriebe mit 30 N haben soviel Kraft, dass sie den Bagger ganz locker hochstemmen. Man bedenke, dass dieser 6,6 Kilogramm wiegt. Das alles geht auch nur dann, wenn man die entsprechende Konstruktion einsetzt. Nicht verzweifeln, wenn es beim ersten Mal nicht klappt; da stecken 35 Jahre fischertechnik-Erfahrung drin.
