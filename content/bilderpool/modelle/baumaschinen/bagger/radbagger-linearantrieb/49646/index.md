---
layout: "image"
title: "Radbagger mit Linearantrieb (3)"
date: 2023-03-17T17:34:52+01:00
picture: "DO-Radbagger-03.jpeg"
weight: "3"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Bei dem Modell habe ich ein Bluetooth-Control-Set eingesetzt mit zwei Empfängern: einen im Unterbau für Lenkung mit Linearantrieb, den Fahrantrieb und das Schiebeschild. Im Oberwagen der zweite Empfänger für Drehkranz, Hubarm, Stiel und Schaufel.
