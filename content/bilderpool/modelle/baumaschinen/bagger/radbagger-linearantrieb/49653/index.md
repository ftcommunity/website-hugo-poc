---
layout: "image"
title: "Radbagger mit Linearantrieb (1)"
date: 2023-03-17T17:35:01+01:00
picture: "DO-Radbagger-01.jpeg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

