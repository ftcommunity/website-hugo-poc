---
layout: "image"
title: "Radbagger mit Linearantrieb: Vorderansicht"
date: 2023-03-17T17:34:46+01:00
picture: "DO-Radbagger-08.jpeg"
weight: "8"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Schild aufgesetzt. Auch hier kann man sehen, wie gut die Aufnahmen der Linearantriebe zum fischertechnik-System passen.
