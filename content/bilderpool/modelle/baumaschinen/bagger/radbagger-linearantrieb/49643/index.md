---
layout: "image"
title: "Radbagger mit Linearantrieb (6)"
date: 2023-03-17T17:34:48+01:00
picture: "DO-Radbagger-06.jpeg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

In einem [Video](https://youtu.be/50cDMSyKk5E) kann man sehen, wie extrem gut der Bagger mit den Linearantrieben funktioniert. Die glasierten Sojabohnen, die ich als Stückgut verwende, sind nicht gerade leicht: eine randvolle Schaufelfüllung wiegt gute 300 g.
