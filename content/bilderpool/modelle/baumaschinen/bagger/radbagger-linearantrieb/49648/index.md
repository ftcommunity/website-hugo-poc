---
layout: "image"
title: "Hochgestemmter Bagger"
date: 2023-03-17T17:34:54+01:00
picture: "DO-Radbagger-14.jpeg"
weight: "14"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Ich hoffe, dass wir jetzt alle ganz viele Modelle sehen werden mit Linearantrieb. Jetzt ist möglich, wovon wir alle schon über 50 Jahre träumen.