---
layout: "image"
title: "Befestigung des Baggerstiels"
date: 2023-03-17T17:34:56+01:00
picture: "DO-Radbagger-13.jpeg"
weight: "13"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Die „Hydraulikschläuche“ sind ein Silikonschlauch mit 4 mm Innendurchmesser. In diesem habe ich die Kabel der Linearantriebe verschwinden lassen. So ist das Kabel unsichtbar und ich hatte gleich einen „Hydraulikschlauch“.
