---
layout: "image"
title: "Radbagger mit Linearantrieb (4)"
date: 2023-03-17T17:34:51+01:00
picture: "DO-Radbagger-04.jpeg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell entspricht dem Maßstab 1:10. Da fischertechnik keine passende Schaufel hat, habe ich mir eine Schaufel und ein paar Teile, die fischertechnik nicht in Blau hatte, in 3D-Druck entwerfen und drucken lassen. Ein großes Dankeschön und Lob an Peter Habermehl.
