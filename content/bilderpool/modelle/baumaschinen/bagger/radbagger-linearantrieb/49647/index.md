---
layout: "image"
title: "Radbagger mit Linearantrieb (2)"
date: 2023-03-17T17:34:53+01:00
picture: "DO-Radbagger-02.jpeg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Linearantrieb"]
uploadBy: "Website-Team"
license: "unknown"
---

In dem Modell sind folgende Linearantriebe verbaut: 

* ein Antrieb mit 50 mm Aushub, 30 mm/s Geschwindigkeit und 30 N Kraft für die Lenkung 
* zwei Antriebe mit 100 mm Aushub, gleiche Werte für den Hubarm
* ein Antrieb, gleicher Typ für den Stiel
* ein Antrieb, gleicher Typ für die Schaufel
* ein Antrieb mit 21 mm Aushub, 30 mm/s Geschwindigkeit und 30 N als Schild-Heber-Senker

Als Fahrantrieb habe ich einen Powermotor 20:1 verwendet und einen Powermotor 50:1 für den Drehkranz.
