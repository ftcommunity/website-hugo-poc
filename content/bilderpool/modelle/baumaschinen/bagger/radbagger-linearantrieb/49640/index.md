---
layout: "image"
title: "Radbagger mit Linearantrieb: Führerhaus mit geschlossener Tür"
date: 2023-03-17T17:34:44+01:00
picture: "DO-Radbagger-09.jpeg"
weight: "9"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Die Monstertruck-Reifen, die wir schon lange bei fischertechnik verwenden, halten das Gewicht des Baggers nicht. Ich habe hier die Hartkunststoff-Reifen eines Bruder-Modell-Baggers benutzt und die Monstertruck-Reifen darüber gezogen: nicht ganz leicht, aber ein hervorragender Effekt.
