---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr26.jpg"
weight: "25"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47133
- /detailseea0.html
imported:
- "2019"
_4images_image_id: "47133"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47133 -->
bei der arbeit