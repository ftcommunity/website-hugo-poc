---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:23"
picture: "bagegr10.jpg"
weight: "9"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47117
- /details5182-2.html
imported:
- "2019"
_4images_image_id: "47117"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47117 -->
praktische hilfe. mit dieser konstruktion wird verhindert dass das Seil von der Rolle (-Rollen-) rutscht. 
funktioniert einwandfrei