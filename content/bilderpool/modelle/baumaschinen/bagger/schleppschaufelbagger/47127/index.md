---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr20.jpg"
weight: "19"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47127
- /details3f68.html
imported:
- "2019"
_4images_image_id: "47127"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47127 -->
ein encoder motor für die schaufel (vor-zurück)  und ein nicht sichbarer ebenfalls für die schaufel (hoch-runter) ist etwas weiter hinten montiert