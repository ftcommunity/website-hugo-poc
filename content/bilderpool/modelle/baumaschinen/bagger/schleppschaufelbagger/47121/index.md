---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr14.jpg"
weight: "13"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47121
- /details65f6-2.html
imported:
- "2019"
_4images_image_id: "47121"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47121 -->
treppen, geländer und co gibt es auch.