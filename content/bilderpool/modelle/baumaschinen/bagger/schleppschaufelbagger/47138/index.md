---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr31.jpg"
weight: "30"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47138
- /details1f3b-3.html
imported:
- "2019"
_4images_image_id: "47138"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47138 -->
von der seite.  übrigens das laufwerk funktioniert zwar gut ist aber recht langsam 1min pro 50cm aber dafür sehr spektakular wenn der ganze bagger angehoben wird.
mit der schaufel kann man tarieren sodass der bagger beim laufen immer schön in der waagerechten bleibt.

Ja ich weiß die Originalen schleppschaufelbagger können nur rückwärtslaufen wel sie vornüberkippen. meiner allerdings kann vor und zurücklaufen. und das ohne zu kippen.
