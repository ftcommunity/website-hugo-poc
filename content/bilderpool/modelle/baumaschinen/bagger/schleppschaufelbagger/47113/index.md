---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:23"
picture: "bagegr06.jpg"
weight: "5"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47113
- /detailsce2b-2.html
imported:
- "2019"
_4images_image_id: "47113"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47113 -->
die zwei masten