---
layout: "image"
title: "Kettenbagger, hinten"
date: "2011-07-14T11:58:24"
picture: "kettenbagger04.jpg"
weight: "4"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31246
- /details03ce.html
imported:
- "2019"
_4images_image_id: "31246"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31246 -->
Das Z20 Zahnrad sitzt auf der Achse der Schnecke, die den Arm bewegt.