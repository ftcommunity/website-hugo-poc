---
layout: "image"
title: "Kettenbagger"
date: "2011-07-14T11:58:24"
picture: "kettenbagger26.jpg"
weight: "24"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31268
- /details366d.html
imported:
- "2019"
_4images_image_id: "31268"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31268 -->
zu sehen sind die beiden IR- Empfänger