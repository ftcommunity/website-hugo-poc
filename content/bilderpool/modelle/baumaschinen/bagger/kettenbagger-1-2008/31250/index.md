---
layout: "image"
title: "Kettenbagger, oben"
date: "2011-07-14T11:58:24"
picture: "kettenbagger08.jpg"
weight: "8"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31250
- /details4e8f.html
imported:
- "2019"
_4images_image_id: "31250"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31250 -->
