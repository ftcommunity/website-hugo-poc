---
layout: "image"
title: "Drehkranz Oberteil"
date: "2006-06-20T21:35:52"
picture: "DSCN0797.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6449
- /details5689.html
imported:
- "2019"
_4images_image_id: "6449"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6449 -->
Ansicht von unten
