---
layout: "image"
title: "Ansicht von vorn"
date: "2006-07-06T15:33:42"
picture: "DSCN0846.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6600
- /details53be.html
imported:
- "2019"
_4images_image_id: "6600"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-06T15:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6600 -->
Die grauen Bausteine am Löffel habe ich gegen schwarze ausgetauscht. Das sieht doch wesentlich besser aus. Alles was jetzt noch in grau zu sehen ist wird noch verkleidet.
