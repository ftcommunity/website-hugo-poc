---
layout: "image"
title: "Seitenansicht"
date: "2006-07-06T15:33:42"
picture: "DSCN0845.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6601
- /details30f8.html
imported:
- "2019"
_4images_image_id: "6601"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-06T15:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6601 -->
Das kleine rote "Kästchen" an der rechten Vorderkante ist das Führerhaus. Die Bauplatte 15x45 weiss soll die Fensterscheibe darstellen.
