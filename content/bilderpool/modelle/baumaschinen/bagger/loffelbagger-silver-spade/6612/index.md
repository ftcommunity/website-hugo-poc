---
layout: "image"
title: "Detailaufnahme"
date: "2006-07-10T17:38:40"
picture: "DSCN0865.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6612
- /details1962.html
imported:
- "2019"
_4images_image_id: "6612"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-10T17:38:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6612 -->
des "Energiebausteines" ;-)
