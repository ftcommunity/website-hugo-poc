---
layout: "image"
title: "oberer Anschlag"
date: "2014-07-11T14:18:06"
picture: "pbagger9.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39019
- /detailsf91f-2.html
imported:
- "2019"
_4images_image_id: "39019"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39019 -->
nach oben sind es zwei U-Träger, die der Arm überstreichen kann.
