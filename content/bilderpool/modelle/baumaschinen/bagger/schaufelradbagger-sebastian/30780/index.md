---
layout: "image"
title: "Fahrwerk"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger4.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30780
- /detailsa9b6.html
imported:
- "2019"
_4images_image_id: "30780"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30780 -->
Hier sieht man das Fahrwerk.