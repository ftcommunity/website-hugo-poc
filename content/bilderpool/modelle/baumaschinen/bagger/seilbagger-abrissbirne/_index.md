---
layout: "overview"
title: "Seilbagger mit Abrissbirne"
date: 2023-07-28T16:27:23+02:00
---

Seilbagger mit einer Abrissbirne zum Einreißen von großen Gebäuden.

Ein Kleinmodell, ca. 20 cm lang, ohne Elektrik und Motor, mit 2 Seilwinden. 
Das Modell lässt sich schieben und schwenken. Mehr gibt es dazu eigentlich nicht zu sagen.
