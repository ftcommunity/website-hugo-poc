---
layout: "image"
title: "Seilbagger – Abrissbirne"
date: 2023-07-28T16:27:27+02:00
picture: "seilbagger-05.jpg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

