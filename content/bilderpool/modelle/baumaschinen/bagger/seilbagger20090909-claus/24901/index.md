---
layout: "image"
title: "[2/4] Diagonalansicht rechts"
date: "2009-09-10T21:27:15"
picture: "seilbaggerclaus2.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24901
- /details4f52.html
imported:
- "2019"
_4images_image_id: "24901"
_4images_cat_id: "1715"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24901 -->
