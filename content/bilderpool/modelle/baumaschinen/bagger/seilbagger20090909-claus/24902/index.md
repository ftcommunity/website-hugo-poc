---
layout: "image"
title: "[3/4] Draufsicht"
date: "2009-09-10T21:27:15"
picture: "seilbaggerclaus3.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24902
- /details1fb8-2.html
imported:
- "2019"
_4images_image_id: "24902"
_4images_cat_id: "1715"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24902 -->
