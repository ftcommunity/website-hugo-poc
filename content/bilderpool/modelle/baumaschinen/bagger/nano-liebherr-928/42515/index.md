---
layout: "image"
title: "... auch gut 'einpacken'."
date: "2015-12-09T12:11:32"
picture: "nanobagger6.jpg"
weight: "6"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "dito"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42515
- /details5e2b-2.html
imported:
- "2019"
_4images_image_id: "42515"
_4images_cat_id: "3160"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42515 -->
Damit kann er gut herumfahren.