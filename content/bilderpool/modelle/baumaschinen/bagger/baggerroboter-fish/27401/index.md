---
layout: "image"
title: "Bagger komplett (Version 2)"
date: "2010-06-06T21:36:58"
picture: "baggerfishv2.jpg"
weight: "7"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27401
- /detailse4e7.html
imported:
- "2019"
_4images_image_id: "27401"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27401 -->
Der ganze Bagger in neuer Version mit Ketten.