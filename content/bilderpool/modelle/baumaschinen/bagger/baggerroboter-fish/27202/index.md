---
layout: "image"
title: "Bagger Gesamtansicht (Version 1)"
date: "2010-05-08T20:05:20"
picture: "bagger1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27202
- /detailsba1b.html
imported:
- "2019"
_4images_image_id: "27202"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27202 -->
Man sieht den Bagger mit dem Baggerarm, den Kompressoren und den Rädern.