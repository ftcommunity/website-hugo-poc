---
layout: "image"
title: "Baggerarm"
date: "2010-05-08T20:05:20"
picture: "bagger5.jpg"
weight: "5"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27206
- /details91e5.html
imported:
- "2019"
_4images_image_id: "27206"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27206 -->
Das ist der Baggerarm mit vier Zylindern.