---
layout: "image"
title: "Programmbild"
date: "2010-06-06T21:36:58"
picture: "baggerfishv1.jpg"
weight: "6"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27400
- /details8548.html
imported:
- "2019"
_4images_image_id: "27400"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27400 -->
Bedienfeld zur Steuerung der Ketten / Räder des Baggers und dem Baggerarm. Das Programm kann heruntergeladen werden unter: http://www.ftcommunity.de/data/downloads/robopro/bagger.rpp