---
layout: "overview"
title: "Baggerroboter (fish"
date: 2020-02-22T08:12:12+01:00
legacy_id:
- /php/categories/1952
- /categories3ad9.html
- /categoriesc734.html
- /categories6b21.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1952 --> 
Pneumatischer Baggerroboter mit vier Zylindern, drei Magnetventielen, zwei Kompressoren, zwei Lampen, einem Farbsensor, einem Fahrgestell und einem Robo Interface.