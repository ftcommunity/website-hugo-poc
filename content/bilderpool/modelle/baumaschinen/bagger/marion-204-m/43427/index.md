---
layout: "image"
title: "Draaikrans"
date: "2016-05-25T10:12:52"
picture: "P3280053.jpg"
weight: "13"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43427
- /details0295.html
imported:
- "2019"
_4images_image_id: "43427"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43427 -->
Aandrijving van de draaikrans.