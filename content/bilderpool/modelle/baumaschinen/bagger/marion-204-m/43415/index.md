---
layout: "image"
title: "Marion 204 M"
date: "2016-05-25T10:12:52"
picture: "P3190003_-_kopie.jpg"
weight: "1"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43415
- /details771c.html
imported:
- "2019"
_4images_image_id: "43415"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43415 -->
Mijn nieuwste model een Marion 204 M