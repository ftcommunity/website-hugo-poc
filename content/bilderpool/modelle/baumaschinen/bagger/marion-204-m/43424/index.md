---
layout: "image"
title: "Inzicht in de machine"
date: "2016-05-25T10:12:52"
picture: "P3280052.jpg"
weight: "10"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43424
- /details6497.html
imported:
- "2019"
_4images_image_id: "43424"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43424 -->
Zicht op de beide hoofd lieren die de arm bewegen. Achterin is de enkele motor te zien die via het hevelsysteem de hoek van de graafbak bepaald.