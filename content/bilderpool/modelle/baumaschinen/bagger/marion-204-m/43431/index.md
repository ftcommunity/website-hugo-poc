---
layout: "image"
title: "Alweer gesloopt"
date: "2016-05-25T10:12:53"
picture: "P3280055.jpg"
weight: "17"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/43431
- /details6381.html
imported:
- "2019"
_4images_image_id: "43431"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43431 -->
Alweer een gedeelte gedemonteerd, op naar het volgende bouwsel.