---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:58"
picture: "schaufelradbagger11.jpg"
weight: "11"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29713
- /details3371.html
imported:
- "2019"
_4images_image_id: "29713"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29713 -->
Das Schaufelrad von der Montageseite her gesehen.
