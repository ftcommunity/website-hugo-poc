---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger03.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29705
- /details362f.html
imported:
- "2019"
_4images_image_id: "29705"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29705 -->
Die beiden schrägen Teile mit den verchromten Achsen sind mechanische Seilspanner die mittels Gummiband die Seilspannung gleichmäßig halten.
