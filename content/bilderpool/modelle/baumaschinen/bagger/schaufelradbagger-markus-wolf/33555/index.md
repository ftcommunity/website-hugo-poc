---
layout: "image"
title: "schaufelradbagger11.jpg"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger11_2.jpg"
weight: "24"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33555
- /detailscb37.html
imported:
- "2019"
_4images_image_id: "33555"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33555 -->
Das Rad ist nur einseitig gelagert. Mittels des Haltearmes
kann man das Rad sehr genau aun das Förderband  justieren.
Der Antrieb erfolgt über eine 20:1 Powermotor.
