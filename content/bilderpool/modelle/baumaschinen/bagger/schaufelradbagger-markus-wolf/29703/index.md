---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger01.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29703
- /details3e09.html
imported:
- "2019"
_4images_image_id: "29703"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29703 -->
Hier mal mein fast fertiger Schaufelradbagger. Es fehlen noch die Baggerbrücke,der Führerstand und die Verkabelung. Das Fundament des Bagger ist mangels Alu aus 15 mm Birkensperrholz.Die Umzäunung deutet die Form an.Darauf ist eine Schwarze Bauplatte geschraubt auf der die Oberkonstruktion ruht.
