---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger02.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29704
- /detailsc3a8.html
imported:
- "2019"
_4images_image_id: "29704"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29704 -->
Hier das FT Männle bei der Arbeit. Darunter das grüne Teil in dem BSB Halter ist ein 12V Maxon Motor den ich aus einem Amerikanischen Moving Lite ausgebaut habe. Das Teil hat Power,der für den Hauptausleger aber auch nötig ist.
