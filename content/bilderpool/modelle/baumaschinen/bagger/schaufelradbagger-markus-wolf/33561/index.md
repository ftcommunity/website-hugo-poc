---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger17.jpg"
weight: "30"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33561
- /details5877.html
imported:
- "2019"
_4images_image_id: "33561"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33561 -->
Hier das Bang von oben.
Ich würde die Umlenkachse im Nachhinein irgendwie  im Winkel  justierbar machen,
so das man den  links/rechts Lauf des Bandes einstellen kann.
