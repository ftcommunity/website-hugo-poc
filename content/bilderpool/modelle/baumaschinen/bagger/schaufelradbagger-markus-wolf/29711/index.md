---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger09.jpg"
weight: "9"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29711
- /details1852-2.html
imported:
- "2019"
_4images_image_id: "29711"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29711 -->
Das Schaufelrad nimmt Schüttgut (z.B. S-Riegel) auf und befördert es weiter.In den Schaufelkammern kann man  die schräg eingesetzte Baublatte erkennen die das Schüttgut auf das Förderband leitet.
