---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger15.jpg"
weight: "28"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33559
- /details225e-2.html
imported:
- "2019"
_4images_image_id: "33559"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33559 -->
Hier die fünf Antriebe.
Der Bagger fuhr damit erstaunlich flott.
Größtes Problem der Fahrwerke waren allerdings die Raupenbeläge
die sich gerne mal lösen. Die alten Ketten mit den aufschiebbaren
Belägen wären da eindeutig die bessere Wahl. Bei der Ausstellung
in Münster sind mir 5 mal Kettenglieder abgegangen und einmal ein Kette offen.
Für eine Dauerbetrieb also gar nich so schlecht  ;)
