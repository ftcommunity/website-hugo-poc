---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger04.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29706
- /details258f-2.html
imported:
- "2019"
_4images_image_id: "29706"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29706 -->
Die Drehkänze sind mittels kleiner Bauplatten von unten an die schwarze Sperrholzplatte montiert.Dort befindet sich auch der Scheckenmechanismus der Lenkung. 5 Powermotor-angetriebene Kettefahrwerke bewegen das Teil. Nur die Lenkung habe ich unter Last noch nicht probiert.
