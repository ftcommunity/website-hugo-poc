---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger08.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29710
- /details7ab1-2.html
imported:
- "2019"
_4images_image_id: "29710"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29710 -->
Hier der Antrieb des Hauptförderbandes.Das Fördeband besteht aus sehr dünnem Gummituch,wie wir es im Orgelbau auch verwenden.Die Gewebeseite läuft außen. Die gummierte Seite innen.Der Antrieb erfolgt über 3 auf die Antriebsachse  gesteckte 20 mm Rollen die wiederum mit einem 120er schleifpapier beklebt sind
