---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger06.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29708
- /detailsa07e.html
imported:
- "2019"
_4images_image_id: "29708"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29708 -->
Das Rad läuft ausschließlich auf dem Drehkranz .Etwas Silikonspray macht die Sache noch geschmeidiger.Das Ganze kann mann nach den lösen der Kette mit einem Handgriff samt Halter abnehmen.
