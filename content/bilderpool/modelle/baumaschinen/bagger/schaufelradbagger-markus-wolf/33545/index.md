---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger01_2.jpg"
weight: "14"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33545
- /details97af.html
imported:
- "2019"
_4images_image_id: "33545"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33545 -->
Hier die Vorlagenskizze.
