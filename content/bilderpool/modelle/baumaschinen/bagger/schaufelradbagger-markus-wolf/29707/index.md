---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger05.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29707
- /detailsbdfc.html
imported:
- "2019"
_4images_image_id: "29707"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29707 -->
110 cm hoch und 165 cm lang und 12,9 kilo schwer.
