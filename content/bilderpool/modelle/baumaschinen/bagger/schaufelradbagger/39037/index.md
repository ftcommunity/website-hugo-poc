---
layout: "image"
title: "Stützen"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger13.jpg"
weight: "17"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39037
- /details6716.html
imported:
- "2019"
_4images_image_id: "39037"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39037 -->
Ein Maßstabsgetreues, kräftiges Fahrwerk war mir zu aufwändig, die Bagger fahren (in echt) aber eh nur selten.