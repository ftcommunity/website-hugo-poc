---
layout: "image"
title: "Im Einsatz"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger04.jpg"
weight: "8"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39028
- /detailsa173.html
imported:
- "2019"
_4images_image_id: "39028"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39028 -->
Das Schaufelrad wirft ziemlich viel daneben...