---
layout: "image"
title: "Heckauslegerende"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger14.jpg"
weight: "18"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39038
- /details4329.html
imported:
- "2019"
_4images_image_id: "39038"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39038 -->
In den Batteriekästen sind Metallkugeln, und das Gewicht des Akkus wird auch noch gebraucht, socnst würde der Bagger nach vorne Kippen.