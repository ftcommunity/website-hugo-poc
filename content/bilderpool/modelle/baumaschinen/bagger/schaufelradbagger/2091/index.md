---
layout: "image"
title: "Schaufelradbagger"
date: "2004-01-28T11:31:39"
picture: "schaufelradbagger_25.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/2091
- /detailse3e3.html
imported:
- "2019"
_4images_image_id: "2091"
_4images_cat_id: "229"
_4images_user_id: "14"
_4images_image_date: "2004-01-28T11:31:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2091 -->
