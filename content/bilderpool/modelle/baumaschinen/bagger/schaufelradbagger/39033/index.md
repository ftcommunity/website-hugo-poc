---
layout: "image"
title: "Vorderer Ausleger"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger09.jpg"
weight: "13"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/39033
- /details0f1c.html
imported:
- "2019"
_4images_image_id: "39033"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39033 -->
