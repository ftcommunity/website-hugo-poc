---
layout: "image"
title: "Gewichtsprobleme"
date: "2014-02-21T20:18:35"
picture: "raupenbagger09.jpg"
weight: "9"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38324
- /details1fee.html
imported:
- "2019"
_4images_image_id: "38324"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38324 -->
Ohne untergelegten Bock sieht man eines der Probleme, die ich noch habe:
Die Unterwagen-Konstruktion ist noch nicht stabil genug für das doch recht hohe Gewicht des Oberwagens.
Der Bagger macht die Grätsche! Allerdings hält alles und er fährt auch in diesem Zustand.