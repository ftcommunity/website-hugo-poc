---
layout: "image"
title: "Stecker Modding"
date: "2014-07-26T11:32:13"
picture: "Stecker.jpg"
weight: "17"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/39047
- /details6603-2.html
imported:
- "2019"
_4images_image_id: "39047"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-07-26T11:32:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39047 -->
Mit den Standard-Steckern klappt das natürlich nicht mehr.
Ich habe deswegen die Stecker vom Gehäuse befreit, abgesägt und die Drähte dann drangelötet.
Dann passen die Stecker auf dem Millimeter genau gerade zwischen die Bauteile