---
layout: "image"
title: "von Vorne/Hinten"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen02.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38380
- /details07bc-2.html
imported:
- "2019"
_4images_image_id: "38380"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38380 -->
nochmal die beiden Varianten in einer anderen Ansicht.
Die Motoren sind noch nicht verdrahtet