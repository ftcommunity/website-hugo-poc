---
layout: "image"
title: "Neubau Unterwagen"
date: "2014-06-10T06:55:41"
picture: "Unterwagen_V2.jpg"
weight: "12"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Raupenbagger", "Unterwagen", "Drehkranz", "Schleifring"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38925
- /detailsf969-2.html
imported:
- "2019"
_4images_image_id: "38925"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38925 -->
Da die erste Version des Unterwagens bei Belastung stark nachgegeben hat, habe ich dieses zentrale Teil neu konstruiert.