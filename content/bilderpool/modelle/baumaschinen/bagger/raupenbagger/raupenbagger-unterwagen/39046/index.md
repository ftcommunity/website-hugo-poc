---
layout: "image"
title: "Unterwagen verkabelt"
date: "2014-07-26T11:32:13"
picture: "verkabelt_2.jpg"
weight: "16"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Raupenbagger", "Unterwagen", "Kettenantrieb", "Schleifring"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/39046
- /detailsb7dc-5.html
imported:
- "2019"
_4images_image_id: "39046"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-07-26T11:32:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39046 -->
Wie sie sehen, sehen Sie (fast) nichts. ;-)

Im Unterwagen selbst sind keine Kabel zu sehen, obwohl 5! Motoren verkabelt sind.
Die Kabel oben aus dem Schleifring raus verschwinden später im Raupenbagger Oberwagen