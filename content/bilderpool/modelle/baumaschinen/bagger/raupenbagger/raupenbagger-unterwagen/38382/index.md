---
layout: "image"
title: "Antriebswelle finale Version"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen04.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38382
- /details8217.html
imported:
- "2019"
_4images_image_id: "38382"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38382 -->
Das ist meine finale Version, Ansicht von der Innenseite des Kettenfahrwerks
Die Rastachsen-Kegelzahnräder sind für die auftretenden Kräfte ausreichend. Gerade bei den Kegelzahnrädern muß man auf die Querkräfte aufpassen.
Deswegen läuft das eine Kegelzahnrad auch in einem Kugellager. Das sitzt viel besser als nur in einem 32321 und läuft trotzdem leichtgängig.
Auch die anderen Achsen in dem Bild sind gelagert. Die Kugellager werden in den Schneckenmuttern gehalten.
Wegen der unterschiedlichen Anordnung von Nut und Feder verwende ich unterschiedliche Schneckenmuttern 35973 und 37925. Ziel war es ja die Konstruktion filigran, aber stabil hinzubekommen.