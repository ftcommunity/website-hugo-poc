---
layout: "image"
title: "Kettenfahrwerk Antriebskopf"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen05.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38383
- /details0bc7.html
imported:
- "2019"
_4images_image_id: "38383"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38383 -->
Der Antriebskopf ders Kettenfahrwerks von der Aussenseite betrachtet (ohne Zahnräder)

Die einzelnen Bausteine sind möglichst vielseitig verschachtelt und vernutet, damit die ganze Konstruktion stabil ist.
Die Kette ist vorne ja nur auf einem Z20 gespannt. Dadurch ist zwischen der Kette nicht viel Platz.
Wichtig ist, das nichts an der Kette schleift, Grössere glatte Reibflächen wären ja noch hinnehmbar, aber wenn die Kette an einem Bausteinzapfen scheuern würde, würde es ziemlich hakeln.

Die Wellen am Antriebskopf sind alle kugelgelagert, da hier die höchsten Kräfte auftreten. Um die Querkräfte aufzufangen (vor allem auch durch die gespannte Kette), habe ich Kugellager auf beiden Seiten der Schneckenmuttern eingelegt. Es sind als an einem Antriebskopf 6 Kugellager verbaut.

! Übrigens kann ich euch Kugellager günstig abgeben! Ich habe beide Varianten für beide Schneckenmuttern
