---
layout: "image"
title: "Original..."
date: "2014-02-23T18:04:06"
picture: "Raupenbagger-Vorbild.jpg"
weight: "13"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38372
- /detailse04b.html
imported:
- "2019"
_4images_image_id: "38372"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38372 -->
dieser Bagger war in etwa mein Vorbild