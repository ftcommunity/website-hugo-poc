---
layout: "image"
title: ".... und Nachbau"
date: "2014-02-23T18:04:06"
picture: "Nachbau.jpg"
weight: "14"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38373
- /details9b7b.html
imported:
- "2019"
_4images_image_id: "38373"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38373 -->
ich habe versucht, ein Photo in etwa mit der gleichen Armstellung und Perspektive zu machen.
Für meine erste Modellversion kommt das schon ganz gut hin.
Im Detail sind noch ein paar Unterschiede. Wenn ich die Bilder vergleiche, fällt mir folgendes auf:
- Der Oberwagen sitzt etwas zu hoch. Der Abstand zwischen Ober und Unterwagen müsste kleiner sein
- Der Arm ist etwas zu lang
- Die Raupenketten sind zu lang. Das ist allerdings die Folge meines Antriebskonzeptes und schwierig zu ändern
- Der Oberwagen dürfe hinten noch länger sein. Das hätte dann natürlich den Vorteil, daß ich (falls ich das jemals angehe) für die Pneumatik etwas mehr Platz hätte.