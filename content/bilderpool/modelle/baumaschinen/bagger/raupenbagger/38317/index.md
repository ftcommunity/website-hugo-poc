---
layout: "image"
title: "schräg von oben"
date: "2014-02-21T20:18:35"
picture: "raupenbagger02.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38317
- /details4b1a.html
imported:
- "2019"
_4images_image_id: "38317"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38317 -->
