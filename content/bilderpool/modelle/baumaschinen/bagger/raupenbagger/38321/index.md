---
layout: "image"
title: "Bagger Draufsicht"
date: "2014-02-21T20:18:35"
picture: "raupenbagger06.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38321
- /details2c8c.html
imported:
- "2019"
_4images_image_id: "38321"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38321 -->
Naja, den Deckel muß ich noch ein bisschen verschönern. Durch die Lücke hinten kann man durchschauen. Das Loch werde ich wahrscheinlich mit Statikstreben zupflastern,
Dieses Bild ist eigentlich die einzige Ansicht, in der man noch was vom Schleifring in der Mitte sieht.