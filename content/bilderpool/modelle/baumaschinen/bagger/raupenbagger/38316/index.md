---
layout: "image"
title: "Gesamtansicht"
date: "2014-02-21T20:18:34"
picture: "raupenbagger01.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38316
- /details85d0.html
imported:
- "2019"
_4images_image_id: "38316"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38316 -->
Der Unterwagen ist voll funktionsfähig. Details dazu später.
Oberwagen und Unterwagen sind durch einen Drehkranz mit integriertem Schleifring gekoppelt (6 Kontakte).
Im Oberwagen befinden sich die Akkus und der Empfangsteil für die  Fernsteuerung (habe ich zum aktuellen Zeitpunkt noch nicht fertig, deswegen fährt er sofort los, wenn ich einschalte)
Der Baggerarm hat noch keinerlei Funktion. Er dient nur der Optik, um die Proportionen des Modells stimmig darzustellen.
Das wäre endlich einmal ein Anlass für mich, sich mit dem Thema Pneumatik zu beschäftigen.