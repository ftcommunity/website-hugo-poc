---
layout: "image"
title: "Vorlage"
date: "2014-02-26T08:17:56"
picture: "baggerschaufel.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38391
- /details3234.html
imported:
- "2019"
_4images_image_id: "38391"
_4images_cat_id: "2855"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38391 -->
Baggerschaufeln gibt es in 1000 verschiedenen Formen. Von dieser hier habe ich ein ganz gutes Bild in Seitenansicht gefunden (gut als Vorlage zum Maßnehmen).
Die Form ist einigermaßen komplex. Mit normalen ft Bausteinen kann man das nur in einem sehr großen Maßstab nachbauen. 
Ich hab da trotzdem ein paar Ideen, die ich aber noch umsetzen muß 
( @ ft-Puristen: Vorsicht, das wird ein Modding Projekt! )