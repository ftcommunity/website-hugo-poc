---
layout: "image"
title: "Caterpillar-950H-2.0  mit Dreh-Zylinder"
date: "2015-01-24T21:42:56"
picture: "caterpillar01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40397
- /details3f60.html
imported:
- "2019"
_4images_image_id: "40397"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40397 -->
Schaal 1 : 14

Dreh-Zylinder Antrieb Alternativen
Schau mal :     http://www.ftcommunity.de/details.php?image_id=39290

Conrad-art.nrs: 
297313 Messing buis buitenØ5,0 binnenØ4,1 mm,  500mm 
221795 Messing buis buitenØ6,0 binnenØ4,0 mm,  500mm 
297321-62 Messing-Rohr-Profil  6 mm buiten + 5.1 mm binnen , 500 mm
225410 Stelringset 4mm (10 stuks) 
225428 Stelringset 5mm (10 stuks) 
225436 Stelringset 6mm (10 stuks) 
295612 KOGELLAGER inw. 6mm x 12 x 4 
RVS-Draadeinden :           https://www.rvspaleis.nl/
221786 Draadeind 500mm M4 Messing 
221787 Draadeind 500mm M5 Messing 
183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm 
Adapter 4mm auf 5mm Andreas Tacke Münster. Ich mache davon M4 auf 5mm.

M1 = Drehzylinder- Antrieb -Laadbak  dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen
M2 = Drehzylinder-Antrieb -Giek  dmv 2x Schwarze XM-Motor + Ketten-Antrieb + 2x M5 zum hoch und nach unten
M3 = Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4  zum Links und Rechts
Servo =  Fahr-Antrieb (2x Powermotor 20:1) mit Thor4 am servo-Ausgang zum Vor- und -Ruckwärts-gang
