---
layout: "image"
title: "Trick :  FT-Control-set- zender 90 graden kantelen + servo-joystick boven !!!.."
date: "2015-01-24T21:42:56"
picture: "caterpillar08.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40404
- /details7bef.html
imported:
- "2019"
_4images_image_id: "40404"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40404 -->
De praktijk wijst uit dat voor veel modellen slechts één kanaal echt zwaar belast wordt > 1A. 
FT-Control-set bevat één servo-uitgang (= rechter joystick) die dmv. een Thor4 of een Thor4S (=zelf lerend} ook hoge vermogens aan kan.  Via de Thor4-regelaar ( 21 Euro) kan dit goed geregeld worden.
Schau auch mal: 
http://www.cti-modellbau.de/CTI-Fahrregler/-74-102-110-161-224.html?XTCsid=13774bc74906d704b1b9a1be617d5907

http://www.rc-point.nl/index.php?action=search&lang=nl&srchval=thor4

und :    http://forum.ftcommunity.de/viewtopic.php?f=4&t=1460&p=9482&hilit=Thor#p9482

Veelal  heeft de rij-aandrijving  een relatief zware aanloop-stroom waardoor de FT-controller het totaal-vermogen gaat afknijpen.   

Trick :    Wanneer de FT-Control-set- zender 90 graden wordt gekanteld, zit servo-joystick boven !!!.. 
Door dan de servo-joystick naar voor of naar achteren te bewegen, kan dan logisch naar voor en naar achteren worden gereden, zónder dat FT-controller het totaal-vermogen gaat afknijpen 
Schau auch mal im Forum :    http://forum.ftcommunity.de/viewtopic.php?f=4&t=543&p=17818#p17818


M1 = Drehzylinder- Antrieb -Laadbak  dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen
M2 = Drehzylinder-Antrieb -Giek  dmv 2x Schwarze XM-Motor + Ketten-Antrieb + 2xM5 zum hoch und nach unten
M3 = Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4  zum Links und Rechts
Servo =  Fahr-Antrieb (2x Powermotor 20:1) mit Thor4 am servo-Ausgang zum Vor- und -Ruckwärts-gang
