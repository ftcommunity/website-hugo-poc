---
layout: "image"
title: "Drehzylinder Antrieb-Prinzip mit 10 x 12 x 26mm Getriebemotor Pololu geeignet für M4 oder M5"
date: "2015-01-25T20:02:28"
picture: "caterpillardetails06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40418
- /details41a6.html
imported:
- "2019"
_4images_image_id: "40418"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40418 -->
297321-62 Messing-Rohr-Profil 6 mm buiten + 5.1 mm binnen , 500 mm 

297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen,   500mm 

225436 Stelringset 6mm (10 stuks) 

295612 KOGELLAGER inw. 6mm x 12 x 4 

RVS-Draadeinden : https://www.rvspaleis.nl/ 
221786 Draadeind 500mm M4 Messing 
221787 Draadeind 500mm M5 Messing 

183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm 
Adapter 4mm auf 5mm Andreas Tacke Münster. Ich mache davon M4 auf 5mm. 
