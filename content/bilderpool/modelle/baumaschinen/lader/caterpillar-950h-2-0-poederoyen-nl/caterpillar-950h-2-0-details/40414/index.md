---
layout: "image"
title: "Stuur-unit  -zijdelings"
date: "2015-01-25T20:02:27"
picture: "caterpillardetails02.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40414
- /details71e4-2.html
imported:
- "2019"
_4images_image_id: "40414"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40414 -->
