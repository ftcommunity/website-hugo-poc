---
layout: "image"
title: "Drehzylindern für Antrieb -Laadbak dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen"
date: "2015-01-25T20:02:27"
picture: "caterpillardetails05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40417
- /details723b-2.html
imported:
- "2019"
_4images_image_id: "40417"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40417 -->
Dreh-Zylinder Antrieb Alternativen 
Schau mal : http://www.ftcommunity.de/details.php?image_id=39290 

Conrad-art.nrs: 
297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen,   500mm  zum 10 x 12 x 26mm Getriebemotor 

297313 Messing buis buitenØ5,0 binnenØ4,1 mm, 500mm 
221795 Messing buis buitenØ6,0 binnenØ4,0 mm, 500mm 
297321-62 Messing-Rohr-Profil 6 mm buiten + 5.1 mm binnen , 500 mm 
225410 Stelringset 4mm (10 stuks) 
225428 Stelringset 5mm (10 stuks) 
225436 Stelringset 6mm (10 stuks) 
295612 KOGELLAGER inw. 6mm x 12 x 4 
RVS-Draadeinden : https://www.rvspaleis.nl/ 
221786 Draadeind 500mm M4 Messing 
221787 Draadeind 500mm M5 Messing 
183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm 
Adapter 4mm auf 5mm Andreas Tacke Münster. Ich mache davon M4 auf 5mm. 

M1 = Drehzylinder- Antrieb -Laadbak dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen 
M2 = Drehzylinder-Antrieb -Giek dmv 2x Schwarze XM-Motor + Ketten-Antrieb + 2x M5 zum hoch und nach unten 
M3 = Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4 zum Links und Rechts 
