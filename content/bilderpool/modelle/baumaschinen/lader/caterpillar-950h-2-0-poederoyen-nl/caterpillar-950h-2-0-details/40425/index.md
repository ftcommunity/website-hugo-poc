---
layout: "image"
title: "Drehzylinder Antrieb-Prinzip mit 10 x 12 x 26mm Getriebemotor Pololu geeignet für M4 oder M5"
date: "2015-01-25T20:02:28"
picture: "caterpillardetails13.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40425
- /details1356.html
imported:
- "2019"
_4images_image_id: "40425"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40425 -->
6mm Draai-cilinder is voor- en achter Zahnrad-10 voorzien van kogellagers.
Aan achterzijde kan in principe bij het intrekken/indraaien de zuigerstang (ver) doorlopen. 

Dit is een (zeer) belangrijk voordeel voor een zo groot mogelijke slaglengte !!!!!!.... o.a. noodzakelijk voor de 

Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4 zum Links und Rechts

Z10 mit 6mm Bohrung
