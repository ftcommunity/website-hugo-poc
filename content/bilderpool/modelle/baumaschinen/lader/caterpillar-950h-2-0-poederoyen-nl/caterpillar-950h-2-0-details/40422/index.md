---
layout: "image"
title: "Drehzylinder Antrieb-Prinzip mit 10 x 12 x 26mm Getriebemotor Pololu geeignet für M4 oder M5"
date: "2015-01-25T20:02:28"
picture: "caterpillardetails10.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40422
- /details6356-2.html
imported:
- "2019"
_4images_image_id: "40422"
_4images_cat_id: "3029"
_4images_user_id: "22"
_4images_image_date: "2015-01-25T20:02:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40422 -->
De 4mm-bu-3,1mm-bi messingbuis zit over de motor-as, waardoor de aandrijf-belasting op de Fischertechnik-behuizing komt.

Z10 mit 6mm Bohrung 

Inkorten van Bodenplatten 30 x 90 gaat zeer goed met een parket-knipschaar. 
Ook onder verstek door de aangegeven verstekhoeken
