---
layout: "image"
title: "Caterpillar-950H-2.0  + Dreh-Zylinder -micro-Getriebemotoren -Antrieb   Info"
date: "2015-01-24T21:42:56"
picture: "caterpillar09.jpg"
weight: "9"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40405
- /details34f0.html
imported:
- "2019"
_4images_image_id: "40405"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40405 -->
Schaal 1 : 14

Interessante sehr kleine (10 x 12 x26mm) Getriebemotoren passend im FT-Raster gibt es bei : 

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1 

http://www.pololu.com/product/2218 

Overview 

These tiny brushed DC gearmotors are intended for use at 6 V, though in general, these kinds of motors can run at voltages above and below this nominal voltage, so they should comfortably operate in the 3 - 9 V range (rotation can start at voltages as low as 0.5 V). Lower voltages might not be practical, and higher voltages could start negatively affecting the life of the motor. The micro metal gearmotors are available in a wide range of gear ratios-from 5:1 up to 1000:1-and offer a choice between three different motors: high-power (HP), medium-power (MP), and standard. With the exception of the 1000:1 gear ratio versions, all of the micro metal gearmotors have the same physical dimensions, so one version can be easily swapped for another if your design requirements change. 

Ook interessant in 12V -uitvoering (levering vanuit Frankrijk) :
http://www.robotshop.com/eu/en/12v-460rpm-20oz-in-micro-gearmotor.html
 
12 6 - 12 5:1 4900  RB-Sct-805 Servo City 
12 6 - 12 10:1 2600  RB-Sct-553 Servo City 
12 6 - 12 30:1 900  RB-Sct-749 Servo City 
12 6 - 12 50:1 460  RB-Sct-773 Servo City 
12 6 - 12 100:1 270  RB-Sct-722 Servo City 
12 6 - 12 150:1 175  RB-Sct-171 Servo City 
12 6 - 12 210:1 130  RB-Sct-810 Servo City 
12 6 - 12 250:1 110  RB-Sct-697 Servo City 
12 6 - 12 298:1 90  RB-Sct-169 Servo City
