---
layout: "image"
title: "Radlader"
date: "2014-12-26T16:00:29"
picture: "rld1.jpg"
weight: "1"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39985
- /details0650-2.html
imported:
- "2019"
_4images_image_id: "39985"
_4images_cat_id: "3006"
_4images_user_id: "2228"
_4images_image_date: "2014-12-26T16:00:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39985 -->
Antrieb:      Powermotor 20:1
                  Allradantrieb
Lenkung:   Pneumatische Knicklenkung
Steuerung: steuerbar über das Fischertechnik Control Set
Sonstiges:  Schaufel lässt sich pneumatisch anheben und kippen
