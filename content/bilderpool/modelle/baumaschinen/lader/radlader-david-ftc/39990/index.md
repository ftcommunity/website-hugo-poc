---
layout: "image"
title: "Design"
date: "2014-12-26T16:00:29"
picture: "rld6.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39990
- /detailsfe35.html
imported:
- "2019"
_4images_image_id: "39990"
_4images_cat_id: "3006"
_4images_user_id: "2228"
_4images_image_date: "2014-12-26T16:00:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39990 -->
Der Radlader ist ein relativ maßstabsgetreues Modell des Liebherr Radladers L580. Kleine Details wie der Einstieg zum Führerhaus wurden übernommen
