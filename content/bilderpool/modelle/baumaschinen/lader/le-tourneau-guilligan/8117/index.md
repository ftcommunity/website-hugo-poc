---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:39:46"
picture: "baumaschinen03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8117
- /detailsabc6.html
imported:
- "2019"
_4images_image_id: "8117"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:39:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8117 -->
Gesamtansicht ohne Räder
