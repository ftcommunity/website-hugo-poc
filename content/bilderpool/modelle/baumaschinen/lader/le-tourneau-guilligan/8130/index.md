---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:40:01"
picture: "baumaschinen16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8130
- /detailsda3e.html
imported:
- "2019"
_4images_image_id: "8130"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:01"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8130 -->
Hinterachse als Pendelachse
