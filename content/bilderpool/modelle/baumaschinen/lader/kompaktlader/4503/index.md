---
layout: "image"
title: "Kp5"
date: "2005-07-05T21:50:45"
picture: "Kompaktlader_011.jpg"
weight: "7"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4503
- /details72c8.html
imported:
- "2019"
_4images_image_id: "4503"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-05T21:50:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4503 -->
Von hinten
