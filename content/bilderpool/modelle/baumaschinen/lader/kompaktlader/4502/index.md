---
layout: "image"
title: "Steuerung2"
date: "2005-07-05T21:50:45"
picture: "Kompaktlader_007.jpg"
weight: "6"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4502
- /details58b7.html
imported:
- "2019"
_4images_image_id: "4502"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-05T21:50:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4502 -->
Schaltkasten etwas näherer
