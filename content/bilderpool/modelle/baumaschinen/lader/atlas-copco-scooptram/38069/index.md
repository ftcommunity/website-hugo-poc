---
layout: "image"
title: "Kap"
date: "2014-01-13T13:02:35"
picture: "P1130047.jpg"
weight: "12"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/38069
- /detailsc235.html
imported:
- "2019"
_4images_image_id: "38069"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38069 -->
Nog een "slechte" foto van de complete bovenkant.