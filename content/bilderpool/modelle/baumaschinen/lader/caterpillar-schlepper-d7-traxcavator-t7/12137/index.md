---
layout: "image"
title: "Umbau"
date: "2007-10-05T22:34:52"
picture: "DSCN1652.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12137
- /detailsb325.html
imported:
- "2019"
_4images_image_id: "12137"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-05T22:34:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12137 -->
Der Motorblock mit den beiden S-Motoren hat es echt nicht gebracht. Deswegen habe ich anstelle der S-Motoren Power Motoren eingebaut.
