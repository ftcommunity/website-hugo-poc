---
layout: "overview"
title: "Caterpillar Schlepper D7 mit Traxcavator Schaufel Modell T7"
date: 2020-02-22T08:09:54+01:00
legacy_id:
- /php/categories/1079
- /categories0b65.html
- /categories8fd2.html
- /categories3d17.html
- /categoriesa066.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1079 --> 
Das Original wurde von Caterpillar 1937 so gebaut und war eine Vorstufe der heutigen Hydraulik Schlepper.