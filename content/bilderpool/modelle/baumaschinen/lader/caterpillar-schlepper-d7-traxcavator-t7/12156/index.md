---
layout: "image"
title: "Überarbeitet"
date: "2007-10-07T14:25:33"
picture: "DSCN1669.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12156
- /details208b.html
imported:
- "2019"
_4images_image_id: "12156"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-07T14:25:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12156 -->
Das Design hat sich etwas verändert. Das vorherige sah zu protzig aus und passte auch nicht so richtig zum Original.
Die Ketten sitzen jetzt richtig fest. Die zusätzlich angebrachten roten Bauplatten (zwischen den Rädern) sorgen für die notwendige Kettenspannung. Den Sitz habe ich auch weiter angepasst.
Das einzige was mich noch ein wenig stört sind die großen Z30 Zahnräder. Die passen vom Maßstab nicht so richtig zum Modell.
ABER: Ich denke mal das Modell ist jetzt fertig.
