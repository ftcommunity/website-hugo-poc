---
layout: "image"
title: "Kabelfernsteuerung"
date: "2007-10-08T21:42:17"
picture: "DSCN1685.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12172
- /detailsfbcd.html
imported:
- "2019"
_4images_image_id: "12172"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-08T21:42:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12172 -->
