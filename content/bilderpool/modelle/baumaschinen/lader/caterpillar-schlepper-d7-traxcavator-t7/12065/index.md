---
layout: "image"
title: "Komplettes Fahrwerk"
date: "2007-10-02T08:32:54"
picture: "DSCN1618.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12065
- /details7a91.html
imported:
- "2019"
_4images_image_id: "12065"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12065 -->
Hier das Fahrwerk. Die Ketten habe ich vom Modell des Silver Spade übernommen. (Der ist mir ja leider abgestürzt)
;--((
