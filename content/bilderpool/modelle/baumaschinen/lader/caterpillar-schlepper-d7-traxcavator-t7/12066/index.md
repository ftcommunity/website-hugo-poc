---
layout: "image"
title: "Motorblock"
date: "2007-10-02T08:32:54"
picture: "DSCN1620.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12066
- /details0829.html
imported:
- "2019"
_4images_image_id: "12066"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12066 -->
Das ist der Motorblock für die beiden Seilantriebe die die Schaufel bewegen.
