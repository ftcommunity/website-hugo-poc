---
layout: "image"
title: "Modell"
date: "2007-10-02T08:32:54"
picture: "DSCN1630.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12068
- /details8d7a.html
imported:
- "2019"
_4images_image_id: "12068"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12068 -->
Tja, und so soll´s denn mal aussehen wenn alles fertig ist.
Für die Bilder habe ich es mal schnell zusammengebaut.

Maße des Modells:
l = 45 cm
h = 25 cm
b = 22 cm
