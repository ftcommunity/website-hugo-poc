---
layout: "image"
title: "4 wiel aandrijving"
date: "2011-04-30T20:58:18"
picture: "950h_012.jpg"
weight: "29"
konstrukteure: 
- "Ruurd"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30511
- /detailsb4ab.html
imported:
- "2019"
_4images_image_id: "30511"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-30T20:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30511 -->
De aandrijfas gaat weer omhoog. Dit moet nog steviger gemaakt worden