---
layout: "image"
title: "achteras"
date: "2011-04-27T22:02:43"
picture: "pivot_016.jpg"
weight: "15"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30497
- /details0213-2.html
imported:
- "2019"
_4images_image_id: "30497"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-27T22:02:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30497 -->
De aandrijfas loopt door een schanier heen