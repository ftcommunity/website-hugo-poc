---
layout: "image"
title: "Aandrijfing"
date: "2011-04-30T20:58:18"
picture: "950h_007.jpg"
weight: "24"
konstrukteure: 
- "Ruurd"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30506
- /details43f0.html
imported:
- "2019"
_4images_image_id: "30506"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-30T20:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30506 -->
XM motor en powermotor gekoppeld met diff voor de aandrijving. Xm motor rechtstreeks en de powermotor via 2x z20 tandwiel