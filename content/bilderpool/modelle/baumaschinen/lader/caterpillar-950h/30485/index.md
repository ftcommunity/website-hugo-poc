---
layout: "image"
title: "achteraanzicht"
date: "2011-04-27T22:02:30"
picture: "pivot_003.jpg"
weight: "3"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30485
- /details7e63-2.html
imported:
- "2019"
_4images_image_id: "30485"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-27T22:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30485 -->
