---
layout: "image"
title: "hefsysteem"
date: "2011-04-30T20:58:18"
picture: "950h_002.jpg"
weight: "19"
konstrukteure: 
- "Ruurd"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/30501
- /details9bc5-3.html
imported:
- "2019"
_4images_image_id: "30501"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-30T20:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30501 -->
Ketting loopt rond zodat deze niet overslaat. Alle kracht die de motor levert gaat naar de draadeinden