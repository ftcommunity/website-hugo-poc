---
layout: "comment"
hidden: true
title: "14197"
date: "2011-04-28T17:28:41"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Rob, wenn ich dich richtig verstehe (Steckachsen vertragen keine großen Kräfte?) dann beachte, dass am Rad noch eine Untersetzung 1:3 folgt. Das erlaubt es, auch schwache Achsen auf dem Weg bis dahin zu verwenden!

Gruß,
Harald