---
layout: "image"
title: "Original"
date: "2008-03-21T14:11:04"
picture: "BILD0712.jpg"
weight: "26"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13988
- /details60ae-2.html
imported:
- "2019"
_4images_image_id: "13988"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-21T14:11:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13988 -->
Au Backe, ich habe die Rückspiegel vergessen ...

Was die Kinematik der Schaufelbewegung angeht, so habe ich mir etwas die Zähne ausgebissen. Um nach vielen Tagen und Wochen endlich fertig zu werden, habe ich mich für eine Kinematik entschieden die sich bei Radladern weitgehend durchgesetzt hat. Warum Volvo diese Kinematik gewählt hat? Außerdem funktioniert sie im Modell nicht. Gekauft auf der Modellschow Europe in Bemmel 06 - für 15 Euro.
