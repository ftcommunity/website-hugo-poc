---
layout: "image"
title: "000668"
date: "2008-03-20T15:18:00"
picture: "BILD0668.jpg"
weight: "13"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13975
- /details196d.html
imported:
- "2019"
_4images_image_id: "13975"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13975 -->
