---
layout: "image"
title: "000661"
date: "2008-03-20T14:53:54"
picture: "BILD0661.jpg"
weight: "8"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13970
- /details4ae4-3.html
imported:
- "2019"
_4images_image_id: "13970"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T14:53:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13970 -->
