---
layout: "image"
title: "000665"
date: "2008-03-20T14:53:54"
picture: "BILD0665.jpg"
weight: "11"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13973
- /details1c76-2.html
imported:
- "2019"
_4images_image_id: "13973"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T14:53:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13973 -->
