---
layout: "image"
title: "000696"
date: "2008-03-20T15:23:40"
picture: "BILD0696.jpg"
weight: "20"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/13982
- /details3e00-2.html
imported:
- "2019"
_4images_image_id: "13982"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:23:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13982 -->
Das Planetenradgetriebe ist in ähnlicher Form in der Community schon einmal dargestellt worden. Unterschied ist nicht nur der innere Aufbau sondern auch die innenliegende "Felge" die in dem anderen Entwurf fehlt
