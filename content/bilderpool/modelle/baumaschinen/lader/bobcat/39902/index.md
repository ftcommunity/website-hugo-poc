---
layout: "image"
title: "Cabine"
date: "2014-12-08T17:05:15"
picture: "PC080204.jpg"
weight: "9"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39902
- /details1f86.html
imported:
- "2019"
_4images_image_id: "39902"
_4images_cat_id: "2997"
_4images_user_id: "838"
_4images_image_date: "2014-12-08T17:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39902 -->
Beter zicht op de binnenkant