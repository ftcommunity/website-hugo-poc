---
layout: "image"
title: "Aandrijving"
date: "2014-12-08T17:05:15"
picture: "PC080208.jpg"
weight: "13"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39906
- /details407a-2.html
imported:
- "2019"
_4images_image_id: "39906"
_4images_cat_id: "2997"
_4images_user_id: "838"
_4images_image_date: "2014-12-08T17:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39906 -->
Hij wil goed vooruit met 4 motoren. Had niet verwacht dat het zo goed ging. Rijdt perfect en draait perfect