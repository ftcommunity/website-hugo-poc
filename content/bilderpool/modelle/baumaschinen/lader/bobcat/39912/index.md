---
layout: "image"
title: "neigcilinders"
date: "2014-12-08T17:05:15"
picture: "PC080214.jpg"
weight: "19"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/39912
- /detailsda53-3.html
imported:
- "2019"
_4images_image_id: "39912"
_4images_cat_id: "2997"
_4images_user_id: "838"
_4images_image_date: "2014-12-08T17:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39912 -->
Aandrijving voor het kiepen van de bak