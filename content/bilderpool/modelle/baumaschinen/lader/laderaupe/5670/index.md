---
layout: "image"
title: "End-Schalter für Aufreißer (2)"
date: "2006-01-25T16:43:07"
picture: "DSCN0583.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5670
- /detailsb3c7-4.html
imported:
- "2019"
_4images_image_id: "5670"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5670 -->
Hier das Ganze nochmal aus einer anderen Perspective
