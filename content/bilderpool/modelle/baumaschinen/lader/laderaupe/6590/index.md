---
layout: "image"
title: "neu überabeitet (2)"
date: "2006-07-01T21:32:21"
picture: "DSCN0825.jpg"
weight: "31"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6590
- /detailsff77.html
imported:
- "2019"
_4images_image_id: "6590"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-07-01T21:32:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6590 -->
Hier mal ein Blick aus der Sicht des "Fahrers"
