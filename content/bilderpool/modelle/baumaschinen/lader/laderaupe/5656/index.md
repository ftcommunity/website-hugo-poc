---
layout: "image"
title: "Antrieb"
date: "2006-01-25T14:42:52"
picture: "DSCN0531.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5656
- /details7bf4.html
imported:
- "2019"
_4images_image_id: "5656"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T14:42:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5656 -->
von den schwarzen Zahnrädern wird die Kraft auf die roten und dann auf das große Zahnrad übertragen.
