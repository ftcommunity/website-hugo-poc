---
layout: "image"
title: "Antrieb (3)"
date: "2006-01-25T16:42:58"
picture: "DSCN0558.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5658
- /details6e53.html
imported:
- "2019"
_4images_image_id: "5658"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5658 -->
Hier sieht man einen der beiden Antriebs-Power Motoren und den S-Motor der den Aufreißer antreibt.
