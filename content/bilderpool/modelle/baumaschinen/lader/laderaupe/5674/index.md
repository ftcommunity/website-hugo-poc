---
layout: "image"
title: "Ersatzfahrer"
date: "2006-01-25T16:43:07"
picture: "DSCN0601.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5674
- /detailsb06b-2.html
imported:
- "2019"
_4images_image_id: "5674"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5674 -->
Jetzt macht er das.
