---
layout: "image"
title: "End-Schalter für Aufreißer"
date: "2006-01-25T16:43:07"
picture: "DSCN0581.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5669
- /detailsf8da.html
imported:
- "2019"
_4images_image_id: "5669"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5669 -->
Die beiden Schalter stoppen den Motor, damit der Aufreißer die Raupe nicht anhebt. Gut zu erkennen die Platine mit den beiden Dioden.
