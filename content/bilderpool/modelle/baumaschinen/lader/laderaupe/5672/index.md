---
layout: "image"
title: "Sitz"
date: "2006-01-25T16:43:07"
picture: "DSCN0600.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5672
- /details980f-2.html
imported:
- "2019"
_4images_image_id: "5672"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5672 -->
Wer schwer arbeitet soll auch bequem sitzen. .....alles schwarz-rot, auch die Kabel ....
