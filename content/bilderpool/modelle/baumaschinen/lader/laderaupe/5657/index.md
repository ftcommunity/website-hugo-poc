---
layout: "image"
title: "Antrieb (2)"
date: "2006-01-25T14:42:52"
picture: "DSCN0535.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5657
- /detailsfa4b.html
imported:
- "2019"
_4images_image_id: "5657"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T14:42:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5657 -->
Zwei Powermotoren treiben die beiden roten Schnecken an. Diese wiederum zwei Kettenräder.
