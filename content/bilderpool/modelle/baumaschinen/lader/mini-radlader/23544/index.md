---
layout: "image"
title: "Mini-Radlader 2"
date: "2009-03-29T19:07:18"
picture: "Radlader_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23544
- /detailscf57.html
imported:
- "2019"
_4images_image_id: "23544"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23544 -->
Der Radstand ist für stimmige Proportionen leider etwas zu groß. Aber die beiden Mini-Motoren für die Handventile mussten zwingend zwischen die Achsen.