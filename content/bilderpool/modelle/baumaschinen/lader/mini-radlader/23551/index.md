---
layout: "image"
title: "Mini-Radlader 9"
date: "2009-03-29T19:07:18"
picture: "Radlader_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23551
- /detailsc37c.html
imported:
- "2019"
_4images_image_id: "23551"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23551 -->
Der "Starthebel" nach vorn gedrückt startet Empfänger und Kompressor, indem das untere Ende des Hebels den Mini-Taster betätigt. Jetzt kann's losgehen!