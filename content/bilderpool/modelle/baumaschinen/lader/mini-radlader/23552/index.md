---
layout: "image"
title: "Mini-Radlader 10"
date: "2009-03-29T19:07:18"
picture: "Radlader_10.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23552
- /detailsaace-2.html
imported:
- "2019"
_4images_image_id: "23552"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23552 -->
Der Unterboden. Man erkennt gut das kompakte Package der drei Mini-Motoren, der Antriebsachse und der gelenkten Hinterachse mit Servo. Dank des geringen Lenkhubs des Servos berühren die Hinterräder bei maximalem Lenkeinschlag gerade so nicht die Getriebe der seitlichen Mini-Motoren.