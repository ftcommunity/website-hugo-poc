---
layout: "image"
title: "Radlader 2"
date: "2007-07-15T17:49:00"
picture: "radladerstefanl02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11085
- /details9b4a.html
imported:
- "2019"
_4images_image_id: "11085"
_4images_cat_id: "1005"
_4images_user_id: "502"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11085 -->
