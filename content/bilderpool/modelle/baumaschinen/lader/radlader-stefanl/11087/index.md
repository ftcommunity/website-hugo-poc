---
layout: "image"
title: "Radlader 4"
date: "2007-07-15T17:49:00"
picture: "radladerstefanl04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11087
- /details3fd3.html
imported:
- "2019"
_4images_image_id: "11087"
_4images_cat_id: "1005"
_4images_user_id: "502"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11087 -->
Die Schaufel wird über Seilzug gekippt, mit Schnecke hat es nicht geklappt. Die Gummis sind dafür da damit sie auch herunter klappt.
