---
layout: "image"
title: "Antrieb neu"
date: "2006-12-13T21:55:00"
picture: "Radlader11b.jpg"
weight: "11"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7896
- /details982d-2.html
imported:
- "2019"
_4images_image_id: "7896"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-13T21:55:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7896 -->
Habe heute den Antrieb nochmal komplett umgebaut. So gefällt mir das schon viel besser. 
Nach unten bekommt der Motor noch eine Abstützung, wenn ich mit der Pendelachse anfange und weiß, was wo hinkommt.