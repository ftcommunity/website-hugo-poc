---
layout: "image"
title: "Pendelhinterachse (Detail)"
date: "2006-12-19T21:20:02"
picture: "Radlader18b.jpg"
weight: "18"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7960
- /details8c0f.html
imported:
- "2019"
_4images_image_id: "7960"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-19T21:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7960 -->
Die Unterseite der Pendelachse mit Abstützung der Lagerwelle.
