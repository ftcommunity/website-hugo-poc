---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:24"
picture: "Radlader29b.jpg"
weight: "29"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8298
- /detailsdcbd.html
imported:
- "2019"
_4images_image_id: "8298"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8298 -->
Hier nun wie schon erwähnt das Kardangelenk des Vorderachsantriebes (etwa in Bildmitte zu erkennen). Diese Ansicht ist der Blick direkt vor dem Führerhaus nach unten.
