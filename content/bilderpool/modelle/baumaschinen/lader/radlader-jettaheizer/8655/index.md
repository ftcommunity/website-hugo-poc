---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:37:35"
picture: "Radlader38b.jpg"
weight: "38"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8655
- /detailsa327-2.html
imported:
- "2019"
_4images_image_id: "8655"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8655 -->
Maximale Hubhöhe. Die Gewindestäbe für den Kippantrieb der Schaufel sind zu kurz, die Schaufel kann in dieser Hubhöhe nicht mehr nach oben gekippt werden. Hatte leider nicht mehr Gewindestab. Werde mir aber bei Gelegenheit eine neue Gewindestange besorgen und diesen "Schönheitsfehler" beseitigen