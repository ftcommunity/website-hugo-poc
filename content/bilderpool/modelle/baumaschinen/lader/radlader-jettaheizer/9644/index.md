---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:53"
picture: "Radlader67b.jpg"
weight: "67"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9644
- /details432e.html
imported:
- "2019"
_4images_image_id: "9644"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9644 -->
Das ganze nochmal ein wenig schärfer. Die beiden Conrad-Motoren sind hinten mit Ketten nach oben an der Querverbindung abgefangen.