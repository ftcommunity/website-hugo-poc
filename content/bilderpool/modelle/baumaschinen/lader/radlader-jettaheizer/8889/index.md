---
layout: "image"
title: "Vorderachse neu"
date: "2007-02-04T19:01:20"
picture: "Radlader59b.jpg"
weight: "59"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8889
- /detailsb5e1-2.html
imported:
- "2019"
_4images_image_id: "8889"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-04T19:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8889 -->
Ein Blick ins Innere. Befestigt ist die Vorderachse an 4 S-Riegeln und 4 Bausteinen. Sollte reichen. Die linke der beiden Antriebsachsen mußte ich in der Länge anpassen.