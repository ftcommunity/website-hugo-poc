---
layout: "image"
title: "Pendelhinterachse neu"
date: "2007-02-04T19:01:19"
picture: "Radlader52b.jpg"
weight: "52"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8882
- /detailsa515-2.html
imported:
- "2019"
_4images_image_id: "8882"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-04T19:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8882 -->
irgendwo da durch die Mitte mußte ja noch die 20mm-Achse!
Also nochmal von vorne...