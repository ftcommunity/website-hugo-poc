---
layout: "image"
title: "Radlader02"
date: "2006-12-07T22:48:31"
picture: "Radlader02b.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7714
- /details38ca.html
imported:
- "2019"
_4images_image_id: "7714"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-07T22:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7714 -->
Momentan steht die Konstruktion auf Stützen, weil ich immernoch an den Hebellängen für die Kippmechanik tüftele. Aber man kann schon grob abschätzen, wie groß der Radlader wird.
Hoffentlich find ich passende Räder...

btw: im Hintergrund ist übrigens der größte Teil meines Materials zu sehen.