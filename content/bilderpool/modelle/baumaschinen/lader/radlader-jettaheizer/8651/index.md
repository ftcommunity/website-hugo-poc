---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:37:35"
picture: "Radlader34b.jpg"
weight: "34"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8651
- /detailsad57-2.html
imported:
- "2019"
_4images_image_id: "8651"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8651 -->
Das ganze nochmal von unten.
Sieht nicht besonders stabil aus, ist es aber, weil die Platten 30x90 auf Druck in Längsrichtung belastet werden.