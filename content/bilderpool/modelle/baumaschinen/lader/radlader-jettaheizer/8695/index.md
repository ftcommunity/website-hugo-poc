---
layout: "image"
title: "Radlader"
date: "2007-01-25T18:51:20"
picture: "Radlader49b.jpg"
weight: "49"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8695
- /details3fe1.html
imported:
- "2019"
_4images_image_id: "8695"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-25T18:51:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8695 -->
Geländegängig...