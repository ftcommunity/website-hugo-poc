---
layout: "image"
title: "Antrieb neu"
date: "2007-02-12T18:45:45"
picture: "Radlader60b.jpg"
weight: "60"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9014
- /detailsb299.html
imported:
- "2019"
_4images_image_id: "9014"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-12T18:45:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9014 -->
Hier die vorerst endgültige Version des neuen Antriebes. Hat mich reichlich Nerven gekostet...