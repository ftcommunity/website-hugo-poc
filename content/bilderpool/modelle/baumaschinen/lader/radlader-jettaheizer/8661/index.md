---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:38:07"
picture: "Radlader44b.jpg"
weight: "44"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8661
- /details915f.html
imported:
- "2019"
_4images_image_id: "8661"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:38:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8661 -->
Gleiche Stelle, andere Perspektive