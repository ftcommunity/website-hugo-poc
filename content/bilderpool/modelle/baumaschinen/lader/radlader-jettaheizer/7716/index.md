---
layout: "image"
title: "Radlader Schaufel"
date: "2006-12-07T22:48:31"
picture: "Radlader04b.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7716
- /details4c06-3.html
imported:
- "2019"
_4images_image_id: "7716"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-07T22:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7716 -->
In der großen Schaufel steht der leicht modifizierte Radlader aus der Start 200-Anleitung, der wiederum in seiner Schaufel die einzige Vorlage für den großen Radlader trägt: einen Zettelmeyer ZL 1801 von Kibri im Maßstab 1:87.