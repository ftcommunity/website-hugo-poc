---
layout: "image"
title: "Vorderachse neu"
date: "2007-02-04T19:01:20"
picture: "Radlader58b.jpg"
weight: "58"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8888
- /details9aff.html
imported:
- "2019"
_4images_image_id: "8888"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-04T19:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8888 -->
Die Vorderachse ist analog zur Hinterachse aufgebaut. Die einzigen Unterschiede sind die Zuführung der Antriebsachsen von oben und das fehlen der Pendelaufhängung.