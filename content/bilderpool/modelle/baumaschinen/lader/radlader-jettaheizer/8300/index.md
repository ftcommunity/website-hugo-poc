---
layout: "image"
title: "Radlader"
date: "2007-01-03T19:25:27"
picture: "Radlader31b.jpg"
weight: "31"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8300
- /details65bd.html
imported:
- "2019"
_4images_image_id: "8300"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8300 -->
Und so könnte er mal aussehen. Man kann hier schon ganz gut die Dimensionen erahnen, in denen sich das Modell bewegt.
Lust auf ein Suchspiel? In diesem Bild sind 4 ft-Männchen versteckt... ;o)
(okay, ist nicht wirklich schwer...)
Es ist allerdings noch ein weiter Weg, bis ich sagen kann "Ich habe fertig".
