---
layout: "comment"
hidden: true
title: "1878"
date: "2007-01-04T21:29:09"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Jürgen,

man soll es nicht glauben, aber auch in einem solch großen Modell kann es eng werden...
Die Kette samt zugrhörigem Ritzel sitzt oben unmittelbar vor der Lenkmechanik, da hätte ich mit Kardan o.ä. nichts machen können.
Wie Du siehst, mußte ich sogar unten aus dem Rahmen eine Querverbindung herausnehmen (da, wo jetzt die Platte 15x90 sitzt), weil ich sonst keinen Platz für die Antriebswelle gehabt hätte...

@Thomas & Stefan: da habt ihr Recht, wird geändert!

Gruß,
Franz