---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader24b.jpg"
weight: "24"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8293
- /details4838-2.html
imported:
- "2019"
_4images_image_id: "8293"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8293 -->
Detailansicht des Hinterachsantriebes (eine von zwei Antriebswellen). Dieses Gebilde gefällt mir ganz und garnicht, das werd ich noch ändern.
