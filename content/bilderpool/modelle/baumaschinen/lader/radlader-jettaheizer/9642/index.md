---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:53"
picture: "Radlader65b.jpg"
weight: "65"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9642
- /detailsdd76.html
imported:
- "2019"
_4images_image_id: "9642"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9642 -->
