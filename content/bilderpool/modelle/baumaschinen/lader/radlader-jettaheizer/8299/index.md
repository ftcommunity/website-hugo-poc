---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:24"
picture: "Radlader30b.jpg"
weight: "30"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
schlagworte: ["Monsterreifen"]
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8299
- /details3b46-3.html
imported:
- "2019"
_4images_image_id: "8299"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8299 -->
Größenvergleich zwischen einem Vorderrad und einem ft-Männchen.
