---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:38:07"
picture: "Radlader43b.jpg"
weight: "43"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8660
- /detailse795.html
imported:
- "2019"
_4images_image_id: "8660"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:38:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8660 -->
Kippantrieb der Schaufel im Detail. Hier ist die Unterseite zu sehen. Die Kettenführung ist etwas unkonventionell, aber das war notwendig