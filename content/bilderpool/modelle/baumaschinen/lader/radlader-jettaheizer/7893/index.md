---
layout: "image"
title: "Lenkmechanik und Antrieb"
date: "2006-12-12T21:57:02"
picture: "Radlader08b.jpg"
weight: "8"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7893
- /detailsbf54.html
imported:
- "2019"
_4images_image_id: "7893"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-12T21:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7893 -->
Unter dem Antrieb ist übrigens der Minimot zu sehen, der für die Lenkung zuständig ist.