---
layout: "image"
title: "Antrieb neu"
date: "2007-02-12T18:45:45"
picture: "Radlader61b.jpg"
weight: "61"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9015
- /details0c4b.html
imported:
- "2019"
_4images_image_id: "9015"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-02-12T18:45:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9015 -->
Die Ansicht von hinten. Oben die Welle des oberen Mitteldifferentials, darunter (das Z10) das Zentraldifferential und unten das untere Mitteldifferential. Links wird der zweite Antriebsmotor montiert, sobald ich die Anbauplatte habe.