---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:53"
picture: "Radlader70b.jpg"
weight: "70"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9647
- /details6ca0.html
imported:
- "2019"
_4images_image_id: "9647"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9647 -->
Der vordere Bereich er Hinterachsaufhängung. Diese Konstruktion hat genau eine Schwachstelle: die Verbindung des grauen BS30 Loch mit den beiden Riegelsteinen (etwa in Bildmitte zu sehen). Der graue Baustein hält mit zwei Federnocken nicht wirklich fest in den Riegelsteinen. Allerdings ist das nur dann relevant, wenn man das Modell anhebt, da im Normalzustand das Gewicht des Modells auf diese Stelle drückt.

P.S.: auf diesen Fotos steht das Modell auf Böcken, die ich mittig unter die Achsen gestellt habe. So läßt sich der Antrieb erstmal ohne die Belastung durch das Modell testen.