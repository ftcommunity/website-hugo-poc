---
layout: "image"
title: "Radlader"
date: "2007-01-25T18:51:20"
picture: "Radlader50b.jpg"
weight: "50"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8696
- /details20b9.html
imported:
- "2019"
_4images_image_id: "8696"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-25T18:51:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8696 -->
Ohne Karosserie können die Hinterräder ca. 22cm Höhenunterschied gutmachen. Ich werde das aber mechanisch auf ca. 10cm begrenzen, wenn die Karosserie mit Kotflügeln dran ist