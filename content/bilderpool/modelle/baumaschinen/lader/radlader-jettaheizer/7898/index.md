---
layout: "image"
title: "Antrieb neu"
date: "2006-12-13T21:55:01"
picture: "Radlader13b.jpg"
weight: "13"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7898
- /detailsb0ec-2.html
imported:
- "2019"
_4images_image_id: "7898"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-13T21:55:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7898 -->
Die beiden Mitteldifferentiale werden jetzt vom längs eingebauten Motor über ein Rast-Z10 angetrieben.