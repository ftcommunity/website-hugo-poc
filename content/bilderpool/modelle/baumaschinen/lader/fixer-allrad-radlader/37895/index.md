---
layout: "image"
title: "Schaufel oben"
date: "2013-12-02T12:57:36"
picture: "fixerradlader1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37895
- /detailsd419.html
imported:
- "2019"
_4images_image_id: "37895"
_4images_cat_id: "2817"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37895 -->
Die Schaufel läßt sich schnell kippen und der Schnecken-Hub hat eine gute Kraftentfaltung. Die Knicklenkung wird direkt mit dem Servo angetrieben und machen das Modell sehr wendig. Je nach Untergrund kann man ein Rad der starren Achsen frei drehen lassen - das tut der Schubkraft des Modells keinen Abbruch. Das Modell ist nach dem Motto aufgebaut: soviel Motoren wie möglich auf kleinstem Platz!

Der Lader erreicht eine recht ordentliche Hubhöhe und durch die Motoren kann die Schaufel sehr gut entleert werden. Die beiden Motoren machen es sehr geländegängig und flott.

Viel Spaß beim Nachbauen/Weiterbauen!