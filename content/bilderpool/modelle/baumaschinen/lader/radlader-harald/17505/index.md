---
layout: "image"
title: "ExplorerMk2-33.JPG"
date: "2009-02-24T11:32:50"
picture: "ExplorerMk2-33.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17505
- /details81da.html
imported:
- "2019"
_4images_image_id: "17505"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:32:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17505 -->
Die Einzelteile der Servolenkung. Die Minitaster muss man soweit zusammenschieben, dass in Ruhelage gerade eben keiner von ihnen gedrückt wird. In begrenztem Maße kann man auch die Null-Lage damit verstellen.
