---
layout: "image"
title: "ExplorerMk2-29.JPG"
date: "2009-02-24T11:27:15"
picture: "ExplorerMk2-29.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17503
- /details8c1e-2.html
imported:
- "2019"
_4images_image_id: "17503"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:27:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17503 -->
Die Mechanik zum Kippen der Schaufel. Die zweite K-Achse ist nötig, damit das Gelenk beim Auskippen nicht überstreckt wird und in eine Totpunktlage gerät.
