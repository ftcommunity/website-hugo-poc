---
layout: "image"
title: "2"
date: "2009-01-17T13:23:39"
picture: "rhvonterex02.jpg"
weight: "1"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/17019
- /details8ad4.html
imported:
- "2019"
_4images_image_id: "17019"
_4images_cat_id: "1530"
_4images_user_id: "822"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17019 -->
