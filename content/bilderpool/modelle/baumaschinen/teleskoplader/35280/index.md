---
layout: "image"
title: "Teleskoplader 010"
date: "2012-08-10T17:56:38"
picture: "teleskoplader10.jpg"
weight: "10"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35280
- /details2d88-2.html
imported:
- "2019"
_4images_image_id: "35280"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35280 -->
Hier sieht man die drei Zahnräder der Lenkung.