---
layout: "image"
title: "'Gerippe' von oben"
date: "2018-02-14T16:38:04"
picture: "telbly15.jpg"
weight: "37"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47283
- /detailsb653.html
imported:
- "2019"
_4images_image_id: "47283"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47283 -->
Fahrzeug ohne Räder, Teleskopausleger und Elektronik
