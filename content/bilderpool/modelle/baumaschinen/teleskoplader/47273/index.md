---
layout: "image"
title: "Wifi-Controller"
date: "2018-02-14T16:38:04"
picture: "telbly05.jpg"
weight: "27"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47273
- /details2511.html
imported:
- "2019"
_4images_image_id: "47273"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47273 -->
Dieser Wifi Controller verbindet Fischertechnik Modelle mit dem Internet der Dinge. Herzstück ist der WLan fähige Mikrocontroller Particle Photon, der auf das Board gesteckt wird.
In Blynk kann man seine eignen Fernsteuerungsoberfläche entwerfen und über Smartphone Fischertechnik Modelle fernsteuern. Im nächsten Bild ist die Fernsteuerung abgebildet.

Video zum Controller: https://youtu.be/jBZQri6zQZs
