---
layout: "image"
title: "Teleskoplader 022"
date: "2012-08-10T17:56:38"
picture: "teleskoplader22.jpg"
weight: "22"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35292
- /detailsc93a.html
imported:
- "2019"
_4images_image_id: "35292"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35292 -->
Anstatt der Staplergabel, kann man auch eine Schaufel an der Kippmechanik befestigen!!!
Geplant ist auch noch eine Seilwinde.