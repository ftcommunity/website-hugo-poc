---
layout: "image"
title: "Teleskoplader 020"
date: "2012-08-10T17:56:38"
picture: "teleskoplader20.jpg"
weight: "20"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35290
- /detailsf81c-3.html
imported:
- "2019"
_4images_image_id: "35290"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35290 -->
Leider war im Führerhaus kein Platz mehr für einen Sitz und eine FT-Figur,
da die beiden IR-Empfänger auch noch untergebracht werden mussten.