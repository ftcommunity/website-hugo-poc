---
layout: "image"
title: "Teleskoplader 013"
date: "2012-08-10T17:56:38"
picture: "teleskoplader13.jpg"
weight: "13"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35283
- /details0112.html
imported:
- "2019"
_4images_image_id: "35283"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35283 -->
Der Arm ist mit zwei Gelenkwürfeln auf dem XM-Mot angemacht.
Das ist leider nicht gerade sehr stabil, aber es ging nicht anders!!! :-)