---
layout: "image"
title: "Fernsteuerung"
date: "2018-02-14T16:38:04"
picture: "telbly06.jpg"
weight: "28"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47274
- /details38ae-2.html
imported:
- "2019"
_4images_image_id: "47274"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47274 -->
Mit der App Blynk (iOS / Android) wird das Modell gesteuert. Die Oberfläche lässt sich frei gestalten:

Funktionen:
- Auswahl des Lenkmodus
- Live-Anzeige des Lenkeinschlags der beiden Achsen (hier 12° in die selbe Richtung, also Hundeganglenkung)
- Trimmen der Lenkservos
- Licht (unten links, verdeckt)
- Pneumatik
- Joystick zum Steuern (unten rechts, verdeckt)

Video zur Fernsteuerung: https://youtu.be/jBZQri6zQZs
