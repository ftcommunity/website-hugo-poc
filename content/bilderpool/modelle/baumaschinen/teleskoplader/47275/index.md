---
layout: "image"
title: "Pneumatik"
date: "2018-02-14T16:38:04"
picture: "telbly07.jpg"
weight: "29"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47275
- /detailse149-2.html
imported:
- "2019"
_4images_image_id: "47275"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47275 -->
zwei Zylinder heben den Arm an
