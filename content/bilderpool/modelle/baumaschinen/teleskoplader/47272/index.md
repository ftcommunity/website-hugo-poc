---
layout: "image"
title: "rechte Seite"
date: "2018-02-14T16:38:04"
picture: "telbly04.jpg"
weight: "26"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47272
- /details929d.html
imported:
- "2019"
_4images_image_id: "47272"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47272 -->
Der Controller befindet sich über dem Druckluftspeicher
