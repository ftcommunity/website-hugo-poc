---
layout: "image"
title: "Teleskoplader 012"
date: "2012-08-10T17:56:38"
picture: "teleskoplader12.jpg"
weight: "12"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35282
- /details1031.html
imported:
- "2019"
_4images_image_id: "35282"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35282 -->
Der Teleskoparm besteht aus zwei Aluprofilen, die übereinander liegen.
Das obere Aluprofil kann mit einem Hubgetriebe ausgefahren werden.