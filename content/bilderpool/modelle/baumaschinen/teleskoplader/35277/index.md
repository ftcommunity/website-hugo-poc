---
layout: "image"
title: "Teleskoplader 007"
date: "2012-08-10T17:56:37"
picture: "teleskoplader07.jpg"
weight: "7"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- /php/details/35277
- /details15f8.html
imported:
- "2019"
_4images_image_id: "35277"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35277 -->
Hier sieht man die Kippmechanik für Anschlussgeräte.
Im Moment ist eine Staplergabel montiert. Ganz oben ist der XS-Motor der die Schnecke antreibt.
Beim Übergang vom Motor zur Schnecke musste ich leider etwas improvisieren und habe die Verbindung
desshalb mit einem blauen Pneumatik Schlauch gemacht!!!