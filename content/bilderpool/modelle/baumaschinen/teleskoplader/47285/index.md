---
layout: "image"
title: "Achsaufhängung"
date: "2018-02-14T16:38:04"
picture: "telbly17.jpg"
weight: "39"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47285
- /detailsf7c3.html
imported:
- "2019"
_4images_image_id: "47285"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47285 -->
Die Achsaufhängung beansprucht nur 30mm pro Seite dank aufgebohrter Lochsteine 15
