---
layout: "image"
title: "Teleskoparm"
date: "2018-02-14T16:38:04"
picture: "telbly02.jpg"
weight: "24"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47270
- /detailsb942.html
imported:
- "2019"
_4images_image_id: "47270"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47270 -->
nicht vollständig ausgefahren, aber maximaler Winkel
