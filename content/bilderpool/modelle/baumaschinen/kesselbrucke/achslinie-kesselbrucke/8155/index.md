---
layout: "image"
title: "diverse Achskonstruktion für die Kesselbrücke"
date: "2006-12-26T15:40:22"
picture: "achsliniefuerdiekesselbruecke10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8155
- /detailsdb9e.html
imported:
- "2019"
_4images_image_id: "8155"
_4images_cat_id: "753"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8155 -->
schlechte Konstruktion - da zu breit
