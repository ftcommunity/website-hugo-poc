---
layout: "image"
title: "diverse Achskonstruktion für die Kesselbrücke"
date: "2006-12-26T15:40:18"
picture: "achsliniefuerdiekesselbruecke04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/8149
- /details72b2.html
imported:
- "2019"
_4images_image_id: "8149"
_4images_cat_id: "753"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8149 -->
