---
layout: "image"
title: "Caterpillar D11T"
date: "2008-12-23T15:26:43"
picture: "caterpillardt1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/16702
- /detailsae7f-2.html
imported:
- "2019"
_4images_image_id: "16702"
_4images_cat_id: "1512"
_4images_user_id: "502"
_4images_image_date: "2008-12-23T15:26:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16702 -->
