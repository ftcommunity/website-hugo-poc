---
layout: "image"
title: "Caterpillar D11T"
date: "2008-12-23T15:26:44"
picture: "caterpillardt2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/16703
- /detailsabd3.html
imported:
- "2019"
_4images_image_id: "16703"
_4images_cat_id: "1512"
_4images_user_id: "502"
_4images_image_date: "2008-12-23T15:26:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16703 -->
