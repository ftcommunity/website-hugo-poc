---
layout: "image"
title: "Gesamtansicht"
date: 2023-03-03T11:02:22+01:00
picture: "DO-Hanomag-15.jpeg"
weight: "15"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Raupe mit geöffneter Motorhaube