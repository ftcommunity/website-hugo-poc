---
layout: "image"
title: "Raupe mit geöffneter Motorhaube"
date: 2023-03-03T11:02:23+01:00
picture: "DO-Hanomag-14.jpeg"
weight: "14"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Unter der geöffneten Motorhaube ist die Seilwinde zu sehen.