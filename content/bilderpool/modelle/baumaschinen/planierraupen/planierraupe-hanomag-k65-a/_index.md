---
layout: "overview"
title: "Planierraupe Hanomag K65 A"
date: 2023-03-03T11:02:12+01:00
---

Produktionszeitraum: 1956 bis 1964. 

Entschieden habe ich mich für das Modell, weil die Aufhängung des Schiebeschilds so genial war, dass es mit fischertechnik leicht zu konstruieren ist.
Der Hydraulikzylinder schiebt einen Querriegel an, der das Schild hebt. 
An dem Querriegel auf beiden Seiten habe ich ein Alu-Profil unter der Motorhaube eingebaut, das diese beiden verbindet. Das Schiebeschild wird mit einer Seilwinde hoch und runter gezogen.
Die ganze Konstruktion ist unsichtbar unter der Motorhaube untergebracht.
