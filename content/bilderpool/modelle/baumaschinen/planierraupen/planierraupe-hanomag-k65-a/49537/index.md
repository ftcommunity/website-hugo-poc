---
layout: "image"
title: "Querriegel"
date: 2023-03-03T11:02:18+01:00
picture: "DO-Hanomag-4.jpeg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Querriegel mit einem Stück vom Seil
