---
layout: "image"
title: "Ansicht des Kettenlaufwerks von oben"
date: 2023-03-03T11:02:15+01:00
picture: "DO-Hanomag-6.jpeg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

