---
layout: "image"
title: "Schiebeschild von oben"
date: 2023-03-03T11:02:13+01:00
picture: "DO-Hanomag-8.jpeg"
weight: "8"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

