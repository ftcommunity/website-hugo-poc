---
layout: "image"
title: "grosserbulldozer17.jpg"
date: "2011-07-13T14:32:41"
picture: "grosserbulldozer17.jpg"
weight: "17"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31065
- /details09ed.html
imported:
- "2019"
_4images_image_id: "31065"
_4images_cat_id: "2319"
_4images_user_id: "833"
_4images_image_date: "2011-07-13T14:32:41"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31065 -->
gut zu sehen sind die beweglich gelagerten Laufrollen