---
layout: "image"
title: "grosserbulldozer24.jpg"
date: "2011-07-13T14:32:41"
picture: "grosserbulldozer24.jpg"
weight: "24"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31072
- /details28dc.html
imported:
- "2019"
_4images_image_id: "31072"
_4images_cat_id: "2319"
_4images_user_id: "833"
_4images_image_date: "2011-07-13T14:32:41"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31072 -->
