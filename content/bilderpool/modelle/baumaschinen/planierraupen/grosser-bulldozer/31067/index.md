---
layout: "image"
title: "grosserbulldozer19.jpg"
date: "2011-07-13T14:32:41"
picture: "grosserbulldozer19.jpg"
weight: "19"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31067
- /details0201.html
imported:
- "2019"
_4images_image_id: "31067"
_4images_cat_id: "2319"
_4images_user_id: "833"
_4images_image_date: "2011-07-13T14:32:41"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31067 -->
