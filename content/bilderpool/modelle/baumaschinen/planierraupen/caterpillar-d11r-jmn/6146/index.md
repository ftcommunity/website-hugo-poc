---
layout: "image"
title: "Caterpillar D11R Klaue"
date: "2006-04-27T13:15:36"
picture: "05_Caterpillar_D11R_Klaue.jpg"
weight: "5"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6146
- /details4afa.html
imported:
- "2019"
_4images_image_id: "6146"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6146 -->
