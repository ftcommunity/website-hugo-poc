---
layout: "image"
title: "Caterpillar D11R mit Mann"
date: "2006-04-27T13:15:47"
picture: "12_Caterpillar_D11R_mit_mannchen.jpg"
weight: "12"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6153
- /details9f6d-2.html
imported:
- "2019"
_4images_image_id: "6153"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6153 -->
