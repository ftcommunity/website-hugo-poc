---
layout: "image"
title: "Detail Kettenaufhängung"
date: "2006-11-05T14:39:25"
picture: "Bulldozer7.jpg"
weight: "7"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7409
- /detailsc884.html
imported:
- "2019"
_4images_image_id: "7409"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7409 -->
Hier die Kettenaufhängung im Detail. Da das Modell relativ groß ist hab ich Z30 genommen.
