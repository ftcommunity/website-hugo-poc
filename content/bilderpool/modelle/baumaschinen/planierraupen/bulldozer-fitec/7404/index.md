---
layout: "image"
title: "Hinteransicht"
date: "2006-11-05T14:39:25"
picture: "Bulldozer2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7404
- /detailsd4a8.html
imported:
- "2019"
_4images_image_id: "7404"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7404 -->
Hier sieht man den Bulldozer von hinten.
