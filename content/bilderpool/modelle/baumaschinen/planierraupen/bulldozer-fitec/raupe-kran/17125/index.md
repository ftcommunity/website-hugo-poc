---
layout: "image"
title: "Raupenhälfte unten 2"
date: "2009-01-22T21:34:22"
picture: "raupemitkran4.jpg"
weight: "4"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17125
- /details1dce.html
imported:
- "2019"
_4images_image_id: "17125"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17125 -->
Die Raupe besteht aus 2 dieser Hälften (eine dvon spiegelverkehrt)!