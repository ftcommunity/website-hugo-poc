---
layout: "image"
title: "Raupe unten"
date: "2009-01-22T21:34:22"
picture: "raupemitkran7.jpg"
weight: "7"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17128
- /details2130.html
imported:
- "2019"
_4images_image_id: "17128"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17128 -->
Das ist die komplett zusammengebaute Raupe von unten!