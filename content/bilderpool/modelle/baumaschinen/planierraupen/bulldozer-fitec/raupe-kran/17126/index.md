---
layout: "image"
title: "Raupe oben"
date: "2009-01-22T21:34:22"
picture: "raupemitkran5.jpg"
weight: "5"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17126
- /detailsf5e6-2.html
imported:
- "2019"
_4images_image_id: "17126"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17126 -->
Das ist die komplett zusammengebaute Raupe von oben!