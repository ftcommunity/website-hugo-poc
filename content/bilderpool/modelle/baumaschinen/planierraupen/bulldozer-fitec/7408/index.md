---
layout: "image"
title: "Bulldozer"
date: "2006-11-05T14:39:25"
picture: "Bulldozer6.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7408
- /detailse0ba.html
imported:
- "2019"
_4images_image_id: "7408"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7408 -->
Man kann die Motorhaube einfach abnehmen und den Akuu wechseln oder aufladen.
