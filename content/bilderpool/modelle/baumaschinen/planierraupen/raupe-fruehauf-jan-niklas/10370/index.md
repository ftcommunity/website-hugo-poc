---
layout: "image"
title: "Raupe"
date: "2007-05-12T12:41:14"
picture: "raupe2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10370
- /detailse78a.html
imported:
- "2019"
_4images_image_id: "10370"
_4images_cat_id: "945"
_4images_user_id: "557"
_4images_image_date: "2007-05-12T12:41:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10370 -->
aufreißer zum festhalten