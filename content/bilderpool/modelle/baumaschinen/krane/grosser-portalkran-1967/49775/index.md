---
layout: "image"
title: "Laufkatze von unten (2)"
date: 2023-03-29T18:11:55+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Längs-Balken über dem Grundaufbau verhindert ein zu starkes Kippen der Laufkatze beim Verfahren.