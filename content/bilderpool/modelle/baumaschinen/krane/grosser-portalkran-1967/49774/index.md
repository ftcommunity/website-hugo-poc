---
layout: "image"
title: "Gesamtansicht"
date: 2023-03-29T18:11:54+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier noch ein Blick von der Seite aufs Modell. 135 Grundbausteine 30 zählt der fischertechnik-Designer. Für weitere Details siehe die ft:pedia 2/2023.