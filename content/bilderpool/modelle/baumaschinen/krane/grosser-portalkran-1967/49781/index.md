---
layout: "image"
title: "Der Nachbau"
date: 2023-03-29T18:12:03+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Nachbau ist recht einfach, wenn man genügend Grundbausteine sein Eigen nennt. Lediglich bei der Laufkatze muss man ein bisschen raten.