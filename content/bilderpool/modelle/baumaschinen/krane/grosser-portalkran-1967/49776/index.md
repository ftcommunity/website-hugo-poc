---
layout: "image"
title: "Laufkatze von unten (1)"
date: 2023-03-29T18:11:57+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier beginnt das Raten. Nach genauem Hinschauen aufs Originalbild und einiger Überlegung halte ich es für einigermaßen wahrscheinlich, dass die Laufkatze unten so aufgebaut war.