---
layout: "image"
title: "Laufkatze zentriert"
date: 2023-03-29T18:11:58+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Laufkatze ist ansonsten minimalistisch aufgebaut. Sie trägt sich (anders als die Fassung des Modells aus der Anleitung von 1969) nur durch das Fahr-Z20 in der Mitte.