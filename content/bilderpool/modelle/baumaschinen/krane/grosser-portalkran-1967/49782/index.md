---
layout: "image"
title: "Das Originalbild"
date: 2023-03-29T18:12:04+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses ein einzige Bild des Portalkrans findet sich in der Ur-fischertechnik-Anleitung von 1967. Das Folgende ist der Versuch, das möglichst genau nachzubauen und auch in der ft:pedia 1/2023 beschrieben.