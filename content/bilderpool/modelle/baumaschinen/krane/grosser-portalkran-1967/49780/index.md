---
layout: "image"
title: "Näherer Blick auf die Träger"
date: 2023-03-29T18:12:01+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das war alles noch zu einer Zeit, in der es noch nicht einmal ft-Statik gab - die kam erst 1970. Alles wurde also aus Unmengen von Grundbausteinen hergestellt.