---
layout: "image"
title: "Laufkatze von Antriebsseite"
date: 2023-03-29T18:12:00+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Motor verfährt die Laufkatze auf der Zahnstange.