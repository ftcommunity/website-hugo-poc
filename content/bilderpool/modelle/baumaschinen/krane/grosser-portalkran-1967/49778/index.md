---
layout: "image"
title: "Laufkatze von Seiltrommel-Seite"
date: 2023-03-29T18:11:59+02:00
picture: "2022-10-18_Grosser_Portalkran_1967-5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Motor (ebenfalls ein mot.1 + mot.2-Aufsteckgetriebe) dreht die Seiltrommel.