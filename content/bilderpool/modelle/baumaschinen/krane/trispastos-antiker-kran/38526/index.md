---
layout: "image"
title: "Trispastos - Flaschenzug (unten)"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38526
- /details32e9.html
imported:
- "2019"
_4images_image_id: "38526"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38526 -->
Detailansicht des unteren Teils des Flaschenzugs.