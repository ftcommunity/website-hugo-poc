---
layout: "image"
title: "Trispastos"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38521
- /details8cbc.html
imported:
- "2019"
_4images_image_id: "38521"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38521 -->
Schöne grafische Darstellung eines Trispastos von Eric Gaba (Wikipedia, CC). Der Flaschenzug verstärkt die eingesetzte Hebekraft um den Faktor 3, der Hebel um den Faktor 4 - ergibt eine Gesamtverstärkung von 12.