---
layout: "image"
title: "Trispastos - Flaschenzug"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38524
- /details2451.html
imported:
- "2019"
_4images_image_id: "38524"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38524 -->
Der Drei-Rollen-Zug verstärkt die eingesetzte Kraft um den Faktor 3 indem es die für dieselbe Hubarbeit erforderliche Seilstrecke verdreifacht.