---
layout: "image"
title: "Trispastos - Gesamtansicht"
date: "2014-03-30T13:33:43"
picture: "trispastosantikerkran2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38522
- /details9876.html
imported:
- "2019"
_4images_image_id: "38522"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-03-30T13:33:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38522 -->
fischertechnik-Modell eines Trispastos zur Veranschaulichung der Wirkung eines einfachen Flaschenzugs und Hebels.