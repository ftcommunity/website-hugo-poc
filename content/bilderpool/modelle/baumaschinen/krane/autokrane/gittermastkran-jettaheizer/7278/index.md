---
layout: "image"
title: "Wippspitze 2"
date: "2006-10-29T19:02:13"
picture: "Kran07a.jpg"
weight: "3"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7278
- /detailse307-2.html
imported:
- "2019"
_4images_image_id: "7278"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7278 -->
Detailansicht des Gelenks zwischen Hauptmast und Wippspitze. Einfach, aber effektiv. 
Leider läßt die Qualität der Bilder zu wünschen übrig, aber das war das Beste, was ich aus den Aufnahmen meiner Billig-Cam machen konnte. Die Bausteine sind nicht wirklich grün, das wirkt hier auf dem Foto nur so.