---
layout: "image"
title: "Oberwagen neu Gesamtansicht 2"
date: "2006-11-19T13:35:02"
picture: "Oberwagenrahmen05b.jpg"
weight: "8"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7493
- /detailsb241.html
imported:
- "2019"
_4images_image_id: "7493"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-11-19T13:35:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7493 -->
Hier nochmal das ganze mit angehobenem SA-Bock. Der Kran soll später entfernt angelehnt sein an den Liebherr LG 1750. Das war der einzige Gittermastkran mit 8 Achsen, von dem ich gescheite technische Zeichnungen gefunden hab.
Der Bock ist leider auch noch nicht verwindungssteif genug, da arbeite ich später noch dran, wenn alle Winden an ihrem Platz sitzen.