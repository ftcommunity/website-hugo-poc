---
layout: "image"
title: "Drehkranz 1"
date: "2007-01-12T17:00:20"
picture: "gittermastkran3.jpg"
weight: "14"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8368
- /detailsfa7c-2.html
imported:
- "2019"
_4images_image_id: "8368"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8368 -->
Der Drehkranz basiert auf diesem Vorschlag von honjo1: http://www.ftcommunity.de/details.php?image_id=2300. Allerdings mit Abwandlungen. So habe ich kein Rohr drin und verwende statt Aluplatten Plexiglas.
