---
layout: "image"
title: "Gesamtansicht"
date: "2007-01-12T17:00:20"
picture: "gittermastkran1.jpg"
weight: "12"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8366
- /details6e10-3.html
imported:
- "2019"
_4images_image_id: "8366"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8366 -->
Neu gegenüber dem Vorgänger ist die Verstärkung der Ausleger. Es gibt keine Möglichkeit mehr noch weitere verstrebungen anzubringen. Dafür ist der Kran geschrumpft und jetzt nur noch 120 cm hoch. Der Antrieb (Last hoch-runter und Ausleger hoch-runter) erfolgt je über einen PowerMotor mit angesetztem Getriebe (siehe nächste Fotos). Damit geht das zwar gaaaaanz langsam, aber dafür hebt der Kran auch an jeder Stelle 3,5kg ohne jegliche Probleme.
Außerdem habe ich einen Drehkranz selber konstruiert, Beschreibung bei den Detailbildern.
