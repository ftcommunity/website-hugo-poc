---
layout: "image"
title: "Kran liegend"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran11.jpg"
weight: "11"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/7486
- /detailsfb54.html
imported:
- "2019"
_4images_image_id: "7486"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7486 -->
