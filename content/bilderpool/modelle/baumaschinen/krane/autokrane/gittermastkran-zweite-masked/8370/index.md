---
layout: "image"
title: "Getriebe Detail"
date: "2007-01-12T17:00:21"
picture: "gittermastkran5.jpg"
weight: "16"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8370
- /details7198.html
imported:
- "2019"
_4images_image_id: "8370"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8370 -->
Daran muss ich noch arbeiten, das ist deutlich zu groß...Aber immerhin laufen die Motoren ohne Murren selbst bei größten Lasten.
