---
layout: "image"
title: "Schleifring"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran07.jpg"
weight: "7"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/7482
- /details1747.html
imported:
- "2019"
_4images_image_id: "7482"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7482 -->
