---
layout: "image"
title: "Der Hauptausleger von unten"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran05.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/7480
- /details3d52-2.html
imported:
- "2019"
_4images_image_id: "7480"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7480 -->
Die Arbeitsfläche wird von 5 LEDs beleuchtet, außerdem ist auf jeder Mastspitze je eine Flugwarnleuchte installiert.
