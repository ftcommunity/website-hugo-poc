---
layout: "image"
title: "Drehkranz Detail"
date: "2007-01-12T17:00:21"
picture: "gittermastkran4.jpg"
weight: "15"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/8369
- /detailsab9f.html
imported:
- "2019"
_4images_image_id: "8369"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8369 -->
