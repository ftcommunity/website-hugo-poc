---
layout: "image"
title: "Autokran 16"
date: "2007-02-03T12:18:38"
picture: "autokran08.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8808
- /details7514-2.html
imported:
- "2019"
_4images_image_id: "8808"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-03T12:18:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8808 -->
