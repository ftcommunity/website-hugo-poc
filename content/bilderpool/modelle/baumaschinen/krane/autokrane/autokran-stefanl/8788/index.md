---
layout: "image"
title: "Autokran 8"
date: "2007-02-02T21:23:13"
picture: "autokran8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8788
- /details1f7f.html
imported:
- "2019"
_4images_image_id: "8788"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8788 -->
