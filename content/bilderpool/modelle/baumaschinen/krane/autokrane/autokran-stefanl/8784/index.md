---
layout: "image"
title: "Autokran 4"
date: "2007-02-02T21:23:13"
picture: "autokran4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8784
- /details6efe.html
imported:
- "2019"
_4images_image_id: "8784"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8784 -->
