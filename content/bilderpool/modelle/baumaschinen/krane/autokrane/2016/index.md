---
layout: "image"
title: "Autokran mit 15x15 Alu-Bauprofil5"
date: "2003-12-08T15:17:19"
picture: "FT-efteling0005.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/2016
- /detailscb7f.html
imported:
- "2019"
_4images_image_id: "2016"
_4images_cat_id: "117"
_4images_user_id: "22"
_4images_image_date: "2003-12-08T15:17:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2016 -->
