---
layout: "image"
title: "Faltkran Model 2-30"
date: "2005-09-25T14:02:42"
picture: "Faltkran_2-40.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4812
- /details3d56-2.html
imported:
- "2019"
_4images_image_id: "4812"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4812 -->
