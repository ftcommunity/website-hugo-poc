---
layout: "image"
title: "Detail Lenkung"
date: "2006-01-02T23:29:20"
picture: "Faltkran_2-44_2.jpg"
weight: "34"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/5554
- /details8d5b-2.html
imported:
- "2019"
_4images_image_id: "5554"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2006-01-02T23:29:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5554 -->
