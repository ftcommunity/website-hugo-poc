---
layout: "image"
title: "Detail Antrieb Kran 2"
date: "2006-01-02T23:29:20"
picture: "Faltkran_2-48.jpg"
weight: "38"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/5558
- /detailsbb67-2.html
imported:
- "2019"
_4images_image_id: "5558"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2006-01-02T23:29:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5558 -->
