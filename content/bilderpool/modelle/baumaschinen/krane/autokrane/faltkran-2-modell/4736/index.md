---
layout: "image"
title: "Faltkran Model 2-10"
date: "2005-09-25T09:00:49"
picture: "Faltkran_2-13.jpg"
weight: "10"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4736
- /detailsc8ab-2.html
imported:
- "2019"
_4images_image_id: "4736"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T09:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4736 -->
Das oberste Teil wird Hoch geklappt mit 2 S-motore. Ich habe es erst mahl versucht mit nur eine, aber das geht ein bisschen zu schwer. Deswegen brauchte ich 2. Das ist aber auch noch etwas zu schwer und es braucht auch noch einige Hilfe. 
Ein Power motor wurde es schaffen, aber dafur gibt es kein Platz.
