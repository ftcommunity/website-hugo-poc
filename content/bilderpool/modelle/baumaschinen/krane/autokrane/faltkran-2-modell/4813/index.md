---
layout: "image"
title: "Faltkran Model 2-31"
date: "2005-09-25T14:02:59"
picture: "Faltkran_2-42.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4813
- /details407d.html
imported:
- "2019"
_4images_image_id: "4813"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4813 -->
