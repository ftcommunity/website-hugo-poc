---
layout: "image"
title: "Detail Antrieb Kran"
date: "2006-01-02T23:29:20"
picture: "Faltkran_2-47.jpg"
weight: "37"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/5557
- /details6d4b-3.html
imported:
- "2019"
_4images_image_id: "5557"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2006-01-02T23:29:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5557 -->
