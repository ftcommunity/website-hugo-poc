---
layout: "image"
title: "Faltkran Model 2-11"
date: "2005-09-25T14:02:34"
picture: "Faltkran_2-14.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4793
- /details2a8f.html
imported:
- "2019"
_4images_image_id: "4793"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4793 -->
