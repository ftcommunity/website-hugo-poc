---
layout: "image"
title: "Faltkran Model 2-32"
date: "2005-09-25T14:03:00"
picture: "Faltkran_2-44.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/4814
- /detailsdf47.html
imported:
- "2019"
_4images_image_id: "4814"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:03:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4814 -->
