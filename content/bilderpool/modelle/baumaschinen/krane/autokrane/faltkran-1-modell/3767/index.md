---
layout: "image"
title: "Faltkran05"
date: "2005-03-12T19:48:41"
picture: "Faltkran05.JPG"
weight: "5"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3767
- /detailsef11.html
imported:
- "2019"
_4images_image_id: "3767"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-12T19:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3767 -->
