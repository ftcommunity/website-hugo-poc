---
layout: "image"
title: "Faltkran28"
date: "2005-03-14T14:45:05"
picture: "Faltkran28.JPG"
weight: "28"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3839
- /detailsb205.html
imported:
- "2019"
_4images_image_id: "3839"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3839 -->
