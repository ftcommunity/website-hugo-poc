---
layout: "image"
title: "Faltkran02"
date: "2005-03-12T19:48:41"
picture: "Faltkran02.JPG"
weight: "2"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3764
- /details95db-2.html
imported:
- "2019"
_4images_image_id: "3764"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-12T19:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3764 -->
