---
layout: "image"
title: "Faltkran32"
date: "2005-03-14T14:45:05"
picture: "Faltkran32.JPG"
weight: "32"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3843
- /details6cac-2.html
imported:
- "2019"
_4images_image_id: "3843"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3843 -->
