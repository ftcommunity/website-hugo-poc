---
layout: "image"
title: "Faltkran25"
date: "2005-03-14T14:45:05"
picture: "Faltkran25.JPG"
weight: "25"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3836
- /details62d2.html
imported:
- "2019"
_4images_image_id: "3836"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3836 -->
