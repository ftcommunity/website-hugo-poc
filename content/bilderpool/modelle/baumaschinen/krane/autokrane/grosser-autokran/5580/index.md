---
layout: "image"
title: "kranhaken 1"
date: "2006-01-07T19:17:28"
picture: "kranhaken_2.jpg"
weight: "46"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/5580
- /detailsd5d0.html
imported:
- "2019"
_4images_image_id: "5580"
_4images_cat_id: "334"
_4images_user_id: "368"
_4images_image_date: "2006-01-07T19:17:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5580 -->
