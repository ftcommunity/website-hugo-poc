---
layout: "image"
title: "Fahrerkabine"
date: "2005-02-28T12:29:30"
picture: "DSC00029.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3666
- /details3974.html
imported:
- "2019"
_4images_image_id: "3666"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-02-28T12:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3666 -->
Die Kabine bietet Platz für 2 FT-Männchen