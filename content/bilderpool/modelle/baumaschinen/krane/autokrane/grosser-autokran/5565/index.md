---
layout: "image"
title: "kran auf reisen 2"
date: "2006-01-07T01:18:41"
picture: "kran_auf_reisen_2.jpg"
weight: "33"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/5565
- /details905c-2.html
imported:
- "2019"
_4images_image_id: "5565"
_4images_cat_id: "334"
_4images_user_id: "368"
_4images_image_date: "2006-01-07T01:18:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5565 -->
dies ist das zweite bild kran wird zur zeit überarbeitet und kann in vier versionen aufgebaut werden.
1. einfacher ausleger 2.00m höhe
2. ausleger mit wippspitze ca 3.00m höhe
3. maxilift mit zwei auslegern
4. maxilift mit zwei auslegern und  
   wippspitze
weitere bilder folgen mit auslegeraufbau