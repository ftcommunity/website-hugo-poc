---
layout: "image"
title: "Rahmenbefestigung"
date: "2005-02-28T12:29:30"
picture: "Unterwagen_Rahmendetail.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3670
- /detailsac50.html
imported:
- "2019"
_4images_image_id: "3670"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-02-28T12:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3670 -->
Hier sieht man das hintere Rahmenende mit Schraube