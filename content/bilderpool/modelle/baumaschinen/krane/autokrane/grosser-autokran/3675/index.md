---
layout: "image"
title: "Oberwagen"
date: "2005-03-04T14:31:33"
picture: "DSC00047.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3675
- /details6d11.html
imported:
- "2019"
_4images_image_id: "3675"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-03-04T14:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3675 -->
Der Oberwagen stützt sich hauptsächlich über die Alus am Unterwagen ab.Ein Drehkranz übernimmt den Rest