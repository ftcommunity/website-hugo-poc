---
layout: "image"
title: "wippspitze 2"
date: "2006-01-07T19:17:28"
picture: "wipspitze_hauptarm.jpg"
weight: "45"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/5579
- /detailsf81a.html
imported:
- "2019"
_4images_image_id: "5579"
_4images_cat_id: "334"
_4images_user_id: "368"
_4images_image_date: "2006-01-07T19:17:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5579 -->
wippspitze 2