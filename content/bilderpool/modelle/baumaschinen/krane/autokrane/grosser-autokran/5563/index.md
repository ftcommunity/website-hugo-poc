---
layout: "image"
title: "mittelteil"
date: "2006-01-07T00:38:43"
picture: "mittelteil1.jpg"
weight: "31"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- /php/details/5563
- /detailsacc3-2.html
imported:
- "2019"
_4images_image_id: "5563"
_4images_cat_id: "334"
_4images_user_id: "368"
_4images_image_date: "2006-01-07T00:38:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5563 -->
der kran hat acht achsen und ist einem gottwald ak 450 nachempfunden