---
layout: "image"
title: "vorderes Rahmenende"
date: "2005-03-04T14:31:33"
picture: "Rahmenende_vorn.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3672
- /detailsdbfd.html
imported:
- "2019"
_4images_image_id: "3672"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-03-04T14:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3672 -->
Der Rahmen fängt direkt hinter dem Fahrerhaus an.