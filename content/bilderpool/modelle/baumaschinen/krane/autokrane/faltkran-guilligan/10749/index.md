---
layout: "image"
title: "Faltkran beim Aufbau"
date: "2007-06-09T14:58:57"
picture: "faltkranguilligan13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10749
- /detailsd48e-2.html
imported:
- "2019"
_4images_image_id: "10749"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:57"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10749 -->
