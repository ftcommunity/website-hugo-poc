---
layout: "image"
title: "Vorderseite"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10740
- /details3d7b.html
imported:
- "2019"
_4images_image_id: "10740"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10740 -->
