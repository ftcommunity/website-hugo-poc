---
layout: "image"
title: "Grundmodell Faltkran"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10737
- /detailsbc5f.html
imported:
- "2019"
_4images_image_id: "10737"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10737 -->
Dieses Modell ist dem Liebherr MK 110 nachempfunden. Es ist ein 5 achsiger Mobilbaukran als Faltkran. Die Maße meines Modell: Gesamthöhe 180cm(Bilder folgen noch), Gesamtausladung 160cm
