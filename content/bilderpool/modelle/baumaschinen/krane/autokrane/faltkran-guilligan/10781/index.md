---
layout: "image"
title: "Faltkran02"
date: "2007-06-09T22:56:32"
picture: "faltkran4.jpg"
weight: "27"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10781
- /details9472-2.html
imported:
- "2019"
_4images_image_id: "10781"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T22:56:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10781 -->
Noch mal eine andere Perspektive.
