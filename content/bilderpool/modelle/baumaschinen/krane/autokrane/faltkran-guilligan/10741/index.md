---
layout: "image"
title: "Vorderseite"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10741
- /details6899-2.html
imported:
- "2019"
_4images_image_id: "10741"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10741 -->
