---
layout: "image"
title: "Draufsicht Oberwagen"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10742
- /detailsc2de.html
imported:
- "2019"
_4images_image_id: "10742"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10742 -->
