---
layout: "image"
title: "Faltkran beim Aufbau"
date: "2007-06-09T14:58:57"
picture: "faltkranguilligan19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10755
- /details5bea.html
imported:
- "2019"
_4images_image_id: "10755"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:57"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10755 -->
