---
layout: "image"
title: "Ausleger02"
date: "2007-06-17T22:19:51"
picture: "kran3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10875
- /detailsa802.html
imported:
- "2019"
_4images_image_id: "10875"
_4images_cat_id: "984"
_4images_user_id: "389"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10875 -->
Hier sieht man die Mastoberseite. In Bildmitte mit dem weißen Seil ist die Trommel für das Hubseil. Links daneben mit dem Aluprofil ist die Mechanik für das Aufrichten des Auslegers. Die rote Platte (30*15) die oben am Profil befestigt ist greift in eine Kurbelwelle die sich am Gegenausleger befindet.