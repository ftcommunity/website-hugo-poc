---
layout: "image"
title: "Ausleger01"
date: "2007-06-17T22:19:51"
picture: "kran2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10874
- /details47e5.html
imported:
- "2019"
_4images_image_id: "10874"
_4images_cat_id: "984"
_4images_user_id: "389"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10874 -->
Der Ausleger ist wie zu sehen mit den Metallwinkelachsen am Mast befestigt.
Um Stabilität in den Teleskopmast zu bekommen habe ich Aluprofile quer zur Ausschubrichtung verbaut. Am ersten Mastende sind es 4*105mm(nicht ganz zu erkennen)
Am zweiten Mastende sind es 4*75mm.