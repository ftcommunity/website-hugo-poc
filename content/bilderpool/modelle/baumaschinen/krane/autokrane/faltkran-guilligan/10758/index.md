---
layout: "image"
title: "Oberteil"
date: "2007-06-09T14:59:04"
picture: "faltkranguilligan22.jpg"
weight: "22"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/10758
- /details6063.html
imported:
- "2019"
_4images_image_id: "10758"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:59:04"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10758 -->
