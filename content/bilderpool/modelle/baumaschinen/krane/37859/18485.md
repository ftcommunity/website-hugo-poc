---
layout: "comment"
hidden: true
title: "18485"
date: "2013-11-30T18:17:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein toller Einstand, gratuliere! Ein schönes Modell, klassisch und hervorragend gebaut, viele tolle Details (vor allem so einen Kran nach ft 400S-Zeiten mal wieder zu sehen, und natürlich die tolle Energiekette und die Schleifringe!) und gute Fotos.

Steckt auf diesem Foto ein S-Riegel in einem BS5? Musstest Du den abfeilen?

Gruß,
Stefan