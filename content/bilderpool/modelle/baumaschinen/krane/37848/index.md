---
layout: "image"
title: "Kran mit Greifarm"
date: "2013-11-29T21:55:24"
picture: "IMG_2893.jpg"
weight: "7"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- /php/details/37848
- /details2894.html
imported:
- "2019"
_4images_image_id: "37848"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37848 -->
Meine erste ft-Modellveröffentlichung. Kran mit 6 Funktionen.