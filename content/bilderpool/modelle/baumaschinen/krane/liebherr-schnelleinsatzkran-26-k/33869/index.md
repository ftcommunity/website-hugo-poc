---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:55"
picture: "IM003408.jpg"
weight: "5"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33869
- /detailsa5fb.html
imported:
- "2019"
_4images_image_id: "33869"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33869 -->
Der Turm wird durch den gleichen Flaschenzug ausgezogen mit der er schon aufgerichtet wurde.
