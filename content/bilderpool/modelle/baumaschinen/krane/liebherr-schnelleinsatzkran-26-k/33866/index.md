---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:55"
picture: "IM003412.jpg"
weight: "2"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33866
- /details286b.html
imported:
- "2019"
_4images_image_id: "33866"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33866 -->
An der Baustelle angekommen wird der Kran abgestützt und der Turm kann aufgerichtet werden. Dies geschieht durch eine (handbetriebene) Seilwinde die, wie beim Original, auf einen Flaschenzug wirkt.
