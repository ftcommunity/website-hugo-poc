---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:55"
picture: "IM003413.jpg"
weight: "1"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33865
- /details43fe.html
imported:
- "2019"
_4images_image_id: "33865"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33865 -->
Der 26 K in Transtportstellung. Im Hintergrund zu sehen die Transportachse mit deren Hilfe der Kran auf der Strasse als Sattelzug gefahren werden kann.
