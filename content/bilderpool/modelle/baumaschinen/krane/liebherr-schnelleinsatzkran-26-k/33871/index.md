---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:56"
picture: "IM003404.jpg"
weight: "7"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33871
- /details78b0.html
imported:
- "2019"
_4images_image_id: "33871"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33871 -->
Beim Turmhub wird der Ausleger immer knapp über dem Boden gehalten bis er vollständig ausgeklappt ist.
