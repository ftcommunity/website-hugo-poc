---
layout: "image"
title: "kleiner Turmdrehkran"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran03.jpg"
weight: "3"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31214
- /detailsed2b.html
imported:
- "2019"
_4images_image_id: "31214"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31214 -->
