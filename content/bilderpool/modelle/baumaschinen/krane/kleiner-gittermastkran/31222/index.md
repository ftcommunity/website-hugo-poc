---
layout: "image"
title: "Oberweagen, oben"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran11.jpg"
weight: "11"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31222
- /detailsea60-2.html
imported:
- "2019"
_4images_image_id: "31222"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31222 -->
