---
layout: "image"
title: "Kranheck"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran09.jpg"
weight: "9"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/31220
- /details8af2.html
imported:
- "2019"
_4images_image_id: "31220"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31220 -->
Der Strom kommt von einem 7,2 Volt Modelbauakku, der Gleichzeitig als Gegengewicht dient.