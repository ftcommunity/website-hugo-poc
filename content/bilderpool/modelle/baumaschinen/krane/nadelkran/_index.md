---
layout: "overview"
title: "Nadelkran"
date: 2022-12-05T11:39:16+01:00
---

Der Baukran Wetzel mit Balkenausleger zählt zu den Nadelkänen. Das System hat sich nicht durchgesetzt, 
weil der Außleger am Auflagepunkt zu starken Belastungen ausgesetzt war.

Ein Video dazu findet ihr unter:

https://1drv.ms/u/s!AnRUc7zxfYRNxw9cor_wZKWtIxyM?e=Uk1s4e

Viel Spaß bei ansehen.

Detlef Ottmann