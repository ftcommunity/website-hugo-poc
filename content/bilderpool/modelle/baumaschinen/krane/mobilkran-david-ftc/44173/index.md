---
layout: "image"
title: "Lenkung: Aufbau"
date: "2016-08-04T14:29:47"
picture: "lenkung2.jpg"
weight: "22"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44173
- /detailsbdfd-3.html
imported:
- "2019"
_4images_image_id: "44173"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-04T14:29:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44173 -->
oben: zusammengesetzer linker Teil der Lenkung
unten: Kernbestandteile des rechten Teil der Lenkung:
 - Messingstange (23mm) im Raststangenformat (lässt sich sowohl in das Differentialgetriebe als auch in die Kardanklaue stecken)
 - um 7.5mm auf 8mm aufgebohrter Baustein 15 sodass sich die Kardanklaue um jeweils 7.5 mm hineinschieben lässt. Dadurch wird die Lenkung insgesamt um 30 mm schmaler.
