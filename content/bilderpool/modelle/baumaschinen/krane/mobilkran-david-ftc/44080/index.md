---
layout: "image"
title: "Kabine"
date: "2016-08-01T19:00:30"
picture: "mlkn12.jpg"
weight: "12"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44080
- /details3a9d.html
imported:
- "2019"
_4images_image_id: "44080"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44080 -->
Führerhaus mit LEDs: https://ftcommunity.de/categories.php?cat_id=3161
