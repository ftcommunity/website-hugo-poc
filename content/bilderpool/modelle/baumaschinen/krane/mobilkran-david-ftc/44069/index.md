---
layout: "image"
title: "Mobilkran"
date: "2016-08-01T19:00:30"
picture: "mlkn01.jpg"
weight: "1"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44069
- /details0584.html
imported:
- "2019"
_4images_image_id: "44069"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44069 -->
Modell eines Liebherr LTM 1060 Mobilkran
Funktionen:
- Allradlenkung
- Allradantrieb
- Drehbarer Aufbau mit Teleskopausleger
