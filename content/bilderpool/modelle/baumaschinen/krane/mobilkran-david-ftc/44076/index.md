---
layout: "image"
title: "Kran"
date: "2016-08-01T19:00:30"
picture: "mlkn08.jpg"
weight: "8"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44076
- /details5641.html
imported:
- "2019"
_4images_image_id: "44076"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44076 -->
maximaler Winkel des Auslegers, der Kranarm ist einfach ausfahrbar
