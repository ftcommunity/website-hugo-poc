---
layout: "image"
title: "Aufbau"
date: "2016-08-01T19:00:30"
picture: "mlkn10.jpg"
weight: "10"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44078
- /detailsf832-3.html
imported:
- "2019"
_4images_image_id: "44078"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44078 -->
drehbarer Aufbau
