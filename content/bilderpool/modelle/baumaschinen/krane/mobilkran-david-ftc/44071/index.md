---
layout: "image"
title: "Lenkung (2)"
date: "2016-08-01T19:00:30"
picture: "mlkn03.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44071
- /details1434.html
imported:
- "2019"
_4images_image_id: "44071"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44071 -->
Lenkmodus: verzögerter Hundegang, die hinteren Achsen schlagen nicht ganz so stark ein wie die vorderen
