---
layout: "overview"
title: "Mobilkran (david-ftc)"
date: 2020-02-22T08:11:50+01:00
legacy_id:
- /php/categories/3263
- /categories081e.html
- /categoriesb3eb.html
- /categoriesbba8.html
- /categoriesb979.html
- /categories52fd.html
- /categoriesabd0.html
- /categories73e8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3263 --> 
Modell eines Liebherr LTM 1060 Mobilkran
Funktionen:
- Allradlenkung
- Allradantrieb
- Drehbarer Aufbau mit Teleskopausleger