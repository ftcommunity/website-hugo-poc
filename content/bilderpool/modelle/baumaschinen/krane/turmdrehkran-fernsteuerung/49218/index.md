---
layout: "image"
title: "Antrieb und Seilführung der Laufkatze"
date: 2021-05-31T17:20:58+02:00
picture: "03_Turmdrehkran_AntriebLaufkatze.jpeg"
weight: "3"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Kran", "Turmdrehkran", "Fernsteuerung", "fernsteuerbar"]
uploadBy: "Website-Team"
license: "unknown"
---

