---
layout: "image"
title: "Laufkatze"
date: 2021-05-31T17:21:00+02:00
picture: "02_Turmdrehkran_Laufkatze.jpeg"
weight: "2"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Kran", "Turmdrehkran", "Fernsteuerung", "fernsteuerbar"]
uploadBy: "Website-Team"
license: "unknown"
---

