---
layout: "image"
title: "Kran mit drei fernsteuerbaren Bewegungsrichtungen"
date: 2021-05-31T17:21:01+02:00
picture: "01_Turmdrehkran.jpeg"
weight: "1"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Kran", "Turmdrehkran", "Fernsteuerung", "fernsteuerbar"]
uploadBy: "Website-Team"
license: "unknown"
---

