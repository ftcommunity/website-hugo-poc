---
layout: "overview"
title: "Turmdrehkran mit Fernsteuerung"
date: 2021-05-31T17:20:56+02:00
---

Ein Kran fürs Kinderzimmer (nicht zu groß und fernsteuerbar). Die Laufkatze gleitet auf dem Ausleger. Die Reibung ist jedoch recht gering und eine Verstellung mittels S-Motor funktioniert tadellos.
