---
layout: "image"
title: "Obendreher, Ausleger, Seite"
date: "2007-04-03T17:33:56"
picture: "Kran04.jpg"
weight: "3"
konstrukteure: 
- "Paul und Tobias"
fotografen:
- "Paul"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- /php/details/9923
- /details139e.html
imported:
- "2019"
_4images_image_id: "9923"
_4images_cat_id: "609"
_4images_user_id: "459"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9923 -->
