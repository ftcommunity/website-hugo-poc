---
layout: "image"
title: "Gesamtansicht"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran16.jpg"
weight: "16"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36300
- /details567f.html
imported:
- "2019"
_4images_image_id: "36300"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36300 -->
-