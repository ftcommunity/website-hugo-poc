---
layout: "image"
title: "Gesamtansicht links"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran02.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36286
- /detailsa803.html
imported:
- "2019"
_4images_image_id: "36286"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36286 -->
-