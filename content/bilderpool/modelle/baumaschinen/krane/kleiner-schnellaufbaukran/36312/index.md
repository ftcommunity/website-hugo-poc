---
layout: "image"
title: "Seilwinde"
date: "2012-12-16T18:21:04"
picture: "kleinerschnellaufbaukran28.jpg"
weight: "28"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36312
- /detailsf2d5.html
imported:
- "2019"
_4images_image_id: "36312"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:04"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36312 -->
-