---
layout: "image"
title: "Von Vorne mit ausgefahrenen Stützen"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36291
- /detailsfb7b.html
imported:
- "2019"
_4images_image_id: "36291"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36291 -->
Die Stützen muss man bisher noch mit der Hand ausfahren.