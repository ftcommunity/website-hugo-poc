---
layout: "image"
title: "Der Ausleger beim Aufbau"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran18.jpg"
weight: "18"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/36302
- /details1f0a-2.html
imported:
- "2019"
_4images_image_id: "36302"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36302 -->
-