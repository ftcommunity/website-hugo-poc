---
layout: "image"
title: "Drehkranz klein Gesamtansicht ohne Scheibe"
date: "2006-11-14T22:58:19"
picture: "Drehkranz03.jpg"
weight: "32"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7464
- /details8d4f.html
imported:
- "2019"
_4images_image_id: "7464"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7464 -->
Hier nun ohne die Scheibe.
Man kann schon gut den gesamten Aufbau erkennen. Weitere Details gibt es auf den nächsten Bildern.