---
layout: "image"
title: "Drehkranz3"
date: "2006-04-25T06:33:47"
picture: "IMG_4273.jpg"
weight: "21"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/6135
- /details691c.html
imported:
- "2019"
_4images_image_id: "6135"
_4images_cat_id: "214"
_4images_user_id: "389"
_4images_image_date: "2006-04-25T06:33:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6135 -->
