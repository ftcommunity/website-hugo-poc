---
layout: "image"
title: "Drehkranz klein Unterseite"
date: "2006-11-14T22:58:19"
picture: "Drehkranz05.jpg"
weight: "34"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7466
- /detailsecbe-2.html
imported:
- "2019"
_4images_image_id: "7466"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7466 -->
Hier die Unterseite des Drehkranzes. Einfacher geht´s nimmer.