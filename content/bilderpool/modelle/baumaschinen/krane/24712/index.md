---
layout: "image"
title: "Kran"
date: "2009-08-05T11:56:28"
picture: "20080307_Fischertechnik_Kran_24.jpg"
weight: "5"
konstrukteure: 
- "Unbekannt"
fotografen:
- "PM"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/24712
- /details7a52.html
imported:
- "2019"
_4images_image_id: "24712"
_4images_cat_id: "609"
_4images_user_id: "327"
_4images_image_date: "2009-08-05T11:56:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24712 -->
