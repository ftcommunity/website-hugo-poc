---
layout: "overview"
title: "Kran Cobra Typ 200"
date: 2022-12-03T14:25:36+01:00
---

Das ist ein Kran Cobra Typ 200


Den Namen hat der Kran durch eine Studie bekommen.

Das Projekt ist von einer Schweizer Maschinenbau Universität durch Studenten  entwickelt worden.
Nach dem Abschluss des Projektes hat man um finanzielle Unterstützung in der Maschinenbauindustrie geworben. 
Daraufhin hat sich die AG Wolf Kranbautechnik aus der Schweiz  dazu entschlossen diesen zu bauen. 
Leider war der Kran in der Herstellung zu teuer und der Bau wurde eingestellt.
 
Besonderheiten des Krans:
- Er zählt zu den Nadelkränen
- Er ist ein Oberturmdrehkran
- Das Kontergewicht fährt wie bei einem Hafenkran mit dem Ausleger nach hinten aus
- Ein weiterer Vorteil ist im Gegensatz zum Nadelkran, das die Flaschenhöhe nicht ausgeglichen werden muss, da der Knick im Ausleger dafür sorgt, dass das Ende des Auslegers, egal in welchem Bereich er sich befindet, immer auf gleicher Höhe ist
- Dass er mit der Flasche direkt bis an den Mast (Kranstamm) fahren kann, was bei einem Nadelkran nicht möglich ist.

Ein Video dazu findet ihr unter:

https://1drv.ms/u/s!AnRUc7zxfYRNxxETIoI01346cRVr?e=JxuZ0y

Viel Spaß bei ansehen.

Detlef Ottmann
