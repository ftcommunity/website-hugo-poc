---
layout: "image"
title: "Liebherr LR13000"
date: "2015-08-19T16:44:51"
picture: "IMG_1226.jpg"
weight: "9"
konstrukteure: 
- "Samuel"
fotografen:
- "Samuel"
uploadBy: "_Samuel_"
license: "unknown"
legacy_id:
- /php/details/41832
- /detailsbaf0.html
imported:
- "2019"
_4images_image_id: "41832"
_4images_cat_id: "2724"
_4images_user_id: "2473"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41832 -->
Nach langer Zeit zum ersten Mal Fischertechnik wieder ausgepackt. Die Vorlage war folgendes Bild: http://www.equipmenthandbooks.com/2012/06/liebherr-lr-13000-on-crane-days/liebherr-lr11350-9716/ Was mich immer ein bisschen störte, war dass die Fischertechnikmodelle selten eine Maßstabsgetreue Last anheben konnten. Das wollte ich bei diesem Kran anders machen. Hat leider nicht ganz funktioniert. DIe volle 1,5L-Flasche hat er nur angehoben wenn man ihn gegen seitliches Drehen fixiert hat. Lag möglicherweise auch an der Drehkranzkonstruktion.