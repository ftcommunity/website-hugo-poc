---
layout: "image"
title: "Kran mit Last"
date: "2013-03-07T13:30:38"
picture: "20111213_100244_Apple_iPhone_4_IMG_1530.jpeg"
weight: "3"
konstrukteure: 
- "Marc Stephan Tauchert"
fotografen:
- "Marc Stephan Tauchert"
uploadBy: "mst"
license: "unknown"
legacy_id:
- /php/details/36725
- /details3c95.html
imported:
- "2019"
_4images_image_id: "36725"
_4images_cat_id: "2724"
_4images_user_id: "1621"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36725 -->
