---
layout: "image"
title: "von der einen Seite"
date: "2011-03-15T18:50:59"
picture: "minikranwagen1.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30253
- /detailsd57d.html
imported:
- "2019"
_4images_image_id: "30253"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30253 -->
Das Unterteil des Krans ist aus einer allten Bauanleitung.
Der Kran wird über 4 Motoren gesteuert.
Leider kann man mit der neuen Fernbedinung nur 3 motoren ansteuern.