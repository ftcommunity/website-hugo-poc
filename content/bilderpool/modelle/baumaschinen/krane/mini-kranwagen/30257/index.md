---
layout: "image"
title: "das Gegengewicht"
date: "2011-03-15T18:50:59"
picture: "minikranwagen5.jpg"
weight: "5"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30257
- /details5c36.html
imported:
- "2019"
_4images_image_id: "30257"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30257 -->
Der Akku wird als Gegengewicht verwendet.