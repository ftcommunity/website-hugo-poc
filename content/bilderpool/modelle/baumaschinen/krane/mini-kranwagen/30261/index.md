---
layout: "image"
title: "von unten"
date: "2011-03-15T18:50:59"
picture: "minikranwagen9.jpg"
weight: "9"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30261
- /details23d2.html
imported:
- "2019"
_4images_image_id: "30261"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30261 -->
Die Achse wird über einen Mini- Motor angetriben