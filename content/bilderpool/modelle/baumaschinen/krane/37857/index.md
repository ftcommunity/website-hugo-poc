---
layout: "image"
title: "Detail Kran mit Greifer"
date: "2013-11-29T21:55:24"
picture: "IMG_2905.jpg"
weight: "16"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- /php/details/37857
- /details3d07.html
imported:
- "2019"
_4images_image_id: "37857"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37857 -->
Kabeltrommel für Zugseil Auf-Ab Greifer (links) und Energieversorgung für Greifer, (rechts), mit Schleifring Marke Eigenbau (2 Kontakte). Die beiden anderen Kontakte für den 2. Mini-Motor am Greifer werden über die elektrisch getrennte Mittelachse übertragen.