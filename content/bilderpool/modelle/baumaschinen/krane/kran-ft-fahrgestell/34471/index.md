---
layout: "image"
title: "11 Mast + Haken"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34471
- /details0153-3.html
imported:
- "2019"
_4images_image_id: "34471"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34471 -->
Der Mast von oben und der Haken