---
layout: "image"
title: "37 Drehkranz"
date: "2012-04-01T17:12:16"
picture: "kran4.jpg"
weight: "37"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34739
- /details37c4.html
imported:
- "2019"
_4images_image_id: "34739"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-04-01T17:12:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34739 -->
die Anschlüsse beim Drehkanz