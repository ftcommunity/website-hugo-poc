---
layout: "image"
title: "16 Drehkranz"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell16.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34476
- /details2dd0-2.html
imported:
- "2019"
_4images_image_id: "34476"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34476 -->
eigentlich ist hier die Seilrolle