---
layout: "comment"
hidden: true
title: "16520"
date: "2012-02-27T20:43:29"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Hallo Matthias,

Wirklich sehr schöner Kran und wie Stefan schon sagte, professionelles Schaltpult. Das kleine kompakte und doch stabile Unterteil gefällt mir sehr gut. Hast du irgendwo Gewichte eingesetzt, sodass der Kran nicht kippen kann?

MfG
Endlich