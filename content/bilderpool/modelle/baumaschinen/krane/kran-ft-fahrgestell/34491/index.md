---
layout: "image"
title: "31 Baustein"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell31.jpg"
weight: "31"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34491
- /detailsced6.html
imported:
- "2019"
_4images_image_id: "34491"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34491 -->
von oben