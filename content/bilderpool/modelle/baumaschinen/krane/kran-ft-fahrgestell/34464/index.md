---
layout: "image"
title: "04 Front"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34464
- /detailseb73.html
imported:
- "2019"
_4images_image_id: "34464"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34464 -->
die Front - in den Lampen ist jeweils eine LED drin