---
layout: "comment"
hidden: true
title: "16530"
date: "2012-02-28T13:11:53"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Matthias,
wirklich ein sehr schönes Modell. Auch nicht überdimensioniert - es sieht zumindest so aus, als ob der Kran außer sich selbst auch noch etwas tragen kann. Und kein "Edelpfusch" verbaut, wie Remadus das nennen würde... Besonders gefällt mir der dreirippige Ausleger und das unkonventionelle Gelenk. Und die Beleuchtung ist einfach chic. Klasse...
Gruß, Dirk