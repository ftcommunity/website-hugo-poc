---
layout: "image"
title: "28 Steuerpult"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell28.jpg"
weight: "28"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34488
- /detailsccf5-2.html
imported:
- "2019"
_4images_image_id: "34488"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34488 -->
sieht etwas unordentlich aus ... die gelben Kabel kommen von den Schaltern