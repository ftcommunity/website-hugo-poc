---
layout: "image"
title: "Fahrwerk von unten (2)"
date: 2023-03-29T17:54:16+02:00
picture: "2022-09-25_Grosser_Raupenkran_196916.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man auch nochmal die Position des Hilfsbalkens zwischen den Achsen.