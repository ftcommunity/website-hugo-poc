---
layout: "image"
title: "Das Führerhaus"
date: 2023-03-29T17:54:11+02:00
picture: "2022-09-25_Grosser_Raupenkran_196903.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alles ist aus Grundbausteinen aufgebaut - Statik kam ja erst 1970.