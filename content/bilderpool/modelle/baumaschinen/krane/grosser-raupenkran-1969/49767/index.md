---
layout: "image"
title: "Fahrwerk von unten (1)"
date: 2023-03-29T17:54:17+02:00
picture: "2022-09-25_Grosser_Raupenkran_196915.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Zwillingsreifen sind vermutlich aus der Kombination zweier Achsen mit Kupplungsstück dazwischen aufgebaut. Die ergeben die richtige Länge und sind - wie im Original - in den Naben aus dem Blickwinkel der Aufnahme gerade nicht mehr zu sehen.

Der Längsbalken in der Bildmitte ist pure Spekulation. Ohne sowas drehen sich die Achsen, die ja durch die Raupen unter Biegespannung stehen, nach kurzer Fahrzeit aus den Kupplungen. So geht die Fahrt viel leichter und bleibt zuverlässig.