---
layout: "image"
title: "Kranaufbau von unten"
date: 2023-03-29T17:54:19+02:00
picture: "2022-09-25_Grosser_Raupenkran_196913.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man beachte die Aussparung im Boden der Kabine - da ragt der Motor hinein.