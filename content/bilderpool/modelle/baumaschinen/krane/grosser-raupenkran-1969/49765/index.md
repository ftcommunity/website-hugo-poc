---
layout: "image"
title: "Fahrwerk von unten (3)"
date: 2023-03-29T17:54:14+02:00
picture: "2022-09-25_Grosser_Raupenkran_196917.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ich meine, alles ist so, dass es nirgends dem Originalfoto widerspricht.