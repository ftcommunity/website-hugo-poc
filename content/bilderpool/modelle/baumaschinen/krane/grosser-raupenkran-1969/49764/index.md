---
layout: "image"
title: "Lagerung des Kranaufbaus"
date: 2023-03-29T17:54:13+02:00
picture: "2022-09-25_Grosser_Raupenkran_196918.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Meiner Vermutung nach könnten zwei BS30 mit Bohrung so verbaut sein. Die Befestigung des mot.2-Getriebeaufsatzes ergibt sich recht eindeutig (auch Federnocken gab es damals ja noch lange nicht, nur Verbinder 15, 30 und etwas später 45).