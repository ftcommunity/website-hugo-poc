---
layout: "image"
title: "Front"
date: 2023-03-29T17:54:22+02:00
picture: "2022-09-25_Grosser_Raupenkran_196911.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So - vermute ich - müsste das Modell von vorne genauer ausgesehen haben.