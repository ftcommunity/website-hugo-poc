---
layout: "image"
title: "Antriebsstrang"
date: 2023-03-29T17:54:05+02:00
picture: "2022-09-25_Grosser_Raupenkran_196908.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Details dazu finden sich in der ft:pedia 1/2023. Jedenfalls vermute ich, dass der Antrieb so weitergeht: Z10 vom mot.2-Getriebe auf Z10 einer per Kupplung 20 verlängerten Achse. Die Räder sind - wie in praktisch jedem Fahrzeugmodell der damaligen Zeit - in senkrecht nach unten ragenden Bausteinen 30 gelagert.