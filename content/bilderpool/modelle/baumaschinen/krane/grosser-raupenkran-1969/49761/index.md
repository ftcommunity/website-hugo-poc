---
layout: "image"
title: "Seilantrieb von vorne"
date: 2023-03-29T17:54:10+02:00
picture: "2022-09-25_Grosser_Raupenkran_196904.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick auf den Seilantrieb. Merkt schon jemand etwas im Vergleich zum Originalfoto?