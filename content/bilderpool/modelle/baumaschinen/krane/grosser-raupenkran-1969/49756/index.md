---
layout: "image"
title: "Rückseite (1)"
date: 2023-03-29T17:54:04+02:00
picture: "2022-09-25_Grosser_Raupenkran_196909.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick ins einfache Getriebe.