---
layout: "image"
title: "Der Nachbau"
date: 2023-03-29T17:54:12+02:00
picture: "2022-09-25_Grosser_Raupenkran_196902.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist der Versuch eines Nachbaus. Details dazu finden sich in ft:pedia 1/2023.