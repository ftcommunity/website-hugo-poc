---
layout: "image"
title: "Fahrwerk von oben"
date: 2023-03-29T17:54:18+02:00
picture: "2022-09-25_Grosser_Raupenkran_196914.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So sieht meiner Vermutung nach das Fahrwerk ohne Kranaufbau von oben aus. Die BS30 mit Bohrung erscheinen mir am wahrscheinlichsten als Lager der Drehaufhängung des Aufbaus, sowohl von der Position her als auch von der Zeit-typischen Bauweise.