---
layout: "image"
title: "Das Originalbild"
date: 2023-03-29T17:54:24+02:00
picture: "2022-09-25_Grosser_Raupenkran_196901.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses eine einzige Bild gibt es in der Ur-Anleitung von 1969. Und damit geht das Gerate los: Wie ist der Antrieb genau aufgebaut? Wie sitzt der Seilantriebs-Motor genau da drin?