---
layout: "image"
title: "Antriebsmotor"
date: 2023-03-29T17:54:07+02:00
picture: "2022-09-25_Grosser_Raupenkran_196906.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antriebsmotor sitzt, wie auf dem Originalfoto sichtbar - fast! Fallt etwas auf?