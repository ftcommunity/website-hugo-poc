---
layout: "image"
title: "Seilantrieb von der Seite"
date: 2023-03-29T17:54:09+02:00
picture: "2022-09-25_Grosser_Raupenkran_196905.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier blickt man seitlich in die Kabine.