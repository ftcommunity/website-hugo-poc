---
layout: "image"
title: "Das spiegelverkehrte Getriebe"
date: 2023-03-29T17:54:06+02:00
picture: "2022-09-25_Grosser_Raupenkran_196907.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das mot.2-Aufsteckgetriebe ist auf dem Originalfoto meiner Überzeugung nach in einer unmöglichen Ausrichtung eingebaut. Man bekommt es nicht hin, das Getriebe so auf den Motor zu stecken, dass alles genauso aussieht wie im Originalfoto. Deshalb sage ich: Das Foto in der Original-Anleitung ist - weiß der Himmel, warum - seitenverkehrt abgebildet. Nur wenn man links und rechts (bzw. vorne und hinten beim Modell) vertauscht, bekommt man das Getriebe so hin wie im Originalfoto.