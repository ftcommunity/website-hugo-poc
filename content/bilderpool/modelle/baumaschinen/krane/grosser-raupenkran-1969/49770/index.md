---
layout: "image"
title: "Aufbau"
date: 2023-03-29T17:54:20+02:00
picture: "2022-09-25_Grosser_Raupenkran_196912.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dass da eine Drehscheibe verwendet wurde, sieht man klar. So dürfte der Aufbau befestigt sein.