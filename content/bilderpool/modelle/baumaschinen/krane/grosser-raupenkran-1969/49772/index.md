---
layout: "image"
title: "Rückseite (2)"
date: 2023-03-29T17:54:23+02:00
picture: "2022-09-25_Grosser_Raupenkran_196910.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick auf die andere Seite des Antriebs.