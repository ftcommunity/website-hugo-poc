---
layout: "image"
title: "Sockel"
date: "2009-09-27T23:59:15"
picture: "kranvonmagier10.jpg"
weight: "10"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25400
- /detailsb808.html
imported:
- "2019"
_4images_image_id: "25400"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25400 -->
Der Sockel ist wie schon erwähnt nicht sehr Stabil, da er aus vier Paltten besteht. Diese sind zusätzlich leicht gewölbt 8ab Werk). Das Kabel habe ich einfach schön gerade nach oben geführt, so dass es nur verdreht wird.
