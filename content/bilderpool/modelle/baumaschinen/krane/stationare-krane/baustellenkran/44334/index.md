---
layout: "image"
title: "Motoren, Kabel und Endtaster"
date: "2016-09-03T11:07:00"
picture: "baustellenkran6.jpg"
weight: "6"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44334
- /detailsc445.html
imported:
- "2019"
_4images_image_id: "44334"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44334 -->
Links sind die zwei Powermotoren 1:8 zu sehen. Für den Flaschenzug des Auslegers wäre ein Powermotor 1:20 oder gar 1:50 besser gewesen, da sich der Ausleger viel zu schnell und Kraftlos bewegt hat bevor ich einige weitere Rollen eingebaut habe.
Rechts daneben ist der zweite Endtaster und die dazugehörige Diode.