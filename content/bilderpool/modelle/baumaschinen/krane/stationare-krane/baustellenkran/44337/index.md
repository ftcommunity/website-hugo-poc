---
layout: "image"
title: "Der Antrieb des Drehkranzes"
date: "2016-09-03T11:07:00"
picture: "baustellenkran9.jpg"
weight: "9"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44337
- /detailse223.html
imported:
- "2019"
_4images_image_id: "44337"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44337 -->
Der Powermotor 1:50 treibt über eine Unterstzung 1:2 (die Kette) die Schnecke an. 
Die Drehgeschwindigkeit ist liegt knappen 7°/sec. , also völlig in Ordnung für so einen Kran.