---
layout: "image"
title: "Gesamtansicht"
date: "2016-09-03T11:07:00"
picture: "baustellenkran1.jpg"
weight: "1"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/44329
- /details5353.html
imported:
- "2019"
_4images_image_id: "44329"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44329 -->
Eine Gesamtansicht des Krans.
Jetzt ist der Ausleger so weit es geht nach oben gefahren, die Spitze ist etwas mehr als einen Meter über dem Boden.