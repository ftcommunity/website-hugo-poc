---
layout: "image"
title: "Zweiter Versuch"
date: "2007-10-12T21:26:45"
picture: "gittermastkran4.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/12190
- /details2e0f.html
imported:
- "2019"
_4images_image_id: "12190"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12190 -->
Hier dann mit etwas gekürzten Ausleger. Damit war dann auchdas Problem des Gegengewichts geklärt, denn hierfür reicht es dicke.
