---
layout: "image"
title: "Derrickausleger"
date: "2008-03-21T15:41:45"
picture: "gittermastkranx2.jpg"
weight: "11"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/13990
- /detailsb98c-4.html
imported:
- "2019"
_4images_image_id: "13990"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2008-03-21T15:41:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13990 -->
Mit dem üblichen 6kg-Trafokern als Gegengewicht und Alu-Profilen als Verstärkung.
