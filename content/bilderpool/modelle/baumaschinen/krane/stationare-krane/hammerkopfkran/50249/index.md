---
layout: "image"
title: "Hammerkopfkran"
date: 2024-01-24T11:04:57+01:00
picture: "Hammerkopfkran_03.JPG"
weight: "3"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Hammerkopfkran"]
uploadBy: "Website-Team"
license: "unknown"
---

Gesamtansicht von oben