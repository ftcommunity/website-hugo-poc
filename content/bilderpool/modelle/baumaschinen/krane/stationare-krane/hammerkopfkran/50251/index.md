---
layout: "image"
title: "Hammerkopfkran"
date: 2024-01-24T11:05:00+01:00
picture: "Hammerkopfkran_01.JPG"
weight: "1"
konstrukteure: 
- "Florian73"
fotografen:
- "Florian73"
schlagworte: ["Hammerkopfkran"]
uploadBy: "Website-Team"
license: "unknown"
---

Gesamtansicht vorne