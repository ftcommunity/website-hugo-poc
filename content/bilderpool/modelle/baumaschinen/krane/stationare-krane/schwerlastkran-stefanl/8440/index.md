---
layout: "image"
title: "Schwerlastkran 13"
date: "2007-01-13T22:12:46"
picture: "schwerlastkran1.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8440
- /detailsdf66-3.html
imported:
- "2019"
_4images_image_id: "8440"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T22:12:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8440 -->
