---
layout: "image"
title: "Schwerlastkran 12"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8430
- /detailsa780.html
imported:
- "2019"
_4images_image_id: "8430"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8430 -->
