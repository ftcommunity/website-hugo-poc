---
layout: "image"
title: "Schwerlastkran 9"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8427
- /details3bb8-2.html
imported:
- "2019"
_4images_image_id: "8427"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8427 -->
