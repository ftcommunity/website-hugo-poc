---
layout: "image"
title: "Schwerlastkran 10"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8428
- /details85cf-2.html
imported:
- "2019"
_4images_image_id: "8428"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8428 -->
