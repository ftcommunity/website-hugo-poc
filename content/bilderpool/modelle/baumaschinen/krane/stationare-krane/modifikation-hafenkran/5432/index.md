---
layout: "image"
title: "Modifikation Hafenkran"
date: "2005-11-30T17:28:46"
picture: "Gesamt.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/5432
- /details7671.html
imported:
- "2019"
_4images_image_id: "5432"
_4images_cat_id: "645"
_4images_user_id: "59"
_4images_image_date: "2005-11-30T17:28:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5432 -->
Bei diesem Kran handelt es sich um das dritte Modell des Konstruktions-
kanstes "Hafenkräne", das einen Doppellenker darstellt.

Den Unterbau habe ich komplett mit schwarzer Statik aufgebaut. Für die
langen Stützen kommen U-Träger zum Einsatz, wodurch der Kran etwas
höher ist als im Original. Den Ausleger habe ich ebenfalls, soweit es ging,
mit U-Trägern gestaltet; und die lange Zug-Strebe (wie nennt man so ein
Teil eigentlich genau?), die den Doppellenker im Gleichgewicht hält, ist
aus je 2 x X-106, X169,6 sowie Lasche15 zusammengesetzt. Die Führer-
kanzel stammt aus einem Modell der frühen 80er Jahre.

Der Kran ist vollständig motorisiert und wird über eine separate Handsteue-
rung (Elektro-Eigenbau) gesteuert. Das Modell entspricht, bis auf die oben
genannten Modifikationen, in allen weiteren Einzelheiten der originalen Bau-
anleitung des erwähnten Kastens.
