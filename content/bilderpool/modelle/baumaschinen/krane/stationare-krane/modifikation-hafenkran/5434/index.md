---
layout: "image"
title: "Blick von oben"
date: "2005-11-30T17:28:46"
picture: "von-oben.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/5434
- /detailsde2a.html
imported:
- "2019"
_4images_image_id: "5434"
_4images_cat_id: "645"
_4images_user_id: "59"
_4images_image_date: "2005-11-30T17:28:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5434 -->
