---
layout: "image"
title: "Hafenkran Bild 1"
date: "2003-08-12T19:29:59"
picture: "RIMG0051.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- /php/details/1331
- /detailsbe8a-3.html
imported:
- "2019"
_4images_image_id: "1331"
_4images_cat_id: "144"
_4images_user_id: "26"
_4images_image_date: "2003-08-12T19:29:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1331 -->
selbstgebauter Hafenkran

mit Interface und Joystick gesteuert