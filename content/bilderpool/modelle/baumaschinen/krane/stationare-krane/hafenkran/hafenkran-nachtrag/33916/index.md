---
layout: "image"
title: "Hafenkran von Ixer, Nachtrag"
date: "2012-01-13T19:03:10"
picture: "hafenkran1.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33916
- /detailsce64-3.html
imported:
- "2019"
_4images_image_id: "33916"
_4images_cat_id: "2509"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33916 -->
Detailaufnahme. Der Ausleger wird über eine Schnecke gesteuert. Man braucht dafür keinen besonders kräftigen Motor, da der Ausleger und das Gegengewicht (gelber Kasten, gefüllt mit Metallkugeln) sich etwa im Gleigewicht befinden.