---
layout: "image"
title: "Hafenkran von Ixer, Nachtrag"
date: "2012-01-13T19:03:10"
picture: "hafenkran2.jpg"
weight: "2"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33917
- /details645d-3.html
imported:
- "2019"
_4images_image_id: "33917"
_4images_cat_id: "2509"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33917 -->
Ausleger eingezogen.