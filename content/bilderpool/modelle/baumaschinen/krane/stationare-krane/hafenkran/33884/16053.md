---
layout: "comment"
hidden: true
title: "16053"
date: "2012-01-11T19:19:49"
uploadBy:
- "McDoofi"
license: "unknown"
imported:
- "2019"
---
Klasse Steuerung, sauber verkabelt, übersichtlich und beschriftet. Aber wie stecken denn da die Kabel links unten in den Power-Controllern? MfG McDoofi