---
layout: "image"
title: "baukran39.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran39.jpg"
weight: "51"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45319
- /details7281.html
imported:
- "2019"
_4images_image_id: "45319"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45319 -->
