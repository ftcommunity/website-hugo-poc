---
layout: "image"
title: "baukran35.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran35.jpg"
weight: "47"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45315
- /detailsf388.html
imported:
- "2019"
_4images_image_id: "45315"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45315 -->
