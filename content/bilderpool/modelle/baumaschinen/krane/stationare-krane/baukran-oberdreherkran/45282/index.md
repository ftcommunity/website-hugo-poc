---
layout: "image"
title: "baukran02.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran02.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45282
- /details5bbe.html
imported:
- "2019"
_4images_image_id: "45282"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45282 -->
