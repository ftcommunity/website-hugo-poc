---
layout: "image"
title: "baukran18.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran18.jpg"
weight: "30"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45298
- /detailsda51-2.html
imported:
- "2019"
_4images_image_id: "45298"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45298 -->
Achten Sie beim Bauen des Auslegers auf den Richtigen Aufeinanderfolge. De Streben sind X 42.4