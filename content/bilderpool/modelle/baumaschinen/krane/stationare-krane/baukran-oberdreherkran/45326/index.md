---
layout: "image"
title: "baukran46.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran46.jpg"
weight: "58"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45326
- /detailse741-2.html
imported:
- "2019"
_4images_image_id: "45326"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45326 -->
