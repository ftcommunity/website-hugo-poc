---
layout: "image"
title: "baukran20.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran20.jpg"
weight: "32"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45300
- /detailsf677-2.html
imported:
- "2019"
_4images_image_id: "45300"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45300 -->
