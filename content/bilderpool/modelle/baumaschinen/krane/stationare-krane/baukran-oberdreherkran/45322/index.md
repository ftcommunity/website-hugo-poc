---
layout: "image"
title: "baukran42.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran42.jpg"
weight: "54"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45322
- /details7a08-2.html
imported:
- "2019"
_4images_image_id: "45322"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45322 -->
