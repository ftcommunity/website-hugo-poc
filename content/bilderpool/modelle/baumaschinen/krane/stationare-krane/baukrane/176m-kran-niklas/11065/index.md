---
layout: "image"
title: "Kran 1,76m"
date: "2007-07-15T17:48:59"
picture: "kran3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11065
- /detailsdfce.html
imported:
- "2019"
_4images_image_id: "11065"
_4images_cat_id: "1002"
_4images_user_id: "557"
_4images_image_date: "2007-07-15T17:48:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11065 -->
und vorne