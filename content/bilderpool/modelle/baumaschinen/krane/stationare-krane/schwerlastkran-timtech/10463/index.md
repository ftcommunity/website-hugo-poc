---
layout: "image"
title: "Dreh- Kranz"
date: "2007-05-19T09:12:25"
picture: "PICT0058.jpg"
weight: "4"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10463
- /details8585.html
imported:
- "2019"
_4images_image_id: "10463"
_4images_cat_id: "953"
_4images_user_id: "590"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10463 -->
