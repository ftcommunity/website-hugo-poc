---
layout: "image"
title: "Befestigung des Auslegers"
date: "2012-03-02T14:25:00"
picture: "yantairafflesinkleinerversion12.jpg"
weight: "12"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34505
- /detailsea9c.html
imported:
- "2019"
_4images_image_id: "34505"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:25:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34505 -->
-