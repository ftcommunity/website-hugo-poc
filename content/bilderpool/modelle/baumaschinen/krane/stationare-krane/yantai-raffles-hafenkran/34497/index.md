---
layout: "image"
title: "Die Motoren"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion04.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34497
- /details669d-2.html
imported:
- "2019"
_4images_image_id: "34497"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34497 -->
Der Motor, der gerade gut zu sehen ist, ist für die Seilwind der Hacken.