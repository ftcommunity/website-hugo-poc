---
layout: "image"
title: "Von Vorne"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion03.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34496
- /detailsfe60-3.html
imported:
- "2019"
_4images_image_id: "34496"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34496 -->
Meine kleine Version hat, wie das Original zwei Hacken.
Nur der kleine Kran auf der Spitze hat noch keinen Motor für den Hacken.