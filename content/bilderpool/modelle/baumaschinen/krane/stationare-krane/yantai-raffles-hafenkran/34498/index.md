---
layout: "image"
title: "Von Hinten"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion05.jpg"
weight: "5"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34498
- /details90e8.html
imported:
- "2019"
_4images_image_id: "34498"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34498 -->
Hier sieht man den Motor, der die Drehung des Oberteils ermöglicht.