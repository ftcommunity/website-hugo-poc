---
layout: "image"
title: "Kran02"
date: "2008-01-27T17:30:27"
picture: "kranmitschuettgutgreifer2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13450
- /detailsabd2-3.html
imported:
- "2019"
_4images_image_id: "13450"
_4images_cat_id: "1225"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13450 -->
Kran von vorne