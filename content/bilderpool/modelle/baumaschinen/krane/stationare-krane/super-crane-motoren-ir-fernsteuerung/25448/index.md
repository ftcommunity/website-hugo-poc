---
layout: "image"
title: "Der Ausleger mit Laufkatze vom Super Crane"
date: "2009-10-01T19:18:52"
picture: "dersupercranemitmotorenundderirfernsteuerung6.jpg"
weight: "6"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25448
- /details9fd8-2.html
imported:
- "2019"
_4images_image_id: "25448"
_4images_cat_id: "1781"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25448 -->
