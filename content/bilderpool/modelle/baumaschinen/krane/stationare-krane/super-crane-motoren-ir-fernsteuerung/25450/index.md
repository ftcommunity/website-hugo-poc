---
layout: "image"
title: "Das Grundgerüst vom Super Crane"
date: "2009-10-01T19:18:52"
picture: "dersupercranemitmotorenundderirfernsteuerung8.jpg"
weight: "8"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25450
- /details95b1.html
imported:
- "2019"
_4images_image_id: "25450"
_4images_cat_id: "1781"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25450 -->
