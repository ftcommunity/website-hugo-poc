---
layout: "image"
title: "Gegengewicht"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran6.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9910
- /detailsf8b0.html
imported:
- "2019"
_4images_image_id: "9910"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9910 -->
Hier sieht man das Gegengewicht. Ich habe eine Kasstettenbox genommen und Blei reingefüllt.
