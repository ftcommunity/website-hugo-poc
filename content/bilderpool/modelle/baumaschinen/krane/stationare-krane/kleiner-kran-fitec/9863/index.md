---
layout: "image"
title: "Kleiner Kran"
date: "2007-03-31T18:08:13"
picture: "Kleiner_Kran5.jpg"
weight: "5"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9863
- /details064f.html
imported:
- "2019"
_4images_image_id: "9863"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-03-31T18:08:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9863 -->
Hier sieht man den Ausleger.
