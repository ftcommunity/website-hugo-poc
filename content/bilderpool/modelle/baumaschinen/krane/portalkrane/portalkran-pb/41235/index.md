---
layout: "image"
title: "Containerkr-lr-44"
date: "2015-06-24T14:11:15"
picture: "containerkran31.jpg"
weight: "31"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41235
- /details8c02-2.html
imported:
- "2019"
_4images_image_id: "41235"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41235 -->
Den Greifer.