---
layout: "image"
title: "Containerkr-lr-14"
date: "2015-06-24T14:11:15"
picture: "containerkran09.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41213
- /details54c6.html
imported:
- "2019"
_4images_image_id: "41213"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41213 -->
