---
layout: "image"
title: "Containerkr-lr-40"
date: "2015-06-24T14:11:15"
picture: "containerkran28.jpg"
weight: "28"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41232
- /details90cd.html
imported:
- "2019"
_4images_image_id: "41232"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41232 -->
Laufkatze. Um die Antriebsräder hab ich eine kleine Gummi-ring getan (ein Loombändchen meiner Tochter) für bessere Traktion.