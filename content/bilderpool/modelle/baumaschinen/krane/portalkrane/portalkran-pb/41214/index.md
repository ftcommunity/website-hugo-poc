---
layout: "image"
title: "Containerkr-lr-15"
date: "2015-06-24T14:11:15"
picture: "containerkran10.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41214
- /details8011-3.html
imported:
- "2019"
_4images_image_id: "41214"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41214 -->
