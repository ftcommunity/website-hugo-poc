---
layout: "image"
title: "Containerkr-lr-17"
date: "2015-06-24T14:11:15"
picture: "containerkran12.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41216
- /detailsdae9.html
imported:
- "2019"
_4images_image_id: "41216"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41216 -->
