---
layout: "overview"
title: "Containerbrücke"
date: 2020-02-22T08:11:23+01:00
legacy_id:
- /php/categories/1234
- /categoriesaceb.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1234 --> 
Containerbrücke von Jan Knobbe



Ausgangspunkt: Die Arbeit am Greifer. Ich bin fasziniert von den Lösungen von Dirk Kutsch, Juergen Warwel und IM000806, die auf dieser homepage zu besichtigen sind. Hut ab! Von Anfang an strebte ich allerdings einen möglichst kleinen Greifer an, um die eigentliche Brücke dann nicht zu groß werden zu lassen...

Der Greifer sieht zwar etwas klobig aus, bietet aber durch die halbautomatische Steuerung mit zwei Aus-Tastern einigen Komfort. Ohne Taster oder mit Mini-Tastern wäre es noch eleganter.

Die Brücke ist etwa im Maßstab 1 : 60 gebaut. Kleiner Wermutstropfen. Sie ist nur für 20-Fuß-Container geeignet.

Das Modell soll allen Mut machen, die von den faszinierenden Fotos auf dieser web-site begeistert sind, aber vielleicht nur über \"geringfügig\" weniger Bauteile verfügen...

