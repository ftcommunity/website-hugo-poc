---
layout: "image"
title: "containerbrücke von jan Knobbe - ansicht von unten"
date: "2008-02-01T17:44:12"
picture: "containerbruecke1.jpg"
weight: "1"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13492
- /detailscb5c.html
imported:
- "2019"
_4images_image_id: "13492"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13492 -->
