---
layout: "image"
title: "containerbrücke von jan Knobbe - seilwinden"
date: "2008-02-01T17:44:12"
picture: "containerbruecke8.jpg"
weight: "8"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13499
- /details4890.html
imported:
- "2019"
_4images_image_id: "13499"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13499 -->
vier seilwinden für die vier tragseile des greifers