---
layout: "image"
title: "containerbrücke von jan Knobbe - greifer (3)"
date: "2008-02-01T17:44:12"
picture: "containerbruecke6.jpg"
weight: "6"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13497
- /detailsbbb0-2.html
imported:
- "2019"
_4images_image_id: "13497"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13497 -->
