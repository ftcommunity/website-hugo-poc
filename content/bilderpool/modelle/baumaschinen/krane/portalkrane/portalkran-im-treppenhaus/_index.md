---
layout: "overview"
title: "Portalkran im Treppenhaus"
date: 2020-02-22T08:11:32+01:00
legacy_id:
- /php/categories/3536
- /categories60a2.html
- /categoriescc0c.html
- /categoriesb2a3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3536 --> 
Bevor der Kran völlig im Staub versinkt, wird er nun endlich abgebaut. Evtl. sollte ich mir angewöhnen Fotos direkt nach dem Bau zu machen...

Ziel war es einen einfach Kran zu haben, mit dem der Sohnemann kleine "Lasten" durch das Treppenhaus heben kann. Herausgekommen ist dieser ferngesteuerte Portalkran. Wir hatten viel Spaß, doch nach fast einem Jahr muss er etwas neuem weichen.