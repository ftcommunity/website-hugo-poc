---
layout: "image"
title: "Portalkran Seitenansicht"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-3.jpg"
weight: "3"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
schlagworte: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/48199
- /detailse6ab.html
imported:
- "2019"
_4images_image_id: "48199"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48199 -->
Seitenansicht des Kranes. Die Seilwinde wird von einem alten M-Motor angetrieben.