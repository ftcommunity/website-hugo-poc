---
layout: "image"
title: "Portalkran von schräg oben"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-6.jpg"
weight: "6"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/48202
- /details1718.html
imported:
- "2019"
_4images_image_id: "48202"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48202 -->
Ich freue mich immer wieder, wie gut sich Fischertechnik an örtliche Gegebenheiten oder systemfremde Materialien anpassen lässt. In diesem Fall unsere Treppe mit Geländer.