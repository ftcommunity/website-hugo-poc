---
layout: "image"
title: "Molenkran Antrieb"
date: "2009-05-14T16:34:27"
picture: "FTC-Antrieb.jpg"
weight: "2"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/24016
- /details710f.html
imported:
- "2019"
_4images_image_id: "24016"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24016 -->
Angetrieben wird das Fahrwerk (die mittleren Achsen) über mehrere
Zahnräder. Die äußeren Laufräder dienen als zusätzliche Stütze. Auf
der rechten Bildseite sind übrigens die beiden Endschalter des Fahr-
werks zu sehen. Der Kabelbaum dort entspringt dem Hauptanschluß
an dieser Seite.
