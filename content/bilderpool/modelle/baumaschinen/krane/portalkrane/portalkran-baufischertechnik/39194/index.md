---
layout: "image"
title: "E-Magnet mit Spiralkabel"
date: "2014-08-08T21:21:23"
picture: "DSC00047.jpg"
weight: "6"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39194
- /details8ce2.html
imported:
- "2019"
_4images_image_id: "39194"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39194 -->
Den Haken habe ich durch einen E-Magenten ersetzt, welcher über ein Spiralkabel versorgt wird.