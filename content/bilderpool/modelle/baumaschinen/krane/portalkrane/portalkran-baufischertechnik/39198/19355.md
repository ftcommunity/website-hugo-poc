---
layout: "comment"
hidden: true
title: "19355"
date: "2014-08-09T22:50:18"
uploadBy:
- "bauFischertechnik"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

sie hat was mit dem Portalkran zu tun.

Das mit dem Müll sollte eigentlich automatisch durch verschiedene Kugelgewichte geschehen, ließ sich aber nicht realisieren, aufgrund von Zeitmangel (Schule). So entscheidet der Kranführer, was weiter kommt.

Der Zylinder ist von zwei Stahlstangen gestützt und fängt die Kugeln, dass sie nicht durchrauschen.

Gruß
Pascal