---
layout: "image"
title: "neue Seilwinden"
date: "2014-08-08T21:21:23"
picture: "DSC00235.jpg"
weight: "4"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39192
- /detailsf0fb.html
imported:
- "2019"
_4images_image_id: "39192"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39192 -->
Ich habe die Seilwinden aus dem Orginal durch diese ersetzt, da die des Orginals durchdrehten und man nach einiger zeit einen Seildurchhänger hatte