---
layout: "image"
title: "Not-Aus-Taster und Reedkontakt"
date: "2014-08-08T21:21:23"
picture: "DSC00234.jpg"
weight: "11"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
schlagworte: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- /php/details/39199
- /details3cae-2.html
imported:
- "2019"
_4images_image_id: "39199"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39199 -->
Der Kran verfügt über eine Not-Aus-Funktion, die den Karan stoppt.
Der Kran kann nur durch die aktivierung des Reedkontakts wieder die Arbeit aufnehmen