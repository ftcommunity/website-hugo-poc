---
layout: "image"
title: "Portalkran 2"
date: "2007-01-12T17:00:21"
picture: "portalkran2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8375
- /detailsb204.html
imported:
- "2019"
_4images_image_id: "8375"
_4images_cat_id: "772"
_4images_user_id: "502"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8375 -->
