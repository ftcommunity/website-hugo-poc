---
layout: "image"
title: "PortalKrane"
date: "2009-05-22T14:40:02"
picture: "Krane_2_001_2.jpg"
weight: "11"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24082
- /detailsfac3-2.html
imported:
- "2019"
_4images_image_id: "24082"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T14:40:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24082 -->
