---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:23"
picture: "Krane_2_011.jpg"
weight: "20"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24091
- /details284d.html
imported:
- "2019"
_4images_image_id: "24091"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24091 -->
Back of the kran showing the interface and akku.

Rückseite des der Kran, zeigt die Schnittstelle und der Akku
