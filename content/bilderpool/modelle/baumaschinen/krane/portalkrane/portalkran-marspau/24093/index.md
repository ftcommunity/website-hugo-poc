---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Krane_2_013.jpg"
weight: "22"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24093
- /details3182.html
imported:
- "2019"
_4images_image_id: "24093"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24093 -->
Left side showing the operator cabin.

Links, die zeigen der Operator-Kabine
