---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Krane_2_021.jpg"
weight: "25"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24096
- /details12eb.html
imported:
- "2019"
_4images_image_id: "24096"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24096 -->
Container and motor with chains and sprokect wheels to make the whole kran travel.

Container und Motor mit Ketten und Sprokect Räder, die gesamte Kran Reisen zu machen.
