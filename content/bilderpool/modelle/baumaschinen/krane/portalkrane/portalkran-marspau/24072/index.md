---
layout: "image"
title: "PortalKran"
date: "2009-05-21T22:29:48"
picture: "Krane_1_009.jpg"
weight: "1"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24072
- /details92b7.html
imported:
- "2019"
_4images_image_id: "24072"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24072 -->
