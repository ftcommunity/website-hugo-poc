---
layout: "image"
title: "PortalKran (Marspau) update"
date: "2009-06-15T22:51:45"
picture: "Kran_4_007_2.jpg"
weight: "32"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24390
- /detailsb6ef-2.html
imported:
- "2019"
_4images_image_id: "24390"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24390 -->
Here you see the other side of the 
PowerMotor that make the whole portalkran
move back and forth along a theoretical
harbor dock.

Hier sehen Sie die andere Seite der PowerMotor, die die gesamte portalkran
entlang einer theoretischen hin-und her bewegen
Harbor Station.