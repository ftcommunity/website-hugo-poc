---
layout: "image"
title: "Portalkrane"
date: "2009-05-22T20:56:48"
picture: "Krane_2_007_2.jpg"
weight: "17"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24088
- /details8797-2.html
imported:
- "2019"
_4images_image_id: "24088"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T20:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24088 -->
Detail of the worm screw that move a part to actuate one of the hook limit switch and block the light beam to the phototransistor to stop the hook at the low position. Thanks to "Laserman" for the idea.

Detail der der Wurm-Schraube, die einen Teil eines der Haken Grenzwert zu betätigen verschieben wechseln und blockieren den Lichtstrahl um die Phototransistor der Haken an der niedrigen Position zu beenden. Danke der "Laserman" für die Idee.