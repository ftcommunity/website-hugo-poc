---
layout: "image"
title: "PortalKran"
date: "2009-05-22T14:40:02"
picture: "Krane_2_009.jpg"
weight: "10"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24081
- /detailsbada.html
imported:
- "2019"
_4images_image_id: "24081"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T14:40:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24081 -->
