---
layout: "image"
title: "PortalKran(marspau) update"
date: "2009-06-15T22:51:46"
picture: "Kran_4_009.jpg"
weight: "33"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24391
- /detailsbd43-2.html
imported:
- "2019"
_4images_image_id: "24391"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-06-15T22:51:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24391 -->
Here you see the other side of the 
PowerMotor that make the whole portalkran
move back and forth along a theoretical
harbor dock.


Hier sehen Sie die andere Seite der PowerMotor, die die gesamte portalkran
entlang einer theoretischen hin-und her bewegen
Harbor Station.