---
layout: "image"
title: "Molenkran Ausleger"
date: "2009-05-14T16:34:27"
picture: "FTC-AuslegerLED2.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/24017
- /details5203-2.html
imported:
- "2019"
_4images_image_id: "24017"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24017 -->
Hier ist eine der blinkenden LEDs und die vordere Endabschaltung
der Laufkatze zu sehen. Die Blinkelektronik sowie einzelne Steuer-
elemente sind in Schaltkästen im oberen Gitterwerk plaziert. Recht
gut erkennbar ist hier auch die dezente Verkabelung.

Daneben im kleinen Bildausschnitt zu sehen ist die LED (fixiert an
einer eingekerbten I-Strebe 15) auf der Spitze des Krans.
