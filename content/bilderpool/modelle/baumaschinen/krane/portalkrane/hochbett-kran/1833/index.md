---
layout: "image"
title: "Hochbett-Kran_002"
date: "2003-10-14T11:35:27"
picture: "Portalkran_IR_002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Kran", "Laufkran"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1833
- /details6917.html
imported:
- "2019"
_4images_image_id: "1833"
_4images_cat_id: "195"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:35:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1833 -->
Ein Kran der per IR Kekse und andere Sachen ins Hochbett hebt.