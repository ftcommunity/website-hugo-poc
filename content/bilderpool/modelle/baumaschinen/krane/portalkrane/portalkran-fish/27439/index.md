---
layout: "image"
title: "Antrieb Räder"
date: "2010-06-08T16:27:43"
picture: "portalkranfish4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27439
- /details78c4.html
imported:
- "2019"
_4images_image_id: "27439"
_4images_cat_id: "1969"
_4images_user_id: "1113"
_4images_image_date: "2010-06-08T16:27:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27439 -->
Die Räder werden über einen Powermotor angetieben. Die Antriebskraft wird mit Stangen zu den Rädern Übertragen.