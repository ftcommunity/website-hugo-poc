---
layout: "image"
title: "Laufkatze"
date: "2010-06-08T16:27:43"
picture: "portalkranfish3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27438
- /details7956.html
imported:
- "2019"
_4images_image_id: "27438"
_4images_cat_id: "1969"
_4images_user_id: "1113"
_4images_image_date: "2010-06-08T16:27:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27438 -->
Die Laufkatze mit Seil und Haken.