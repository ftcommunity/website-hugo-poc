---
layout: "image"
title: "6"
date: "2012-03-03T21:35:26"
picture: "halbportalkran06.jpg"
weight: "6"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34534
- /details2249.html
imported:
- "2019"
_4images_image_id: "34534"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34534 -->
Detail vom angetriebenen Rad an der Stütze - 1