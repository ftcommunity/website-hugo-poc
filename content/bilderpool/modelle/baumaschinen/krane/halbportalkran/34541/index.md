---
layout: "image"
title: "13"
date: "2012-03-03T21:35:39"
picture: "halbportalkran13.jpg"
weight: "13"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34541
- /details10f9.html
imported:
- "2019"
_4images_image_id: "34541"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34541 -->
Detail Kranfahrantrieb am Laufsteg.