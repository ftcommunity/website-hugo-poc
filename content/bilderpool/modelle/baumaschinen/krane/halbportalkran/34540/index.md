---
layout: "image"
title: "12"
date: "2012-03-03T21:35:39"
picture: "halbportalkran12.jpg"
weight: "12"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34540
- /details3143.html
imported:
- "2019"
_4images_image_id: "34540"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34540 -->
Führerhaus Rückseite mit Zugangstreppe.