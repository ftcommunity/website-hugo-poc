---
layout: "image"
title: "11"
date: "2012-03-03T21:35:39"
picture: "halbportalkran11.jpg"
weight: "11"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34539
- /detailsb9e0.html
imported:
- "2019"
_4images_image_id: "34539"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34539 -->
Führerhaus mit Kranführer.