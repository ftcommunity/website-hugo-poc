---
layout: "image"
title: "14"
date: "2012-03-03T21:35:39"
picture: "halbportalkran14.jpg"
weight: "14"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34542
- /details9d4f.html
imported:
- "2019"
_4images_image_id: "34542"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34542 -->
Detail Seiltrommel. Die Trommel bestet aus Rädern 23 und Rad 30 auf Flachnabe als Bordscheiben.