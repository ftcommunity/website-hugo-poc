---
layout: "image"
title: "5"
date: "2012-03-03T21:35:26"
picture: "halbportalkran05.jpg"
weight: "5"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/34533
- /detailsceb1.html
imported:
- "2019"
_4images_image_id: "34533"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34533 -->
Laufrad angetrieben am oberen Kopfträger. Die Steuerung über Taster war noch sehr rudimentär, das wird beim nächsten Projekt auch anders gelöst.