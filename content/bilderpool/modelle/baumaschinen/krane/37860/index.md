---
layout: "image"
title: "Detail Kran mit Greifer"
date: "2013-11-29T21:55:24"
picture: "IMG_2908.jpg"
weight: "19"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- /php/details/37860
- /details389c-2.html
imported:
- "2019"
_4images_image_id: "37860"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37860 -->
Teil der Übersetzung des Mechanismus der Turmdrehung.