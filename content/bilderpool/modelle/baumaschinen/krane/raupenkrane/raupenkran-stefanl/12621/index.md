---
layout: "image"
title: "Raupenkran 21"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12621
- /detailsd8d6-5.html
imported:
- "2019"
_4images_image_id: "12621"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12621 -->
