---
layout: "image"
title: "Raupenkran 25"
date: "2007-11-24T12:18:23"
picture: "raupenkran04.jpg"
weight: "25"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12802
- /detailsde71-4.html
imported:
- "2019"
_4images_image_id: "12802"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12802 -->
