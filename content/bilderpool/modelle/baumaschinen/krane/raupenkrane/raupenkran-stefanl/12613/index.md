---
layout: "image"
title: "Raupenkran 13"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12613
- /detailse032-2.html
imported:
- "2019"
_4images_image_id: "12613"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12613 -->
