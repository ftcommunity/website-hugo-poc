---
layout: "image"
title: "Raupenkran 20"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12620
- /details0e8a.html
imported:
- "2019"
_4images_image_id: "12620"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12620 -->
