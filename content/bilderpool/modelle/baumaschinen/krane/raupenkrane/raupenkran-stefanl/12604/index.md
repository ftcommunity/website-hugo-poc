---
layout: "image"
title: "Raupenkran 4"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12604
- /details1540.html
imported:
- "2019"
_4images_image_id: "12604"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12604 -->
