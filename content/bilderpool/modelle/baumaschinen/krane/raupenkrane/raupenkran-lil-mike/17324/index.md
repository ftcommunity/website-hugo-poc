---
layout: "image"
title: "gesamt5"
date: "2009-02-07T23:09:43"
picture: "raupenkran005.jpg"
weight: "2"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- /php/details/17324
- /detailse4fb-3.html
imported:
- "2019"
_4images_image_id: "17324"
_4images_cat_id: "1559"
_4images_user_id: "822"
_4images_image_date: "2009-02-07T23:09:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17324 -->
