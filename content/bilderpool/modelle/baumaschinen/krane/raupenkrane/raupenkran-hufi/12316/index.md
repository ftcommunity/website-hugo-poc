---
layout: "image"
title: "Raupenkran & Rechner"
date: "2007-10-25T22:06:54"
picture: "DSC03849.jpg"
weight: "13"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12316
- /details7a6b.html
imported:
- "2019"
_4images_image_id: "12316"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-10-25T22:06:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12316 -->
