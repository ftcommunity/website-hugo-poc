---
layout: "image"
title: "Raupenkran & Rechner"
date: "2007-10-25T16:46:26"
picture: "SUPu.jpg"
weight: "9"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12304
- /detailsca2f-2.html
imported:
- "2019"
_4images_image_id: "12304"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-10-25T16:46:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12304 -->
Ein neues Projekt ist im Laufen.
Werden soll das mal ein Kran unter Einsatz von etwas mehr Elektronik als im Normalfall, einem  bewährten Z72 Drehkranz und etlicher Ideen von denen sich noch zeigen wird wie weit ich die umsetze.
Fix ist das das Teil in allen Funktionen per analoger Joysticks bedient werden soll wobei die Bedienung letztendlich per Digitalfunk zum Kran kommen soll.
Das Raupenfahrwerk wir von 4 Motoren angetrieben, der Drehkranz von 2 Stück.
Der Rechner am Kran selbst wird die gesamte Steuerung der Drehzahl der Motoren auf 8 Kanälen vom empfangenen Funk weg übernehmen, auch soll evtl. einiges an Sensorik rein was z.B. verhindern soll das man den Kran durch falsche Bedienung kippen kann bzw. Informationen über Last, Akkuzustand usw. zurücksendet.
Wie gesagt, wir sicher noch interessant.