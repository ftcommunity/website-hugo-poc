---
layout: "image"
title: "Teleskopausleger Details"
date: "2007-11-04T21:50:49"
picture: "041107E.jpg"
weight: "20"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12475
- /details9710-3.html
imported:
- "2019"
_4images_image_id: "12475"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-04T21:50:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12475 -->
Die Gesamte Kraft wir von den Alu´s getragen. Die restlichen Teile sind mehr Deko als funktionell, aber ein zu schmaler Ausleger würde auch optisch nichts hermachen.
Das Ganze ist auf Kugellagern geführt 1 mal auf der Unterseite 3 mal auf der Oberseite und mit M4 Gewindestagen verschraubt.
Diese Verschraubung trägt auch sehr wesentlich zur Stabilität bei.