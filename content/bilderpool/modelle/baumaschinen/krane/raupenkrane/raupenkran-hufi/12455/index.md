---
layout: "image"
title: "Teleskopausleger Details"
date: "2007-11-04T20:22:26"
picture: "041107A.jpg"
weight: "19"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12455
- /details8e6f.html
imported:
- "2019"
_4images_image_id: "12455"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-04T20:22:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12455 -->
Der Antrieb erfolgt mit einem auf eine Messingachse aufgezogenen Gummischlauch(blau).
Als Motor dient ein sehr hochübersetzter Miniatur Getriebe Motor der an der Abtriebswelle ca. 60U/min und ein hohes Drehmoment bringt (Selber Motor wie die 6 Stk. am Fahrwerk bzw. fürs Drehen)
Damit das Drehmoment auch ankommt sind die Motorachse und die Messingachse flach angeschliffen damit die Rastritzel Z 10 draufpassen.
Die Umlenkung der Seile in den Ausleger hinein muss natürlich auf der Auslegerbefestigungsachse passieren damit sich die Seilspannung beim Heben und Senken des Haupauslegers möglich wenig ändert.