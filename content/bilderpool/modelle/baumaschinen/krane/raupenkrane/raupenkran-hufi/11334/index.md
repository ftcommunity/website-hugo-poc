---
layout: "image"
title: "Raupenkran"
date: "2007-08-10T17:07:04"
picture: "DSC03603.jpg"
weight: "7"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/11334
- /details53bc.html
imported:
- "2019"
_4images_image_id: "11334"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-08-10T17:07:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11334 -->
max Höhe 1,70m