---
layout: "image"
title: "Raupenkran mit Teleskopausleger"
date: "2007-01-08T16:53:44"
picture: "DSC03566.jpg"
weight: "3"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/8332
- /details3ad4.html
imported:
- "2019"
_4images_image_id: "8332"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-01-08T16:53:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8332 -->
Unterbau