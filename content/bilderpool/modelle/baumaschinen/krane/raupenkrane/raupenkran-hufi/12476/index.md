---
layout: "image"
title: "Teleskopausleger Details"
date: "2007-11-04T21:50:49"
picture: "041107C.jpg"
weight: "21"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12476
- /details52a5-2.html
imported:
- "2019"
_4images_image_id: "12476"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-04T21:50:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12476 -->
Unteransicht mit der Kugellagerung und Seilumlekung bzw. Befestigung