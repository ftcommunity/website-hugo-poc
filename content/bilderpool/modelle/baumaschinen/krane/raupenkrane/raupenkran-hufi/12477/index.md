---
layout: "image"
title: "Teleskopausleger Details"
date: "2007-11-04T21:50:49"
picture: "041107D.jpg"
weight: "22"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12477
- /detailsdfd1.html
imported:
- "2019"
_4images_image_id: "12477"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-04T21:50:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12477 -->
Befestigung am inneren Teleskopteil