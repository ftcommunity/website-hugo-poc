---
layout: "image"
title: "Raupenkran & Rechner"
date: "2007-10-27T23:48:04"
picture: "DSC03871.jpg"
weight: "16"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/12352
- /details8342.html
imported:
- "2019"
_4images_image_id: "12352"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-10-27T23:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12352 -->
