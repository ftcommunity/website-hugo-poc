---
layout: "image"
title: "Kran"
date: "2007-05-06T21:37:12"
picture: "raupenkran2.jpg"
weight: "25"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10329
- /detailsa902.html
imported:
- "2019"
_4images_image_id: "10329"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-06T21:37:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10329 -->
Wippausleger etwas geneigt.