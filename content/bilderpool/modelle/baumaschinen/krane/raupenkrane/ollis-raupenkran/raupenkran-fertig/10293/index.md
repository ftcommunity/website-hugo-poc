---
layout: "image"
title: "Fahrgestell alt"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli01.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10293
- /details64d3-2.html
imported:
- "2019"
_4images_image_id: "10293"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10293 -->
Das ist das alte Fahrgestell. Est ist sehr instabil.