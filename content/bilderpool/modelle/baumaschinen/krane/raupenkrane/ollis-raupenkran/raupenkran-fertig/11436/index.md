---
layout: "image"
title: "Einzelteile"
date: "2007-09-02T12:56:12"
picture: "einzelteile1.jpg"
weight: "31"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11436
- /details0848.html
imported:
- "2019"
_4images_image_id: "11436"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-09-02T12:56:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11436 -->
Da ich mich jetzt endlich dazu durchgerungen habe meinen kran auseinander zubauen, wollte ich nochmal alle Einzelteile fotografieren. Das sind sie.