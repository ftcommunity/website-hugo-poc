---
layout: "image"
title: "Statikteile"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli03.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10295
- /details5958-4.html
imported:
- "2019"
_4images_image_id: "10295"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10295 -->
Das hier sind alle Statikteile vom Kran.