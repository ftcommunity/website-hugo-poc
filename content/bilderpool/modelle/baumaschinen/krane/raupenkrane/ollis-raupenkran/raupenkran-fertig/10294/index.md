---
layout: "image"
title: "Fahrgestell neu"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli02.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10294
- /details6e71.html
imported:
- "2019"
_4images_image_id: "10294"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10294 -->
Das ist das neue Fahrgestell. Es ist sehr stabil, da es mit Aluprofilen gebaut worden ist.