---
layout: "comment"
hidden: true
title: "8030"
date: "2008-12-20T02:17:42"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Interessante Sache. Nur wenn du dir schon diesen Aufwand gönnst, dann
stell das doch einfach mal in ein Dokument zusammen kommentiert dort
mit ein paar Zeilen, die komplizierte Bauabschnitte näher beschreiben.
Und fertig ist eine Bauanleitung, wie es sie hier mittlerweile viele gibt.

Ich selbst hab' das auch schon oft gemacht, und einige davon sind hier
publiziert.

Gruß, Thomas