---
layout: "comment"
hidden: true
title: "3277"
date: "2007-05-29T18:41:34"
uploadBy:
- "Olli"
license: "unknown"
imported:
- "2019"
---
Der Kran ist jetzt auch auf der fischertechnikWebsite als Projekt zu sehen. Allerdings sind dort einige Bilder doppelt und die Daten sind ein bisschen falsch. Ich werde Herrn Brezing nochmal eine E-mail schreiben.

Gruß Olli