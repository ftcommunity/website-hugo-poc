---
layout: "image"
title: "Fahrgestell"
date: "2007-05-05T21:04:14"
picture: "raupenkranolli11.jpg"
weight: "11"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10303
- /details7552.html
imported:
- "2019"
_4images_image_id: "10303"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10303 -->
Fahrgestell von oben.