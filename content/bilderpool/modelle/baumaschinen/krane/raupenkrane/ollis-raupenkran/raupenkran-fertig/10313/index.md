---
layout: "image"
title: "Gesamtansicht"
date: "2007-05-05T21:04:15"
picture: "raupenkranolli21.jpg"
weight: "21"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/10313
- /details5a7a.html
imported:
- "2019"
_4images_image_id: "10313"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:15"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10313 -->
Nochmal der Kran insgesamt.