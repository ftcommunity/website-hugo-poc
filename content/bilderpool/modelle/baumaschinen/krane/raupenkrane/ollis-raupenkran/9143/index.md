---
layout: "image"
title: "Ansicht"
date: "2007-02-24T13:52:51"
picture: "DSCI0060.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Raupenkran"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9143
- /details2545-2.html
imported:
- "2019"
_4images_image_id: "9143"
_4images_cat_id: "832"
_4images_user_id: "504"
_4images_image_date: "2007-02-24T13:52:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9143 -->
Ausleger mit Fahrgestell.