---
layout: "image"
title: "Ferngesteuerter Raupenkran (Seitenansicht)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-12.jpg"
weight: "12"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47257
- /detailse48d.html
imported:
- "2019"
_4images_image_id: "47257"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47257 -->
Die Kette wird hinten von einem 20er Rastkettenrad angetrieben und läuft vorne und mittig über weitere 20er Kettenräder. Oberhalb des mittigen Kettenrades befinden sich Z10 Rastzahnräder, welche nach links und rechts verschiebbar sind, um die Kettenspannung einzustellen.