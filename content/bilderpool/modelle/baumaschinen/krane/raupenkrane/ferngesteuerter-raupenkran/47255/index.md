---
layout: "image"
title: "Ferngesteuerter Raupenkran (Unterseite/Detail)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-10.jpg"
weight: "10"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- /php/details/47255
- /details3d79.html
imported:
- "2019"
_4images_image_id: "47255"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47255 -->
In der Mitte ist der Empfänger zu sehen.
Links davon sitzen zwei graue Taster, welche als Polwendeschalter verschaltet sind und durch den dazwischen sitzenden Servo betätigt werden. Auf diese Weise kann man auch bei einem Kettenfahrzeug den vierten, ausschließlich für Servus gedachten, Fernsteuerungskanal sinnvoll nutzen. Natürlich gibt es dabei keine Drehzahlabstufungen. Es gibt nur Aus oder Ein. Über diese Konstruktion werden wahlweise die Drehung oder Neigung des Kranes gesteuert.
Rechts sind die alten Encodermotoren für den Antrieb sichtbar. Leider hatte ich keine anderen geeigneten Motoren und so mussten sich diese beiden mehr schlecht als recht abmühen. Doch es reichte...