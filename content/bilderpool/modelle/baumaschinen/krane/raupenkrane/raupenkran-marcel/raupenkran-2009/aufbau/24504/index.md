---
layout: "image"
title: "Aufrichten Derrickmast"
date: "2009-07-07T18:23:47"
picture: "20090628-140702-Aufbau.jpg"
weight: "2"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- /php/details/24504
- /detailse5cf-3.html
imported:
- "2019"
_4images_image_id: "24504"
_4images_cat_id: "1685"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T18:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24504 -->
