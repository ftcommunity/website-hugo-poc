---
layout: "image"
title: "raupenkran"
date: "2004-01-27T14:00:50"
picture: "honjo_raupenkran_14.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- /php/details/2087
- /detailse4d4.html
imported:
- "2019"
_4images_image_id: "2087"
_4images_cat_id: "228"
_4images_user_id: "14"
_4images_image_date: "2004-01-27T14:00:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2087 -->
