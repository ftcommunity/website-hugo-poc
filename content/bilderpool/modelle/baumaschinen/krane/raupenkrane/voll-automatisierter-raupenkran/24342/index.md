---
layout: "image"
title: "Kran klein"
date: "2009-06-12T19:42:07"
picture: "cn27.jpg"
weight: "27"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24342
- /detailse6d6.html
imported:
- "2019"
_4images_image_id: "24342"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:07"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24342 -->
Zu guter letzt gibt es noch einmal den Kran, diesmal allerdings eingefahren.