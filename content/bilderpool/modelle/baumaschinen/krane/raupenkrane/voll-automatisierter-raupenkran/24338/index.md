---
layout: "image"
title: "Die Größe des Krans"
date: "2009-06-12T19:42:06"
picture: "cn23.jpg"
weight: "23"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24338
- /details0d47-2.html
imported:
- "2019"
_4images_image_id: "24338"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:06"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24338 -->
Der Kran ist variierbar in seiner Größe. Das lässt sich momentan leider nur mir Metallstangen die hineingeschoben werden lösen. Ich muss mir noch etwas einfallen lassen auch das noch zu motorisieren.