---
layout: "image"
title: "Ein Fototransistor"
date: "2009-06-12T19:41:21"
picture: "cn15.jpg"
weight: "15"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24330
- /details36f1-2.html
imported:
- "2019"
_4images_image_id: "24330"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24330 -->
Hier sieht man besagten Fototransistor