---
layout: "image"
title: "Ein weiterer Endschalter"
date: "2009-06-12T19:41:20"
picture: "cn11.jpg"
weight: "11"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24326
- /details9441-2.html
imported:
- "2019"
_4images_image_id: "24326"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24326 -->
Damit die Katze nicht weiter läuft als der Arm lang ist, gibt es auch hier wieder Endschalter.