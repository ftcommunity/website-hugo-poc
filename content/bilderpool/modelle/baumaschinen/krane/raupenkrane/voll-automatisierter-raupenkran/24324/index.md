---
layout: "image"
title: "Endschalter"
date: "2009-06-12T19:41:12"
picture: "cn09.jpg"
weight: "9"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24324
- /details5511.html
imported:
- "2019"
_4images_image_id: "24324"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24324 -->
Hier sieht man die Endschalter für den Arm.