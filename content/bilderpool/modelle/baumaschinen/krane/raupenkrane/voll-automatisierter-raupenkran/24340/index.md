---
layout: "image"
title: "Der Kran vom höchsten Punkt"
date: "2009-06-12T19:42:07"
picture: "cn25.jpg"
weight: "25"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24340
- /detailsc47c-2.html
imported:
- "2019"
_4images_image_id: "24340"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:07"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24340 -->
Einmal ein Bild vom höchsten Punkt des Krans nach unten.