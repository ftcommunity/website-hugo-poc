---
layout: "image"
title: "Der Antrieb"
date: "2009-06-12T19:41:12"
picture: "cn06.jpg"
weight: "6"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24321
- /detailsd7b7.html
imported:
- "2019"
_4images_image_id: "24321"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24321 -->
Hier der Antrieb noch einmal von oben. Auch die Seilwinde ist noch sichtbar am linken Rand.