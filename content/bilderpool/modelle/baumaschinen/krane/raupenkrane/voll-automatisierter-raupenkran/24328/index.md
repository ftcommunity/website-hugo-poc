---
layout: "image"
title: "Die Laufkatze"
date: "2009-06-12T19:41:21"
picture: "cn13.jpg"
weight: "13"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24328
- /details7df1-2.html
imported:
- "2019"
_4images_image_id: "24328"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24328 -->
Eine Laufkatze mal von unten...