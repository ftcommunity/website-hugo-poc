---
layout: "image"
title: "Stützräder"
date: "2009-06-12T19:41:11"
picture: "cn02.jpg"
weight: "2"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24317
- /details9a6f.html
imported:
- "2019"
_4images_image_id: "24317"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24317 -->
Kleine Seilrollen helfen dem Kran stabil zu stehen und das Gewicht abzufangen, das auf dem Drehkranz lastet.