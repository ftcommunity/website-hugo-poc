---
layout: "image"
title: "Spiegel-Update"
date: "2009-06-18T14:10:57"
picture: "update2.jpg"
weight: "29"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24414
- /details5bfd-2.html
imported:
- "2019"
_4images_image_id: "24414"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-18T14:10:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24414 -->
Hier sieht man den Spiegel eingebaut auf dem Haken. Er ist mit einem Zapfen auf einem Stein befestigt, während die andere Hälfte in der Luft schwebt (nicht unbedingt die sauberste Lösung, aber es funktioniert).