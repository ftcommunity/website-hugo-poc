---
layout: "image"
title: "Motorisierung des Arms"
date: "2009-06-12T19:42:06"
picture: "cn21.jpg"
weight: "21"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24336
- /details5cde.html
imported:
- "2019"
_4images_image_id: "24336"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:06"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24336 -->
Man sieht einen weiteren Motor. Dieser ist für die Stellung des Armes zuständig