---
layout: "image"
title: "Der Antrieb"
date: "2009-06-12T19:41:12"
picture: "cn05.jpg"
weight: "5"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24320
- /detailsa030-2.html
imported:
- "2019"
_4images_image_id: "24320"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24320 -->
Das ist der Kettenantrieb, angetrieben von zwei Powermotoren 50:1. Leider konnte ich bisher noch nicht testen, ob der Kran auch wirklich fahren kann, denn das Pakett ist zu glatt.