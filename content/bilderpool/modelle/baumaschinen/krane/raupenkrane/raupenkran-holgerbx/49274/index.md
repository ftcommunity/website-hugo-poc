---
layout: "image"
title: "Fahrwerk mit Drehkranz"
date: 2021-12-28T14:05:42+01:00
picture: "FahrwerkmitDrehkranz.jpg"
weight: "12"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Fahrwerk", "Raupenkran"]
uploadBy: "Website-Team"
license: "unknown"
---

Zusammenbau Fahrwerk mit Drehkranz