---
layout: "image"
title: "Raupenkran 2"
date: 2021-12-28T14:05:33+01:00
picture: "Raupenkran2.jpg"
weight: "2"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Raupenkran", "Ausleger"]
uploadBy: "Website-Team"
license: "unknown"
---

Raupenkran mit Ausleger hoch