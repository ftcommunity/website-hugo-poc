---
layout: "image"
title: "Flaschenzug-Stop"
date: 2021-12-28T14:05:38+01:00
picture: "Fzugstop.jpg"
weight: "16"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Flaschenzug", "Endstop"]
uploadBy: "Website-Team"
license: "unknown"
---

Flaschenzug Ausleger mit Stop