---
layout: "image"
title: "Drehkranz mit Achse"
date: 2021-12-28T14:05:22+01:00
picture: "DrehkranzmitAchse.jpg"
weight: "8"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Drehkranz", "Drehkranzachse"]
uploadBy: "Website-Team"
license: "unknown"
---

Drehkranz mit Achse