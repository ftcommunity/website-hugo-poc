---
layout: "overview"
title: "Raupenkran-HolgerBx"
date: 2021-12-28T14:05:21+01:00
---

Das Modell des Raupenkrans folgt dem Original von Liebherr LR1110, ein beeindruckender Kran mit all seinen variablen Funktionen und den vielfältigen Einsatz-Möglichkeiten. Hat mich schon länger fasziniert.
Eine Herausforderung ist natürlich immer der Drehkranz, der alle Kräfte aufnehmen muss, nach einigem Probieren fand ich eine Lösung die nicht zu sehr aufbaut (und nicht zu groß wird und somit die Größe des Modells vorschreibt). 
Als Gegengewicht eignen sich drei parallel geschaltete Batteriepacks(Antrieb für die Seilwinden den Ausleger und den Drehkranz), das vierte Pack (treibt das Fahrwerk) kann als Zusatzgewicht hinzugefügt werden für extreme Auslenkungen der Ausleger.
Dazu ein Steuerpult zur Steuerung der Funktionen. Das Modell kann natürlich auch über einen Trafo versorgt werden, der direkt an die beiden Kontaktschienen angeschlossen wird. (siehe Foto Anschluss-Steuereinheit)
