---
layout: "image"
title: "Raupenkran 1"
date: 2021-12-28T14:05:46+01:00
picture: "Raupenkran1.jpg"
weight: "1"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Raupenkran", "Ausleger"]
uploadBy: "Website-Team"
license: "unknown"
---

Raupenkran mit Ausleger weit