---
layout: "image"
title: "Drehkranz-Laufrolle"
date: 2021-12-28T14:05:23+01:00
picture: "DrehkranzLaufrolle.jpg"
weight: "7"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Drehkranz", "Laufrolle"]
uploadBy: "Website-Team"
license: "unknown"
---

Drehkranz mit doppelter Laufrolle