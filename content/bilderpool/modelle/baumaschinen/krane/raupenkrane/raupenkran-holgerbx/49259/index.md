---
layout: "image"
title: "Auslegerzug"
date: 2021-12-28T14:05:24+01:00
picture: "Auslegerzug.jpg"
weight: "6"
konstrukteure: 
- "HolgerBx"
fotografen:
- "HolgerBx"
schlagworte: ["Zugansteuerung", "Ausleger"]
uploadBy: "Website-Team"
license: "unknown"
---

Zugansteuerung Ausleger