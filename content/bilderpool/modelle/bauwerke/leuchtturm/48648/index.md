---
layout: "image"
title: "Antrieb (2)"
date: 2020-05-01T11:16:26+02:00
picture: "2019-10-25 Leuchtturm3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Aufhängung der Antriebsachse.
