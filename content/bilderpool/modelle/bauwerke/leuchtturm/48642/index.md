---
layout: "image"
title: "Linsenkorb"
date: 2020-05-01T11:16:19+02:00
picture: "2019-10-25 Leuchtturm9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick von oben in den aufgesetzten Linsenkorb. Je drei Kombinationen von Hohlspiegel und gegenüber liegender Linse führen zur drei deutlichen Lichtflecken, die im Dunkeln über die Zimmerwände wandern. Der Hohlspiegel trägt nicht zum optischen Effekt bei, sieht aber halt gut aus ;-)
