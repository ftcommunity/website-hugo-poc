---
layout: "image"
title: "Antrieb (1)"
date: 2020-05-01T11:16:27+02:00
picture: "2019-10-25 Leuchtturm2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor geht über seine kleine Zusatzschnecke auf das feine Zahnrad aus einem Minimotor-U-Getriebeblock. Der geht über Rastachsen und zwei Kardangelenke (hier nicht sichtbar) nach oben zum Korbantrieb.
