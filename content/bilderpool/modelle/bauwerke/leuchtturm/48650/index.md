---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-01T11:16:28+02:00
picture: "2019-10-25 Leuchtturm1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Turm ist einfach gebaut und steht zur Geräuschdämpfung auf Federn. Ein XS-Motor treibt den Linsenkorb oben langsam an. Die Lampe darin steht nämlich fest und die Linsen drehen sich um sie herum.
