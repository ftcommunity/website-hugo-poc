---
layout: "image"
title: "Lagerung des Linsenkorbs (1)"
date: 2020-05-01T11:16:21+02:00
picture: "2019-10-25 Leuchtturm7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nur das im Bild linke Z10 treibt den Korb an, der hier abgenommen ist. Die anderen beiden Z10 dienen nur zur Führung.

Die Lampe ist mittels Rastadapter mittig und fest angebracht. Damit sich kein Stecker bei der Drehung des Korbs verhakt, sind die Litzen ohne Stecker mit einem 36495 Leuchtstein-Stopfen ec + em direkt im Leuchtstein festgeklemmt.
