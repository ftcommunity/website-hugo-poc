---
layout: "image"
title: "Antrieb (3)"
date: 2020-05-01T11:16:25+02:00
picture: "2019-10-25 Leuchtturm4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Und hier schließlich der Ein-/Ausschalter.
