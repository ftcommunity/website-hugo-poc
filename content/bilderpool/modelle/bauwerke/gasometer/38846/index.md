---
layout: "image"
title: "Kugelbahninnenleben"
date: "2014-05-25T17:49:46"
picture: "IMG_0080.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Gasometer", "Kugelbahn"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38846
- /details59a7.html
imported:
- "2019"
_4images_image_id: "38846"
_4images_cat_id: "2902"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38846 -->
Die Fliehkraft drückt die Kugeln während der Abwährtsfart durch die Rotation nach aussen, sodaß die Bahn selber nur eine Begrenzung aussen benötigt.
