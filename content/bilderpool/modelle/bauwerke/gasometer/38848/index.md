---
layout: "image"
title: "Aussenhülle Gasometer"
date: "2014-05-25T17:49:46"
picture: "IMG_0076.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38848
- /detailsd770.html
imported:
- "2019"
_4images_image_id: "38848"
_4images_cat_id: "2902"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38848 -->
was da wohl drinsteckt?!
