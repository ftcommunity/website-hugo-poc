---
layout: "image"
title: "von schräg oben"
date: "2014-05-25T17:49:46"
picture: "IMG_0077.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38847
- /detailsb1f2.html
imported:
- "2019"
_4images_image_id: "38847"
_4images_cat_id: "2902"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38847 -->
