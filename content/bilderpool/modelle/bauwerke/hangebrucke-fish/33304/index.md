---
layout: "image"
title: "Gesamtansicht Vorne"
date: "2011-10-23T16:00:07"
picture: "haengebrueckefish2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/33304
- /details7c6a.html
imported:
- "2019"
_4images_image_id: "33304"
_4images_cat_id: "2465"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33304 -->
siehe Projektbeschreibung