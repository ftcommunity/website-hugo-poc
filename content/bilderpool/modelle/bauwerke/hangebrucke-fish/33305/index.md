---
layout: "image"
title: "Schräg Oben"
date: "2011-10-23T16:00:07"
picture: "haengebrueckefish3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/33305
- /detailsf6b2.html
imported:
- "2019"
_4images_image_id: "33305"
_4images_cat_id: "2465"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33305 -->
siehe Projektbeschreibung