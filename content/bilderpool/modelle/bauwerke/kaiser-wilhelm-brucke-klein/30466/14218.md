---
layout: "comment"
hidden: true
title: "14218"
date: "2011-05-02T09:57:25"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald, danke für die Blumen!
So eine Brücke ist wirklich nur etwas für die graue Statik...
Probieren musste ich tatsächlich - sowohl das Modell als auch die Fotos haben einige Anläufe erfordert. Bei den Schatten habe ich aus der Not (ziemlich großes Modell, daher nicht so einfach im Haus mit genug Licht zu fotografieren) eine Tugend gemacht...
Gruß, Dirk