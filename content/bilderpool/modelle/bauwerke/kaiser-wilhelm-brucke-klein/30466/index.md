---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Längsansicht)"
date: "2011-04-16T18:30:46"
picture: "kaiserwilhelmbrueckewilhelmshavenklein5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30466
- /details6e09.html
imported:
- "2019"
_4images_image_id: "30466"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30466 -->
Das Original (http://www.ftcommunity.de/details.php?image_id=13455) wirkt breiter - vielleicht hätten wir es doch mit zwei Fahrbahnen versuchen sollen. Dann wären jedoch die Kreuzverstrebungen unterhalb der Brücke nicht mehr originalgetreu möglich gewesen, da die Länge der 169,6er X-Strebe dafür nicht ausreicht.