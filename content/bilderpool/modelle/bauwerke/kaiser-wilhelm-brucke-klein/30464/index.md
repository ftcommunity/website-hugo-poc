---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Detailansicht)"
date: "2011-04-16T18:30:46"
picture: "kaiserwilhelmbrueckewilhelmshavenklein3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30464
- /details45fa.html
imported:
- "2019"
_4images_image_id: "30464"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30464 -->
Das Gewicht der Brücke wird geradezu "schulbuchmäßig" auf Drehkranz und Rollenlager abgeleitet. Eine Statikkonstruktion vom Feinsten - wie für fischertechnik gemacht...