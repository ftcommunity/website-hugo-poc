---
layout: "image"
title: "Pflanzendrehteller-2"
date: "2010-07-18T19:16:42"
picture: "pflanzendreher-2010-07-18-002.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["Drehteller", "Drehkranz", "Getriebe", "Drehbewegung", "langsam"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27770
- /detailsa995.html
imported:
- "2019"
_4images_image_id: "27770"
_4images_cat_id: "656"
_4images_user_id: "9"
_4images_image_date: "2010-07-18T19:16:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27770 -->
Eine sehr, sehr langsame Bewegung.