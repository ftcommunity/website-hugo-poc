---
layout: "image"
title: "hausemodelfuersiemens17.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens17.jpg"
weight: "17"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27850
- /details34dc.html
imported:
- "2019"
_4images_image_id: "27850"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27850 -->
Das Haus wird die folgenden funktionen koennen:
Automatisiertes Tor und Garagentor
Aussenbeleuchtung mit Lichtsensor
Alarm aussen und innen (Tuer mit Magnetsensor) Alarmsimulation durch rote Lampen und einem Buzzer.
Poolbeleuchtung und Pumpensimulation durch blaue Lampe
Innenbeleuchtung
Deckenventilator
Boiler simulation durch LED's (rot=heiss  orange=wird erhitzt)
Sprinklersystem fuer Garten (gruene LED Simulation)
Solarzellen auf dem Dach, die den Boiler erhitzen

Steuerung wird durch eine Siemens LOGO! (SPS) erziehlt.