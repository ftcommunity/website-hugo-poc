---
layout: "image"
title: "hausemodelfuersiemens16.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens16.jpg"
weight: "16"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27849
- /detailsef9b.html
imported:
- "2019"
_4images_image_id: "27849"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27849 -->
