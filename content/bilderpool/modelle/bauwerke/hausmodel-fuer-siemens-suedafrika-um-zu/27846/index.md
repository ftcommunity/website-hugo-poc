---
layout: "image"
title: "hausemodelfuersiemens13.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens13.jpg"
weight: "13"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27846
- /detailsdf2d.html
imported:
- "2019"
_4images_image_id: "27846"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27846 -->
