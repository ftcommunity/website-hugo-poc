---
layout: "image"
title: "hausemodelfuersiemens02.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens02.jpg"
weight: "2"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27835
- /detailsbad8.html
imported:
- "2019"
_4images_image_id: "27835"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27835 -->
