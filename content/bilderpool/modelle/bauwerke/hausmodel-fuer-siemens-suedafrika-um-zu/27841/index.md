---
layout: "image"
title: "hausemodelfuersiemens08.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens08.jpg"
weight: "8"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27841
- /details7d1d.html
imported:
- "2019"
_4images_image_id: "27841"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27841 -->
