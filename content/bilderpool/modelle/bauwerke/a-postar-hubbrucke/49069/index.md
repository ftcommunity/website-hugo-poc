---
layout: "image"
title: "Barge from below"
date: 2021-03-16T19:01:05+01:00
picture: "15_Barge_from_below.jpg"
weight: "15"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

Here the barge is turned upside down to show the neodymium magnet that triggers the reed switches at the extreme positions. The pulleys allow the vehicle to run smoothly and with very little power, so I can drive the motor at a slow speed (a setting of 3 seems to be ideal) to better simulate the real thing.