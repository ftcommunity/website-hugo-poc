---
layout: "image"
title: "Semaphore assembly"
date: 2021-03-16T19:00:53+01:00
picture: "8_Semaphore_assembly.jpg"
weight: "8"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

The semaphores have no visible plugs: Their wires are inserted directly inside the lamp holder sockets and secured with a small piece of solid wire. These are not original fischertechnik LEDs, but two very bright RGB LEDs with series resistors mounted in counterphase, i.e. their anodes and cathodes are reversed in relation to each other. This works because they are never lit at the same time, and it saves a ground wire for each side.