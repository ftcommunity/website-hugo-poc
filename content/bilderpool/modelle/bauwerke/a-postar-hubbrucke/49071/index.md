---
layout: "image"
title: "Top view"
date: 2021-03-16T19:01:09+01:00
picture: "13_Top_view.jpg"
weight: "13"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

In my first attempts I used aluminum bars to connect the three base plates 500 together, but I had to remove them to make room for the long rails. I ended up using eight sets of blocks 15 and mounting plates 45. The entire assembly is thus pretty solid and won't come apart easily. The model proportions roughly correspond to the actual bridge, and the total width of the model is around 73 cm.
