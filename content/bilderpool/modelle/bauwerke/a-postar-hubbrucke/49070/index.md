---
layout: "image"
title: "Barge with containers"
date: 2021-03-16T19:01:07+01:00
picture: "14_Barge_with_containers.jpg"
weight: "14"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

This is a close-up of the barge. I tried to make the various containers and crates different from one another as much as possible, but the variety of colors generally offered by fischertechnik is very limited. So I used some old-style lamp caps and even some green plug covers so obtain more visual diversity.