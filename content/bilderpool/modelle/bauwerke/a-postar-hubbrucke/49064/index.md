---
layout: "image"
title: "Relay module"
date: 2021-03-16T19:00:58+01:00
picture: "5_Relay_module.jpg"
weight: "5"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

One of main challenges was to find a way to drive five reversible motors and two lamps from the TXT, which has very few outputs. This was circumvented by the custom-made relay assembly shown in the photo. It has three 12-volt DPDT (double pole double throw) relays taken from scrap material and soldered to a PCB which in turn is mounted on a fischertechnik 60 x 120 9-volt battery case lid. Two relays allow the gate motors to use the same outputs as the elevator motors, because they don't all operate at the same time. A third relay allows the barge motor to run. A better idea would be to create an I2C module with an Arduino and some motor drivers, but the relays were simple and effective.