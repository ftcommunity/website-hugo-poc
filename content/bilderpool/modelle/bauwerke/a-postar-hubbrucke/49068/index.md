---
layout: "image"
title: "Scratch program"
date: 2021-03-16T19:01:04+01:00
picture: "16_Scratch_program.png"
weight: "16"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

This image shows the whole Scratch program. I decided against the use of Robo Pro because its user interface is really awkward and it takes a long time to get results. I also think it's probably too complicated for models that use just simple logic sequences like this one. Scratch is straightforward, makes it easy to test new ideas, and motor adjustments are a breeze. The disadvantage is the that the model needs to be tethered to a PC via WLAN the whole time, but for static models this is not a problem.

The smaller blocks below allow for small adjustments to the lift and gate motors.

[Scratch program for download](Liftbridge.sb3)
