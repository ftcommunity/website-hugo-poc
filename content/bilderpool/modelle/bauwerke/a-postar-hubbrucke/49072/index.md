---
layout: "image"
title: "View from above"
date: 2021-03-16T19:01:10+01:00
picture: "12_View_from_above.jpg"
weight: "12"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

I used a mixture of gray, black and red parts for the bridge. Being able to do everything in gray would be ideal because the actual bridge is made of concrete with a gray coating. In any case I reserved the more colorful parts for the barge and its containers so as to differentiate it better from the rest.

The custom-made relay module can be seen behind the pushbutton at the bottom right.