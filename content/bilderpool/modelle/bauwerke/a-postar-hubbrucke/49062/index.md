---
layout: "image"
title: "Barge drive"
date: 2021-03-16T19:00:55+01:00
picture: "7_Barge_drive.jpg"
weight: "7"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

This photograph was taken from the back of the model with the barge removed. The barge drive uses a XS motor with gearbox and chain. The matte black thing that can be seen inserted on the block 30 at the front is a inexpensive glass reed switch that was soldered to a pair of wires, then covered with heat shrink tubing.

Wiring was tricky. There were dozens of cables and many meters of wires to deal with. I spent several days experimenting until I decided to use the thick gray 6-way cable to send signals from the left half of the model to the other half, where the relay module and the TXT controller are located.