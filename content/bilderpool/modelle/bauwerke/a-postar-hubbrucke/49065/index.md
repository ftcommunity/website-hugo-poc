---
layout: "image"
title: "Elevator drive"
date: 2021-03-16T19:00:59+01:00
picture: "4_Elevator_drive.jpg"
weight: "4"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

The left section and the protective cover were removed to show some details. Each pair of elevators is driven by an encoder motor, a worm gear (here hidden behind several red parts) and two gear chains that slowly rotate the winch drums, which in turn pull the ropes. In the final model I needed the encoder inputs for the gates so I did not use them here. Since the two motors run practically at the same speed, a simple timer was enough. If I was to prepare this model for an exhibition I would certainly have to use the encoders to ensure the two motors run exactly in sync with one another.