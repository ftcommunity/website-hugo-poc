---
layout: "image"
title: "Boom gate"
date: 2021-03-16T19:00:52+01:00
picture: "9_Boom_gate.jpg"
weight: "9"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

Here is one of the the boom gates when raised. It is pulled from below via a short rope. Metal screws and bolts are used instead of regular fischertechnik rivets because otherwise the gates would be too lightweight and they wouldn't always return to their resting position when the rope is released. I think they look good, but the mechanics were diifficult to get right. (See next image.)
