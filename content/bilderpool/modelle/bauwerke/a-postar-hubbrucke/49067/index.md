---
layout: "image"
title: "Front view"
date: 2021-03-16T19:01:03+01:00
picture: "2_Front_view.jpg"
weight: "2"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

It operates as follows: Pressing the front button makes the green traffic lights flash, then turn red, then the boom gates are lowered. The bridge rises, the white lights turn on and the yellow barge starts moving slowly until it reaches the front. The bridge is then lowered, the gates rise and the lights turn green again. A further button press at this point raises the bridge just a little bit so that the barge can quickly go back to its home position, and the bridge is lowered again. This completes the reset process. Everything is run on my PC by a Scratch program.