---
layout: "image"
title: "Boom gate drive from below"
date: 2021-03-16T19:01:13+01:00
picture: "10_Boom_gate_drive_from_below.jpg"
weight: "10"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

To make the gate drive assembly compact, I used an encoder motor, gears, a cam disk and a switch to signal the start position as shown in the picture. Usually there are no major problems during operation, but when Scratch is disconnected (or the TXT is turned off and then on again), the software may think is has to raise gates which are already raised. This would make the cams do a whole turn, so the program thinks the gates are down instead of up. Two switches would be a much better solution, but I did not find a compact enough way to do it. It also would require an extra wire plus an extra TXT input for each side, which were not available.

See next picture for other solutions I've tried.