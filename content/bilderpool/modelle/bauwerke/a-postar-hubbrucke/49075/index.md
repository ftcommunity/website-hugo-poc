---
layout: "image"
title: "Ponte do Guaíba"
date: 2021-03-16T19:01:15+01:00
picture: "1_Ponte_do_Guaiba.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Sergio Kaercher"
uploadBy: "Website-Team"
license: "unknown"
---

Here is a photograph of the bridge, located in my hometown. It is a beautiful engineering achievement, and probably still the largest one of its type in this part of the world. I thought it deserved a fischertechnik model. Here is a link to the [relevant topic](https://forum.ftcommunity.de/viewtopic.php?f=6&t=6697) in the ftc forum.
