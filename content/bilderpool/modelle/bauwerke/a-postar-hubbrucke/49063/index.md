---
layout: "image"
title: "Movable span lifted"
date: 2021-03-16T19:00:56+01:00
picture: "6_Movable_span_lifted.jpg"
weight: "6"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

Here is the bridge lifted at its maximum height. The counterweights hang around 1 cm above the hinges used as shaft holders. Two fischertecnhik LEDs are wired in parallel to the XS motor so they light up when the motor drives the barge forward. The limit switches are activated when the movable span, which is kept straight with the help of an aluminum bar, is lowered.