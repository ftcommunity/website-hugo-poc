---
layout: "image"
title: "Tower and counterweight"
date: 2021-03-16T19:01:01+01:00
picture: "3_Tower_and_counterweight.jpg"
weight: "3"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

A close-up of the towers shows that the pulley has two ropes: One that pulls the movable section and another one for the counterweight. Although the weights are fundamental for the real bridge to work, the model would work without them as well. The weights are original fischertechnik ballasts (part no. 35867).