---
layout: "overview"
title: "Lift bridge in Brazil - Hubbrücke"
date: 2021-03-16T19:00:52+01:00
---

Working model of a lift bridge, complete with boom gates, traffic lights and a moving barge. It uses a TXT and a special relay module to control five motors. It is modeled after the "Ponte do Guaíba" a famous landmark in Porto Alegre, Brazil which was designed in Germany in the 1950s.

See video at: https://youtu.be/xd23-nzO0bA
