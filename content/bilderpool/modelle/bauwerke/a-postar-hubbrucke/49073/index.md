---
layout: "image"
title: "Gate drive options"
date: 2021-03-16T19:01:12+01:00
picture: "11_Gate_drive_options.jpg"
weight: "11"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "Website-Team"
license: "unknown"
---

Here are some of the other options I've tried to drive the gates. A servo with gears would probably be the best solution, but unfortunately the original TXT does not offer servo outputs, and I wasn't really in the mood for Arduino programming this time.