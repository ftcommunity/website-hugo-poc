---
layout: "comment"
hidden: true
title: "18799"
date: "2014-03-02T14:14:18"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Ja, Stefan, so ist es. Auf der anderen Brückenseite sind zwei Umlenkspiegel montiert, der erste ein Hohlspiegel aus dem FT ec3, der das Licht ein wenig bündelt und der zweite ein Eigenbau mit Alufolie. Bilder dazu folgen noch.