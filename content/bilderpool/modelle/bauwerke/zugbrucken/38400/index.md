---
layout: "image"
title: "automatische Zugbrücke - Lichtschranke - Fototransistor mit Sammellinse"
date: "2014-03-01T15:55:19"
picture: "2054.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Linse", "Optik", "Konvex", "Fototransistor", "Streulichtkappe"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38400
- /detailsf24b.html
imported:
- "2019"
_4images_image_id: "38400"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38400 -->
Damit das Licht nach zwei Umlenkspiegeln und 1m Wegstrecke von der Infrarot-LED noch ausreicht für das Auslösen der Lichtschranke, wird das Licht vor dem Fototransistor mit einer Konvexlinse gesammelt.