---
layout: "image"
title: "Magnetkupplung und Formschluss an den Brückenteile"
date: "2014-03-02T18:43:51"
picture: "S1040127.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Magnetkupplung", "Formschluss", "Brücketeile"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38412
- /detailsc394.html
imported:
- "2019"
_4images_image_id: "38412"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38412 -->
Hier zwei grüne FT-Magnete als Kupplung. Am anderen Brückenteil sind entsprechend zwei rote FT-Magnete angebracht. 
Ausserdem greifen die Brückenteile mechanisch ineinander -> Formschlussverbindung.

Dadurch sollen die beiden Brückenteile fester miteinander verbunden sein. Die Aufhängung über die Seile senkt sich etwas unter Gewicht, da die Seile ja dehnbar sind.