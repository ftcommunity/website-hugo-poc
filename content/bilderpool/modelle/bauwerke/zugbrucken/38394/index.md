---
layout: "image"
title: "automatische Zugbrücke von der Seite"
date: "2014-03-01T15:55:18"
picture: "2037.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Ampel", "Schiffsverkehr", "Lichtschranken", "Lageerkennung", "Potentiometer"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38394
- /details25ef.html
imported:
- "2019"
_4images_image_id: "38394"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38394 -->
Wenn ein Schiff die Lichtschranke auf Wasserniveau unterbricht, werden die Ampeln geschaltet und, sobald kein Auto mehr auf der Brücke ist (Erkennung über die Lichtschranke auf Straßenniveau) wird die Zugbrücke hochgezogen. 

Sobald die Lichtschranke wieder frei ist, wird die Zugbrücke abgesenkt und die Ampeln umgeschaltet.

Die Lage der Brücke wird über die Stellung eines Potentiometers erkannt, das am linksseitigen Brückenteil auf der Drehachse montiert ist.

Beide Brückenteile werden über Seilzüge durch einen Motor bewegt.