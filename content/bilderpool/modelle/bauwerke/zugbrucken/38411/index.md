---
layout: "image"
title: "Zugbrücke hochgezogen"
date: "2014-03-02T18:43:51"
picture: "S1040123.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38411
- /details408b-2.html
imported:
- "2019"
_4images_image_id: "38411"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38411 -->
