---
layout: "image"
title: "Brücke in Autofahrtrichtung"
date: "2014-03-02T18:43:51"
picture: "S1040114.jpg"
weight: "13"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38413
- /details5c58.html
imported:
- "2019"
_4images_image_id: "38413"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38413 -->
Hier nochmal ein schärferes Bild bei mehr Licht