---
layout: "image"
title: "Kabelchaos 5"
date: "2018-09-23T13:24:04"
picture: "gehuse06.jpg"
weight: "94"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47916
- /details3c9d.html
imported:
- "2019"
_4images_image_id: "47916"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47916 -->
Und simsalabim!

Hier ist das ganze Kabelchaos hin gekommen.
In das obere Gerätehaus des Turm 2 (Bergstation).

Der Serielle Stecker (grüner Pfeil) kommt nun von unten an das Gerätehaus und ist daher in der Ansicht kaum zu sehen. Das Kabel macht einen leichten Bogen und verschwindet unter der Fahrbahn links am BIldrand Richtung Turm 1. (Zu sehen hier auch das rot-weiße Zugseil, das über der Fahrbahn liegt und in den Turm geht, dort um eine Rolle geht und unter der Fahrbahn wieder zurück zur Gondel.)

Auch die Traverse mit der Gabel (blauer Pfeil) ist gut zu sehen und benötigt nur wenig Platz, so dass alle Kabel sich hinter den Fachbausteinen verbergen.

Dazu gekommen ist ein Arbeitslicht (magenta-Pfeil) das auch in der Nacht eine Wartung ermöglicht. Es wird gesteuert von der Klappe / Schalter am Maschienenraum. (Dazu später mehr.)

Auch sichtbar am oberen Bildrand ist, dass ich die Gelenksteine nun schon mit den Winkelsteinen versteift habe.

Die Fahrbahn mit den Schienen endet übrigens auf beiden Seiten mit einem Alu-Profil, in das Winkelstangen greifen, die in den Baustein 15 (unterhalb des Minitasters) laufen. Dies ermöglicht, dass die Fahrbahn nach oben und unten geneigt werden kann, wen Belastung durch die Gondel kommt und wenn die Anlage auf unterschiedlichen Höhen der Bergstation angepasst wird. Außerdem verlängert und verkürzt sich die Fahrbahn wenn sich die Last bewegt. Dies gleicht dieses Loselager der Brücke aus, indem die Winkelstangen in den Aluprofilen geschoben werden.