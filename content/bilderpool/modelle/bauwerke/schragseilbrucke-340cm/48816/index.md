---
layout: "image"
title: "Fernbedienung – Kabelpeitsche"
date: 2020-08-30T16:23:09+02:00
picture: "Verkabelung14.jpg"
weight: "128"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Unter der Fernbedienung (hier noch ohne Unterbau) kommt das Telefonkabel (vom Centronics-Stecker) an. Es ist direkt als Kabelpeitsche ausgeführt, so dass die Schalter und Lampen direkt angeschlossen werden können.