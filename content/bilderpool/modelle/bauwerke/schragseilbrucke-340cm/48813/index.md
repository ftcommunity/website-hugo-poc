---
layout: "image"
title: "Weihnachten unter der Brücke "
date: 2020-08-30T16:23:06+02:00
picture: "Weihnachten.jpg"
weight: "131"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Was gibt es schöneres, als Weihnachten 2017 unter der Brücke zu feiern. (Hüstel.)
Meine Frau lies uns an Heiligabend spielen denn 540cm quer durch das Wohnzimmer zu haben strapaziert die Geduld der besten Hausfrau erheblich. Danke dafür.
Aber Vater und  Sohn sind stolz auf das Werk.