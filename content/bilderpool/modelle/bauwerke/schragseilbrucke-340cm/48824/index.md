---
layout: "image"
title: "Wartungsarbeiten 2"
date: 2020-08-30T16:23:20+02:00
picture: "Montagearbeit.jpg"
weight: "120"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Herr Fischer ist noch mit Wartungsarbeiten am Oberbau der Bergstation beschäftigt.
Gut zu sehen ist nun auch die Krafteinleitung der Schrägseile in den Turm: über die Rolle wird das Seil an den Block geführt. Um die Rolle eingehängt ist das nächst-tiefere Seil. Zur Versteifung ist der Querträger (zwei graue BS30) mit einer Stange versteift. Die Beleuchtung (Selbstbau-LED) strahlt die Seile in zwei verschiedenen Winkeln an. Die Störlichtkappen sind mit Tesa für den Transport und Ausstellungen gesichert.