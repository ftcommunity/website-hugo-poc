---
layout: "image"
title: "Schacht zur Seilspannung"
date: "2018-09-23T13:24:04"
picture: "Schacht01.jpg"
weight: "62"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47884
- /details0f8f-3.html
imported:
- "2019"
_4images_image_id: "47884"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47884 -->
Jetzt wird es kompliziert.
Das Gewicht ( http://www.ftcommunity.de/details.php?image_id=47878 ) muss nun in der Führung in den Schacht eingebaut werden, der sich unter den Turm 1 (Talstation) hängt.

Das Gewicht (blauer Pfeil) ist später am Seil eingehängt um dieses zu streffen. (Das Seil erkennt man über und unter der Fahrbahn am oberen rechten Bildrand.)
Verkürzt sich das Seil durch ungleiche Spulendurchmesser, so wird das Gewicht nach oben gezogen.
Die Strecke wird nicht ausreichen um die Laufunterschiede auszugleichen. Deswegen benötige ich eine Regelung, die über Reedkontakte und diverse Relais gesteuert wird. (Siehe hierzu Bilder später.) Ich werde dazu auch noch einen ausführlichen Artikel in der ft:pedia schreiben.

Der Trick um die Steeuerung nicht ständig anlaufen zu lassen, wenn das Seil in Schwingung gerät ist, dass es eine Schalthysterese gibt. Diese wird mechanisch realisiert:
die Kette (gelber Pfeil) wird mit den Mitnehmerzapfen nur dann mitgenommen, wenn sich das Gesicht bereits ausreichend weit bewegt hat. Erst an dieser Kette ist der Magnet, der die Reedkontakte auslöst. (Siehe weitere Fotos)

Natürlich kann auch etwas schief gehen. Daher sind oben und unten im Schacht Endkontakte angebracht, die sowohl den Seilantriebstoppen, als auch die Regelung unterbrechen und zudem einen Alarm geben. (grüner Pfeil).