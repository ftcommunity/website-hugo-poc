---
layout: "image"
title: "Fahrbahnbeleuchtung (am Turm)"
date: "2016-12-28T12:29:46"
picture: "IMG_20161227_110913.jpg"
weight: "35"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["beleuchtung", "schrägseilbrücke", "led"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44943
- /details93ac.html
imported:
- "2019"
_4images_image_id: "44943"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44943 -->
Hier die zwei LED Leuchten mit Lochblende 6mm die in zwei verscheidenen Winkeln am Turm (oberer Mast) angebracht sind, um die Fahrbahn in der Nähe des Turms besonders zu beleuchten.

Auch zu sehen ist eines der Lager, die zum Spannen eines der Tragseile benötigt werden.

Hinweis: noch ist das Seilende (schlaufe um den Lagerblock) und provisorisch eingehängt. Später wird es zusätzlich um den grauen Stein geschlungen sein (mit einem doppelten Ankerstich, wie der Feuerwehrmann sagen würde) um die Zuglast (ist eigentlich nicht groß) auf den grauen Stein und die Achse (zur Versteifung) zu verteilen und den Lagerblock (besonders den Zapfen) zu entlasten.