---
layout: "image"
title: "Turm 1 Technik"
date: "2018-09-23T13:24:04"
picture: "Turm-1-technik.jpg"
weight: "97"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47919
- /details5547.html
imported:
- "2019"
_4images_image_id: "47919"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47919 -->
HIer der Turm 1 noch einmal in Nahaufnahme von unten unter der Fahrbahn von hinten (Rückseite).
Zu sehen sind die diagonalen Beine des Turms (Alu-Profile) und das obere Gerätehaus.

Schön zu erkennen die Aufhängung der Fahrbahn mit Hilfe der Winkelstangen.

Darunter hängt die Gondel und betätigt mit ihrem "Dach" die Rollen der Gabel, die  mit dem Stoßdämpfer nach unten gedrückt werden und den Endschalter betätigen (magenta Pfeil). Ja, die Diode sorgt für den Endanschlag und die Abfahrt wenn die Spannung ungepolt wird.

Unter der Fahrbahn hängt das Serielle Kabel, das um den Turm geführt ist und mit schwarzen Gummiringen befestigt wird. (Es ist unten im Maschienenraum eingesteckt und versorgt Turm 2) Diese Gummiringe müssen zum Abbau jedesmal entfernt werden, da das Kabel fest an der Fahrbahn ist.

Im Schacht, der unter dem Turm hängt bewegt sich das Gewicht (grüner Pfeil) und hält die Seilspannung konstant. Wenn sich das Seil verlängert oder kürzt zieht es die Kette links daneben mit (die Mitnehmer sind auf der Rückseite).
Die Kette trägt einen kleinen Neodym-Magnet (orange Pfeil) der sich gerade vor Reedkontakten befindet. Auch am oberen Ende sind die Reedkontakten in den Baustein 15 mit Loch (weißer Pfeil) und enden direkt hinter der Kette. Wenn also das Seil zu kurz wird, dann schalten diese und das Gleichlaufgetriebe gibt Seil nach.

Der hellblaue Pfeil zeigt auf drei kaskadierte Minitaster. Einer davon schaltet den Antrib aus, einer den Motor für die Seilspannung auf Stopp und einer die rote Signallampe auf der Bedienung an. ("Anlage Störung") Sie begrenzen das Gewicht nach oben. Unten im Schacht (hinter der Verkleidung - siehe Bild vorher) sind ebenfalls 3 Schalter.
Sie dienen der Sicherheit und sollten im Normalbetrieb nicht betätigt werden.
(das Kabelchaos ist gar nicht so schlimm. Sie sind sauber zu einem Kabelbaum zusammengefasst, müssen aber auch in die Stecker rein.