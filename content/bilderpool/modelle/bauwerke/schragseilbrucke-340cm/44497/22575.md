---
layout: "comment"
hidden: true
title: "22575"
date: "2016-10-03T13:05:12"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Wenn die Bahn obenauf liegen würde (mit der Achse nur zur Fixierung), bräuchten die vier BS30 keine Scherkräfte auf die Zapfen zu tragen.

Gruß,
Harald