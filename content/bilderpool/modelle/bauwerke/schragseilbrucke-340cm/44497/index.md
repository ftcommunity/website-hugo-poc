---
layout: "image"
title: "Detail: neuer Turm Fahrbahnlager"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_183742a.jpg"
weight: "10"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Alu", "Hängebrücke", "Laufschiene", "Statik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44497
- /details5ede-2.html
imported:
- "2019"
_4images_image_id: "44497"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44497 -->
Nun am neuen Turm: die Fahrschiene ist jetzt mit einer Stange beidseitig in graue 30er mit Loch eingehängt, wobei diese nach unten versteift sind und daher noch mehr Gewicht tragen können.
Gut zu erkennen ist auch, dass der vordere Rahmen der Türme komplett aus Alu-Profilen gefertigt ist um Lasten besser tragen zu können.
Später kann an dieser "Splint-Stange" die Fahrbahn ausgehängt werden um sie zum Transport ins Auto zu packen und so die ganze Anlage auseinander zu nehmen.