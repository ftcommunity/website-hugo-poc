---
layout: "image"
title: "Wartungsbetrieb"
date: 2020-08-30T16:23:07+02:00
picture: "Wartung01.jpg"
weight: "130"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Zur Fehlersuche können die Motoren auf „Wartung“ geschaltet werden. Dann bewegen sie sich nicht, wenn entsprechende Ansteuerungen kommen, sondern die blauen Lampen leuchten. Diese wurden später durch mehrfarb-LEDs ersetzt. So konnte man auch die Drehrichtung erkennen. Zusätzlicher Nutzen dieser Schalter: bei einer Ausstellung können diese auf „Wartung“ gestellt werden und keiner kann an der Anlage spielen – nur noch die Lichter lassen sich schalten.
Diese Wartungsschalter sind versteckt unter der Maschinenhausabdeckung.
Klar, dass der Sicherheitshinweis dazu muss.