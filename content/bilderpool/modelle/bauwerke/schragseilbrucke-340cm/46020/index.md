---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 1)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-1.jpg"
weight: "44"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Gleichlaufgetriebe", "Seilbahn", "Antrieb", "Motor"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46020
- /detailse879.html
imported:
- "2019"
_4images_image_id: "46020"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46020 -->
Dies ist der Testbetrieb.
Das Getriebe wurde angeschlossen und kann mit den Schaltern und Tastern gesteuert werden.

An den Spulen hängt ein Seil, das von der einen Spule auf die andere Spule umgespult wird.
Dadurch bewegt sich das graue Gewicht (gefüllt mit Kieselsteinen, links) nach oben und unten (je nach Laufrichtung). Dies symbolisiert die Last der späteren Fahrgondel der Hängebahn welche auf der Laufbahn der Schrägseilbrücke fahren wird.

Das gelbe Gewicht dient zum Straffen des Seils und als Wendepunkt. (Entspricht später der Bergstation).

Aufgrund der Tatsache, dass eine Spule immer dünner wird (Seil wird abgespult) und die andere dicker, ändert sich die Seillänge - dadurch wird das gelbe Gewicht nach oben oder unten bewegt.

=> hier kommt das Gleichlaufgetriebe ins Spiel:
der Verstellmotor (entspricht der Lenkung), kann jetzt die Seillänge korrigieren, wenn es gewisse Grenzen über- oder unterschreitet.

[Thema Steuerung: kommt später, wird klassisch nur mit Schaltern und Kontakten ausgeführt, pure Elektromechanik - nix Computa oder so :-) ]