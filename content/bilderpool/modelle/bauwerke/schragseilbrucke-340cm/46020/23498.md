---
layout: "comment"
hidden: true
title: "23498"
date: "2017-07-04T17:22:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr ordentlich, sogar mit einem Test! Die Steuerung riecht jetzt schon danach, wie wenn die unbedingt als Beitrag in der ft:pedia erscheinen sollte, meinst Du nicht?

Gruß,
Stefan