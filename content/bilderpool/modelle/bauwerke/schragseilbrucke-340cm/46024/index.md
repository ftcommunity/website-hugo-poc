---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 7)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-15.jpg"
weight: "48"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Minitaster", "Alektrik", "Elektromechanik", "Tagscheibe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46024
- /detailsaa91-2.html
imported:
- "2019"
_4images_image_id: "46024"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46024 -->
Richtungsgeber

Eine Achse der Spulen wurde durch eine längere ersetzt.

Daran sind nun zwei Taktgeber-Scheiben befestigt, die jeweils ca 90° lang einen Impuls geben können, wobei die zwei Räder um 90° versetzt sind.
Dadurch entstehen an den 4 Minitastern 4 aufeinanderfolgende Impulse, die je nach Laufrichtung in entgegengesetzter Reihenfolge erscheinen. Damit lässt sich eine Anzeige erzeugen, ob sich die Spulen bewegen, wie schnell und in welche Richtung. (Siehe nächste Bilder.)

Durch die genaue Höhe der Minitaster lässt sich der Takt exakt einstellen.