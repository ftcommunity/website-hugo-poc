---
layout: "image"
title: "Fahrbahnbeleuchtung + Kollisionswarnleuchte"
date: "2016-12-28T12:29:45"
picture: "IMG_20161227_110500.jpg"
weight: "28"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["beleuchtung", "LED", "lampe", "elektrik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44936
- /details2b4d-3.html
imported:
- "2019"
_4images_image_id: "44936"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44936 -->
Über der Fahrbahn spannt sich von Turmspitze zur anderen ein Seil, an dem 5 LED Leuchten zur Fahrbahnbeleuchtung angebracht sind. 
Zusätzlich sind an 2 dieser Lampen nach oben gerichtete rote LED Warnleuchten angebracht, die wie bei realen Hochbauten eine gewisse Bauhöhe signalisieren.

Gut erkennbar ist das Tragseil. Ein Baustein 7,5 mit seitlichen Nuten wurde mit einem Verbinder 15 benutzt, um an das Seil die Lampe zu befestigen.
Oben drauf eine LED mit roter Kappe (neue Bauweise), darunter die Fahrbahnbeleuchtung mit Lochblende 6mm.

Die Zuleitungen wurden mit dem Tragsei verflochten, was zusätzliche Befestigungen (z.B. mit Kabelbindern) überflüssig macht und ein Gewisses Design ergibt.
Die Fahrbahnbeleuchtungen sind selbstbau LEDs (siehe Foto weiter hinten).