---
layout: "image"
title: "Seilbefestigung an der Fahrbahn"
date: "2016-10-02T17:43:47"
picture: "IMG_20160921_152023a.jpg"
weight: "12"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Knoten", "Seil", "Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44514
- /details95d2.html
imported:
- "2019"
_4images_image_id: "44514"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44514 -->
HIer nach der Montage der zweiten Schiene. So ist das Seil nicht mehr im Weg. (Der Knoten wird nun auf die Unterseite in den Träger geschoben, damit nur noch das saubere Seil heraus kommt.