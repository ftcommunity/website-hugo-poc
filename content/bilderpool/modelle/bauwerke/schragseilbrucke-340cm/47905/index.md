---
layout: "image"
title: "Out-Takes"
date: "2018-09-23T13:24:04"
picture: "ausbruch.jpg"
weight: "83"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Gelenkbausteine"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47905
- /detailsd7fe.html
imported:
- "2019"
_4images_image_id: "47905"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47905 -->
AUch das passiert: die Türme können für den Transport umgeklappt werden. Daher sind die oberen Teile mit den alten Gelenksteinen gelagert. Da ich (siehe Foto http://www.ftcommunity.de/details.php?image_id=44664 ) vor allem die Höhe optimiert habe waren diese nicht stabil genug verbaut.

Beim Abbau ist der Turm umgefallen und die Gelenkbausteine sind ausgerissen. Ein kapitaler Schaden war die Folge. Leider (Dummheit siegt) ist mir das noch einmal passiert.

Meine Lösung? Siehe nächstes Bild