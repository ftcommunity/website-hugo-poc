---
layout: "image"
title: "Süd-Convention 2018"
date: 2020-08-30T16:23:19+02:00
picture: "Schraegseilbruecke-ftc-2018b.jpg"
weight: "121"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Ausstellung der Schrägseilbrücke auf der Süd-Convention 2018 in Frankfurt. Das Bauwerk ist so groß, ich bekomme es kaum auf das (unscharfe) Foto.
Auf dem Boden rechts liegt ein Monitor. Über diesen zeige ich Details mit Beschreibungen für interessierte Besucher.
Die Brücke steht nun auf Holzplatten (unter dem Stoff) montiert. Darunter Holzkästen (Transport der Kleinteile) und zum erhöhen der Anlage darunter Cola-Kisten.