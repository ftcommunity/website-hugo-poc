---
layout: "image"
title: "Kabelsalat 1"
date: "2018-09-23T13:24:04"
picture: "kabelsalat01.jpg"
weight: "67"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47889
- /details6828.html
imported:
- "2019"
_4images_image_id: "47889"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47889 -->
Die blauen Anschlusskabel der Reedkontakte (immerhin 8 Stück) habe ich aufgerollt und mit Kabelbindern am oberen Ende des Turms befestigt. Ich habe mich entschlossen, diese dort sichtbar hängen zu lassen, um bei Ausstellungen die Reedkontakte als Thema zu haben.

Auch zu erkennen hier im Bild:
- am oberen Bildrand zwei Rollen. Das eine Zugseil kommt von rechts, über die rechte Rolle, hinab zum Gewicht, wieder hoch, über die linke Rolle, dann zur Spule. (Genauer Seilverlauf wird noch fotografiert)
- rechts und links der Rollen ist die Kette, die im Schacht nach unten führt, dort über zwei Z15 und wieder hoch. Oberhalb des Bildrandes sind die anderen zwei Z15. An der Kette ist dann der Neodymmagnet. Und die Kette wird von den Mitnehmern des Gewichtes betätigt.
- in der Bildmite von oben nach unten die eine Führungsstange, an der das Gewicht auf und ab fahren kann ohne zu reiben.
- am unteren Bildrand das Gewicht, gefüllt mit Kiselsteinen (hinter der Stange)


(Zur Orientierung: wir befinden uns am Gerätehaus des unteren Turmes 1 "Talstation")