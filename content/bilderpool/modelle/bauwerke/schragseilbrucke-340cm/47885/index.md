---
layout: "image"
title: "Reedkontakte für Seilspannung 1"
date: "2018-09-23T13:24:04"
picture: "reed01.jpg"
weight: "63"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilspannung", "Reedkontakt", "Neodymmagnet"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47885
- /detailsa856.html
imported:
- "2019"
_4images_image_id: "47885"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47885 -->
Der Schacht von hinten gesehen. (vgl. voriges Foto)

Zuerkennen ist das Gesicht, das dür die Seilspannung sorgt (grauer Kasten im HIntergrund unter dem grünen Pfeil). Die Führung des Gewichtes mit den Stangen oben und unten ist ebenfalls zu erkennen. Ebenso am oberen Ende noch das weiße Seil, das um die Rolle geht.

Wichtig ist hier aber zum einen die Kette, die von dem Gewicht mitgenommen wird. (Siehe dazu http://www.ftcommunity.de/details.php?image_id=47878 hier sieht man an der rechten Version die Zwei Radachsen, durch die die Kette führt. - nicht sichtbar auf dem Foto, da auf der anderen Seite des Schachts.)
An dieser Kette ist ein kleiner Neodym Magnet (grüner Pfeil) den ich mit Heißkleber befestigt habe. Das ist zwar gegen meine ft-Ethik, aber viele andere Lösungen sind nach Tests ausgeschieden.

Dieser Neodymmagnet betätigt die Reed relais.
Und zwar in folgender Weise:
- ist die Seilspannung zu hoch, wandert das Gewicht nach oben. Die Kette wird nach einigen Zentimetern mitgenommen (Rückseite) und bewegt sich hier auf dieser Seite deswegen nach unten.
- sobald der Magnet nahe genug am ersten der zwei Reedkontakte ist, löst er ein Relais aus, das den Motor für die Seilspannung veranlasst Seil nachzugeben.
- der zweite Reedkontakt (ganz unten) ist als Redundanz gedacht, falls der erste Kontakt nicht auslöst oder das Seil sich noch weider spannt und Gefahr besteht, das der Magnet wieder aus dem Kontaktfeld des Reed heraus kommt. (Die Reedkontakte sind daher parallel geschaltet.)

Gleiches geschieht entsprechend andersrum bei zu geringer Seilspannung mit den oberen Reedkontakten.

Ich habe hier die gleichen Reedkontakte verwendet, die es von Fischertechnik auch gibt.
Das war soweit ok, aber ziemliche FUmmelei und fehleranfällig. Und wie ich nach einem halben Jahr merkte auch brüchig. (Siehe nächstes Foto)