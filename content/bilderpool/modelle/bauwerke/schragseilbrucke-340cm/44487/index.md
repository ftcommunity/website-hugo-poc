---
layout: "image"
title: "Probeaufbau des Funktionsprototyps Hängebahnantrieb"
date: "2016-10-01T22:12:29"
picture: "GOPR9188a.jpg"
weight: "2"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Getriebe", "Differenzial", "Prototyp"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44487
- /detailsf71b.html
imported:
- "2019"
_4images_image_id: "44487"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44487 -->
Erklärung und Video mit Funktionstest hier: https://youtu.be/-EDMZNxTwsE