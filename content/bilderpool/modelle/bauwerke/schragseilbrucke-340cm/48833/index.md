---
layout: "image"
title: "Turmarbeiten"
date: 2020-08-30T16:23:31+02:00
picture: "Deko02.jpg"
weight: "111"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Auch am Turm sind Arbeiten nötig. Hr. Fischer ist gerade dabei die Bauwerksbeleuchtung zu warten. Sicherheitshelm gehört dazu – ein Knochenjob. (Versteckt am Oberturm der Bergstation)