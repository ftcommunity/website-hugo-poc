---
layout: "image"
title: "Entwurf"
date: 2020-08-30T16:23:47+02:00
picture: "Entwurf.png"
weight: "100"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Der erste Entwurf der Schrägseilbrücke von meinem Sohn Jan (7) zeigt schon viele Details, die wir später tatsächlich so umgesetzt haben. Sein Vorbild war die Rheinbrücke bei Speyer.