---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 3)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-4a.jpg"
weight: "51"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Antrieb", "Gleichlaufgetriebe", "Differenzialgetriebe", "Motor"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46027
- /details7578.html
imported:
- "2019"
_4images_image_id: "46027"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46027 -->
Fertiges Getriebe mit Seilspulen,

Wie auf dem vorherigen Bild zu sehen sind die Abtriebsritzel 10 (unten) leicht versetzt angebracht. Dadurch ist es möglich auf diesem kleinen Raum den Abtrieb mit Z30 zu realisieren. Auf deren Achsen, die einseitig gelagert sind, befinden sich die Spulen für das Antriebsseil, welches später die Hängebahn auf unserer Schrägseilbrücke bewegen.

Der Antriebsmotor ist nun nach rechts oben gewandert (noch nicht final).
Der Motor für die Seilspannung (entspricht dem Lenkmotor bei Kettenfahrzeugen) ist rechts unten und dtreibt eine Schnecke an.
[Der 3. Motor im Bild links ist außer Betrieb und wird später entfernt.]