---
layout: "image"
title: "Fernbedienung Innenleben"
date: "2018-09-23T13:24:04"
picture: "Bedienung-innenleben.jpg"
weight: "85"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Fernbedienung", "Bedienpult"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47907
- /details6a49.html
imported:
- "2019"
_4images_image_id: "47907"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47907 -->
Wie wird die Anlage gesteuert?
Der Gag ist ja, dass KEIN Controller oder PC an der Anlage ist, denn als "Classic-Man" soll die Technik der 70er und 80er Jahre gezeigt werden.

Im Normalbetrieb läuft die Anlage alleine und verwendet dazu Relais (kommt später in der Erklärung). [Anm.: in der Technik nennt man das "Schütz-Steuerung"]
Dennoch benötigt man Möglichkeiten zum Eingriff. Und deswegen dieses Bedienpult.

Es ist mit einem 3m langen Centronics-Kabel verbunden und in zwei Teile aufgebaut.

(links unter der Klappe) Der Not-Handbetrieb-Teil. Hier können Seilspannung (mit dem Wippschalter) manuell nachgelassen oder angehoben werden. Die Fahrt wird manuell mit dem Taster (querliegend) gegeben.
Die zwei Taster am unteren Bildrand werden mit den Riegeln an der Klappe betätigt, wenn diese geschlossen ist. So erkannt die Anlage Hand- oder AUtomatikbetrieb.
Der Magnet sorgt für sicheren Verschluss der Klappe.

(rechte zwei Drittel) Im Normalbetrieb spielt lediglich der Kippschalter eine Rolle. Er wählt in welche Richtung die Gondel gefahren wird. Sie fährt bis zum Endanschlag am Turm. (Hierzu siehe meine Erklärungen zur Traverse). Die Fahrtrichtung wird durch die Reihe weißer Lampen (später LED) angezeigt und vom Fahrtrichtungsgeber (siehe Fotos weiter vorne) gesteuert. Die blauen Lampen zeigen die Endstation am Turm an.
Wenn die Seilspannung von den Schützen automatisch geregelt wird, so leuchten die gelben Lampen zur Kontrolle auf.
Am rechten Eck ist eine grüne Lampe die den Betrieb (Spannung vorhanden) anzeigt. Darüber eine gelbe, welche Blinkt, wenn die Anlage auf "Wartung" geschaltet ist (siehe später).
Etwa ind er Mitte befinden sich zwei Schiebschalter, die zum einen die Fahrbahnbeleuchtung und zum anderen die Kollisionslampen für den Nachtbetrieb schalten. Dazu gehören die Kontrollleuchten in grün oben drüber). [Anm.: natürlich sollten die Kollisionslampen immer leuchten. Das habe ich später auch so umgebaut - nun betätigt der Schalter ein Arbeitslicht.]

Der Kabelsalat befindet sich unterhalb der Grundplatten und die ganze Bedienung bekommt noch eine Abdeckung mit Beschriftung.