---
layout: "image"
title: "Zusammengeklappt"
date: 2020-08-30T16:23:04+02:00
picture: "Zusammengeklappt.jpg"
weight: "132"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Der Turm der Talstation ist fertig zum Transport.
Gut zu sehen: die Seile und die Abspannung des Turms werden einfach nach hinten gelegt. Die Spitze des Turms ist mit einem Alu-Profil (nicht ft) gestützt. Da alle Platten mit Spax festgeschraubt sind (siehe an der Bauplatte die Metallplatten.) lässt sich das Modell an der Holzplatte gut tragen, ohne dass etwas verrutscht.