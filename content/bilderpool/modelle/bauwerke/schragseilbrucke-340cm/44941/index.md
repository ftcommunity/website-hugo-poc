---
layout: "image"
title: "Beleuchtungstest (Nacht) Subjektive"
date: "2016-12-28T12:29:46"
picture: "IMG_20161226_232333.jpg"
weight: "33"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["elektrik", "LED", "lampe", "schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44941
- /detailsd7a9-2.html
imported:
- "2019"
_4images_image_id: "44941"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44941 -->
Zum ersten Mal teste ich die Beleuchtung der Brückenkonstruktion. Das ganze Wohnzimmer (Weihnachten) ist nur von der Brücke erleuchtet.

Gut zu erkennen ist die 3,40m lange Fahrbahn der Hängebrücke. Hier ist sie wunderbar beleuchtet - das war ja Sinn der Sache.
Im Hintergrund die Bergstation (Turm 2) auf dem weißen Unterbau (dieser gleicht den Höhenunterschied von Terrasse und Rasen über den Teich aus - dort wird später die Brücke aufgebaut.
Dazwischen spannt sich über der Fahrbahn das Tragseil der Beleuchtung.

Es leuchten:
- 5 weiße LED zur Fahrbahnbeleuchtung am Tragseil
- 2 blinkende rote LED an der Turmspitze zur Kollisionsbeleuchtung
- 2 rote LED am Tragseil über der Fahrbahnbeleuchtung
- je 1 rote LED an der hinteren Strebe zur Kollisionsbeleuchtung
- je 2 weiße LED in unterschiedlichen Winkeln an jedem Turm (ca. halbe Höhe) zur Fahrbahnbeleuchtung
- je 1 weiße LED am Lagerblock, die den Turm von schräg unten beleuchtet (schönes Schattenspiel an der Decke leider nicht zu sehen)

Die Subjektive zeigt schön, wie die Fahrbahn einen Bogen macht und die Schrägseile in der Fahrbahn verankert sind.

Das Bild wurde nur mit Handy gemacht - wenn alles fertig ist werde ich im Garten (Sommer) Bilder mit HDR zur blauen Stunde machen, wenn die Brücke mit Fahrzeug über dem Gartenteich steht....