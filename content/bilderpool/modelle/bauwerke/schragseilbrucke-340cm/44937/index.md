---
layout: "image"
title: "Blinkleuchte an der Mastspitze"
date: "2016-12-28T12:29:46"
picture: "IMG_20161227_110705.jpg"
weight: "29"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["led", "lampe", "elektrik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44937
- /details50ba-3.html
imported:
- "2019"
_4images_image_id: "44937"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44937 -->
Auf der Spitze der Türme befindet sich eine LED mit roter Kappe (neue Bauweise) die mit dem Blinkmodul als Kollisionswarnleuchte dient.

Hier sieht man gleichzeitig (unterer Turm), wie das Tragseil für die Fahrbahnbeleuchtung (siehe Fotos davor) eingehängt ist (führt weiter rechts aus dem Bild heraus). Die verflochtenen Kabel werden mit Steckern und Buchsen angeschlossen, um später für den Transport getrennt zu werden.
Dann führen alle Kabel an der hinteren Strebe herunter und versorgen auf dem Weg noch die Kollisionsleuchte in der Mitte der Strebe (nächstes Bild).