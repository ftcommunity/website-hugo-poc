---
layout: "image"
title: "Reedkontakte für Seilspannung 3"
date: "2018-09-23T13:24:04"
picture: "reed03.jpg"
weight: "65"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Reedkontakt"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47887
- /details08ef.html
imported:
- "2019"
_4images_image_id: "47887"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47887 -->
Wer mich kennt weiß: ich liebe Funktionsprototypen.
Daher habe ich die Reedkontakte ersetzt durch neue Kunststoffgegossene.
(Hier in der Version mit Durchmesser 4mm - später, beim Fahrtrichtungsgeber nutze ich welche mit 5mm)

Im Vergleich hier die alten Glaskörperkontakte, die mit Kabelbindern befestigt waren und nach und nach brachen. (links)
Daneben die neue Ausführung, die ebenfalls direkt an der Strebe des Schachtes eingesetzt werden mussten. (rechts)

Die Kontakte wurden im Prototyp eingesetzt und mit dem Neodym getestet. Es erwies sich, dass sie sehr zuverlässig und genau schalteten, so dass ich den Abstand der oeberen und unteren Reedkontakte erweitern konnte und damit die Schalthysterese vergrößern. Nun brauche ich noch weniger Motoraktivität um das Seil nachzuführen.

Die langen Anschlußkabel wollte ich nicht kürzen (getreu dem Motto: "fünf mal abgeschnitten und immer noch zu kurz") da ich nicht weiß, welche zukünftigen Projekte die längeren Kabel bräuchten.

Nach dem Test des Prototypen ging es um den Einbau.