---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 5)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-3.jpg"
weight: "43"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Gleichlaufgetriebe", "Antrieb", "Motor", "Getriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46019
- /details971c.html
imported:
- "2019"
_4images_image_id: "46019"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46019 -->
Hier die Gegenseite.

Der schräge Motor dient als Hauptantrieb auf Z20 und treibt die Achsen der Differenziale an.
Im HIntergrund die Spulen für das Seil.

Schalter und Taster sind Provisorien um den Testbetrieb durchzuführen.

Der Motor rechts ist außer Betrieb und wird später entfernt.