---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 9)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-8.jpg"
weight: "50"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Lauflicht", "Taktgeber", "Minitaster", "Elektromechanik", "Elektrik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46026
- /details1cfc.html
imported:
- "2019"
_4images_image_id: "46026"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46026 -->
Testlauf des Lauflichtes / Richtungsanzeige

Der fliegende Aufbau der Verkabellung zeigt die Funktion der Taktgeber:
Immer eine Lampe ist später aktiv und zeigt, je nach Drehrichtung der Spulen einen Eindruck eines Lauflichtes entweder in die eine oder die andere Richtung.

Am Bedienpult wird später die Fahrt von dr einen Station zur anderen angezeigt, die unsere Hängebahn auf der Schrägseilbrücke macht.

Der Bedienpult ist übrigens mit einem 3m langen Kabel versehen, so dass der Bediener sich bewegen kann.