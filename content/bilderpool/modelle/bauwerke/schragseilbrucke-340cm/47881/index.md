---
layout: "image"
title: "Neues Gleichlaufgetriebe 1"
date: "2018-09-23T13:24:03"
picture: "gleichlaufgetriebe01.jpg"
weight: "59"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Powermotor", "Gleichlaufgetriebe", "Differenzial"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47881
- /details8e5d.html
imported:
- "2019"
_4images_image_id: "47881"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47881 -->
Die alten Motoren waren zu schwach für das neue Gewicht. Und so musste ich trotz meines Rufes als "Classic-Man" über meinen Schatten springen und mir Powermotoren anschaffen.
Ich hatte keine Ahnung von diesen und musste mich erst bei Fischertechnikfriendsman beraten lassen. Er stellte mit mir alle drei Motoren mit allen Anschlussmöglichkeiten zusammen.

Wer mich kennt, der weiß, dass ich Fan von Funktionsprototypen bin. Und so begann die Konstruktion des Gleichlaufgetriebes aufs Neue.
Die Rahmendebingung war, dass es in den Platz des alten Getriebes passen musste und außerdem die Seilrollen entsprechend aufnehmen musste.

Dies ist die finale Version des Getriebes.
Umgesetzt mit Powermotoren besiert sie auf dem alten Getriebe (Siehe http://www.ftcommunity.de/details.php?image_id=46016 )
Die neue Konstruktion hat zudem den Vorteil weiter versteift zu sein. Als Modul passt es genau an den Platz des alten Getriebes,