---
layout: "image"
title: "Unteres Ende des Schachtes"
date: 2020-08-30T16:22:59+02:00
picture: "Kubus-innen.jpg"
weight: "136"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

In diesem Schacht läuft das Spanngewicht (grüner Pfeil). Es ist so in die Antriebsseile eingehängt, dass diese ausreichend straff sind. Da sich diese aber (Zwei Spulen deren Durchmesser sich ändert) in der Länge verändern, während die Gondel fährt, zieht das Spanngewicht etwas Seil im Turm nach unten oder oben und hält dabei die Spannung konstant. Nun ist es so, dass der Weg dieses Spanngewicht im Turm durch die Bauhöhe begrenzt ist. Also muss das Seil durch Motoren verlängert oder gekürzt werden, wenn das Gewicht zu weit fährt.
Anhand von zwei Reed-Kontakten (in Wirklichkeit habe ich 2 x 2 parallel geschaltet, falls einer nicht auslöst) wird dem Relais-Baustein signalisiert, ob Seil gespannt oder nachgelassen werden muss. Die Auslösung erfolgt durch einen kleinen Neodym Magnet, der sich (im Bild nicht sichtbar) an der Kette befindet.
Der Clou: da es während der Fahrt durch Ruckeln der Gondel zu Schwingungen im Gewicht kommen kann, wäre es nicht sinnvoll, wenn die Regelung sofort und schnell reagiert. Denn kaum ausgelöst würde die Längenänderung das Gewicht wieder aus dem Reedkontakt holen – die Anlage würde sich aufschwingen (Resonanz).
Eine „Zeitverzögerung“ ist nicht möglich, denn der Zeitraum der ausreicht oder zu viel ist lässt sich nicht bestimmen – das Gewicht könnte längst am Boden angekommen sein.
Die Lösung: eine mechanische Schalthysterese. (Siehe auch ftPedia 4/2020)
Das Gewicht zieht die Kette nämlich nicht direkt mit, sondern über einen Gleiter verzögert. Es muss also einige cm  fahren, bevor es die Kette mitnimmt. Fährt es wieder in Gegenrichtung bewegt sich der Magnet nicht – bis das Gewicht einige Strecke gefahren ist. Diese Strecke kann es ruckeln und wackeln, ohne dass die Regelung beeinflusst wird.