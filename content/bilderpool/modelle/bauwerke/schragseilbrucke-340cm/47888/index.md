---
layout: "image"
title: "Reedkontakte für Seilspannung 4"
date: "2018-09-23T13:24:04"
picture: "reed041.jpg"
weight: "66"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Reedkontakt", "Neodymmagnet"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47888
- /details171a.html
imported:
- "2019"
_4images_image_id: "47888"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47888 -->
Fertig eingebaut sieht man die gleiche Ansicht wie im Bild "Reedkontakte für Seilspannung 1", jedoch mit den neuen Kunststoff-Reedkontakten.
Sieht deutlich sauberer aus. Und funktioniert besser.

Durch die Bausteine 15 mit Loch lassen sich die 4mm Kontakte bestens schieben und justieren. Die Sensorspitzen hängen hier hinter der Kette (blaue Pfeile), so dass der Neodymmagnet (grüner Pfeil) keine Chance hat, sich an den Kontakten anzuhaften.