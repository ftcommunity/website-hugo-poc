---
layout: "image"
title: "Turm und Schrägseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20161002_171139a.jpg"
weight: "21"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Statik", "Turm", "Schrägseilbrücke", "Hängebrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44523
- /detailsc5c2.html
imported:
- "2019"
_4images_image_id: "44523"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44523 -->
Der 2. Turm "Bergstation" im Gegenlicht. 
Gut zu erkennen sind nun die schrägen Seile, welche die Fahrbahn halten.

Eigentlich sind es 4 Flaschenzüge, wobei der Fixpunkt am Turm jeweuks in unterschiedlicher Höhe ist, die lose Rolle eigentlich der Haken, welcher in die Schlafe eingehängt ist, die an der Fahrbahn befestigt ist. Und die Winde sich unten, hinter dem Turm im Lagerblock befindet.

Das System funktioniert so genau, dass wir die Biegung der Fahrbahn exakt bestimmen könnnen. Außerdem werden die Kräfte so gut aufgenommen, dass die Fahrbahn in der Mitte (Spannweite gesammt 240cm) mehrere Kilo aufnehmen kann ohne dass Probleme auftreten.
Wir erwägen daher die Spannweite nochmals zu vergrößern.

Live ist die Brücke sehr ästhetisch und elegant, die Proportionen harmonisch und machen Lust auf mehr. Wir sind gespannt wie das Ganze mit Beleuchtung aussehen wird.