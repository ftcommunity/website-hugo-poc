---
layout: "image"
title: "Fernbedienung Kabelraum"
date: "2018-09-23T13:24:04"
picture: "Bedienung-Kabelraum.jpg"
weight: "95"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47917
- /details39e9.html
imported:
- "2019"
_4images_image_id: "47917"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47917 -->
Noch ein nachgeliefertes Bild:
Die Fernbedienung mit offenem Kabelraum.
(Ansicht von der Rückseite. Die Statikplatten wurden entfernt oder hochgedreht.)

Zu erkennen sind einige Versteifungen der Bedienung. Dazu der nach oben gehende Mast für die Beleuchtung.
Schräg nach rechts unten geht das Centronics-Kabel raus. Das Ende des Kabels ist als Kabelpeitsche ausgeführt. Alle Kabel haben unterschiedliche Stecker / Buchsen und werden mit den Kabeln der Schalter und Lampen verbunden.

In diesem schmalen Kabelraum sind nun alle Kabel untergebracht und ordentlich beschriftet, so dass bei einem Problem der passende Stecker leicht gefunden wird.

Die Abdeckung der Bedienung mit Beschriftung liegt oben auf und ist mit Riegeln befestigt.
Ebenso die untere Abdeckung, die aus einer festen Pappe mit Ausschnitten für das Centronics und die Statik-Stützen versehen ist, sowie Beschriftung. (Siehe Fotos vorher). Sie ist eingehängt in die querliegende Statikträger, der dann wieder an den Platten von außen angeschraubt ist.

Um die Rasterhöhe einzuhalten muss ich die 60er Statikbeine mit zwei 5er Steinen verlängern. Das gibt der Bedienung einen Klotz an Höhe Bodenfreiheit - genug, dass das Centronicskabel raus gehen kann.