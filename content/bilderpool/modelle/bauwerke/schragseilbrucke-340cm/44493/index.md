---
layout: "image"
title: "Vergleich: neuer Turm (hinten) und alter"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_182804a.jpg"
weight: "6"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Statik", "Hängebahn", "Brücke", "Turm"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44493
- /details3688.html
imported:
- "2019"
_4images_image_id: "44493"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44493 -->
Vergleich:
Deutlich zu sehen ist, dass der neue Turm eine andere Fahrschienenaufhängung erhalten hat. Außerdem ist der obere Ausleger nun deutlich schwerer und stabiler geworden und kann so als Ggengewicht für die Fahrbahn eingesetzt werden. Auch die Verstrebung ist deutlich stabiler geworden, ebenso die Abspannungen nach hinten.