---
layout: "image"
title: "Handbetrieb"
date: 2020-08-30T16:23:29+02:00
picture: "Fernbedienung03.jpg"
weight: "113"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Handsteuerung. Im linken Drittel der Fernbedienung befindet sich hinter einer Klappe geschützt der Handbetrieb der Anlage.
Hier ist der Decken / Klappe geöffnet. Wenn diese geschlossen wird aktivieren die beiden Riegel, die am oberen Rand der Klappe nach unten gerichtet sind zwei Taster ( zu finden unter den Löchern ganz unten am Bildrand). Diese schalten die Anlage auf Automatik oder Handbetrieb.
Damit die Klappe geschlossen bleibt wird sie von den zwei Magneten zusätzlich gehalten.
Hier muss die Klappe offen sein – dazu ist eigentlich eine Stütze links am Rand eingebaut, die sich hochklappen lässt. (Habe ich leider vergessen.)
Sobald die Klappe geöffnet ist bleibt der Antrieb stehen und die gelbe Lampe links oben leuchtet.
Der Schalter mit Mittelstellung ist nun für die manuelle Steuerung der Seilspannung zuständig. Das ist auch nötig wenn die Anlage auf- oder abgebaut wird: so kann das Seil im Maschinenraum für den Transport verstaut werden.
Der Taster aktiviert den Antrieb des Faahrmotors. Die Richtung entspricht jener der Hauptsteuerung.
Natürlich sind Warnhinweise und Beschriftungen angebracht. Über die fast 4 Jahre Betrieb hat die Abdeckung aber leider etwas gelitten.