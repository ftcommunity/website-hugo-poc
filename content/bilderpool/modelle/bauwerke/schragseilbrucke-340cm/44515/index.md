---
layout: "image"
title: "Chefkonstrukteur bei der Arbeit"
date: "2016-10-02T17:43:47"
picture: "IMG_20160918_194816a.jpg"
weight: "13"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44515
- /details420b.html
imported:
- "2019"
_4images_image_id: "44515"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44515 -->
Jan (7 Jahre) beschäftigt sich mit dem Bau einiger Teile an der Hängebahn. Generationenübergabe gelungen.