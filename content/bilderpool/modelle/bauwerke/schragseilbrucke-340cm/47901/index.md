---
layout: "image"
title: "Fahrtrichtungsgeber Version 2"
date: "2018-09-23T13:24:04"
picture: "Fahrtrichtungsgeber-entwicklung02.jpg"
weight: "79"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Redkontakt", "Fahrtanzeiger"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47901
- /detailse912.html
imported:
- "2019"
_4images_image_id: "47901"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47901 -->
Der Fahrtrichtungsgeber, der direkt auf der Spulenwelle war hat uns bei der Ausstellung in Dreieich ft:con 2017 immer wieder Probleme bereitet. Nun muss auch dieser an ein Redesign glauben.

Die alte Version findet sich hier: http://www.ftcommunity.de/details.php?image_id=46024#col3
und hier: http://www.ftcommunity.de/details.php?image_id=46025#col3

Die Probleme waren durch die erhebliche Reibung und die Stöße bedingt, die entstehen wenn die Taktscheiben auf die Taster treffen.
Zudem brauchte alles viel Platz.

In der neuen Version (Achtung Spoiler - das wird nciht die letzte Version bleiben!) sind nun Reedkontakte verbaut. Reibungslos und zuverlässig.
Ich nutze 5mm Kontakte in Kunststoffgehäusen, da die Glaskontakte (vgl. mein Beitrag über die Seilspannungssteuerung) gebrochen sind. Sie lassen sich bestens in die Halter (Gelenke) einsetzen und sind mit Gummiringen gesichert. So lassen sie sich in der Länge und Drehung gut justieren.

Ausgelöst werden die Reedkontakte durch ein unter dem Z40 aufgeklebten Magneten eines Namensschildchens. Dass dieses 2 Magnete hat ist von Vorteil, da so ein größerer Winkel ausgelöst wird. Sonst Blinkt die Lampe zwar, aber es gibt auch Stellungen, bei denen kein Kontakt ausgelöst werden würde.

Und klar - wer mich kennt - ich wiederhole mich - habe ich das erst einmal als Funktionsprototyp gebaut.