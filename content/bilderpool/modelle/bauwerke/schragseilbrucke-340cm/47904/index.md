---
layout: "image"
title: "Fahrtrichtungsgeber Version 3 - Endversion"
date: "2018-09-23T13:24:04"
picture: "Fahrtrichtungsgeber.jpg"
weight: "82"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Reedkontakt"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47904
- /detailsc20f-3.html
imported:
- "2019"
_4images_image_id: "47904"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47904 -->
Und was macht ein Entwickler, wenn er schlaflose Nächte hat? Er entwickelt den Fahrtrichtungsgeber neu.

Das Grundgestell mit den Reedkontakten entspricht noch etwa jenem der Version 2 (Bild vorher). 
Aber die Magnete wurden durch kleine Neodymknöpfe ersetzt. Sie befinden sich auf dem Steckrad und sind mit Heißkleber fest gemacht (ich hasse Heißkleber, aber hier rettet er meinen Hintern).
Das Rad sitzt auf einer Stange, die mit dem Z40 im Hintergrund verbunden ist. Es greift in das Z10 (zu erkennen an der Stange links), welche die Welle der Spule ist.

Da der Geber nun aufrecht steht und das Rad innerhalb der Reedkontakte liegt, ist die Baugröße stark geschrumpft und das Kopfgetriebe entfällt. Nun ist er absolut zuverlässig.

Zu sehen noch etwas Deko: die gelben Streben sollen eine Absperrung sein.
Der Taster links im Bild hat damit nichts zu tun - er schaltet später das Arbeitslicht.