---
layout: "image"
title: "Fehlversuch: Ausleger / Lösung"
date: "2016-10-23T20:57:10"
picture: "IMG_20161021_150551b.jpg"
weight: "25"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["fehlversuch", "statik", "schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44665
- /details1bb4.html
imported:
- "2019"
_4images_image_id: "44665"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44665 -->
Der obere Ausleger der Schrägseilbrücke wurde wie ein Kranausleger konstruiert - ein Fehler!
(zur Verdeutlichung sind die drei Bilder um 90° gegen den Uhrzeigersinn gedreht, denn im Original steht der Turm natürlich fast aufrecht.)

Die obere Spitze des Turms (im Bild links) wird immer leichter. Das Fachwerk (im Bild rechter Rand) bekommt einen ausleger und einen freien Kragarm (rot/schwarz).

Oberes Bild:
Das obere Schrägseil zieht am blauen Pfeil nach unten. Diese Kraft wird durch die Streben (oberer blauer Pfeil) ausgeglichen. Der Ausleger bleibt durch das Kraftgleichgewicht gerade.

Mittleres Bild:
Das zweite Seil von oben zieht am Übergang des Fachwerks zum leichteren Teil. Problem hierbei ist, dass der Fachwerkarm zwar stabil nach hinten (hier oben) gezogen wird (mit der nicht markierten Strebe nach rechts oben im Bild).
Aber: da auch die obere Strebe den freien Kragarm nach hinten zieht knickt dieser ab. (Zum Vergleich: hellgrüne Linie.)
Die Strebe (drüberPfeil nach rechts) versagt, da sie auf Druck belastet wird.
Bei einem Kran käme dieser Fall nicht vor, da zum einen das Gewicht des freien Kragarms nach unten zieht - beim Turm aber dieses Gewicht nach hinten/unten (hier im Bild nach oben) zieht. Und zudem die obere Strebe (linker blauer Pfeil) nicht existiert.

Unteres Bild:
Eine der möglichen Lösungen (und für uns die schönste) ist das Ersetzen der Streben durch eine steife Stange, die Schubkräfte übertragen kann.
Zusammen mit beiden Zugseilen (Pfeile nach unten) verteilt sich nun die Last wunderbar auf die Streben und das Fachwerk und der Trum bleibt an der Spitze weiterhin gerade (hellgrüne Linie. Hinweis: der kleine Knick kommt durch die Perspektive / Kameraposition zustande, im Modell ist der Ausleger kerzengerade.)