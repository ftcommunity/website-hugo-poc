---
layout: "image"
title: "Trennung"
date: 2020-08-30T16:23:12+02:00
picture: "Verkabelung06.jpg"
weight: "126"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Die Fahrbahn kann in der Mitte mit zwei Riegeln (schwarz markiert) auseinander genommen werden. Nun hält nur noch das Serielle Kabel die Teile zusammen. Dann können diese zusammengeklappt werden. Nun ist die Stange ca. 170 lang und passt ins Auto oder lässt sich gut lagern.