---
layout: "image"
title: "Antriebsstränge / Gleichlaufgetriebe "
date: 2020-08-30T16:23:41+02:00
picture: "Antrieb.jpg"
weight: "104"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Hier erkennt man den Kraftverlauf des Antriebs:
(magenta) der rechte Powermotor treibt die Anlage an und nennt sich „Fahrt“. Seine Welle geht nach vorne an die Front des Maschinenhauses in der Talstation und treibt über das rechte 20er Ritzel die beiden anderen 20er in gleicher Geschwindigkeit an.
(gelb + blau) die angetriebenen Achsen gehen durch die (verborgenen) Differentialgetriebe (das dreht die Laufrichtung) und damit auf die Spulen (durch den 2. Motor verdeckt). Die Verlängerung der blauen Achse greift am oberen Bildrand mit einem 10er auf ein 40er Ritzel. Das ist der Geber für die Fahrtrichtungsanzeige. (Am Ritzel ist ein weißes Dreieck aufgeklebt, das die Stellung des Magneten zeigt.)
(orange) der 2. Powermotor wird durch das Relais angesteuert und greift über ein 10er Ritzel von oben auf die Differentialgehäuse. Damit werden die Abtriebe (Spulen) gegeneinander verdreht. Diese Bewegung addiert sich zur Hauptbewegung. So können die Spulen unterschiedlich viel Seil abgeben / aufnehmen und damit die Länge des Seils bestimmen.
Außerdem im Bild: unten die „Abschrankung“ in gelb ausgeführt.
Links am Bildrand noch der gebogene Statikträger, auf dem die Beleuchtung sitzt welche bei geschlossenem Gehäuse einklappt.
Di Achse ganz rechts ist das Scharnier des Gehäuses.