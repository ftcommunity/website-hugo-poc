---
layout: "image"
title: "Schönheits-OP (Teil 1) Vergleich"
date: "2017-07-29T19:32:20"
picture: "hintere_Seile_vergleich.jpg"
weight: "54"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46102
- /details7429.html
imported:
- "2019"
_4images_image_id: "46102"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-29T19:32:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46102 -->
Der Vergleich der hinteren Seile zeigt deutlich:
(links) die Seile waren vertauscht und bildeten kein schönes Bild mit den Streben des Turms. Zudem reibt ein Seil an dem Block.
(rechts) schöner, ordentlicher, so sieht es gut aus.