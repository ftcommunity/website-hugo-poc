---
layout: "overview"
title: "Schrägseilbrücke (340cm)"
date: 2020-02-22T08:39:15+01:00
legacy_id:
- /php/categories/3291
- /categories8124.html
- /categories8da3.html
- /categoriese087.html
- /categories886d.html
- /categories04e0.html
- /categories4fe3.html
- /categories4ed0.html
- /categoriesa359.html
- /categories04f3.html
- /categoriesc770.html
- /categoriesc188.html
- /categoriesad78.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3291 --> 
Die Schrägseilbrücke aus Classic-Teilen 
Breite über alles 510cm / Turmhöhe 140cm / Fahrbahnlänge = Spannweite 340cm / Höhe über Grund ca. 40cm / max. Höhenunterschied der Stationen +- 40cm 
Kabelfernsteuerung allerFunktionen. Voll beleuchtet. Demontierbar für Transport in wenigen Minuten. Voll bespielbar. 
Bauwerk aus ca. 98% Classic-Teilen im Privatbesitz (wird nach der Ausstellung wieder zum spielen benutzt). 
Design und Statik von Jan (7 Jahre). Funktion und Technik von Tilo (46 Jahre)