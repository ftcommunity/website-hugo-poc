---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 2)"
date: "2017-07-04T13:01:19"
picture: "a-Gleichlaufgetriebe-6.jpg"
weight: "41"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Gleichlaufgetriebe", "Antrieb", "Motor", "Differenzialgetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46017
- /detailsb024.html
imported:
- "2019"
_4images_image_id: "46017"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T13:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46017 -->
Ein guter Ingenieur schaut immer erst einmal was andere bereits vor ihm gemacht haben.
Und so habe ich mich von NBGer  inspirieren lassen: http://www.ftcommunity.de/details.php?image_id=38975 

HIer nun das fast fertige Getriebe, das in sich stabil ist und auf kleinem Raum gebaut wurde.
Man erkennt 3 Motoren:
1. Motor am oberen Bildrand: Hauptantrieb, greift direkt auf die 20er Ritzel
2. Motor rechts, mit Schraube nach unten, greift auf ein 10er Ritzel und dann eine Schnecke, die ein 10er Ritzel antreibt, welches die zwei großen Ritzel der Differenzialgetriebe blockiert oder versetzt. Das wäre die "Lenkung", für meinen Antriebsfall aber die Seillängenkompensation. (Erklärung später).
3. Motor (links) außer Betrieb. Ist ein Überbleibsel einer vorherigen Konstruktion, wird später entfernt.

(Fortsetzung nächstes Bild)