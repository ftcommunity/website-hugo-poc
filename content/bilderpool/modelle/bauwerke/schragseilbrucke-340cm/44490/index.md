---
layout: "image"
title: "Baustelle Hängebahnbrücke 2,4m Spannweite / 4m Gesamtlänge"
date: "2016-10-01T22:12:29"
picture: "GOPR9186a.jpg"
weight: "4"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Hängebahn", "Brücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44490
- /detailsf218.html
imported:
- "2019"
_4images_image_id: "44490"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44490 -->
Im Fischertechnikzimmer herrscht Chaos. Jan und ich bauen an der 4m langen Hängebahn. Fahrbahnlänge sind 2,4m.

Im Video erkläre ich den Antrieb
https://youtu.be/-EDMZNxTwsE