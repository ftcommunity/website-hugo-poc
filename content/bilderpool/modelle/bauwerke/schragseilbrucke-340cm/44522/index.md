---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20161002_171203a.jpg"
weight: "20"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Brücke", "Hängebrücke", "Schrägseilbrücke", "Seilwinde"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44522
- /details73b0-2.html
imported:
- "2019"
_4images_image_id: "44522"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44522 -->
Lagerblock in Funktion von oben.
Von rechts kommen die Seile vom Turm herunter.
Alle 4 Halteseile werden im Lagerblock aufgespult. Die Riegel blockieren die Zahnräder und somit die Spulen. Außerdem werden die Kräfte der Haltestreben aufgenommen und in die Grundplatte eingeleitet. Nach tests zeigt sich: das System ist absolut perfekt und arbeitet millimetergenau.