---
layout: "image"
title: "ft:express - neue Version"
date: "2018-09-23T13:24:04"
picture: "express.jpg"
weight: "77"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Gondel", "Hängebahn"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47899
- /details0571.html
imported:
- "2019"
_4images_image_id: "47899"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47899 -->
Auch die Gondel bekommt ein Update.

Nachdem wir 2017 auf der ft:con in Dreieich waren und wir mit der Anlage einige Problemchen hatten, habe ich eines nach dem anderen behoben und versucht alles noch stabiler, schöner, eleganter, technishc besser etc. zu machen.

(oben) Die alte Gondel wurde von meinem Sohn Jan (9) gebaut. In der Ursprungs-Version war sie sogar noch schwingend mit Gelenken angebaut um immer gerade zu hängen. Das brachte nicht nur Probleme mit sich, sondern war auch viel zu wuchtig und der Nutzen war nicht ersichtlich.
Daraus ergab sich die hier gezeigte Version (Dreieich 2017). Die Gondel hängt an den drei schwarzen Rollen die auf dem Alu-Profil angebracht sind. Ursprünglich hatten sie ein Widerlager in Rollen am unteren Alu-Profil. Der ABstand entspricht dann genau der Laufschiene.

(unten) Die neue Gondel hat nun einheitliches Aussehen (keine grauen und roten Steine mehr) und etwas Platten zur Optik erhalten. Dadurch wird die Konstruktion außerdem stabiler
Der Aufkleber wurde entfernt und wird durch einen neuen ersetzt.

Zu sehen sind noch die Enden aus schwarzen 15er Statiksteinen, die den Endanschlag auslösen sollen. Wie im Bild vorher gezeigt wurden diese umgebaut.

Die Stoßdämpfer sind lediglich für die Optik und wurden vom Designer Jan als Pflicht angesehen.