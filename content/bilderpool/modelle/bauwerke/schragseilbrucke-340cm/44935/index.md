---
layout: "image"
title: "Fahrbahnbeleuchtung (Mitte)"
date: "2016-12-28T12:29:45"
picture: "IMG_20161227_110533.jpg"
weight: "27"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["beleuchtung", "elektrik", "LED", "lampen"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/44935
- /details277e.html
imported:
- "2019"
_4images_image_id: "44935"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44935 -->
Über der Fahrbahn spannt sich von Turmspitze zur anderen ein Seil, an dem 5 LED Leuchten zur Fahrbahnbeleuchtung angebracht sind. Dies ist die Lampe in der Mitte.

Gut erkennbar ist der rote Punkt, der die Position der Lampe am Seil markierte (ausgemessen). Ein Baustein 7,5 mit seitlichen Nuten wurde mit einem Verbinder 15 benutzt, um an das Seil die Lampe zu befestigen.
Die Zuleitungen wurden mit dem Tragsei lverflochten, was zusätzliche Befestigungen (z.B. mit Kabelbindern) überflüssig macht und ein Gewisses Design ergibt.

Die Fahrbahnbeleuchtungen sind selbstbau LEDs (siehe Foto weiter hinten) und eine Lochblende 6mm.