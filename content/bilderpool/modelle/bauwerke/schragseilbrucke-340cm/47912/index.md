---
layout: "image"
title: "Kabelchaos 1"
date: "2018-09-23T13:24:04"
picture: "gehuse01.jpg"
weight: "90"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/47912
- /detailsb8da.html
imported:
- "2019"
_4images_image_id: "47912"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47912 -->
Natürlich ist auch der "passive" Turm 2 (Bergstation) nicht so passiv, wie es klingt.
Er beherrbergt nicht nur die Endschalter an der Traverse (siehe vorher), sondern auch die Lichter zur Trumbeleuchtung / Fahrbahnbeleuchtung (hier eine Lampe am Block rechts).
Die Signale / Strom werden mit Hilfe eines unter der Fahrbahn angebrachten Seriellen Kabels übertragen.
Der Stecker des Kabels benörigt eine Verbndung - und diese befindet sich im "Betriebstgehäuse", das leider erst nachträglich aufgebaut wurde.

Es hat mich schon auf der ft:con 2017 in Dreieiech gestört aber war einfach nötig.
Diesmal sollte die Anlage aber ein Facelift bekommen und wesentlich verbessert werden.
Also: weg damit!
Und die ganzen Kabel wo anders in. Den Prozess zeige ich in den nächsten Bildern.