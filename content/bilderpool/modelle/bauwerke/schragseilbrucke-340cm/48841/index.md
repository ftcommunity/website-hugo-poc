---
layout: "image"
title: "Funktionsprinzip"
date: 2020-08-30T16:23:42+02:00
picture: "Funktionsprinzip.png"
weight: "103"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Das Funktionsprinzip der Gondelsteuerung:
Die Gondel wird mit zwei Seilen (blau = Talfahrt / rot = Bergfahrt) an der Fahrbahn gezogen.
Diese Seile werden über Differentiale (orange) im Gleichlaufgetriebe und 3 Ritzel 20 mit dem Motor f. Fahrt gleichmäßig angetrieben (auf- oder abgespult, je nach Fahrtrichtung).
Durch unterschiedliche Durchmesser der Spulen (ändert sich, wenn Seil abgegeben oder aufgenommen wird) verlängert oder verkürzt sich das Seil.
Dabei bleibt die Spannung des Seils immer gleich, denn das Spanngewicht im Unterturm „Schacht“ der Talstation verändert sich nicht. Es wird aber durch verkürzen des Seils (grauer Pfeil) nach oben gezogen. Nach etwas Spiel nimmt der Mitnehmer die Steuerkette (grün) mit. Der auf der Gegenseite der Kette angebrachte Magnet bewegt sich nach unten, bis er den Reedkontakt auslöst. Dieser gibt über den Relais-Baustein (Selbstbau-Silberling) dem Motor f. Seillänge die Richtung vor. Nun wird über die Welle oberhalb der Differentiale deren Gehäuse gegeneinander verdreht. Damit die Laufgeschwindigkeiten der Spulen unterschiedlich.
Folge: das Seil wird länger, das Gewicht senkt sich wieder. Im ersten Moment kann dieses die Steuerkette aber nicht mitnehmen (mech. Hysterese), wodurch mehr Seil gegeben wird (ca. 2 Mal den Weg des Mitnehmers). Erst danach wird der Magnet vom Reed gelöst und kein Seil mehr nachgelassen.
Die andere Richtung funktioniert entsprechend.
Außerdem sind im Schacht am oberen und unteren Ende (gelbe Dreiecke) Schalter angebracht, die beide Motoren sofort auf Halt setzen und an der Fernbedienung „Störung!“ signalisieren.
In den Türmen sind außerdem Schalter (grüne Dreiecke), welche „Ankunft Station“ signalisieren und den Antrieb in diese Richtung unterbrechen (realisiert mit Dioden).
Außerdem: an der roten Spule ist über ein Ritzel 10 ein Ritzel 40 angetrieben, welches einen Magneten an 4 Reekdkontakten vorbei dreht. Diese signalisieren in der Fernbedienung die Fahrtrichtung als Lauflicht (4 weiße LED). Wird unterbrochen, wenn „Ankunft“ (blaue LED für Tal- / Bergstation) signalisiert wird.
Der Motor f. Fahrt bekommt im Automatikbetrieb bei Fahrtrichtungswechsel wieder direkt Strom, oder wird in der Handsteuerung mit Taster versorgt.
Ebenfalls kann die Seilspannung im Handbetrieb manuell gesteuert werden (Regelung außer Betrieb). Dies erlaubt auch den Auf-/Abbau der Anlage, da das Seil zum Transport im Turm aufgespult wird.
