---
layout: "comment"
hidden: true
title: "24149"
date: "2018-08-30T11:14:13"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hi zusammen...

etwas spät mein Kommentar...ich war länger nicht mehr auf der Seite...hab beruflich zu viel zu tun...
aber schön, dass meine Getriebe-Idee Verwendung findet! und besonders schön, so alles komplett mit den alten Teilen und Motoren! 

Obwohl ich selbst nicht mehr zu baue.
Die alten Motoren sind mir zu schwach, zu groß; und die normalen Naben an den Zahnrädern rutschen mir zuviel. Aber ich baue auch meistens schweres Gerät, das Fischertechnik mechanisch weit über seine Grenzen hinaus bringt.