---
layout: "image"
title: "Blick von der Seite"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48207
- /detailsbd68.html
imported:
- "2019"
_4images_image_id: "48207"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48207 -->
Die Garage hat ebenfalls eine Fensterfläche.
