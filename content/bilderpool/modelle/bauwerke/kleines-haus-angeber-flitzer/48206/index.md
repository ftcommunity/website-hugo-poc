---
layout: "image"
title: "Blick in die Wohnung"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48206
- /details5ea9.html
imported:
- "2019"
_4images_image_id: "48206"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48206 -->
Naja, es ist eher ein 1-Zimmer-Appartement, aber immerhin mit einer schmalen Tür und einer Terrasse.
