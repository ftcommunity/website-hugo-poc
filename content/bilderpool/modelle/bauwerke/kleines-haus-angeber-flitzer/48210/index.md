---
layout: "image"
title: "Blick in die Garage"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48210
- /details4086.html
imported:
- "2019"
_4images_image_id: "48210"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48210 -->
Die Winkel (und einige Federnocken) sind natürlich nicht gerade klassisches Ur-ft, aber so verrutschen die Träger der Garage nicht so leicht.
