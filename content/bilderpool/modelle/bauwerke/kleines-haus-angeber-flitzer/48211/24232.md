---
layout: "comment"
hidden: true
title: "24232"
date: "2018-10-15T14:22:44"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Meine Originalscheiben waren verzogene (da gibt's hier auch Gegenüberstellungsfotos). Diese hier sind gebraucht von ffm von vor mehreren Jahren, und die waren in diesem sehr guten Zustand.

Gruß,
Stefan