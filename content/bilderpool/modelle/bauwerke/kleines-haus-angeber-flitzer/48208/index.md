---
layout: "image"
title: "Rückseite"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48208
- /details2ffc.html
imported:
- "2019"
_4images_image_id: "48208"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48208 -->
Auch nach hinten gibt's Fenster
