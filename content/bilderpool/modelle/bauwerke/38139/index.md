---
layout: "image"
title: "Voilà la tour d'Eiffel en miniature"
date: "2014-01-29T12:41:28"
picture: "P1010728.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Eiffelturm", "Mini", "Mini-Modell", "Turm"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/38139
- /detailsbce2.html
imported:
- "2019"
_4images_image_id: "38139"
_4images_cat_id: "656"
_4images_user_id: "1557"
_4images_image_date: "2014-01-29T12:41:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38139 -->
Na ja, fast. Das war gar nicht als Modell geplant aber die Statik hat sich derart schön in die bekannte Form eingestellt, daß ich einfach nicht widerstehen konnte.

Hier nochmal ein bißchen gedreht, falls auf der anderen Ansicht etwas nicht zu erkennen sein sollte.