---
layout: "image"
title: "Pflanzendrehteller-3"
date: "2010-07-18T19:17:42"
picture: "pflanzendreher-2010-07-18-001.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27771
- /detailsd640.html
imported:
- "2019"
_4images_image_id: "27771"
_4images_cat_id: "656"
_4images_user_id: "9"
_4images_image_date: "2010-07-18T19:17:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27771 -->
Eine sehr, sehr langsame Bewegung.