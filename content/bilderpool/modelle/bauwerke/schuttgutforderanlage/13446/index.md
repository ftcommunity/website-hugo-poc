---
layout: "image"
title: "Schüttgutförderanlage03"
date: "2008-01-27T17:30:27"
picture: "schuettgutfoerderanlage3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13446
- /details6934.html
imported:
- "2019"
_4images_image_id: "13446"
_4images_cat_id: "1223"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13446 -->
Schüttgutförderanlage