---
layout: "image"
title: "von Oben"
date: "2009-05-05T16:44:02"
picture: "strommast08.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/23881
- /detailsae04-2.html
imported:
- "2019"
_4images_image_id: "23881"
_4images_cat_id: "1632"
_4images_user_id: "791"
_4images_image_date: "2009-05-05T16:44:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23881 -->
