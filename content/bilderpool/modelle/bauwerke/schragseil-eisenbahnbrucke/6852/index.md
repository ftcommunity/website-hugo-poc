---
layout: "image"
title: "Schrägseil-eisenbahnbrücke"
date: "2006-09-16T23:12:56"
picture: "FT-Schrgseilbrcke_001.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6852
- /details731a.html
imported:
- "2019"
_4images_image_id: "6852"
_4images_cat_id: "657"
_4images_user_id: "22"
_4images_image_date: "2006-09-16T23:12:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6852 -->
Fischertechnik Schrägseil-Eisenbahnbrücke

System wie die Normandie-Brücke an der französische Atlantikküste.
