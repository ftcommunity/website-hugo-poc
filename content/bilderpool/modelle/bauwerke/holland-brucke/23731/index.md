---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:03"
picture: "hollandbruecke1.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23731
- /detailsdc68.html
imported:
- "2019"
_4images_image_id: "23731"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23731 -->
Dies ist die Seitenansicht der Brücke.