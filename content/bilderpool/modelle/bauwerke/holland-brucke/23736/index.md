---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:05"
picture: "hollandbruecke6.jpg"
weight: "6"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23736
- /detailsb072.html
imported:
- "2019"
_4images_image_id: "23736"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23736 -->
Seitenansicht der geöffneten Brücke.