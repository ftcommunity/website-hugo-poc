---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:05"
picture: "hollandbruecke4.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23734
- /detailsd739.html
imported:
- "2019"
_4images_image_id: "23734"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23734 -->
Dies ist die Frontalansicht der Brücke im geöffneten Zustand.