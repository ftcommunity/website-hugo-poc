---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:04"
picture: "hollandbruecke2.jpg"
weight: "2"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/23732
- /details68cf.html
imported:
- "2019"
_4images_image_id: "23732"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23732 -->
Dies ist die Frontalansicht der Brücke.