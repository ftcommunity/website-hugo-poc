---
layout: "image"
title: "Laura's House"
date: "2008-06-11T06:55:31"
picture: "L_House_a.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["House"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14671
- /details45a4.html
imported:
- "2019"
_4images_image_id: "14671"
_4images_cat_id: "656"
_4images_user_id: "585"
_4images_image_date: "2008-06-11T06:55:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14671 -->
This is a model that Laura constructed during a meeting.
