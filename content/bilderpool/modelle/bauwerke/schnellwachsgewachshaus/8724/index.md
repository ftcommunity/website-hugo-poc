---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-28T17:16:49"
picture: "Schnellwachsgewchshaus28.jpg"
weight: "28"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8724
- /details13e1.html
imported:
- "2019"
_4images_image_id: "8724"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-28T17:16:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8724 -->
Jetzt bekommt jede Pflanze gleich viel Wasser!
