---
layout: "image"
title: "Ventile"
date: "2008-02-21T20:46:22"
picture: "Schnellwachsgewchshaus85.jpg"
weight: "78"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13717
- /details50ad-3.html
imported:
- "2019"
_4images_image_id: "13717"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-21T20:46:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13717 -->
Das sind die Ventile wie sie auf einer Platte sind. Um so wenig Kabelsalat wie möglich zu machen, habe ich ein Flachbandkabel für die Motoren benutzt. (Im Gegensatz zum Innenteil des Geächshauses sieht man hier ja die Kabel). es müssen nur noch ein paar Schläuche verlegt werden.
