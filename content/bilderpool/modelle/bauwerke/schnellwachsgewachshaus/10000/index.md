---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-04-06T19:01:52"
picture: "Schnellwachsgewchshaus56.jpg"
weight: "55"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10000
- /details1809.html
imported:
- "2019"
_4images_image_id: "10000"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-04-06T19:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10000 -->
Da mich die herunliegenden Kabel immer genervt haben, habe ich Kabelleitungshalter gebaut, die die Kabel halten. So ist es viel ordentlicher.
