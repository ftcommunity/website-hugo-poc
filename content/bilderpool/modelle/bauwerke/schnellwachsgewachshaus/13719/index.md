---
layout: "image"
title: "Ventile+Tank"
date: "2008-02-21T20:46:23"
picture: "Schnellwachsgewchshaus87.jpg"
weight: "80"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13719
- /detailsc274.html
imported:
- "2019"
_4images_image_id: "13719"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-21T20:46:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13719 -->
Diese Platte wird noch an das Gewächshaus montiert.
