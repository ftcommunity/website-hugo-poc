---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-04-06T19:01:52"
picture: "Schnellwachsgewchshaus59.jpg"
weight: "58"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10003
- /detailsd30e-2.html
imported:
- "2019"
_4images_image_id: "10003"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-04-06T19:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10003 -->
Luftaufnahme.
