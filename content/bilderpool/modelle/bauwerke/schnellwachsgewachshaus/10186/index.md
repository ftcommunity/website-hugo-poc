---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-04-28T17:52:10"
picture: "Schnellwachsgewchshaus66.jpg"
weight: "65"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10186
- /details3252-2.html
imported:
- "2019"
_4images_image_id: "10186"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-04-28T17:52:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10186 -->
Das sind die Paprikapflanzen.
