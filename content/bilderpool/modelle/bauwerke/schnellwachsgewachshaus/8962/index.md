---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-02-11T15:18:39"
picture: "Schnellwachsgewchshaus31.jpg"
weight: "31"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8962
- /details24e0.html
imported:
- "2019"
_4images_image_id: "8962"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8962 -->
Hier sieht man eine Lüftungsklappe. Sie wird von einem Zylinder gesteuert. Ich habe aber nur eine von 2 Lüftungsklappen automatisiert, weil ich denke, dass das reicht.
