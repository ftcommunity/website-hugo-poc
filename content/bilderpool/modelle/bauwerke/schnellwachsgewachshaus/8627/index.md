---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:32"
picture: "Schnellwachsgewchshaus4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8627
- /detailsc9fe.html
imported:
- "2019"
_4images_image_id: "8627"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8627 -->
Das ist die Halterung für den Wassertank. Er fasst 0.5 Liter und ist selbstgebaut.
