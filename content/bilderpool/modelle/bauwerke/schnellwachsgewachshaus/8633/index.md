---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:33"
picture: "Schnellwachsgewchshaus10.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8633
- /details063f.html
imported:
- "2019"
_4images_image_id: "8633"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8633 -->
Das ist der Deckel von der Flasche. Er hat 4 Öffnungen. 2 für die Schläuche und 2 für die Kabel. Sie sind mit Heißkleber abgedichtet.
