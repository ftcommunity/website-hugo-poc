---
layout: "image"
title: "Töpfe"
date: "2008-02-20T19:54:17"
picture: "Schnellwachsgewchshaus81.jpg"
weight: "74"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13713
- /details5048-2.html
imported:
- "2019"
_4images_image_id: "13713"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-20T19:54:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13713 -->
Es sind 8 Töpfe zu gießen. Die Verschlauchung ist noch nicht angecshlossen, kommt aber bald. Links sieht man den NTC-Widerstand zur Messung der Temperatur. Es ist schwarz angemalt, weil schwarz ja bekanntlich die Sonne anzieht.
