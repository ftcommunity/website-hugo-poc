---
layout: "image"
title: "Bedienung"
date: "2008-02-20T19:54:16"
picture: "Schnellwachsgewchshaus78.jpg"
weight: "71"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13710
- /detailsf1c4.html
imported:
- "2019"
_4images_image_id: "13710"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-20T19:54:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13710 -->
Auf die 4 Tasten kommen Gießen, Lüften, Enter und +.
Drücke ich Gießen, muss ich so oft auf + drücken bis ich den Topf ausgewählt habe, den ich gießen möchte. Die Nummer des Topfes wird auf der Anzeige angezeigt. Dann drücke ich Enter.

Drücke ich Lüften, dann drücke ich so oft +, bis ich die Anzahl an Sekunden ausgewählt habe, wie lang gelüftet werden soll. Auch das zeigt die Anzeige an. Dann drückt man auf Enter.

Auf die Tasten kommen natürlich noch Aufkleber.
