---
layout: "image"
title: "Romp achtkantmolen met benamingen"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen71.jpg"
weight: "71"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47379
- /details4c92.html
imported:
- "2019"
_4images_image_id: "47379"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47379 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant
