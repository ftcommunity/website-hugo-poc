---
layout: "image"
title: "Floris-aflevering 'De Vrijbrief'"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen39.jpg"
weight: "39"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47347
- /detailsf34e.html
imported:
- "2019"
_4images_image_id: "47347"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47347 -->
In de Floris-aflevering "De Vrijbrief" werden de soldaten van Maarten van Rossum met Pestmaskers op molenwieken vast gebonden. Dit met stuntmannen + Loyds-verzekering voor een halfuur filmdraaien. Terug te zien op Youtube na ca. 28 min. : 
https://www.youtube.com/watch?v=R8unaDyOV78&list=FLvBlHQzqD-ISw8MaTccrfOQ&index=1&t=1680s 

In fischertechnik via de link : 
Youtube-link : 

https://www.youtube.com/watch?v=UliZqs6p7HM&t=0s

Instagram account van Peter Paul Klapwijk 
https://www.instagram.com/ppkhm/ 
#lifeatawindmill 

https://www.instagram.com/explore/tags/lifeatawindmill/
