---
layout: "image"
title: "Achtkantige Polder Windmolen met scheprad te Zuilichem"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen85.jpg"
weight: "85"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47393
- /details73ac.html
imported:
- "2019"
_4images_image_id: "47393"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47393 -->
