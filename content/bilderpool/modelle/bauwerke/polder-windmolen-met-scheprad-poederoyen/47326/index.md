---
layout: "image"
title: "Kinderdijk  UNESCO- Welt Kultur Erbe  (=Vijzelmolen)"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen18.jpg"
weight: "18"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47326
- /details91aa.html
imported:
- "2019"
_4images_image_id: "47326"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47326 -->
Interessante achtergrond-info :
http://www.wilcosproductions.nl/poldermill/index.php/kinderdijk

Instagram account van Peter Paul Klapwijk 
https://www.instagram.com/ppkhm/ 
#lifeatawindmill 

https://www.instagram.com/explore/tags/lifeatawindmill/
