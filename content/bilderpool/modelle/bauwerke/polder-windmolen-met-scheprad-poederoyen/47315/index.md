---
layout: "image"
title: "Scheprad"
date: "2018-03-09T22:19:45"
picture: "polderwindmolen07.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47315
- /details7b46.html
imported:
- "2019"
_4images_image_id: "47315"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:19:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47315 -->
