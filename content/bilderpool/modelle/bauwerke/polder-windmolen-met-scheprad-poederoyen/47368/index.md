---
layout: "image"
title: "Staart + lange Schoor + Vangstok"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen60.jpg"
weight: "60"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47368
- /detailsa878.html
imported:
- "2019"
_4images_image_id: "47368"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47368 -->
