---
layout: "image"
title: "Bovenas- Penlager"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen15.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47323
- /details3e19.html
imported:
- "2019"
_4images_image_id: "47323"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47323 -->
