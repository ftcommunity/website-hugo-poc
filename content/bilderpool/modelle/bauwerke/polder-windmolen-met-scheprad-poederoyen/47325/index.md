---
layout: "image"
title: "Penlager met taats"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47325
- /detailscf2c-3.html
imported:
- "2019"
_4images_image_id: "47325"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47325 -->
