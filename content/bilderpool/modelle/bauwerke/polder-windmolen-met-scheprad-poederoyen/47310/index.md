---
layout: "image"
title: "Polder- Windmolen met scheprad"
date: "2018-03-09T22:19:45"
picture: "polderwindmolen02.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47310
- /detailsd753.html
imported:
- "2019"
_4images_image_id: "47310"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:19:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47310 -->
Polder- Windmolen met scheprad 
-Achtkantige bovenkruier met Vlaamse Blokvang 

Interessante achtergrondinfo : 
http://www.wilcosproductions.nl/poldermill/index.php/molens-algemeen
