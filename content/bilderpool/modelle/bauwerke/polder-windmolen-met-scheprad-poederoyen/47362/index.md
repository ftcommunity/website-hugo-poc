---
layout: "image"
title: "Vangbalk met Sabelijzer"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen54.jpg"
weight: "54"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47362
- /details30ef.html
imported:
- "2019"
_4images_image_id: "47362"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47362 -->
