---
layout: "image"
title: "Benamingen achtkantmolen"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen70.jpg"
weight: "70"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47378
- /detailsb50a-2.html
imported:
- "2019"
_4images_image_id: "47378"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47378 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant
