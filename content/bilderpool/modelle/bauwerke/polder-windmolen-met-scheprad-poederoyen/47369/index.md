---
layout: "image"
title: "Kruiwerk"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen61.jpg"
weight: "61"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47369
- /details80c2.html
imported:
- "2019"
_4images_image_id: "47369"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47369 -->
Kruiwerk in fischertechnik dmv kogels
