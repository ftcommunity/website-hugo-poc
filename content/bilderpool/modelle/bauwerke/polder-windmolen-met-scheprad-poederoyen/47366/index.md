---
layout: "image"
title: "Staart + lange Schoor + Vangstok"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen58.jpg"
weight: "58"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47366
- /details14cb.html
imported:
- "2019"
_4images_image_id: "47366"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47366 -->
