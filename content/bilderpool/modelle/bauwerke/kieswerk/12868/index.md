---
layout: "image"
title: "Förderbänder 1"
date: "2007-11-28T21:04:56"
picture: "kieswerk08.jpg"
weight: "8"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12868
- /detailsa025.html
imported:
- "2019"
_4images_image_id: "12868"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12868 -->
Drei Bänder, drei Geschwindigkeiten. Am Ende haben die Riegel genügen Abstand voneinander um problemlos die Sortieranlage zu durchlaufen.
