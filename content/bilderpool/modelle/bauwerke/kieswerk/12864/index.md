---
layout: "image"
title: "Aufzug 2"
date: "2007-11-28T21:04:56"
picture: "kieswerk04.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12864
- /details8e79.html
imported:
- "2019"
_4images_image_id: "12864"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12864 -->
Hier sieht man wie sich die Mulde anhebt. Dies geschieht über die Umlenkrolle die an der Mulde angebracht ist.
