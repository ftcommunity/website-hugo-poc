---
layout: "image"
title: "Lichtschranken von hinten"
date: "2007-12-03T23:30:28"
picture: "kieswerk3.jpg"
weight: "16"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12990
- /detailsaedb.html
imported:
- "2019"
_4images_image_id: "12990"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-12-03T23:30:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12990 -->
Links die zwei Lichtschranken für den S-Riegel6, rechts für den S-Riegel8
