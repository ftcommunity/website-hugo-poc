---
layout: "image"
title: "Aufzug 1"
date: "2007-11-28T21:04:56"
picture: "kieswerk03.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12863
- /detailsfbe1.html
imported:
- "2019"
_4images_image_id: "12863"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12863 -->
Wird mit einem Flaschenzug hochgezogen. Leider ein bisschen langsam, doch es hat seinen Grund. Siehe die nächsten zwei Bilder.
