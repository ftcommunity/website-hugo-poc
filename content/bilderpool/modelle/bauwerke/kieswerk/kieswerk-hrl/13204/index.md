---
layout: "image"
title: "Abholen"
date: "2008-01-02T21:29:31"
picture: "kieswerk4.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13204
- /detailse9c6.html
imported:
- "2019"
_4images_image_id: "13204"
_4images_cat_id: "1192"
_4images_user_id: "424"
_4images_image_date: "2008-01-02T21:29:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13204 -->
Hier werden die sortierten Riegel abgeholt von wo Sie ihrem Bestimmungszweck zugeführt werden.
Das Programm für das HRL ist soweit fertig und funktioniert einwandfrei.
