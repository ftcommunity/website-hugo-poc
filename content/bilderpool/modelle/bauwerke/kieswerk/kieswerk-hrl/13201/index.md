---
layout: "image"
title: "Ansicht"
date: "2008-01-02T21:29:31"
picture: "kieswerk1.jpg"
weight: "1"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13201
- /detailsb9d1-2.html
imported:
- "2019"
_4images_image_id: "13201"
_4images_cat_id: "1192"
_4images_user_id: "424"
_4images_image_date: "2008-01-02T21:29:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13201 -->
Gesamtansicht Kieswerk. Im Hintergrund die Sortieranlage und im Vordergrund die HRL-Halle mit dem Bahnverlad. Rechts das HRL für die leeren Kassetten.
Was jetzt noch fehlt  ist die Eisenbahn und die Ausgabe für den Verbraucher (Ich).
Sobald ich einen geeigneten Standort (Platzproblem) gefunden habe geht's weiter.
