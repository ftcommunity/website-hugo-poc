---
layout: "image"
title: "Sortieranlage 1"
date: "2007-11-28T21:05:13"
picture: "kieswerk11.jpg"
weight: "11"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12871
- /details4e72.html
imported:
- "2019"
_4images_image_id: "12871"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:05:13"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12871 -->
Bereitete mir bis jetzt das grösste Kopfzerbrechen
