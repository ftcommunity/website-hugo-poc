---
layout: "image"
title: "Pflanzendrehteller-1"
date: "2010-07-18T19:15:38"
picture: "pflanzendreher-2010-07-18-003.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27769
- /details5953.html
imported:
- "2019"
_4images_image_id: "27769"
_4images_cat_id: "656"
_4images_user_id: "9"
_4images_image_date: "2010-07-18T19:15:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27769 -->
Sonnenblumen (das sind doch welche?) haben die unschoene Eigenschaft, immer zur Sonne zu wachsen. Sie haengen also im rechten Winkel an der Fensterscheibe und gedeihen nicht so, wie sie koennten. Was hilft? 

Eine sehr, sehr langsame Drehbewegung, so dass die Pflanzen moeglichst keine bevorzugte Richtung mehr haben, aus der sie maximale Sonneneinstrahlung bekommen koennen. Ideal ist eine Drehzahl, die viel hoeher ist als 1/Tag -- was dieses Getriebe fuer eine Drehzahl macht, habe ich aber noch nicht gemessen. Die Bewegung ist jedenfalls nicht mehr sichtbar.