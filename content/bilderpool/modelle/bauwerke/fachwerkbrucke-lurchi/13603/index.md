---
layout: "image"
title: "Einblick in das Brückentragwerk"
date: "2008-02-08T23:17:03"
picture: "Neues_Bild2.jpg"
weight: "5"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/13603
- /details803b.html
imported:
- "2019"
_4images_image_id: "13603"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-08T23:17:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13603 -->
