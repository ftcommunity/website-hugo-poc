---
layout: "comment"
hidden: true
title: "5286"
date: "2008-02-11T16:04:19"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo miteinander,

in der Brücke wurden u.a. folgende Bauteile verbaut:

180 x Winkelträger 120 grau (in den Pfeilern und im Brückenoberbau)
48 x U-Träger 150 rot (im Brückenoberbau)
96 x I-Strebe 120 grau (im „Brückenkopf“)
20 x Platte (große Statikplatte) 180 x 90 rot (Fahrbahn)
38 x Kreuzknotenplatte grau mit je vier angesetzten (=152 Stück) X-Streben 42,4 grau (im „Brückendach“)
ca. 200 x X-Strebe 106 grau (in den Pfeilern und im Brückenoberbau)
ca. 200 x I-Strebe 75 grau (in den Pfeilern und im Brückenoberbau)
16 x Winkelträger 7,5° grau (in den ¼-Bogen)
96 x Winkelstein 7,5° rot (in den ¼-Bogen)
ca. 120 x Winkelträger 60 grau (davon 104 in den ¼-Bogen)
Rund 3000 x S-Riegel 4 rot
u. v. m.

Gruß

Lurchi :-)