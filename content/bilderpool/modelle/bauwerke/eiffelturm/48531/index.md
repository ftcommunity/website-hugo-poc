---
layout: "image"
title: "Einfacher Eiffelturm"
date: "2020-03-30T13:28:47+01:00"
picture: "Eiffelturm-Clemens.jpg"
weight: "6"
konstrukteure: 
- "Clemens Püttmann"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "Website-Team"
license: "unknown"

---
