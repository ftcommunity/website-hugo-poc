---
layout: "overview"
title: "Eiffelturm"
date: 2020-02-22T08:38:15+01:00
legacy_id:
- /php/categories/2110
- /categoriesc520.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2110 --> 
Der Maßstab ist ca. 1:100,
der Eiffelturm ist somit ca.3 m hoch.
Er hat eine kleine Beleuchtung, aber leider keinen Aufzug.