---
layout: "image"
title: "Das letzte gebogene Trägersegment"
date: 2021-02-27T23:39:25+01:00
picture: "EiffelturmClubheft1970-311.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die im Bild vorher angesprochenen I-Streben 30. Am unteren Ende der oberen Statikträger sitzen zum ersten Mal einfach S-Laschen 30.