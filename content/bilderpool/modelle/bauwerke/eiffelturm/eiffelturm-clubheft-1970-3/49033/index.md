---
layout: "image"
title: "Obere Plattform, Beleuchtung, Turmspitze"
date: 2021-02-27T23:39:23+01:00
picture: "EiffelturmClubheft1970-313.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Plattform ist mit nur 2 gegenüberliegenden S-Riegeln befestigt. Dadurch lässt sie sich ohne temporäres Verbiegen von Teilen anbringen.

Die roten Platten habe ich ergänzt. Ich glaube, dass das Original auf der obersten Plattform keine besaß.

Die vier BS15 darüber escheinen mir plausibel, aber sehen kann man sie nicht. Darauf habe ich - ebenfalls etwas geraten - vier senkrecht stehende BS15 angesetzt, die die vier Lampen tragen. Im Original ragten die Stecker nach unten. Wegen der leichteren Parallelschaltung aller vier Lampen habe ich die Buchsen waagerecht angeordnet.

Die Turmspitze ist vermutlich aus zwei S-Trägern 60, evtl. auch einem 60 und einem 30, aufgebaut.