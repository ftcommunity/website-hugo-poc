---
layout: "image"
title: "Änderung: Kabelführung"
date: 2021-02-27T23:39:18+01:00
picture: "EiffelturmClubheft1970-318.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Leitungen zwischen Akku und Beleuchtung führe ich einfach auf einer Seite des Turms. Zu diesem Zweck sind diese beiden S-Laschen 30 mit S-Riegeln 6 befestigt, damit die Kabel darunter eingeklemmt werden können.