---
layout: "image"
title: "Übergang zur obersten Plattform"
date: 2021-02-27T23:39:24+01:00
picture: "EiffelturmClubheft1970-312.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit einem weiteren Ring aus S-Laschen 30 endet der Hauptturm hier, und zwar mit S-Trägern 60 oben. An denen hängt die oberste Plattform