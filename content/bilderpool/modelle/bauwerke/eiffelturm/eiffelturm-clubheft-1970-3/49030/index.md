---
layout: "image"
title: "Änderung: Stromversorgung"
date: 2021-02-27T23:39:20+01:00
picture: "EiffelturmClubheft1970-316.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man den im Turm lose versteckten 9V-Akku. Man kann einen der Träger leicht herausziehen, um ihn zu wechseln.