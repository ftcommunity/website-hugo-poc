---
layout: "image"
title: "Letzte Strebengruppe unterhalb der obersten Plattform"
date: 2021-02-27T23:39:26+01:00
picture: "EiffelturmClubheft1970-310.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies sind X-Streben 127,7. Im Loch darüber - hier gerade nicht mehr sichtbar - liegen I-Streben 30 quer. Alles passt sehr gut.