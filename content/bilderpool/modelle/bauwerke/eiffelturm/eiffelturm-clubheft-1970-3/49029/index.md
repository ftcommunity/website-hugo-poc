---
layout: "image"
title: "Änderung: Untere Plattform"
date: 2021-02-27T23:39:19+01:00
picture: "EiffelturmClubheft1970-317.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf diese Weise können die großen Platten verwendet werden. Sowohl Clipsplatten als auch die neueren mit Zapfen können verwendet werden.