---
layout: "image"
title: "Änderung: Untere Plattform"
date: 2021-02-27T23:39:22+01:00
picture: "EiffelturmClubheft1970-314.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Damit ich die größeren Verkleidungsplatten verwenden kann, baute ich die untere Plattform nochmal anders.