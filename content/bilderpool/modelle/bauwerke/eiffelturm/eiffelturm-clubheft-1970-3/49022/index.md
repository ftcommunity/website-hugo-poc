---
layout: "image"
title: "Übergang zur mittleren Plattform"
date: 2021-02-27T23:39:11+01:00
picture: "EiffelturmClubheft1970-305.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unten waagerecht liegt jeweils die Kombination I-Strebe 120, S-Lasche 30, I-Strebe 90. Das ist ein Hauch zu lang, weshalb sich das etwas nach innen biegt.

Obendrüber sind es die Kombinationen X-Strebe 169,9, S-Lasche 30, I-Strebe 60.

Die 169,9 muss es also schon gleich zu Anfang der Statik (1970) gegeben haben, obwohl sie in keinem der Ur-Statikkästen 100S, 200S, 300S, 400S enthalten war. Meines Wissens gab es die nur in den hobby-S bzw. dem gleichen Kasten unter der älteren Bezeichnung U-T S (Unterricht-Technik).