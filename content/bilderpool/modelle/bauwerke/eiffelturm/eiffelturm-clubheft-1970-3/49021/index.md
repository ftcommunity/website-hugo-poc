---
layout: "image"
title: "Übergang zur mittleren Plattform"
date: 2021-02-27T23:39:10+01:00
picture: "EiffelturmClubheft1970-306.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier dasselbe von innen gesehen.