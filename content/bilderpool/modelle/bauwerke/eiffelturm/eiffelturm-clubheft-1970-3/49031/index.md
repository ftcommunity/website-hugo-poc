---
layout: "image"
title: "Änderung: Untere Plattform"
date: 2021-02-27T23:39:21+01:00
picture: "EiffelturmClubheft1970-315.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da sitzen nun außen je zwei BS30 und dazwischen je 1 BS30, 2 S-Träger 60 und 2 S-Träger 15 mit zwei Zapfen.