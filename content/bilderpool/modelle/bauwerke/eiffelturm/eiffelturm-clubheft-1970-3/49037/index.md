---
layout: "image"
title: "Gesamtansicht"
date: 2021-02-27T23:39:28+01:00
picture: "EiffelturmClubheft1970-301.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies zeigt den Turm so original wie ich es erkennen konnte mit folgenden Abweichungen:

- Für die unterste Plattform fehlen mir die unzähligen kleinen Verkleidungsplatten.

- Der obersten Plattform hingegen spendierte ich Platten, die ich auf dem Foto aber nicht sah.

- An zwei Stellen, an denen die Träger besonders stark gebogen werden und deshalb die Zapfen potenziell leiden, habe ich anstatt - soweit ich das erkennen konnte - einheitlich S-Träger 120 je zwei S-Träger 60 verwendet, damit sich die Biegung auf mehrere Zapfen verteilt. So wurden gleich die zweiten S-Träger 120 von unten gesehen und nochmal insgesamt vier im oberen Strebenblock durch 60er ersetzt.

- Die Beleuchtung besteht statt aus roten Ur-Glühlämpchen aus vier der mehrfarbig blinkenden ft-LEDs.

- In der auf dem Clubheft-Foto kaum erkennbaren Turmspitze steckt am meisten Vermutung anstatt Wissen.

- Die Stromzufuhr kommt im Original über ein Kabel, das von unten an einem der Träger hoch geführt wurde. Bei meinem Nachbau steckt ein 9V-Blockakku ohne Gehäuse im obersten Segment gebogener Statikträger 120.

Möglicherweise erkennen andere Konstrukteure Details anders - das lässt sich gut im zugehörigen Forumsthread https://forum.ftcommunity.de/viewtopic.php?f=6&t=6516 besprechen.