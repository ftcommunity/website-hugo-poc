---
layout: "image"
title: "Änderung: Beleuchtung"
date: 2021-02-27T23:39:16+01:00
picture: "EiffelturmClubheft1970-319.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Verkabelung. Der Taster ist in die Plattform eingelassen und kann von dem Plättchen 15x15 links dauerhaft gedrückt werden. Das geht zuverlässig nur mit einem Plättchen mit Zapfen. Mit Clipsplatten ist der Verschiebeweg knapp.