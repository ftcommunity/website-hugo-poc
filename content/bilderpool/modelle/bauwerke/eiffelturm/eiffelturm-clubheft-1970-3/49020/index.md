---
layout: "image"
title: "Mittlere Plattform"
date: 2021-02-27T23:39:09+01:00
picture: "EiffelturmClubheft1970-307.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die nach oben gehenden Streben sind wieder X-Streben 169,9. Die mittlere Plattform ist, soweit ich das erkennen kann, so befestigt: X-Streben 63,6 halten sie, und ich verwende an beiden Enden S-Riegel 6.