---
layout: "image"
title: "Befestigung der untersten Plattform"
date: 2021-02-27T23:39:12+01:00
picture: "EiffelturmClubheft1970-304.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da war die Kamera wohl zu nah dran für ein scharfes Bild, sorry. Oben stecken wie gesagt 6-mm-Riegel.