---
layout: "image"
title: "Über der mittleren Plattform"
date: 2021-02-27T23:39:07+01:00
picture: "EiffelturmClubheft1970-308.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die X-Streben 169,9 sind oben über S-Riegel 6 mit waagerecht liegenden I-Streben 60 verbunden. Erst ein Loch darüber beginnt der nächste Strebensatz.