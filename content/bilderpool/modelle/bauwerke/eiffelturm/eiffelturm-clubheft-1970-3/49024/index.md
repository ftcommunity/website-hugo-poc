---
layout: "image"
title: "Befestigung der untersten Plattform"
date: 2021-02-27T23:39:13+01:00
picture: "EiffelturmClubheft1970-303.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dort passen X-Streben 63,6 sehr gut. Ich habe sie oben mit langen S-Riegeln mit der Plattform verbunden, weil sonst hohe Biegekräfte auftreten.

Auf der Platttform fehlen die vielen kleinen Clipsplatten. Damit das Endprodukt die Plattform in rot hat, baute ich sie später stark um, sodass ich Platten 30x90 und 30x30 verwenden konnte.