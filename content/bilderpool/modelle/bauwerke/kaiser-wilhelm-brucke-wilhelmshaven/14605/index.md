---
layout: "image"
title: "Die Befestigung der Streben an den senkrechten Trägern..."
date: "2008-05-30T22:39:36"
picture: "bruecke06_2.jpg"
weight: "27"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14605
- /detailsc8fb-2.html
imported:
- "2019"
_4images_image_id: "14605"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14605 -->
...sieht man hier.