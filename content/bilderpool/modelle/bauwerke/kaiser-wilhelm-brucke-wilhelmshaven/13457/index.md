---
layout: "image"
title: "Kaiser-Wilhelm-Brücke"
date: "2008-01-29T16:30:45"
picture: "kwbruecke3.jpg"
weight: "3"
konstrukteure: 
- "Oberbaurat Ernst Troschel, Firma MAN Nürnberg"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13457
- /detailsad5a.html
imported:
- "2019"
_4images_image_id: "13457"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-01-29T16:30:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13457 -->
Einer von den beiden Drehpfeilern. In dem Container unterhalb der Brücke sitzt der Antriebsmotor der über die links im Bild zu sehende Antriebswelle die Brücke unten am Drehkranz dreht.

EDIT: Wie ich letzte Tage sehen konnte ist auf der anderen Seite auch noch ein Antriebsstrang angebaut. Ich hab immer gedacht da wäre nur einer...so kann man sich irren.
