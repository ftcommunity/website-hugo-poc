---
layout: "image"
title: "Frontansicht"
date: "2008-09-08T17:45:23"
picture: "bruecke4_2.jpg"
weight: "36"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15213
- /details5382.html
imported:
- "2019"
_4images_image_id: "15213"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-08T17:45:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15213 -->
Hier nochmal ein Blick auf die beiden Halterungen. Der Bogen ist neu gestaltet. Es fehlen noch einige Streben nach oben und von einem Bogen zum anderen, sowie der obere Abschluss.