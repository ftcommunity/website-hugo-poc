---
layout: "image"
title: "Ein Bogenstück"
date: "2008-05-30T22:39:36"
picture: "bruecke10_2.jpg"
weight: "31"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14609
- /details4cec.html
imported:
- "2019"
_4images_image_id: "14609"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14609 -->
Das wird die rechte Seite der Brücke. Steht hier nur andersherum auf dem Tisch.