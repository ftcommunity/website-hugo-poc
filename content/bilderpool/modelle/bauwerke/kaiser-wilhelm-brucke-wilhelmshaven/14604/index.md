---
layout: "image"
title: "Blick auf die 'Durchfahrt' 2"
date: "2008-05-30T22:39:36"
picture: "bruecke05_2.jpg"
weight: "26"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14604
- /details529f.html
imported:
- "2019"
_4images_image_id: "14604"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14604 -->
Nochmal das gleiche Bild nur etwas näher dran.