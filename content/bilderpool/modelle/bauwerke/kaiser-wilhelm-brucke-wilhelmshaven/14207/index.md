---
layout: "image"
title: "Modell der Kaiser-Wilhelm-Brücke"
date: "2008-04-09T21:09:35"
picture: "bruecke04.jpg"
weight: "15"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14207
- /details415b.html
imported:
- "2019"
_4images_image_id: "14207"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-04-09T21:09:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14207 -->
Frontansicht des unteren Teiles. Die rechte Seite muss ich nochmal ändern. Da stimmt der Winkel nicht. Sie ist auf der rechten Seite Rechts flacher als Links verstrebt. Wenn man sich die technischen Zeichnungen ansieht dann stellt man fest das die Brücke zur Mitte hin ansteigt. Wahrscheinlich um die Durchfahrtshöhe etwas zu erhöhen. Hab ich gestern Abend festgestellt, da wars aber schon angebaut. Naja, soo schlimm ist das auch nicht. Brauch ja nur den Winkelstein durch einen, oder mehrere auszutauschen. Ist zwar ne Verzögerung am Bau aber egal.