---
layout: "image"
title: "Bauplan von der KW Brücke"
date: "2008-02-11T20:34:22"
picture: "bruecke5.jpg"
weight: "9"
konstrukteure: 
- "Oberbaurat Ernst Troschel, Firma MAN Nürnberg"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13641
- /detailsf80a.html
imported:
- "2019"
_4images_image_id: "13641"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-02-11T20:34:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13641 -->
Mit freundlicher Genehmigung von den Stadtwerken Wilhelmshavens!
