---
layout: "image"
title: "Bauplan von der KW Brücke"
date: "2008-02-11T20:34:22"
picture: "bruecke4.jpg"
weight: "8"
konstrukteure: 
- "Oberbaurat Ernst Troschel, Firma MAN Nürnberg"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13640
- /details254b-2.html
imported:
- "2019"
_4images_image_id: "13640"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-02-11T20:34:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13640 -->
Mit freundlicher Genehmigung von den Stadtwerken Wilhelmshavens!
