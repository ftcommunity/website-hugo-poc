---
layout: "image"
title: "Seitenansicht"
date: "2008-05-30T22:39:36"
picture: "bruecke09_2.jpg"
weight: "30"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14608
- /details6834.html
imported:
- "2019"
_4images_image_id: "14608"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14608 -->
Sie wächst, zwar langsam aber stetig. Ist ne ganz schöne Fummelei die passenden Winkel zu ermitteln. Der kleinste Winkelstein ist nunmal "nur" 7,5° und manchmal ist das noch zu viel.