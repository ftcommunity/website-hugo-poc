---
layout: "image"
title: "Kaiser-Wilhelm-Brücke"
date: "2008-01-29T16:30:45"
picture: "kwbruecke2.jpg"
weight: "2"
konstrukteure: 
- "Oberbaurat Ernst Troschel, Firma MAN Nürnberg"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/13456
- /details3d09.html
imported:
- "2019"
_4images_image_id: "13456"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-01-29T16:30:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13456 -->
Blick von unten auf einen Pfeiler.
