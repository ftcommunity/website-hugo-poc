---
layout: "image"
title: "Modell der Kaiser-Wilhelm-Brücke"
date: "2008-04-09T21:09:35"
picture: "bruecke06.jpg"
weight: "17"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/14209
- /detailsa35d.html
imported:
- "2019"
_4images_image_id: "14209"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-04-09T21:09:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14209 -->
Draufsicht auf den linken Ausleger.