---
layout: "image"
title: "Antennen (2)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47091
- /details8363.html
imported:
- "2019"
_4images_image_id: "47091"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47091 -->
Sowas ähnliches wie eine Fernsehantenne, und eine kleine Richtfunkantenne. Naja ;-)
