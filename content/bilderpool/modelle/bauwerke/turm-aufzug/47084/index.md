---
layout: "image"
title: "Gesamtansicht"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47084
- /details93ee.html
imported:
- "2019"
_4images_image_id: "47084"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47084 -->
Die mindestens zwei tollen Eiffeltürme der letzten Conventions trieben mich dazu, auch mal wieder was Statisches zu bauen. Ich gestehe, dass es ein Eiffelturm hätte werden sollen, aber von den Proportionen ist es eben was völlig anderes geworden. Sämtliche meine grauen Statikträger 120 und fast alle 60er sind verbaut - für 8 Stränge pro Turmfuß wie beim richtigen Eiffelturm reicht mein Material in dieser Größe bei weitem nicht.

Der zweite Anlass waren die tollen Aufzüge in den Eiffelturm-Modellen - sowas wollte ich auch mal. Also läuft im Turm von (im Bild) rechts vorne unten bis ganz zur Spitze eine Reihe von ft-Statikschienen, an dessen gebogener Bahn ein Aufzug eine Person zum Fuß und den drei Aussichtsplattformen bringen kann. Der Gag ist eher die recht einfache, aber funktionale Steuerung des Aufzugs, die unterhalb der mittleren Aussichtsplattform zwischen den beiden vorderen Trägern sitzt.
