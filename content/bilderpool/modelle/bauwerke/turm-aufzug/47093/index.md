---
layout: "image"
title: "Befestigung der Aufzugsschiene (2)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47093
- /detailsbe98.html
imported:
- "2019"
_4images_image_id: "47093"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47093 -->
Die andere Seite der oberen Schienenführung.
