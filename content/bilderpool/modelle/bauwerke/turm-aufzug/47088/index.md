---
layout: "image"
title: "Haltestelle 3"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47088
- /details1cff.html
imported:
- "2019"
_4images_image_id: "47088"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47088 -->
Ganz oben direkt unter dem Antriebsmotor befindet sich der oberste Endlagentaster des Aufzugs.
