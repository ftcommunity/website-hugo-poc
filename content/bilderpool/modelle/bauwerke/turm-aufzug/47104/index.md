---
layout: "image"
title: "Steuermodul vor dem Einbau (2)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47104
- /details9ce2.html
imported:
- "2019"
_4images_image_id: "47104"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47104 -->
Die Rückseite desselben. Die Taster für "Stockwerk rauf/runter" sind doppelt ausgeführt (sonst würde man noch zwei Relais brauchen).
