---
layout: "image"
title: "Innenansicht"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47102
- /detailsd3cd-2.html
imported:
- "2019"
_4images_image_id: "47102"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47102 -->
Ein Blick von unten nach oben im Turm. Man sieht ganz gut, dass im gebogenen Teil des Turms kurze Statikträger (30mm) verwendet wurden, damit jeder einzelne Zapfen weniger stark gequält wird.
