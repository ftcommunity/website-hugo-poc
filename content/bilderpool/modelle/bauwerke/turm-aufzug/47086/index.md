---
layout: "image"
title: "Haltestelle 1"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47086
- /detailse8c9.html
imported:
- "2019"
_4images_image_id: "47086"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47086 -->
Außer dem Taster ist nichts Besonderes verbaut, allerdings musste die Verstrebung an dieser Stelle geändert werden, um dem Taster Platz zu machen.
