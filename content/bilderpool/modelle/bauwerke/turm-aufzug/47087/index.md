---
layout: "image"
title: "Haltestelle 2"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47087
- /details1bc0.html
imported:
- "2019"
_4images_image_id: "47087"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47087 -->
Hier treffen sich die ersten Turmstränge.
