---
layout: "image"
title: "Brücke"
date: "2011-06-03T19:21:14"
picture: "bruecke3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30771
- /detailsf3a3.html
imported:
- "2019"
_4images_image_id: "30771"
_4images_cat_id: "2296"
_4images_user_id: "1162"
_4images_image_date: "2011-06-03T19:21:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30771 -->
Die Stützpfeiler
