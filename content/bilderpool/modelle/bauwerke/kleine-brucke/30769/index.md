---
layout: "image"
title: "Brücke von oben"
date: "2011-06-03T19:21:14"
picture: "bruecke1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30769
- /details52ba.html
imported:
- "2019"
_4images_image_id: "30769"
_4images_cat_id: "2296"
_4images_user_id: "1162"
_4images_image_date: "2011-06-03T19:21:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30769 -->
Die Brücke von oben, sie ist zwar nicht arg groß, aber erfüllt ihren Zweck
