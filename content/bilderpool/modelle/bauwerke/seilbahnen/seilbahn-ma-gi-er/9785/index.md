---
layout: "image"
title: "Seilbahn 3"
date: "2007-03-25T21:57:05"
picture: "seilbahn3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9785
- /details4346.html
imported:
- "2019"
_4images_image_id: "9785"
_4images_cat_id: "882"
_4images_user_id: "445"
_4images_image_date: "2007-03-25T21:57:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9785 -->
