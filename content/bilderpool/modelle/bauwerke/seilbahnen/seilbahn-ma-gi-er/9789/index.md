---
layout: "image"
title: "Seilbahn 6"
date: "2007-03-25T21:57:05"
picture: "seilbahn7.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9789
- /details6f69.html
imported:
- "2019"
_4images_image_id: "9789"
_4images_cat_id: "882"
_4images_user_id: "445"
_4images_image_date: "2007-03-25T21:57:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9789 -->
