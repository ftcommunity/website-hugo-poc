---
layout: "overview"
title: "Einfache Kuppelbare Seilbahn"
date: 2020-02-22T08:39:04+01:00
legacy_id:
- /php/categories/3159
- /categories0895.html
- /categoriesfe86.html
- /categories9abf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3159 --> 
Dies ist eine Seilbahn, die nach etlichem Tüfteln es schafft, Figuren an der Berg- und Talstation automatisch vom Seil zu heben, damit sie langsam ein- und aussteigen können. Die Idee war, möglichst einfach die Funktion nachzubauen. Der Geschwindigkeitsunterschied ist recht groß, was das an- und abkoppeln zu einer spannenden Geschichte macht. Movies gibts unter: www.dieterb.de/ft/Seite_1.mov, www.dieterb.de/ft/Seite_2.mov und www.dieterb.de/ft/vorne.mov. Viel Spaß beim weiterbauen!