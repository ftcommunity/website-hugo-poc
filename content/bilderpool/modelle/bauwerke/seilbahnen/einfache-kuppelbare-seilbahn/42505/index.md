---
layout: "image"
title: "Dito, von der anderen Seite"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn11.jpg"
weight: "11"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42505
- /details8237.html
imported:
- "2019"
_4images_image_id: "42505"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42505 -->
Der Motor paßt noch gut zwischen die ganzen Ketten und Seile