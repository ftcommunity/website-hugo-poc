---
layout: "image"
title: "Manschgerl andere Seite."
date: "2015-12-09T12:11:32"
picture: "personenseilbahn15.jpg"
weight: "15"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/42509
- /detailsccb4.html
imported:
- "2019"
_4images_image_id: "42509"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42509 -->
Die drehende Lagerung oben ist wichtig fürs Ablegen auf das schräge Raupenband.