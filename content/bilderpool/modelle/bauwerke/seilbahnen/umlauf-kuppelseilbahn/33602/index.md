---
layout: "image"
title: "Blick vom Masten in die Talstation"
date: "2011-12-03T19:52:32"
picture: "Neu_4.jpg"
weight: "13"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33602
- /details8c57-2.html
imported:
- "2019"
_4images_image_id: "33602"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33602 -->
Hier gibt es eigentlich nicht sonderlich viel darüber zu sagen.