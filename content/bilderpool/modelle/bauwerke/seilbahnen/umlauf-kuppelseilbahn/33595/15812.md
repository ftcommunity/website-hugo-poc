---
layout: "comment"
hidden: true
title: "15812"
date: "2011-12-02T21:49:11"
uploadBy:
- "Ma-gi-er"
license: "unknown"
imported:
- "2019"
---
Wollte ich auch schon immer mal bauen. Aber ich habe immer mit den Masten begonnen - was ein schwerer Anfang ist, wenn man sie original-gefedert bauen will.