---
layout: "image"
title: "Zwischenmasten der Bahn mit Bergstation im Hintergrund"
date: "2011-12-03T19:52:32"
picture: "Neu_1.jpg"
weight: "11"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33600
- /details031a.html
imported:
- "2019"
_4images_image_id: "33600"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33600 -->
Hier könnt ihr die Seilbahn im aufgebauten Zustand sehen. Ich habe dazu noch 2 Zwischenmasten gebaut, einer, der das Seil von unten hält und einer, der es von oben hält.