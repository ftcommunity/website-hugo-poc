---
layout: "image"
title: "Station von oben"
date: "2011-12-02T21:37:41"
picture: "DSCF7790.jpg"
weight: "9"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33597
- /details8a30.html
imported:
- "2019"
_4images_image_id: "33597"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-02T21:37:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33597 -->
Hier sieht man die nächsten Verbesserungen. Größere Zahnräder ermöglichen sanftes Fahren um die Kurven. Das Seil ist außerdem in einem spitzen Winkel und mit Gummiräden angetrieben, was die Reibung erhöht und das Fahrverhalten sehr verbessert. Ein mit Schnecke übersetzter Minimotor treibt die Kette nun an, was ebenfalls eine deutliche Verbesserung der Fahreigenschaften hervorruft.