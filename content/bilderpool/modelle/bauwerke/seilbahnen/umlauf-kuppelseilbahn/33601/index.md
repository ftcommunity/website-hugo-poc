---
layout: "image"
title: "Unterer Mast mit 'Schnee'"
date: "2011-12-03T19:52:32"
picture: "Neu_3.jpg"
weight: "12"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/33601
- /detailse572.html
imported:
- "2019"
_4images_image_id: "33601"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33601 -->
Der untere Mast ist zwar nicht gefedert (wie Ma-gi-er vorgeschlagen hat), aber die Rollen können frei schwingen, was einer Federung nahezu gleichkommt.