---
layout: "comment"
hidden: true
title: "7114"
date: "2008-09-09T23:54:43"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

Spannwerke: 
Das feststehende Tragseil (3mm) wird nur im Talstation mit ca. 1,1 kg vorgespannt. 

Das separate Zugseil (2mm) wird nur im Talstation uber eine dobbelte Drehscheiben mit gummi (Fahrrad-innen-Reifen)angetrieben und nur im Bergstation mit ca. 1,3 kg vorgespannt.

Beide Stationen sind fixiert. 

Gruss,

Peter D.
Poederoyen NL