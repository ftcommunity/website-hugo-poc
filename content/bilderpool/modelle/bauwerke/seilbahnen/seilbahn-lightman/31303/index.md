---
layout: "image"
title: "Bergstation Bügelschließer"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman16.jpg"
weight: "16"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31303
- /detailsa9bd.html
imported:
- "2019"
_4images_image_id: "31303"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31303 -->
Dieses Modul schließt den Bügel des Sessels, sobald dieser vorbeikommt. Hier in deaktivierter Stellung. In echten Bahnen schaut eine Lichtschranke, ob Personen zugestiegen sind. Wenn nicht, schließt sie automatisch den Bügel, um die Sitze vor dem Wetter zu schützen.