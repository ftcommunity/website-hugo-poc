---
layout: "image"
title: "Bergstation Seilkontrolle"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman23.jpg"
weight: "23"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31310
- /details4e81-2.html
imported:
- "2019"
_4images_image_id: "31310"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31310 -->
Die Lichtschranke prüft an einem nicht angetriebenen Rad, ob das Seil noch in Bewegung ist. Sollte das bei laufenden Motoren nicht der Fall sein, werden die Motoren angehalten und der Fehler wird gemeldet.