---
layout: "image"
title: "Bergstation Bügelschließer"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman20.jpg"
weight: "20"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31307
- /detailsd70a.html
imported:
- "2019"
_4images_image_id: "31307"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31307 -->
