---
layout: "image"
title: "Talstation Gesamtansicht"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman24.jpg"
weight: "24"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31311
- /details62d7.html
imported:
- "2019"
_4images_image_id: "31311"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31311 -->
Die Talstation hat mangels Teile und Zeit keine Bügelöffner und -schließer.