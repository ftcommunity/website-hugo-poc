---
layout: "image"
title: "Steuerprogramm"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman26.jpg"
weight: "26"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31313
- /detailsec3e.html
imported:
- "2019"
_4images_image_id: "31313"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31313 -->
Steuerprogramm mit den Bibliotheken von Ulrich Müller, http://www.ftcomputing.de/index.htm, entwickelt in FreePascal mit Lazarus.
Alle Motoren sind einzeln und in Gruppen steuerbar.