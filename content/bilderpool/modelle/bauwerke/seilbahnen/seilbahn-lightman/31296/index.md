---
layout: "image"
title: "Bergstation Einfahrt"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman09.jpg"
weight: "9"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31296
- /detailscf7e.html
imported:
- "2019"
_4images_image_id: "31296"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31296 -->
Der Sessel "setzt" sich mit seinen Rollen links auf die Schiene und das Seil verschwindet nach unten und dann über eine Rolle nach links.