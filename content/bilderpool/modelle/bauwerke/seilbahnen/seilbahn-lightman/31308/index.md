---
layout: "image"
title: "Bergstation Bügelöffner"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman21.jpg"
weight: "21"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- /php/details/31308
- /details2956.html
imported:
- "2019"
_4images_image_id: "31308"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31308 -->
Hier im deaktivierten Zustand.