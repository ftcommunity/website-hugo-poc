---
layout: "image"
title: "Seilbahn 4"
date: "2010-03-23T21:06:12"
picture: "seilbahn4.jpg"
weight: "4"
konstrukteure: 
- "Michael Biehl und Fabian"
fotografen:
- "Michael Biehl und Fabian"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26810
- /details680e.html
imported:
- "2019"
_4images_image_id: "26810"
_4images_cat_id: "1915"
_4images_user_id: "1051"
_4images_image_date: "2010-03-23T21:06:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26810 -->
Bergstation mit nachträglicher Befestigung