---
layout: "overview"
title: "Seilbahn (mike)"
date: 2020-02-22T08:38:52+01:00
legacy_id:
- /php/categories/1915
- /categorieseb85.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1915 --> 
Die Seilbahn ist auf einer über 30 Jahre alten Geländeplatte von mir (1,25m*0,55m - konter der flachen Eisenbahnplatte von meinem Opa und Vater- in eine neue 1,80m*0,85m große integriert) montiert - Vorbild  Hobby 1 Band 4  - ca 1m lang. Probleme gab es leider mit der Spannung, da das Polystyrol mit der Zeit etwas nachgab

Hier werde Ich mit nun meinem Enkel Fabian (4) das ein oder andere Objekt draufbauen. 

Gruß an Thomas Falkenberg für den Bildertip von ft und an die Motivation selbst Bilder einzustellen.

Michael Biehl