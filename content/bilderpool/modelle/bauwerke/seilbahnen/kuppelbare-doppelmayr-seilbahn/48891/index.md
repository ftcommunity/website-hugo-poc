---
layout: "image"
title: "Seilscheibe v1_MS CAD"
date: 2020-10-31T15:33:09+01:00
picture: "012-Seilscheibe-v3_MS-(2)-CAD.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

In der CAD Darstellung der Seilscheibe ist nun erkennbar, wie die Lager und die Achse eingebaut werden müssen.