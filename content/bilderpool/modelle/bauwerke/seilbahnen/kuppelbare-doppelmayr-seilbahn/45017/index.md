---
layout: "image"
title: "Gesamtansicht (fast final) von Gondel und Klemme"
date: 2022-04-02T13:22:14+02:00
picture: "22 Gondel v_3_2 RG (7).jpg"
weight: "22"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

In der Gesamtansicht der nun fast finalen Version sehen wir oben die Klemme (3D Druck) die später noch verbessert wurde. Dazu die Klemmenhalterung, auf welcher die Gondel später in der Station auf den Schienen fahren wird. Darunter das Gehänge, welches in der späteren Version deutlich kürzer (und damit näher am Original) ausfällt. Sowie die Gondel selbst (fast final) in der eine noch einmal verbesserte Türschwenkung verankert sein wird.
Die Planung und Prototypenentwicklung rentiert sich, immerhin müssen später 35 Gondeln gebaut werden.