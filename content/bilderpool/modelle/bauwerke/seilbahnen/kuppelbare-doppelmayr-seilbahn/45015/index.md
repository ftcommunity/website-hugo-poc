---
layout: "image"
title: "Stütze Prototyp (Ansicht von oben)"
date: 2022-04-02T13:22:11+02:00
picture: "24 Stütze Prototyp 01.jpg"
weight: "24"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Auch die Stütze und Rollenbatterie kann nun getestet werden. Die Rollenbatterie wird später fast komplett aus Alu-Profilen gebaut. Ihre Beweglichkeit kann hier schon gesehen werden. AUch die Größenverhältnisse sind ästhetisch und entsprechen dem Original.
