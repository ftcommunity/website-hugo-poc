---
layout: "image"
title: "Seilscheibe v3"
date: 2020-10-31T15:33:06+01:00
picture: "013-Seilscheibe-v3_0-01.jpg"
weight: "12"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

In der finalen Version erkennt man an der ft-Designer Datei, dass das Konzept von Michael sich durchgesetzt hat.
Es überwiegen die Vorteile, vor allem hinsichtlich der Stabilität.
Hier sind auch schon die Elemente zur Seilführung dabei und alle Teile als Alu-Elemente ausgeführt.

Aus dieser ft-Designer Datei konnten wir schließlich die Teileliste erstellen und über unsere Sponsoren bei den fischertechnik-Werken direkt bestellen.