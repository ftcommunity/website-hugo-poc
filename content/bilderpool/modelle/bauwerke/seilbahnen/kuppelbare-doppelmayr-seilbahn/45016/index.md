---
layout: "image"
title: "Simulation Gondel und Rollenbatterie"
date: 2022-04-02T13:22:13+02:00
picture: "23 Stütze und Gondel CAD.jpg"
weight: "23"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Aus dem ft-Designer lässt sich gut erkennen, wie GOndel (ohne 3D Druck-Klemme) und Rollenbatterie zusammenwirken. Die Größen- und Kräfteverhältnisse stimmen. Später werden die Gondeln in 4,60m Höhe über den Köpfen der Besucher über diese Rollenwippen fahren. Der Abstand der Rollen (=Spurweite) beträgt ca. 80cm.
