---
layout: "image"
title: "Messprotokoll"
date: 2022-04-02T13:22:21+02:00
picture: "19 Messprotokoll montiert.jpg"
weight: "19"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Messprotokoll überrascht sogar uns (positiv). [x = seitliche Abweichung | y = Exzentrizität | Maßstab: 1 Teilstrich = 1mm] In der Montage sind die Messungen übereinandergelegt. Der Offset kommt durch eine schlechte Positionierung des Target zustande und ist irrelevant.
Zu erkennen ist, dass
1. die Seilscheibe bis auf +- 1,5mm genau zentriert wurde. Eine Abweichung, die bei einem Radius von knapp 400mm doch sehr gering ist (<1%). Zu erkennen am Ausschlag der blauen Messung.
2. selbst nach einer Dauerbelastung mit über 120% der Nennlast zeigt die Seilscheibe keine Verformung: die rote Messung hat die gleiche Form wie die blaue Grundmessung.
3. Auch nachdem die Seilscheibe unter last alternierend bewegt wurde zeigt die schwarze Linie keine Verformung. Zu erwarten wäre eine “Ei-förmige” verschiebung der Teile, welche zu einer (messbaren) Exzentrizität führen würde.
4. Nachdem die Seilscheibe unter Last mit Schwingschleifer (am Seil), Bohrmaschine mit Imbusschlüssel als Exzenter malträtiert wurde und ich (ClassicMan) eine halbe Stunde auf der Last getanzt habe (siehe Video) fällt die Messung noch einmal mit einer großen Überraschung unverändert aus.
Video dazu: https://youtu.be/adP3e-Z14y4
