---
layout: "image"
title: "Gondel v1 im Seil"
date: 2020-10-31T15:32:59+01:00
picture: "006-Klemme-v2_MS-am-Seil--(7).jpg"
weight: "6"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Auch die Gondel in der ersten Version hängt mit Hilfe der Seilklemme und den 3D gedruckten Teilen sicher im Seil.

(Um die Frage nach dem Stahlseil zu beantworten: wir planen eine Seilbahn mit einer Seillänge von 70m – siebzig – und etwa 30 Gondeln mit je ca. 1kg Gewicht, bei einer Abschnittslänge von über 10m. Da bleibt bei Kunststoff kein Auge trocken.)