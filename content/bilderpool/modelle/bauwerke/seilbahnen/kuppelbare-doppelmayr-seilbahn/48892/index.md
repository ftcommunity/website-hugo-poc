---
layout: "image"
title: "Seilscheibe v2_TR mit Motoren"
date: 2020-10-31T15:33:10+01:00
picture: "010-Seilscheibe-v1_TR-mit-Motoren-(2).jpg"
weight: "10"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

In dieser Darstellung aus dem ft-Designer lässt sich erkennen, wie die Kraftaufnahme in Form von Rollenlagern und der Antrieb (nur 1 Powermotor ist dargestellt – Platz hätten 18 Stück – pro Seite) gedacht war.