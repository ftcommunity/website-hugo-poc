---
layout: "image"
title: "Verriegelung der Seilscheibe"
date: 2022-04-02T13:22:28+02:00
picture: "15 seilscheibe-bau.png"
weight: "15"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Seilscheibe ist sicherheitskritisch und darf auf keinen Fall auseinandergehen. Daher wurde sie mit vielen zusätzlichen Kleinteilen so verriegelt, dass auch bei starker Belastung und Vibration kein Baustein verschoben oder herausfallen kann. Das ist stundenlange Tüftelarbeit.