---
layout: "image"
title: "Gondel v1 CAD"
date: 2020-10-31T15:33:03+01:00
picture: "003-Gondel-Ansichten-2.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Ordentliche Ingenieure bauen ihre Modelle auch noch einmal im Computer nach, um die Bauteile zu ermitteln oder Details zu klären.

In dieser Zeichnung ist zu sehen, wie das Sei liegen wird und an welchen Stellen der Auslösemechanismus der Klemme angreifen muss:
in der linken Ansicht kann man bereits die Schienen erkennen.