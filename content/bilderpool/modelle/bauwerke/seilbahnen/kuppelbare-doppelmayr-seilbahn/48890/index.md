---
layout: "image"
title: "Seilscheibe v1_MS"
date: 2020-10-31T15:33:07+01:00
picture: "011-Seilscheibe-v3_MS-(2).jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Entwurf von Michael geht einen anderen Weg.
Die Scheibe ist asymmetrisch, hat also eine Ober- und Unterseite.
Auf der Flachen Seite ist der Zahnkranz aufgesetzt.
Die Lasten werden von der Nabe aufgenommen – hierzu sind allerdings Rollenlager in 3D-Druckteilen und einer stärkere Achse vorgesehen.

Um die Kräfte dann tatsächlich aufzunehmen wird das Rad dann aber in Alu-Profilen hergestellt.