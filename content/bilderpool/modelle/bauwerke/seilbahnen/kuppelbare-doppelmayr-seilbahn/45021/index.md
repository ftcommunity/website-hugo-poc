---
layout: "image"
title: "Teststand Seilscheibe"
date: 2022-04-02T13:22:22+02:00
picture: "18 seilscheibe-messung.png"
weight: "18"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Unter der Seilscheibe ist eine in alle Richtungen lose gelagerte Messsonde angebracht, die genau in der Spur des Seils läuft. Während der Messung wird die Seilscheibe eine ganze Umdrehung absolvieren und die seitliche und exzentrische Abweichung mit einem Stift auf dem Target (links der Flachbaustein) aufgezeichnet.
Die Tests sind: Grundmessung nach dem Zentrieren der Seilscheibe ohne belastung / Statischer Test nach Dauerbelastung von 75kg über 5 Tage / Dynamische Belastung mit 50kg und täglich mehreren Bewegungen um 90° alternierend / Vibrationsmessung nach Belastung mit 50kg und Schwingungen in unterschiedlichen Frequenzbereichen über mehrere Stunden.
Video dazu: https://youtu.be/adP3e-Z14y4
