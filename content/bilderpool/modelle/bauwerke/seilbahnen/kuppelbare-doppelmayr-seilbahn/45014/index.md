---
layout: "image"
title: "Stütze mit Rollenbatterie und Seilscheibe (Ansicht von unten)"
date: 2022-04-02T13:22:09+02:00
picture: "25 Stütze Prototyp 04.jpg"
weight: "25"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Auch hier sind die Größenverhältnisse gut zu erkennen. Durchmesser Seilscheibe = Spurweite = Abstand der Rollenbatterien = 80cm. Wir benötigen 4 Stützen (je ca. 960 Teile) und 2 Seilscheiben (je ca. 1040 Teile)