---
layout: "image"
title: "Klemmvorgang v2 Detail"
date: 2020-10-31T15:32:57+01:00
picture: "008-Klemmvorgang-v3_MS--(12).jpg"
weight: "8"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Klemmvorgang im Detail.

Die Klemme ist fast geschlossen – wenn die Gondel von links nach rechts befördert wird und in das Seil einkuppelt.
Umgekehrt von rechts nach links würde die Ankunft in der Station erfolgen und damit das Auskuppeln.