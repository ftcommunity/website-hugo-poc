---
layout: "image"
title: "Seilklemme v2.2 im Seil"
date: 2020-10-31T15:33:01+01:00
picture: "005-Klemme-v2_MS-am-Seil--(4).jpg"
weight: "5"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Dank Peter Habermehl, der zu unserem Team gestoßen ist, konnten nun die individuellen Teile, die Michael bei Version v1 noch aus Holz schnitzte professionell in 3D gedruckt werden. Das ließ uns weitere Detailverbesserungen machen.

In dieser Ansicht ist schön zu erkennen, wie die geschlossene Klemme auf dem 6mm Stahlseil hängt.

(Um die Frage nach dem Stahlseil zu beantworten: wir planen eine Seilbahn mit einer Seillänge von 70m – siebzig – und etwa 30 Gondeln mit je ca. 1kg Gewicht, bei einer Abschnittslänge von über 10m. Da bleibt bei Kunststoff kein Auge trocken.)