---
layout: "image"
title: "Klemmvorgang v2 Gesammtansicht"
date: 2020-10-31T15:32:58+01:00
picture: "007-Klemmvorgang-v3_MS--(1).jpg"
weight: "7"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

In diesem Funktionsprototypen können wir bereits zeigen, dass die Klemme in der Station zuverlässig automatisiert ein- und ausgekuppelt werden kann.

Hier steht sie links, ist ausgekuppelt.
Die Passagiere sind in der Station bereits in die Gondel eingestiegen, die Türen geschlossen. Die Gondel wird mit dem Rollenförderer (hier nicht vorhanden).

Wird die Gondel weiter nach rechts befördert (Reifenförderer) wird sie beschleunigt auf die Seilgeschwindigkeit. Gleichzeitig sorgen die schrägen Schienen im Hintergrund für das einkuppeln der Klemme. Rechts am Bild würde die Gondel dann frei im Seil hängen und aus der Station fahren.

Der Prozess kann umgekehrt werden, die Ankunft an der Station mit Auskuppeln und Bremsen geschieht dann von rechts nach links.