---
layout: "image"
title: "Seilscheibe v3 Details"
date: 2020-10-31T15:33:05+01:00
picture: "014-Seilscheibe-v3_0-03.jpg"
weight: "13"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Details aus der finalen Version der Seilscheibe v3

Die Kraftverteilung der zusätzlichen Speichen geschieht über das Prinzip der gegenseitigen Haltung. [l.o.]

Die Nabe besteht aus 6 X Alu-Element [r.o.]

Die Halter für die Lager (wir verwenden übliche Inliner-Rollenlager) [links unter der Nabe]
Und der Achshalter in der Nabe [rechts unter der Nabe als CAD Zeichnung dargestellt] werden in 3D Gedruckt oder später aus Alu gefräst.

Ein Element aus der Seilscheibe zeigt dein modularen Aufbau. Insgesamt 12 solcher Module werden zusammengesetzt (die Nabe nicht mitgerechnet, das Rad und die Achse entfallen) [unten]