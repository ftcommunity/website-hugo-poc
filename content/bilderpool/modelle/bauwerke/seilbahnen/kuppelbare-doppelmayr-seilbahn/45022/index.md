---
layout: "image"
title: "Seilscheibe komplett"
date: 2022-04-02T13:22:24+02:00
picture: "17 seilscheibe-komplett.png"
weight: "17"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Seilscheibe ist nun fertig und steht auf dem Teststand. Unter ihr ist bereits unsere Messeinrichtung angebracht mit der sie auf Stabilität geprüft wird. Später wird sie ein 6mm Stahlseil tragen müssen, das mit einer statischen Last von 500n (=50kg) belastet wird. Dazu Schwingungen und Lastwechsel, sowie die Drehmomente. Und das alles nonstop über viele Monate rund um die Uhr.