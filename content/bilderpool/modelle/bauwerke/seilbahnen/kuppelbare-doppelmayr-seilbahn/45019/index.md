---
layout: "image"
title: "Seilscheibe und Gondel"
date: 2022-04-02T13:22:19+02:00
picture: "20 Seilscheibe und Gondel 02.jpg"
weight: "20"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Auch wenn die Gondel noch als Prototyp neben der fertigen Seilscheibe (Station Spinelli) besteht, die Größenverhältnisse lassen sich gut erkennen.
Unser Modell wird die 10-MGD nach Doppelmayr/Garaventa im Maßstab 1:10 nachbilden.
(Die Seilscheibe wird in der Station später natürlich liegend angeordnet sein.)