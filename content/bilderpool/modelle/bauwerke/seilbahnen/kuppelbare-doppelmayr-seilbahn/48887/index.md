---
layout: "image"
title: "Gondel v1"
date: 2020-10-31T15:33:04+01:00
picture: "002-Gondel-v1_MS--(2).jpg"
weight: "2"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Auch der erste Prototyp der Gondel wurde von Michael erschaffen.
An ihr befindet sich oben die Klemme (v1) und Schwenktüren.

Mit diesem Modell konnten wir den Maßstab und damit die ungefähre Größe der Anlage abschätzen. Alle weiteren Entwicklungen werden nun auf diesen Maßstab angepasst.

Wir werden etwa im Maßstab 1:10 bauen.
Damit ergibt sich ein Seildurchmesser von 6mm und ein Durchmesser der Seilscheibe von ca. 60-80cm, eine Gondelgröße von ca. 25-30cm