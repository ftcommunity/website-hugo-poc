---
layout: "image"
title: "Seilklemme v2.2"
date: 2020-10-31T15:33:02+01:00
picture: "004-Klemme-v2_2_TR.jpg"
weight: "4"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Nachdem wir die Seilklemme noch einmal konstruieren mussten, um aus ihr eine Totpunktklemme zu machen (war Anforderung des Projektes) entstand diese Konstruktion.
Mit Hilfe des ft-Designers haben wir nicht nur alle Teile zusammengesetzt und daraus eine Bauanleitung erstellt, sondern konnten nun auch eine Preisberechnung starten.
Diese Version ist aktuell die finale Version der Klemme – die ft-Designer Datei zeigt nur die Teile aus fischertechnik, wobei der BS15 mit Loch (schwarz) noch ein Platzhalter für ein leicht gemoddetes Teil.

Die Eigenbauten der eigentlichen Klemme und Federbeine sind natürlich nicht abbildbar.