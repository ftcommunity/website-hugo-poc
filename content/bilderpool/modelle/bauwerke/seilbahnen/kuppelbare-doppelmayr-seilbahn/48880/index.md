---
layout: "image"
title: "Seilscheibe v2_TR"
date: 2020-10-31T15:32:56+01:00
picture: "009-Seilscheibe-v2_TR--(2).jpg"
weight: "9"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Ein weiteres Kernelement der Seilbahn ist die Seilscheibe, das „große Rad“.
Es muss extreme Belastungen aufnehmen, den kompletten Zug zum Spannen des Seils und gleichzeitig eine große Reibung auf das Seil ausüben, damit der Antrieb nicht durchrutscht.

Michael und Tilo entwarfen unabhängig voneinander 2 verschiedene Versionen.

Bei dieser Lösung (Tilo) wurde eine Stabilisierung ähnlich eines Fahrrad-Speichenrades verwendet.
Es ergeben sich somit 3 verschiedene Speichen:
12 „V“ Speichen (erkennbar am gelben Statik 7,5° Stück)
12 „I“ Speichen, die gerade in die Nabe münden

Beiden Entwürfen gemeinsam ist, dass 1 Zahnkranz (hier in grauen BS30 angedeutet) aufsitzt.
Die Idee war, damit weitere Kräfte aufzunehmen und gleichzeitig den Antrieb anzubauen. In diesem Entwurf waren sogar 2 Zahnkränze vorgesehen, um die Kräfte symmetrisch aufzunehmen.