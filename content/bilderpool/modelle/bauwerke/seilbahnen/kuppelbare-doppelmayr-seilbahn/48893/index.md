---
layout: "image"
title: "Seilklemme v1"
date: 2020-10-31T15:33:11+01:00
picture: "001Gondelv1_MS11.jpg"
weight: "1"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Website-Team"
license: "unknown"
---

Alles beginnt mit der Seilklemme.
Jenes Stück wird später die Gondel an dem Trag- und Förderseil halten müssen.

Nach langem tüfteln entschlossen wir uns, einige Teile, die wir nicht mit fischertechnik zusammenbauen können aus anderem Material zu fertigen.

Michael hatte die Lösung erarbeitet: aus Holz fertigte er den ersten funktionierenden Prototypen der Seilklemme.
Sie ist NICHT als Totpunktklemme ausgeführt, sondern wird nur beim Niederdrücken geöffnet.
Auf den weißen Rädern werden die Gondeln später in der Station auf Schienen laufen.

Generell enthält dieser Prototyp schon alles, was für den Betrieb nötig wäre: einen Laufschuh, mit dem die Gondel später in der Station befördert wird.
Und die Verbindung zur Gondel (unten).
