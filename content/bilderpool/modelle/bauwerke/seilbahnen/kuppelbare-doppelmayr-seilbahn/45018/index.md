---
layout: "image"
title: "Vergleich der Gondeln / Prototypen"
date: 2022-04-02T13:22:16+02:00
picture: "21 Gondelvergleich.png"
weight: "21"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

(o.l.) Der erste Entwurf (MIROSE) enthält schon alles was nötig ist und hat sogar funktionierende Schwenktüren. Sie ist aber zu schwer und entspricht nicht dem Originalmaß (das wir damals noch nicht kannten).
(o.M.) dennoch ist die Gondel so dimensioniert, dass sie die Maximalsteigung und Sicherheitsabstände einhält.
(o.r) Auf der Website von Doppelmayr/Garaventa kann man die Gondel des Typ Omega-V konfigurieren und so auch Details erkennen. Etwa so werden diese auch auf der BUGA23 in Mannheim fahren.
(u.l.) die Weiterentwicklung (ClassicMan) entspricht dem Original bis auf wenige Millimeter und hat eine deutlich verbesserte Türschwenkung (MIROSE) - hier mit Pappe angedeutet.
(u.M.) nachdem wir von CWA, dem Gondelhersteller Originalunterlagen erhalten haben, war es möglich die genauen Maße zu entnehmen und auf Details zu achten. (Im Bild die gleiche Version “OMEGA-V” wie o.r.)
(u.r.) eine Zwischenstufe unserer Entwicklung. Noch einmal abgespeckt und besser am Original - aber noch nicht final …