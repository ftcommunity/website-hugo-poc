---
layout: "image"
title: "Detail der Verriegelung"
date: 2022-04-02T13:22:26+02:00
picture: "16 seilscheibe-detail.png"
weight: "16"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "ClassicMan"
schlagworte: ["Seilbahn", "Doppelmayr", "Garaventa", "Gondel", "Großprojekt", "Einseilumlaufbahn", "Skilift", "BUGA", "Mannheim", "Sinsheim", "Bundesgartenschau"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Seilscheibe, fertig verriegelt. Nun kann die Last auf der Lauffläche aufgenommen werden - und nichts bewegt sich.