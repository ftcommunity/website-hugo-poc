---
layout: "image"
title: "Stütze mit 8 Rollen"
date: "2011-09-27T23:37:33"
picture: "Sessellift_Sttze_8_Rollen_11.jpg"
weight: "37"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/32977
- /detailsb0a1.html
imported:
- "2019"
_4images_image_id: "32977"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:37:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32977 -->
