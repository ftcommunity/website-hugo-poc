---
layout: "image"
title: "Stütze mit 4 Rollen"
date: "2011-09-27T23:24:31"
picture: "Sessellift_Sttze_4_Rollen_07.jpg"
weight: "20"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/32960
- /details9287.html
imported:
- "2019"
_4images_image_id: "32960"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32960 -->
