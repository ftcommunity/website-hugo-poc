---
layout: "image"
title: "von hinten"
date: "2011-09-27T23:24:31"
picture: "Sessellift_Sessel_04.jpg"
weight: "4"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/32944
- /detailsfa3b.html
imported:
- "2019"
_4images_image_id: "32944"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32944 -->
