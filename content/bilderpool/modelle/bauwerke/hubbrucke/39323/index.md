---
layout: "image"
title: "Hebebrucke 8"
date: "2014-08-29T20:39:02"
picture: "Hebebrucke_8.jpg"
weight: "8"
konstrukteure: 
- "J. Steeghs"
fotografen:
- "J. Steeghs"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- /php/details/39323
- /detailsc4c1.html
imported:
- "2019"
_4images_image_id: "39323"
_4images_cat_id: "2945"
_4images_user_id: "1295"
_4images_image_date: "2014-08-29T20:39:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39323 -->
