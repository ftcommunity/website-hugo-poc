---
layout: "image"
title: "Kaiser Wilhelm Brug"
date: "2012-05-06T22:24:00"
picture: "4_2.jpg"
weight: "7"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- /php/details/34905
- /details6b84.html
imported:
- "2019"
_4images_image_id: "34905"
_4images_cat_id: "2583"
_4images_user_id: "1295"
_4images_image_date: "2012-05-06T22:24:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34905 -->
