---
layout: "image"
title: "Turm 6"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm06.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27548
- /details9ec6.html
imported:
- "2019"
_4images_image_id: "27548"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27548 -->
Nochmals eine Gesamtansicht.