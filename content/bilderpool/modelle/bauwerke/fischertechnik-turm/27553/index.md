---
layout: "image"
title: "Turm 11"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm11.jpg"
weight: "11"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27553
- /details5de3.html
imported:
- "2019"
_4images_image_id: "27553"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27553 -->
Am nächsten Tag habe ich ihn nochmal aufgebaut, Noch ein halber Meter höher. Allerdings heut´ ohne Karussel. 
Ganz oben, leider nicht gut zu erkennen sitzt ein FT-Männchen. Nach einer weile ist er runtergefallen, und er hatte keinen Fallschirm dabei... Der arme:-(
Nach rechts geht eine Seilbahn ab. 
Dieses Bild ist aus dem Haus gemacht, also von der anderen Seite als die anderen