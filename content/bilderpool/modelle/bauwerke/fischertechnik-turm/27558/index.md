---
layout: "image"
title: "Turm 16"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm16.jpg"
weight: "16"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27558
- /detailsbf21.html
imported:
- "2019"
_4images_image_id: "27558"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27558 -->
Das Seil für die Seilbahn ging von hier durch den Turm hoch.