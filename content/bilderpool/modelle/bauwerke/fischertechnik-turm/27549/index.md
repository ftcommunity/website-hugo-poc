---
layout: "image"
title: "Turm 7"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm07.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27549
- /details8c51.html
imported:
- "2019"
_4images_image_id: "27549"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27549 -->
Der obere Teil des Turmes. Ist aber nicht höher als unser Haus :-) hab halt schräg von unten fotografiert, bin eben nicht fast 4 Meter hoch:-)