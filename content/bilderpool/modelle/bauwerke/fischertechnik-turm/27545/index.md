---
layout: "image"
title: "Turm 3"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm03.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27545
- /details1cf5.html
imported:
- "2019"
_4images_image_id: "27545"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27545 -->
Hier die Befestigung der Abspannungen. Die Streben sind erstaunlich stabil, sie haben sich kaum gebogen