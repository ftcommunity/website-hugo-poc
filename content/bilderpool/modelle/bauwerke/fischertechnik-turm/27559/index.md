---
layout: "image"
title: "Turm 17"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm17.jpg"
weight: "17"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27559
- /details763c-2.html
imported:
- "2019"
_4images_image_id: "27559"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27559 -->
Die obere Abspannung. Beim zweiten Aufbau habe ich nähmlich auf zwei Ebenen abgespannt weil ich ihn ja noch erhöt hatte.