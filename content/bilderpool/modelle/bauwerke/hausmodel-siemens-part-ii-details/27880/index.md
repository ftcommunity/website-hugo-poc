---
layout: "image"
title: "haussiemens07.jpg"
date: "2010-08-22T13:21:08"
picture: "haussiemens07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27880
- /detailsd190-2.html
imported:
- "2019"
_4images_image_id: "27880"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27880 -->
Hauseingang mit Licht