---
layout: "image"
title: "haussiemens17.jpg"
date: "2010-08-22T13:21:08"
picture: "haussiemens17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/27890
- /detailsbd1e-2.html
imported:
- "2019"
_4images_image_id: "27890"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:08"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27890 -->
Haustuer mit Magnetalarm