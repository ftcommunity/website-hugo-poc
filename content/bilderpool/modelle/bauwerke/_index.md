---
layout: "overview"
title: "Bauwerke"
date: 2020-02-22T08:37:39+01:00
legacy_id:
- /php/categories/656
- /categoriesafb3.html
- /categoriesb7cf.html
- /categories96dd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=656 --> 
Gebäude, Brücken, Türme;  kurz: alles was nicht schwimmt, fährt oder fliegt. Krane befinden sich aber unter "Baumaschinen".
