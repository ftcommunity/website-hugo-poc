---
layout: "image"
title: "Detail Torantrieb mit Druckluft"
date: "2008-07-15T22:35:42"
picture: "0007_Detail_Torantrieb_mit_Druckluft.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14890
- /details3f38-2.html
imported:
- "2019"
_4images_image_id: "14890"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14890 -->
Eigentlich mehr aus der Not heraus, weil ich - trotz Erweiterungsmodul - nicht genügend Ausgänge hatte, mußte ich einen Teil der Torantriebe mit Druckluft realisieren. Doch es erwies sich fast als der bessere Antrieb. Denn die Motoren lassen sich mit dem Intelligent Interface nicht in der Drehzahl regeln. Ich muß hier den Motor takten. D.h. er geht ganz kurz an (0,01 Sekunden - also 10 mSek Millisekunden) und dann 0,1 Sekunden aus.
