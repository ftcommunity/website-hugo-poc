---
layout: "image"
title: "Detail Torantrieb mit Motor"
date: "2008-07-15T22:35:42"
picture: "0008_Detail_Tor-Antrieb_mit_Motor.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14891
- /detailsdad0.html
imported:
- "2019"
_4images_image_id: "14891"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14891 -->
Das Zahnrad wirkt über eine Stange auf eine Seiltrommel. Diese ist auf einen normalen Stein geschoben. Dadurch kann ich die reine Drehbewegung in eine Klapp-Bewegung überführen.
