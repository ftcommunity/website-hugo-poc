---
layout: "image"
title: "Detail Wasser"
date: "2008-07-15T22:35:42"
picture: "0014_Detail_Wasser.jpg"
weight: "10"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14895
- /details5446.html
imported:
- "2019"
_4images_image_id: "14895"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14895 -->
