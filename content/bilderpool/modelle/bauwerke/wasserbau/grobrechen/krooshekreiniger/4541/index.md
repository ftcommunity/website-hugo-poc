---
layout: "image"
title: "krooshekreiniger"
date: "2005-08-12T13:08:58"
picture: "Fischertechnik-Krooshekreiniger_in_bedrijf_003.jpg"
weight: "22"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4541
- /detailsf4bc.html
imported:
- "2019"
_4images_image_id: "4541"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-08-12T13:08:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4541 -->
