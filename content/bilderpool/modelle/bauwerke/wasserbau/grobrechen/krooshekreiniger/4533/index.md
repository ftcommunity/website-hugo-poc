---
layout: "image"
title: "krooshekreiniger"
date: "2005-07-30T14:12:03"
picture: "Fischertechnik-krooshekreiniger_008.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4533
- /details64d2-2.html
imported:
- "2019"
_4images_image_id: "4533"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-07-30T14:12:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4533 -->
