---
layout: "image"
title: "Segement-Tubing drehen"
date: "2017-08-22T19:52:01"
picture: "tbm66.jpg"
weight: "66"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46189
- /detailsd5c3.html
imported:
- "2019"
_4images_image_id: "46189"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46189 -->
