---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield   Peter Damen Poederoyen NL"
date: "2017-08-22T19:52:01"
picture: "tbm48.jpg"
weight: "48"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46171
- /details3373.html
imported:
- "2019"
_4images_image_id: "46171"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46171 -->
Hinten

Interessante Weblinks :

https://www.youtube.com/watch?v=kyXQfxsrymg&t=3s&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ

https://youtu.be/6EKA2yZ_aic
