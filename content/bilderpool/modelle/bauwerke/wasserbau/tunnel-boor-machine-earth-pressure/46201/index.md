---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield   Peter Damen Poederoyen NL"
date: "2017-08-22T19:52:01"
picture: "tbm78.jpg"
weight: "78"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46201
- /detailseff6-2.html
imported:
- "2019"
_4images_image_id: "46201"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46201 -->
Ubersicht
