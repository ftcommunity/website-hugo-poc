---
layout: "image"
title: "Reed-wissel-contact als alternatief voor End-Schalter !"
date: "2017-08-22T19:52:01"
picture: "tbm50.jpg"
weight: "50"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46173
- /details4b25-2.html
imported:
- "2019"
_4images_image_id: "46173"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46173 -->
Interessantes Alternativ für End-Schalter !
