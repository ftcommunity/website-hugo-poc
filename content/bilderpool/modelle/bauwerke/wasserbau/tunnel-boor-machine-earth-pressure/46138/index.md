---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:30"
picture: "tbm15.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46138
- /details7307.html
imported:
- "2019"
_4images_image_id: "46138"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46138 -->
Förderrate und Vortriebsgeschwindigkeit regeln Druckverhältnisse
Eine Förderschnecke transportiert das abgebaute Material vom Boden der Abbaukammer auf ein Förderband. Dabei sichert das Zusammenspiel der Förderrate der Schnecke und der Vortriebs­geschwindigkeit die präzise Steuerung des Stützdrucks des Erdbreis. Mittels Erddrucksensoren in der Abbaukammer wird der Gleichgewichtszustand kontinuierlich überwacht. Somit können alle Vortriebsparameter auch bei wechselnden geologischen Bedingungen vom Maschinenfahrer optimal aufeinander abgestimmt werden. Das ermöglicht hohe Vortriebsgeschwindigkeiten und minimiert die Gefahr von Hebungen oder Setzungen an der Oberfläche.
