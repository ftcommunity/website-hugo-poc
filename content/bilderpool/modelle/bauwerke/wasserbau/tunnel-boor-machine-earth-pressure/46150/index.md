---
layout: "image"
title: "Anfang der Bau"
date: "2017-08-22T19:52:01"
picture: "tbm27.jpg"
weight: "27"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46150
- /detailsde3f.html
imported:
- "2019"
_4images_image_id: "46150"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46150 -->
