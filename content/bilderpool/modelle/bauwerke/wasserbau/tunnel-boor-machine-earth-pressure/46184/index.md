---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield"
date: "2017-08-22T19:52:01"
picture: "tbm61.jpg"
weight: "61"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46184
- /details61b8.html
imported:
- "2019"
_4images_image_id: "46184"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46184 -->
Youtube -link :

https://www.youtube.com/watch?v=6EKA2yZ_aic&t=62s
