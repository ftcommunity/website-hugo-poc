---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:30"
picture: "tbm17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46140
- /details8da7-2.html
imported:
- "2019"
_4images_image_id: "46140"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46140 -->
Weblink :

https://www.youtube.com/watch?v=kyXQfxsrymg&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ&t=9s
