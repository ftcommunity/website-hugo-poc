---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:15"
picture: "tbm05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46128
- /detailsaef2.html
imported:
- "2019"
_4images_image_id: "46128"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46128 -->
Ubersicht
