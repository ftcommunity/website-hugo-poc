---
layout: "image"
title: "Erektor Details + Tubing (segment)"
date: "2017-08-22T19:52:01"
picture: "tbm74.jpg"
weight: "74"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46197
- /details9044.html
imported:
- "2019"
_4images_image_id: "46197"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46197 -->
Förderrate und Vortriebsgeschwindigkeit regeln Druckverhältnisse
Eine Förderschnecke transportiert das abgebaute Material vom Boden der Abbaukammer auf ein Förderband. Dabei sichert das Zusammenspiel der Förderrate der Schnecke und der Vortriebs­geschwindigkeit die präzise Steuerung des Stützdrucks des Erdbreis. Mittels Erddrucksensoren in der Abbaukammer wird der Gleichgewichtszustand kontinuierlich überwacht. Somit können alle Vortriebsparameter auch bei wechselnden geologischen Bedingungen vom Maschinenfahrer optimal aufeinander abgestimmt werden. Das ermöglicht hohe Vortriebsgeschwindigkeiten und minimiert die Gefahr von Hebungen oder Setzungen an der Oberfläche.
