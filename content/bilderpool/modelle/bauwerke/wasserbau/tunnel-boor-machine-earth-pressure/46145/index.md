---
layout: "image"
title: "Anfang der Bau  + Erektor"
date: "2017-08-22T19:52:01"
picture: "tbm22.jpg"
weight: "22"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46145
- /detailsd777.html
imported:
- "2019"
_4images_image_id: "46145"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46145 -->
