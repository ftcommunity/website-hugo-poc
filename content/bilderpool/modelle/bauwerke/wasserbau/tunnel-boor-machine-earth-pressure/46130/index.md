---
layout: "image"
title: "Segment-Tubing-Transport TBM"
date: "2017-08-22T19:51:15"
picture: "tbm07.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/46130
- /details7116.html
imported:
- "2019"
_4images_image_id: "46130"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46130 -->
