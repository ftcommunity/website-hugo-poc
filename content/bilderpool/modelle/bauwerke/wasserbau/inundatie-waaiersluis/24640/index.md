---
layout: "image"
title: "Inundatie-Waaiersluis"
date: "2009-07-19T17:35:01"
picture: "FT-Inundatie-Waaiersluis_026_2.jpg"
weight: "46"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24640
- /details8e9e.html
imported:
- "2019"
_4images_image_id: "24640"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24640 -->
Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.

Ter reductie van de belasting op de taats- en tandheugel-constructie, is bij stalen (geklinknagelde of gelaste) waaierdeuren soms een luchtkist aanwezig.
