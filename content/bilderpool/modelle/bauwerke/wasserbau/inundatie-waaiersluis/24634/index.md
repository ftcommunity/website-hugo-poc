---
layout: "image"
title: "Inundatie-Waaiersluis"
date: "2009-07-19T17:27:01"
picture: "Papsluis__Afgedamde_Maastocht_009.jpg"
weight: "40"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24634
- /detailsb962.html
imported:
- "2019"
_4images_image_id: "24634"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:27:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24634 -->
Een (inundatie-) waaiersluis is een speciale sluis, die middels omloopriolen tegen de waterdruk in geopend en gesloten kan worden. Dit type sluis is uitgevonden door Jan Blanken (NL 1755-1838)
