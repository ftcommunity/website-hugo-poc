---
layout: "image"
title: "details"
date: "2009-07-24T17:57:04"
picture: "FT-Inundatie-Waaiersluis-rioolschuifwindwerk-detail.jpg"
weight: "54"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24675
- /detailsb00a.html
imported:
- "2019"
_4images_image_id: "24675"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-24T17:57:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24675 -->
Das kleine Differential Planetenrad 10 (31412) habe ich erst aufbebohrt nach 5mm. 
Dann klemmt dieses über ein 5mm Rohr (etwa 17mm lang) mit M4-innen-Gewinde. 

Beim drehen der Kurbel dreht sich dass 5mm Rohrchen mit M4-innen-Gewinde. 
Die Gewindestange-M4 dreht nicht, sondern bewegt nur nach oben oder unten, abhänglich der Drehrichtung.
