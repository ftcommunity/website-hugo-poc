---
layout: "image"
title: "Inundatie-Waaiersluis"
date: "2009-07-19T17:24:00"
picture: "Papsluis_4.jpg"
weight: "36"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24630
- /details55a5.html
imported:
- "2019"
_4images_image_id: "24630"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:24:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24630 -->
