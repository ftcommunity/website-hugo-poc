---
layout: "comment"
hidden: true
title: "9616"
date: "2009-07-22T09:11:44"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Frederik, 

Die Mechanik in den beiden Kästen mit der Kurbel gibt es hier: 
http://www.ftcommunity.de/details.php?image_id=24604 

Der Trick ist ein 5mm (aussen) Rohr mit M4-innen und ein Differential Planetenrad 10 (31412) aufbebohrt nach 5mm. 


Gruss, 

Peter Damen 
Poederoyen NL