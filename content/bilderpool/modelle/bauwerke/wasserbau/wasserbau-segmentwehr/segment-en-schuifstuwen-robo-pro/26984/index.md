---
layout: "image"
title: "Segment- en Schuifstuwen, naast Robo-Pro-niveau-sturing nu ook met handbediening."
date: "2010-04-25T19:48:52"
picture: "SchuifSegment-Stuwen-AutomHanbediening_008.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26984
- /details0cc3.html
imported:
- "2019"
_4images_image_id: "26984"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2010-04-25T19:48:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26984 -->
Handbedienung macht die Kinder Antonie & Annemieke viel mehr Spass !
