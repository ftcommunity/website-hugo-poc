---
layout: "image"
title: "Schuif- en Segment- Stuwen 2010 met verbeterde aandrijving."
date: "2010-05-24T22:39:22"
picture: "2010-4-Stuwen-HandAutomatisch-NiveauSpoelregeling_015.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/27300
- /details02e8-2.html
imported:
- "2019"
_4images_image_id: "27300"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2010-05-24T22:39:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27300 -->
Schuif- en Segment- Stuwen 2010 met verbeterde aandrijving.
Niveau- en Spoelregeling 4 stuwen middels Robo-Pro, of naar keuze handbediening.
