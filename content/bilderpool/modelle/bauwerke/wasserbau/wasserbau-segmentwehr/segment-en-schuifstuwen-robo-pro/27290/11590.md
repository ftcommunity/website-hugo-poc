---
layout: "comment"
hidden: true
title: "11590"
date: "2010-05-26T15:56:10"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Antrieb Segmentwehre mit Gleichstrom-Getriebemotor CB65
Pollin Bestellnummer: 310 334    Preis: 7,95 € 
- Betriebsspannung 6...24 V-
- Stromaufnahme 0,1 A im Leerlauf
- Drehzahl bei 6 V: 3 U/min, 9 V: 4,9 U/min, 12 V: 6,8 U/min, 24 V: 14,6 U/min
Motormaße ohne Welle (Lxø): 70x33 mm, abgeflachte 5 mm-Achse mit 10 mm Länge.