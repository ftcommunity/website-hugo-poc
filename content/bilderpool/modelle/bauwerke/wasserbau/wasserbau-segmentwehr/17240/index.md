---
layout: "image"
title: "Neubau Schiebe- und Segmentwehre für dem Sommer"
date: "2009-02-01T19:35:38"
picture: "SegmentSchuifstuwen-2009_001.jpg"
weight: "31"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/17240
- /detailsb326-2.html
imported:
- "2019"
_4images_image_id: "17240"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17240 -->
Neubau Schiebe- und Segmentwehre für dem Sommer
