---
layout: "comment"
hidden: true
title: "8915"
date: "2009-04-06T08:35:31"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Ich versuche mit einer Pot-Meter und RoboPro in 5 Wasserbecken eine stabile verstelbare Wasserstand zu erreichen.

Mit kurze Motorbetriebzeiten  ist eine stabile Wasserstand möglich:  [+/-1 cm] 

Statt die Impuls-schalter nutze ich 0,02-1,0 sec für eine kleine Wehr-Motor-Bewegungen. 
Jede 0,3 - 0,5 Sekunde wirt die Wasserstand mit einem US-Sensor (oder Potmeter) gemessen.
Wenn es eine niedrige Wasserstand gibt als mit dem Pot-meter eingefuhrt und berechnet, bewegt die Wehre ein wenig nach Unten damit eine hohere Wasserstand erreicht wird. 
----------------------------------


Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen

Ik kan elke stuwen onafhankelijk bedienen, waarbij in elke stuwpand (=goot) de waterstand middels de instelling van een pot-meter geregeld kan worden. Het werkt prachtig en m'n zoon Antonie (9) en dochter Annemieke spelen er ook graag mee !...........

Niveau + Spoel -regeling voor 5 stuwen:
-Schuif-stuw-1: Ultrasound spanningsuitgang A1,
-Segment-stuw-2: US-FT-D1,
-Segment-stuw-3: US-FT-D2,
-Schuif-stuw-4: Potmeter-vlotter weerstand: AY

-Klep-stuw-5: Ultrasound spanningsuitgang A21

Zoals je weet werk ik zelf bij Waterschap Rivierenland en heb in het verleden ook veel met stuwen te maken gehad. In de praktijk worden 1 of 2 x per jaar de grote A-watergangen "geveegd" met een maaiboot. Alle waterplanten e.d. worden dan onder water afgemaaid. Vervolgens worden de stuwen open gezet en vervolgens snel volledig geopend om het maaisel af te voeren. Een en ander zoals je ook een WC doortrekt......

Voor dichtslibbende havens wordt soms hetzelfde principe toegepast. Water vasthouden om er vervolgens met voldoende stroomsnelheid het afgezette slib weg te spoelen. Vroeger gebeurde dit bijvoorbeeld middels een spuikom (nu jachthaven) ook in Willemstad.

In Frankrijk bouwt men momenteel driftig aan een getijdendam met segmentstuwen om de aanslibbing rond Mont-Saint-Micheal in de toekomst weer te niet te doen.