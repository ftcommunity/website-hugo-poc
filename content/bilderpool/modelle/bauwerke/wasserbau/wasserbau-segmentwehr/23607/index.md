---
layout: "image"
title: "Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen"
date: "2009-04-05T22:44:40"
picture: "Schuif-_Segment-_en_Klepstuwen_-Fischertechnik_006.jpg"
weight: "69"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23607
- /details7ade-2.html
imported:
- "2019"
_4images_image_id: "23607"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-04-05T22:44:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23607 -->
