---
layout: "comment"
hidden: true
title: "6753"
date: "2008-07-21T12:40:27"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Erinnert mich ein bisschen an das Emssperrwerk hier oben bei [Gandersum](http://www.niedersachsen.de/master/C6533317_L20_D0_I5231158_h1.html) bzw an die Schleuse in Henrichenburg. Dort waren auch solche Segmentwehre bzw. Segmenttore im Einsatz. Wo mündet denn der Bach/Fluss? Ist das ein richtiger Bach oder ein künstlicher wo unten ne Pumpe das Wasser wieder nach oben pumpt? Schön gebaut, gefällt mir sehr.