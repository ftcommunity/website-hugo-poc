---
layout: "image"
title: "Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen"
date: "2009-04-05T22:44:40"
picture: "Schuif-_Segment-_en_Klepstuwen_-Fischertechnik_015.jpg"
weight: "77"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23615
- /detailscd74.html
imported:
- "2019"
_4images_image_id: "23615"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-04-05T22:44:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23615 -->
