---
layout: "image"
title: "Detail Wasserrad und Königswelle in der Mitte"
date: "2014-10-20T21:59:39"
picture: "Detail_Wasserrad_und_Knigswelle_in_der_Mitte.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39726
- /detailsa43e-2.html
imported:
- "2019"
_4images_image_id: "39726"
_4images_cat_id: "2980"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39726 -->
Die Königswelle ist in Wirklichkeit tatsächlich ein Baumstamm, der sich in der Mitte dreht.
Die Zahnräder sind in Echt aus Holz und greifen erstaunlich laufruhig ineinander.

Mich haben die Windmühlen im Niederländischen Kinderdijk inspiriert.
Diese kann man auch bis ganz oben (innen) besteigen.
Und wenn man Glück hat, drehen sie sich sogar.
Dazu brauchen sie gar nicht viel Wind...
