---
layout: "image"
title: "Windmühle"
date: "2014-10-20T21:59:39"
picture: "Windmhle.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39724
- /detailse50e.html
imported:
- "2019"
_4images_image_id: "39724"
_4images_cat_id: "2980"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39724 -->
Hier gibt es das Video:
https://www.youtube.com/watch?v=LS1DbwHLFBo&list=UUN792LdoJAkQi9-rPmfzWIQ
