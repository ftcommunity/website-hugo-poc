---
layout: "overview"
title: "Windwatermolen + automatische instelbare peil-beheersing"
date: 2020-02-22T08:38:41+01:00
legacy_id:
- /php/categories/3090
- /categories91c2.html
- /categories36ff.html
- /categories418e.html
- /categoriesad91.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3090 --> 
De toepassing van windmolens voor de aandrijving van pompen is een economisch aantrekkelijk en milieuvriendelijke oplossing voor watertransport. Dit geldt in het bijzonder voor gebieden waar elektriciteit niet voorhanden is. De windwatermolen werd in Nederland hoofdzakelijk gebruikt voor het bemalen van polders, maar tegenwoordig ook in natuurgebieden met zogeheten plas-dras situaties. De molen werkt al met windsnelheden vanaf 3 m/s  (1 Bft.). Door een eenvoudig vlottersysteem is volautomatische peilbeheersing mogelijk. Onder normale omstandigheden (met name vlak open terrein), kan deze molen een gebied van 20-25 ha. verzorgen.

De werking
Aan de werking ervan komt geen mens te pas. 
Er drijft een vlotter op het water, die via een stang de stand van beide vanen instelt. Van de hoofdvaan, die recht achter de wieken zit en van de hulp- of bijvaan, die opzij staat. 
Als het water hoog in de sloot staat, staat de hoofdvaan verticaal en de hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 
Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.
De draaiende wieken drijven een centrifugaalpomp aan, die het water uit de sloot maalt. Als het water genoeg is gezakt, heeft de vlotter inmiddels de vanen zover gedraaid, dat de wieken uit de wind draaien en de bemaling stopt. 
De pomp biedt voldoende weerstand om bij storm het op hol slaan van de molen te voorkomen. Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.
