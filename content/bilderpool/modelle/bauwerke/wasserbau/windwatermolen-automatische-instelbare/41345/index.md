---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41345
- /detailsb9f2.html
imported:
- "2019"
_4images_image_id: "41345"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41345 -->
Er drijft een vlotter op het water, die via een stang de stand van beide vanen instelt t.b.v. een volautomatische peilbeheersing.  
De Hoofdvaan, die recht achter de wieken zit  +  de Hulp- of Bijvaan, die opzij staat. 

Als het water hoog in de sloot staat, staat de Hoofdvaan verticaal en de Hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 

Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.

De draaiende wieken drijven een centrifugaalpomp aan, die het water uit de sloot maalt naar het hogere peilvak.
Als het water genoeg is gezakt, heeft de vlotter inmiddels de vanen zover gedraaid, dat de wieken uit de wind draaien en de bemaling stopt. 

De pomp biedt voldoende weerstand om bij storm het op hol slaan van de molen te voorkomen. 

Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. 
Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.
