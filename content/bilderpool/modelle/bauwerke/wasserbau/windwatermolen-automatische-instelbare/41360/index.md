---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen16.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/41360
- /detailsba6b.html
imported:
- "2019"
_4images_image_id: "41360"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41360 -->
Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. 
Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.
