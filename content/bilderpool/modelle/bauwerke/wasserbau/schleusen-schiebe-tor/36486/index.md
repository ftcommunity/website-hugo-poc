---
layout: "image"
title: "Schleu05"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor5.jpg"
weight: "5"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/36486
- /details4764-2.html
imported:
- "2019"
_4images_image_id: "36486"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36486 -->
Bei dieser Ansicht ist die vordere Wand abgenommen und gibt somit den Blick auf das Schleusen-Tor, die Führung, den Antrieb und den rechten Taster, der die Endstellung (Tor geöffnet) signalisiert.
