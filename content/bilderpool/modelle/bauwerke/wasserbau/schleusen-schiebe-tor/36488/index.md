---
layout: "image"
title: "Schleu07"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor7.jpg"
weight: "7"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/36488
- /details73ea.html
imported:
- "2019"
_4images_image_id: "36488"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36488 -->
Das "Innenleben" des Schleusen-Tores.

