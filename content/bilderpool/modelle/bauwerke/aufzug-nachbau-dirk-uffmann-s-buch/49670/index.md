---
layout: "image"
title: "oberste Stockwerke"
date: 2023-03-27T19:01:18+02:00
picture: "Aufzug_3.jpeg"
weight: "3"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Aufzug", " Silberlinge", " Uffman Buch"]
uploadBy: "Website-Team"
license: "unknown"
---

oberste Stockwerke