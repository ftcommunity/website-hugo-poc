---
layout: "overview"
title: "Aufzug, Nachbau Dirk Uffmann's Buch"
date: 2023-03-27T19:01:14+02:00
---

Das Aufzugsmodell aus den Siebzigern hat mich schon immer begeistert. Das Buch von Dirk Uffmann war der Auslöser, mit dem Sammeln von Silberlingen zu beginnen und das Modell zu bauen. Es lässt sich wunderbar damit spielen, aber jeder Umzug von einem Ort zum anderen ist ein echtes Problem, da immer ein oder zwei Kabel lose sind...

