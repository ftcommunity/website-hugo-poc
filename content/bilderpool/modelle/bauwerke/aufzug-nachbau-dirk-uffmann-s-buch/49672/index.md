---
layout: "image"
title: "Fertig!"
date: 2023-03-27T19:01:21+02:00
picture: "Aufzug_1.jpeg"
weight: "1"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Aufzug", " Silberlinge", " Uffman Buch"]
uploadBy: "Website-Team"
license: "unknown"
---

fertiger Fahrstuhl und Uffi's Buch