---
layout: "image"
title: "CT - Auswertung"
date: "2012-01-28T15:38:06"
picture: "Bild_4.jpg"
weight: "4"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/34070
- /detailse3e1.html
imported:
- "2019"
_4images_image_id: "34070"
_4images_cat_id: "2520"
_4images_user_id: "1239"
_4images_image_date: "2012-01-28T15:38:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34070 -->
Dieses Bild ist ein Screenshot aus RoboPro. Die 16 Lampen zeigen an, wo sich das Objekt befindet und wie groß es ist. Rechts daneben wird der Fortschritt der Messung angezeigt und oben, bei welchem Messvorgang das Gerät gerade ist.