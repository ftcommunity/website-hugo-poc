---
layout: "image"
title: "Abstandsmessung mit dem I2C Sensor HMC5883L"
date: "2016-02-02T13:11:13"
picture: "Abstandsmessung_HMC588L.jpg"
weight: "6"
konstrukteure: 
- "C. Hehr"
fotografen:
- "C. Hehr"
schlagworte: ["HMC5883L", "Sensor", "Magnetfeld", "I2C"]
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/42856
- /details93d0.html
imported:
- "2019"
_4images_image_id: "42856"
_4images_cat_id: "2520"
_4images_user_id: "2374"
_4images_image_date: "2016-02-02T13:11:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42856 -->
Ein fischertechnik Magnet wurde zwischen zwei Anschläge, welche 60 bzw. 70 mm von einem Magnetfeldsensor enfernt sind per Hand bewegt. Dabei wurden das Magnetfeld anhand von dem Sensor HMC5883L per RoboPro aufgezeichnet.