---
layout: "image"
title: "CT-Modell"
date: "2012-01-28T15:38:06"
picture: "Bild_1.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/34067
- /detailsa35a.html
imported:
- "2019"
_4images_image_id: "34067"
_4images_cat_id: "2520"
_4images_user_id: "1239"
_4images_image_date: "2012-01-28T15:38:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34067 -->
Dies ist eine Messeinrichtung nach dem Vorbild eines Computertomographen.
Ein echter Computertomograph dreht sich um das Objekt in der Mitte und macht Bilder per Rhöntgenstrahlen. Er "schneidet" so ein Bild heraus 
(siehe nächstes Foto - auch die genaue Funktion)
Fügt man mehrere Bilder aneinander entsteht ein 3D-Bild des sich in der Mitte befindenden Menschen.

Mein Computertomograph arbeitet mit Lichtschranken, da Rhöntgen mit FT wohl kaum machbar ist. Er setzt die Bilder des Umrisses dann zusammen und erzeugt ein Bild. (erklärt sich auf den nächsten Bildern ->)