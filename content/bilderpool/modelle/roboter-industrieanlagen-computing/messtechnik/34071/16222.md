---
layout: "comment"
hidden: true
title: "16222"
date: "2012-01-29T13:37:32"
uploadBy:
- "majus"
license: "unknown"
imported:
- "2019"
---
Hallo,
das mit der Aufnahme versuche ich noch, nachzuliefern, denn da habe ich noch Beispiele gespeichert.

Das mit den Laserpointern wäre zwar gut, aber meine Versuche haben gezeigt, dass nichts wackeln darf, denn sonst kommen falsche Werte heraus. Wackelt der Laser leicht, dann ist der Punkt, der am Fototransistor ankommt, gleich einige Milimeter verschoben. Und dann läuft die Messung schief. (Schade eigentlich, denn man könnte viel größere Flächen scannen!)

Gruß
Majus