---
layout: "image"
title: "CT - andere Stellung"
date: "2012-01-28T15:38:06"
picture: "Bild_3.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/34069
- /detailsd573.html
imported:
- "2019"
_4images_image_id: "34069"
_4images_cat_id: "2520"
_4images_user_id: "1239"
_4images_image_date: "2012-01-28T15:38:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34069 -->
Hier wird die nächste Messung durchgeführt.