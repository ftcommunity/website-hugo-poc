---
layout: "image"
title: "Training Roboter Original"
date: "2011-05-14T22:07:37"
picture: "DSC00641.jpg"
weight: "20"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30558
- /details2777.html
imported:
- "2019"
_4images_image_id: "30558"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30558 -->
Detail of the driving
moters of the original 
model with the pulse counting devices

Details des ursprünglichen Modells
 Positionierung und moters
 Impulszählung Geräte.
