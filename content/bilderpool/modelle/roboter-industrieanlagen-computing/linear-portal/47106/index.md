---
layout: "image"
title: "Gegenlager"
date: "2018-01-15T16:48:06"
picture: "Portal2_kl.jpg"
weight: "2"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/47106
- /details2317.html
imported:
- "2019"
_4images_image_id: "47106"
_4images_cat_id: "3484"
_4images_user_id: "10"
_4images_image_date: "2018-01-15T16:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47106 -->
Die Achse selbst wird in den Gegenlagern mit einer Imbusschraube festgeklemmt. Das Lager selbst läßt sich mit etwas Nachdruck , nahezu rasterkonform, festklemmen. Es hält bombenfest und hat sich auch im Dauerbetrieb des Laufwagens nicht gelockert.
