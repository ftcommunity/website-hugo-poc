---
layout: "image"
title: "Linearführung"
date: "2018-01-15T16:48:06"
picture: "Portal1_kl.jpg"
weight: "1"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/47105
- /detailsf500.html
imported:
- "2019"
_4images_image_id: "47105"
_4images_cat_id: "3484"
_4images_user_id: "10"
_4images_image_date: "2018-01-15T16:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47105 -->
Da es seitens Fischertechnik keine vernünftige Lösung für eine reibungsarme Gleitführung gibt, habe ich einmal mit einer 8mm Präzisionswelle und einem Linearlager experimentiert. Hier habe ich eine h6 gehärtete Präzisionswelle von 8mm Durchmesser und einer Länge von 490mm genommen.
Ein seitenweicher Lauf und nicht das geringste Durchbiegen, wie bei den Standart 4mm Achsen, sind das Resultaat.
