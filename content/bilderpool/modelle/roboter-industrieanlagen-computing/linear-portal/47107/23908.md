---
layout: "comment"
hidden: true
title: "23908"
date: "2018-01-16T20:54:07"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Solche Lager findet man auch in 3D Druckern wegen der hohen Präzision und des leichten Laufverhaltens. Wer z.B. seinen Fischertechnik Drucker tunen möchte, macht hiermit sicherlich nichts falsch. Aus meiner Sicht ist der Innendurchmesser von 8mm das größte Problem, die Lager ins ft System zu integrieren, da man zusätzliche Achsen, Endlager und einen Adapter Gleitlager-ft benötigt. Gibt es solche Gleitlager auch für 4mm Achsen?

Gruß,
David