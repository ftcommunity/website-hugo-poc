---
layout: "image"
title: "hauptflugel gesichert"
date: "2018-07-20T18:39:16"
picture: "transformer06.jpg"
weight: "6"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/47733
- /detailscbca.html
imported:
- "2019"
_4images_image_id: "47733"
_4images_cat_id: "3523"
_4images_user_id: "162"
_4images_image_date: "2018-07-20T18:39:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47733 -->
