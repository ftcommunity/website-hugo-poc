---
layout: "image"
title: "Heckflugel eingeklappt"
date: "2018-07-20T18:39:16"
picture: "transformer04.jpg"
weight: "4"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/47731
- /details197a.html
imported:
- "2019"
_4images_image_id: "47731"
_4images_cat_id: "3523"
_4images_user_id: "162"
_4images_image_date: "2018-07-20T18:39:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47731 -->
