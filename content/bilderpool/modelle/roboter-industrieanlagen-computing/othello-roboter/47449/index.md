---
layout: "image"
title: "Totale, rechte Seite"
date: "2018-04-17T22:07:47"
picture: "othelloroboter02.jpg"
weight: "2"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47449
- /details1587.html
imported:
- "2019"
_4images_image_id: "47449"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47449 -->
Im unteren Teil des Bildes erkennt man den Kettenantrieb des Drehtischs. Dessen Motor ist fest mit dem Aluprofil verbunden, das von zwei langen Stangen geführt wird und durch einen Schneckenantrieb für den linearen Vorschub des Spielfeldes sorgt. Der Motor des Schneckenantriebs ist ganz rechts unten im Bild teilweise zu erkennen.
Ich habe versucht einen modularen Aufbau zu verwirklichen. So ist der Wende-Mechanismus samt Antrieb an lediglich zwei Punkten an dem Aufbau auf der rechten Seite befestigt und lässt sich so einfach nach vorne hin ausbauen. Das Aluprofil, das den Drehtisch samt Antrieb trägt kann ebenfalls einfach zur Vorderseite hin entfernt werden. Auch der Spielsteinspender ist schnell ausgebaut. ... Naja, wenn die Kabel nicht wären, wär's wirklich einfach.
