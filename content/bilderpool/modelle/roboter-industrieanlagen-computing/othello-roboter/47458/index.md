---
layout: "image"
title: "Drehtisch"
date: "2018-04-17T22:07:48"
picture: "othelloroboter11.jpg"
weight: "11"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47458
- /detailsf896-2.html
imported:
- "2019"
_4images_image_id: "47458"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47458 -->
Auch hier als letztes Antriebselement eine Schnecke, die das Spiel minimiert.
