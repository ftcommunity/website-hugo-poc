---
layout: "image"
title: "Kabelsalat mit Freilaufdioden"
date: "2018-04-17T22:07:48"
picture: "othelloroboter15.jpg"
weight: "15"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47462
- /details3b25-2.html
imported:
- "2019"
_4images_image_id: "47462"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47462 -->
In den Silikonschläuchen sind zwei Freilauf-Dioden versteckt. Sie verbinden die beiden O-Ausgänge in Sperrrichtung über ein ft-Lämpchen als Last mit der Masse. Sie verhindern zuverlässig die -20V Spannungsspitzen beim Abschalten der Magneten. Die Spitzen entsprechen genau den technischen Daten der im TXT verbauten FreeScale/NXP MC33879 Leistungsbrücken. Aus dem Datenblatt:
"... Output voltage clamp, + 45 V (low-side) and - 20 V (high-side) during inductive switching ...."
Wahrscheinlich sind die Freilaufdioden überflüssig. Die 20V für ~1ms können den Magneten wohl kaum etwas anhaben. Der eine defekte Magnet, den ich austauschen musste, mit 15Ohm statt 45Ohm, ist wahrscheinlich in seinem vorherigen Leben gegrillt worden. Aber da noch der Farbsensor am O-Ausgang (zwar über den Umschalter getrennt) hängt, hab ich sie drin gelassen.
