---
layout: "image"
title: "Totale, linke Seite"
date: "2018-04-17T22:07:47"
picture: "othelloroboter01.jpg"
weight: "1"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47448
- /detailse455.html
imported:
- "2019"
_4images_image_id: "47448"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47448 -->
Rechts im Bild zu sehen ist das Spielbrett, das unter dem feststehenden Wendemechanismus (Bildmitte) positioniert wird. Die Positionierung ist mit einem linear verschiebbaren Drehtisch realisiert. Diese Art der Positionierung ermöglicht einen relativ kompakten Aufbau. Links im Vordergrund ist der Spielsteinspender zu sehen. In der rechten unteren Ecke ist eine Spielsteinablage zu erkennen. Der Spender legt jeweils einen Stein darauf ab. Der Stein wird dann entweder dem Spieler gereicht oder vom Wender aufgenommen und auf dem Spielbrett abgelegt. Die Visitenkarte zwischen Kamera und Spielbrett verhindert eine durch das knallgelbe Spielbrett verursachte Fehlkorrektur des automatischen Weißabgleichs der Kamera.
