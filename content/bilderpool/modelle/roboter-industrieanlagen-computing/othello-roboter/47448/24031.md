---
layout: "comment"
hidden: true
title: "24031"
date: "2018-04-18T17:11:17"
uploadBy:
- "hamlet"
license: "unknown"
imported:
- "2019"
---
Hi Dirk,
Dank Dir. Ich war echt aufgeregt, als ich die Idee mit den Planeten-Magneten hatte. Hab direkt mein fischertechnik aus der Ecke gezogen und es ausprobiert. Der erste Entwurf war noch klobiger und wurde durch einen kleinen xs-Motor angetrieben, funktionierte aber fast auf Anhieb. Eine große Erleichterung und Freude nach meinen vorherigen Fehlversuchen.
Beste Grüße,
    Helmut