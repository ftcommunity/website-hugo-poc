---
layout: "image"
title: "Drehtisch  Unterseite"
date: "2018-04-17T22:07:48"
picture: "othelloroboter13.jpg"
weight: "13"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47460
- /details1314.html
imported:
- "2019"
_4images_image_id: "47460"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47460 -->
Die Stützräder sind inzwischen ohne Funktion, da das Aluprofil nun von einer kleinen Kufe, die in der unteren Nut des Profils läuft, getragen und auch seitlich geführt wird (etwas weiter rechts außerhalb des Bildausschnitts).
