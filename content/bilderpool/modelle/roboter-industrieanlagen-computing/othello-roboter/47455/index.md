---
layout: "image"
title: "Spielsteinwender, Detail Breitseite"
date: "2018-04-17T22:07:48"
picture: "othelloroboter08.jpg"
weight: "8"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47455
- /details1745.html
imported:
- "2019"
_4images_image_id: "47455"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47455 -->
Huch, die Schnecke steht aber ein bisschen schief.
Die schwarzen 15er Lagerhülsen haben viel weniger Spiel als die Bohrung in den roten 15er Steine und halten die tragende Achse des Wenders besser in der Waagrechten.
Ich bin dazu übergegangen eine blockierende Schnecke so nah wie möglich am Aktor in die Antriebe zu verbauen. Das minimiert das mechanische Spiel des Aktors. Das Spiel vor der Schnecke kann man schön in der SW kompensieren.   
Die Kabel hab ich einfach mit Zahnstocher Stummeln an den Magneten fixiert da die ft-Steckerchen zu groß sind.
