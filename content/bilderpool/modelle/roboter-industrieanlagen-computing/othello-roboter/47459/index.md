---
layout: "image"
title: "Drehtisch Antrieb"
date: "2018-04-17T22:07:48"
picture: "othelloroboter12.jpg"
weight: "12"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47459
- /details7427.html
imported:
- "2019"
_4images_image_id: "47459"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47459 -->
Der Kettenspanner hält die Kette schön unterhalb des Drehtisches.
Hier sind die alten Kettenelemente ohne Rast-Nupsis echt von Vorteil, auch vorn am Drehteller ist recht eng.
