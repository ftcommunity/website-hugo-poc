---
layout: "image"
title: "Die linke Seite, etwas näher"
date: "2018-04-17T22:07:48"
picture: "othelloroboter04.jpg"
weight: "4"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/47451
- /detailscad5.html
imported:
- "2019"
_4images_image_id: "47451"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47451 -->
Diesmal ohne Spielbrett. Der Spielbrettträger ist nicht ganz ausgefahren. In der äußersten Position ergänzt die schiefe 45mm Platte vor dem Drehtischantriebsmotor die Rutsche im Vordergrund. Vom Spielbrett entfernte Steine werden über diese Rutsche entsorgt, falls die Ablage am Spielbrett (vorne rechts mit dem dünnen blauen Schlauchstück) schon belegt ist, sonst werden sie dort zur späteren Verwendung abgelegt.
