---
layout: "image"
title: "Training Roboter II"
date: "2011-05-14T22:07:37"
picture: "DSC00654.jpg"
weight: "22"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30560
- /details602c.html
imported:
- "2019"
_4images_image_id: "30560"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30560 -->
Details of the limit switch of the tower.

Informationen über den Endschalter des Turms