---
layout: "image"
title: "Ampelanlage"
date: "2015-01-14T19:13:30"
picture: "ampelanlage19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40334
- /details58c4.html
imported:
- "2019"
_4images_image_id: "40334"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40334 -->
