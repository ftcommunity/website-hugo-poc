---
layout: "image"
title: "Ampelanlage Robopro"
date: "2015-01-14T19:13:30"
picture: "ampelanlage05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40320
- /details517c.html
imported:
- "2019"
_4images_image_id: "40320"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40320 -->
Es gibt für jede Spur  Zählschleifen (Robopro Regler). Damit werden alle Autos an der Kreuzung gezählt.
Je mehr Autos in einerSpur, desto länger die Grünphase. Unterbrochen durch die Fußgänger.
