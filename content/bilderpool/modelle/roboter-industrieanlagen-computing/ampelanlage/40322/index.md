---
layout: "image"
title: "Ampelanlage oben"
date: "2015-01-14T19:13:30"
picture: "ampelanlage07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40322
- /details7495.html
imported:
- "2019"
_4images_image_id: "40322"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40322 -->
Eingeschaltete Anlage. Es wurden insgesamt 40 Lämpchen verbaut.
