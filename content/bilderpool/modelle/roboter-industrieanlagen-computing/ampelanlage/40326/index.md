---
layout: "image"
title: "Ampelanlage schräg"
date: "2015-01-14T19:13:30"
picture: "ampelanlage11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40326
- /details0e16.html
imported:
- "2019"
_4images_image_id: "40326"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40326 -->
Sicht Richtung Hautstraße.
