---
layout: "image"
title: "Ampelanlage oben"
date: "2015-01-14T19:13:30"
picture: "ampelanlage08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40323
- /details7929.html
imported:
- "2019"
_4images_image_id: "40323"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40323 -->
