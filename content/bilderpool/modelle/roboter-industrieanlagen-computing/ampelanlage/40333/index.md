---
layout: "image"
title: "Ampelanlage"
date: "2015-01-14T19:13:30"
picture: "ampelanlage18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40333
- /details54bf.html
imported:
- "2019"
_4images_image_id: "40333"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40333 -->
Zebrastreifen und Richtungspfeile habe ich ebenfalls aus Pappe ausgeschnitten und aufgeklebt.
