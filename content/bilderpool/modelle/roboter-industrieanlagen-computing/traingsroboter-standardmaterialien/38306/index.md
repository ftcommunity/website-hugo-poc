---
layout: "image"
title: "Armmechanik von oben"
date: "2014-02-15T20:10:40"
picture: "IMG_0031.jpg"
weight: "14"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38306
- /detailsc84e.html
imported:
- "2019"
_4images_image_id: "38306"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-15T20:10:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38306 -->
