---
layout: "overview"
title: "Traingsroboter mit Standardmaterialien"
date: 2020-02-22T08:08:39+01:00
legacy_id:
- /php/categories/2849
- /categories50ab.html
- /categories8e03.html
- /categories124d.html
- /categoriese8df.html
- /categoriesb8aa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2849 --> 
Ein Versuch, den 80er-Jahre Trainingsroboter mit heute gut verfügbaren Teilen (bzw. ohne Spezialkomponenten - d.h. rote Dreiecksplatte, U-Getriebe mit langer Stange etc.) aufzubauen.