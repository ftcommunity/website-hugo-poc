---
layout: "image"
title: "Seitenansicht"
date: "2014-02-23T18:04:06"
picture: "IMG_0006_2.jpg"
weight: "26"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38371
- /details32b4.html
imported:
- "2019"
_4images_image_id: "38371"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38371 -->
