---
layout: "image"
title: "'Schaltpult' für 3 Achsen, Drehrichtung und Drehzahl über Trafo"
date: "2014-02-16T21:02:14"
picture: "IMG_0007.jpg"
weight: "21"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38313
- /details92cc.html
imported:
- "2019"
_4images_image_id: "38313"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-16T21:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38313 -->
in der Not - bzw. keine Zeit ....
