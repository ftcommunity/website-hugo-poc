---
layout: "comment"
hidden: true
title: "18744"
date: "2014-02-16T19:56:00"
uploadBy:
- "Konrad Planert"
license: "unknown"
imported:
- "2019"
---
Moin,
hast Du schon ganz gut hinbekommen. Ich habe den alten echten noch und möchte ihn aber mit "Robo Pro" betreiben. Weißt Du wo man das entsprechende Programm herbekommt? Das Basicprogramm läuft nur auf meinen alten Commodore C128. Ich verfolge gespannt Deine Konstruktion. Ich habe auch angefangen den Roboter im Fischertechnik-Designer zu konstruieren. Leider gibt s in der Datenbank nicht alle Teil, die so alt sind. Aber wenn Du in der neuen Version baust, könnte es ja auch im Designer hinzu bekommen sein. Gruß Konrad