---
layout: "comment"
hidden: true
title: "18745"
date: "2014-02-16T20:19:21"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
Tach,
leider nicht - ich werde den mit nem alten parallel-Interface + Arduino steuern, oder in Robo-Pro + i/o Extension - mehr steht mir derzeit nicht zur Verfügung - es gibt aber definitiv auch schon Lösungen in Robo-Pro - ich weiß nur nicht, wer die hier erarbeitet hat - Suchfunktion anwerfen ...
Mir macht es eh am meisten Spaß, die Software selbst zu entwickeln - das ist doch grade der "Sport" 
Designer - hab ich noch nicht angefasst, ich bau lieber mit "echten" Steinen - aber Du kannst meine Version ja gerne "virtualisieren" :-) - ich hab eigentlich bislang nur standard-Teile verwendet - am ältesten sind die Impulsscheiben und die Schneckengetriebe für die Mini-Motoren, evtl. sind die oberen Teile auf den Spindeln noch als "exotisch" zu werten, aber die sind definitv auch durchaus ersetzbar.
Gruß Jens