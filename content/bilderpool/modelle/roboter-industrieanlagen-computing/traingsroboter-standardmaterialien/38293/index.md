---
layout: "image"
title: "Gesamtansicht"
date: "2014-02-15T13:51:10"
picture: "IMG_0016.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38293
- /details95d1.html
imported:
- "2019"
_4images_image_id: "38293"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38293 -->
Versuch, einen Trainingsroboter der 80er mit vorhandenen bzw. heute gut verfügbaren Teilen zu bauen

weitere Bilder folgen.
einige Kompromisse mussten eingegangen werden: 

- Greifzangenantrieb fehlt noch
- ebenso die Encoder bzw. Positionsbestimmung für die beiden Achsen des Armes (Update 16.2. - Encoder siehe spätere Bilder)
- "Unterarm" musste verbreitert werden, da konstruktionsbedingt die nachgebaute Dreiecksplatte nicht durschwenken kann

weitere Problemskes:

Weil die Dreiecksplatte nicht 1:1 im Maß ist, gibt es leichte Winkelfehler - d.h. der Arm ganz vorne ist nicht immer zu 100% waagerecht, dies ist aber halbwegs tolerabel.
(16.2.: hat sich erledigt, Dank an Stefan Falk)

Ich konnte mich nicht entscheiden, ob "Motor oben" oder "Motor unten" für den Spindelantrieb schöner ist, weil ich noch keine Drehencoder konstruiert habe. Die Variante "Motor unten" kolidiert aber auch definitiv mit dem Schwenkantrieb. mal sehen, wie ich das Problem auflösen werde. - 16.2. beide Motoren gehen nach unten, "Encoder" sind oben

die (von hinten gesehen) linke Spindel kolidiert zeitweise mit den Zugstreben - hier muss noch justiert werden.
