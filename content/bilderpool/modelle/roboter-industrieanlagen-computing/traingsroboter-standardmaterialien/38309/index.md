---
layout: "image"
title: "Encoder"
date: "2014-02-16T21:02:14"
picture: "IMG_0003.jpg"
weight: "17"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38309
- /details45d7-2.html
imported:
- "2019"
_4images_image_id: "38309"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-16T21:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38309 -->
die Auflösung ist mäßig OK, aber es sollten ja keine exotischen bzw. Fremd-Teile verbaut werden, besagt die selbst-gestellte Aufgabe
