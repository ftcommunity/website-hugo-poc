---
layout: "image"
title: "Antriebssektion mit Encodern seitliche Ansicht"
date: "2014-02-16T21:02:14"
picture: "IMG_0006.jpg"
weight: "20"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38312
- /detailsf19c-2.html
imported:
- "2019"
_4images_image_id: "38312"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-16T21:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38312 -->
