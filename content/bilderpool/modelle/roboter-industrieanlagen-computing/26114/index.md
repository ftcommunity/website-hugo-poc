---
layout: "image"
title: "Teach-In Roboter für C64"
date: "2010-01-24T12:59:32"
picture: "DSC00182.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Hannes"
schlagworte: ["Teach-In", "Roboter"]
uploadBy: "hannes1971"
license: "unknown"
legacy_id:
- /php/details/26114
- /detailsc03d-2.html
imported:
- "2019"
_4images_image_id: "26114"
_4images_cat_id: "125"
_4images_user_id: "1068"
_4images_image_date: "2010-01-24T12:59:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26114 -->
Ich habe den Teach-In Roboter der 1. Genration FT Computing mit ROBO Pro neu programmiert (Daten siehe unter "Downloads")