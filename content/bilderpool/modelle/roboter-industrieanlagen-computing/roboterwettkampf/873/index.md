---
layout: "image"
title: "01"
date: "2003-04-27T17:30:32"
picture: "01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/873
- /details259d.html
imported:
- "2019"
_4images_image_id: "873"
_4images_cat_id: "13"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:30:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=873 -->
