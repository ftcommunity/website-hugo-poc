---
layout: "image"
title: "Version 2.5"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot03.jpg"
weight: "3"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33154
- /detailsb3c7.html
imported:
- "2019"
_4images_image_id: "33154"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33154 -->
Hier wurde eineiges am Greifer geändert, er kann nun um eineiges stärker zupacken und die Dose rutscht nicht mehr raus.