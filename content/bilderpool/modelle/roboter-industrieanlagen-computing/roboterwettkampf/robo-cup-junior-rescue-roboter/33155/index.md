---
layout: "image"
title: "Version 3 Bild 01"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot04.jpg"
weight: "4"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33155
- /details69fc.html
imported:
- "2019"
_4images_image_id: "33155"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33155 -->
Das ist die aktuelle Version, da er leider zu groß ist (kommt nicht um die Kurven in der Ecke der Arena) wird er nachher auseinander gebaut und es gibt einen neuen Versuch. Inzwischen gibt es nur noch eine Platine.