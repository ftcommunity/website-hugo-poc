---
layout: "image"
title: "Version 3 Bild 17"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot20.jpg"
weight: "20"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33171
- /detailsbf25-3.html
imported:
- "2019"
_4images_image_id: "33171"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33171 -->
Hier nochmal aus einer anderen Perspektive.