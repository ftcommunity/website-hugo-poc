---
layout: "image"
title: "Version 3 Bild 03"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot06.jpg"
weight: "6"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33157
- /details76e5-2.html
imported:
- "2019"
_4images_image_id: "33157"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33157 -->
