---
layout: "overview"
title: "Robo Cup Junior Rescue Roboter"
date: 2020-02-22T08:00:49+01:00
legacy_id:
- /php/categories/2452
- /categoriesd383.html
- /categoriesa47d.html
- /categories4393.html
- /categories335b.html
- /categoriesfa90.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2452 --> 
Hier möchet ich euch meinen Kettenroboter vorstellen, Ziel ist es, mit ihm am Robo Cup Junior Rescue Secondary teilzunehmen.
Die "Arena" besteht aus 3 "Räumen" mit je 90 x 130 cm. Zwei sind auf der ersten Ebene, der dritte darüber. Die zwei unteren "Räume" sind über einen "Flur" verbunden, in den oberen kommt man über eine Rampe, welche nicht steiler als 25° ist. 
Folgende Aufgaben sind zu lösen:
  - 1. und 2. Raum:
    - Linien folgen
    - Unterbrechungen darin überbrücken (Unterbrechungen befinden sich nur auf geraden Strecken)
    - Unebenheiten überfahren (Schaschlikspieße, Plastikröhrchen, ...)
    - Hindernisse umfahren (Ziegelsteine, 0,5l-Flaschen, ...)
  - 3. Raum:
    - einer Wand folgen (für die Rampe, da dort keine Linie auf dem Boden ist)
    - selbstständig Orientieren
    - Getränkedose finden, transportieren und auf einem Podest (6 cm hoch) abstellen