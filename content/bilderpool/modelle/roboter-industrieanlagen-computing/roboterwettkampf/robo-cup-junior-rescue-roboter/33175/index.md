---
layout: "image"
title: "Version 3 Bild 21"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot24.jpg"
weight: "24"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33175
- /details234b.html
imported:
- "2019"
_4images_image_id: "33175"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33175 -->
Hier sieht man die beiden Motoren, rechts den Liniensensor und in der Mitte den Farbsensor...