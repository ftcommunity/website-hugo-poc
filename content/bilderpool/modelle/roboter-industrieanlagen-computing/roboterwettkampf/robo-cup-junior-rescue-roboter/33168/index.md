---
layout: "image"
title: "Version 3 Bild 14"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot17.jpg"
weight: "17"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33168
- /details66a9.html
imported:
- "2019"
_4images_image_id: "33168"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33168 -->
