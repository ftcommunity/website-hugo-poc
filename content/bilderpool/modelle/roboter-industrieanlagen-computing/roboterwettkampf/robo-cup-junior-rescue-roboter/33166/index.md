---
layout: "image"
title: "Version 3 Bild 12"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot15.jpg"
weight: "15"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33166
- /detailsbcf9.html
imported:
- "2019"
_4images_image_id: "33166"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33166 -->
