---
layout: "image"
title: "Version 2"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot02.jpg"
weight: "2"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33153
- /details9ef2.html
imported:
- "2019"
_4images_image_id: "33153"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33153 -->
Diese Version besitzt nun einen Greifer und ist deutlich größer. Platinen sind es immer noch mehrere, allerdings nun über den I²C Verbunden. Das spart natürlich schon mal deutlich Platz auf der Platine, da ich nun nicht mehr zwei IC's zusätzlich brauche um kommunizieren zu können (Die AVR's haben alles bis auf zwei PullUp's integriert, was man zu Kommunikation benötigt).