---
layout: "image"
title: "Version 3 Bild 19"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot22.jpg"
weight: "22"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33173
- /detailsebda.html
imported:
- "2019"
_4images_image_id: "33173"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33173 -->
Hier sieht man die zwei Zahnräder, die dafür sorgen, dass sich die Zange synchron öffnet bzw. schließt.