---
layout: "image"
title: "Version 3 Bild 15"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot18.jpg"
weight: "18"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33169
- /detailsafff.html
imported:
- "2019"
_4images_image_id: "33169"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33169 -->
