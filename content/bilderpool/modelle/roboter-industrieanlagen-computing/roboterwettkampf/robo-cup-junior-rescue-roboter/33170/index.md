---
layout: "image"
title: "Version 3 Bild 16"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot19.jpg"
weight: "19"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- /php/details/33170
- /detailsb61a.html
imported:
- "2019"
_4images_image_id: "33170"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33170 -->
Das ist die gesammte Zange.