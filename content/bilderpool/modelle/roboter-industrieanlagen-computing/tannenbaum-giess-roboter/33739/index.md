---
layout: "image"
title: "Der Sensor am Christbaumständer"
date: "2011-12-23T19:30:21"
picture: "IMG_3259.jpg"
weight: "5"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/33739
- /detailsd2f6.html
imported:
- "2019"
_4images_image_id: "33739"
_4images_cat_id: "2495"
_4images_user_id: "986"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33739 -->
