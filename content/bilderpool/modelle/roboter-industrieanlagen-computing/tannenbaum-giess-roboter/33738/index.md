---
layout: "image"
title: "'Under the Christmas Tree'"
date: "2011-12-23T19:30:21"
picture: "IMG_3262.jpg"
weight: "4"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/33738
- /details083f.html
imported:
- "2019"
_4images_image_id: "33738"
_4images_cat_id: "2495"
_4images_user_id: "986"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33738 -->
Das Interface ist zum Schutz in einem Gefrierbeutel. Dahinter der Wassertank. rechts der Tannebaumständer + Sensor