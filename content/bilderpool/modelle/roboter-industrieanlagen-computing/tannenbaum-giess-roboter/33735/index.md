---
layout: "image"
title: "Pumpe + Vorratsspeicher (von oben)"
date: "2011-12-23T19:30:21"
picture: "IMG_3253.jpg"
weight: "1"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
schlagworte: ["Wasser", "Pumpe", "Tannenbaum", "Weihnachten", "Krinner", "Robo", "Interface"]
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/33735
- /details8d10.html
imported:
- "2019"
_4images_image_id: "33735"
_4images_cat_id: "2495"
_4images_user_id: "986"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33735 -->
Das ist die Pumpe (vor Ewigkeiten aus so einem alten Zahnreinigungsding ausgebaut :D) die auf dem Wasserspeicher "sitzt"

Das ganze ist übrigens nicht nur eine Spielerei, sondern soll den Baum wirklich bis zum "fällen" gießen. Bei 'nem 3.20m Baum ist das nämlich nervtötender, als man vlt. erwartet...