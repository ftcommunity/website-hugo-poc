---
layout: "image"
title: "Greifer und Hebemechanismus"
date: "2018-11-12T20:50:21"
picture: "C3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/48402
- /details233a.html
imported:
- "2019"
_4images_image_id: "48402"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48402 -->
