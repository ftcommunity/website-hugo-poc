---
layout: "image"
title: "Übersichtsbild ganzer Schachroboter"
date: "2018-11-12T20:50:21"
picture: "C1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Scara", "Schach", "Bildverarbeitung", "Kamera", "OV7670", "AVR_Max"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/48400
- /details89fa.html
imported:
- "2019"
_4images_image_id: "48400"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48400 -->
Dieser Fischertechnik Roboter spielt Schach gegen einen Menschen. Der Zug des Menschen wird mit einem Alientek Omnivision OV7670 Kamera-Modul und Bildverarbeitung auf dem ATMEGA2560 erkannt. Der AVR prüft die Gültigkeit des Zuges und führt einen Gegenzug aus. Dies wird über die Software AVR Max 4.8 erledigt, eine Portierung der Micro-Max Schachmaschine von Harm-Geert Muller durch Andre Adrian. Dieser Gegenzug wird dann durch einen mit Schrittmotoren betriebenen Scara Arm ausgeführt