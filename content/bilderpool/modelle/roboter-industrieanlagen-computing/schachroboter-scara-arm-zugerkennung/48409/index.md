---
layout: "image"
title: "Display"
date: "2018-11-12T20:50:21"
picture: "C10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Scara", "Schachroboter", "Video"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/48409
- /details15d9.html
imported:
- "2019"
_4images_image_id: "48409"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48409 -->
Hier gibt es ein Video auf youtube:

https://youtu.be/pQyH82JlAB8