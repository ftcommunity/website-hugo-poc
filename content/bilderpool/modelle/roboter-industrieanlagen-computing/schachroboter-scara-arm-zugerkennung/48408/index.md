---
layout: "image"
title: "Scara Arm von oben"
date: "2018-11-12T20:50:21"
picture: "C9.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Scara"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/48408
- /detailsd55f.html
imported:
- "2019"
_4images_image_id: "48408"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48408 -->
