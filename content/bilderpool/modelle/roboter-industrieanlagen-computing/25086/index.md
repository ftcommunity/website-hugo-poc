---
layout: "image"
title: "Stomperbot"
date: "2009-09-22T21:44:14"
picture: "sm_stomperbot.jpg"
weight: "8"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Stomperbot", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25086
- /details50f9.html
imported:
- "2019"
_4images_image_id: "25086"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25086 -->
This is a new version of the Stomperbot using the PCS BRAIN.