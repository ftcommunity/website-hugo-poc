---
layout: "image"
title: "Paketwendeanlage mit IR detector IS471F"
date: "2010-11-13T12:29:45"
picture: "ISF471F-Paketwendeanlage_014.jpg"
weight: "9"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29227
- /detailsc870-2.html
imported:
- "2019"
_4images_image_id: "29227"
_4images_cat_id: "2114"
_4images_user_id: "22"
_4images_image_date: "2010-11-13T12:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29227 -->
Ohne Problemen mit Fremdlichteinstrahlung,  Paketten mit und ohne weisses Etikett  sortieren mit IR detector IS471F :
http://www.conrad.de/ce/de/product/185094/MODULIERTER-IR-DETEKTOR-IS471F-SH
