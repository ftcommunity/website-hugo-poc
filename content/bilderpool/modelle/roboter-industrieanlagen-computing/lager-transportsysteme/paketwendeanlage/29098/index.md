---
layout: "image"
title: "Paketwendeanlage"
date: "2010-10-30T18:28:20"
picture: "Paketwendeanlage_012.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29098
- /detailse89a.html
imported:
- "2019"
_4images_image_id: "29098"
_4images_cat_id: "2114"
_4images_user_id: "22"
_4images_image_date: "2010-10-30T18:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29098 -->
Die Paketwendeanlage hat die Aufgabe, den Strichcode des Paketes zu erkennen und das Paket danach zur weiteren Bearbeitung weiterzugeben.  Die Anlage besteht aus 8 Motoren, von denen 7 Motoren in Rechts- und Linkslauf und variabler Drehzahl betrieben werden müssen. Des Weiteren sind 2 elektropneumatische Ventile zur Steuerung des Geifers und diverse Endtaster montiert.
