---
layout: "comment"
hidden: true
title: "12663"
date: "2010-10-31T13:17:31"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
- Ist es absolut notwendig IRL81A eine eigene 5V versorgungsspannung zu geben ? ( andere Anschluss IRL81A an IS471F nr4)
Standaard 9-12V wäre besser. IS471F nutzt eine versorgungsspannung 4.5-16V

Ein 7,8 Euro SMD SPANNUNGSREGLERPLATINE MIT TA78L05F ( Conrad Best.-Nr.: 140805 - 62 ) ist möglich aber vielleicht nicht notwendig ?
http://www.conrad.de/ce/de/product/140805/SMD-SPANNUNGSREGLERPLATINE-MIT-TA78L05F


Ein LDR-Fotowiderstand, ein Fototransistor oder ein IR-Spursensor funktionieren nicht immer gut wegen Fremdlichteinstrahlung,