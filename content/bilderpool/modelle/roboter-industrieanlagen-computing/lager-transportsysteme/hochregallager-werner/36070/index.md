---
layout: "image"
title: "Regal"
date: "2012-10-22T21:09:23"
picture: "hochregallagerwerner28.jpg"
weight: "28"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36070
- /detailsa8d6-2.html
imported:
- "2019"
_4images_image_id: "36070"
_4images_cat_id: "2683"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:23"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36070 -->
