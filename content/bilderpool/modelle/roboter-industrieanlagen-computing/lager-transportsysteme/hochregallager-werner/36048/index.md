---
layout: "image"
title: "Förderband mit Farberkennung"
date: "2012-10-22T21:09:02"
picture: "hochregallagerwerner06.jpg"
weight: "6"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36048
- /detailscca2.html
imported:
- "2019"
_4images_image_id: "36048"
_4images_cat_id: "2683"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36048 -->
