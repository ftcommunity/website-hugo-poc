---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-22T21:09:01"
picture: "hochregallagerwerner01.jpg"
weight: "1"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36043
- /details912c.html
imported:
- "2019"
_4images_image_id: "36043"
_4images_cat_id: "2683"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36043 -->
Video: http://www.youtube.com/watch?v=xk7J-Jgp5cA
Software: https://www.dropbox.com/s/vo3azvqx3s6dv3x/Hochregallager.rpp
