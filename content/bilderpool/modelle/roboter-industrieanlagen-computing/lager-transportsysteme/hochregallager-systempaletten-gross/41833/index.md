---
layout: "image"
title: "Gesamtansicht"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten01.jpg"
weight: "1"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41833
- /details9f5a-2.html
imported:
- "2019"
_4images_image_id: "41833"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41833 -->
Eine noch größere Ansicht wollte ich euch nicht zumuten, da mein Zimmer recht unordentlich ist (Bastlerwahn)
Ich lade es unter Fehlversuch hoch, da ich nicht mehr weiterbaue aus folgendem Grund. Das RegalBedienGerät (RBG) fährt zwar gut hin und her, jedoch ist das kleine Ritzel nur durch das übliche Klemmen auf die Achse geklemmt und nicht geklebt, das möchte ich nicht. beim Anfahren muss man deshalb sachte anfahren sonst rutscht es möglicherweise durch (je nach Last im Käfig auf der Palette etc.. Außerdem ist mir oben keine gute Führung gelungen, weshalb das RBG recht wackelig ist vor und zurück. Seitlich ist es mir gelungen, es zu führen, damit der Käfig  gut hoch und herunter fahren kann. Der Platz zur Seite war sehr begrenzt. Auf den weiteren Bildern seht ihr, dass man wegen dem enormen Hub die Regalfächer weiter außen positionieren kann. Der Hauptgrund ist für mich, dass ich zwar meinen Spaß mit dem Modell hatte, aber vielmehr dass ich ein ähnliches Modell vor habe und die Bauteile benötige. Das neue Modell soll einen praktischen Nutzen haben und Gegenstände für den täglichen Gebrauch beinhalten. Mir war es wichtig, Bilder hochzuladen, weil es für den ein oder anderen da draußen interessant sein könnte, dass man ein RBG beidseitig auslegen kann und das Gerät mit dem Käfig links UND rechts herausfahren kann. 

Sorry für den Feind eines fischertechnikfans, dem Staub ;)
Das sind die ersten Bilder die ich hochlade ich hoffe mit der Qualität ist alles ok. Keine Bestleistung ich weiß aber anschaubar