---
layout: "image"
title: "Knickführung"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten16.jpg"
weight: "16"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41848
- /details9317-4.html
imported:
- "2019"
_4images_image_id: "41848"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41848 -->
Hier sieht man die Lücke wo der mitfahrende Statikträger drin läuft und den Grundbaustein mit Bohrung welcher die Achse für das Umlenkzahnrad beinhaltet