---
layout: "image"
title: "Führung innen ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten25.jpg"
weight: "25"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41857
- /details564b-2.html
imported:
- "2019"
_4images_image_id: "41857"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41857 -->
Hier kann man erahnen wie der Statikträger läuft und geführt wird. Da er etwa zur Hälfte geführt ist, ist das Ganze wirklich stabil