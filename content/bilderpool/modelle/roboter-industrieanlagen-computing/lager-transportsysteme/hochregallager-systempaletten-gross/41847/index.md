---
layout: "image"
title: "Teleskopbefestigung"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten15.jpg"
weight: "15"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41847
- /details1a8f.html
imported:
- "2019"
_4images_image_id: "41847"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41847 -->
hier ist die Kette befestigt. Das ist der eigentliche Trick des Teleskops. Ich bin nicht der erste der das verwendet aber ich wollte es eben auf beiden Seiten weit genug ausfahrbar machen