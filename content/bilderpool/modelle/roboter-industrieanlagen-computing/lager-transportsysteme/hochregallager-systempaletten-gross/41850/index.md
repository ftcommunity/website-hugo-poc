---
layout: "image"
title: "Höhe ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten18.jpg"
weight: "18"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41850
- /details7877-2.html
imported:
- "2019"
_4images_image_id: "41850"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41850 -->
Hier seht ihr dass das Ganze recht hoch ist. Deswegen musste ich die Höhenabstände zwischen den Regalplätzen hoch wählen damit ich da überhaupt reinfahren kann. Ist aber ohne weiteres möglich, da die Breite kleiner als die Palette ist