---
layout: "image"
title: "Vor- und Zurückfahren"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten02.jpg"
weight: "2"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41834
- /details9876-2.html
imported:
- "2019"
_4images_image_id: "41834"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41834 -->
hier sieht man meinen Antrieb zum Vor- und Zurückfahren welche ein exotisches Zahnrad aus der alten Industrieserie beinhaltet, welches direkt auf die Hubzahnstangen geht. Den 20:1 Motor könnte man auch gegen einen 50:1 Motor tauschen damit das anfahren langsamer geschieht. Durch gute Programmierung kann man ein Durchrutschen an der Kunststoffachse umgehen. Es hat zwar gut geklappt aber bei einer minimal größeren Last rutschte das kleine Zahnrad auf der Achse durch. Kleben wollte ich nicht. Links sieht man den Hoch-Runter-Motor ;)