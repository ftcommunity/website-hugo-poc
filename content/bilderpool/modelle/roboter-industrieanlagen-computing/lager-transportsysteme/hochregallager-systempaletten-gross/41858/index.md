---
layout: "image"
title: "Höhenansicht ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten26.jpg"
weight: "26"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41858
- /detailse633.html
imported:
- "2019"
_4images_image_id: "41858"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41858 -->
Hier nochmal das Problem mit der Höhe. Auch die Breite ist unten sehr kritisch aber da die Statikträger sehr weit unten sind, kann die Palette gut vom Regal aufgenommen werden.