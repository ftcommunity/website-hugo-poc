---
layout: "image"
title: "Links ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten17.jpg"
weight: "17"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41849
- /detailsbf79-2.html
imported:
- "2019"
_4images_image_id: "41849"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41849 -->
Der Abstand auf beiden Seiten ist echt gewaltig. Man muss nicht zu 100% ausfahren da der Kipppunkt gefährlich nah an das Ende des Aluprofils fährt. Man kann das Regal, so wie ich das gemacht habe, weiter nach innen bauen dass man die Palette früher ablegen kann. So hat man genug Stabilität