---
layout: "image"
title: "Der Kern des Ganzen"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten06.jpg"
weight: "6"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- /php/details/41838
- /details2733-2.html
imported:
- "2019"
_4images_image_id: "41838"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41838 -->
Der Grund warum ich das Modell überhaupt hochlade und das Hauptaugenmerk des Ganzen. Ich habe vorallem bereits die Hochregallagermodelle von Staudinger, also die Fertigmodelle, hier im Bilderpool gesehen und diverse Hochregallagermodelle. Ich habe schon viele Hochregallager gebaut weil ich davon am meisten fasziniert bin. Ich war aber leider zu faul es hochzuladen :( 

Jetzt ist es mir gelungen, mit normalen fischertechnikteilen ohne spanendes Bearbeiten von zwei Aluprofilen wie bei Staudinger, oder wie auch immer das gelöst wurde, eine Konstruktion zu entwerfen, welche AUF BEIDE SEITEN ausfahren kann und gleichzeitig stabil ist. Sie ist leider sehr schwer und groß geworden aber ich denke man kann hier bestimmt kleinere Zahnräder nehmen und vieles optimieren. Die mittlere Position kann man über einen Taster über dem Drehkranz abfragen, allerdings habe ich hier keinen verbaut. Ich bin davon ausgegangen, dass ich die Impulse des Encodermotors benutze und die Palette immer in der Mitte startet und wieder genau dorthin zurückfährt ohne dass man das HRL abschaltet in einer anderen Position. Ich schätze eine andere Positionsabfrage ist nicht möglich. Sicherlich könnte man irgendwo einen Magnet und einen Reedkontakt verwenden aber so wie ich das gemacht habe müsste man zuerst eine Lücke schaffen und anstatt Statikträgern Grundbausteine verwenden.