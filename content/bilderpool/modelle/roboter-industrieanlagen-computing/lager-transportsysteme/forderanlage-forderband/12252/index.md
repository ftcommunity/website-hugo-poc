---
layout: "image"
title: "Zuführeinheit"
date: "2007-10-17T13:29:23"
picture: "DSCN1760.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12252
- /details35a0-2.html
imported:
- "2019"
_4images_image_id: "12252"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T13:29:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12252 -->
Ein Blick von ober in den konischen Auslaß.
