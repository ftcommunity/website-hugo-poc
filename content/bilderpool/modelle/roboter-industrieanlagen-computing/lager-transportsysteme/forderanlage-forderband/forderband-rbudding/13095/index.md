---
layout: "image"
title: "Farbsensor aus"
date: "2007-12-17T22:22:39"
picture: "close_up_aus.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13095
- /details221d.html
imported:
- "2019"
_4images_image_id: "13095"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13095 -->
At the end of the conveyor belt, the bar is directly attached. 
less then 0.1 cm.
