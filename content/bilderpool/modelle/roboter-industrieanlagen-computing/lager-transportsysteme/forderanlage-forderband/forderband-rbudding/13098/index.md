---
layout: "image"
title: "Farbsensor III"
date: "2007-12-17T22:22:39"
picture: "overview2.jpg"
weight: "7"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13098
- /details7080-2.html
imported:
- "2019"
_4images_image_id: "13098"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13098 -->
