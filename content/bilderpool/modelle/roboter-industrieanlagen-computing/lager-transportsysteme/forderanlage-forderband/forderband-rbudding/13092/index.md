---
layout: "image"
title: "Förderband overview"
date: "2007-12-17T22:22:39"
picture: "overview.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/13092
- /detailsb6dc.html
imported:
- "2019"
_4images_image_id: "13092"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13092 -->
Example how to transport and determine FT pieces and colors.
Used the new color sensor of FT. (robo explorer box)
