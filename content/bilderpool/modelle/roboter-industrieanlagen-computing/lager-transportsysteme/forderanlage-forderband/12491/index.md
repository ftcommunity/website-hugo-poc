---
layout: "image"
title: "Beladung"
date: "2007-11-05T15:54:02"
picture: "DSCN1956.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12491
- /details36b4.html
imported:
- "2019"
_4images_image_id: "12491"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12491 -->
Hier wird ein Fahrzeig direkt durch die Zuführeinheit beladen.
