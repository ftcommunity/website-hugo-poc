---
layout: "image"
title: "Ansicht 2"
date: "2007-10-22T15:19:23"
picture: "PICT2213.jpg"
weight: "32"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12275
- /details93c0.html
imported:
- "2019"
_4images_image_id: "12275"
_4images_cat_id: "1092"
_4images_user_id: "424"
_4images_image_date: "2007-10-22T15:19:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12275 -->
