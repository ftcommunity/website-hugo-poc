---
layout: "image"
title: "Ansicht"
date: "2007-10-13T11:40:15"
picture: "DSCN1703.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12203
- /details2ccc.html
imported:
- "2019"
_4images_image_id: "12203"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12203 -->
So sieht´s von der Seite aus. Damit die Kette und die Mitnehmer nicht über den Boden schleifen habe ich unten einen zusätzlichen "Kanal" angebracht.
