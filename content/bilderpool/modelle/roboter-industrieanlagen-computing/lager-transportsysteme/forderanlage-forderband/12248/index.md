---
layout: "image"
title: "Zuführeinheit"
date: "2007-10-17T13:29:22"
picture: "DSCN1752.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12248
- /detailsa4cb.html
imported:
- "2019"
_4images_image_id: "12248"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T13:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12248 -->
Genau wie beim Transportband auch hier ein Schutz für die Rückgeführten Mitnehmer. So laufen sie nicht über die Tischplatte.
