---
layout: "image"
title: "Zuführung überarbeitet"
date: "2007-10-22T15:19:24"
picture: "DSCN1819.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12279
- /details18c2.html
imported:
- "2019"
_4images_image_id: "12279"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-22T15:19:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12279 -->
Hier schiebt sich gerade ein Mitnehmer in die passenden Nuten.
