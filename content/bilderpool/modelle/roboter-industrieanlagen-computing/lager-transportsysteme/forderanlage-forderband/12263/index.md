---
layout: "image"
title: "Übergang"
date: "2007-10-17T14:21:09"
picture: "DSCN1794.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12263
- /details743b.html
imported:
- "2019"
_4images_image_id: "12263"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T14:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12263 -->
zwischen Zuführeinheit und Transportband. Man könnte an Stelle eines Transportbandes auch einen LKW Anhänger beschicken.
