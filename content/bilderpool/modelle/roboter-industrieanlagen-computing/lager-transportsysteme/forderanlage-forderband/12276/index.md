---
layout: "image"
title: "Mitnehmer"
date: "2007-10-22T15:19:23"
picture: "PICT2210.jpg"
weight: "33"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12276
- /details2d09-3.html
imported:
- "2019"
_4images_image_id: "12276"
_4images_cat_id: "1092"
_4images_user_id: "424"
_4images_image_date: "2007-10-22T15:19:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12276 -->
Es handelt sich hier nur um ein Testband.
Mit S-Riegel als  Fördergut sind keine Probleme aufgetreten.
