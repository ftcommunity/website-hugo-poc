---
layout: "image"
title: "Anfang"
date: "2007-10-13T11:40:15"
picture: "DSCN1700.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12198
- /detailsb0bf.html
imported:
- "2019"
_4images_image_id: "12198"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12198 -->
Am Anfang des Förderbandes ist eine Klappe die durch die Mitnehmer, die auf der Förderkette befestigt sind, hochgehoben wird. So vermeide ich es das kleine Teile nach vorn herausfallen.
