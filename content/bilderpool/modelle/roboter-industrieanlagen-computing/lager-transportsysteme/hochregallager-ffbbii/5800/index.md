---
layout: "image"
title: "HRL Roboter (noch im Bau)"
date: "2006-03-03T12:01:01"
picture: "Fischertechnik_005.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5800
- /detailsd537.html
imported:
- "2019"
_4images_image_id: "5800"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-03T12:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5800 -->
