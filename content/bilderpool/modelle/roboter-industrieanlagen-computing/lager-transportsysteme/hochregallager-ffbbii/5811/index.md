---
layout: "image"
title: "HRL"
date: "2006-03-05T12:27:38"
picture: "Fischertechnik_011.jpg"
weight: "6"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5811
- /detailsf208.html
imported:
- "2019"
_4images_image_id: "5811"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-05T12:27:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5811 -->
