---
layout: "image"
title: "HRL Roboter"
date: "2006-03-07T18:20:04"
picture: "Fischertechnik_012.jpg"
weight: "8"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5845
- /detailsb628.html
imported:
- "2019"
_4images_image_id: "5845"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-07T18:20:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5845 -->
