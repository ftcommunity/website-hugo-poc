---
layout: "comment"
hidden: true
title: "10059"
date: "2009-10-12T21:25:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Nachtrag:
wie es auf dem 2. Foto aussieht, transportierst du die Werkstücke über das Rollenfriktionsband auf einem Transportwagen mit Grundplatte 90x90x5. Damit würde sich die Erörterung hier erübrigen, weil du dann mit den BS30sz? eine 90mm? lange Auflage auf den Friktionsrollen hast?