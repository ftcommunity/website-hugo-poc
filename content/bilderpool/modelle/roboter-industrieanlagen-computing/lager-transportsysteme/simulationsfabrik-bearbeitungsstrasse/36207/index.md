---
layout: "image"
title: "Linke Ecke oben"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager33.jpg"
weight: "33"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36207
- /details319f-3.html
imported:
- "2019"
_4images_image_id: "36207"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36207 -->
