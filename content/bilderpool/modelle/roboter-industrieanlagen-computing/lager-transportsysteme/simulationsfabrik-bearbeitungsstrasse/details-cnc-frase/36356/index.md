---
layout: "image"
title: "CNC Fräse Bauphase"
date: "2012-12-28T17:09:58"
picture: "detailscncfraese02.jpg"
weight: "2"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36356
- /details3144-2.html
imported:
- "2019"
_4images_image_id: "36356"
_4images_cat_id: "2699"
_4images_user_id: "941"
_4images_image_date: "2012-12-28T17:09:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36356 -->
Der Motor XS unten sitzt an einem Hubgetriebe und bewegt die komplette Fräse vor und zurück.

Der in der Mitte verbaute S-Motor hebt und senkt den Fräskopf.
Direkt davor sitzt mittig der dazugehörige Endtaster.

Im Fräskopf selbst sitzt ein Motor XS der die Spindel antreibt.

Während der Bauphase wurden leere dummy Hubgetriebe verwendet,  diese wurden dann am Schluss gegen echte Hubgetriebe ausgetauscht.