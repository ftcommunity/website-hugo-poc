---
layout: "image"
title: "CNC Fräse / fertig verkabelt"
date: "2012-12-28T17:09:58"
picture: "detailscncfraese05.jpg"
weight: "5"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36359
- /details92be.html
imported:
- "2019"
_4images_image_id: "36359"
_4images_cat_id: "2699"
_4images_user_id: "941"
_4images_image_date: "2012-12-28T17:09:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36359 -->
Der Motor XS unten sitzt an einem Hubgetriebe und bewegt die komplette Fräse vor und zurück.

Der in der Mitte verbaute S-Motor hebt und senkt den Fräskopf.
Direkt davor sitzt mittig der dazugehörige Endtaster.

Im Fräskopf selbst sitzt ein Motor XS der die Spindel antreibt.

Die Kabel sind aus alten IDE Festplattenkabeln selbstgemacht.

Die passenden Stecker gibts z.B. bei Conrad / Pollin / usw.