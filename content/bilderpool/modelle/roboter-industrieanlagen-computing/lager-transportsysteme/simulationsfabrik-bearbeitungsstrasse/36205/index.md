---
layout: "image"
title: "Gesamtansicht von rechts"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager31.jpg"
weight: "31"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36205
- /details6bc9.html
imported:
- "2019"
_4images_image_id: "36205"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36205 -->
