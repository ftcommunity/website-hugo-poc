---
layout: "image"
title: "Werkstückrutsche"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager50.jpg"
weight: "50"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36224
- /details7b88-2.html
imported:
- "2019"
_4images_image_id: "36224"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36224 -->
