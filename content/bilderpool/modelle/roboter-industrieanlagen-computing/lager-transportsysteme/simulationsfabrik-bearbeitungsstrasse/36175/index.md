---
layout: "image"
title: "Gesamtansicht von vorne 1"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager01.jpg"
weight: "1"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36175
- /details67c2.html
imported:
- "2019"
_4images_image_id: "36175"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36175 -->
