---
layout: "image"
title: "Die drei ROBO TX"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager56.jpg"
weight: "56"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36230
- /details61e9.html
imported:
- "2019"
_4images_image_id: "36230"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36230 -->
Die drei ROBO TX steuern die obere Ebene