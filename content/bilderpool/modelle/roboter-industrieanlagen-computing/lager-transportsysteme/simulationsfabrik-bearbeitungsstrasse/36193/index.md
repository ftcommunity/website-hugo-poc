---
layout: "image"
title: "Von rechts unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager19.jpg"
weight: "19"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36193
- /details5aae-2.html
imported:
- "2019"
_4images_image_id: "36193"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36193 -->
von rechts nach links, Werkstückrutsche, Anlauf-Registerlager, Putzstation, Schweißstation