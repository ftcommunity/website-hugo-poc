---
layout: "image"
title: "Hochregallager 4x6 Lagerplätze"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager41.jpg"
weight: "41"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36215
- /details0de5.html
imported:
- "2019"
_4images_image_id: "36215"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36215 -->
Das Hochregallager hat 4x6 Lagerplätze und ist aus ca. 270 BS 7,5 und zig Verbinder gebaut.