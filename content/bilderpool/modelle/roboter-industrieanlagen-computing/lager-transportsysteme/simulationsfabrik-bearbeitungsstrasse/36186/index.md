---
layout: "image"
title: "Oben mitte"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager12.jpg"
weight: "12"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36186
- /details3d0b.html
imported:
- "2019"
_4images_image_id: "36186"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36186 -->
von links nach rechts, Pneumatische Magnet Umsetzer, Werkstück-Wendeanlage
im Hintergrund Kompressor, Eckschieber 4, Übergabestelle an Regalbediengerät (RBG), Hochregallager