---
layout: "image"
title: "Regalbediengerät"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager48.jpg"
weight: "48"
konstrukteure: 
- "tz"
fotografen:
- "tz"
schlagworte: ["32455"]
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36222
- /detailsfcc7.html
imported:
- "2019"
_4images_image_id: "36222"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36222 -->
