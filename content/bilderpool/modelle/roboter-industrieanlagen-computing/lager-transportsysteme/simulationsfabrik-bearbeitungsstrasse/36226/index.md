---
layout: "image"
title: "Von rechts"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager52.jpg"
weight: "52"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36226
- /details000b-3.html
imported:
- "2019"
_4images_image_id: "36226"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36226 -->
