---
layout: "image"
title: "Eckschieber 4"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager40.jpg"
weight: "40"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36214
- /details18a8.html
imported:
- "2019"
_4images_image_id: "36214"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36214 -->
