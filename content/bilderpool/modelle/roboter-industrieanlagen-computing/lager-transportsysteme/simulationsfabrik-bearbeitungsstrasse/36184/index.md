---
layout: "image"
title: "Linke Ecke unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager10.jpg"
weight: "10"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36184
- /details0189-3.html
imported:
- "2019"
_4images_image_id: "36184"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36184 -->
von rechts nach links, Frässtation CNC 2, Bohrstation, Eckschieber 2, Stanze