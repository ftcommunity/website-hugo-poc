---
layout: "image"
title: "Linke Ecke oben"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager14.jpg"
weight: "14"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36188
- /details6fc5-3.html
imported:
- "2019"
_4images_image_id: "36188"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36188 -->
von links nach rechts, Aufzugabnehmer, Scannerstation, Eckschieber 3, Pneumatischer Magnet-Umsetzer
im Hintergrund Kompressor, Eckschieber 4, Übergabestelle an Regalbediengerät (RBG)