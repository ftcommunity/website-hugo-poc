---
layout: "image"
title: "Regalbediengerät"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager46.jpg"
weight: "46"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36220
- /detailsc20b.html
imported:
- "2019"
_4images_image_id: "36220"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36220 -->
