---
layout: "image"
title: "ROBO Interface + 3 ROBO I/O Extension Verkabelung im Detail"
date: "2012-12-15T13:32:46"
picture: "dftextak7.jpg"
weight: "7"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36279
- /detailsb187-3.html
imported:
- "2019"
_4images_image_id: "36279"
_4images_cat_id: "2693"
_4images_user_id: "941"
_4images_image_date: "2012-12-15T13:32:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36279 -->
Vier Anschlüssen für die Stromversorgung zu einem zusammengefasst.

Die passenden Steckern & Buchsen gibts z.B. bei Conrad / Pollin / usw.