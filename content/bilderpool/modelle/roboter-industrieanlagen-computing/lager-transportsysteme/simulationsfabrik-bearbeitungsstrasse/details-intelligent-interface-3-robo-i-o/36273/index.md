---
layout: "image"
title: "ROBO Interface + 3 ROBO I/O Extension"
date: "2012-12-15T13:32:46"
picture: "dftextak1.jpg"
weight: "1"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/36273
- /details57a6.html
imported:
- "2019"
_4images_image_id: "36273"
_4images_cat_id: "2693"
_4images_user_id: "941"
_4images_image_date: "2012-12-15T13:32:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36273 -->
von vorne