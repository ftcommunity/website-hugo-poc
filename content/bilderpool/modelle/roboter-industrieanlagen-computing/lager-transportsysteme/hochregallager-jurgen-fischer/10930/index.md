---
layout: "image"
title: "Regalanlage"
date: "2007-06-26T22:38:48"
picture: "Regalanlage_8.jpg"
weight: "13"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- /php/details/10930
- /detailsd542-2.html
imported:
- "2019"
_4images_image_id: "10930"
_4images_cat_id: "990"
_4images_user_id: "341"
_4images_image_date: "2007-06-26T22:38:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10930 -->
