---
layout: "image"
title: "Regalanlage"
date: "2007-06-27T18:34:58"
picture: "Bilder_meiner_Regalanlage_2007.jpg"
weight: "14"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- /php/details/10934
- /details2e7c.html
imported:
- "2019"
_4images_image_id: "10934"
_4images_cat_id: "990"
_4images_user_id: "341"
_4images_image_date: "2007-06-27T18:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10934 -->
