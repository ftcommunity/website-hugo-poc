---
layout: "image"
title: "Antrieb für das Transportband"
date: "2011-09-18T15:16:14"
picture: "industriemodell29.jpg"
weight: "29"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31841
- /details7800-2.html
imported:
- "2019"
_4images_image_id: "31841"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31841 -->
