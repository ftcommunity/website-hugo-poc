---
layout: "image"
title: "Antrieb der Querachse"
date: "2011-09-18T15:16:14"
picture: "industriemodell35.jpg"
weight: "35"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31847
- /detailsb67e.html
imported:
- "2019"
_4images_image_id: "31847"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31847 -->
Auch mit Impulstaster
