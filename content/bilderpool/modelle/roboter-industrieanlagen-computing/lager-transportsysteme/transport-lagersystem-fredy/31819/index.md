---
layout: "image"
title: "Dreh-Eckenantrieb"
date: "2011-09-18T15:16:13"
picture: "industriemodell07.jpg"
weight: "7"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31819
- /details4eaa.html
imported:
- "2019"
_4images_image_id: "31819"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31819 -->
Der Antrieb läuft über die 4 Kegelzahnräder und die Kette. Sie läuft einmal unter dem Drehteller durch und treibt ein Z20 an.
