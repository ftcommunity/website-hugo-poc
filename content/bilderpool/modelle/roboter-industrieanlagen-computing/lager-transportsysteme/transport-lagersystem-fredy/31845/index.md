---
layout: "image"
title: "Greifer"
date: "2011-09-18T15:16:14"
picture: "industriemodell33.jpg"
weight: "33"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31845
- /detailsaff3.html
imported:
- "2019"
_4images_image_id: "31845"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31845 -->
Hier seht ihr die Aufnahem  für den Greifer
