---
layout: "image"
title: "Übersicht"
date: "2011-09-18T15:16:13"
picture: "industriemodell05.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31817
- /details36ea-3.html
imported:
- "2019"
_4images_image_id: "31817"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31817 -->
