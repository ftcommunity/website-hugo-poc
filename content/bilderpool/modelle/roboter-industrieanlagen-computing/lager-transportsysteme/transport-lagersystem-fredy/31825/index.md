---
layout: "image"
title: "Transportwagen"
date: "2011-09-18T15:16:13"
picture: "industriemodell13.jpg"
weight: "13"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31825
- /detailsb64a.html
imported:
- "2019"
_4images_image_id: "31825"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31825 -->
Hier seht ihr den Wagen in der Station, er kann die Räder nach Rechts oder Links Tranportieren.
