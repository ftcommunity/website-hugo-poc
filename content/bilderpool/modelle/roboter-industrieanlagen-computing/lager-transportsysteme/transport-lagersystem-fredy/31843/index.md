---
layout: "image"
title: "Das Regal"
date: "2011-09-18T15:16:14"
picture: "industriemodell31.jpg"
weight: "31"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31843
- /details9ca3.html
imported:
- "2019"
_4images_image_id: "31843"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31843 -->
Es kann vor und zurück gefahren werden, damit der Greifer die Räder ablegen kann.
