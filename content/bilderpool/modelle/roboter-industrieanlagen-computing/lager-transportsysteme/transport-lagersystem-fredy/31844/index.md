---
layout: "image"
title: "Regal"
date: "2011-09-18T15:16:14"
picture: "industriemodell32.jpg"
weight: "32"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31844
- /detailsca95-4.html
imported:
- "2019"
_4images_image_id: "31844"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31844 -->
Hier liegt ein Rad im Regal.
