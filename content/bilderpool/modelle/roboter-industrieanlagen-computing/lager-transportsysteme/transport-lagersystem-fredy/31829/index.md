---
layout: "image"
title: "Transportwagen"
date: "2011-09-18T15:16:13"
picture: "industriemodell17.jpg"
weight: "17"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31829
- /detailsec77-2.html
imported:
- "2019"
_4images_image_id: "31829"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31829 -->
Übersicht. in der Mitte ist der Auswerfer zu sehen.
