---
layout: "image"
title: "Führung der Querachse"
date: "2011-09-18T15:16:14"
picture: "industriemodell36.jpg"
weight: "36"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31848
- /detailsbf8d-3.html
imported:
- "2019"
_4images_image_id: "31848"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31848 -->
Sie wird über die Achse und die Spindel geführt, der BS30 mit Winkelstein an dem U-Träger verhindert das Schwingen.
