---
layout: "image"
title: "Der ganze Auswerfer"
date: "2011-09-18T15:16:13"
picture: "industriemodell22.jpg"
weight: "22"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31834
- /details9a0f-2.html
imported:
- "2019"
_4images_image_id: "31834"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31834 -->
