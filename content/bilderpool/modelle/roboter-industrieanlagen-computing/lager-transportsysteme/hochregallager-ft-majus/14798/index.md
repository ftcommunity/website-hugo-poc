---
layout: "image"
title: "Förderband 3"
date: "2008-07-04T22:47:08"
picture: "116_1666.jpg"
weight: "21"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14798
- /details2554.html
imported:
- "2019"
_4images_image_id: "14798"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-04T22:47:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14798 -->
Die Positionierung der Kästchen bei der Farbsortierung erfolgt über einen Helligkeitssensor. Befindet sich kein Kästchen über den Sensor, dann hat er in RoboPro den Wert 200. Mit Kästchen den Wert 1023.