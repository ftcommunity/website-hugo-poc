---
layout: "image"
title: "Vakuumgreifer"
date: "2009-12-06T13:37:25"
picture: "HRL_5.12.09_009.jpg"
weight: "79"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/25891
- /details6b19.html
imported:
- "2019"
_4images_image_id: "25891"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-12-06T13:37:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25891 -->
Über die beiden Taster wird erkannt, wenn der Greifer auf eine Kassette aufegesetzt ist.