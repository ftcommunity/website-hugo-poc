---
layout: "image"
title: "Steuerung"
date: "2009-08-22T13:19:15"
picture: "IMG_3775.jpg"
weight: "73"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24825
- /details5bf9.html
imported:
- "2019"
_4images_image_id: "24825"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24825 -->
In der Mitte befindet sich die SPS - Steuerung, links daneben das Netzgerät.
Darüber und rechts davon sind Die Ein -und Ausgangsmodule angebracht.