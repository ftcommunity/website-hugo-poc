---
layout: "image"
title: "HRL"
date: "2008-06-18T18:08:47"
picture: "115_1591.jpg"
weight: "4"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14709
- /detailsc16b.html
imported:
- "2019"
_4images_image_id: "14709"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T18:08:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14709 -->
Der Antrieb der Y-Achse. Der 20:1 P-Motor übertragt die Energie über ein Z10 auf das große Zahnrad in der Mitte. Über zwei Z20 wird die Energie auf die Antriebsschnecken übertragen.