---
layout: "image"
title: "2. Förderband"
date: "2009-08-22T13:19:15"
picture: "IMG_3764.jpg"
weight: "66"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24818
- /details77f7-2.html
imported:
- "2019"
_4images_image_id: "24818"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24818 -->
Auch das 2. Förderband erkennt die Kassetten per Lichttaster.
Der linke Lichttaster erkennt, wenn sich eine Kassette im Tunnel befindet, der rechte diehnt zur Farbsortierung.