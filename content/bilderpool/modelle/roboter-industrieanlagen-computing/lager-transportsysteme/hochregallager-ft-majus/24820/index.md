---
layout: "image"
title: "Einlagerer"
date: "2009-08-22T13:19:15"
picture: "IMG_3768.jpg"
weight: "68"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24820
- /details2b77.html
imported:
- "2019"
_4images_image_id: "24820"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24820 -->
Gesamtansicht des Einlageres.