---
layout: "image"
title: "HRL"
date: "2008-06-18T06:55:42"
picture: "115_1569.jpg"
weight: "1"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14706
- /details6e85.html
imported:
- "2019"
_4images_image_id: "14706"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T06:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14706 -->
Auf diesem Bild kann man das HRL, den Einlagerer und die Transportbänder erkennen.