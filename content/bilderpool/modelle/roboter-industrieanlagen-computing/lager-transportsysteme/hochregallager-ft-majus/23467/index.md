---
layout: "image"
title: "Neu Verkabelung und Ventile"
date: "2009-03-15T15:30:04"
picture: "125_2528.jpg"
weight: "50"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23467
- /detailse970.html
imported:
- "2019"
_4images_image_id: "23467"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-03-15T15:30:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23467 -->
Bisher waren die Ventile unter dem Em befestigt, da dies jetzt jedoch nicht mehr benötigt wird, wurden die Ventile nun auf der Rückseite des Einlgerers befestigt. Daneben wurde das Flachbandkabel mit Heißkleber befestigt.