---
layout: "image"
title: "Steuereinheit"
date: "2008-10-14T21:37:50"
picture: "120_2035.jpg"
weight: "41"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/15986
- /details73a6-3.html
imported:
- "2019"
_4images_image_id: "15986"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15986 -->
Die Steuereinheit besteht aus einem Vakuumpumpe, die den Unterdruck für den Vakummgreifer herstellt. Die Vakuumpumpe ist durch die Kompressorabschaltung gesteuert. Daneben befindet sich das Interface.