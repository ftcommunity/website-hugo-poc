---
layout: "image"
title: "Einlagerer"
date: "2009-08-22T13:19:15"
picture: "IMG_3772.jpg"
weight: "71"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/24823
- /detailsce76-2.html
imported:
- "2019"
_4images_image_id: "24823"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24823 -->
Auch die Positionierung der Y-Achse geschieht per Initiator und nicht mehr durch einen Impulstaster.