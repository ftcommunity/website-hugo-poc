---
layout: "image"
title: "Antrieb der X-Achse"
date: "2008-06-18T06:55:42"
picture: "115_1588.jpg"
weight: "3"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14708
- /details6527.html
imported:
- "2019"
_4images_image_id: "14708"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T06:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14708 -->
Der Antrieb X-Achse des Einlageres erfolgt über einen P-Motor 8:1 angetrieben.