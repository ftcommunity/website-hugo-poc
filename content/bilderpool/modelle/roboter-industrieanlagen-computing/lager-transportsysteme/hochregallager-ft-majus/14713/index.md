---
layout: "image"
title: "HRL"
date: "2008-06-18T18:08:48"
picture: "115_1581.jpg"
weight: "8"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14713
- /detailsa6fb.html
imported:
- "2019"
_4images_image_id: "14713"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T18:08:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14713 -->
In dieser Box werden die Kästchen nach schwarz oder weiß sortiert. Die sortierung erfolgt über einen Fototransistor.