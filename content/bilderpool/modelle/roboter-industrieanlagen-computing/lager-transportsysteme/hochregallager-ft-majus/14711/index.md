---
layout: "image"
title: "HRL"
date: "2008-06-18T18:08:48"
picture: "115_1576.jpg"
weight: "6"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14711
- /details2d39.html
imported:
- "2019"
_4images_image_id: "14711"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T18:08:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14711 -->
Auf diesem Bild sieht man den Vakuumgreifer. Die CNY70 am Vakuumgreifer erkennt, ob im Fach sich ein Kästchen befindet oder nicht.