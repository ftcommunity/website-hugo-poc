---
layout: "image"
title: "Vakuumgreifer"
date: "2009-12-06T13:37:25"
picture: "HRL_5.12.09_008.jpg"
weight: "78"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/25890
- /details30ed-3.html
imported:
- "2019"
_4images_image_id: "25890"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-12-06T13:37:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25890 -->
Detailaufnahme des Vakuumgreifers von der Seite.