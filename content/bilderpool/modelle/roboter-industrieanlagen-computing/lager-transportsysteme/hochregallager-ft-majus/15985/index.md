---
layout: "image"
title: "Förderbänder"
date: "2008-10-14T21:37:50"
picture: "120_2032.jpg"
weight: "40"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/15985
- /detailsecdf-2.html
imported:
- "2019"
_4images_image_id: "15985"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15985 -->
Das dritte und vierte Förderband wurde neu verkabelt, zudem wurde der Tunnel umgebaut.