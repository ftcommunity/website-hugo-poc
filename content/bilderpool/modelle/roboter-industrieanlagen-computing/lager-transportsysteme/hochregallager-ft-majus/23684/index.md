---
layout: "image"
title: "Drehtisch"
date: "2009-04-13T11:49:11"
picture: "126_2642.jpg"
weight: "60"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23684
- /detailsc187.html
imported:
- "2019"
_4images_image_id: "23684"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-13T11:49:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23684 -->
Nocheinmal die Positionierung des Drehtisches.