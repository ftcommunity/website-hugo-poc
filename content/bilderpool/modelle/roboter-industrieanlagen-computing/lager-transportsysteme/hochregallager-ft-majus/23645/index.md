---
layout: "image"
title: "Neu"
date: "2009-04-09T14:13:23"
picture: "125_2570.jpg"
weight: "55"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23645
- /detailsab94.html
imported:
- "2019"
_4images_image_id: "23645"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-09T14:13:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23645 -->
Das dritte und vierte Förderband dienen sowohl zum "hinbringen" der Kästchen, als auch zum Abtransport.