---
layout: "image"
title: "Antrieb Drehtisch"
date: "2008-06-29T18:30:37"
picture: "116_1647.jpg"
weight: "14"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14789
- /detailsdfb2.html
imported:
- "2019"
_4images_image_id: "14789"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-29T18:30:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14789 -->
Der neue Antrieb erfolgt über einen P-Motor 8:1.