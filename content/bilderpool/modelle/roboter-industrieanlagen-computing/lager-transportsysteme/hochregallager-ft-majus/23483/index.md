---
layout: "image"
title: "Platine"
date: "2009-03-20T18:24:53"
picture: "125_2541.jpg"
weight: "52"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/23483
- /details7956-2.html
imported:
- "2019"
_4images_image_id: "23483"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-03-20T18:24:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23483 -->
