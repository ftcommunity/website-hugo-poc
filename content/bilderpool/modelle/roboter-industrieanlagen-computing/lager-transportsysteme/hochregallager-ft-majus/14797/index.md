---
layout: "image"
title: "Drehtisch"
date: "2008-07-04T22:47:08"
picture: "116_1693.jpg"
weight: "20"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- /php/details/14797
- /details68fc-2.html
imported:
- "2019"
_4images_image_id: "14797"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-04T22:47:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14797 -->
Nochmal die Positionierung des Drehtisches.