---
layout: "image"
title: "Vorderseite"
date: "2011-04-10T18:43:01"
picture: "hrl2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30436
- /detailsee8a.html
imported:
- "2019"
_4images_image_id: "30436"
_4images_cat_id: "2263"
_4images_user_id: "373"
_4images_image_date: "2011-04-10T18:43:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30436 -->
Statt der langweiligen Ablage sollen noch weitere Modelle folgen. Mal sehen, was sich da realisieren lässt.
