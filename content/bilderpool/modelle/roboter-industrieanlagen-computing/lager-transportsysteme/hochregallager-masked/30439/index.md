---
layout: "image"
title: "Und jetzt noch..."
date: "2011-04-10T18:43:01"
picture: "hrl5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30439
- /details1e32.html
imported:
- "2019"
_4images_image_id: "30439"
_4images_cat_id: "2263"
_4images_user_id: "373"
_4images_image_date: "2011-04-10T18:43:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30439 -->
...völlig off-Topic:
Wie ein solches Foto entsteht, wenn man keine große Softbox hat. Die Sonne scheint Nachmittags genau auf diese Hauswand, während die Fläche davor bereits im Schatten liegt. Funktioniert wunderbar so. Zusätzlich habe ich mit Blitz noch etwas aufgehellt, um auch die letzten Ecken noch zu erreichen.
