---
layout: "image"
title: "Spitze"
date: "2011-04-10T18:43:01"
picture: "hrl4.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30438
- /details0f1e.html
imported:
- "2019"
_4images_image_id: "30438"
_4images_cat_id: "2263"
_4images_user_id: "373"
_4images_image_date: "2011-04-10T18:43:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30438 -->
Durch das größere Zahnrad müssen natürlich weitere Bausteine für den Abstand eingebaut werden. Außerdem eine etwas andere Motorhalterung, die stabiler und verwindungssteifer ist.
