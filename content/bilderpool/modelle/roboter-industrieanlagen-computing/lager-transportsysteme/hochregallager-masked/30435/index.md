---
layout: "image"
title: "Gesamtübersicht"
date: "2011-04-10T18:43:01"
picture: "hrl1.jpg"
weight: "1"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/30435
- /details34b7-2.html
imported:
- "2019"
_4images_image_id: "30435"
_4images_cat_id: "2263"
_4images_user_id: "373"
_4images_image_date: "2011-04-10T18:43:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30435 -->
Ein Hochregallager auf Basis des Hochregals aus dem neuen "Automation Robots".
Veränderungen gegenüber dem Bauanleitungsmodell:
- 9 statt 6 Lagerplätze, dafür das Regal um eine Raste verschoben
- Untersetzung auf der Höhenachse
- Nur 1 statt 2 Metallachsen-Führungen der Höhenachse, wäre auch ohne stabil genug
- Kugellager an allen möglichen Stellen. Ohne schafft der Motor eine Hoch-Bewegung kaum.
- Kabelführung verbessert
- Programmänderung, die belegten Fächer werden in eine Liste gespeichert (im Beispielprogramm musste das anzufahrende Fach immer manuell ausgewählt werden)
