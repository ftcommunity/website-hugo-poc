---
layout: "image"
title: "Rückseitige Einlagerung"
date: "2007-12-12T07:32:25"
picture: "hrlmitteleskoplift3.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13031
- /detailsbcf5.html
imported:
- "2019"
_4images_image_id: "13031"
_4images_cat_id: "1182"
_4images_user_id: "424"
_4images_image_date: "2007-12-12T07:32:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13031 -->
Lift um 180° gedreht
