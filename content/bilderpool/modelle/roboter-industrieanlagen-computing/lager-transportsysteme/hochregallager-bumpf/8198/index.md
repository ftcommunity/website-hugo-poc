---
layout: "image"
title: "Aufgabe"
date: "2006-12-30T09:56:05"
picture: "HRL_Fischertechnik_017.jpg"
weight: "2"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8198
- /detailsc8ce.html
imported:
- "2019"
_4images_image_id: "8198"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8198 -->
Hier werden die Kassetten noch von Hand aufs Band gelegt.
