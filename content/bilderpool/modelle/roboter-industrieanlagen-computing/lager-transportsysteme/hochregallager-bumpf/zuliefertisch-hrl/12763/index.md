---
layout: "image"
title: "Zuliefertisch HRL 1"
date: "2007-11-18T00:51:46"
picture: "bumpf1.jpg"
weight: "1"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "walter mario graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12763
- /detailse788.html
imported:
- "2019"
_4images_image_id: "12763"
_4images_cat_id: "1150"
_4images_user_id: "424"
_4images_image_date: "2007-11-18T00:51:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12763 -->
Tisch in Transportstellung
