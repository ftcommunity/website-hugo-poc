---
layout: "image"
title: "Zuliefertisch Details"
date: "2007-11-21T17:41:16"
picture: "zuliefertischdetails2.jpg"
weight: "2"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12790
- /details83c6.html
imported:
- "2019"
_4images_image_id: "12790"
_4images_cat_id: "1152"
_4images_user_id: "424"
_4images_image_date: "2007-11-21T17:41:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12790 -->
Die  Verbindungsstücke unter der Bodenplatte sind auch zugeschliffen.
