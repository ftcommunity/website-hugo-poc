---
layout: "image"
title: "Zuliefertisch HRL 3"
date: "2007-11-18T00:51:46"
picture: "bumpf3.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "walter mario graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12765
- /detailsecd8-2.html
imported:
- "2019"
_4images_image_id: "12765"
_4images_cat_id: "1150"
_4images_user_id: "424"
_4images_image_date: "2007-11-18T00:51:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12765 -->
Tisch  nach hinten ausgefahren
