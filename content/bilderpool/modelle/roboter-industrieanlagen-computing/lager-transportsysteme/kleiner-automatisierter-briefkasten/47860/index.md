---
layout: "image"
title: "Gesamt-Links"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten06.jpg"
weight: "6"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47860
- /details306b.html
imported:
- "2019"
_4images_image_id: "47860"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47860 -->
Hier sieht man deutlich die Fächer mit den Schließmotoren. Oben unter den Grauen Steinen sieht man auch den Motor für den Einwurfmechanismus. Man sieht auch, dass das ganze etwas verzogen ist...das liegt an dem roten 5er Stein unter der linken Säule. Ich weiß nicht mehr genau, warum ich den da eingebaut habe...