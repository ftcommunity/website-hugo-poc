---
layout: "image"
title: "Schalter-Vorne"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten17.jpg"
weight: "17"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47871
- /detailsc23b.html
imported:
- "2019"
_4images_image_id: "47871"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47871 -->
Das ist der immer angesprochene Schalter. Er hat schlappe Elf Stecker auf zwei Taster verteilt: Der eine für die weiterleitung des Signals, der andere ist für die Lampen zuständig. DIe Orange Lampe ist unabhängig von dem Schalter.