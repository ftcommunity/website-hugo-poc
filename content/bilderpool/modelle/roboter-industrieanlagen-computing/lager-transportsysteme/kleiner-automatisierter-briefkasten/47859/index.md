---
layout: "image"
title: "Gesamt-Rechts"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten05.jpg"
weight: "5"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47859
- /details8aee-2.html
imported:
- "2019"
_4images_image_id: "47859"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47859 -->
Vorne sieht man den Karteneinzug und den TxT, hinten die Fächer.