---
layout: "image"
title: "Kartenleser-Vorne"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten09.jpg"
weight: "9"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- /php/details/47863
- /details2edf.html
imported:
- "2019"
_4images_image_id: "47863"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47863 -->
Links ist der Ultraschallsensor zur Erkennung von Personen. Direkt daneben sind die Kontrollleuchten: Orange leuchtet bzw. blinkt bei Bearbeitung, oder Fehler, Blau leuchtet bei erkannter Karte, Grün leuchtet bei herunter gedrücktem Schalter, bzw. geöffnetem Fach. Der Schalter ist in die blaue und grüne Lampe eingebaut -> man kann die nach unten und wieder nach oben drücken.