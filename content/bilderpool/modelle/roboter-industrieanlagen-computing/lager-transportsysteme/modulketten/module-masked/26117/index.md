---
layout: "image"
title: "Seitenführung"
date: "2010-01-24T13:44:39"
picture: "module3.jpg"
weight: "3"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26117
- /detailsc572.html
imported:
- "2019"
_4images_image_id: "26117"
_4images_cat_id: "1851"
_4images_user_id: "373"
_4images_image_date: "2010-01-24T13:44:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26117 -->
Aus Verbindern 30 oder 45. Funktioniert sehr gut, wird ja auch von Staudinger angewandt - und somit Dauerbetriebs-fähig.
In der Mitte halten Bauplatten 15xXX die Ketten, da Sie ansonsten in diese Richtung kippen würden.
Das Bild ist nicht so doll, aber bei so viel schwarzen Bauteilen etwas hinzukriegen ist nicht leicht.
