---
layout: "image"
title: "Förderband ohne Ketten"
date: "2010-01-24T13:44:38"
picture: "module2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26116
- /details2b57-2.html
imported:
- "2019"
_4images_image_id: "26116"
_4images_cat_id: "1851"
_4images_user_id: "373"
_4images_image_date: "2010-01-24T13:44:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26116 -->
Eine Seitenführung und auch Stabilisierung in der Mitte musste natürlich auch her. Siehe nächstes Bild
