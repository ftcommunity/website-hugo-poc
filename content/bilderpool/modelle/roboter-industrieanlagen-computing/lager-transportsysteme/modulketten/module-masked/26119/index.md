---
layout: "image"
title: "Detailansicht Drehkranz 1"
date: "2010-01-24T13:44:39"
picture: "module5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/26119
- /detailse1b9.html
imported:
- "2019"
_4images_image_id: "26119"
_4images_cat_id: "1851"
_4images_user_id: "373"
_4images_image_date: "2010-01-24T13:44:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26119 -->
