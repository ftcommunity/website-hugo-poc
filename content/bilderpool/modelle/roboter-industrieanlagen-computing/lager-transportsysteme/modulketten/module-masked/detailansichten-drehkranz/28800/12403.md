---
layout: "comment"
hidden: true
title: "12403"
date: "2010-10-01T20:29:07"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Für Stecker in engen Zwischenräumen gibt's einen einfachen Trick: alte,
ausgeleierte Stecker, bei denen die Schräubchen nicht mehr greifen, habe
ich auf Höhe der Querbohrung durchtrennt und die Kabel fest verlötet. So
kann der eigentliche Stecker weiterhin genutzt werden. Ist nur 3 mm hoch.

Gruß, Thomas