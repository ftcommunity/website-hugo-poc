---
layout: "image"
title: "Übergang zum Nächsten Modul"
date: "2008-08-22T23:17:16"
picture: "modul2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/15070
- /detailsaa73.html
imported:
- "2019"
_4images_image_id: "15070"
_4images_cat_id: "1372"
_4images_user_id: "558"
_4images_image_date: "2008-08-22T23:17:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15070 -->
Die verbindung zwischen zwei Modulen ist gesichert wenn jeder für jede Seite 2 BS15 mit 2 Nocken Mitbringen würde. 
Ich kann mir auch gut vorstellen Doppelketten zu verwnden.