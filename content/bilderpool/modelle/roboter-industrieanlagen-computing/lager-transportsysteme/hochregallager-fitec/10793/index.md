---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-10T20:09:31"
picture: "HRL55.jpg"
weight: "49"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10793
- /details494b-2.html
imported:
- "2019"
_4images_image_id: "10793"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10793 -->
Nochmal eine Gesamtansicht, diesmal mit Einlagerer. Das Industriemodell ist über 3000 groß (1xPlatte1000 / 4xPlatte500 / kleine schwarze Platten). Wenn es verkabelt und fertig programmiert ist wird es wahrscheinlich einmal ein Video geben.
