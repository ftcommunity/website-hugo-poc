---
layout: "image"
title: "Front"
date: "2007-08-02T22:22:55"
picture: "HRL72.jpg"
weight: "60"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11232
- /detailsf573.html
imported:
- "2019"
_4images_image_id: "11232"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-08-02T22:22:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11232 -->
Hier ist die Front mit Endschalter und Impulszähler zu sehen.
