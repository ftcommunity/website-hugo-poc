---
layout: "image"
title: "Y-Achse"
date: "2007-12-02T15:46:17"
picture: "HRL82.jpg"
weight: "70"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12983
- /details9f39.html
imported:
- "2019"
_4images_image_id: "12983"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12983 -->
Die Y-Achse wird jetzt mit 2 8:1 Power-Motoren angetrieben. Beide sind noch 2:1 untersetzt. Da beide Motoren unterschiedliche Abweichungen haben, wie sie jeder Motor hat habe ich folgendes gemacht: Siehe nächtes Bild.
