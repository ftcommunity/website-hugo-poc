---
layout: "image"
title: "Z-Achse"
date: "2007-12-02T15:46:17"
picture: "HRL81.jpg"
weight: "69"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12982
- /detailseb23-2.html
imported:
- "2019"
_4images_image_id: "12982"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12982 -->
Hier hat sich nicht so viel verändert.
