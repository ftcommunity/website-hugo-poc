---
layout: "image"
title: "Laufband mit Tunnel"
date: "2007-06-04T15:48:16"
picture: "HRL38.jpg"
weight: "32"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10704
- /details9037.html
imported:
- "2019"
_4images_image_id: "10704"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10704 -->
Das Laufband besitzt einen Tunnel.
