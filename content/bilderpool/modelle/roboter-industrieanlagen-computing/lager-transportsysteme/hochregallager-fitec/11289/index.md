---
layout: "image"
title: "Antrieb"
date: "2007-08-04T14:01:04"
picture: "HRL75.jpg"
weight: "63"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11289
- /detailse3b1.html
imported:
- "2019"
_4images_image_id: "11289"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-08-04T14:01:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11289 -->
Naja, Antrieb passt gar nicht mehr so gut. Man sieht ja immoment nur die Verkleidung des Antriebs. Wie gesagt, die Achsu wurde gegen eine 30 Achse getauscht und ein 50:1 Power Motor wurde eingebaut. Der Motor wurde sehr "wackelsicher" eingebaut, damit sich bei den hohen Kräften nichts verschiebt.
