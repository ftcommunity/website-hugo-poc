---
layout: "image"
title: "Oberansicht"
date: "2007-07-17T16:52:56"
picture: "HRL69.jpg"
weight: "57"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11121
- /details74e0.html
imported:
- "2019"
_4images_image_id: "11121"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11121 -->
Hier von oben.
