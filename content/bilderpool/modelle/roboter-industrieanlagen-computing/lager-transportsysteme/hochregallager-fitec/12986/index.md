---
layout: "image"
title: "X-Achse"
date: "2007-12-02T15:46:17"
picture: "HRL85.jpg"
weight: "73"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12986
- /detailsd9ec-3.html
imported:
- "2019"
_4images_image_id: "12986"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12986 -->
Der sehr improvisatorische, nicht funktionierende Antrieb. Wer Tipps oder Ideen hat soll sie bitte nicht für sich behalten.
