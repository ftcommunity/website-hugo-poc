---
layout: "image"
title: "Kabelsalat"
date: "2007-06-16T20:59:50"
picture: "HRL57.jpg"
weight: "51"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10871
- /details0ec6-2.html
imported:
- "2019"
_4images_image_id: "10871"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-16T20:59:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10871 -->
Hier sieht man den Kabelsalat. *g*
