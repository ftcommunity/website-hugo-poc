---
layout: "image"
title: "Stromleitungen"
date: "2007-05-25T21:23:34"
picture: "HRL20.jpg"
weight: "14"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10502
- /details5fa7.html
imported:
- "2019"
_4images_image_id: "10502"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10502 -->
Hier sieht man die Stromleitungen, aus Messingachsen, für das Em vom Einlagerer.
