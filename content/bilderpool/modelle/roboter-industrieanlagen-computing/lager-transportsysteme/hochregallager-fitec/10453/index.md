---
layout: "image"
title: "Vakuumgreifer"
date: "2007-05-17T21:54:51"
picture: "HRL10.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10453
- /detailsd800.html
imported:
- "2019"
_4images_image_id: "10453"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-17T21:54:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10453 -->
