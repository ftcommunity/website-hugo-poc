---
layout: "image"
title: "Antrieb"
date: "2007-08-02T22:22:55"
picture: "HRL74.jpg"
weight: "62"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11234
- /details6335-2.html
imported:
- "2019"
_4images_image_id: "11234"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-08-02T22:22:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11234 -->
Das wird der neue Antrieb. Er bekommt natürlich noch eine 30 Achse und einen 50:1 P-mot. Danke Frederik für den Tipp.
