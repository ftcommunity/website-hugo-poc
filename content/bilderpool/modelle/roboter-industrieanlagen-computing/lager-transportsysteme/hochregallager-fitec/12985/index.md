---
layout: "image"
title: "Vakuum-Versorgung"
date: "2007-12-02T15:46:17"
picture: "HRL84.jpg"
weight: "72"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12985
- /details7de8.html
imported:
- "2019"
_4images_image_id: "12985"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12985 -->
Der Einlagerer hat jetzt seine eigene Vakuum-Versorgung.
