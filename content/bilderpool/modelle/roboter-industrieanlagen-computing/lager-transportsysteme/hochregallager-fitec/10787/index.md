---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-10T20:09:31"
picture: "HRL49.jpg"
weight: "43"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10787
- /detailsa379-2.html
imported:
- "2019"
_4images_image_id: "10787"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10787 -->
Hier sieht man meine Industrieanlage wie sie fertig ist. Außer dem Einlagerer muss ich noch alles verkabeln, am Programm bin ich gerade tätig.
