---
layout: "image"
title: "Extension"
date: "2007-07-17T16:52:55"
picture: "HRL65.jpg"
weight: "53"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11117
- /detailsd48e-3.html
imported:
- "2019"
_4images_image_id: "11117"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11117 -->
Das Extension ist nun nicht hinten, sonderm an der Seite festgemacht. Wieso wird man auf einem der nächsten Bilder sehen.
