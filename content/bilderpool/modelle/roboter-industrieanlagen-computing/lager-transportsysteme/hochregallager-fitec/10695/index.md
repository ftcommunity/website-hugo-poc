---
layout: "image"
title: "Antrieb"
date: "2007-06-04T15:48:00"
picture: "HRL29.jpg"
weight: "23"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10695
- /details89c6.html
imported:
- "2019"
_4images_image_id: "10695"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10695 -->
Der Antrieb bei dieser Achse ist durch einen 20:1 Power-Motor. Man sieht den Endschalter sowie den Impulszähler.
