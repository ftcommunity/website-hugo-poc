---
layout: "image"
title: "Vakuum-Versorgung"
date: "2007-08-09T22:02:25"
picture: "HRL78.jpg"
weight: "66"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11322
- /details3510-4.html
imported:
- "2019"
_4images_image_id: "11322"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11322 -->
Hier ist die Vakuum-Versorgung zu sehen. Ich habe es erst so versucht wie niklas, aber irgendwie hat das bei mir nicht geklappt )(siehe hier: http://www.ftcommunity.de/categories.php?cat_id=1015). Ich habe jetzt einen kleinen Tank gebaut, indem waren mal Kaugummis, der 4 Anschlüsse hat. An 2 macht man das Rückschlagventil rein und an die restlichen 2 kann man Abschaltung... anschliesen. Ich habe aber zusätzlich noch einen Air-Tankl von ft verbaut, da meine Eigenbaute relativ klein ist. Das Handventil mit dem Saugnapf war nur zum Testen, das mache ich wieder weg.
