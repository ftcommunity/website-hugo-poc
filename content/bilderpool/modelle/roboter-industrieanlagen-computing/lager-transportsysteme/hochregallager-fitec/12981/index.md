---
layout: "image"
title: "HRL"
date: "2007-12-02T15:46:17"
picture: "HRL80.jpg"
weight: "68"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12981
- /detailsc40e.html
imported:
- "2019"
_4images_image_id: "12981"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12981 -->
Neue Version. Die Y-Achse ist nahezu perfekt, die Z-Achse ist Ok, aber die X-Achse ist noch ganz und gar nicht gut.
