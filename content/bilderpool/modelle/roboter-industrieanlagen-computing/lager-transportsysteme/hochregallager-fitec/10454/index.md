---
layout: "image"
title: "Flachbandkabelhalterung"
date: "2007-05-17T21:55:04"
picture: "HRL11.jpg"
weight: "11"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10454
- /details135e-2.html
imported:
- "2019"
_4images_image_id: "10454"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-17T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10454 -->
Das ist die Halterung für das Flachbandkabel.
