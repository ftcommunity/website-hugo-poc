---
layout: "image"
title: "Rollen"
date: "2007-06-04T15:48:22"
picture: "HRL45.jpg"
weight: "39"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10711
- /detailsba88.html
imported:
- "2019"
_4images_image_id: "10711"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10711 -->
Man sieht die Rollen damit das doch sehr lange Förderband nicht durchhängt.
