---
layout: "image"
title: "Neues Laufband"
date: "2007-06-10T20:09:30"
picture: "HRL46.jpg"
weight: "40"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10784
- /details71aa.html
imported:
- "2019"
_4images_image_id: "10784"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-10T20:09:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10784 -->
Das neue Laufband zwischen Magazin und Drehkranz1 ist fertig.
