---
layout: "image"
title: "Kompressorantrieb"
date: "2008-12-16T18:07:12"
picture: "johannes31.jpg"
weight: "31"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16655
- /detailse3da-2.html
imported:
- "2019"
_4images_image_id: "16655"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16655 -->
Der Kompressor wird von einem Powermotor mit schwarzer Kappe angetrieben.
