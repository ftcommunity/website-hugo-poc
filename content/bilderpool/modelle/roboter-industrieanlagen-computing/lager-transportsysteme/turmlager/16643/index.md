---
layout: "image"
title: "Greifer von der Seite"
date: "2008-12-16T18:07:12"
picture: "johannes19.jpg"
weight: "19"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16643
- /details52cb.html
imported:
- "2019"
_4images_image_id: "16643"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16643 -->
