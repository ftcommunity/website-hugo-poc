---
layout: "image"
title: "Polwendeschalter"
date: "2008-12-16T18:07:12"
picture: "johannes30.jpg"
weight: "30"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16654
- /detailse804.html
imported:
- "2019"
_4images_image_id: "16654"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16654 -->
Der Polwendeschalter wechselt die Laufrichtung des Drehantriebes, da mir nur noch ein O-Ausgang zur Verfügung stand.
