---
layout: "image"
title: "Überwachung"
date: "2008-12-16T18:07:12"
picture: "johannes28.jpg"
weight: "28"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/16652
- /detailsd085-2.html
imported:
- "2019"
_4images_image_id: "16652"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16652 -->
