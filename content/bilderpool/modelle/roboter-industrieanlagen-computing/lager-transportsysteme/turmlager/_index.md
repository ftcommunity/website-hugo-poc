---
layout: "overview"
title: "Turmlager"
date: 2020-02-22T08:07:10+01:00
legacy_id:
- /php/categories/1506
- /categoriese14d.html
- /categoriesf993.html
- /categories9301.html
- /categoriesaaba.html
- /categories8749.html
- /categories848e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1506 --> 
von Johannes Westphal



Dieses Modell stellt ein komplettes Lager dar, das kreisförmig aufgebaut ist. Es besitzt 15 Fächer, die es zum Ein- und Auslagern ansteuern kann. Dabei kann jedes Fach auch ohne PC einzeln zum Auslagern ansgewählt werden. Die Ein- und Auslagerung erfolgt sehr präzise und mit relativ hoher Geschwindigkeit.



Technische Daten:

Länge: 80cm

Breite: 40cm

Höhe: 40cm

Anzahl der Fächer: 15



Verwendetes Material:

7 Taster

4 Motoren

3 Elektromagnetventile

6 Pneumatikzylinder

2 Fototransistoren

1 Potentiometer

4m Schlauch

8m Kabel