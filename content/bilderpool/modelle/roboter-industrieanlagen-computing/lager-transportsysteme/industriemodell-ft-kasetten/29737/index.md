---
layout: "image"
title: "Industriemodell - Kabel"
date: "2011-01-21T15:16:09"
picture: "modell16.jpg"
weight: "16"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29737
- /details3412.html
imported:
- "2019"
_4images_image_id: "29737"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:09"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29737 -->
Hier sieht man die Kabel, die von vorne nach hinten laufen.
