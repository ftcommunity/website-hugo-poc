---
layout: "image"
title: "Industriemodell - von oben"
date: "2011-01-21T15:16:09"
picture: "modell18.jpg"
weight: "18"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29739
- /details700a.html
imported:
- "2019"
_4images_image_id: "29739"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:09"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29739 -->
Das Modell von oben.
