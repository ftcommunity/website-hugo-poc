---
layout: "image"
title: "Industriemodell - Großhandtaster"
date: "2011-01-21T15:16:08"
picture: "modell04.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29725
- /detailse2c3-2.html
imported:
- "2019"
_4images_image_id: "29725"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29725 -->
"Bilder sagen mehr als 1000 Worte"
