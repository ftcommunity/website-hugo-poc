---
layout: "image"
title: "Industriemodell - Förderband"
date: "2011-01-21T15:16:08"
picture: "modell14.jpg"
weight: "14"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29735
- /details1f53-2.html
imported:
- "2019"
_4images_image_id: "29735"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29735 -->
Hier sieht man das Förderband. Am Rande des Förderbandes findet man drei Lichtschranken.
