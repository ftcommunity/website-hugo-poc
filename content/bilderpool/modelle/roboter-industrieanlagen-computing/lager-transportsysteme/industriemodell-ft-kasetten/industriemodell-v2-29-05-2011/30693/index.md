---
layout: "image"
title: "Rollenband"
date: "2011-05-29T15:10:01"
picture: "modell07.jpg"
weight: "7"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30693
- /details9020.html
imported:
- "2019"
_4images_image_id: "30693"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30693 -->
Die Rollen, auf denen die Kassette herunterrutscht.
