---
layout: "image"
title: "'Kassettenbot'"
date: "2011-05-29T15:10:01"
picture: "modell03.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30689
- /detailsa1ea.html
imported:
- "2019"
_4images_image_id: "30689"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30689 -->
Hier mein neues, altes Modell. Das ich jetzt verbessert habe mit Rollenband & Co.
