---
layout: "image"
title: "Schlüsselschalter und Statusleuchten"
date: "2011-05-29T15:10:01"
picture: "modell04.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30690
- /detailsb422.html
imported:
- "2019"
_4images_image_id: "30690"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30690 -->
Hier die Anzeige, das Blaue sind Schläuche, die in die Ritze der Steine hineingeschoben wurden.
