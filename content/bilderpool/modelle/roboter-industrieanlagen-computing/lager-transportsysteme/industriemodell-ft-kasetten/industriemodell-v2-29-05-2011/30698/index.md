---
layout: "image"
title: "Serienaufnahme"
date: "2011-05-29T15:10:01"
picture: "modell12.jpg"
weight: "12"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30698
- /detailsb8c4-2.html
imported:
- "2019"
_4images_image_id: "30698"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30698 -->
