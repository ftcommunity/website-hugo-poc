---
layout: "image"
title: "Industriemodell - Schlüsselschalter und Statuslampen"
date: "2011-01-21T15:16:07"
picture: "modell02.jpg"
weight: "2"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29723
- /details6cb4-2.html
imported:
- "2019"
_4images_image_id: "29723"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29723 -->
Hier könnt ihr den Schlüsselschalter sehen, mit dem man die Anlage ein bzw. auch wieder ausschalten kann. Daneben sind die zwei LEDs, die den Status der Maschine anzeigen. Ich habe dazu zwei Löcher in eine Flachplatte 30 gebohrt und dann die LEDs durchgesteckt.
