---
layout: "image"
title: "Industriemodell - Kassette"
date: "2011-01-21T15:16:08"
picture: "modell08.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29729
- /detailsf3d1.html
imported:
- "2019"
_4images_image_id: "29729"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29729 -->
Damit der E-Magnet die Kasette überhaubt greifen kann, wird vorne auf die Bauplatte 30*30mm noch eine dünne Metallplatte angebracht.
