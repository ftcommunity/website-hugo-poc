---
layout: "image"
title: "Industriemodell - E-Magnet"
date: "2011-01-21T15:16:08"
picture: "modell07.jpg"
weight: "7"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29728
- /detailse6d4-2.html
imported:
- "2019"
_4images_image_id: "29728"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29728 -->
von der Seite
