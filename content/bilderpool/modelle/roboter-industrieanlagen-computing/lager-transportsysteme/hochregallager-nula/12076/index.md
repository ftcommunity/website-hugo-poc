---
layout: "image"
title: "HRL (7)"
date: "2007-10-03T08:48:26"
picture: "hrl07.jpg"
weight: "7"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12076
- /details418c-2.html
imported:
- "2019"
_4images_image_id: "12076"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12076 -->
Im Hintergrund sieht man das Bedienfeld. Auf ihm kann man den Befehl geben, eine bestimmte Kiste herauszugeben.