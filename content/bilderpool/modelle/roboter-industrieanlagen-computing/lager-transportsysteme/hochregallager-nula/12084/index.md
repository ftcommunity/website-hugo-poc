---
layout: "image"
title: "HRL (15)"
date: "2007-10-03T08:48:26"
picture: "hrl15.jpg"
weight: "15"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12084
- /detailsb871.html
imported:
- "2019"
_4images_image_id: "12084"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12084 -->
Die Befestigung aus einer anderen Sicht.