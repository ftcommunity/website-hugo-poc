---
layout: "image"
title: "HRL (24)"
date: "2007-10-03T08:48:26"
picture: "hrl24.jpg"
weight: "24"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12093
- /detailsca3d.html
imported:
- "2019"
_4images_image_id: "12093"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12093 -->
Man sieht, wie ich den Antrieb um den Arm drumherum gebaut habe.