---
layout: "image"
title: "HRL (22)"
date: "2007-10-03T08:48:26"
picture: "hrl22.jpg"
weight: "22"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12091
- /details9107.html
imported:
- "2019"
_4images_image_id: "12091"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12091 -->
Die Führung des Greifarms.