---
layout: "image"
title: "HRL (6)"
date: "2007-10-03T08:48:26"
picture: "hrl06.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12075
- /details343a.html
imported:
- "2019"
_4images_image_id: "12075"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12075 -->
Der Einlagrer von hinten.