---
layout: "image"
title: "HRL (26)"
date: "2007-10-03T08:48:26"
picture: "hrl26.jpg"
weight: "26"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12095
- /detailsd6f9.html
imported:
- "2019"
_4images_image_id: "12095"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12095 -->
Der Antrieb erfolgt durch einen MiniMot.