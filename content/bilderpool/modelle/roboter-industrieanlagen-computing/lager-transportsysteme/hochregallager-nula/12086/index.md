---
layout: "image"
title: "HRL (17)"
date: "2007-10-03T08:48:26"
picture: "hrl17.jpg"
weight: "17"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12086
- /details11ff.html
imported:
- "2019"
_4images_image_id: "12086"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12086 -->
Der Antrieb der Hochachse geschieht durch einen 20:1 Powermotor.
Ich habe mich für zwei Alus entschieden, da diese die Konstruktion schön stabil machen.