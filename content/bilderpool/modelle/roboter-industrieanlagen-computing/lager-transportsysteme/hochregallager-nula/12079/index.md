---
layout: "image"
title: "HRL (10)"
date: "2007-10-03T08:48:26"
picture: "hrl10.jpg"
weight: "10"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12079
- /details33a9-2.html
imported:
- "2019"
_4images_image_id: "12079"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12079 -->
Nochmal die Schienen. Der Schlitten/Einlagerer hat keinen eigenen Antrieb, sondern wird von einer Kette hin- und hergezogen.