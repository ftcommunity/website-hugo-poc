---
layout: "image"
title: "HRL (27)"
date: "2007-10-03T08:48:26"
picture: "hrl27.jpg"
weight: "27"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12096
- /details105a.html
imported:
- "2019"
_4images_image_id: "12096"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12096 -->
Auf das Förderband wird die Kiste gestellt, die einsortiert werden soll.