---
layout: "image"
title: "HRL (11) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau11.jpg"
weight: "11"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25387
- /details67d3.html
imported:
- "2019"
_4images_image_id: "25387"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25387 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.