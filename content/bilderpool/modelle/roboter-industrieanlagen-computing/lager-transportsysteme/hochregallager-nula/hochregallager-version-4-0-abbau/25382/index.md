---
layout: "image"
title: "HRL (6) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau06.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25382
- /detailsf1b5-2.html
imported:
- "2019"
_4images_image_id: "25382"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25382 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.