---
layout: "image"
title: "HRL (3) von oben"
date: "2008-06-04T18:44:51"
picture: "hochregallager03.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/14619
- /details794a-2.html
imported:
- "2019"
_4images_image_id: "14619"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14619 -->
Draufsicht. Mit den Kabeln zum Einlagerer muss ich mir noch was einfallen lassen, ich habe jetzt einfach Strohälme drumgeklebt.