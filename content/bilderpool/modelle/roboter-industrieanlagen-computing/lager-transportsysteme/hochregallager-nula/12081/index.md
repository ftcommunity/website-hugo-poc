---
layout: "image"
title: "HRL (12)"
date: "2007-10-03T08:48:26"
picture: "hrl12.jpg"
weight: "12"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12081
- /detailse637.html
imported:
- "2019"
_4images_image_id: "12081"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12081 -->
Gesammtansicht vom Antrieb.
Die Konstruktion des Impulstasters hat etwas gewackelt, ich habe dies jetzt verbessert!
siehe HRL (38)!