---
layout: "image"
title: "HRL (9)"
date: "2007-10-03T08:48:26"
picture: "hrl09.jpg"
weight: "9"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12078
- /details13ce.html
imported:
- "2019"
_4images_image_id: "12078"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12078 -->
Auf diesem Bild sieht man die Schienen, auf denen der Einlagerer hin und herfährt.