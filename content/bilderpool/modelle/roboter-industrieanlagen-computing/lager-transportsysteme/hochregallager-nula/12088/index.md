---
layout: "image"
title: "HRL (19)"
date: "2007-10-03T08:48:26"
picture: "hrl19.jpg"
weight: "19"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12088
- /details1139-4.html
imported:
- "2019"
_4images_image_id: "12088"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12088 -->
Hier sieht man den Impulstaster.