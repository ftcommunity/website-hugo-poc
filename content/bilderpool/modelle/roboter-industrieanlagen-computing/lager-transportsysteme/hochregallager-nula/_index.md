---
layout: "overview"
title: "Hochregallager (nula)"
date: 2020-02-22T08:06:30+01:00
legacy_id:
- /php/categories/1080
- /categoriese313.html
- /categoriesa1dd.html
- /categories4b20-2.html
- /categoriesdc84.html
- /categories6899.html
- /categories56a5.html
- /categoriesae70.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1080 --> 
Über ein Förderband werden Fischertechnikkisten zugeführt und automatisch in einen freien Platz im Hochregal eingelagert. Per Mausklick kann jede Kiste auch wieder ausgelagert werden.