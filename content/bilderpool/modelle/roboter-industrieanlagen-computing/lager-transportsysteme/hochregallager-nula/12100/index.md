---
layout: "image"
title: "HRL (31)"
date: "2007-10-03T08:48:27"
picture: "hrl31.jpg"
weight: "31"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/12100
- /details51c2-2.html
imported:
- "2019"
_4images_image_id: "12100"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:27"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12100 -->
Die Verkabelung. Ich habe ein Fachbandkabel gewählt, da sonst ein Kabelsalat entstehen würde *würg*