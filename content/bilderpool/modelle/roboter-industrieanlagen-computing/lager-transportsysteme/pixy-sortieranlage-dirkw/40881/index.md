---
layout: "image"
title: "Vakuumerzeugung mit 2 Pneumatikzylindern"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40881
- /detailsbbcb.html
imported:
- "2019"
_4images_image_id: "40881"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40881 -->
Die Vakuumerzeugung.
