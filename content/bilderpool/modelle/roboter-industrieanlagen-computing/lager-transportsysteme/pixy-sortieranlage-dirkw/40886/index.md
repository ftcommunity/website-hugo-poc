---
layout: "image"
title: "Pixy Camera"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40886
- /detailsa0ca.html
imported:
- "2019"
_4images_image_id: "40886"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40886 -->
Links vom Magazin gesehen.
