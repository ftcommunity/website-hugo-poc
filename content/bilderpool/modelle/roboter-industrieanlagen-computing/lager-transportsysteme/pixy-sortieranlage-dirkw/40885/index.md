---
layout: "image"
title: "Vakuumgreifer"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40885
- /details9d19.html
imported:
- "2019"
_4images_image_id: "40885"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40885 -->
Der Vakuumgreifer mit Dämpfungsfeder für das Greifen der Werkstücke.
Der Prüftisch und im Hintergrund die Pixy-Camera.
