---
layout: "image"
title: "Display LCD 2004"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40878
- /detailsbccc.html
imported:
- "2019"
_4images_image_id: "40878"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40878 -->
Alle Meldungen werden über Variablen abgerufen und angezeigt. 
Statusanzeige der Station.
