---
layout: "image"
title: "Magazin mit Steinen"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40880
- /detailsca28.html
imported:
- "2019"
_4images_image_id: "40880"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40880 -->
Das Magazin wird über die Lichtschranke geprüft.
