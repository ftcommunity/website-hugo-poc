---
layout: "image"
title: "RoboPro"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40892
- /details71b9-3.html
imported:
- "2019"
_4images_image_id: "40892"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40892 -->
Das Unterprogramm Farberkennung.
