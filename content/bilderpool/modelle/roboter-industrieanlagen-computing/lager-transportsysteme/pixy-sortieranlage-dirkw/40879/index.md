---
layout: "image"
title: "Robo TX Controller"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40879
- /detailsffea-2.html
imported:
- "2019"
_4images_image_id: "40879"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40879 -->
