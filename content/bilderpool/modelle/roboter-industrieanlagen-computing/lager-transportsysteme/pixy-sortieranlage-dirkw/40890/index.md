---
layout: "image"
title: "Meldungen im LCD Display"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40890
- /detailsf652.html
imported:
- "2019"
_4images_image_id: "40890"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40890 -->
Hier eine kleine Übersicht der einzelnen Meldungen aus dem Display.
