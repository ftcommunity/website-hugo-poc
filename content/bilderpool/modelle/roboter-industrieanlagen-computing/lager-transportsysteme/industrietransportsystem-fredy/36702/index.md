---
layout: "image"
title: "Industrietransportsystem"
date: "2013-02-27T20:52:49"
picture: "industrie2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36702
- /details4bd8-2.html
imported:
- "2019"
_4images_image_id: "36702"
_4images_cat_id: "2721"
_4images_user_id: "453"
_4images_image_date: "2013-02-27T20:52:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36702 -->
