---
layout: "image"
title: "Turmregallager von oben (3)"
date: "2013-09-23T21:35:22"
picture: "turmregallager03.jpg"
weight: "3"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37416
- /details2568.html
imported:
- "2019"
_4images_image_id: "37416"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37416 -->
Die S-Laufschiene (36333) dienen als führung für den arm der auf rollen gelegert ist.