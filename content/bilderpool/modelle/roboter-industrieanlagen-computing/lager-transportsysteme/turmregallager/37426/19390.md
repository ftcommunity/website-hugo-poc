---
layout: "comment"
hidden: true
title: "19390"
date: "2014-08-12T08:06:41"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hast du aus Platzgründen den Power Controller dazwischen? Das Netzteil könntest du auch direkt in den TXC stecken :)