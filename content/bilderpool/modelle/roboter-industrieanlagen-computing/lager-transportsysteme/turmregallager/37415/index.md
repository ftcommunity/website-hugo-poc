---
layout: "image"
title: "Turm oberteil (2)"
date: "2013-09-23T21:35:22"
picture: "turmregallager02.jpg"
weight: "2"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37415
- /detailsaa0b.html
imported:
- "2019"
_4images_image_id: "37415"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37415 -->
Die Seilrollen lenken die seile um das der arm leichter angehoben werden kann.