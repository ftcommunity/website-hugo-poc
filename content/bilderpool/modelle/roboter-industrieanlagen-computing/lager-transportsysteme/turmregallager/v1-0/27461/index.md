---
layout: "image"
title: "Steuerungsmodul"
date: "2010-06-11T18:37:54"
picture: "Steuerungsmodul.jpg"
weight: "12"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
schlagworte: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- /php/details/27461
- /detailsa71d.html
imported:
- "2019"
_4images_image_id: "27461"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27461 -->
Hier seht ihr als Steuerungsmodul den neuen Robo TX Controller.