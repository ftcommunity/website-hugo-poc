---
layout: "image"
title: "Verstärktes Regal"
date: "2014-08-11T22:13:27"
picture: "turmregallagerverstaerkt1.jpg"
weight: "1"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/39237
- /details0cbf-2.html
imported:
- "2019"
_4images_image_id: "39237"
_4images_cat_id: "2935"
_4images_user_id: "1463"
_4images_image_date: "2014-08-11T22:13:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39237 -->
Diese Bilder wurden nach einer weiteren Verändereung von mir aufgenommen wo rauf man sehen kan wie ich das Regalsystem verstärkt habe um es in sich selbst stabiler zu machen. 
Was die Programirung des Lagers um ein vielfaches erleichterte, da von nun an die Regale sich nun nicht mehr so leicht verbigen konten.