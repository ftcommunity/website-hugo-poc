---
layout: "image"
title: "Abholplatz + Packet (14)"
date: "2013-09-23T21:35:22"
picture: "turmregallager14.jpg"
weight: "14"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- /php/details/37427
- /detailsdb07-2.html
imported:
- "2019"
_4images_image_id: "37427"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37427 -->
Ein Packet das im Abholplatz bereit liegt.