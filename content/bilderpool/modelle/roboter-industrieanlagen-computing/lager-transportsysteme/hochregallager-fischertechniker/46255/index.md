---
layout: "image"
title: "Gegengewicht"
date: "2017-09-17T18:02:23"
picture: "hochregallager07.jpg"
weight: "7"
konstrukteure: 
- "Alwin (fischertechniker)"
fotografen:
- "Alwin (fischertechniker)"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46255
- /details3fc5.html
imported:
- "2019"
_4images_image_id: "46255"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46255 -->
Das RBG braucht ein Gegengewicht wegen dem Motor
