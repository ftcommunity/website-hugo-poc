---
layout: "comment"
hidden: true
title: "23629"
date: "2017-09-17T18:26:54"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
An deinen Kabeln wirst du nicht lange Freude haben. Die baumeln ja alle nur noch an zwei oder drei Kupferäderchen herum. Tipp für einen verregneten Nachmittag: alle Kabel glatt abschneiden und neu konfektionieren, und zwar so, dass ein Stück Isolation mit ins Steckergehäuse kommt und von der Schraube festgeklemmt wird. Auch für die Schraube im ft-Stecker gilt: nach fest kommt lose --> wer zu fest andreht, quetscht Kupfer und Isolation ab, und darf nochmal anfangen.

Gruß,
Harald