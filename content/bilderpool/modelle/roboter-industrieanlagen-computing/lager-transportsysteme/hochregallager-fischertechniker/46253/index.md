---
layout: "image"
title: "Hoch und runter"
date: "2017-09-17T18:02:23"
picture: "hochregallager05.jpg"
weight: "5"
konstrukteure: 
- "Alwin (fischertechniker)"
fotografen:
- "Alwin (fischertechniker)"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46253
- /details4caa.html
imported:
- "2019"
_4images_image_id: "46253"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46253 -->
Das hoch- und runter fahren geschieh über eine Schnecke
