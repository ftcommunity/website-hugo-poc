---
layout: "image"
title: "Schienen und Endtaster"
date: "2017-09-17T18:02:23"
picture: "hochregallager09.jpg"
weight: "9"
konstrukteure: 
- "Alwin (fischertechniker)"
fotografen:
- "Alwin (fischertechniker)"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46257
- /detailsd03c.html
imported:
- "2019"
_4images_image_id: "46257"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46257 -->
Hier ist der Endtaster für das 2. Regal
