---
layout: "image"
title: "raus- und reinfahreinheit"
date: "2017-09-17T18:02:23"
picture: "hochregallager04.jpg"
weight: "4"
konstrukteure: 
- "Alwin (fischertechniker)"
fotografen:
- "Alwin (fischertechniker)"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- /php/details/46252
- /details2766.html
imported:
- "2019"
_4images_image_id: "46252"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46252 -->
Als Getriebe habe ich eins von den alten mini-motor Getrieben genommen die haben oben eine Führungsschiene für die alten Zahnradschienen.
