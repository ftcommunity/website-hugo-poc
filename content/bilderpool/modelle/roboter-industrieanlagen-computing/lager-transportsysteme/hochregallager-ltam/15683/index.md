---
layout: "image"
title: "18"
date: "2008-09-30T09:59:36"
picture: "hochregallagerltam18.jpg"
weight: "18"
konstrukteure: 
- "RS Components"
fotografen:
- "Claude Wolmering"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/15683
- /details0709-2.html
imported:
- "2019"
_4images_image_id: "15683"
_4images_cat_id: "1440"
_4images_user_id: "473"
_4images_image_date: "2008-09-30T09:59:36"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15683 -->
Hochregallager mit Ein- und Auslieferung, Prüfung und Sortierung der eingelagerten Produkten