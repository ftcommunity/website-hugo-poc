---
layout: "image"
title: "60"
date: "2008-09-30T09:59:36"
picture: "hochregallagerltam60.jpg"
weight: "60"
konstrukteure: 
- "RS Components"
fotografen:
- "Claude Wolmering"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/15725
- /details7edb.html
imported:
- "2019"
_4images_image_id: "15725"
_4images_cat_id: "1440"
_4images_user_id: "473"
_4images_image_date: "2008-09-30T09:59:36"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15725 -->
Hochregallager mit Ein- und Auslieferung, Prüfung und Sortierung der eingelagerten Produkten