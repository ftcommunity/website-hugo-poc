---
layout: "image"
title: "65"
date: "2008-09-30T09:59:36"
picture: "hochregallagerltam65.jpg"
weight: "65"
konstrukteure: 
- "RS Components"
fotografen:
- "Claude Wolmering"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/15730
- /details00c3.html
imported:
- "2019"
_4images_image_id: "15730"
_4images_cat_id: "1440"
_4images_user_id: "473"
_4images_image_date: "2008-09-30T09:59:36"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15730 -->
Hochregallager mit Ein- und Auslieferung, Prüfung und Sortierung der eingelagerten Produkten