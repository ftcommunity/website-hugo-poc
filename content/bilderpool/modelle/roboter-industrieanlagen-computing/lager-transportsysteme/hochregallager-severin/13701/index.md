---
layout: "image"
title: "Der Schlitten von oben"
date: "2008-02-19T17:20:51"
picture: "hochregallager3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13701
- /detailseee9.html
imported:
- "2019"
_4images_image_id: "13701"
_4images_cat_id: "1260"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13701 -->
