---
layout: "image"
title: "Joystick 05"
date: "2010-05-15T23:49:44"
picture: "joystick05.jpg"
weight: "43"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27232
- /details1453.html
imported:
- "2019"
_4images_image_id: "27232"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27232 -->
von unten