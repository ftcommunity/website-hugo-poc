---
layout: "image"
title: "Innenleben des Steuerpultes"
date: "2006-10-16T19:01:32"
picture: "Steuerpult_004.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7202
- /detailse421.html
imported:
- "2019"
_4images_image_id: "7202"
_4images_cat_id: "691"
_4images_user_id: "453"
_4images_image_date: "2006-10-16T19:01:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7202 -->
