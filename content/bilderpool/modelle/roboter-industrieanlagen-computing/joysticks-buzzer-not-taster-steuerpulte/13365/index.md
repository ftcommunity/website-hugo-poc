---
layout: "image"
title: "Koordinatenschalter (Joy-Stick)"
date: "2008-01-22T06:32:23"
picture: "koordinatenschalter2.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13365
- /detailsf297.html
imported:
- "2019"
_4images_image_id: "13365"
_4images_cat_id: "909"
_4images_user_id: "424"
_4images_image_date: "2008-01-22T06:32:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13365 -->
