---
layout: "image"
title: "Joystick 08"
date: "2010-05-15T23:49:44"
picture: "joystick08.jpg"
weight: "46"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/27235
- /detailsc7c2.html
imported:
- "2019"
_4images_image_id: "27235"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27235 -->
Hier ein Verwendungsbeispiel. Ich habe hier drei solcher Joysticks verbaut, wobei einer nur in zwei Richtungen bewegt werden kann.