---
layout: "image"
title: "joystick_13"
date: "2008-05-09T10:29:41"
picture: "joystick_13.jpg"
weight: "27"
konstrukteure: 
- "pk"
fotografen:
- "pk"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/14491
- /details9f7c-2.html
imported:
- "2019"
_4images_image_id: "14491"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-09T10:29:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14491 -->
Dies ist das vorbild von meinen ft-joystick:
Der Spectravideo Quickshot 2 plus.
Der schiebeschalter ist der "autofire" schalter.