---
layout: "image"
title: "Koordinatenschalter"
date: "2008-01-23T21:40:59"
picture: "Koordinatenschalter-2_002.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13372
- /details6a18.html
imported:
- "2019"
_4images_image_id: "13372"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2008-01-23T21:40:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13372 -->
