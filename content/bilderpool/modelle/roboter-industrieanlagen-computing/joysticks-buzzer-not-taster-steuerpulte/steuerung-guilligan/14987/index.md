---
layout: "image"
title: "Steuerung 3"
date: "2008-08-03T09:13:55"
picture: "steuerung3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14987
- /details959f.html
imported:
- "2019"
_4images_image_id: "14987"
_4images_cat_id: "1366"
_4images_user_id: "389"
_4images_image_date: "2008-08-03T09:13:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14987 -->
In jedem Kasten  ist ein Wippschalter 2 polig(tastend - rastend - tastend) eingebaut und 
jeweils 2 Buchsen auf jeder Stirnseite für den Ein- und Ausgang.
Hier ist der Eingang zu sehen.
