---
layout: "comment"
hidden: true
title: "17875"
date: "2013-03-15T15:20:36"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

Einige Monaten her hat meine Sohn Antonie mir eine Nunchuk-Nachbauten 
gegeben weil ich die FT-Stecker montieren möchte.
Die functionierte einwandfrei,  wie auch beim FT-Community 03.11.12 20:22 gemeldet.
Das war mit dem Wissen von heute denn aber eigentlich "zufall".............

Heute habe ich eine Wii-chuck-Adapter genutzt für eine Versuch mit die 
original Nintendo-Wii-Nunchuck. Dieser funtioniert nicht.

Es wäre schön wenn das Problem im Level-Shift gelösst wäre - die Dioden
reduzieren zwar den VCC, aber korrigieren nicht die Signallevel.

Auch für de Fischerwerken, weil es dann mehrere Anwendungmöglichkeiten gibt 
mit dem TX.

Grüss,

Peter
Poederoyen NL