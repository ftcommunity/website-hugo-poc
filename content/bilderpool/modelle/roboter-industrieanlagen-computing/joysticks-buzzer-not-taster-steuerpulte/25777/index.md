---
layout: "image"
title: "Joysticks JX-1D15F1500  + Pneumatik-3D-Slurf"
date: "2009-11-13T21:18:51"
picture: "Pneumatik-3D-Slurf_008_2.jpg"
weight: "37"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25777
- /detailsdedd-2.html
imported:
- "2019"
_4images_image_id: "25777"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2009-11-13T21:18:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25777 -->
Low cost switch joysticks.
Lieferant : APEM
