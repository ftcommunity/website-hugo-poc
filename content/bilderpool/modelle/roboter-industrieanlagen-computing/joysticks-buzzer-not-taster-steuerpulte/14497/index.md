---
layout: "image"
title: "joystick_19"
date: "2008-05-09T10:45:07"
picture: "joystick_19.jpg"
weight: "33"
konstrukteure: 
- "pk"
fotografen:
- "pk"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/14497
- /details58d3.html
imported:
- "2019"
_4images_image_id: "14497"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-09T10:45:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14497 -->
Innenansicht von das oberteil.
In das kugelformige loch dreht das kugelformige teil der stick. Damit ist der gardanische aufhängung gewehrleisted.