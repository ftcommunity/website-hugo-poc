---
layout: "image"
title: "Mini-Schalter"
date: "2010-01-28T18:52:33"
picture: "Mini-Schalter.jpg"
weight: "38"
konstrukteure: 
- "ich nicht"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26178
- /details2f2e-2.html
imported:
- "2019"
_4images_image_id: "26178"
_4images_cat_id: "909"
_4images_user_id: "328"
_4images_image_date: "2010-01-28T18:52:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26178 -->
Oftmals wünsche ich mir einfach einen kleinen und flexiblen Ein/Aus-Schalter, aber Fischertechnik bietet da leider bislang nichts.

Der klassische Polwendeschalter ist mir oft zu groß und unflexibel einbaubar, unergonomisch und fummelig bedienbar und nicht mechanisch ansteuerbar.

Der hier gezeigte Mini-Schalter wurde zwar hier und da schonmal verwendet, ist aber meiner Meinung nach so gut, dass er mal explizit gezeigt werden sollte! ;o)

Er ist winzig, lässt sich nahezu überall flexibel einbauen, ist leicht und sicher bedienbar und kann bei Bedarf über eine Anbindung am Ende der Statik-Strebe angesteuert werden.