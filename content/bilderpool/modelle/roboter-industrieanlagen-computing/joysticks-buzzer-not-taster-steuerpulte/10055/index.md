---
layout: "image"
title: "Joistik 1"
date: "2007-04-11T17:01:07"
picture: "joistiks1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10055
- /detailsb729.html
imported:
- "2019"
_4images_image_id: "10055"
_4images_cat_id: "909"
_4images_user_id: "445"
_4images_image_date: "2007-04-11T17:01:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10055 -->
Das ist der Joistik der sich bei mir am besten bewährt hat, Er ist theoretisch eine Grundform.