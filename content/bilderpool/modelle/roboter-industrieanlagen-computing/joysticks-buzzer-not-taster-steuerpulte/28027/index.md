---
layout: "image"
title: "Joystick"
date: "2010-09-02T18:58:17"
picture: "magjoy1.jpg"
weight: "49"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28027
- /details616b-2.html
imported:
- "2019"
_4images_image_id: "28027"
_4images_cat_id: "909"
_4images_user_id: "558"
_4images_image_date: "2010-09-02T18:58:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28027 -->
Ein kleiner Joystick, Funktion ist ja erkennbar :-)