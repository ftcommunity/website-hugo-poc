---
layout: "image"
title: "Koordinatenschalter"
date: "2008-01-23T21:41:00"
picture: "Koordinatenschalter-2_004.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13374
- /details0d61.html
imported:
- "2019"
_4images_image_id: "13374"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2008-01-23T21:41:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13374 -->
