---
layout: "image"
title: "Das Türsystem"
date: "2013-05-27T15:45:22"
picture: "Splmaschine3.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/36993
- /detailsffdf.html
imported:
- "2019"
_4images_image_id: "36993"
_4images_cat_id: "2751"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36993 -->
Hier seht ihr den Stoßdämpfer aus einem Zylinder und einem versperrten Rückschlagventil. Dieses versperrt die Luft, die Abfließen soll, da es aber immer Undichtigkeiten gibt, passiert das aber trotzdem und die Tür geht langsamer auf. Schließt man sie wieder lässt das Ventil ungehindert die Luft durch.
Oben links seht ihr den Verschlussmechanismus der Tür.