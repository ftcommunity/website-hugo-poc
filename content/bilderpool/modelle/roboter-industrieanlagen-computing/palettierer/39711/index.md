---
layout: "image"
title: "Palettierer Gesamtansicht"
date: "2014-10-20T21:59:38"
picture: "1_-_Palettierer_Gesamtansicht.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["Palettierer"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39711
- /detailsa024.html
imported:
- "2019"
_4images_image_id: "39711"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39711 -->
Hier gibt es das Video dazu:
https://www.youtube.com/watch?v=T0BRMUsy0c0&list=UUN792LdoJAkQi9-rPmfzWIQ
