---
layout: "image"
title: "Detail Dreh- und Hubtisch"
date: "2014-10-20T21:59:39"
picture: "7_-_Detail_Dreh-_und_Hubtisch.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39717
- /details33b5.html
imported:
- "2019"
_4images_image_id: "39717"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39717 -->
Hier sieht man, wie der Hubtisch funktioniert:
Die 2 Zylinder heben den Drehkranz mitsamt dem Motor und den 2 Endschaltern hoch.
Dann dreht der Motor bis zum entsprechenden Endschalter. Stoppt.
Und dann geht der Tisch wieder runter.
