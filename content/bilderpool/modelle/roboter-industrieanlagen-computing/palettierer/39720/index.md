---
layout: "image"
title: "Detail Antrieb Fallboden"
date: "2014-10-20T21:59:39"
picture: "10_-_Detail_Antrieb_Fallboden.jpg"
weight: "10"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39720
- /details6321-2.html
imported:
- "2019"
_4images_image_id: "39720"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39720 -->
1 Hubgetriebe auf jeder Seite öffnet den Fallboden.
