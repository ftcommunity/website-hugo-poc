---
layout: "image"
title: "Ergebnis - fertige Palette"
date: "2014-10-20T21:59:39"
picture: "13_-_Ergebnis_-_fertige_Palette.jpg"
weight: "13"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39723
- /details32f7-2.html
imported:
- "2019"
_4images_image_id: "39723"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39723 -->
So sieht das Ganze dann fertig aus.
Es ist stabil wie ein gut gebautes Legohaus (versetzte Steine) und kann mit dem Gabelstapler oder auf Neudeutsch Flurförderfahrzeug abtransportiert werden.
