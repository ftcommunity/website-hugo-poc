---
layout: "image"
title: "Detail Drehkreuz"
date: "2014-10-20T21:59:39"
picture: "6_-_Detail_Drehkreuz.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39716
- /detailsc3aa.html
imported:
- "2019"
_4images_image_id: "39716"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39716 -->
Hiermit werden die Schachteln bei Bedarf angehoben und um 90° gedreht.
An der Lichtschranke stoppen die Schachteln (Lichtschranke unterbrochen).
Bzw. werden gezählt (Lichtschranke unterbrochen und wieder frei).
