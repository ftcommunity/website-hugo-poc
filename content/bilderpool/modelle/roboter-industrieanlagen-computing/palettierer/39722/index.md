---
layout: "image"
title: "Detail Auslagerer mit Palette"
date: "2014-10-20T21:59:39"
picture: "12_-_Detail_Auslagerer_mit_Palette.jpg"
weight: "12"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39722
- /detailsd7e6.html
imported:
- "2019"
_4images_image_id: "39722"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39722 -->
Der Lift fährt runter.
Und dann wird die Palette aus dem Modell rausgefahren.
