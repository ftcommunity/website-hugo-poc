---
layout: "image"
title: "Palettierer mit geöffnetem Fallboden"
date: "2014-10-20T21:59:38"
picture: "3_-_Palettierer_mit_geffnetem_Fallboden.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/39713
- /details83d9.html
imported:
- "2019"
_4images_image_id: "39713"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39713 -->
