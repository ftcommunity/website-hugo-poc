---
layout: "image"
title: "Computer-Bedienfeld"
date: 2021-04-03T19:41:23+02:00
picture: "IMG_3046.JPG"
weight: "5"
konstrukteure: 
- "Stefan Sedlmair"
fotografen:
- "Stefan Sedlmair"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der ROBOPro-GUI wird jeweils der Fortschritt im Palettiervorgang sowie die Lagerauslastung angezeigt. Hier wird der Vorgang auch gestartet und gesteuert, die zu beladende Palette und deren Menge an 5er-Lagen angegeben.