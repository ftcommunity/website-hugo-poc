---
layout: "image"
title: "Dreheinrichtung"
date: 2021-04-03T19:41:28+02:00
picture: "IMG_3042.JPG"
weight: "3"
konstrukteure: 
- "Stefan Sedlmair"
fotografen:
- "Stefan Sedlmair"
uploadBy: "Website-Team"
license: "unknown"
---

Nach der Ausgabe aus dem Magazin werden die Werkstücke je nach Bedarf gedreht, oder nur gerade auf dem Band positioniert, indem die drei Greifarme nach unten drücken und den Block von 3 Seiten positionieren.