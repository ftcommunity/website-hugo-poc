---
layout: "overview"
title: "Palettierer mit Lagereinheit"
date: 2021-04-03T19:41:20+02:00
---

Hier ein paar Bilder meines neuesten Modells, dem Palettierer. Er kann Paletten mit 1 bis 3 Schichten mit je 5 Werkstücken aufrichten und einlagern. Bei Fragen bitte einfach kontaktieren (Im Forum sedl.stef per DN), genaueres könnt ihr dem Video entnehmen: https://youtu.be/DIIbEnZ7RrI
