---
layout: "image"
title: "Lager"
date: 2021-04-03T19:41:30+02:00
picture: "IMG_3040.JPG"
weight: "2"
konstrukteure: 
- "Stefan Sedlmair"
fotografen:
- "Stefan Sedlmair"
uploadBy: "Website-Team"
license: "unknown"
---

Hier werden Paletten jeweils vor und nach dem Beladevorgang aufgenommen und danach abgestellt.