---
layout: "image"
title: "Robo Bug"
date: "2009-09-18T20:32:47"
picture: "sm_roach2.jpg"
weight: "5"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "BRAIN", "Robo", "Bug"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24927
- /details256e.html
imported:
- "2019"
_4images_image_id: "24927"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-18T20:32:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24927 -->
This is a light-fleeing robot integrating the PCS BRAIN. This was an early version of the model.