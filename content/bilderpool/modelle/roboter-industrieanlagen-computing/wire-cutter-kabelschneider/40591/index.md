---
layout: "image"
title: "zerlegt"
date: "2015-02-22T18:01:07"
picture: "DS_05_zerlegt.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/40591
- /details04a7.html
imported:
- "2019"
_4images_image_id: "40591"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T18:01:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40591 -->
Um das Ganze besser nachbauen zu können, in zwei Teile zerlegt.
