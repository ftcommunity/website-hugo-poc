---
layout: "overview"
title: "Wire Cutter / Kabelschneider"
date: 2020-02-22T08:08:50+01:00
legacy_id:
- /php/categories/3037
- /categories94a8.html
- /categoriesddb0.html
- /categories9ace.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3037 --> 
Halbautomatischer Kabel- / Litzenschneider
Man stellt Länge und Menge (am TX-Controller) ein
Dann wird die Litze um den eingestellten Wert weiter transportiert
Nun kann man die Litze mit dem Seitenscheider abschneiden