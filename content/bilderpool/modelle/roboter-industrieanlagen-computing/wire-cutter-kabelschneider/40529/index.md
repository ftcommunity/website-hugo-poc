---
layout: "image"
title: "Wire Cutter / Kabelschneider"
date: "2015-02-12T22:59:16"
picture: "1_Wire_Cutter.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40529
- /details695f.html
imported:
- "2019"
_4images_image_id: "40529"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40529 -->
Halbautomatischer Kabel- / Litzenschneider
Man stellt Länge und Menge ein
Dann wird die Litze um das eingestellte Maß weitertransportiert
Nun kann die Litze mit dem Seitenschneider abgeschnitten werden
