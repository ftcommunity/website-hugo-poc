---
layout: "image"
title: "Die andere Seite"
date: "2015-02-22T18:01:07"
picture: "DS_04_Ausgang.jpg"
weight: "10"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/40590
- /details2aa5.html
imported:
- "2019"
_4images_image_id: "40590"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T18:01:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40590 -->
