---
layout: "image"
title: "Stanleymesser - die Quelle für das Messer"
date: "2015-02-22T18:01:07"
picture: "DS_01_Klinge.jpg"
weight: "7"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/40587
- /details655b.html
imported:
- "2019"
_4images_image_id: "40587"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T18:01:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40587 -->
Ich denke, daß die folgenden Bilder den Kabelschneider von Laserman ergänzen könnten.
