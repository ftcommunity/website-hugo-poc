---
layout: "image"
title: "Eingabe Menge und Länge"
date: "2015-02-12T22:59:16"
picture: "6_Eingabe_Menge_und_Lnge.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40534
- /details04e9.html
imported:
- "2019"
_4images_image_id: "40534"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40534 -->
Man kann nun die Menge in 1er / 10er und 100er Schritten einstellen
Dann kann man die Länge in mm einstellen (ebenfalls 1er / 10er und 100er Schritte)
Gerade bei kurzen Stücken geht das prima
