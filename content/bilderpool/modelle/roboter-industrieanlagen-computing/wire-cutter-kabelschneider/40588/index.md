---
layout: "image"
title: "Klinge mit ft verbinden"
date: "2015-02-22T18:01:07"
picture: "DS_02_mit_ft_verbinden.jpg"
weight: "8"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/40588
- /details53f9.html
imported:
- "2019"
_4images_image_id: "40588"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T18:01:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40588 -->
Ich habe 3 Lagen Klebeband auf die Klinge geklebt und dann in den Spalt geschoben.
Das hält hervorragend.
Paßt bitte beim Nachbau auf, daß Ihr euch nicht schneidet!
