---
layout: "image"
title: "Steuerpult"
date: "2015-07-31T11:29:50"
picture: "bzmfr04.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41662
- /detailseea5.html
imported:
- "2019"
_4images_image_id: "41662"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41662 -->
- Dank Kippschalter kann gesteuert werden, ob der Roboter arbeiten oder pausieren soll
- kleines Gadget: Ausschalten der Maschine über Schlüsselschalter
