---
layout: "image"
title: "Schweißstation"
date: "2015-07-31T11:29:50"
picture: "bzmfr08.jpg"
weight: "8"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41666
- /details86a6.html
imported:
- "2019"
_4images_image_id: "41666"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41666 -->
schnelles Aufblitzen der Linsenlampen simuliert das Schweißen
