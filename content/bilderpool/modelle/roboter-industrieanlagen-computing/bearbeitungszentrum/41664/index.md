---
layout: "image"
title: "Förderband (2)"
date: "2015-07-31T11:29:50"
picture: "bzmfr06.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41664
- /details6f59.html
imported:
- "2019"
_4images_image_id: "41664"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41664 -->
Förderband liefert "neues Material"
Lichtschranke
