---
layout: "overview"
title: "Bearbeitungszentrum"
date: 2020-02-22T08:08:56+01:00
legacy_id:
- /php/categories/3105
- /categories1e4f.html
- /categoriesab92.html
- /categories7018-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3105 --> 
- Das Bearbeitungszentrum ist eines meiner Modelle, dass ich am Fischertechnik Fan Club Tag 2015 ausgestellt habe.
- Es besteht aus einem 5-Achsroboter und mehreren Bearbeitungsstationen.
- Programmablauf:
  - Roboter nimmt per Vakuumsauger ein Werkstück vom Förderband
  - Der Farbsensor bestimmt die Farbe des Werkstücks (mehr dazu siehe Bild "Scanner")
  - Das Plättchen wird von drei Seiten geschweißt
  - Zuletzt wird es an der Stanzpresse bearbeitet und dann nach Farbe in ein Magazin sortiert