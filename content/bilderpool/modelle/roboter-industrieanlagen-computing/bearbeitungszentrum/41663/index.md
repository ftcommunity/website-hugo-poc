---
layout: "image"
title: "Förderband (1)"
date: "2015-07-31T11:29:50"
picture: "bzmfr05.jpg"
weight: "5"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/41663
- /details7d37.html
imported:
- "2019"
_4images_image_id: "41663"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41663 -->
Das Förderband besteht nur aus zwei Ketten, auf die das Material gelegt wird
