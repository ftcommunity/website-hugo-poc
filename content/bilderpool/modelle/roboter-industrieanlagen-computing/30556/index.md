---
layout: "image"
title: "Original model"
date: "2011-05-14T22:07:37"
picture: "DSC00651.jpg"
weight: "18"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30556
- /details694e.html
imported:
- "2019"
_4images_image_id: "30556"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30556 -->
Original model with the universal interface.

  Original-Modell mit der universellen Schnittstelle