---
layout: "image"
title: "Roboter-Tiefbett-Schlitten-01"
date: "2012-02-18T13:28:17"
picture: "Schlitten-01_2.jpg"
weight: "1"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
schlagworte: ["Roboter", "Schlitten", "Verfahrweg", "Drehkranz"]
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34207
- /details0f4f.html
imported:
- "2019"
_4images_image_id: "34207"
_4images_cat_id: "2534"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34207 -->
mich hat es bei den Industrie-Robotern schon immer gestört, dass sie einen recht beschränkten Wirkungskreis haben. Aus diesem Grund habe ich mir für den- z.B. Teach-In Roboter ein Tiefbett ausgedacht. Es ist mit "einfachen" Mitteln- aber mit höchstem Wirgungsgrad zu betreiben. Sinn und Zweck ist es erst einmal gewesen, den Schwerpunkt so tief wie möglich zu legen. Bis Oberkante Drehkranz hat der Schlitten eine Höhe von 35mm. Der Verfahrweg beträgt 295mm. Der Antrieb des Drehkranz, zum zurücklegen des Verfahrweg, erfolgt über eine Schnecke. Wie ich diese Schnecke antreibe habe ich noch nicht entschieden. Einfach über Zahnrad, oder Motor, oder Kette. Schrittmotor wird aber wohl die erste Wahl werden, schau´n wir mal.
Den Schlitten könnte man z.B. über 7 Transportbändern montieren, die dann vom Roboter bedient werden. Oder man baut sich etwas, das den Schlitten heben und senken kann.