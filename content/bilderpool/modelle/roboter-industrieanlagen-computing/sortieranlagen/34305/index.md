---
layout: "image"
title: "Sortieranlage 3"
date: "2012-02-19T20:35:28"
picture: "FT_Derk_03-2012.jpg"
weight: "14"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34305
- /details221d-2.html
imported:
- "2019"
_4images_image_id: "34305"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:35:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34305 -->
