---
layout: "image"
title: "TST Drehkranz"
date: "2012-04-30T20:02:36"
picture: "Draaikrans_01.jpg"
weight: "31"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34844
- /detailsb772-2.html
imported:
- "2019"
_4images_image_id: "34844"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-04-30T20:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34844 -->
Die super drehkranz von Andreas.