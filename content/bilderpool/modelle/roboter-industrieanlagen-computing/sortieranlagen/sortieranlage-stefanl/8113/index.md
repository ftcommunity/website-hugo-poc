---
layout: "image"
title: "Sortieranlage (neu) 4"
date: "2006-12-25T14:31:01"
picture: "sortieranlage4.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8113
- /detailsac55.html
imported:
- "2019"
_4images_image_id: "8113"
_4images_cat_id: "750"
_4images_user_id: "502"
_4images_image_date: "2006-12-25T14:31:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8113 -->
