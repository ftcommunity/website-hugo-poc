---
layout: "image"
title: "Sortieranlage (neu) 1"
date: "2006-12-25T14:31:01"
picture: "sortieranlage1.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8110
- /details251e.html
imported:
- "2019"
_4images_image_id: "8110"
_4images_cat_id: "750"
_4images_user_id: "502"
_4images_image_date: "2006-12-25T14:31:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8110 -->
Diese Version ist jetzt noch mal doppelt so schnell wie die alte, also 6 in 9 sekunden. Aber noch genauso genau.
