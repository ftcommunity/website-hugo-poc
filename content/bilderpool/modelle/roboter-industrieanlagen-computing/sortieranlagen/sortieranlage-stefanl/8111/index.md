---
layout: "image"
title: "Sortieranlage (neu) 2"
date: "2006-12-25T14:31:01"
picture: "sortieranlage2.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8111
- /details1cea-2.html
imported:
- "2019"
_4images_image_id: "8111"
_4images_cat_id: "750"
_4images_user_id: "502"
_4images_image_date: "2006-12-25T14:31:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8111 -->
