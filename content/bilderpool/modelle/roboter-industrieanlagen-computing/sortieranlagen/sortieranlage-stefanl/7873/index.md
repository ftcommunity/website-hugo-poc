---
layout: "image"
title: "Sortieranlage 9"
date: "2006-12-11T16:12:06"
picture: "sortieranlage1_2.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7873
- /detailsb242.html
imported:
- "2019"
_4images_image_id: "7873"
_4images_cat_id: "750"
_4images_user_id: "502"
_4images_image_date: "2006-12-11T16:12:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7873 -->
Der neue Antrieb. Die Räder werden jetzt viel schneller auf das Band gelegt. So werden 2 weiße, 2 schwarze und 2 rote in ca. 16 Sekunden sortiert.
