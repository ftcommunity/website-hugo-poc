---
layout: "image"
title: "Seitenansicht"
date: "2014-11-23T19:12:24"
picture: "ksa2.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39850
- /details6f98.html
imported:
- "2019"
_4images_image_id: "39850"
_4images_cat_id: "2991"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39850 -->
Die Sammelbecken der Steine sind geneigt, sodass die Steine von selbst nach unten rutschen. Nachdem die Steine das Magazin verlassen haben, werden sie auf ihre Farbe überprüft. Um Störlicht zu vermeiden (und als nette Spielerei, wird der Farbscannbereich durch herabhängende Ketten abgedunkelt.
