---
layout: "image"
title: "Ansicht von hinten"
date: "2014-11-23T19:12:24"
picture: "ksa3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39851
- /details56a3.html
imported:
- "2019"
_4images_image_id: "39851"
_4images_cat_id: "2991"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39851 -->
Zwei Zylinder können jeweils einen Stein einer Farbe aussortieren. Der dritte Stein wird von selbst aussortiert. Das Förderband wird durch eine S-Motor angetrieben.
