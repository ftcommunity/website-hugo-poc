---
layout: "image"
title: "Vorderansicht"
date: "2014-11-23T19:12:24"
picture: "ksa1.jpg"
weight: "1"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39849
- /detailsc089.html
imported:
- "2019"
_4images_image_id: "39849"
_4images_cat_id: "2991"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39849 -->
Die Steine werden in drei Sammelbecken einsortiert, die dicht nebeneinander stehen. Von dort aus lassen sie sich leicht mit einem Vakuumsauger greifen.
