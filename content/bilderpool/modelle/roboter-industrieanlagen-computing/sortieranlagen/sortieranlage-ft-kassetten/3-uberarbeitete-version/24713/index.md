---
layout: "image"
title: "Anlage gesamt"
date: "2009-08-09T23:39:11"
picture: "ueberarbeiteteversion01.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/24713
- /details9720-2.html
imported:
- "2019"
_4images_image_id: "24713"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24713 -->
Hinzugekommen ist eine Auswurfanlage für kleine weiße "Klötze", die in gelbe Kasetten geworfen werden, bevor diese aussortiert werden.
Die Bearbeitungsstation habe ich komplett überarbeitet und sie mit Wechselwerkzeugen ausgestattet, so dass jetzt graue Kasetten zuerst an der Oberfläche gereinigt werden, ihnen dann in der Mitte ein Loch gebohrt wird, bevor sie noch ein Stückchen vorfahren und und dann noch die Fräse zum Einsatz kommt, ehe sie dann weitertransportiert und ausgeworfen werden.
