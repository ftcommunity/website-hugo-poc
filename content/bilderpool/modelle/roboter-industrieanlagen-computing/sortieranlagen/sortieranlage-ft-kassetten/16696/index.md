---
layout: "image"
title: "pneumatische Auswurfanlage"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten16.jpg"
weight: "16"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16696
- /detailsbad2.html
imported:
- "2019"
_4images_image_id: "16696"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16696 -->
