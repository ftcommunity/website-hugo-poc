---
layout: "image"
title: "Die überarbeitete gesamte Anlage"
date: "2009-01-09T22:16:26"
picture: "ueberarbeiteteversion01.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16962
- /detailsc487.html
imported:
- "2019"
_4images_image_id: "16962"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:16:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16962 -->
Ich habe folgende Sachen noch geändert bzw. hinzugefügt:
Die Messstation wurde überdacht um störendes Umgebungslicht fern zu halten.
Der Kompressor wurde aus Platzgrünen versetzt.
Die Bearbeitungsstation wurde stabiler gebaut und mit einem Schrittmotor als Hub-Antrieb versehen.
Es wurde noch ein Schienenwagen hinzugefügt, mit dem die grauen Kasetten noch bis unten gefahren werden, wo sie dann von einem 3-Achsroboter (Ihr müsst ihn euch denken) zum Abtransport aufgeschichtet werden.
