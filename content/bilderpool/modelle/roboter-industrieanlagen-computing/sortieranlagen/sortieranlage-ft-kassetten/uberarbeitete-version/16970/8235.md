---
layout: "comment"
hidden: true
title: "8235"
date: "2009-01-10T12:23:15"
uploadBy:
- "equester"
license: "unknown"
imported:
- "2019"
---
Hast Recht, ja! Ist auch nicht optimal, zumal die Lichtschranke dann auch noch dazu kam. Hab aber (noch) keine Energiekette. Die Idee mit dem Wagen kam mir, als ich schon eine Bestellung bei Knobloch aufgegeben hatte. Und mir jetzt nur eine Energiekette zu bestellen, ist auch ein wenig unsinnig...