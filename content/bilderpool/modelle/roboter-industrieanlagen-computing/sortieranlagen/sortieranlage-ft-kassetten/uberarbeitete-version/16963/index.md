---
layout: "image"
title: "Überdachung Messstation"
date: "2009-01-09T22:16:26"
picture: "ueberarbeiteteversion02.jpg"
weight: "2"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16963
- /details17c5.html
imported:
- "2019"
_4images_image_id: "16963"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:16:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16963 -->
