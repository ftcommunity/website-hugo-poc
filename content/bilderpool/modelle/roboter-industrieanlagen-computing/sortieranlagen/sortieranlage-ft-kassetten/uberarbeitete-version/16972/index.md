---
layout: "image"
title: "Verkabelung an Interface und Extension"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion11.jpg"
weight: "11"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16972
- /details35e3.html
imported:
- "2019"
_4images_image_id: "16972"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16972 -->
Links im Vordergrund erkennt man über dem Schalter, mit dem alle Lampen (für die Lichtschranken) eingeschaltet werden, den Not-Aus-Taster, mit dem die ganze Anlage gestoppt werden kann.
