---
layout: "image"
title: "Schienenwagen mit grauer Kasette"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion07.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16968
- /detailsc6ee-5.html
imported:
- "2019"
_4images_image_id: "16968"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16968 -->
