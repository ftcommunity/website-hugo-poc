---
layout: "image"
title: "Schienenwagen"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion08.jpg"
weight: "8"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16969
- /details541f.html
imported:
- "2019"
_4images_image_id: "16969"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16969 -->
Die Schrauben und Achsen im vorderen Teil des Wagen dienen als Gewicht, weil sonst das Antriebsrad durchdrehen würde.
