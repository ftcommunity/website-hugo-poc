---
layout: "image"
title: "Verkabelung an Interface und Extension"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion10.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16971
- /details5167-2.html
imported:
- "2019"
_4images_image_id: "16971"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16971 -->
