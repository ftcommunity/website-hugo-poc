---
layout: "image"
title: "drehbares Förderband 2 von vorne"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten12.jpg"
weight: "12"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16692
- /details949b.html
imported:
- "2019"
_4images_image_id: "16692"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16692 -->
