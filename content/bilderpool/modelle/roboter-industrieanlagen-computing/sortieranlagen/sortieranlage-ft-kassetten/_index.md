---
layout: "overview"
title: "Sortieranlage für ft Kassetten"
date: 2020-02-22T08:05:30+01:00
legacy_id:
- /php/categories/1511
- /categories7912.html
- /categories2c92.html
- /categories942c.html
- /categories7d9f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1511 --> 
Es wird nach vier Kriterien aussortiert: Ist die Kassette zu groß? Wenn nicht, ist die gelb? Wenn nicht (sie ist also grau), ist sie magnetisch? Und die resultierenden grauen Kassetten druchlaufen anschließend noch eine Bearbeitungsstation, bevor sie ebenfalls ausgeworfen werden.