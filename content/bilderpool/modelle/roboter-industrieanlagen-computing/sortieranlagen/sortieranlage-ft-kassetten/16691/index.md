---
layout: "image"
title: "Förderband 1 mit Messeinheit von vorne"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten11.jpg"
weight: "11"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16691
- /details42a6.html
imported:
- "2019"
_4images_image_id: "16691"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16691 -->
