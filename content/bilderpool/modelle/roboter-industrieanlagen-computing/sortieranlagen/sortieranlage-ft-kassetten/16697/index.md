---
layout: "image"
title: "gelbe Kasette bei Messung"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten17.jpg"
weight: "17"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16697
- /details1288-2.html
imported:
- "2019"
_4images_image_id: "16697"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16697 -->
Hier ist eine gelbe Kasette gerade bei der Messung. Die obere Lichtschranke ist frei, also ist die Kasette nicht zu groß. Der Farbsensor erkennt die Kasette als gelb...
