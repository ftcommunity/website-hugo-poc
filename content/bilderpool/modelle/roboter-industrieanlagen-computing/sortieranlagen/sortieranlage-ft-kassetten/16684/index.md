---
layout: "image"
title: "Anlage gesamt von hinten"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten04.jpg"
weight: "4"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16684
- /details8f7f.html
imported:
- "2019"
_4images_image_id: "16684"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16684 -->
