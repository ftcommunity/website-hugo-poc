---
layout: "image"
title: "Anlage gesamt von oben"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten03.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16683
- /detailse7b4.html
imported:
- "2019"
_4images_image_id: "16683"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16683 -->
