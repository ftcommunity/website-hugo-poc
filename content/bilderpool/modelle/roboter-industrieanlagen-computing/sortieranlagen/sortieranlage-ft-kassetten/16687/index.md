---
layout: "image"
title: "drehbares Förderband 2"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten07.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16687
- /detailsaff4-2.html
imported:
- "2019"
_4images_image_id: "16687"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16687 -->
Nach oben werden die zu großen Kasetten aussortiert, nach unten die gelben.
