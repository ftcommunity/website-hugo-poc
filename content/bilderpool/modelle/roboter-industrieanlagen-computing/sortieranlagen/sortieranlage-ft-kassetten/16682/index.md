---
layout: "image"
title: "Anlage gesamt von vorne"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten02.jpg"
weight: "2"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16682
- /details1281.html
imported:
- "2019"
_4images_image_id: "16682"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16682 -->
