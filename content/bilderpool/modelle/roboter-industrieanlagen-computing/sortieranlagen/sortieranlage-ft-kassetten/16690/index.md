---
layout: "image"
title: "Förderband 1 mit Messeinheit"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten10.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16690
- /detailsecdc-2.html
imported:
- "2019"
_4images_image_id: "16690"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16690 -->
Durch Drücken des Starttasters am Anfang der Anlage startet man einen neuen Durchlauf.
