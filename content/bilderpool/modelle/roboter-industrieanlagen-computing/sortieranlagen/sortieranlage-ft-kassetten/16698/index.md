---
layout: "image"
title: "gelbe Kasette kurz vor Auswurf"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten18.jpg"
weight: "18"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16698
- /details3219-3.html
imported:
- "2019"
_4images_image_id: "16698"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16698 -->
...und sie wird nach unten ausgeworfen.
