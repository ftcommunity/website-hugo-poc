---
layout: "image"
title: "Bearbeitungsstation (a)"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion05.jpg"
weight: "5"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/23582
- /details2365-2.html
imported:
- "2019"
_4images_image_id: "23582"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23582 -->
Im Vordergrund die Luftkühlung, hinten der Bohrer und die Fräse.
