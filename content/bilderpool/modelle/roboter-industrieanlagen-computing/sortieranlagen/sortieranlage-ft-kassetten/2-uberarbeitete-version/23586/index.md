---
layout: "image"
title: "In der Bearbeitungsstation"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion09.jpg"
weight: "9"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/23586
- /details9913.html
imported:
- "2019"
_4images_image_id: "23586"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23586 -->
