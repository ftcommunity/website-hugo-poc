---
layout: "image"
title: "Auswurf-Förderband für magnetische Kisten (a)"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion10.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/23587
- /details9b46.html
imported:
- "2019"
_4images_image_id: "23587"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23587 -->
Da ich am Interface und am Extension keine Ein-, bzw. Ausgänge mehr zur Verfügung habe, wird das Förderband unabhängig von der Anlage über einen Flip-Flop-Baustein gesteuert.
Es startet, sobald der Reedkontakt auslöst und stoppt über den Taster am Ende.
