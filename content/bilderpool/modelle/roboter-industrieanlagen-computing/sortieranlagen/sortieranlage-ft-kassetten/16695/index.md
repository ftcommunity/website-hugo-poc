---
layout: "image"
title: "Förderband 3 von oben"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten15.jpg"
weight: "15"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/16695
- /detailsb4e3.html
imported:
- "2019"
_4images_image_id: "16695"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16695 -->
