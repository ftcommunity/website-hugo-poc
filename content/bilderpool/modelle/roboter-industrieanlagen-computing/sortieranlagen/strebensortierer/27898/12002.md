---
layout: "comment"
hidden: true
title: "12002"
date: "2010-08-24T16:45:35"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo fischli,
ein sehr interessantes Modell hast du da gebaut. Vielleicht kannst du uns noch näher erklären, wie du mit der Lichtschranke die Strebenlänge misst und dann mit dem Ergebnis den Sortierschieber steuerst. Vielleicht ist das auch falls du es veröffentlichst im Robo-Pro?Steuerprogramm  ersichtlich? Sortierst du damit auch Streben nach mit oder ohne Loch?
Bleibt jetzt noch eine Frage offen: Hast du so viele Streben, dass die bei dir eine Maschine sortieren muss :o)
Gruss, Udo2