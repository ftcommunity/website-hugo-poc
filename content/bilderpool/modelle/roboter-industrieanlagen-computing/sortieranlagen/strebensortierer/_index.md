---
layout: "overview"
title: "Strebensortierer"
date: 2020-02-22T08:05:38+01:00
legacy_id:
- /php/categories/2018
- /categoriesabc2.html
- /categoriesbb12.html
- /categoriesae7d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2018 --> 
Diese Maschine sortiert FT-Streben nach ihrer Länge. Leider funktioniert es nicht so gut, die Maschine kann nur die vier längsten Streben sortieren, obwohl es eigentlich 8 sein sollten. Außerdem verhakt sich alles oft ein bisschen.