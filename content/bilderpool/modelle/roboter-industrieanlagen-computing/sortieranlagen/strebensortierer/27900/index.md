---
layout: "image"
title: "3"
date: "2010-08-23T23:25:26"
picture: "strebensortierer03.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27900
- /details7e55-3.html
imported:
- "2019"
_4images_image_id: "27900"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27900 -->
Das ganze wird vom Akku versorgt, muss deshalb öfters aufgeladen werden.