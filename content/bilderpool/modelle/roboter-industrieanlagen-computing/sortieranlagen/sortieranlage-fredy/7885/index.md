---
layout: "image"
title: "Drehkranz"
date: "2006-12-12T18:06:00"
picture: "Fr_ftcommunity1_007.jpg"
weight: "16"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7885
- /details2947.html
imported:
- "2019"
_4images_image_id: "7885"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-12T18:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7885 -->
Der neue erste Drehkranz.
