---
layout: "image"
title: "Druckluft Tank"
date: "2007-01-07T13:19:19"
picture: "Neuer_Ordner_2_004_2.jpg"
weight: "38"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8312
- /detailsffb7-2.html
imported:
- "2019"
_4images_image_id: "8312"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-07T13:19:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8312 -->
Ich habe den Druckluft Tanke jetzt unter dem Laufband der Zuhfürung, das spart Platz.
