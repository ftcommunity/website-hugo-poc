---
layout: "image"
title: "Drehkranz 2"
date: "2006-10-12T11:45:33"
picture: "Fr_ftcommunity_005.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7181
- /details7875-2.html
imported:
- "2019"
_4images_image_id: "7181"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-10-12T11:45:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7181 -->
