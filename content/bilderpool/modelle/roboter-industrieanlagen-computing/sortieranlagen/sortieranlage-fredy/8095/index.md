---
layout: "image"
title: "Anschluss am Interface"
date: "2006-12-21T15:29:24"
picture: "Neuer_Ordner_2_001_2.jpg"
weight: "30"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8095
- /details6164-2.html
imported:
- "2019"
_4images_image_id: "8095"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-21T15:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8095 -->
