---
layout: "image"
title: "Drehkranz mit Führung"
date: "2007-01-01T19:50:00"
picture: "Neuer_Ordner_2_002_3.jpg"
weight: "33"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8244
- /details1c87-2.html
imported:
- "2019"
_4images_image_id: "8244"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-01T19:50:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8244 -->
