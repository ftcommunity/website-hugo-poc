---
layout: "image"
title: "Gesamtansicht"
date: "2007-03-17T14:33:15"
picture: "bilder_005.jpg"
weight: "51"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9539
- /details4817-2.html
imported:
- "2019"
_4images_image_id: "9539"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-03-17T14:33:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9539 -->
Hier noch mal eine Gesamtansicht auf der man sieht wo das Lcd angebracht ist.
