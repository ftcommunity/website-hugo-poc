---
layout: "image"
title: "Drehkranz"
date: "2007-01-07T13:19:19"
picture: "Neuer_Ordner_2_001_4.jpg"
weight: "35"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8309
- /details28f7.html
imported:
- "2019"
_4images_image_id: "8309"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-07T13:19:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8309 -->
