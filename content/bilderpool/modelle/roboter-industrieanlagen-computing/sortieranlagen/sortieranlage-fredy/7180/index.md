---
layout: "image"
title: "Drekranz 1"
date: "2006-10-12T11:45:33"
picture: "Fr_ftcommunity_004.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7180
- /detailsc1de-2.html
imported:
- "2019"
_4images_image_id: "7180"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-10-12T11:45:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7180 -->
