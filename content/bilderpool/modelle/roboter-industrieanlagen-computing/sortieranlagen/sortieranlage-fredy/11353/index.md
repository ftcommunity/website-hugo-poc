---
layout: "image"
title: "Ketten unten"
date: "2007-08-11T17:21:31"
picture: "industriemodell1.jpg"
weight: "53"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11353
- /details8570-3.html
imported:
- "2019"
_4images_image_id: "11353"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11353 -->
