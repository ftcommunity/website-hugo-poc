---
layout: "image"
title: "Top ansiegt Tabelle Roboter"
date: "2009-01-31T12:59:10"
picture: "DSC_2071_-_Version_2.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/17220
- /detailsb25a.html
imported:
- "2019"
_4images_image_id: "17220"
_4images_cat_id: "1544"
_4images_user_id: "371"
_4images_image_date: "2009-01-31T12:59:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17220 -->
