---
layout: "image"
title: "Tisch element part I"
date: "2009-01-31T12:59:11"
picture: "DSC_2079_-_Version_2.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/17223
- /details1c13-2.html
imported:
- "2019"
_4images_image_id: "17223"
_4images_cat_id: "1544"
_4images_user_id: "371"
_4images_image_date: "2009-01-31T12:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17223 -->
