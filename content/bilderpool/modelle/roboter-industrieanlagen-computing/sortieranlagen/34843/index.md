---
layout: "image"
title: "Logo FT Mozaiek 03"
date: "2012-04-30T09:55:38"
picture: "Debby_03.jpg"
weight: "30"
konstrukteure: 
- "Debby"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34843
- /details36d7-2.html
imported:
- "2019"
_4images_image_id: "34843"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-04-30T09:55:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34843 -->
Door mijn vriendin gemaakt, en als cadeau gekregen.
Mozaiek is haar hobby, en ze doet dit ook in opdracht.