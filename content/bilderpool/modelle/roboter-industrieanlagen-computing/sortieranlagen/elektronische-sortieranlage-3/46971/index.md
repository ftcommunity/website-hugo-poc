---
layout: "image"
title: "Abtastung und Ausgabe"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46971
- /detailsd619.html
imported:
- "2019"
_4images_image_id: "46971"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46971 -->
Die beiden Lichtschranken müssen genau justiert sein, damit die Sache klappt. Der Erkennung funktioniert nämlich so:

1. Jeder Baustein durchbricht ja zunächst die erste (linke) Lichtschranke.

2. Die Unterscheidung der Längen findet statt, wenn die zweite Lichtschranke unterbrochen wird:

a) Bei einem zu kurzen Baustein wird die erste Lichtschranke "schon lange" wieder freigegeben worden sein.

b) Bei einem mittellangen Baustein wird die erste Lichtschranke innerhalb eines kurzen, definierten Zeitraums nach der Unterbrechung der zweiten freigegeben werden.

c) Bei einem zu langen Baustein wird die erste Lichtschranke erst "lange" nach der Unterbrechung der zweiten freigegeben.

Dieses "lange" wird über ein kurz eingestelltes Monoflop realisiert. Das definiert die Zeitspanne, innerhalb der die erste Lichtschranke nach Unterbrechung der zweiten freigegeben werden muss, damit das Bauteil als "mittellang" erkannt wird. Der Verteiler dreht sich dann gar nicht.

In den Fällen a) und b) dreht sich der Verteiler auf jeden Fall, aber eben in der jeweils nötigen Richtung.
