---
layout: "image"
title: "Antrieb"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46968
- /details4b63.html
imported:
- "2019"
_4images_image_id: "46968"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46968 -->
Ein XS-Motor treibt das Förderband an, das aus zwei der alten fischertechnik-Raupengummibänder besteht. In die Bauplatte 500 sind unten übrigens vier ft-Kunststofffedern eingesteckt, sodass kein Körperschall auf den Tisch übertragen wird und die Anlage angenehm leise läuft.
