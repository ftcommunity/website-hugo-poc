---
layout: "image"
title: "Kabelführung"
date: "2017-04-13T17:42:39"
picture: "tvha4.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45739
- /details407e-2.html
imported:
- "2019"
_4images_image_id: "45739"
_4images_cat_id: "3399"
_4images_user_id: "2228"
_4images_image_date: "2017-04-13T17:42:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45739 -->
Der Spiralschlauf führt die Kabel so, dass sie nicht in die Mechanik geraten.
