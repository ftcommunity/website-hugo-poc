---
layout: "image"
title: "Pneumatischer Greifer"
date: "2017-04-13T17:42:39"
picture: "tvha2.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45737
- /details248b-2.html
imported:
- "2019"
_4images_image_id: "45737"
_4images_cat_id: "3399"
_4images_user_id: "2228"
_4images_image_date: "2017-04-13T17:42:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45737 -->
Der Zylinder 45 öffnet und schließt die Zange pneumatisch. Die Winkelsteine 10x15x15 dienen zur Führung des Baustein B7,5 , sodass die Zange synchron schließt.
