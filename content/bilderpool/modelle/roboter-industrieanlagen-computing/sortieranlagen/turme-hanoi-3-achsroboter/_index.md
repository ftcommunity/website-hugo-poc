---
layout: "overview"
title: "Türme von Hanoi (3-Achsroboter)"
date: 2020-02-22T08:05:56+01:00
legacy_id:
- /php/categories/3399
- /categoriesde78.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3399 --> 
3-Achsroboter löst das Knobelspiel "Türme von Hanoi"