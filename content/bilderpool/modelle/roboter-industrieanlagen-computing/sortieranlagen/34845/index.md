---
layout: "image"
title: "Robot arm nu met 2 wormassen en 2 geleiderassen"
date: "2012-04-30T20:02:36"
picture: "Robot_02.jpg"
weight: "32"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34845
- /detailsa130-2.html
imported:
- "2019"
_4images_image_id: "34845"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-04-30T20:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34845 -->
