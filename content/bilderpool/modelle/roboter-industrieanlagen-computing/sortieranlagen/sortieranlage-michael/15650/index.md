---
layout: "image"
title: "Sotieranlage (5)"
date: "2008-09-27T21:15:12"
picture: "SNV800014.jpg"
weight: "5"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/15650
- /detailsf087.html
imported:
- "2019"
_4images_image_id: "15650"
_4images_cat_id: "1438"
_4images_user_id: "820"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15650 -->
