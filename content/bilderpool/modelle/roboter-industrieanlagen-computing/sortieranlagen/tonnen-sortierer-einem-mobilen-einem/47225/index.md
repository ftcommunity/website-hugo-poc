---
layout: "image"
title: "Die Tonne wurde abgestellt für den stationären Roboter"
date: "2018-01-30T16:23:43"
picture: "T2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Roboter", "Kooperation", "mobile", "industry", "stationär", "Tonnen", "Farbmarkierung", "Ziffern"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47225
- /detailsd728-2.html
imported:
- "2019"
_4images_image_id: "47225"
_4images_cat_id: "3495"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47225 -->
