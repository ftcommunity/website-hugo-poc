---
layout: "image"
title: "Der stationäre Roboter fährt die Tonne an"
date: "2018-01-30T16:23:43"
picture: "T3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47226
- /details6c2e.html
imported:
- "2019"
_4images_image_id: "47226"
_4images_cat_id: "3495"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47226 -->
