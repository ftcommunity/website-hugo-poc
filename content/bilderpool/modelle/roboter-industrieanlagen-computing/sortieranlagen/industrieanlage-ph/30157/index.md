---
layout: "image"
title: "Bearbeitungsstation"
date: "2011-02-28T17:31:46"
picture: "industrieanlage03.jpg"
weight: "3"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30157
- /details62e6.html
imported:
- "2019"
_4images_image_id: "30157"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30157 -->
Hier wird gestempelt.