---
layout: "image"
title: "Das Steinelager"
date: "2011-02-28T17:32:13"
picture: "industrieanlage21.jpg"
weight: "21"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30175
- /details6e7e.html
imported:
- "2019"
_4images_image_id: "30175"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30175 -->
Das Steinelager von oben.