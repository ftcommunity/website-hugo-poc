---
layout: "image"
title: "Die Steine"
date: "2011-02-28T17:32:13"
picture: "industrieanlage20.jpg"
weight: "20"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30174
- /details664e.html
imported:
- "2019"
_4images_image_id: "30174"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:13"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30174 -->
16 Stück