---
layout: "image"
title: "Fließband 2"
date: "2011-02-28T17:32:01"
picture: "industrieanlage19.jpg"
weight: "19"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30173
- /details7f44-2.html
imported:
- "2019"
_4images_image_id: "30173"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:01"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30173 -->
