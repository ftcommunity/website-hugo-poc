---
layout: "image"
title: "Die Farberkennung"
date: "2011-02-28T17:31:46"
picture: "industrieanlage04.jpg"
weight: "4"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30158
- /detailsb502.html
imported:
- "2019"
_4images_image_id: "30158"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30158 -->
Rot und Weiß= Auf das nächste Fließband
Blau= In die erste Kiste.