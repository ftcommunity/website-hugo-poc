---
layout: "image"
title: "Die Blauekiste am Ende des ersten Fließbandes"
date: "2011-02-28T17:31:46"
picture: "industrieanlage05.jpg"
weight: "5"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/30159
- /details9ae4.html
imported:
- "2019"
_4images_image_id: "30159"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30159 -->
Die blauen Steine kommen in diese Kiste.