---
layout: "image"
title: "Werkstück auf dem Förderband"
date: "2010-08-28T09:49:16"
picture: "Ein_Werkstck_auf_dem_Frderband_I.jpg"
weight: "7"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/27989
- /detailsf40a.html
imported:
- "2019"
_4images_image_id: "27989"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27989 -->
