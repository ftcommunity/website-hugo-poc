---
layout: "image"
title: "Sortierer"
date: "2007-04-11T09:59:11"
picture: "sortieranlage4.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/10046
- /details240b.html
imported:
- "2019"
_4images_image_id: "10046"
_4images_cat_id: "907"
_4images_user_id: "424"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10046 -->
30er Steine werden vom Band geworfen
