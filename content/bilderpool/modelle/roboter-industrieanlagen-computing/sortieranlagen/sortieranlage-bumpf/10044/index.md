---
layout: "image"
title: "Förderband"
date: "2007-04-11T09:59:11"
picture: "sortieranlage2.jpg"
weight: "2"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/10044
- /detailsb71c.html
imported:
- "2019"
_4images_image_id: "10044"
_4images_cat_id: "907"
_4images_user_id: "424"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10044 -->
Drei Förderbänder mit verschiedener Geschwindigkeit damit die Steine einen grösseren Abstand bekommen
