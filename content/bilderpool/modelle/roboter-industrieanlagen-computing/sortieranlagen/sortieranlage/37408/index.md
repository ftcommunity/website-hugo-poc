---
layout: "image"
title: "Der Antrieb"
date: "2013-09-17T19:09:15"
picture: "sortieranlage09.jpg"
weight: "9"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/37408
- /detailseb72.html
imported:
- "2019"
_4images_image_id: "37408"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37408 -->
Die Schnecke und die Anbauplatte sind von TST.
(Siehe Ft:pedia 3/2012)