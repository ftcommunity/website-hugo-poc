---
layout: "image"
title: "Die Lichtschranke"
date: "2013-09-17T19:09:15"
picture: "sortieranlage05.jpg"
weight: "5"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/37404
- /detailsd796.html
imported:
- "2019"
_4images_image_id: "37404"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37404 -->
Hier werden zu lange Teile erkannt.