---
layout: "image"
title: "Hochregal Zwischenlager"
date: "2006-02-07T17:46:01"
picture: "HochR0006.jpg"
weight: "8"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5745
- /details7ad5.html
imported:
- "2019"
_4images_image_id: "5745"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2006-02-07T17:46:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5745 -->
Mal von der anderen Seite. Die Maschine ist durch die Kettenantriebe recht schnell zu Fuß. Wichtig um nicht zuviel Prozesszeit zu verschwenden.