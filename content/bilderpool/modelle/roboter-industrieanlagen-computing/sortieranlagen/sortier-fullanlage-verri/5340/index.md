---
layout: "image"
title: "Füllroboter Detail"
date: "2005-11-13T21:17:25"
picture: "FT0008.jpg"
weight: "6"
konstrukteure: 
- "verri"
fotografen:
- "verri"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5340
- /detailsbc2a.html
imported:
- "2019"
_4images_image_id: "5340"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5340 -->
Hier der Füllroboter im Detail.