---
layout: "image"
title: "Fassmagazin"
date: "2005-11-13T21:17:25"
picture: "FT0005.jpg"
weight: "4"
konstrukteure: 
- "verri"
fotografen:
- "verri"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5338
- /details15a9.html
imported:
- "2019"
_4images_image_id: "5338"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5338 -->
Hier die Lagervorrichtung der Fässer.Der Zuführer wird jeweils zu einem Ablaufband bewegt und führt dann ein fass per Ausrücker zu.