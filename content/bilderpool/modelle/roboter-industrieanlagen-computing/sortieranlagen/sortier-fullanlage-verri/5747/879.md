---
layout: "comment"
hidden: true
title: "879"
date: "2006-02-07T20:17:10"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sauber!Starkes Stück! Könntest Du noch ein paar Detailfotos machen, z. B. von der Wendeanlage?

Gruß und Kompliment,
Stefan