---
layout: "image"
title: "Vereinzelung und Wendung"
date: "2005-11-13T21:17:24"
picture: "FT0001.jpg"
weight: "1"
konstrukteure: 
- "Verri"
fotografen:
- "Verri"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5335
- /details7336.html
imported:
- "2019"
_4images_image_id: "5335"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5335 -->
Hier werden die Werkstücke vereinzelt sowie auf ihre Lage geprüft.Bei Falschlage wird das Werkstueck Pneumatisch gewendet und dem nächsten Prozess zugeführt.