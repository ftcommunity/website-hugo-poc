---
layout: "image"
title: "HR Abrückband"
date: "2006-02-07T17:46:01"
picture: "HochR0005.jpg"
weight: "9"
konstrukteure: 
- "Jörg Verbeck"
fotografen:
- "Jörg Verbeck"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5746
- /details3360.html
imported:
- "2019"
_4images_image_id: "5746"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2006-02-07T17:46:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5746 -->
Hier werden die Ausgelagerten Fässer an den nächsten Prozess ( Palettierung ) weiter gegeben. Das Band ist nicht über das Interface sondern über einen FlipFlop geschaltet.