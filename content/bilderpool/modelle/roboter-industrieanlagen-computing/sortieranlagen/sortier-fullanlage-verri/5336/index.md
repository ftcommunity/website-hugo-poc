---
layout: "image"
title: "Farberkennung"
date: "2005-11-13T21:17:25"
picture: "FT0002.jpg"
weight: "2"
konstrukteure: 
- "Verri"
fotografen:
- "Verri"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- /php/details/5336
- /details2362.html
imported:
- "2019"
_4images_image_id: "5336"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5336 -->
Hier wird das Werkstück einer Farbprüfung unterzogen. Der Farbwert wird für die spätere Sortierung gespeichert. Werkstücke mit der Farbe rot werden als Ausschuss betrachtet und vom Band entfernt. Weiße und schwarze Werkstücke werden dem nächsten Prozess zugeführt.