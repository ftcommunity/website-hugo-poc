---
layout: "image"
title: "Streben weg wischen"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben07.jpg"
weight: "7"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36453
- /detailsd719.html
imported:
- "2019"
_4images_image_id: "36453"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36453 -->
Mit diesem Gestell wird die Strebe, nachdem sie gemessen wurde, wieder von dem Messtisch herunter geschoben. Ein Mini-Mot. dreht die Scheibe, die unter dem Z40 Zahnrad sitztund an der Scheibe sind zwei Platten befestigt , die dann über den wischen. Die Scheibe wird immer eine halbe Umdrehung gedreht und hält wieder in der Position in der sie im Bild zu sehen ist.
Der Power-Mot. in der Mitte unten bewegt den Messtisch. Der Power-Mot. in der linken unteren Ecke bewegt das erste Förderband.