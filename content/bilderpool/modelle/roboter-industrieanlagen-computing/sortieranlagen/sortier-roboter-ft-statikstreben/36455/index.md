---
layout: "image"
title: "Streben zum Kasten bringen"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben09.jpg"
weight: "9"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36455
- /details49e7.html
imported:
- "2019"
_4images_image_id: "36455"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36455 -->
Nach dem die Strebe von dem Messtisch geschoben wurde landet sie auf dem Förderband, wo sie dann fährt, bis sie auf dem zweiten Förderband liegt. Das zweite Förderband kann dann an dem Turm hoch fahren, der im Bild mit der verstrebung zu sehen ist. Ganz links befindet sich dann das Regal in dem die Kästen stehen.