---
layout: "image"
title: "Regal-Motor"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben12.jpg"
weight: "12"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/36458
- /detailsca67-2.html
imported:
- "2019"
_4images_image_id: "36458"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36458 -->
Das Regal in dem die Kästchen stehen ist drehbar. Dadurch kann ich erstens doppelt so viele Kästchen bei gleicher Höhe einstellen und brauche zweitens nur einen Interface-Output weil sich das Regal immer um die gleiche Richtung weiter dreht. Im Robo-Pro Programm wird gesteuert ob sich das Regal dreht. Je nach dem benötigten Ziel-Kasten der bearbeiteten Strebe.