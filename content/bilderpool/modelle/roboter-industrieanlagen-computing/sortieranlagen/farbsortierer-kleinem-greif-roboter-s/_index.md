---
layout: "overview"
title: "Farbsortierer mit kleinem Greif-Roboter (Martin S)"
date: 2020-02-22T08:05:41+01:00
legacy_id:
- /php/categories/2173
- /categories96aa.html
- /categories05fc.html
- /categories7012.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2173 --> 
Diese Maschine nimmt sich den Zylinder von der Startposition und bringt ihn zur Farbmessstation.
Nachdem die Farbe ermittelt wurde wirft der Roboter den Zylinder in die entsprechende Box.
Video bald unter http://www.youtube.com/user/Mulwurf100 zu finden.