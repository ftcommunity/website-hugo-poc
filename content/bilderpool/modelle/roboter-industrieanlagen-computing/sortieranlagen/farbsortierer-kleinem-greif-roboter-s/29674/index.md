---
layout: "image"
title: "Achse 2 Motor"
date: "2011-01-11T19:13:20"
picture: "farbsortierermitkleinemachsengreifroboter09.jpg"
weight: "9"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29674
- /details7f2f.html
imported:
- "2019"
_4images_image_id: "29674"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29674 -->
