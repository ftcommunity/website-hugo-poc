---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-11T19:13:19"
picture: "farbsortierermitkleinemachsengreifroboter02.jpg"
weight: "2"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29667
- /details1bcf.html
imported:
- "2019"
_4images_image_id: "29667"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29667 -->
