---
layout: "image"
title: "Farbsensor"
date: "2011-01-11T19:13:20"
picture: "farbsortierermitkleinemachsengreifroboter07.jpg"
weight: "7"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29672
- /details1b8b.html
imported:
- "2019"
_4images_image_id: "29672"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29672 -->
