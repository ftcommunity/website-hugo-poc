---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader15.jpg"
weight: "15"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33967
- /detailsa6a6-2.html
imported:
- "2019"
_4images_image_id: "33967"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33967 -->
Die entnommenen Paletten