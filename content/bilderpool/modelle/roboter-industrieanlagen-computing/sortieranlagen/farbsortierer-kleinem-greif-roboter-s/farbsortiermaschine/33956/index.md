---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader04.jpg"
weight: "4"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33956
- /detailscd6f-2.html
imported:
- "2019"
_4images_image_id: "33956"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33956 -->
Blick von hinten auf die Auswerfer