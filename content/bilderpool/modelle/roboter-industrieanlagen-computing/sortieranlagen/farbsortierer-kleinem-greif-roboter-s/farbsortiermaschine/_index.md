---
layout: "overview"
title: "Farbsortiermaschine mit Palettenladeeinrichtung"
date: 2020-02-22T08:05:41+01:00
legacy_id:
- /php/categories/2513
- /categories20cb.html
- /categories701f.html
- /categories812a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2513 --> 
In das Dreischachtmagazin werden 54 Grundbausteine - 18 rote, 18 gelbe und 18 schwarze - völlig ungeordnet eingelegt. Nach dem Start werden die Schächte der Reihe nach von rechts nach links geöffnet, so dass jeweils ein Baustein auf das Förderband fällt. Ft-Farbsensoren, die über dem Förderband angeordnet sind erkennen auf Grund der Stärke der Reflexion welche Farbe der unter ihnen durchlaufende Baustein trägt. Drei hinter dem Förderband angordnete Auswerfer schieben den entsprechenden Baustein auf je eine vor dem Förderband angebrachte Palette. Sobald eine Palette 6 Bausteine aufgenommen hat, senkt sie sich um eine Bausteinhöhe ab, so dass nun eine weitere Bausteinreihe geladen werden kann. Sobald alle drei Paletten mit 18 Bausteinen (3 Reihen à 6 Bausteine) beladen sind können die vollen Paletten zur besseren Entnahme nach oben gefahren und entfernt werden. 
Der weiße Nothaltknopf kann bei Störungen gedrückt werden. Dabei ertönt ein Warnsignal. Alle Motoren bleiben bei Nothalt stehen. Nach Beseitigung der Störung läuft die Steuerung der Maschine durch das Computerprogramm genau an der unterbrochenen Stelle weiter.
Ein Video, dass die Maschine in Betrieb zeigt, kann in Youtube unter "FarbSortMasch02.avi" angesehen werden.