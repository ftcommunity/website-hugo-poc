---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:02"
picture: "farbsortiermaschemitpalettenlader11.jpg"
weight: "11"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33963
- /detailsfc5a.html
imported:
- "2019"
_4images_image_id: "33963"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:02"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33963 -->
Magazinmechanik. Der vorderste Stein wird durch eine Sperrklinke am Herausrutschen gehindert. Wird die Sperrklinke durch einen der rotiernden Hebel angehoben, drückt eine Feder, die mit der Sperrklinke eine Wippe bildet auf den zweiten Stein. Der erste Stein rutscht heraus, der zweite Stein klemmt fest. Wenn die Sperrklinke wieder sperrt, gibt die Feder den zweiten Stein  frei und alle Steine des Schachtes rutschen nach. Der Winkel des Magazins ist so gewählt, dass die Steine gerade so mit Sicherheit rutschen. Ein zu steiler Winkel des Magazins würde den Druck der "Steinsäule" zu stark erhöhen, so dass die Klemmfeder die Steine nicht mehr sicher halten kann.