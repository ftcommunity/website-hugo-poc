---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader17.jpg"
weight: "17"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33969
- /details153c-2.html
imported:
- "2019"
_4images_image_id: "33969"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33969 -->
Verbindungskabegewirrl zu Interface und Erweiterung