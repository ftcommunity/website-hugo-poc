---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-11T19:13:19"
picture: "farbsortierermitkleinemachsengreifroboter01.jpg"
weight: "1"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- /php/details/29666
- /details972d.html
imported:
- "2019"
_4images_image_id: "29666"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29666 -->
