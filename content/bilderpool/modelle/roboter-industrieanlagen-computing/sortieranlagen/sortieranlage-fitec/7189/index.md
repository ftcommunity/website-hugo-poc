---
layout: "image"
title: "...von links..."
date: "2006-10-16T19:01:02"
picture: "Sortiermaschine12.jpg"
weight: "12"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7189
- /details600d-3.html
imported:
- "2019"
_4images_image_id: "7189"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7189 -->
Hier sieht man gut wie der erste Zylinder gesteuert wird.
