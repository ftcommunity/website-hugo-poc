---
layout: "comment"
hidden: true
title: "1465"
date: "2006-10-22T16:23:34"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Ach ja, ich habe vergessen zu sagen, dass man die Lampe auf Helligkeitsstufe 2 stellen muss, sonst erkennt der Fototransistor jedes mal einen weißen Baustein.

Gruß fitec