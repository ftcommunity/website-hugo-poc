---
layout: "image"
title: "Sortieranlage"
date: "2006-10-02T16:28:11"
picture: "Sortiermaschine4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7099
- /details49d7.html
imported:
- "2019"
_4images_image_id: "7099"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7099 -->
Das sind die Magnetventile mit denen die Zylinder gesteuert werden.
