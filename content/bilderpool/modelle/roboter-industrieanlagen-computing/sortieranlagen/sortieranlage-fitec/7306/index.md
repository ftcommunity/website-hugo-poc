---
layout: "image"
title: "Sortieranlage gesamt"
date: "2006-11-03T18:11:48"
picture: "PICT0976.jpg"
weight: "16"
konstrukteure: 
- "Uwe Timm (Chemikus)"
fotografen:
- "Uwe Timm (Chemikus)"
uploadBy: "Chemikus"
license: "unknown"
legacy_id:
- /php/details/7306
- /details2964.html
imported:
- "2019"
_4images_image_id: "7306"
_4images_cat_id: "685"
_4images_user_id: "156"
_4images_image_date: "2006-11-03T18:11:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7306 -->
Gesamtansicht mit Eingabe, Scanner, Schwenkroboter, 3-Achsroboter1, 3-Achsroboter2 und Ausgabe
