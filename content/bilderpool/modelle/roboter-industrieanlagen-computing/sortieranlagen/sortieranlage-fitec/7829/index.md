---
layout: "image"
title: "Sortieranlage 2"
date: "2006-12-10T18:30:25"
picture: "sortieranlage2.jpg"
weight: "29"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7829
- /detailsf8e9.html
imported:
- "2019"
_4images_image_id: "7829"
_4images_cat_id: "685"
_4images_user_id: "502"
_4images_image_date: "2006-12-10T18:30:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7829 -->
