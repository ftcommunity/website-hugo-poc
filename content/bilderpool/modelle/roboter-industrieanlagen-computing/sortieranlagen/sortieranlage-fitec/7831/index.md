---
layout: "image"
title: "Sortieranlage 4"
date: "2006-12-10T18:30:25"
picture: "sortieranlage4.jpg"
weight: "31"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7831
- /details9823.html
imported:
- "2019"
_4images_image_id: "7831"
_4images_cat_id: "685"
_4images_user_id: "502"
_4images_image_date: "2006-12-10T18:30:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7831 -->
