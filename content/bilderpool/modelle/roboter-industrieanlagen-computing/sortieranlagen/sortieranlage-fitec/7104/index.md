---
layout: "image"
title: "Sortieranlage"
date: "2006-10-02T16:29:33"
picture: "Sortiermaschine9.jpg"
weight: "9"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7104
- /detailse8a4-2.html
imported:
- "2019"
_4images_image_id: "7104"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-02T16:29:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7104 -->
Das ist der drehbare Greifer. Man sieht auch den Impulszähler.
