---
layout: "image"
title: "Sortierer"
date: "2006-11-28T21:19:00"
picture: "Sortiermaschine22.jpg"
weight: "27"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7643
- /details3c6a-2.html
imported:
- "2019"
_4images_image_id: "7643"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-28T21:19:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7643 -->
