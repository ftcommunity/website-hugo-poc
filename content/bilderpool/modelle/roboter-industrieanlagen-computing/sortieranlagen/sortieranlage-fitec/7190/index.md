---
layout: "image"
title: "...und von rechts"
date: "2006-10-16T19:01:02"
picture: "Sortiermaschine13.jpg"
weight: "13"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7190
- /detailscf5a.html
imported:
- "2019"
_4images_image_id: "7190"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7190 -->
Hier die Magnetventile für den zweiten Zylinder.
