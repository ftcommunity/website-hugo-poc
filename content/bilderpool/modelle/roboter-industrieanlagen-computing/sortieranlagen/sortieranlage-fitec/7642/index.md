---
layout: "image"
title: "Sortiermaschine"
date: "2006-11-28T21:19:00"
picture: "Sortiermaschine21.jpg"
weight: "26"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7642
- /details94d4-2.html
imported:
- "2019"
_4images_image_id: "7642"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-28T21:19:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7642 -->
Ich habe noch einen Impulszähler ans Förderband gebaut.
