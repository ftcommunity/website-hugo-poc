---
layout: "image"
title: "Förderbandantrieb"
date: "2013-03-22T10:51:12"
picture: "farbsortierer05.jpg"
weight: "5"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36790
- /details848b-2.html
imported:
- "2019"
_4images_image_id: "36790"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36790 -->
Das Förderband läuft die ganze Zeit, außer wenn gerade ein Teil gemessen oder mittels Greifer vom Band entnommen wird.

Rechts sieht man einen Positionstaster für die Drehbewegung des Roboterarms.
