---
layout: "image"
title: "Ablagefächer"
date: "2013-03-22T10:51:13"
picture: "farbsortierer08.jpg"
weight: "8"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36793
- /details46f0.html
imported:
- "2019"
_4images_image_id: "36793"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36793 -->
Hier kommen die sortierten Räder hinein.
