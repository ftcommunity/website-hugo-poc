---
layout: "image"
title: "Detailblick in die Mechanik"
date: "2013-03-22T10:51:13"
picture: "farbsortierer10.jpg"
weight: "10"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36795
- /details9dba.html
imported:
- "2019"
_4images_image_id: "36795"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36795 -->
Wie man sieht, verkabelt Sylvia als gelernte Mechatronikerin drastisch schöner als ihr Vater...
