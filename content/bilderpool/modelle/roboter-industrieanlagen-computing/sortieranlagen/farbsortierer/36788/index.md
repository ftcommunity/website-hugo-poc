---
layout: "image"
title: "Messstation (1)"
date: "2013-03-22T10:51:12"
picture: "farbsortierer03.jpg"
weight: "3"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36788
- /detailsbe35.html
imported:
- "2019"
_4images_image_id: "36788"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36788 -->
In der gegen Umgebungslicht abgeschirmten Messstation befinden sich

- eine Lichtschranke, die feststellt, wann ein Teil zu messen ist sowie
- eine Kombination aus Lampe und Fotowiderstand, die die Reflexion des gerade einliegenden Rades und somit seine Farbe misst.
