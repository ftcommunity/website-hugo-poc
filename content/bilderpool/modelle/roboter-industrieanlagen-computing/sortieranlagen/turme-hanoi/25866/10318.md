---
layout: "comment"
hidden: true
title: "10318"
date: "2009-12-01T19:51:36"
uploadBy:
- "Ironsmurf"
license: "unknown"
imported:
- "2019"
---
Richtig, der Elektromagnet stammt von Conrad.
Er kann ca. 900 Gramm heben, wenn er volle Spannung bekommt, für diesen Einsatz reichen die 9 Volt vom Interface.
Auf den Bechern sind die Deckel von Konservendosen geklebt. Und ebenfalls richtig löst ein Schalter aus, wenn der Ausleger auf eine Dose aufsetzt.
je nachdem welches Unterprogramm gerade abgearbeitet wird.