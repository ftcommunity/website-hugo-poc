---
layout: "image"
title: "Frontansicht"
date: "2016-01-24T16:06:06"
picture: "ursortieranlagemitlestaeben1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42729
- /details6464.html
imported:
- "2019"
_4images_image_id: "42729"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42729 -->
Diese Sortiermaschine ist ausschließlich aus unveränderten fischertechnik-Teilen aufgebaut, die es 1969 schon gab. Sie entstammt dem Band 2 der Anleitung der Lichtelektronik-Baukästen l-e1 bzw. l-e2 aus eben diesem Jahr. Die Anleitung ist unter http://ft-datenbank.de/details.php?ArticleVariantId=fc58cc86-c8aa-4556-8655-82edd3d8a3b8 ladbar; dieses Modell steht auf den Seiten mit den Seitennummern 168/169 (die Seitennummerierung setzt sich nahtlos nach Band 1 fort) bzw. PDF-Seiten76/77.

Das Modell hat mich fasziniert, seit ich es das erste Mal in der PDF sah. Darin ist auch nur ein einziges Gesamt-Foto und eine Detailaufnahme des Baustein-Rings um die Wippe dargestellt, und es ist so schlecht erkennbar, dass ich die Flachsteine zunächst für ein breites Gummiband hielt... Damals hatte man sich noch blind darauf verlassen (können!), dass die Leute den Trieb, das Modell zum Funktionieren zu bringen, von ganz alleine haben. Wenn ich da an diese Teil-für-Teil-Anleitungen für Doofe von heute denke... ;-) Aber auch daran, wie fischertechnik damals bis ins Extreme darauf achtete, ja nur mit wenigen Bauteilen auszukommen. Dafür gibt es in den beiden genannten Anleitungen schier unglaublich kreative Beispiele - unbedingte Anguck-Empfehlung!
