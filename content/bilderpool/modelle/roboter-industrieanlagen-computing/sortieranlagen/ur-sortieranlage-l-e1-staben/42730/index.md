---
layout: "image"
title: "Rückseite"
date: "2016-01-24T16:06:07"
picture: "ursortieranlagemitlestaeben2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42730
- /detailsdf06-2.html
imported:
- "2019"
_4images_image_id: "42730"
_4images_cat_id: "3182"
_4images_user_id: "104"
_4images_image_date: "2016-01-24T16:06:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42730 -->
Das Modell ist enorm teilesparend und mechanisch ganz einfach aufgebaut, allerdings äußerst knifflig zu justieren. Es gibt nur einen mini-mot 1 als einzigem aktiven Element darin. Bei der Elektronik wurde aber nicht gespart: Gleich zwei Lichtelektronik-Stäbe sind verbaut.
