---
layout: "overview"
title: "Ur-Sortieranlage mit l-e1-Stäben"
date: 2020-02-22T08:05:55+01:00
legacy_id:
- /php/categories/3182
- /categoriesfb23.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3182 --> 
Ein originalgetreuer Nachbau der wohl ersten dokumentierten Sortiermaschine für Bausteine - aus 1969!