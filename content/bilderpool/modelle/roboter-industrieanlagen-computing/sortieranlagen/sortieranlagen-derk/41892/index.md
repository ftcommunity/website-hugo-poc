---
layout: "image"
title: "Nieuwe Hoogregelaar 1"
date: "2015-09-14T17:32:04"
picture: "DSC_6659.jpg"
weight: "47"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/41892
- /detailsbb88.html
imported:
- "2019"
_4images_image_id: "41892"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2015-09-14T17:32:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41892 -->
Rechts op de foto de nieuwe hoog regelaar.