---
layout: "image"
title: "Neue Gripper 3"
date: "2014-12-06T12:36:44"
picture: "DSC_6055.jpg"
weight: "28"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/39891
- /detailsb4c5-2.html
imported:
- "2019"
_4images_image_id: "39891"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2014-12-06T12:36:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39891 -->
