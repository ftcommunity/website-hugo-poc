---
layout: "image"
title: "Neue Portal Robot 3"
date: "2014-07-03T22:25:50"
picture: "105.jpg"
weight: "19"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/38985
- /detailsb448.html
imported:
- "2019"
_4images_image_id: "38985"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2014-07-03T22:25:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38985 -->
