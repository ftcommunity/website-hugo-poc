---
layout: "image"
title: "Trainingsroboter 2016"
date: "2016-06-10T13:57:10"
picture: "IMG_2988.jpg"
weight: "59"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/43737
- /detailsa432-2.html
imported:
- "2019"
_4images_image_id: "43737"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2016-06-10T13:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43737 -->
Die Trainingsroboter mit powermotoren.