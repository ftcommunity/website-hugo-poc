---
layout: "image"
title: "FT23"
date: "2013-04-30T20:49:18"
picture: "FT_Community_023_FTC.jpg"
weight: "9"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/36887
- /detailsa63f-2.html
imported:
- "2019"
_4images_image_id: "36887"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2013-04-30T20:49:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36887 -->
