---
layout: "image"
title: "Neue Robotarm 6"
date: "2015-02-06T21:35:55"
picture: "DSC_6116.jpg"
weight: "36"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/40453
- /details9156-2.html
imported:
- "2019"
_4images_image_id: "40453"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2015-02-06T21:35:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40453 -->
