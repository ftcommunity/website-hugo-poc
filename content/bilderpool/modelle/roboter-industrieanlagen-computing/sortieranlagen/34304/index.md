---
layout: "image"
title: "Sortieranlage 2"
date: "2012-02-19T20:33:56"
picture: "FT_Derk_02-2012.jpg"
weight: "13"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/34304
- /details4090-3.html
imported:
- "2019"
_4images_image_id: "34304"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34304 -->
