---
layout: "overview"
title: "Autowaschstraße"
date: 2020-02-22T08:08:24+01:00
legacy_id:
- /php/categories/2468
- /categoriesb8f0.html
- /categories7f3a.html
- /categories3cfb.html
- /categories8611.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2468 --> 
Meine FT-Autowaschstraße simuliert eine vollautomatische Wasch- und Trocknungsanlage für PKW, wie sie beispielsweise an Tankstellen und in Werkstätten eingesetzt wird. Die Autowaschstraße besteht aus einem fahrbaren Portal, an dem ein Gebläsetrockner, eine Horizontalbürste, zwei Vertikalbürsten und zwei Felgenbürsten beweglich angebracht sind. Im Simulationsablauf wird ein PKW-Modell "gewaschen" und "getrocknet".