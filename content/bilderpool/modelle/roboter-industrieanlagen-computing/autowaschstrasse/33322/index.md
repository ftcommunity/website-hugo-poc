---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33322
- /detailsdd3b.html
imported:
- "2019"
_4images_image_id: "33322"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33322 -->
Fischertechnik Autowaschstrasse

Meine FT-Autowaschstraße simuliert eine vollautomatische Wasch- und Trocknungsanlage für PKW, wie sie beispielsweise an Tankstellen und in Werkstätten eingesetzt wird. Die Autowaschstraße besteht aus einem fahrbaren Portal, an dem ein Gebläsetrockner, eine Horizontalbürste, zwei Vertikalbürsten und zwei Felgenbürsten beweglich angebracht sind. Im Simulationsablauf wird ein PKW-Modell "gewaschen" und "getrocknet". 

.	Das Portal  bewegt sich um die Vertikalbürsten in die Position der Fahrzeug-hintenseite zu bringen. Ich nutze eine Distanz-Sensor (D1A) dafür. Die Vertikalbürsten beginnen sich zu drehen und aufeinander zuzufahren. Nach dem Aufeinandertreffen der beiden Vertikalbürsten, das durch einen Reed-Schalter erfasst wird, führen die beiden Vertikalbürsten zusammen eine Hin- und Herbewegung aus, die durch mechanische Endtaster begrenzt wird. 

.	Anschließend fahren die beiden Vertikalbürsten gerade zum PKW.   Position der Vertikalbürsten wird von einer Schiebepotentiometer bestimmt : gleich die  Distanz-Sensorwert (D1A).  Es gibt grosse und kleinere PKW !!!
http://www.conrad.de/ce/de/product/442121/SCHIEBEPOTENTIOMETER

.	Das Portal bewegt sich weiter während sich die Querbürste zu drehen beginnt und die Vertikalbürsten ihre Drehung fortsetzen. Die Querbürste fährt, gesteuert von zwei Laser-Lichtschranken signalen die Höhenkontur des Fahrzeugs ab. Ich habe verschiedene billige Laserpointer dafür genutzt.
http://www.conrad.de/ce/de/product/776265/LASERPOINTER-SCHLUeSSELANHAeNGER-ROT

Für die spanung der Laserpointer nutze ich eine Conrad Spannungsregler-Platine für LM 317-T Ausgangsspannung 1.2 - 32 V/DC:
http://www.conrad.de/ce/de/product/130312/UNIVERSAL-SPANNUNGSREGLER-BAUSTEIN

.	Erreichen bei dieser Portalbewegung die Felgenbürsten die Vorderräder, bzw. die Hinterräder des Fahrzeugs, so stoppt die Portalbewegung kurzzeitig, um einen Felgenreinigungsvorgang anzudeuten. Bei diesem beginnen sich die Felgenbürsten zu drehen und bewegen sich pneumatisch bis zum PKW-Felgen. Sie verweilen kurz und bewegen pneumatisch wieder in die Ausgangsposition zurück.  

Als Kompressor nütze ich die sehr bilige Pollin-Luftpumpe CONJOIN CJP37-C12A2
Funktioniert prima und past im FT-Raster !   Die Pumpen wurden aus Geräten ausgebaut, waren nur kurzzeitig in Betrieb und sind voll funktionsfähig.  Pollinbestelnr: 330 036
http://www.pollin.de/shop/dt/MzY5OTY2OTk-/Bauelemente_Bauteile/Pumpen/Luftpumpe_CONJOIN_CJP37_C12A2.html
.	Wenn die Vertikalbürsten das Fahrzeugfront-heck erreicht haben, stoppt wieder die Bewegung des Portals und die beiden Vertikalbürsten führen dieselbe Bewegung wie zuvor aus. Während dieser Zeit kehrt sich die Drehrichtung der Querbürste um, um das Rückfenster und die Kofferraumpartie des Wagens noch besser zu reinigen als das Portal zum Anfangsposition zuruck bewegt. 

.	Anschließend fahren die Vertikalbürsten in ihre äußere Endlage und die Querbürste in ihre obere Endstellung, wobei die Bürstendrehungen gestoppt wird. 

.	Der Gebläsetrockner, der wie zuvor die Querbürste von zwei Laser-Lichtschrankensignalen gesteuert wird, fährt die Fahrzeugkontur ab, um einen Trocknungsvorgang zu simulieren. Die obere und untere Endlage von Querbürste und Gebläsetrockner werden durch Schalter erfasst.   
Ich habe herausgefunden dass :
o	Fotozellen mit dem richtingen kleine Störlichtkappen  mit Laserlicht  ebenso gut funktionieren als 
o	Fototransistoren  mit Laserlicht.
