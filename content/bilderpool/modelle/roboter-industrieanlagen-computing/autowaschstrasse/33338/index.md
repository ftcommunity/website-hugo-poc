---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33338
- /details4757-2.html
imported:
- "2019"
_4images_image_id: "33338"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33338 -->
Erreichen bei dieser Portalbewegung die Felgenbürsten die Vorderräder, bzw. die Hinterräder des Fahrzeugs, so stoppt die Portalbewegung kurzzeitig, um einen Felgenreinigungsvorgang anzudeuten. Bei diesem beginnen sich die Felgenbürsten zu drehen und bewegen sich pneumatisch bis zum PKW-Felgen. Sie verweilen kurz und bewegen pneumatisch wieder in die Ausgangsposition zurück.  


