---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse19.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33340
- /details5237.html
imported:
- "2019"
_4images_image_id: "33340"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33340 -->
Der Gebläsetrockner, der wie zuvor die Querbürste von zwei Laser-Lichtschrankensignalen gesteuert wird, fährt die Fahrzeugkontur ab, um einen Trocknungsvorgang zu simulieren. Die obere und untere Endlage von Querbürste und Gebläsetrockner werden durch Schalter erfasst.
