---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse10.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33331
- /details2850.html
imported:
- "2019"
_4images_image_id: "33331"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33331 -->
Anschließend fahren die beiden Vertikalbürsten gerade zum PKW.   Position der Vertikalbürsten wird von einer Schiebepotentiometer bestimmt : gleich die  Distanz-Sensorwert (D1A).  Es gibt grosse und kleinere PKW !!!
http://www.conrad.de/ce/de/product/442121/SCHIEBEPOTENTIOMETER
