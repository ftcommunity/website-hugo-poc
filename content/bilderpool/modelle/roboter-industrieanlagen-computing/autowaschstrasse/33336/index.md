---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse15.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33336
- /detailsa63c-2.html
imported:
- "2019"
_4images_image_id: "33336"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33336 -->
Die Querbürste fährt, gesteuert von zwei Laser-Lichtschranken signalen die Höhenkontur des Fahrzeugs ab. 

