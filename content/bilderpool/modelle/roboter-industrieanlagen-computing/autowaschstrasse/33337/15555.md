---
layout: "comment"
hidden: true
title: "15555"
date: "2011-10-26T18:03:37"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Auch interessant:  http://www.ftcommunity.de/categories.php?cat_id=1523

Bei Conrad gibt es als alternative: Laserpointmodule 1MW LM01RDD ( 506110 ) á 35 Euro. 
http://www.conrad.de/ce/de/product/506110/Lasermodul-Punktlaser-Rot-LM01RDD-Rot-Leistung-1-mW-Laserklasse-2 


Interessanter und billiger sind aber die 5 Euro Laserpointer nr. 776265 
http://www.conrad.de/ce/de/product/776265/LASERPOINTER-SCHLUeSSELANHAeNGER-ROT 

Ich verwende diese für meine Auto-wasstrasse 

Grüss, 

Peter 
Poederoyen NL