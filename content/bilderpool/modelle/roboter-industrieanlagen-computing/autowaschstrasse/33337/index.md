---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse16.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33337
- /detailsc727.html
imported:
- "2019"
_4images_image_id: "33337"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33337 -->
Ich habe verschiedene billige Laserpointer genutzt.
http://www.conrad.de/ce/de/product/776265/LASERPOINTER-SCHLUeSSELANHAeNGER-ROT

Für die spanung der Laserpointer nutze ich eine Conrad Spannungsregler-Platine für LM 317-T Ausgangsspannung 1.2 - 32 V/DC:
http://www.conrad.de/ce/de/product/130312/UNIVERSAL-SPANNUNGSREGLER-BAUSTEIN

Ich habe herausgefunden dass :
o	Fotozellen mit dem richtingen kleine Störlichtkappen  mit Laserlicht  ebenso gut funktionieren als 
o	Fototransistoren  mit Laserlicht.
