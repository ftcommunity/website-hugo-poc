---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse08.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33329
- /details5ea0.html
imported:
- "2019"
_4images_image_id: "33329"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33329 -->
Nach dem Aufeinandertreffen der beiden Vertikalbürsten, das durch einen Reed-Schalter erfasst wird, führen die beiden Vertikalbürsten zusammen eine Hin- und Herbewegung aus, die durch mechanische Endtaster begrenzt wird.
