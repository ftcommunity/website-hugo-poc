---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:54"
picture: "autowaschstrasse18.jpg"
weight: "18"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33339
- /detailsd427.html
imported:
- "2019"
_4images_image_id: "33339"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:54"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33339 -->
Erreichen bei dieser Portalbewegung die Felgenbürsten die Vorderräder, bzw. die Hinterräder des Fahrzeugs, so stoppt die Portalbewegung kurzzeitig, um einen Felgenreinigungsvorgang anzudeuten. Bei diesem beginnen sich die Felgenbürsten zu drehen und bewegen sich pneumatisch bis zum PKW-Felgen. Sie verweilen kurz und bewegen pneumatisch wieder in die Ausgangsposition zurück.  

Als Kompressor nütze ich die sehr bilige Pollin-Luftpumpe CONJOIN CJP37-C12A2
Funktioniert prima und past im FT-Raster !   Die Pumpen wurden aus Geräten ausgebaut, waren nur kurzzeitig in Betrieb und sind voll funktionsfähig.  Pollinbestelnr: 330 036
http://www.pollin.de/shop/dt/MzY5OTY2OTk-/Bauelemente_Bauteile/Pumpen/Luftpumpe_CONJOIN_CJP37_C12A2.html
