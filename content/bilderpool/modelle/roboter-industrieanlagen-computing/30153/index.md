---
layout: "image"
title: "3D-Rob gerendert mit POV-RAY"
date: "2011-02-27T20:53:29"
picture: "FT-Robot_POV1.jpg"
weight: "16"
konstrukteure: 
- "??"
fotografen:
- "con.barriga"
schlagworte: ["POV-RAY", "ldraw"]
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30153
- /details7256.html
imported:
- "2019"
_4images_image_id: "30153"
_4images_cat_id: "125"
_4images_user_id: "1284"
_4images_image_date: "2011-02-27T20:53:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30153 -->
