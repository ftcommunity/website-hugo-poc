---
layout: "image"
title: "Komplette Ansicht"
date: "2013-02-15T00:33:19"
picture: "IMG_4606.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/36631
- /details85ee.html
imported:
- "2019"
_4images_image_id: "36631"
_4images_cat_id: "2715"
_4images_user_id: "1631"
_4images_image_date: "2013-02-15T00:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36631 -->
Hier folgen nun ein paar Bilder von dem versuch eine Kreissäge aus Fischertechnik nach zu bauen.