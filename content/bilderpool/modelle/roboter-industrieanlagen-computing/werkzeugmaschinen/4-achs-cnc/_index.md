---
layout: "overview"
title: "4-Achs CNC"
date: 2020-02-22T08:06:09+01:00
legacy_id:
- /php/categories/3424
- /categories239b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3424 --> 
Hier die Fotos meiner 4-Achs CNC Maschine ;-)

Der Hauptteil der Fräse wird mit einem TX angesteuert. Die Spindel wird über eins der alten Interfaces angesteuert.
Weitere Features:

-Automatische Türen
-Druckluftanschluss
-Werkzeughalterung
-Beleuchtung