---
layout: "image"
title: "Maximale Verfahrwege"
date: "2017-08-01T09:50:58"
picture: "achscnc4.jpg"
weight: "4"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/46107
- /details239e.html
imported:
- "2019"
_4images_image_id: "46107"
_4images_cat_id: "3424"
_4images_user_id: "1164"
_4images_image_date: "2017-08-01T09:50:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46107 -->
Das Werkstück kann in X Y Richtung circa 30cm verfahren. Der Fräskopf kann sich um circa 320° drehen und verfährt in Z Richtung auch circa 30 cm