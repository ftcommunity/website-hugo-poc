---
layout: "image"
title: "Gesammtansicht"
date: "2005-05-14T21:16:17"
picture: "motorisierte_Roboter_005.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4143
- /details0d47.html
imported:
- "2019"
_4images_image_id: "4143"
_4images_cat_id: "353"
_4images_user_id: "189"
_4images_image_date: "2005-05-14T21:16:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4143 -->
Schweißroboter
