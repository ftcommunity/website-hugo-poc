---
layout: "image"
title: "untere Dreh-Bewegung"
date: "2005-05-17T12:48:32"
picture: "motorisierte_Roboter_017.jpg"
weight: "5"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4151
imported:
- "2019"
_4images_image_id: "4151"
_4images_cat_id: "353"
_4images_user_id: "189"
_4images_image_date: "2005-05-17T12:48:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4151 -->
