---
layout: "image"
title: "Schweißroboter-1. Achse"
date: "2006-12-20T17:23:14"
picture: "Schweiroboter_003.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/7983
- /details17c8-2.html
imported:
- "2019"
_4images_image_id: "7983"
_4images_cat_id: "745"
_4images_user_id: "420"
_4images_image_date: "2006-12-20T17:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7983 -->
