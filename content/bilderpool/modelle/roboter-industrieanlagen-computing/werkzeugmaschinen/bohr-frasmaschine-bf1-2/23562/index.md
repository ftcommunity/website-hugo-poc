---
layout: "image"
title: "[3/5] BF1-2-RI, Mini-Motoren 1"
date: "2009-03-30T20:09:36"
picture: "bfetappe3.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23562
- /detailsba72.html
imported:
- "2019"
_4images_image_id: "23562"
_4images_cat_id: "1610"
_4images_user_id: "723"
_4images_image_date: "2009-03-30T20:09:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23562 -->
Die Motoren des Kreuzschlittens links (Linearachse X) und rechts (Linearachse Y) sowie oben des Teilkopfes (Drehachse A).
