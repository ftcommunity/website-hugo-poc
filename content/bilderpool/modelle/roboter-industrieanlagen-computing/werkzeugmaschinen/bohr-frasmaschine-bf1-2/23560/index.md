---
layout: "image"
title: "[1/5] BF1-2-RI, Modellanlage"
date: "2009-03-30T20:09:36"
picture: "bfetappe1.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23560
- /details82c6.html
imported:
- "2019"
_4images_image_id: "23560"
_4images_cat_id: "1610"
_4images_user_id: "723"
_4images_image_date: "2009-03-30T20:09:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23560 -->
Die Modellanlage der Bohr- und Fräsmaschine BF1 in der Etappe 2 als BF1-2-RI mit der Technik-Generation ROBO Interface.
Das Modell in der Etappe 1 hatte ausser der Arbeitsspindel noch Handkurbeln und ist jetzt mit 7 Motoren ausgestattet. Es wird nun von einer 6+1-Achsen-Steuerung bewegt. Die Verkabelung ist noch als "Versuchsfeld" provisorisch mit Festlängen aus den Baukästen ausgeführt. Die aufklappbare Kopfverkleidung wurde hier mal abgenommen zur optischen Freisetzung der Antriebsmechanik an der Säule.
Nachtrag 31.03.2009:
BF1 mit Handkurbeln - Vorstellung von Aufbau und Zubehör
http://www.ftcommunity.de/details.php?image_id=15967
