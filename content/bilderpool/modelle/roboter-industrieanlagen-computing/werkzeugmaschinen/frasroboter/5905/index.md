---
layout: "image"
title: "Fräse"
date: "2006-03-18T22:59:05"
picture: "Fischertechnik-Bilder_012.jpg"
weight: "5"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5905
- /detailscbff.html
imported:
- "2019"
_4images_image_id: "5905"
_4images_cat_id: "512"
_4images_user_id: "420"
_4images_image_date: "2006-03-18T22:59:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5905 -->
