---
layout: "image"
title: "Gesamter Styroporschneider"
date: "2011-07-22T16:23:27"
picture: "g01.jpg"
weight: "1"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31329
- /detailscfd6.html
imported:
- "2019"
_4images_image_id: "31329"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31329 -->
Man sieht links das Schneidegerüst, rechts das Interface