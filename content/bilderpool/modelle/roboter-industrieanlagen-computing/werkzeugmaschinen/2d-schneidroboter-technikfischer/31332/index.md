---
layout: "image"
title: "X-System"
date: "2011-07-22T16:23:27"
picture: "g04.jpg"
weight: "4"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31332
- /details4cc9-2.html
imported:
- "2019"
_4images_image_id: "31332"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31332 -->
Hier das System, durch dass die X-Achsen angetrieben werden