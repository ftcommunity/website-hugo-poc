---
layout: "image"
title: "Y-Achse"
date: "2011-07-22T16:23:27"
picture: "g07.jpg"
weight: "7"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31335
- /details361b.html
imported:
- "2019"
_4images_image_id: "31335"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31335 -->
Die Y-Achse und die Kabel, die zum verschiebbaren Teil gehören