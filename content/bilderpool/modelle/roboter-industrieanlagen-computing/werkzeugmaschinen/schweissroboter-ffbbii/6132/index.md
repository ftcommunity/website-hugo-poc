---
layout: "image"
title: "kleine Schweißstraße Gesamtansicht"
date: "2006-04-17T20:29:35"
picture: "Fischertechnik-Bilder_025.jpg"
weight: "6"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/6132
- /detailsf624.html
imported:
- "2019"
_4images_image_id: "6132"
_4images_cat_id: "634"
_4images_user_id: "420"
_4images_image_date: "2006-04-17T20:29:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6132 -->
