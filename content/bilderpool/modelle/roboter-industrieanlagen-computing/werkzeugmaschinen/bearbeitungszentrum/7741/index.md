---
layout: "image"
title: "Schalter"
date: "2006-12-08T22:51:19"
picture: "DSCI0032.jpg"
weight: "10"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Taster"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7741
- /details4bff.html
imported:
- "2019"
_4images_image_id: "7741"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7741 -->
Das ist der Taster mit dem man durch das Programm geführt wird. Am Anfang wenn man das Programm neustartet muss man nämlich die verbliebenen Werkstücke des letzten Arbeitsganges entfernen und nachdem alle Motoren in ihre Ausgansstellung gefahren sind den Taster drücken damit der nächste Schritt eingeleitet wird.