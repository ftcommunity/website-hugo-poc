---
layout: "image"
title: "Ansicht 1"
date: "2006-12-08T22:51:18"
picture: "DSCI0020.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7732
- /detailsd10b.html
imported:
- "2019"
_4images_image_id: "7732"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7732 -->
Das ist das Bearbeitungszentrum mit Magazin, Stanze, Bohrmaschine und Abförderband.