---
layout: "image"
title: "2. Bohrmaschine"
date: "2006-12-17T11:38:54"
picture: "Bearbeitungszentrum_056.jpg"
weight: "18"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Bohrmaschine"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7911
- /details5164.html
imported:
- "2019"
_4images_image_id: "7911"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-17T11:38:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7911 -->
Das ist die zweite Bohrmaschine die man anstatt der ersten anbringen kann. Sie wirkt etwas realistischer da sie noch etwas runterfährt. Allerdings ist es auch wieder etwas unlogisch das sich der Bohrer immer in die Falsche Richtung dreht.