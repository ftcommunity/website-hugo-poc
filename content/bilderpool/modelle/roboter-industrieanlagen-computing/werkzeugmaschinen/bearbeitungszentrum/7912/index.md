---
layout: "image"
title: "2. Bohrmaschine"
date: "2006-12-17T11:38:54"
picture: "Bearbeitungszentrum_057.jpg"
weight: "19"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Kegelzahnräder"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7912
- /detailsa0c6.html
imported:
- "2019"
_4images_image_id: "7912"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-17T11:38:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7912 -->
Noch einmal die Kegelzahnräder