---
layout: "image"
title: "Personal"
date: "2006-12-08T22:51:19"
picture: "DSCI0028.jpg"
weight: "8"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Personal"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7739
- /detailsc20c.html
imported:
- "2019"
_4images_image_id: "7739"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7739 -->
...diesen beiden Angestellten geprüft und notfalls aussortiert werden.