---
layout: "image"
title: "Werkstücke"
date: "2006-12-08T22:51:26"
picture: "DSCI0023.jpg"
weight: "13"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Werkstücke"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7744
- /details63d2-2.html
imported:
- "2019"
_4images_image_id: "7744"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7744 -->
Das sind einige Werkstücke die im 14-Sekundentakt aus der Maschine befördert werden.