---
layout: "comment"
hidden: true
title: "1737"
date: "2006-12-17T13:18:09"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Franz,

das Dumme am Rast-Kegelrad ist aber, dass es nur eine Achse aufnehmen kann. Da ist Essig mit "Welle weiterführen".

Du kannst aber die Antriebswelle für den Bohrer verlängern und das Kegelzahnrad dann "rückwärts" draufsetzen, dann dreht der Bohrer auch anders herum:
http://www.ftcommunity.de/details.php?image_id=7122
(Habe ich irgendwo bei Claus abgeguckt, finde aber das Original nicht mehr)


Gruß,
Harald