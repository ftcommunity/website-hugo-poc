---
layout: "image"
title: "Auswerfer"
date: "2006-12-08T22:51:19"
picture: "DSCI0014.jpg"
weight: "7"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Auswerfer"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7738
- /detailsf531-2.html
imported:
- "2019"
_4images_image_id: "7738"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7738 -->
Das ist der Auswerfer der die Werkstücke vom Drehtisch auf das Abförderband befördert wo sie von...