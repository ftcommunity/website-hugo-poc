---
layout: "image"
title: "Magazin"
date: "2006-12-08T22:51:19"
picture: "DSCI0019.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Bearbeitungszentrum", "Magazin"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/7734
- /details9d73.html
imported:
- "2019"
_4images_image_id: "7734"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7734 -->
Wie man in diesem Bild sehr gut sehen kann hat das Magazin eine Lichtschranke die, die Produktion stoppt sollte das Magazin leer sein.