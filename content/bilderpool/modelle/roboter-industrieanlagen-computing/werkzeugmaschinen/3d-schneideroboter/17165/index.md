---
layout: "image"
title: "(8) Y-Achse Impulszähler"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter8.jpg"
weight: "8"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17165
- /details268a.html
imported:
- "2019"
_4images_image_id: "17165"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17165 -->
