---
layout: "image"
title: "(6) X-Achse"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter6.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17163
- /detailsc106.html
imported:
- "2019"
_4images_image_id: "17163"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17163 -->
