---
layout: "image"
title: "(4) Motor Drehkranz"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter4.jpg"
weight: "4"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17161
- /detailsf86a.html
imported:
- "2019"
_4images_image_id: "17161"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17161 -->
