---
layout: "image"
title: "(3) Schneidefläche"
date: "2009-01-24T17:46:14"
picture: "dschneideroboter3.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/17160
- /details2aab.html
imported:
- "2019"
_4images_image_id: "17160"
_4images_cat_id: "1540"
_4images_user_id: "592"
_4images_image_date: "2009-01-24T17:46:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17160 -->
