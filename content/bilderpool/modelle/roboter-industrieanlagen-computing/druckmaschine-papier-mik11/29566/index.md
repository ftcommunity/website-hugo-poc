---
layout: "image"
title: "Druckmaschine - Seitenansicht"
date: "2011-01-01T11:41:32"
picture: "P1000194.jpg"
weight: "3"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/29566
- /detailsc409.html
imported:
- "2019"
_4images_image_id: "29566"
_4images_cat_id: "3398"
_4images_user_id: "1258"
_4images_image_date: "2011-01-01T11:41:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29566 -->
