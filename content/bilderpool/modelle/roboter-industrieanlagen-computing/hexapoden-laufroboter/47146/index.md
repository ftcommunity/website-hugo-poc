---
layout: "image"
title: "IMG_20160604_080908"
date: "2018-01-21T09:22:20"
picture: "roboter01.jpg"
weight: "1"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47146
- /details1105.html
imported:
- "2019"
_4images_image_id: "47146"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47146 -->
ein angefangenes projekt aus fischertechnik das schlussendlich in einem metallroboter endete.
 
einen sechsbeinigen laufroboter mit 18-  Freiheitsgraden  zu bauen ist echt schwierig. 

hier mein vorschlag/Idee. Man kann sehen dass noch 4 Beine fehlen ( hinten beide komplett und die anderen 2 gegenüberligenden Beine vorne und in der mitte). 

das Ganze sieht dann aus wie ein Insekt ohne kopf und hinterteil. 


Leider musste ich feststellen dass das projekt mit fischertechnik nicht ganz einfach ist. Somit habe ich erstmal mit sperholz experimentiert und bin erst nur zu einem halben fischertechnik roboter gekommen.


Ich hoffe Ihr könnt euch vorstellen wie das ganze aussehen soll