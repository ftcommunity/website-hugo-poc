---
layout: "image"
title: "IMG_20160604_081143"
date: "2018-01-21T09:22:20"
picture: "roboter08.jpg"
weight: "8"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47153
- /details1bd5.html
imported:
- "2019"
_4images_image_id: "47153"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47153 -->
man sieht den bewegungsspielraum der beine. und wie die Füße mal aussehen sollen