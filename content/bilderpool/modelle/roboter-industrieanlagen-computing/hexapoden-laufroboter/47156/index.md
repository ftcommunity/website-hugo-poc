---
layout: "image"
title: "projekt"
date: "2018-01-21T09:22:20"
picture: "roboter11.jpg"
weight: "11"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47156
- /detailsfa14.html
imported:
- "2019"
_4images_image_id: "47156"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47156 -->
der kopf geöffnet