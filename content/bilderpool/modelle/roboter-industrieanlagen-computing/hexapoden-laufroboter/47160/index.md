---
layout: "image"
title: "IMG_20160604_081413"
date: "2018-01-21T09:22:21"
picture: "roboter15.jpg"
weight: "15"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- /php/details/47160
- /details7ab7.html
imported:
- "2019"
_4images_image_id: "47160"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47160 -->
ein befestigungspunkt für ein bein mit kabelanschluss
