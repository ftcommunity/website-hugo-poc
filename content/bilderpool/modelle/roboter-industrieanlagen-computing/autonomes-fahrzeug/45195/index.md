---
layout: "image"
title: "Gesamtansicht RoBoss-Fahrzeugmodell"
date: "2017-02-12T20:43:13"
picture: "selbststaendigfahrendeseinparkendesautocarolocup1.jpg"
weight: "1"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "johafo"
license: "unknown"
legacy_id:
- /php/details/45195
- /detailsdfbc.html
imported:
- "2019"
_4images_image_id: "45195"
_4images_cat_id: "3366"
_4images_user_id: "1292"
_4images_image_date: "2017-02-12T20:43:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45195 -->
Bei dem seit 2008 jährlich an der TU Braunschweig veranstalteten Carolo-Cup treten Studierendenteams mit selbst entwickelten autonomen Fahrzeugen im Maßstab 1:10 gegeneinander an. Die Fahrzeugmodelle müssen einem Staßenverlauf mit Kreuzungen und unterbrochenen Fahrbahnmarkierungen folgen und selbstständig einparken können (http://www.carolocup.de).

Als Team RoBoss sind wir, Johann Fox und Robin Pfannendörfer, mit einem fischertechnik-Auto gestartet. Bis auf einen Laser, ein Servo-Shield und zwei Spannungswandler besteht der Roboter zu 100 % aus fischertechnik. Am 06.02.2017 fand unser offizieller Wertungslauf statt. In der Disziplin "Rundkurs ohne Hindernisse" legte unser Auto souverän 90 Meter zurück und in der Disziplin "Einparken" parkten wir in weniger als 8 Sekunden ein. Beweisvideo: https://youtu.be/K7pG3Md4btM

Im Hauptwettbewerb der studentischen Teams hätten wir beim Einparken den dritten Platz, beim Rundkurs immerhin noch einen 8. Platz (von 12 Teams) erreicht. Den Junior-Cup hätten wir - wenn wir zugelassen worden wären - mit diesen Ergebnissen gewonnen.