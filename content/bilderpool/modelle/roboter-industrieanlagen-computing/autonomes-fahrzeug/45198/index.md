---
layout: "image"
title: "Sichtfeld der Kamera (schematisch)"
date: "2017-02-12T20:43:14"
picture: "selbststaendigfahrendeseinparkendesautocarolocup4.jpg"
weight: "4"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "johafo"
license: "unknown"
legacy_id:
- /php/details/45198
- /details345b.html
imported:
- "2019"
_4images_image_id: "45198"
_4images_cat_id: "3366"
_4images_user_id: "1292"
_4images_image_date: "2017-02-12T20:43:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45198 -->
Dieses Schema gibt maßstabsgetreu das Sichtfeld der Kamera wieder.

Unsere erste Messreihe erkennt die Spur etwa 20 cm vor dem Fahrzeug, die zweite liegt etwa 1 m davor.
Mit der ft-Kamera kann man also arbeiten! Jetzt muss die Spur nur noch erkannt und ausgewertet werden. Wie das funktioniert erklärt Robin in dem Video: https://youtu.be/K7pG3Md4btM