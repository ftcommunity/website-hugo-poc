---
layout: "overview"
title: "Radar 360 (Abstandsensoren)"
date: 2020-02-22T08:08:08+01:00
legacy_id:
- /php/categories/760
- /categories06f4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=760 --> 
Bewegungen im Wohnzimmer mit 4 Robo Schnittstellen und acht Abstandsensoren feststellen.