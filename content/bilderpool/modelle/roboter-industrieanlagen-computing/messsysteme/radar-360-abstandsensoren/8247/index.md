---
layout: "image"
title: "Van de zijkant..."
date: "2007-01-02T14:58:38"
picture: "fischertechnik_005.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/8247
- /details13e7.html
imported:
- "2019"
_4images_image_id: "8247"
_4images_cat_id: "760"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8247 -->
