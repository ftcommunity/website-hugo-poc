---
layout: "image"
title: "gesamte Versuchsanordnung"
date: "2008-02-01T17:44:25"
picture: "Anordnung_gesamt_von_vorne.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13501
- /details0a5d.html
imported:
- "2019"
_4images_image_id: "13501"
_4images_cat_id: "1233"
_4images_user_id: "731"
_4images_image_date: "2008-02-01T17:44:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13501 -->
Dies ist die gesamte Anordnung: optische Geräte zur Zerlegung des Lichtes in seine Bestandteile und die anschließende Messanordnung zur Aufnahme der Spekten.
