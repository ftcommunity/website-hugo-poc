---
layout: "image"
title: "MPU9255 Gyroscope data"
date: "2016-02-13T21:30:12"
picture: "MPU9255_Gyroscope.jpg"
weight: "10"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
schlagworte: ["MPU6050", "MPU9150", "MPU9250", "Gyroscope", "Speed", "control", "Sensor"]
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/42868
- /details07fa.html
imported:
- "2019"
_4images_image_id: "42868"
_4images_cat_id: "2887"
_4images_user_id: "2374"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42868 -->
Objective: I2C MPU driver optimization wrt user needs.
Setup: Encodermotor with Turning table and lever arm. The Sensor MPU9255 is located at the outer position with a lever arm of approx. 17cm.
Table is rotated firstly by +360° afterwards by -360° with two different speed.
No closed loop control.
MPU9255 Gyroscope Sensor data has been recorded.
Issues:
&#8220;Closed loop Speed control &#8220; is missing in RoboPro
Accurate sampling rat