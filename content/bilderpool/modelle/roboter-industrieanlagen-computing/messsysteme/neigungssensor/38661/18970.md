---
layout: "comment"
hidden: true
title: "18970"
date: "2014-04-26T17:29:32"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Sehr schöne Idee.
Für Anwendungen an einem bewegten Objekt ist wahrscheinlich ein Gewicht am Pendel und eine schwergängigere Achse sinnvoll, damit das Pendel nicht durch die Bewegung des Objekts schwankt.
Gruß, Dirk