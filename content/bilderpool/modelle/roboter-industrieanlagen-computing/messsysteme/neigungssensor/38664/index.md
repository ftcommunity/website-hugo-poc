---
layout: "image"
title: "Neigungssensor_004"
date: "2014-04-25T22:31:17"
picture: "004.jpg"
weight: "4"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
schlagworte: ["Neigungssensor", "Gleichgewicht", "Sensor", "Gyro", "Messen", "Messtechnik"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38664
- /details8791.html
imported:
- "2019"
_4images_image_id: "38664"
_4images_cat_id: "2887"
_4images_user_id: "2167"
_4images_image_date: "2014-04-25T22:31:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38664 -->
