---
layout: "image"
title: "BNO055 some Sensor data like Quaternions, Heading, Roll, Pitch and Acceleration"
date: "2016-02-13T21:30:12"
picture: "BNO055_Sensor_data.jpg"
weight: "9"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
schlagworte: ["BNO055", "Sensor", "Quaternions", "Heading", "Roll", "Pitch", "Acceleration"]
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/42867
- /details506a.html
imported:
- "2019"
_4images_image_id: "42867"
_4images_cat_id: "2887"
_4images_user_id: "2374"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42867 -->
Objective: I2C MPU driver optimization wrt user needs.
Setup: Encodermotor with Turning table and lever arm. The Sensor BNO055 is located at the outer position with a lever arm of approx. 17cm.
Table is rotated firstly by +360° afterwards by -360° with two different speed.

BNO055 Sensor data has been recorded (not all)
Issues:
Accurate sampling rate
Data transfer capacity