---
layout: "overview"
title: "Akku - und Batteriemessstation"
date: 2020-02-22T08:08:09+01:00
legacy_id:
- /php/categories/2001
- /categories6e4a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2001 --> 
Mit diesem Gerät kann man ganz einfach 9V und 1,2(1,5) Batterien und Akkus
nachmessen.