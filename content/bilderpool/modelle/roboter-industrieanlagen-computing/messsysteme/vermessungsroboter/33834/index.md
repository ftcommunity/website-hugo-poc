---
layout: "image"
title: "Unterprogramm Kalibrierung"
date: "2012-01-02T19:55:05"
picture: "Kalib.jpg"
weight: "6"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33834
- /details1269.html
imported:
- "2019"
_4images_image_id: "33834"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33834 -->
Unterprogramm Kalibrierung - Misst die Zeit zum abfahren der Strecke, um Schwankungen der Versorgungsspannung auszugleichen.