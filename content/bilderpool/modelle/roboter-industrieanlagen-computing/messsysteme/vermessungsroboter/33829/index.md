---
layout: "image"
title: "Gesamtansicht"
date: "2012-01-02T19:55:04"
picture: "Bildschirmfoto_2012-01-02_um_17.19.53.jpg"
weight: "1"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33829
- /details2cbf.html
imported:
- "2019"
_4images_image_id: "33829"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33829 -->
Youtube Video unter: http://www.youtube.com/watch?v=dHm5MEEubzY

Der Messroboter besteht aus einem motorisierten Arm, der an seiner Spitze einen Taster trägt, um die zu messenden Objekte zu erfassen. Der maximale Messweg beträgt 9,6 Centimeter.