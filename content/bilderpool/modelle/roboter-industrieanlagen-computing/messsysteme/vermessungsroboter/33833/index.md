---
layout: "image"
title: "Hauptprogramm"
date: "2012-01-02T19:55:05"
picture: "Hauptprogramm.jpg"
weight: "5"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- /php/details/33833
- /details4cbe-2.html
imported:
- "2019"
_4images_image_id: "33833"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33833 -->
Hauptporgramm - Dient zum einmaligen aufrufen des Unterprogramms Kalibrieren und Messen als Schleife.