---
layout: "image"
title: "Plotter 5"
date: "2007-12-09T13:33:17"
picture: "plotter5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13024
- /details9daf-2.html
imported:
- "2019"
_4images_image_id: "13024"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13024 -->
