---
layout: "image"
title: "Neue Stifthalterung 2"
date: "2008-02-24T14:33:01"
picture: "plotter3_3.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13731
- /detailsa8fd.html
imported:
- "2019"
_4images_image_id: "13731"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2008-02-24T14:33:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13731 -->
