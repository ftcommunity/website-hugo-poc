---
layout: "image"
title: "Plotter 7"
date: "2007-12-09T13:33:17"
picture: "plotter7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13026
- /details4152-3.html
imported:
- "2019"
_4images_image_id: "13026"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13026 -->
