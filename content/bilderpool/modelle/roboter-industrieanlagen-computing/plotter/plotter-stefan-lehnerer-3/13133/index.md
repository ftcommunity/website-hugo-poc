---
layout: "image"
title: "Plotter 9"
date: "2007-12-21T16:24:43"
picture: "plotter2_2.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13133
- /detailsa9e9.html
imported:
- "2019"
_4images_image_id: "13133"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-21T16:24:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13133 -->
