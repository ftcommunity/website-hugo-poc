---
layout: "image"
title: "Plotter 10"
date: "2007-12-21T16:24:43"
picture: "plotter3_2.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13134
- /detailseae9.html
imported:
- "2019"
_4images_image_id: "13134"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-21T16:24:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13134 -->
