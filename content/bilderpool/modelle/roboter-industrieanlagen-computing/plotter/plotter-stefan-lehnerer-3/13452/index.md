---
layout: "image"
title: "Ergebnis"
date: "2008-01-27T18:48:00"
picture: "plotter1_3.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13452
- /details2858.html
imported:
- "2019"
_4images_image_id: "13452"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2008-01-27T18:48:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13452 -->
Jetzt endlich mit Bresenham-Algorithmus!
