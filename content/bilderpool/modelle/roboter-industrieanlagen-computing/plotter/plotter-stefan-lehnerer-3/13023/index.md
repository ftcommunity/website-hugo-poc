---
layout: "image"
title: "Plotter 4"
date: "2007-12-09T13:33:17"
picture: "plotter4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13023
- /details9476.html
imported:
- "2019"
_4images_image_id: "13023"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13023 -->
