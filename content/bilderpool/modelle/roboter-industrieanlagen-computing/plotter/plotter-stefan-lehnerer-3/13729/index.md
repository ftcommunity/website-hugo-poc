---
layout: "image"
title: "Ergebnis 2"
date: "2008-02-24T14:33:00"
picture: "plotter1_4.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13729
- /details474f-2.html
imported:
- "2019"
_4images_image_id: "13729"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2008-02-24T14:33:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13729 -->
