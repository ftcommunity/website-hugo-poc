---
layout: "image"
title: "Plotter 6"
date: "2007-12-09T13:33:17"
picture: "plotter6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13025
- /detailsde8c.html
imported:
- "2019"
_4images_image_id: "13025"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13025 -->
