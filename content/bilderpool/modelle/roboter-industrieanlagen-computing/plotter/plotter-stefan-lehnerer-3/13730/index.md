---
layout: "image"
title: "Neue Stifthalterung 1"
date: "2008-02-24T14:33:00"
picture: "plotter2_3.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/13730
- /details85d5.html
imported:
- "2019"
_4images_image_id: "13730"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2008-02-24T14:33:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13730 -->
Die neue Halterung ist jetzt auch ruckelfrei und noch kompakter.
