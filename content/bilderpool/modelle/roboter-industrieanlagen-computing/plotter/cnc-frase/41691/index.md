---
layout: "image"
title: "Antreib zweite Schnecke"
date: "2015-08-03T13:01:55"
picture: "cncfraese4.jpg"
weight: "4"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/41691
- /details4c28.html
imported:
- "2019"
_4images_image_id: "41691"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41691 -->
Die zweite Schnecke wird durch den Powermotor über eine Kette angetrieben, man sieht auch einen der beiden Endtaster