---
layout: "comment"
hidden: true
title: "17393"
date: "2012-10-07T13:05:06"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist ja mal raffiniert geschummelt! Zwei Taster hintereinander, und nur der hintere wird verkabelt? Einfallsreich.

Gruß,
Stefan