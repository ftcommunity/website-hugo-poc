---
layout: "image"
title: "Die vier Eingabetaster"
date: "2012-10-07T12:48:51"
picture: "schrankplotter1.jpg"
weight: "13"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35802
- /detailse57e.html
imported:
- "2019"
_4images_image_id: "35802"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-07T12:48:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35802 -->
Für Harald:
Die Verkabelung der vier Eingabetaster auf der Front.
Insgasamt sind neun Taster für diesen Zweck verbaut.