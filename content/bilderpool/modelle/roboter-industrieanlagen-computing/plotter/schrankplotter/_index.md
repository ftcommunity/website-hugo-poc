---
layout: "overview"
title: "Schrankplotter"
date: 2020-02-22T08:05:10+01:00
legacy_id:
- /php/categories/2660
- /categories20f5.html
- /categoriesed5e.html
- /categoriesf50f.html
- /categories08ea.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2660 --> 
Das ist ein Plotter, der dafür gedacht ist, etwa postkatengroße Plots zu plotten.
Wie der Name vermuten lässt, ist er für den Einsatz in meinem Schrank gedacht.

Darum waren die Konstruktionsziele die folgenden:
 - Eine möglichst hohe Genauigkeit
 - Direkte Ansteuerung über ein Programm am Computer
 - Und ein möglichst kompaktes Ausmaß

Er ist für Zeichnungen im Quervormat gedacht.
Ausserdem ist er in zwei Module unterteilt:
 - Das Plotmodul
 - Und das Technikmodul