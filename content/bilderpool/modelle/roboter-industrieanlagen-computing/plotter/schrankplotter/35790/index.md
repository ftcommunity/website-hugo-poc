---
layout: "image"
title: "Der Schrankplotter im Ganzen"
date: "2012-10-06T21:10:39"
picture: "schrankplotter01.jpg"
weight: "1"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35790
- /detailsa3e0.html
imported:
- "2019"
_4images_image_id: "35790"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35790 -->
Das ist mein Schrankplotter.
[Details in der Übersicht]
Es fehlt noch eine Stifthalterung!