---
layout: "image"
title: "Detail Y-Achsen-Antrieb"
date: "2012-10-06T21:10:39"
picture: "schrankplotter06.jpg"
weight: "6"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35795
- /detailsf001.html
imported:
- "2019"
_4images_image_id: "35795"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35795 -->
Detailansicht der Übersetzung