---
layout: "image"
title: "Detail X-Achse"
date: "2012-10-06T21:10:39"
picture: "schrankplotter09.jpg"
weight: "9"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35798
- /details60f5-2.html
imported:
- "2019"
_4images_image_id: "35798"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35798 -->
Man kann den nach hinten überstehenden Antrieb der X-Achse erkennen