---
layout: "image"
title: "Frontansicht Technikmodul"
date: "2012-10-06T21:10:39"
picture: "schrankplotter11.jpg"
weight: "11"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- /php/details/35800
- /details6a35.html
imported:
- "2019"
_4images_image_id: "35800"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35800 -->
Von links nach rechts:
Not-Aus-Schalter, vier Eingabetaster, Stromversorgung, zwei Schlüsselschalter