---
layout: "image"
title: "Scanner/Plotter 34"
date: "2007-04-23T21:15:31"
picture: "scannerplotter4_2.jpg"
weight: "34"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10151
- /details09c0.html
imported:
- "2019"
_4images_image_id: "10151"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10151 -->
Der Stift wird per Seilzug angehoben.
