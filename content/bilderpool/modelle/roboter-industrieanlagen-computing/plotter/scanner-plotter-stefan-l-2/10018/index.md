---
layout: "image"
title: "Scanner/Plotter 2"
date: "2007-04-07T11:10:28"
picture: "scannerplotter02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10018
- /details4655-2.html
imported:
- "2019"
_4images_image_id: "10018"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10018 -->
Der Antrieb. Große Übersetzung damit er möglichst genau ist.
