---
layout: "image"
title: "Scanner/Plotter 19"
date: "2007-04-07T11:10:28"
picture: "scannerplotter19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10035
- /details73e7.html
imported:
- "2019"
_4images_image_id: "10035"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10035 -->
