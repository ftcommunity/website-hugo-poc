---
layout: "image"
title: "Scanner/Plotter 37"
date: "2007-04-23T21:15:31"
picture: "scannerplotter7_2.jpg"
weight: "37"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10154
- /details9412.html
imported:
- "2019"
_4images_image_id: "10154"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10154 -->
