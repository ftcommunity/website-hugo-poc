---
layout: "comment"
hidden: true
title: "19156"
date: "2014-06-15T20:42:33"
uploadBy:
- "AKtobi47"
license: "unknown"
imported:
- "2019"
---
Es liegt nicht am Bild sondern an der Linse der Fotokamera.
Man sieht die Krümmung der Sammellinse.

==> Um solche Krümmungen zu vermeiden, kann man die Krümmung mit manchen Fotosoftweren ausgleichen.