---
layout: "image"
title: "Scanner/Plotter 9"
date: "2007-04-07T11:10:28"
picture: "scannerplotter09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10025
- /detailsd101.html
imported:
- "2019"
_4images_image_id: "10025"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10025 -->
In der Mitte ist ein Lichtsensor.
