---
layout: "image"
title: "Scanner/Plotter 15"
date: "2007-04-07T11:10:28"
picture: "scannerplotter15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10031
- /details7655.html
imported:
- "2019"
_4images_image_id: "10031"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10031 -->
