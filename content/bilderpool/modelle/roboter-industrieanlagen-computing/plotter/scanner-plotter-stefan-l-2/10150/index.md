---
layout: "image"
title: "Scanner/Plotter 33"
date: "2007-04-23T21:15:31"
picture: "scannerplotter3_2.jpg"
weight: "33"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10150
- /details9a6a.html
imported:
- "2019"
_4images_image_id: "10150"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10150 -->
Der neu Schreibkopf. Absolut spielfrei und sogar noch kompakter wie der alte.
