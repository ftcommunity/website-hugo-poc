---
layout: "image"
title: "Scanner/Plotter 8"
date: "2007-04-07T11:10:28"
picture: "scannerplotter08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10024
- /detailsbc20.html
imported:
- "2019"
_4images_image_id: "10024"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10024 -->
