---
layout: "image"
title: "Scanner/Plotter 25"
date: "2007-04-13T16:09:16"
picture: "scannerplotter4.jpg"
weight: "25"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10082
- /detailsb17e-2.html
imported:
- "2019"
_4images_image_id: "10082"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-13T16:09:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10082 -->
Rad zur Kettenspannung.
