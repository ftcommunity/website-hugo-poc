---
layout: "comment"
hidden: true
title: "12129"
date: "2010-09-09T17:57:36"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@Arsamenes
Hallo Michael,
zunächst danke für de Blömsche.
Wenn es eine reine ft-Lösung werden soll, bleibt nur der Encodermotor als Alternative mit 75 Impulsen/Zapfenumdrehung. Der ft-Schrittmotor hat 48 Schritte/Umdrehung. Zu beachten wäre, dass im ft-System als Treiber für den SM nur Softwarelösungen verfügbar sind. Einen SM erfolgreich anzutreiben, gelingt nur richtig mit einer Hardwarelösung den sogenannten Schrittmotortreibern. Der ft-Encodermotor ist allerdings nur einpolig. Da sind in Abhängigkeit seines Einsatzes "Überrraschungen" nicht ausgeschlossen. Einen echten Vorteil hätte er aber wenn zweipolig gegenüber dem SM, ein SM kann gesteuerte Schritte verlieren ... 
Gruss, Ingo