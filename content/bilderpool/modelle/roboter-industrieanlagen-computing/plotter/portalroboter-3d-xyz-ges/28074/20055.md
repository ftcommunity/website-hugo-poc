---
layout: "comment"
hidden: true
title: "20055"
date: "2015-01-16T12:03:51"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Heinz,
Danke ! Das ist aber nur eine Modellbasis. Ich hoffe noch Zeit für Ergänzungen zu finden, die dann Einsatzmöglichkeiten aufzeigen.
Eine 3D-XYZ-REK (Räderschlitten/Encodermotor/Kettengetriebe) - vorgestellt auf der ftConvention 2012 - existiert auch noch.
Gruß Udo2