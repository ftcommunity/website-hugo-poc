---
layout: "image"
title: "[11/13] XY-Testplott, Quadrat 6 von 35"
date: "2010-09-08T14:39:29"
picture: "portalroboterdxyzges11.jpg"
weight: "11"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28078
- /detailsaa18-2.html
imported:
- "2019"
_4images_image_id: "28078"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28078 -->
Eine innere Programmschleife "5 Quadrate" wird in 7 äusseren Schleifendurchläufen zu insgesamt 35 Quadrate auf A4-Format gezeichnet. Das kleine Steuerprogramm hat 3 UPs: Referenzfahrt, Quadrat und Diagonalen. Es erfolgt nur eine Referenzfahrt am Anfang des Plotts.
