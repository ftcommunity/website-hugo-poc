---
layout: "image"
title: "12 Portalroboter Laufwerk rechts"
date: "2005-05-11T16:15:30"
picture: "12-Rechtes_Laufwerk.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4136
- /detailsff97.html
imported:
- "2019"
_4images_image_id: "4136"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4136 -->
Jeder Wagen läuft auf insgesamt 8 Rädern. Vier kleine nehmen die senkrechte Last auf und vier große machen die seitliche Führung. Herzstück der Führung ist die geschliffene 12 mm Stahlschiene, die die Leichtläufigkeit eines Kugellagers vermittelt. Die Stabilität dieser Schiene ist fantastisch. Man könnte sie nur durch eine 15 mm Schiene ersetzen, dann paßte sie besser in das Fischertechnik-Raster.

Über der Schiene ist die Zugschnur mit der Federvorspannung zu sehen.