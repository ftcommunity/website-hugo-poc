---
layout: "image"
title: "05 Portalroboter Kabelbaum"
date: "2005-05-11T16:15:30"
picture: "05-Portal_vorgefahren.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4129
- /details7fe8-4.html
imported:
- "2019"
_4images_image_id: "4129"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4129 -->
Der Anblick von rechts zeigt den Kabelbaum genauer. Es ist die Verbindung für 4 Endschalter und 2 Schrittmotore.