---
layout: "image"
title: "06 Portalroboter Z-Achse Überblick"
date: "2005-05-11T16:15:30"
picture: "06-Z-Achse_Gesamtansicht.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4130
- /detailsec5d-2.html
imported:
- "2019"
_4images_image_id: "4130"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4130 -->
Der Schlitten der Z-Achse (senkrecht) ist auf vier Metallachsen gelagert. Das ergibt ein minimiertes Spiel und eine gute Steifigkeit ohne Schwergängigkeit. Quer dazu ist die Z-Achse an die X-Achse gekoppelt, von der die Schnecke zu sehen ist und die beiden 8 mm Stahlschienen. Diese geben ebenfalls eine perfekte Steifigkeit und Leichtläufigkeit.