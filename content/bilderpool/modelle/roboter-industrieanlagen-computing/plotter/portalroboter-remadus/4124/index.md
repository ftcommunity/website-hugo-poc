---
layout: "image"
title: "00Portalroboter im Überblick"
date: "2005-05-10T23:27:34"
picture: "00-berblick.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4124
- /details64e3.html
imported:
- "2019"
_4images_image_id: "4124"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4124 -->
60 cm lang, 52 cm breit und 42 cm hoch bietet der Portalroboter einen Arbeitsraum von 35 x 22 x 6 cm. Die ganze Konstruktion bringt ca. 6 kg auf die Waage, davon entfallen fast 1,5 kg auf das Querhaupt, welches die X-Achse und die Z-Achse trägt. Im Bild ist ein DIN-A4-Blatt aufgelegt, das knapp den zur Verfügung stehenden Fahrweg darstellt.