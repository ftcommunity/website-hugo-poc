---
layout: "image"
title: "02 Portalroboter rechts"
date: "2005-05-10T23:27:35"
picture: "02-Gesamtmodell-rechts.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4126
- /detailsedba.html
imported:
- "2019"
_4images_image_id: "4126"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4126 -->
Im Größenvergleich ein kleiner 9"-Kassenmonitor mit Notebook-Tastatur. Die Rechenanlage macht einen recht schlanken Fuß. Der Rechner selbst ist nicht zu sehen, es ist aber die kleine Box, wie sie auch das Hexapod mal gesteuert hat.