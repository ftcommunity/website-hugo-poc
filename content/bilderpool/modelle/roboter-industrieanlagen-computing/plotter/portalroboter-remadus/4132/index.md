---
layout: "image"
title: "08 Portalroboter Z-Achse Antrieb"
date: "2005-05-11T16:15:30"
picture: "08-Z-Achse_Seiltrieb.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4132
- /detailsdab9.html
imported:
- "2019"
_4images_image_id: "4132"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4132 -->
Hier ist der Seiltrieb der Z-Achse zu sehen. Das Seil kommt von rechts und wird im Gegenuhrzeigersinn über die obere Umlenkrolle nach unten geführt. Dann um 180 Grad in gleichem Drehsinn um die untere Rolle wieder nach oben umgelenkt und um weitere 90 Grad weiter nach links. Dort ist die Schnur angebunden. Es ergibt sich also eine Schlaufe von genau 360 Grad. Wird das Seil eingezogen, dann hebt sich die Z-Achse mit halber Seilgeschwindigkeit (Flaschenzug). Fährt der Schlitten jedoch in X-Richtung, dann bleibt er stets auf gleicher Höhe, lediglich die Seilschlaufe beginnt um die Rollen zu laufen. Da der Drehsinn gleichläufig ist, macht es nichts, daß die Zugschnur zweimal auf der oberen Rolle aufliegt.

Auch zu beachten, daß die Zugschnur in gleicher Höhe auf die Rolle kommt, wie der Führungsbolzen im Antrieb der X-Achse. Dadurch wird die Kraft der Zugschnur, die zunächst am Schlitten der Z-Achse angreift, drehmomentenfrei aufgefangen und abgeleitet.