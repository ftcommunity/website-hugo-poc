---
layout: "image"
title: "07 Portalroboter Auflagerung Z-Schlitten"
date: "2005-05-11T16:15:30"
picture: "07-Z-Achse_Fhrung.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4131
- /details889d.html
imported:
- "2019"
_4images_image_id: "4131"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4131 -->
Der Z-Achsenschlitten ist statisch exakt bestimmt mit drei Rädern oben und zwei Gleitkufen unten geführt. Damit ist die Führung der X-Achse spielfrei. Leider hat die Schnecke noch Spiel, weil der Bauraum nicht reicht, durch eine vorgespannte Schneckenmutter das Spiel herauszudrücken. Hier wird ein Gummispannelement hinzugefügt, dann ist auch diese Achse spielfrei. Leider nicht zu sehen ist der Antriebsmotor der X-Achsenspindel. Der ist nämlich als Verlängerung der Schnecke unter dem schwarz-roten Würfel auf der (von hier aus gesehen) linken Seite untergebaut.