---
layout: "image"
title: "Die Extension ( sauber bestückt?!)"
date: "2010-12-23T15:37:40"
picture: "plotter3_2.jpg"
weight: "9"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29528
- /details5995-2.html
imported:
- "2019"
_4images_image_id: "29528"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29528 -->
