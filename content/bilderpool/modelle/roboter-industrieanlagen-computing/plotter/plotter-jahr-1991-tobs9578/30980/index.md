---
layout: "image"
title: "Der Plotter"
date: "2011-06-29T07:16:20"
picture: "plotter3_3.jpg"
weight: "14"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/30980
- /details9926.html
imported:
- "2019"
_4images_image_id: "30980"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2011-06-29T07:16:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30980 -->
Etwas unaufgeräumt.
Aber es sind ja Ft-Teile.
