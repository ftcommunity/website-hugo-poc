---
layout: "image"
title: "Schrift"
date: "2010-12-19T10:14:51"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29489
- /details0d7d.html
imported:
- "2019"
_4images_image_id: "29489"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2010-12-19T10:14:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29489 -->
