---
layout: "image"
title: "Die neue Verkabelung"
date: "2010-12-23T15:37:40"
picture: "plotter2_2.jpg"
weight: "8"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/29527
- /detailsb1ea-2.html
imported:
- "2019"
_4images_image_id: "29527"
_4images_cat_id: "2147"
_4images_user_id: "1007"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29527 -->
