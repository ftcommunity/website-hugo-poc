---
layout: "image"
title: "Zum Vergleich: Das 'Große Landeswappen Baden-Württemberg' als Bildschirm-Plot"
date: "2012-03-14T23:40:08"
picture: "Bildschirm-Plot_Landeswappen_BaW.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34649
- /detailsd99f.html
imported:
- "2019"
_4images_image_id: "34649"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2012-03-14T23:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34649 -->
Hier zum Vergleich der originale "Bildschirm-Plot" des Großen Baden-Württembergischen Landeswappens .