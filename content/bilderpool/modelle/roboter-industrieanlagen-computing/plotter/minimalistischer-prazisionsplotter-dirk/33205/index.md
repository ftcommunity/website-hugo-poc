---
layout: "image"
title: "Antrieb und Endlagentaster I"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33205
- /details0840.html
imported:
- "2019"
_4images_image_id: "33205"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33205 -->
Die Schneckengetriebe werden von zwei senkrecht montierten Encoder-Motoren mit Rast-Ritzel Z10 angetrieben, die jeweils ein Z40 als Kronrad nutzen (10:32). Daraus errechnet sich die Auflösung wie folgt: 75 Impulse des Encoder-Motors (je Umdrehung)/10*32 = 240 Impulse je Umdrehung des Schneckengewindes, das entspricht 0,5 cm Translation der Schneckenmutter, also liegt die Auflösung bei 0,0208 mm je Impuls. Das ist sogar etwas besser als die Default-Auflösung von HP-Plottern (1/1016'' = 0,025 mm).
Die Endlagenschalter werden nur bei der Initialisierung des Plotters abgefragt (die "Home Position" des Plotters nach HP-GL Rev. C ist die linke obere Ecke der Zeichenfläche).