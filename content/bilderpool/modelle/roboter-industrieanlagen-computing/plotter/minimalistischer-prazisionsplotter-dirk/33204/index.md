---
layout: "image"
title: "Schlitten und Schneckenantrieb (rechts)"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33204
- /details9428.html
imported:
- "2019"
_4images_image_id: "33204"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33204 -->
Für die seitlichen Schneckenantriebe reichen die (längsten) Metallachsen 200, als Führungsstangen genügen zwei Metallachsen 170. Die seitlichen Schneckenantriebe sind mit einer Kette über zwei Z40 verbunden, um die Auswirkungen der Elastizität der Kette bei einem Richtungswechsel möglichst gering zu halten.
Führung und Schneckenantrieb des Schlittens müssen mit Klemmkupplungen (31024) verlängert werden, um die gesamte Breite der Experimentierplatte nutzen zu können.