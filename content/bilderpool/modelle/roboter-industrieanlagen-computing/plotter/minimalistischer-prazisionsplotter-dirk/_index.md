---
layout: "overview"
title: "Minimalistischer Präzisionsplotter (Dirk Fox)"
date: 2020-02-22T08:05:05+01:00
legacy_id:
- /php/categories/2456
- /categoriesadaf.html
- /categories6108.html
- /categoriese3a6.html
- /categoriesfe9e.html
- /categories7d9c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2456 --> 
Plotter mit Standard-ft-Teilen (Ansteuerung über TX mit Encoder-Motoren).
Ansteuerung mit einer Genauigkeit von 0,02 mm