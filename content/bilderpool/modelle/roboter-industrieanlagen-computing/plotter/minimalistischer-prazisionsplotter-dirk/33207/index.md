---
layout: "image"
title: "Schreibkopf (linke Seite)"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33207
- /details739c.html
imported:
- "2019"
_4images_image_id: "33207"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33207 -->
Das "große" Seilwindengestell (31997) dient als Stifthalter (sehr hohe Steifigkeit), es kann über zwei Rollenlager an zwei Metallachsen 50 auf- und abgleiten.