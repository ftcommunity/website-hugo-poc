---
layout: "image"
title: "Schreibkopf (rechte Seite)"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33208
- /details1e0c.html
imported:
- "2019"
_4images_image_id: "33208"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33208 -->
Das Spiel der Schneckenmutter auf der Schnecke wird durch eine zweite Schneckenmutter aufgehoben.
Gehoben und gesenkt wird der Stift durch einen Getriebehalter mit Schnecke - die Idee geht auf einen Lenkhebel-Mechanismus von thomas004 zurück (siehe http://www.ftcommunity.de/details.php?image_id=10722).