---
layout: "comment"
hidden: true
title: "15798"
date: "2011-11-30T17:58:05"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
danke dir im voraus. Wenn die Anpassungen erledigt sind, werden Plots übereinander sicher zur Modellfunktion und zu den einpoligen Encodern aufschlußreich sein. Vor wieder eigenen Programmierarbeiten habe ich bei meinen Modellbauten noch fleißig statisch und mechanisch meine Vorstellungen und Wünsche zu realisieren.
Gruß, Ingo