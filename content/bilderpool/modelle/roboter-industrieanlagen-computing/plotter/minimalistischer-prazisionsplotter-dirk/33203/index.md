---
layout: "image"
title: "Gesamtansicht des Plotters"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33203
- /details4679.html
imported:
- "2019"
_4images_image_id: "33203"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33203 -->
Schreibkopf und Schlitten des Plotters werden über insgesamt drei Schneckengetriebe positioniert. Dank jeweils zwei miteinander verbundenen benachbarten Schneckenmuttern (um 90° verdreht) ist die Konstruktion vollständig spielfrei. Die Metallstangen sorgen für eine leichte und gerade Führung von Schlitten und Schreibkopf.