---
layout: "image"
title: "Stifthalterung"
date: "2010-03-31T20:00:31"
picture: "plotterseb6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26857
- /detailsfc6c.html
imported:
- "2019"
_4images_image_id: "26857"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26857 -->
