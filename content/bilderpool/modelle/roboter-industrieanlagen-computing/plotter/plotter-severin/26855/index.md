---
layout: "image"
title: "Funktion der Y-Achse"
date: "2010-03-31T20:00:30"
picture: "plotterseb4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26855
- /details6dbd.html
imported:
- "2019"
_4images_image_id: "26855"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26855 -->
Ich wollte unbedingt den Schrittmotor der Y-Achse auf die Platte bekommen um die Trägheit zu verringern. Wenn man bedenkt das der komplette Schlitten nur wenig mehr wiegt als ein Schrittmotor... Der Effekt ist der Wahnsinn.