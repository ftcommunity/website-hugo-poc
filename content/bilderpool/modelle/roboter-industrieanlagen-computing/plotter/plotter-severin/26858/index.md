---
layout: "image"
title: "Die Mechanik"
date: "2010-03-31T20:00:31"
picture: "plotterseb7.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/26858
- /details74b3.html
imported:
- "2019"
_4images_image_id: "26858"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26858 -->
Einzigstes Manko ist das Getriebe am X-Achsen Motor, das ein Spiel von ca 0,25 mm zu verantworten hat. Die Nutzbare Geschwindigkeit ist einorm gestiegen.