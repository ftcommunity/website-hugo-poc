---
layout: "image"
title: "Plotter 5"
date: "2006-11-20T19:01:26"
picture: "plotterneueversion1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7512
- /details3a0e.html
imported:
- "2019"
_4images_image_id: "7512"
_4images_cat_id: "708"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7512 -->
Antrieb der y-Achse.
