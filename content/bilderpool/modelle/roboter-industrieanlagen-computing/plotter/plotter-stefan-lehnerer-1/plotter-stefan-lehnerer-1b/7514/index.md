---
layout: "image"
title: "Plotter 7"
date: "2006-11-20T19:01:26"
picture: "plotterneueversion3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7514
- /detailsfd06-3.html
imported:
- "2019"
_4images_image_id: "7514"
_4images_cat_id: "708"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7514 -->
Antrieb der x-Achse
