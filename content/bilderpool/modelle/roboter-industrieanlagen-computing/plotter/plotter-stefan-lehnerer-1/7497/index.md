---
layout: "image"
title: "Plotter3"
date: "2006-11-19T16:27:23"
picture: "plotter3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/7497
- /details1d5b-3.html
imported:
- "2019"
_4images_image_id: "7497"
_4images_cat_id: "706"
_4images_user_id: "502"
_4images_image_date: "2006-11-19T16:27:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7497 -->
