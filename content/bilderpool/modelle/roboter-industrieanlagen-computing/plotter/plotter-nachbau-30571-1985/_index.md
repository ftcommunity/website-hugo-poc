---
layout: "overview"
title: "Plotter Nachbau (30571 aus 1985)"
date: 2020-02-22T08:05:14+01:00
legacy_id:
- /php/categories/3202
- /categoriesf4c2.html
- /categoriesf161.html
- /categories561c.html
- /categories4b2e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3202 --> 
Ich habe mit vorhanden Teilen und etwas Zukauf bei ffm (Danke Stefan) den Plotter aus 1985 nachgebaut. die Schrittmotoren sind kein Original von ft, sondern bei ebay für kleines Geld erstanden. Der Schrittwinkel beträgt nur 15 Grad (statt 7,5) somit ergibt sich für  die X-Achse etwa eine Auflösung von 320 und für Y etwa 250 Schritten auf einem Din A 4 Blatt.

Die Software: ist zunächst original ohne Anpassungen lauffähig . man muss natürlich etwas aufpassen, dass man den maximalen x / y- Ausschlag nicht überfährt. 
Qualitativ ist das Ganze für den Aufwand in Ordnung. siehe Fotos.

Alles in Allem macht es richtig Spass, mal auf dem C64 "back2 the roots" rumzudaddeln .. :-)
analog zu meinem Versuch müsste das Ganze natürlich immer noch genauso mit alten Amigas, Apples, Sinclairs, PCs (XT/AT) usw. spielen - Leute (ab 40.. ;-)   , es macht RICHTIG Spass..
holt die alten Kisten vom Dachboden, jagt in der Bucht .. .... 
