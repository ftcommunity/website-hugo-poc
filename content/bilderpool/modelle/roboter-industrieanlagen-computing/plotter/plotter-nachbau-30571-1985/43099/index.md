---
layout: "image"
title: "PLOT..."
date: "2016-03-13T12:19:14"
picture: "plotternachbauaus09.jpg"
weight: "9"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43099
- /details0081.html
imported:
- "2019"
_4images_image_id: "43099"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2016-03-13T12:19:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43099 -->
