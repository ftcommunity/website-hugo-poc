---
layout: "image"
title: "Übersetzung der Minimotoren"
date: "2011-07-28T19:40:55"
picture: "DSCF7561.jpg"
weight: "2"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/31399
- /details522e.html
imported:
- "2019"
_4images_image_id: "31399"
_4images_cat_id: "2336"
_4images_user_id: "1239"
_4images_image_date: "2011-07-28T19:40:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31399 -->
Hier kann man die Übersetzung der Mini Motoren sehen. Diese ist notwendig, da die Motoren (rechts befindet er sich unter dem Bedienfeld) sonst nicht stark genug waren und zu schnell gelaufen sind.