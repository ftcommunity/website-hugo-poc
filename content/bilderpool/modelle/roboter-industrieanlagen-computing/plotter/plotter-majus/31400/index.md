---
layout: "image"
title: "Stift anheben"
date: "2011-07-28T19:40:55"
picture: "DSCF7568.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/31400
- /details7cee.html
imported:
- "2019"
_4images_image_id: "31400"
_4images_cat_id: "2336"
_4images_user_id: "1239"
_4images_image_date: "2011-07-28T19:40:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31400 -->
Die Zylinder links und rechts (unter der Feder) ziehen sich zusammen, wodurch der Stift nur leicht angehoben wird. Ich habe dies so gelöst, da mir der Weg des Zylinders direkt zu lange war. Außerdem darf der Stift nicht wackeln, da sonst die Schrift unsauber wird.