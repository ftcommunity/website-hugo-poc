---
layout: "image"
title: "Präzisionsplotter 2"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34347
- /detailse95e.html
imported:
- "2019"
_4images_image_id: "34347"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34347 -->
Führung der Y-Achse
