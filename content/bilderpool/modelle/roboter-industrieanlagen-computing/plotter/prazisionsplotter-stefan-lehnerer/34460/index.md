---
layout: "image"
title: "Plot 8"
date: "2012-02-27T13:01:10"
picture: "plot1_3.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34460
- /details8418.html
imported:
- "2019"
_4images_image_id: "34460"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-27T13:01:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34460 -->
Mal was Aufwändigeres. Breite ca. 15cm.
