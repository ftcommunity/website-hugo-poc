---
layout: "image"
title: "Präzisionsplotter 4"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34349
- /details61f0.html
imported:
- "2019"
_4images_image_id: "34349"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34349 -->
Führung der X-Achse
