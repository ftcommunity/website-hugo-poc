---
layout: "image"
title: "Plot 5"
date: "2012-02-26T14:42:24"
picture: "plot1_2.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34442
- /detailsc505.html
imported:
- "2019"
_4images_image_id: "34442"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-26T14:42:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34442 -->
