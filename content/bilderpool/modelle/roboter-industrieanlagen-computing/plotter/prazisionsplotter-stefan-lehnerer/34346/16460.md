---
layout: "comment"
hidden: true
title: "16460"
date: "2012-02-24T19:38:04"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
wußte natürlich nicht, daß du im Halbschrittmodus arbeitest. Die Algorithmen von Holger und Alfred sind mir ja nicht unbekannt. Die Schneckensteigung berechnet sich mit Modul x Pi = 4,712 mm. Das Messen ist hier bei der ft-Schnecke so eine Sache, die ich lieber umgehe. Du wirst aber hier sicher im Download-Modus arbeiten. Obwohl es von mir mal mit Programmen von dir vor längerer Zeit getestet auch ab und an mal stolperte. Das könnte aber dann unter Last evtl. nicht der Fall sein.
Gruß Udo2