---
layout: "image"
title: "Präzisionsplotter 1"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/34346
- /details14c7-2.html
imported:
- "2019"
_4images_image_id: "34346"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34346 -->
Die Genauigkeit beträgt dank Schrittmotoren, Schneckenantrieb und 1:4 Übersetzung 0,0123 mm (12,3 µm).
Dadurch ist er leider sehr langsam. Das Spiel konnte ich auf ein Minimum reduzieren, zusätzlich habe ich noch einen Spielausgleich programmiert.
Erste Plots sehen auf jeden Fall schon mal sehr vielversprechend aus.
