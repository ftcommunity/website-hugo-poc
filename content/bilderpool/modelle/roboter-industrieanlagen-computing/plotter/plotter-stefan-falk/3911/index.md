---
layout: "image"
title: "Gesamtansicht 1"
date: "2005-03-29T00:25:16"
picture: "Plotter_001.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3911
- /details7ae2.html
imported:
- "2019"
_4images_image_id: "3911"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3911 -->
Ein einigermaßen minimalistischer Plotter, aufgebaut ausschließlich aus Original-ft-Teilen. Da ich keine Schrittmotoren habe, wird er von zwei Minimot angesteuert.
