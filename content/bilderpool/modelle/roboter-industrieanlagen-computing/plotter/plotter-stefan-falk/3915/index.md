---
layout: "image"
title: "Rückansicht"
date: "2005-03-29T00:25:17"
picture: "Plotter_005.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3915
- /detailsb2c4.html
imported:
- "2019"
_4images_image_id: "3915"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3915 -->
Hier sieht man beide Antriebe von hinten.