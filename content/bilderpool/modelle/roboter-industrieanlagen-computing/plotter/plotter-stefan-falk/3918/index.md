---
layout: "image"
title: "Stifthalterung von vorne"
date: "2005-03-29T09:54:46"
picture: "Plotter_008.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3918
- /details186e.html
imported:
- "2019"
_4images_image_id: "3918"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3918 -->
Der Stift wird von einem Gummi gehalten, der für einen der alten Reifen 45 gedacht war. Die Klemmen zur Stromzufuhr stammen aus dem alten Elektromechanik-Programm von ft (von dem ich einige Teile im heutigen vermisse).
