---
layout: "image"
title: "Stromversorgung, Nullabschaltung"
date: "2005-03-29T09:54:46"
picture: "Plotter_007.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3917
- /details7c19.html
imported:
- "2019"
_4images_image_id: "3917"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3917 -->
Hier sieht man die Stromzufuhr für den Elektromagneten und den Null-Positions-Taster für die lange Achse.