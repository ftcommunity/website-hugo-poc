---
layout: "image"
title: "Antrieb mit einem Schrittmotor"
date: "2007-04-30T13:23:03"
picture: "plotter3.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10235
- /details5b9b.html
imported:
- "2019"
_4images_image_id: "10235"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-04-30T13:23:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10235 -->
