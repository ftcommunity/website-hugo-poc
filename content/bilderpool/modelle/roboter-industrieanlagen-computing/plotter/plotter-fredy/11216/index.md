---
layout: "image"
title: "Plotter"
date: "2007-07-23T12:07:00"
picture: "plotter13.jpg"
weight: "18"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11216
- /detailsa19c-2.html
imported:
- "2019"
_4images_image_id: "11216"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:07:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11216 -->
