---
layout: "image"
title: "Führung durch eine Metallstange"
date: "2007-04-30T13:23:03"
picture: "plotter5.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10237
- /detailsf993.html
imported:
- "2019"
_4images_image_id: "10237"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-04-30T13:23:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10237 -->
Durch diese art der führung habe ich in dem Schlitten kein Spiel. Jetzt muss ich nur mal schauen wie ich die Stifthalterung fertig mache.
