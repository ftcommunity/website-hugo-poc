---
layout: "image"
title: "Antrieb"
date: "2007-07-23T12:03:13"
picture: "plotter04.jpg"
weight: "9"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11207
- /details9d00.html
imported:
- "2019"
_4images_image_id: "11207"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11207 -->
Hier kann man die Kette für den Stiftantrieb erkenne, er bewegt den Stift und seinen Halter(schlitten) in beide richtungen, nach Links un drechts.
