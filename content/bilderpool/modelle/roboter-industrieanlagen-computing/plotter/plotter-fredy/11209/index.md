---
layout: "image"
title: "Stifthalter"
date: "2007-07-23T12:03:13"
picture: "plotter06.jpg"
weight: "11"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11209
- /details5431.html
imported:
- "2019"
_4images_image_id: "11209"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11209 -->
Der Elektromagnet zieht den Dauerrmagneten am Stift an, wenn der Strom wieder abgeschaltet wird, zieht das Gummi den stift wieder nach oben.
