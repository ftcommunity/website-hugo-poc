---
layout: "image"
title: "Führung durch eine Zahnstange"
date: "2007-04-30T13:23:03"
picture: "plotter4.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10236
- /details1d79-3.html
imported:
- "2019"
_4images_image_id: "10236"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-04-30T13:23:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10236 -->
