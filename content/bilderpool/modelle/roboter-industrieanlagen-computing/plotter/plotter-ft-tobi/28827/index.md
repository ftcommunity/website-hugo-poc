---
layout: "image"
title: "Zeichnungen"
date: "2010-10-02T23:55:12"
picture: "plotter8.jpg"
weight: "8"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- /php/details/28827
- /details36ab.html
imported:
- "2019"
_4images_image_id: "28827"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28827 -->
Die Kreise, die hier zu sehen sind, wurden ohne "Softwarekorrektur" gezeichnet. Der Unterschied ist klar erkennbar. Diese Ungenauigkeiten kommen dadurch zustande, dass die Achsen einen kleinen Bewegungsfreiraum haben. Wenn dann eine Achse die Richtung wechselt, bleibt sie erst einige Schritte auf der Stelle stehen, wärend die andere Achse aber normal weiterzeichnet.