---
layout: "image"
title: "Von Hinten"
date: "2010-04-18T19:35:08"
picture: "plotter07.jpg"
weight: "7"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26966
- /details23ff.html
imported:
- "2019"
_4images_image_id: "26966"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26966 -->
man kann die Kette und den Endpunkt der Längsachse sehen.