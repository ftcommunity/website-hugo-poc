---
layout: "image"
title: "Von rechts"
date: "2010-04-18T19:35:08"
picture: "plotter05.jpg"
weight: "5"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26964
- /detailsfe79.html
imported:
- "2019"
_4images_image_id: "26964"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26964 -->
Der P-motor (50:1) ist noch einmal 3:1 untersetzt