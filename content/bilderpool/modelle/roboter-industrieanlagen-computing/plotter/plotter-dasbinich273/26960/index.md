---
layout: "image"
title: "Detail des Längsachsensntriebs"
date: "2010-04-18T19:35:07"
picture: "plotter01.jpg"
weight: "1"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26960
- /details6d61.html
imported:
- "2019"
_4images_image_id: "26960"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26960 -->
Mein erster Plotter.
Bei dem Plotter wird das Papier vor- und rückwärts bewegt und der Stift nur nach links und rechts