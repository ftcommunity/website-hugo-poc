---
layout: "image"
title: "Plotter Schiene"
date: "2010-04-23T19:51:01"
picture: "plotter4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26980
- /details11c8-2.html
imported:
- "2019"
_4images_image_id: "26980"
_4images_cat_id: "1939"
_4images_user_id: "1112"
_4images_image_date: "2010-04-23T19:51:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26980 -->
Die Zahnstangen rutschen hervorragend auf den Aluprofilen hin und her.