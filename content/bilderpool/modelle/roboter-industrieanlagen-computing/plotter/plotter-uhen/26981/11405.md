---
layout: "comment"
hidden: true
title: "11405"
date: "2010-04-24T15:00:19"
uploadBy:
- "uhen"
license: "unknown"
imported:
- "2019"
---
Ja, richtig. Ich zeichne aber mit der Maus. Wenn man die linke Taste drückt, wird eine senkrechte oder wagerechte LInie angedeutet, je nachdem was näher am Ausgangspunt ist. Klickt man nochmal mit der linken Taste, wird sie wirklich gezeichnet. Mit der rechten Maustaste verläßt man den Modus und kann mit der linken Maustaste an einer anderen Stelle neu ansetzten. Also ganz wie in einem Grafikzeichenprogramm. Ein Minikästchen entspricht in etwa einem halben Millimeter auf dem Plotter. Eine nochmalige Glättung findet nicht statt, weil der Plotter dies eh nicht umsetzen könnte.

Gruß

Udo