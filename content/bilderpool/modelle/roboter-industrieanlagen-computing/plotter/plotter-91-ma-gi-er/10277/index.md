---
layout: "image"
title: "Gesamtansicht"
date: "2007-05-02T17:54:58"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10277
- /details6b1d-2.html
imported:
- "2019"
_4images_image_id: "10277"
_4images_cat_id: "933"
_4images_user_id: "445"
_4images_image_date: "2007-05-02T17:54:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10277 -->
Mit Ketten betrieben anstelle Schneken, und auch sonst nur fähig ein Rechteck zu zeichnen.