---
layout: "image"
title: "Minimalistischer Präzisionsplotter von Dirk Fox"
date: "2014-01-20T20:51:52"
picture: "plotter01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "supermaxi19840"
license: "unknown"
legacy_id:
- /php/details/38102
- /details17d2.html
imported:
- "2019"
_4images_image_id: "38102"
_4images_cat_id: "2835"
_4images_user_id: "2109"
_4images_image_date: "2014-01-20T20:51:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38102 -->
Gesamtansicht von vorne