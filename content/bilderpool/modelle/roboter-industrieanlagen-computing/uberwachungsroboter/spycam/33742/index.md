---
layout: "image"
title: "Ansicht mit Kamera"
date: "2011-12-23T19:30:21"
picture: "spycam02.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33742
- /details1310.html
imported:
- "2019"
_4images_image_id: "33742"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33742 -->
Das ganze noch mit Kamera.
