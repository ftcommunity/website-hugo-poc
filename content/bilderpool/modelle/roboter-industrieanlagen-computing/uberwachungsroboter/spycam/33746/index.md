---
layout: "image"
title: "Drehmotor"
date: "2011-12-23T19:30:21"
picture: "spycam06.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33746
- /detailsf6cf.html
imported:
- "2019"
_4images_image_id: "33746"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33746 -->
Der PM für die Drehung