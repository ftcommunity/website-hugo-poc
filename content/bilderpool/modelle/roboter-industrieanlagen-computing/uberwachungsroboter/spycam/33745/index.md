---
layout: "image"
title: "Die Steuerung"
date: "2011-12-23T19:30:21"
picture: "spycam05.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33745
- /details570a.html
imported:
- "2019"
_4images_image_id: "33745"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33745 -->
Das ist die in der ft:pedia 2/2011 beschriebene Schaltung (für die Neigung) plus einen Polwendeschalter für die Drehung.