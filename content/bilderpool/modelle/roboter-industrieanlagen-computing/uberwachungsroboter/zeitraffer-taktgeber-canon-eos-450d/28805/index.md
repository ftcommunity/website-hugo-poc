---
layout: "image"
title: "Das Relais"
date: "2010-10-02T16:10:46"
picture: "video_004.jpg"
weight: "3"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/28805
- /details46a0.html
imported:
- "2019"
_4images_image_id: "28805"
_4images_cat_id: "2099"
_4images_user_id: "986"
_4images_image_date: "2010-10-02T16:10:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28805 -->
Und zu guter letzt noch das Relais als Nahaufnahme. Die Aufgabe davon ist, das Signal von der Kamera "kurzuschliessen". Ich weiß, die Lötstellen sind nicht gerade schön, obwohl ich Sohn eines Elektronikers bin...Asche auf mein Haupt :P