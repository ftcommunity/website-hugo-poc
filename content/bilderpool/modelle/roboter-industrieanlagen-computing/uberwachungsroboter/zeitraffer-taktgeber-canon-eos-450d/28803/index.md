---
layout: "image"
title: "Gesamtansicht"
date: "2010-10-02T16:10:45"
picture: "video_006.jpg"
weight: "1"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/28803
- /detailse363.html
imported:
- "2019"
_4images_image_id: "28803"
_4images_cat_id: "2099"
_4images_user_id: "986"
_4images_image_date: "2010-10-02T16:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28803 -->
Hier sieht man den ganzen aufbau; Das Interface schaltet das relais, das die kamera - die per 2,5mm Klinke angeschlossen ist - auslöst.