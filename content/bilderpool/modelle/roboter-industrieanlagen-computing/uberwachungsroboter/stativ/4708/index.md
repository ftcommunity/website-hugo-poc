---
layout: "image"
title: "Stativ_9"
date: "2005-08-30T21:06:48"
picture: "Stativ_009.jpg"
weight: "7"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4708
- /detailsf7c7.html
imported:
- "2019"
_4images_image_id: "4708"
_4images_cat_id: "380"
_4images_user_id: "332"
_4images_image_date: "2005-08-30T21:06:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4708 -->
Der Auslöser ist klappbar konstruiert.
