---
layout: "image"
title: "Stativ_4"
date: "2005-08-30T20:32:24"
picture: "Stativ_005.jpg"
weight: "3"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4702
- /details4285.html
imported:
- "2019"
_4images_image_id: "4702"
_4images_cat_id: "380"
_4images_user_id: "332"
_4images_image_date: "2005-08-30T20:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4702 -->
Hier die zwei Powermmmotoren. Ich musste sie beide aus Gleichgewichtsgründen auf der selben Seite montieren.
