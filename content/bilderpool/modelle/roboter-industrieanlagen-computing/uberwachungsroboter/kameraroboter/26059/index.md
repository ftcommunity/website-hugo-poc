---
layout: "image"
title: "Gesamt"
date: "2010-01-11T18:19:57"
picture: "kameraroboter05.jpg"
weight: "5"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26059
- /detailsec53.html
imported:
- "2019"
_4images_image_id: "26059"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26059 -->
Mit Licht und Kamera bewegt