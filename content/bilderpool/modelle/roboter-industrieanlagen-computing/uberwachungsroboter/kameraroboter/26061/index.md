---
layout: "image"
title: "Detail3"
date: "2010-01-11T18:19:57"
picture: "kameraroboter07.jpg"
weight: "7"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26061
- /detailsd7b5.html
imported:
- "2019"
_4images_image_id: "26061"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26061 -->
