---
layout: "image"
title: "gesamt mit Kamera"
date: "2010-01-11T18:19:56"
picture: "kameraroboter02.jpg"
weight: "2"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26056
- /detailscfa2.html
imported:
- "2019"
_4images_image_id: "26056"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26056 -->
