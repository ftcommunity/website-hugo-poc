---
layout: "image"
title: "Gesamt"
date: "2010-01-11T18:19:57"
picture: "kameraroboter06.jpg"
weight: "6"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/26060
- /details16df.html
imported:
- "2019"
_4images_image_id: "26060"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26060 -->
Ohne Licht