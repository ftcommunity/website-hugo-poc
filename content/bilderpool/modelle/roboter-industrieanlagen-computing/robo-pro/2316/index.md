---
layout: "image"
title: "LLWin-Testboard"
date: "2004-03-13T21:06:34"
picture: "LLWin_Testboard.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["fischertechnik", "Interface", "Testboard"]
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2316
- /detailsfd5f-2.html
imported:
- "2019"
_4images_image_id: "2316"
_4images_cat_id: "1553"
_4images_user_id: "54"
_4images_image_date: "2004-03-13T21:06:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2316 -->
Programm-Testboard für FT-Interface. Macht man bei Testläufen einen Programmfehler läuft plötzlich ein Antrieb ungewollt weiter, kann das auch schon mal mechanische Folgen haben. So kam die Idee für dieses Testboard.