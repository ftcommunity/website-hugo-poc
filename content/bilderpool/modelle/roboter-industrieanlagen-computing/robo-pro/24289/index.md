---
layout: "image"
title: "Temperaturzuordnung"
date: "2009-06-08T23:39:42"
picture: "Temperaturzuordnung.jpg"
weight: "7"
konstrukteure: 
- "Patrick P."
fotografen:
- "Patrick P."
schlagworte: ["Temperatur", "Temperaturzuordnung", "Hilfe", "Programm", "programmieren"]
uploadBy: "Patrick P."
license: "unknown"
legacy_id:
- /php/details/24289
- /details0e7a.html
imported:
- "2019"
_4images_image_id: "24289"
_4images_cat_id: "1553"
_4images_user_id: "966"
_4images_image_date: "2009-06-08T23:39:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24289 -->
Hier ist eine meiner neuesten Programme:

Damit könnte ich dem Analog in der einen Liste eine Tmperatur zuweisen, aber das klappt leider nicht.

Kann mir da vielleicht jemand helfen? Weil ich kann das Foto ja nicht im Forum ausstellen, wenn es nicht schon ins Internet hochgeladen ist.

Wenn ich euch das Programm schicken soll, damit ihr mir vielleicht helfen könnt werde ich das natürlich auch tun.

Euer Patrick P.