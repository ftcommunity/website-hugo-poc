---
layout: "comment"
hidden: true
title: "9384"
date: "2009-06-09T13:36:53"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
I understand that you want to do a search in the Analogwert list (the number of list items is less than the number of analog values). So the value at the output of the list has to track the analog value. When the value is too low you increase the index (that is correct for an ascending list). But when it is êqual or too high you also increase the index (that is wrong), you should decrease it (too high) or maintain it (equal). When you change the +0 command into -2 it will already work better. The Anzeige however will keep toggling between the two nearest values.