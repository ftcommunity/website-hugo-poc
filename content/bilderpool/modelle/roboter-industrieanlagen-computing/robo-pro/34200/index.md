---
layout: "image"
title: "RoboPro-PROBLEM"
date: "2012-02-17T19:22:58"
picture: "roboproproblem1.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Hallo111"
license: "unknown"
legacy_id:
- /php/details/34200
- /details1f0d-2.html
imported:
- "2019"
_4images_image_id: "34200"
_4images_cat_id: "1553"
_4images_user_id: "1281"
_4images_image_date: "2012-02-17T19:22:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34200 -->
Ich habe das Problem mit diesem Programm, das die Motoren zwar reagieren aber "stottern", also nicht flüssig laufen. Hat jemand eine Idee, wie ich dieses Problem lösen kann?
Danke im voraus!
mfg Hallo111