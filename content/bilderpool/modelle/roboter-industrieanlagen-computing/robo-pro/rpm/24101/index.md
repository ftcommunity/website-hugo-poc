---
layout: "image"
title: "Unterprogramm"
date: "2009-05-24T19:31:40"
picture: "rpm2.jpg"
weight: "2"
konstrukteure: 
- "Kacker303"
fotografen:
- "Kacker303"
uploadBy: "Kacker303"
license: "unknown"
legacy_id:
- /php/details/24101
- /details37c8.html
imported:
- "2019"
_4images_image_id: "24101"
_4images_cat_id: "1652"
_4images_user_id: "924"
_4images_image_date: "2009-05-24T19:31:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24101 -->
Unterprogramm  für einstellung der Impulsanzahl pro Umdrehung.