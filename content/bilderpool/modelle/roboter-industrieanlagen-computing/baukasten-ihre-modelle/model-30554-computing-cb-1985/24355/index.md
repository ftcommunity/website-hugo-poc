---
layout: "image"
title: "fast fertig"
date: "2009-06-14T09:40:20"
picture: "DSC_2319.jpg"
weight: "11"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24355
- /details3163-3.html
imported:
- "2019"
_4images_image_id: "24355"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24355 -->
