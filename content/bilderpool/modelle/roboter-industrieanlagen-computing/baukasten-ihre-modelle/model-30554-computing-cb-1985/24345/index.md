---
layout: "image"
title: "300 DM (!!!)"
date: "2009-06-14T09:40:01"
picture: "DSC_2300_-_Version_2.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24345
- /detailse635.html
imported:
- "2019"
_4images_image_id: "24345"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24345 -->
