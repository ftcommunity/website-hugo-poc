---
layout: "overview"
title: "Model 30554: Computing CB 1985"
date: 2020-02-22T08:08:15+01:00
legacy_id:
- /php/categories/1667
- /categoriesd492.html
- /categories7443.html
- /categories871d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1667 --> 
Dieser Baukasten ermöglicht den Bau von 10 verschiedenen Modellen zum erlernen der Ansteuerung mit einem parallel Interface.
Dem Baukasten liegt ein Coupon bei zum Bestellen der Software bei fischertechnik.