---
layout: "image"
title: "Inhalt und Preis (??)"
date: "2009-06-14T09:40:01"
picture: "DSC_2300.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24348
- /details055d.html
imported:
- "2019"
_4images_image_id: "24348"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24348 -->
