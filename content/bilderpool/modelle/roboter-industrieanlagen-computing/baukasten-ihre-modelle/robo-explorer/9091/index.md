---
layout: "image"
title: "explorer for the  Stoßdämpfer project"
date: "2007-02-20T10:56:35"
picture: "explorer_001.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
schlagworte: ["explorer"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/9091
- /detailsd517.html
imported:
- "2019"
_4images_image_id: "9091"
_4images_cat_id: "827"
_4images_user_id: "371"
_4images_image_date: "2007-02-20T10:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9091 -->
