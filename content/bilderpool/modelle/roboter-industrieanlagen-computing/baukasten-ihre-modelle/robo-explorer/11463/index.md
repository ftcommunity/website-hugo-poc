---
layout: "image"
title: "Kompletter Baukasten 1"
date: "2007-09-16T00:04:24"
picture: "Kompletter_Baukasten_alles_2.jpg"
weight: "11"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/11463
- /details33bc-2.html
imported:
- "2019"
_4images_image_id: "11463"
_4images_cat_id: "827"
_4images_user_id: "426"
_4images_image_date: "2007-09-16T00:04:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11463 -->
