---
layout: "image"
title: "GymBot_01"
date: "2011-07-17T14:47:32"
picture: "gymbot1.jpg"
weight: "1"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/31315
- /details017f.html
imported:
- "2019"
_4images_image_id: "31315"
_4images_cat_id: "2327"
_4images_user_id: "1177"
_4images_image_date: "2011-07-17T14:47:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31315 -->
CD im Detail 
Mit geschlossener Sensorklappe
