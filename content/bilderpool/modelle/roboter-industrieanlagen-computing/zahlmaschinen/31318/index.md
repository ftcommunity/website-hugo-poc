---
layout: "image"
title: "GymBot_04"
date: "2011-07-17T14:47:32"
picture: "gymbot4.jpg"
weight: "4"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/31318
- /details3ef5.html
imported:
- "2019"
_4images_image_id: "31318"
_4images_cat_id: "2327"
_4images_user_id: "1177"
_4images_image_date: "2011-07-17T14:47:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31318 -->
CD im Detail 
Mit geöffneter Sensorklappe
Wenn man die Sensorklappe öffnet gehen sofort Lampen an damit man mögliche 
Probleme besser beseitigen kann.
Beim schließen der Klappe gehen alle Lampen wieder aus.
