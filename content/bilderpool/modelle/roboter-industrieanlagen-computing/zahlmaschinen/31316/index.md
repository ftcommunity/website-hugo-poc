---
layout: "image"
title: "GymBot_02"
date: "2011-07-17T14:47:32"
picture: "gymbot2.jpg"
weight: "2"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/31316
- /details60db.html
imported:
- "2019"
_4images_image_id: "31316"
_4images_cat_id: "2327"
_4images_user_id: "1177"
_4images_image_date: "2011-07-17T14:47:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31316 -->
(geschlossene Sensorklappe)

Hauptaufgabe des GymBot ist es so genannte "Gym-Taler" zu zählen.
Entweder in 10er Packs oder alle insgesamt.
Mit den Schaltern kann man jeden motor einzeln ein und aus schalten mit kontroll leuchte.
