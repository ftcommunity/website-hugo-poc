---
layout: "image"
title: "First model"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38093
- /details8950.html
imported:
- "2019"
_4images_image_id: "38093"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38093 -->
The first model I made was pretty high, as I hoped that a high point of gravity, with the battery on top, would make the balancing easier. It turned out that a lower and stiffer frame was actually much easier to balance and drive. Also, longer power lines to the battery caused significant power loss. Having the battery closer to the controller helped a lot.

I had the initial model run on a over-inflated basket ball. The ball did not have a smooth surface, so the robot didn't ride or even balance very well. Where would I find a smooth, light, stiff ball?