---
layout: "image"
title: "Drive axle"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38085
- /details7c88-2.html
imported:
- "2019"
_4images_image_id: "38085"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38085 -->
One of the 3 axles. For the first models I used rast axles that were connected to the power motors using a rast connector sleeve (35142). However, there is too much slack in these, and I resorted to metal axles and Graupner 4mm to 5mm shaft connectors (http://www.graupner.de/de/products/3374/product.aspx) to mount them onto the power motors.

Each axle has a quad encoder connected to know its position and speed. These are AMT-103 capacitive encoders by CUIC. They can be set to a wide range of frequencies to suit the limited TX counter inputs, that can realistically handle no more than 500 Hz. I glued them to a Lagerbock or Kupplungsstück 5 (38252), and fit them to the end of the axle. They operate at 5V and produce quad encoder A and B signals. I use the LS7183 IC by LSI Computer Systems to decode these into an up and a down counter. So each wheel uses two TX counter inputs. The TX simply subtracts the two values to find the wheel position. This means I need 2 TXs, as you need 3 axles times 2 counters is 6 counter inputs.

Too bad the TX counter inputs don't have a mode to decode the quad encoder signals directly. I presume the TX counter input values are software based. If so, providing a mode that would produce a single counter value out of the quad encoder signals would be a relatively simple solution to implement. I bet ft will never do that. We need open source!

For the balancing function you need to know the ball or robot speed in the X and Y direction. This can of course be derived from the position information that the wheel encoders provide, but a Kalman filter (http://en.wikipedia.org/wiki/Kalman_filter) needs to be applied to provide smooth position and speed information. Screen shots of that in a later picture.