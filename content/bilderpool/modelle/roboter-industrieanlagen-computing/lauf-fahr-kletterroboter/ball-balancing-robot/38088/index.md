---
layout: "image"
title: "Kalman filters"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38088
- /details5fdd.html
imported:
- "2019"
_4images_image_id: "38088"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38088 -->
This is a close up from the previous screen. It shows the raw wheel position of one axle (blue) and the Kalman filtered positon (red) and speed (green) curves.

The robot maintains its balance using a simple linear controller with angular speed, angle and ball speed as inputs. For staying in position, the ball position is used as well. Tuning of the controller was done manually (and took many hours of trial and error).