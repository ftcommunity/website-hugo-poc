---
layout: "image"
title: "Bluetooth telemetrie"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/38087
- /details3b63.html
imported:
- "2019"
_4images_image_id: "38087"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38087 -->
The robot talks bluetooth to a PC to send sensor and other data in real time, so the data can be analysed on the PC. It is also possible to send commands and information from the PC to the robot. This can be used to easily fine tune the controller parameters and the Kalman filters. Note that the robot runs autonomously; the PC is just used to send it commands to start, stop, drive, turn, etc. and tune the controller parameters. 

Of course the TX only supports bluetooth message exchanges to another TX, not to a PC. I've modified the MAC address of a bluetooth dongle for the PC to trick the TX into thinking that the PC is another TX, so that it will do proper message exchanges. I've described that in the forum: http://forum.ftcommunity.de/viewtopic.php?f=8&t=1960 before.

Since the TX can only send 16 bytes per message, the message exchange capacity is fairly limited. I send five 3-byte floating point variables per message (plus one message type byte makes 16). By ditching 1 byte of the normal TX 4-byte floating point mantisse, the accuracy is limited to about 4-5 digits, but that is enough for the purpose.