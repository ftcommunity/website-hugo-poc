---
layout: "image"
title: "Pixy Software"
date: "2014-11-15T19:29:09"
picture: "pixysoftware5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39822
- /details8092.html
imported:
- "2019"
_4images_image_id: "39822"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39822 -->
Bitte diese Parameter in der PIXY Software einstellen und mit Apply speichern.

 Data Out Port = 1 (I2C Modus)
 I2C Adress = 0X14 (gleiche Hex Adresse wie im RoboPro Programm)



