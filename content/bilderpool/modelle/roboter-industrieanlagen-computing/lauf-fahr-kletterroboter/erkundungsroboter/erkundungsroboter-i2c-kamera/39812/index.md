---
layout: "image"
title: "Objekt erkennen Teil 1"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung23.jpg"
weight: "23"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39812
- /details6b0f.html
imported:
- "2019"
_4images_image_id: "39812"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39812 -->
Als nächstes geht ihr auf die Objekterkennung. Dann Linse scharf stellen.

