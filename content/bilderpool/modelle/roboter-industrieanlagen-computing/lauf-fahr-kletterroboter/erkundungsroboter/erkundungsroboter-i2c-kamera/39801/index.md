---
layout: "image"
title: "Kamera Klarsichtdeckel"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39801
- /details9351.html
imported:
- "2019"
_4images_image_id: "39801"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39801 -->
Er ist mit einer Bohrung ca. 13,0 mm und einer kleinen Nut versehen.
