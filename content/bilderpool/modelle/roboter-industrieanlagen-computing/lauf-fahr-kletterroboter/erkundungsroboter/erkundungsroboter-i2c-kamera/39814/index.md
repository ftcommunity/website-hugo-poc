---
layout: "image"
title: "Objekt erkennen Teil 3"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung25.jpg"
weight: "25"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39814
- /details4ec9.html
imported:
- "2019"
_4images_image_id: "39814"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39814 -->
Anschließend in der Software in die "Home" Position springen. 
Jetzt werden die Bildaten über I2C übergeben.

