---
layout: "image"
title: "Pixy I2C Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39798
- /details5303.html
imported:
- "2019"
_4images_image_id: "39798"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39798 -->
Der Deckel ist nur aufgesteckt.
