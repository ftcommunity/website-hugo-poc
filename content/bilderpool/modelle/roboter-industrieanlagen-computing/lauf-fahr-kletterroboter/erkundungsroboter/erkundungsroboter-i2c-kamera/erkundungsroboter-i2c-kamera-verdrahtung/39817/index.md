---
layout: "image"
title: "Robo Pro I2C Objekterkennung Verdrahtung"
date: "2014-11-12T18:50:54"
picture: "roboproicobjekterkennungverdrahtung2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39817
- /detailsa147-3.html
imported:
- "2019"
_4images_image_id: "39817"
_4images_cat_id: "2986"
_4images_user_id: "2303"
_4images_image_date: "2014-11-12T18:50:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39817 -->
Für eure erste Beschaltung empfehle ich euch farbige Steckbrücken Female -Female.

Bevor ihr später euer Kabel selbst konfiguriert, bitte vorher die PIN-Belegung prüfen. ;-)
