---
layout: "image"
title: "Pixy I2C Kamera vorne"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/39794
- /details61f6.html
imported:
- "2019"
_4images_image_id: "39794"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39794 -->
Die Kamera ist auf einem Gelekstein 35831 befestigt.
