---
layout: "overview"
title: "Erkundungsroboter"
date: 2020-02-22T08:01:39+01:00
legacy_id:
- /php/categories/1813
- /categories3c03.html
- /categories8a2c.html
- /categories3e11.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1813 --> 
Ein einfaches Modell zum Fahren, Erkunden und "aufräumen".