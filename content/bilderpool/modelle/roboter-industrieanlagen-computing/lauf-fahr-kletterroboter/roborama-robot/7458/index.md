---
layout: "image"
title: "Robbie - top view"
date: "2006-11-14T16:23:03"
picture: "Robbie_topview.jpg"
weight: "3"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/7458
- /detailscbc8-2.html
imported:
- "2019"
_4images_image_id: "7458"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7458 -->
Fischertechnik Robo Interface inside :-)

The large open area above the interface is to hold the cans that were collected.

Also note the big red STOP switch and the green START switch, located in the corners.
