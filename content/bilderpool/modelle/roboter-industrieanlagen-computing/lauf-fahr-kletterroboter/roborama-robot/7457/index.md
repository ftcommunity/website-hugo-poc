---
layout: "image"
title: "Robbie with the certificate"
date: "2006-11-14T16:23:03"
picture: "Robbie_certificate.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/7457
- /detailscfc3-2.html
imported:
- "2019"
_4images_image_id: "7457"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7457 -->
