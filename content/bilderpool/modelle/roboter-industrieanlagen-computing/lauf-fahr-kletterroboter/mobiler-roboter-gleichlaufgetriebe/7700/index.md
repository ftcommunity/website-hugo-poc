---
layout: "image"
title: "Beide Antriebe"
date: "2006-12-04T16:39:28"
picture: "MobilerRoboter3.jpg"
weight: "3"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7700
- /detailsf537.html
imported:
- "2019"
_4images_image_id: "7700"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7700 -->
Hinzu kommt der Antrieb des Lenkdifferenzials. Es ist das selbe Prinzip wie beim Fahrdifferenzial.
