---
layout: "image"
title: "Antrieb des Fahrdifferenzials"
date: "2006-12-04T16:39:28"
picture: "MobilerRoboter2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7699
- /detailsd45d.html
imported:
- "2019"
_4images_image_id: "7699"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7699 -->
Das ist der Antrieb vom Fahrdifferenzial.
Ich benutze 50:1 Powermots mit zusätzlicher 2:1 Untersetzung über eine Kette.
