---
layout: "image"
title: "Hinterachsfederung"
date: "2005-10-29T17:25:15"
picture: "07-gefederte_Hinterachse.jpg"
weight: "7"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5129
- /details0e3f-2.html
imported:
- "2019"
_4images_image_id: "5129"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5129 -->
Damit vier Räder immer auf dem Boden bleiben, muß mindestens eine Achse gefedert sein. Diese Ausführung ist einfach und wirkungsvoll. Die Konstruktion wird ein wenig kippelig, wenn die Vorderachse um 90 Grad eingelenkt ist.
