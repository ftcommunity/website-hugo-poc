---
layout: "image"
title: "Teilscheibe und Elektronik"
date: "2005-10-29T19:06:49"
picture: "16-Teilscheibe_mit_Elektronik.jpg"
weight: "16"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5138
- /details2317-3.html
imported:
- "2019"
_4images_image_id: "5138"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T19:06:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5138 -->
Auf diesen Teilen basiert die Wegzählung. Zum ersten eine Teilscheibe von 40 mm Durchmesser mit 250 Linien (2 Linien je Millimeter), dann im gläsernen Gehäuse der Infrarotsensor für die Striche und daneben der Quadraturdekoder, der die Signale für den Computer fertig aufbereitet.

Wenn das so klappt, wie ich mir das denke, dann arbeitet die Odometrieanlage mit 1/7 mm Auflösung. Hoffentlich.
