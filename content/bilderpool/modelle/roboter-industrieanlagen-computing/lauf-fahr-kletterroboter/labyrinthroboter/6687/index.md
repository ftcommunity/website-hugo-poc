---
layout: "image"
title: "Radsensor ausgeführt"
date: "2006-08-15T21:04:46"
picture: "Sensor-neu.jpg"
weight: "25"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/6687
- /detailsa361-3.html
imported:
- "2019"
_4images_image_id: "6687"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-08-15T21:04:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6687 -->
So sieht er bestückt aus. Im Hintergrund habe ich noch ein Ritzel Z10 dabeigepackt. Daran kann man erkennen, daß die Sensorplatine die Größe eines Fingernagels hat.
