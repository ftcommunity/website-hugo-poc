---
layout: "image"
title: "Schnittstellen"
date: "2005-10-30T10:05:47"
picture: "19-Beidenfeld.jpg"
weight: "19"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5141
- /details9872-2.html
imported:
- "2019"
_4images_image_id: "5141"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-30T10:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5141 -->
Solange der Roboter in der Entwicklung bleibt, behält er seinen Tastatur- und Monitoranschluß. Der Roboter benimmt sich dann wie ein ganz gewöhnlicher PC, vielleicht in ungewöhnlichem Gehäuse.

Daneben natürlich auch noch die Ladebuchse für den Akku.
