---
layout: "image"
title: "Wendemanöver 2"
date: "2005-10-29T17:25:15"
picture: "09-komplizierte_Wende.jpg"
weight: "9"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5131
- /detailse51c.html
imported:
- "2019"
_4images_image_id: "5131"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5131 -->
Tja, nachdem die Sackgasse erkannt worden ist, setzt der Roboter in die Ecke zurück und kann mit voll eingeschlagener Vorderachse wieder herausfahren.

Sensorisch und algorithmisch ist dieses Manöver der absolulte Wahnsinn.
