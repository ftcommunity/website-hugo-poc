---
layout: "image"
title: "FT Explorer"
date: "2011-10-20T21:35:01"
picture: "FT_Derk_023.jpg"
weight: "6"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- /php/details/33272
- /detailse7d6.html
imported:
- "2019"
_4images_image_id: "33272"
_4images_cat_id: "579"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T21:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33272 -->
