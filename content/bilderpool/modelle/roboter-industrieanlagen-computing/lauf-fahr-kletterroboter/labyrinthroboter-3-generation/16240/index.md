---
layout: "image"
title: "09-Seitenansicht"
date: "2008-11-09T14:32:58"
picture: "09-Seitenansicht.jpg"
weight: "9"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Interface", "Roboter"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16240
- /detailsb6be.html
imported:
- "2019"
_4images_image_id: "16240"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16240 -->
Die Länge über alles sind 22 cm. Die Breite sind 11 cm und die Höhe 12 cm.

Sollte eines Tages das neue Interface kommen und es tatsächlich nur 9 cm lang sein, dann richte ich den um 30 Grad nach hinten gekippten Motor auf, ziehe den Ultraschallsensor über die Lenkung und beschränke dann die Länge des Roboters auf gerade mal noch 16 cm.
