---
layout: "image"
title: "07-vor der Hochzeit"
date: "2008-11-09T14:32:58"
picture: "07-komplett.jpg"
weight: "7"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Akku", "Karosserie", "Chassis", "Interface", "Ultraschallsensor"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16238
- /details8b23.html
imported:
- "2019"
_4images_image_id: "16238"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16238 -->
Normalerweise ist die Hochzeit eines Autos das Aufsetzen der Karosserie auf das Chassis, hier ist es das Aufsetzen des Interfaces auf das Chassis.

Die Akkus sind in Position, Halterungen für das Interface vorhanden und am Interface ist auch schon der Ultraschallsensor für die Wegmessung nach vorne.
