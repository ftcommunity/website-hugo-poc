---
layout: "image"
title: "10-Heckansicht"
date: "2008-11-09T14:32:58"
picture: "10-Heckansicht.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16241
- /details9685.html
imported:
- "2019"
_4images_image_id: "16241"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16241 -->
Der Vollständigkeit halber auch von hinten zu sehen: Hier ist nur die geplante Stromverteilung von größerem Interesse.
