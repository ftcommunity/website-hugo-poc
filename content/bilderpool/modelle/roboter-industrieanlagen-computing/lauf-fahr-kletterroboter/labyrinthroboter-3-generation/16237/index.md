---
layout: "image"
title: "06-Akkus"
date: "2008-11-09T14:32:57"
picture: "06-Akkus.jpg"
weight: "6"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Akku", "Ladebucht", "Kapazität", "Rahmen", "Chassis", "Interface"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/16237
- /details9670.html
imported:
- "2019"
_4images_image_id: "16237"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16237 -->
Die obere Rahmenkonstruktion stabilisiert das Chassis. Die etwas merkwürdige Konstruktion mit den verschobenen Bausteinen ist leider notwendig, weil die Geometrie nicht astrein aufgeht und weil unbedingt Platz für die Vorderräder bleiben muß. Bei vollen Lenkeinschlag beanspruchen die Räder enorm viel Platz.

Günstig sind die beiden Winkelträger 30, die eine ideale Ladebucht für die beiden Akkuhalter abgeben. Die Akkukapazität von 8 Stück AAA (Micro) ist auf 700 mAh bei knapp 10 Volt beschränkt. Ist nicht so wahnsinnig viel, aber damit soll es mal losgehen.

Die hochragenden Rahmenteile nehmen dann noch das Interface auf.
