---
layout: "image"
title: "Roboter mit sämtlichen Aufbauten von vorne"
date: "2015-08-03T13:01:55"
picture: "1027.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Mobiler", "Roboter", "Kettenfahrwerk", "Omnivision", "OV7670", "Atmel", "AT90USB", "Farbdisplay", "ILI9341", "Farberkennung", "digitaler", "Farbfilter", "Pneumatik", "Greifer", "Color", "Blob", "Tracking"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41704
- /detailsfa12.html
imported:
- "2019"
_4images_image_id: "41704"
_4images_cat_id: "3109"
_4images_user_id: "579"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41704 -->
Mobiler Roboter mit Kettenfahrwerk und CMOS Kamera sucht, erkennt und greift eine mit einem roten Streifen markierte Tonne mit seinem Pneumatik-Greifer.

Video gibt es hier:

https://www.youtube.com/watch?v=vDrwGIaLlIo

Auf dem Farbdisplay wird oben das originale Farbbild der Kamera dargestellt (RGB565-Format) und unten das aus dem YCbCr-Format nach einem Farbfilter auf rote Farbe gefilterte Bild. Die x-y-Bildkoordinaten der gefilterten Objekte werden dann im Originalbild oben an den Objekten angezeigt.

Steuerelektronik:

- AT90USB1287 Controller mit Farb-Display 2,2 Zoll (ILI9341) an der SPI-Schnittstelle 
- Omnivision OV7670 Kamera mit FIFO AL422
- Ultraschall-Entfernungsmesser
- Gabellichtschranken als Motor-Encoder