---
layout: "image"
title: "Kompass-Sensor HMC5883 und Treiberplatine für Magnetventile"
date: "2015-08-03T13:01:55"
picture: "1039.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41708
- /detailsd0a9.html
imported:
- "2019"
_4images_image_id: "41708"
_4images_cat_id: "3109"
_4images_user_id: "579"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41708 -->
