---
layout: "image"
title: "Farbdisplay"
date: "2015-08-03T13:01:55"
picture: "1038.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Display", "RGB", "YCbCr", "Farbdisplay", "Farbfilter", "Color", "Blob", "Tracking", "Farbbild"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41707
- /details97d6-2.html
imported:
- "2019"
_4images_image_id: "41707"
_4images_cat_id: "3109"
_4images_user_id: "579"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41707 -->
Auf dem Farbdisplay wird oben das originale Farbbild der Kamera dargestellt (RGB565-Format) und unten das aus dem YCbCr-4:2:2-Format nach einem Farbfilter auf rote Farbe gefilterte Bild. Die x-y-Bildkoordinaten der gefilterten Objekte werden dann im Originalbild oben an den Objekten angezeigt.