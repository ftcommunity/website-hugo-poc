---
layout: "image"
title: "Gesamtansicht"
date: "2005-08-26T17:42:52"
picture: "motorisierte_Roboter_079.jpg"
weight: "1"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4655
- /details53c3.html
imported:
- "2019"
_4images_image_id: "4655"
_4images_cat_id: "377"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T17:42:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4655 -->
