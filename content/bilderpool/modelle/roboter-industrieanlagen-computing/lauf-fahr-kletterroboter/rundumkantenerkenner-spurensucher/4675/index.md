---
layout: "image"
title: "Gesamtansicht von der Seite"
date: "2005-08-26T20:52:34"
picture: "motorisierte_Roboter_082.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4675
- /detailsd7c6.html
imported:
- "2019"
_4images_image_id: "4675"
_4images_cat_id: "377"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T20:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4675 -->
