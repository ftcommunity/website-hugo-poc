---
layout: "image"
title: "TX Roboter"
date: "2009-06-05T16:02:16"
picture: "tx1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/24158
- /detailscc3b-2.html
imported:
- "2019"
_4images_image_id: "24158"
_4images_cat_id: "1659"
_4images_user_id: "453"
_4images_image_date: "2009-06-05T16:02:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24158 -->
Hier mal ein erster kleiner TX Roboter zum testen der Funktionen.
