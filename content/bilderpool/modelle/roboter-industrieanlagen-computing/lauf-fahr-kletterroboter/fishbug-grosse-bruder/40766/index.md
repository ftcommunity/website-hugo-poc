---
layout: "image"
title: "Fishbug von der Seite"
date: "2015-04-16T17:40:45"
picture: "fishbugdergrossebruder4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40766
- /details62d7-3.html
imported:
- "2019"
_4images_image_id: "40766"
_4images_cat_id: "3063"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T17:40:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40766 -->
