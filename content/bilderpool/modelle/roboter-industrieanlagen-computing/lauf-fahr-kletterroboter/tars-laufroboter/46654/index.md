---
layout: "image"
title: "Endanschlag eines Beines"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter06.jpg"
weight: "6"
konstrukteure: 
- "olagino - Leon"
fotografen:
- "olagino - Leon"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/46654
- /details776a-2.html
imported:
- "2019"
_4images_image_id: "46654"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46654 -->
Die beiden Endanschläge der äußeren Beine sind jeweils symmetrisch zueinander aufgebaut, das heißt sie drehen sich beim initialisieren nicht in dieselbe Richtung sondern entgegengesetzt zueinander. Schwachstelle hierbei ist vor allem, dass der Taster manchmal knapp am Baustein 5 vorbeischrammt bzw. sich vor allem durch die Verzögerung im Bluetooth-Onlinemodus der Endanschlag von Zeit zu Zeit verschiebt.
