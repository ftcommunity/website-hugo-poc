---
layout: "image"
title: "Übersicht"
date: "2012-03-30T13:45:04"
picture: "roboter1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34706
- /details7719-2.html
imported:
- "2019"
_4images_image_id: "34706"
_4images_cat_id: "2563"
_4images_user_id: "453"
_4images_image_date: "2012-03-30T13:45:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34706 -->
Dieses Fahrwerk mit Mecanum-Rädern kann vor, zurück sich drehen und Seitlich fahren. 
Angtrieben werden die Räder so:
http://robotics.ee.uwa.edu.au/eyebot/doc/robots/omni-principle.gif
http://robotics.ee.uwa.edu.au/eyebot/doc/robots/omni.html

Video
http://youtu.be/tDBjIEHQZiY

