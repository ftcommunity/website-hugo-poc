---
layout: "image"
title: "defiant_prototyp_3_rad"
date: "2008-06-11T22:04:12"
picture: "p6110061.jpg"
weight: "1"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
schlagworte: ["mobile", "robot", "test", "bürstenlos"]
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14673
- /details9d31.html
imported:
- "2019"
_4images_image_id: "14673"
_4images_cat_id: "1346"
_4images_user_id: "3"
_4images_image_date: "2008-06-11T22:04:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14673 -->
Einige von euch wissen schon, daß ich große Probleme habe, meine beiden neuen superschnellen Motoren in meinem jetzigen mobilen Roboter zu kontrollieren. Die neuen Motoren machen schnelle Geschwindigkeiten (>30cm/s) sehr gut mit, aber für langsame Fahrten, oder genaue Steuerungen kann ich sie leider nicht verwenden. Deswegen diese Idee:
1. Vorne sitzt ein normaler Motor wie auf den Bild zu sehen.
2. Der vordere Motor wird bei bedarf weggeklappt/eingezogen.
3. Hinten kommen die schnellen Motoren ran.

Das ganze soll dann so laufen:

Bei langsamen Geschwindigkeiten übernimmt der vordere Motor. Das ganze Modell verhält sich dann wie ein Auto. Die schnellen Motoren hinten drehen dann einfach mit, was nur wenig Kraft kostet.

Für schnelle Geschwindigkeiten wird der vordere Motor eingeklappt und die hinteren, schnellen Motoren übernehmen das Ruder. Das Modell verhält sich dann wie ein Kettenfahrzeug.

Und jetzt möchte ich zur Diskussion einladen.