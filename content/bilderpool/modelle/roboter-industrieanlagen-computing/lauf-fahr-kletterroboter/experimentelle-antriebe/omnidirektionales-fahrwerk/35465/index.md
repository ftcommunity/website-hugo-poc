---
layout: "image"
title: "von unten"
date: "2012-09-08T15:23:52"
picture: "omnidirektionalesfahrwerk4.jpg"
weight: "4"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35465
- /details23c4.html
imported:
- "2019"
_4images_image_id: "35465"
_4images_cat_id: "2629"
_4images_user_id: "1196"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35465 -->
Der Fahrroboter wird von drei Encodermotoren angetrieben.
