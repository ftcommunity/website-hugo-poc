---
layout: "image"
title: "Von der Seite"
date: "2012-09-08T15:23:52"
picture: "omnidirektionalesfahrwerk5.jpg"
weight: "5"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35466
- /detailsdf88.html
imported:
- "2019"
_4images_image_id: "35466"
_4images_cat_id: "2629"
_4images_user_id: "1196"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35466 -->
Ein Video gibt es leider noch nicht.
