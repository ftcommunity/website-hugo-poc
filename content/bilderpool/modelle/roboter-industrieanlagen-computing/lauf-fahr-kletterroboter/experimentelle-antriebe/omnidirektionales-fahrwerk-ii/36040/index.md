---
layout: "image"
title: "von vorne"
date: "2012-10-22T19:11:45"
picture: "omnidirektionalesfahrwerkii2.jpg"
weight: "2"
konstrukteure: 
- "werner"
fotografen:
- "werner"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36040
- /details4b16.html
imported:
- "2019"
_4images_image_id: "36040"
_4images_cat_id: "2682"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36040 -->
