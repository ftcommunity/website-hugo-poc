---
layout: "comment"
hidden: true
title: "17463"
date: "2012-10-22T19:46:13"
uploadBy:
- "werner"
license: "unknown"
imported:
- "2019"
---
Das habe ich auch schon versucht, aber die Räder rutschen trotzdem durch.
Außerdem kann ich die Encodermotoren nicht so langsam drehen lassen.
Und bei der Anlaufgeschwindigkeit der Motoren drehen die Räder schon durch.

WERNER