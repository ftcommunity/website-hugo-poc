---
layout: "comment"
hidden: true
title: "17462"
date: "2012-10-22T19:21:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
... oder eine "Anfahrrampe", also das langsame Beschleunigen, wobei die Beschleunigung immer unter der Grenze bleibt, ab der die Räder durchdrehen würden.

Gruß,
Stefan