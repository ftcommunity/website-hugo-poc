---
layout: "image"
title: "mit Ultraschallsensor"
date: "2012-10-22T21:09:01"
picture: "omnidirektionalesfahrwerkii4.jpg"
weight: "4"
konstrukteure: 
- "werner"
fotografen:
- "werner"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36042
- /details302a.html
imported:
- "2019"
_4images_image_id: "36042"
_4images_cat_id: "2682"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36042 -->
