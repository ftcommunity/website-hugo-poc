---
layout: "image"
title: "defiant_prototyp_3_rad_v2"
date: "2008-07-13T16:52:02"
picture: "p7130061.jpg"
weight: "2"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
schlagworte: ["mobile", "robot", "test", "bürstenlos"]
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/14832
- /details47ac-2.html
imported:
- "2019"
_4images_image_id: "14832"
_4images_cat_id: "1346"
_4images_user_id: "3"
_4images_image_date: "2008-07-13T16:52:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14832 -->
Version 2: So in etwa hatte ich mir das ganze vorgestellt.
Für eine detaillierte Beschreibung bitte das Bild davor ansehen.
Hier ist die Lift-Mechanik für das dritte Rad ist dazugekommen.
 
Etwas wackelig ist das ganze aber noch...