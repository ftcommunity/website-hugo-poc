---
layout: "image"
title: "Autonomer fischertechnik Roboter mit Ardunino Uno und Pixy Kamera"
date: "2017-02-28T18:21:37"
picture: "arduinounoautonomerroboterausfischtechnik1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/45336
- /details0ec1.html
imported:
- "2019"
_4images_image_id: "45336"
_4images_cat_id: "3376"
_4images_user_id: "34"
_4images_image_date: "2017-02-28T18:21:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45336 -->
Ein autonomer fischertechnik Roboter mit Pixy Kamera  und Objekterkennung
ft Modell BBC Roboter bzw CVK "Schildkröte" 67068 von 1991

Arduino Uno( bzw.hier im Bild  Nachbau von Joy-it)
Adafruit Servo-Motor Schield 2.3
Pixy Kamera
ft-Accu
unf etwas Werbung :-)

Gesteuert mit einem Aduino-Programm
Einfaches Programm das versucht das Objekt im Zentrum der Kamera zu halten 
und über die Größe der Fläche den Abstand (wie im Bild)

Einstellung der Pixy Kamera nur auf eine Farbe.
Anzahl der Objekte ist auch reduziert, somt ist die Reaktion schneller.
Der Roboter selbst ist allerdings nicht der schnellste - aber es funktioniert.

H. Howey
