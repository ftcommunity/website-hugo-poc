---
layout: "image"
title: "Autonomer fischertechnik Roboter mit Ardunino Uno und Pixy Kamera Rückansicht"
date: "2017-02-28T18:21:37"
picture: "arduinounoautonomerroboterausfischtechnik2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/45337
- /details2870.html
imported:
- "2019"
_4images_image_id: "45337"
_4images_cat_id: "3376"
_4images_user_id: "34"
_4images_image_date: "2017-02-28T18:21:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45337 -->
Der Ardunino Uno ist auf zwei Bauplatten festgeschraubt
die Pixy Kamera an zwei roten Winkel(Eck)-Bausteinen.
Außerdem ist die Kamera um 30 Grad geneigt.
Die Taster sind nicht aktiviert, auch nicht für die Bewegung der Räder.
Evtl. kommt noch ein Ultraschallsensor dazu.
Man könnte auch einen Foßballroboter daraus programmieren.
Spannung aus dem ft-Accu geht über den Motorshield zum Ardunino.

H. Howey

PS Was mich wunterte ist, dass es keine Beispielprogramme für so eine 
Kombiunation im Netz gibt. (Zumindest hab ich keine gefunden)

Tipp: Beim einlöten der Steckerleisten am Motorshield diese -nicht- nach hinten 
überstehen lassen!!! Der Stecker der Pixy-Kamera ist zu hoch und die Motorschieldplatine
bekommt sonst nur so eben kontaskt mit den Buchsen des Ardunino Uno. Das haben die 
von Adafruit wohl auch gemerkt und das bei ihrer Beschreibung von dem Pixy Roboter
die Buchsenleiste ihres Uno-Nachbaus nach oben gesetzt. Bei der Bauanleitung zum 
Adafruit Motorshield ist das nicht beschrieben. :-(