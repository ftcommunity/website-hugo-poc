---
layout: "image"
title: "Knie"
date: "2007-03-28T09:08:54"
picture: "131_3176.jpg"
weight: "8"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9824
- /detailscf9e.html
imported:
- "2019"
_4images_image_id: "9824"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-28T09:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9824 -->
