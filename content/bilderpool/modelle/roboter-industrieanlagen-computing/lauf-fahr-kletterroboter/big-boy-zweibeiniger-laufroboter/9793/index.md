---
layout: "image"
title: "Big Boy"
date: "2007-03-27T12:30:44"
picture: "131_3195.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["Laufroboter", "zwei", "Beine", "Humanoid"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9793
- /details0ec8.html
imported:
- "2019"
_4images_image_id: "9793"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-27T12:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9793 -->
Modell eines zweibeinigen Laufroboters.
Ziel ist es gewesen damit Treppen raufzulaufen.
Steuerung war nur per hand. Ein RoboPro programm ist in arbeit gewesen war aber sehr sehr aufwändig. Evt sollte ein anderer Ansatz über Listen erfolgen.
