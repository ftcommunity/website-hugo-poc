---
layout: "image"
title: "Bein heben"
date: "2007-03-27T12:30:45"
picture: "131_3189.jpg"
weight: "3"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9795
- /detailse210.html
imported:
- "2019"
_4images_image_id: "9795"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-27T12:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9795 -->
Die Powermotoren sind das Gegengewicht zu den Füßen. So ist immer alles in Waage.
