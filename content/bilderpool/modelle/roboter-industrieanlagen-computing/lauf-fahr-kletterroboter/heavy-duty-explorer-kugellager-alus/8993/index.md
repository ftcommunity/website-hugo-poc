---
layout: "image"
title: "Heavy-duty Explorer mit Kugellager und Alu's"
date: "2007-02-12T17:47:07"
picture: "Explorer-Eucalypta_012.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/8993
- /details3476.html
imported:
- "2019"
_4images_image_id: "8993"
_4images_cat_id: "818"
_4images_user_id: "22"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8993 -->
Heavy-duty Explorer mit Kugellager und Alu's
