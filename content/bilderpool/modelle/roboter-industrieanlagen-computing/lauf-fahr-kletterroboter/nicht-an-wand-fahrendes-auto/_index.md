---
layout: "overview"
title: "Nicht an die Wand fahrendes Auto"
date: 2020-02-22T08:01:24+01:00
legacy_id:
- /php/categories/1357
- /categories6422.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1357 --> 
Das ist ein Auto, das nicht an die Wand fährt. Wenn es kurz davor ist geht der Summer an und das Auto fährt zwei Sekunden lang Rückwärts.