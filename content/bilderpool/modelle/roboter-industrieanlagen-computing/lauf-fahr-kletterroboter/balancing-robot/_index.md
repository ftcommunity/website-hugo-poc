---
layout: "overview"
title: "Balancing Robot"
date: 2020-02-22T08:01:47+01:00
legacy_id:
- /php/categories/2741
- /categories0d3c.html
- /categories15d3.html
- /categories0d77.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2741 --> 
Using the TX Controller and some I2C sensors you can build a balacing robot. Some Arduino-based models have been shown, but this one consist entirely out of fischertechnik parts, apart from the sensor.

It uses a gyro (to measure rotation, or the speed of falling) and an accelerometer (to determine 'up') that are connected to the TX's I2C interface. 100 per second the sensors are read, the data filtered and the motor PWMs calculated and adjusted using a PID controller. The TX has plenty of power to do the math.

The robot can balance just using the gyro and accelerometer data, but it has no sense of where it is, and if it is moving. To control that, this model also uses a (non-ft) quad encoder sensor (http://www.ni.com/white-paper/4763/en) on the wheels and a IC to transform the quad encoder data into counter signals that the counter inputs of the TX can handle. Adding position and speed into the PID controller, the robot can stay in place, even when out of balance, or move using the ft remote control. Unfortunately the TX (unlike the Robo Interface) has no infrared receiver. so to use the remote controller, I simply connected two motor outputs of the remote controller to TX inputs in analogue 10V mode. When operated they give a nice reading between 5-9V that can be used as inputs in the PID controller to drive the robot.

Some pictures below, but this is all about movement, so you need to see the video of the balancing robot here: http://www.youtube.com/watch?v=DWVOfmidFlE
