---
layout: "image"
title: "The wheel encoders"
date: "2013-05-12T16:50:50"
picture: "balancingrobot07.jpg"
weight: "7"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36897
- /details79ea.html
imported:
- "2019"
_4images_image_id: "36897"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36897 -->
For positioning and driving, wheel encoders are needed. These are AMT-103 capacitive encoders by CUIC. They can be set to wide range of frequencies to suit the limited TX counter inputs that can realistically handle no more than 500 Hz. I glued them to a Lagerbock or Kupplungsstück 5 (38252), and they just fit in the model here. They operate on 5V and produce quad encoder A and B signals. I use the LS7183 IC by LSI Computer Systems to decode them into an up and a down counter. So each wheel uses two TX counter inputs. The TX simply subtracts the two values to find the wheel position.

Too bad (I) the fischertechnik encoder motors have ony one pulse, which does not allow you to determine in which direction the motor is turning.

Too bad (II) the TX counter inputs don't have a mode to decode the quad encoder signals directly. I presume the TX counter input values are software based. If so, providing a mode that would produce a single counter value out of the quad encoder signals would be relatively simple solution to implement. I bet ft will never do that. We need open source!