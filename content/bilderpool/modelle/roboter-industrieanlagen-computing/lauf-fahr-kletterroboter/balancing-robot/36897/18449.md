---
layout: "comment"
hidden: true
title: "18449"
date: "2013-11-01T11:09:41"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hi Willem,

the 24V version of the Encoder motor is available with quad encoder. There was also a discussion with the RoboPro developer about adding a quad encoder support in the TX firmware in the beta test. Not sure about the current status, but will ask :)

Thomas