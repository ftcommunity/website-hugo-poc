---
layout: "image"
title: "Balancing Robot"
date: "2013-05-12T16:50:50"
picture: "balancingrobot01.jpg"
weight: "1"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36891
- /detailsd612.html
imported:
- "2019"
_4images_image_id: "36891"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36891 -->
The drive parts are below the ground plate 120*60. The sensor and the TX are sitting on top of it. To make stabalizing a bit easier the weighty battery is on top, with the remote control unit mounted next to the battery with the infrared receiver upwards.