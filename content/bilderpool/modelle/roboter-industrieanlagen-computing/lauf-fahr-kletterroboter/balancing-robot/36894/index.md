---
layout: "image"
title: "The gyro/accelerometer"
date: "2013-05-12T16:50:50"
picture: "balancingrobot04.jpg"
weight: "4"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36894
- /details4020.html
imported:
- "2019"
_4images_image_id: "36894"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36894 -->
The little green board is a MinIMU-9 v2 breakout board by Pololu (http://www.pololu.com/catalog/product/1268) that houses a gyro, a combined accelerometer/magnetometer (for compass function, not used here), and a 5V I2C level shifter, so that it can be connected directly to the TX's 5V I2C connector. It needs 5V power, which I generate on the board with the quad encoder ICs. I have not tried sourcing the 5V from the 5V pin in the TX's I2C connector, as that doesn't seem to be designed as power source, but it may just work.

The gyro is needed to provide fast rotational speed information. As gyro's have a bias that drifts over time (that is, even when not in motion, they will typically indicate a small degrees per second rotation which may change over time) an accelerometer is needed to correct the bias and provide the robot a sense of where 'above' and 'below' are. I use a simple Kalman filter (http://en.wikipedia.org/wiki/Kalman_filter) to maintain a filtered bias and robot angle. The rotational speed (un-biased) is used unfiltered.