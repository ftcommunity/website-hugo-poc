---
layout: "image"
title: "The drive part - 1"
date: "2013-05-12T16:50:50"
picture: "balancingrobot05.jpg"
weight: "5"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/36895
- /details983f.html
imported:
- "2019"
_4images_image_id: "36895"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36895 -->
Building the drive part was a challenge. In order to keep the basis as small as possible the two 1:50 power motors need to be placed next to each other rather than in-line. The challenging form factor of the power motors makes for a somewhat messy looking solution.