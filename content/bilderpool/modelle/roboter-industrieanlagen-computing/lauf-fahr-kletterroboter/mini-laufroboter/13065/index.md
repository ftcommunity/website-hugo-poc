---
layout: "image"
title: "Laufroboter 2"
date: "2007-12-12T20:32:33"
picture: "2.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13065
- /detailscda4.html
imported:
- "2019"
_4images_image_id: "13065"
_4images_cat_id: "1184"
_4images_user_id: "34"
_4images_image_date: "2007-12-12T20:32:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13065 -->
