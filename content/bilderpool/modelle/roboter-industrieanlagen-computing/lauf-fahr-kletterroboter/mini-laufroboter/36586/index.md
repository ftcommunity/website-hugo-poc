---
layout: "image"
title: "blackfoot & friend"
date: "2013-02-07T14:24:48"
picture: "ft_blackfoot3.jpg"
weight: "8"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/36586
- /details11e2.html
imported:
- "2019"
_4images_image_id: "36586"
_4images_cat_id: "1184"
_4images_user_id: "427"
_4images_image_date: "2013-02-07T14:24:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36586 -->
