---
layout: "overview"
title: "Mini Laufroboter"
date: 2020-02-22T08:01:15+01:00
legacy_id:
- /php/categories/1184
- /categoriesccae.html
- /categories8615.html
- /categoriesb650.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1184 --> 
Zweibeiniger Mini - Laufroboter
Funktionsweise ist wie eine Aufziehspielzeug nur halt mit einem ft - Minimot.
Der Roboter läuft etwas im Kreis. Ab und an verhackt er sich an den Streben. Statt der Streben kann man auch die langen 90er + 15er Verbinder nehmen und den 15er etwas raus schauen lassen. 
Wenn man den Trafo voll aufdreht ist er sehr schnell!
Holger Howey