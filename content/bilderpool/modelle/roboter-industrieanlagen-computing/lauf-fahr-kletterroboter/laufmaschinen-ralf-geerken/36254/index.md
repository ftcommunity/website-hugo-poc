---
layout: "image"
title: "Herrmann, der kleine Laufroboter Vorne"
date: "2012-12-10T22:46:33"
picture: "herrmann2.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/36254
- /details9bf7.html
imported:
- "2019"
_4images_image_id: "36254"
_4images_cat_id: "2690"
_4images_user_id: "381"
_4images_image_date: "2012-12-10T22:46:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36254 -->
Bei youtube gibts auch ein paar Filmchen von mir:
HerrmannFrontal: http://www.youtube.com/watch?v=exlkGJJXY0A
HerrmannVonDerSeite: http://www.youtube.com/watch?v=zedhHhmnyZY
HerrmannErklimmtDieTeppichkante: http://www.youtube.com/watch?v=XCxEF36rJOk
