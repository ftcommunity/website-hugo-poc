---
layout: "image"
title: "Huuuch,"
date: "2012-12-10T22:41:44"
picture: "hugo2.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/36250
- /details65e3.html
imported:
- "2019"
_4images_image_id: "36250"
_4images_cat_id: "2690"
_4images_user_id: "381"
_4images_image_date: "2012-12-10T22:41:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36250 -->
ein Mensch! Denkt die Maschine. Huuuch, eine Maschine! Denkt der Mensch.
