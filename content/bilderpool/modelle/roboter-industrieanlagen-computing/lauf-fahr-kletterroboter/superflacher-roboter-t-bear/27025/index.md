---
layout: "image"
title: "Von oben"
date: "2010-05-01T14:59:00"
picture: "superflacherroboter3.jpg"
weight: "3"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- /php/details/27025
- /detailscd34.html
imported:
- "2019"
_4images_image_id: "27025"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27025 -->
Der roboter von oben.