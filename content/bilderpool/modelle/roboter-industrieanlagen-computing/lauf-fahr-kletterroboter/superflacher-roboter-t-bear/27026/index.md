---
layout: "image"
title: "Die seite."
date: "2010-05-01T14:59:01"
picture: "superflacherroboter4.jpg"
weight: "4"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- /php/details/27026
- /details1918.html
imported:
- "2019"
_4images_image_id: "27026"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27026 -->
Das hier ist die linke seite. Der impulszaehler ist auf der anderen seite gebaut, wie im den robo mobile set.