---
layout: "image"
title: "Explorer 1"
date: "2007-10-06T18:50:06"
picture: "explorerstefanl01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12144
- /details3046.html
imported:
- "2019"
_4images_image_id: "12144"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12144 -->
Hier mal meine Version des Robo Explorers. Er hat zusätzlich noch 2 Lichtsensoren und seitlich jeweils einen Taster damit er erkennt ob er seitlich anstößt da der Abstandssensor ab einem gewissen Winkel ja nichts mehr erkennt.
