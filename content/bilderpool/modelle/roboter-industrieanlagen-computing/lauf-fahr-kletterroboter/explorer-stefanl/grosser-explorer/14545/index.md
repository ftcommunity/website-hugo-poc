---
layout: "image"
title: "Explorer 3"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14545
- /details0f95-2.html
imported:
- "2019"
_4images_image_id: "14545"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14545 -->
