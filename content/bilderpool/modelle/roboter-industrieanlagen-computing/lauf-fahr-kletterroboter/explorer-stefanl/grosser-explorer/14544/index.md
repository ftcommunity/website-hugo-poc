---
layout: "image"
title: "Explorer 2"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14544
- /details28f5-2.html
imported:
- "2019"
_4images_image_id: "14544"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14544 -->
