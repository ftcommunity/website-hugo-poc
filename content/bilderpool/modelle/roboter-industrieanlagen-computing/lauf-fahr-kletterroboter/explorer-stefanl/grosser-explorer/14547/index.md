---
layout: "image"
title: "Explorer 5"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/14547
- /detailsf13f.html
imported:
- "2019"
_4images_image_id: "14547"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14547 -->
