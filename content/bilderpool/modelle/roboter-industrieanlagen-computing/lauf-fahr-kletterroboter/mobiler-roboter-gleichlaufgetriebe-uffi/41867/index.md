---
layout: "image"
title: "Umbau zur Untersetzung vom Lenkgetriebe: Detail2"
date: "2015-08-26T21:05:18"
picture: "GK5.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41867
- /details81f7.html
imported:
- "2019"
_4images_image_id: "41867"
_4images_cat_id: "3108"
_4images_user_id: "579"
_4images_image_date: "2015-08-26T21:05:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41867 -->
