---
layout: "image"
title: "Umbau zur Untersetzung vom Lenkgetriebe: Ansicht von oben"
date: "2015-08-26T21:05:18"
picture: "GK1.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Gleichlaufgetriebe", "Lenkmotor", "Untersetzung"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41864
- /details1d76.html
imported:
- "2019"
_4images_image_id: "41864"
_4images_cat_id: "3108"
_4images_user_id: "579"
_4images_image_date: "2015-08-26T21:05:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41864 -->
Nachdem der Motor für die Lenkung gelegentlich zu wenig Drehmoment hatte, um das Fahrzeug auf der Stelle zu drehen, habe ich eine Untersetzung eingebaut