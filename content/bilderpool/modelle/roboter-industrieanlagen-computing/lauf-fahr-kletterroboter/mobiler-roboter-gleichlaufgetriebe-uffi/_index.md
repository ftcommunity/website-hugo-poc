---
layout: "overview"
title: "Mobiler Roboter mit Gleichlaufgetriebe (uffi)"
date: 2020-02-22T08:01:51+01:00
legacy_id:
- /php/categories/3108
- /categories9f0e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3108 --> 
Gleichlaufgetriebe mit den alten grauen M-Motoren und den alten roten Differential-Getrieben als Fahrgestell für einen mobilen Roboter. Die Übersetzung ist allerdings grenzwertig groß, er ist sehr schnell. Damit die Motoren gut anlaufen, braucht man eine Spannung > 8V. Ich habs mit LiPo-2S-Zellen (8,4V) getestet, damit geht es gut. Ist aber nichts für feine Positionierungen.