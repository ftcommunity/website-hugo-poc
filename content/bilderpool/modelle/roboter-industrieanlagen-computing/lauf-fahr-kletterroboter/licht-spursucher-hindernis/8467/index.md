---
layout: "image"
title: "von unten"
date: "2007-01-15T17:09:29"
picture: "roboterkhls2.jpg"
weight: "2"
konstrukteure: 
- "Wert"
fotografen:
- "Wert"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8467
- /details1ab7.html
imported:
- "2019"
_4images_image_id: "8467"
_4images_cat_id: "779"
_4images_user_id: "366"
_4images_image_date: "2007-01-15T17:09:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8467 -->
Modell von unten. Jetzt kann man gut erkennen, das der roboter ähnlich wie die Fahrroboter aus dem RoboMobileSet aufgebaut sind.