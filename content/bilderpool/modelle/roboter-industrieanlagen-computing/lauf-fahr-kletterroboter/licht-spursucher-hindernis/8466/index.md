---
layout: "image"
title: "von oben"
date: "2007-01-15T17:09:29"
picture: "roboterkhls1.jpg"
weight: "1"
konstrukteure: 
- "Wert"
fotografen:
- "Wert"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8466
- /detailsfeb3.html
imported:
- "2019"
_4images_image_id: "8466"
_4images_cat_id: "779"
_4images_user_id: "366"
_4images_image_date: "2007-01-15T17:09:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8466 -->
Das ist ein Roboter, der Licht und Spuren sucht und gleichzeitigKanten und Hindernisse erkennt. Es ist absichtlich ohne Sensorkabel Fotografiert. Das Modell ist leider etwas lang und breit geworden(breit wegen kombinierter Kanten/Hindernisserkennung, lang, weil ich nur in diesen Längen Alus habe. Ohne ist das Modell nicht stabil genug.) Es funktioniert aber einwandfrei. Das Programm werde ich bald mal hochladen