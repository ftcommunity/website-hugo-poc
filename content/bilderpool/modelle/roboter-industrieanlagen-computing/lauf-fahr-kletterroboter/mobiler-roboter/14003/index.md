---
layout: "image"
title: "Radar"
date: "2008-03-22T12:31:00"
picture: "mr8.jpg"
weight: "8"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/14003
- /details7f02.html
imported:
- "2019"
_4images_image_id: "14003"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-03-22T12:31:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14003 -->
Impulstaster und erste Leitungsübertragung.
