---
layout: "image"
title: "Mein neuer Robbi Bild 3"
date: "2003-07-20T19:33:03"
picture: "Robbi2.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "-?-"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1251
- /details982b.html
imported:
- "2019"
_4images_image_id: "1251"
_4images_cat_id: "209"
_4images_user_id: "130"
_4images_image_date: "2003-07-20T19:33:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1251 -->
