---
layout: "image"
title: "Darstellung der Labyrinthwege im Display"
date: "2016-06-08T18:14:50"
picture: "Display_unten.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["BBC", "Buggy", "Economatics", "Labyrinth", "Weg", "Pfad", "reduziert", "kürzester", "Display"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43709
- /details529a.html
imported:
- "2019"
_4images_image_id: "43709"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43709 -->
Oben ist der erkundete Weg im Labyrinth vom Start bis zum Ziel als Buchstabenfolge dargestellt.

L = links;  R = rechts;  S = geradeaus (straight);  B = zurück (back).

Darunter ist der berechnete reduzierte Pfad dargestellt, also der kürzeste Weg vom Start zum Ziel.

Ganz unten sind der erkundete Weg im Labyrinth weiß und der berechnete kürzeste Weg rot dargestellt.