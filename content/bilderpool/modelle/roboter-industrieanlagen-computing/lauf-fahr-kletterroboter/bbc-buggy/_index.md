---
layout: "overview"
title: "BBC Buggy"
date: 2020-02-22T08:01:54+01:00
legacy_id:
- /php/categories/3237
- /categoriesce15.html
- /categoriesae05.html
- /categories9270.html
- /categoriesf143.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3237 --> 
Hier seht Ihr eine leicht modifizierte Replik des orginalen Fischertechnik BBC Buggies der Firma Economatics von 1983. Das Modell ist in Kooperation von "allsystemgmbh" und "uffi" entstanden auf Anregung von Dirk Fox.

Die Steuerung besteht aus einem 
- Arduino Mega Board mit AVR ATMEGA2560, einem 
- 240 x 400 Pixel Farb-Display mit ILI9327 Controller, und einem 
- selbst konstruierten Linien-Sensor mit 4 Sensorelementen ITR8307 angelehnt an das Meßprinzip des Polulu-Sensors QTR-8RC. 

Wie das Original erkundet der Buggy ein Linien-Labyrinth bis zum Zielpunkt und berechnet danach den kürzesten Pfad zwischen Start und Ziel. Diesen Pfad fährt er dann vom Zielpunkt zurück bis zum Startpunkt.