---
layout: "image"
title: "BBC Buggy von unten mit Liniensensor"
date: "2016-06-08T18:14:50"
picture: "Buggy9.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Liniensensor"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43706
- /details71ac.html
imported:
- "2019"
_4images_image_id: "43706"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43706 -->
