---
layout: "image"
title: "Von hinten"
date: "2016-06-08T18:14:50"
picture: "Buggy4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Linien"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43701
- /details16e2.html
imported:
- "2019"
_4images_image_id: "43701"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43701 -->
Hier sieht man die beiden Schrittmotortreiber-Platinen mit den L298-Motortreibern