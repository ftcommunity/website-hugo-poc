---
layout: "image"
title: "Systemschalter-Platine für Stromverteilung und Ein-/Aus-Schalten"
date: "2016-06-08T18:14:50"
picture: "Buggy8.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Power-MOSFET"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43705
- /detailsa577.html
imported:
- "2019"
_4images_image_id: "43705"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43705 -->
Über einen Power-MOSFET wird das gesamte System ein- und aus-geschaltet.
Der Buggy zieht mit den Schrittmotoren und dem Display nahezu 2 Ampere an Strom. 
Wenn da direkt der FT-Motorschalter eingesetzt würde, würde sich schon dessen Widerstand bemerkbar machen. Hier muss er nun nur noch den Schaltstrom für den Power-MOSFET schalten.