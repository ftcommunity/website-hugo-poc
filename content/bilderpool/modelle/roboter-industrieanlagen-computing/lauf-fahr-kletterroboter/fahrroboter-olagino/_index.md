---
layout: "overview"
title: "Fahrroboter von Olagino"
date: 2020-02-22T08:01:53+01:00
legacy_id:
- /php/categories/3236
- /categories84db.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3236 --> 
Der gezeigte Roboter wurde für eine Präsentation in der Schule gebaut und programmiert - wobei der Fokus auf der Programmierung lag. Das Fahrwerk basiert dabei teilweise aus dem Fischertechnik TXT Discovery Set. Der Roboter in Aktion und die dazugehörigen Programme sind unter https://youtu.be/-g0o633Fjts zu sehen.