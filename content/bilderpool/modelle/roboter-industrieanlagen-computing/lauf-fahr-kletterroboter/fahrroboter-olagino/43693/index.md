---
layout: "image"
title: "Schräg oben"
date: "2016-06-06T17:29:03"
picture: "fahrroboter1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/43693
- /detailscae8.html
imported:
- "2019"
_4images_image_id: "43693"
_4images_cat_id: "3236"
_4images_user_id: "2042"
_4images_image_date: "2016-06-06T17:29:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43693 -->
Auf diesem Bild ist der gesamte Roboter gut zu erkennen, wie auch die Kabelführung der Kamera und der Motoren bzw. LED's.