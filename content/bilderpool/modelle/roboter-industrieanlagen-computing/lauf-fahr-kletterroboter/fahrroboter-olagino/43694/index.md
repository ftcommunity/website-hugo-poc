---
layout: "image"
title: "Verkabelung"
date: "2016-06-06T17:29:03"
picture: "fahrroboter2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/43694
- /detailsed77.html
imported:
- "2019"
_4images_image_id: "43694"
_4images_cat_id: "3236"
_4images_user_id: "2042"
_4images_image_date: "2016-06-06T17:29:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43694 -->
Obwohl die Verkabelung hier nicht ganz so schön geworden ist, ist dennoch zu sehen, dass der Roboter außer der Kamera keine Sensoren besitzt, was für das Schulprojekt völlig ausreichend war.