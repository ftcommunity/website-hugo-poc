---
layout: "image"
title: "Schutzblech"
date: "2007-04-04T10:29:45"
picture: "blinderroboter09.jpg"
weight: "16"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9941
- /details6116.html
imported:
- "2019"
_4images_image_id: "9941"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9941 -->
