---
layout: "image"
title: "Schutzblechhalterung"
date: "2007-04-04T10:29:45"
picture: "blinderroboter08.jpg"
weight: "15"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9940
- /details730d-2.html
imported:
- "2019"
_4images_image_id: "9940"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9940 -->
