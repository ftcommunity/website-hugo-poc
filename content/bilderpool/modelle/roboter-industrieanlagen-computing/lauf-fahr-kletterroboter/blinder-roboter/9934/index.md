---
layout: "image"
title: "Taster für Abgrunderkennung"
date: "2007-04-04T10:29:45"
picture: "blinderroboter02.jpg"
weight: "9"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9934
- /details0891.html
imported:
- "2019"
_4images_image_id: "9934"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9934 -->
Es geht etwa so wie die Kantenerkennung bei Mobile Robots Set. Es ist eine V-Achse, die den Taster drückt, wenn es Boden hat.