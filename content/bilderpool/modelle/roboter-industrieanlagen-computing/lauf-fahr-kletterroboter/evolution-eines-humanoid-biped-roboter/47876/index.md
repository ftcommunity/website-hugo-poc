---
layout: "image"
title: "2 Beine fertig"
date: "2018-09-16T18:16:17"
picture: "2Beine.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47876
- /details856a-2.html
imported:
- "2019"
_4images_image_id: "47876"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-16T18:16:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47876 -->
es geht langsam vorwärts....ist aber echt viel Arbeit!

hab ein paar Details zu den Beinen überarbeitet, außerdem endlich auch das 2. aufgebaut.
unterhalb der Hüfte ist alles komplett:
- Alus für die Stabilität
- 6 Motoren (Sprunggelenk, Knie, Hüfte / je Seite)
- an jedem Gelenk ein Potentiometer
- alles verkabelt

der Antrieb für die Hüfte bzw. das Hin- und Herbewegen für den Oberkörper fehlt noch.

Meine Idee war ja, dass ich mit einem Gegengewicht im Oberkörper es schaffe, jeweils ein Bein zu heben. Ich habe versucht, im Bild mal einen Schritt darzustellen. Ein Bein ist angehoben, der Oberkörper in die Gegenrichtung geneigt. Der Akkuhalter oben stellt das Gegengewicht dar (wiegt allerdings viel zu wenig)

Ich bin inzwischen sehr skeptisch, ob das klappt. Ein Bein wiegt ca. 800g; ich müsste wahrscheinlich mind. 500g Oberkörper rechts/links hin-- und herwuchten....ob das funktioniert????
Vielleicht muss ich doch noch mehr Aufwand reinstecken und mehr Freiheitsgrade in den Beinen einbauen.
mal sehen....