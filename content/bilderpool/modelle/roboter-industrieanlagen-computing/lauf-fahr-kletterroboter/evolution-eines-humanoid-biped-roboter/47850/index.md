---
layout: "image"
title: "Winkelmesser"
date: "2018-09-01T20:48:11"
picture: "Absolutwertgeber_2.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47850
- /detailsdfb8-2.html
imported:
- "2019"
_4images_image_id: "47850"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-01T20:48:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47850 -->
das Poti hat einen Winkelbereich von 270 Grad, also ausreichend für die 180 Grad des Gelenksteines.
Hier sieht man auch gut die einfache Befestigung per Lampenfuß. An den mittleren Kontakt des Potis habe ich eine Hülse für die Stecker angelötet