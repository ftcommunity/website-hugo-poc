---
layout: "image"
title: "Unterschenkel komplett Stellung B"
date: "2018-09-03T16:06:26"
picture: "u_schenkel_B.jpg"
weight: "9"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/47854
- /details8de8.html
imported:
- "2019"
_4images_image_id: "47854"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-03T16:06:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47854 -->
und hier in der hintersten Stellung.
mit dem gesamten Verfahrbereich bin ich noch nicht ganz zufrieden, aber das gibt die Mechanik momentan her.Für's Gehen wahrscheinlich ausreichend.
Alle weiteren Ideen zur Optimierung würden den Modding- bzw. Fremdteile-Anteil erhöhen