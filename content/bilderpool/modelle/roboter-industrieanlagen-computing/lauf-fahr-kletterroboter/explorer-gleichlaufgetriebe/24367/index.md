---
layout: "image"
title: "Vorne"
date: "2009-06-14T19:34:55"
picture: "explorer7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24367
- /detailsbebf-3.html
imported:
- "2019"
_4images_image_id: "24367"
_4images_cat_id: "1669"
_4images_user_id: "920"
_4images_image_date: "2009-06-14T19:34:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24367 -->
Abstandssensor und Lichtsucher.