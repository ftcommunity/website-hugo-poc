---
layout: "image"
title: "Gesamtansicht"
date: "2009-06-14T19:34:55"
picture: "explorer1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24361
- /detailscf6b.html
imported:
- "2019"
_4images_image_id: "24361"
_4images_cat_id: "1669"
_4images_user_id: "920"
_4images_image_date: "2009-06-14T19:34:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24361 -->
Ein Explorer mit Farberkennung, Spurensucher, Abstandssensor, Lichtsucher, Helligkeitssensor, Summer, Scheinwerfern, Gleichlaufgetriebe.