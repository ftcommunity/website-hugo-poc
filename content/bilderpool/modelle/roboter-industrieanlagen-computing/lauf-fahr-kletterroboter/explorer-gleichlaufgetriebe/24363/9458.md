---
layout: "comment"
hidden: true
title: "9458"
date: "2009-06-20T08:56:31"
uploadBy:
- "Mr Smith"
license: "unknown"
imported:
- "2019"
---
Der Farbsensor strahlt ja auch Licht ab, dass den Spursensor stören könnte, also musste er etwas Abstand zum Spursensor haben. Hätte ichs hintereinander gemacht wäre das Fahrzeug länger geworden. Außerdem kann man so den Explorer gleichzeitig auf einer Linie fahren und Farben erkennen lassen, währen beide Sensoren hintereinander wäre das nicht möglich.