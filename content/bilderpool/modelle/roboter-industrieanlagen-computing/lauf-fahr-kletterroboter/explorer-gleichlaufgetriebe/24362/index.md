---
layout: "image"
title: "Seite"
date: "2009-06-14T19:34:55"
picture: "explorer2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24362
- /detailse2a2-2.html
imported:
- "2019"
_4images_image_id: "24362"
_4images_cat_id: "1669"
_4images_user_id: "920"
_4images_image_date: "2009-06-14T19:34:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24362 -->
Die 3 Lampen zeigen die vom Farbsensor erkannte Farbe an