---
layout: "image"
title: "180mm-FT-Kugel"
date: "2009-06-23T07:42:22"
picture: "180mm-Kugel_002.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24431
- /details989b-3.html
imported:
- "2019"
_4images_image_id: "24431"
_4images_cat_id: "1675"
_4images_user_id: "22"
_4images_image_date: "2009-06-23T07:42:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24431 -->
160mm-FT-Kugel ist gewachsen: 
ist jetzt 180mm, hat mehr Energie und bewegt jetzt besser !
