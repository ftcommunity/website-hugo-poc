---
layout: "image"
title: "Zini - fern"
date: "2009-06-16T17:17:03"
picture: "IMG_2100_1.jpg"
weight: "10"
konstrukteure: 
- "Fitzcarraldo"
fotografen:
- "Fitzcarraldo"
schlagworte: ["Kugel", "Zini"]
uploadBy: "Fitzcarraldo"
license: "unknown"
legacy_id:
- /php/details/24394
- /details92d2.html
imported:
- "2019"
_4images_image_id: "24394"
_4images_cat_id: "1500"
_4images_user_id: "971"
_4images_image_date: "2009-06-16T17:17:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24394 -->
Eine Fernaufnahme von Zini.