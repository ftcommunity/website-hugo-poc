---
layout: "image"
title: "von unten"
date: "2008-12-10T16:45:53"
picture: "kugelroboter2.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/16575
- /details8771.html
imported:
- "2019"
_4images_image_id: "16575"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2008-12-10T16:45:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16575 -->
Hier sieht man sehr gut die Räder. Sie müssen schräg angebaut sein, um auf der Kugel zu fahren. Er kann auch auf geradem Boden fahren, aber dann kommen nur die äußeren Radteile auf den Boden und der Roboter fährt etwas holperig.
