---
layout: "image"
title: "Winkelmessung mit Motor"
date: "2009-02-18T21:52:04"
picture: "winkelmessung2.jpg"
weight: "6"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/17434
- /detailsf433.html
imported:
- "2019"
_4images_image_id: "17434"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-02-18T21:52:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17434 -->
Und hier derselbe Versuch bei laufenden Motoren. Auffällig ist der gelbe Gyro: seine Werte schwanken deutlich stärker. Das liegt daran das ich ein Pad verbrutzelt habe, wo eigentlich ein Filter-Kondensater dran gehört. 
Allerdings müsste der Roboter immer noch gut den Winkel berechnen können, z.B. indem er einen Mittelwert aus den Beschleunigungssensorwerten bildet. Aber weil die Beschleunigungssensoren beim Fahren auch andere Beschleunigungen messen geht das leider nicht mehr.
