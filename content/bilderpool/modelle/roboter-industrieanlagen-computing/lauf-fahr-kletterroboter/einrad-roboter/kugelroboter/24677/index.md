---
layout: "image"
title: "Kugelroboter - Fernsteuerung (offen)"
date: "2009-07-24T17:57:05"
picture: "fernsteuerung2.jpg"
weight: "18"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/24677
- /detailsbf48-2.html
imported:
- "2019"
_4images_image_id: "24677"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-24T17:57:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24677 -->
Die Elektronik der Fersteuerung. Als Prozessor benutze ich einen Mega32. Das ist übrigens schon der zweite. Der erste redet nicht mehr mit mir, seit ich ihn eingelötet habe (wahrscheinlich überhitzt). 
Ganz oben die beiden Schnittstellen: ISP und RS232. Der Pegelwandler (Max232) mit den Kondensatoren befindet sich links. Er erzeugt übrigens auch die Kontrastspannung für das Display. Angeblich gibt das Display an einem Pin -10V aus. Es sind leider nur -5, sodass man nichts erkennen kann. Die -8,x des Max232 reichen gerade so.
Unter der RS232-Schnittstelle befindet sich das Funkmodul. Rechts neben dem Mega32 der Beschleunigungssensor um die Neigung zu messen. Weil er die Pads UNTER dem Gehäuse hat, konnte ich ihn nicht normal löten, deshalb schwebt er an Drähten etwas über der Platine. Funktioniert aber super.
