---
layout: "image"
title: "Zini - rechts"
date: "2009-06-16T17:17:04"
picture: "IMG_2095_1.jpg"
weight: "11"
konstrukteure: 
- "Fitzcarraldo"
fotografen:
- "Fitzcarraldo"
schlagworte: ["Kugel", "Zini"]
uploadBy: "Fitzcarraldo"
license: "unknown"
legacy_id:
- /php/details/24395
- /detailsc764.html
imported:
- "2019"
_4images_image_id: "24395"
_4images_cat_id: "1500"
_4images_user_id: "971"
_4images_image_date: "2009-06-16T17:17:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24395 -->
Zini's rechte Seite