---
layout: "comment"
hidden: true
title: "9435"
date: "2009-06-16T20:51:31"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Bleiplatten?:o)
Fitzcarraldo, mein Glückwunsch zu deiner interessanten Lösung.
Mit dem Körperschwerpunkt von Fernsteuerung und Antrieb bist du aber sehr hoch gekommen, weil diese Kugel hierfür zu klein ist? Vier? Stützrollen ergeben übrigens ein überbestimmtes System, wobei im Gegensatz zu drei nicht alle Rollen gleichmäßig an der Innenwandung laufen.
Sollte ich einer solchen Kugel mal habhaft werden, baue ich sowas später auch mal.
Gruß Udo2