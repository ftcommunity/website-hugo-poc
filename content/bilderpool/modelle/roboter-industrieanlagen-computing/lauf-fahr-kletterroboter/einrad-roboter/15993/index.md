---
layout: "image"
title: "Seitenansicht"
date: "2008-10-16T00:46:14"
picture: "einradroboter2.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15993
- /details5e53.html
imported:
- "2019"
_4images_image_id: "15993"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15993 -->
Der Powermoter im Inneren dreht zusammen mit einem anderen das Gewicht.
