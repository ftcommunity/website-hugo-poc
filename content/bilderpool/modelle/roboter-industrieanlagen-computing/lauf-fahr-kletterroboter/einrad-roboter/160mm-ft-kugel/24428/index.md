---
layout: "image"
title: "160mm-FT-Kugel"
date: "2009-06-21T11:29:38"
picture: "2009-juni-Zaltbommel_009_2.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24428
- /detailsc35a.html
imported:
- "2019"
_4images_image_id: "24428"
_4images_cat_id: "1674"
_4images_user_id: "22"
_4images_image_date: "2009-06-21T11:29:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24428 -->
