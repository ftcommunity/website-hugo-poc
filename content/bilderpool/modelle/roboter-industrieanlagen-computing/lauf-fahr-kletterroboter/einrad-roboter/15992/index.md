---
layout: "image"
title: "Gesamtansicht"
date: "2008-10-16T00:46:13"
picture: "einradroboter1.jpg"
weight: "1"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15992
- /detailsfaa7.html
imported:
- "2019"
_4images_image_id: "15992"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15992 -->
Der Roboter soll auf einem Rad balancieren: Mit dem Rad gleicht er eine Neigung nach vorne/hinten ausg, mit dem drehbaren Gewicht(hier kaum zu sehen) eine Neigung nach links/rechts. Allerdings ist das Gewicht viel zu leicht, sodass er immer nach links oder rechts umkippt. Er kann also nicht wirklich balancieren.
