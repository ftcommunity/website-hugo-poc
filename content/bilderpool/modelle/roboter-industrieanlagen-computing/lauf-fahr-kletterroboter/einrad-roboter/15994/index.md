---
layout: "image"
title: "Von hinten"
date: "2008-10-16T00:46:14"
picture: "einradroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15994
- /detailse4ff.html
imported:
- "2019"
_4images_image_id: "15994"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15994 -->
Der Roboter misst seinen Winkel nicht mehr mit Abstandssensoren, sondern mit einem Beschleunigungssensor.
