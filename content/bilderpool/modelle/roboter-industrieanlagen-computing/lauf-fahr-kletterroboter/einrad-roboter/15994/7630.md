---
layout: "comment"
hidden: true
title: "7630"
date: "2008-10-19T14:30:41"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Video gibts von dem nicht. Er kann ja gar nicht balancieren, weil das Gewicht zu leicht ist. Vorwärts und rückwärts geht eigentlich, aber weil er immer zu Seite umkippt sieht man davon nichts. Ich habe ihn jetzt wieder auseinander genommen und starte einen neuen Versuch mit einem anderen Gewicht. 
Gesteuert wird er über zwei PID-Regler:
Einen für links/rechts einen für vorwärts/rückwärts. Beide haben als Eingangsgröße die Abweichung von der Senkrechten, der vorwärts/rückwärts-Regler gibt die Geschwindigkeit der Motoren für das Rad aus, der andere die Beschleunigung der Motoren für das Gewicht.

Ich habe jetzt aber ein [Video](http://de.youtube.com/watch?v=bXGDs1l0B5Q) vom Zweirad-Roboter gemacht, der hat das gleiche Prinzip, aber eben nur zwei Richtungen in die er umkippen kann.