---
layout: "image"
title: "Ohne Elektronik"
date: "2008-10-16T00:46:14"
picture: "einradroboter4.jpg"
weight: "4"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15995
- /details1723.html
imported:
- "2019"
_4images_image_id: "15995"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15995 -->
Hier kann man sehr gut das Gewicht sehen. Die Powermotoren sind gleichzeitig Antrieb und Gewicht, sie bekommen Strom über den Schleifring.
