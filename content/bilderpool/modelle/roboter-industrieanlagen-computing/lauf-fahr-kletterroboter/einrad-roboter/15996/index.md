---
layout: "image"
title: "Gewicht"
date: "2008-10-16T00:46:14"
picture: "einradroboter5.jpg"
weight: "5"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/15996
- /detailsec7c.html
imported:
- "2019"
_4images_image_id: "15996"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15996 -->
Es ist leider viel zu leicht um den Roboter zu beeinflussen.
