---
layout: "comment"
hidden: true
title: "14325"
date: "2011-05-28T12:24:07"
uploadBy:
- "Martin S"
license: "unknown"
imported:
- "2019"
---
Die Wii hat damit gar nichts zu tun.
Die Wii Fernbedienung wird über Bluetooth mit meinem Laptop verbunden, auf dem ein kleines VB Programm läuft das auf das TX und die Fernbedienung zugreifen kann.
Das hat zwar eine sehr schlechte Übertragungsrate aber für manche Modelle kann man diese Art gut verwenden.
Somit könnte man auch das RoboInt oder ältere Controller ansteuern.
Alle Eingabegeräte der Wii funktionieren über Bluetooth.
Ich hab mal vor n paar Monaten ein Programm geschrieben mit dem man einen Explorer über das Wii Balance Board steuern konnte.
Mfg Martin