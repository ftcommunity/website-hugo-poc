---
layout: "image"
title: "robomax20.jpg"
date: "2005-07-11T09:52:34"
picture: "img_4488_resize.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4514
- /detailsc3b9.html
imported:
- "2019"
_4images_image_id: "4514"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4514 -->
Noch eine Variante mit gekürztem Z10 direkt am U-Getriebe, Ansteuerung und Spurstange jetzt ordentlich getrennt. Baut aber zu tief vom ALU-Träger weg :(