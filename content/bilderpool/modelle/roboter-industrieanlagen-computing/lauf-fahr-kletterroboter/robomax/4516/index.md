---
layout: "image"
title: "robomax22.jpg"
date: "2005-07-11T09:52:34"
picture: "img_4493_resize.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4516
- /detailscf0e.html
imported:
- "2019"
_4images_image_id: "4516"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4516 -->
Antriebsseite mit PM 50 und Untersetzung. Rutscht leider noch seitlich weg wenn die zu übertragende Antriebskraft zu groß ist (Teppichkante!) Die Sensorik zur Erfassung der Umdrehungen fehlt auch noch. Impulsrad scheidet aber aus!