---
layout: "image"
title: "robomax16.jpg"
date: "2005-07-11T09:52:25"
picture: "img_3897_resize.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4510
- /details03b1.html
imported:
- "2019"
_4images_image_id: "4510"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4510 -->
Neue Variante mit Gelenksteinen, Lenkung leider noch ohne Antrieb.