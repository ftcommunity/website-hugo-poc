---
layout: "image"
title: "robomax05.jpg"
date: "2004-11-23T19:48:21"
picture: "img_3664_rotate_resize.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3315
- /detailsfd7f.html
imported:
- "2019"
_4images_image_id: "3315"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3315 -->
Endlich mit 3 Beinen!