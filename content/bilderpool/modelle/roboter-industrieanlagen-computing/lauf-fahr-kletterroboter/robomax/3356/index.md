---
layout: "image"
title: "robomax09.jpg"
date: "2004-11-24T13:31:48"
picture: "img_3702_resize.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/3356
- /details3908-2.html
imported:
- "2019"
_4images_image_id: "3356"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-24T13:31:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3356 -->
Zahnräder sind noch genügend vorhanden, Metallachsen nicht :(