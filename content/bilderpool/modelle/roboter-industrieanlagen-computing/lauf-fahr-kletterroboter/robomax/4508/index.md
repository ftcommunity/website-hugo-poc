---
layout: "image"
title: "robomax14.jpg"
date: "2005-07-11T09:52:25"
picture: "img_4480_rotate_resize.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/4508
- /details14fc.html
imported:
- "2019"
_4images_image_id: "4508"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4508 -->
Seitenansicht im Überblick, 1. und 3. Achse werden angetriebn und lenkbar. Achse 2 wird nur als Stütze gebraucht, wenn eine der anderen Achsen hochgezogen bzw. abgesenkt wurde um auf die nächste Stufe zu kommen. Vom Hintergrund aus wird der ganze Bau argwönisch beobachtet :)