---
layout: "image"
title: "H-Brücken mit AVR Controller"
date: 2024-10-19T18:06:50+02:00
picture: "motorctrl.jpg"
weight: "7"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Über den Encodermotoren sind die H-Brücken zur Ansteuerung der Motoren mit AVR für deren Regelung