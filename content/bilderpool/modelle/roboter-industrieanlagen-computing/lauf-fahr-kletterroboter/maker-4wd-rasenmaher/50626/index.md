---
layout: "image"
title: "Frontansicht"
date: 2024-10-19T18:06:58+02:00
picture: "front.jpg"
weight: "1"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Der 360° Laserscanner hat die Hinternisse im Blick und ist per USB an den Computer angeschlossen. Hier mit montierten Mähmessern,  die nicht ohne Aufsicht betrieben werden dürfen.