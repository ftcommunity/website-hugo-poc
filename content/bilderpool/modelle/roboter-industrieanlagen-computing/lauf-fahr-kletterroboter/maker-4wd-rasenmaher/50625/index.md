---
layout: "image"
title: "Linke Seite"
date: 2024-10-19T18:06:57+02:00
picture: "left.jpg"
weight: "2"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Linke Seite: W-Lan Antenne, 5V-Regler und 5V-Sicherung. Klebeband. Doppelseitiges Klebeband ist zum Glück im ft-Sortiment enthalten.