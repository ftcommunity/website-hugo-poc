---
layout: "image"
title: "Einplatinencomputer"
date: 2024-10-19T18:06:51+02:00
picture: "computer.jpg"
weight: "6"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Blick in den Maschinenraum: Der Quartz 64 Einplatinencomputer