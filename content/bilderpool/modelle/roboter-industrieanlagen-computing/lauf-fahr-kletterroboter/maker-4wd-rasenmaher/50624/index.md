---
layout: "image"
title: "Rechte Seite"
date: 2024-10-19T18:06:55+02:00
picture: "right.jpg"
weight: "3"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Die großen Speichenräder sorgen für ausreichende Bodenfreiheit