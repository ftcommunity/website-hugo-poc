---
layout: "overview"
title: "Maker 4wd Rasenmäher"
date: 2024-10-19T18:06:50+02:00
---

# Maker Kit 4wd als Rasenmäher

* Ansteuerung der Motoren erfolgt mittels TLE5205 H-Brücke, die Geschwindigkeit der vier Motoren wird durch je einen PID-Controller gesteuert.
* Durch die auf etwa einen Zentimeter genaue Positionsbestimmung des RTK-GPS-Modul Zed-F9P wird kein Begrenzungskabel benötigt.
* Erkennung der Orientierung mittels BNO055 Inertial Measurement Unit (IMU).
* Hinderniserkennung durch Rplidar A2 360° Laserscanner
* Mähmotor: Bürstenloser Außenläufer für Quadrocopter
* Rechenleistung wird durch einen Pine64 Quartz64 Model B (4x Cortex A55) geliefert.
* Software: Robot Operating System (ROS2)
* Batterie: 7x Babyzelle Akkus = 8,4V
