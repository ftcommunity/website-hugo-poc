---
layout: "image"
title: "Heckansicht"
date: 2024-10-19T18:06:54+02:00
picture: "aft.jpg"
weight: "4"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Heckansicht, der schwarze Kasten enthält die Sicherung für die Motoren