---
layout: "image"
title: "Ansicht von Unten"
date: 2024-10-19T18:06:53+02:00
picture: "bottom.jpg"
weight: "5"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Von Unten, ohne die drei gefährlichen Messer auf der gedruckten Messerhalterung. Eine Reinigung ist fällig.