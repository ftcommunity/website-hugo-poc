---
layout: "image"
title: "Draufsicht"
date: 2024-10-19T18:06:59+02:00
picture: "top.jpg"
weight: "0"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Website-Team"
license: "unknown"
---

Von oben zu sehen: Laserscanner, GPS-Modul, IMU und Antennen