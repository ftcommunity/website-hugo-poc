---
layout: "image"
title: "Seitenblick"
date: "2008-11-12T21:53:43"
picture: "autonomerkleinroboter1.jpg"
weight: "1"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16258
- /detailsd169.html
imported:
- "2019"
_4images_image_id: "16258"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16258 -->
Ich wollte den Roboter möglichst klein halten - oder, besser ausgedrückt, den zum Manövrieren notwendigen Raum wollte ich möglichst klein halten, aber auch nicht zu sehr in die Höhe bauen.