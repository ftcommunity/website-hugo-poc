---
layout: "image"
title: "Gleichlaufgetriebe mit Motoren"
date: "2013-03-04T14:09:17"
picture: "spurverfolgermitgleichlaufgetriebe3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36711
- /detailsaab9.html
imported:
- "2019"
_4images_image_id: "36711"
_4images_cat_id: "2722"
_4images_user_id: "1126"
_4images_image_date: "2013-03-04T14:09:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36711 -->
Der Motorenanbau gelingt leicht - auch ohne TSTs Schneckenaufsatz lassen sich Power-Motoren mit Rastachse und Schnecke nutzen.
Beide Motoren werden von den Seiten "aufgeschoben".