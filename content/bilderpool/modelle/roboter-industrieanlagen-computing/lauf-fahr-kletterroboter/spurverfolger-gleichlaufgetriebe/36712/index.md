---
layout: "image"
title: "Kompletter Spurverfolger"
date: "2013-03-04T14:09:17"
picture: "spurverfolgermitgleichlaufgetriebe4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36712
- /detailsaf16.html
imported:
- "2019"
_4images_image_id: "36712"
_4images_cat_id: "2722"
_4images_user_id: "1126"
_4images_image_date: "2013-03-04T14:09:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36712 -->
Auf das Chassis wird der TX gesteckt. Als Stromversorgung empfiehlt sich wegen des geringeren Gewichts ein 9V-Batteriegehäuse statt des fischertechnik-Akkus.
(Sobald die Robo-Pro-Software freigeschaltet ist, füge ich einen Link auf das Programm ein.)