---
layout: "image"
title: "Anbau von M-Motoren"
date: "2014-03-16T12:02:06"
picture: "S1050595.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38457
- /detailsd9f4.html
imported:
- "2019"
_4images_image_id: "38457"
_4images_cat_id: "2722"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T12:02:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38457 -->
Hab hier mal eine Variante mit M-Motoren nachgebaut. Davon hab ich noch mehr rumliegen, als von den Power-Motoren. Das Getriebe ist wirklich sehr leichtgängig. Bei 8V aus einem LiPo-Akku flitzt das Ding wie irre los.