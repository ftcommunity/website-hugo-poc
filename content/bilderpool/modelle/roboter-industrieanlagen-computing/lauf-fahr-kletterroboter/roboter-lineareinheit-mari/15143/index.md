---
layout: "image"
title: "Vogelperspektive"
date: "2008-08-30T13:50:53"
picture: "Schienen-Magnet-Roboter_3.jpg"
weight: "2"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15143
- /detailscdd5.html
imported:
- "2019"
_4images_image_id: "15143"
_4images_cat_id: "1387"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15143 -->
