---
layout: "image"
title: "Gesamtansicht"
date: "2008-08-30T13:50:57"
picture: "Schienen-Magnet-Roboter_12.jpg"
weight: "5"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15146
- /details0813.html
imported:
- "2019"
_4images_image_id: "15146"
_4images_cat_id: "1387"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15146 -->
