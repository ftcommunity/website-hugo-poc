---
layout: "image"
title: "Wagen auf Lineareinheit"
date: "2008-08-30T13:50:53"
picture: "Schienen-Magnet-Roboter_7.jpg"
weight: "4"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/15145
- /details3e89-3.html
imported:
- "2019"
_4images_image_id: "15145"
_4images_cat_id: "1387"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15145 -->
