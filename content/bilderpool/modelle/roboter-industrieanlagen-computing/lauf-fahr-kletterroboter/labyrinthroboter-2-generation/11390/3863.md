---
layout: "comment"
hidden: true
title: "3863"
date: "2007-08-19T04:29:49"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Donnerwetter. Am Ende berechnest du mir noch die Ideallinie durchs Labyrinth ...

Wir haben uns damals darauf konzentriert, dass wir den Wänden nicht zu nah kamen, und dass wir wussten, auf welchem Feld wir sind. Das hat fast funktioniert. Es kam dann nur darauf an, dass der Fehler in der Odometrie nicht so groß wurde, dass wir uns um mehr als 15cm verschätzen ... den Abstand nach links und rechts haben wir mit etwas kontrolliert, das wir heute wohl Regelkreis nennen würden. Der zusätzliche Weg, den wir durch das links-rechs-Korrigieren gefahren sind, hat sich fast nie auf 15cm aufsummiert. Ein größeres Problem war das Getriebespiel, weil ich mich stur auf das Gleichlaufgetriebe eingeschossen hatte. 

Das Verfahren war rudimentär, aber prinzipiell funktionsfähig. Leider konnte man nicht von Zuverlässigkeit sprechen. Dafür fehlte die Erfahrung, das Hintergrundwissen und schließlich die Feinjustage.

Aber zumindest softwareseitig gibt es, bei allem Respekt, einfachere Lösungen. Der Wettbewerbssieger hatte einen 7.4MHz-M68HC11 mit einem Interpreter zur Verfügung.