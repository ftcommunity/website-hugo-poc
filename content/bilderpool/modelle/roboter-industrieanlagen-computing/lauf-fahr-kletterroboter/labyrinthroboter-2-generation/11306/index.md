---
layout: "image"
title: "Lab2-09"
date: "2007-08-08T20:06:33"
picture: "RoboterimEck.jpg"
weight: "9"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11306
- /detailsd5d2.html
imported:
- "2019"
_4images_image_id: "11306"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-08-08T20:06:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11306 -->
Hier steht der Roboter in einer Ecke und "schaut" sich um. Dabei zeichnet der Entfernungssensor während einer vollen Umdrehung des "Radars" die Entfernungen auf.

Die Entfernung nach vorne ist 29 cm und nach links 16 cm. Im nächsten Bild ist das Ergebnis zu sehen.
