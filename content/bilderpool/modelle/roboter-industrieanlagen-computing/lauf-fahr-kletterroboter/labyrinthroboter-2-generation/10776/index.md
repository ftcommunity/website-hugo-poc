---
layout: "image"
title: "Lab2-07"
date: "2007-06-09T20:47:34"
picture: "Lab2-07.jpg"
weight: "7"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/10776
- /detailsad94-2.html
imported:
- "2019"
_4images_image_id: "10776"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10776 -->
Das ganze Teil von links. Hier jetzt zu sehen, daß der Entfernungssensor mit 7,5 Grad nach unten geneigt ist.
