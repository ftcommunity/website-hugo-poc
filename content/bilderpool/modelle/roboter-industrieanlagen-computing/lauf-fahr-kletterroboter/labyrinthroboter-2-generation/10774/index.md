---
layout: "image"
title: "Lab2-05"
date: "2007-06-09T20:47:34"
picture: "Lab2-05.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/10774
- /details264c.html
imported:
- "2019"
_4images_image_id: "10774"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10774 -->
Jetzt wird es eng: Antrieb und Lenkung sind notwendiger Luxus. Das muß klein sein. Ergo quetscht man einen Getriebemotor unter einen  Schleifring und wählt das kleinstmögliche Antriebsrad.

Am Schleifring darf ich die Aufmerksamkeit auf die federvorgespannten, blanken Litzen lenken. Diese Kontaktgabe ist superzuverlässig.
