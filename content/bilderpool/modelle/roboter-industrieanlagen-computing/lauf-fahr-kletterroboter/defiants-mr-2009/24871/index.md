---
layout: "image"
title: "impulsedecoder.jpg"
date: "2009-08-30T18:33:41"
picture: "impulsedecoder.jpg"
weight: "2"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/24871
- /details8d3b.html
imported:
- "2019"
_4images_image_id: "24871"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-08-30T18:33:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24871 -->
Die Regelung für die Motoren soll über diese Gabellichtschranken erfolgen.
Leider funktionieren diese nicht so sauber wie gedacht weswegen ich derzeit auf der Suche nach Alternativen bin.
Schade, die konnte man so schön an ft anbauen.