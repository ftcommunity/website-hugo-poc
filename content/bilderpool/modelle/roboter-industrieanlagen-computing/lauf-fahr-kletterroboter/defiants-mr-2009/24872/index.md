---
layout: "image"
title: "stromversorgung.jpg"
date: "2009-08-30T18:33:42"
picture: "stromversorgung.jpg"
weight: "3"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/24872
- /details7d50-2.html
imported:
- "2019"
_4images_image_id: "24872"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-08-30T18:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24872 -->
Energieversorgung.