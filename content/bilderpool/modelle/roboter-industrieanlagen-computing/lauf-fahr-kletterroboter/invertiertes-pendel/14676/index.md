---
layout: "image"
title: "Plattform"
date: "2008-06-14T13:25:32"
picture: "invertiertespendel2.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/14676
- /detailsd0c5-3.html
imported:
- "2019"
_4images_image_id: "14676"
_4images_cat_id: "1347"
_4images_user_id: "521"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14676 -->
Hier sieht man die Plattform, auf der der Stab befestigt ist. Der Winkel des Stabes wird mit einem Poti gemessen.
Unter der Plattform ist noch der Sensor zur Wegmessung angebracht. Er besteht aus einem CNY37 und einem Rad aus einer alten Maus.
