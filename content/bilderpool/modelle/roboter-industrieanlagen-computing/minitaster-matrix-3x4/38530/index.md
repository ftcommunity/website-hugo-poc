---
layout: "image"
title: "von Unten"
date: "2014-04-04T22:26:58"
picture: "IMG_0024.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Tastaturmatrix", "Minitaster", "Arduino", "Matrix"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38530
- /detailsc83f.html
imported:
- "2019"
_4images_image_id: "38530"
_4images_cat_id: "2876"
_4images_user_id: "1359"
_4images_image_date: "2014-04-04T22:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38530 -->
jede Spalte ist in Gelb durchverkabelt
jede Zeile ist in Grün durchverkabelt
das Schliessen eines Tasters verbindet die jew. Spalte mit der jew. Zeile.
Auswertung später per Arduino
