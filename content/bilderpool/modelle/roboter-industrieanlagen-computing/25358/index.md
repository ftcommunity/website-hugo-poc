---
layout: "image"
title: "Walking Robot"
date: "2009-09-26T00:18:19"
picture: "sm_walker_1.jpg"
weight: "10"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["walker", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25358
- /details658d.html
imported:
- "2019"
_4images_image_id: "25358"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-26T00:18:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25358 -->
We have been working on a series of walking robots with the PCS BRAIN.