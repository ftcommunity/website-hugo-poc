---
layout: "overview"
title: Arduino-Roboter
date: 2020-08-17T13:08:46+02:00
---
Für Modelle passend zum Buch [„fischertechnik-Roboter mit Arduino“](https://fischertechnik-roboter-mit-arduino.de/), 
aber auch für alle anderen Modelle, die mit einem Arduino gesteuert werden.


