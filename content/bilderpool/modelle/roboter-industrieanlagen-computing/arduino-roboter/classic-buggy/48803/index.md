---
layout: "image"
title: "Seitenansicht"
date: 2020-08-17T14:40:52+02:00
picture: "P1000915.JPG"
weight: "5"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

In der Seitenansicht ohne Räder erkennt man auch, wie der Arduino mit dem Chassis verbunden ist: mit gelben Klebepads an einer Bauplatte. Die Jumperkabel sind selbst gebaut: aus einer dünnen Büroklammer, Kabelendhülsen und etwas Schrumpfschlauch.