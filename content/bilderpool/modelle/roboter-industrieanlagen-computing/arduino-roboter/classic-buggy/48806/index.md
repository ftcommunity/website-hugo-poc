---
layout: "image"
title: "Bumper"
date: 2020-08-17T14:40:55+02:00
picture: "P1000911.JPG"
weight: "2"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bumper auf der Vorderseite betätigen die Taster, so dass das Signal eine Richtungsänderung auslöst.