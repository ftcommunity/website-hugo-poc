---
layout: "image"
title: "Förderband, Betatigung der Knöpfe & 'Tresor'"
date: "2011-12-18T10:29:46"
picture: "a.jpg"
weight: "2"
konstrukteure: 
- "Christopher Kepes"
fotografen:
- "Christopher Kepes"
uploadBy: "McDoofi"
license: "unknown"
legacy_id:
- /php/details/33697
- /details27dd.html
imported:
- "2019"
_4images_image_id: "33697"
_4images_cat_id: "2492"
_4images_user_id: "1401"
_4images_image_date: "2011-12-18T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33697 -->
Förderband (mitte), Betätigung der Fernbedienung (links) & 'Tresor' (rechts)