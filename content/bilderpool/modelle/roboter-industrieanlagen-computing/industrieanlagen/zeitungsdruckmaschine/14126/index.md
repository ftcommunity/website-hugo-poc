---
layout: "image"
title: "Trockenstation"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine4.jpg"
weight: "4"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- /php/details/14126
- /details130d-2.html
imported:
- "2019"
_4images_image_id: "14126"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14126 -->
