---
layout: "image"
title: "Oberlauf"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine1.jpg"
weight: "1"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- /php/details/14123
- /detailsb1fd.html
imported:
- "2019"
_4images_image_id: "14123"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14123 -->
