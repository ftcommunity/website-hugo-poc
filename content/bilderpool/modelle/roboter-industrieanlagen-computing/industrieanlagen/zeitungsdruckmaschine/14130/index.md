---
layout: "image"
title: "Schneidestation"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine8.jpg"
weight: "8"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- /php/details/14130
- /details85e2.html
imported:
- "2019"
_4images_image_id: "14130"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14130 -->
