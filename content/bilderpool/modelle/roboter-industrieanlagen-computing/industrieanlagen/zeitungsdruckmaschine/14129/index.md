---
layout: "image"
title: "von Oben"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine7.jpg"
weight: "7"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- /php/details/14129
- /detailsa6c3.html
imported:
- "2019"
_4images_image_id: "14129"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14129 -->
