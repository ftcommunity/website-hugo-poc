---
layout: "image"
title: "C-Achste"
date: "2014-08-20T14:19:12"
picture: "trl04.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39248
- /details3565.html
imported:
- "2019"
_4images_image_id: "39248"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39248 -->
Die Kabel verlaufen durch die Drehscheibe, dies ermöglicht, dass sich der Roboter mehrmals um sich selbst drehen kann. Der Motor der C-Achse ist aus Platzgründen nicht wie normal auf der Grundplatte befestigt sondern auf dem Aufbau. Ebenfalls gut zu erkennen ist der Endschalter auf dem Boden. Er kann von beiden Seiten betätigt werden.
