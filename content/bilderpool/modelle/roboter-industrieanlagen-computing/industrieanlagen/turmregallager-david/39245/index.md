---
layout: "image"
title: "Turmregallager"
date: "2014-08-20T14:19:12"
picture: "trl01.jpg"
weight: "1"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39245
- /details2a2c.html
imported:
- "2019"
_4images_image_id: "39245"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39245 -->
Hier ist eine Komplettansicht des Turmregallagers. Das Lager verfügt über 44 Lagerplätze, die auf 11 kreisförmig angeordnete Regale verteilt sind. Das 12. Regal dient zu Annahme / Abgabe von den gelben Tonnen, die in das Lager ein / ausgelagert werden können. Die kreisform ist kompakt und platzsparend.
Als Regalbedienung dient ein 3-Achs Roboter, der sich in der Mitte des runden Lagers befindet. Er kann sich theoretisch mehrmals um die eigene Achse drehen, da die Kabel durch die Drehscheibe verlegt sind. In der Praxis wird dies allerdings durch das Programm verhindert. Die kreisförmige Anordnung der Regale ermöglicht aber auch kurze Fahrwege der Regalbedienung. Diese ist so programmiert, sodass sie sich stets den kürzesten Weg zum Zielfach sucht. Da eine exakte Positionierung der Regalbedienung unausweichlich ist, habe ich für die vertikale Achse und die Drehachse Encodermotoren verwendet.
