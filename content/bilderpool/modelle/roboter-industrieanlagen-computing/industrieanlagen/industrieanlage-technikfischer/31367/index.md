---
layout: "image"
title: "Gesamt seite"
date: "2011-07-27T13:51:08"
picture: "bild01.jpg"
weight: "1"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31367
- /detailsdf95-3.html
imported:
- "2019"
_4images_image_id: "31367"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31367 -->
Hier sieht man das ganze Modell:
Hinten ist der Stapel mit den "rohen" Blöcken, die dann mit dem Förderband unter den Putz/Fräs-Arm befördert werden.
Der Arm ist bdrehbar, dazu genaueres weiter hinten.
Dann kommen sie zum Schweißen (der rote Zylinder) und werden dann mit dem Farbsensor (graue Steine nach Schweiß-Zylinder) und dem Sortierarm  richtig in die Boxen einsortiert.