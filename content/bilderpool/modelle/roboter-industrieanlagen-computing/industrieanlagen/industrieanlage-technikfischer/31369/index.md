---
layout: "image"
title: "Der Vorrat"
date: "2011-07-27T13:51:08"
picture: "bild03.jpg"
weight: "3"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31369
- /details776e-4.html
imported:
- "2019"
_4images_image_id: "31369"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31369 -->
Die Steine werden durch den Zylinder auf das Band gelegt, der Motor steuert das Ventil