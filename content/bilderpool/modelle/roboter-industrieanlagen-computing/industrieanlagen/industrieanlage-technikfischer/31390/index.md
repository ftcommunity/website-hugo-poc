---
layout: "image"
title: "P/F-Arm Drehübertragung"
date: "2011-07-27T13:51:08"
picture: "bild24.jpg"
weight: "24"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31390
- /details2af4.html
imported:
- "2019"
_4images_image_id: "31390"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31390 -->
Durch die Kette wird das ganze gedreht