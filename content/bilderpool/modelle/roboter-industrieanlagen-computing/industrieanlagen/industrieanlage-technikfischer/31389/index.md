---
layout: "image"
title: "P/F-Arm Gelenk"
date: "2011-07-27T13:51:08"
picture: "bild23.jpg"
weight: "23"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31389
- /details0fa1-2.html
imported:
- "2019"
_4images_image_id: "31389"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31389 -->
So kommt die Achse aus dem Gerät