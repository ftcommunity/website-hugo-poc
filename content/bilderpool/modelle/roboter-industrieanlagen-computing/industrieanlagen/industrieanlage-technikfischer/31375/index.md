---
layout: "image"
title: "Bandmotor, Verbindung der Bänder"
date: "2011-07-27T13:51:08"
picture: "bild09.jpg"
weight: "9"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31375
- /details3d08.html
imported:
- "2019"
_4images_image_id: "31375"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31375 -->
Hier der Powermotor, der die Förderbänder antreibt, diese sind über die Zahnräder verbunden