---
layout: "image"
title: "Das Hauptinterface"
date: "2011-07-27T13:51:08"
picture: "bild06.jpg"
weight: "6"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31372
- /detailsa996-2.html
imported:
- "2019"
_4images_image_id: "31372"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31372 -->
