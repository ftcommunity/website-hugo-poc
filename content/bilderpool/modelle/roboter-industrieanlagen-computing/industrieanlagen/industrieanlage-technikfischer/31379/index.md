---
layout: "image"
title: "Sortierboxen"
date: "2011-07-27T13:51:08"
picture: "bild13.jpg"
weight: "13"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31379
- /details3f1a.html
imported:
- "2019"
_4images_image_id: "31379"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31379 -->
Die Steine werden hier farblich sortiert