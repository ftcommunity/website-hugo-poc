---
layout: "image"
title: "Gesamt vorne"
date: "2011-07-27T13:51:08"
picture: "bild02.jpg"
weight: "2"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31368
- /details1ede-4.html
imported:
- "2019"
_4images_image_id: "31368"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31368 -->
