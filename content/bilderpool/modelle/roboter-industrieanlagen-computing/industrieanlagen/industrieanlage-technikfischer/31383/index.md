---
layout: "image"
title: "P/F-Arm Drehmotor"
date: "2011-07-27T13:51:08"
picture: "bild17.jpg"
weight: "17"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31383
- /details7a55-2.html
imported:
- "2019"
_4images_image_id: "31383"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31383 -->
Hier der Drehmotor von unten, die Stange rechts unten ist die Lagerstange für die Hoch/Runter-Bewegung