---
layout: "image"
title: "Etikettiermaschine"
date: "2012-01-12T16:52:51"
picture: "etikettirmaschine08.jpg"
weight: "8"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33892
- /detailsbdad-2.html
imported:
- "2019"
_4images_image_id: "33892"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33892 -->
Ansicht von hinten