---
layout: "image"
title: "Springfedern der Zeitglieder"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35615
- /detailsf9e3.html
imported:
- "2019"
_4images_image_id: "35615"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35615 -->
Die Zeitglieder laufen ja nur ganz langsam. Wenn man ein ft-Pneumatikventil aber nur langsam betätigt, sind bis zum Durchschalten all drei Wege der Ventile miteinander verbunden: Druckluftzufuhr, Ausgang - und der Abluftausgang ins Freie. Dabei entweicht die Druckluft einfach ins Freie, der Luftdruck in der Anlage sinkt auf Null oder nur knapp darüber, und das war's dann. Die ganze Anlage stünde still.

Also müssen die Ventile hinreichend schlagartig betätigt werden. Das bewirkt diese Hysteresemechanik, die ähnlich wie die Springkontakte in den ft-Tastern aufgebaut ist. Schlagartig springt das hier linke Ende der Spiralfedern um, und wenn das Pneumatikventil begonnen wird einzudrücken, genügt der Druck der Spiralfedern, um das Ventil ganz durchzuschalten.

Die Federn hier stammen aus dem alten ft-Elektromechanikprogramm.

Das obere Zeitglied wird für die Steuerung der beiden Stopperzylinder verwendet. Da dessen Ventil ein Schließer (roter Stößel) ist und dieser eine etwas andere Kraft-/Weg-Charakteristik hat, ist die Geometrie nicht exakt dieselbe wie beim unteren Zeitglied (was einen Schließer betätigt), und außerdem sind dort zwei Federn verbaut. Bei der Justage der Ventile kommt es wiedermal auf Bruchteile von Millimetern an, die zwischen gar nicht eindrücken, nur halb drücken und allen Druck verlieren, korrekt betätigen und zu früh betätigen entscheiden. Die Entfernung der Ventile von der Mechanik muss über die Nuten der BS15 genau eingestellt werden, damit alles sauber funktioniert.
