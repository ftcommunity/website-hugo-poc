---
layout: "image"
title: "Signalverstärkung"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35599
- /details0b57.html
imported:
- "2019"
_4images_image_id: "35599"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35599 -->
Der Ausgang der Staudüse geht auf den hier sichtbaren blauen Pneumatik-Doppelbetätiger. Wer diese Teile nicht kennt: Darin befindet sich ein Gummibalg (hier beidseitig, es gibt aber auch Einfachbetätiger, die den Gummibalg nur nach einer Seite haben). Der geht bei Beaufschlagung mit Druckluft einige mm heraus und kann so Pneumatikventile oder auch elektrische Taster und den alten Kipphebel-Polwendeschalter betätigen. Man kann die Betätiger auch direkt zum Greifen von leichten Teilen verwenden.

Der Doppelbetätiger hier (mittig unter den schwarzen Bausteinen) drückt auf zwei Pneumatikventile links und rechts. Das sind im Fachjargon "3/2-Wegeventile", das heißt Ventile mit drei Anschlüssen und zwei Schaltstellungen. Sie verbinden nämlich den Ventilausgang entweder mit dem Drucklufteingang (der Ausgang wird also mit Druckluft beaufschlagt) oder mit dem Abluftausgang, der bei den fischertechnik-Ventilen einfach ins freie strömt (der Ausgang wird also entlüftet - die Druckluft darin kann ins Freie entweichen).

Eines der Ventile ist ein Öffner (erkennbar an dem hier aber kaum sichtbaren blauen Stößel - der Ausgang wird mit Druckluft beaufschlagt, wenn der Stößel gedrückt ist), und eines ein Schließer (roter Stößel, der Ausgang bekommt Druckluft, wenn der Stößel losgelassen ist). Wir haben also hier sowohl das Signal "es ist gerade eine Flasche vor dem Sensor" als auch seine Negation zur Verfügung.
