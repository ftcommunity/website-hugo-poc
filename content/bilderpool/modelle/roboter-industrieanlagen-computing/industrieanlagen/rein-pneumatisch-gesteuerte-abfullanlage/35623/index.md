---
layout: "image"
title: "Flaschen in Füllstation"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35623
- /details65bb.html
imported:
- "2019"
_4images_image_id: "35623"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35623 -->
So sieht es aus, wenn die Flaschen in der Füllstation und die Füllköpfe abgesenkt sind. Der erste Stopperzylinder hat sie durch gelassen, der zweite hält sie auf. Sie wurden bis vier abgezählt und der Füllvorgang beginnt.
