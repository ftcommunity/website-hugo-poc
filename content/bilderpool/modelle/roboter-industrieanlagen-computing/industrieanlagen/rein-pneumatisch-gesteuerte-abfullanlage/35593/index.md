---
layout: "image"
title: "Gesamtansicht von hinten"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35593
- /detailsd95d.html
imported:
- "2019"
_4images_image_id: "35593"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35593 -->
Tatsächlich wurde Trägerflüssigkeit ("Dispersant", eine klare, benzinähnliche Flüssigkeit) für Toner für die in den 1970er Jahren üblichen Nasskopierer (nicht zu verwechseln mit "Abziehgeräten", sondern echte Kopiergeräte) abgefüllt, oder auch gleich das fertige Mischprodukt aus Dispersant + Toner = "Premix". Da die Flüssigkeit brennbar war, musste alles ex-geschützt (explosionsgeschützt) ausgeführt sein. Das einzig Elektrische an der Gesamtanlage war der Motor fürs Förderband und die Maschine für den dritten Arbeitsgang, nämlich das Falten und Verkleben der fertig mit Flaschen gefüllten Kartons. Alles, wirklich alles andere ging rein pneumatisch: Je vier Flaschen abzählen, Füllen, einzeln Verschließen.
