---
layout: "image"
title: "Weg-Zeit-Diagramm"
date: "2015-04-05T22:03:32"
picture: "diagrammederabfuellanlage1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40723
- /detailsa027.html
imported:
- "2019"
_4images_image_id: "40723"
_4images_cat_id: "3060"
_4images_user_id: "104"
_4images_image_date: "2015-04-05T22:03:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40723 -->
Der zu realisierende Ablauf ist wie folgt:

1. Vier übers Förderband kommende Flaschen müssen abgezählt werden.
2. Sobald die vierte erkannt wurde (alle Flaschen werden von Stopperzylinder 2 aufgehalten), müssen sich die Füllköpfe langsam senken.
3. Sobald die unten angekommen sind, wird über ein entsprechend angebrachtes Ventil ein Zeitglied gestartet. Für seine Ablaufdauer läuft auch die elektrische Pumpe (im Original war das auch ein großer Pneumatikzylinder, der einen Pumpzylinder schob).
4. Sobald die Füllzeit abgelaufen ist, müssen die Füllköpfe wieder angehoben werden. In diesem Modell wird die Pumpe etwas vorher ausgeschaltet, um ein Nachtropfen möglichst zu vermeiden. Außerdem verfügt das Modell noch über eine Schlauchabklemmung, die aber keinen tatsächlichen Nutzen brachte.
5. Sobald die Füllköpfe wieder oben angekommen sind, müssen die Stopperzylinder umgesteuert werden: Nr. 1 hindert weitere Flaschen daran, in die Füllanlage einzulaufen, Nr. 2 lässt die vier gefüllten Flaschen ablaufen.
6. Das geht ebenfalls eine gewisse Zeit lang über ein pneumatisches Zeitglied. Nach dessen Ablauf werden die Stopperzylinder wieder in ihre Ausgangsposition umgesteuert. Neue Flaschen können einlaufen; der Vorgang wiederholt sich ab hier.
