---
layout: "image"
title: "Senken und Heben der Füllköpfe"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35606
- /detailsb218.html
imported:
- "2019"
_4images_image_id: "35606"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35606 -->
Hier sieht man den Pneumatikzylinder, der für das Absenken und wieder Anheben der Füllköpfe zuständig ist. An seiner unteren Endlage befindet sich ein Pneumatikventil, dass also betätigt wird, wenn die Füllköpfe ganz abgesenkt sind. Das startet dann den Füllvorgang.
