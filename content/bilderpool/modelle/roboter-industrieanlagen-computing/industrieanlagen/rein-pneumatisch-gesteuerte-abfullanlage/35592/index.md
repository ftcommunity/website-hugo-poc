---
layout: "image"
title: "Gesamtansicht von vorne"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35592
- /details036a-2.html
imported:
- "2019"
_4images_image_id: "35592"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35592 -->
Rechts stellte man Flaschen aufs immer laufende Förderband, die dann - immer vier Flaschen auf ein Mal - gefüllt wurden. Weiter links fehlt noch die Verschließmaschine, die Deckel auf die Flaschen schraubte - für die fehlen mir sowohl mehr Druckluft als auch weitere Pneumatikventile.

Das einzig Elektrische an diesem Modell sind der Antriebsmotor fürs Förderband und die Lemo-Solar-Membranpumpe für die Flüssigkeit (hier Leitungswasser). Alles andere, insbesondere die komplette Logiksteuerung des gesamten Füllvorgangs, ist rein pneumatisch ausgeführt. Es gibt keinerlei weitere Elektrik, Elektronik oder Computerei bei diesem Modell.
