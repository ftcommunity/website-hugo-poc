---
layout: "image"
title: "Gedämpftes Pumpen von Wasser"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35610
- /details2ee7.html
imported:
- "2019"
_4images_image_id: "35610"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35610 -->
Die Membranpumpe pumpt das Wasser stoßartig durch die Schläuche. Das kann man sehr gut am "Ausschlagen" des Zuführungsschlauches aus dem Wasserreservoir sehen. Damit diese Druckstöße nicht bis vorne zu den Füllköpfen gelangen und etwa ein Schlauch abspringt und das Publikum nass spritzt, wird das Wasser stoßartig in die beiden oberen Anschlüsse dieses Pneumatiktanks gepumpt. Das Wasser fällt dann herab und kann aus den unteren Anschlüssen heraus. Im Inneren baut sich dann durch die Verdrängung der Luft durchs Wasser ein Luftdruck auf. Der bewirkt, dass das Wasser unten gleichmäßig, gedämpft und praktisch gänzlich stoßfrei durch die Schläuche nach vorne strömt.
