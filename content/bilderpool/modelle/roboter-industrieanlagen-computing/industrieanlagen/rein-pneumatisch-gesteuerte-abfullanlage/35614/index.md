---
layout: "image"
title: "Ausgänge der Zeitglieder"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35614
- /details238a.html
imported:
- "2019"
_4images_image_id: "35614"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35614 -->
Das untere Zeitglied bestimmt also wie gesagt, wie lange gefüllt wird. Am Ende der Zeit wird sein Pneumatikventil betätigt, welches das Anheben der Füllköpfe und das wieder Abklemmen der Wasserschläuche über die beschriebene Kniehebelmechanik einleitet. Außerdem unterbricht der kurz vorher betätigte Taster, den man hier sieht, die Stromzufuhr der Wasserpumpe, etwas vor dem Wiederanheben der Köpfe. Das gibt Zeit zum Nachtropfen ohne größere Sauerei. Dieser Taster hier ist einfach in Serie mit dem anderen der Wasserpumpe geschaltet.
