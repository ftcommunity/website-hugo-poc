---
layout: "image"
title: "Staudüse"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35598
- /details31cd.html
imported:
- "2019"
_4images_image_id: "35598"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35598 -->
Hier ein Blick auf die fischertechnik-Staudüse (aus dem ersten Pneumatikprogramm von fischertechnik, das zu meinem größten Bedauern nicht mehr hergestellt wird). Aus dem durch die hier von Hand gezogene Schwingfeder sichtbaren kleinen Loch strömt die Luft aus, und ein zweiter Schlauchanschluss führt den Überdruck bei abgedecktem Loch zu einem Pneumatik-Doppelbetätiger. Dort kommt genug Druck an, um Ventile zu schalten, die dann den vollen Betriebsdruck weiterleiten können.

Die bei meinem Modell verwendeten Fläschchen (es handelt sich um Stuhlprobenfläschchen - neue natürlich...) sind allerdings viel zu leicht, um direkt die Staudüse zu betätigen. Deshalb habe ich als langen und besonders leichtgängigen Hebel eine fischertechnik-Schwingfeder vornedran gebaut. Die wird von den vorbeilaufenden Flaschen hinreichend gebogen, um das Austrittsloch der Staudüse so abzudecken, dass die Ventile dahinter schalten können.

Oben im Bild sieht man die Pneumatik-Drossel, die dafür sorgt, dass nur ein bisschen Druckluft zur Staudüse gelangt - gerade genug zur weiteren Verarbeitung. Sonst würde hier viel zu viel Druckluft verloren gehen.
