---
layout: "image"
title: "Abzählen von vier Flaschen"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35601
- /details7e04.html
imported:
- "2019"
_4images_image_id: "35601"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35601 -->
Wir blicken hier auf die Rückseite der Anlage.

Da immer vier Flaschen gleichzeitig gefüllt werden, muss also gewartet werden, bis sich auch wirklich vier Stück in der Füllstation befinden. Im Original war ein Standard-FESTO-Zählwerk verbaut. Das war ein sechsstelliges Dezimalzählwerk (mit einer Übertragsmechanik zwischen den Ziffern), welches pneumatisch betätigt und also bis fast eine Million eingestellt werden konnte. Bei uns war es eben immer auf "4" eingestellt und gab also an seinem Ausgang bei jedem vierten Druckluftimpuls am Eingang einen Druckluftimpuls ab, der dann den Füllvorgang auslöste.

Nach unzähligen Versuchen, das Abzählen von vier Flaschen durch pneumatisch angetriebene Schrittschaltwerke zu realisieren, verwarf ich diesen Weg und beschloss diese Materialschlacht hier: Eine pneumatisch-mechanische Logikschaltung, die bis vier zählt. Sie tut das in 8 Halbschritten (deshalb benötige ich von der Staudüse sowohl das Signal als auch seine Negation).

Der im Bild links unten ankommende Schlauch führt Druckluft, wenn eine Flasche die Schwingfeder und damit die Staudüse gerade betätigt, der links direkt darüber ankommende Schlauch ist beaufschlagt, wenn die Schwingfeder gerade losgelassen ist. Durch die Verschlauchung werden diese Signale nun Zählelement für Zählelement weiter gereicht. Wenn das letzte der acht Ventil (am rechten Ende auf diesem Bild) betätigt wird, ist die vierte Flasche gerade "durch".
