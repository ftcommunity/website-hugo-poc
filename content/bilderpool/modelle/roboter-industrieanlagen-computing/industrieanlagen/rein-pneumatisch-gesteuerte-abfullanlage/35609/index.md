---
layout: "image"
title: "Aufhängung der Pumpe"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/35609
- /detailsf320.html
imported:
- "2019"
_4images_image_id: "35609"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35609 -->
Hier ein seitlicher Blick auf die zwecks Geräusch- und Vibrationsdämpfung gefederte Pumpenaufhängung.
