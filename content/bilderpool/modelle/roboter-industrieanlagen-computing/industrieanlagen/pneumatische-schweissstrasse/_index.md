---
layout: "overview"
title: "Pneumatische Schweißstraße"
date: 2020-02-22T08:03:49+01:00
legacy_id:
- /php/categories/1269
- /categoriesbee5.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1269 --> 
Diese pneumatische Schweißstraße verschweißt ein Flugzeugteil an zwei Stellen.