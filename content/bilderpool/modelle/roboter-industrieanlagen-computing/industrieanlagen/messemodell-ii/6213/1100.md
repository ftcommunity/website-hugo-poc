---
layout: "comment"
hidden: true
title: "1100"
date: "2006-05-17T04:53:54"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Die Palette wird auf das Transportband gelegt und die Anlage mit dem Taster auf der linken Seite gestartet. Das Band besteht aus einem Papierband, das über gefederte Walzen auf Spannung gehalten und angetrieben wird.