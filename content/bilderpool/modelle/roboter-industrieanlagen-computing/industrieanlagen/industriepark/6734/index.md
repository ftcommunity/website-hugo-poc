---
layout: "image"
title: "MIP 022"
date: "2006-08-28T23:28:42"
picture: "B_060826_Industriemodule_12V_012.jpg"
weight: "20"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6734
- /details0939-2.html
imported:
- "2019"
_4images_image_id: "6734"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6734 -->
