---
layout: "image"
title: "MIP 013"
date: "2006-08-28T23:28:21"
picture: "060828_Industriepark_013.jpg"
weight: "12"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6724
- /details43b8.html
imported:
- "2019"
_4images_image_id: "6724"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6724 -->
