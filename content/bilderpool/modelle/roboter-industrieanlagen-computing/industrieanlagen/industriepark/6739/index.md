---
layout: "image"
title: "MIP 032"
date: "2006-08-28T23:28:42"
picture: "B_060826_Industriemodule_12V_019.jpg"
weight: "25"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6739
- /details993d-3.html
imported:
- "2019"
_4images_image_id: "6739"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6739 -->
