---
layout: "image"
title: "Industriepark 002"
date: "2006-08-28T23:28:03"
picture: "060828_Industriepark_002.jpg"
weight: "2"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6714
- /details588b-2.html
imported:
- "2019"
_4images_image_id: "6714"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6714 -->
