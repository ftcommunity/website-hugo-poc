---
layout: "image"
title: "MIP 006"
date: "2006-08-28T23:28:03"
picture: "060828_Industriepark_006.jpg"
weight: "6"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6718
- /details5945-2.html
imported:
- "2019"
_4images_image_id: "6718"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6718 -->
