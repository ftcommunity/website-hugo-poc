---
layout: "image"
title: "MIP 040"
date: "2006-08-28T23:29:11"
picture: "B_060826_Industriemodule_12V_026.jpg"
weight: "29"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6743
- /details7494-2.html
imported:
- "2019"
_4images_image_id: "6743"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6743 -->
