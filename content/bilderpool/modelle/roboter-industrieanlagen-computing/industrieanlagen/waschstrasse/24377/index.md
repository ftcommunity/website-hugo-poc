---
layout: "image"
title: "Antrieb, der die Bürste hoch und runter bewegt."
date: "2009-06-15T22:51:31"
picture: "waschstrasse04.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24377
- /details33d3.html
imported:
- "2019"
_4images_image_id: "24377"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24377 -->
