---
layout: "image"
title: "Ampel am Ende der Waschstraße."
date: "2009-06-15T22:51:31"
picture: "waschstrasse06.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24379
- /details7269-2.html
imported:
- "2019"
_4images_image_id: "24379"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24379 -->
