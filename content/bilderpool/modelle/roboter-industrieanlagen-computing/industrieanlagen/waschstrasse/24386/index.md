---
layout: "image"
title: "Ansicht von Oben"
date: "2009-06-15T22:51:45"
picture: "waschstrasse13.jpg"
weight: "13"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24386
- /detailsad4e.html
imported:
- "2019"
_4images_image_id: "24386"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24386 -->
