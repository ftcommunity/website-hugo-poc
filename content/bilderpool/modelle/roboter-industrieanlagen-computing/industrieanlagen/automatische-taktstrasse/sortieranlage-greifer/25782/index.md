---
layout: "image"
title: "Greifer"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer3.jpg"
weight: "3"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- /php/details/25782
- /detailsaffe-2.html
imported:
- "2019"
_4images_image_id: "25782"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25782 -->
Greifer mit Endabschalter