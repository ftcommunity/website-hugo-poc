---
layout: "image"
title: "Gesamtansicht"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer1.jpg"
weight: "1"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- /php/details/25780
- /details1cd2.html
imported:
- "2019"
_4images_image_id: "25780"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25780 -->
