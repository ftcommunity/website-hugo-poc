---
layout: "image"
title: "Von Vorne"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer5.jpg"
weight: "5"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- /php/details/25784
- /detailsbbb7.html
imported:
- "2019"
_4images_image_id: "25784"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25784 -->
Im Vordergrund die Sortieranlage der Rote Kartong ist die Abdeckung des Fabmessers, da sonst je nachdem wie hell es ist die Messwerte beieiflusst werden.