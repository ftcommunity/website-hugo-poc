---
layout: "image"
title: "Förderbänder"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse04.jpg"
weight: "4"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13686
- /detailsb67a.html
imported:
- "2019"
_4images_image_id: "13686"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13686 -->
