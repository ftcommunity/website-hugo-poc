---
layout: "image"
title: "Automatische Taktstraße"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse01.jpg"
weight: "1"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/13683
- /detailsa209.html
imported:
- "2019"
_4images_image_id: "13683"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13683 -->
Die Drehmechanik der Mehrspindeleinheit