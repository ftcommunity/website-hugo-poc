---
layout: "comment"
hidden: true
title: "6132"
date: "2008-04-03T08:43:12"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Tja, Gratulation zu dieser Aufgabe! Die Chance bekommen sicher nicht viele von der Schule :)

Ich würde das Ding zunächst mal ordentlich Abstauben, aber aufpassen das die Aufkleber nicht wegfliegen...

Viel Erfolg, wir verfolgen den Fortschritt gern in einem Beitrag im Forum :)

Thomas