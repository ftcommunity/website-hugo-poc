---
layout: "overview"
title: "Automatische Taktstraße"
date: 2020-02-22T08:03:46+01:00
legacy_id:
- /php/categories/1259
- /categoriesd50d.html
- /categories6cc5.html
- /categories975a.html
- /categoriese2f7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1259 --> 
Ich habe es als Aufgeabe von meiner Schule bekommen im Rahmen der Technikschen FiÜ-Prüfung diese 3-Stufige Taktstraße funktionsfähig zu machen und dann auch etwas zu Dokumentieren