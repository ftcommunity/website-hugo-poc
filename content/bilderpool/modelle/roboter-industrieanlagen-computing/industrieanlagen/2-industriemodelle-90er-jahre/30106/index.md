---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre01.jpg"
weight: "1"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30106
- /details95c3.html
imported:
- "2019"
_4images_image_id: "30106"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30106 -->
Hier mal mein frisch erworbenes Industriemodell ais den 90zigern.
Es handelt sich um ein SPS Trainingsmodell der Fa. Staudinger.
