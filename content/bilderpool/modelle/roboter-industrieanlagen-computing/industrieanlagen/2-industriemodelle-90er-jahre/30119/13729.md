---
layout: "comment"
hidden: true
title: "13729"
date: "2011-02-27T22:27:51"
uploadBy:
- "FtfanClub"
license: "unknown"
imported:
- "2019"
---
ich habe jetzt gerade gesehen, gelesen, das Du die Modelle demontieren willst. Das wäre wahnsinnig schade, weil es davon nicht mehr sehr viele gibt. Viele sind in Einzelteilen über die Bucht gegangen und ich versuche sie wieder aufzubauen. Programmtechnisch bekomme ich das auch nicht hin, habe aber im Nachbarhaus den Prof.Zacher wohnenm der sich speziell mit der Steuerung von Industrieanlagen, sprich ft, beschäftigt. Ich plane derzeit eine Sim Fabrik mit 2 Hochregallager, 12 Bearbeitungsstationen, 6 Roboter und etwa 40 Transportbänder mit 8 Drehtischen.  Lasse Deine Modelle bestehen und versuche Dich mal damit auseinanderzusetzen. Du wirst mehr davon haben als die paar Teile in Kisten zu sortieren. Eine Bitte habe ich... wenn Du es demontierst, würde ich mich über eine genaue Bildfolge freuen. Noch schöner wäre es, wenn sich die Industriebauer mal treffen und ein paar Studenten dazu einladen, die Dinger wieder zum Laufen zu bringen. Die Uni's haben so etwas nicht mehr

Gruß aus Wiesbaden  Micha