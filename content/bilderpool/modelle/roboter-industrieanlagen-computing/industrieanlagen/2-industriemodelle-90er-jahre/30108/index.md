---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre03.jpg"
weight: "3"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30108
- /details7bcf-2.html
imported:
- "2019"
_4images_image_id: "30108"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30108 -->
Hier die aufrechten Maschineständer und ein  Pusher, der das Werkstück auf die nach links 
führenden Transportbänder schieben kann.
