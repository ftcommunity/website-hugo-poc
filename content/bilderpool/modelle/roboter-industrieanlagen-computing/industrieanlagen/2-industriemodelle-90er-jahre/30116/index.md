---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:51:09"
picture: "industriemodellederzigerjahre11.jpg"
weight: "11"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30116
- /details6b3f.html
imported:
- "2019"
_4images_image_id: "30116"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:51:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30116 -->
Hier eine Leiste mit Relais. Das sind scheinbar original FT  Teile. Weis jemand das was genaues?
