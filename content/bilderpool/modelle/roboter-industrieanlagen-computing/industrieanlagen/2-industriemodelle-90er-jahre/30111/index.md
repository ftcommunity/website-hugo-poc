---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre06.jpg"
weight: "6"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30111
- /details5e8d.html
imported:
- "2019"
_4images_image_id: "30111"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30111 -->
Hier das Siemens Simatic S5 .
