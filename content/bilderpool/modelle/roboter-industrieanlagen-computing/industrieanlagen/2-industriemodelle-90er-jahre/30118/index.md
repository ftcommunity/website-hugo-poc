---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:51:09"
picture: "industriemodellederzigerjahre13.jpg"
weight: "13"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30118
- /detailscbf8.html
imported:
- "2019"
_4images_image_id: "30118"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:51:09"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30118 -->
Hier das Transportband mit zwei von drei Schiebern .

