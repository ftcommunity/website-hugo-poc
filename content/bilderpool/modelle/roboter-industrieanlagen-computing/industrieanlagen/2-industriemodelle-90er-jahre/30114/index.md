---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre09.jpg"
weight: "9"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "M.Wolf"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/30114
- /details4668.html
imported:
- "2019"
_4images_image_id: "30114"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30114 -->
Der" Dreiachskran" von oben.
