---
layout: "image"
title: "RBG von unten"
date: "2016-07-28T16:54:28"
picture: "logzen1.jpg"
weight: "14"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44037
- /details7360.html
imported:
- "2019"
_4images_image_id: "44037"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44037 -->
Die Edelstahlstangen dienen dazu, dass das RBG auf seiner Schiene bleibt.
