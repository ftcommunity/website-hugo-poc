---
layout: "image"
title: "Warenausgabe"
date: "2016-07-25T16:45:59"
picture: "logzen09.jpg"
weight: "9"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44008
- /detailsb91b.html
imported:
- "2019"
_4images_image_id: "44008"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44008 -->
Über eine Vakuumsauger werden die Werkstücke vom Drehtisch zur Warenausgabe gebracht
