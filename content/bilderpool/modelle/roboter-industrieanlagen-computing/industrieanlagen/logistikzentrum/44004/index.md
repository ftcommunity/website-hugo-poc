---
layout: "image"
title: "Drehtisch von Oben"
date: "2016-07-25T16:45:59"
picture: "logzen05.jpg"
weight: "5"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44004
- /details629b.html
imported:
- "2019"
_4images_image_id: "44004"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44004 -->
Der Drehtisch sorgt dafür, dass ein schneller Wechsel zwischen eingehenden und ausgehenden Waren zustande kommt. Er dreht sich nach dem Einbahnstraßenprinzip nur in eine Richtung und gewährleistet eine reibungslosen Warenfluss und minimiert die Stehzeit des Regalbediengeräts.
