---
layout: "image"
title: "Benutzersteuerung"
date: "2017-01-09T17:51:47"
picture: "Benutzersteuerung_-_Kopie.jpg"
weight: "15"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45024
- /detailsf638.html
imported:
- "2019"
_4images_image_id: "45024"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2017-01-09T17:51:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45024 -->
GUI in Robo Pro zur Steuerung der Anlage

Video: https://www.youtube.com/watch?v=w6GWn2Gp60c
