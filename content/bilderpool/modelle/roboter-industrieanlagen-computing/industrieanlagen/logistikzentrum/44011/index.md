---
layout: "image"
title: "Regalbediengerät"
date: "2016-07-25T16:46:11"
picture: "logzen12.jpg"
weight: "12"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44011
- /detailsb891-5.html
imported:
- "2019"
_4images_image_id: "44011"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:46:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44011 -->
X / Z Achse werden jeweils über einen Encodermotor angetrieben
