---
layout: "image"
title: "Scanner im Drehtisch"
date: "2016-07-25T16:45:59"
picture: "logzen06.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44005
- /details561a.html
imported:
- "2019"
_4images_image_id: "44005"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44005 -->
Über den im Drehtisch integrierten Farbsensor (er dreht sich nicht mit dem Drehtisch dreht, sondern ist fest auf der Grundplatte montiert)  wird die Farbe der Ware [des Werkstücks] noch vor dem Einlagern bestimmt.
