---
layout: "image"
title: "Gabel"
date: "2016-07-25T16:46:11"
picture: "logzen13.jpg"
weight: "13"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44012
- /details55e1.html
imported:
- "2019"
_4images_image_id: "44012"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:46:11"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44012 -->
Gabel zur Aufnahme der Ware, jeweils zwei Taster zur Endlagenabschaltung. Antrieb über XS Motor
