---
layout: "image"
title: "Getriebe für Auflockerung"
date: "2011-07-08T18:00:37"
picture: "chekmaker13.jpg"
weight: "17"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31004
- /detailsd710.html
imported:
- "2019"
_4images_image_id: "31004"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31004 -->
Da der PowerMotor etwas zu schnell war, haben wir eine Übersetzung eingebaut.
