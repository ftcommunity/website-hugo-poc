---
layout: "image"
title: "Geldeinwurf + Produktwahl"
date: "2011-07-08T18:00:37"
picture: "chekmaker04.jpg"
weight: "8"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30995
- /details4017.html
imported:
- "2019"
_4images_image_id: "30995"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30995 -->
Hier sieht man den Geldeinwurf (20 Cent) und die orangene Betriebsstatusanzeige (leuchtend - wartet auf Geldeinwurf, blinkend - produziert)

Das blaue Licht zeigt an, ob man Kaba (aus) oder Eiskaffee (an) gewählt hat...
