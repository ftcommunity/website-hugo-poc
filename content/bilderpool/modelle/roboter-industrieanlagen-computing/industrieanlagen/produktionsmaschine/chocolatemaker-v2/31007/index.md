---
layout: "image"
title: "Übergang von Pulverbehälter zur Kette"
date: "2011-07-08T18:00:37"
picture: "chekmaker16.jpg"
weight: "20"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31007
- /detailsfd59.html
imported:
- "2019"
_4images_image_id: "31007"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31007 -->
Hier ist die Lichtschranke, eine Schaufel und ein PowerMotor, der die Kette antreibt, zu sehen.
