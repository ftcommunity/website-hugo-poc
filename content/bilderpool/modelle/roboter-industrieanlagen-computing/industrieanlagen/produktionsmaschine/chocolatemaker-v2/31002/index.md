---
layout: "image"
title: "Becherhalterung von der Seite"
date: "2011-07-08T18:00:37"
picture: "chekmaker11.jpg"
weight: "15"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31002
- /details0d88-2.html
imported:
- "2019"
_4images_image_id: "31002"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31002 -->
Die Becherhalterung aus einer anderen Perspektive.
