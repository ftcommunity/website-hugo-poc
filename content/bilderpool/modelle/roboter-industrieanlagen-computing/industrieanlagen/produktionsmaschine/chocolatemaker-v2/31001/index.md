---
layout: "image"
title: "Becherhalterung"
date: "2011-07-08T18:00:37"
picture: "chekmaker10.jpg"
weight: "14"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/31001
- /details9e3f-2.html
imported:
- "2019"
_4images_image_id: "31001"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31001 -->
Auf diesem Bild ist die Becherhalterung auf dem Förderband abgebildet.
