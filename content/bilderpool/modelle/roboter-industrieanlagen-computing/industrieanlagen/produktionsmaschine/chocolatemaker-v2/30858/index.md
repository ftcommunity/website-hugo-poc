---
layout: "image"
title: "Münzeinwurf"
date: "2011-06-14T22:35:23"
picture: "chocolatemakerv1.jpg"
weight: "1"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/30858
- /detailsfc4d.html
imported:
- "2019"
_4images_image_id: "30858"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30858 -->
Der Geldeinwurf (von Fanclubmodell Taschentuchspender) mit einer Ampel.
