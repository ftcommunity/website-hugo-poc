---
layout: "image"
title: "Das Förderband 4"
date: "2008-09-03T18:23:32"
picture: "produktionmaschine13.jpg"
weight: "13"
konstrukteure: 
- "alone"
fotografen:
- "alone"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15173
- /details279a-2.html
imported:
- "2019"
_4images_image_id: "15173"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15173 -->
Mit Antriebsmotor
Hier habe ich beide ketten mal getrennt, um die "Schiene" zu verdeutlichen.