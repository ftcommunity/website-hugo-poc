---
layout: "image"
title: "Die Stanze"
date: "2008-09-03T18:23:32"
picture: "produktionmaschine08.jpg"
weight: "8"
konstrukteure: 
- "alone"
fotografen:
- "alone"
uploadBy: "alone"
license: "unknown"
legacy_id:
- /php/details/15168
- /details9610-2.html
imported:
- "2019"
_4images_image_id: "15168"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15168 -->
Damit die "Rohlinge" auch passgenau liegen, werden sie noch richtig gerückt