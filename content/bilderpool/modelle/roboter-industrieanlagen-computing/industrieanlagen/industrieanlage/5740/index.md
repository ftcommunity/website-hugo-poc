---
layout: "image"
title: "3D-Roboter"
date: "2006-02-05T16:30:58"
picture: "DSC01423Greifer.jpg"
weight: "2"
konstrukteure: 
- "Felix"
fotografen:
- "Felix"
uploadBy: "felix"
license: "unknown"
legacy_id:
- /php/details/5740
- /detailsd65b-2.html
imported:
- "2019"
_4images_image_id: "5740"
_4images_cat_id: "492"
_4images_user_id: "410"
_4images_image_date: "2006-02-05T16:30:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5740 -->
