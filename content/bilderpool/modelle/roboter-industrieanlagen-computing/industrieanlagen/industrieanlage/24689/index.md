---
layout: "image"
title: "Magazin und Roboterarm"
date: "2009-07-29T23:29:17"
picture: "industrieanlage12.jpg"
weight: "15"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24689
- /details1927.html
imported:
- "2019"
_4images_image_id: "24689"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24689 -->
