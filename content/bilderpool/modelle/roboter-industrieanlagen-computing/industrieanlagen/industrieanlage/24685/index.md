---
layout: "image"
title: "Bohreinheit von oben"
date: "2009-07-29T23:29:11"
picture: "industrieanlage08.jpg"
weight: "11"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24685
- /details7214.html
imported:
- "2019"
_4images_image_id: "24685"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24685 -->
