---
layout: "image"
title: "Kompressor und Magazin"
date: "2009-07-29T23:29:11"
picture: "industrieanlage09.jpg"
weight: "12"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24686
- /details4706-2.html
imported:
- "2019"
_4images_image_id: "24686"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24686 -->
