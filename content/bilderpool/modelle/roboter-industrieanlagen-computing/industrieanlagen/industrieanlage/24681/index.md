---
layout: "image"
title: "Roboterarm und Magazin"
date: "2009-07-29T23:29:11"
picture: "industrieanlage04.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/24681
- /details535f-2.html
imported:
- "2019"
_4images_image_id: "24681"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24681 -->
Das Magazin gibt dem Roboterarm ein Baustein, dieser stellt den Baustein dann auf das Fließband, wo er gebort und geschweißt wid.