---
layout: "image"
title: "industriemodellvonthomasoft19.jpg"
date: "2010-08-25T00:43:04"
picture: "industriemodellvonthomasoft19.jpg"
weight: "19"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27927
- /details5589.html
imported:
- "2019"
_4images_image_id: "27927"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:04"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27927 -->
