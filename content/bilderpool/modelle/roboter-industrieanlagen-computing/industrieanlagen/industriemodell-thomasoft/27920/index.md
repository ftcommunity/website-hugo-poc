---
layout: "image"
title: "industriemodellvonthomasoft12.jpg"
date: "2010-08-25T00:43:04"
picture: "industriemodellvonthomasoft12.jpg"
weight: "12"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/27920
- /detailsea5a-2.html
imported:
- "2019"
_4images_image_id: "27920"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:04"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27920 -->
