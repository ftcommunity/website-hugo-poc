---
layout: "image"
title: "Bearbeitungszentrum 2"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9101
- /details8945.html
imported:
- "2019"
_4images_image_id: "9101"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9101 -->
