---
layout: "image"
title: "Bearbeitungszentrum 15"
date: "2007-02-20T14:44:44"
picture: "bearbeitungszentrum15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9114
- /detailsc23e.html
imported:
- "2019"
_4images_image_id: "9114"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:44"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9114 -->
