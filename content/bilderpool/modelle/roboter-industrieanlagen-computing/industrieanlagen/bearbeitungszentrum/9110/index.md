---
layout: "image"
title: "Bearbeitungszentrum 11"
date: "2007-02-20T14:44:44"
picture: "bearbeitungszentrum11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9110
- /details834e.html
imported:
- "2019"
_4images_image_id: "9110"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:44"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9110 -->
