---
layout: "image"
title: "Bearbeitungszentrum 9"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9108
- /details91bc.html
imported:
- "2019"
_4images_image_id: "9108"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9108 -->
