---
layout: "image"
title: "Drehmechanik"
date: "2008-03-07T07:03:14"
picture: "olli11.jpg"
weight: "11"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13854
- /details99d1.html
imported:
- "2019"
_4images_image_id: "13854"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13854 -->
Hier mal die Taster für die Endabschaltung. Da die Motoren sich ja unterschiedlich schnell drehen, aber an einem Ausgang hängen habe wird die Stromzufuhr unterbrochen wenn einer der Endtaster geschaltet wird. Das wurde halt mit Dioden gebaut.