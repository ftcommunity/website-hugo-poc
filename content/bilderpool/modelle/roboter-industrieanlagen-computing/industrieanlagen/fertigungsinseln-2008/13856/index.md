---
layout: "image"
title: "Diode"
date: "2008-03-07T07:03:14"
picture: "olli13.jpg"
weight: "13"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13856
- /details211e.html
imported:
- "2019"
_4images_image_id: "13856"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13856 -->
Konstruktion mit Diode.