---
layout: "image"
title: "Fotowiderstand"
date: "2008-03-07T07:03:14"
picture: "olli16.jpg"
weight: "16"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/13859
- /details289f-2.html
imported:
- "2019"
_4images_image_id: "13859"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13859 -->
Eine Lichtschranke wurde mit einem Fotowiderstand gebaut, da ich nur 3 Fototransistoren habe.