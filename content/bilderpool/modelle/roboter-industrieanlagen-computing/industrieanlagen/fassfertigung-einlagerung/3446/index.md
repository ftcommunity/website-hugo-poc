---
layout: "image"
title: "regal4"
date: "2005-01-02T15:48:44"
picture: "regal4.JPG"
weight: "9"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3446
- /details0630.html
imported:
- "2019"
_4images_image_id: "3446"
_4images_cat_id: "319"
_4images_user_id: "5"
_4images_image_date: "2005-01-02T15:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3446 -->
