---
layout: "image"
title: "Säulenroboter"
date: "2005-01-04T15:19:19"
picture: "sule.jpg"
weight: "12"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/3483
- /details9532.html
imported:
- "2019"
_4images_image_id: "3483"
_4images_cat_id: "319"
_4images_user_id: "94"
_4images_image_date: "2005-01-04T15:19:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3483 -->
