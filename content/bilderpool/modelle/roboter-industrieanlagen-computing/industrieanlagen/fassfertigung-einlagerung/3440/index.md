---
layout: "image"
title: "Tafel"
date: "2005-01-02T15:48:44"
picture: "Tafel.JPG"
weight: "3"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3440
- /detailsea66-2.html
imported:
- "2019"
_4images_image_id: "3440"
_4images_cat_id: "319"
_4images_user_id: "5"
_4images_image_date: "2005-01-02T15:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3440 -->
