---
layout: "image"
title: "2. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_2_06.jpg"
weight: "18"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17071
- /details1bdb.html
imported:
- "2019"
_4images_image_id: "17071"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17071 -->
Beginn des Abbruchs. Gut zu sehen der Meßkontakt.
