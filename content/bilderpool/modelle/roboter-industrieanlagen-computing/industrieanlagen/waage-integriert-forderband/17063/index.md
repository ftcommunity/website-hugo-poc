---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:36"
picture: "Frderband__Waage_1_10.jpg"
weight: "10"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17063
- /detailsd653.html
imported:
- "2019"
_4images_image_id: "17063"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17063 -->
Ende der Messung: Kassette mit 2 Bausteinen 30
