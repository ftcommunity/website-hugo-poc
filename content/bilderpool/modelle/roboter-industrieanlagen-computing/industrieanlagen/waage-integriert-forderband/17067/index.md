---
layout: "image"
title: "2. Waage"
date: "2009-01-18T18:10:41"
picture: "Frderband__Waage_2_02.jpg"
weight: "14"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
schlagworte: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/17067
- /details73bf-2.html
imported:
- "2019"
_4images_image_id: "17067"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17067 -->
Noch einmal von vorne
Rechts unten sieht man eine „Ampel“:
Bei einer leeren Kassette leuchtet kein Licht.
Kassette gefüllt mit einem Baustein 30: das grüne Licht brennt.
Mit 2-3 Bausteinen 30: die gelbe Lampe leuchtet.
Bei 4-6 Bausteinen 30 leuchtet die orange Lampe.
Bei ganz schwerem Meßgut leuchtet die rote Lampe.
