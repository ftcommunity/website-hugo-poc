---
layout: "overview"
title: "Flugsimulator (Johannes)"
date: 2020-02-22T08:03:51+01:00
legacy_id:
- /php/categories/1285
- /categories1989.html
- /categories6994.html
- /categoriesccdd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1285 --> 
Das ist ein Flugsimulator der ein Höhenruder, ein Querruder ein Propeller dessen Geschwindigkeit man verstellen kann und eine Warnung die den Piloten warnt, wenn er zu steil hoch fliegt.