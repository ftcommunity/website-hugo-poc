---
layout: "image"
title: "Motor, der das Querruder antreibt"
date: "2008-03-22T22:21:06"
picture: "flugsimulator4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14034
- /details2565.html
imported:
- "2019"
_4images_image_id: "14034"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14034 -->
Hier sieht man den Motor, der das Querruder antreibt.