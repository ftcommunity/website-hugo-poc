---
layout: "image"
title: "Flaschenfüll02.JPG"
date: "2004-01-07T20:37:04"
picture: "Flaschenfll02.jpg"
weight: "2"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2051
- /details93b6-2.html
imported:
- "2019"
_4images_image_id: "2051"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2051 -->
Detail der Waschanlage. Die federbelasteten Rollen lassen die Schläuche je nach Position im Karussel unterschiedlich tief in die Flaschen eintauchen.