---
layout: "image"
title: "Flaschenfüll04.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll04.jpg"
weight: "4"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2053
- /details06f9.html
imported:
- "2019"
_4images_image_id: "2053"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2053 -->
Station 3: die Flaschen wandern von rechts her zu und während der Runde durchs Karussell kommt der Inhalt hinein. Jede Flasche kommt in ein eigenes 'Körbchen', das an Stangen geführt ist und während der Drehbewegung zum Füllstutzen hin angehoben wird, damit nichts danebengeht.