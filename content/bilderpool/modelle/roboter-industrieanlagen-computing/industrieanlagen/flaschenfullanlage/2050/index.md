---
layout: "image"
title: "Flaschenfüll01.JPG"
date: "2004-01-07T20:37:04"
picture: "Flaschenfll01.jpg"
weight: "1"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
schlagworte: ["Industrie", "Flaschen", "Füllanlage"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2050
- /details8c27.html
imported:
- "2019"
_4images_image_id: "2050"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2050 -->
Die Flaschenfüllanlage von Frans Leurs, gezeigt in Schoonhoven 2003. Die Bilder sind nach dem Treffen irgendwie übersehen worden; das Hochladen wird jetzt aber nachgeholt. 

Die Anlage säubert die Flaschen, untersucht sie auf Restschmutz und Schäden (entsprechende Exemplare werden ausgeworfen), befüllt sie, holt sich Deckel aus einem Schüttbehälter, dreht selbige in richtige Position, schraubt sie auf die Flaschen, bedruckt diese und packt sie anschließend in Kästen ab.

Insgesamt eine Waaaahnsinns-Anlage, die sich über so etwa 5 laufende Meter Tapeziertisch erstreckt.

Hier zu sehen ist Station 1, die Waschanlage. Nach einer Runde im Karussel sind die Flaschen sauber.
