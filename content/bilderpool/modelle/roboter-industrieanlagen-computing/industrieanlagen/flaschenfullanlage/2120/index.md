---
layout: "image"
title: "Flafue07.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue07.jpg"
weight: "19"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2120
- /details9818.html
imported:
- "2019"
_4images_image_id: "2120"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2120 -->
Station "Deckel aufschrauben", Veghel 2004.

Einfach genial! Die Flaschen kommen von links und werden in eine der Transportbuchten geschoben. Das schwarze Riesenzahnrad (nicht Original-ft) steht, während der ganze obere Aufbau mitsamt den Buchten gegen den Uhrzeigersinn drumherum rotiert. Dabei rollen die schwarzen Z10 auf dem Riesenzahnrad ab, so dass die Drehscheiben über den Kettenantrieb gegen die Fahrtrichtung, also im Uhrzeigersinn drehen und damit den Deckel festschrauben.

Nun kommt Teil 2 der Übung: Die Schraubvorrichtungen müssen beim Einfahren der Flaschen angehoben sein und  zum Festschrauben der Deckel abgesenkt werden. Dafür sorgen die Rollen oben/innen, die auf einem 3/4 kompletten Kranz aus Bausteinen 15+Winkelsteinen laufen. Wo der Kranz unterbrochen ist, senken sich die Arme ab und bringen die Zahnräder in Eingriff. Es drehen sich also nur die im Vordergrund sichtbaren Drehscheiben, da sie vor dem Erreichen des Förderbands rechts wieder angehoben und ausgekuppelt werden.