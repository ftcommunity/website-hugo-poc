---
layout: "image"
title: "Flaschenfuell15.jpg"
date: "2004-01-07T20:37:28"
picture: "Flaschenfll15.jpg"
weight: "15"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2064
- /details0b79.html
imported:
- "2019"
_4images_image_id: "2064"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2064 -->
Das ist die Zielgerade. Die volle Kiste ist von links oben her gekommen und kriegt jetzt einen abschließenden Schubs, der sie nach rechts oben auf die Rampe befördert.

Ja, und auch in Holland hat man sich Lösungen für die doch etwas kleinen Original-P-Zylinder einfallen lassen!