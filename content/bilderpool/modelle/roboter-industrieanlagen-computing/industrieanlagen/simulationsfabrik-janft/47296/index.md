---
layout: "image"
title: "Greifer Rückansicht"
date: "2018-02-15T18:59:05"
picture: "simulationsfabrikjanft10.jpg"
weight: "10"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/47296
- /details5147-3.html
imported:
- "2019"
_4images_image_id: "47296"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47296 -->
