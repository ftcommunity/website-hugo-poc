---
layout: "overview"
title: "Simulationsfabrik (Janft)"
date: 2020-02-22T08:04:40+01:00
legacy_id:
- /php/categories/3499
- /categoriescd78.html
- /categories847d.html
- /categoriesa6db-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3499 --> 
Hier zu sehen ist meine Simulationsfabrik nach Vorbild der FT-Industriemodelle.
Sie besteht aus einer Sortieranlage, einem 3-Achs-Greifer und einem Brennofen.
