---
layout: "image"
title: "Greifer Frontansicht"
date: "2018-02-15T18:59:05"
picture: "simulationsfabrikjanft09.jpg"
weight: "9"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/47295
- /details2a08.html
imported:
- "2019"
_4images_image_id: "47295"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47295 -->
das Modell ist der 3-Achs-Roboter aus dem Industry Robots II Set. Einzig und Allein der Greifer wurde umgebaut und ein Vakuumsauger nahm seinen Platz ein.