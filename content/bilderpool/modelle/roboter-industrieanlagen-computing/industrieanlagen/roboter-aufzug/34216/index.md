---
layout: "image"
title: "Roboter-Aufzug-03"
date: "2012-02-18T13:28:18"
picture: "HUB-03.jpg"
weight: "3"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34216
- /details13b9-3.html
imported:
- "2019"
_4images_image_id: "34216"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34216 -->
Hier ist die Hebevorrichtung zu sehen. Durch ihr Eigengewicht und die spezielle Montage der Führungsrollen ist ein entgleisen oder rausrutschen aus den Schienen unmöglich. Das Hubgetriebe wird weitesgehend entlastet. Die Bausteine, an denen die Hub-Zahnstangen befestigt sind, sind in der gegeüberliegenden Nut durch eine Metallachse verbunden. So ist sichergestellt, das es nicht zu Verformungen in einer Richtung kommt.