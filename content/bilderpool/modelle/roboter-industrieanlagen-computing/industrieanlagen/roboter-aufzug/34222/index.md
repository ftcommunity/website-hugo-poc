---
layout: "image"
title: "Roboter-Aufzug-09"
date: "2012-02-18T13:28:18"
picture: "HUB-09.jpg"
weight: "9"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34222
- /detailsb1c1.html
imported:
- "2019"
_4images_image_id: "34222"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34222 -->
hier die gleiche Ansicht, nur von Vorne. Ich hoffe es hat gefallen. Gruß und Spaß, Andreas