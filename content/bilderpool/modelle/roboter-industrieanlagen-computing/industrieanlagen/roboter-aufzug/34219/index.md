---
layout: "image"
title: "Roboter-Aufzug-06"
date: "2012-02-18T13:28:18"
picture: "HUB-06.jpg"
weight: "6"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34219
- /detailsa40c-2.html
imported:
- "2019"
_4images_image_id: "34219"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34219 -->
hier ist es einfach eine andere Ansicht der vorderen Laufräder