---
layout: "image"
title: "Roboter-Aufzug-05"
date: "2012-02-18T13:28:18"
picture: "HUB-05.jpg"
weight: "5"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- /php/details/34218
- /detailse4be-2.html
imported:
- "2019"
_4images_image_id: "34218"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34218 -->
hier ein Detail, aus Richtung der Hub-Zahnstangen gesehen. Man kann erkennen wie sich die Laufräder selbstklemmend an den Laufschienen halten. Dieser Schalter, wie alle anderen an den Schienen haben die Fuktion "Endschalter Ebene -x-