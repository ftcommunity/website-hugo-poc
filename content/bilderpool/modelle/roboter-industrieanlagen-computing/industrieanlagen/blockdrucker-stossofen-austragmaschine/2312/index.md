---
layout: "image"
title: "Austrag08"
date: "2004-03-11T20:12:31"
picture: "Austrag-08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2312
- /details1c43.html
imported:
- "2019"
_4images_image_id: "2312"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2312 -->
So sieht es auch in der Realität aus, wenn man auf dem Blockdrücker steht und in den Ofeneinlauf blickt.