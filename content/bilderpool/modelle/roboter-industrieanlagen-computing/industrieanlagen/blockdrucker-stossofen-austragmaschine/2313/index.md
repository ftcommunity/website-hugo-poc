---
layout: "image"
title: "Austrag09"
date: "2004-03-11T20:12:31"
picture: "Austrag-09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2313
- /detailse2de-2.html
imported:
- "2019"
_4images_image_id: "2313"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2313 -->
Diese Perspektive (und Größenordnung) hat man vor der Austragmaschine mit Blick auf die Ofentür und Rollgang.