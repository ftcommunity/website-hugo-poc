---
layout: "image"
title: "Austrag05"
date: "2004-03-11T20:12:31"
picture: "Austrag-05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2309
- /details8a68.html
imported:
- "2019"
_4images_image_id: "2309"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2309 -->
Hier die Vorderansicht auf die Ofentür und die Antriebsmechanik.