---
layout: "image"
title: "Austrag02"
date: "2004-03-11T20:12:31"
picture: "Austrag-02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2306
- /details8c99.html
imported:
- "2019"
_4images_image_id: "2306"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2306 -->
Hier ein Bild der gesamten Anlage. Das Bild zeigt (von rechts): den Blockdrücker, den langen Stoßofen, dann den Rollgang und die Austragmaschine.