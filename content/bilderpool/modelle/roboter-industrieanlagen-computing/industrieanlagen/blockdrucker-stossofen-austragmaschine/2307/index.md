---
layout: "image"
title: "Austrag03"
date: "2004-03-11T20:12:31"
picture: "Austrag-03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2307
- /details9efb.html
imported:
- "2019"
_4images_image_id: "2307"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2307 -->
Ofenauslauf : Eine Seitenansicht mit FT-Block auf dem Rollgang.