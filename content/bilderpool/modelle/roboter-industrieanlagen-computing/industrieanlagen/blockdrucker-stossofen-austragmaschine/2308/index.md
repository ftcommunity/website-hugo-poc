---
layout: "image"
title: "Austrag04"
date: "2004-03-11T20:12:31"
picture: "Austrag-04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2308
- /detailsd2ed.html
imported:
- "2019"
_4images_image_id: "2308"
_4images_cat_id: "236"
_4images_user_id: "54"
_4images_image_date: "2004-03-11T20:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2308 -->
Übersicht (von links) auf die Ofentür, den Rollgang mit Anschlag und die Austragmaschine mit "Schleppkabel".