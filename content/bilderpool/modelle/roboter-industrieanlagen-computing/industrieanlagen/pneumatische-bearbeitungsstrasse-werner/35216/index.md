---
layout: "image"
title: "Adapterkabel"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse03.jpg"
weight: "3"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35216
- /details6b13-3.html
imported:
- "2019"
_4images_image_id: "35216"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35216 -->
