---
layout: "image"
title: "Druckluftversorgung"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse04.jpg"
weight: "4"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35217
- /details2a52.html
imported:
- "2019"
_4images_image_id: "35217"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35217 -->
