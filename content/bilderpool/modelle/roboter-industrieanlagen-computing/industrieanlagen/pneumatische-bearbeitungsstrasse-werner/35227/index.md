---
layout: "image"
title: "Antrieb Drehtisch"
date: "2012-07-30T18:30:30"
picture: "pneumatischebearbeitungsstrasse14.jpg"
weight: "14"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35227
- /details6b8c.html
imported:
- "2019"
_4images_image_id: "35227"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:30"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35227 -->
