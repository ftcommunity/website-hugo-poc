---
layout: "image"
title: "Die Stanze"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse10.jpg"
weight: "10"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35223
- /details9df5.html
imported:
- "2019"
_4images_image_id: "35223"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35223 -->
