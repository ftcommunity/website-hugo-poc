---
layout: "image"
title: "Ansicht vorne"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse05.jpg"
weight: "5"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35218
- /details05d1-2.html
imported:
- "2019"
_4images_image_id: "35218"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35218 -->
