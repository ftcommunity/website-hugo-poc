---
layout: "image"
title: "Ventilinsel"
date: "2012-07-30T18:30:30"
picture: "pneumatischebearbeitungsstrasse11.jpg"
weight: "11"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35224
- /detailsa92f.html
imported:
- "2019"
_4images_image_id: "35224"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35224 -->
