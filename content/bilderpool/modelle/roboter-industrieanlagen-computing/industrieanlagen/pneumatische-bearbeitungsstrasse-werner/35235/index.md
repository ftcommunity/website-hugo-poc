---
layout: "image"
title: "Verkabelung TX Controller"
date: "2012-08-01T13:07:11"
picture: "pneumatischebearbeitungsstrasse3.jpg"
weight: "22"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/35235
- /details27cb.html
imported:
- "2019"
_4images_image_id: "35235"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-08-01T13:07:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35235 -->
