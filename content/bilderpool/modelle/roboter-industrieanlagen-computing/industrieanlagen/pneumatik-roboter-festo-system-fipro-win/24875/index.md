---
layout: "image"
title: "Pneumatik-Roboter"
date: "2009-09-06T19:17:11"
picture: "Pneumatik-Roboter2_800x600.jpg"
weight: "1"
konstrukteure: 
- "Lorenz Parting"
fotografen:
- "Lorenz Parting"
schlagworte: ["Pneumatik", "Computing", "graue", "Steine"]
uploadBy: "Macgyver"
license: "unknown"
legacy_id:
- /php/details/24875
- /detailsbe00-2.html
imported:
- "2019"
_4images_image_id: "24875"
_4images_cat_id: "1711"
_4images_user_id: "726"
_4images_image_date: "2009-09-06T19:17:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24875 -->
Ein Modell, dass ich damals in der 10ten Klasse für den "Tag der offnen Tür" gebaut habe. 
Das Ganze ist noch mit dem alten Pneumatiksystem von Festo