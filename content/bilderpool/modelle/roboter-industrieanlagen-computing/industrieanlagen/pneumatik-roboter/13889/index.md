---
layout: "image"
title: "Pneumatik-Roboter 5 Bild 1"
date: "2008-03-08T22:39:11"
picture: "Pneumatik-Roboter_5_Bild_1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["FT Experimenta Schulprogramm"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13889
- /details864e-3.html
imported:
- "2019"
_4images_image_id: "13889"
_4images_cat_id: "1274"
_4images_user_id: "724"
_4images_image_date: "2008-03-08T22:39:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13889 -->
Links meine Kompressorstation. Drückt bei 12V 0,4 bar.
Das blaue rechts unten am Roboter ist eine alte P-Drossel. Die braucht man, da sonst die Mittelpositionierung nicht, oder kaum geht. Entweder man hat noch einen alten Pneumatik-Kasten (so wie ich), oder man schaut bei ebay, bzw. hier:
http://www.fischerfriendswoman.de/
