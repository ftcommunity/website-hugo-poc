---
layout: "image"
title: "5"
date: "2010-06-25T18:20:41"
picture: "zweiroboter5.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27565
- /details1341.html
imported:
- "2019"
_4images_image_id: "27565"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27565 -->
Mit dem Hubgetriebe fährt der S-Motor immer ein Stück weiter und drückt immer einen anderen Taster, so wird der ganze Roboter gesteuert. Das System funktioniert nicht einwandfrei, ich habe aber kein Interface oder TX-Controller. Leider:-(