---
layout: "image"
title: "7"
date: "2010-06-25T18:20:41"
picture: "zweiroboter7.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27567
- /details707a.html
imported:
- "2019"
_4images_image_id: "27567"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27567 -->
Mein selbst gebautes Schloss. Besteht aus einem alten Schloss für Fahrradpumpen am Fahrrad. Genauere Bilder werden folgen. Inzwischen habe ich nähmlich an der Seite unnötige Teile abgesägt und er ist hinten auch nicht mehr offen.