---
layout: "image"
title: "Walzen"
date: "2017-05-21T13:53:33"
picture: "pwa3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45915
- /details77db-2.html
imported:
- "2019"
_4images_image_id: "45915"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45915 -->
Als Walzen dienen Malerrollen aus dem Baumarkt
