---
layout: "image"
title: "Mechanik der vertikalen Walzen"
date: "2017-05-21T13:53:33"
picture: "pwa7.jpg"
weight: "7"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45919
- /detailsbcc3.html
imported:
- "2019"
_4images_image_id: "45919"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45919 -->
Mechanik, um die vertikalen Walzen zu verstellen
