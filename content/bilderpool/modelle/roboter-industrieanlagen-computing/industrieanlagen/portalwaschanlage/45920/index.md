---
layout: "image"
title: "Linearantrieb"
date: "2017-05-21T13:53:33"
picture: "pwa8.jpg"
weight: "8"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45920
- /detailsd56e.html
imported:
- "2019"
_4images_image_id: "45920"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45920 -->
Mechanik des Linearantriebs und Blick auf die Steuereinheit
