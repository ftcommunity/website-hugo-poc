---
layout: "image"
title: "Steuerungseinheit"
date: "2017-05-21T13:53:33"
picture: "pwa6.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45918
- /details3db9.html
imported:
- "2019"
_4images_image_id: "45918"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45918 -->
Die Steuerungseinheit besteht aus 2 TX Controllern
