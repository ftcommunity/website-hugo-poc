---
layout: "image"
title: "Erfassung der Fahrzeugbreite"
date: "2017-05-21T13:53:33"
picture: "pwa5.jpg"
weight: "5"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45917
- /details6d78.html
imported:
- "2019"
_4images_image_id: "45917"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45917 -->
Der Ultraschallsensor erfasst die Fahrzeugbreit. Man geht davon aus, dass das Fahrzeug mittig auf der Spur steht
