---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage06.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage06.jpg"
weight: "5"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40165
- /details4cd9.html
imported:
- "2019"
_4images_image_id: "40165"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40165 -->
