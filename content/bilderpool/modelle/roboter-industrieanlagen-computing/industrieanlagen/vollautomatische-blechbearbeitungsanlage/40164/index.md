---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage05.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage05.jpg"
weight: "4"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40164
- /details42f0.html
imported:
- "2019"
_4images_image_id: "40164"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40164 -->
