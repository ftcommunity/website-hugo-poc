---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage19.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage19.jpg"
weight: "18"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40178
- /detailsf051-3.html
imported:
- "2019"
_4images_image_id: "40178"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40178 -->
