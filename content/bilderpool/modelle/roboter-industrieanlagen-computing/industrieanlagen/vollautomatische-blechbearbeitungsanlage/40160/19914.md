---
layout: "comment"
hidden: true
title: "19914"
date: "2015-01-04T08:08:13"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das ist eine sehr beeindruckende Anlage, aber ohne ein Wort der Beschreibung wird man die Arbeit und Mühe nicht würdigen können, die da drin steckt. Was ist das grüne da? Und wo sind die Bleche? 

Tipp: mit dem ft-Publisher (oben links unter "Bilderpool") kann man Fotos nachbearbeiten und z.B. die Ränder wegschneiden).

Gruß,
Harald