---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage24.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage24.jpg"
weight: "23"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40183
- /detailsc1a2.html
imported:
- "2019"
_4images_image_id: "40183"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40183 -->
