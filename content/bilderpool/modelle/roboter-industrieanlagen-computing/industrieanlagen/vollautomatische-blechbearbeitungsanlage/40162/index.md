---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage03.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage03.jpg"
weight: "3"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- /php/details/40162
- /detailsbf17.html
imported:
- "2019"
_4images_image_id: "40162"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40162 -->
