---
layout: "image"
title: "Starttaster"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik02.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14217
- /details3852.html
imported:
- "2019"
_4images_image_id: "14217"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14217 -->
Hier sieht man den Starttaster, den man betätigen muss das die Fabrik anfängt zu arbeiten.