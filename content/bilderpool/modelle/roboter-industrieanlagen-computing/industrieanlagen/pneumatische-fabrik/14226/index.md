---
layout: "image"
title: "Schieber"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik11.jpg"
weight: "11"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14226
- /details9760-2.html
imported:
- "2019"
_4images_image_id: "14226"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14226 -->
Hier sieht man den pneumatischen Schieber, der die Werkstücke herausholt. Sie fallen dann in ein Kästchen.