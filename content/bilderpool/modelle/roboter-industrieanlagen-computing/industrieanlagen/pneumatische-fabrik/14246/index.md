---
layout: "image"
title: "Magnetventile"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik31.jpg"
weight: "31"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14246
- /details6635-2.html
imported:
- "2019"
_4images_image_id: "14246"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14246 -->
Hier die anderen zwei Magnetventile und nochmal den Summer.