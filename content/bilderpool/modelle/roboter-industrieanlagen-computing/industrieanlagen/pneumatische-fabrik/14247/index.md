---
layout: "image"
title: "Magnetventile"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik32.jpg"
weight: "32"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14247
- /details4e5e.html
imported:
- "2019"
_4images_image_id: "14247"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14247 -->
Nochmal die Magnetventile.