---
layout: "image"
title: "Ampel"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik25.jpg"
weight: "25"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14240
- /detailsab98.html
imported:
- "2019"
_4images_image_id: "14240"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14240 -->
Jetzt ist die Ampel rot.