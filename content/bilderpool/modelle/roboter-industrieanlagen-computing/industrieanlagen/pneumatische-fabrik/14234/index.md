---
layout: "image"
title: "Kästchen-Wechsler"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik19.jpg"
weight: "19"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14234
- /details7804-2.html
imported:
- "2019"
_4images_image_id: "14234"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14234 -->
Hier sieht man den Kästchen-Wechsler. Ihn kann man mit Knöpfen im Bedienfeld steuern.