---
layout: "image"
title: "Not-Aus-Buzzer + Wasserflasche"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat05.jpg"
weight: "5"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33458
- /details1a0a.html
imported:
- "2019"
_4images_image_id: "33458"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33458 -->
Hier ist der Not-Stopp-Schalter zu sehen, dahinter die Wasserflasche. Links sieht man den Abstandssensor zur Füllstandsmessung im Becher.