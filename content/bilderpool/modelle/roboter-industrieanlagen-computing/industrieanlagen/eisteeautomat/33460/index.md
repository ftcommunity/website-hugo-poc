---
layout: "image"
title: "Eistee wird ausgekippt"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat07.jpg"
weight: "7"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33460
- /details578e.html
imported:
- "2019"
_4images_image_id: "33460"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33460 -->
Hier vorne gehört normalerweise ein Trichter hinein, aber Edelstahl spiegelt zusehr, weshalb ich ihn weggelassen habe.