---
layout: "comment"
hidden: true
title: "15699"
date: "2011-11-15T09:27:45"
uploadBy:
- "scripter1"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

Erstmal Danke für die netten Worte und den Verbesserungsvorschlag - werde ich auf jeden Fall umsetzen. Vielleicht probiere ich es auch mal mit einem neuen XM-Motor, den ich mir kürzlich zugelegt habe.

Bei der Füllstands-Prüfung gab es leider ein paar Probleme mit den kleinen Bechern, der Ultraschallsensor, der schon eine Genauigkeit von 1 cm hat, hat sich da leider manchmal vermessen. Eventuell mache ich es mit 2 Drähten, die vorher in den Becher "fahren" und dann rechtzeigig abschalten, das würde dann aber nicht mehr so faszinierend aussehen ;-)

Bei der Schienenstrecke habe ich mich an die Laufkatze aus dem Super-Cranes-Baukasten gehalten...

Viele Grüße
Lukas