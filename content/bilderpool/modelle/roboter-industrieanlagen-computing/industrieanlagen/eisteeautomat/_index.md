---
layout: "overview"
title: "Eisteeautomat"
date: 2020-02-22T08:04:21+01:00
legacy_id:
- /php/categories/2480
- /categories5911-2.html
- /categories1ec7.html
- /categoriesf931.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2480 --> 
Hier stelle ich euch meinen Eisteeautomat vor, der auch auf der Convention zu sehen war. Ich habe vor, alle 1-3 Monate neue Bilder zu veröffentlichen. Nähere Beschreibungen und die jeweiligen Funktionsweisen findet ihr in der Beschreibung der Bilder.