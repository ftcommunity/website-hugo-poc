---
layout: "image"
title: "Gesamt"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat12.jpg"
weight: "12"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33465
- /details5e2a-2.html
imported:
- "2019"
_4images_image_id: "33465"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33465 -->
Folgende Teile wurden verbaut:

5 Grundplatten (eine ist unten drinn festgeschraubt)
3 Power-Motoren (1x 8:1, 2x 50:1)
2 S-Motoren
5 Lampen
2 Fototransistoren
6 Taster + 1 Reedkontakt (der 2. ist mir leider verloren gegangen :( )
2 Magnetventile
1 Interface
1 Erweiterungsmodul
1 Trafo aus dem Energy Set

Folgendes kommt bald hinzu:

Arduino Uno
ein paar Lampen werden durch LEDs ersetzt
LCD
1 Magnetventil für den Vakuumsauger
1 XM-Motor
1 Power Motor (wahrscheinlich 20:1)
Steuerungsplatine (Transistoren usw...)