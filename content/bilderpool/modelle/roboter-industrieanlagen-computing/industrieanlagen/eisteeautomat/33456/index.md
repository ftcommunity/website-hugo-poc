---
layout: "image"
title: "Ansicht von links"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat03.jpg"
weight: "3"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- /php/details/33456
- /details8f72.html
imported:
- "2019"
_4images_image_id: "33456"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33456 -->
Von der anderen Seite auch ohne Wasserflasche.