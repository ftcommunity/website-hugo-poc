---
layout: "image"
title: "Kompressor"
date: "2005-06-12T17:43:16"
picture: "Kompr.jpg"
weight: "1"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4425
- /details7675.html
imported:
- "2019"
_4images_image_id: "4425"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4425 -->
Drucklufterzeugung.Abschaltung über einen einstellbaren Druckschalter.
