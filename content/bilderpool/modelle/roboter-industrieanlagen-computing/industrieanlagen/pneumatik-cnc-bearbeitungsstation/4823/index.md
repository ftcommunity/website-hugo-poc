---
layout: "image"
title: "Peumatik Presse 1"
date: "2005-09-25T14:03:05"
picture: "PneuPresse.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4823
- /details8f04-2.html
imported:
- "2019"
_4images_image_id: "4823"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-09-25T14:03:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4823 -->
