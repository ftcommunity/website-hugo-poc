---
layout: "image"
title: "Ansicht von oben"
date: "2005-06-12T18:01:17"
picture: "Gesamt.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4443
- /detailse333-3.html
imported:
- "2019"
_4images_image_id: "4443"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T18:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4443 -->
Gesamtansicht von oben. Gesteuert wird das Modell über eine S7 315-2DP mit dezentralen Ein und Ausgängen. Die Bedienung erfolgt über eine PC-Visualisierung mit Touch.
17 Motoren, 64 Eing., 64 Ausg., 20 Leuchtm.
und diverse Weggeber und Endschalter sind Programmtechnisch zu verarbeiten.