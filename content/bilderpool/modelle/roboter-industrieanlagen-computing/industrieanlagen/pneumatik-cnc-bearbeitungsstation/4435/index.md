---
layout: "image"
title: "CNC-Fräse"
date: "2005-06-12T17:43:32"
picture: "CNC-Frse1.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4435
- /details91fa-2.html
imported:
- "2019"
_4images_image_id: "4435"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T17:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4435 -->
CNC-Fäse mit Werkzeugwechsel