---
layout: "image"
title: "Motorsteuerkarte"
date: "2005-06-12T18:01:17"
picture: "Relais.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4442
- /detailsef89-2.html
imported:
- "2019"
_4images_image_id: "4442"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T18:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4442 -->
Relaiskarten für die Motoren mit zwei Laufrichtungen.