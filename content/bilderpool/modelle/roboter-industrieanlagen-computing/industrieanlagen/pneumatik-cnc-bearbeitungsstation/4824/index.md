---
layout: "image"
title: "Pneumatik Presse 2"
date: "2005-09-25T14:03:05"
picture: "PneuPresse1.jpg"
weight: "29"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4824
- /detailsb4ab-2.html
imported:
- "2019"
_4images_image_id: "4824"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-09-25T14:03:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4824 -->
