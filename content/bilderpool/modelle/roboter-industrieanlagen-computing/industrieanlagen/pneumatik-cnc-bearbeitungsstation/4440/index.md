---
layout: "image"
title: "Roboter Rückführung"
date: "2005-06-12T18:01:17"
picture: "RobRck.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- /php/details/4440
- /details3870-2.html
imported:
- "2019"
_4images_image_id: "4440"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T18:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4440 -->
Der Roboter schließt den Kreislauf und führt das Produkt aus der Qualitätskontrolle zurück in den Kreislauf.
Dadurch ist ein Umlauf im Modell gegeben.