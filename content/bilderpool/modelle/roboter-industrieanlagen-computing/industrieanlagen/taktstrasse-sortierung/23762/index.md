---
layout: "image"
title: "Taktstraße mit Sortierung 07"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung07.jpg"
weight: "7"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23762
- /detailsbbc9-2.html
imported:
- "2019"
_4images_image_id: "23762"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23762 -->
Ein Blick von links die Straße entlang :)