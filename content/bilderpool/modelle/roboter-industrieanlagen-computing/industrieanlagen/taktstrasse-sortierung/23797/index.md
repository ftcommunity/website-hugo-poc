---
layout: "image"
title: "Taktstraße mit Sortierung 42"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung42.jpg"
weight: "42"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23797
- /detailse182.html
imported:
- "2019"
_4images_image_id: "23797"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23797 -->
Nahaufnahme der Bohrstation.

Diese wird mit einem kleinen Zylinder auf der linken Seite über ein Magnetventil angesteuert und hebt bzw. senkt den Bohrer.

Der Taster auf der Rückseite schaltet den Motor des Bohrers bei gesenktem Bohrer ein bzw. bei angehobenen wieder aus.