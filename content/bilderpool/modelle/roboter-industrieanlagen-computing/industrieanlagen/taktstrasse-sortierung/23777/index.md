---
layout: "image"
title: "Taktstraße mit Sortierung 22"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung22.jpg"
weight: "22"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23777
- /details239c-2.html
imported:
- "2019"
_4images_image_id: "23777"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23777 -->
Interfaceverkabelung Vorderseite