---
layout: "image"
title: "Taktstraße mit Sortierung 24"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung24.jpg"
weight: "24"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23779
- /details93fa.html
imported:
- "2019"
_4images_image_id: "23779"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23779 -->
Kabelmanagement sowie Rückseite der Bohr- und Schweißstation