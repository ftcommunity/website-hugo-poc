---
layout: "image"
title: "Taktstraße mit Sortierung 06"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung06.jpg"
weight: "6"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23761
- /details8adb-2.html
imported:
- "2019"
_4images_image_id: "23761"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23761 -->
Nahaufnahme der beiden Behälter/Fächer für die Aussortierung der Werkstücke.

In der Mitte kann man die beiden Taster für die Pneumatikgreifer-Position von Fach 1 und 2 erkennen, sowie teilweise die Vakuumpumpe.