---
layout: "image"
title: "Taktstraße mit Sortierung 28"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung28.jpg"
weight: "28"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23783
- /details3972.html
imported:
- "2019"
_4images_image_id: "23783"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23783 -->
Der Straße entlang, von links nach rechts.