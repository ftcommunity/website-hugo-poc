---
layout: "image"
title: "Taktstraße mit Sortierung 14"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung14.jpg"
weight: "14"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23769
- /details3f69.html
imported:
- "2019"
_4images_image_id: "23769"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23769 -->
Putz- und Bohrstation von rechts oben.