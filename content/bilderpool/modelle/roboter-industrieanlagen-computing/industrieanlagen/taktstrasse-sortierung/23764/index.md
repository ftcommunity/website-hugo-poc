---
layout: "image"
title: "Taktstraße mit Sortierung 09"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung09.jpg"
weight: "9"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23764
- /details4fc6.html
imported:
- "2019"
_4images_image_id: "23764"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23764 -->
Nahaufnahme der Putzstation.

Rechts im Bild kann man teilweise noch den Pneumatischen Schieber erkennen.