---
layout: "comment"
hidden: true
title: "9067"
date: "2009-04-27T09:38:01"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Tolles Modell!!! Und auch optisch so ansprechend, klasse!

Hast Du alle Kabel und Schläuche speziell für dieses Modell auf die exakt passende Länge zurechtgeschnitten? Respekt! Was für ein Aufwand!

Ich verwende immer Standard-Kabel und -Schläuche, und habe immer störende und unpassende Überlängen...

Gruß, Thomas