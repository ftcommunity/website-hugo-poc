---
layout: "image"
title: "Taktstraße mit Sortierung 36"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung36.jpg"
weight: "36"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23791
- /details6cec.html
imported:
- "2019"
_4images_image_id: "23791"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23791 -->
