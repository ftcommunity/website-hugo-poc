---
layout: "image"
title: "Taktstraße mit Sortierung 47"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung47.jpg"
weight: "47"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23802
- /details9431-2.html
imported:
- "2019"
_4images_image_id: "23802"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23802 -->
