---
layout: "image"
title: "Taktstraße mit Sortierung 40"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung40.jpg"
weight: "40"
konstrukteure: 
- "tz"
fotografen:
- "tz"
uploadBy: "tz"
license: "unknown"
legacy_id:
- /php/details/23795
- /detailsc4eb.html
imported:
- "2019"
_4images_image_id: "23795"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23795 -->
