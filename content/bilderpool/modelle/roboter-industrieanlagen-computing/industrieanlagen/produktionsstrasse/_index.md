---
layout: "overview"
title: "Produktionsstraße"
date: 2020-02-22T08:04:35+01:00
legacy_id:
- /php/categories/3245
- /categoriesd41a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3245 --> 
Eine einfache Produktionstraße, die auf eine Bauplatte 500 passt.

Aus einem Auswurf fallen die Werkstücke aufs Förderband und werden zur ersten Station transportiert. Dort werden die Steine gleichmäßig "besprüht", dargestellt durch eine bewegliche Druckluft-Düse.
Dann werden sie in der nächsten Station "gebacken" und fallen danach in einen Auswurf.
