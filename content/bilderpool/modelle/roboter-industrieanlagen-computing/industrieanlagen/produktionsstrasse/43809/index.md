---
layout: "image"
title: "Der Auswurf"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse6.jpg"
weight: "6"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/43809
- /detailsc7f0.html
imported:
- "2019"
_4images_image_id: "43809"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43809 -->
Hier fallen die fertigen Werkstücke rein und rutschen ans Ende des Auswurfs, wo sie dann entnommen werden können.