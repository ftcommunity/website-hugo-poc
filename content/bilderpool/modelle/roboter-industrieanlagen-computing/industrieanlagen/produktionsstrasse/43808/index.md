---
layout: "image"
title: "Der 'Ofen'"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse5.jpg"
weight: "5"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- /php/details/43808
- /details6709.html
imported:
- "2019"
_4images_image_id: "43808"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43808 -->
Der "Ofen" ist relativ unspektakulär, auf beiden Seiten hängen damit es besser aussieht je drei kurze Ketten herab. Man hätte auch noch eine Lampe im Ofen einbauen können, aber ich hab sie weggelassen.