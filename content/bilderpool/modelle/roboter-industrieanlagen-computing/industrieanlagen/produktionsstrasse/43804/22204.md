---
layout: "comment"
hidden: true
title: "22204"
date: "2016-07-05T13:22:12"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
@ Stefan:
In den BS30 hängen die Ketten. Mit WT30 wird das daher wohl nicht gehen.

@ ALL:
Aber wenn in der Mitte unter den Bauplatten nichts ist, dann könnte die Lampe auf einer Seite an einen BS30 dran, die Stecker nach links oder rechts oder die Kabel direkt mit dem Leuchtstein-Stopfen 36495 im Lampensockel kontaktiert und gesichert.

Irgendjemand hat im Pool auch schon Bilder eingestellt, in denen gezeigt wird, wie das ersatzweise auch mit den Röhrchen von Wattestäbchen funktioniert - ich finde das leider nicht; eventuell war es aber auch in einer ft:pedia ...

Grüße
H.A.R.R.Y