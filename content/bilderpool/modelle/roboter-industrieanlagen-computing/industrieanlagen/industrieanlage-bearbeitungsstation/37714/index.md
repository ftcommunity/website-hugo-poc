---
layout: "image"
title: "Ansicht Ofen"
date: "2013-10-12T14:55:42"
picture: "industrieanlage14.jpg"
weight: "14"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37714
- /details4493.html
imported:
- "2019"
_4images_image_id: "37714"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37714 -->
Von dieser Seite kommen die Werkstücke in den Ofen. Innen sind zwei Lampen und ein Temperatursensor befestigt.