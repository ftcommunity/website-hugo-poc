---
layout: "image"
title: "Übersicht"
date: "2013-10-12T14:55:42"
picture: "industrieanlage01.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37701
- /details18e2-2.html
imported:
- "2019"
_4images_image_id: "37701"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37701 -->
Zu erkennen ist die obere Ebene mit dem Magazin, der ersten Bearbeitungsstation (nicht besetzt), der zweiten Bearbeitungsstation (Presse), dem fahrbahren Fließband und dem Brennofen samt Rollenband.