---
layout: "image"
title: "Übersicht"
date: "2013-10-12T14:55:42"
picture: "industrieanlage02.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37702
- /details81d7-2.html
imported:
- "2019"
_4images_image_id: "37702"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37702 -->
Schräg fotografiert. Hier kann man den Ofen und das Rollenband erkennen. Am Ende des Rollenbandes befindet sich ein Motor welcher die Aufgabe hat die Werkstücke abzubremsen. 
Links vom Ofen befindet sich noch ein Band welches als Zwischenspeicher dient falls der Ofen noch nicht die geforderte Temperatur erreicht hat. (Zylinder wird ausgefahren).
Sollte das Band einmal voll sein, wird das von dem Fototransistor erkannt und es werden solange keine Werkstücke mehr nachgeliefert bis der Zwischenspeicher wieder geleert ist.