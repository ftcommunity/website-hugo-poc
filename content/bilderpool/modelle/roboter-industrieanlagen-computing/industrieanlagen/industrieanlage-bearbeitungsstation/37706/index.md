---
layout: "image"
title: "Ansicht Stanze"
date: "2013-10-12T14:55:42"
picture: "industrieanlage06.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37706
- /detailsa6f5-3.html
imported:
- "2019"
_4images_image_id: "37706"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37706 -->
Die Rückseite der Stanze.