---
layout: "image"
title: "Übersicht"
date: "2013-10-12T14:55:42"
picture: "industrieanlage04.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37704
- /details186b.html
imported:
- "2019"
_4images_image_id: "37704"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37704 -->
Auf diesem Foto kann man den Fertigungsteil bestehend aus einem Magazin, einer Stanze und dem fahrbahren Fließband sehen. Links von der Stanze ist noch Platz für eine weitere Bearbeitungsstation.