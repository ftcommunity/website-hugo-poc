---
layout: "image"
title: "Detail fahrbahres Fließband"
date: "2013-10-12T14:55:42"
picture: "industrieanlage08.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37708
- /detailsd072.html
imported:
- "2019"
_4images_image_id: "37708"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37708 -->
Erneute Detailansicht des Fließbandes.