---
layout: "image"
title: "Detail Rollenband"
date: "2013-10-12T14:55:42"
picture: "industrieanlage16.jpg"
weight: "16"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37716
- /details48e4.html
imported:
- "2019"
_4images_image_id: "37716"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37716 -->
Hier erkennt man das Rollenband welches dazu dient die Werkstücke nach dem Brennen abzukühlen. Der Apperat Links ist dazu da um die Werkstücke auf die eingestellte Geschwindigkeit herabzubremsen.