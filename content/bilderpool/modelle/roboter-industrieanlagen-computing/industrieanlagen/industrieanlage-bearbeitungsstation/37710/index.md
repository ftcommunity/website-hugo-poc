---
layout: "image"
title: "Detail Antrieb"
date: "2013-10-12T14:55:42"
picture: "industrieanlage10.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37710
- /details4f84.html
imported:
- "2019"
_4images_image_id: "37710"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37710 -->
Hier erkennt man den Power Motor welcher das Fließband verschiebt.