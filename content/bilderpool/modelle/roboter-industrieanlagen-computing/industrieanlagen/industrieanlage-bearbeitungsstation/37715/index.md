---
layout: "image"
title: "Ansicht Ofen"
date: "2013-10-12T14:55:42"
picture: "industrieanlage15.jpg"
weight: "15"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/37715
- /detailsb5ff-3.html
imported:
- "2019"
_4images_image_id: "37715"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37715 -->
Hier sieht man den Ausgang des Brennofens.