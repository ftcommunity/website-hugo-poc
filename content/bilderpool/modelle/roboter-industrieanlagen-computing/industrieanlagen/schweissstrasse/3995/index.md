---
layout: "image"
title: "Schweißstraße14"
date: "2005-04-17T11:49:02"
picture: "schweistrae14.jpg"
weight: "14"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3995
- /details336d.html
imported:
- "2019"
_4images_image_id: "3995"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3995 -->
Schweißstraße mit vier 3-Achs-Robotern
