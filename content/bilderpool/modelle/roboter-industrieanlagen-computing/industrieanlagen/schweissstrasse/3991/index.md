---
layout: "image"
title: "Schweißstraße10"
date: "2005-04-17T11:48:45"
picture: "schweistrae10.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3991
- /detailsc25b.html
imported:
- "2019"
_4images_image_id: "3991"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3991 -->
Schweißstraße mit vier 3-Achs-Robotern
