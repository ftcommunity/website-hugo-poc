---
layout: "image"
title: "Schweißstraße05"
date: "2005-04-17T11:48:45"
picture: "schweistrae05.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3986
- /details6805-3.html
imported:
- "2019"
_4images_image_id: "3986"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3986 -->
Schweißstraße mit vier 3-Achs-Robotern
