---
layout: "image"
title: "Schweißstraße12"
date: "2005-04-17T11:49:02"
picture: "schweistrae12.jpg"
weight: "12"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3993
- /details8fb2.html
imported:
- "2019"
_4images_image_id: "3993"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3993 -->
Schweißstraße mit vier 3-Achs-Robotern
