---
layout: "image"
title: "Schweißstraße06"
date: "2005-04-17T11:48:45"
picture: "schweistrae06.jpg"
weight: "6"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3987
- /details22c3-3.html
imported:
- "2019"
_4images_image_id: "3987"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3987 -->
Schweißstraße mit vier 3-Achs-Robotern
