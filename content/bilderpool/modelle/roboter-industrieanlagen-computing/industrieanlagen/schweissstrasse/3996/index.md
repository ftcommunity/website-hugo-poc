---
layout: "image"
title: "Schweißstraße15"
date: "2005-04-17T11:49:02"
picture: "schweistrae15.jpg"
weight: "15"
konstrukteure: 
- "Martin Romann (Remadus)"
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/3996
- /detailsb914.html
imported:
- "2019"
_4images_image_id: "3996"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3996 -->
Schweißstraße mit vier 3-Achs-Robotern
