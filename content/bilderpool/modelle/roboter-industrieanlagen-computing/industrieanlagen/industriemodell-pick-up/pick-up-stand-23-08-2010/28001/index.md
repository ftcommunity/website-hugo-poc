---
layout: "image"
title: "Pick-up mit Förderband"
date: "2010-08-28T14:01:14"
picture: "pickup06.jpg"
weight: "6"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28001
- /details4942-2.html
imported:
- "2019"
_4images_image_id: "28001"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28001 -->
Hier könnt ihr das Förderband von vorne bis nach hinten sehen.
