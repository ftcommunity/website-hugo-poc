---
layout: "image"
title: "Hauptschalter"
date: "2010-08-28T14:01:14"
picture: "pickup04.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27999
- /details6d13-3.html
imported:
- "2019"
_4images_image_id: "27999"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27999 -->
Hier könnt ihr den Hauptschalter sehen, durch ihn kann man die komplette Anlage ein- bzw. ausschalten.
