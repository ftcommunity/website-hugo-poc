---
layout: "comment"
hidden: true
title: "12068"
date: "2010-08-30T16:18:21"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
In der Beschreibung steht doch, das auf dieser Tonne jetzt keine Unterlegscheibe geklebt ist, weil sie abgefallen ist, aber der E-Magnet zieht ja Metall an, und so kann er dann die Tonne mit der Scheibe hochheben.

MfG
Endlich