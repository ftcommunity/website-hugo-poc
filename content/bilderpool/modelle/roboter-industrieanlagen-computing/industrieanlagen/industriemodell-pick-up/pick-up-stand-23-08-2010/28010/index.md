---
layout: "image"
title: "Pick up"
date: "2010-08-28T14:01:14"
picture: "pickup15.jpg"
weight: "15"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28010
- /details70b0-2.html
imported:
- "2019"
_4images_image_id: "28010"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28010 -->
... und greift sie sich.
