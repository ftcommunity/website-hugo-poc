---
layout: "image"
title: "Motor"
date: "2010-08-28T14:01:14"
picture: "pickup08.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28003
- /detailsa368.html
imported:
- "2019"
_4images_image_id: "28003"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28003 -->
Hier könnt ihr den Motor sehen, der den Pick-up drehen lässt.
