---
layout: "image"
title: "Motor Förderband"
date: "2010-08-28T14:01:14"
picture: "pickup05.jpg"
weight: "5"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28000
- /details36be-2.html
imported:
- "2019"
_4images_image_id: "28000"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28000 -->
Hier seht ihr den Mini-Motor, der das Förderband laufen lässt.
