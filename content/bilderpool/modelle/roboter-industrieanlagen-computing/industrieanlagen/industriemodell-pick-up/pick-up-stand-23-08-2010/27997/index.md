---
layout: "image"
title: "Pick-up mit Förderband"
date: "2010-08-28T14:01:13"
picture: "pickup02.jpg"
weight: "2"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27997
- /details185f.html
imported:
- "2019"
_4images_image_id: "27997"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27997 -->
Hier könnt ihr das Förderband sehen. Wenn eine Tonne draufgestellt wird, erkennt die Lichtschranke die Tonne und fährt zur 2. Lichtschranke. Wenn die signalisiert, das eine Tonne da steht, kommt der Pick-up holt sie , und stellt sie dann ab.
