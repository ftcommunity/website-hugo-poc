---
layout: "image"
title: "Industriemodell Pick-up"
date: "2010-08-26T17:40:16"
picture: "pickup5.jpg"
weight: "5"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27960
- /details042e.html
imported:
- "2019"
_4images_image_id: "27960"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27960 -->
Hier kann man den E-Magnet gut sehen.  Und dass sich der obere Drehkranz um 360° drehen kann. Und auch der Taster, der signalisieren soll ob eine Tonne am Magneten hängt.
