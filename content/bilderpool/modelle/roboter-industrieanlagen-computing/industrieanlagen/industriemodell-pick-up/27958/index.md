---
layout: "image"
title: "Industriemodell Pick-up"
date: "2010-08-26T17:40:16"
picture: "pickup3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27958
- /details5c46-2.html
imported:
- "2019"
_4images_image_id: "27958"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27958 -->
Hier kann man den Pick-up von der Seite sehen, wie er gerade die Tonne auf dem Tisch abstellt.
