---
layout: "image"
title: "Förderband für Pick-up"
date: "2010-08-26T17:40:16"
picture: "pickup2.jpg"
weight: "2"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27957
- /detailsc8e5-2.html
imported:
- "2019"
_4images_image_id: "27957"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27957 -->
Das ist das Förderband auf dem die Tonnen anfahren.
