---
layout: "image"
title: "3D-Drucker Poederoyen-2012"
date: "2012-05-05T13:12:25"
picture: "ddruckerpoederoyen18.jpg"
weight: "18"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/34874
- /detailscae7.html
imported:
- "2019"
_4images_image_id: "34874"
_4images_cat_id: "2582"
_4images_user_id: "22"
_4images_image_date: "2012-05-05T13:12:25"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34874 -->
3D-Drucken  2x  "Schnee"  + "Opel"


Schau auch :
http://reprap.org/mediawiki/index.php?title=FTIStrap&oldid=57809
