---
layout: "image"
title: "3D Drucker"
date: "2014-12-19T11:56:14"
picture: "ddrucker05.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39933
- /details14ac.html
imported:
- "2019"
_4images_image_id: "39933"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2014-12-19T11:56:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39933 -->
Der neue Druckkopf macht einfach einen irre Unterschied. An die, die auch einen bauen wollen: Daran nicht sparen!