---
layout: "image"
title: "BS30"
date: "2015-02-17T21:23:03"
picture: "drucker2.jpg"
weight: "17"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/40559
- /details7a01.html
imported:
- "2019"
_4images_image_id: "40559"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40559 -->
