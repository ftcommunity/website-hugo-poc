---
layout: "image"
title: "Spiral Vase"
date: "2015-02-17T21:23:03"
picture: "drucker9.jpg"
weight: "24"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/40566
- /details9604.html
imported:
- "2019"
_4images_image_id: "40566"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40566 -->
Hier neben dem LED Strahler als Größenvergleich.
Leider nicht Wasserdicht, hätte mehr Materialvorschub nehmen müssen. So kann man jetzt einfach ein Reagenzglas drin versenken.