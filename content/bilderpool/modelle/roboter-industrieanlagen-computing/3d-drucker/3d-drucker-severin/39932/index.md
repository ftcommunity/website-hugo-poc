---
layout: "image"
title: "3D Drucker"
date: "2014-12-19T11:56:14"
picture: "ddrucker04.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39932
- /detailsba6f.html
imported:
- "2019"
_4images_image_id: "39932"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2014-12-19T11:56:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39932 -->
Portal weighted compation cube im Druck