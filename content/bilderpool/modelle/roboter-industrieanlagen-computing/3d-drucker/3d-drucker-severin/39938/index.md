---
layout: "image"
title: "3D Drucker"
date: "2014-12-19T11:56:14"
picture: "ddrucker10.jpg"
weight: "10"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39938
- /detailsa8bb.html
imported:
- "2019"
_4images_image_id: "39938"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2014-12-19T11:56:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39938 -->
