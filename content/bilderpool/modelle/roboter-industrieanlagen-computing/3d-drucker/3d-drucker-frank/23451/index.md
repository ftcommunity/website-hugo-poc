---
layout: "image"
title: "3D-Drucker13"
date: "2009-03-14T20:33:25"
picture: "PICT4675.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23451
- /details4b7a-2.html
imported:
- "2019"
_4images_image_id: "23451"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23451 -->
Detail der X und Y-Achsen