---
layout: "image"
title: "3D-Drucker09"
date: "2009-03-14T20:33:25"
picture: "PICT4671.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23447
- /details1344.html
imported:
- "2019"
_4images_image_id: "23447"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23447 -->
Ansicht von vorne

Der Drucker ist 30 mm in der Höhe gewachsen, die Verfahrwege betragen 120 mm für alle Achsen.