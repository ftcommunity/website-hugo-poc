---
layout: "image"
title: "3D-Drucker15"
date: "2009-03-14T20:33:25"
picture: "PICT4677.jpg"
weight: "15"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
schlagworte: ["Frank", "Jakob", "3D-Drucker"]
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23453
- /details20b1.html
imported:
- "2019"
_4images_image_id: "23453"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23453 -->
Detail der X und Y-Achsen