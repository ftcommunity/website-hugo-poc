---
layout: "image"
title: "3D-Drucker06"
date: "2009-03-09T08:11:10"
picture: "ddruckerfrank6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23439
- /detailsd839.html
imported:
- "2019"
_4images_image_id: "23439"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-09T08:11:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23439 -->
Detail der X und Y-Achsen