---
layout: "image"
title: "3D-Drucker03"
date: "2009-03-09T08:11:10"
picture: "ddruckerfrank3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23436
- /detailsf76b.html
imported:
- "2019"
_4images_image_id: "23436"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-09T08:11:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23436 -->
Ansicht von hinten