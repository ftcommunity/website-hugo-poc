---
layout: "image"
title: "3D-Drucker11"
date: "2009-03-14T20:33:25"
picture: "PICT4673.jpg"
weight: "11"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/23449
- /details34fe-2.html
imported:
- "2019"
_4images_image_id: "23449"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23449 -->
Ansicht von hinten