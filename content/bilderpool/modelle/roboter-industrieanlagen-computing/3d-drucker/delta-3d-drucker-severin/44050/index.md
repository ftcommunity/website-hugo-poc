---
layout: "image"
title: "Kabelschläuche und Heizbett"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker05.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44050
- /detailsea86.html
imported:
- "2019"
_4images_image_id: "44050"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44050 -->
Die Kabelschläuche kamen erst später und wirken sich sehr positiv auf die Optik aus