---
layout: "image"
title: "Bedienoberfläche"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker12.jpg"
weight: "12"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44057
- /detailsd841.html
imported:
- "2019"
_4images_image_id: "44057"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44057 -->
Auf dem kleinen Pi mit DSI Touchscreen läuft OctoPi. Dadurch kann ich sowohl am Touchscreen, als auch an einem PC im Netzwerk, den Drucker steuern. Die Kamera bietet dabei Liveview und Zeitraffer aufnahmen des Drucks