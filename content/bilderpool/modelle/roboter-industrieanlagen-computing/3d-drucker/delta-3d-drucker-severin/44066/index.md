---
layout: "image"
title: "Der ganze Drucker"
date: "2016-07-31T17:44:19"
picture: "deltaddrucker21.jpg"
weight: "21"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44066
- /details9551.html
imported:
- "2019"
_4images_image_id: "44066"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44066 -->
1m lange Alus und ganz schön schwer..