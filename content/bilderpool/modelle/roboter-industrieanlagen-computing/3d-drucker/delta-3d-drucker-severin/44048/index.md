---
layout: "image"
title: "Riemenantrieb"
date: "2016-07-31T17:44:03"
picture: "deltaddrucker03.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44048
- /details5593.html
imported:
- "2019"
_4images_image_id: "44048"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44048 -->
Die Motoren können in Ecke hinein gesteckt werden und sind direkt betriebsbereit.