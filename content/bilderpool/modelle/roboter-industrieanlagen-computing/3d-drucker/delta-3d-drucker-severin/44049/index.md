---
layout: "image"
title: "Riemenantrieb"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker04.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44049
- /detailscd6c-2.html
imported:
- "2019"
_4images_image_id: "44049"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44049 -->
Die Motorwelle wird gegenüber von einem Kugellager geführt um nicht die volle Kraft des Riemens aufnehmen zu müssen.