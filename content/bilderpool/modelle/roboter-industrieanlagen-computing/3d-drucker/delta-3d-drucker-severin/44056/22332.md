---
layout: "comment"
hidden: true
title: "22332"
date: "2016-07-31T20:39:25"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein 3D-Drucker druckt den nächsten? Hast Du den ersten "Deep Thought" genannt und den zweiten "Erde"? ;-)

Gruß,
Stefan