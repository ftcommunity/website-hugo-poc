---
layout: "image"
title: "Extruder"
date: "2009-02-26T14:40:27"
picture: "3D-Drucker_Extruder.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23225
- /details2e31.html
imported:
- "2019"
_4images_image_id: "23225"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-02-26T14:40:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23225 -->
Hier sieht man die "Innerei" einer Heißklebe-Pistole, eingebettet in Balsa-Holz.
Die Heißklebe-Sticks werden mit 2 gegenläufigen Walzen, die mit Sandpapier beklebt sind, dem Extruder zugeführt (Schrittmotorgesteuert).
Die Glasplatte unter dem Extruder wird schrittweise abgesenkt. Der Extruder kann in X und Y-Richtung bewegt werden .Auf diese Weise können dreidimensionale Objekte erstellt werden:
- Türkeile
- Tassen
- Trichter
- Abdeckungen
- Zahnräder
- uns vieles vieles mehr
