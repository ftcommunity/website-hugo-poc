---
layout: "image"
title: "Detail Trichter für Klebesticks"
date: "2009-03-09T18:52:33"
picture: "Detail_Trichter_fr_Klebesticks.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23443
- /details345c.html
imported:
- "2019"
_4images_image_id: "23443"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-03-09T18:52:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23443 -->
Kleine Verbesserung:
Durch diesen Trichter (2x Winkelstein 60 + 2x Platte 15x15) verhaken sich die Klebesticks nicht im Reservoir.
