---
layout: "image"
title: "Trichter von Oben"
date: "2009-03-07T20:06:53"
picture: "Trichter_Oben.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23418
- /details5e1e.html
imported:
- "2019"
_4images_image_id: "23418"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-03-07T20:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23418 -->
