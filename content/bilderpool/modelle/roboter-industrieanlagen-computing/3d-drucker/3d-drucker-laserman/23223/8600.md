---
layout: "comment"
hidden: true
title: "8600"
date: "2009-02-26T18:04:37"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Nun, es ist ein etwas modifiziertes Original. Am Original von Andreas Rozek bestehen die Eckstützen aus Statikteilen, hier sind es unten 180er? Alu-Profile. Das hat einen nicht unbedeutenden Einfluss auf die Stabilität des Gestellrahmens.
Wie lang sind bitte die Lineachsen X, Y und Z? Vielleicht werde ich das mal später als eine der Anwendungen in meine 3D-XYZ modifizieren.
Gruss, Udo2