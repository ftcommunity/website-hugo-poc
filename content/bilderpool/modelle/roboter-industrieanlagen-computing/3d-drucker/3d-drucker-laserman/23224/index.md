---
layout: "image"
title: "3D-Drucker hinten"
date: "2009-02-26T14:40:27"
picture: "3D-Drucker_Hinten.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23224
- /details4aac.html
imported:
- "2019"
_4images_image_id: "23224"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-02-26T14:40:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23224 -->
