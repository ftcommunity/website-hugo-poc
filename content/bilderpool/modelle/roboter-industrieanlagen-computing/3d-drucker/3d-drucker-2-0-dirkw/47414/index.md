---
layout: "image"
title: "3D-Drucker 2.0 schräg rechts"
date: "2018-04-16T19:24:07"
picture: "ddrucker03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47414
- /detailsa246.html
imported:
- "2019"
_4images_image_id: "47414"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47414 -->
