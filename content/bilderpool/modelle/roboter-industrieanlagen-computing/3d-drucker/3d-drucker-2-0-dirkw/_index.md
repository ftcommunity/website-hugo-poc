---
layout: "overview"
title: "3D-Drucker 2.0 (DirkW)"
date: 2020-02-22T08:09:19+01:00
legacy_id:
- /php/categories/3504
- /categoriesad96.html
- /categories7232.html
- /categories06a5.html
- /categories4292.html
- /categories4ee2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3504 --> 
Nach zwei Jahren intensiven Einsatzes des fischertechnik 3D-Printers wurden verschiedene Schwächen im Aufbau und der Druckqualität sichtbar. 
Deshalb beschloss ich, den 3D-Drucker mit verschiedenen Modifikationen neu zu konstruieren.

Grundlage für die Neukonstruktion waren, die von mir festgestellten baulichen Schwächen des 3D Printers:

- keine kompakte Bauform (mehrteiliger Aufbau)

- die Höhe des Druckbetts lässt sich nicht einstellen

- die Halterung für die Filamentrolle istnicht geeignet

- der 3D Printer lässt sich schwer reinigen

- das Einstellen der Z-Achse über die Schnecke ist problematisch

- die Abtriebshülse für den Schrittmotor (160549) der X-Achse ist nicht langlebig

- Die Achsen haben keine Kugellager

- zum Kühlen beim 3D-Druck fehlt ein Lüfter

- das Druckbett ist nicht ausreichend eben

- eine Beleuchtung fehlt

Der Drucker kann mit der ftcommunity-Firmware oder mit 3D-Print Control betrieben werden. Durch die Summe der verschiedenen baulichen
Veränderungen konnte der 3D-Druck erheblich verbessert werden.

Weitere Info zum 3D Drucker findet ihr in der ft:pedia 2018-1:

https://ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-1.pdf

oder unter YouTube:

https://www.youtube.com/watch?v=r0-PNF5eWyo
