---
layout: "image"
title: "3D-Drucker 2.0  oben"
date: "2018-04-16T19:24:12"
picture: "ddrucker15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47426
- /details2fd8-4.html
imported:
- "2019"
_4images_image_id: "47426"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47426 -->
In der Draufsicht kann man die beiden X- und Y-Achsen mit dem Kettenantrieb erkennen.
