---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:12"
picture: "ddrucker16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47427
- /detailse220.html
imported:
- "2019"
_4images_image_id: "47427"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47427 -->
