---
layout: "image"
title: "3D-Drucker 2.0 Aufhängung Z-Achse"
date: "2018-04-16T19:24:18"
picture: "ddrucker27.jpg"
weight: "27"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47438
- /details4ab8.html
imported:
- "2019"
_4images_image_id: "47438"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47438 -->
Nach vielen fehlgeschlagenen Versuchen mit 2 Fixpunkten, habe ich mich für 4 Fixpunkte beim Druckbett entschieden. Der Grund ist, das sich das Bett einfacher justieren lässt.
