---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:07"
picture: "ddrucker07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47418
- /details9acd.html
imported:
- "2019"
_4images_image_id: "47418"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47418 -->
Das Netzteil für das Heizbett wird über einen Wippenschalter 220 Volt eingeschaltet. 
Rechts der Kaltgerätestecker dafür.
