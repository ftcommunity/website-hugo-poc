---
layout: "image"
title: "Gehäuse I2C-Expander aus dem 3D-Drucker 2.0 Nuten"
date: "2018-04-16T20:48:11"
picture: "ddrucker1.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47447
- /details44e2.html
imported:
- "2019"
_4images_image_id: "47447"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T20:48:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47447 -->
