---
layout: "comment"
hidden: true
title: "8675"
date: "2009-03-05T17:18:25"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Werkstückprogramm mit Wegbedingungen für NC-Maschinen vgl. DIN 66025 T2. Da kann man ja alte Kenntnisse wieder auffrischen.