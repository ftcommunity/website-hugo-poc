---
layout: "image"
title: "1e Product “FT-3D-Drucker-Poederoyen-NL”:  Trechter"
date: "2009-03-04T21:18:57"
picture: "3D-Drucker-Poederoyen-NL-funnel-techter_033.jpg"
weight: "38"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23372
- /details30b0.html
imported:
- "2019"
_4images_image_id: "23372"
_4images_cat_id: "1585"
_4images_user_id: "22"
_4images_image_date: "2009-03-04T21:18:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23372 -->
