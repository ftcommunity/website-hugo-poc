---
layout: "image"
title: "“Fischertechnik-3D-Drucker-Poederoyen-NL”"
date: "2009-03-01T20:05:03"
picture: "3D-Drucker-Poederoyen-NL_006.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23320
- /details2717.html
imported:
- "2019"
_4images_image_id: "23320"
_4images_cat_id: "1585"
_4images_user_id: "22"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23320 -->
