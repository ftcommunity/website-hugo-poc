---
layout: "image"
title: "Achse 2 (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23750
- /details92f1.html
imported:
- "2019"
_4images_image_id: "23750"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23750 -->
Hier kann man Wunderbar die Mechanik erkennen