---
layout: "image"
title: "Achse 4 und 5 (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin7.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23754
- /details1543.html
imported:
- "2019"
_4images_image_id: "23754"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23754 -->
Das Handgelenk ist noch im sehr frühen Versuchsstadium. Ich möchte die 5. Achse auf jedenfall noch näher an die 4. Achse heranbekommen.