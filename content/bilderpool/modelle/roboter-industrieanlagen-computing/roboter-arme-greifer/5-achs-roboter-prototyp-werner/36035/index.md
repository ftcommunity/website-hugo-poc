---
layout: "image"
title: "Mechanik Hand"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner5.jpg"
weight: "5"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36035
- /details5111.html
imported:
- "2019"
_4images_image_id: "36035"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36035 -->
Die Messingadapter nutze ich hier als Gegengewicht der Hand
Drehen die beiden Motoren in gleiche Richtung dreht der Greifer
Drehen die beiden Motoren in verschiedene Richtungen knickt das Handgelenk
