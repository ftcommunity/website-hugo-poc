---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner1.jpg"
weight: "1"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36031
- /details250d.html
imported:
- "2019"
_4images_image_id: "36031"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36031 -->
Die schwarzen Statikstreben hinten müsst ihr euch wegdenken, die entlasten die Kette im Ruhezustand
