---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner8.jpg"
weight: "8"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36038
- /details41cf.html
imported:
- "2019"
_4images_image_id: "36038"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36038 -->
