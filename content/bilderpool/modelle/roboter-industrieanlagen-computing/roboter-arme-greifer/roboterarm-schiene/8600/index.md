---
layout: "image"
title: "4. Achse"
date: "2007-01-21T21:01:29"
picture: "roboterarm08.jpg"
weight: "8"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8600
- /detailscc7f-3.html
imported:
- "2019"
_4images_image_id: "8600"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8600 -->
Die 2. Achse zum bewegen des Arms und die minimotoren des Arms