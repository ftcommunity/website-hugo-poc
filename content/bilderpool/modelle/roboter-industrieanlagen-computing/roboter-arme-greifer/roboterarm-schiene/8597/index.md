---
layout: "image"
title: "Seitenansicht_unten"
date: "2007-01-21T15:27:21"
picture: "roboterarm05.jpg"
weight: "5"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8597
- /details44ad.html
imported:
- "2019"
_4images_image_id: "8597"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8597 -->
Man sieht den Motor für Bewegung auf der Schiene, den Endschalter und den Impulszähler.