---
layout: "image"
title: "unten_seite"
date: "2007-01-21T21:01:29"
picture: "roboterarm10.jpg"
weight: "10"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8602
- /details79a4.html
imported:
- "2019"
_4images_image_id: "8602"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8602 -->
unten_seite