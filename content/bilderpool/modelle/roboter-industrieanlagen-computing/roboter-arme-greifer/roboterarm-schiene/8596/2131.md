---
layout: "comment"
hidden: true
title: "2131"
date: "2007-01-22T15:01:20"
uploadBy:
- "Wert"
license: "unknown"
imported:
- "2019"
---
Alles unterhalb des Drehkranzes bewegt sich höchstens 2mm(durch die feste Konstruktion an der Schiene). Wenn der Arm waagrecht steht, und man den Greifer antippt, kann er aber auch schon mal so 2cm nach unten Schwanken, kommt aber wieder nach oben. Die Kette ist so fest wie (ohne reißen) möglich gespannt. Die Z15 sind übrigens mit einem papierschnipsel dazwischen an der Achse befestigt. Ich hatte auch zuerst die 31053 ausprobiert, aber ich hatte nur 80cm Gesamtlänge von ihnen.