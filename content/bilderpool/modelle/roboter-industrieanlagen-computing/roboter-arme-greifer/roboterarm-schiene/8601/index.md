---
layout: "image"
title: "Greifer"
date: "2007-01-21T21:01:29"
picture: "roboterarm09.jpg"
weight: "9"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8601
- /details7b23.html
imported:
- "2019"
_4images_image_id: "8601"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8601 -->
der Greifer