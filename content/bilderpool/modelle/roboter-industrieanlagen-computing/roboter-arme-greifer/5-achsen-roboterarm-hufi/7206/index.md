---
layout: "image"
title: "6AX V2 Flachkabel in Nut verlegt"
date: "2006-10-21T22:26:57"
picture: "DSC03520.jpg"
weight: "31"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7206
- /details858f.html
imported:
- "2019"
_4images_image_id: "7206"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-10-21T22:26:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7206 -->
8 poliges Flachkabel mit 1mm Pitch lässt sich perfekt in die Nut der FT Bauteile einziehen.