---
layout: "comment"
hidden: true
title: "1114"
date: "2006-05-30T12:49:13"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Wow, da steckt viel Zeit an der Fräsmaschine drin!

Wie sieht es denn mit dem Spiel zwischen Drehkranz und Schnecke aus? Ohne geht fast gar nicht so weit ich weiß und die 0,02 Grad erscheinen mir sehr theoretisch :) Sehr wenig Spiel geht doch nur bei hoher Reibung und damit Abrieb an der roten Schnecke oder liege ich da falsch?

Gruß,
Thomas