---
layout: "image"
title: "5 Achser Inkrementalgeber"
date: "2006-04-11T22:20:22"
picture: "Sensor.jpg"
weight: "6"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6067
- /detailsa357.html
imported:
- "2019"
_4images_image_id: "6067"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-04-11T22:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6067 -->
