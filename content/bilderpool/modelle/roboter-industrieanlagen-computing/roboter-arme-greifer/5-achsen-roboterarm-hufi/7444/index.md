---
layout: "image"
title: "6AX, Das Display"
date: "2006-11-10T11:56:25"
picture: "DSC03543.jpg"
weight: "39"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7444
- /detailsd3b4.html
imported:
- "2019"
_4images_image_id: "7444"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-11-10T11:56:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7444 -->
Immer wieder ein willkommenes Hilfsmittel bei der Fehlersuche, da die ganze Firmware in Assembler geschrieben wurde
Angezeigt wird:
Pro Achse die aktuelle Positon (16Bit), die Spannung die dem jeweiligen Motor angelegt wird (8Bit Hex Wert), die Bewegungsgeschwindigkeit der Achse sowie der Status
Weiters Informationen über den Greifer wie der Strom (Kraft) mit der zugegriffen werden soll, Status, Delays..
Die CPU Auslastung zeigt an wieviel "Ressource" noch verfügbar ist wenns gegen Null geht wirds kritisch für die serielle Datenübertragung, da dann unter Umständen Bytes verloren gehen.
OPMODE = verschiedene Betriebs Modi die per PC umgeschaltet werden (z.B. Teach in oder Betrieb)