---
layout: "image"
title: "6AX V2 Verkabelung"
date: "2006-10-21T22:26:57"
picture: "DSC03519.jpg"
weight: "33"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7208
- /details5b37-2.html
imported:
- "2019"
_4images_image_id: "7208"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-10-21T22:26:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7208 -->
Die Kabelführung sollte (hoffentlich) nie im Weg sein.