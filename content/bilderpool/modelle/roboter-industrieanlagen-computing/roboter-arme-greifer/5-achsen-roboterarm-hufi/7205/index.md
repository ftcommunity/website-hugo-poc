---
layout: "image"
title: "6AX V2 Sensoren und Verkabelung"
date: "2006-10-21T22:26:57"
picture: "DSC03517.jpg"
weight: "30"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7205
- /details2883.html
imported:
- "2019"
_4images_image_id: "7205"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-10-21T22:26:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7205 -->
Verkabelung einer Achse,
jede Achse wird über ein 8poliges Flachkabel versorgt. (2xMotor,4xEncoder,2xNullpunkterkennung)
In diesem Fall erfolgt die Nullpunkterkennung durch einen Gabellichschranken,weswegen VCC beim Decoder angezapft wurde.