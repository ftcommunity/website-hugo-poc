---
layout: "image"
title: "6AX V2 Kabelführung"
date: "2006-10-21T22:26:57"
picture: "DSC03518.jpg"
weight: "32"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7207
- /details2680-2.html
imported:
- "2019"
_4images_image_id: "7207"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-10-21T22:26:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7207 -->
Alle Verbindungen werden durch den Vertikalsupport bis ganz nach oben geführt
Von dort aus in Schleifen zu den Zielen.