---
layout: "image"
title: "6AX Alu Drehkranz seite"
date: "2006-05-29T00:33:48"
picture: "0005.jpg"
weight: "15"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6290
- /details42d8.html
imported:
- "2019"
_4images_image_id: "6290"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-05-29T00:33:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6290 -->
