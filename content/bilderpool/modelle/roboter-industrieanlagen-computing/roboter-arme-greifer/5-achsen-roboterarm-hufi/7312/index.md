---
layout: "image"
title: "6AX - es geht voran"
date: "2006-11-03T23:16:07"
picture: "DSC03535.jpg"
weight: "36"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7312
- /details2aa3.html
imported:
- "2019"
_4images_image_id: "7312"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-11-03T23:16:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7312 -->
Sein zukünftiger Arbeitsplatz