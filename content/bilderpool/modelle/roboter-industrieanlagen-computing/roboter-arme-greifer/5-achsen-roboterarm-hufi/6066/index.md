---
layout: "image"
title: "5 Achser Detail 3"
date: "2006-04-11T22:18:33"
picture: "DET3.jpg"
weight: "5"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6066
- /detailse916-2.html
imported:
- "2019"
_4images_image_id: "6066"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-04-11T22:18:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6066 -->
