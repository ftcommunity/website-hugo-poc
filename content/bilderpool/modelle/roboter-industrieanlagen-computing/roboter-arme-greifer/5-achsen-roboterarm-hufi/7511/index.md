---
layout: "image"
title: "6AX Auch Fischertechnik spielt mit LEGO-Video"
date: "2006-11-19T22:19:25"
picture: "DSC03560.jpg"
weight: "40"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/7511
- /detailsa350.html
imported:
- "2019"
_4images_image_id: "7511"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-11-19T22:19:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7511 -->
Habe mir vom Sohn sein Lego ausgeborgt, zum spielen.
Video: www.bigwig.at/ft/ROBOLEGO.wmv