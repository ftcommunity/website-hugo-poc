---
layout: "image"
title: "6AX V2 Sensoren am Greifer"
date: "2006-09-01T23:19:02"
picture: "DSC03515.jpg"
weight: "29"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6767
- /details53fa.html
imported:
- "2019"
_4images_image_id: "6767"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-09-01T23:19:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6767 -->
Erster Anbau der Sensoren am Greifer Achsen 5 und 6 mit den GPA Encodern, der Greifer selbst wird mit dem mittleren Motor betätigt von dem lediglich der Strom überwacht wird.