---
layout: "image"
title: "6AX Z40 Drehkranz"
date: "2006-06-02T22:22:52"
picture: "0407.jpg"
weight: "18"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6407
- /detailsbee3.html
imported:
- "2019"
_4images_image_id: "6407"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-06-02T22:22:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6407 -->
Noch ein Z40 Drehkranz