---
layout: "image"
title: "6AX Alu Drehkranz von unten"
date: "2006-05-29T00:33:48"
picture: "0004.jpg"
weight: "14"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- /php/details/6289
- /details7abc.html
imported:
- "2019"
_4images_image_id: "6289"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-05-29T00:33:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6289 -->
