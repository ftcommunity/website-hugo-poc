---
layout: "image"
title: "Fin-Ray - Prinzip 3"
date: "2013-10-02T19:18:30"
picture: "DSC00289_800x800.jpg"
weight: "7"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
schlagworte: ["Fin-Ray"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37494
- /detailsceb9.html
imported:
- "2019"
_4images_image_id: "37494"
_4images_cat_id: "633"
_4images_user_id: "1806"
_4images_image_date: "2013-10-02T19:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37494 -->
