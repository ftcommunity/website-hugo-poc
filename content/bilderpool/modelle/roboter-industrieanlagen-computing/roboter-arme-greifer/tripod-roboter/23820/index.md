---
layout: "image"
title: "Tripod-1"
date: "2009-04-27T21:09:57"
picture: "Tripod-1.jpg"
weight: "1"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/23820
- /details2d08-2.html
imported:
- "2019"
_4images_image_id: "23820"
_4images_cat_id: "1627"
_4images_user_id: "46"
_4images_image_date: "2009-04-27T21:09:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23820 -->
Ein erstes, mal eben schnell zusammengestecktes Ausprobiermodell. Immerhin zeigt es, daß die Mechanik, die ich mir für ein Messemodell überlegt hatte, prinzipiell funktioniert.

Dummerweise ist alles an dem Modell schiefwinklig, so daß es sich nur ganz schlecht fotografieren läßt.
