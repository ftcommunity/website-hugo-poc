---
layout: "comment"
hidden: true
title: "15664"
date: "2011-11-11T18:38:30"
uploadBy:
- "Kieseleck"
license: "unknown"
imported:
- "2019"
---
Hallo,
was ist jetzt mit der Anwendung? Ich würde spontan sagen, dass man doch einfach einen E-Magneten unten anbaut und schon hat man einen Art "Greifer" oder man baut unten einen Greifer an und lässt ihn würfeln... dazu bräuchte man aber ein "ortungssystem" für den Würfel. Das ist ein echt tolles Modell, remandus!

Viele Grüße, Kieseleck