---
layout: "overview"
title: "Tripod-Roboter"
date: 2020-02-22T08:03:02+01:00
legacy_id:
- /php/categories/1627
- /categories3ce1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1627 --> 
Drei Achsen frei im Raum beweglich. Dafür keine Rotationen.