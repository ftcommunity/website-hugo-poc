---
layout: "image"
title: "Tripod-2"
date: "2009-04-27T21:09:57"
picture: "Tripod-2.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/23821
- /details52ae.html
imported:
- "2019"
_4images_image_id: "23821"
_4images_cat_id: "1627"
_4images_user_id: "46"
_4images_image_date: "2009-04-27T21:09:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23821 -->
Ein Detail der Hängekonstruktion von der Seite her gesehen. Es hat doch einige Zeit gekostet, bis ich die Streben und die Gelenke endlich ausreichend kompakt hatte.

Jetzt aber funktioniert es und die Gondel in der Mitte läßt sich sehr schön positionieren.
