---
layout: "image"
title: "Ein/Aus Schalter"
date: 2021-05-10T19:42:06+02:00
picture: "Publisher16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Stromversorgung erfolgt über ein Netzteil mit 12 Volt und 5 Ampere.
Das ist nötig, da die Schrittmotoren einen hohen Strombedarf benötigen.