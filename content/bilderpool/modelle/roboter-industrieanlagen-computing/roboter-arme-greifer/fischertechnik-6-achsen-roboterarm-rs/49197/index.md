---
layout: "image"
title: "fischertechnik Kamera"
date: 2021-05-10T19:42:00+02:00
picture: "Publisher20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kamera ist zum Beobachten der Fläche gedacht und kann später im Programm zur Orientierung des Greifer
eingesetzt werden.