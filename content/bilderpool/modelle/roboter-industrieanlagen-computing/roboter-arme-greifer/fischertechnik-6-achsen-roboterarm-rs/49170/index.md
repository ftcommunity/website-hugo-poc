---
layout: "image"
title: "Sauggreifer"
date: 2021-05-10T19:41:30+02:00
picture: "Publisher45.jpg"
weight: "45"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der normale Greifer kann gegen den Sauggreifer getauscht werden.