---
layout: "image"
title: "Kopf hinten"
date: 2021-05-10T19:41:31+02:00
picture: "Publisher44.jpg"
weight: "44"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Für die Lageerkennung des Greifers ist ein 158402 Kombisensor von fischertechnik im Roboterkopf verbaut worden.