---
layout: "image"
title: "Drehkranz 3D Druck"
date: 2021-05-10T19:41:27+02:00
picture: "Publisher48.jpg"
weight: "48"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Drehkranz von Till Harbaum auf Thingiverse

https://www.thingiverse.com/thing:4336232