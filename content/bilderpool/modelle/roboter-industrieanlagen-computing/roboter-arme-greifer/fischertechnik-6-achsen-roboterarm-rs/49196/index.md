---
layout: "image"
title: "Hinter der fischertechnik Kamera"
date: 2021-05-10T19:41:59+02:00
picture: "Publisher21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Dahinter sind die zwei kugelgelagerten Drehkränze von Andreas Tacke zu sehen.