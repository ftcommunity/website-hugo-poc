---
layout: "image"
title: "OLED Display"
date: 2021-05-10T19:41:40+02:00
picture: "Publisher37.jpg"
weight: "37"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Über das 1,3 OLED-Display werden die jeweiligen Parameter der Pixy-Kamera, der Endschalter, und des Komisensors ausgegeben. 
Das Display und der Kombisensor werden über den I2C-Anschluss vom TXT-Controller angesteuert.