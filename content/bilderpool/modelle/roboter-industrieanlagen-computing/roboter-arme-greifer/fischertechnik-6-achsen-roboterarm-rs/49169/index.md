---
layout: "image"
title: "Plattform"
date: 2021-05-10T19:41:29+02:00
picture: "Publisher46.jpg"
weight: "46"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Universalplattform für weitere Anwendungen