---
layout: "image"
title: "Kopf demontiert"
date: 2021-05-10T19:41:34+02:00
picture: "Publisher41.jpg"
weight: "41"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Kopf wird mit 2 Neodymmagneten fixiert und kann schnell getauscht werden