---
layout: "image"
title: "Schleifring Kopf und Rumpf"
date: 2021-05-10T19:41:38+02:00
picture: "Publisher39.jpg"
weight: "39"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Schleifring im Kopf hat 12 Adern, der im Rumpf hat 6 Adern.