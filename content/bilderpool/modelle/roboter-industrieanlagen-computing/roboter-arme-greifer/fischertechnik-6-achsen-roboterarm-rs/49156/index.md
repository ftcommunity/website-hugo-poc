---
layout: "image"
title: "TXT Controller"
date: 2021-05-10T19:41:14+02:00
picture: "Publisher08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Info Anzeige vom fischertechnik Gyro-Sensor und  der Pixy-Kamera 