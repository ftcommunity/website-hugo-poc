---
layout: "image"
title: "Nema 17 Schrittmotor"
date: 2021-05-10T19:42:08+02:00
picture: "Publisher14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Für den Antrieb des Schultergelenks habe ich zwei Nema 17 Schrittmotoren 17HS24-0644S von Stepperonline verbaut. 
Diese verfügen über ein hohes Drehmoment 60Ncm. Dieses wird benötigt, da der komplette Arm ein hohes Gewicht hat. 
Der Halter für den Nema 17 Schrittmotor ist ein 3D-Druck und stammt aus thingiverse.de