---
layout: "image"
title: "Antrieb hinten"
date: 2021-05-10T19:42:03+02:00
picture: "Publisher19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hinten ist der Antrieb über Kette zu sehen. Angesteuert über zwei Nema 14 Motoren von fischertechnik.