---
layout: "image"
title: "Rumpf ohne Seitenverkleidung"
date: 2021-05-10T19:42:09+02:00
picture: "Publisher13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kann man die Kugeldrehverbindung und den Antrieb für das Schultergelenk sehen.