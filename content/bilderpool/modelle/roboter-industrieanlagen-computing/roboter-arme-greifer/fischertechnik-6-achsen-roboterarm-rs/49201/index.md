---
layout: "image"
title: "Unterarm Aluprofile"
date: 2021-05-10T19:42:05+02:00
picture: "Publisher17.jpg"
weight: "17"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Zur Versteifung wurden im Robotearm 12  Aluprofile verbaut.