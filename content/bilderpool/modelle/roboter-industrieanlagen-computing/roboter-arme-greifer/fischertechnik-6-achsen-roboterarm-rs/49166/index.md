---
layout: "image"
title: "ROBOPro Software"
date: 2021-05-10T19:41:25+02:00
picture: "Publisher49.jpg"
weight: "49"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

ROBO Pro Software Ansteuerung über TeachIn Programm. Es können verschiedene, gespeicherte Programme abgerufen werden.