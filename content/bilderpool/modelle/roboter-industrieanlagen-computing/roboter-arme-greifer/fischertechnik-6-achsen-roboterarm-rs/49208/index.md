---
layout: "image"
title: "Der ftPwrDrive Controller"
date: 2021-05-10T19:42:13+02:00
picture: "Publisher10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der ftPwrDriveController regelt die Schrittmotoren und wird über I2C vom TXT Controller angesteuert