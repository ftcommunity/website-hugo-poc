---
layout: "image"
title: "Not-Halt-Taster und Stromversorgung"
date: 2021-05-10T19:42:07+02:00
picture: "Publisher15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der 6-Achs Roboterarm verfügt über einen Not-Halt-Taster, dieser unterbricht die Bewegungen der Schritt- und Encodermotoren.