---
layout: "image"
title: "Greifer unten"
date: 2021-05-10T19:41:45+02:00
picture: "Publisher33.jpg"
weight: "33"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die beiden Endschalter für den Greifer