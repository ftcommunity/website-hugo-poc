---
layout: "image"
title: "Rumpf vorn"
date: 2021-05-10T19:42:10+02:00
picture: "Publisher12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Vorn befindet sich der Endschalter für das Drehen des Rumpfs. 
Mittig ist ein StepDown Modul zum reduzieren der 12 Volt Spannung auf 9 Volt zu erkennen.
Links und rechts jeweils die kugelgelagerten Drehkränze von Andreas Tacke.