---
layout: "image"
title: "Kopfplattform"
date: 2021-05-10T19:41:33+02:00
picture: "Publisher42.jpg"
weight: "42"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kopfplattform ist ein 3D-Druckaus ABS. Zuerst wurde eine Platine entworfen, dann mit Inventor gezeichnet und gedruckt.

Anschlüsse:
I2C 6-polig und 10-polig, (für verschiedene Sensoren) Servo, 3,3V, 5,0 V, 9,0V, M1, M2, I7, I8

