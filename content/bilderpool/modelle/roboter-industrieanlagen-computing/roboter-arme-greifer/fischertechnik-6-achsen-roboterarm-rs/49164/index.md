---
layout: "image"
title: "Beschriftung Seitenverkleidung"
date: 2021-05-10T19:41:23+02:00
picture: "Publisher50.jpg"
weight: "50"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beschriftung wurde mit einem Plotter Brother Scancut SDX 1000 erstellt.

Der Schriftfont "Classic Robot" wurde installiert. In Corel Draw wurden die Schriften als Vectorgrafik gespeichert.
Anschließend in den Plotter übertragen und die Schrift auf die richtige Größe skaliert, danach abgespeichert und geplottet.
Die Schriftfolie wurde entgittert und mit durchsichtiger Trägerfolie auf dem Bauteil fixiert.