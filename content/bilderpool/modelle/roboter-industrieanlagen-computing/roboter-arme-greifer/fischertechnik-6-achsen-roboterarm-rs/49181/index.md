---
layout: "image"
title: "Pixy Software"
date: 2021-05-10T19:41:42+02:00
picture: "Publisher35.jpg"
weight: "35"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Über die Länge und Breite wird die Greifposition berechnet