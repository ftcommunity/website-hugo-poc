---
layout: "image"
title: "Kopf 360 Grad drehbar"
date: 2021-05-10T19:41:39+02:00
picture: "Publisher38.jpg"
weight: "38"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Durch den Schleifring ist der Kopf 360 Grad drehbar.