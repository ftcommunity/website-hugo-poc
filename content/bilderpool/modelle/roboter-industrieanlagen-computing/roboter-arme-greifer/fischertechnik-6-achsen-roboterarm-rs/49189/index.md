---
layout: "image"
title: "Antrieb Kopf"
date: 2021-05-10T19:41:51+02:00
picture: "Publisher28.jpg"
weight: "28"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Oben mittig ist der Reed-Endschalter zu sehen.