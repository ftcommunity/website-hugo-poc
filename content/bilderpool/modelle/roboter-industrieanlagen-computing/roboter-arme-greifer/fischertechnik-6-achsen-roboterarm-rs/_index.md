---
layout: "overview"
title: "fischertechnik 6-Achsen Roboterarm RS 816"
date: 2021-05-09T19:32:52+02:00
---

Hallo Technikfreunde,

hiermit möchte ich euch meinen 6-Achsen Roboterarm RS 816 vorstellen.

Ziel war es einen technisch voll funktionsfähigen Roboterarm zu konstruieren, welcher den orignalen Roboterarm
in Aussehen und Funktionen in nichts nachsteht. Es wurde dabei Wert auf Stabilität der Arme, module Komponenten und erweiterbar Sensorik gelegt. 
Der 6-Achsen Roboterarm besteht aus 6-Achsen und einem Greifer. 

Ein Video dazu unter YouTube:

https://www.youtube.com/watch?v=OdPd-DBR3cE

ft:pedia Artikel unter  01/2021:

https://ftcommunity.de/ftpedia/2021/2021-1/ftpedia-2021-1.pdf

Folgende Bauteile wurden verbaut, auf diese Bauteile werde ich in den folgenden Bildern näher eingehen. Durch den Einsatz des ftPwrDrive konnte auf einen zweiten TXT Controller verzichtet werden.

Controller:
1 TXT Controller angesteuert über W-LAN
1 ftPwrDrive über I2C und 1 ftExtender verbunden

Antriebe:
4 Nema 14 Stepper für das Drehen des Grundkörpers, Drehen und Heben des Arms, 
2 Nema 17 Stepper für das  Heben und Senken
2 ft Encoder Motoren für das Drehen und Heben des Greiferkopfs

Anzeige:
1 OLED-Display

Sensoren:
1 Pixy-Kamera für die Bilderkennung
1 ft Kombisensor für die Lageerkennung des Kopfs

Kopf:
2 Step-Down Module zum reduzieren der Spannung auf 5 und 3,3 Volt
1 S-Motor für den Greifer
360 Grad Kopf 
Buchsen: Über Schleifring, I2C, Stromversorgung, 3,3Volt, 5 Volt, 9 Volt, 1 Servoansteuerung (ftpwrDrive) 

Rumpf:
360 Grad Stromversorgung und Not-Aus Taster

Lagerung
5 Drehkränze mit Kugellager für Rumpf, Arme und Kopf

Schalter
6 Endschalter
1 Not-Aus Taster

Statik:
12 Alu-Profile in den Rumpf, Armen

Software:
ROBO Pro Software Ansteuerung über TeachIn Programm

Hallo Technikfreunde,

hiermit möchte ich euch meinen 6-Achsen Roboterarm RS 816 vorstellen.

Ziel war es einen technisch voll funktionsfähigen Roboterarm zu konstruieren, welcher den orignalen Roboterarm
in Aussehen und Funktionen in nichts nachsteht. Es wurde dabei Wert auf Stabilität der Arme, module Komponenten und erweiterbar Sensorik gelegt. 
Der 6-Achsen Roboterarm besteht aus 6-Achsen und einem Greifer. 

Ein Video dazu unter YouTube:

https://www.youtube.com/watch?v=OdPd-DBR3cE

ft:pedia Artikel unter  01/2021:

https://ftcommunity.de/ftpedia/2021/2021-1/ftpedia-2021-1.pdf

Folgende Bauteile wurden verbaut, auf diese Bauteile werde ich in den folgenden Bildern näher eingehen. Durch den Einsatz des ftPwrDrive konnte auf einen zweiten TXT Controller verzichtet werden.

Controller:
1 TXT Controller angesteuert über W-LAN
1 ftPwrDrive über I2C und 1 ftExtender verbunden

Antriebe:
4 Nema 14 Stepper für das Drehen des Grundkörpers, Drehen und Heben des Arms, 
2 Nema 17 Stepper für das  Heben und Senken
2 ft Encoder Motoren für das Drehen und Heben des Greiferkopfs

Anzeige:
1 OLED-Display

Sensoren:
1 Pixy-Kamera für die Bilderkennung
1 ft Kombisensor für die Lageerkennung des Kopfs

Kopf:
2 Step-Down Module zum reduzieren der Spannung auf 5 und 3,3 Volt
1 S-Motor für den Greifer
360 Grad Kopf 
Buchsen: Über Schleifring, I2C, Stromversorgung, 3,3Volt, 5 Volt, 9 Volt, 1 Servoansteuerung (ftpwrDrive) 

Rumpf:
360 Grad Stromversorgung und Not-Aus Taster

Lagerung
5 Drehkränze mit Kugellager für Rumpf, Arme und Kopf

Schalter
6 Endschalter
1 Not-Aus Taster

Statik:
12 Alu-Profile in den Rumpf, Armen

Software:
ROBO Pro Software Ansteuerung über TeachIn Programm

