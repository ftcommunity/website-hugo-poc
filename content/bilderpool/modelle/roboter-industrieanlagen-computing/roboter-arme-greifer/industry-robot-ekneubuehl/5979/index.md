---
layout: "image"
title: "Bild3"
date: "2006-03-28T14:54:55"
picture: "S246.jpg"
weight: "3"
konstrukteure: 
- "Erwin Kneubuehl"
fotografen:
- "Erwin Kneubuehl"
uploadBy: "ekneubuehl"
license: "unknown"
legacy_id:
- /php/details/5979
- /detailsbf0d-2.html
imported:
- "2019"
_4images_image_id: "5979"
_4images_cat_id: "540"
_4images_user_id: "428"
_4images_image_date: "2006-03-28T14:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5979 -->
