---
layout: "image"
title: "Bild9"
date: "2006-03-28T14:54:55"
picture: "S2400003_2.jpg"
weight: "9"
konstrukteure: 
- "Erwin Kneubuehl"
fotografen:
- "Erwin Kneubuehl"
uploadBy: "ekneubuehl"
license: "unknown"
legacy_id:
- /php/details/5985
- /details5a64.html
imported:
- "2019"
_4images_image_id: "5985"
_4images_cat_id: "540"
_4images_user_id: "428"
_4images_image_date: "2006-03-28T14:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5985 -->
