---
layout: "image"
title: "06-Verkabeln-1"
date: "2015-02-08T16:31:35"
picture: "I-Robot-Kabel1.jpg"
weight: "6"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/40508
- /details6ee1.html
imported:
- "2019"
_4images_image_id: "40508"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-02-08T16:31:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40508 -->
Nachdem es sehr, sehr lange ziemlich still in meiner Werkstatt war, geht es jetzt endlich weiter an meinem Roboter. Mittlerweile ist die Mechanik fertig geworden.

Wie im Bild erkennbar, trägt die erste Auslegerachse ein Messinggewicht hinten. Das sind 3 Pfund Gegengewicht, so dass diese Achse ausbalanciert ist. Bei der folgenden Achse dienen die beiden Antriebsmotore als Gegengewichte, so dass auch diese Achse balanciert ist. Die beiden vordersten Achsen sind so leicht, dass hier keine Notwendigkeit des Massenausgleichs besteht. Insgesamt sind die Bewegungskräfte jetzt sehr klein geworden, so dass die Motoren fast nichts zu tun haben. Ok - etwas Reibung ist noch.

Kleines, aber feines Detail: In die beiden Drehkränze des Schultergelenks habe ich je eine Radnabe eingebaut. Mit dieser Maßnahme lässt sich das Radialspiel im Drehkranz vermindern. Da noch zwei Ausleger auf diese Drehachse folgen, hat die Verminderung des Radialspiels eine erhebliche Steigerung der Genauigkeit gebracht. Man merkt es sehr, wenn die Drehrichtung der Achse umkehrt. Früher ist die Achse statt sich zu drehen, erst einmal um zwei Zehntelmillimeter der Schneckenrichtung gefolgt. Da die Ausleger fast zehnmal so lang sind, wie der Antriebsradius am Drehkranz der Schulter, macht das 2 Millimeter vorne am Werkzeug aus. Das war mir dann doch zuviel.

Nun aber zum Verkabeln: Während ich früher die Steuerelektrik neben die Anlage gestellt habe und dann haufenweise Kabel verlegte, kommt jetzt die ganz Stromaufbereitung auf den Roboter drauf. Im Bild oben sieht man ein kleines Türmchen aus Platinen. Das ist die Aufbereitung der Gabellichtschranke (Nullpunkt), die Endschalterauswertung und die Schrittmotorkarte. Zuletzt kommt noch eine Platine oben drauf, die die beiden Schalter für die wählbare Schrittauflösung der Motoren bedient. Das soll dann mal in einem roten Kasten verschwinden. Eingangssignale für die erste Achse (Rotation) sind dann Takt und Richtung vom Computer und das Nullsignal für die Lage. Macht ganze drei Kabel.
