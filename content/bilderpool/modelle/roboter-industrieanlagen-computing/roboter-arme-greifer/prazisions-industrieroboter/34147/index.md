---
layout: "image"
title: "04-Achsen drei, vier und fünf"
date: "2012-02-12T14:48:21"
picture: "04-Roboter.jpg"
weight: "4"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/34147
- /details0409.html
imported:
- "2019"
_4images_image_id: "34147"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34147 -->
Ab jetzt heißt es Gewicht zu sparen, denn sonst gibt es Probleme. Dieses Aggregat ist volle 37 cm lang und wiegt "nur" 640 g. Und das beste kommt noch: Durch die Anordnung der Motoren ganz hinten ist die Achse ausbalanciert! Wow, davon hatte ich geträumt. Lediglich der noch zu konstruierende Greifer wird ein veränderliches Gewichtsmoment verursachen.

Also Achse drei sind die beiden Drehkränze rechts, die an den Auslegern der Achse zwei festgemacht werden. Die Ausladung steigt damit um weitere 21 cm. Ein einzelnes Aluprofil muss für diese Achse reichen, dann ganz vorne kommt ja nichts wesentliches mehr.

Achse vier wird von dem Motor ganz rechts angetrieben. Ewig lange Welle durch den Drehkranz links zur Schnecke. Diese treibt über ein Ritzel 10Z die Zwischenwelle, die ihrerseits über zwei Ritzel die Zahnräder Z40 antreiben. Die resultierende Achse kompensiert die Drehungen von Achse zwei und drei und orientiert so die Hand. Die einzigen Anschlüsse nach vorne sind dann die Handsteuerung (Greifen) und der Initiator von Achse vier. Das sollte machbar sein, ohne die Rotation von Achse fünf allzusehr zu beschränken. Achse vier schafft 220°.

Zu Achse fünf: Das ist wohl die mit der geringsten Untersetzung, weil ich einfach keine hinkriege. Der Drehkranz steht in Achsrichtung zum Motor und schon kommt man mit einer Schnecke nicht mehr ran. Macht nichts, das spart Teile und Gewicht und ein großes Drehmoment wird wohl nicht abgefordert. Hoffe ich jedenfalls. Naja, ganz so schwach sind die Schrittmotore nun auch wieder nicht.

Eine sechste Achse wird es nicht geben. Tut mir leid, aber ich will mich nicht quälen.
