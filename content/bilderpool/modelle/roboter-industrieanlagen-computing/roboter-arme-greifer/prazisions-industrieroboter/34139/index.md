---
layout: "image"
title: "02-Die erste Achse"
date: "2012-02-12T14:48:21"
picture: "02-Roboter.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/34139
- /detailsba9d.html
imported:
- "2019"
_4images_image_id: "34139"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34139 -->
Um keine Genauigkeit schon am Anfang zu verschenken, greife ich zu reichlich Edelpfusch, den ich mir gerne vorwerfen lasse. Ich denke, es bleibt noch genug Fischertechnik übrig.

Zu den harten Fakten: vom Motor zur Zwischenwelle erfolgt eine Untersetzung von 1:3 mit spielfreiem Riementrieb. Danach kommt eine Untersetzung von ungefähr 1:24 auf den großen Kranz, ebenfalls mittels Zahnriemen. Die genaue Untersetzung muss ich noch ausmessen, wenn sie läuft. Der Motor hat eine Auflösung von 200 Schritt/Umdrehung, was für den Kranz eine Auflösung von 14400 Schritt/Umdrehung oder 0,025°/Schritt ausmacht. Das scheint sehr viel zu sein, aber es kommt ja noch die Ausladung des Roboterarms und die Drehauflösung zählt ganz vorne.

Die beiden Schienenkränze stützen den Drehkranz gegen die Riemenspannung. Das hat zwar den Vorteil, dass der Riemen akkurat arbeitet, aber gleichzeitig den Nachteil, dass die Rundlaufgenauigkeit des gelben Kranzes voll in das Ergebnis der Mittellage eingeht. Tja, damit ist die Genauigkeit des Drehpunktes auf bestenfalls einen halben Millimeter begrenzt. Aber ich will nicht überpedantisch werden.

Zu den Einschränkungen: Die volle Rotation wird nicht angestrebt. Statt dessen müssen +/-90° reichen. Es ginge mit dieser Konstruktion zwar deutlich mehr, aber ich muss an all die Kabel denken, die noch kommen. Und bauartbedingt komme ich mit den Kabeln nicht nach unten durch den Kranz durch.

Die Lichtschranke ganz links im Bild initiiert die Achse. Die Drehachse ist die einzige von allen weiteren, die Endschalter haben wird. Die anderen Achsen werden ebenfalls einzeln initiiert und ihre Position von dort aus weitergezählt. Endlagen müssen daher durch Zählen verfolgt werden. Der Vorteil: viel weniger Kabelsalat, der Nachteil: schwierigere Initiierung der Achsen, so dass vor dem Abschalten des Roboters die Achsstellungen gespeichert werden müssen, damit nach dem Wiederanfahren in der Nähe des Initiators gesucht werden kann.
