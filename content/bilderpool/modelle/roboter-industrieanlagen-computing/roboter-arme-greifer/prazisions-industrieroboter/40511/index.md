---
layout: "image"
title: "09-Verkabeln-4"
date: "2015-02-08T16:31:35"
picture: "I-Robot-Kabel4.jpg"
weight: "9"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/40511
- /details3ea9-2.html
imported:
- "2019"
_4images_image_id: "40511"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-02-08T16:31:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40511 -->
Hier etwas mehr Überblick.

Die erste Achse ist fertig und sie läuft. Es ist fantastisch das Ding sich endlich bewegen zu sehen.

Was soll ich sagen: der erste Eindruck befriedigt jeden Aufwand. Nicht klappert, nichts wackelt und im Achtelschrittbetrieb ist die Bewegung unfassbar hoch aufgelöst. Wenn die anderen Achsen nur halb so gut geraten, dann darf ich höchst zufrieden sein. Hoffentlich halten alle Lötstellen.

Sowie der Roboter dann komplett verkabelt ist und läuft, beginnt die eigentliche Arbeit erst.
PROGRAMMIEREN!
