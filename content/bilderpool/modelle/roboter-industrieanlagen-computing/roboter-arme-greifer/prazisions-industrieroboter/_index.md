---
layout: "overview"
title: "Präzisions-Industrieroboter"
date: 2020-02-22T08:03:14+01:00
legacy_id:
- /php/categories/2528
- /categories524c-2.html
- /categories3e5f.html
- /categoriesea5c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2528 --> 
Bei diesem Projekt soll ein Handhabungsroboter herauskommen, der seine Positionen sicher und exakt erreicht. Dafür wird ein eigenes, fischertechnik-taugliches Konzept entwickelt.