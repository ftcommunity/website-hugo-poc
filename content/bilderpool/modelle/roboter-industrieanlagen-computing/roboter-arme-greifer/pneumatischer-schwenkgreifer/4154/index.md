---
layout: "image"
title: "Ventil-technik von vorne"
date: "2005-05-17T19:35:24"
picture: "Pneumatischer-schwenkgreifer014.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4154
- /details83ca.html
imported:
- "2019"
_4images_image_id: "4154"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-17T19:35:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4154 -->
