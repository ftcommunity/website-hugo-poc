---
layout: "image"
title: "Greifzange von vorne"
date: "2005-05-26T12:15:43"
picture: "Schwenkbare-pneumaticgreifzange004.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4180
- /details4bb7.html
imported:
- "2019"
_4images_image_id: "4180"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-26T12:15:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4180 -->
