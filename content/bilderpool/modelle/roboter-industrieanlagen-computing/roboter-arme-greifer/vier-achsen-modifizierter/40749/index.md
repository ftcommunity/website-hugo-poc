---
layout: "image"
title: "Einsortieren von Tonnen mit Ziffernerkennung"
date: "2015-04-10T16:19:04"
picture: "IMG_0657.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Bildverarbeitung", "Ziffernerkennung", "ATMEGA2560", "TFT", "Omnivision", "OV7670", "Kamera", "Sharp", "GP2D12", "Entfernungsmesser", "Drehteller", "Hochregal"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40749
- /detailsc58d-2.html
imported:
- "2019"
_4images_image_id: "40749"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-04-10T16:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40749 -->
Weiter umgebaut und erweitert um:

- ATMEGA2560 Controller mit TFT 2,4"
- Omnivision OV7670 Kamera
- Sharp GP2D12 Entfernungsmesser
- Drehteller
- Hochregal

Der Roboter räumt mit Ziffern markierte Tonnen auf. Die Tonnen werden nach Ziffernerkennung sortiert ins Hochregal eingeräumt.