---
layout: "image"
title: "Station mit Kamera und Drehteller zur Ziffernerkennung"
date: "2015-04-10T16:19:04"
picture: "IMG_0658.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40750
- /details02a4.html
imported:
- "2019"
_4images_image_id: "40750"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-04-10T16:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40750 -->
