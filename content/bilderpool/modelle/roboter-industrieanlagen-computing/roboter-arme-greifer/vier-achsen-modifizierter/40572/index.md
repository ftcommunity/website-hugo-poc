---
layout: "image"
title: "3 M-Motoren für die 3 Drehachsen"
date: "2015-02-18T15:55:34"
picture: "IMG_0329.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40572
- /details8137.html
imported:
- "2019"
_4images_image_id: "40572"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-02-18T15:55:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40572 -->
