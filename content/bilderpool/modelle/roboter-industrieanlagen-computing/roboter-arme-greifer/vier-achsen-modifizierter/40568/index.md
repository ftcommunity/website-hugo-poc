---
layout: "image"
title: "Details zur Drehung um die vierte Achse mit Exzenter-Scheiben"
date: "2015-02-18T15:55:34"
picture: "IMG_0321.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Exzenter"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40568
- /details19ec.html
imported:
- "2019"
_4images_image_id: "40568"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-02-18T15:55:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40568 -->
