---
layout: "comment"
hidden: true
title: "20455"
date: "2015-04-11T15:53:03"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Das ist ein Sharp GP2D12 Entfernungsmesser. Der misst die Entfernung zur weißen Tonne und dann berechnet der Controller daraus die nötigen Encoder-Schritte, um die Tonne zu greifen.