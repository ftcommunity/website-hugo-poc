---
layout: "image"
title: "interfaces"
date: "2009-05-06T21:41:35"
picture: "DSCF4124.jpg"
weight: "4"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboterarm", "cool", "Roboter"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23909
- /details979f.html
imported:
- "2019"
_4images_image_id: "23909"
_4images_cat_id: "1638"
_4images_user_id: "934"
_4images_image_date: "2009-05-06T21:41:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23909 -->
