---
layout: "image"
title: "kopf - Unterseite"
date: "2009-05-08T23:33:19"
picture: "achsroboter06.jpg"
weight: "6"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23931
- /detailsf48c.html
imported:
- "2019"
_4images_image_id: "23931"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23931 -->
die Drehmechanik der Achse 6
