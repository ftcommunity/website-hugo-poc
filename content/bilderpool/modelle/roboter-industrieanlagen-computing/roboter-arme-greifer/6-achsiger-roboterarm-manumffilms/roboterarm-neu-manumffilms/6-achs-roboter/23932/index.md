---
layout: "image"
title: "zahnrad"
date: "2009-05-08T23:33:19"
picture: "achsroboter07.jpg"
weight: "7"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23932
- /details198e-2.html
imported:
- "2019"
_4images_image_id: "23932"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23932 -->
das antriebs- und Impulszahnrad, beide mit dem motor verbunden
