---
layout: "image"
title: "grundplatte detail"
date: "2009-05-06T21:41:35"
picture: "DSCF4123.jpg"
weight: "3"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboterarm", "cool", "Roboter"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23908
- /details6cf6-2.html
imported:
- "2019"
_4images_image_id: "23908"
_4images_cat_id: "1638"
_4images_user_id: "934"
_4images_image_date: "2009-05-06T21:41:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23908 -->
