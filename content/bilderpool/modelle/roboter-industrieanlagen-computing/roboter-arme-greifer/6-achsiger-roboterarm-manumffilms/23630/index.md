---
layout: "image"
title: "steuerpult(noch ohne Beschriftung)"
date: "2009-04-06T10:07:34"
picture: "DSCF3937.jpg"
weight: "5"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboter", "Roboterarm", "6-achsig"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23630
- /details35e7.html
imported:
- "2019"
_4images_image_id: "23630"
_4images_cat_id: "1612"
_4images_user_id: "934"
_4images_image_date: "2009-04-06T10:07:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23630 -->
