---
layout: "image"
title: "gesamt (im bau)"
date: "2009-04-06T10:07:58"
picture: "DSCF3946.jpg"
weight: "12"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboter", "Roboterarm", "6-achsig"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23637
- /detailsac41.html
imported:
- "2019"
_4images_image_id: "23637"
_4images_cat_id: "1612"
_4images_user_id: "934"
_4images_image_date: "2009-04-06T10:07:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23637 -->
