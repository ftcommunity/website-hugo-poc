---
layout: "image"
title: "achse 2(alte Version)"
date: "2009-04-06T10:07:34"
picture: "DSCF3935.jpg"
weight: "3"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboter", "Roboterarm", "6-achsig"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23628
- /details67be.html
imported:
- "2019"
_4images_image_id: "23628"
_4images_cat_id: "1612"
_4images_user_id: "934"
_4images_image_date: "2009-04-06T10:07:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23628 -->
