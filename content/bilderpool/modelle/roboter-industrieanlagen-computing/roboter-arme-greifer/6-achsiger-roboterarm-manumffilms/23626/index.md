---
layout: "image"
title: "Roboterarm gesamt(alte Version)"
date: "2009-04-06T10:07:33"
picture: "DSCF3933.jpg"
weight: "1"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
schlagworte: ["Roboter", "Roboterarm", "6-achsig"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/23626
- /details3d61-2.html
imported:
- "2019"
_4images_image_id: "23626"
_4images_cat_id: "1612"
_4images_user_id: "934"
_4images_image_date: "2009-04-06T10:07:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23626 -->
dies ist mein Roboterarm (im Bau)
