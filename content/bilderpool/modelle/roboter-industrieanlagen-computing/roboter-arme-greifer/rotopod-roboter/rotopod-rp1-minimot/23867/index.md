---
layout: "image"
title: "[3/7] Draufsicht"
date: "2009-05-04T21:14:31"
picture: "rotopodrp3.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23867
- /details0874.html
imported:
- "2019"
_4images_image_id: "23867"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23867 -->
2x7m Kabel für 18 Kabelstücke 70mm waren fällig. Die kamen aber bei der Lieferung von der Rolle abgewickelt in der "verstärkten" Ausführung ...
Der untere Ring liegt wegen der Kabelunterführung auf 12 Riegelsteinen und ist mit vier BS15 mit runden Zapfen auf der Arbeitsplatte befestigt
