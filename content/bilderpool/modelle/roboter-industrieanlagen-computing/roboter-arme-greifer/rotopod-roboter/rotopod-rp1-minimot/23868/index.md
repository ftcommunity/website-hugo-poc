---
layout: "image"
title: "[4/7] Ringführung"
date: "2009-05-04T21:14:32"
picture: "rotopodrp4.jpg"
weight: "4"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/23868
- /detailsd2d9-2.html
imported:
- "2019"
_4images_image_id: "23868"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23868 -->
Der obere Führungsring steht mit 6 Stützen stabil auf dem Konsolring. Die Einlagen aus BSB dienen zur sicheren Überwindung der Federkraft der Referenztaster.
Die Konsolen der unteren Stützbeinlagerung erscheinen optisch mit BS30 und BS15 hoch. Eine weiterentwickelte aber hier noch nicht eingebaute Lösung liegt dann um 7,5mm niedriger.
