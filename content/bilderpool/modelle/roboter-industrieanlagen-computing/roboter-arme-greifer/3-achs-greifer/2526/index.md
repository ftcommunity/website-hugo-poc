---
layout: "image"
title: "§-Achs-Greifzange"
date: "2004-06-13T17:20:44"
picture: "Greifzange_4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2526
- /details91d9-2.html
imported:
- "2019"
_4images_image_id: "2526"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2526 -->
Nochmal von vorn.
Der Greifzylinder ist in dem Schwarzen
Teil drehbahr gelagert. über einen
Zahnkranz wird er von der Schnecke des
Motors gedreht.
Hinten am Zylinder (2. Bild) ist ein Kunststoffring angebracht in den ich 4
Schlitze 90 Grad versetzt gesägt habe.
Hier soll noch eine CNY 37 angbracht werden.
Der Luftanschluß erfolgt über einen Schwenkanschluß vom Zentrum hinten.