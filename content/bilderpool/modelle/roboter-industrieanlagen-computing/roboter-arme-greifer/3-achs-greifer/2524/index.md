---
layout: "image"
title: "3-Achs-Greifer"
date: "2004-06-13T17:20:44"
picture: "greifzange_2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Schwenkantrieb", "Greifer", "Greifzange"]
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2524
- /detailsd93c.html
imported:
- "2019"
_4images_image_id: "2524"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2524 -->
Hier sieht man den Schwenkantrieb gut,
mittels Zahnstange/Zahnrad.
Der kurze Zylinder wird doppeltwirkend
eingesetzt, im eingefahrenen Zustand dichtet ein aufgeschobenes,  kurzes Stück
Silikonschlauch auf der Kolbenstange ab.