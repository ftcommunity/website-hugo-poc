---
layout: "image"
title: "3-Achs-Greifzange"
date: "2004-06-13T17:20:44"
picture: "Greifzange_1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Greifzange", "Greifer"]
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2523
- /details7213.html
imported:
- "2019"
_4images_image_id: "2523"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2523 -->
Leider für meinen Trainingroboter-Nachbau
zu lang geraten....
Pneumatisch Zange zu einfach (Feder öffnet)
Pneumatisch 90 Grad schwenken
Motorisch 4X 90 Grad drehbar
(Hier fehlt noch die Gabellichtschranke)