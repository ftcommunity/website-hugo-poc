---
layout: "comment"
hidden: true
title: "17839"
date: "2013-03-01T15:47:38"
uploadBy:
- "werner"
license: "unknown"
imported:
- "2019"
---
Da lässt sich mit Hydraulik  sicherlich ein kompaktes Roboterhandgelenk bauen...
Einfach einen ft Zylinder mit zwei Schläuchen mit dem Schwenkmodul verbinden, Wasser rein, fertig. Den Zylinder kann man dann mit einer Spindel bewegen.