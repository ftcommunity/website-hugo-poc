---
layout: "image"
title: "Feb 2014 -- fully stretched out"
date: "2014-02-22T11:10:25"
picture: "IMG_3073.jpg"
weight: "3"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38328
- /detailsc0c5.html
imported:
- "2019"
_4images_image_id: "38328"
_4images_cat_id: "2844"
_4images_user_id: "2106"
_4images_image_date: "2014-02-22T11:10:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38328 -->
This is the current state of the robot, photos taken today. This picture shows the robot with links 2, 3 and 5 fully extended.

I also took a video of the robot calibrating itself today: http://youtu.be/MmOgYaB4Gns