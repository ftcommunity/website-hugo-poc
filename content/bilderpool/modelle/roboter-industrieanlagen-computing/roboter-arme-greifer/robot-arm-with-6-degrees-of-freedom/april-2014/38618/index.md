---
layout: "image"
title: "Greifer (3926)"
date: "2014-04-22T16:15:54"
picture: "Greifer_3926.jpg"
weight: "27"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38618
- /details0460-2.html
imported:
- "2019"
_4images_image_id: "38618"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38618 -->
