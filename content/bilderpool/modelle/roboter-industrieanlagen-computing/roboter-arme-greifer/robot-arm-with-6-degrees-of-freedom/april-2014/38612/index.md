---
layout: "image"
title: "Gelenk 6 (3907)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_6_3906_2.jpg"
weight: "21"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38612
- /details822d.html
imported:
- "2019"
_4images_image_id: "38612"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38612 -->
Gelenk 6 in ca. 50%.