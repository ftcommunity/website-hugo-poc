---
layout: "image"
title: "Gelenk 4 (3898)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_4_3898.jpg"
weight: "13"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38604
- /detailsf496.html
imported:
- "2019"
_4images_image_id: "38604"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38604 -->
Gelenk 4 ca. 25% gedreht. Man beachte die beiden Encodermotoren in der Mitte (es sind zwei, der hintere ist nur schlecht sichtbar); diese treiben die Gelenke 5 und 6 über eine Hohlwelle durch den Drehkranz von Gelenk Nr. 4 an (besser zu sehen in Bild 3932).