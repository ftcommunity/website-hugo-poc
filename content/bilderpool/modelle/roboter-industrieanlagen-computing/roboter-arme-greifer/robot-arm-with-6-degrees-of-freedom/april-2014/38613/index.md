---
layout: "image"
title: "Gelenk 6 (3908)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_6_3908.jpg"
weight: "22"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38613
- /details9b47.html
imported:
- "2019"
_4images_image_id: "38613"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38613 -->
Gelenk Nr. 6 auf 100%. Man erkennt den Unterschied fast nur an der Verdrehung der Schläuche links unten.