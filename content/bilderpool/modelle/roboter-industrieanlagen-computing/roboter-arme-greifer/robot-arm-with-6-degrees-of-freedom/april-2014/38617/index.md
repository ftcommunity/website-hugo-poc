---
layout: "image"
title: "Greifer (3925)"
date: "2014-04-22T16:15:54"
picture: "Greifer_3925.jpg"
weight: "26"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38617
- /details9013.html
imported:
- "2019"
_4images_image_id: "38617"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38617 -->
