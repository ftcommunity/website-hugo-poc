---
layout: "image"
title: "Gelenk 6 (3906)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_6_3906.jpg"
weight: "20"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38611
- /details47e1-2.html
imported:
- "2019"
_4images_image_id: "38611"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38611 -->
Gelenk 6 rotiert den Greifer selbst, so dass man Teile in verschiedenen Positionen greifen kann. Hier sind am meisten Zahnräder am Werk: angetrieben wird Gelenk 6 tatsächlich von dem hinteren der beiden Encodermotoren vor Gelenk Nr. 4.