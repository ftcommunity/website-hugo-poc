---
layout: "image"
title: "Gelenk 2 (3890)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_2_3890.jpg"
weight: "8"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38599
- /details6bc4.html
imported:
- "2019"
_4images_image_id: "38599"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38599 -->
Hier ist das Gelenk 2 voll ausgefahren (auf 100%): jetzt ist der Endtaster links von dem anderen Statikstein gedrückt. Der obere gelbe Statikstein drückte im vorigen Bild den Endtaster rechts.