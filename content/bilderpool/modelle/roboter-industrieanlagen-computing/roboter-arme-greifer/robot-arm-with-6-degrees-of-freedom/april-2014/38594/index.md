---
layout: "image"
title: "A - Gesamtansicht (3922)"
date: "2014-04-22T16:15:53"
picture: "A_-_Gesamtansicht_3922.jpg"
weight: "3"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38594
- /details2c54.html
imported:
- "2019"
_4images_image_id: "38594"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38594 -->
Gelenk 2 und 3 maximal gestreckt. Wie man an den gelben Steinen sieht, ist das aber nicht die Maximalstellung dieser Gelenke, die können überstrecken (im Video gut sichtbar).