---
layout: "image"
title: "Gelenk 4 (3897)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_4_3897.jpg"
weight: "12"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38603
- /details682b-3.html
imported:
- "2019"
_4images_image_id: "38603"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38603 -->
Gelenk 4 dreht sich um Achsteil Nr. 3 (also in "roll"-Dimension; vgl. http://en.wikipedia.org/wiki/Aircraft_principal_axes ). Damit kann das Handgelenk mit den Gelenken 5 und 6 positioniert werden. Hier ist wieder ein Encodermotor am Werk, der ganz rechts über dem Gelenk Nr. 3 sitzt und über eine lange Rastachse antreibt. Unterhalb der Leuchte oben sitzen die beiden Endtaster für Gelenk Nr. 4, das sich um fast 360° drehen kann. (Die Beschränkung liegt wiederum an Kabeln und Schläuchen.)