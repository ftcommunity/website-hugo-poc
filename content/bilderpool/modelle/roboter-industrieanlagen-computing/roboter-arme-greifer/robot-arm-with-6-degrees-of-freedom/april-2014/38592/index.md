---
layout: "image"
title: "A - Gesamtansicht (3919)"
date: "2014-04-22T16:15:53"
picture: "A_-_Gesamtansicht_3919.jpg"
weight: "1"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38592
- /details97ac.html
imported:
- "2019"
_4images_image_id: "38592"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38592 -->
Der Roboter heißt "Bertha". Hier sieht man Bertha in minimaler Größe, komplett eingefaltet. Alle Gelenke sind in Stellung 0. Diese Stellung ist so definiert, dass Bertha minimal Platz verbraucht. Sechs Endtaster sind gedrückt, so dass die Software die Position eindeutig bestimmen kann. Aus dieser Stellung kann die Kalibrierung gestartet werden (siehe Video). Die Kalibrierung fährt alle sechs Gelenke jeweils bis zum anderen Endtaster einmal voll aus, so dass die Software für jedes Gelenk die Encoder-Ticks zählen kann. Die Endtaster werden von den gelben Statik-Winkelträgern 15 im Bild gedrückt, so ist das deutlich zu erkennen.