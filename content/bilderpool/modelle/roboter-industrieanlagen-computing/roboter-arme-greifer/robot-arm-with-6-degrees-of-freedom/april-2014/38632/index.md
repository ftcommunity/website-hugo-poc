---
layout: "image"
title: "Y - Arduino (3934)"
date: "2014-04-22T16:15:54"
picture: "Y_-_Arduino_3934.jpg"
weight: "41"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38632
- /detailsc0bc.html
imported:
- "2019"
_4images_image_id: "38632"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38632 -->
Elektronik von der anderen Seite. Das schöne alte ft-Relais schaltet zwischen den beiden Ventilen für die Pneumatik um. For old time's sake.