---
layout: "image"
title: "Gelenk 2 (3889)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_2_3889.jpg"
weight: "7"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38598
- /details462f.html
imported:
- "2019"
_4images_image_id: "38598"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38598 -->
Der rote Powermotor quält sich in Gelenk 2. Die Übersetzung 50:1 ist gerade noch so in der Lage, den Arm auch bei voller Streckung noch anzuheben, aber man hört die Schmerzen. Da der Powermotor kein Encodersignal liefert, sitzt an der Achse ein Minitaster (vertikal unter dem horizontalen Endtaster links zu sehen). Das Gelenk ist in der Nullposition: der Endtaster rechts wird von dem gelben Winkelträger 15 gedrückt.