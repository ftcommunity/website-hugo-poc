---
layout: "image"
title: "Gelenk 3 (3892)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_3_3892.jpg"
weight: "10"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38601
- /details6d54.html
imported:
- "2019"
_4images_image_id: "38601"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38601 -->
Gelenk 3 ist jetzt in Stellung 100%, der Endtaster rechts ist gedrückt.

Armteil 2 besteht übrigens tatsächlich nur aus einem einzigen Aluprofil. Hier sieht man aber schön, welche Kräfte wirken -- oberhalb des Profiles musste ich sehr verstärken, um den Drehkranz und den grauen Powermotor in Stellung zu halten.