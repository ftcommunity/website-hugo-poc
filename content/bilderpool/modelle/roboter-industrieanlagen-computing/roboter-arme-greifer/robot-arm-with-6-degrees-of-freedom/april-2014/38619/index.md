---
layout: "image"
title: "Greifer geschlossen (3917)"
date: "2014-04-22T16:15:54"
picture: "Greifer_geschlossen_3917.jpg"
weight: "28"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38619
- /detailsb3d4.html
imported:
- "2019"
_4images_image_id: "38619"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38619 -->
Der Greifer kann auch schwerere Teile als eine Packung Taschentücher greifen, allerdings muss ich noch mit Gummibändern experimentieren, damit die Sachen nicht immer wegrutschen.