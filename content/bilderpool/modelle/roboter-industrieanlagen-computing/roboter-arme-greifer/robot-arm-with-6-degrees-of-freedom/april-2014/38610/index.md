---
layout: "image"
title: "Gelenk 5 (3905)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_5_3905.jpg"
weight: "19"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38610
- /detailsa894-2.html
imported:
- "2019"
_4images_image_id: "38610"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38610 -->
Gelenk Nr. 5 in 100%-Position: der Endtaster oben ist gedrückt.