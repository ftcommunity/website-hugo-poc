---
layout: "image"
title: "Nov 2013 (2)"
date: "2014-02-08T22:18:24"
picture: "IMG_1938.jpg"
weight: "2"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38215
- /details4090-2.html
imported:
- "2019"
_4images_image_id: "38215"
_4images_cat_id: "2844"
_4images_user_id: "2106"
_4images_image_date: "2014-02-08T22:18:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38215 -->
Side shot of that same old version.