---
layout: "image"
title: "18 Stand Sept: gesamt (5490)"
date: "2014-09-04T22:58:48"
picture: "18_Stand_Sept_gesamt_5490.jpg"
weight: "18"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39334
- /detailsd376.html
imported:
- "2019"
_4images_image_id: "39334"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39334 -->
Das ist der Prototyp für den dritten Arduino, der auf dem Arm sitzen und die Motoren 2 bis 5 ansteuern wird. Während Motor 1, wie gesagt, direkt vom Controller angetrieben wird, wird der Controller für die Motoren 2 bis 5 über I2C Befehle an diesen Arduino hier schicken, der dann die Motoren entsprechend steuert. Der Grund dafür ist, dass sich so nur vier Kabel mit der Grundplatte drehen müssen (Stromversorgung und I2C). Bei V1 war es ein Wust von über 30 Kabeln, die der Arm hinter sich herzog und die sich auch öfters mal verhakten.

Links sieht man einen weiteren Arduino Micro, rechts drei doppelte Motortreiber TI DRV8835, für die es ein schönes Board gibt (http://www.pololu.com/product/2135 -- bei Watterott erhältlich). Jeder der Treiber kann zwei Motoren treiben, also habe ich insgesamt sechs Motorausgänge hier. Mit derm schwarzen Taster wird derzeit ein Eingang auf HIGH gezogen, dann geht der Encodermotor an zum Testen. (Der graue Taster ist mit Masse und RESET vom Micro verbunden.)

Die roten Kabel von links sind wieder die Stromversorgung +9V vom Controller. Das I2C-Kabel wird noch hinzukommen.