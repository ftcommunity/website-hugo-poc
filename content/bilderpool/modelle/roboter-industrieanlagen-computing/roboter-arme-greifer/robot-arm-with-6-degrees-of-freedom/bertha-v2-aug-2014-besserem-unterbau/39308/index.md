---
layout: "image"
title: "10 Bertha V2 gesamt (5464)"
date: "2014-08-26T18:59:41"
picture: "10_Bertha_V2_gesamt_5464.jpg"
weight: "10"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39308
- /details9fb4-2.html
imported:
- "2019"
_4images_image_id: "39308"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39308 -->
Wie 09, aber der Arm teilweise eingeklappt. Das Foto ist neuer als 09, hier sieht man, dass der Encodermotor in der Mitte jetzt anders montiert ist (noch besser auf den folgenden Fotos).