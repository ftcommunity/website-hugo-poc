---
layout: "image"
title: "13 Stand Sept: Übersicht (5481)"
date: "2014-09-04T22:58:48"
picture: "13_Stand_Sept_bersicht_5481.jpg"
weight: "13"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39329
- /detailsba12.html
imported:
- "2019"
_4images_image_id: "39329"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39329 -->
Der Roboterarm selbst ist seit Aug im wesentlichen unverändert. Hinzugekommen ist der Arduino Uno auf dem Sockel und ein LCD (mehr bei Bild 16), die Fernbedienung mit einem Arduino Micro (mehr bei Bild 17) und der Prototyp für einen dritten Arduino, der auf dem Arm sein wird (mehr bei Bild 18).