---
layout: "image"
title: "01 Bertha V2 Baustufe (5434)"
date: "2014-08-26T18:59:41"
picture: "01_Bertha_V2_Baustufe_5434.jpg"
weight: "1"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39299
- /details85ea.html
imported:
- "2019"
_4images_image_id: "39299"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39299 -->
Der Kern des Fußes (montiert in Bild 04 zu sehen). Die schwarzen 90-Grad-Anbauwiinkel 32615 machen das Ding so stabil, dass es tatsächlich nachher auf 4 Bausteinen 30 stehen kann. Die acht schwarzen Bausteine 30 auf dem Drehkranz umschließen ein Aluprofil, von dem nur der obere schwarze Zapfen zu sehen ist (über vier Verbinder mit den Bausteinen 30 verbunden). Das Aluprofil geht durch den oberen UND den unteren Drehkranz und zwingt beide zur synchronen Drehung. Mit dieser Konstruktion kann der Fuß die extremen seitlichen Kräfte aushalten, die bei seitlicher Streckung des Roboterarms auftreten, ohne dass die Bausteine sich verbiegen oder die Drehkränze herausspringen.