---
layout: "image"
title: "15 Stand Sept: LCD (5485)"
date: "2014-09-04T22:58:48"
picture: "15_Stand_Sept_LCD_5485.jpg"
weight: "15"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39331
- /detailsc2aa.html
imported:
- "2019"
_4images_image_id: "39331"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39331 -->
Hier sieht man die Anzeigetafel. Die Motoren 2 bis 5 gibt es noch nicht, deswegen wird für sie "??" angezeigt. Motor 1 hat die Position 96%. Wie man also sieht, funktionieren Encodersignal, Endtaster und Kalibrierung für Motor 1 bereits. Motor 1 wird, da er unterhalb der Drehplatte sitzt, direkt vom Hauptcontroller mit Adafruit Motor Shield (Bild 16) angetrieben.

Das Display ist von SainSmart und wird über I2C angesteuert. Es ist schön hell, hat 4x20 Zeichen und braucht eben nur die zwei I2C-Pins am Arduino.