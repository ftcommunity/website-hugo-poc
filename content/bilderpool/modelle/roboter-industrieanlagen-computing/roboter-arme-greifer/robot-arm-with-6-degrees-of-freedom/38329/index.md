---
layout: "image"
title: "Feb 2014 -- all links in their home position"
date: "2014-02-22T11:10:25"
picture: "IMG_3076.jpg"
weight: "4"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38329
- /detailsf1f6-3.html
imported:
- "2019"
_4images_image_id: "38329"
_4images_cat_id: "2844"
_4images_user_id: "2106"
_4images_image_date: "2014-02-22T11:10:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38329 -->
This picture shows all six links in their "home" position, i.e. the default position after the robot has started.