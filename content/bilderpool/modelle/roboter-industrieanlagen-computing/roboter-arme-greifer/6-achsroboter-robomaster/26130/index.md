---
layout: "image"
title: "Seitenansicht"
date: "2010-01-24T18:15:18"
picture: "DSCN55362.jpg"
weight: "10"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26130
- /details2d8f.html
imported:
- "2019"
_4images_image_id: "26130"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T18:15:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26130 -->
hier kann man gut erkannen, dass sich der Schwerpunkt im Mittelpunkt befindet