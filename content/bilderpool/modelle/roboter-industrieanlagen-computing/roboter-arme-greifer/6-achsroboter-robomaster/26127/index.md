---
layout: "image"
title: "roboterarm zurück gezogen"
date: "2010-01-24T18:00:47"
picture: "DSCN55332.jpg"
weight: "7"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
schlagworte: ["roboterarm"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26127
- /details4391.html
imported:
- "2019"
_4images_image_id: "26127"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T18:00:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26127 -->
In dieser Position würde man ein Objekt greifen, welches sehr nah und etwas höher liegt. Der Roboter könnte so auch schwere Dinge heben, weil der Schwerpunkt im Mittelpunkt liegt.