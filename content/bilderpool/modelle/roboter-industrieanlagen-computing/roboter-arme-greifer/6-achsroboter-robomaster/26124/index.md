---
layout: "image"
title: "Roboterarm nachforn gebeugt.2"
date: "2010-01-24T17:34:25"
picture: "DSCN55302.jpg"
weight: "4"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
schlagworte: ["Roboterarm", "RoboMaster"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- /php/details/26124
- /detailsa21c.html
imported:
- "2019"
_4images_image_id: "26124"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T17:34:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26124 -->
Hier kann man noch einmal sehen wie weit der Greifer, an dem später das Gewicht hängt vom Mittelpunkt entfernt ist.