---
layout: "image"
title: "Robot-2005"
date: "2005-04-20T16:01:47"
picture: "Fischertechnik-Robot-2005_004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4048
- /details9a5b.html
imported:
- "2019"
_4images_image_id: "4048"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-04-20T16:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4048 -->
