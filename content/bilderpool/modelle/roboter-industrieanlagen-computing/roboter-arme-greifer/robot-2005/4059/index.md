---
layout: "image"
title: "Robot-2005"
date: "2005-04-20T16:01:47"
picture: "Fischertechnik-Robot-2005_015.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4059
- /detailsd73f.html
imported:
- "2019"
_4images_image_id: "4059"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-04-20T16:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4059 -->
