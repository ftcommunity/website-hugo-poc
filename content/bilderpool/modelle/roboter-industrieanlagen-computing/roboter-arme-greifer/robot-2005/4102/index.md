---
layout: "image"
title: "Robot-2005"
date: "2005-05-02T10:15:54"
picture: "Robot-2005_002.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/4102
- /details6412.html
imported:
- "2019"
_4images_image_id: "4102"
_4images_cat_id: "348"
_4images_user_id: "22"
_4images_image_date: "2005-05-02T10:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4102 -->
