---
layout: "image"
title: "bewegungsraum 004"
date: "2005-01-04T18:55:18"
picture: "bewegungsraum_004.JPG"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3522
- /detailsee0c-2.html
imported:
- "2019"
_4images_image_id: "3522"
_4images_cat_id: "186"
_4images_user_id: "5"
_4images_image_date: "2005-01-04T18:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3522 -->
