---
layout: "image"
title: "neues Handgelenk 1"
date: "2005-11-23T21:23:50"
picture: "Handgelenk_neu_-_1.jpg"
weight: "25"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["Roboterarm", "Handgelenk", "Getriebe", "Kegelrad", "Kegelräder", "Brickwedde", "Sägen", "Säge", "Aluprofil"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5355
- /detailse025.html
imported:
- "2019"
_4images_image_id: "5355"
_4images_cat_id: "186"
_4images_user_id: "9"
_4images_image_date: "2005-11-23T21:23:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5355 -->
