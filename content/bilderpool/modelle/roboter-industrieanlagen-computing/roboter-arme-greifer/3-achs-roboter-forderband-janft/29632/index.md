---
layout: "image"
title: "Antrieb Hebeturm"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband17.jpg"
weight: "17"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29632
- /details30e0-2.html
imported:
- "2019"
_4images_image_id: "29632"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29632 -->
Anschluss an Interface: M3