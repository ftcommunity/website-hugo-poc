---
layout: "image"
title: "Antrieb Drehkranz"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband16.jpg"
weight: "16"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29631
- /detailsbf7e.html
imported:
- "2019"
_4images_image_id: "29631"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29631 -->
Anschluss an Interface: M1