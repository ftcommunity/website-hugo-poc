---
layout: "image"
title: "3-achs Roboter"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband02.jpg"
weight: "2"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29617
- /detailse9e0.html
imported:
- "2019"
_4images_image_id: "29617"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29617 -->
Der  Roboter ist aus dem Set "Industry Robots"