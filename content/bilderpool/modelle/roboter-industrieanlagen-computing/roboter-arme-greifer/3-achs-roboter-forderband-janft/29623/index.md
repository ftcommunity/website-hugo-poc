---
layout: "image"
title: "Verkabelung 3-achs Roboter"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband08.jpg"
weight: "8"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29623
- /details8d24.html
imported:
- "2019"
_4images_image_id: "29623"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29623 -->
Es sind:
3 mini Motoren
1 Power Motor
8 Taster
verbaut