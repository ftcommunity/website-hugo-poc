---
layout: "image"
title: "ausfahrbarer Arm"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband12.jpg"
weight: "12"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29627
- /details882c-2.html
imported:
- "2019"
_4images_image_id: "29627"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29627 -->
wird mit einem Mini Mot angetrieben