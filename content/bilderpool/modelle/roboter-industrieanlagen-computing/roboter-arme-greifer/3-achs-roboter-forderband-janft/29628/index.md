---
layout: "image"
title: "Greifer mit Werkstück"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband13.jpg"
weight: "13"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/29628
- /details687d.html
imported:
- "2019"
_4images_image_id: "29628"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29628 -->
wird mit einem mini Mot angetrieben