---
layout: "image"
title: "Greifroboter"
date: "2007-12-04T16:58:10"
picture: "greifer2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/12999
- /detailsc08d-2.html
imported:
- "2019"
_4images_image_id: "12999"
_4images_cat_id: "1178"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12999 -->
