---
layout: "image"
title: "Relaisplatine"
date: "2010-09-18T13:36:53"
picture: "achsroboterarmgreifergesteuertviawebcam06.jpg"
weight: "6"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28168
- /details9264-3.html
imported:
- "2019"
_4images_image_id: "28168"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28168 -->
Das ist die selbstgebaute/gelötete Relaisplatine, die dafür sorgt, dass an den Motoren, die stark belastet werden (M1 + M3, Achse 1 und 3) genug Strom ankommt. 
Da am Interface zu wenig "Strom rauskommt", wird hier über ein externes Netzteil die benötigte Energie eingespeist...
