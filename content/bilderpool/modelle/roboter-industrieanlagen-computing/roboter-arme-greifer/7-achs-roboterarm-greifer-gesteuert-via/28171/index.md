---
layout: "image"
title: "Zusatz-Interface"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam09.jpg"
weight: "9"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28171
- /details9226.html
imported:
- "2019"
_4images_image_id: "28171"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28171 -->
Ich habe (falls das eine Interface plötzlich den Geist aufgeben sollte) zum Glück noch ein weiteres ersteigert (relativ günstig), wodurch ich immer ein Backup dabei habe :)
