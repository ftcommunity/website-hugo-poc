---
layout: "image"
title: "Webcam-Mast"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam16.jpg"
weight: "16"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28178
- /detailsfb87.html
imported:
- "2019"
_4images_image_id: "28178"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28178 -->
Damit die Webcam den nötigen "Überblick" hat, sitzt sie hoch über dem Roboter (ca. 1,5 m)...
