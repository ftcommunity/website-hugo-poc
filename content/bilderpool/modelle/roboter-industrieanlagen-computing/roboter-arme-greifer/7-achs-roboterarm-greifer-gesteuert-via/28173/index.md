---
layout: "image"
title: "Achse 2"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam11.jpg"
weight: "11"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28173
- /details017c.html
imported:
- "2019"
_4images_image_id: "28173"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28173 -->
Hier ist die Achse 2 teils zu sehen
