---
layout: "image"
title: "Delta Version 2 Oberarm"
date: "2009-04-12T23:41:58"
picture: "Delta_Oberarm-004.jpg"
weight: "11"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23666
- /details88bc-2.html
imported:
- "2019"
_4images_image_id: "23666"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2009-04-12T23:41:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23666 -->
Nochmal der Oberarm als Ganzes. 

Das Faszinierende ist, dass nur die drei Schultern angetrieben werden. Die Ellbogen drehen sich um zwei Achsen, die Handgelenke ebenso. Wenn man nur steif genug baut, bleibt dann die Arbeitsplattform in der waagrechten.

Mit fischertechnik ist genau das herausfordernd. Schon das geringste Spiel reicht, um die Arbeitsplattform deutlich zu kippen. In Belastungsrichtung (nach unten, wo man etwas dranhängen würde) treten am Arm zwar keine Lastwechsel auf, was die Konstruktion ziemlich sexy macht. Weil aber die Unterarme nach links und nach rechts gekippt werden, und dann in unterschiedliche Richtungen verwinden können, ist der Aufbau schon wieder heikel.

Hier steckt der Teufel im Detail. Aber vielleicht wird aus mir ja noch ein guter Exorzist.