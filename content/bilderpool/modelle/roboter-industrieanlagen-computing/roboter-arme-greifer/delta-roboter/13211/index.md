---
layout: "image"
title: "Delta-Roboter Version 1 Arme gestreckt"
date: "2008-01-03T02:47:40"
picture: "delta-prototyp-8.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/13211
- /detailseb35.html
imported:
- "2019"
_4images_image_id: "13211"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:47:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13211 -->
Genau dann, wenn alle Arme gestreckt sind, läuft der Roboter in eine Singularität. Von dort aus können die Ellbogen nach innen oder außen einklappen. 

Andere Singularitäten gibt es nicht.