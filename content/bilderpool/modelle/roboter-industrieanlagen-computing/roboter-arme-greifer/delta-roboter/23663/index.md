---
layout: "image"
title: "Delta Version 2 Schulter"
date: "2009-04-12T23:27:19"
picture: "Delta_Oberarm-001.jpg"
weight: "8"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["Flexpicker", "Delta", "Roboter"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/23663
- /details5ff2.html
imported:
- "2019"
_4images_image_id: "23663"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2009-04-12T23:27:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23663 -->
Zweite Version, diesmal mit stabilem Rahmen, so dass die Arbeitsplattform nach unten hängt. 

Die Antriebe sind völlig vorläufig, ungetestet und sollen die Drehkränze nur in Stellung halten :-)