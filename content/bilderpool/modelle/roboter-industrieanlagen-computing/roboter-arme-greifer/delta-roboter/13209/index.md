---
layout: "image"
title: "Delta-Roboter Version 1 Rechts"
date: "2008-01-03T02:46:07"
picture: "delta-prototyp-5.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/13209
- /details2956-2.html
imported:
- "2019"
_4images_image_id: "13209"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:46:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13209 -->
