---
layout: "image"
title: "Teilasnicht II"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset03.jpg"
weight: "3"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/30202
- /details6231-2.html
imported:
- "2019"
_4images_image_id: "30202"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30202 -->
Teilansicht des Roboters