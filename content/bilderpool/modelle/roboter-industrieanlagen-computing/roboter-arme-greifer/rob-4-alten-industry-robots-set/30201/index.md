---
layout: "image"
title: "Teilansicht I"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset02.jpg"
weight: "2"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/30201
- /detailscb95.html
imported:
- "2019"
_4images_image_id: "30201"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30201 -->
Teilansicht des Roboters