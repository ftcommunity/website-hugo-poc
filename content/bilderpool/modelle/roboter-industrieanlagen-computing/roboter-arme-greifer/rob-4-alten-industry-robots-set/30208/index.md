---
layout: "image"
title: "Drehkranz + Encoder Motor"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset09.jpg"
weight: "9"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/30208
- /details3713.html
imported:
- "2019"
_4images_image_id: "30208"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30208 -->
Links Encoder Motor und EndLichtschranke. Die Lampe leutet nur, wenn der Motor sich dreht