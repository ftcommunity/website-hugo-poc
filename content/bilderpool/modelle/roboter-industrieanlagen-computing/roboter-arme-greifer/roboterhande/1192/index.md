---
layout: "image"
title: "Das Steuerpult"
date: "2003-06-20T06:11:03"
picture: "fthandpult.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1192
- /details10bd.html
imported:
- "2019"
_4images_image_id: "1192"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1192 -->
Das steuern mit den Handschuhen war ziemlich schwierig, dashalb das Pult. Damit auch ungeübte was damit anfangen konnten.