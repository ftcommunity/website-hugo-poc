---
layout: "image"
title: "Die Handschuhe"
date: "2003-06-20T06:11:03"
picture: "fthandschuhe.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1193
- /detailsa845.html
imported:
- "2019"
_4images_image_id: "1193"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1193 -->
Ich hatte damals viel rumprobiert. Auch um die Steuerung proportional zu ermöglichen. Aber das war aufgrund der fehlenden Materialien wie z.B. Dehnungsmessstreifen (wie in echten CyberspaceHandschuhen üblich) nicht so einfach machbar. Deshalb blieb nur die "Auf-Stop-Zu"-steuerung via Neigungsschalter. Aber eine ruhige Hand musste man schon haben. Außerdem durfte man die Hand nicht um 180 Grad drehen sonst schlug die Sicherung an (vorwärts und rückwärts gleichzeitig geht halt nicht ;-) Aber mit ein bisschen Übung...