---
layout: "image"
title: "'Schultergelenk'"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39226
- /details0db9.html
imported:
- "2019"
_4images_image_id: "39226"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39226 -->
Das Schultergelenk wird von zwei parallelen und synchronen Schneckengetrieben und einem Powermotor angetrieben, die zwei Drehkränze steuern. Dadurch verteilt sich die Last gleichmäßiger. Es sitzt direkt auf einem dritten Drehkranz, der den gesamten Knickarm um die eigene Achse dreht, angetrieben von einem Mini-Motor.