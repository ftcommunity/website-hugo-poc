---
layout: "image"
title: "Knickarm halb aufgerichtet"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39223
- /details1079.html
imported:
- "2019"
_4images_image_id: "39223"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39223 -->
Der Greifer erreicht bei aufgerichtetem unterem Abschnitt auf dem Boden stehende Objekte.