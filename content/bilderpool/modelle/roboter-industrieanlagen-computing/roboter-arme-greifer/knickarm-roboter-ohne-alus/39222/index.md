---
layout: "image"
title: "Gesamtansicht Knickarm-Roboter"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39222
- /detailsdd72.html
imported:
- "2019"
_4images_image_id: "39222"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39222 -->
Der Knickarm-Roboter wird von zwei Power-, einem Mini- und einem XS-Motor (Greifer) angetrieben.
An den TX ist via I²C-Protokoll ein kabelloser Nunchuk als Fernsteuerung angeschlossen.