---
layout: "overview"
title: "Knickarm-Roboter (ohne Alus)"
date: 2020-02-22T08:03:22+01:00
legacy_id:
- /php/categories/2934
- /categories7b10.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2934 --> 
Ein Roboterarm mit drei Freiheitsgraden - ohne Alus, ohne Modding.