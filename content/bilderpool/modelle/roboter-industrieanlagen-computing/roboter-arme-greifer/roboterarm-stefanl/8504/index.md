---
layout: "image"
title: "Roboterarm 8"
date: "2007-01-18T22:04:46"
picture: "roboterarm08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8504
- /details9300.html
imported:
- "2019"
_4images_image_id: "8504"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8504 -->
