---
layout: "image"
title: "Roboterarm 23"
date: "2007-01-20T16:46:18"
picture: "roboterarm11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8551
- /details4f2b.html
imported:
- "2019"
_4images_image_id: "8551"
_4images_cat_id: "788"
_4images_user_id: "502"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8551 -->
