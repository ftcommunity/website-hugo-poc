---
layout: "image"
title: "Roboterarm 13"
date: "2007-01-20T16:46:18"
picture: "roboterarm01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8541
- /detailsf6c8.html
imported:
- "2019"
_4images_image_id: "8541"
_4images_cat_id: "788"
_4images_user_id: "502"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8541 -->
