---
layout: "image"
title: "Roboterarm 6"
date: "2007-01-18T22:04:46"
picture: "roboterarm06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8502
- /details5f0a.html
imported:
- "2019"
_4images_image_id: "8502"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8502 -->
