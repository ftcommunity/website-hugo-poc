---
layout: "comment"
hidden: true
title: "2090"
date: "2007-01-20T12:20:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Falls es wirklich mal an Impulsrädern mangelt, gehen ersatzweise auch die alten kleinen Seilrollen 31016, http://www.knobloch-gmbh.de/shop/query.php?cp_sid=284034f68820&cp_tpl=5504&cp_pid=16149&cp_cat=

So findet sich das auch in den Anleitungen zu den frühen Computingkästen. Man hat nur halb so viele Impulse pro Umdrehung wie beim Impulsrad, aber damit kann man wohl auch leben.

Gruß,
Stefan