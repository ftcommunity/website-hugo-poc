---
layout: "image"
title: "Roboterarm 6"
date: "2008-08-17T18:33:42"
picture: "roboterarmneu06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15057
- /detailsf7c1.html
imported:
- "2019"
_4images_image_id: "15057"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15057 -->
