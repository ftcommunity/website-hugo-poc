---
layout: "image"
title: "Roboterarm 3"
date: "2008-08-17T18:33:41"
picture: "roboterarmneu03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15054
- /detailsb3a2.html
imported:
- "2019"
_4images_image_id: "15054"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15054 -->
