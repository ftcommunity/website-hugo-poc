---
layout: "image"
title: "Roboterarm 1"
date: "2008-08-17T18:33:41"
picture: "roboterarmneu01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15052
- /details2eb1-2.html
imported:
- "2019"
_4images_image_id: "15052"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15052 -->
