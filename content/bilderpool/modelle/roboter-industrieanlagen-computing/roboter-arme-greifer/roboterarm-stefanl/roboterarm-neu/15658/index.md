---
layout: "image"
title: "Roboterarm 16"
date: "2008-09-28T13:58:10"
picture: "roboterarm6.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15658
- /detailse66b.html
imported:
- "2019"
_4images_image_id: "15658"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15658 -->
