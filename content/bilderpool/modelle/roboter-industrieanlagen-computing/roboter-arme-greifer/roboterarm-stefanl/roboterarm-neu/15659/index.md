---
layout: "image"
title: "Roboterarm 17"
date: "2008-09-28T13:58:10"
picture: "roboterarm7.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/15659
- /detailsfa89.html
imported:
- "2019"
_4images_image_id: "15659"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15659 -->
Hier der Testaufbau. Der Roboterarm nimmt...
