---
layout: "image"
title: "Roboterarm 3"
date: "2007-01-18T22:04:46"
picture: "roboterarm03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8499
- /details4ca6.html
imported:
- "2019"
_4images_image_id: "8499"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8499 -->
