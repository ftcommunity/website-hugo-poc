---
layout: "image"
title: "Roboterarm 7"
date: "2007-01-18T22:04:46"
picture: "roboterarm07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8503
- /details78b6-2.html
imported:
- "2019"
_4images_image_id: "8503"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8503 -->
