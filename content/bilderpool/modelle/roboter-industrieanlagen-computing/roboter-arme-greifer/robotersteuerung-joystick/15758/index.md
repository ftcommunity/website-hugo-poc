---
layout: "image"
title: "Joy2Rob 2"
date: "2008-09-30T22:22:56"
picture: "IMGP8024.jpg"
weight: "2"
konstrukteure: 
- "Friedrich Mütschele nach FT Anleitung Modifiziert"
fotografen:
- "Friedrich Mütschele"
uploadBy: "fridl"
license: "unknown"
legacy_id:
- /php/details/15758
- /details952c.html
imported:
- "2019"
_4images_image_id: "15758"
_4images_cat_id: "1441"
_4images_user_id: "753"
_4images_image_date: "2008-09-30T22:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15758 -->
