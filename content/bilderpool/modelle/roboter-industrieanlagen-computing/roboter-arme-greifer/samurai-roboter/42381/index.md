---
layout: "image"
title: "Samurai Roboter von vorne"
date: "2015-11-15T19:54:42"
picture: "dirkw1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42381
- /detailsd484-3.html
imported:
- "2019"
_4images_image_id: "42381"
_4images_cat_id: "3154"
_4images_user_id: "2303"
_4images_image_date: "2015-11-15T19:54:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42381 -->
Hallo zusammen,

hier ein Versuch den Samurai Roboter von 2003 weiter zu automatisieren.
Leider ist der Versuch gescheitert, da dieser zu "Kopflastig" geworden ist. :-(

Die Beine sollten mit Hilfe von 2 Encoder-Motoren angehoben werden. Die 
Füße, wie in der Bauanleitung, über 2 S-Moteren mit Hubgetriebe.

Pdf-Download:
http://www.fischertechnik-museum.ch/museum/displayimage.php?album=72&pos=77
