---
layout: "image"
title: "Samurai Roboter Beine"
date: "2015-11-15T19:54:42"
picture: "dirkw4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42384
- /detailsbc40.html
imported:
- "2019"
_4images_image_id: "42384"
_4images_cat_id: "3154"
_4images_user_id: "2303"
_4images_image_date: "2015-11-15T19:54:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42384 -->
Hier mit Encoder-Motoren zum anheben der Beine.
