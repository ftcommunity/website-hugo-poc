---
layout: "overview"
title: "5-Achs Roboter (david-ftc)"
date: 2020-02-22T08:03:23+01:00
legacy_id:
- /php/categories/2990
- /categories93d2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2990 --> 
Dies ist mein erster Roboter, den drei drehbaren Achsen basiert: A, B und C. Diese Achsen erlauben der Maschine, eine sehr große Anzahl an Positionen anzufahren. Um den Roboter noch flexiebler zu machen, lässt sich der gesamte Aufbau drehen und horizontal um etwa 30 cm bewegen.