---
layout: "image"
title: "X und Y Achse"
date: "2014-11-23T19:12:24"
picture: "far3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39847
- /details2dfc.html
imported:
- "2019"
_4images_image_id: "39847"
_4images_cat_id: "2990"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39847 -->
Die Kabel werden durch zwei Energieketten zum Roboter geführt. Meine Verkabelung ist "interessant": Da der Platz in den Energieketten begrenzt ist, muss ich Kabel sparen. Hierzu füge ich die Masskontakte aller Taster und Encoder zu einem zusammen. Dies wirkt auf den ersten Blick unübersichtlich (ist es eigentlich auch), ist aber unbedingt erforderlich, da ich nur zwei (kurze) Energieketten habe.
