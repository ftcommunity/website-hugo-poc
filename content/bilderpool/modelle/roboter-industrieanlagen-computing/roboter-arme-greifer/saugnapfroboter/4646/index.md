---
layout: "image"
title: "Gesamtansicht Saugnapfroboter"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_070.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4646
- /detailsf477-3.html
imported:
- "2019"
_4images_image_id: "4646"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4646 -->
Hier ist wieder der Roboter mit dem Saugnapf und dem Vakuum-Glas mit Kompressor. Nur dieses mal von der anderen Seite.
