---
layout: "image"
title: "Gesamtansicht"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_069.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4645
- /detailsea41-2.html
imported:
- "2019"
_4images_image_id: "4645"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4645 -->
Saugnapfroboter mit dem Vakuum-Glas und Kompressor.
