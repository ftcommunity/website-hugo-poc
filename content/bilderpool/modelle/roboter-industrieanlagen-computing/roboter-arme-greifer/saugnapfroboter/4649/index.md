---
layout: "image"
title: "Untere Drehfunktion"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_073.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4649
- /detailscdde.html
imported:
- "2019"
_4images_image_id: "4649"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4649 -->
Hier ist der Motor der ein großes Zahnrad betreibt und damit den Roboter zum Drehen bringt.
