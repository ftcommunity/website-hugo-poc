---
layout: "image"
title: "Achse 2 Halterung"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24299
- /details3629.html
imported:
- "2019"
_4images_image_id: "24299"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24299 -->
Eine der 2 Halterungen der Drehkränze für Achse 2. Außerdem befindet sich hier der Kabelkanal