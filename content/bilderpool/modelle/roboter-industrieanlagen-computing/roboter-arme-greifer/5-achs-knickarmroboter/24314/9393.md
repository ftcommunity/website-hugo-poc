---
layout: "comment"
hidden: true
title: "9393"
date: "2009-06-10T20:25:36"
uploadBy:
- "Mr Smith"
license: "unknown"
imported:
- "2019"
---
Das ganze lässt sich relativ leicht mit RoboPro ausgleichen . Wenn ich zB. Achse 5 den Befehl gebe, sich 10 Impulse nach rechts zu bewegen bewegt sich der Greifmotor eine bestimmte Zahl nach rechts. 
Die Achse ist aber eigentlich nur zum kippen gedacht, also bis 180° in beide Richtungen. D.h. eine halbe Drehung der Welle für den Greifer. Bei einer halben Drehung passiert quasi _nichts_ am Greifer. Da der Greifer sowieso etwas fester zugreifen kann, kann man hier den Softwareseitigen Ausgleich auch weglassen. Das ist nur bei der Achse davor nötig. 

Die andere Möglichkeit wäre, den Motor vor den Drehkranz zu setzen. Das habe ich nicht gemacht, weil der Arm in der jetzigen Länge perfekt ausbalanciert ist, was ein Hauptziel war.