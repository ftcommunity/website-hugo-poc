---
layout: "image"
title: "Greifer"
date: "2009-06-10T14:57:08"
picture: "knickarmroboter24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24314
- /detailsd3b2-2.html
imported:
- "2019"
_4images_image_id: "24314"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:08"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24314 -->
Powermotor im Greifer