---
layout: "image"
title: "Antrieb Achse 1"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24294
- /detailsed10-2.html
imported:
- "2019"
_4images_image_id: "24294"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24294 -->
