---
layout: "image"
title: "Drehkranz"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24295
- /details5d6e.html
imported:
- "2019"
_4images_image_id: "24295"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24295 -->
Eine der Befestigungen des Drehkranzes