---
layout: "image"
title: "Die befestigung des Motors für Achse 3"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24303
- /details06d5.html
imported:
- "2019"
_4images_image_id: "24303"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24303 -->
