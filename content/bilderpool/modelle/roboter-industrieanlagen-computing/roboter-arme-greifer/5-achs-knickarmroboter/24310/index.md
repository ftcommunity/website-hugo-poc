---
layout: "image"
title: "Achse 5 Antrieb"
date: "2009-06-10T14:57:08"
picture: "knickarmroboter20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24310
- /details30b5.html
imported:
- "2019"
_4images_image_id: "24310"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:08"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24310 -->
Hier wird der Antrieb von Achse 5 über achse 4 hinweggelenkt. Wenn sich Achse 4 bewegt, bewegt sich dadurch auch ein wenig die Achse 5. Das gleiche ich Softwareseitig aus, Da für beide Achsen Impulszähler vorhanden sind.