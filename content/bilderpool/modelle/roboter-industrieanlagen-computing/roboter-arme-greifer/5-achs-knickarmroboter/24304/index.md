---
layout: "image"
title: "Nochmal die Befestigung des Motors"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24304
- /detailsd102.html
imported:
- "2019"
_4images_image_id: "24304"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24304 -->
