---
layout: "image"
title: "Gesamtansicht"
date: "2009-06-10T14:57:06"
picture: "knickarmroboter01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24291
- /details64d2.html
imported:
- "2019"
_4images_image_id: "24291"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24291 -->
Ein 5-achsiger Knickarmroboter. Das Modell ist darauf ausgelegt, einen Powermotor tragen zu können, ohne das dadurch die Motoren (wesentlich) stärker belastet werden. 
Ich werde wahrscheinlich aber noch ein paar kleinere Dinge verändern.