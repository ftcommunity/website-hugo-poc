---
layout: "image"
title: "Regal"
date: "2007-01-20T16:46:18"
picture: "DSCI0066.jpg"
weight: "8"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/8561
- /detailsab53.html
imported:
- "2019"
_4images_image_id: "8561"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8561 -->
Hier noch mal mein kleines Ragal mit 3 Fächern. Das ist eigentlich nur so als Versuch gedacht für einen größeren Lagerroboter.