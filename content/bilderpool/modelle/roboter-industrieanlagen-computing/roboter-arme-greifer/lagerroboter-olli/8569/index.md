---
layout: "image"
title: "Lagerroboter 3"
date: "2007-01-20T16:46:18"
picture: "DSCI0059.jpg"
weight: "15"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/8569
- /detailscd2f-2.html
imported:
- "2019"
_4images_image_id: "8569"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8569 -->
Der Motor für das Drehen.