---
layout: "image"
title: "Taster zum Bedienen"
date: "2007-01-20T16:46:18"
picture: "DSCI0051.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/8556
- /detailsac52-2.html
imported:
- "2019"
_4images_image_id: "8556"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8556 -->
Hier nochmal die Taster.