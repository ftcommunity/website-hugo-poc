---
layout: "image"
title: "Bedienungstaster"
date: "2007-01-20T16:46:18"
picture: "DSCI0065.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/8555
- /details8f5a-2.html
imported:
- "2019"
_4images_image_id: "8555"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8555 -->
Hier wird die Tonne draufgestellt. Wenn man eine Tonne einlagern will stellt man sie auf den Abholtisch (oder wie soll man das sonst nennen?) und drückt den rechten Taster. Dann ist registriert das eine Tonne eingelagert werden soll. Dann muss man wieder mit dem rechten Taster das Fach angeben in das die Tonne eingelagert werden soll. Mit einem Druck auf den rechten Taster wird der Vorgang gestartet. Wenn man eine Tonne auslagern will muss man am Anfang nur den rechten Taster drücken und dann wie beschrieben vorgehen. Das Programm merkt sogar ob ein Fach leer oder voll ist.