---
layout: "image"
title: "3Achs Rob_01"
date: "2011-03-04T09:23:07"
picture: "3Achs_Rob_1_2.jpg"
weight: "1"
konstrukteure: 
- "con.barriga"
fotografen:
- "con.barriga"
schlagworte: ["LDraw", "MLCad", "LPub"]
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30188
- /detailsadd2-2.html
imported:
- "2019"
_4images_image_id: "30188"
_4images_cat_id: "2242"
_4images_user_id: "1284"
_4images_image_date: "2011-03-04T09:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30188 -->
Auszug einer mit LDraw, MLCAD u. LPub erstellten Anleitung
