---
layout: "image"
title: "Achse 2 im Detail"
date: "2009-03-20T16:55:18"
picture: "achsroboter7.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23479
- /details8d00.html
imported:
- "2019"
_4images_image_id: "23479"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23479 -->
Wie man sieht habe ich die 2 Motoren mechanisch Parallel geschaltet