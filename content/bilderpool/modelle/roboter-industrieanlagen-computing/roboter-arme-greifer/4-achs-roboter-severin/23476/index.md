---
layout: "image"
title: "Grundplatte"
date: "2009-03-20T16:55:18"
picture: "achsroboter4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23476
- /details150e.html
imported:
- "2019"
_4images_image_id: "23476"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23476 -->
Noch eine genaue Ansicht