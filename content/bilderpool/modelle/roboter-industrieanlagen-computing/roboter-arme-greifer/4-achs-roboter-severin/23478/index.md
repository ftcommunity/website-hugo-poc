---
layout: "image"
title: "Seitenansicht"
date: "2009-03-20T16:55:18"
picture: "achsroboter6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/23478
- /detailsd1c0.html
imported:
- "2019"
_4images_image_id: "23478"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23478 -->
Wieder eine Automatisch angefahrene Position