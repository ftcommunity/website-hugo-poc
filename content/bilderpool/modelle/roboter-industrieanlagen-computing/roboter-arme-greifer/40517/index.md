---
layout: "image"
title: "Umgebauter Trainingsroboter von der anderen Seite"
date: "2015-02-09T17:13:11"
picture: "S1160008.jpg"
weight: "13"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Kompressor", "Roboterarm", "Gegengewicht", "Ausbalancieren"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40517
- /detailsc852.html
imported:
- "2019"
_4images_image_id: "40517"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40517 -->
Der Kompressor fügt sich wunderbar ein am Ende des Roboterarms.