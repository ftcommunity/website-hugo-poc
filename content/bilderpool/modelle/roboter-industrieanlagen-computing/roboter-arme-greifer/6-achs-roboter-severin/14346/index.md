---
layout: "image"
title: "Seitenansicht"
date: "2008-04-21T16:01:00"
picture: "robi1_2.jpg"
weight: "44"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/14346
- /details1a57-2.html
imported:
- "2019"
_4images_image_id: "14346"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14346 -->
Achse 3 ist am Maximalanschlag. Dabei läst sich achse 4 nicht mehr vollständig drehen.