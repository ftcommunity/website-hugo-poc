---
layout: "image"
title: "Roboter von Oben"
date: "2007-03-18T11:28:25"
picture: "achsroboter9.jpg"
weight: "18"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9565
- /details101f-3.html
imported:
- "2019"
_4images_image_id: "9565"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9565 -->
