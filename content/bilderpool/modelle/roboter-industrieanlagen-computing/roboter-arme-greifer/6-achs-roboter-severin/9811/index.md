---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:34:49"
picture: "achsroboter09.jpg"
weight: "27"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9811
- /details54dc-2.html
imported:
- "2019"
_4images_image_id: "9811"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:34:49"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9811 -->
Gegengewicht