---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:34:49"
picture: "achsroboter02.jpg"
weight: "20"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9804
- /details83fe.html
imported:
- "2019"
_4images_image_id: "9804"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:34:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9804 -->
Das Gegengewicht beträgt insgesammt 4,5Kg und besteht aus schweren Stahlplatten. In der "Box"  ist je eine Stahlplatte mit 1.5Kg aussen eine aufgeschraubte mit 750g.