---
layout: "image"
title: "6-Achsrobo"
date: "2007-04-19T20:05:30"
picture: "roboterseverin1.jpg"
weight: "34"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/10097
- /details0693.html
imported:
- "2019"
_4images_image_id: "10097"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-04-19T20:05:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10097 -->
