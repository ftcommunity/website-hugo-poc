---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:35:55"
picture: "achsroboter16.jpg"
weight: "33"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9817
- /details2a92-2.html
imported:
- "2019"
_4images_image_id: "9817"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:35:55"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9817 -->
Extensions Module