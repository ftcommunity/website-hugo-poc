---
layout: "image"
title: "Komplettansich ohne Handgelenk"
date: "2007-03-18T11:28:25"
picture: "achsroboter4.jpg"
weight: "13"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9560
- /details7fcf.html
imported:
- "2019"
_4images_image_id: "9560"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9560 -->
