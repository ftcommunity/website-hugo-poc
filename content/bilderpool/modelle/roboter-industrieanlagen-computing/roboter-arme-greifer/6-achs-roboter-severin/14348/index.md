---
layout: "image"
title: "Achse 2"
date: "2008-04-21T16:01:00"
picture: "robi3_2.jpg"
weight: "46"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/14348
- /detailsa39d-2.html
imported:
- "2019"
_4images_image_id: "14348"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14348 -->
Hier die Aufhängung der Achse 2. die fast identisch zu achse 3 ist.