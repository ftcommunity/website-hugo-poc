---
layout: "comment"
hidden: true
title: "2702"
date: "2007-03-16T14:42:47"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Ah, danke! Mir gefällt, wie du den Power-Motor eingebaut hast, und dass endlich mal jemand außer mir Anbauwinkel verwendet. Bin gespannt drauf, wie's aussieht, wenn sich der Arm in Bewegung versetzt :-)