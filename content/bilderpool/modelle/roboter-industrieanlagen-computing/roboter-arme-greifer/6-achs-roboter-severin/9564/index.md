---
layout: "image"
title: "Robo und Intelligent Interface"
date: "2007-03-18T11:28:25"
picture: "achsroboter8.jpg"
weight: "17"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9564
- /detailsb4d2.html
imported:
- "2019"
_4images_image_id: "9564"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9564 -->
