---
layout: "image"
title: "Bodenplatte"
date: "2007-03-12T18:06:47"
picture: "IMG_1246.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9414
- /details4b83.html
imported:
- "2019"
_4images_image_id: "9414"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-12T18:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9414 -->
