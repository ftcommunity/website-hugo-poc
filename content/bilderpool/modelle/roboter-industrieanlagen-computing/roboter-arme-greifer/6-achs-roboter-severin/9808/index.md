---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:34:49"
picture: "achsroboter06.jpg"
weight: "24"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/9808
- /details1ed7.html
imported:
- "2019"
_4images_image_id: "9808"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:34:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9808 -->
Handgelenk