---
layout: "image"
title: "Kompressor am Armende als Gegengewicht"
date: "2015-02-09T17:13:11"
picture: "S1160006.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Kompressor", "Roboterarm"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40515
- /details7050.html
imported:
- "2019"
_4images_image_id: "40515"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40515 -->
Die Vibrationen sind noch tolerierbar. Der Kompressor muss auch nur jeweils kurz laufen für einen Greifvorgang.