---
layout: "overview"
title: "Zeitball
"
date: 2020-09-05T15:59:37+02:00
---

Ende des 19.-ten Jahrhunderts war für die Navigation in der Schifffahrt zur Bestimmung des Längengrades (der Position des Schiffes) eine genaue Uhrzeit erforderlich. Genaue Uhren gab es bereits, diese wurden vor Abfahrt in den Häfen genau eingestellt, d.h. zur von Sternwarten ermittelten Uhrzeit synchronisiert. Dazu diente die Beobachtung eines Zeitballes, der sekundengenau zu einer festgelegten Uhrzeit herabfiel, ausgelöst durch ein elektrisches Signal von der nahe gelegenen Sternwarte. 
Auch heute noch gibt es solche Zeitbälle, u.a. in Greenwich am Nullmeridian und am Time Square in Manhattan (Sylvesterfeier).
Hier wird ein Modell gezeigt, das aus Fischertechnik-Bausteinen aufgebaut wurde und von einer Zeitwortuhr gesteuert wird (gesteuert durch einen Arduino Nano mit Relais-Karte).
Der Zeitball ist hier eine Metallkugel mit Bohrung und stammt ursprünglich von einem Eier-Sollbruchstellen-Erzeuger.

Youtube-Video hier: https://youtu.be/mA78Leh91WU

Diskussion des Modells und des Themas im Forum: https://forum.ftcommunity.de/viewtopic.php?f=6&t=6319
