---
layout: "image"
title: "FT-DCF77-Kuckucksuhr"
date: "2013-07-28T19:13:47"
picture: "kuckucksuhr3.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37203
- /details1835.html
imported:
- "2019"
_4images_image_id: "37203"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-28T19:13:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37203 -->
Goldplatten gab ich beim FAN-Clubtag-2013.
