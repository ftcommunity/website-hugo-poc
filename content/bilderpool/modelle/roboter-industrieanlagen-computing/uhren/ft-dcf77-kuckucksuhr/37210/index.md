---
layout: "image"
title: "Kuckucksuhr - Details"
date: "2013-07-30T23:44:19"
picture: "kuckuksuhrdetails03.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37210
- /detailse835.html
imported:
- "2019"
_4images_image_id: "37210"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-30T23:44:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37210 -->
