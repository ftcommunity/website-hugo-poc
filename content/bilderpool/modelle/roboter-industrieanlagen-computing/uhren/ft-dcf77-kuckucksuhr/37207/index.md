---
layout: "image"
title: "FT-DCF77-Kuckucksuhr"
date: "2013-07-28T19:13:47"
picture: "kuckucksuhr7.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37207
- /details39c4.html
imported:
- "2019"
_4images_image_id: "37207"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-28T19:13:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37207 -->
Details
