---
layout: "image"
title: "Kuckucksuhr - Details"
date: "2013-07-30T23:44:19"
picture: "kuckuksuhrdetails08.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37215
- /details6b83-2.html
imported:
- "2019"
_4images_image_id: "37215"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-30T23:44:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37215 -->
