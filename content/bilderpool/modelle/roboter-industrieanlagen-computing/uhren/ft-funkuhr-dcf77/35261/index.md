---
layout: "image"
title: "ft-Funkuhr mit LED-Display"
date: "2012-08-04T23:33:33"
picture: "ftfunkuhrdcf3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35261
- /details32a9.html
imported:
- "2019"
_4images_image_id: "35261"
_4images_cat_id: "2613"
_4images_user_id: "1126"
_4images_image_date: "2012-08-04T23:33:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35261 -->
Hier noch eine Variante der ft-Funkuhr, die auf der Convention 2012 zu sehen sein wird: Die ft-Funkuhr zeigt Zeit (Stunde/Minute), Datum (Tag/Monat) und Jahr im Wechsel auf einem I²C-LED-Display an (Conrad, Best.-Nr. 198344).