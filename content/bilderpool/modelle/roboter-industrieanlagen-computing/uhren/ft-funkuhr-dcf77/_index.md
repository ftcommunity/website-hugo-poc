---
layout: "overview"
title: "ft-Funkuhr (DCF77)"
date: 2020-02-22T08:08:38+01:00
legacy_id:
- /php/categories/2613
- /categorieseb82.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2613 --> 
Mit einer DCF-Empfängerplatine von Conrad und einem RoboPro-Programm wird der TX zur vollwertigen Funkuhr.