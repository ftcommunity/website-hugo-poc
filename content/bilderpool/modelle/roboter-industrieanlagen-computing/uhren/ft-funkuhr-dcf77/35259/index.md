---
layout: "image"
title: "ft-Funkuhr in Funktion"
date: "2012-08-04T23:33:33"
picture: "ftfunkuhrdcf1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35259
- /details89d3.html
imported:
- "2019"
_4images_image_id: "35259"
_4images_cat_id: "2613"
_4images_user_id: "1126"
_4images_image_date: "2012-08-04T23:33:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35259 -->
Die ft-Funkuhr liest das DCF77-Zeitsignal (die "Normalzeit") der Physikalisch-Technischen Bundesanstalt (PTB) in Braunschweig ein, das von Mainflingen (bei Frankfurt) über ein 77,5 kHz Trägersignal (Langwelle) mit einer Reichweite von bis zu 2.000 km ausgestrahlt wird. Der Dekoder wurde in RoboPro entwickelt und kann im Download-Bereich der ft:c (http://www.ftcommunity.de/data/downloads/robopro/dcf77decoderv2.0.rpp) heruntergeladen werden. 

Die Codierung des Zeitsignals und die Funktionsweise des Dekoders haben ftDirk und ich ausführlich in einem ft:pedia-Artikel erläutert, der in Heft 3/2012 erscheinen wird. Zahlreiche Tipps und Hinweise finden sich auch im entsprechenden Thread des ft:c-Forums (http://forum.ftcommunity.de/viewtopic.php?f=8&t=1421).