---
layout: "image"
title: "Gesamtansicht"
date: "2016-09-08T14:31:03"
picture: "netduinodemo1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44340
- /detailsec07.html
imported:
- "2019"
_4images_image_id: "44340"
_4images_cat_id: "3273"
_4images_user_id: "104"
_4images_image_date: "2016-09-08T14:31:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44340 -->
In der Mitte sitzt (sichtbar) ein Adafruit 2.3 Motor Shield und darunter ein Netduino 3 WiFi (WLAN wird in dieser Demo aber nicht verwendet). Das Board fährt das Microsoft .NET Micro Framework (4.3) und steuert über ein hübsch strukturiertes C#-Programm folgende Modelle unabhängig voneinander:

1. Die Ampel links hinten.

2. Die kleine Exzenter-Presse links vorne, die immer langsam und stetig anläuft, aber sofort anhält, wenn man die Lichtschranke davor unterbricht.

3. Den Schrittmotor vorne in der Mitte, der mit je einer Sekunde Pause zufällige Positionen ansteuert, und zwar mit sanftem Beschleunigen und Bremsen.

4. Das Selbstbau-Schiebepotentiometer ganz rechts am Rand, das den Ventilator links beliebig zwischen "volle Kraft zurück" über "Stillstand" bis "volle Kraft voraus" ansteuert.

5. Der - genauso wie die anderen 4 als "Modell" in Software implementierte - große Not-Aus-Taster rechts neben dem Schrittmotor. Wir der betätigt, halten alle Modelle schlagartig an und die rote Lampe rechts oben blink, bis man alles durch Druck auf den Minitaster neben dem Not-Aus-Taster wieder weiterlaufen lässt.
