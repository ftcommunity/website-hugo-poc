---
layout: "overview"
title: "Netduino-Demo"
date: 2020-02-22T08:08:31+01:00
legacy_id:
- /php/categories/3273
- /categories4425.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3273 --> 
Ein Demonstrationsmodell mit einem Netduino-Board und einem Adafruit-Motorshield. Demonstriert wird Multithreading mit dem Microsoft .NET Micro Framework, um 4 Modelle mitsamt gemeinsamem Not-Aus unabhängig voneinander parallel laufen zu lassen.