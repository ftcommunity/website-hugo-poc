---
layout: "image"
title: "Anschlüsse"
date: "2012-10-07T17:05:47"
picture: "internet-03.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
schlagworte: ["Elektromechanik", "Internet"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/35815
- /details7f55.html
imported:
- "2019"
_4images_image_id: "35815"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35815 -->
Hier werden die Server und das Terminal angeschlossen.
Wie man sieht, sind nur zwei Server angeschlossen.