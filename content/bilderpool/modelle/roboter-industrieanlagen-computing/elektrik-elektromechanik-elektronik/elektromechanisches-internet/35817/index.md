---
layout: "image"
title: "Zwei Server"
date: "2012-10-07T17:05:47"
picture: "internet-06.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
schlagworte: ["Elektromechanik", "Internet"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/35817
- /detailsc226.html
imported:
- "2019"
_4images_image_id: "35817"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35817 -->
Die beiden Server, gebaut nach dem 2-in-1-Kochrezept...