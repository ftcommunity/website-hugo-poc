---
layout: "image"
title: "Schaltplan"
date: "2012-10-07T17:11:44"
picture: "schaltplan.jpg"
weight: "13"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
schlagworte: ["Elektromechanik", "Internet", "Schaltplan"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/35825
- /detailsbdc4-2.html
imported:
- "2019"
_4images_image_id: "35825"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:11:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35825 -->
Mit den Adresstaster werden die beiden Relais gesteuert, über die dann das Signal des Servers zur Datenlampe kommt.

Die Lampe ist leider eckig :(