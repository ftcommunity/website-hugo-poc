---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-07T17:05:47"
picture: "internet-01.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
schlagworte: ["Elektromechanik", "Internet", "Gesamtansicht"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/35813
- /detailsab73-2.html
imported:
- "2019"
_4images_image_id: "35813"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35813 -->
Mein Internet.
Es handelt sich dabei um zwei "Server" (Die *Scheiben*), das Abrufterminal (das Ding mit den Knöpfen und der Lampe) und natürlich einen Provider (Na was wohl!?). Aber Spaß beiseite:
Die zwei Server (bis zu vier sind möglich) senden permanent dieselbe Information (z.B. 1010111010). Das Terminal sagt dann: Ich will Server 11, 10 , 01 oder 00 (Taster 1 gedrückt, Taster 2 gedrückt ; ...). Je nach dem was ankommt werden beim "Provider" zwei Relais gestellt sodass die Signale des gewünschten Servers ankommen.