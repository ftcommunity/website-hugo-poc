---
layout: "image"
title: "Software-Beschreibung - Software description"
date: "2018-05-06T15:34:11"
picture: "nanoframeworkmotorendemo4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47596
- /details0ff1.html
imported:
- "2019"
_4images_image_id: "47596"
_4images_cat_id: "3510"
_4images_user_id: "104"
_4images_image_date: "2018-05-06T15:34:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47596 -->
Die Software is das Beispiel Sample08SmoothManyAnalogOutputs aus meinem abstrakten I/O-Framework (https://github.com/steffalk/AbstractIO). Die Demo selbst ist das völlig board-unabhängige Sample08SmoothManyAnalogOutputs (siehe https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Samples/Sample08SmoothManyAnalogOutputs.cs), und hereingereicht werden in Sample09LetManyMotorsRun aus https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Netduino3.Samples/Netduino3SamplesMain.cs einfach die konkreten I/O-Objekte des Netduino bzw. der Adafruit-Boards. Im Beispiel wird durch eine simple, ebenfalls abstrakt wiederverwendbare I/O-Transformation geregelt, dass der alte 6V-Motor nur 6 anstatt 9 V bekommt, und dass bei einem der Motoren (zur Demonstration des Frameworks) die Anschlüsse vertauscht wurden.

----------

The software is example Sample08SmoothManyAnalogOutputs of my abstract I/O framework (https://github.com/steffalk/AbstractIO). The demonstration is the fully board-independent Sample08SmoothManyAnalogOutputs (see https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Samples/Sample08SmoothManyAnalogOutputs.cs), and the actual I/O objects of the Netduino and Adafruit boards are simply passed in. In this example, a simple I/O transformation is used to limit the voltage of the old fischertechnik motor to 6V instead of 9V, and to handle that one motors has its pins swapped (for demonstrating the framework).
