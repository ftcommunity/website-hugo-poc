---
layout: "image"
title: "Robo TX Controller"
date: "2013-02-14T13:45:39"
picture: "IMG_4585.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/36608
- /detailsc1dc.html
imported:
- "2019"
_4images_image_id: "36608"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-14T13:45:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36608 -->
Hier kann  man meinen TX controller sehen.