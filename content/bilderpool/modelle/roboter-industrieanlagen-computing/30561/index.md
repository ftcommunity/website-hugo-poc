---
layout: "image"
title: "Training Roboter II"
date: "2011-05-14T22:07:37"
picture: "DSC00648.jpg"
weight: "23"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30561
- /detailsbe14-2.html
imported:
- "2019"
_4images_image_id: "30561"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30561 -->
Details of the  Y axis limit switch.

Informationen über die Y-Achse Endschalter.