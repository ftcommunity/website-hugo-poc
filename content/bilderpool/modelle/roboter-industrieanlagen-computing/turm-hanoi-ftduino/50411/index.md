---
layout: "image"
title: "Turm von Hanoi - Bild 7"
date: 2024-07-02T19:07:33+02:00
picture: "Picture140.jpg"
weight: "7"
konstrukteure: 
- "Jürgen Vollmer"
fotografen:
- "Jürgen Vollmer"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Version mit Schneckengetriebe. Deutlich langsamer aber mit präzisere Positionierung