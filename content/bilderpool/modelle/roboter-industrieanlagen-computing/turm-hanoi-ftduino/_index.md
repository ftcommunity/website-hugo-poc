---
layout: "overview"
title: "Turm von Hanoi mit ftDuino"
date: 2024-07-02T19:07:33+02:00
---

Der Turm von Hanoi mit E-Magnet, Pneumatik, Encodermotor und ftDuino

Die Metallscheiben müssen von der linken Ablage auf die rechte Ablage, unter
zur Hilfenahme der mittleren Ablage, transportiert werden. Die bekannten
Regeln sind einzuhalten.
Das 'Projekt-Modell' aus dem Baukasten 'STEM Pneumatics' diente als Vorlage.
Für die Auf-und Abbewegung ist ein Federzylinder eingebaut, dessen 
Luftanschluss über eine Drossel läuft damit die Bewegungen nicht zu heftig
erfolgen. Die Feder sorgt für eine gute Anpassung des Magneten an die 
verschiedenen Säulenhöhen.
Die Drehung des Arms übernimmt ein Encodermotor. Damit der Ablauf nicht zu lahm
von statten geht, ist die Übersetzung 5,8 : 1. Für 4 Scheiben werden 90 
Sekunden benötigt. 
Ein Endschalter bestimmt die Position der ersten Ablage. Die beiden anderen 
Ablagen werden mit Hilfe des Counters angesteuert.
Zwei Start-Taster für 3 oder 4 Scheiben. Die Steuerung übernimmt ein ftDuino.

[Programm zum Modell mit Schneckengetriebe](https://ftcommunity.de/knowhow/computing/ftduino/t_hanoi_4/)

[Programm zum Modell, schnelle Version](https://ftcommunity.de/knowhow/computing/ftduino/t_hanoi_5/)
