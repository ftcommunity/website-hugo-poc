---
layout: "image"
title: "Turm von Hanoi - Bild 6"
date: 2024-07-02T19:07:35+02:00
picture: "Picture139.jpg"
weight: "6"
konstrukteure: 
- "Jürgen Vollmer"
fotografen:
- "Jürgen Vollmer"
uploadBy: "Website-Team"
license: "unknown"
---

Vorne die 2 Taster für 3 oder 4 Scheiben und der Notaus-Schalter