---
layout: "image"
title: "Turm von Hanoi - Bild 1"
date: 2024-07-02T19:07:40+02:00
picture: "Picture134.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Vollmer"
fotografen:
- "Jürgen Vollmer"
uploadBy: "Website-Team"
license: "unknown"
---

Mit 4 Scheiben in Startposition