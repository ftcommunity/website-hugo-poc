---
layout: "image"
title: "Turm von Hanoi - Bild 5"
date: 2024-07-02T19:07:36+02:00
picture: "Picture138.jpg"
weight: "5"
konstrukteure: 
- "Jürgen Vollmer"
fotografen:
- "Jürgen Vollmer"
uploadBy: "Website-Team"
license: "unknown"
---

In der Bildmitte der Endschalter für die linke Ablage