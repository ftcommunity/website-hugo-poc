---
layout: "comment"
hidden: true
title: "21221"
date: "2015-11-07T11:19:53"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Danke für die ausführliche Antwort und den Tipp mit den Kugelbahnen. Das fand mein Sohn (heute 18) auch mit 8 Jahren schon interessant. Vielleicht kann ich meine Tochter damit auch begeistern. Der Altersabstand zwischen den beiden liegt leider bei 7 Jahren...

Gruß, Dirk