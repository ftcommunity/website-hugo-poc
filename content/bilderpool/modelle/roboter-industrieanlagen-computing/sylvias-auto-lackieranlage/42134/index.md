---
layout: "image"
title: "Richtungsumkehr"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage7.jpg"
weight: "7"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42134
- /detailsec5c-2.html
imported:
- "2019"
_4images_image_id: "42134"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42134 -->
Dieser kreativ angebrachte :-) Polwendeschalter wird von einem Stück Schnur, welches mit einem Fahrzeug fest verbunden ist, in den Endlagen umgeschaltet und steuert so das Vor- und Zurückfahren der Fahrzeuge.
