---
layout: "image"
title: "Fahrzeugtransport"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage6.jpg"
weight: "6"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42133
- /detailse3cc.html
imported:
- "2019"
_4images_image_id: "42133"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42133 -->
Dieser Motor treibt über einige Untersetzungsgetriebe die Förderkette an, an der die Fahrzeuge befestigt sind. Am Boden waagerecht sieht man die umlaufende Kette fürs Anheben/Absenken der Mechanik.
