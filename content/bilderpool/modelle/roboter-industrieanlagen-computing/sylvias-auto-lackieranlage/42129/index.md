---
layout: "image"
title: "Hebe-/Senkantrieb"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage2.jpg"
weight: "2"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42129
- /detailsea8e.html
imported:
- "2019"
_4images_image_id: "42129"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42129 -->
Der Hauptteil der Mechanik ist komplett höhenverstellbar und ausschließlich auf vier senkrechten Schneckenstrecken gelagert, von denen man hier eine sieht. Der senkrecht stehende PowerMotor dreht diese Schnecken durch eine komplett am Boden umlaufende Kette.
