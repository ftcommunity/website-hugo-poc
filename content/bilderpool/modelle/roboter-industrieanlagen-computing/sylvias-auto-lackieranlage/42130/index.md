---
layout: "image"
title: "Düsentransport"
date: "2015-10-25T14:30:02"
picture: "sylviasautolackieranlage3.jpg"
weight: "3"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42130
- /detailsb40b.html
imported:
- "2019"
_4images_image_id: "42130"
_4images_cat_id: "3138"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T14:30:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42130 -->
Der kleine Wagen in Bildmitte wird von er Kette immer hin- und hergezogen. Er folgt den Bogen an den Seiten und geht bis hinunter an den Fahrzeugboden. So werden sowohl die Fahrzeugseiten als auch die Dächer "lackiert". Das Bild wurde bei laufender Anlage aufgenommen.
