---
layout: "image"
title: "Portalroboter-Gesammtansicht"
date: "2015-03-21T18:16:47"
picture: "portalroboter01.jpg"
weight: "1"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- /php/details/40668
- /detailsbfe8.html
imported:
- "2019"
_4images_image_id: "40668"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40668 -->
Portalroboter der Klötze stapelt