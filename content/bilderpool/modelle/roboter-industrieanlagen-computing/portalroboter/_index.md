---
layout: "overview"
title: "Portalroboter"
date: 2020-02-22T08:08:55+01:00
legacy_id:
- /php/categories/3054
- /categories15f4.html
- /categories62cf.html
- /categoriesa0c2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3054 --> 
Ein Portalroboter mit beweglichem Tisch und variablen Einsatzzwecken. Hier ist er mit einem Vakuumgreifer ausgestattet, um Holzklötze zu stapeln. Denkbar wäre aber auch ein Stift um ihn als Plotter zu verwenden oder eine Zange um auch andere Dinge greifen zu können.