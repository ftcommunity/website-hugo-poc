---
layout: "image"
title: "Training Roboter  original"
date: "2011-05-14T22:07:37"
picture: "DSC00647.jpg"
weight: "25"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/30563
- /detailsc393.html
imported:
- "2019"
_4images_image_id: "30563"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30563 -->
Details of the the base of the tower,X axis,of the original.

Informationen über die Basis des Turms, X-Achse, des Originals.