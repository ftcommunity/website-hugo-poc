---
layout: "overview"
title: "Bearbeitungsinsel"
date: 2020-02-22T08:08:54+01:00
legacy_id:
- /php/categories/3050
- /categories4fb9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3050 --> 
Es werden verschiedenfarbige Werkstücke endlos im Kreis herum bewegt und dabei bearbeitet.Die Idee für dieses Modell bekam ich beim Durchforsten des Bilderpools.
