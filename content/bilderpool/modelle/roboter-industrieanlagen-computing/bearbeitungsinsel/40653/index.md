---
layout: "image"
title: "Draufsicht"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel1.jpg"
weight: "1"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40653
- /detailsfa29.html
imported:
- "2019"
_4images_image_id: "40653"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40653 -->
Das rote Förderband hinten links ist das Magazin. Rechts daneben ein Drehkreuz mit zwei Bearbeitungsstationen. Vorne ein schwarzes Förderband das die Werkstücke nach links transportiert und in der Mitte eine Pressstation hat.