---
layout: "image"
title: "Putzstation"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel6.jpg"
weight: "6"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- /php/details/40658
- /details74c7.html
imported:
- "2019"
_4images_image_id: "40658"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40658 -->
Oben links in der Ecke sieht man den Pneumatikzylinder, der den Motor mit dem Putzwerkzeug nach unten drückt.