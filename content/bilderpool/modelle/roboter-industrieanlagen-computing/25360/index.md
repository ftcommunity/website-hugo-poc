---
layout: "image"
title: "Walking Robot v2"
date: "2009-09-26T00:18:20"
picture: "sm_walker_2.jpg"
weight: "12"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["walker", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25360
- /detailscdeb.html
imported:
- "2019"
_4images_image_id: "25360"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-26T00:18:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25360 -->
We are working on a series of walking robots using the PCS BRAIN.