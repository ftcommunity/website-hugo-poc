---
layout: "image"
title: "Spindelkopf seitlich"
date: "2017-02-14T19:26:39"
picture: "fraese14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45213
- /details7d4f.html
imported:
- "2019"
_4images_image_id: "45213"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45213 -->
