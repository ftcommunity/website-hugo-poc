---
layout: "image"
title: "Ansicht schräg rechts"
date: "2017-02-14T19:16:21"
picture: "fraese09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45208
- /detailsee0e.html
imported:
- "2019"
_4images_image_id: "45208"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45208 -->
