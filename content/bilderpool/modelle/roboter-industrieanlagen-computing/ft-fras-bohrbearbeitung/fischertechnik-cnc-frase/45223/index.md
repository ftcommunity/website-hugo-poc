---
layout: "image"
title: "Fräsen Drehteil"
date: "2017-02-14T19:27:01"
picture: "fraese24.jpg"
weight: "24"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45223
- /details5d69-3.html
imported:
- "2019"
_4images_image_id: "45223"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45223 -->
Hier seht ihr ein Drehteil, welches mit dem Fräser bearbeitet werden soll.
