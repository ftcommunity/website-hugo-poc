---
layout: "image"
title: "Fräsen Nut"
date: "2017-02-14T19:27:01"
picture: "fraese23.jpg"
weight: "23"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45222
- /detailsaf42.html
imported:
- "2019"
_4images_image_id: "45222"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45222 -->
Mit Hilfe des Drehtischs wird eine Ringnut gefräst.

Video:
https://www.youtube.com/watch?v=YZNMv2GfTkE
