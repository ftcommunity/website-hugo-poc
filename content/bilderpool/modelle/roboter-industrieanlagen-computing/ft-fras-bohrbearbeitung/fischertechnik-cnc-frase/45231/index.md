---
layout: "image"
title: "Ansicht hinten links"
date: "2017-02-14T19:27:09"
picture: "fraese32.jpg"
weight: "32"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45231
- /detailsfa8a-2.html
imported:
- "2019"
_4images_image_id: "45231"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45231 -->
Der Drehteller wird über Steckachsen und Rastkardan gedreht.
