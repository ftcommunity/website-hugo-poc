---
layout: "comment"
hidden: true
title: "23022"
date: "2017-02-14T20:52:36"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

ich habe bereits vor ein paar Wochen das Video deines Modells gesehen und war schwer beeindruckt. Die Fotos, die du jetzt eingestellt hast ändern auch nichts mehr daran :-) Mich würde interessieren, wie stabil der Aufbau ist. Bei derartigen Funktionsmodellen aus Fischertechnik besteht häufig die Problematik, dass die Wiederholgenauigkeit deutlich unter Augenmaß liegt oder dass zu viel Spiel an der Werkzeughalterung besteht. Ich habe gesehen, dass du Alus und TSTs Drehkranz verwendest, auch die Erzeugnisse der Fräse sahen vielversprechend aus.
Eine weitere Frage betrifft die Ansteuerung der Achsen: Ist es möglich, beispielsweise eine zusammengesetzte Bewegung in X und in Y Richtung zu fahren, also beispielsweise die Diagonale eines Rechtecks präzise zu treffen? Kann man den Vorschub der Maschine einstellen?

Jetzt nachdem Fischertechnik endlich Schrittmotoren im Sortiment hat wären die doch eine ideale Erweiterung oder?

Beste Grüße
David