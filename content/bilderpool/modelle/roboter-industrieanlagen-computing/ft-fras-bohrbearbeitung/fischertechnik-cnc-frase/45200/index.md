---
layout: "image"
title: "fischertechnik CNC-Fräse vorne"
date: "2017-02-14T19:16:21"
picture: "fraese01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45200
- /details3b13.html
imported:
- "2019"
_4images_image_id: "45200"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45200 -->
Die Abmaße betragen 370 mm Höhe, 320 mm Breite und 300 mm Tiefe.
