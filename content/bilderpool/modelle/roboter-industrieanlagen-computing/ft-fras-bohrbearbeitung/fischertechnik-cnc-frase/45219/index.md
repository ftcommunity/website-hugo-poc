---
layout: "image"
title: "Augenschutz"
date: "2017-02-14T19:26:39"
picture: "fraese20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45219
- /details9015.html
imported:
- "2019"
_4images_image_id: "45219"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45219 -->
Der Augenschutz ist ein Klarsichtdeckel von einer Kassette und ist beweglich.
