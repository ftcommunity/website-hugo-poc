---
layout: "image"
title: "Ansicht hinten links"
date: "2017-02-14T19:27:09"
picture: "fraese33.jpg"
weight: "33"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45232
- /detailsc5ea-3.html
imported:
- "2019"
_4images_image_id: "45232"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45232 -->
Hier seht ihr die Ansteuerung über den Encoder-Motor zum Drehen des Drehtellers (C-Achse).
Unten die ist die Kabelführung zu erkennen.
