---
layout: "image"
title: "Spindel mit Bohrfutter"
date: "2017-02-14T19:27:01"
picture: "fraese27.jpg"
weight: "27"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45226
- /details1b71-2.html
imported:
- "2019"
_4images_image_id: "45226"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45226 -->
Die Spindel ist mit einem verstellbaren Bohrfutter 1- 3,2 mm über
einen Adapter verbunden. Dadurch ist es möglich verschiedene Fräser/Bohrer einzusetzen.
