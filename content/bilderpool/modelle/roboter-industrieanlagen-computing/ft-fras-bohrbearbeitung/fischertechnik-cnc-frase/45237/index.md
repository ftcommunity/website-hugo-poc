---
layout: "image"
title: "Steckschaum"
date: "2017-02-14T19:27:09"
picture: "fraese38.jpg"
weight: "38"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45237
- /details65bc.html
imported:
- "2019"
_4images_image_id: "45237"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45237 -->
Der Steckschaum ist für Blumengestecke. Ihr könnt ihn verwenden, ohne euere Bauteile zu beschädigen.
Ist danach aber immer sehr viel zu reinigen. ;-)
