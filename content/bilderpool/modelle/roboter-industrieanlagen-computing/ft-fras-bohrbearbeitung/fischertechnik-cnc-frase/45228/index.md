---
layout: "image"
title: "Ansicht seitlich"
date: "2017-02-14T19:27:01"
picture: "fraese29.jpg"
weight: "29"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45228
- /details063e-4.html
imported:
- "2019"
_4images_image_id: "45228"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45228 -->
