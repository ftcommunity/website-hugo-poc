---
layout: "image"
title: "ROBO Pro Ansteuerung"
date: "2017-02-14T19:27:09"
picture: "fraese37.jpg"
weight: "37"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45236
- /details5b8d.html
imported:
- "2019"
_4images_image_id: "45236"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:09"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45236 -->
Die CNC-Fräsmaschine kann über die ROBO Pro Software Online angesteuert werden. Dazu werden Vorlauf und Rücklaufgeschwindigkeit und die Position gespeichert.
