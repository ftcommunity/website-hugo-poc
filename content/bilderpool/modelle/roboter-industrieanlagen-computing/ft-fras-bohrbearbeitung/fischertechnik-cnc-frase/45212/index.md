---
layout: "image"
title: "Spindelkopf"
date: "2017-02-14T19:26:39"
picture: "fraese13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45212
- /details1564.html
imported:
- "2019"
_4images_image_id: "45212"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45212 -->
Vorne eine Segmentanzeige für die Drehzahl (1-8), gesteuert über I2C.
