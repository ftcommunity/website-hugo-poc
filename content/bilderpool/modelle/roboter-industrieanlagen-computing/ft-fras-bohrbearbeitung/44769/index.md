---
layout: "image"
title: "Bearbeitungszentrum"
date: "2016-11-15T17:08:09"
picture: "2016-11-14_10.19.301.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/44769
- /details3e22.html
imported:
- "2019"
_4images_image_id: "44769"
_4images_cat_id: "3336"
_4images_user_id: "2496"
_4images_image_date: "2016-11-15T17:08:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44769 -->
