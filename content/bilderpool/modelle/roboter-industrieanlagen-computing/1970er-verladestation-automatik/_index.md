---
layout: "overview"
title: "1970er Verladestation mit Automatik"
date: 2022-01-19T13:25:41+01:00
---

Die "Verladestation" aus der 1970er Anleitung für den 400S Statik-Kasten, ergänzt um eine Steuerung mit Silberlingen.