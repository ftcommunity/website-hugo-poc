---
layout: "image"
title: "Das Modell"
date: 2022-01-19T13:25:49+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Damit die Elektronik und das Netzteil untergebracht werden konnten, wurde das Modell auf eine 1000er-Bauplatte gesetzt. Schiebt man den Wagen nach links, unterbricht er die dortige Lichtschranke. Das bewirkt das Anheben des Wagens zum Auskippen, einige Sekunden Wartezeit und schließlich das langsame Absenken. Dann kann der Wagen wieder von Hand nach rechts geschoben werden.