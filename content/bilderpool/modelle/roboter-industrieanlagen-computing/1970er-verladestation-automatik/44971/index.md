---
layout: "image"
title: "Die Vorlage"
date: 2022-01-19T13:25:54+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Ur-Statik-Anleitung von fischertechnik für die Kästen 100S - 400S aus den 1970er Jahren findet sich beim 400S eine Verladestation. Da mir das Modell schon immer gefiel, baute ich es zunächst einfach nochmal nach, ergänzte es aber dann um eine automatische Steuerung mit einer Lichtschranke, 1972er Elektronik-Bausteinen ("Silberlingen") und einem Ur-fischertechnik-Motor.

Die Ur-Anleitung findet sich z.B. unter https://ft-datenbank.de/ft-article/428 und ein Video unter https://youtu.be/Hmp4NvK3yFc