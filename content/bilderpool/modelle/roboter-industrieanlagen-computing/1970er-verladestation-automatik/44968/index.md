---
layout: "image"
title: "Obere Endlage"
date: 2022-01-19T13:25:50+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die obere Endlage ist erreicht. Die Pause wird abgewartet, bevor die Schiene und mit ihr der Wagen wieder abgesenkt wird.