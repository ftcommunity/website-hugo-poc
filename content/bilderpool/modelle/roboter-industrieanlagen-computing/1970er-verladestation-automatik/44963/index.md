---
layout: "image"
title: "Blick von der Unterseite"
date: 2022-01-19T13:25:44+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Neben dem Motor sieht man hier, wie die Winkelachse in der Bausteinreihe steckt und senkrecht abgewinkelt vom Zugseil gezogen wird und über die Streben die Schienen anhebt.