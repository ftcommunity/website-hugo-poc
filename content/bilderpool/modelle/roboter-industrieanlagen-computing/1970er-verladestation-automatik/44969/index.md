---
layout: "image"
title: "Anheben fast fertig"
date: 2022-01-19T13:25:51+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Mechanik ist fast an ihrer oberen Endlage angekommen. Der Wagen wird von den kleinen senkrecht stehenden I-Streben 30 gehalten, aber sein Inhalt kippt heraus.