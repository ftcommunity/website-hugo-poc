---
layout: "image"
title: "Das Anheben beginnt"
date: 2022-01-19T13:25:53+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Schiene ist schon etwas angehoben.