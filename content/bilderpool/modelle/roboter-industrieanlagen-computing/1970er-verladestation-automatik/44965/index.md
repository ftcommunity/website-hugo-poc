---
layout: "image"
title: "Elektronik"
date: 2022-01-19T13:25:47+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von links nach rechts:

1. Gleichrichter-Baustein zur Stromversorgung (nur der Motor hängt am vorderen Gleichspannungsausgang des Netzgeräts, die Elektronik über den Gleichrichter am seitlichen Wechselspannungsausgang).

2. Grundbaustein für die Lichtschranke.

3. Flip-Flop, das durch das Unterbrechen der Lichtschranke gesetzt wird. Das schaltet das erste Relais ein und lässt den Motor den Wagen mit voller Kraft anheben.

4. Mono-Flop für eine kurze Pause, wenn das erste Flipflop vom Taster für die obere Endlage zurückgesetzt und der Motor angehalten wurde. Die Pause gibt dem Schüttgut im Wagen Zeit, aus diesem herauszurutschen.

5. Flip-Flop, das nach der Pause vom Mono-Flop gesetzt wird und den Motor über das zweite Relais den Wagen absenken lässt. Das geschieht langsam, weil diese Verbindung zum Motor über die vorne angebrachte Linsenlampe als Vorwiderstand verläuft.

6. OR/NOR-Baustein, der logisch "1" ist, wenn die Pause (Monoflop) oder die Rückwärtsbewegung (Flipflop 2) aktiv sind. Sein Ausgang setzt das erste Flipflop statisch (mit Vorrang) zurück. Ansonsten könnte das Anheben-Flipflop gesetzt werden, wenn die Lichtschranke während der Abwärtsbewegung durch Wagenteile beleuchtet und wieder abgedunkelt wird.

7. Relais 1, steuert den Motor zum Anheben an.

8. Relais 2, steuert den Motor (über eine Linsenlampe als in Serie geschalteten Widerstand) zum langsamen Absenken an.