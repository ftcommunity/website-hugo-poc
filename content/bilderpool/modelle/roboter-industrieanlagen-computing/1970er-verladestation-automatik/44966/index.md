---
layout: "image"
title: "Lichtschranke und Mechanik"
date: 2022-01-19T13:25:48+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Lichtschranke wurde gegenüber der Vorlage ergänzt. Unten wurde ein Ur-fischertechnik-Motor angebracht. Den kann man nach unten wegdrehen und die Anlage wieder - wie in der Vorlage - per Kurbel manuell antreiben. Dazu muss die Rücklaufsperre (die Winkelachse, die man ganz links im Bild nach unten hängen sieht) wieder ins (hier nicht sichtbare) Z20 geklappt werden.