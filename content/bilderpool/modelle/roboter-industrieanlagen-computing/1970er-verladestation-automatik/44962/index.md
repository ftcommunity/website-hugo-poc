---
layout: "image"
title: "Endlagentaster, Gelenke, Motorbefestigung"
date: 2022-01-19T13:25:43+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Motor ist mit seiner Dreh-/Schiebeplatte an zwei Grundbausteinen aufgehängt. Um die Gelenkbausteine oben wird die Schiene angehoben, bis der Endlagentaster ganz rechts vom Winkelstein gedrückt wird.