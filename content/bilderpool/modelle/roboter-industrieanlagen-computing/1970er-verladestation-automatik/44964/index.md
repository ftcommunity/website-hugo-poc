---
layout: "image"
title: "Motor"
date: 2022-01-19T13:25:45+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Ur-Motoren lassen sich wunderbar drehen und verschieben und so genau ins Modell einpassen.