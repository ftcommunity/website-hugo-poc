---
layout: "image"
title: "Blick von der Auskipp-Seite"
date: 2022-01-19T13:25:42+01:00
picture: "2022-01-16_1970er_Verladestation_mit_Automatik08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man das Z20 mit der Kurbel, das ohne den Motor zum von-Hand-Spielen verwendet werden kann.