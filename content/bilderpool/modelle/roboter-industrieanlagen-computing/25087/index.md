---
layout: "image"
title: "Stomperbot"
date: "2009-09-22T21:44:14"
picture: "sm_stomperbot2.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Stomperbot", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25087
- /details26fd.html
imported:
- "2019"
_4images_image_id: "25087"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25087 -->
This is the Stomperbot integrating the PCS BRAIN.