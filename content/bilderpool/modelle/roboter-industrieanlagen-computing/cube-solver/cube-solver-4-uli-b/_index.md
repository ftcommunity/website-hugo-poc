---
layout: "overview"
title: "------ Cube Solver 4 (Uli B)"
date: 2020-02-22T08:02:12+01:00
legacy_id:
- /php/categories/2305
- /categoriesdf7f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2305 --> 
Sie finden hier den Nachbau eines Cube Solvers.
Nach ca. 15 Jahren fischertechnik-Abstinenz habe ich als Wiedereinstieg einen Cube Solver ausgewählt und nachgebaut.