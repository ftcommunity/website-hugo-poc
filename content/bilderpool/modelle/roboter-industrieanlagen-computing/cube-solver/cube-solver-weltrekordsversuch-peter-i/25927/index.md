---
layout: "image"
title: "webcam"
date: "2009-12-11T23:27:16"
picture: "cubesolver12.jpg"
weight: "18"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25927
- /details0ee7-2.html
imported:
- "2019"
_4images_image_id: "25927"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25927 -->
Die Webcam hat 10€ gekostet und liefert 320*240 Bilder(das Programm schneidet sowieso links und rechts ab und rechnet das Bild auf 3*3 runter)