---
layout: "image"
title: "Version 2.0 1/2"
date: "2009-11-16T20:40:39"
picture: "20_3_-_Kopie.jpg"
weight: "5"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
schlagworte: ["Cube", "Solver", "Zauberwürfel", "hman13", "Peter", "Pötzi"]
uploadBy: "hman13"
license: "unknown"
legacy_id:
- /php/details/25778
- /details405a.html
imported:
- "2019"
_4images_image_id: "25778"
_4images_cat_id: "1790"
_4images_user_id: "1100"
_4images_image_date: "2009-11-16T20:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25778 -->
Vorderansicht

Die drei Motoren treiben den Drehkranz an.
Damit der Roboter nicht zu schnell ist, werden sie mit 5V betrieben