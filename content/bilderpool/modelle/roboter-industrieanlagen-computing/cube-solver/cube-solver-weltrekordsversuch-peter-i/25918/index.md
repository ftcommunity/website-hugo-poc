---
layout: "image"
title: "275W Computer netzteil"
date: "2009-12-11T23:27:06"
picture: "cubesolver03.jpg"
weight: "9"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25918
- /details26ae-2.html
imported:
- "2019"
_4images_image_id: "25918"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25918 -->
details: 12V/8A
5V/22A
Außerden moch 3.3V und -12,-5,-3.3 V