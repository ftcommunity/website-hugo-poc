---
layout: "image"
title: "Gesamtansicht rechter schieber"
date: "2009-12-11T23:27:06"
picture: "cubesolver08.jpg"
weight: "14"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25923
- /details0aa0-2.html
imported:
- "2019"
_4images_image_id: "25923"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25923 -->
Die Räder haben wie beim linken Schieber den Sinn, dass der Schieber nicht zu sehr auf und abwackelt