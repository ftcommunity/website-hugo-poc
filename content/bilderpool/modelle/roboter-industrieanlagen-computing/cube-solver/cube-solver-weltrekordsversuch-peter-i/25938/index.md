---
layout: "image"
title: "Linker Schieber(Ausgangsposition)"
date: "2009-12-11T23:27:16"
picture: "cubesolver23.jpg"
weight: "25"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25938
- /detailsf972.html
imported:
- "2019"
_4images_image_id: "25938"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25938 -->
Der taster ist gedrückt