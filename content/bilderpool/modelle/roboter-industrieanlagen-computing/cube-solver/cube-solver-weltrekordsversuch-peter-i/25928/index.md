---
layout: "image"
title: "Webcam vom Drehkranz betrachtet"
date: "2009-12-11T23:27:16"
picture: "cubesolver13.jpg"
weight: "19"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25928
- /details3873.html
imported:
- "2019"
_4images_image_id: "25928"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25928 -->
Leider ein bisschen unscharf, da ich keinen Blitz verwenden konnte