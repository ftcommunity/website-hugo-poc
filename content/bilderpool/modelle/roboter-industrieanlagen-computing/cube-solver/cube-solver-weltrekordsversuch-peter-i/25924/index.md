---
layout: "image"
title: "Hinterseite des rechten Schiebers"
date: "2009-12-11T23:27:06"
picture: "cubesolver09.jpg"
weight: "15"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25924
- /detailscc66.html
imported:
- "2019"
_4images_image_id: "25924"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25924 -->
Der schieber ist zurückgefahren