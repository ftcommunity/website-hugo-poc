---
layout: "image"
title: "Schrägansicht"
date: "2009-12-11T23:27:06"
picture: "cubesolver04.jpg"
weight: "10"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25919
- /detailsd4d9.html
imported:
- "2019"
_4images_image_id: "25919"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25919 -->
Im Holzkastl in der Mitte unten sind alle Kabel die vom Netzteil rausgehen(ich wollte sie nicht abschneiden)