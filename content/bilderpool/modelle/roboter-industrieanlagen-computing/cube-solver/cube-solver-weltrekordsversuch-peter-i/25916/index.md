---
layout: "image"
title: "Gesamtansicht"
date: "2009-12-11T23:27:06"
picture: "cubesolver01.jpg"
weight: "7"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/25916
- /details5db4.html
imported:
- "2019"
_4images_image_id: "25916"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25916 -->
Die Webcam fehlt noch