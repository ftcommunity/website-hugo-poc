---
layout: "image"
title: "Kippvorrichtung"
date: "2009-09-27T11:41:17"
picture: "kippvorrichtung1.jpg"
weight: "9"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/25370
- /details6fcc.html
imported:
- "2019"
_4images_image_id: "25370"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2009-09-27T11:41:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25370 -->
Diese Konstruktion kann den Zauberwürfel in ca. 0,25 Sekunden kippen. Dazu hebt zuerst der rechte Hebel den Würfel an, sodass er gekippt wird, der linke Hebel drückt ihn dann zurück in seine Halterung.
