---
layout: "image"
title: "Kippen des Würfels"
date: "2008-02-17T07:55:26"
picture: "cubesolver2.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Jan (kehrblech)"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/13657
- /details129a.html
imported:
- "2019"
_4images_image_id: "13657"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T07:55:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13657 -->
Hier wird der Würfel gerade gekippt, dabei fährt erst der linke Schieber vor und hebt den Würfel an, 
danach schiebt der rechte Schieber den Würfel zurück auf den Drehkranz.
