---
layout: "image"
title: "ftcs 001"
date: "2005-08-26T17:57:06"
picture: "ftcs_001.JPG"
weight: "1"
konstrukteure: 
- "Markus Mack (MarMac)"
fotografen:
- "Markus Mack (MarMac)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/4656
- /details29a3.html
imported:
- "2019"
_4images_image_id: "4656"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4656 -->
Der Cube Solver: Genaueres auf der <a href="http://www.ftcommunity.de/ftcs/" target="_blank">Projektseite</a> (Englisch) und im <a href="http://www.fischertechnik.de/fanclub/forum/topic.asp?TOPIC_ID=2813&FORUM_ID=12&CAT_ID=2&Topic_Title=Cube+Solver&Forum_Title=Modellideen" target="_blank">Forum</a>

<hr>
The cube solver: Visit the <a href="http://www.ftcommunity.de/ftcs/" target="_blank">project page</a> for more information.
