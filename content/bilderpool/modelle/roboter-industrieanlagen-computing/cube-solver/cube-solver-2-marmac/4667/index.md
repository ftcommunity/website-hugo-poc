---
layout: "image"
title: "ftcs 012"
date: "2005-08-26T17:57:06"
picture: "ftcs_012.JPG"
weight: "12"
konstrukteure: 
- "Markus Mack (MarMac)"
fotografen:
- "Markus Mack (MarMac)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/4667
- /details839e-3.html
imported:
- "2019"
_4images_image_id: "4667"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4667 -->
