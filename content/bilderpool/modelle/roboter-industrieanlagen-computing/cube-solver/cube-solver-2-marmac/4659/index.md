---
layout: "image"
title: "ftcs 004"
date: "2005-08-26T17:57:06"
picture: "ftcs_004.JPG"
weight: "4"
konstrukteure: 
- "Markus Mack (MarMac)"
fotografen:
- "Markus Mack (MarMac)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/4659
- /details5e22.html
imported:
- "2019"
_4images_image_id: "4659"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4659 -->
Das Kippen des Würfels (nur bei offenem Deckel möglich):
zuerst drückt der rechte Schieber  gegen die rechte Oberkante des Würfels, dann drückt der links von der anderen seite her blitzschnell nach unten. Wenn der rechte nicht gerade klemmt funktioniert das absolut zuverlässig.

<hr>

Toppling the cube:
First its upper-right side is being pushed to the left like shown on the picture, then the left pusher will press it into the turntable again.
This mechanism works reliably unless some more lubrication is needed.
