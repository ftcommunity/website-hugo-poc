---
layout: "image"
title: "Magnetventile"
date: "2016-10-30T12:19:35"
picture: "IMG_4885.jpg"
weight: "4"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- /php/details/44695
- /detailsed0a.html
imported:
- "2019"
_4images_image_id: "44695"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44695 -->
Die beiden Magnetventile für den Pusher und den GRabber