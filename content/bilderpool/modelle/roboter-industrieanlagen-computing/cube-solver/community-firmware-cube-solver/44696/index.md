---
layout: "image"
title: "Draufsicht"
date: "2016-10-30T12:19:35"
picture: "IMG_4882.jpg"
weight: "5"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- /php/details/44696
- /details6b44.html
imported:
- "2019"
_4images_image_id: "44696"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44696 -->
Sicht auf den Pusher und die Ventile