---
layout: "image"
title: "e-buc Bild 5"
date: "2011-10-24T21:37:28"
picture: "ebuc5.jpg"
weight: "5"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33320
- /details7381.html
imported:
- "2019"
_4images_image_id: "33320"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33320 -->
Das Z30 ist direkt mit dem Deckel verbunden, das Z10 des Motors dreht das Z30. Der Deckel hat nach oben hin keinen Anschlag, der Cubesolver drohte bei früheren Versionen mit Anschlag immer mal dazu, sich zu zerlegen.