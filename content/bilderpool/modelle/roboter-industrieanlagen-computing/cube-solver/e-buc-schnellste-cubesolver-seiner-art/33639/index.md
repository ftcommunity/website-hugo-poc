---
layout: "image"
title: "e-buc Bild 8"
date: "2011-12-12T17:15:29"
picture: "ebuc08.jpg"
weight: "13"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33639
- /details9cbb.html
imported:
- "2019"
_4images_image_id: "33639"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33639 -->
