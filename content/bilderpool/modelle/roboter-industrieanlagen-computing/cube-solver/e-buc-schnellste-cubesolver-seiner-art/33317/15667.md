---
layout: "comment"
hidden: true
title: "15667"
date: "2011-11-12T08:41:49"
uploadBy:
- "nula"
license: "unknown"
imported:
- "2019"
---
Es gibt ein paar kleine Neuerungen, der linke Schieber ist etwas kürzer geworden und ragt links nicht mehr so weit über die Bauplatte. Außerdem ist der Antriebsmotor für diesen Schieber nach hinten gekommen, wodurch die Front um einiges aufgeräumter und glatter ist.