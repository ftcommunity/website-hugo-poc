---
layout: "image"
title: "e-buc Bild 3"
date: "2011-10-24T21:37:28"
picture: "ebuc3.jpg"
weight: "3"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/33318
- /details78f8.html
imported:
- "2019"
_4images_image_id: "33318"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33318 -->
Ich wollte von anfang an alles so kompakt wie möglich bauen, habe also die ersten Steine auch auf die kleine Grundbauplatte gesetzt. Zwischendurch wurde es etwas eng, aber mit der Zeit war ich in der Lage, alles kompakter zu gestalten.