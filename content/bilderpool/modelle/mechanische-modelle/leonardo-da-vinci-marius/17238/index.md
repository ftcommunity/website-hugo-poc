---
layout: "image"
title: "'Sicheln'"
date: "2009-02-01T19:35:38"
picture: "streitwagen3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17238
- /detailscba1.html
imported:
- "2019"
_4images_image_id: "17238"
_4images_cat_id: "1546"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17238 -->
Hier sieht man das Teil, wo in Wirklichkeit die Sicheln oder Schwerter drauf sind.