---
layout: "image"
title: "eingefahren-hinten"
date: "2009-02-03T00:59:28"
picture: "sturmleiter5.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17282
- /details5246.html
imported:
- "2019"
_4images_image_id: "17282"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17282 -->
