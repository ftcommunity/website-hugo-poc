---
layout: "image"
title: "sturmleiter3.jpg"
date: "2009-02-03T00:59:28"
picture: "sturmleiter3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17280
- /details606e.html
imported:
- "2019"
_4images_image_id: "17280"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17280 -->
