---
layout: "image"
title: "Vogelperspektive"
date: "2009-02-03T00:59:29"
picture: "sturmleiter9.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17286
- /details8dbc.html
imported:
- "2019"
_4images_image_id: "17286"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17286 -->
