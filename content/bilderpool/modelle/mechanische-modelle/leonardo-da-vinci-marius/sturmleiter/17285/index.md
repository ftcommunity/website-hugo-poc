---
layout: "image"
title: "voll ausgefahren-hinten"
date: "2009-02-03T00:59:29"
picture: "sturmleiter8.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17285
- /details274f.html
imported:
- "2019"
_4images_image_id: "17285"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17285 -->
