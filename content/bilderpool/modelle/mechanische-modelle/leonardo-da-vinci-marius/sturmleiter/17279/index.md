---
layout: "image"
title: "voll ausgefahrener Zustand"
date: "2009-02-03T00:59:28"
picture: "sturmleiter2.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17279
- /details6b69.html
imported:
- "2019"
_4images_image_id: "17279"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17279 -->
