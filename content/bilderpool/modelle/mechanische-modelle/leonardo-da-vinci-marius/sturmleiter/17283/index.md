---
layout: "image"
title: "Schnecke und Zahnrad"
date: "2009-02-03T00:59:28"
picture: "sturmleiter6.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17283
- /detailse652.html
imported:
- "2019"
_4images_image_id: "17283"
_4images_cat_id: "1549"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17283 -->
