---
layout: "image"
title: "geladenes Katapult"
date: "2009-02-03T00:59:28"
picture: "katapult8.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17277
- /details96bc.html
imported:
- "2019"
_4images_image_id: "17277"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17277 -->
