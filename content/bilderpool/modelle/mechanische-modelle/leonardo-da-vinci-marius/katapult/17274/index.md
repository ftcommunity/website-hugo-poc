---
layout: "image"
title: "katapult5.jpg"
date: "2009-02-03T00:59:28"
picture: "katapult5.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17274
- /details9719-3.html
imported:
- "2019"
_4images_image_id: "17274"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17274 -->
