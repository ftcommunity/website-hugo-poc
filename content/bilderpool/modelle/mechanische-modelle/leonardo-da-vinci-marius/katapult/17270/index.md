---
layout: "image"
title: "katapult1.jpg"
date: "2009-02-03T00:59:28"
picture: "katapult1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17270
- /details2ef6.html
imported:
- "2019"
_4images_image_id: "17270"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17270 -->
