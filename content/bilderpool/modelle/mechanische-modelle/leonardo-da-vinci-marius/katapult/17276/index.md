---
layout: "image"
title: "katapult7.jpg"
date: "2009-02-03T00:59:28"
picture: "katapult7.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17276
- /detailsba08.html
imported:
- "2019"
_4images_image_id: "17276"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17276 -->
