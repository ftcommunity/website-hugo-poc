---
layout: "image"
title: "Schieblehre  / Messschieber"
date: "2014-04-30T12:23:43"
picture: "schieblehremessschieber2.jpg"
weight: "2"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38730
- /details37ae.html
imported:
- "2019"
_4images_image_id: "38730"
_4images_cat_id: "2892"
_4images_user_id: "2138"
_4images_image_date: "2014-04-30T12:23:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38730 -->
Mit Innenmaß, Außenmaß und Tiefenmaß + Feststellschraube.
Ohne Nonius da nur im mm Bereich genau.
