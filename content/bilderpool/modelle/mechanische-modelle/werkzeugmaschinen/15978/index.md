---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (12/15)"
date: "2008-10-14T09:00:13"
picture: "bohrundfraesmaschinebf12.jpg"
weight: "12"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15978
- /detailse093.html
imported:
- "2019"
_4images_image_id: "15978"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T09:00:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15978 -->
Auf dem Arbeitstisch des Kreuzschlittens das "Sonderzubehör" Aufsatzdrehtisch mit zusätzlicher Arbeitsplatte. Der Aufsatzdrehtisch dreht sich um die Drehachse C, die parallel zur Linearachse Z (Höhenverstelllung des Spindelkopfes) liegt.
