---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (13/15)"
date: "2008-10-14T09:00:14"
picture: "bohrundfraesmaschinebf13.jpg"
weight: "13"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15979
- /details3ee0.html
imported:
- "2019"
_4images_image_id: "15979"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T09:00:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15979 -->
Ein Flachspanner für prismatische (rechtwinklige) Werkstücke.
