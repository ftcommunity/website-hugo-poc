---
layout: "image"
title: "Futter02.JPG"
date: "2004-11-30T19:55:42"
picture: "Futter02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Bohrfutter", "Schnecke", "35977"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3361
- /details26a6.html
imported:
- "2019"
_4images_image_id: "3361"
_4images_cat_id: "299"
_4images_user_id: "4"
_4images_image_date: "2004-11-30T19:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3361 -->
Beim Zusammenstecken muss darauf geachtet werden, dass die Bausteine BS5 (je 3x in jedem Sechseck) genau übereinander stehen.
Das Z20 sitzt lose auf der Achse. Die Klemmvorrichtung wird über eins der Z10 geöffnet und geschlossen.
