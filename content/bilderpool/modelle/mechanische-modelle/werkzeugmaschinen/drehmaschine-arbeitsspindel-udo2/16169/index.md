---
layout: "image"
title: "(6/9) Spindelansicht"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel6.jpg"
weight: "6"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16169
- /details6e36-2.html
imported:
- "2019"
_4images_image_id: "16169"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16169 -->
Arbeitsspindel ohne Laufringe:
Sichtbar wird die gesamte Anordnung der Rollkörper sowie von Z40 (Antrieb vom Motorgetriebe) und Z30 (Abtrieb zum (Vorschubgetriebe)
