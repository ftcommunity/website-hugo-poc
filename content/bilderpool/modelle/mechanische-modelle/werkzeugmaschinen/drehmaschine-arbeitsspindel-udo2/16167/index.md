---
layout: "image"
title: "(4/9) Kopfansicht"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel4.jpg"
weight: "4"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16167
- /details9573.html
imported:
- "2019"
_4images_image_id: "16167"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16167 -->
Kopfflansch zur Aufnahme der Werkstückspanner:
- Spannflansch mit Zentrieransatz 3mm und 6kt 41,5mm
- Dreipunktspannung durch Baustein 15 mit Bohrung im Teilkreis Ø66,5mm
