---
layout: "image"
title: "(9/9) Laufring mit Lagerschale"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel9.jpg"
weight: "9"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16172
- /details3e92.html
imported:
- "2019"
_4images_image_id: "16172"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16172 -->
Die zwei äußeren Laufringe haben natürlich axial keine Stabilität. Sie müssen deshalb beidseitig axial gefaßt und auch radial zentriert werden. Das geschieht in je zwei miteinander verzapften Lagerschalenhälften. Zum Einsatz kommen Laufringe mit 48 Winkelsteinen 7,5°, die dann auch genau in die radiale Zentrierung passen. Da die Rollkörper radial über ein 48-Eck und radial als auch axial über 48 Fugen laufen müssen, entstehen natürlich entsprechende Laufgeräusche. Das wird kompensiert durch Aufkleben von Selbstklebefolie für alle vier Laufflächen (2x Zylinderring und 2x Kreisring). Gleichzeitig wird damit das radiale Lagerspiel bei 48 Laufringteilen kompensiert. Da es keine Platten 15x45 mit 3 Zapfen gibt, müssen die 6 radialen Zentrierteile Baustein 7,5 mit Klebepunkten in ihrer Lage fixiert werden. Zum radialen Zentrieren reichen theoretisch 3 Punkte. Die Laufringe sind aber radial elastisch und beim Lauf treten auch Unwucht und unregelmäßige Radialkräfte in Form von Schwingungen auf.
