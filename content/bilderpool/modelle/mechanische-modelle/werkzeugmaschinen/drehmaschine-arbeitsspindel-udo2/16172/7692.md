---
layout: "comment"
hidden: true
title: "7692"
date: "2008-11-03T16:56:39"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Bei aktueller Bepreisung und Beschaffung in Einzelstückzahlen steht zu befürchten, dass der Erwerb hinreichender Mengen (Stückzahlen) der ft-Massivteile mit kleinstmöglichem von Null verschiedenem Winkel die je nach aktuellen Gegebenheiten und bereits verbauten Mengen variieren mögenden finanziellen Möglichkeiten Einzelner überschreiten könnte.

Will sagen: Da gehen die 7,5er mal wieder weg wie die warmen Semmeln. :-)

Tolles Modell. Am besten gefällt mir, wie Du den durchgängigen Achsdurchlass hinbekommen hast.

Gruß,
Stefan