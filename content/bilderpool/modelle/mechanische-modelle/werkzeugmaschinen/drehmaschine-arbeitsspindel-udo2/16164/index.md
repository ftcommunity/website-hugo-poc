---
layout: "image"
title: "(1/9) Gesamtansicht"
date: "2008-11-03T07:17:19"
picture: "drehmaschinearbeitsspindel1.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16164
- /detailscc97-2.html
imported:
- "2019"
_4images_image_id: "16164"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16164 -->
Vorweg:
Das Modell meiner Bohr- und Fräsmaschine BF1 habe ich aus Gründen der Verfügbarkeit von Bauteilen klein gehalten. Die Pinole mit der Arbeitsspindel war so nur als Funktionsprinzip mit einer Umgehungslösung in das Modell umsetzbar. Das Modell einer Drehmaschine könnte ich ja nun etwas größer halten. Dazu wollte ich vorab in Erfahrung bringen, inwieweit hier die Arbeitspindel in welcher Größe möglichst realistisch in so ein Modell mit ft-Teilen umsetzbar ist. Das ist nun hier das Ergebnis meiner Machbarkeitsstudie aus genau 532 Bauteilen zu den Baugruppen Arbeitsspindel mit Lagerung nach dem Vorbildstandard einer konventionellen Drehmaschine.
Modelllösung:
- 4-fach rollengelagerte Arbeisspindel (je 2x radial und axial)
- Kopfflansch mit Zentrieransatz und Dreipunktbestigung für die Werkstückspanner wie Drei- und Vierbackenfutter sowie Planscheibe
- Stangendurchlaß
- Spindelantrieb über Kette vom entfernten Spindelgetriebe auf Zahnrad Z40 (in der technischen Praxis Zahnriemen, früher Keilriemen)
- Abgriff für den Antrieb des Vorschubgetriebes über Zahnrad Z30 
- Kopf- und Fußlagergehäuse des Spindelstocks
Es fehlt lediglich der Innenkonus am Spindelkopf z.B. zur Aufnahme einer Zentrierspitze zum Drehen zwischen den Spitzen.
Praktizierte Vorgehensweise:
In gemischter Form mit wechselndem Vorlauf bei der Konstruktion mit dem Designer oder bei der Konstruktion mittels partiellem Aufbau der Funktionsdetails.
Ausblick:
Die Funktion des Gesamtaufbaus wird von mir mit den aktuellen Erkenntnissen ohne Vorstellung einer max. Drehzahl als gesichert eingeschätzt. Der Gesamtaufbau ist allerdings anspruchsvoll und mit allgemein üblichen Gewohnheiten besonders im Hinblick auf den Rundlauf nicht erfolgreich. Dazu muß z.B. jedes Bauteil im Ringmantel der Welle exakt positioniert werden. Mit der Erfordernis des Anbringens von ft-systemfremden Ausgleichteilen (Metall) zum Wuchten muß dennoch gerechnet werden.
Die Konstruktion einer Planscheibe mit der wohl größten Belastung für die Arbeitsspindel ist hilfreich. Sobald ich diese Konstruktion gelegentlich erarbeitet habe, werde ich das Ganze dann mit einem Motorantrieb zur Beprobung mal aufbauen.
