---
layout: "image"
title: "(3/9) Ansicht von links"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel3.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16166
- /detailse00d.html
imported:
- "2019"
_4images_image_id: "16166"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16166 -->
Arbeitsspindel ohne Lagergehäuse mit den beiden äußeren Laufringen:
Das Axiallager im Fußlager nimmt axiale Kräfte von links auf.
