---
layout: "image"
title: "(7/9) Kopflager"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel7.jpg"
weight: "7"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16170
- /details6b29.html
imported:
- "2019"
_4images_image_id: "16170"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16170 -->
Die Entwicklung der Baugruppen erfolgte wie unter (1/9) beschrieben gemischt, also mit dem ftD sowie über den partiellen Aufbau aller Funktionsgruppen. Von Interesse waren dabei Montierbarkeit, Festigkeit sowie Kreisbogen- und Winkelsteinababstimmungen. Dieser Laufring hier besteht nur aus 47 Winkelsteinen 7,5°. Da läuft das Radiallager ohne Dämpfungsfolie spielfrei.
Die radialen Rollkörper sind zusätzlich gegen Abspringen von den Radachsen gesichert durch Kopfteilstücke von Flachkopf-Klammern für Mustertaschen.
Der aufmerksame Betrachter des Fotos erkennt weiterhin links oben die Papiereinlage zwischen den beiden gelben Winkelsteinen 7,5°. Die habe ich vor dem Fotografieren vergessen noch herauszunehmen. Sie bedeutet weder Trick noch Funktionsnot, sondern entstammt aus Versuchen zu einer gezielten Radialelastizität.
