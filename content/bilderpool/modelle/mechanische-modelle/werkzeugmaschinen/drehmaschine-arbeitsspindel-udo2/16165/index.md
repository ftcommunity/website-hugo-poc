---
layout: "image"
title: "(2/9) Ansicht von rechts"
date: "2008-11-03T07:17:20"
picture: "drehmaschinearbeitsspindel2.jpg"
weight: "2"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16165
- /detailsed6c.html
imported:
- "2019"
_4images_image_id: "16165"
_4images_cat_id: "1461"
_4images_user_id: "723"
_4images_image_date: "2008-11-03T07:17:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16165 -->
Arbeitsspindel ohne Lagergehäuse mit den beiden äußeren Laufringen:
In der technischen Praxis bestehen Wälzlager als Verschleißbaugruppe in der Regel aus zwei Laufringen und dazwischen dem Wälzkäfig mit den auf Abstand geführten Wälzkörpern. Das war hier nicht möglich. Die Rollkörper Rad 14 sind ohne Wälzkäfig auf der Welle gelagert. Innere Laufringe gibt es hier also nicht. Die beiden äußeren Laufringe bestehen aus je 48 Winkelsteinen 7,5° und die beiden Lagergruppen in Kopf- und Fußlager aus je 2x6 Rollkörpern. Das Axiallager im Kopflager nimmt axiale Kräfte von rechts auf.
