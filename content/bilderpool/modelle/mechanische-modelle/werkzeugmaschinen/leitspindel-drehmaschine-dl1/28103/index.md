---
layout: "image"
title: "[3/11] Wechselrädergetriebe I"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl03.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28103
- /details82f2.html
imported:
- "2019"
_4images_image_id: "28103"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28103 -->
Ein Wechselrädergetriebe synchronisiert übersetzt die Leitspindel mit der Arbeitsspindel, was z.B. beim Gewindedrehen für eine gewünschte Steigung erforderlich ist.

Hier ist die am Modell grösste mögliche Übersetzung mit der Räderanordnung Z10:Z40=Z15:Z40=Z10:Z40 zum Langdrehen aufgesteckt. Der Bettschlittenvorschub erreicht damit 0,11 mm/Spindelumdrehung. Das oben freilaufende klemmbare Z10 ist ein Zwischenrad, damit die Leitspindel die richtige Steigungsrichtung zum Drehsinn der Arbeitsspindel hat. Die freilaufenden Radpaare Z40=Z15 sowie Z40=Z10 sind aufgebohrt und auf Ms-Rohr 5x0,5 aufgezogen.
