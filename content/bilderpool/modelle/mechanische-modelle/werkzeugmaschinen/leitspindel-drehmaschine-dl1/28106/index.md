---
layout: "image"
title: "[6/11] Reitstockverstellung X minus"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl06.jpg"
weight: "6"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28106
- /details6862.html
imported:
- "2019"
_4images_image_id: "28106"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28106 -->
Die Verstellbarkeit des Reitstocks in der X-Achse quer zur Bettachse Z ermöglicht das Drehen zwischen den Spitzen von konischen Teilen, deren Mantellinie länger als der Arbeitsweg des Oberschlittens ist. Hierbei sind Teile möglich, die sich zur Arbeitsspindel hin im Durchmesser vergrössern (X minus) oder verjüngen ( X plus). Beim Drehen von langen zylindrischen Teilen normal mit X=0 kann die X-Verstellbarkeit des Reitstocks hilfreich sein parallel zur Bettachse so zu drehen, dass der gewünschte Durchmesser des Drehteils auch bei einer grösseren Werkstücklänge in der vorgegebenen Toleranz gehalten wird. Erfahrene Dreher kennen diese Praxis falls die Maschine "schräg zieht".

Der Reitstock hier quer zur Bettachse Z in der Stellung  X -5 mm und ...
