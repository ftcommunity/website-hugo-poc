---
layout: "comment"
hidden: true
title: "12170"
date: "2010-09-14T11:29:05"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Nachtrag:
In Abhängigkeit des Interesses am Modell könnte ich ähnlich wie von der  Drema von Claus-Werner später mal eine "Reprise" erstellen, die dann in wenigen 2Ds alles Funktionelle erhellt, weil die Fotografierei zu den Innereien fast einen Rückbau erfordert. Allerdings gibt es zu den Wechselrädern und der Schlossmutter keine 3D-Teile im ftD.