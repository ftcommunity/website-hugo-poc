---
layout: "image"
title: "[1/11] Modellansicht"
date: "2010-09-13T14:37:24"
picture: "leitspindeldrehmaschinedl01.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28101
- /details728c.html
imported:
- "2019"
_4images_image_id: "28101"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28101 -->
Mein neues ft-Modell "Leitspindel-Drehmaschine DL1" mit seinem aktuellen Stand in der noch nicht abgeschlossenen Bauetappe 1 "Manuelle Bedienung". Es ist wegen der Späne bereits teilweise mit Bauplatten "eingehaust".
Die rückseitige "Spritzwand" und der hochklapp- sowie verschiebbare Sichtschutz gehören eigentlich nicht zum Modell. Sie habe ich spontan zusammengeschossen, nachdem ich und das Umfeld nach dem ersten Tag mit Drehversuchen von Spänen (geschäumter Styropor!) vollgemüllt waren.
Die Versuche mit verschiedenen logischerweise nichtmetallischen Werkstoffen geringer Festigkeit und dem Ziel späterer programmgesteuerter Arbeitsabläufe fahre ich zu Prüfzwecken der Modellstabilität bis zu 9 ... 12 VDC mit einem Power Supply. Da kommt der Power-Motor 8:1 auf über 1000 U/min, die mit der High-Stufe des Spindelgetriebes noch um den Faktor 1,5 bis zur Arbeitsspindel erhöht werden.
Die Funktionen des etwas grösser gehaltenen Modells lehnen sich wie bei meinem ft-Modell Bohr- und Fräsmaschine BF1 im Sinne einer überschaubaren Anlagenkomponente an die Modellbaumaschinen der Heimwerkerklasse an: verripptes Maschinenbett mit Zahnstange und Leitspindel, Spindelstock mit 2-stufigem Spindelgetriebe und Leerlauf, Support (Werkzeugträger) mit 3 Schlitten (Bett-, Quer- und Oberschlittten) und Schlossplatte, Reitstock mit Pinole, Wechselrädergetriebe für die Leitspindel zum Lang- und Gewindedrehen, Planscheiben mit 3 oder 4 Spannbacken und Vierstahlhalter. 
Von einer Hohlspindel habe ich Abstand genommen, da die hierzu geeigneten Drehteilwerkstoffe bei langen und sehr dünnen Abmessungen der Werkstücke nicht die erforderliche Festigkeit haben.
Da momentan mit "Vario" und später dann programmgesteuert mit 8 besser mit 512 Geschwindigkeitsstufen etwas langsamer gesteuert wird, sollte mit nur 2 mechanischen Getriebestufen (Low- und High) dennoch ein ausreichender Drehzahlumfang angenähert stufenlos nutzbar sein.
Das Wechselrädergetriebe lässt nur geometrisch begrenzt ebenfalls eine Vielzahl von Leitspindelumdrehungen zu. Allerdings sind mit der m1,5-Steigung 4,71mm der ft-Spindel keine standardisierten Gewindesteigungen möglich ...

Anmerkungen:
Das Drehmaschinenmodell von Claus-Werner Ludwig (Convention 2008) und meine DL1 sind von Aufgabenstellung, Bauteileaufwand und Modellgrösse her gesehen keine Konkurrenzmodelle. Jeder verfolgt hierbei seine eigenen Modellziele: Claus ein Einzelkomfortmodell und ich eine Anlagenkomponente.
Mein Modell enthält in Anlehnung an seine Vorbilder auch Funktionsgruppen von Drehmaschinen, die im Modell von Claus-Werner durch andere nicht vorhanden sind. Das sind die Baugruppen verripptes Maschinenbett in Maschinenlänge, Wechselrädergetriebe, Schlossmutter, Reitstockverstellung in der X-Achse, Planscheiben mit 3 bzw. 4 Spannbacken und Vierstahlhalter. Erste drei Baugruppen zum Modell hatte ich bereits 2007 im ftD konzipiert. Da ich nun hoffe als ft-Einsteiger 12/2006 inzwischen im Umgang mit ft etwas gereifter geworden zu sein, habe ich mich ab Ende Juni 2010 nun getraut :o) ein solches Modellvorhaben fortzuführen. Das gesamte Modell ohne den Späneschutz habe ich wie kann es bei mir anders sein :o) vor dem Aufbau im ftD konstruiert, soweit das natürlich mit den verfügbaren 3D-Teilen in spezifischen Details machbar ist.
