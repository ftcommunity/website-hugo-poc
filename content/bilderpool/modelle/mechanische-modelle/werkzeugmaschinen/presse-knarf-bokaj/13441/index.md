---
layout: "image"
title: "Presse01"
date: "2008-01-27T17:30:27"
picture: "presse1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13441
- /detailsb8b7.html
imported:
- "2019"
_4images_image_id: "13441"
_4images_cat_id: "1222"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13441 -->
Presse von vorne