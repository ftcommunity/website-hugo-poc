---
layout: "image"
title: "Bohrmaschine (Antrieb Koordinatentisch rechts/links)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/41089
- /details3dfd.html
imported:
- "2019"
_4images_image_id: "41089"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41089 -->
Mit diesem Motor wird der Tisch nach links und rechts bewegt.