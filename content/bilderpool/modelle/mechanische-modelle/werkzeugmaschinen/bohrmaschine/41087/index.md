---
layout: "image"
title: "Bohrmaschine (von oben)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/41087
- /detailsc668-2.html
imported:
- "2019"
_4images_image_id: "41087"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41087 -->
Oben rechts und links erfolgt die Steuerung der Auf- und Abbewegung.