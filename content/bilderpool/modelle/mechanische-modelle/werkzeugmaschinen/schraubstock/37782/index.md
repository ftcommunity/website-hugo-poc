---
layout: "image"
title: "Schraubstock"
date: "2013-11-01T14:03:14"
picture: "schraubstock4.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37782
- /details30b4.html
imported:
- "2019"
_4images_image_id: "37782"
_4images_cat_id: "2807"
_4images_user_id: "453"
_4images_image_date: "2013-11-01T14:03:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37782 -->
