---
layout: "image"
title: "Schraubstock"
date: "2017-08-16T20:50:57"
picture: "schraubstock2.jpg"
weight: "2"
konstrukteure: 
- "ft"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46122
- /details5e44.html
imported:
- "2019"
_4images_image_id: "46122"
_4images_cat_id: "3426"
_4images_user_id: "2303"
_4images_image_date: "2017-08-16T20:50:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46122 -->
