---
layout: "image"
title: "Schraubstock"
date: "2013-11-01T14:03:14"
picture: "schraubstock2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37780
- /details91da-2.html
imported:
- "2019"
_4images_image_id: "37780"
_4images_cat_id: "2807"
_4images_user_id: "453"
_4images_image_date: "2013-11-01T14:03:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37780 -->
