---
layout: "image"
title: "36/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus28.jpg"
weight: "28"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16201
- /details733a.html
imported:
- "2019"
_4images_image_id: "16201"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16201 -->
Reitstock
