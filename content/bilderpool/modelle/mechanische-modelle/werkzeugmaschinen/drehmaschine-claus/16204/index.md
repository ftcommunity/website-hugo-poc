---
layout: "image"
title: "41/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus31.jpg"
weight: "31"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16204
- /details784b-2.html
imported:
- "2019"
_4images_image_id: "16204"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16204 -->
Reitstock, manuelle Verschiebung längs
