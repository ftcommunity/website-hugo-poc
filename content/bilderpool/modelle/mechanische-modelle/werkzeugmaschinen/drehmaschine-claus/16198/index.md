---
layout: "image"
title: "33/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus25.jpg"
weight: "25"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16198
- /detailsd2ca-2.html
imported:
- "2019"
_4images_image_id: "16198"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16198 -->
Reitstock, Ansicht von hinten
