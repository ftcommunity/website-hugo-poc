---
layout: "image"
title: "45/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus35.jpg"
weight: "35"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16208
- /detailsf5c2-2.html
imported:
- "2019"
_4images_image_id: "16208"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16208 -->
Oberschlitten, Drehmechanik (Getriebeschnecke und Drehkranz)
