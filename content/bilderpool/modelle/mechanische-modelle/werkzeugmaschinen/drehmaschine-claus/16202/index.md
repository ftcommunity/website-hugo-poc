---
layout: "image"
title: "39/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus29.jpg"
weight: "29"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16202
- /details0c8e.html
imported:
- "2019"
_4images_image_id: "16202"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16202 -->
Reitstock, hintere Stirnseite
