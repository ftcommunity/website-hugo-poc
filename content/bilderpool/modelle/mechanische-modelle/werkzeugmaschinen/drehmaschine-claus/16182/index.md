---
layout: "image"
title: "15/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus09.jpg"
weight: "9"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16182
- /details568d.html
imported:
- "2019"
_4images_image_id: "16182"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16182 -->
Vorschubgetriebe von hinten
