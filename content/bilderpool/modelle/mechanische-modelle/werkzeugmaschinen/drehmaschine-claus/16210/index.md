---
layout: "image"
title: "47/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus37.jpg"
weight: "37"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16210
- /details53a8.html
imported:
- "2019"
_4images_image_id: "16210"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16210 -->
Vorschubgetriebe, Teilansicht
