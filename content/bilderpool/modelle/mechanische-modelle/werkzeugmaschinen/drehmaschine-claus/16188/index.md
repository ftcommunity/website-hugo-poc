---
layout: "image"
title: "21/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus15.jpg"
weight: "15"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16188
- /details7a51-2.html
imported:
- "2019"
_4images_image_id: "16188"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16188 -->
Spindelgetriebe, Teilansicht von hinten
