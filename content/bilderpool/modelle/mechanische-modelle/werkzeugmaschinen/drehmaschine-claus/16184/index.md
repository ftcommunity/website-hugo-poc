---
layout: "image"
title: "17/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus11.jpg"
weight: "11"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16184
- /detailsc711.html
imported:
- "2019"
_4images_image_id: "16184"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16184 -->
Das Hauptgetriebe (Spindelgetriebe) wird durch die beiden Power-Motoren 50:1 rot über ein Differenzial angetrieben.
Das Hauptgetriebe besteht aus einem 2-Gang-Getriebe mit Rückwärtsgang und einem nachgeschalteten 3-Gang-
Getriebe. Die Arbeitsspindel mit dem 3-Backen-Futter ist als Nachgelege verschiebbar mit 2 Gängen und Leerlauf.
Damit hat die Arbeitsspindel 18 mechanische Gänge.
