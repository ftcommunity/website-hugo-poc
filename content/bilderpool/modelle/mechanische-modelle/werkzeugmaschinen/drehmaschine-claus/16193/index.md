---
layout: "image"
title: "26/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus20.jpg"
weight: "20"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16193
- /detailsa738-2.html
imported:
- "2019"
_4images_image_id: "16193"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16193 -->
3-Backen-Futter selbstzentrierend, Ansicht von hinten
