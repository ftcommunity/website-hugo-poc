---
layout: "image"
title: "30/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus22.jpg"
weight: "22"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16195
- /detailsd188-2.html
imported:
- "2019"
_4images_image_id: "16195"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16195 -->
Schloßmutter des Werkzeugschlittens, von hinten:
Kupplung des maschinellen Längsvorschubes, Stellung ausgekuppelt
