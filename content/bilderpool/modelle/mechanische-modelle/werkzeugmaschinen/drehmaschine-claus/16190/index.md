---
layout: "image"
title: "23/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus17.jpg"
weight: "17"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16190
- /details880c-2.html
imported:
- "2019"
_4images_image_id: "16190"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16190 -->
Vorschubgetriebe von hinten
Das Vorschubgetriebe kann über ein Zwischengetriebe mit 2 Vor- und einem Rückwärtsgang mit
dem Hauptgetriebe verbunden werden. Die Kraftübertragung des Hauptgetriebes stößt hierbei an
ihre Grenze und ist nur in den unteren Gängen möglich.
Deshalb ist für den separaten Antrieb des Vorschubgetriebes der Power-Motor 20:1 grau
zusätzlich vorgesehen.
