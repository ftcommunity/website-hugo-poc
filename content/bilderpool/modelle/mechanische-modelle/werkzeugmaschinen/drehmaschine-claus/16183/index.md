---
layout: "image"
title: "16/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus10.jpg"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16183
- /detailscf92.html
imported:
- "2019"
_4images_image_id: "16183"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16183 -->
Spindelgetriebe von hinten
