---
layout: "image"
title: "32/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus24.jpg"
weight: "24"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbarbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16197
- /detailsc1e5.html
imported:
- "2019"
_4images_image_id: "16197"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16197 -->
Werkzeugschlitten, Ansicht von hinten
Vorn mit der Kupplung des maschinellen Planvorschubes, Stellung ausgekuppelt
