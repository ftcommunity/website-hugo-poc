---
layout: "image"
title: "52/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus42.jpg"
weight: "42"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16215
- /detailsfe98.html
imported:
- "2019"
_4images_image_id: "16215"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16215 -->
Spindelgetriebe, hintere Stirnseite
