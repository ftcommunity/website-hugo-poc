---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (2/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf02.jpg"
weight: "2"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15968
- /details6e51.html
imported:
- "2019"
_4images_image_id: "15968"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15968 -->
Aus Wartungs- und Studiengründen habe ich abweichend von der Modellvorlage den Spindelkopf mit einer zweiflügeligen Kopfverkleidung ausgestattet. Das Spindelkopfgetriebe befindet so sich in einem selbsttragenden C-Rahmen, der mit der Verdrehsicherung der Spindelpinole zum O-Rahmen wird.
