---
layout: "image"
title: "Baumstammsäge"
date: "2013-06-29T20:52:03"
picture: "baumstammsaege5.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37124
- /details9503.html
imported:
- "2019"
_4images_image_id: "37124"
_4images_cat_id: "2755"
_4images_user_id: "453"
_4images_image_date: "2013-06-29T20:52:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37124 -->
