---
layout: "image"
title: "Anzeige- & Bedinfeld"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded22.jpg"
weight: "22"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42197
- /detailsb82a.html
imported:
- "2019"
_4images_image_id: "42197"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42197 -->
Die Leuchtsteine haben zwei Aufgaben...

1. sie zeigen welchen Level man gewählt hat
2. wieviel Leben/Versuche für die Runde noch hat

Damit man von den verbauten Leds nicht geblendet wird, ist die Leuchtkappe mit etwas Watte gefüllt.

(Wer Rechtschreibfehler findet darf sie selbstverständlich behalten oder sie zur belustigung mit Freunden und Verwandten teilen!)
