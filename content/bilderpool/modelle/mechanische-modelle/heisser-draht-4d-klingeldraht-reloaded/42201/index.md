---
layout: "image"
title: "Bedienfeld von  - Vorderseite 2 -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded26.jpg"
weight: "26"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42201
- /details611b.html
imported:
- "2019"
_4images_image_id: "42201"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42201 -->
Die Kabel wurden platzsparend mit QTip-Röhrchen anstatt mit ftSteckern befestigt.
