---
layout: "image"
title: "Bedienfeld von  - Vorderseite 1 -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded25.jpg"
weight: "25"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42200
- /details95ba-2.html
imported:
- "2019"
_4images_image_id: "42200"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42200 -->
