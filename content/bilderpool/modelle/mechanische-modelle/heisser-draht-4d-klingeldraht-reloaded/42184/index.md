---
layout: "image"
title: "Aufhängung des Kontaktdrahts Links & Endlagentaster"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded09.jpg"
weight: "9"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42184
- /details727c.html
imported:
- "2019"
_4images_image_id: "42184"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42184 -->
Die Startzone ist mit Hilfe eines Stücks Schrumpfschlauch vom Kontaktdraht isoliert.
Verbunden ist die Startzone über einen Metall Klemmkontakt (31338) auf der rechten Seite.
Auf der linken Seite befindet sich ebenfalls ein Metall Klemmkontakt zwischen Stellring mit Kabelbinder und BS 15 mit Loch. Dieser verbindet KD mit dem TX.
