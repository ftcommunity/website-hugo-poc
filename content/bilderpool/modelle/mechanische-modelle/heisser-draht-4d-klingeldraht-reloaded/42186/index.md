---
layout: "image"
title: "Aufhängung & Führung des Kontaktdrahts, rechte Seite von - vorne -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded11.jpg"
weight: "11"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42186
- /detailsc716.html
imported:
- "2019"
_4images_image_id: "42186"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42186 -->
