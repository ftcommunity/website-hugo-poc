---
layout: "image"
title: "Aufhängung & Führung des Kontaktdrahts - oben -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded13.jpg"
weight: "13"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42188
- /detailsb967-2.html
imported:
- "2019"
_4images_image_id: "42188"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42188 -->
Links befindet sich der Metall Klemmkontakt verborgen unter der KD Drehvorrichtung, sehen kann man nur den grünen ftStecker.

Der Minidrehkranz mit Kugellager von MisterWho hat diese Projekt, in der kompakten Art erst ermöglicht. (Vielen DANK nochmals!)
