---
layout: "image"
title: "Bedienfeld von  - Seite - Detail"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded27.jpg"
weight: "27"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/42202
- /detailsb5b1-2.html
imported:
- "2019"
_4images_image_id: "42202"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42202 -->
