---
layout: "image"
title: "Gesamtansicht"
date: "2008-02-27T18:12:59"
picture: "papierfliegerabschussrampe1.jpg"
weight: "1"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/13797
- /detailsfad6-2.html
imported:
- "2019"
_4images_image_id: "13797"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:12:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13797 -->
rechts oben das Gegengewicht.
links unten der Joystick.
Linker Taster: abschießen und dann sofort wieder spannen.
Rechter Taster: Abschuß (bzw. Entspannen)