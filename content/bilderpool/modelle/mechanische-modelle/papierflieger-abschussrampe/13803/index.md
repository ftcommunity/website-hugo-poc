---
layout: "image"
title: "Detail Rampe"
date: "2008-02-27T18:13:00"
picture: "papierfliegerabschussrampe7.jpg"
weight: "7"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/13803
- /details1b0d.html
imported:
- "2019"
_4images_image_id: "13803"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:13:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13803 -->
Die Rollen vorne verhindern in dieser Anordnung sehr effektiv das herausspringen des Gummis