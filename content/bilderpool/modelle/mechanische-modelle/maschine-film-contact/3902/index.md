---
layout: "image"
title: "Antrieb rechte Seite"
date: "2005-03-28T23:31:27"
picture: "Contact-Maschine_005.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3902
- /detailsceaa.html
imported:
- "2019"
_4images_image_id: "3902"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3902 -->
Das rote Z40 wird angetrieben, sitzt aber lose auf der Metallachse, die wiederum unbeweglich eingespannt ist. Genauso unbeweglich bleibt das schwarze Z40 stehen, auf dem durch die Zwangsdrehung des Rings das Rast-Z10 abrollt...