---
layout: "image"
title: "Gesamtansicht 1"
date: "2005-03-28T23:31:27"
picture: "Contact-Maschine_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3899
- /details6987.html
imported:
- "2019"
_4images_image_id: "3899"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3899 -->
Kennt jemand den Science-Fiction "Contact"? Da gibt es eine Maschine, die die Menschen nach den Plänen von Außerirdischen bauen, um einen Menschen hin zu schicken. Darin bewegen sich drei ineinander verschachtelte große Ringe, der äußere aufgehängt zwischen zwei Türme, die inneren frei schwebend, nach einem wie ich finde faszinierenden Muster. Das "frei schwebend" ist mir dann doch etwas schwer gefallen. Außerdem drehen sich die "echten" Ringe auch noch wie ein Rad um sich selbst - das hab ich hier noch gar nicht realisiert. Ebenfalls fehlt noch der Kran, von dem aus genau über der Mitte der Ringe die Kapsel mit Jodie Foster drin durch die Ringe fallen gelassen und auf eine höchst relativistische Reise geschickt wird. Das Ding hat mich reichlich fasziniert.