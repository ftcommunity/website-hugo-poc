---
layout: "image"
title: "Queransicht"
date: "2005-03-28T23:50:43"
picture: "Contact-Maschine_011.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3908
- /details7868.html
imported:
- "2019"
_4images_image_id: "3908"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3908 -->
Hier gibt's nicht sooo viel zu sehen, außer vielleicht die Antriebsachse längs durch den Bodentunnel.