---
layout: "comment"
hidden: true
title: "22529"
date: "2016-09-20T11:00:38"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Ihr seid ja ein lustigen Völkchen, das habe ich im Forum bereits bemerkt.
Und hier? Wer den Schaden hat, braucht für den Spott nicht zu sorgen.
@H.A.R.R.Y.: Mir ging es bei dem Getriebe nur darum zu zeigen, dass dieser Schrittmotor ausreichend Drehmoment liefert.
@geometer: Deine Getriebe sind klasse, habe ich bereits bewundert!
@Dirk: Der "andere Motor" liefert übrigens in einer Variante genügend Drehmoment für ein kleines Getriebe.