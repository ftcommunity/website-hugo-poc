---
layout: "image"
title: "Der innere Kern"
date: 2022-01-21T10:06:59+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unter den vielen Bausteinen ist via BS5 (weil es sonst zu eng würde) noch eine Drehscheibe eingebaut, aber die wäre sogar unnötig.