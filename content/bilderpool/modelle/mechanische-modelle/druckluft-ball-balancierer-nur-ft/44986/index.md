---
layout: "image"
title: "Überblick (1)"
date: 2022-01-21T10:07:10+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

11 fischertechnik-Kompressoren arbeiten zusammen, um eine kleinen grünen Plastikball (aus dem fischertechnik Robotics SmartTech-Kasten) in der Luft zu halten. Bisher gelang mir das nur mit einem größeren Kompressor (siehe https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/pneumatik-kompressoren-anderes/druckluft-ball-balancierer/gallery-index/ und https://youtu.be/nwEi3VpBSXY sowie https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/pneumatik-kompressoren-anderes/druckluft-ball-balancierer-2-dusen/gallery-index/ und https://youtu.be/tDQxQxtOjwU). Es bleibt noch zu prüfen, was die minimale ft-Kompressoranzahl dafür ist.

Ein Video gibt es unter https://youtu.be/-8XnnknQXG4