---
layout: "image"
title: "Elektrik"
date: 2022-01-21T10:07:01+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Strom kommt von unten an den Lampensockel. Über einen Ein-/Ausschalter geht es sowohl nach links als auch nach rechts zu den Kompressoren. Die sind als echter "Kreis" verdrahtet, und der Strom wird beidseitig zugeführt, damit eventuelle Leitungs- oder Steckerwiderstände weniger wichtig sind. Alles läuft von einem einzigen nicht-ft-9V-Netzteil.

Das Körbchen ist für den Ball, wie man sieht. Hinden dran sind zwei alte ft-Lampen in Serie mit dem XS-Motor zum Drehen - der soll langsam laufen.