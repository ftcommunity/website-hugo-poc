---
layout: "image"
title: "Überblick (2)"
date: 2022-01-21T10:07:02+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vier Lufttanks puffern die Druckluft und pusten sie oben aus den vier Düsen heraus. Das genügt nicht für einen Tischtennis-Ball - keine Chance - viel zu schwer. Aber für den kleinen leichten Ball genügt es.