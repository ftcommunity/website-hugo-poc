---
layout: "image"
title: "Unterseite"
date: 2022-01-21T10:06:57+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Bei jedem Segment sitzt unten eine ft-Feder. Die bewirkt, dass die Maschine nur leise surrt und rauscht, aber nicht weiter stört.

Die Drehscheibe hätte ich auch weglassen können, aber das wurde mir erst später klar.