---
layout: "image"
title: "Rückseite"
date: 2022-01-21T10:07:00+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alle 12 Segmente außer dem mit dem Schalter tragen je einen ft-Kompressor. Je 3 gehen an einen Lufttank (einmal halt nur 2). Alles in allem sind 4 Tanks verbaut, die jeweils eine der älteren Pneumatik-Strahldüsen betreiben.