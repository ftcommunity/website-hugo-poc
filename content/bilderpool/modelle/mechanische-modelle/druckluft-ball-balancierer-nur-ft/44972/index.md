---
layout: "image"
title: "Bei der Arbeit (1)"
date: 2022-01-21T10:06:54+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier einige Momentaufnahmen davon, wie der Ball - frei in der Luft schwebend - über dem Modell kreist.