---
layout: "image"
title: "Taumelscheibe (2)"
date: 2022-01-21T10:06:55+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Drehmittelpunkt des Kardans sitzt genau in der Mitte der Drehscheibe. Auf dem Video sieht man die Wirkungsweise ganz gut.

Die Rollen sind so justiert, dass sie immer mit der Drehscheibe in Kontakt sind und nicht in ein Loch oder einen Schlitz geraten. Andernfalls würde das ruckeln und die Gefahr bestehen, dass der Ball herunterfällt.