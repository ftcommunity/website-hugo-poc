---
layout: "image"
title: "Taumelscheibe (1)"
date: 2022-01-21T10:06:56+01:00
picture: "2022-01-16_Druckluft-Ball-Balancierer_nur_mit_ft-Kompressoren07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Einen netten Zusatzeffekt sollte es geben: Der "Finger", der auf die Kugel zeigt, sollte sich im Kreis bewegen. Nun darf der sich aber nicht drehen, weil sich sonst die Schläuche verdrillen und verheddern würden. Deshalb gibt es diese Mechanik: Die Drehscheibe sitzt mit einer Freilaufnabe auf einer Rastachse mit Platte. Die steckt in einem Rastadapter, und in dem steckt ein Kardanbaustein. Die obere Drehscheibe dreht sich also nicht, wird aber durch die Rollen in eine Taumelbewegung gebracht.