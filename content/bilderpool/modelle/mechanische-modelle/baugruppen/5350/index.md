---
layout: "image"
title: "pneumatischer Zweifinger-Parallelgreifer"
date: "2005-11-22T22:16:55"
picture: "Greifer.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["Pneumatik", "Greifer", "Roboter", "Finger", "Hand", "Modul", "Baugruppe"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5350
- /detailsa5c7.html
imported:
- "2019"
_4images_image_id: "5350"
_4images_cat_id: "462"
_4images_user_id: "9"
_4images_image_date: "2005-11-22T22:16:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5350 -->
Die Achse, die die Greiferzangen führt, müsste so 150mm lang sein. Der dritte Baustein 15 mit Loch auf dieser Achse sorgt dafür, dass ich den Greifer auf eine Drehscheibe 60 montieren kann. 

Ich glaube, sonst ist hier kein Baustein zu viel, und kleiner kann man es auch nicht bauen.