---
layout: "image"
title: "Basküle0037.JPG"
date: "2013-10-19T15:53:09"
picture: "Baskle0037.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37719
- /detailsc506.html
imported:
- "2019"
_4images_image_id: "37719"
_4images_cat_id: "462"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T15:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37719 -->
Ein Baskülenverschluss (sowas findet man in Schrank- oder Tresortüren oder bei Fensterflügeln).
