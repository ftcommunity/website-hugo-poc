---
layout: "image"
title: "So sieht´s von innen aus"
date: "2007-10-08T14:12:09"
picture: "DSCN1679.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12170
- /details29cd.html
imported:
- "2019"
_4images_image_id: "12170"
_4images_cat_id: "1089"
_4images_user_id: "184"
_4images_image_date: "2007-10-08T14:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12170 -->
Es geht noch breiter ....
