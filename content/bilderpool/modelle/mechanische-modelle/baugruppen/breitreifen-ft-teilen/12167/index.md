---
layout: "image"
title: "Ansicht"
date: "2007-10-08T14:12:09"
picture: "DSCN1684.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12167
- /details7fc4.html
imported:
- "2019"
_4images_image_id: "12167"
_4images_cat_id: "1089"
_4images_user_id: "184"
_4images_image_date: "2007-10-08T14:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12167 -->
Ich sitze hier gerade an meinem Basteltisch und halte ein Innenzahnrad in der Hand.
Irgendwie wird das Teil selten verbaut. Da dachte ich mir, mach doch mal etwas damit .....
