---
layout: "comment"
hidden: true
title: "23115"
date: "2017-02-27T09:19:18"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Boah -  grandios, Ludger! Ich habe schon einige Zeit versenkt bei dem Versuch, irgendetwas zwischen die Zähne der Z20/Z30/Z40 zu frickeln und bin daran stets gescheitert. Dein Foto war zwei oder drei Jahre, bevor ich den Bilderpool hier regelmäßig durchgeschaut habe, sonst hätte ich das bestimmt im Kopf gehabt.

Natürlich habe ich es jetzt sofort nachgebaut. Passt alles zusammen, als wären die Teile für diesen Zweck entworfen worden. Große Klasse!