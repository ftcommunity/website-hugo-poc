---
layout: "image"
title: "Rollenbock und Zahnrad Z30"
date: "2006-04-05T11:41:20"
picture: "DSCN0687.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Vieleck"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6020
- /details30f4.html
imported:
- "2019"
_4images_image_id: "6020"
_4images_cat_id: "522"
_4images_user_id: "184"
_4images_image_date: "2006-04-05T11:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6020 -->
Mit 10 Stück Rollenbock und entsprechendem Zubehör kann man auch etwas machen......
