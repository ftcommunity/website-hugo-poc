---
layout: "image"
title: "Die Antriebsseite"
date: "2010-02-16T11:28:38"
picture: "powermotormitgegenlaeufigemabtrieb3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26459
- /details3d4a.html
imported:
- "2019"
_4images_image_id: "26459"
_4images_cat_id: "1882"
_4images_user_id: "104"
_4images_image_date: "2010-02-16T11:28:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26459 -->
Hier sieht man teilweise, wie alles befestigt ist.
