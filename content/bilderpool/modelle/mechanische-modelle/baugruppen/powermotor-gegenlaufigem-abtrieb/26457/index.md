---
layout: "image"
title: "Gesamtansicht"
date: "2010-02-16T11:28:37"
picture: "powermotormitgegenlaeufigemabtrieb1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26457
- /details435f.html
imported:
- "2019"
_4images_image_id: "26457"
_4images_cat_id: "1882"
_4images_user_id: "104"
_4images_image_date: "2010-02-16T11:28:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26457 -->
Ein Powermotor soll zwei Wellen gegenläufig antreiben. Diese Lösung baut recht kompakt, super stabil und mit definierten Maßen. Man muss nirgendwo feinjustieren, sondern kann immer etwas bis zu einem Anschlag schieben.
