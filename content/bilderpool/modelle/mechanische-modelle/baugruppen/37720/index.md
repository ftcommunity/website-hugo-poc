---
layout: "image"
title: "Basküle0038.JPG"
date: "2013-10-19T15:53:29"
picture: "Baskle0038.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37720
- /detailsb787.html
imported:
- "2019"
_4images_image_id: "37720"
_4images_cat_id: "462"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T15:53:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37720 -->
Zentrales Element ist das Spurstangengelenk 35068.
