---
layout: "image"
title: "Kraftheber2-01.JPG"
date: "2005-11-11T12:08:55"
picture: "Kraftheber2-01.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5300
- /detailsa3d1.html
imported:
- "2019"
_4images_image_id: "5300"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:08:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5300 -->
Die Schnecken müssen UNTER den Rastkurbeln sitzen, damit sie bei Belastung ins Lager hineingedrückt werden.
An diesem Modell ist nichts geschnippelt oder geklebt, es ist alles Original-ft.
