---
layout: "image"
title: "Kraftheber3-02.JPG"
date: "2005-11-11T12:15:12"
picture: "Kraftheber3-02.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5304
- /details4d32.html
imported:
- "2019"
_4images_image_id: "5304"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5304 -->
