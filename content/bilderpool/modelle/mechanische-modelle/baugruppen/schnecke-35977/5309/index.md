---
layout: "image"
title: "Winkelantrieb01.JPG"
date: "2005-11-11T12:25:23"
picture: "Winkelantrieb01.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5309
- /detailsc086-2.html
imported:
- "2019"
_4images_image_id: "5309"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5309 -->
So richtig glücklich macht diese Konstruktion nicht, kein Teil bleibt im ft-Rastermaß.
