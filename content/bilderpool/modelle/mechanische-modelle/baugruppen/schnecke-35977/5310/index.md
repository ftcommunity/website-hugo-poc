---
layout: "image"
title: "Winkelantrieb02.JPG"
date: "2005-11-11T12:27:16"
picture: "Winkelantrieb02.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5310
- /details6758-2.html
imported:
- "2019"
_4images_image_id: "5310"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:27:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5310 -->
Dieser Antrieb läuft super. Sogar auf eine Sicherung gegen Herausziehen der Schnecken aus den Lagersteinen kann man verzichten, wenn die Kräfte nicht allzu groß werden. Das ist der Antrieb fürs Seitenruder der A-340 geworden.
