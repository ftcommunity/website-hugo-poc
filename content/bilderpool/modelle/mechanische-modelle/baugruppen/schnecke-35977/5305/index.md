---
layout: "image"
title: "Querruder01.JPG"
date: "2005-11-11T12:18:22"
picture: "Querruder01.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5305
- /details6ffd-2.html
imported:
- "2019"
_4images_image_id: "5305"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:18:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5305 -->
Für den Flieger A340 brauchte ich ein Getriebe, das die beiden Querruder genau gegenläufig antreibt und in beiden Endpositionen den Motor über Taster stillsetzt. Mehr als ca. 60° Drehwinkel sind nicht gefordert, und das hier ist daraus geworden.
