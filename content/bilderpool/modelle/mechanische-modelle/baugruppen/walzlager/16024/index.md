---
layout: "image"
title: "(5/8) Drehkranz als Drehtisch"
date: "2008-10-20T21:35:35"
picture: "drehkranz5.jpg"
weight: "12"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16024
- /details48f5-2.html
imported:
- "2019"
_4images_image_id: "16024"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16024 -->
Der Drehkranz mit vertikaler Achse und Axialgewicht im Probelauf.
