---
layout: "image"
title: "(7/8) Drehkranz als Horizontallager 2"
date: "2008-10-20T21:35:35"
picture: "drehkranz7.jpg"
weight: "14"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16026
- /details5841-3.html
imported:
- "2019"
_4images_image_id: "16026"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16026 -->
Der Drehkranz mit waagerechter Achse im Probelauf. Die Federvorspannung des Tauchantriebs drückt hier zusätzlich auf das Drehringpaar senkrecht nach unten.
