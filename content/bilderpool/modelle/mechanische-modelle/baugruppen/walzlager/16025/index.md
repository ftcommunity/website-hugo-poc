---
layout: "image"
title: "(6/8) Drehkranz als Horizontallager 1"
date: "2008-10-20T21:35:35"
picture: "drehkranz6.jpg"
weight: "13"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16025
- /detailsfe5b.html
imported:
- "2019"
_4images_image_id: "16025"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16025 -->
Der Drehkranz mit horizontaler Achse im Probelauf. Die Federvorspannung des Tauchantriebs drückt hier waagerecht auf das Drehringpaar.
