---
layout: "image"
title: "Nachbau Axiallager (1/7) Biegeteile"
date: "2008-10-03T09:42:10"
picture: "axiallagernachbau1.jpg"
weight: "1"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15801
- /details4698.html
imported:
- "2019"
_4images_image_id: "15801"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15801 -->
Thematik:
Nachbau des Axiallagers von Martin Roman [Remadus], vorgestellt zur Convention 2008
Aufgabenstellung:
Kennenlernen der Eigenschaften dieser Konstruktion zur Ideenfindung für weiterführende konstruktive Lösungen.
Baugrundlage:
Zum Einsatz kamen ausschließlich neue Bauteile aus meiner Bevorratung und nachbestellte Bogenstücke 60°.
Bauvorstufe:
Der Flachträger 120 ist ein Mehrzweckteil zusammensetzbar mit dem Flachstück 120 sowie den Bogenstücken 60° und 30°. Wenn der Flachträger 120 mit dem Bogenstück 60° zusammengesteckt wird, hat er besonders das Bestreben sich wieder aufzubiegen. Selbst 2 Bogenstücke 60° sind nicht stabil genug, das 100%ig zu verhindern. Die Folge ist dann mehr oder weniger hier ein leicht sechseckiger Kreisbogen. Also habe ich die Flachträger 120 etwas "vorgespannt". Dabei muss aber darauf geachtet werden, daß diese Vorspannbögen "rund" sind und keine Knicke neben den Löchern haben. Leider hatte ich damit nur 2 Std. Geduld, um endlich das Modell aufbauen zu können. Rechts der einzelne Flachträger 120 wäre eine Rückspannvariante als letzte Stufe nach dem Rückbau eines Modells, die sicherlich im Verhältnis zum Aufbau zeitlich kürzer anzusetzen ist.

Nachtrag:
Haralds und Stefans Kommentarreaktionen signalisieren mir, hier wichtige Hinweise vergessen zu haben, die man allerdings selbst mit Sachkenntnis automatisch beachtet. Deshalb dazu von mir folgende Ergänzungen:
1. Die vorgestellten Biegungen bewegen sich in der Nähe der Elastizitätsgrenze des Werkstoffs. Eine unrunde Biegung führt örtlich zu einer stärkeren und kann dort die Elastizitätsgrenze überschreiten.
2. Derartige Biegungen sind nur mit temperierten Teilen durchführbar, was wohl Harald mit dem Wasserbad meinte. Dabei sind 20°C eine Mindestanforderung.
3. Als Alternative können dienen Biegungen im Halbkreis mit fixierten Enden.
4. Meine praktizierte Vorspannung stellt keine Anleitung zum Handeln für die Allgemeinheit dar. Sie ist lediglich als Denkanstoß für die sicher auch schon teils bekannten Zusammenhänge gedacht, deren Umsetzung sich jeder individuell zunutze machen kann.

Nachtrag 21.10.2008:
Studie, einstellbares Axiallager
http://www.ftcommunity.de/details.php?image_id=15662
Tauchantrieb mit Führungsring zum Axiallager von Remadus
http://www.ftcommunity.de/details.php?image_id=15961
Entwicklung zum Drehkranz
http://www.ftcommunity.de/details.php?image_id=16020
