---
layout: "image"
title: "Nachbau Axiallager (7/7) Wälzkäfig, Teilansicht"
date: "2008-10-03T09:42:12"
picture: "axiallagernachbau7.jpg"
weight: "7"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15807
- /detailse310.html
imported:
- "2019"
_4images_image_id: "15807"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15807 -->
Wer danach fragt, wo die Überdeckung zwischen dem Umkreis Ø204mm und den inneren Verbindungsstößen der Laufringe Ø202,5mm aufgefangen wird, findet sie hier bei den Zapfenverbindungen zwischen Radachse, BS 7,5 und Winkelstein 15°. Durch den Hebelarm der Achse ist sie an ihrem Zapfen am meisten beteiligt . Natürlich hat auch die Elastizität der beiden Laufringe und des Wälzkäfigs daran ihren Anteil.
Laufeigenschaften:
Wenn man mit zunächst vorsichtiger axialer Belastung den oberen Laufring dreht, findet man eine erstaunliche Laufeigenschaft des Axiallagers. Sobald die weißen und schwarzen Räder die Verbindungsstöße der Laufringbögen überqueren ist natürlich etwas Widerstand zu spüren.
Weiterentwicklung:
Meine Erkenntnisse am gerade aufgebauten Modell sind noch frisch und bedürfen ihrer ergänzenden Abklärung.
Zu überlegen ist auf aktueller Grundlage zunächst inwieweit eine Einstellbarkeit des Umlaufdurchmessers auf den Innendurchmesser der Laufringe notwendig bzw. sinnvoll ist. 
Ich habe weiterhin vor dem Modellaufbau Lösungsansätze zur Versteifung des Wälzkäfigs in 3D im fischertechnik designer vorbereitet.
Bei allgemeinem Interesse kann nach ihrer praktischen Beprobung über erste Erfahrungen berichtet werden.
Vielleicht entsteht auch hier in der tfCommunity unter uns ft-Freunden eine rege Diskussion, denn es sollte nicht nur für Martin Roman [Remadus] als Konstrukteur und mich ein interessantes Thema sein. Dazu gehört natürlich auch der Antrieb zunächst eines solchen Drehtisches. Dazu habe ich zwei Ideen, die nach der Beprobung der Wälzkäfigversteifungen in mir abfragbar sind.
