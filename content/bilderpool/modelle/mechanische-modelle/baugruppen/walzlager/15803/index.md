---
layout: "image"
title: "Nachbau Axiallager (3/7) Laufring unten"
date: "2008-10-03T09:42:11"
picture: "axiallagernachbau3.jpg"
weight: "3"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15803
- /details3116-2.html
imported:
- "2019"
_4images_image_id: "15803"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15803 -->
Dieser untere Laufring ist mit den vier BS5 auf der Arbeitsplatte 270x390 an den Verbindungen auf den inneren Ø202,5mm zu justieren.  Das Maß zwischen den Verbindungen stellt sich an den Bogenstücken auf Ø203,5mm ein.

