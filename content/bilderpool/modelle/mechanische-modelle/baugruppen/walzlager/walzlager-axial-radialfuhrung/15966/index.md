---
layout: "image"
title: "(6/6) Radialführung, Teilansicht"
date: "2008-10-13T23:09:48"
picture: "waelzlageraxialradialfuehrung6.jpg"
weight: "6"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15966
- /detailsa163.html
imported:
- "2019"
_4images_image_id: "15966"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15966 -->
Die Nut der Achshalter ist für quadratische Zapfen ausgelegt. Der Zapfen der Achsverschraubungen ist aber 10 mm lang. Durch die entgegengesetzte Anordnung der Achshalter ergibt sich damit ein axialer Versatz von 6 mm. Diesen Versatz muß der Führungsring ebenfalls ausgleichen.
