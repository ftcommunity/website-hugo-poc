---
layout: "comment"
hidden: true
title: "7577"
date: "2008-10-14T17:15:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Heiko,
noch schnell ein Nachtrag, weil dir "solch Aufwand" unverständlich vorkommt. Er hat zumindest ausreichende Erkenntnisse gebracht, daß ich nächste Woche bei erfolgreicher Erprobung bereits mein erstes horizontales Axiallager vorstellen kann.
Grüße dich, Ingo