---
layout: "image"
title: "(1/6) Gesamtansicht"
date: "2008-10-13T23:09:47"
picture: "waelzlageraxialradialfuehrung1.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15961
- /details79a2.html
imported:
- "2019"
_4images_image_id: "15961"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15961 -->
Vorweg:
Meinen Originalnachbau des von Martin Romann [Remadus] zur Convention 2008 vorgestellten Axiallagers habe ich hier in der ftC am 03.10.2008 veröffentlicht. Der von Remadus zur Convention benutzte Zahnriemenantrieb zur Stabilisierung der Ortslage des oberen Laufringes besteht jedoch aus ft-systemfremden Teilen. Es galt daher nach Möglichkeiten zu suchen, die Ortslage des oberen Laufringes für den Einsatz eines Antriebes aus ft-Teilen zu stabilisieren.
Gesamtmodell:
Dieser Modellaufbau wurde mit einer axialen Belastung (Gewicht) von ca. 1 kg mit einer Spannungsversorgung des Motors zwischen 3 bis 12 Volt erfolgreich erprobt.

Nachtrag 21.10.2008:
Studie, einstellbares Axiallager
http://www.ftcommunity.de/details.php?image_id=15662
Nachbau des Axiallagers von Remadus
http://www.ftcommunity.de/details.php?image_id=15801
Entwicklung zum Drehkranz
http://www.ftcommunity.de/details.php?image_id=16020
