---
layout: "image"
title: "(4/8) Drehkranz, Wälzkäfig"
date: "2008-10-20T21:35:34"
picture: "drehkranz4.jpg"
weight: "11"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16023
- /details0dd4-4.html
imported:
- "2019"
_4images_image_id: "16023"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16023 -->
Zusätzlich zum Axiallager muß beim Drehkranz der Drehring (oberes Ringpaar) auch Hebelkräfte aufnehmen können. Deshalb habe ich den Wälzkäfig entsprechend geändert. Die für die Ortslage der beiden Ringpaare zuständigen Wälzkörper habe ich verdoppelt und paarweise in Höhe der Abstandsrollen angeordnet. Sie sind jetzt 45° zur Drehkranzachse geneigt und halten krallenartig die beiden Ringpaare zusammen. Die mögliche Laufkante an den Bogenstücken liegt jedoch auf der Trennebene im Spritzgußwerkzeug. Sie ist deshalb nicht gratfrei und wird beim  manuellen Nacharbeiten nicht runder. Deshalb habe ich mich vorerst für die Seilrollen 21 entschieden, deren Flanken so neben dieser Gratkante laufen. Die Ringpaarhälften sind jeweils miteinander auf Abstand 5mm verschraubt. Die Ausführung des hier vorgestellten Wälzkäfigs sehe ich noch nicht als vollendet an. An seine Verbesserung habe ich noch Wünsche und erste Ideen zu seiner Weiterenwicklung. Dazu gehört u.a. auch die Lagesicherung der Wälzkäfigteile.
