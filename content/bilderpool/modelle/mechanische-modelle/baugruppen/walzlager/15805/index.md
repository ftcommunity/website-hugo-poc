---
layout: "image"
title: "Nachbau Axiallager (5/7) Laufring oben"
date: "2008-10-03T09:42:12"
picture: "axiallagernachbau5.jpg"
weight: "5"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15805
- /detailsc6b5.html
imported:
- "2019"
_4images_image_id: "15805"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15805 -->
Hier der obere Laufring vor dem Schließen.
