---
layout: "image"
title: "Greifer"
date: "2010-10-01T14:51:17"
picture: "greifer2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
schlagworte: ["35977"]
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/28794
- /detailsa080-2.html
imported:
- "2019"
_4images_image_id: "28794"
_4images_cat_id: "2096"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T14:51:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28794 -->
mit Kassette