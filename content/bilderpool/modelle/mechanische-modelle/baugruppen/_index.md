---
layout: "overview"
title: "Baugruppen"
date: 2020-02-22T08:13:56+01:00
legacy_id:
- /php/categories/462
- /categories74ce.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=462 --> 
Baugruppen und Module, die in mehreren Modellen verwendet werden können.