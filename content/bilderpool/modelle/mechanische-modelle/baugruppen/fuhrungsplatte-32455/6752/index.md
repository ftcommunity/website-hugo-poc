---
layout: "image"
title: "32455-Hub01.JPG"
date: "2006-08-29T21:08:49"
picture: "32455-Hub01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6752
- /detailsf665-2.html
imported:
- "2019"
_4images_image_id: "6752"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:08:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6752 -->
Das hier wird eine Alternative zum ft-Hubgetriebe, das bei größeren Kräften auch mal Schaden nimmt. Die hier verwendeten Teile sind auch nicht unzerstörbar, aber leichter zu ersetzen.

Der Antrieb erfolgt von der schwarzen Achse über die drei Riegelscheiben auf die Zahnstange (hier nicht im Bild), die in den Führungsplatten geführt wird. Damit zwischen Achse und Riegelscheiben eine formschlüssige Kraftübertragung erfolgt, ist die Achse auf ca. 5 mm abgeflacht worden (aber erst mit einem Abstand von ca. 2 mm vom Clips, damit sie nachher noch "rund" läuft).

Das weiße Röhrchen ist der Mitnehmer zwischen Achse und Zahnrädern. Hier kann man alles einsetzen, was 2 mm Durchmesser hat.

Auf das Teile-Modding kann man auch verzichten, aber dann rutscht der Antrieb irgendwann durch.
