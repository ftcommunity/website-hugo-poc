---
layout: "image"
title: "32455-Lenk03.JPG"
date: "2006-08-29T21:14:58"
picture: "32455-Lenk03.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6757
- /detailsbec7-2.html
imported:
- "2019"
_4images_image_id: "6757"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6757 -->
