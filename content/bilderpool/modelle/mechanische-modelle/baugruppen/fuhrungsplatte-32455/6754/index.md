---
layout: "image"
title: "32455-Hub04.JPG"
date: "2006-08-29T21:12:12"
picture: "32455-Hub04.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6754
- /details700f.html
imported:
- "2019"
_4images_image_id: "6754"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:12:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6754 -->
Jetzt kann die Passfeder (hier: das weiße Röhrchen) eingefädelt werden. Dazu die ft-Achse langsam in den Zahnradblock einschieben.
