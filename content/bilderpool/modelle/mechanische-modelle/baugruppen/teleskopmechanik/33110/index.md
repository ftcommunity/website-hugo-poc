---
layout: "image"
title: "Teleskop"
date: "2011-10-05T21:35:46"
picture: "teleskopmechanik1.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/33110
- /detailscb73.html
imported:
- "2019"
_4images_image_id: "33110"
_4images_cat_id: "2439"
_4images_user_id: "9"
_4images_image_date: "2011-10-05T21:35:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33110 -->
Das linke Alu-Stück kann man auf dem rechten hin- und herschieben. Oder das rechte auf dem linken. Auf der Rückseite sind keine weiteren Teile. Das Gleitlager hält also nur mit Vorspannung und möchte gern "nach oben gedrückt" werden.

Wie das Teil heißt, das in die Nut des Aluprofils greift, weiß ich nicht. Der Abstand stimmt jedenfalls, wenn man den Baustein 7.5 mit einer Bauplatte 15x15 unterfüttert. Auf der Rückseite verbindet dann eine Bauplatte 15x30x3,75 die beiden Bausteine 7,5 über zwei Federnocken. Der Federnocken in der Bildmitte ist übrigens unnötig. Es kann sein, dass das vordere Aluprofil sich aus dem BS 7,5 herausdrücken möchte. Dann kann man auf der Unterseite noch ein bisschen Reibung erzeugen, indem man die beiden Teile nochmal zusätzlich verbindet.