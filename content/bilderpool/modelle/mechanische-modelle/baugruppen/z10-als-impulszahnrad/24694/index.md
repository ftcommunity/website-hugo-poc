---
layout: "image"
title: "Lange schmale Variante (1)"
date: "2009-08-03T22:51:43"
picture: "zalsimpulszahnrad1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24694
- /detailsdaf0.html
imported:
- "2019"
_4images_image_id: "24694"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24694 -->
Wenn das normale Impulszahnrad nicht hoch genug auflöst, kann man das Auslenken der Zähne eines Z10 mit einem Hebel so verstärken, dass es einen Taster betätigen kann. Schlüssel dieser Vorschläge ist, den Statikadapter 35975 als Hebel zu verwenden, in dem eine Seite in eine Strebe eingeklinkt und die andere mit einem Klemmring dicker gemacht wird. Obendrauf kann man dann eine Hebelverlängerung anbringen.
