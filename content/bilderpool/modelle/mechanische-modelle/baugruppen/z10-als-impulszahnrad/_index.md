---
layout: "overview"
title: "Z10 als Impulszahnrad"
date: 2020-02-22T08:14:06+01:00
legacy_id:
- /php/categories/1696
- /categories4b68.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1696 --> 
Wenn das normale Impulszahnrad nicht hoch genug auflöst, kann man das Auslenken der Zähne eines Z10 mit einem Hebel so verstärken, dass es einen Taster betätigen kann.