---
layout: "image"
title: "Kurze dicke Variante (2)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24698
- /details0e43.html
imported:
- "2019"
_4images_image_id: "24698"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24698 -->
Bei beiden Varianten sind die Strebe und das Z10 völlig im Raster angebracht.
