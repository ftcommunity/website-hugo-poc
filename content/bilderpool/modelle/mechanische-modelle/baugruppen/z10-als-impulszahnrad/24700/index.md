---
layout: "image"
title: "Kurze dicke Variante (4)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24700
- /detailsab62-2.html
imported:
- "2019"
_4images_image_id: "24700"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24700 -->
Blick von oben auf die Geometrie.
