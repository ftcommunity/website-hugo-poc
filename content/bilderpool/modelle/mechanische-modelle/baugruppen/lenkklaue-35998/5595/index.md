---
layout: "image"
title: "35998-06.JPG"
date: "2006-01-16T18:05:31"
picture: "35998-06.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5595
- /detailsfbe7.html
imported:
- "2019"
_4images_image_id: "5595"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:05:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5595 -->
Durch Ziehen an der Kette kann man das Gelenk bewegen. Das könnte mal eine Frachtluke werden.

Die neuere Ausführung des Lenkwürfels hat innen drin Versteifungsrippen, die man ein ganz kleines bisschen (ganze 0,5 mm) kappen muss, damit die Kette hindurchpasst.
