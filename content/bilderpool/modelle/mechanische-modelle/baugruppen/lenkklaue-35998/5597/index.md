---
layout: "image"
title: "Container-A01.JPG"
date: "2006-01-16T18:10:29"
picture: "Container-A01.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5597
- /detailsc365.html
imported:
- "2019"
_4images_image_id: "5597"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:10:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5597 -->
Die Winkellasche 35738 ( = alte Ausführung von 31670) hat genau die richtige Spitze, um sich mit leichtem Druck in die Lenkklaue einzufädeln.
