---
layout: "image"
title: "35998-04.JPG"
date: "2006-01-16T18:01:00"
picture: "35998-04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5593
- /details71ce.html
imported:
- "2019"
_4images_image_id: "5593"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:01:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5593 -->
Mit einer Klemmbuchse 10 ergibt die Lenkklaue eine Umlenkmöglichkeit für die "alten" Ketten (diejenigen ohne die seitlichen Noppen am Kettenglied).

Dieses Förderband kann durch seitliches Anstückeln beliebig breit gebaut werden.
