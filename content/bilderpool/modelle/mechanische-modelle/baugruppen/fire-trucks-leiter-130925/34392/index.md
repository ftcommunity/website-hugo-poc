---
layout: "image"
title: "zwei Leitern als ausfahrbare Rampe"
date: "2012-02-24T18:19:39"
picture: "DSCN4612.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34392
- /details0b09-2.html
imported:
- "2019"
_4images_image_id: "34392"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-24T18:19:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34392 -->
Auf der Innenseite läuft eine Kette an der Leiter entlang. Die Zahnstangen werden an nur 3 Punkten mit Winkelsteinen gehalten.
