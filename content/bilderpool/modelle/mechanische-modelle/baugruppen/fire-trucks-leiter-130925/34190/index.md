---
layout: "image"
title: "Leiter mit Getriebe"
date: "2012-02-16T20:38:33"
picture: "DSCN4578.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34190
- /details6e94.html
imported:
- "2019"
_4images_image_id: "34190"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-16T20:38:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34190 -->
Die BS15 mit Loch werden ebenfalls mit Klemmhülsen und Verbinder 15 gehalten.
Zwei Kegelzahnräder mit Rastachse (versehen mit je einer Hüse 15)  und eine
Rastkupplung passen genau durch die Leiter hindurch.
