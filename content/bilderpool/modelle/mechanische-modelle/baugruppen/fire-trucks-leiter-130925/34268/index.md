---
layout: "image"
title: "Leiter mit Getriebe V1.2"
date: "2012-02-19T13:45:05"
picture: "DSCN4592.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34268
- /details59c1-2.html
imported:
- "2019"
_4images_image_id: "34268"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-19T13:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34268 -->
Die Halterung passt nicht genau ins Raster deswegen dieser Aufbau.
