---
layout: "image"
title: "Befestigung"
date: "2012-02-15T14:35:41"
picture: "DSCN4563.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34188
- /detailsc3fd-2.html
imported:
- "2019"
_4images_image_id: "34188"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-15T14:35:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34188 -->
Die Befestigung ist genau so wie bei den Bildern vorher.
