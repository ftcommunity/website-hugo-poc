---
layout: "image"
title: "Leiter_32455.jpg"
date: "2012-03-17T14:42:15"
picture: "Leiter_32455.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34655
- /detailsb676-2.html
imported:
- "2019"
_4images_image_id: "34655"
_4images_cat_id: "2529"
_4images_user_id: "4"
_4images_image_date: "2012-03-17T14:42:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34655 -->
... und mit der Führungsplatte für E-Magnet (32455) geht auch noch was.
