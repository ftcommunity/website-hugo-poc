---
layout: "image"
title: "5Eck_6030"
date: "2011-09-05T20:20:16"
picture: "IMG_6030m.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31754
- /details34c1.html
imported:
- "2019"
_4images_image_id: "31754"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T20:20:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31754 -->
Dieses Fünfeck hat schwenkbare Arme und rotiert auf einem Rastdifferenzial. Letzteres kann man fest stehend montieren, wenn man die Innereien durch einen Stirnzapfen 31942 ersetzt (auf dem Bild links aber von der "falschen Seite" her eingebaut). Gegenüber helfen ein Rad 14 (36573) und eine Riegelscheibe im Differenzial, um die nach oben heraus führende Achse stabil zu führen.
