---
layout: "image"
title: "5Eck_B_5716m"
date: "2011-09-05T20:08:54"
picture: "5eck-B_5716m.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31751
- /details8a8e.html
imported:
- "2019"
_4images_image_id: "31751"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T20:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31751 -->
Der Zusammenbau gestaltet sich als 3D-Puzzle mit (je nach Charakter) meditativer oder den Adrenalinpegel steigender Wirkung. Die beiden Seiten werden mit vormontierten BS15 auf den V-Platten 90x15 gleitend aufeinander zu geschoben, bis man einen BS7,5 zwischen die gegenüber liegenden Steine einfügen kann.
