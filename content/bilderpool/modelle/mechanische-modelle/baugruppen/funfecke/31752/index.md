---
layout: "image"
title: "5Eck-C-D_2718m"
date: "2011-09-05T20:10:44"
picture: "5eck-C-D_2718m.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31752
- /details86ec.html
imported:
- "2019"
_4images_image_id: "31752"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T20:10:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31752 -->
Fünfecke gibt es auch in "klein", mit den Spurkränzen oder den Moosgummireifen 23 als Füllung
