---
layout: "image"
title: "Antrieb1601.JPG"
date: "2013-10-19T20:19:38"
picture: "PC160001.JPG"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37742
- /details90f4-2.html
imported:
- "2019"
_4images_image_id: "37742"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T20:19:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37742 -->
Noch zwei Varianten, um mit dem kleinen Schneckengetriebe ein Z44 anzutreiben. Die Achse liegt nicht im Raster ... *noch* nicht, denn das ist manchmal nur eine Frage des Bezugspunktes --> nächstes Bild.
