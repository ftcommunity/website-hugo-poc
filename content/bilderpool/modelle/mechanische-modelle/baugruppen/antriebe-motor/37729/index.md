---
layout: "image"
title: "Antrieb8869.JPG"
date: "2013-10-19T16:44:03"
picture: "IMG_8869.JPG"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37729
- /details5ebd-2.html
imported:
- "2019"
_4images_image_id: "37729"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:44:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37729 -->
Nichts besonderes, und wurde bestimmt schon von anderen erfunden. Aber alles schön im Raster.
