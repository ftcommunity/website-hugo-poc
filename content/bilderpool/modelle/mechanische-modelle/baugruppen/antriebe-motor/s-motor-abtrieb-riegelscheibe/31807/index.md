---
layout: "image"
title: "Von unten"
date: "2011-09-17T16:48:52"
picture: "smotorabtriebmitriegelscheibe2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31807
- /details72e7.html
imported:
- "2019"
_4images_image_id: "31807"
_4images_cat_id: "2374"
_4images_user_id: "104"
_4images_image_date: "2011-09-17T16:48:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31807 -->
Der zweite Baustein 15 auf der hier linken Seite ist für die Anordnung unerheblich, er war nur noch von dem Prototypen-Modell übrig.
