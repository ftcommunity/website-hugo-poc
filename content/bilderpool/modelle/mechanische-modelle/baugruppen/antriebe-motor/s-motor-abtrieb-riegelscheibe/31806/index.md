---
layout: "image"
title: "Von oben"
date: "2011-09-17T16:48:52"
picture: "smotorabtriebmitriegelscheibe1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31806
- /details3282.html
imported:
- "2019"
_4images_image_id: "31806"
_4images_cat_id: "2374"
_4images_user_id: "104"
_4images_image_date: "2011-09-17T16:48:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31806 -->
Diese Anordnung ist im ft-Raster und passt genau. Die Riegelscheibe reicht unten bis in die Nut des darunter liegenden Bausteins 30. Bei einer schweren Schwungscheibe etwa, die auf der Achse montiert wird, dreht sie auch schön durch und bewirkt ein nicht ruckartiges Anfahren und Auslafuen.
