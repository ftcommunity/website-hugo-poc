---
layout: "image"
title: "Aufbau"
date: "2011-09-17T16:48:52"
picture: "smotorabtriebmitriegelscheibe4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/31809
- /details6090-2.html
imported:
- "2019"
_4images_image_id: "31809"
_4images_cat_id: "2374"
_4images_user_id: "104"
_4images_image_date: "2011-09-17T16:48:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31809 -->
Hier sieht man nur noch einen vorher immer verdeckten Verbinder 15.
