---
layout: "image"
title: "IMG_0043.JPG"
date: "2013-10-19T16:33:28"
picture: "IMG_0043.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37725
- /detailsba9e-2.html
imported:
- "2019"
_4images_image_id: "37725"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:33:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37725 -->
Drei Schnecken in Reihe (2 große, 1 kleine) -- sowas braucht man für ganz langsame Antriebe. Vorne drauf und hier verborgen sitzt ein Rast-Z10.
