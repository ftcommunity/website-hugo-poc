---
layout: "image"
title: "Antrieb0017b.JPG"
date: "2013-10-19T17:15:34"
picture: "IMG_0018.JPG"
weight: "29"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37736
- /details887b.html
imported:
- "2019"
_4images_image_id: "37736"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:15:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37736 -->
Die Rückseite. Die zwei BS5 und die kleine V-Platte halten den Drehschieber 31070 an seinem Platz.
