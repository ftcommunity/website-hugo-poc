---
layout: "image"
title: "Antrieb9524.JPG"
date: "2013-10-19T17:37:36"
picture: "IMG_9524.JPG"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37741
- /detailscaba.html
imported:
- "2019"
_4images_image_id: "37741"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:37:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37741 -->
Hier wurden die richtigen Abstände nur mit einigen Verrenkungen bewerkstelligt und mittels Strebe 30 fixiert.
