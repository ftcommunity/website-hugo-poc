---
layout: "comment"
hidden: true
title: "10633"
date: "2010-01-28T08:03:57"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Sieht wirklich schick aus! Aber große Kräft (z.B. in einem großen Kran) kann man da kaum übertragen. Das Ding wird in alle Richtungen auseinander gehen... Das alte FT-Problem... ;o)

Gruß, Thomas