---
layout: "image"
title: "IMG_2110c.jpg"
date: "2010-01-27T19:15:41"
picture: "IMG_2111b.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26170
- /details4252.html
imported:
- "2019"
_4images_image_id: "26170"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:15:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26170 -->
Hier die Einzelteile.
