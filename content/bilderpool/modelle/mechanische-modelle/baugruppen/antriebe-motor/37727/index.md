---
layout: "image"
title: "Antrieb8860.jpg"
date: "2013-10-19T16:40:17"
picture: "IMG_8860mit.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37727
- /detailsfbad.html
imported:
- "2019"
_4images_image_id: "37727"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:40:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37727 -->
Ein sehr langsamer Antrieb mit dem Rast-Z20.
