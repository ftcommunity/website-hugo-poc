---
layout: "image"
title: "Antrieb2098b.jpg"
date: "2010-01-27T18:58:18"
picture: "IMG_2098b.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["31069"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26159
- /details65fe.html
imported:
- "2019"
_4images_image_id: "26159"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T18:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26159 -->
Noch ein Zahnradantrieb, diesmal mit Z44.
