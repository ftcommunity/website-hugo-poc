---
layout: "image"
title: "Jalousie-Aufzug"
date: "2016-02-21T23:03:44"
picture: "jalousie1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/42928
- /detailsd224.html
imported:
- "2019"
_4images_image_id: "42928"
_4images_cat_id: "3191"
_4images_user_id: "558"
_4images_image_date: "2016-02-21T23:03:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42928 -->
Der Powermotor wickelt das Fischertechnikseil auf, bis der Knoten den Taster betätigt.