---
layout: "image"
title: "Seilantrieb"
date: "2008-10-25T21:35:41"
picture: "Seilantrieb-Seite.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/16073
- /details2d1a-2.html
imported:
- "2019"
_4images_image_id: "16073"
_4images_cat_id: "2220"
_4images_user_id: "59"
_4images_image_date: "2008-10-25T21:35:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16073 -->
Aufgrund der zuweilen hohen Zugkräfte in Seilantrieben (z. B. Seilbahn) sollten stets
Metallachsen verwendet werden. Falls das hier gezeigte Alu-Profil durch Bausteine
ersetzt wird, sollten diese durch seitlich eingeschobene Metallachsen verstärkt wer-
den. Eine durchgehende Achse für die zwei kleinen Umlenkrollen paßt hier nicht. Bei
entsprechender Belastung müssen diese Rolenlager zusätzlich stabilisiert werden.
