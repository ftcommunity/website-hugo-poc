---
layout: "image"
title: "31998-01.JPG"
date: "2006-04-12T20:24:03"
picture: "31998-01.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6076
- /details647d.html
imported:
- "2019"
_4images_image_id: "6076"
_4images_cat_id: "523"
_4images_user_id: "4"
_4images_image_date: "2006-04-12T20:24:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6076 -->
Die Rasthülse und die Kurbel stoßen bei jeder Umdrehung leicht an, aber das ließe sich ja noch ändern.
