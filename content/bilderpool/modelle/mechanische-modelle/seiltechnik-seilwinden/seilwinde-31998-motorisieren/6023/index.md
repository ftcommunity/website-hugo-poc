---
layout: "image"
title: "Seilwinde 31998 1"
date: "2006-04-06T21:54:32"
picture: "Seilwinde_31998_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/6023
- /details8b94.html
imported:
- "2019"
_4images_image_id: "6023"
_4images_cat_id: "523"
_4images_user_id: "328"
_4images_image_date: "2006-04-06T21:54:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6023 -->
Da ich einige der Standard-Seilwinden 31998 mit Kurbel besitze (sind in jedem Kran-Kasten drin...), aber nicht die Seiltrommel für Rastachsen 35070, habe ich versucht, die Seilwinde 31998 möglichst kompakt und elegant zu motorisieren. Ich denke, die Lösung kann sich sehen lassen: Alles sehr stabil und vollständig im FT-Raster.