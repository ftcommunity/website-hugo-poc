---
layout: "image"
title: "06-Rohsignal"
date: "2006-02-11T14:18:07"
picture: "06_-Rohsignal.jpg"
weight: "6"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5753
- /details6a99.html
imported:
- "2019"
_4images_image_id: "5753"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:18:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5753 -->
Sehr unbefriedigend, wenn das Signal, so wie es vom Sensor kommt, aufgezeichnet wird. Der AD-Wandler liefert eine 12-bit Auflösung im Eingabebereic +/-5V. Bei genauerem Hinsehen ist eine gewisse Struktur in den Daten nicht zu leugnen.
