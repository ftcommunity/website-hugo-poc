---
layout: "image"
title: "09-Targetdrehung"
date: "2006-02-11T14:53:11"
picture: "09-Targetdrehung.jpg"
weight: "9"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5756
- /detailsdb98.html
imported:
- "2019"
_4images_image_id: "5756"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:53:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5756 -->
Der Einfluß des Winkels auf das Meßsignal ist erstaunlich gering. Die Eignung der Sensoren für kleine Roboter sollte damit ausreichend bewiesen sein.
