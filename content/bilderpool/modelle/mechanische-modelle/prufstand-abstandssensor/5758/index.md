---
layout: "image"
title: "10-Signal GP2D120"
date: "2006-02-11T16:06:28"
picture: "10-Signal_GP2D120.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5758
- /detailsa04d.html
imported:
- "2019"
_4images_image_id: "5758"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T16:06:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5758 -->
Der Sensor für die kurze Reichweite ist bis 30 cm spezifiziert, seine Reichweite scheint aber 40 cm noch zuzulassen.
