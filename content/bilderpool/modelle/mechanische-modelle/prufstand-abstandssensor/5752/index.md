---
layout: "image"
title: "05-Kontrolle des Fahrwegs"
date: "2006-02-11T14:18:07"
picture: "05-Kontrolle_des_Fahrwegs.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5752
- /details294a.html
imported:
- "2019"
_4images_image_id: "5752"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:18:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5752 -->
Mit dieser Marke kann nachgesehen werden, ob der Wagen jeweils auf die korrekte Position fährt. Trotz des sehr einfachen Aufbaus ist die Positionsgenauigkeit stets besser 1 mm. Die gespannte Kette hat sich um 1,13% gegenüber des Fischertechnik-Nennmaßes gedehnt.
