---
layout: "image"
title: "08-Korrelation"
date: "2006-02-11T14:53:11"
picture: "08-Korrelation.jpg"
weight: "8"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/5755
- /detailsb651.html
imported:
- "2019"
_4images_image_id: "5755"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:53:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5755 -->
Im Meßbereich 100 bis 600 mm zeigt das Signal des Sensors GP2D12 eine praktisch perfekte Korrelation mit der einfachen Ausgleichsfunktion. Damit ist eine Entfernungsauflösung im gesamten Bereich mit 5 mm möglich.
