---
layout: "image"
title: "Kompasswagen vom Typ Lanchester"
date: "2014-05-05T23:06:56"
picture: "Lancaster2c.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Kompasswagen", "Differenzialgetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38748
- /detailsae85.html
imported:
- "2019"
_4images_image_id: "38748"
_4images_cat_id: "2896"
_4images_user_id: "1088"
_4images_image_date: "2014-05-05T23:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38748 -->
An dieser klassischen Variante eines Kompasswagens wird die Funktion möglichst klar. Das verwendete Differentialgetriebe findet sich in der Kategorie Getriebe.
