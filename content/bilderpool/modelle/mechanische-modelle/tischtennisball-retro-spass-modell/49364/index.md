---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-10T13:49:45+01:00
picture: "2020-04-12_Tischtennisball-Retro-Spass-Modell2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man braucht ein paar Kurvenscheiben, aber die älteren ft-Raupenbänder sind auch durch Haushaltsgummis ersetzbar. Ansonsten ist das Modell auch mit heutigen Teilen leicht zu bauen.