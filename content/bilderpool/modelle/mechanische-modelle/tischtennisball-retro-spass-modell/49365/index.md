---
layout: "image"
title: "Original und Nachbau"
date: 2022-01-10T13:49:46+01:00
picture: "2020-04-12_Tischtennisball-Retro-Spass-Modell1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im 1972er Buch "Kleine Erfinder - große Ideen", genauer gesagt im Modellbau-Anhang auf Seite 14, sieht man dieses Modell, dass mich zum Nachbau reizte.

Ein Video gibt's unter https://youtu.be/yWM2kW2wgg8