---
layout: "image"
title: "Draufsicht"
date: 2022-01-10T13:49:44+01:00
picture: "2020-04-12_Tischtennisball-Retro-Spass-Modell3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell wirbelt einen eingelegten Tischtennisball mehr oder weniger wild umher und schickt ihn ebenfalls mehr oder weniger zufällig in eine der Eckplätze.