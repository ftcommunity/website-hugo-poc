---
layout: "overview"
title: "Tischtennisball-Retro-Spaß-Modell"
date: 2022-01-10T13:49:44+01:00
---

Ein Nachbau aus dem fischertechnik-Buch "Kleine Erfinder, große Ideen" von 1972. Das Modell ist im Modellanhang auf Seite 14 beschrieben.