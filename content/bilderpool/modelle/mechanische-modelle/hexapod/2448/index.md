---
layout: "image"
title: "Seilwinde Detail"
date: "2004-05-31T19:50:20"
picture: "H2-14-Tandemseilwinde_Detail.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2448
- /detailsa010.html
imported:
- "2019"
_4images_image_id: "2448"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2448 -->
Hier gut zu erkennen ist unten die gegenseitige Drehmomentstütze der Motoren, die sonst nur auf ihren Wellen gelagert sind. Ebenso gut zu erkennen, die 1:3 Untersetzung. Auf den Winden ist Kevlar aufgelegt.