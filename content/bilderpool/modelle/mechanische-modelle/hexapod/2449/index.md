---
layout: "image"
title: "Software"
date: "2004-05-31T19:50:20"
picture: "H2-19-Software.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2449
- /details5823-2.html
imported:
- "2019"
_4images_image_id: "2449"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2449 -->
Hier die typische Bildschirmmaske, mit der sich die Software meldet. Gegenwärtig ist das Hexapod nur verschoben und nicht verdreht, wie an den drei Achsen der Orientierung zu sehen ist.