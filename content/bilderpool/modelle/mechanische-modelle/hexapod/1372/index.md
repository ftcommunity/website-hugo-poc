---
layout: "image"
title: "Hexapod6"
date: "2003-09-07T14:28:44"
picture: "Hexapod6-Das_Innere.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1372
- /details8338.html
imported:
- "2019"
_4images_image_id: "1372"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T14:28:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1372 -->
Blick in Innere. Die Plattform hängt in ca. 3 cm Höhe über dem Boden