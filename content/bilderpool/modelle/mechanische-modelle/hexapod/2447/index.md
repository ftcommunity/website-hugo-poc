---
layout: "image"
title: "Seilumlenkung2"
date: "2004-05-31T19:50:20"
picture: "H2-13-Seilumlenkung.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2447
- /detailsfbeb.html
imported:
- "2019"
_4images_image_id: "2447"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2447 -->
Hier der etwas bessere Überblick, sofern man bei dem fast nicht zu fotografierenden Rahmen überhaupt von Überblick reden kann.