---
layout: "image"
title: "Hexapod1"
date: "2003-09-07T13:47:33"
picture: "Hexapod1_-_Gesamtmodell.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1367
- /details30e4.html
imported:
- "2019"
_4images_image_id: "1367"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T13:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1367 -->
Das Gesamtmodell: Oktaedrischer Rahmen mit Brückendreieck für Seilwinden