---
layout: "image"
title: "Tandemseilwinde2"
date: "2004-05-31T19:50:20"
picture: "H2-10-Tandemseilwinde.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2453
- /details2f19-2.html
imported:
- "2019"
_4images_image_id: "2453"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2453 -->
Etwas näher. Gut zu sehen ist jetzt die Gegenläufigkeit der Seilwinden. Der Wickelumfang beträgt exakt 60 mm, so daß bei einer Schrittauflösung der Motore von 200 Schritt/Udrehung und einer Untersetzung von 1:3 der Seilvorschub genau 0,1 mm/Schritt ist.