---
layout: "image"
title: "Rahmen oberes Dreieck"
date: "2004-05-31T19:50:19"
picture: "H2-02-Rahmen.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2443
- /detailsf46f.html
imported:
- "2019"
_4images_image_id: "2443"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2443 -->
Sicht entlang einer Dreieckskante oben