---
layout: "image"
title: "Hexapod7"
date: "2003-09-07T15:24:00"
picture: "Hexapod7-Steuerrechner.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1373
- /details3070.html
imported:
- "2019"
_4images_image_id: "1373"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T15:24:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1373 -->
Der Steuerrechner mit 12-bit-Ausgang und 3-bit-Eingang. Im Hintergrund der neue Plotter.