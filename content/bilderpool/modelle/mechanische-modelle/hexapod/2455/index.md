---
layout: "image"
title: "Winkellage Seilwinden"
date: "2004-05-31T19:50:20"
picture: "H2-11-Winkellage_Seilwinden.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2455
- /detailsa986.html
imported:
- "2019"
_4images_image_id: "2455"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2455 -->
Der Vorteil dieser Anordnung ist, daß die Seile nahezu senkrecht auf die Winden laufen und daß die Seile mit dem Rahmen (beinahe) eine Winkelhalbierende bilden (wie bei Fördertürmen). Obwohl die Seilwinden recht 'schlabberig' im Rahmen hängen, sind sie in Seilrichtung total steif.