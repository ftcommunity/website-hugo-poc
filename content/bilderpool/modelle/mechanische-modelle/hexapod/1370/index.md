---
layout: "image"
title: "Hexapod4"
date: "2003-09-07T13:47:33"
picture: "Hexapod4_-_Unterer_Knoten.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1370
- /details908e.html
imported:
- "2019"
_4images_image_id: "1370"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T13:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1370 -->
Unterer Knoten, der recht merkwürdige Winkel aufweist.