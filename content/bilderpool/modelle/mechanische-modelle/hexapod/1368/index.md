---
layout: "image"
title: "Hexapod2"
date: "2003-09-07T13:47:33"
picture: "Hexapod2-Oberer_Knoten.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1368
- /detailsb11c.html
imported:
- "2019"
_4images_image_id: "1368"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T13:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1368 -->
Oberer Knoten  des Oktaeders