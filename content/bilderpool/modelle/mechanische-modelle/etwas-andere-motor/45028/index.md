---
layout: "image"
title: "Der etwas andere Motor gerade gerückt"
date: "2017-01-13T13:00:50"
picture: "gerade_gerckt.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45028
- /details6711.html
imported:
- "2019"
_4images_image_id: "45028"
_4images_cat_id: "3352"
_4images_user_id: "2635"
_4images_image_date: "2017-01-13T13:00:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45028 -->
