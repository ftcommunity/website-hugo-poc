---
layout: "image"
title: "Zwei Abtriebswellen"
date: "2017-01-13T13:00:50"
picture: "zwei_Abtriebswellen.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45027
- /detailsb719.html
imported:
- "2019"
_4images_image_id: "45027"
_4images_cat_id: "3352"
_4images_user_id: "2635"
_4images_image_date: "2017-01-13T13:00:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45027 -->
Der Motor schafft auch 2 Abtriebswellen. Bei dreien gibt es chaotisches Verhalten: Mal geht es kurze Zeit &#8230; oder auch nicht.