---
layout: "image"
title: "Drehtisch Detail oben"
date: "2007-02-11T12:23:26"
picture: "drehtisch4.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/8929
- /details9339.html
imported:
- "2019"
_4images_image_id: "8929"
_4images_cat_id: "810"
_4images_user_id: "1"
_4images_image_date: "2007-02-11T12:23:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8929 -->
