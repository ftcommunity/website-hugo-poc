---
layout: "image"
title: "Drehtisch unten"
date: "2007-02-11T12:23:25"
picture: "drehtisch1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/8926
- /details3a56.html
imported:
- "2019"
_4images_image_id: "8926"
_4images_cat_id: "810"
_4images_user_id: "1"
_4images_image_date: "2007-02-11T12:23:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8926 -->
Ich habe mal einen Anfang für einen Drehtisch (6 Stationen)  für eine Industriemaschine gemacht.
Grundlage bildet ein ft Malteserkreuz (leider eine Sonderteil was es so nie gegeben hat).
Der Rundtisch hat derzeit eine Spannweite von ca. 33cm.
Theoretisch könnte man diese sogar noch durch längere Alu's erweitern.

Gruß
Sven
