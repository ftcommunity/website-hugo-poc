---
layout: "image"
title: "FB12_03.JPG"
date: "2006-03-12T16:09:07"
picture: "FB12_03.jpg"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5881
- /detailsea22.html
imported:
- "2019"
_4images_image_id: "5881"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T16:09:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5881 -->
