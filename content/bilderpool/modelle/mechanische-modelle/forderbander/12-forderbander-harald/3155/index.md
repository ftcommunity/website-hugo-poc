---
layout: "image"
title: "FoeBa3_01.JPG"
date: "2004-11-15T17:29:03"
picture: "FoeBa3_01.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3155
- /details8f93-2.html
imported:
- "2019"
_4images_image_id: "3155"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3155 -->
Dieses Stück (wie die anderen Förderbänder auch) ist ein Entwurf für einen Kartoffel- oder Rübenernter. Aus dem Projekt ist aber noch nichts greifbares geworden.
