---
layout: "image"
title: "FoeBa3_02.JPG"
date: "2004-11-15T17:29:03"
picture: "FoeBa3_02.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3156
- /details6825-2.html
imported:
- "2019"
_4images_image_id: "3156"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3156 -->
