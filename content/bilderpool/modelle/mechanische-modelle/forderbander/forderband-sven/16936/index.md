---
layout: "image"
title: "Förderband"
date: "2009-01-07T15:12:39"
picture: "foerderband07_2.jpg"
weight: "20"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16936
- /details7736-2.html
imported:
- "2019"
_4images_image_id: "16936"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-07T15:12:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16936 -->
Hier sieht man wie ich das Getriebe für den Antrieb des Förderbandes fixiert habe.
Bisher rutsche das vom Motor nach ner Zeit runter.
Das passiert nun nicht mehr. Alles schön fest!
