---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband05.jpg"
weight: "5"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16908
- /detailsc3f2.html
imported:
- "2019"
_4images_image_id: "16908"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16908 -->
Als Grundplatte habe ich die kleine schwarze Platte 120x60 gewählt.
