---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband09.jpg"
weight: "9"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16912
- /details9bf8.html
imported:
- "2019"
_4images_image_id: "16912"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16912 -->
Hier sieht man den Antrieb.
Um alles schön kompakt hinzubekommen gabs keine andere Lösung als Antrieb von innen.
Es durfte ja auch nichts an der Kette stören, da diese sonst nicht laufen würde!
