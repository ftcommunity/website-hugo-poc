---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband01.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16904
- /details6db4.html
imported:
- "2019"
_4images_image_id: "16904"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16904 -->
Mein Ziel ist es ein Förderband in der Form wie das von Staudinger zu bauen.
Also klein, kompakt und auf Drehkranz drehbar.
Weiterhin wollte ich keine Spezialteile verwenden, denn 1. habe ich die nicht und 2. sollte alles mit aktuellen Standard ft Komponenten gebaut werden!
Dieses Foto zeigt das Förderband von oben.
