---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband06.jpg"
weight: "6"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/16909
- /detailsa8b1.html
imported:
- "2019"
_4images_image_id: "16909"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16909 -->
