---
layout: "overview"
title: "Förderband von DasKasperle"
date: 2020-02-22T08:13:53+01:00
legacy_id:
- /php/categories/2746
- /categoriesa16f.html
- /categoriesacf0.html
- /categories6415.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2746 --> 
Das Förderband ist für Kastaniene ausgelegt. Die Neigung des Förderbandes wird per Motor und IR Control eingestellt. Dank der alten Controler lässt sich auch die Geschindigkeit in zwei Stufen einstellen.
Mit dem Förderband beladen wir unseren Pneumatik-Kipplaster sowie unser ft-Ships & More Rhein-Frachter. Befüllt wird das Förderband von einem IR-Pneumatik Radlader mit Frontladeschaufel oder vom IR-Pneumatik Löffelbagger.

IR Pneumatik Radlader belädt ein Förderband mit Kastanien
LINK: http://www.youtube.com/watch?v=SF-P_u1B4_Q&feature=youtu.be