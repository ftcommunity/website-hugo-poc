---
layout: "image"
title: "Förderband von DasKasperle - Hochgestellt - SEITE 1 mit Deko"
date: "2013-05-26T09:50:17"
picture: "foerderbandvondaskasperle03.jpg"
weight: "3"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36939
- /details2ced.html
imported:
- "2019"
_4images_image_id: "36939"
_4images_cat_id: "2746"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36939 -->
