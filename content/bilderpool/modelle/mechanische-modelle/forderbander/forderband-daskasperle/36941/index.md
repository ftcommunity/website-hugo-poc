---
layout: "image"
title: "Förderband von DasKasperle - Hochgestellt - SEITE 3 mit Deko"
date: "2013-05-26T09:50:17"
picture: "foerderbandvondaskasperle05.jpg"
weight: "5"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36941
- /detailsa723-2.html
imported:
- "2019"
_4images_image_id: "36941"
_4images_cat_id: "2746"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36941 -->
