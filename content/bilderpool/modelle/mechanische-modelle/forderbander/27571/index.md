---
layout: "image"
title: "Transportband mit Bearbeitungsstation (neue Version)"
date: "2010-06-25T18:21:27"
picture: "SDC10352_NEW.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Ihrig"
fotografen:
- "Jürgen Ihrig"
uploadBy: "juergen669"
license: "unknown"
legacy_id:
- /php/details/27571
- /details6aa5.html
imported:
- "2019"
_4images_image_id: "27571"
_4images_cat_id: "278"
_4images_user_id: "1158"
_4images_image_date: "2010-06-25T18:21:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27571 -->
