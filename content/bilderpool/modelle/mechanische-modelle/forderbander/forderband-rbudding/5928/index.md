---
layout: "image"
title: "part3_detail"
date: "2006-03-26T14:48:45"
picture: "fischertechnik_011.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/5928
- /detailse99b.html
imported:
- "2019"
_4images_image_id: "5928"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5928 -->
