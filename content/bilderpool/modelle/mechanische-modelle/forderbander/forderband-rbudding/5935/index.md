---
layout: "image"
title: "part4-3"
date: "2006-03-26T14:53:33"
picture: "fischertechnik_029.jpg"
weight: "13"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/5935
- /detailsa6f3-2.html
imported:
- "2019"
_4images_image_id: "5935"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5935 -->
