---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-15T17:17:39"
picture: "IMG_0717.jpg"
weight: "1"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/44568
- /detailsdf58.html
imported:
- "2019"
_4images_image_id: "44568"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-15T17:17:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44568 -->
Mijn eerste werkende prototype van een 2 traps mechanische cilinder met een inbouw maat van 15 bij 15 mm