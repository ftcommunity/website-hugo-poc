---
layout: "image"
title: "Strickmaschine"
date: "2015-04-30T22:08:33"
picture: "Strickmaschine_aus_fischertechnik.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Stricken", "Strickmühle", "Strickmaschine", "Kurbeltrieb", "Handkurbel", "Planetengetriebe"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/40893
- /details20e4.html
imported:
- "2019"
_4images_image_id: "40893"
_4images_cat_id: "3072"
_4images_user_id: "381"
_4images_image_date: "2015-04-30T22:08:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40893 -->
Die handkurbelgetriebene Maschine strickt mit Hilfe einer handelsüblichen Strickmühle Bänder.
Flott gedreht strickt die Maschine ein 20cm langes Freundschaftsband in ca 20 Sekunden.
Der benötigte Zug wird durch eine Art Planetengetriebe und 2 gegenläufigen Gummiwalzen erzeugt.
