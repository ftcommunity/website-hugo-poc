---
layout: "comment"
hidden: true
title: "1136"
date: "2006-06-19T16:29:21"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Remadus,

ich weiß nicht, ob Michael hier mitliest. Ich hatte das Modell vor einiger Zeit mal nachgebaut und hier eingestellt.

Du kannst die Drehungen aber gut im zugehörigen Video, das Sven gerade freischaltet, sehen.

Gruß,
Stefan