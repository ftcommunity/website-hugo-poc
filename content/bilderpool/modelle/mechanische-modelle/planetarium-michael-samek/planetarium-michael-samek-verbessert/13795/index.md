---
layout: "image"
title: "Arm Sonne-Erde-Mond"
date: "2008-02-25T21:06:33"
picture: "4.jpg"
weight: "7"
konstrukteure: 
- "Michael Samek"
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13795
- /details58f0.html
imported:
- "2019"
_4images_image_id: "13795"
_4images_cat_id: "1263"
_4images_user_id: "731"
_4images_image_date: "2008-02-25T21:06:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13795 -->
