---
layout: "image"
title: "Planetarium verbessert"
date: "2008-02-23T17:21:53"
picture: "Planetarium.jpg"
weight: "1"
konstrukteure: 
- "Michael Samek"
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13726
- /details689d.html
imported:
- "2019"
_4images_image_id: "13726"
_4images_cat_id: "1263"
_4images_user_id: "731"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13726 -->
Dies ist die vorerst verbesserte Variante des Planetariums von Michael Samek. Außer ein paar Verstärkungen habe ich nur das Getriebe neu entworfen, das es dem Mond nun ermöglicht innerhalb 30 Tagen um die Erde zu drehen, statt zuvor 12 Tagen.
