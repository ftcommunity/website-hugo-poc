---
layout: "image"
title: "Windmill Project"
date: "2010-05-28T14:22:08"
picture: "ft_windmill_b.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27317
- /details188d.html
imported:
- "2019"
_4images_image_id: "27317"
_4images_cat_id: "1962"
_4images_user_id: "585"
_4images_image_date: "2010-05-28T14:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27317 -->
This is a second shot of a windmill driven oil derrick.