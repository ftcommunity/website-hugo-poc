---
layout: "image"
title: "Gabellichtschranke"
date: "2004-10-16T18:51:55"
picture: "04-Gabellichtschranke_mit_Viertelkreis.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Lichtschranke", "Drehzahlmessung"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2716
- /details3992.html
imported:
- "2019"
_4images_image_id: "2716"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2716 -->
Verdrehsicher angebauter Viertelkreissektor zur Messung des absoluten Ortes des Schwungrads und der aktuellen Drehzahl