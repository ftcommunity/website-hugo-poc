---
layout: "image"
title: "Meterwurf"
date: "2004-11-21T13:37:35"
picture: "Wurfreproduzierbarkeit.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3303
- /detailsddb0.html
imported:
- "2019"
_4images_image_id: "3303"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3303 -->
Bei einem Meter sind die Ergebnisse nicht schlechter. Alle Einstellungen der Maschine bleiben konstant.