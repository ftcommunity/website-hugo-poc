---
layout: "image"
title: "Ballwurfmaschine"
date: "2004-10-16T18:51:54"
picture: "01-Gesamtmodell.jpg"
weight: "1"
konstrukteure: 
- "remadus"
fotografen:
- "remadus"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2713
- /details01d3.html
imported:
- "2019"
_4images_image_id: "2713"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2713 -->
Überblick über das Modell