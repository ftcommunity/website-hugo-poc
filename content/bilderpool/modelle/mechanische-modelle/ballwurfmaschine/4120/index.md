---
layout: "image"
title: "Wurfmaschine Typ II"
date: "2005-05-10T23:27:34"
picture: "Wurfmaschine_TypII_2.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Wurfmaschine", "Drehgeber", "Kettenantrieb", "Schleifring"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/4120
- /details2ebe-3.html
imported:
- "2019"
_4images_image_id: "4120"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4120 -->
Das ist die neukonstruierte Wurfmaschine.
Das Schwungrad wurde um 90 Grad gedreht, jetzt quer zur Bauplatte und ein neuer Drehaufnehmer mit höherer Auflösung ist drangekommen. Schwungrad, Schleifring und Drehaufnehmer sind jetzt auf einer Welle, so daß das Verdrehspiel aus dem Kettenantrieb weg ist. Insgesamt wirkt sie jetzt auch kompakter.