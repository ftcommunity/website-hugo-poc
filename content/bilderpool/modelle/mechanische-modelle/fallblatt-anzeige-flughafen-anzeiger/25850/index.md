---
layout: "image"
title: "Detail Aufhängung"
date: "2009-11-28T14:30:57"
picture: "Detail_Aufhngung.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["Split-Flap", "Display", "Anzeigetafel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/25850
- /detailsf4b8.html
imported:
- "2019"
_4images_image_id: "25850"
_4images_cat_id: "1815"
_4images_user_id: "724"
_4images_image_date: "2009-11-28T14:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25850 -->
Insgesamt 39 Löcher pro runder Plastik-Scheibe (hergestellt aus Kabelkanal-Deckeln), die an der Drehscheibe 60 befestigt ist.
Die Buchstaben sind beidseitig auf Plastik-Scheiben aufgeklebt: Oberteil alter Buchstabe (z.B. A) auf der einen Seite, und unterteil neuer Buchstabe (z.B. B) auf der anderen Seite.
Die Drähte (ganz normaler Silberdraht) wurden mit einem Feuerzeug heiß gemacht, und mit einer Spitzzange in die Plastik-Scheiben eingeschmolzen. Hält wie geklebt.

Hier gibts ein Video von der Aufhängung:
http://www.youtube.com/watch?v=7J2KhtI0l7w
