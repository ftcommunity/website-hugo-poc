---
layout: "image"
title: "Blick auf den Absenker"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27287
- /detailse9ee.html
imported:
- "2019"
_4images_image_id: "27287"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27287 -->
Der mittlere BS30 ist mit der Ziffer verbunden. Oben trägt er quer einen Verbinder 45. Die anderen schwarzen Bausteine und die rote Verbindung oben gehören zum Absenker selbst. Alle anderen Ziffern werden ebenfalls wie hier von einem schwarzen BS30 und einem Verbinder 45 getragen.
