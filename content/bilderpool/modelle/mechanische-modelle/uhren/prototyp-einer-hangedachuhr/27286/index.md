---
layout: "image"
title: "Blick in die Mechanik bei abgesenkter Ziffer"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27286
- /details9bc3.html
imported:
- "2019"
_4images_image_id: "27286"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27286 -->
Man sieht die anderen Ziffern eingehängt, während eine gerade abgesenkt ist.
