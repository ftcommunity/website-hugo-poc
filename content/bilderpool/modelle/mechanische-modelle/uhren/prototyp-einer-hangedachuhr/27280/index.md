---
layout: "image"
title: "Vorderansicht"
date: "2010-05-17T21:02:53"
picture: "prototypeinerhaengedachuhr1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27280
- /details220c-3.html
imported:
- "2019"
_4images_image_id: "27280"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27280 -->
Dies ist ein Prototyp einer einzelnen Ziffer einer Uhr, die ich vor der aktuellen Digitaluhr angefangen hatte. Da mir die Teile ausgehen, wollte ich sie vor der Zerlegung noch dokumentieren.

Die Idee ist, vier solcher Baugruppen zu haben, für jede Ziffer von HH:MM eine. Die Ziffern sollen voll ausgeformt aus fischertechnik-Bausteinen hergestellt sein. Man sieht hier eine "9" im Vordergrund. Alle Ziffern 0 - 9 sind hintereinander aufgehängt. Diese Gruppe kann vor und zurück bewegt werden, und eine - die gerade benötigte Ziffer nämlich - wird so weit abgesenkt, dass sie einzeln und somit lesbar unten heraus hängt.

Ursprünglich wollte ich solche Ziffern verwenden: http://www.ftcommunity.de/categories.php?cat_id=1785 Das scheitert aber ganz klar an meinem Bauteilebestand. Hier sind deshalb einfache aus grauen BS30 verwendet.
