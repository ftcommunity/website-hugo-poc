---
layout: "image"
title: "Rastketten-Z10 der Einerstelle"
date: 2022-01-08T16:59:21+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik48.jpg"
weight: "48"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man wohl gut, wie Rastketten-Z10, Z20, Rückholfeder und Drehscheibe zusammenhängen. Nur das Z10 und das linke Z20 sind auf den Achsen festgeschraubt. Das rechte Z10 und die Drehscheibe sitzen beide auf Freilaufnaben und sind durch die K-Achse der Rückholfeder gekoppelt.