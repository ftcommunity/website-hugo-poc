---
layout: "image"
title: "Schnelllaufmotor (2)"
date: 2022-01-08T17:00:03+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese Baugruppe kann auf die Uhr gesteckt werden, wenn man sie zu Entwicklung oder Tests schnell laufen lassen will.