---
layout: "image"
title: "Frontverkleidung abgenommen"
date: 2022-01-08T16:59:27+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik43.jpg"
weight: "43"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Frontplatten lassen sich recht leicht abnehmen. Das ist nicht nur wichtig zum besseren Zeigen der Mechanik, sondern auch um beim Tüfteln, Justieren und Fluchen besser an alles dranzukommen.