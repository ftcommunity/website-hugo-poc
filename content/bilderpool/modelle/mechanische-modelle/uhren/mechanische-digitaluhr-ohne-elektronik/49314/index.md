---
layout: "image"
title: "Rastketten-Z10 der Zehnerstelle"
date: 2022-01-08T16:59:14+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik53.jpg"
weight: "53"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Z10 dreht sich also jede Minute um 1/10 Umdrehung, sodass nach 10 Minuten die Rückholfeder der Zehnerstelle voll aufgezogen ist, freigegeben wird und so die Zehnerstelle um eine Ziffer weiter schaltet.