---
layout: "image"
title: "Übertrag zur Zehnerstelle der Minuten (2)"
date: 2022-01-08T16:59:41+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Genau alle 10 Minuten muss die Zehnerstelle der Minuten ebenfalls um eine Ziffer (0 - 5) weitergeschaltet werden. Dafür brauchen wir eine Untersetzung wie folgt:

Das Z20 der Achse der Einerstelle wird jede Minute um 7 Zähne, also um genau eine 7/20 Umdrehung weitergedreht. Nach 10 Umdrehungen, wenn wieder die 0 kommt, muss die Antriebsachse des Zehnerstellen-Getriebes mit seinem Rastketten-Z10 eine Umdrehung vollführt haben, damit die Zehnerstelle um eine Ziffer weiterrutscht.

Wir brauchen also eine Untersetzung von 20:70 = 2:7. Das klappte platzsparend durch zwei Selbstbauzahnräder:

- links unten im Bild sieht man die Kombination aus 31717 Schnecken-Spannzange m1,5 und aufgeschraubter 31711 Schnecken-Kontermutter m1,5. Um die Kontermutter passten stramm genau 12 Kettenglieder. Damit haben wir das Z12 gebastelt, das im vorigen Bild schon erwähnt wurde.

- Nach einem Zwischen-Z15 (weil zwei Ketten-Selbstbauzahnräder ja nicht direkt miteinander kämmen können) folgt ein schon von Triceratops' Trickfibel bekanntes Z21, bestehend aus Nabe, kleinem Reifen und darum gespannten 21 Kettengliedern. Danke an Triceratops für den genialen Einfall von damals.

Zusammen mit der Z10-Z20-Kette haben wir damit 10:20 * 12:15 * 15:21 = 2:7. Voilà.