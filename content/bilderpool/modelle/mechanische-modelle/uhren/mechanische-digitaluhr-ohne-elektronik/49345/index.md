---
layout: "image"
title: "Erste Schrittschalt-Mechanik, erster Übertrag"
date: 2022-01-08T16:59:52+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Z40 in Bildmitte triebt ein erstes Z10 an, dass mit 7 bzw. 8 (je nach Konstruktionssituation) schwarzen Rastkettengliedern und dem Rest in normalen nicht-Rast-Kettengliedern bestückt ist. Diese Kombination treibt das Z20 obendrüber an. Das bewirkt nun, dass das Z20 eine Weile lang gedreht, plötzlich aber freigegeben wird. Die gerade noch neben dem Z20 sichtbare fischertechnik-Antriebsfeder (aus den frühen ft-Zeiten) ist hier als laaange Zugfeder eingesetzt. Die bewirkt, dass das Z20, sobald es vom Z10 freigeben wurde, zurückschnalzt. Das Z20 wird also langsam (in 45 - 50 Sekunden) ein Stückweit gedreht und dann schlagartig zurückgedreht.

Das Z20 sitzt auf einer Freilaufnabe, ist also auf der Achse, auf der es sitzt, völlig frei drehbar. Den Sinn davon besprechen wir später bei den Bildern mit abgenommener Frontverkleidung - da sieht man das besser.