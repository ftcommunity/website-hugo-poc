---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-08T17:00:12+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Geheimnisse verbergen sich hinter der Frontverkleidung. Die Uhr läuft bis auf 7 Ticks pro Minute und einem mechanischen Schaltvorgang jede Minute völlig geräuschlos.

Ein Video gibt's unter https://youtu.be/lx6FMS0xJ1c