---
layout: "image"
title: "Schnellantrieb"
date: 2022-01-08T17:00:02+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die Kette, die vom Z10 des Schwungrades kommt, ausgeklinkt. Stattdessen ist der Schnelllauf-Motor von oben eingesteckt. Seine Anbauteile sind so, dass man einfach bis zum Anschlag schieben kann und der Motor sitzt, wo er muss.