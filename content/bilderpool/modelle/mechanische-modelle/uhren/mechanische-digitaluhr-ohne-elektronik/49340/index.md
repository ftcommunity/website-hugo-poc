---
layout: "image"
title: "Rechte Seite"
date: 2022-01-08T16:59:46+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Teilweise noch verdeckt, aber dahinter gibt's Getriebe ohne Ende.

Der bildet die Basis für viele ft-Federn, die die Uhr gedämpft halten. Damit kommt kein bisschen Erschütterung auf den Tisch, weshalb die Uhr fast immer praktisch lautlos läuft.