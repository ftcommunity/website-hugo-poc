---
layout: "image"
title: "Sperrklinke der Stunden"
date: 2022-01-08T16:59:08+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik58.jpg"
weight: "58"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Stunden erforderten wieder andere Gewichtsverhältnisse an der Sperrklinke.