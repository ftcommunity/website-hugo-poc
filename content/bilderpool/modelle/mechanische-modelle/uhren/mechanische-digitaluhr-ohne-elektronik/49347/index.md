---
layout: "image"
title: "Getriebefortsetzung in der Uhr"
date: 2022-01-08T16:59:54+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der herausnehmbare Getriebeblock endet an diesem Z40 mit der letzten 32:20-Untersetzung und anschließendem Z10 auf Z40. Das letzte Z40 (hinter Z40 und Z20 in der Abbildung) dreht sich damit genau ein Mal pro Minute.

Das Z15 vor dem letzten Z40 trägt die Kette, die nach oben in Richtung Sekundenrad läuft.