---
layout: "image"
title: "Sperrklinke der Einerstelle"
date: 2022-01-08T16:59:18+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik50.jpg"
weight: "50"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Sperrklinke ist mit der K-Achse oberhalb des Z20 drehbar gelagert. Vorne sitzt ein BS15 als Gewicht, hinten sieht man die Platte, die den Endanschlag des Federgelenksteins bildet.