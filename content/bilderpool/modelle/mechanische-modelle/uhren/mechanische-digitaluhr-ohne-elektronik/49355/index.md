---
layout: "image"
title: "Schnelllaufmotor (1)"
date: 2022-01-08T17:00:04+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Außerdem nimmt die Wartungsstation auch eine kleine Baugruppe mit einem M-Motor auf. Der kann leicht anstatt des Rades als Antrieb der Uhr angebracht werden. Damit läuft die Uhr sehr schnell, was für Tests der Mechanik angenehm ist.