---
layout: "image"
title: "Wartungsstation (1)"
date: 2022-01-08T17:00:08+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Rad aus der Uhr entnommen und in eine Wartungsstation eingelegt. Damit verzieht es sich nicht, anders als wenn man es auf den Tisch legen würde (es könnte ja wegen der Achse nicht flach auf dem Tisch liegen), und es benötigt weniger Platz auf dem Arbeitstisch.