---
layout: "image"
title: "Blick von unten"
date: 2022-01-08T16:59:05+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Boden ist komplett gefedert. Da die Uhr schwer ist, sind es 16 ft-Federn. Die sitzen auf dem hier sichtbaren Rahmen und stecken oben mit Vorstuferadhaltern in den Bauplatten 500. Die Frontverkleidung hat oben genug freien Platz und stößt nicht an die Bauplatten an.

Der Effekt ist, dass beim Lauf keinerlei Geräusch auf die Tischplatte kommt und die als Resonanzkörper missbrauchen könnte. Stille ist, solange es nicht kurz "Tick" macht, wenn ein Federgelenkstein in den nächsten Zahn eines Z20 einrastet oder wenn - dann allerdings gut hörbar - die Mechanik eine Zahl weiterschaltet.