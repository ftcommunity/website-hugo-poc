---
layout: "image"
title: "Synchronrad"
date: 2022-01-08T16:59:00+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sitzen 2 * 48 = 96 Neodym-Magnete, immer abwechselnd ausgerichtet. Wenn das Rad synchron läuft, macht es also bei 50 Hz genau 50/48 Umdrehungen pro Sekunde, also etwas schneller als eine pro Sekunde. Gestartet wird das Rad durch Drehen am roten Handrad links.