---
layout: "image"
title: "Wartungsstation (1)"
date: 2022-01-08T17:00:06+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die leere Wartungsstation. Sie enthält neben der Aufnahme fürs Schwungrad weitere.