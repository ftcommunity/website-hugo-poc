---
layout: "image"
title: "Antrieb der Einerstelle"
date: 2022-01-08T16:59:16+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik51.jpg"
weight: "51"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse in Bildmitte treibt die Einerstelle der Minuten an. An ihr wird aber auch die Rückholfeder der Stunden umgelenkt, sodass ihre Lagerung, die ja leichtgängig sein und also sauber fluchten muss, z.B. mit dem Paket von Streben verstärkt wurde.