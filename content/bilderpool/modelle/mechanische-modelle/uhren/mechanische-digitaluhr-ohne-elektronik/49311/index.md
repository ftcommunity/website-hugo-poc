---
layout: "image"
title: "Ziffern"
date: 2022-01-08T16:59:10+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik56.jpg"
weight: "56"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht, wie die Achse, die die Zehnerstelle dreht, zum Z20 mit Kette links geht. Von da geht es nach unten zum Getriebe für die Stunden.