---
layout: "overview"
title: "Mechanische Digitaluhr ohne Elektronik"
date: 2022-01-08T16:58:59+01:00
---

Eine Digitaluhr, deren einzige elektrische Komponenten ein fischertechnik-Trafo, ein Ein-/Ausschalter, ein Elektromagnet und eine LED sind. Alles andere ist pure Mechanik.