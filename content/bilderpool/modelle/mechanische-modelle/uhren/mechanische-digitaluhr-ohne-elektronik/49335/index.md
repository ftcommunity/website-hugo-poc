---
layout: "image"
title: "Übertrag zur Zehnerstelle der Minuten (3) und Aufhängung der Spannfedern (1)"
date: 2022-01-08T16:59:39+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man nochmal die Kombination aus Z12, Z15, Z21.

Die Spiralfedern werden bedarfsgerecht langgezogen, damit sie die richtige Kraft entfalten. Sie werden bei den Mechaniken um eine kleine Seilrolle (bei den Freilaufgetrieben) geführt an der schon beschriebenen K-Achse eingehängt. Dadurch ist die Weg-Änderung, die die Länge der Spannfedern bei der ca. 3/4-Umdrehung der Freilaufkupplungen mitmacht, gering gegenüber der Gesamtlänge der Feder. Das wiederum bewirkt, dass die Kraft, die die Feder ausübt, über die gesamte Rotationsstrecke fast konstant bleibt.

Leider aber nicht ganz konstant genug. Über die drei Getriebe (Einer, Zehner, Stunden) üben sie insgesamt (bei 23:59) zusammen die größte Kraft auf die Mechanik aus, und bei 00:00 die geringste. Dieser, wenn auch möglichst gering gehaltene, Unterschied reicht aber trotzdem leider aus, um die Mechanik der Einerstelle nicht sehr zuverlässig zu machen. Sie müsste immer um genau 7 Zähne schaltet, schaltet aber manchmal nur 6 und manchmal doch 8. Das ist für eine Uhr natürlich Mist, weshalb ich mit dem Modell nicht ganz zufrieden bin.

Mit Gewichten anstatt Federn ginge es vielleicht zuverlässiger, weil dann die Kraft tatsächlich konstant bleibt. Vielleicht gibt es ja mal eine Version 2 der Modellidee, ähnlich wie vor Jahren bei meiner elektropneumatischen Digitaluhr, bei der ja auch erst die zweite Version perfekt funktionierte.