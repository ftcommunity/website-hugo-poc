---
layout: "image"
title: "Frontverkleidung"
date: 2022-01-08T16:59:26+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik44.jpg"
weight: "44"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Aus diesen Elementen besteht die Frontverkleidung (die Verkleidung des gefederten Fußes unterhalb der Bauplatten 500 lässt sich auch abnehmen).

Ab jetzt kommen also Blicke von vorne in die unverkleidete Mechanik. Da sieht man dann noch ein paar Details mehr. Wem die bisherigen Bilder genügten und es nicht genauer sehen möchte, könnte jetzt auch aufhören, zu lesen. ;-)