---
layout: "image"
title: "Spannfedern und Antrieb des Sekundenrades"
date: 2022-01-08T16:59:49+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick auf die Aufhängung verschiedener Spann-Antriebsfedern. Die haben wir auch schon auf vorhergehenden Bildern wie dem der Gesamtübersicht gesehen.

Man sieht es kaum, aber einen Baustein hinter und unter dem hier sichtbaren Riegelstein sitzt das Rast-Winkelgetriebe, das das Sekundenrad antreibt.