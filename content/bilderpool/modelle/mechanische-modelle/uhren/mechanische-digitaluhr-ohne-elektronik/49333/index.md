---
layout: "image"
title: "Anzeige"
date: 2022-01-08T16:59:37+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von vorne sieht man dadurch die Einerstelle der Minuten und links daneben deren Zehnerstelle. Die Ziffern sind links- bzw. rechtsbündig aufs Papier gedruckt, damit die Minutenziffern möglichst nahe beieinander liegen.