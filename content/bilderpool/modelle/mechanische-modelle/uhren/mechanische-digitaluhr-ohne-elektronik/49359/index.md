---
layout: "image"
title: "Das erste Zahnrad"
date: 2022-01-08T17:00:09+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das etwa in Bildmitte liegende Z10 ist das erste Zahnrad der ganzen Getriebe-Orgie. Das dreht sich also mit 50/48 Umdrehungen pro Sekunde (50 Hz Netzfrequenz, abgebildet auf 48 Neodym-Magnet-Paare). Von dort aus geht es über eine locker aufgehängte Kette zur nächsten Getriebestufe.

Das Schwungrad kann man dadurch leicht komplett aus der Uhr herausnehmen. Sonst kommt man nämlich nicht an die Innereien heran (vor allem während der Konstruktion und der Tests).