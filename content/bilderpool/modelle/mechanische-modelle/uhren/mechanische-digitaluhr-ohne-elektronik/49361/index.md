---
layout: "image"
title: "Stroboskop (2)"
date: 2022-01-08T17:00:11+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So sieht das dann beim Synchronlauf aus. Man erkennt klar helle und dunkle Bereiche auf den das Licht der LED reflektierenden Neodym-Magneten. Die schwingen dann zwar leicht und langsam nach links und rechts, aber im Wesentlichen sind sie ortsfest.