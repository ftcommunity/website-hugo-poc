---
layout: "image"
title: "Untersetzungsgetriebe zur Zehnersteller"
date: 2022-01-08T16:59:15+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik52.jpg"
weight: "52"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick von vorn auf die Kombination aus Z12, Z15 und Z21.