---
layout: "image"
title: "Sekundenrad"
date: 2022-01-08T16:59:48+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vor dem Sekundenrad befindet sich an der Frontseite der Uhr eine 15x15-Öffnung, damit man die Sekunden ablesen kann. Das Sekundenrad ist nur leicht festgeschraubt, damit man es auch im laufenden Betrieb justieren kann.