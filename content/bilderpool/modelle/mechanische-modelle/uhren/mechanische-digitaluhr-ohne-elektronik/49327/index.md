---
layout: "image"
title: "Stundenmechanik"
date: 2022-01-08T16:59:30+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der Blick auf die Stundenmechanik. Sie ist genauso aufgebaut wie die bei den beiden Minutenstellen (bis auf Details der Sperrklinken, bedingt durch Platz-Einschränkungen).