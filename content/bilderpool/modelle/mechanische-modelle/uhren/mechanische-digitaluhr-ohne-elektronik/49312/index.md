---
layout: "image"
title: "Antrieb der Zehnerstelle"
date: 2022-01-08T16:59:11+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik55.jpg"
weight: "55"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse in Bildmitte geht nach rechts zum darauf festgeschraubten Z20 der Kette mit den Ziffern 0 - 5. Die komplizierte Verspannung der Feder ist nötig, damit sie lang und damit kräftig genug wird, ohne aber dem kurz davor befindlichen Schwungrad in die Quere zu kommen.