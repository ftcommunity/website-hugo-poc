---
layout: "image"
title: "Mechanik für die Stunden"
date: 2022-01-08T16:59:09+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik57.jpg"
weight: "57"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von hinten kommt das schon gezeigte Paar von Z10 (wegen der richtigen Drehrichtung), das Z21 und die schon bekannte Zähl- und Schaltmechanik, hier nur für die Stunden.