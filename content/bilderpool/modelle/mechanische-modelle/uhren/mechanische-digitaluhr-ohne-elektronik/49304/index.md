---
layout: "image"
title: "Synchronantrieb"
date: 2022-01-08T16:59:02+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kommen die 50 Hz vom Wechselspannungsausgang des Trafos über einen Ein-/Ausschalter an einem einzigen Elektromagneten an. Der ist in der Lage, das Rad, wenn es einmal synchron läuft, am Laufen zu halten. Anwerfen muss man die Maschine aber manuell.