---
layout: "image"
title: "Sekundenanzeige"
date: 2022-01-08T16:59:47+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So sieht das dann von vorne aus. Da sich das Sekundenrad kontinuierlich dreht, ziehen die Zahlen langsam hinter dem Anzeigeausschnitt vorbei.