---
layout: "image"
title: "Stroboskop (1)"
date: 2022-01-08T16:58:59+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist eine LED in Serie mit einer normalen Diode geschaltet. Das Ganze hängt parallel geschaltet am Elektromagneten. Die zusätzliche Diode soll die LED vor Spannungsspitzen des E-Magneten schützen.

Das ergibt, wohl auch wegen des Spannungsabfalls von 0,7 V an der Diode, die dazu führt, dass die LED erst später "zündet", ein für diese Zwecke sehr brauchbares Möchtegern-Stroboskop. Man kann dann beim Starten der Uhr am roten Ritzel drehen, bis das Rad etwa eine Umdrehung pro Sekunde macht. Mit dem Stroboskop kann man dann so fein beschleunigen oder auslaufen lassen, dass das Rad synchron mit den 50 Hz des Elektromagneten dreht. Diesen Zustand kann der einzelne Elektromagnet auch bei wechselnder Last in der Mechanik zuverlässig einhalten. Im Video sieht man das recht gut.