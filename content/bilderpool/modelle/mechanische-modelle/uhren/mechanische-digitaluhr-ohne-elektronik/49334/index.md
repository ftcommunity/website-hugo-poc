---
layout: "image"
title: "Aufhängung der Spannfedern (2)"
date: 2022-01-08T16:59:38+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick auf die Federaufhängung für die Zehnerstelle der Minuten, sowie auf die Übertragsmechanik davon.