---
layout: "image"
title: "Rastketten-Z10 der Einerstelle"
date: 2022-01-08T16:59:22+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik47.jpg"
weight: "47"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Z40 in Bildmitte und das Ketten-belegte Z10 links davon drehen sich genau ein Mal pro Minute.

Die Achse oberhalb ist schon die der Einerstelle der Minuten, von der die Kette hoch zum Zehnerstellen-Getriebe geht. Die Kette für das Sekundenrad ist hinter dem rechten schwarzen Statikträger gerade noch zu erkennen.