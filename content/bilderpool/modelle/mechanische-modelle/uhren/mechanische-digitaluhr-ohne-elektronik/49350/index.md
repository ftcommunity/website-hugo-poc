---
layout: "image"
title: "Übergang zum fest angebauten Getriebe"
date: 2022-01-08T16:59:58+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weiter im Text nach dem Ausflug in die Wartungsstation: Die Kette des Schwungrades kommt auf diesem Z40 an. Ziel ist eine Untersetzung so, dass wir aus den 50/48 Umdrehungen pro Sekunde genau eine Umdrehung pro Minute machen.