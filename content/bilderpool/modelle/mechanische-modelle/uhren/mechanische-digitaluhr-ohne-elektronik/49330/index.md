---
layout: "image"
title: "Überblick über die Stundenmechanik"
date: 2022-01-08T16:59:33+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die schräge Kette geht auf das zugehörige Untersetzungsgetriebe. Man sieht auch die Aufhängung der Rückholfeder der Stunden.