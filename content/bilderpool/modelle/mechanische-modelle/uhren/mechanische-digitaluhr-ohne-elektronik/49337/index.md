---
layout: "image"
title: "Übertrag zur Zehnerstelle der Minuten (1)"
date: 2022-01-08T16:59:42+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse der Einerstelle der Minuten, die also alle Minute weiterschaltet, geht über das Z10 und eine Kette (in der Bildmitte) auf ein Z20. Auf dessen Achse sitzt rechts daneben ein Selbstbau-Z12. Im nächsten Bild ist erklärt, was es damit auf sich hat.