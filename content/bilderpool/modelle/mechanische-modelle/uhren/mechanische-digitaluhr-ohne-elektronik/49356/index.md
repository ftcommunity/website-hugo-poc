---
layout: "image"
title: "Wartungswerkzeuge"
date: 2022-01-08T17:00:05+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Wartungsstation sitzen folgende Werkzeuge:

- Ein Riegelschlüssel, um die Frontplatten leicht abnehmen zu können.

- Ein Schraubendreher.

- Eine lange Achse mit einem Klemmring, der für fummelige Arbeiten tief in der Mechanik hilfreich ist.

- Ein Stab mit Haken, mit dem man die später noch beschriebenen fischertechnik-Antriebsfedern in die Federmechanik einhängen kann.