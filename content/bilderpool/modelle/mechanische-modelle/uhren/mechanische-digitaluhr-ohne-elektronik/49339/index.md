---
layout: "image"
title: "Zählmechanik"
date: 2022-01-08T16:59:44+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Zählmechanik für die Einerstelle der Minuten. Rechts vom linken Statikträger sehen wir das Z20 auf seiner Freilaufnabe. Auch die Drehscheibe rechts davon sitzt auf einer Freilaufnabe, dazwischen eine kleine Seilrolle, um die die Feder gespannt ist.

Die Feder verläuft vorne um die gerade noch sichtbare schwarze K-Achse, die in einem Loch der Drehscheibe und auf der linken Seite zwischen zwei Zähnen des linken Z20 sitzt. Drehscheibe und Z20 sind damit kraftschlüssig verbunden, und die Feder an der K-Achse ist das Element, dass das Ganze nach Freigabe des Kettenglieder-Z10 unten wieder zurückschnalzen lässt.

Auf der Drehscheibe wiederum sitzt der graue Federgelenkstein, der in die Zähne des rechen Z20 eingreift. Das ist nun endlich mit einer normalen Nabe auf der Achse festgeschraubt.

Das Ganze funktioniert dann so:

- Das Z20 wird vom Z10 mit seinen Rastkettengliedern gedreht.

- Über die K-Achse dreht die Drehscheibe synchron mit; gleichzeitig wird die Feder gespannt und aufgezogen - wir speichern Energie in ihr.

- Der Federgelenkstein an der Drehscheibe bildet mit dem rechten Z20 eine Freilaufkupplung und gleitet also einfach über das festgeschraubte Z20 drüber. Eine Rücklaufsperre (oben über dem rechten Z20) verhindert, dass sich das Z20 dabei mitdreht.

- Sobald das Rastketten-Z10 das linke Z20 freigibt, holt die Feder das linke Z20 und die Drehscheibe, und damit auch den Federgelenkstein zurück. Jetzt aber in der Richtung, in der die Drehung aufs rechte Z20 übertragen wird - bis der Gelenkstein am Endanschlag oben ankommt.

Das sollten bitte genau 7 Zähne pro Lauf sein. Damit drehen wir nämlich die Achse mit einem weiteren Z20, auf dem die umlaufende Kette mit den Ziffern 0 - 9 der Einerstelle der Minuten sitzt, um genau 7 Zähne und damit um genau eine Ziffer weiter.

Soweit die Theorie. Wie später noch erläutert, funktioniert das je nach Spannungszustand der nachfolgenden Federn aber nicht total zuverlässig. Es kommt leider vor, dass eben doch nur 6 oder sogar 8 Zähne weitergeschaltet wird und die Ziffer damit nicht genau die gewünschte Position einnimmt. Schlimmer: Diese Fehler addieren sich über die Zeit auf. Ganz zufrieden bin ich also noch nicht. Warum genau das passiert, kommt später noch.