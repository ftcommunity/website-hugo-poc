---
layout: "image"
title: "Überblick über die Uhrenmechanik"
date: 2022-01-08T17:00:00+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Überblick über die Komponenten der Uhr bei herausgenommenem Schwungrad.

Das Getriebe beginnt also links mit deiner Untersetzung auf eine Umdrehung pro Minute. Von da geht es über die zweite Kette (links, senkrecht) hoch zu einer Achse, die in einem Rast-Winkelgetriebe endet, das das obenauf waagerecht liegende Sekunden-Speichenrad antreibt (das sich ja ein Mal pro Minute drehen muss).

Außerdem geht es von den Minutenrädern durch drei hintereinander geschalteten Übertrags- und Zählgetriebe, die ein Vorrücken der jeweils benötigten Kette mit den Zahlen für die Einerstelle (0 - 9) und Zehnerstelle (0 - 5) der Minuten sowie für die Stunden (00 - 23) im richtigen Moment bewirken.

Aber der Reihe nach.