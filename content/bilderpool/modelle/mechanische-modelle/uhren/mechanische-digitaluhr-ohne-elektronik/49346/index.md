---
layout: "image"
title: "Getriebeblock eingebaut"
date: 2022-01-08T16:59:53+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So passt alles zusammen, wenn der Getriebeblock angesteckt ist. Insgesamt ist das vom Schwungrad bis zum letzten Z40 gesehen also Z10 per Kette auf Z40, dann 32:20, 10:40, 32:20, 10:40, 32:20, 10:40.