---
layout: "image"
title: "Antrieb der Zehnerstelle"
date: 2022-01-08T16:59:13+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik54.jpg"
weight: "54"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das kennen wir nun schon - es funktioniert genau wie bei den anderen Stellen.