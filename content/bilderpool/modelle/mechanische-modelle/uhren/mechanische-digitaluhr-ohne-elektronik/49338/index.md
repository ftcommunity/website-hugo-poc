---
layout: "image"
title: "Einerstelle der Minuten"
date: 2022-01-08T16:59:43+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achse in Bildmitte ist die eben besprochene. Die geht auf ein darauf geschraubtes Z20, dass die Kette mit den Ziffern 0 - 9 für die Einerstelle der Minuten trägt.