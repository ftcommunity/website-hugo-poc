---
layout: "image"
title: "Linke Seite"
date: 2022-01-08T16:59:59+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein alter fischertechnik-Trafo mit echtem Wechselspannungsausgang an der Seite liefert die 50 Hz Netzfrequenz als Zeitbasis.