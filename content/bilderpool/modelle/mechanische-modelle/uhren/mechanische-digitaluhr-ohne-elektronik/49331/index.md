---
layout: "image"
title: "Blick bis in die Stundenzahlen"
date: 2022-01-08T16:59:35+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weiter geht es nach rechts (von hinten gesehen) zu den Stundenzahlen. Die gehen von 00 - 23 und die Kette ist dementsprechend lang (24 * 7 Kettenglieder). Bei der schrägen Kette unten rechts beginnt der Übergang von der Zehnerstelle auf die Zählmechanik der Stunden.