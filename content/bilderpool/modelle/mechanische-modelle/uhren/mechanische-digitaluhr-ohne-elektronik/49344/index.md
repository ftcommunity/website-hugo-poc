---
layout: "image"
title: "Ketten zum Sekundenrad und zur Zehnerstellen-Mechanik"
date: 2022-01-08T16:59:50+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die mittlere senkrecht stehende Kette geht hoch zum Sekundenrad. Die rechte Kette (von einem Z10 zu dem oben liegenden Z20) wird schon von der Übertragsmechanik der Einerstelle der Minuten angetrieben und führt zum Getriebe für die Zehnerstelle der Minuten.