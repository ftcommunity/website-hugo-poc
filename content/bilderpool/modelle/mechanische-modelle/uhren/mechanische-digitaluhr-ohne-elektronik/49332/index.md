---
layout: "image"
title: "Teilblick"
date: 2022-01-08T16:59:36+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Mechanik für die Einer- und Zehnerstellen im Überblick.