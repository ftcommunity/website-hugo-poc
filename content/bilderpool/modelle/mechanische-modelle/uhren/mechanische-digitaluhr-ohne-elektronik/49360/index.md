---
layout: "image"
title: "Lagerung der Radachse"
date: 2022-01-08T17:00:10+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf beiden Seiten der durchs Schwungrad gehenden langen Metallachse ist dieselbe Art von Lager eingebaut. Je zwei versetzt angebrachte Vorstuferäder mit per Verbinder anstatt Zapfen verdrehsicher leichtgängig gelagerten und geschmierten Achsen tragen die zugehörigen Gummireifen. In die tiefste Stelle zwischen beiden Reifen ist die Achse des Schwungrads einfach aufgelegt. Dadurch entsteht für die Reibung der kleinen Achsen an den BS7,5 eine starke Untersetzung (zwischen Durchmesser der Achse und Durchmesser der Reifen nämlich). Das führt zu einer extrem leichtgängigen Lagerung des schweren Rades.

Lediglich gegen Versatz in Achsrichtung gibt es auf jeder Seite je eine leichtgängig drehbare kleine schwarze Seilrolle. Auch da reibt kaum etwas Schwergängiges aneinander.