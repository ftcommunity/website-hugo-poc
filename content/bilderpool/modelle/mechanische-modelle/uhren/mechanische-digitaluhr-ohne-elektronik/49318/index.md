---
layout: "image"
title: "Blick von oben"
date: 2022-01-08T16:59:19+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wir blicken von hinten oben in die Uhr. Vom großen Rad geht eine Kette zu einem Untersetzungsgetriebe 2:125, dessen letztes Z40 sich in einer Minute genau ein Mal dreht. Von da geht es auf Sekundenrad oben und auf drei hintereinandergeschaltete Übertragsgetriebe. Die sorgen dafür, dass die Z20, die die Ketten mit den Zahlenplättchen tragen, immer um genau 7 Zähne - dem Abstand zwischen zwei Plättchen - weitergeschaltet werden. Und zwar schlagartig.