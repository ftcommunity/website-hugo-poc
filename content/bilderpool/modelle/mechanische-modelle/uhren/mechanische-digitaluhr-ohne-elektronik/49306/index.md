---
layout: "image"
title: "Stroboskop-Elektronik"
date: 2022-01-08T16:59:04+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik_Nachtrag1.jpg"
weight: "60"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Stroboskop, das parallel zum Elektromagneten betrieben wird, besteht aus der Serienschaltung eines Widerstands, einer Diode und einer LED. Damit funktioniert das gut - es gibt einen geeignet langen Lichtimpuls pro positiver Halbwelle, also 50 Stück pro Sekunde. Außerdem sollte das die LED vor induktiven Spannungsspitzen des Elektromagneten schützen.