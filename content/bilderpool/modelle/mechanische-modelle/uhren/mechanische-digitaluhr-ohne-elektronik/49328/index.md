---
layout: "image"
title: "Untersetzungsgetriebe für die Stunden"
date: 2022-01-08T16:59:31+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn die 6 Ziffern 0 - 5 der Zehnerstelle der Minuten ein Mal durch sind, haben die also 6 * 7 Zähne eines Z20 zurückgelegt. Das sind also 6 * 7 / 20 = 21/10 Umdrehungen Das muss zu einer Umdrehung des Rastketten-Z10 der Stundenmechanik werden. Wir brauchen also eine Untersetzung 1:(21:10) = 10:21. Das geht relativ einfach mit einem Z10 auf einem Selbstbau-Z21 (= Nabe plus kleiner Reifen und 21 Kettenglieder drumrum).