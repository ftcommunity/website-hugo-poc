---
layout: "image"
title: "Untersetzung auf eine Umdrehung je Minute"
date: 2022-01-08T16:59:55+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier eine Sicht sozusagen schräg von oben (nach Einbau in die Uhr) auf das Getriebe.