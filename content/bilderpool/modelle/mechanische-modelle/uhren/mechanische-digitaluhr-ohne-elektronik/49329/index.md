---
layout: "image"
title: "Rückseite"
date: 2022-01-08T16:59:32+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das große Rad trägt 96 Neodym-Magnete, immer paarweise verdreht angebracht (in den Beschriftungen im Video ist irrtümlich von 48 Magneten die Rede). Es wird von Hand am roten Handknopf mittig angeworfen, bis es genau die richtige Geschwindigkeit hat nämlich 50/48 Umdrehungen pro Sekunde. Um das zu erleichtern, sitzt links unten nach einem zusätzlichen Vorwiderstand und einer Diode eine LED am 50-Hz-Ausgang als Stroboskop - ganz wie bei den früheren Schallplattenspielern.