---
layout: "image"
title: "Kette zum Sekundenrad und zum Zehnerstellen-Getriebe"
date: 2022-01-08T16:59:24+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik46.jpg"
weight: "46"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die rechte Kette geht hoch zum Sekundenrad, die linke zur Zehnerstelle. Von der sieht man hier auch mal das Rastketten-Z10 links oben.