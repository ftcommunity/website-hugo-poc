---
layout: "image"
title: "Mechanik der Einerstelle"
date: 2022-01-08T16:59:20+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik49.jpg"
weight: "49"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Was da vorne ins linke Z20 eingreift, ist die Sperre gegen "mit Schwung zu weit drehen", wenn die Rückholfeder das Ganze zurückschnalzen lässt. Es wird in diese Position gedrückt, wenn der Federgelenkstein der Drehscheibe hinten oben anschlägt.