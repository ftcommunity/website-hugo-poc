---
layout: "image"
title: "Erster Getriebeblock und Übergang zur Einerstelle"
date: 2022-01-08T16:59:25+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik45.jpg"
weight: "45"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das wurde vorhin schon beschrieben, hier aber eine Sicht von vorne.