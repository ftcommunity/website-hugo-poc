---
layout: "image"
title: "Untersetzung auf eine Umdrehung je Minute"
date: 2022-01-08T16:59:57+01:00
picture: "2020-04-28_Mechanische_Digitaluhr_ohne_Elektronik21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch dieser erste Untersetzungs-Getriebeblock ist recht leicht herausnehmbar.

Wir müssen untersetzen von 50/48 Umdrehungen pro Sekunde auf 1 Umdrehung je Minute. Wir brauchen also einen Faktor (50/48) / (1/60) = 125/2, also eine Untersetzung 125:2.

125 = 5\*5\*5. Die benötigte 5 steckt in den 32 Zähnen des Kronenzahnrads eines Z40: Geben wir das auf ein Z20 (links oben im Bild), erhalten wir 32:20 = 16:10 = 8:5 - wir haben eine 5.

Von der Kette aus geht es nun drei Mal von Z10 auf Z40 (1:4) und von da auf 32:20. 1/4 \* 32:20 = 2:5. Das drei Mal hintereinander ist also (2:5)³ = 2\*2\*2/5\*5\*5=8/125. Ein letztes Z10 auf Z40 ergibt die gewünschte 8/125 \* 1/4 = 2/125.
