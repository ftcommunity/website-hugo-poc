---
layout: "image"
title: "Frontansicht"
date: "2017-06-18T23:03:33"
picture: "hzuhrmitdirektantrieb01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45962
- /details9bbf-2.html
imported:
- "2019"
_4images_image_id: "45962"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45962 -->
Diese Uhr hat keinen Sekundenzeiger, aber eine im Sekundentakt aufleuchtende und sanft erlöschende Lampe, die die Rückseite des Ziffernblatts illuminiert.

Die Uhr läuft aber absolut geräuschlos. Man hört wirklich *nichts*. Die Zeitbasis sind die 50 Hz Wechselspannung eines alten fischertechnik-mot-4-Trafos. Der wird über zwei ft-Electronics-Moduln 1:50 in Sekunden und dann nochmal in Minuten geteilt. Alle Minute wird der große Rotor mit 15 der älteren ft-Dauermagneten mit einer 4-Phasen-Ansteuerung zweier Magnete angesteuert. 4 * 15 = 60, was bedeutet: Der Rotor des "Schrittmotors" *ist* der Minutenzeiger.
