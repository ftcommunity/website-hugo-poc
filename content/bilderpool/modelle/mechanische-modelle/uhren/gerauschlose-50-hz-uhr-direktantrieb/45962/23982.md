---
layout: "comment"
hidden: true
title: "23982"
date: "2018-03-07T14:07:31"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Natürlich hätte man diese Verschiebung mit einer fischertechnik-Synchronuhr beobachtet. Leider hatte ich in den letzten Monaten keine mehr im Dauerbetrieb. Die aktuelle Verschiebung kann man immer hier finden:
http://www.swissgrid.ch/swissgrid/de/home/experts/topics/frequency.html
Viele Grüße
Thomas