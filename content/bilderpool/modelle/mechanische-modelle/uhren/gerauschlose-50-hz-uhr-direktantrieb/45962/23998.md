---
layout: "comment"
hidden: true
title: "23998"
date: "2018-03-12T14:11:20"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Das Europäische Stromnetz schwächelt. Die Synchronuhren gehen immer noch 6 Minuten nach und die Netzfrequenz bleibt niedrig, also unter 50 Hz.
Bei meiner neuesten Synchronuhr macht das nichts, die kann man nur auf 3 Minuten genau ablesen. Kommt demnächst.
Gruß
Rüdiger