---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Direktantrieb des Minutenzeigers"
date: 2020-02-22T08:16:04+01:00
legacy_id:
- /php/categories/3416
- /categoriesf42e.html
- /categories9daf.html
- /categoriese4d8.html
- /categories89b9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3416 --> 
Diese Zeigeruhr hat keinen Sekundenzeiger, läuft aber absolut geräuschlos und ist präzise 50-Hz-getaktet.