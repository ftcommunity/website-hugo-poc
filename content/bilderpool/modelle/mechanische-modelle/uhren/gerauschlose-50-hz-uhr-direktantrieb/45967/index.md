---
layout: "image"
title: "Aufhängung des Rotors"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45967
- /details71db.html
imported:
- "2019"
_4images_image_id: "45967"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45967 -->
Die Drehscheibe und das Z30 sitzen via Freilaufnabe auf der langen Achse. Auf dieser Achse ist der Stundenzeiger mittels eines 31712 Statik-Mitnehmers plus Klemmring befestigt (siehe vorheriges Bild). Drehscheibe und Z30 sind nur mit drei K-Achsen verbunden, und so kann die Drehung der Drehscheibe und des Rotors hinreichend weit aus dem Rotor heraus gebracht werden, sodass über zwei Mal Z30 plus Z10 auf einer Achse und einen Kettentrieb vom letzten Z10 auf ein Z40 die lange Stundenzeiger-Metallachse sich korrekt untersetzt dreht.
