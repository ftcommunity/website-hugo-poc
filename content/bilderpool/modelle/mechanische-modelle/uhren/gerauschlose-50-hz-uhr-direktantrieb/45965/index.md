---
layout: "image"
title: "Antrieb des Rotors"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45965
- /detailseabf.html
imported:
- "2019"
_4images_image_id: "45965"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45965 -->
Die Elektromagnete fügen sich brauchbar ins Zifferblatt ein. Immer genau einer der vier Pole der (beiden Magnete) zieht gerade einen der Dauermagnete zu sich: Erst der (von links nach rechts gezählt) Pol 1, dann 3 (der in diesem Bild gerade zieht), dann 2, dann 4, und wieder von vorne.

Die graue K-Achse vorne unten dient der Fixierung des Antriebs-Ziffernblatts, insbesondere gegen Verdrehung.
