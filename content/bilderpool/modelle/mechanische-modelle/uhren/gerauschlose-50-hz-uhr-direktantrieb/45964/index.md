---
layout: "image"
title: "Rückansicht 2"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45964
- /detailsd15c.html
imported:
- "2019"
_4images_image_id: "45964"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45964 -->
Die vier Elektronik-Bausteine sind mit Kombinationen aus 2 WS60° plus BS7,5 am mittleren waagerechten Bausteinstrang befestigt. Unten durch diesen Strang (hier also nicht sichtbar) verläuft eine Metallachse 110 zu Stabilisierung. Verbaut ist ein ft-h4-Gleichrichter für die Stromversorgung; von einem weiteren wird nur der Kondensator für die Lampe verwendet (die Schaltung wird noch beschrieben). Ansonsten sind 2 Electronics-Moduln, 1 E-Tec (das könnte auch ein weiteres Electronics- oder MiniBots-Modul sein) und 1 MiniBots-Modul (das könnte auch ein weiteres Electronics-Modul sein) verbaut.
