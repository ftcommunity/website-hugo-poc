---
layout: "image"
title: "Getriebeteile"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46797
- /detailsbe55-2.html
imported:
- "2019"
_4images_image_id: "46797"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46797 -->
Das ist die "senkrechte" Achse und die Querachse, die sie antreibt.
