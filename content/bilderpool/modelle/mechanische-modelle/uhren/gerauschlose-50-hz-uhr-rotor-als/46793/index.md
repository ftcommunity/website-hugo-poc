---
layout: "image"
title: "Rechte Seite"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46793
- /detailsb9f1.html
imported:
- "2019"
_4images_image_id: "46793"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46793 -->
Hier sieht man die Mimik mit dem Winkelgetriebe und der Schnecke. Die senkrecht stehende Achse steht leicht schräg, damit die Geometrie passt. Deshalb ist sie auch nur in S-Kupplungen ganz leichtgängig gelagert.
