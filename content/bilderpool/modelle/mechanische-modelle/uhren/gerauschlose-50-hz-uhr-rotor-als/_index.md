---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Rotor als Sekundenwelle"
date: 2020-02-22T08:16:06+01:00
legacy_id:
- /php/categories/3464
- /categories1295.html
- /categoriescce5.html
- /categoriesa9a0.html
- /categories24d6.html
- /categoriese41f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3464 --> 
Genau wie ihr Vorgänger https://www.ftcommunity.de/categories.php?cat_id=3416 gibt es einen Direktantrieb eines Zeigers, aber hier ist es der Sekundenzeiger.