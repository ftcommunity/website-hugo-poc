---
layout: "image"
title: "Zeiger"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46800
- /detailsea02.html
imported:
- "2019"
_4images_image_id: "46800"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46800 -->
Stunden- und Minutenzeiger sind mit je einem Seilklemmstift in den Z30 gelagert. Sie sitzen auf Freilaufnaben und haben einen Abstandshalter zwischen sich, damit die Zeiger an den Ketten vorbeikönnen.
