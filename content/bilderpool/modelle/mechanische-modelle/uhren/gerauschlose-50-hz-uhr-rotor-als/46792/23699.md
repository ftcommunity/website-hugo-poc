---
layout: "comment"
hidden: true
title: "23699"
date: "2017-10-17T21:18:45"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan, 
gratuliere zu deiner neuesten Uhr-Kreation! Eine gelungene Umsetzung in ft.
Insbesondere der 4-Phasen Synchron-Motor fasziniert mich. Dass du aus dem Minuten-Antrieb einen Sekunden-Antrieb machen konntest ist prima. Auf deinem schönen Video sieht man aber auch eine Grenze, die durch die Pendelschwingungen bei jedem Stop gegeben ist. Diese Schwingungen sind mit ft-Mitteln wohl nicht zu dämpfen, da bräuchte man einen massiven Kupferring außen herum. Einen Gleichstrommotor kann man ja durch Kurzschließen zum Stoppen zwingen, aber mit den E-Magneten wird das wohl nicht funktionieren.
Gruß
Rüdiger