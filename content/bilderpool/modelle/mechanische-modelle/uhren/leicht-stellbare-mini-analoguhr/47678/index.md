---
layout: "image"
title: "Blick von oben mit abgenommener Verkleidung"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47678
- /details7609-3.html
imported:
- "2019"
_4images_image_id: "47678"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47678 -->
Von der Achse des Z25 geht es über Z10, Kette, Z20 auf die Achse des Minutenzeigers. Das ergibt eine Untersetzung von 10:25 * 10:20 = 1:5. Das wiederum erlaubt es, für den Minutenschritt genau vier Schritte des Schrittmotors zu verwenden. Seine Wicklungen sind danach wieder in genau demselben Zustand, sodass man die Uhr durch Drehen am Z25 völlig frei einstellen kann, und sie trotzdem immer genau Minuten zeigt und keine Zwischenstände. Die Software dafür ist super einfach: Einfach ein Timer, der alle 60 Sekunden 4 Schrittmotor-Schritte ausgibt. Es handelt sich um das Beispiel https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Samples/Sample10StepperMotorClock.cs, in das die konkreten Netduino-Objekte aus Sample10StepperMotorClock in https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Netduino3.Samples/Netduino3SamplesMain.cs hinein gereicht werden - siehe https://github.com/steffalk/AbstractIO.

----------

The axis with the Z25 on in also carries another Z10 with a chain going around a Z20 on the axis of the minute hand. This results in a gear reduction of 10:25 * 10:20 = 1:5. This allows using exactly four stepper motor steps for switching from one minute to the next. Its coils are then in the same state again, so that you can set the clock freely just by turning the Z25 manually, and it will continue at an exact minute position, not in some in-between setting. The software for this is super simple: Just a timer, firing every 60 seconds and turning the stepper by four steps. This is sample https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Samples/Sample10StepperMotorClock.cs, into which the actual Netduino objects get passed in Sample10StepperMotorClock of https://github.com/steffalk/AbstractIO/blob/master/source/AbstractIO.Netduino3.Samples/Netduino3SamplesMain.cs -see https://github.com/steffalk/AbstractIO.
