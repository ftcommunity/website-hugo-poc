---
layout: "image"
title: "Uhrwerk (4)"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47682
- /detailseb3f-2.html
imported:
- "2019"
_4images_image_id: "47682"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47682 -->
Schließlich wird aus der Untersetzung 1:4 über die Kombination Z10 auf Z30 (auf einer Freilaufnabe) die benötigten 1:12 zwischen Minuten- und Stundenzeiger, der einfach aus einer I-Strebe 15 besteht, der mit einem Statikstopfen auf dem Z30 fixiert ist.

----------

Finally, the gear reduction of 1:4 is supplemented by a combination of this Z10 onto a Z30 (sitting on a freewheel) to the needed 1:12 between minute hand and hour hand, which is simply an I-strut 15, attached to the Z30 using a "Statikstopfen".
