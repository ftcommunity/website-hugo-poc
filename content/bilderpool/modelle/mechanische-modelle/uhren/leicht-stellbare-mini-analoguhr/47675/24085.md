---
layout: "comment"
hidden: true
title: "24085"
date: "2018-05-21T12:11:59"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Danke! Die Untersetzung ist allerdings fast identisch mit der hier: https://www.ftcommunity.de/categories.php?cat_id=1676, nur der Antrieb ist neu.

Gruß,
Stefan