---
layout: "comment"
hidden: true
title: "15093"
date: "2011-09-13T09:00:05"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

ein Video ist kurzfristig unter http://homepages.rub.de/Thomas.Puettmann/Uhr.mov verfügbar.

Die Magneten werden einfach parallel an den Wechselspannungsausgang des alten Netzteils angeschlossen.

Viele Grüße, Thomas