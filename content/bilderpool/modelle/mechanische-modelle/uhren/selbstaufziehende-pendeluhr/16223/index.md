---
layout: "image"
title: "Endabschaltung"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- /php/details/16223
- /detailsea7a.html
imported:
- "2019"
_4images_image_id: "16223"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16223 -->
Blick von hinten auf das Aufzug- und Umschaltwerk; schön zu sehen sind hier die beiden Endabschalter für die obere Gewichtslage.