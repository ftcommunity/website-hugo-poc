---
layout: "comment"
hidden: true
title: "7732"
date: "2008-11-08T16:54:00"
uploadBy:
- "flyingcat"
license: "unknown"
imported:
- "2019"
---
Jau, ist mittlerweile korrigiert - war auch nicht wirklich schwer, jetzt sitzt der Minutenzeiger eben auf einer Scheibe, die auf der Achse nicht festgezogen ist.

Schade, mir gefiel am Drehkranz, dass genau diese Art der Rutschkupplung nicht nötig war... Wenn ich mal in Rente bin, schreibe ich an die Vereinten Nationen und schlage vor, die 60-Minuten-Stunde durch die 58-Minuten-Stunde zu ersetzen, dann paßt es wieder :-)