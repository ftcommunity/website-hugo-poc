---
layout: "overview"
title: "Selbstaufziehende Pendeluhr"
date: 2020-02-22T08:15:24+01:00
legacy_id:
- /php/categories/1463
- /categories5888.html
- /categoriesc6da.html
- /categories3983.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1463 --> 
Eine Pendeluhr mit zwei Gewichten, von denen jeweils eines die Uhr antreibt, während das andere entweder motorisch hochgezogen wird oder oben in Bereitschaft steht, um einzuspringen, wenn das andere unten angekommen ist.