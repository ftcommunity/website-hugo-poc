---
layout: "image"
title: "Design-Uhr mit Sekunden-, Minuten und Stundenanzeige"
date: 2020-03-16T18:00:00+01:00
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Uhr entstand nach einer Idee von Rüdiger Riedel mit seinen Berichten in der ft:pedia 2/2017 und 2/2018. Das Design wurde weiterentwickelt, sodass die Uhr kein Ziffernblatt hat, sondern die grauen Bauplatten auf den jeweiligen Ringen die Zeit anzeigen.

Die Uhr verfügt neben einer Stundenanzeige (Außenring) auch über Minuten (mittlerer Ring) und Sekundenanzeige.  
