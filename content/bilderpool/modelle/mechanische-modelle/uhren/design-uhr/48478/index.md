---
layout: "image"
title: "Uhrenbeleuchtung "
date: 2020-03-16T17:59:39+01:00
picture: "bild8.jpg"
weight: "8"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

…. sondern auch von Getriebe und Motor. Dadurch entsteht ein schönes Licht und Schattenspiel an der Wand.
