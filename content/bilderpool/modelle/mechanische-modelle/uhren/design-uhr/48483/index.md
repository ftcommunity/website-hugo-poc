---
layout: "image"
title: "Antriebsdetails"
date: 2020-03-16T17:59:52+01:00
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Nordconvention-2019", "Südconvention-2019"]
uploadBy: "Website-Team"
license: "unknown"
---

Die vom Motor angetriebene senkrechte Welle treibt über die Kegelzahnräder direkt das Sekundenrad an.
