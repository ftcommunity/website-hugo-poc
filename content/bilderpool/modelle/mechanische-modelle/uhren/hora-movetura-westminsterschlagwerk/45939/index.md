---
layout: "image"
title: "Hora Movetura 02"
date: "2017-06-14T20:18:47"
picture: "IMG_7951.jpg"
weight: "2"
konstrukteure: 
- "ist"
fotografen:
- "ist"
uploadBy: "FTbyIST"
license: "unknown"
legacy_id:
- /php/details/45939
- /details243d-2.html
imported:
- "2019"
_4images_image_id: "45939"
_4images_cat_id: "3412"
_4images_user_id: "2145"
_4images_image_date: "2017-06-14T20:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45939 -->
Das Stundenschlagwerk. Ein alter Minimotor löst das Pendel aus. Ein Taster unterbricht nach einer Runde durch ein Relais die Stromzufuhr. Gesteuert wird das ganze Schlagwerk durch den Arduino oben links.