---
layout: "image"
title: "Detail Zeiger"
date: "2013-01-25T19:32:47"
picture: "althzuhr09.jpg"
weight: "9"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36515
- /details87a4-2.html
imported:
- "2019"
_4images_image_id: "36515"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36515 -->
Die Aufnahme und Antriebe der Minuten- und Stunden-Innenzahnkränze.
Minuten: Hinterer Zahnkranz, Antrieb ist links unten
Stunden:  Vorderer Zahnkranz, Antrieb ist rechts oben
