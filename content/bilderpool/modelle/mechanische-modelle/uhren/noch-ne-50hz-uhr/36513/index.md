---
layout: "image"
title: "Linke Seite Detail"
date: "2013-01-25T19:32:47"
picture: "althzuhr07.jpg"
weight: "7"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36513
- /details5e5f.html
imported:
- "2019"
_4images_image_id: "36513"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36513 -->
Von der zentralen Sekundenachse geht es über die Schnecke und  z10, einmal um die Ecke, über die z10 auf z20 Untersetzung auf ein Z10, das einerseits über das untere z30 in den Stundentrieb verzweigt und über eine z10-z10-Umkehr den Innenzahnkranz des Minutenzeigers (z10 auf iz30) antreibt.  
