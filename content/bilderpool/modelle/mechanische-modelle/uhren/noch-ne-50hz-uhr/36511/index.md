---
layout: "image"
title: "Getriebe"
date: "2013-01-25T19:32:47"
picture: "althzuhr05.jpg"
weight: "5"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36511
- /details179c.html
imported:
- "2019"
_4images_image_id: "36511"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36511 -->
Zunächst werden die 500UpM des Antriebs durch eine  z10 auf z40 Untersetzung auf 125UpM gebracht.
Mit den beiden durch Schnecken angetriebenen z10 geht's runter auf 1.25 UpM.
Das untere z40 (auf Freilaufnabe) treibt dann die zentrale Sekundenzeigerachse mit 1.25Upm*32/40 = 1UpM an. 
