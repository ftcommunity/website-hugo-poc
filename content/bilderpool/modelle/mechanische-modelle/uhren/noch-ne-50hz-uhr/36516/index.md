---
layout: "image"
title: "Detail Zeiger 2"
date: "2013-01-25T19:32:47"
picture: "althzuhr10.jpg"
weight: "10"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36516
- /details22b2.html
imported:
- "2019"
_4images_image_id: "36516"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36516 -->
