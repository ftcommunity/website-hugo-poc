---
layout: "image"
title: "Schwungrad des Motors"
date: "2013-01-25T19:32:47"
picture: "althzuhr04.jpg"
weight: "4"
konstrukteure: 
- "Helmut (hamlet)"
fotografen:
- "Helmut (hamlet)"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- /php/details/36510
- /details76cb-2.html
imported:
- "2019"
_4images_image_id: "36510"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36510 -->
Die Magnete stammen aus einem Geomag-Baukasten. 6 Magnetstäbchen entsprechen 12 Einzel-Magneten. Der Motor läuft mit 50Hz * 2 / 12 * 60s/min = 500 UpM. 
Manchmal springt er 5-mal hintereinander auf Anhieb an. Das andere Mal benötigt man 20 Versuche bis er endlich läuft. Wenn er einmal läuft, dann absolut stabil und man kann erstaunlich viel Leistung abgreifen. Für den Antrieb der Uhr reicht es dicke. Die beiden E-Magnete sind in Reihe geschaltet und ziehen im Betrieb 47mA (RMS) am 6.8V Wechselspannungausgang des alten ft-Netzteils. Lustigerweise sind es im Stillstand über 70mA. Hab das mit einem Oszilloskop kontrolliert, P(t)=U(t)*I(t). Der Motor verbrät im Betrieb weniger Leistung als im Stillstand. Wie dem auch sei, die E-Magnete bleiben kalt.
