---
layout: "image"
title: "Teilansicht Uhrwerk"
date: 2020-06-02T11:43:26+02:00
picture: "IMG_0582.JPG"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Website-Team"
license: "unknown"
---
Von dem rechten schwarzen Z10 geht es um 90 Grad gedreht auf die Welle für den Sekundenanzeiger. Gegenüber geht es über Z10, Z30, Z20, Z40 über die Schnecke auf das Z10 (fehlt in dieser Ansicht) auf den Minutenzeiger.
