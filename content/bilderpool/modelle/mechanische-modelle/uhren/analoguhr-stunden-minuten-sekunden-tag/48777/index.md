---
layout: "image"
title: "2. Seitenansicht von rechts"
date: 2020-05-26T17:33:16+02:00
picture: "IMG_0537.JPG"
weight: "4"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz oben geht von der Welle nach vorn auf den Antrieb für die Stundenanzeige und nach rechts über die Z 40 erst nach unten und dann wieder nach vorne auf die Kette für die Tag- und Nachtanzeige (Z 10/Z 20)
