---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-26T17:33:25+02:00
picture: "IMG_0542.JPG"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Uhr verfügt über eine große Stunden- und Minutenanzeige, eine kleine seperate Sekundenanzeige sowie eine darunter liegende Tag- und Nachtanzeige
