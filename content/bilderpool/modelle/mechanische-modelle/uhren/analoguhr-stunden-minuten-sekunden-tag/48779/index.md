---
layout: "image"
title: "Seitenansicht von rechts"
date: 2020-05-26T17:33:22+02:00
picture: "IMG_0549.JPG"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Website-Team"
license: "unknown"
---

Gut zu sehen ist auch der 50 Hz-Motor mit 12 Polpaaren und damit 250U/min.
