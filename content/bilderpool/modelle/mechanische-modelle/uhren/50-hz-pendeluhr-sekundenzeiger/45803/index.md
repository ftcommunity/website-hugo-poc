---
layout: "image"
title: "Plätzchen im Esszimmer (1)"
date: 2023-01-20T09:40:21+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wie praktisch alle meine Uhren darf auch diese einige Zeit in der Wohnung stehen.