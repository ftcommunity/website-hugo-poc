---
layout: "image"
title: "Übergang zum Uhrwerk"
date: 2023-01-20T09:40:28+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Pendel schaltet - man sieht es rechts oben - ein Z30 um genau einen Zahn pro Sekunde weiter. Damit es nicht etwa durch Reibungseffekte zurückgedreht wird, befindet sich unterhalb des Z40 die schräg stehende Rastsperre. Auf der Achse des Z30 sitzt ein Z20, das auf das Z40 der Sekundenwelle geht. Die dreht sich also genau ein Mal pro Minute und trägt den Sekundenzeiger. Zum schnellen Stellen der Uhr kann man die Sekundenachse über die Kurbel rasch drehen.