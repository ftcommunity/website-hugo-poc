---
layout: "image"
title: "Service Pack: Schrittbegrenzer (1)"
date: 2023-12-11T21:12:03+01:00
picture: "Service-Pack_1.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Dauerbetrieb stellte sich insbesondere nach Transporten oder Verstauben heraus, dass es möglich ist, dass die Auslenkung des elektromagnetischen Antriebs entweder nicht groß genug ist, um das Z30 tatsächlich einen Zahn weiter zu schalten, oder dass sie so groß ist, dass manchmal zwei Zähne anstatt nur ein Zahn weitergeschaltet wird. Deshalb wurden ein paar Teile zusätzlich angebaut, das zu verhindern.