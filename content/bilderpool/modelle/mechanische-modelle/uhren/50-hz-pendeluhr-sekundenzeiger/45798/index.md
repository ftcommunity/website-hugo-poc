---
layout: "image"
title: "Rundgang (4)"
date: 2023-01-20T09:40:15+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein schöner Rücken kann ja auch entzücken...