---
layout: "image"
title: "Elektromagnet (1)"
date: 2023-01-20T09:40:30+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Elektromagnet zieht also jede Sekunde für eine halbe Sekunde an der alten ft-Rückschlussplatte (die Metallplatte obendrüber) und stößt so das Pendel an.