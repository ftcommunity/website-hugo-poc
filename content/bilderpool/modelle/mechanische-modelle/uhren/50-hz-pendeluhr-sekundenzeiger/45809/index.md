---
layout: "image"
title: "Elektromagnet (2)"
date: 2023-01-20T09:40:29+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Pendel würde normalerweise zwar ungefähr, aber eben nicht genau ein Mal pro Sekunde schwingen. Die elektromagnetische Erregung des Pendels zwingt es aber zu genau einer Schwingung pro Sekunde.