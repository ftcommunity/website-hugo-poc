---
layout: "image"
title: "Service Pack: Schrittbegrenzer (3)"
date: 2023-12-11T21:12:01+01:00
picture: "Service-Pack_3.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Links am Baustein 30 sitzt ein BS7,5 mit einer S-Kupplung 2. Die ist sehr fein so justiert, dass der Mitnehmer (der waagerecht an einer K-Achse 30 befestigte BS7,5 mit einem Verbindungsstück 15 darunter), wenn er denn zu weit nach links (im Bild gesehen) ausschlägt, nicht etwa in einen Zahn zu weit eingreifen kann. Er wird von der S-Kupplung rechtzeitig vom darunter befindlichen Z30 abgehoben und somit getrennt.