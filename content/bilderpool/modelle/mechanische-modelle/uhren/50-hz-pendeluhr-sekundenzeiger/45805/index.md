---
layout: "image"
title: "Weg zum Stundenzeiger"
date: 2023-01-20T09:40:24+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nach rechts im Bild (bzw. nach hinten in der Uhr) geht ein Z10 per Kette auf das Z20 rechts unten. Nach einer weiteren Z10:Z20-Kombination endet das vorne wiederum in einer Z10:Z15-Untersetzung. Die schließlich treibt über Z10 auf Z30 den Stundenzeiger an.