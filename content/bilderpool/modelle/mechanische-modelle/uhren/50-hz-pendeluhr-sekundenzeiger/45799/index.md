---
layout: "image"
title: "Rundgang (3)"
date: 2023-01-20T09:40:16+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Schalter dient zum Ein- und Ausschalten der elektronischen Steuerung.