---
layout: "image"
title: "Rundgang (5)"
date: 2023-01-20T09:40:14+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Prototyp stand einfach auf 4 x je 2 Statikträgern 120. Das saht natürlich total langweilig her - etwas mehr Design musste also her. Das führte zu dieser geschwungenen Konstruktion im Stil eines Arcade-Spielautomaten aus den 1970ern.