---
layout: "image"
title: "Service Pack: Schrittbegrenzer (2)"
date: 2023-12-11T21:12:02+01:00
picture: "Service-Pack_2.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Rechts unten außen angeschlagen und in Richtung vorne/hinten sowie hoch/runter fein justierbar gibt es zwei senkrecht stehende Bausteine 30 - rechts im Bild. Auf denen sitzt quer ein BS15, WS7,5°, Federnocke, BS30 mit Anbauteilen.