---
layout: "image"
title: "Service Pack: Staubabdeckung (2)"
date: 2023-12-11T21:11:57+01:00
picture: "Service-Pack_6.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht es nicht, aber an der Oberkante der unteren Statikplatte ist zusätzlich eine I-Strebe 120 von unten angebracht, und zwar eine graue ohne Löcher. Die dichtet damit auch noch die S-Riegel-Aussparungen der Platten ab.

Die Platten sind aber auf Ausstellungen auch mit wenigen Handgriffen abgebaut, sodass das Publikum gut ins Uhrwerk schauen kann.