---
layout: "image"
title: "Service Pack: Staubabdeckung (1)"
date: 2023-12-11T21:11:58+01:00
picture: "Service-Pack_5.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Außerdem bekam die Uhr oben zwei Statikplatten 90x180 als Staubschutz, denn Staub ist der Feind jedes Getriebes.