---
layout: "image"
title: "Plätzchen im Esszimmer (2)"
date: 2023-01-20T09:40:20+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die beste aller Ehefrauen (was sag ich - die einzige!) genehmigte das Schmuckstück. Es arbeitet nämlich so leise, dass es niemanden stört.