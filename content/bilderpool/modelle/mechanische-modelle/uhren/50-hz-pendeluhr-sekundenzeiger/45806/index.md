---
layout: "image"
title: "Aufteilung in Minuten und Sekunden"
date: 2023-01-20T09:40:25+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Z40 wird von einer Schnecke auf der Sekundenwelle angetrieben. Damit haben wir schon mal auf dem Weg zu den anderen Zeigern eine Untersetzung 1:40. Über das Kronradgetriebe verzweigt sich der Kraftfluss nach links (in Richtung Front der Uhr) für die Minuten. Dort geht ein Z10 per Kette auf ein Z15 - damit haben wir 1:40 * 10:15 = 1:60. Das geht durch die Durchführung (siehe die Uhr von 2017 in https://www.ftcommunity.de/bilderpool/modelle/mechanische-modelle/uhren/gerauschlose-50-hz-uhr-neodym/) durch das Z30 im Zifferblatt zum Minutenzeiger.