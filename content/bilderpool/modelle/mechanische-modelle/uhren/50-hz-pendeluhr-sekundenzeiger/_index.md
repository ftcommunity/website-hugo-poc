---
layout: "overview"
title: "50-Hz-Pendeluhr mit Sekundenzeiger"
date: 2023-01-20T09:40:10+01:00
---

Diese Uhr verwendet nochmal ein schon 2017 von mir gebautes Uhrwerk, aber mit einem neuen Antrieb: Ein Pendel wird von den 50 Hz eines alten Trafos und einem Electronics-Modul zu genau einer Schwingung pro Sekunde gezwungen.