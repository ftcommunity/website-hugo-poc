---
layout: "image"
title: "Pendelantrieb und Uhrwerk von oben"
date: 2023-01-20T09:40:26+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick von oben in die Uhr zeigt links die Lasche, mit der der Elektromagnet das Pendel anstößt, das Pendel selbst und rechts daneben das Uhrwerk.