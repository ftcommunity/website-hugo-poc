---
layout: "image"
title: "Zifferblatt"
date: 2023-01-20T09:40:11+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das grüne ist der Sekundenzeiger, die beiden gelben sind für die Minuten und Stunden.