---
layout: "image"
title: "Frontansicht"
date: 2023-01-20T09:40:33+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Uhrwerk selbst ist einfach das gut funktionierende und kompakte von 2017 nochmal nachgebaut: https://youtu.be/ZUd8MotaJQk bzw. https://www.ftcommunity.de/bilderpool/modelle/mechanische-modelle/uhren/gerauschlose-50-hz-uhr-neodym/ - aber der Antrieb und das Design sind neu.

Das Uhrwerk selbst wird noch incl. Bauanleitung ausführlich in einer ft:pedia vorgestellt. Es ist nämlich vielseitig einsetzbar und kompakt, und soll in mindestens noch einer Uhrenvariante Dienst tun.

Ein Video dieser Uhr hier gibt es unter https://youtu.be/qaahMzngiD4