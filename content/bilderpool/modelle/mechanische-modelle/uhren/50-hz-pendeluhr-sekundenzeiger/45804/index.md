---
layout: "image"
title: "Blick von unten ins Uhrwerk"
date: 2023-01-20T09:40:22+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die beiden Z20 des Stunden-Strangs.