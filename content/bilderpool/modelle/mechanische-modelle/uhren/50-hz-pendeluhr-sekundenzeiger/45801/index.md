---
layout: "image"
title: "Rundgang (1)"
date: 2023-01-20T09:40:19+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Es handelt sich um eine Pendeluhr, deren Uhrwerk "frei schwebend" über dem Grund hängt. Das Pendel hängt einfach unten heraus und tut, was so ein Pendel eben tut: pendeln.