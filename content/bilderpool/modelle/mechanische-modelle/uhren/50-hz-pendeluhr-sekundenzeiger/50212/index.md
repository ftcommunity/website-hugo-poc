---
layout: "image"
title: "Service Pack: Schrittbegrenzer (4)"
date: 2023-12-11T21:11:59+01:00
picture: "Service-Pack_4.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die ebenfalls im Winkel von 7,5° angebrachte und fein justierbare Platte 15x30 macht dasselbe in die andere Richtung (hier also wieder nach links im Bild): Sollte der Mitnehmer das Z30 zu weit schalten wollen, wird er von der Platte 15x30 abgehoben und kann also das Z30  nicht mehr weiter als erwünscht bewegen.

Damit kann er E-Magnet so justiert werden, dass er immer einen zu großen Ausschlag bewirken würde. So wird sichergestellt, dass nie "kein Zahn" geschaltet wird. Das Abheben des Mitnehmers in beiden Endlagen stellt dann sicher, dass "nie mehr als ein Zahn" weiter geschaltet wird.

Obwohl die Uhr bei uns im Esszimmer ca. 3 Monate völlig problemlos durchlief, wurde diese Erweiterung nach weiteren Monaten und Verstaubung notwendig.