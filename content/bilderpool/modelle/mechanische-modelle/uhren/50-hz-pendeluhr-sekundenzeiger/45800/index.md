---
layout: "image"
title: "Rundgang (2)"
date: 2023-01-20T09:40:17+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Neue ist die Steuerung: Rechts unten sieht man einen alten fischertechnik-Trafo, der noch über einen echten Wechselspannungsausgang verfügt. Von dort werden die recht genauen 50 Hz des Stromnetzes abgegriffen.