---
layout: "image"
title: "Rundgang (5)"
date: 2023-01-20T09:40:12+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Außer dass sie hoffentlich eine gute Figur macht, funktioniert sie auch noch prächtig und präzise.