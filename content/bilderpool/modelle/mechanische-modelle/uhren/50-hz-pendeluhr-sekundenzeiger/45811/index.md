---
layout: "image"
title: "Elektronik"
date: 2023-01-20T09:40:31+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Wechselspannungsausgang des Trafos geht direkt zum Eingang I1 des Electronics-Moduls. Das ist so geschaltet, dass es zwei Teile 1:5 bietet. Vom ersten Teiler wird Ausgang O2 verwendet - der ist nochmal 1:2 geteilt, insgesamt also 1:10. Der zweite Teiler ist direkt dahinter geschaltet (von O2 geht es zum Eingang I5). Damit haben wir 1:5 * 1:2 * 1:5 = 1:50. Am Ausgang O3 rechts oben liegt also eine Frequenz von genau 1 Hz an. Das geht zu einem Elektromagneten oben im Uhrwerk.