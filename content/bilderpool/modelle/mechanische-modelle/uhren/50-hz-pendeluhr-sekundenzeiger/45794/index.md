---
layout: "image"
title: "50 Hz"
date: 2023-01-20T09:40:10+01:00
picture: "50-Hz-Pendeluhr_mit_Sekundenzeiger_09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Trafo liefert am seitlichen Ausgang 6,8V echte Wechselspannung. Über den hobby-4-Gleichrichter-Silberling wird das ft-Electronics-Modul versorgt.