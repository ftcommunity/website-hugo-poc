---
layout: "overview"
title: "Uhr mit 50-Hz Synchronmotor und Malteser Schrittschaltwerk"
date: 2020-02-22T08:16:02+01:00
legacy_id:
- /php/categories/3403
- /categories0fe9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3403 --> 
Vorlage war die 50Hz-Uhr mit Schrittschaltwerk für den Minutenzeiger von Dirk Fox. Das war die Gelegenheit, endlich die Malteser-Schrittschaltscheibe, die ich von Roland Enzenhofer bekommen habe, in einem passenden Modell zu verwenden.