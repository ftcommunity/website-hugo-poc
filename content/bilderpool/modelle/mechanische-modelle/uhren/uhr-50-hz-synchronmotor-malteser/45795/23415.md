---
layout: "comment"
hidden: true
title: "23415"
date: "2017-05-16T23:00:38"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Eine sehr schöne Uhrvariante! Toll.
Und das Malteserrad ist großartig - das spart die Konstruktion der "Einrast-Bremse".
(Dafür kann man die Uhr im Betrieb nicht mehr stellen - aber das sollte man ja eigentlich auch nicht müssen.)
Gruß, Dirk