---
layout: "image"
title: "Ansicht auf das Malteser-Schrittschaltwerk"
date: "2017-05-02T19:57:48"
picture: "Uhr_von_schraeg_oben.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Synchronmotor", "50Hz", "Malteser", "Schrittschaltwerk", "Uhr"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45795
- /details51db.html
imported:
- "2019"
_4images_image_id: "45795"
_4images_cat_id: "3403"
_4images_user_id: "579"
_4images_image_date: "2017-05-02T19:57:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45795 -->
Vorlage war die Uhr mit Schrittschaltwerk für den Minutenzeiger von Dirk Fox. Das war die Gelegenheit, endlich die Malteser-Schrittschaltscheibe, die ich von Roland Enzenhofer bekommen habe, in einem passenden Modell zu verwenden.  

Dazu gab es ein paar weitere Modifikationen: an anderer Synchronmotor mit 333 Umdrehungen / Minute und daraus folgend eine andere Übersetzung.

Sie läuft seit über 50 Stunden sehr leise und stabil.