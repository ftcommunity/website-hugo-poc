---
layout: "image"
title: "Uhr von vorne rechts"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34855
- /detailse2b4-3.html
imported:
- "2019"
_4images_image_id: "34855"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34855 -->
