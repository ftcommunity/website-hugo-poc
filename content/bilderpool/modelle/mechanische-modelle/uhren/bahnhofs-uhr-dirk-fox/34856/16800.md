---
layout: "comment"
hidden: true
title: "16800"
date: "2012-05-02T20:50:59"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Wenn ich das Getriebe richtig interpretiere, so bewegt sich nicht nur der Minutenzeiger, sondern auch der Sekundenzeiger jeweils "ruckartig".

Bei einer "richtigen" Bahnhofsuhr bewegt sich der Sekundenzeiger völlig autark in etwa 57 - 58 Sekunden (darin liegt der Trick) gleichmäßig einmal herum und stoppt dann. Von der bundesweiten Zentrale bekommen wiederum sämtliche (!) Uhren ihren Impuls, um den Minutenzeiger um 1 Minute weiterzubewegen. Und genau dieser Impuls setzt auch den Sekundenzeiger für die nächste Runde erneut in Bewegung.

Der Sinn, der dahintersteckt, ist recht simpel: auf diese Weise ist gewährleistet, daß bundesweit sämtliche Bahnhofsuhren synchron laufen.

Gruß, Thomas