---
layout: "comment"
hidden: true
title: "21339"
date: "2015-11-29T15:10:22"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nachtrag: Ich weiß nicht, ob man es gut erkennen kann, aber die Feder wird in der Statiklasche von einem roten "Klemmstift 15" innendrin fixiert. Alleine würde sie nicht halten.

Gruß,
Stefan