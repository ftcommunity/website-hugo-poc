---
layout: "image"
title: "Blick von vorne ins Leuchtraster"
date: "2015-12-28T19:08:43"
picture: "klartextuhrnachtrag3.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42618
- /details61f8.html
imported:
- "2019"
_4images_image_id: "42618"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42618 -->
Und dasselbe von vorne bei abgenommenem Anzeigeblatt.
