---
layout: "image"
title: "Elektronik - Silberlinge"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42390
- /details1bfb.html
imported:
- "2019"
_4images_image_id: "42390"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42390 -->
Unten sieht man die geringwertigsten 4 Bits des Zählwerks. Die beiden AND/NAND-Bausteine bilden zusammen ein AND/NAND mit sieben Eingängen: Die 125 in Binär ist nämlich 1111101. Bei dieser Kombination (alle Flipflops auf "1", per Definition in diesem Modell -9V wie bei den Silberlingen üblich, nur das zweite Flipflop von links auf "0") geht vom AND-Ausgang des rechten AND/NAND ein Signal zum Mono-Flop rechts oben. Das geht aber durch einen Taster, damit man einen Impuls zum Stellen der Uhr auch manuell erteilen kann.

Immer beim Übergang von Silberlingen ("0" = 0V, "1" = -9V) zu den E-Tecs/Electronics-Moduln ("0" = 0V, "1" = +9V) muss man umdenken. So geht z.B. beim Binärzähler innerhalb der Silberlinge immer der Q[nicht]-Ausgang zum CP-Eingang des nächsten Flipflops. Nach dem 4. Flipflop geht aber Q (und nicht Q[nicht]) zum E-Tec und wahrt so die korrekte Signalausrichtung.

Das Monoflop schaltet dann den später noch gezeigten Motor kurz ein, damit der die Uhr auf die nächsten fünf Minuten einstellt.
