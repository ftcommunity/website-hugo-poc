---
layout: "image"
title: "Rückseite"
date: "2015-12-20T13:46:57"
picture: "ft_ZeitwortUhr_back.jpg"
weight: "16"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Mini-USB", "Stromversorgung"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42532
- /detailsbed1.html
imported:
- "2019"
_4images_image_id: "42532"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42532 -->
Hier die Rückseite mit dem Arduino Nano Board (ATMEGA 328P). Stromversorgung über den Mini-USB-Anschluss.