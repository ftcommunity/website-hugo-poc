---
layout: "image"
title: "Elektronik - Überblick"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42389
- /details8936.html
imported:
- "2019"
_4images_image_id: "42389"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42389 -->
Die gesamte Uhr hängt am Wechselstromausgang eines alten ft-Trafos (der hinter der Elektronik untergebracht ist). Die Lampen werden über die 6,8 V Wechselspannung betrieben, die Elektronik vom Gleichrichterbaustein links oben mit geglätteten 9 V.

Das neue Electronics-Modul (direkt rechts neben den Silberlingen) zählt die 50 Hz - man beachte die Leitung vom Wechselstromeingang des Gleichrichters zum Electronics-Modul. Es ist außerdem so eingestellt, dass es durch 24 und durch 5 teilt. EIN FETTER DANK AN DIE KNOBLOCH GMBH, die das Electronics-Modul so weise einstellbar gemacht hat!

Insgesamt gibt es 50 Impulse pro Sekunde, davon 60 pro Minute, davon fünf pro Schaltintervall. Wir müssen also bis 50 * 60 * 5 = 15.000 zählen. 15.000 / 24 / 5 = 125. Von den Ausgangsimpulsen des Electronics-Moduls müssen also 125 Stück abgezählt werden, bis die Lampen das nächste Mal umgeschaltet werden müssen. Das erledigt ein 7-Bit-Binärzähler, aufgebaut aus 4 Stück hobby-4-Flip-Flop-Silberlingen und 3 als Flip-Flop eingestellte E-Tec-Moduln.
