---
layout: "image"
title: "Das Modell"
date: 2020-04-14T17:29:16+02:00
picture: "gone-uhr-komplett.JPG"
weight: "1"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

„Gone“ ist eine Zeiteinheit, die von der Wissenschaft als Alternative zum 24-Stunden-Tag erdacht wurde und auf dem metrischen System basiert.
Dieses Modell zeigt eine dieser Uhren mit der Zeitbasis eines Tages. Zeitgeber ist ein ftDuino.

Video zum Modell auf YouTube
https://youtu.be/yzO0Hn1wkxc

Einheiten:
Orb = 1000 Round (entspricht einem „Jahr“, wobei hier nicht das Erdenjahr angenommen wurde.)
Round = 100 Gone (entspricht einem „Tag“, wobei nicht der Erdentag ausschlaggebend ist)
Gone  (Grundeinheit der Zeitrechnung) Dazu entsprechend „dezi-Gone“ oder „milli-Gone“ oder „kilo Orb“.
Wissenschaftlich wurde ein „Gone“ aufgrund eines metrischen Vielfachen einer physikalischen Zeit-Grundeinheit erdacht. (Leider kann ich nicht mehr finden, was als Basis benutzt wurde). Ursprünglich ist unten „Mitternacht“, also 0 Gone und oben „High-Noon“, also 12:00 Uhr Mittag = 50 Gone. (Dann wäre 06:00 Uhr = 25 Gone und abends 18:00 Uhr = 75 Gone) eine Runde des kleinen (schnellen) Zeigers wäre aber NICHT eine Stunde, sondern 100stel Tag!

Das Modell
ist eine Gone-Uhr, die einen Erdentag als Zeitbasis verwendet:
1 Tag = 1 Round = 100 Gone
Der große Zeiger stellt die Gone dar, also eine Umdrehung pro Tag.
Der kleine Zeiger stellt die Centi-Gone dar, also eine Umdrehung pro Gone = 100 Umdrehungen pro Tag mit Unterteilung in 100 Schritten = 1 Schritt / Centi-Gone.