---
layout: "image"
title: "Nahaufnahme"
date: 2020-04-14T17:29:08+02:00
picture: "gone-uhr-nah.JPG"
weight: "5"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Science Fiction

Vor einigen Jahren habe ich einen Science Fiction geschrieben. Dazu ein Supplement, in dem Personen und Technik erklärt werden, wie auch ein Glossar. Darin habe ich die Zeiteinheiten „Orb“, „Round“, „Gone“ verwendet. Insgesamt basiert der SciFi auf viel Technik und Wissenschaft. Er ist ein dickes Buch, wer also viel lesen möchte und Fan von Star-Trek ist, der findet hier das richtige Buch.
Wie gesagt: das Supplement parallel zum Buch ….
(P.S.: ich möchte damit nur bekannt machen, wo man mehr über „Gone“ erfahren kann – bitte nicht als Verkaufswerbung meines Buches missverstehen, an dem Buch verdiene ich nichts.)
Und dieser SciFi hat mich dazu gebracht, diese Uhr einmal mit einem Erdentag auszutesten. Was lag also näher als sie mit fischertechnik zu bauen?

Buch:
„Elite1 – Europas Untergang“
Novum Verlag
Autor: Alex Trust
Ca. 780 Seiten
(Zu erhalten direkt bei mir.)

Supplement als PDF oder eBook zum kostenlosen Download:
http://www.alex-trust.com/supplement/

Übrigens: der Buchtitel „Europas Untergang“ hat nichts mit dem Kontinent Europa zutun, sondern mit dem Jupitermond Europa – der SciFi spielt im Weltall.