---
layout: "image"
title: "Getriebeebenen"
date: 2020-04-14T17:29:10+02:00
picture: "gone-uhr-seite.JPG"
weight: "4"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "Website-Team"
license: "unknown"
---

Sinn des Modells

Für mich waren 3 Aspekte wichtig das Modell zu bauen

1. ich wollte wissen, wie es sich anfühlt einen Tag in Gone auszudrücken.Losgelöst von Sekunden, Minuten und Stunden. Ich wollte wissen, ob es gut ist zu sagen „ich stehe um 27 Gone auf“ oder „um 54 Gone ist Mittagessen“ oder „noch 0,6 Gone, dann muss ich aus dem Haus“ oder „ich habe gerade 120 mGone die Zähne geputzt“. Ergebnis: spannend und sehr einfach danach zu leben.

2. Ich wollte mich mit dem Bau von Getrieben beschäftigen und dazu vor allem die großen Drehscheiben benutzen. Ebenso mit den Schneckengetrieben und deren Stabilität experimentieren. Auch das anschließende Miniaturisieren habe ich etwas geübt.

3. Ich wollte mit meinem (recht neuen) ftDuino eine praktische Anwendung haben. Die Programmierung klappt gut und schnell, ich brauchte nur 2 Stunden - sorry: ca. 7 Gone - (mit Test) um das Programm fertig zu stellen. (Hinweis: es war nicht nur der Taktgeber von 8640 ms, denn das dauert nur wenige Minuten, sondern auch eine Anzeige per Serieller Monitor, der den Tag / Round in unterschiedliche „Schichten“ einteilt, wie sie in meinem SciFi verwendet werden – s.u.)