---
layout: "image"
title: "Ansicht rechte Seite"
date: 2020-05-10T11:37:07+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Gleichzeitig enthält die Uhr das kleinste Uhrwerk, das mir bisher gelang. Außer einem Z10 und einem Z15 sind sämtliche Getriebeteile ausschließlich mit feinen Zähnen realisiert. Die groben Zähne des zweiten Z15 werden nur für die Führung des Minutenzeiger benötigt.

----------

View on right side

At the same time, this is the most compact clock gear I ever could create. Besides a Z10 (Z10 means a gear wheel with 10 teeth) and a Z15, the gear consists only of gear wheels with fine teeth. The large teeth of the 2nd Z15 are only used to guide the minutes hand.
