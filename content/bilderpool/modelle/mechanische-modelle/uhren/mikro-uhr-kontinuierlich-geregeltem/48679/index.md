---
layout: "image"
title: "Prototypen (1)"
date: 2020-05-10T11:37:11+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das war ein erster Prototyp der Uhr. Da war das Ziel noch, die Uhr insgesamt so winzig wie nur möglich zu bauen.

----------

Prototypes (1)

This was a first prototype of the clock. At its time, the goal was to create a clock as small as possible.
