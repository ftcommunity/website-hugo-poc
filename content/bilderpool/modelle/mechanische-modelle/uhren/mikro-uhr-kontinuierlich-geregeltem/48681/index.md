---
layout: "image"
title: "Kontrollierter Lauf unter Visual Studio"
date: 2020-05-10T11:37:13+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das feine am nanoFramework ist, dass man den ganzen .net- und Visual-Studio-Komfort nutzen kann. Hier ein Screenshot von Visual Studio 2017 mit einem Ausschnitt des Quellcodes. Das Programm läuft gerade, und der Debugger schaut live ins Board. Wir können Haltepunkte setzen, Variablen inspizieren und ändern und all die anderen angenehmen Dinge tun.

----------

Controlled execution using Visual Studio

The fine thing about nanoFramework is that you can use all the comfort that .net and Visual Studio have to offer. This screen shot shows Visual Studio 2017, showing a part of the clock's source code. The program is running, and the debugger is live-watching inside the board. We can set breakpoints, inspect and modify variables and use all the other pleasant features.
