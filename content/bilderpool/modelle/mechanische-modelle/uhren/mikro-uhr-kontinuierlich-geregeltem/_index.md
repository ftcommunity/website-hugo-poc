---
layout: "overview"
title: "Mikro-Uhr mit kontinuierlich geregeltem Gleichstrommotor"
date: 2020-05-10T11:36:53+02:00
---

In dieser Uhr läuft ein XS-Gleichstrommotor einfach permanent durch. Seine Betriebsspannung wird nach Messung der tatsächlichen Umdrehungsgeschwindigkeit permanent nachgeregelt. Die Software ist in .net/C# geschrieben und läuft auf dem nanoFramework direkt in einem Netduino 3 Microcontroller.

----------

Micro clock with continuously regulated DC motor

In this clock, a XS DC motor simply is continuously running. Its voltage gets permanently regulated by measuring the actual turnaround speed. The Software is written in .net/C# and runs on nanoFramework directly inside of a Netduino 3 micro controller.