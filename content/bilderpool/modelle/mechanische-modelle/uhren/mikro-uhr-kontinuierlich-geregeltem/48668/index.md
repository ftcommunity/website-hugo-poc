---
layout: "image"
title: "Abtastung (2)"
date: 2020-05-10T11:36:57+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zu jeder Umdrehung wird berechnet, wann der Kontakt idealerweise geschlossen werden sollte. Das ist n * 3600 / 22 / 14 Sekunden nach dem Start der Uhr der Fall, wobei n die einfach hochgezählte erwartete Anzahl von Umdrehungen ist. Der Zeitpunkt des Kontakts wird gemessen und anschließend berechnet, wieviel Spannung wohl auf den Motor gegeben werden muss, damit der nächste Kontakt möglichst pünktlich zum idealen Zeitpunkt stattfindet.

----------

Measurement (2)

For each turn, the time at which the contact should ideally happen gets computed. This is n * 3600 / 22 / 14 seconds after the start time of the clock, where n is the simply counted expected number of turns. The time of contact gets measured and then the controller computes the amount of voltage which should be put on the motor in order to reach the next contact in ideal time.
