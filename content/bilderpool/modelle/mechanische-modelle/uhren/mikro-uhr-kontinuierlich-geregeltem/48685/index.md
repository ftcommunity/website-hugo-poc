---
layout: "image"
title: "Getriebe (3)"
date: 2020-05-10T11:37:18+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von da geht es weiter nach hinten auf eine Kombination von 31915 Zangenmutter mit 35113 Spannzange, die beide 22 Zähne haben. Von der Zangenmutter weiter auf eine 31050 Metallachse 50 + Zahnrad Z44 m0,5 vom mot. 2, die wieder nur lose in einem BS7,5 steckt. Das ist also nochmal eine Untersetzung 1:2, und wir haben bisher (1:2) * (1:2) * (1:2) = 1:8.

----------

Gear (3)

The gear continues to the back side with a combination of 31915 claw nut with 35133 collet, which both have 22 teeth. The claw nut continues to a 31050 metal axis 50 with gear wheel Z44 m0, 5 of the mot.2 that, again, sits loosely in a BS7, 5. So we get another ratio of 1:2, and so far have (1:2) * (1:2) * (1:2) = 1:8.
