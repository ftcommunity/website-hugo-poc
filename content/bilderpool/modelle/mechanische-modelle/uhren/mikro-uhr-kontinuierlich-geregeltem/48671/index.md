---
layout: "image"
title: "Ansicht linke Seite"
date: 2020-05-10T11:37:01+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das größte an der Uhr ist das Zifferblatt, dass man durchaus auch einfach weglassen könnte, und die Zeiger, die auch kürzer ausgeführt sein könnten.

----------

View on left side

The largest parts of the clock are the clock face, which one could easily just omit, and the hands, which could be used shorter also.
