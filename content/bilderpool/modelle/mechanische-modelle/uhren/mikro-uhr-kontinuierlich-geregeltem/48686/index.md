---
layout: "image"
title: "Getriebe (2)"
date: 2020-05-10T11:37:19+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man, wie das Z44 die Drehung einfach ans feine Z22 des Z15 weitergibt Man beachte, dass das vom Stundenzeiger aus zum Getriebe gesehen auch eine 1:2-Untersetzung ist. Das Z44 steckt einfach lose in einem BS7,5. Es fällt nicht heraus, weil es vorne vom Z15 begrenzt wird.

Das kleine Zahnrad des Z44 hat ebenfalls 14 Zähne und geht auf die schwarze 31082 Rastachse + Zahnrad Z28 m0,5. Das ergibt eine Untersetzung von 14:28 = 1:2. Zusammen mit den 1:2 von eben ergibt das 1:4.

----------

Gear (2)

Here you see how the Z44 passes its turning to the fine 22 teeth of the Z15 (note that this is also a 1:2 reduction seen from the hours hand to the gear). The Z22 is sitting loose in the BS7, 5. It does not fall apart, however, because the Z15 will hold it in place.

The small gear wheel of the Z44 also has 14 teeth and continues to the black 31082 snapping axis with gear wheel Z22 m0, 5. This results in the gear transmission ratio of 14:28 = 1:2. Combined witht the 1:2 ratio mentioned above, this is 1:4.
