---
layout: "image"
title: "Prototypen (3)"
date: 2020-05-10T11:37:08+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Messung der Umdrehungen sollte so stattfinden: Die ft-Spiralfeder wird vom Z14 der waagerecht nach vorne zeigenden Schnecke pro Zahn vom direkt darüber befindlichen Metallkontakt abgehoben und wieder drauf prallen lassen. Rechts wird die Feder vom Stecker selbst im Metallkontakt fixiert.

Allerdings war das nicht zuverlässig genug, weil sich die Spiralfeder trotz allem mit der Zeit aus herauswand und so kein Kontakt mehr entstand.

----------

Prototypes (3)

The turning measurement should happen as follows: The fischertechnik spiral spring gets moved off and back onto the metallic contact above it by the small teeth of the Z14 of the worm gear horizontally pointing to the front. On the right side, the spring is hold in place by the plug itself.

However, this was not reliable enough, because the spiral spring worked its way out over time and thus no contact was made any more.
