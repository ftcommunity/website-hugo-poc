---
layout: "image"
title: "Getriebe (1)"
date: 2020-05-10T11:36:53+02:00
picture: "2019-01-02 Mikro-Uhr mit geregeltem Motorantrieb 09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die letzte Metallschnecke (die des Motors mitgezählt also die vierte) geht auf das zweite Einsteckzahnrad eines 31048 mot.2 Stufengetriebes. Das hat außen 44 Zähne, die direkt zur feinen Verzahnung des Z15 geht, das den Minutenzeiger trägt. Das hat 22 Zähne, und das ergibt von der gemessenen Schnecke die 14 Zähne des Getriebebocks und die 22 des Z15. Eine Umdrehung des Minutenzeigers muss 3600 Sekunden dauern, und deshalb also eine der zu messenden Schnecke 3600 / 14 / 22 Sekunden.

----------

Gear (1)

The last metallic worm gear (so, including the one of the motor itself, the 4th) continues to the 2nd plug-in gearwheel of a 31048 mot.2 gear box. On its outside, it has 44 teeth, leading directly to the fine teeth of the Z15 holding the minutes hand. That has 22 teeth, and so we get, counting from the measured worm, 14 teeth of the worm gear and 22 of the Z15. One turn of the minutes hand must take 3600 seconds, and so the worm being measured has to take 3600 / 14 / 22 seconds for one turn.
