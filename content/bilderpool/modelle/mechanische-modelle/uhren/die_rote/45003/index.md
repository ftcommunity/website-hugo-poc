---
layout: "image"
title: "Seitenansicht rechts III"
date: 2022-02-16T17:21:11+01:00
picture: "Bild_11_Seitenansicht_rechts_III.JPG"
weight: "11"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Und hier das Ganze nochmal von rechts oben.