---
layout: "image"
title: "Beleuchtung"
date: 2022-02-16T17:19:32+01:00
picture: "Bild_14_Beleuchtung.JPG"
weight: "14"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Spiel mit Licht und Schatten