---
layout: "image"
title: "Vorderansicht II"
date: 2022-02-16T17:18:00+01:00
picture: "Bild_2_Vorderansicht_II.JPG"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Idee mit den Säulen ist von einer Uhr, die im Clubblad 3/1993 des fischertechnikclub.nl von Evert Hardendood vorgestellt wurde.