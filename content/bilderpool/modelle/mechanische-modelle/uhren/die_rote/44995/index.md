---
layout: "image"
title: "Seitenansicht links I"
date: 2022-02-16T17:16:20+01:00
picture: "Bild_6_Seitenansicht_links_I.JPG"
weight: "6"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Gut zu sehen, die gezielte Beleuchtung aller Uhrbereiche. Über den Taster unten links wird das Stroboskop geschaltet. 