---
layout: "image"
title: "Zifferblatt"
date: 2022-02-16T17:16:25+01:00
picture: "Bild_4_Ziffernblatt.JPG"
weight: "4"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Hier kommen die neuen runden Bausteine zum Einsatz. Die beiden gelben S-Riegel-4 habe ich bei Stefan Roth gefunden.