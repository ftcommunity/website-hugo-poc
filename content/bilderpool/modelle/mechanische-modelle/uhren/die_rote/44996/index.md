---
layout: "image"
title: "Sekundenanzeige"
date: 2022-02-16T17:16:23+01:00
picture: "Bild_5_Sekundenanzeige.JPG"
weight: "5"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Hier wäre Platz für ein Pendel. Da der verwendete Antrieb das nicht braucht, habe ich den Platz für die Sekundenanzeige genutzt.