---
layout: "image"
title: "Seitenansicht rechts I"
date: 2022-02-16T17:16:13+01:00
picture: "Bild_9_Seitenansicht_rechts_I.JPG"
weight: "9"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die beiden hinteren Säulenpaare dienen mehr der Optik und dem Gesamtbild als dass sie eine tragene Rolle haben.