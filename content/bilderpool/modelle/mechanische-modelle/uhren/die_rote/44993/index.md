---
layout: "image"
title: "Seitenansicht links III"
date: 2022-02-16T17:16:15+01:00
picture: "Bild_8_Seitenansicht_links_III.JPG"
weight: "8"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Motorwelle und Z40-Welle sind kugelgelagert.