---
layout: "image"
title: "Vorderansicht I"
date: 2022-02-16T17:21:15+01:00
picture: "Bild_1_Vorderansicht_I.JPG"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Rote ist eine Uhr ganz in Rot gehalten und im Stil einer Kaminuhr. Sie hat im großen Ziffernblatt eine Minuten- und Stundenanzeige und ein waagerecht darunter liegendes Sekundenrad. Der Antrieb erfolgt über einen 50 Hz Motor.