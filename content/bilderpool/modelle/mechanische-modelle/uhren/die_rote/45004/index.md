---
layout: "image"
title: "Seitenansicht rechts II"
date: 2022-02-16T17:21:13+01:00
picture: "Bild_10_Seitenansicht_rechts_II.JPG"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Drehscheibe in der Getriebemitte sitzt auf der Minutenwelle, ist mit zwei Messingstücken gefüllt und dient als Gegengewicht zum Minutenzeiger. Damit sie Wirkung zeigt, muss sie bei jedem Stellen des Minutenzeigers entsprechend angepasst werden.   