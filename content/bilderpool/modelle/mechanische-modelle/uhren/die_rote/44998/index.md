---
layout: "image"
title: "Vorderansicht III"
date: 2022-02-16T17:16:28+01:00
picture: "Bild_3_Vorderansicht_III.JPG"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Der gelbe Verbinder hinter dem Sekundenrad markiert die 12. Die beiden Z20 oben links könnten auch durch eine Kette ersetzt werden.