---
layout: "image"
title: "Antrieb"
date: 2022-02-16T17:19:34+01:00
picture: "Bild_13_Antrieb_von_hinten.JPG"
weight: "13"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Klassischer 50 Hz Motor nach einer Idee von Rüdiger Riedel (ft:pedia 2/17) mit 12 Polpaaren. Damit der Läufer etwas größer wurde als bei Rüdiger, ist die Reihenfolge der Bausteine geändert in WS15 und 2*BS5. Bei den Magneten wurden oben goldfarbene und unten silberne verwendet. Damit die Elektromagneten nicht heiß werden, erfolgt die Stromversorgung mit dem alten ft Trafo.  