---
layout: "image"
title: "Rückseite"
date: 2022-02-16T17:19:37+01:00
picture: "Bild_12_Rueckseite.JPG"
weight: "12"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die schwarzen Stromverteilerleisten (unten im Bild) stammen von Andreas Tacke.