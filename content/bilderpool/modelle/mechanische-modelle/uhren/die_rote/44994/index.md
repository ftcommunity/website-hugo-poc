---
layout: "image"
title: "Seitenansicht links II"
date: 2022-02-16T17:16:18+01:00
picture: "Bild_7_Seitenansicht_links_II.JPG"
weight: "7"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
schlagworte: ["Uhr"]
uploadBy: "Website-Team"
license: "unknown"
---

Die vordere obere LED ist an Wechselstrom angeschlossen und dient als Stroboskop beim Anwerfen der Uhr. Nach einer Idee von Rüdiger Riedel in ft:pedia 4/16.