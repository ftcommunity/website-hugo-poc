---
layout: "image"
title: "Pneumatik-Orgie (2)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15282
- /details7719.html
imported:
- "2019"
_4images_image_id: "15282"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15282 -->
Hier ein leicht nach links versetztes Motiv: Man sieht das Ventil "hinten oben rechts" (wir blicken von hinten auf die Uhr). Ein Stockwerk tiefer kann man weitere Elektro-Ventile erahnen.
