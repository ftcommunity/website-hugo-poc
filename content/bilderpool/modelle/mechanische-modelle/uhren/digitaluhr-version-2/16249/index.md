---
layout: "image"
title: "Wagen (7)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen07.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16249
- /detailscbc5.html
imported:
- "2019"
_4images_image_id: "16249"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16249 -->
Drei oder vier Schläuche... ;-)
