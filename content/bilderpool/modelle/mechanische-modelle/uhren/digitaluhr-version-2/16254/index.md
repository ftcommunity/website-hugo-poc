---
layout: "image"
title: "Wagen (12)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen12.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16254
- /details854d-2.html
imported:
- "2019"
_4images_image_id: "16254"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16254 -->
Schräg von vorne rechts aufgenommen.
