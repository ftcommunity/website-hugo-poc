---
layout: "comment"
hidden: true
title: "7177"
date: "2008-09-17T03:09:59"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Da fällt mir ein Zitat ein, das ich vor gut 23 Jahren in meiner
Berufsausbildung gefunden habe:
"Fingerdicke Kabel mäandrieren durch den Salon."
(in Anspielung auf Kabel bei HiFi-Anlagen)

Es wird eben nichts mehr versteckt und blanke Technik prä-
sentiert. Bei meiner Eisenbahnbrücke 2006 sah's elektrisch
ja genauso aus...

Gruß, Thomas