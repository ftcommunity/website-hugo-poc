---
layout: "image"
title: "Pneumatik-Orgie (1)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15281
- /details7061-2.html
imported:
- "2019"
_4images_image_id: "15281"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15281 -->
Dieses Kuddelmuddel hier hat tatsächlich System: Nebeneinander sind sieben gleiche Kombinationen aus je einem Pneumatik-Doppelbetätiger (mittig), einem Öffner-Ventil (oben) und einem Schließer-Ventil (unten) angeordnet. Die Betätiger erhalten ihre Luft von den elektropneumatischen Ventilen. Sofern sie unter Druck stehen (das Ventil also eingeschaltet ist), betätigen sie mit ihrem Gummibalg die Ventile oben und unten. Der Öffner öffnet, der Schließer schließt: Der an diesen beiden Ausgängen angeschlossene Pneumatikzylinder wird umgesteuert.

Da zu jedem Betätiger ein Steuerschlauch, zu jedem der 14 Ventile eine Luftversorgung und von jedem der Ventile der Ausgang abgeleitet werden muss, wird es hier tierisch eng. Mehrere gleichzeitig auftretende Undichtigkeiten zu suchen kann einen an den Rand des Wahnsinns treiben. Ohne Pinzette und zuletzt sogar OP-Besteck geht hier praktisch nichts.

Obendrauf sitzt ein Einfachbetätiger, der gegen Federdruck eine Stange gegen den Taster rechts drückt. Das war ursprünglich als Druckabschaltung geplant, dient aber nur noch der Druckkontrolle: Die Uhr wartet ggf. darauf, dass genug Druck ansteht.

Der Schlauch, der dazwischen durchgeht, wird vom Kompressor gefüllt und ggf. von der Stange etwas eingedrückt. Damit haben wir wenigstens etwas Minderung des Durchflusses - das als Kompressor verwendete Inhaliergerät kann ich mangels ft-Netzschaltgerät (das gabs mal in den 1970er Jahren) nicht steuern.
