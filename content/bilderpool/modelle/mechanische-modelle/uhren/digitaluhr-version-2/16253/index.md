---
layout: "image"
title: "Wagen (11)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen11.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16253
- /details289f.html
imported:
- "2019"
_4images_image_id: "16253"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16253 -->
Und schließlich die Frontseite mit den sieben Schubgelenken.
