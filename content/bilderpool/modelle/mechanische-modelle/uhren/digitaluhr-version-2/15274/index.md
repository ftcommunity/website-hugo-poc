---
layout: "image"
title: "Eine Ziffer von hinten (mit Blitz)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15274
- /detailsbc29.html
imported:
- "2019"
_4images_image_id: "15274"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15274 -->
Hier dasselbe Motiv mit Blitz aufgenommen.
