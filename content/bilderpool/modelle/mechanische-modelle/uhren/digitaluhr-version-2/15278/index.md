---
layout: "image"
title: "Der Wagen von links"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15278
- /details6b90.html
imported:
- "2019"
_4images_image_id: "15278"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15278 -->
Man sieht den Pneumatikverhau links, aber auch den PowerMotor zum Verschieben des Geräteträgers nach vorne bzw. hinten. Der treibt einen "Getriebehalter mit Schnecke" an, welches auf ein Z15 geht, auf dessen Achse eine ft-Kurbel (die aktuelle Version, in schwarz) sitzt. Die bildet ein Exzenter, mit dem der Geräteträger geschoben wird. Zwei Endanschlagstaster vervollständigen das.
