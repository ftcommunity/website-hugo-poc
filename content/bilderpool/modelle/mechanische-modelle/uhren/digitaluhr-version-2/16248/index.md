---
layout: "image"
title: "Wagen (6)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen06.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16248
- /details64c8.html
imported:
- "2019"
_4images_image_id: "16248"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16248 -->
Ein Blick von hinten links auf die Stellmechanik. Die beiden nach unten zeigenden Taster im Vordergrund sind der für die Initialposition links außerhalb der Einerstelle der Minuten (der linke) und der für die Position bei einer Ziffer (der rechte).
