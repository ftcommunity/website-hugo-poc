---
layout: "image"
title: "Luftdruckversorgung"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15288
- /detailsd9e6.html
imported:
- "2019"
_4images_image_id: "15288"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15288 -->
Als Kompressor dient ein "PARI Inhalierboy". Dessen Pumpe bringt zwischen 0,3 und 0,5 bar Luftdruck und angenehm viel Luftstrom für die Versorgung der vielen Ventile und Zylinder. Leider muss der durchlaufen und macht etwas Krach, dafür passt ein älterer, hellblauer Pneumatikschlauch wie dafür gemacht in seine Tülle, die man hier sieht. Die neueren Schläuche sind etwas dicker und passen nicht hinein.
