---
layout: "image"
title: "Eine Ziffer von hinten (ohne Blitz)"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15273
- /detailseddc.html
imported:
- "2019"
_4images_image_id: "15273"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15273 -->
Hier eine Detailaufnahme der Zehnerstelle der Stunden (von hinten also die rechteste Ziffer).

Die senkrecht stehenden Segmente, die nach links/rechts verschoben werden, brauchen keine besondere Fixierung, lediglich ein paar Klemmringe als Anschlag. Die waagerechten Segmente benötigen aber eine Klemmung, damit sie von ihrer oberen Position nicht in die untere zurückfallen können. Da die Platzverhältnisse bei den drei Segmenten jeweils sehr unterschiedlich sind, ergaben sich nach viel Tüftelei auch verschiedene Lösungen für dieses Problem. Alle unteren Segmente werden mit einer ft-Kunststofffeder fixiert, die mittleren mit einer Mimik aus einer Statikstrebe und zwei S-Riegeln, und die oberen schließlich mit einem Federstab aus dem älteren Elektromechanik-Programm von fischertechnik.
