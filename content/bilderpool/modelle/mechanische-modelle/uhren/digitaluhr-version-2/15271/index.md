---
layout: "image"
title: "Gesamtansicht von links"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15271
- /details76de-2.html
imported:
- "2019"
_4images_image_id: "15271"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15271 -->
Man sieht ganz gut die Baugruppen:

- Elektronik im Vordergrund
- Anzeige, im Bild rechts
- Steuermechanik - der große Wagen da hinten
- Rückwand

Der senkrecht aus der Rückwand kommende Stab hält den Luftversorgungs-Schlauch so, dass er beim Hin- und Herfahren des Wagens nirgendwo hängt.
