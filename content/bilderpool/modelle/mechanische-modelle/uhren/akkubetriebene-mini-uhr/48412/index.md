---
layout: "image"
title: "Rechte Seite"
date: "2018-11-23T21:15:00"
picture: "akkubetriebeneminiuhr3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48412
imported:
- "2019"
_4images_image_id: "48412"
_4images_cat_id: "3546"
_4images_user_id: "104"
_4images_image_date: "2018-11-23T21:15:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48412 -->
Dies ist die Seite mit dem Getriebe: Der Schrittmotor geht über sein Z10 aufs Z25, das über Z10 und Z20 auf die Welle des Minutenzeigers geht. Der Schrittmotor (original ft) macht jede Minute 4 Schritte. Einfach durch drehen am Z25 kann man die Uhr ruck-zuck leicht stellen.

Die Untersetzung 1:12 vom Minuten- auf den Stundenzeiger geht über 2 x 1:2 von einem Ritzel (das hat 22 feine Zähne) auf eine mot.2-Achse (die hat 44 Zähne), und schließlich 1:3 von Z10 auf Z30. Das schwarze Zwischenzahnrad ist von einem S-Motor-Kasten.
