---
layout: "image"
title: "Digitaluhr Seitenansicht"
date: "2015-10-01T18:18:47"
picture: "Digitaluhr_Seitenansicht.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42028
- /details5235-2.html
imported:
- "2019"
_4images_image_id: "42028"
_4images_cat_id: "3123"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42028 -->
Positionierung links und rechts durch Encodermotor

Positionierung vor und zurück über Lichtschranke und Kette (die Kettenglieder unterbrechen die Lichschranke, wenn der Schlitten fährt)
