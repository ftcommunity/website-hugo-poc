---
layout: "image"
title: "Digitaluhr Detail Ziffern"
date: "2015-10-01T18:18:47"
picture: "Digitaluhr_Detail_Ziffern.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42029
- /detailsa469.html
imported:
- "2019"
_4images_image_id: "42029"
_4images_cat_id: "3123"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42029 -->
Oben in den Ziffern sind Metallstangen.
So kann der Elektromagnet die Ziffern anheben.
