---
layout: "image"
title: "11-Teststand"
date: "2010-03-15T16:56:38"
picture: "11-Teststand.jpg"
weight: "11"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26696
- /detailsa9a8-2.html
imported:
- "2019"
_4images_image_id: "26696"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26696 -->
Hier der Teststand in der Seitenansicht.

Das angehängte Gewicht sind hier 108 g. In 15 min senkt es sich und 35 mm ab. Das macht einen Leistungsumsatz von gerade mal noch 40 Mikrowatt. Das ist bisheriger Rekord, bedenkt man, daß zwei zusätzliche Wellen gedreht werden müssen.

Die Bolzen der Hemmung schubsen die Rollen des Hemmungsankers ganz behutsam. Die Maschine arbeitet fast unhörbar und schier unendlich zart.
