---
layout: "comment"
hidden: true
title: "11118"
date: "2010-03-02T16:55:04"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Martin,
ich verfolge still interessiert auch deine Modellentwicklung mit dem Motiv, ob du da noch eine Möglichkeit offen lässt, zum Thema "Hemmung" irgend wann auch mal was Eigenständiges kreieren zu können. Wie du ja weisst, gibt es oft nur einen optimalen Lösungsweg. Das hier dürfte deiner werden! Ich freue mich schon auf deine 48-Std-Uhr ...
Gruss, Ingo