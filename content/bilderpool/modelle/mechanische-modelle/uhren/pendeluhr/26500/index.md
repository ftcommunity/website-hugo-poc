---
layout: "image"
title: "01-Hemmung"
date: "2010-02-21T22:18:18"
picture: "01-Hemmung.jpg"
weight: "1"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26500
- /details3abf-2.html
imported:
- "2019"
_4images_image_id: "26500"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26500 -->
Hier der erste Versuch einer einfachen, symmetrischen Rollen-Bolzen-Hemmung. Eine ähnliche Anordnung hat es hier in der ftcommunity schon mal gegeben.

Die beiden Hemmungsrollen sind vorgeschoben, um ihre Anordnung auf den Achsen besser zu sehen. Sie gehören aber zwischen die beiden schwarzen Scheiben, die die Bolzen der Hemmung tragen.

Ein erster Test dieser Hemmung ergibt, daß sie mit einem Pendelwinkel von fast 10 Grad arbeitet. Das Antriebsgewicht von 170 g senkt sich innerhalb von 220 s um 10 cm. Damit verbrät diese Hemmung ca. 800 Mikrowatt Leistung. Das ist zuviel. Mit einem Meterkilogramm liefe die Uhr gerade mal 3,5 Stunden.
