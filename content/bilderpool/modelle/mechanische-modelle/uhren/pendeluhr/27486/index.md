---
layout: "image"
title: "14-Uhrwerk"
date: "2010-06-13T13:46:58"
picture: "14-Uhrwerk.jpg"
weight: "14"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27486
- /details64c3.html
imported:
- "2019"
_4images_image_id: "27486"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T13:46:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27486 -->
Das hier ist jetzt das neue Uhrwerk, im Augenblick jedoch noch ohne Zeigerwerk.

Die Gesamtübersetzung vom Hemmungsrad bis zu den Schnurlaufrollen, wo die Antriebsgewichte wirken, beträgt insgesamt 256:1. Ab der Hemmungswelle: 1:4, 1:4, 1:4, dann 1:2 (Minutenwelle) und nocheinmal 1:2 auf die Antriebswelle.

Oben links sitzt der Antriebsmotor, der im Normalbetrieb der Uhr die linke Schnurlaufrolle festhält. Die rechte Schnurlaufrolle treibt das Uhrwerk an und die Hemmung sorgt für einen gleichmäßigen Lauf.

Die Welle, die links noch hinter dem Motor zu erkennen ist, ist die Minutenwelle. Diese dreht sich je Stunde einmal. Aufgrund der Übersetzung von 1:128 auf die Hemmung mit 12 Teilungen muß das Pendel die Schwingungsdauer von 75/32 Sekunden machen. (Das darf gerne nachgerechnet werden.)

Am Pendelstab (links vor der Hemmung) sind die beiden Hemmungsrollen zu erkennen, die jeweils abwechselnd das Hemmrad aufhalten. Schieben die Bolzen des Hemmungsrades die Hemmungsrollen beiseite, dann geht das als Antriebsenergie auf das Pendel über.
