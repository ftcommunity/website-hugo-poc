---
layout: "image"
title: "18-Justage"
date: "2010-06-13T14:58:59"
picture: "18-Pendel2.jpg"
weight: "18"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/27490
- /detailse7d4.html
imported:
- "2019"
_4images_image_id: "27490"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T14:58:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27490 -->
Ein Blick unter das Pendel: die beiden Halbgewichte sind auf einer ft-Schnecke aufgelagert, die in ihrer Bohrung jeweils eine Stahlkugel trägt. Eine Umdrehung der Schraube hebt das Gewicht um 2 mm an.

Leider können die Schrauben das Gewicht nicht so tragen, sondern die Schnecken drehen sich unter der Gewichtseinwirkung von alleine zurück. Deshalb müssen sie geklemmt werden, was mit den dreieckigen Klötzen geschieht.

Die Verstellung um einen Zahn verschiebt den Pendelschwerpunkt ungefähr um 0,1 mm und verändert die Pendelgeschwindigkeit so, daß es auf einen Tag ca. 2 Sekunden ausmacht.
