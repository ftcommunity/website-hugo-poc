---
layout: "image"
title: "05-Hemmung"
date: "2010-02-21T22:18:19"
picture: "05-Hemmung.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26504
- /details64fc-2.html
imported:
- "2019"
_4images_image_id: "26504"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26504 -->
Langsam wird es anspruchsvoller: In diesem Aufbau ist das Pendel mechanisch von der Hemmung entkoppelt. Es hängt jetzt als Stabpendel (4 mm Welle) an einem Kreuzgelenk.

Die oszillierende Welle der Hemmung trägt an ihrem vorderen Ende jetzt ein kurzes Pendel, welches an seinem unteren Ende eine kleine Gabel aufweist. Diese Gabel umfaßt die eigentliche Pendelstange, die in diesem Bild nur verkürzt zu sehen ist. Das eigentliche Stabpendel der letzten Tests hat eine Länge von 135 cm und paßt natürlich nicht mehr ins Bild.

Der Trick besteht darin, der Antriebshebelarm des antreibenden Pendels dreimal kürzer ist, als die Entferung des Aufhängepunktes des getriebenen Pendels vom Antriebspunkt. Deshalb kann das Pendel einen dreimal kleineren Pendelwinkel ausführen, als die oszillierende Welle der Hemmung.

Im Ergebnis heißt das, daß das 135 cm lange Pendel nur 2,5 cm nach rechts und nach links ausschlägt. Die Amplitude ist damit gerade noch 1 Grad. Die Hebung des Pendels ist stark verringert.

Bestwert bis jetzt: 170 Mikrowatt Leistungsbedarf. Mit dem Meterkilogramm läuft die Hemmung jetzt schon 16 Stunden.

Nächste Schritte: Das Pendel auf Schneiden reibungsfrei lagern, evtl. die Hemmungsräder, die in die Bolzen fassen, durch Kugellager ersetzen.
