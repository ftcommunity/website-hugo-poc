---
layout: "comment"
hidden: true
title: "18578"
date: "2014-01-16T10:55:58"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ich habe doch diese Uhr erst diese Woche gesehen! Kann das sein, daß sie im neuen Foyer bei Ralf Knobloch steht? Ich stand jedenfalls staunend davor und ich kann gar nicht in Worte fassen wie fasziniert ich alleine die Statik angeschaut habe.

Klasse, Klasse und nochmal Klasse.