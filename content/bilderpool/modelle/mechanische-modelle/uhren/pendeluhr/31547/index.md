---
layout: "image"
title: "27-Abschlußbild"
date: "2011-08-07T17:42:21"
picture: "27-Uhr.jpg"
weight: "27"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/31547
- /detailsed78-2.html
imported:
- "2019"
_4images_image_id: "31547"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2011-08-07T17:42:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31547 -->
Zum Schluß das "Offizielle Abschlußfoto"

Auf den ersten Blick wirkt die Uhr etwas flau. Die Aufnahme ist als HDR gemacht. Der Kontrast zwischen Fenster mit Sonne und Zifferblatt sind 6 Belichtungsstufen. Deshalb habe ich in das Bild einen Gesamtkontrastumfang von 10 Belichtungsstufen hineingepackt.
