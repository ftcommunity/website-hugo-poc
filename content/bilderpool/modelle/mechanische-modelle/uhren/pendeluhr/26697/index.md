---
layout: "image"
title: "12-Teststand"
date: "2010-03-15T16:56:38"
picture: "12.Teststand.jpg"
weight: "12"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/26697
- /detailsdaa7-2.html
imported:
- "2019"
_4images_image_id: "26697"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26697 -->
Hier der ganze Teststand.

Das Pendel ist 120 cm lang und macht 30 Schwingungen in der Minute. Die Pendelamplitude am unteren Ende sind gerade mal noch 15 mm.

Wenn man der Maschine zuschaut, meint man zunächst, sie stünde. Aber sie läuft stabil.
