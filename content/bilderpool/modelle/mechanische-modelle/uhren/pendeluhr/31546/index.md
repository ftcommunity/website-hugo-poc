---
layout: "image"
title: "26-Making of"
date: "2011-08-07T17:42:21"
picture: "26-Making_of.jpg"
weight: "26"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/31546
- /details5444.html
imported:
- "2019"
_4images_image_id: "31546"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2011-08-07T17:42:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31546 -->
Aufzug in die Senkrechte

Ruhige Kameraaufnahmen brauchen ein Stativ. Dann bewegt sich aber nichts. Dieser Aufzug hat immerhin eine senkrechte Kamerafahrt ermöglicht.

Ruckelfrei lief das erst, als ich die Seilwinde aufgegeben und das Gewicht der Kamera und Aufzugskabine mit einem Gegengewicht austariert hatte. So brauchte ich nur die Umlenkrolle anzutreiben und das hat dann gefruchtet.
