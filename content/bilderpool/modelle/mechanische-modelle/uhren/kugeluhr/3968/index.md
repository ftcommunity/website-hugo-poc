---
layout: "image"
title: "Kugeluhr Steuerung"
date: "2005-04-08T20:54:59"
picture: "08-Steuerung.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3968
- /details371a-2.html
imported:
- "2019"
_4images_image_id: "3968"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3968 -->
In echter Digitaltechnik steuert dieser Drahtverhau die Uhr komplett.
Oben rechts drei Digitalanzeigen für Stunde, Minute und Sekunde. Dazwischen alle erdenklichen Gatter, Flankendetektoren, Speicher-Flipflops und natürlich die Relais zur Ansteuerung der Motore.