---
layout: "image"
title: "Kugeluhr Rücklauf"
date: "2005-04-08T20:54:59"
picture: "05-Rcklauf.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3965
- /detailsb49a.html
imported:
- "2019"
_4images_image_id: "3965"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3965 -->
Der Querbalken staut die Kugeln auf. Zur vollen Stunde hebt er sich auf der linken Seite an und um Mitternacht ganz hoch. Dann rollen die Kugeln zurück in den Förderkorb.