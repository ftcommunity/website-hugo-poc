---
layout: "image"
title: "Kugeluhr Aufzug"
date: "2005-04-08T20:54:59"
picture: "06-Aufzug.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/3966
- /details1acc-3.html
imported:
- "2019"
_4images_image_id: "3966"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3966 -->
Der Aufzug bringt die Kugeln wieder nach oben. Der Förderkorb entleert automatisch in die Kugelmaschine.