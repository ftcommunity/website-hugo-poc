---
layout: "image"
title: "Antrieb"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46835
- /detailsad1f.html
imported:
- "2019"
_4images_image_id: "46835"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46835 -->
Mit jeder Sekunde zieht immer abwechselnd einer der Magneten immer anders herum gepolt an den Magneten (für eine genaue Beschreibung des Verfahrens siehe https://www.ftcommunity.de/details.php?image_id=27014). Die Magnete sind so justiert, dass immer einer einen Magneten direkt vor sich hat und der jeweils andere genau die Lücke zwischen zwei Magneten (also den Zahn der Z30). In diesen Halbschritten werden aus 30 Magneten also 60 Schritte, und wir erhalten einen völlig geräuschlosen Direktantrieb der Sekundenwelle.

Ich glaube, ich muss doch mal einen ft:pedia-Artikel über die Sache schreiben.
