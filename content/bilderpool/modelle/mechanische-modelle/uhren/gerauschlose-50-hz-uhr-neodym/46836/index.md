---
layout: "image"
title: "Blicke ins Getriebe (1)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46836
- /detailsd29a.html
imported:
- "2019"
_4images_image_id: "46836"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46836 -->
In der Bildmitte kann man hier das hintere Z15 des Minutenzeiger-Trägers sehen, wie es von der Kette vom Z10 oben getrieben wird.
