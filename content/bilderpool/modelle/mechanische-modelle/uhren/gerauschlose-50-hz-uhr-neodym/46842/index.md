---
layout: "image"
title: "Blicke ins Getriebe (7)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46842
- /detailse48b-2.html
imported:
- "2019"
_4images_image_id: "46842"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46842 -->
Links und rechts neben der Kette und des Z40 sieht man hier auch, wie die Führung des Stunden-Z30 über BS15 und Federnocken mit den Bausteinen verbunden ist.
