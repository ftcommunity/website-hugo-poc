---
layout: "image"
title: "Blicke ins Getriebe (4)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46839
- /details759d.html
imported:
- "2019"
_4images_image_id: "46839"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46839 -->
Der Diamantring stammt direkt von Cartier ;-) Das sieht im echten Modell wirklich sehr edel aus, wenn der sich im Sekundentakt ein wenig weiterdreht.

Es ist eine ziemliche Fummelei, die vielen Magneten auf die Z30 zu bringen, weil die so stark sind. Beim kleinsten Ausrutscher macht es ZACKZACKZACK, und man hat eine schöne lange Stange von 30 Magneten auf dem Tisch rumliegen. Letztlich ging es erträglich einfach so:

1. Beide Z30 festziehen.

2. Gummi recht stramm drum rum.

3. Mit dem ft-Schraubendreher an einer Stelle zwischen die Zähne fahren und das Gummi vollständig (alle Wicklungen!) anheben

4. Da drunter ein Ende der Propellerschraube (32247) hebeln. Dann kann der metallene und also magnetische Schraubendreher nämlich weg und man zieht mit ihm nicht versehentlich schon eingesetzte Magnete wieder heraus.

5. Ab da immer Zahn für Zahn per Propellerschraube das Gummi etwas anheben und den nächsten Magneten einsetzen. Schön auf immer abwechselnde Polarität achten. Und mit dem nächsten Magneten immer schön von der unbestückten Seite her ankommen, sonst zieht der auch gleich wieder einen Magneten heraus.

Und ich wiederhole fischerfriendmans Warnung: Neodym-Magnete sind saumäßig stark und gehören NICHT IN DIE HÄNDE KLEINER KINDER. Besonders das Verschlucken mehrerer Magnete kann übelst ausgehen!
