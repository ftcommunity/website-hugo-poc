---
layout: "image"
title: "Das herausgenommene Uhrwerk"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46990
- /details6956.html
imported:
- "2019"
_4images_image_id: "46990"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46990 -->
Das Uhrwerk ist also ein kompakter Getriebeblock, der einfach in die Frontplatte eingehängt und nur mit den beiden BS7,5 vom vorhergehenden Bild links und rechts an den Federnocken befestigt wird.
