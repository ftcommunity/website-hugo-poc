---
layout: "image"
title: "Die Rückseite ohne Kabel"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46987
- /details9922-2.html
imported:
- "2019"
_4images_image_id: "46987"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46987 -->
Aller abnehmbaren Kabel beraubt sieht die Sache so aus.
