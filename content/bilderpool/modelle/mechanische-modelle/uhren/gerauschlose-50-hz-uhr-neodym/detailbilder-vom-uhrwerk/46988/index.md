---
layout: "image"
title: "Trafo herausgenommen"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46988
- /details10bb.html
imported:
- "2019"
_4images_image_id: "46988"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46988 -->
Den Trafo kann man nach unten aus der Uhr herausschieben.
