---
layout: "image"
title: "Die Unterseite"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46991
- /detailscfad-3.html
imported:
- "2019"
_4images_image_id: "46991"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46991 -->
Blick auf das Uhrwerk von unten.
