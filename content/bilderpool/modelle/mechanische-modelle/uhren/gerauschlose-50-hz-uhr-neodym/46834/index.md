---
layout: "image"
title: "Elektronik"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46834
- /detailsc84a.html
imported:
- "2019"
_4images_image_id: "46834"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46834 -->
Von einem der beiden Wechselspannungspole des Netzteils geht eine Leitung zum oberen der beiden Electronics-Module an I1 (linker unterer Anschluss). Das Modul ist - wie in den Vorgängeruhren schon - als zweimaliger Teiler 1:5 eingestellt. Immer die zweiten Motorausgänge liefern das nochmal 1:2 geteilt. Genau das wird hier gemacht: Wir teilen die 50 Hz zweimal durch 10, also insgesamt nicht etwa durch 50, sondern durch 100, und erhalten so 0,5 Hz am rechten oberen Ausgang des ersten Moduls.

Dieser Ausgang geht zweimal auf je ein D-Flipflop im unteren Electronics-Modul (wer's nachbauen möchte: Ein MiniBots-Modul kann das auch, oder ersatzweise zwei E-Tec-Module). Das eine Flipflop schaltet immer bei steigender Flanke um, das zweige bekommt dasselbe Signal invertiert zugeführt und zählt also immer bei abfallender Flanke. Damit haben wir bei 0,5 Hz bei jeder auf- oder absteigenden Flanke einen Umschaltvorgang - und also wieder pro Sekunde genau eine Änderung.

Die Negation des Signals geht so: Das Ausgangssignal des oberen Moduls geht über einen 100 k?-Widerstand zur Basis eines Transistors vom Electronics-Baukasten. Der sitzt auf dem 15x30-Träger oben zwischen den beiden Widerständen. Sein Emitter ist mit "-" verbunden, sein Kollektor über einen 10 k?-Widerstand mit "+". Solange der Transistor nicht durchschaltet, liegt der Kollektor also vom 10k-Widerstand nach + gezogen. Sobald die Basis mit + beaufschlagt wird, schaltet der Transistor nach "-" kurz - damit haben wir ein Nicht-Glied.

Der Taster rechts oben steuert, welches Signal denn tatsächlich den Flipflops zugeführt wird: In Ruhestellung der rechte obere Ausgang des oberen Electronics-Moduls, also die 0,5 Hz. Drückt man den Taster, wird stattdessen der linke obere Ausgang verwendet. Der teilt die 50 Hz ja anstatt wie normal durch 100 nur durch 5, sodass wir die Uhr mit 20-facher Geschwindigkeit laufen lassen können. Um die Uhr also eine Minute vor zu stellen, muss man den Taster drei Sekunden lang drücken. Auf dem Video, was noch folgt, kann man das gut sehen.
