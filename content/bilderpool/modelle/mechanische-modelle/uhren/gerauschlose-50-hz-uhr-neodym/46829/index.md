---
layout: "image"
title: "Frontansicht"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46829
- /details90a2.html
imported:
- "2019"
_4images_image_id: "46829"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46829 -->
Diese Uhr läuft geräuschlos und hat einen im Sekundentakt gepulsten Direktantrieb des Sekundenzeigers. Über einen Rotor und zwei Elektromagnete läuft dieselbe Vier-Phasen-Ansteuerung, nur hier unter Verwendung kleiner 4 x 10 mm-Neodym-Magnetstifte. Die Ansteuertechnik ist ansonsten identisch mit https://www.ftcommunity.de/categories.php?cat_id=1943 (wo sie auch ausführlich beschrieben wird), https://www.ftcommunity.de/categories.php?cat_id=3172, https://www.ftcommunity.de/categories.php?cat_id=3416 und https://www.ftcommunity.de/categories.php?cat_id=3464.
