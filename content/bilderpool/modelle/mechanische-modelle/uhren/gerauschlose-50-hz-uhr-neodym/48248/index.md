---
layout: "image"
title: "Schaltplan"
date: "2018-10-20T17:10:10"
picture: "schaltplan1.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48248
- /details7875.html
imported:
- "2019"
_4images_image_id: "48248"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2018-10-20T17:10:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48248 -->
Nicht schön, aber selten, und besser spät als nie: Hier der Schaltplan der Uhr nachgereicht. Die Mimik mit dem Transistor und den zwei Widerständen (beide aus dem ft-electronics-Kasten) bildet das benötigte Nicht-Glied.
