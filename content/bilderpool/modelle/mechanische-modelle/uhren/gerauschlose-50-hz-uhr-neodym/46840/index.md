---
layout: "image"
title: "Blick ins Getriebe (5)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46840
- /detailsd96b.html
imported:
- "2019"
_4images_image_id: "46840"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46840 -->
Hier sieht man nochmal gut:

- Die Z15/Z10-Kombination unter dem Z30 des Stundenzeigers.

- Die seitliche Führung des ansonsten völlig lose auf dem Z10 stehenden Z30. Die besteht aus links und rechts je einem Baustein 5 15x30 (35049) und einer E-Magnet-Führungsplatte 15x20 (32455).

- Die beiden zwischen Z15 sitzenden Zangenmuttern, in deren Innerem sich eine Hülse 15 befindet und die beiden hinreichend kraftschlüssig miteinander verbindet. Alle fünf Bauteile drehen ganz leichtgängig auf der Sekundenachse. Das hintere Z15 wird von der Kette angetrieben, das fordere dreht den Sekundenzeiger über dessen S-Riegel.
