---
layout: "comment"
hidden: true
title: "23731"
date: "2017-10-23T07:18:39"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Okay, wenn Du nix hören magst, dann schreibe ich es halt:

Was nutzen denn die schönen Kabelhalter, wenn die Strippen da reingespannt werden wie Wäscheleinen? Den Zentimeter extra hätte die ein oder andere Leine auch noch vertragen.

Ach übrigens, wo wir schon dabei sind: es gibt da im Originalzubehör so praktische Papierstreifen mit Drahteinlagen. Die sind leider in der ftdb ihrer Artikelnummer beraubt worden, eignen sich aber nichtsdestotrotz, anstelle ft-fremdem Spiralschlauch, ebenso prima um die Einzeladern zu bündeln und den Kabelbund noch ordentlicher von den Zähnen des Getriebes wegzuhalten.

;)
H.A.R.R.Y.