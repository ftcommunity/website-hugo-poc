---
layout: "image"
title: "Frontansicht"
date: "2010-04-30T00:11:56"
picture: "fluesterleisepraezisemechanischedigitaluhr01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27008
- /detailsc9c7-2.html
imported:
- "2019"
_4images_image_id: "27008"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27008 -->
Dies ist eine Digitaluhr fürs Wohnzimmer - ihr Betriebsgeräusch ist kaum wahrnehmbar. Das klappt deshalb, weil keinerlei schnell bewegliche Teile darin vorkommen - alles geht ganz langsam. Aber dennoch präzise: Sie bezieht ihre Zeitbasis aus dem supergenauen 50-Hz-Stromnetz. Die Anzeige funktioniert rein mechanisch, und sie kommt gänzlich ohne Computer oder Interfaces aus. Die einzigen nicht Original-fischertechnik-Teile darin sind bedruckte Papierstreifen für die Ziffernanzeige und zwei kurze Stückchen Faden, aber das kommt bei den weiteren Bildern noch ausführlich.

Ich hatte mal eine ähnlich funktionierende Nachttischuhr namens "Digi-Glo": Sie wurde von einem relativ leisen Motor angetrieben (vermutlich ein vom Stromnetz getakteter Wechselstrommotor), der ein Sekundenrad antrieb und kurz vor der vollen Minute begann, via Übertrag die Einer- und Zehnerräder der Minuten und schließlich ein Stundenrad um eine Ziffer weiter zu drehen. Dabei änderte die Uhr ihr Betriebsgeräusch etwas wegen der wenn auch geringen, so doch nicht verschwindenden Last während des Schaltvorgangs. Das brachte eine Tante von mir, die mal in meinem Zimmer schlafen wollte, um eben ihren Schlaf. Leiser als diese fischertechnik-Uhr war das echte Vorbild auch nicht.

Ein Video des Modells gibt's unter http://www.youtube.com/watch?v=d7VekGj-gIE.
