---
layout: "comment"
hidden: true
title: "11461"
date: "2010-05-03T17:19:45"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Hi Stefan, da dein neuer Motor genau 30 mal langsamer läuft als dein alter Motor, fängt deine neue Uhr also nach 90 Tagen an zu quietschen. Als kleiner Trost, - das Quietschen müsste eigentlich in einer 30-fach niedrigeren Frequenz sein. Nehmen wir mal an die Quietschfrequenz deiner alten Uhr war 13,2 KHz dann würdest du am 29.07.2010 den Kammerton A wahrnehmen. D.h. es ist alles gar nicht so schlimm.
Ach ja, tolles Modell übrigens.

Viele Grüße, Ralf