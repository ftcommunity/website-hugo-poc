---
layout: "image"
title: "Überblick"
date: "2010-04-30T00:11:57"
picture: "fluesterleisepraezisemechanischedigitaluhr02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27009
- /detailsc458.html
imported:
- "2019"
_4images_image_id: "27009"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27009 -->
Die Uhr besteht aus drei Baugruppen:

Hinten links die aus Silberlingen bestehende Elektronik, die die 50 Hz in eine niedrigere Frequenz wandelt und zwei E-Tec-Moduln als geräuschlose Treiberbausteine für den Selbstbau-Elektromotor mit zwei Elektromagneten - rechts hinten - ganz langsam anzusteuern.

Der Rotor dieses Motors mit seinen 12 Dauermagneten dreht sich nur 25 Mal pro Minute. Das war das langsamste, was noch genügen Drehmoment ergab, um alles Nachfolgende antreiben zu können. Eine Umdrehung dauert damit genau 2,4 Sekunden. Das ist so gemächlich, dass die Drehung fast kein Geräusch verursacht - auch nach tagelangem Betrieb fangen die Lager des Rotors nicht etwa an zu quietschen. Vergleiche hierzu meine erste 50-Hz-Analoguhr, deren Rotorlager durch die 12,5 Umdrehungen pro Sekunde nach gut 3 Tagen zu quietschen begannen (http://www.ftcommunity.de/categories.php?cat_id=235).

Im Vordergrund sieht man - von rechts nach links - die Walzen Anzeige. Die sind jeweils mit bedruckten und zugeschnittenen Papierstreifen, die an ihren Enden einfach zwischen Bausteine eingeklemmt sind, versehen. Von rechts nach links sind das die Walzen für die Sekunden (00 - 59, in 5-Sekunden-Schritten beschriftet), die Einerstelle der Minuten (0 - 9, mithin also in 10 Stellungen zu unterscheiden), die Zehnerstelle der Minuten (zwei Mal 0 - 5, also in 2 * 6 = 12 Stellungen zu unterscheiden) und schließlich die Stunden (00 - 23, also in 24 Stellungen zu unterscheiden).

Nur das Sekundenrad läuft ständig (sozusagen analog). Die anderen drei werden während der ca. 6 Sekunden vor der vollen Minute durch ein Getriebe mit Mitnehmern und geeigneten, ganz reizvollen Untersetzungen um genau den richtigen Winkel weiter gedreht. Erst dadurch verdient sich das Maschinchen den Namen Digitaluhr.

Vorne ist noch eine Blende angebracht, so dass man die gerade gültigen Zahlen ablesen kann. Der Rest blieb unverkleidet, um den freien Blick auf die Technik nicht zu trüben.
