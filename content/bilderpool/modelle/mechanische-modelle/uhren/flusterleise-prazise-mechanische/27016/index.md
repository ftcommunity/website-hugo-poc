---
layout: "image"
title: "Untersetzung auf eine Umdrehung pro Minute"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27016
- /details5bf2.html
imported:
- "2019"
_4images_image_id: "27016"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27016 -->
25 Umdrehungen pro Minute sind 5 * 5 Umdrehungen pro Minute. Die Untersetzung auf genau eine Umdrehung pro Minute, die für das Sekundenrad benötigt wird, gelingt also mit zwei kaskadierten Untersetzungen 5:1. Die ist mit je einem Z10 und einem Eigenbau-Z50 realisiert. Um je zwei mittels gelochter Statikstreben 60 auch seitlich fixierten fischertechnik-Innenzahnräder ist je eines der Gummi-Raupenbänder aus den Frühzeiten von fischertechnik gelegt. Da herum passt eine Kette aus genau 50 Kettengliedern ziemlich genau.

Um jeglichen sich über die Zeit ja aufaddierenden Schlupf zu unterbinden, der zu einem Nachgehen der Uhr führen könnte, ist die Kette sicherheitshalber jeweils mit einem kurzen Stück Faden mit dem Innenzahnrad verbunden. Der theoretisch mögliche Schlupf wird dadurch irgendwann absolut begrenzt - dann nämlich, wenn er die Länge des angebrachten Fadens ganz ausgereizt hätte.

Das zweite (hier: hintere) Z50 dreht sich nun also in einer Minute ganz genau ein Mal, ohne irgendwelche kumulativen Fehler.
