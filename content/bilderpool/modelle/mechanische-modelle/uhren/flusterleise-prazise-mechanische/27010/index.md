---
layout: "image"
title: "50-Hz-Steuerung"
date: "2010-04-30T00:11:58"
picture: "fluesterleisepraezisemechanischedigitaluhr03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27010
- /details1518.html
imported:
- "2019"
_4images_image_id: "27010"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27010 -->
Wie man an der Kabelführung sehen kann, wird der obere dieser beiden Elektromagnete (übrigens die älteren Modelle mit dem Metallbügel, die hier völlig genügen) direkt von der Wechselstromversorgung eines alten ft-Trafos (der noch einen echten Wechselspannungsausgang hat und nicht nur eine gleichgerichtete pulsierende Spannung liefert) versorgt. Mit seinem darunter befestigten Gegenstück bildet er einen kleinen Trafo, der zur galvanischen Trennung zum hobby-4-Grundbaustein dient, der an dem unteren Magneten angeschlossen ist. Das ist dasselbe Prinzip, das sich auch schon bei meiner ersten Digitaluhr (http://www.ftcommunity.de/categories.php?cat_id=1396) bewährt hat.

Die etwas unmotiviert in der Mitte herumstehenden Bausteine sind tatsächlich überflüssig - sie stammen von Übungen am Getriebe. Ich muss nur noch einen finden, der die Elektronik nochmal abnimmt, um sie zu entfernen.
