---
layout: "image"
title: "Elektronik"
date: "2010-04-30T00:11:58"
picture: "fluesterleisepraezisemechanischedigitaluhr04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27011
- /details86ba.html
imported:
- "2019"
_4images_image_id: "27011"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27011 -->
Die von den Elektromagneten gelieferten 50 Hz Wechselspannung landen bei einem hobby-4-Grundbaustein, der sie als Operationsverstärker wie üblich digitalisiert. Der OR-NOR-Baustein dient nur nochmal als Verstärker, um über den Dyn.-AND-Baustein aus 50 Hz zunächst 100 Hz zu machen. Die linken der vier Flipflops sind als Teiler durch 5 beschaltet und liefern also 20 Hz. Das vierte Flipflop verteilt seine beiden Ausgänge auf zwei als D-Flipflop geschalteten E-Tec-Moduln, die damit einer 4-Phasen-Ansteuerung des Motors ermöglichen. Dadurch wird die Frequenz noch mal um einen Faktor 4 geteilt, so dass nur genau 5 Hz am Motor ankommen.

Die Schaltung ist in einem weiteren Bild noch ausführlich dargestellt.
