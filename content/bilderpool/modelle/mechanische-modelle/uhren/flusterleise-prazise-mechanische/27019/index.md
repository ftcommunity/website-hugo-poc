---
layout: "image"
title: "Übertrag vom Sekundenrad auf die Einerstelle der Minuten"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27019
- /details9fb4.html
imported:
- "2019"
_4images_image_id: "27019"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27019 -->
Die Achse vom hier rechts sichtbaren letzten Z50 trägt als nächstes ein Z30, welches über ein Z40 als Zwischenzahnrad das Z30 des Sekundenrades antreibt. Das Sekundenrad dreht sich also ständig. Außerdem trägt es noch eine Drehscheibe, auf der eine später noch deutlicher sichtbare Anordnung befestigt ist, die ein einzelnes Förderkettenglied trägt.

Dieses kommt also genau ein Mal pro Minute, also alle 60 Sekunden, am im Vordergrund sichtbaren Z30 vorbei und nimmt das - nach einer sehr feinen Justage - um genau drei Zähne, beim Z30 also genau 1/10 Umdrehung, mit. Wiederum dient ein Z40 als Zwischenzahnrad, um das Z30 der Walze mit der Einerstelle der Minuten zu erreichen. Voilà: Die Einerstelle der Minuten werden alle 60 Sekunden um 1/10 Umdrehung, also um genau eine Ziffer ihrer Papierstreifen, weiter geschaltet.

Dieser Umschaltvorgang setzt etwa bei Sekunde 54 ein und dauert ca. 6 Sekunden. Pünktlich zur Sekunde 00 ist der Umschaltvorgang beendet, und des Erbauers Herz lacht.
