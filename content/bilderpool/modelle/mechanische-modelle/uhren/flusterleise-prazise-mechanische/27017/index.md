---
layout: "image"
title: "Anzeigewalzen"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27017
- /detailsde3c-2.html
imported:
- "2019"
_4images_image_id: "27017"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27017 -->
Dieses Bild zeigt die vier Walzen, die - von links nach rechts - die Stunden, Zehnerstelle der Minuten, Einerstelle der Minuten und die Sekunden anzeigen sollen. Das Sekundenrad dreht sich synchron mit dem letzten Z50, also von alleine schon genau ein Mal pro Minute.

Auf den Walzen sind je zwei Papierstreifen befestigt, einer pro Walzenhälfte. Der Walzenumfang beträgt (mit einem Maßband gemessen) 31,7 bis 31,8 cm. Das ist zu lang, um auf eine bedruckte DIN-A4-Seite zu passen. Deshalb wurden je zwei Halbbänder verwendet. Diese sind jeweils ca. 2 mm an jedem Ende zu lang geschnitten. Dieser Überstand wird einfach zwischen die Winkelsteine 60° und die Bausteine 50 eingeklemmt - ganz einfach, und ganz ohne Klebstoff.
