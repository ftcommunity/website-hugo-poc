---
layout: "image"
title: "Motor"
date: "2010-04-30T00:11:58"
picture: "fluesterleisepraezisemechanischedigitaluhr05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27012
- /detailsb2a3-3.html
imported:
- "2019"
_4images_image_id: "27012"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27012 -->
Dieser Elektromotor bedient sich zweier der neueren, stärkeren Elektromagnete, die, wie später noch beschrieben, in 4 Phasen angesteuert werden. Das führt trotz der geringen Frequenz von nur 5 Hz zu einem recht ruhigen, gleichmäßigen Lauf. Da der Rotor auch noch 12 Dauermagnete trägt, braucht er für eine Umdrehung 1 / 5 Hz * 12 = 0,2 s * 12 = 2,4 s. Schön langsam, und deshalb schön leise.

Damit auch die letzten Schwingungen nicht etwa bis zur Großbauplatte und damit evtl. auf Möbel darunter durchkommen, die etwa unerwünscht als Resonanzkörper missbrauchen und dadurch ebenso unerwünschten Krach veranstalten, ist der Motor in sich zwar sehr starr aufgebaut, um möglichst wenig Energieverlust zu haben, wird als Ganzes aber nur von 4 fischertechnik-Kunststofffedern getragen. Die dämpfen die letzten Schwingungen ganz wunderbar weg, so dass das Betriebsgeräusch sogar ohne weitere Bürokratie die Genehmigung der besten aller Ehefrauen zum Aufstellen im Wohnzimmer erhielt.
