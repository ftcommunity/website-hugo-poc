---
layout: "image"
title: "4-Phasen-Steuerung"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27014
- /details7967.html
imported:
- "2019"
_4images_image_id: "27014"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27014 -->
In diesem Bild sehen wir Diagramme über den zeitlichen Verlauf der Signale an verschiedenen Stellen der Schaltung, und zwar über genau zwei ganze Perioden aufgetragen:

- Oben ist das Signal des normalen und des invertierten Ausgangs des letzten Silberling-Flipflops dargestellt, von denen jeder wiederum ein E-Tec als Binärzähler ansteuert. "Oben" ist jeweils die logische 1 in Silberling-Sichtweise (also -9 V) gemeint.

- E-Tec 1 ändert seinen Zustand immer dann, wenn der Ausgang Q des Flipflops 4 von 1 auf 0 in Silberling-Denkweise, also von -9 V auf 0 V, aus Sicht des E-Tec also von 0 V auf +9 V ändert. "Oben" ist hier auch die logische 1 gemeint, aber in E-Tec-Sichtweise, sprich +9 V.

- E-Tec 2 macht's genauso, nur hängt dieser am negierten Ausgang Q- des Flipflops 4. Es ändert also seinen Zustand immer zeitlich genau in der Mitte der stabilen Zustandsphasen des E-Tec 1.

- Um daraus eine hübsche vierphasige Ansteuerung der beiden Elektromagnete des Motors zu machen, teilen sich beide Magnete den Q-Ausgang des E-Tec 1, sind aber am anderen Pol mit je einem Q bzw. Q- des E-Tec 2 verbunden. Dadurch sehen die Magnete nicht nur einfach ein- und ausgeschaltete Spannung, sondern die Differenz der Spannungen zwischen diesen jeweiligen Ausgängen.

Das ergibt für Magnet 1 den dargestellten Verlauf der Differenz zwischen Q von E-Tec 1 und Q von E-Tec 2. Er ist also a) immer nur für zwei Viertel der Periodendauer (wie gesagt zeigen die Diagramme zwei volle Perioden) überhaupt eingeschaltet (was Strom spart, die Treiberbausteine schont und vor allem eine geringerer Erwärmung der Magnete bewirkt - die würden sonst nämlich richtig heiß), und b) wird er immer umgepolt eingeschaltet.

Dasselbe gilt für Magnet 2, der von der Differenz von Q des E-Tec 1 und Q- des E-Tec 2 betrieben wird, nur ist dieser immer genau dann eingeschaltet, wenn Magnet 1 ausgeschaltet ist, und umgekehrt.

Das, kombiniert mit einer geeigneten, sehr genauen Justierung der beiden Magnete führt dazu, dass immer abwechselnd je ein Pol je eines Magneten einen der Dauermagnete gerade zu sich hin zieht. Die Dauermagnete werden also für jede 1/12 Umdrehung, also von Magnet zu Magnet, in vier Phasen schön fein, gleichmäßig und sicher genau zu einem Magnetpol gezogen. Das bewirkt, dass der Rotor trotz der ungewöhnlich geringen Drehzahl ruhig, gleichmäßig und mit genügend Drehmoment dreht.
