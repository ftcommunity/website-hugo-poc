---
layout: "image"
title: "Gesamtansicht der 50-Hz-Uhr"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45091
- /details9160.html
imported:
- "2019"
_4images_image_id: "45091"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45091 -->
Die Zeitanzeigeneinheit kann (auch während des Betriebs) nach vorne abgezogen und wieder aufgesetzt werden.