---
layout: "comment"
hidden: true
title: "22954"
date: "2017-01-29T16:29:08"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Danke, Ralf!
Die Federn haben einen runden Nocken - und der passt genau in die runden Aussparungen der Experimentierplatte... als wäre es so gedacht gewesen ;-)
Gruß, Dirk