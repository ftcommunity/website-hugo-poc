---
layout: "image"
title: "Präzise (und sehr leise) 50-Hz-Uhr mit Schrittschaltwerk für den Minutenzeiger"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45083
- /details7c39.html
imported:
- "2019"
_4images_image_id: "45083"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45083 -->
Gesamtansicht der 50-Hz-Uhr. Die Uhr läuft sehr leise - die Übertragung der Motorvibration auf einen Resonanzkörper verhindern vier unter der Experimentierplatte angebrachte Kunststoff-Federn 26.
Die Uhr ist sehr kompakt: Sie misst (ohne Minutenzeiger) gerade einmal 20 x 11 x 11 cm (LxBxH).