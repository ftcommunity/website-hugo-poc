---
layout: "image"
title: "Frontansicht der Uhr (Zeitanzeige)"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45088
- /detailsa11d.html
imported:
- "2019"
_4images_image_id: "45088"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45088 -->
Stunden- und Minutenzeiger wurden auf einer Grundplatte 90 x 45 montiert. Mit der kleinen Kurbel (links im Bild) kann die Uhr gestellt werden - sogar im Betrieb, wenn die Segmentscheibe gerade nicht ins Schrittschaltwerk eingreift.
Hinter den Antriebszahnrädern der Zeitanzeige lässt sich noch ein Uhr-Zifferblatt aus Papier anbringen (Durchmesser: ca. 14-15 cm).