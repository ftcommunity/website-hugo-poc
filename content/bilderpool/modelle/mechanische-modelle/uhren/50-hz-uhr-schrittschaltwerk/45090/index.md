---
layout: "image"
title: "Untersetzung Stundenanzeige"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45090
- /details60f2.html
imported:
- "2019"
_4images_image_id: "45090"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45090 -->
Die Stundenanzeige wird vom Minutenzeiger angetrieben: auf der Rückseite (siehe Foto) eine Untersetzung 1:3 über ein Kettengetriebe, vorne (siehe vorausgehendes Foto) über eine Untersetzung 1:4.