---
layout: "overview"
title: "50-Hz-Uhr mit Schrittschaltwerk"
date: 2020-02-22T08:16:00+01:00
legacy_id:
- /php/categories/3357
- /categories94e6.html
- /categoriesb766.html
- /categories762e.html
- /categories2259.html
- /categories6052.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3357 --> 
Viele Analoguhren bewegen den Minutenzeiger nicht kontinuierlich, sondern schalten ihn schrittweise Minute für Minute weiter. Besonders gut gefiel mir das immer an Bahnhofsuhren...
Daher möchte ich die Sammlung der 50-Hz-Uhren im Bilderpool um eine mit Synchronuhr Schrittschaltwerk ergänzen. Bei der Konstruktion des Synchronmotors ließ ich mich von Hamlets Uhr mit sechs Neodym-Stabmagneten inspirieren.