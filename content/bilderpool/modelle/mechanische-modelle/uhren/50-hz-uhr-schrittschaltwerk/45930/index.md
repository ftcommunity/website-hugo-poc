---
layout: "image"
title: "50-Hz-Synchronuhr mit Zifferblatt"
date: "2017-06-04T12:09:54"
picture: "50-Hz-Uhr_mit_Zifferblatt_ftc.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
schlagworte: ["50Hz-Uhr", "Synchronuhr", "Schrittschaltwerk", "Maltesergetriebe", "Uhr"]
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/45930
- /details1a79-2.html
imported:
- "2019"
_4images_image_id: "45930"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-06-04T12:09:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45930 -->
Jetzt habe ich der Uhr noch ein Zifferblatt und ein Video spendiert: https://youtu.be/uRlEA3wmYNc