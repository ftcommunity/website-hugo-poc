---
layout: "image"
title: "50Hz-motor-Alternativ"
date: "2011-07-07T21:09:21"
picture: "50Hz-Motor.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/30991
- /detailsbbe2.html
imported:
- "2019"
_4images_image_id: "30991"
_4images_cat_id: "235"
_4images_user_id: "22"
_4images_image_date: "2011-07-07T21:09:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30991 -->
50Hz-motor-Alternativ mit 8 st Supermagnete S-04-4-N
