---
layout: "image"
title: "50-Hz-Uhr"
date: "2004-04-23T18:18:59"
picture: "50-Hz-Uhr_001F.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2361
- /detailsa7b3.html
imported:
- "2019"
_4images_image_id: "2361"
_4images_cat_id: "235"
_4images_user_id: "104"
_4images_image_date: "2004-04-23T18:18:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2361 -->
Gleich vorneweg: Die Idee, den mit ft möglichen Selbstbaumotor einfach direkt an den Wechselstromausgang des Trafos zu hängen, stammt nicht von mir, sondern von jemandem, der sowas schon mal auf einer Ausstellung mitbrachte (ich glaube, ein Holländer, aber ich weiß es nicht). Ich fand die Idee aber dermaßen genial, dass ich das umsetzen wollte, und zwar möglichst klein und dauerbetriebsfähig. Diese Uhr lief in diesem Zustand gut 3,5 Tage durch (mit Null Abweichung!), bis die Lager etwas zu Quietschen anfingen. Die Untersetzung zwischen Rotor (12,5 U/s) auf den Minutenzeiger ist übrigens 45000:1, und der Rotor dreht sich pro Tag genau 1.080.000 Mal.