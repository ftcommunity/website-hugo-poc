---
layout: "image"
title: "50-Hz-Uhr"
date: "2004-04-23T18:19:00"
picture: "50-Hz-Uhr_002F.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2362
- /details8891-2.html
imported:
- "2019"
_4images_image_id: "2362"
_4images_cat_id: "235"
_4images_user_id: "104"
_4images_image_date: "2004-04-23T18:19:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2362 -->
Die Uhr während des Laufs. Das Handtuch darunter (vorheriges Bild) dient übrigens zur Vermeidung von 50-Hz-Vibrationsbrummen. Schließlich soll die Uhr so leise wie möglich laufen.