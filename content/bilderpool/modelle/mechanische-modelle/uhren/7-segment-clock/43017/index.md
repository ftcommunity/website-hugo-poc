---
layout: "image"
title: "Segment Codes"
date: "2016-03-07T12:45:30"
picture: "segmentclock16.jpg"
weight: "16"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43017
- /details65cf-2.html
imported:
- "2019"
_4images_image_id: "43017"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43017 -->
The ROBOPro program preloads a list of 7 bit codes that indicate which segment needs to be set in order to make a particular digit (or other symbol). It calculates the difference between the current setting and the new setting, which determines if the levers need to be turned 90 degrees left or right.