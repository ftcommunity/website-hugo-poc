---
layout: "image"
title: "Bare bones"
date: "2016-03-07T12:45:30"
picture: "segmentclock13.jpg"
weight: "13"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43014
- /details564c.html
imported:
- "2019"
_4images_image_id: "43014"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43014 -->
The clock with all segments in place, but most other coverage removed.

The seconds indicator in the middle is turned by an S-Motor below, 90 degrees every 0,25 seconds, controlled by a simple Taster and an 'Impuls Zahnrad 4'.