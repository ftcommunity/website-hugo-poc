---
layout: "overview"
title: "7-Segment Clock"
date: 2020-02-22T08:15:58+01:00
legacy_id:
- /php/categories/3199
- /categoriesb23b.html
- /categoriescec3.html
- /categoriese897.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3199 --> 
There are a number of documented attempts to build 7-segment displays in FischerTechnik. It turns out to be surprisingly difficult to do in a compact way. Driving seven different elements that are grouped in a not-so-easy pattern with a limited number of parts is hard. For the ones on the community, the driving part is multiple times larger than the display itself.
I tried myself, and it took many attempts. I must admit that the result isn't as reliabale as I would have liked it. Sometimes display elements fall over or are missed, but generally, it works and displays the time.

There is a video here: https://youtu.be/Ntt9MMFAdE0



