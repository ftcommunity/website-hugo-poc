---
layout: "image"
title: "7-Segment Clock"
date: "2016-03-07T12:45:30"
picture: "segmentclock01.jpg"
weight: "1"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43002
- /detailsba47.html
imported:
- "2019"
_4images_image_id: "43002"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43002 -->
The problem with mechanical 7-segment drives is that the 7 segments are layed out in a way that makes it difficult to drive all segments with the same mechanical power source. Of course you can use one motor or one electromagnetic power source for each segment, but that requires 28 motors or electromagnets for one clock. It is impractical, and too large fysically.

My goal was to use one power source for all seven segments.