---
layout: "image"
title: "Backside"
date: "2016-03-07T12:45:30"
picture: "segmentclock06.jpg"
weight: "6"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43007
- /details7022.html
imported:
- "2019"
_4images_image_id: "43007"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43007 -->
This is what the backside looks like: 7 neatly spaced shafts. They need to be turned 90 degrees clockwise or counterclockwise is order to switch a segment from red to yellow or vice verse.