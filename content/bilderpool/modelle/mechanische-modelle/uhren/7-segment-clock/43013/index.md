---
layout: "image"
title: "Carriage (4)"
date: "2016-03-07T12:45:30"
picture: "segmentclock12.jpg"
weight: "12"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/43013
- /details63a8-3.html
imported:
- "2019"
_4images_image_id: "43013"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43013 -->
Carriage in bottom position. As can be seen, it is a tight fit.