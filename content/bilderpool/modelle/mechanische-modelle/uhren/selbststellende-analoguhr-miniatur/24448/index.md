---
layout: "image"
title: "Minutenweises Stellen (2)"
date: "2009-06-23T16:02:24"
picture: "selbststellendeanaloguhr13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24448
- /detailse034.html
imported:
- "2019"
_4images_image_id: "24448"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24448 -->
Über diese Statikstrebe wird die Bewegung auf den Pendelbalken in der Mitte übertragen. Der trägt oben eine ft-Winkelachse, die in die Zähne des Z30 eingreift. Da der Hub des Dauermagneten unterhalb der Elektromagnete etwas zu klein ist, wird über geeignete Hebelgeometrie eine Übersetzung gebildet: Der linke Hebel dreht sich etwas weiter als der bei den Magneten, damit der erreichbare Drehwinkel auch garantiert für einen Zahn des Z30 genügt.
