---
layout: "image"
title: "Details zum Uhrwerk (4)"
date: "2009-07-08T21:11:44"
picture: "detailszumuhrwerk4.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24519
- /detailsbbcf.html
imported:
- "2019"
_4images_image_id: "24519"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24519 -->
Auf der herausstehenden Achse saß das Stunden-Z30. Man sieht, wie der BS30 mit BS7,5, der die Riegelscheibe als Zwischenzahnrad trägt, flexibel auf der hier linken Seite mit einer Platte 30x15 befestigt ist. Das ermöglicht es, dass die Riegelscheibe richtig sitzt und nur leicht angedrückt wird. Sonst wäre es zu eng und würde klemmen.
