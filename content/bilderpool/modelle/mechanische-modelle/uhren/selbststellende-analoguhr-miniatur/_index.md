---
layout: "overview"
title: "Selbststellende Analoguhr mit Miniatur-Uhrwerk"
date: 2020-02-22T08:15:27+01:00
legacy_id:
- /php/categories/1676
- /categories72f2.html
- /categories3e31.html
- /categoriesfad2.html
- /categories5efe.html
- /categories5b13.html
- /categories9872.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1676 --> 
Eine kompakte Zeigeruhr, die sich automatisch auf die Uhrzeit des Rechners einstellt.



Mit praktisch derselben Funktionsweise hatte ich in den 1980ern schon mal so eine Uhr gebaut, die aber viel größer war (6 Bogenstücke 60 als Ziffernblatt, per motorischer Umschaltung wurde zwischen Stellmotor und Taktmagneten umgestellt, es wurden wirklich vier Schaltwalzen benötigt anstatt wie hier letztlich nur drei). Die Idee ist also nicht ganz neu, aber hier sollte es kompakter und weiter entwickelt sein.