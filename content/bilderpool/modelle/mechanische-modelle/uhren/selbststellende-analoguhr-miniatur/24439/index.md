---
layout: "image"
title: "Uhrwerk (2)"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24439
- /details2170.html
imported:
- "2019"
_4images_image_id: "24439"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24439 -->
Hier sieht man nochmal den Übertrag vom Z22-Ritzel über die Riegelscheibe als Zwischenzahnrad auf das zweite Z44.
