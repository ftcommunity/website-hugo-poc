---
layout: "image"
title: "Stundenmessung (3)"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24443
- /details57ce-2.html
imported:
- "2019"
_4images_image_id: "24443"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24443 -->
Die mittlere Schaltwalze zwischen den beiden Drehscheiben wird von zwei Tastern gemeinsam benutzt.

Nur hier ist ein BS15 mit zwei Zapfen verbaut, um die beiden Drehscheiben garantiert zu koppeln. Alle anderen 15-mm-Bausteine sind die roten rechten Winkel, bei denen der zweite Zapfen der daneben liegenden WS60 genügend Platz im leeren Inneren findet.
