---
layout: "image"
title: "Detailblick von links"
date: "2007-07-16T16:20:57"
picture: "digitaluhr6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11102
- /detailsf0d7.html
imported:
- "2019"
_4images_image_id: "11102"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11102 -->
Hier sieht man den von vorne aus gesehen linken Keil nebst Endtaster für die angehobene Position. Die Anhebemechanik ist bereits getestet und funktioniert trotz des hohen anzuhebenden Gewichtes sehr gut.
