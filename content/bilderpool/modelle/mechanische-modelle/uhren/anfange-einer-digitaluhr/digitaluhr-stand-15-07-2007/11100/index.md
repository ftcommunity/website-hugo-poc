---
layout: "image"
title: "Ansicht von der rechten Seite"
date: "2007-07-16T16:20:57"
picture: "digitaluhr4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11100
- /details1a8c.html
imported:
- "2019"
_4images_image_id: "11100"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11100 -->
... denn die Mimik muss auch einige Millimeter senkrecht nach oben gehoben (erste Bewegungsachse) und nach hinten aus den Ziffern herausgefahren werden können (zweite Bewegungsachse). Man sieht hier die (von vorne aus gesehen) rechte Seite. Hier kann man die Hebemechanik: Der PowerMotor dreht an beiden Seiten über die Kette an Schnecken, die wiederum je einen rollbaren Keil unter die an insgesamt 6 Längslenkern aufgehängte Segment-Einstellmechanik schieben. Damit kommt hinreichend Kraft zustande, um die wegen der 7 Elektromagnete, vielen Metallachsen und sonstiger Bauteile recht schwere Mechanik anzuheben. Durch dieses Absenken bzw. Anheben werden die Greifer ganz vorne (hier links) an der Mechanik auf die Klemmringe der Segmente abgesenkt und damit in Eingriff gebracht bzw. nach oben gehoben und somit ausgekuppelt - so ist zumindest die Theorie. Man sieht auch den Endtaster für die abgesenkte Position im rechten unteren Bildviertel.
