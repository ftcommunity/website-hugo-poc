---
layout: "image"
title: "Mechanik von vorne"
date: "2007-07-16T16:20:57"
picture: "digitaluhr7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11103
- /details62ea-2.html
imported:
- "2019"
_4images_image_id: "11103"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11103 -->
So sieht das gesamte zwischen den Ziffern zu bewegende Teil aus. Dies sind also die 7 Segmentsteller, die im Verbund einige mm angehoben werden können. All dies muss nun noch nach vorne und hinten zum Einschieben in eine Ziffer und zum Wiederherausholen sowie nach rechts und links zum Anfahren der vier Ziffern verfahren werden.
