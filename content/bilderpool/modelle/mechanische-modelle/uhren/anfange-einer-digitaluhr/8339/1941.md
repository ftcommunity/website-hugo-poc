---
layout: "comment"
hidden: true
title: "1941"
date: "2007-01-09T22:14:11"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ja, die 7 Magnete mit der Verschiebemechanik werden komplett von Ziffer zu Ziffer verfahren. Das kommt als nächste Bauphase dran. Momentan kann man das auf den Fotos noch nicht sehen ;-)

Gruß,
Stefan