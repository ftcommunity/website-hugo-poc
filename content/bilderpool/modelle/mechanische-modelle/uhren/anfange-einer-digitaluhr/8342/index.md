---
layout: "image"
title: "Detailaufnahme eines Verschiebers"
date: "2007-01-08T17:16:10"
picture: "digitaluhrprototyp8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8342
- /detailsf6ed.html
imported:
- "2019"
_4images_image_id: "8342"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8342 -->
Durch Einschalten des Elektromagneten mit der jeweils richtigen Polung wird der Dauermagnet zum einen oder eben anderen Pol des E-Magneten verschoben. Das wiederum schiebt über die Strebe den BS30 mit dem Lagerbock, welcher in den Klemmring eines Segments eingreift. Nach der Aktion kann der Magnet wieder ausgeschaltet werden.