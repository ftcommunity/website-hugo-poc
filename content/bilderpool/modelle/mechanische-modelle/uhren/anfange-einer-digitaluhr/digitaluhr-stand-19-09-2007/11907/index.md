---
layout: "image"
title: "Die ganze Uhr von vorne"
date: "2007-09-23T11:13:27"
picture: "digitaluhr2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11907
- /detailsa1ee-2.html
imported:
- "2019"
_4images_image_id: "11907"
_4images_cat_id: "1067"
_4images_user_id: "104"
_4images_image_date: "2007-09-23T11:13:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11907 -->
Man sieht das Ziffernblatt, oben RoboInt und Extensions, davor die aus Silberlingen erstellte Elektronik für die Zeitbasis, in der Mitte die 5 Taster für das Stellen und Starten der Uhr, und vorne die 7-Segment-Ziffern, welche vom Wagen im Inneren betätigt werden. Vornedran fehlt noch die matte Folie oder das Milchglas, was den optischen Effekt der Anzeige erst ermöglicht.

Auf diesem Bild ist kein Silberling-Gleichrichter eingebaut, weil ich da noch dachte, die 9 V fürs RoboInt, die aus dem Netzteil kommen, seien schon hinreichend geglättet. Weit gefehlt: So läuft die Zeitbasis überhaupt nicht. Erst nach Einbau eines h4GB bewirkte der darin enthaltene dicke Kondensator die Lauffähigkeit.
