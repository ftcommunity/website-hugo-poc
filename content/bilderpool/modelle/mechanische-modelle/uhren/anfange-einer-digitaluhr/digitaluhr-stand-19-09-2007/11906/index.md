---
layout: "image"
title: "Der Wagen von unten"
date: "2007-09-23T11:13:27"
picture: "digitaluhr1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11906
- /details004c.html
imported:
- "2019"
_4images_image_id: "11906"
_4images_cat_id: "1067"
_4images_user_id: "104"
_4images_image_date: "2007-09-23T11:13:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11906 -->
Wie bekommt man etwas Schweres wirklich präzise positioniert? Eine Möglichkeit sieht man hier. Das Bild zeigt den Wagen mit dem schweren Teil (mit den Elektromagneten drauf) von unten. Die 8 Seilrollen laufen auf Schienen zum Verfahren zwischen den Ziffern. Alles ab den roten Grundbauplatten (mit dem auf der anderen Seite befindlichen Aufbau) muss nun um 30 mm quer zu dieser Richtung verschoben werden, und hier kommt es auf weniger als 1 mm an. Das Alugerüst bleibt also stehen, während die roten Grundplatten auf den 6 Vorstuferädern leicht rollen können. Die präzise Führung erfolgt durch die zwei weit außen liegenden Metallachsen, die den Oberbau gleichzeitig gegen Verdrehen schützen. Der Minimotor am unteren Bildrand treibt die Schnecke an, und die - das ist der Gag - treibt nicht direkt die Grundplatten, sondern nur über eine gefederte Aufhängung. Der Motor kann einfach bis zum Anschlag und noch etwas weiter drehen. Der Anschlag wird durch das Anstoßen der BS15 mit Loch auf den Metallachsen gegen die Winkelsteine, mit denen die Achsen am Chassis befestigt sind, realisiert. Diese Position ist sehr genau. Der Motor dreht einfach noch ein bisschen weiter, bis der jeweilige Endlagentaster betätigt wird. Das geschieht nun eben nicht, wenn der Anschlag erreicht ist, sondern wenn eine bestimmte Federspannung ansteht, da die Taster (das ist hier schlecht sichtbar) an den roten Grundplatten befestigt sind.
