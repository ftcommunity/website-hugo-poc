---
layout: "image"
title: "Arbeitsweise (3) - Motorantrieb"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42642
- /details6805.html
imported:
- "2019"
_4images_image_id: "42642"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42642 -->
Der XS-Motor ist schön leise. Er bekommt jede Sekunde kurz Strom, um das Z30 um einen halben (!) Zahn weiter zu drehen. Wie das geschieht, wird auf dem nächsten Bild erklärt. Diese Drehung geht über die Kette (mit je einem Klemm-Z15 an den Enden) direkt auf die zentrale Welle für den Sekundenzeiger. In 60 Halbschritten dreht sich der also genau ein Mal pro Minute herum.
