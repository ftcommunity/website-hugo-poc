---
layout: "image"
title: "Wirkungsweise und Aufbau"
date: "2016-01-30T17:54:49"
picture: "fluesterleisehzzeigeruhrmitsekundenzeiger4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42854
- /details1d5d.html
imported:
- "2019"
_4images_image_id: "42854"
_4images_cat_id: "3186"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T17:54:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42854 -->
Sowohl die Magnete werden also wechselweise eingeschaltet, und die auch noch immer abwechselnd herum gepolt. Das hat, wenn man alles richtig herum verschaltet, die Folge, dass sich folgender Ablauf ergibt:

1. Die untere Spule des rechten Magneten zieht den roten Dauermagneten an. Das ist der Zustand in diesem Bild.

2. Die linke Spule des oberen Magneten zieht den jetzt gerade etwas links davon befindlichen Magneten an. Der Rotor dreht sich also etwas im Uhrzeigersinn (von hier, hinten, aus gesehen).

3. Der auf diesem Bild nach rechts oben zeigende Magnet ist dadurch etwas weiter nach rechts unten gerutscht und wird im nächsten Schritt vom oberen Pol des rechten Magneten angezogen.

4. Schließlich wird der auf diesem Bild nach oben zeigende Magnet, der jetzt nämlich ziemlich genau zwischen den beiden Polen des oberen Magneten liegt, vom rechten Pol des oberen Magneten angezogen.

5. = 1. Der im Bild nach rechts oben zeigende Magnet ist dadurch zwischen die beiden Pole des rechten Magneten gedreht worden und wird vom unteren Pol des rechten Magneten angezogen.

Die Pole der Elektromagnete sehen also immer nach vier Teilschritten den jeweils nächsten Magneten. Für die 1/6 Umdrehung werden tatsächlich also vier Teilschritte verwendet. Der Motor ist dadurch kräftig genug, und er dreht den Rotor nach 6 * 4 = 24 Takten (= 24 Sekunden) genau einmal ganz herum. Durch die Übertragung von Z10 auf Z25 ergibt das genau 60 Sekunden pro Zeigerumdrehung und außerdem die richtige Drehrichtung der Zeiger.
