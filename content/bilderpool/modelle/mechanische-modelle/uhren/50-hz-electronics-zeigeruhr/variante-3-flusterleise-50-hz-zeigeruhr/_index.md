---
layout: "overview"
title: "Variante 3: Flüsterleise 50-Hz-Zeigeruhr mit Sekundenzeiger und Schrittmotor"
date: 2020-02-22T08:15:54+01:00
legacy_id:
- /php/categories/3186
- /categories5934.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3186 --> 
In dieser Variante wurde der Antrieb nochmal umgestellt, und zwar auf einen Selbstbau-Schrittmotor, der ein Mal pro Sekunde getaktet wird. So läuft die Uhr praktisch völlig geräuschlos.