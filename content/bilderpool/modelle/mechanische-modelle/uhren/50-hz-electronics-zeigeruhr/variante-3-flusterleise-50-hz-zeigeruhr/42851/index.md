---
layout: "image"
title: "Variante mit Selbstbau-Schrittmotor"
date: "2016-01-30T17:54:49"
picture: "fluesterleisehzzeigeruhrmitsekundenzeiger1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42851
- /details85bd-2.html
imported:
- "2019"
_4images_image_id: "42851"
_4images_cat_id: "3186"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T17:54:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42851 -->
Diese Variante der Zeigeruhr hat einen nochmals verbesserten Antrieb bekommen. Nach dem klickenden Taster, der Bauart mit Lichtschranke und nur noch leicht hörbarem Motor folgt hier eine weitere Verbesserung. Man braucht zwar ein elektronisches Modul mehr (3 insgesamt), aber dafür läuft diese Uhr fast völlig geräuschlos (besonders, wenn man sie auch noch auf einen weichen Untergrund stellt).
