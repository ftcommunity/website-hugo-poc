---
layout: "image"
title: "Elektronik"
date: "2016-01-30T17:54:49"
picture: "fluesterleisehzzeigeruhrmitsekundenzeiger5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42855
- /details437d.html
imported:
- "2019"
_4images_image_id: "42855"
_4images_cat_id: "3186"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T17:54:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42855 -->
Unverändert gegenüber den vorherigen beiden Varianten sitzt ganz links das Electronics-Modul. Es ist so verschaltet, dass die 50 Hz zwei Mal durch 10 geteilt werden. Das ergibt also 1/2 Hz.

Der nächste (mittige) Baustein ist ein E-Tec. Hier könnte auch ein Electronics- oder ein MiniBots-Modul stecken. Wir brauchen hier nur einen Inverter. Damit haben wir zwei Ausgänge, und jede Sekunde wird der jeweils andere von 0 nach 1 geschaltet.

Schließlich werden diese beiden (!) Ausgänge an je ein D-Flipflop geschaltet. Hierfür ist hier ein MiniBots-Modul verwendet, es könnte aber auch ein weiteres Electronics-Modul sein. Das ist so eingestellt, dass zwei Flipflops zur Verfügung stehen. Die ergeben an ihren Ausgängen dann wieder genau die unter http://www.ftcommunity.de/details.php?image_id=27013 ff beschriebene vier-Phasen-Ansteuerung für den Schrittmotor.

Im Endeffekt ist diese Uhr damit drastisch leiser als eine echte Wand-Quarzuhr, die pro Sekunde ein Mal "Tick" macht.
