---
layout: "image"
title: "Arbeitsweise (9) - Stundenrad"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42649
- /detailsa8dc-3.html
imported:
- "2019"
_4images_image_id: "42649"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42649 -->
Die 1:2 sind durch die Kombination von Z20 auf Z40 realisiert. Das Z40 dreht sich also alle zwölf Stunden wie gewünscht ein Mal. Das Z20 dreht sich aus diesem Blickwinkel nach links. Beim Drehen "trägt" es das Z40 also, sodass dieses in den BS7,5-Gleitführungen der hinteren Drehscheibe ganz leichtgängig dreht (die andere Drehrichtung wäre schwergängig). Die beiden Drehscheiben und das Z40 sind durch drei Seilklemmstifte 15 fest miteinander verbunden, werden ansonsten aber nur vom Z20 und den Gleitführungen gehalten.
