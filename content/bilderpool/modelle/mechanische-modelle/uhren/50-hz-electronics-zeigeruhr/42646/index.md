---
layout: "image"
title: "Arbeitsweise (7) - Minutenwelle und Halterung des Stundenrades"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42646
- /detailsd08b.html
imported:
- "2019"
_4images_image_id: "42646"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42646 -->
Der eben angesprochene BS15 mit Bohrung sitzt frei drehbar auf der Minutenwelle. Wichtig ist, dass er nichts weiter nach rechts (im Bild) bzw. hinten (im Modell) gedrückt werden kann, weil die Freilaufnabe des Z30 im diesen Weg versperrt, die ja direkt an einem Baustein 30 mit Bohrung anliegt. Wenn man genau hinschaut, sieht man im BS30 mit Bohrung nach vorne abgehend zwei schwarze Rastachsen 30.schräg stecken.

Man sieht hier auch zwei mit Bausteinen 7,5 realisierte Gleitführungen für eine Drehscheibe des Stundenrades.
