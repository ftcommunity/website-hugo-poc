---
layout: "image"
title: "Linke Seite"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42636
- /detailsbef5.html
imported:
- "2019"
_4images_image_id: "42636"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42636 -->
Hier sieht man die beiden Baustein-Stränge, an denen die gesamte Mechanik angebracht ist.
