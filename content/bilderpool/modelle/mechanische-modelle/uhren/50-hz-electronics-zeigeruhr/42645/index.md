---
layout: "image"
title: "Arbeitsweise (6) - Minutenwelle"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42645
- /detailse859.html
imported:
- "2019"
_4images_image_id: "42645"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42645 -->
Die wie auf dem vorherigen Bild beschriebene 1:40 gegenüber der Sekundenwelle untersetzte Achse ist die untere mit dem links von der Bildmitte sichtbaren Z20. Das geht auf das Z30 links im Bild, welches per Freilaufnabe, also frei drehbar, auf der Sekundenachse steckt. Wir haben also 1:40 * 2:3 = 1:60 erreicht. Das Z30 dreht sich alle 60 Umdrehungen des Sekundenzeigers, mithin also genau ein Mal pro Minute.

Am Z30 sitzt mit einem Federnocken verbunden ein BS7,5, der versetzt auf einem Baustein 15 mit Bohrung steckt - siehe nächstes Bild.

Die Z20/Z40-Kombination ist der Antrieb des Stundenzeigers.
