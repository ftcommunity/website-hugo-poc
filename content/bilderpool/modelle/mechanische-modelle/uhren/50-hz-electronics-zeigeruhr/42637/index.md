---
layout: "image"
title: "Rechte Seite"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42637
- /detailseb8c-2.html
imported:
- "2019"
_4images_image_id: "42637"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42637 -->
Das gesamte Getriebe ist eher auf der linken Seite angebracht. Die rechte Seite trägt einen leichtgängigen Steuertaster für die Sekundentaktung (grau, oben) und einen Taster zum schnellen Stellen der Uhr (Minitaster schwarz auf dem Gleichrichterbaustein etwas unterhalb der Bildmitte). Der Schalter rechts unten ist für das Ein- und Ausschalten der Uhr.
