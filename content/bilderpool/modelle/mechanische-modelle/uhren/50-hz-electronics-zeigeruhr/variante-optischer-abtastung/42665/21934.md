---
layout: "comment"
hidden: true
title: "21934"
date: "2016-04-09T20:05:05"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Warum? Reagieren Fototransistoren vorzugsweise auf IR und gar nicht so auf Tages- oder Glühlampenlicht?

Gruß,
Stefan