---
layout: "image"
title: "Variante mit optischer Abtastung - Zusätzliche Untersetzung"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42672
- /details6e00.html
imported:
- "2019"
_4images_image_id: "42672"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42672 -->
Der XS-Motor steht einfach auf einem zusätzlich angebrachten BS15.
