---
layout: "image"
title: "Variante mit optischer Abtastung - Elektronik"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42673
- /details4eb3.html
imported:
- "2019"
_4images_image_id: "42673"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42673 -->
Die drei E-Tecs hier sind laut Zusatzanleitung ein XOR-Glied, aufgebaut aus zwei als AND und einem als OR eingestellten E-Tec. Leider habe ich noch kein zweites Electronics-Modul, welches ein XOR eingebaut hätte. Ob das MiniBots-Modul ein XOR bereitstellt, prüfe ich noch, wenn ich eines habe. Man käme also auch mit insgesamt zwei Electronics-Moduln oder einem Electronics- und einem MiniBots-Modul aus. Das XOR ist zwischen dem 2-Sekunden-Taktausgang des Electronics-Moduls und der Lichtschranke geschaltet, damit der Motor jede Sekunde immer nur so lange läuft, bis der nächste Halbschritt des Z30 beendet ist.
