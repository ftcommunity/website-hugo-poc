---
layout: "image"
title: "3/4-Ansicht von hinten rechts"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42639
- /detailsc78d-2.html
imported:
- "2019"
_4images_image_id: "42639"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42639 -->
Die Arbeitsweise wird auf den folgenden Bildern Schritt für Schritt beschrieben.
