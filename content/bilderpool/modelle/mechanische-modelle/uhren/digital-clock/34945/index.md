---
layout: "image"
title: "RoboPro Program"
date: "2012-05-12T17:08:08"
picture: "digitalclock18.jpg"
weight: "18"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34945
- /details80be.html
imported:
- "2019"
_4images_image_id: "34945"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34945 -->
It turns out that the TX timers run about 1% too fast. I needed to set the timer to 6061 10ms (is 60,61s) steps to make the clock take accurate 1 minute steps.