---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:07"
picture: "digitalclock04.jpg"
weight: "4"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34931
- /detailsdd77.html
imported:
- "2019"
_4images_image_id: "34931"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34931 -->
By fixing the rings between clips on an axle, sufficient friction is created to make the rings move with the axle, but also allow them to rotate freely independent of the axle.