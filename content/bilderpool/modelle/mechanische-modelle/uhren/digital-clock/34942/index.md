---
layout: "image"
title: "Digital Clock v2"
date: "2012-05-12T17:08:08"
picture: "digitalclock15.jpg"
weight: "15"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34942
- /detailsb9df.html
imported:
- "2019"
_4images_image_id: "34942"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34942 -->
I did a 2nd version, with all the motors built in the base. It looks cleaner, but less interesting...