---
layout: "overview"
title: "Digital Clock"
date: 2020-02-22T08:15:41+01:00
legacy_id:
- /php/categories/2586
- /categories7dfe.html
- /categories3c18.html
- /categoriesaf92.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2586 --> 
Digital Clock driven by 6 S-motors and a TX controller. Each of the 4 digits consists of 5 triangular rings. By positioning the rings in different ways, all digits can be formed. The 5  rings that make up one digit can move freely on the axle they are sitting on, but with some friction. When the axle is driven by a motor, the rings will turn as well, because of the friction. However, if you block some rings to prevent them from moving, and the axle turns, the blocked rings don't turn, whereas the free ones do. The clock has a system of levers that can block rings selectively. If a lever is pushed by a cam plate (Nockenscheibe) it unblocks a ring, and the axle motor can turn that ring.

Video of the clock in action: http://www.youtube.com/watch?v=0QyPH_z0ySE

Inspired by Hans Andersson, one of our Lego colleagues: http://tiltedtwister.com/timetwister.html