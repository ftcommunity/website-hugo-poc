---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:07"
picture: "digitalclock05.jpg"
weight: "5"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34932
- /detailsf934.html
imported:
- "2019"
_4images_image_id: "34932"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34932 -->
With these combinations of ring layouts you can create all digits.