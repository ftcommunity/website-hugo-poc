---
layout: "image"
title: "Digital Clock v2"
date: "2012-05-12T17:08:08"
picture: "digitalclock14.jpg"
weight: "14"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- /php/details/34941
- /details5b24.html
imported:
- "2019"
_4images_image_id: "34941"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34941 -->
The second indicator turns 90 degrees every 0,25s, changing from red to yellow. A simple impuls wheel 4 and a mini switch is used to control that.