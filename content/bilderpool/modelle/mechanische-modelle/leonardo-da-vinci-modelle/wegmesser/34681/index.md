---
layout: "image"
title: "Modell des Wegmessers von Leonardo da Vinci"
date: "2012-03-25T13:03:14"
picture: "Wegmesser_nach_Leonardo.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34681
- /detailsf7c7-2.html
imported:
- "2019"
_4images_image_id: "34681"
_4images_cat_id: "2559"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T13:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34681 -->
Mit einem solchen Wegmesser hat Leonardo da Vinci wahrscheinlich 1502 die Stadt Imola vermessen - für den ersten überlieferten Stadtplan überhaupt.