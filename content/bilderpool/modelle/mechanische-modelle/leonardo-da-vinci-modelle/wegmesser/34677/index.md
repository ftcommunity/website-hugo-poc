---
layout: "image"
title: "Wegmesser - Originalzeichung von Leonardo da Vinci"
date: "2012-03-25T10:32:05"
picture: "Wegmesser_Leonardo.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34677
- /detailsb1bd.html
imported:
- "2019"
_4images_image_id: "34677"
_4images_cat_id: "2559"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T10:32:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34677 -->
Der Wegmesser wurde von Leonardo da Vinci nach einer Beschreibung von Vitruv entwickelt und verbessert. Die Speichenräder hatten einen Umfang von 10 Ellen, das Zahnrad des linken Modells 300 Zähne; damit entsprach eine Umdrehung der Distanz von einer Meile.