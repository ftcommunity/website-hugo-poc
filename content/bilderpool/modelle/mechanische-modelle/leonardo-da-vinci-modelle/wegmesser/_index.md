---
layout: "overview"
title: "Wegmesser"
date: 2020-02-22T08:18:16+01:00
legacy_id:
- /php/categories/2559
- /categoriesc5f4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2559 --> 
Von Leonardo da Vinci entwickelter Wegmesser (zur Vermessung von Siedlungen)