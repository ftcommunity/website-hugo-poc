---
layout: "image"
title: "Sichelwagen Typ 2"
date: "2008-02-09T13:45:48"
picture: "Sensenwagen_2_Da_Vinci_Feb_08.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13620
- /detailsf918.html
imported:
- "2019"
_4images_image_id: "13620"
_4images_cat_id: "1252"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T13:45:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13620 -->
Der zweite, etwas aufwändigere Typ von Sichelwagen
