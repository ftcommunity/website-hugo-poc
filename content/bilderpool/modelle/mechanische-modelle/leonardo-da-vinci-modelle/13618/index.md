---
layout: "image"
title: "Sichelwagen"
date: "2008-02-09T13:45:47"
picture: "Sensenwagen_Da_Vinci_Feb_08.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
license: "unknown"
legacy_id:
- /php/details/13618
- /details9e30.html
imported:
- "2019"
_4images_image_id: "13618"
_4images_cat_id: "1252"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13618 -->
Eine von Leonardo Da Vinci um 1485 erfundene Kriegsmaschine mit ft nachgebaut
