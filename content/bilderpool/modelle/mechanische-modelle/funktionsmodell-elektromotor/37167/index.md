---
layout: "image"
title: "Dynamo in grau"
date: "2013-07-19T20:20:07"
picture: "dynamo800_3.jpg"
weight: "2"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
schlagworte: ["Dynamo", "grau", "vintage", "alt"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/37167
- /detailscc97.html
imported:
- "2019"
_4images_image_id: "37167"
_4images_cat_id: "2631"
_4images_user_id: "427"
_4images_image_date: "2013-07-19T20:20:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37167 -->
Fest kurbeln - damit es schön leuchtet.....