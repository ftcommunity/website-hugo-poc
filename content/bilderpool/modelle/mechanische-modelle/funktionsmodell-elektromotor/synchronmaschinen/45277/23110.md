---
layout: "comment"
hidden: true
title: "23110"
date: "2017-02-25T23:47:46"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Rüdiger,
Du hast die Magnete sicher mit wechselnder Polung angebracht.
Die Magnetfelder, die die beiden E-Magnete aufspannen, wechseln bei 50 Hz 100x in der Sekunde, also 100x60 = 6000x in der Minute. Wenn ich die Konstruktion richtig verstehe, dreht sich die Antriebsachse des Motors alle 12 Polwechsel ein Mal, also 500x pro Minute.
Herzlicher Gruß, Dirk