---
layout: "image"
title: "Innenpolige Synchronmaschine"
date: "2017-02-25T18:15:45"
picture: "Innenpolige_Synchronmaschine.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45277
- /details267d.html
imported:
- "2019"
_4images_image_id: "45277"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-02-25T18:15:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45277 -->
Der 12-polige Läufer (Polpaarzahl 6) dreht sich um die beiden E-Magnete. Es ist ein sog. Außenläufer. Die Synchrondrehzahl ist 500 U/min.