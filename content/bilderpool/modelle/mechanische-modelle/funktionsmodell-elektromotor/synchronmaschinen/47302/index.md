---
layout: "image"
title: "Synchronmaschine mit 120 U/min weiterentwicklet aus Ludger Mäsings Rollenblocker"
date: "2018-02-23T19:37:47"
picture: "P1250003kl.jpg"
weight: "20"
konstrukteure: 
- "Reiner Merz"
fotografen:
- "Reiner Merz"
schlagworte: ["Synchronmotor", "Synchronmaschine"]
uploadBy: "ReinerMerz"
license: "unknown"
legacy_id:
- /php/details/47302
- /detailsbd71.html
imported:
- "2019"
_4images_image_id: "47302"
_4images_cat_id: "3374"
_4images_user_id: "2838"
_4images_image_date: "2018-02-23T19:37:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47302 -->
Ich habe Ludger Mäsings 10er Teilung aufgesattelt und mit 50 4x20mm Neodyms versehen.
Ich habe 7,5er Bausteine genutzt und die Magnete mit S-Riegelscheiben festgemacht.
Der Motor läuft noch etwas zickig, ich muss noch weiter dran tunen ! Für Tipps wäre ich dank bar !