---
layout: "image"
title: "Innenpolige Synchronmaschine in Betrieb"
date: "2017-02-25T18:15:45"
picture: "Innenpolige_Synchronmaschine_in_Betrieb.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45276
- /details5f93-2.html
imported:
- "2019"
_4images_image_id: "45276"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-02-25T18:15:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45276 -->
Die innenpolige Synchronmaschine dreht mit 500 U/min.