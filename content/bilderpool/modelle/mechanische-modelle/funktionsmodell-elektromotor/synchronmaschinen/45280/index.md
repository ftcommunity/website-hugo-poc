---
layout: "image"
title: "Synchronmotor mit 600 U/min mit Ludger Mäsings Rollenblock-10-Eck"
date: "2017-02-27T15:16:22"
picture: "SynchronmotorMit600UproMin_2.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/45280
- /details187d.html
imported:
- "2019"
_4images_image_id: "45280"
_4images_cat_id: "3374"
_4images_user_id: "1088"
_4images_image_date: "2017-02-27T15:16:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45280 -->
Eine direkte Anwendung von Ludgers genialem Rollenblock-10-Eck http://ftcommunity.de/details.php?image_id=6020 ist dieser Synchronmotor mit 600 U/min. Dadurch kann ich das Planetetengetriebe in meiner minimalistischen Synchronuhr http://ftcommunity.de/details.php?image_id=44471 ersetzen. Ob dass das Modell verbessert, muss ich allerdings noch testen. Auf jeden Fall spart man zwei Magneten ein.
