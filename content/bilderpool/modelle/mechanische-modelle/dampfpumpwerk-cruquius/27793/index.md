---
layout: "image"
title: "Dampfpumpwerk Cruquius"
date: "2010-08-06T18:43:07"
picture: "Cruquius-Modell.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["Stoomgemaal","Steam-pumping-station","Taktsteuerung","Steuerung ohne PC","Pneumatik","Automatik","Automatische Steuerung","Zyklisch","Wiederholend","Druckluft-Steuerung","Druckluftzylinder","Pneumatikzylinder","Pneumatiksteuerung","Handventil"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/27793
- /details5469.html
imported:
- "2019"
_4images_image_id: "27793"
_4images_cat_id: "2003"
_4images_user_id: "724"
_4images_image_date: "2010-08-06T18:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27793 -->
Nachbau der Dampfpumpstation in Holland.

Hiermit wurde einige Jahre lang das Haarlemmer-Meer trockengelegt.

Der Hauptzylinder hat im Original einen Durchmesser von 2 m, und ist somit der größte, der jemals gebaut wurde.

Die 6 Pumpen (im Modell angedeutet durch die 6 äußerden Zylinder) fördern jeder pro Hub 800 Liter Wasser.
Pro Minute macht die Anlage automatisch etwa 6 Hübe.
Die Förderhöhe beträgt etwa 4 m.

Das Original kann besichtigt werden, und ist sogar wieder in Betrieb, allerdings mittlerweile hydraulisch angetrieben.

Bild vom Original:
http://www.museumdecruquius.nl/images/stories/cruquius/slideshow/dia%2812%29.jpg

Video von meinem Modell:
http://www.youtube.com/watch?v=cg3PWoSjeEc
