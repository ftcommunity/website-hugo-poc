---
layout: "overview"
title: "Sinus eines Winkels"
date: 2020-02-22T08:20:41+01:00
legacy_id:
- /php/categories/2948
- /categoriesf503.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2948 --> 
Bestimmung des Sinus eines Drehwinkels.