---
layout: "image"
title: "Sinus - Variante 9 - Gleitführung mit Alu-Profil"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_9_Bild_3_publish.jpg"
weight: "9"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
schlagworte: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39357
- /details25ff.html
imported:
- "2019"
_4images_image_id: "39357"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39357 -->
Mein vorerst letztes Modell zum Sinus.

Die Gleitführung ist hier durch ein kurzes Alu-Profil ersetzt, was erheblich besser geht (und durch Einsatz von ein wenig Silikonspray noch gewinnt).