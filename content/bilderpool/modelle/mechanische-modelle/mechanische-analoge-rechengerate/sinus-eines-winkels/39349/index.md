---
layout: "image"
title: "Sinusgetriebe Variante 1"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_1_Bild_2_publish.jpg"
weight: "1"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
schlagworte: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39349
- /detailse597.html
imported:
- "2019"
_4images_image_id: "39349"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39349 -->
Mein erster Versuch. Vertikale Achse und horizontale Gleitführung.
Nachteilig ist das Verhalten an den Totpunkten, an denen die Kraft oft nicht reicht um den Baustein auf der Achse in Bewegung zu setzen.