---
layout: "overview"
title: "Consul, The Educated Monkey"
date: 2020-02-22T08:20:42+01:00
legacy_id:
- /php/categories/3023
- /categories084d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3023 --> 
Im Jahr 1916 erschien in den USA die Multiplikationshilfe "Consul, The Educated Monkey". Ein "mechanischer Taschenrechner" - und eine tolle 1x1-Lernhilfe. 
Nachbauten aus Blech können heute noch für ca. 15 ? erworben werden - lassen sich aber auch elegant mit fischertechnik konstruieren.