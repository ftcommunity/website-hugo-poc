---
layout: "image"
title: "Consul, The Educated Monkey - Gesamtansicht"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40308
- /details8cc1.html
imported:
- "2019"
_4images_image_id: "40308"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40308 -->
Hier sieht man den Educated Monkey in ganzer Pracht.
Die Multiplikationstabelle findet ihr [hier](https://fischertechnikblog.files.wordpress.com/2015/01/consul-the-educated-monkey-1x1.pdf), eine Designer-Datei der Konstruktion im  [Downloadbereich](https://www.ftcommunity.de/data/downloads/ftdesignerdateien/educatedmonkey01.ftm).
