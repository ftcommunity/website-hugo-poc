---
layout: "image"
title: "Consul, The Educated Monkey - Multiplikation"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40313
- /details31bd-2.html
imported:
- "2019"
_4images_image_id: "40313"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40313 -->
Der Rechenaffe beherrscht das 1x1 bis 12. Multipliziert wird, indem die "Füße" auf die Faktoren geschoben werden; dann zeigen die "Hände" das Multiplikationsergebnis an.
Die Multiplikationstabelle gibt es zum [Download](http://fischertechnikblog.files.wordpress.com/2015/01/consul-the-educated-monkey-1x1.pdf) - und auf besonderen Wunsch gibt es auch noch eine [Additionstabelle](http://fischertechnikblog.files.wordpress.com/2015/01/consul-the-educated-monkey-11.pdf).
