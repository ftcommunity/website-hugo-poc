---
layout: "image"
title: "Consul, The Educated Monkey - Quadrieren"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40314
- /details4c14-2.html
imported:
- "2019"
_4images_image_id: "40314"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40314 -->
Da man die "Füße" nicht beide auf denselben Faktor schieben kann, gibt es eine Quardrieren-Funktion: linker "Fuß" auf den Faktor, recher "Fuß" auf "Q" (in der Additionstabelle ist es ein "D" für "verDoppeln").