---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe - Skala"
date: "2014-03-02T18:43:51"
picture: "CircularScale.jpg"
weight: "6"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38407
- /details8bbe.html
imported:
- "2019"
_4images_image_id: "38407"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38407 -->
Etwas demonstrativer wird die Funktion mit einer Skala.

(Die kann man natürlich auch mit beliebigen anderen Wertebereichen gestalten).
Zu beachten ist, dass das Verhältnis der Winkel zwischen der oberen Skala und den beiden anderen durch das Verhältnis der Zahnräder bestimmt wird. Nimmt man für das untere Zahnrad (r2) 40 und für das obere (r1) 30 Zähne so folgt daraus ein Verhältnis von 1 zu 1,75.