---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe (Zweite Baustufe)"
date: "2014-03-02T18:43:51"
picture: "AddiererRaedkurbelgetriebeZweiteBaustufe.jpg"
weight: "4"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38405
- /details1379.html
imported:
- "2019"
_4images_image_id: "38405"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38405 -->
Hier sind die Zahnräder, Scheiben und Wellen fertig montiert. (Der blau markierte Baustein steckt nur auf der Grundplatte und hat mit dem Rest erstmal keine Verbindung. Leider unglücklich fotografiert).