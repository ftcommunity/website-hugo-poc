---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe - Benannte Elemente"
date: "2014-03-02T18:43:51"
picture: "AddiererRaederkurbelgetriebeGesamtansichtBenannt.jpg"
weight: "2"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38403
- /details849e.html
imported:
- "2019"
_4images_image_id: "38403"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38403 -->
Hier ein Foto mit Benennung der Elemente für Bezugnahme in evtl. Texten.