---
layout: "image"
title: "unteres Teil"
date: "2014-02-11T15:57:07"
picture: "DSCN5214.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/38230
- /details6dd0.html
imported:
- "2019"
_4images_image_id: "38230"
_4images_cat_id: "2847"
_4images_user_id: "184"
_4images_image_date: "2014-02-11T15:57:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38230 -->
Teil mit fest eingebauter Nabe und Achse 35.
Nur im oberen Teil befindet sich eine Freilaufnabe.
