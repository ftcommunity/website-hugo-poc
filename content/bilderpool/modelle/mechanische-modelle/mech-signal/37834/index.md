---
layout: "image"
title: "1/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal1.jpg"
weight: "1"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- /php/details/37834
- /details0998.html
imported:
- "2019"
_4images_image_id: "37834"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37834 -->
Ich habe von einem Kollegen die abgebildeten ft- Kästen bekommen. Dabei war auch die Bauanleitung für das Signal, welches ich nachgebaut habe. Aber bei diesem Bauvorschlage fehlt das Spannwerk. Ein Außenspannwerk habe ich dem Modell dann später noch hinzugefügt, damit es dem Orginal etwas näher kommt.