---
layout: "image"
title: "6/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal6.jpg"
weight: "6"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- /php/details/37839
- /detailsded2.html
imported:
- "2019"
_4images_image_id: "37839"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37839 -->
mech. Signal mit Außenspannwerk