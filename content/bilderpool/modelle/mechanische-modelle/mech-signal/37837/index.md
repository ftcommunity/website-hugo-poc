---
layout: "image"
title: "4/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal4.jpg"
weight: "4"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- /php/details/37837
- /detailsb2b9.html
imported:
- "2019"
_4images_image_id: "37837"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37837 -->
mech. Signal mit Außenspannwerk