---
layout: "overview"
title: "Interactieve Fin-Ray Lachspiegel"
date: 2020-02-22T08:20:38+01:00
legacy_id:
- /php/categories/2852
- /categories2ca3.html
- /categories97f2-2.html
- /categories4406.html
- /categories3af7.html
- /categories27ba.html
- /categoriesba9a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2852 --> 
Voor m'n lachspiegel heb ik 2 spiegelplaten van 0,5 x 1m middels Fischertechnik I-spanten op regelmatige afstanden aan elkaar verbonden ten behoeve van het Fin-Ray-principe. 

Afhankelijk van de instel-positie en/of het gekozen RoboPro-Programma van de Fin-Ray Lachtspiegel wordt aan de ene zijde de bolling en aan de andere zijde de holling van de spiegel groter, kleiner of omgekeerd.  