---
layout: "image"
title: "Elektrische aandrijving"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel07.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38351
- /details4381.html
imported:
- "2019"
_4images_image_id: "38351"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38351 -->
Bij m'n Fischertechnik -Smartbird, -Qualle en -Pijlstaart-rog wordt een rondgaande motorbeweging omgezet in een schuivende beweging welke sinusvormig is met de tijd. 
Nabij de eindposities wordt  de snelheid geleidelijk minder tot 0 en gaat vervolgens terug met toenemende snelheid tot in de middenpositie.  
Dit is hetzelfde principe zoals bij een traditionele stoommachine of een verbrandingsmotor. Je zag het vroeger ook bij de aandrijving van bruggen en sluisdeuren middels een zogenaamd panama-wiel.   
Nadeel van het bovenstaande principe is dat het aandrijfwiel veel ruimte vergt. 
Om deze reden zocht ik naar een directere schuivende of op en neergaande aandrijving. Een schroefspindel vergt echter eveneens ruimte. 

Ik heb uiteindelijk gekozen voor een elektrische aandrijving met een vaste snelheid en een passende rusttijd in eind-posities.
Dit geeft een mooi vloeiend natuurlijke golf-effect voor de Fin-Ray Lachspiegel.


Voorafgaand heb ik echter veel geëxperimenteerd met pneumatische aandrijvingen met de volgende conclusies :

- Aanvankelijk leek mij een pneumatiek-aandrijving welke (co-) sinusvormig in de tijd beweegt heel geschikt, snel, compact en minder storingsgevoelig.  
- Een (co-) sinusvormige beweging in de tijd is in RoboPro goed programmeerbaar. 
- Pneumatische (co-) sinus-tijd-beweging voor een enkelwerkende terugverende cilinder is mogelijk met beperkt perslucht-gebruik.
- Pneumatische cosinus-tijd-beweging voor een dubbelwerkende cilinder, met terugslagkleppen t.b.v. positie-behoud, is mogelijk  doch vergt (te) veel perslucht.
- Pneumatische regelingen blijken altijd lastig en vereisen veelal een compromis tussen snelheid en nauwkeurigheid.  
- Begrenzingen in bv. voedingsspanning of luchtdruk zijn eveneens relevant voor een soepele beweging. Dit gaat dan echter weer ten koste van de benodigde kracht.



