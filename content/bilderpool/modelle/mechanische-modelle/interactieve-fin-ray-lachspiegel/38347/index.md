---
layout: "image"
title: "Inspiratie door Interactief bewegende flexibele wanden TU-Delft  + Daan Roosegaarde"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel03.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38347
- /details6287.html
imported:
- "2019"
_4images_image_id: "38347"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38347 -->
In 2009 waren bij de TU-Delft afd. Bouwkunde  door Festo onderstaande 7 interactief bewegende flexibele wanden achter elkaar  opgesteld.   
Bewegend ziet dat er heel mooi uit :  https://www.youtube.com/watch?v=PVz2LIxrdKc

Veel elektrisch en pneumatisch aangedreven kunst wordt succesvol toegepast door Daan Roosegaarde : 
www.studioroosegaarde.net

Deze is in 2013 vooral bekend geworden van het VPRO-programma Zomergasten.
Zijn "troefkaart-concepten" zijn interactieve bewegende (kunst-) voorwerpen.
