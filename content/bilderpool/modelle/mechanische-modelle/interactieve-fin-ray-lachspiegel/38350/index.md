---
layout: "image"
title: "Bijeenkomst Fischertechnik-Club-Nederland in s'Gravenzanden 22 febr. 2014"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38350
- /details507d.html
imported:
- "2019"
_4images_image_id: "38350"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38350 -->
Belangstelling genoeg.......
