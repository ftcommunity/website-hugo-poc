---
layout: "image"
title: "Fin-Ray Lachspiegel -Interactief"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel21.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38365
- /details117d.html
imported:
- "2019"
_4images_image_id: "38365"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38365 -->
Om de Fin-Ray Lachspiegel interactief te maken heb ik een Ultrasoon-afstandsensor (=I3) gebruikt die de maximum spiegeluitslag (= amplitude) reduceert wanneer je korter dan 130 cm tot de spiegel staat. De symmetrische uitslag ten opzichte van de middenpositie (= bij verticale ongebogen spiegels) wordt kleiner naarmate je dichterbij komt.
Hoe dichter je bij de spiegel staat, des te geringer de bolling zich instelt. 

Vanwege de relatief lage snelheid, geeft een constante snelheid in de tijd én nul tot enkele seconden rust aan beide eindposities nog het mooiste vloeiende natuurlijke golf-effect.  
De rust-tijd voor bij de eindposities kan ik via een 5K-Potmeter (=I2) traploos naar wens instellen.

Youtube-link: 
http://www.youtube.com/watch?v=hk6pu6vlWG8&list=UUjudqZ8_PVUpZRRLsSIJCPA&feature=share
