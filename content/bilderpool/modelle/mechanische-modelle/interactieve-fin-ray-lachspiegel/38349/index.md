---
layout: "image"
title: "Spiegeltje, spiegeltje aan de wand, wie is het schoonste van het land ?"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38349
- /details1223-2.html
imported:
- "2019"
_4images_image_id: "38349"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38349 -->
Om de Fin-Ray Lachspiegel interactief te maken heb ik een Ultrasoon-afstandsensor (=I3) gebruikt die de maximum spiegeluitslag (= amplitude) reduceert wanneer je korter dan 130 cm tot de spiegel staat. 

De symmetrische uitslag ten opzichte van de middenpositie (= bij verticale ongebogen spiegels) wordt kleiner naarmate je dichterbij komt.
Hoe dichter je bij de spiegel staat, des te geringer de bolling zich instelt. 
