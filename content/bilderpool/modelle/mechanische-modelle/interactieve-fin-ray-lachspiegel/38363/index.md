---
layout: "image"
title: "Fin-Ray Lachspiegel -Cosinus-Programma"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel19.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38363
- /detailsaa6f-4.html
imported:
- "2019"
_4images_image_id: "38363"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38363 -->
In verband met de eenvoud en compactheid ben ik uiteindelijk uitgekomen op een tussen de spiegels opgestelde transmissie-motor-aandrijving. 
Deze heeft ca. 7 omw/min. en een zwaai-arm van ca. 35 cm waarvan de draaihoek-positie middels een 5K Potmeter (= I1) continue wordt gemeten.  
Vanwege het benodigde koppel functioneert (helaas) alleen een V=8 snelheid-instelling goed. 
Zie p29:  http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-4.pdf

Met kleine pulsen + hysterese-bijstellingen verkrijg ik een redelijke cosinus-beweging in de tijd.   
De beweging blijft toch enigszins (te) schokkerig en is niet echt vloeiend zoals bij de flexibele Festo-golf-wanden,  of zoals ook o.a. in de natuur voorkomt bij rietstengels.

Link:
http://www.ftcommunity.de/data/downloads/robopro/pneuhoogtecosinus.rpp
