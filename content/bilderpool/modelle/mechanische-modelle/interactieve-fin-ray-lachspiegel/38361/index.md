---
layout: "image"
title: "Spiegeltje, spiegeltje aan de wand, wie is het schoonste van heel het land ?...."
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/38361
- /detailsb161.html
imported:
- "2019"
_4images_image_id: "38361"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38361 -->
......oppassen dat de spiegel niet gaat breken zoals in de Efteling...........
