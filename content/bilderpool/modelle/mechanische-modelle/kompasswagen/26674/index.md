---
layout: "image"
title: "Hohlrad-Kompasswagen - Finale Version"
date: "2010-03-11T19:52:42"
picture: "Antiverdriller_2.jpg"
weight: "12"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26674
- /details1e91.html
imported:
- "2019"
_4images_image_id: "26674"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26674 -->
Eine Version ohne versenktes Rastkegelzahnrad (dafür müssen dann aber DN 50 O-Ringe aus dem Baumarkt verwendet werden) findet sich mit Bauanleitung unter http://www.odts.de/southptr/
