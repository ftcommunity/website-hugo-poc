---
layout: "image"
title: "Rastdifferential-Kompasswagen (rechts)"
date: "2010-03-15T16:56:36"
picture: "Rechts.jpg"
weight: "14"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26690
- /details1027.html
imported:
- "2019"
_4images_image_id: "26690"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-03-15T16:56:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26690 -->
