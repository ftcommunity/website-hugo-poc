---
layout: "image"
title: "Hohlrad-Kompasswagen 2"
date: "2010-02-27T10:39:08"
picture: "HohlradKompass2.jpg"
weight: "7"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26561
- /detailsf670.html
imported:
- "2019"
_4images_image_id: "26561"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-27T10:39:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26561 -->
Hier ist ein Problem für die ft-Experten. Das Kegelzahnrad ist zu kurz, um auf der anderen Seite die Drehscheibe festzuziehen. Den Radabstand kann ich leider nicht vergrößern.
