---
layout: "comment"
hidden: true
title: "11234"
date: "2010-03-17T21:40:12"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Ja, der Zeiger zeigt auch in Kurvenfahrten in eine konstante Richtung. Ohne die Untersetzung hätten die Räder um den Faktor 40/15 größer sein müssen. Wenn man einen Kompaßwagen hat, der allein mit kreisrunden Zahnrädern funktioniert, braucht man immer nur eine ganze Umdrehung zu testen, dann stimmt der Rest automatisch wegen der Linearität des Getriebes. Es gibt auch Kompaßwagen, die mit Exzentermechanismen arbeiten, da kann das natürlich anders sein.

Gruß, Thomas