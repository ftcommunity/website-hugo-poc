---
layout: "image"
title: "Kompassanhänger von vorne"
date: "2010-02-25T17:41:16"
picture: "AnsichtVonVorn720.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26540
- /details9d67.html
imported:
- "2019"
_4images_image_id: "26540"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-25T17:41:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26540 -->
