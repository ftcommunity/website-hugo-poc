---
layout: "image"
title: "ft-Druckerei mit Lettern und Wappen"
date: "2008-11-16T23:15:13"
picture: "PB160036.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/16286
- /detailsdee7.html
imported:
- "2019"
_4images_image_id: "16286"
_4images_cat_id: "1469"
_4images_user_id: "381"
_4images_image_date: "2008-11-16T23:15:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16286 -->
Zum diesjährigen Weihnachtsmarkt machen wir wieder eine Aktion. Kinder (oder auch Erwachsene) können sich selbst eine ft-Platte mit vorgefertigten Buchstaben bestücken. Dann werden die Buchtaben mithilfe eines Schwämmchen mit Stofffarbe benetzt. Die Platte wird dann an der re. Seite der Maschine befestigt, li. wird eine neue Tasche eingespannt und nach unten geklappt. Dann wird die bestückte Platte nach li. geklappt und fest auf die Tasche gedrückt. Dann vorsichtig abheben, (bei Bedarf den Druckvorgang wiederholen), die Tasche entnehmen, - und fertig.

Li. im Bild sieht man ein vorgefertigtes Negenborn-Wappen (leider fehlt schon ein Pfeil, der wird aber wieder angeklebt.)
