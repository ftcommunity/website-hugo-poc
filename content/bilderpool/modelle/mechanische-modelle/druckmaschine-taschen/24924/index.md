---
layout: "image"
title: "Vorfertigung für die Convention 2009"
date: "2009-09-11T21:41:20"
picture: "TaschenTischZuhause.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Drucken", "Taschen", "Tasche", "Druckmaschine"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/24924
- /details216c-2.html
imported:
- "2019"
_4images_image_id: "24924"
_4images_cat_id: "1469"
_4images_user_id: "381"
_4images_image_date: "2009-09-11T21:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24924 -->
Hier schon mal ein Vorgeschmack für die Convention 2009 in Erbes-Büdesheim
