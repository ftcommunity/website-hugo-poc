---
layout: "image"
title: "Mars"
date: "2003-08-27T15:27:45"
picture: "MarsB.jpg"
weight: "10"
konstrukteure: 
- "unbek."
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1360
- /detailsf322.html
imported:
- "2019"
_4images_image_id: "1360"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1360 -->
Die Montierung erlaubt das Anvisieren und Fotografieren astronomischer Objekte.
