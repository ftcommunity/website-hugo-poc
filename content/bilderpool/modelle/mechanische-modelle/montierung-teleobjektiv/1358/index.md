---
layout: "image"
title: "Gesamtmodell"
date: "2003-08-27T15:27:45"
picture: "Gesamtmodell.jpg"
weight: "8"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1358
- /details2d78.html
imported:
- "2019"
_4images_image_id: "1358"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1358 -->
Untergestell mit aufgesattelter Höhenwiege
