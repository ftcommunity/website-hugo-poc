---
layout: "image"
title: "Höhenwiege mit Optik"
date: "2003-08-27T15:27:45"
picture: "Hhenwiege-schrgvorne.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1355
- /details9bc6.html
imported:
- "2019"
_4images_image_id: "1355"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1355 -->
Höhenwiege zur Vertikalverstellung des Objektivs
