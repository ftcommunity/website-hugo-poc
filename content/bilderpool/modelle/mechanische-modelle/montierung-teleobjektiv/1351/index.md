---
layout: "image"
title: "Drehkranzantrieb"
date: "2003-08-27T15:27:45"
picture: "Drehkranz.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["complication"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/1351
- /detailsd565.html
imported:
- "2019"
_4images_image_id: "1351"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1351 -->
Großes Zahnrad mit 178 Zähnen und spielfreier Antrieb.
