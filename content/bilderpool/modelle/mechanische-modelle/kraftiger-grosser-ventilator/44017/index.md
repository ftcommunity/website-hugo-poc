---
layout: "image"
title: "Rotor und Antrieb"
date: "2016-07-26T10:47:34"
picture: "ventilatorgross3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44017
- /detailsb0b1.html
imported:
- "2019"
_4images_image_id: "44017"
_4images_cat_id: "3258"
_4images_user_id: "558"
_4images_image_date: "2016-07-26T10:47:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44017 -->
Antrieb über zwei M-Motoren gekoppelt per dickem Haargummi um verhältnismäßig leisen Betrieb zu ermöglichen. Normale Haushaltsgummis ribbeln innerhalb kurzer Zeit durch und sind auch deutlich lauter.