---
layout: "comment"
hidden: true
title: "23246"
date: "2017-03-18T19:56:49"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Hallo rito,
ganz prima! Dafür kann ich mich auch begeistern.
Die Schlitze würde ich aber tiefer setzen, knapp oberhalb des "Filmstreifens".
Und noch ein Hinweis: Durch die gegenläufige Bewegung von Schlitz und Film erscheinen die Bilder in der Breite gestaucht. Es wäre also günstig, die Filmbilder breit zu dehnen, dann wird es natürlicher. Dafür würde ich mich aber auf ca. 16 Bilder beschränken.
Gruß
Rüdiger