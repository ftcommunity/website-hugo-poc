---
layout: "image"
title: "automatische Ballzuführung für ft KBEM"
date: "2017-02-12T12:32:19"
picture: "IMG_2091.jpg"
weight: "9"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45194
- /detailsb96e.html
imported:
- "2019"
_4images_image_id: "45194"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-02-12T12:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45194 -->
