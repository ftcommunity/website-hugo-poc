---
layout: "overview"
title: "Ft Kickerball Einwurfmaschine (ftKBEM)"
date: 2020-02-22T08:21:06+01:00
legacy_id:
- /php/categories/3358
- /categories00de.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3358 --> 
Die Maschine kann an einen Kickertisch eingehängt werden und wirft den Ball ein, um Spielmanipulation beim Einwurf zu verhindern. Eine Ablaufsteuerung und Ballzuführung befindet sich noch im Bau)