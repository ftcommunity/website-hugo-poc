---
layout: "image"
title: "Ball in Einwurfposition (Aufzug hochgefahren)"
date: "2017-01-29T14:06:53"
picture: "IMG_1083.jpg"
weight: "7"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45100
- /details9a64.html
imported:
- "2019"
_4images_image_id: "45100"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45100 -->
