---
layout: "image"
title: "Ballträger"
date: "2017-01-29T14:06:53"
picture: "IMG_1082.jpg"
weight: "6"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45099
- /details658e.html
imported:
- "2019"
_4images_image_id: "45099"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45099 -->
