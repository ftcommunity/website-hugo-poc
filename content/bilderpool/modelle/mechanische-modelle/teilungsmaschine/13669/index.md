---
layout: "image"
title: "Ergebnis"
date: "2008-02-17T15:39:32"
picture: "05-Ergebnis.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13669
- /detailseaf2.html
imported:
- "2019"
_4images_image_id: "13669"
_4images_cat_id: "1256"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13669 -->
Da die Teilscheiben funktionieren, will ich das Projekt mal gelten lassen.

Gleichzeitig wurde der kleine Labyrinthroboter auf gummibereifte Rollen umgestellt und eine Kugellagerung eingebaut. So ein bisschen hatte ich das Klack-klack-klack der Taster leid.
