---
layout: "image"
title: "Schneidvorgang"
date: "2008-02-17T15:39:32"
picture: "04-Schneidvorgang.jpg"
weight: "4"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/13668
- /details1460.html
imported:
- "2019"
_4images_image_id: "13668"
_4images_cat_id: "1256"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13668 -->
Der Blitz hat tatsächlich die schnelle Bewegung des Sägeblatts eingefroren.

Wie man sieht, ist die Schneidleistung des Sägeblatts von eher zweifelhafter Qualität. Das wurde enttäuschenderweise immer schlechter, so daß nach 6 Teilscheiben meine anfängliche Euphorie etwas nachzulassen begann.

Die Teilscheiben wurden übrigens mit 32 Schnitten hergestellt.
