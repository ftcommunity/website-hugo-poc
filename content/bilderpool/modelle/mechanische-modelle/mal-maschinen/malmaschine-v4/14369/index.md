---
layout: "image"
title: "Malmaschine V4"
date: "2008-04-23T17:06:42"
picture: "malmaschinevderplanetograph10.jpg"
weight: "10"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14369
- /details7666.html
imported:
- "2019"
_4images_image_id: "14369"
_4images_cat_id: "1329"
_4images_user_id: "729"
_4images_image_date: "2008-04-23T17:06:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14369 -->
Das Malmodul