---
layout: "image"
title: "Malmaschine07"
date: "2008-03-25T17:56:41"
picture: "malmaschine05.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14109
- /details8943.html
imported:
- "2019"
_4images_image_id: "14109"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14109 -->
Getriebe X-Richtung von unten