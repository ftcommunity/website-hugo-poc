---
layout: "image"
title: "Malmaschine13"
date: "2008-03-25T17:56:41"
picture: "malmaschine11.jpg"
weight: "11"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14115
- /details0b20-2.html
imported:
- "2019"
_4images_image_id: "14115"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14115 -->
Das Ergebnis der Bemühungen