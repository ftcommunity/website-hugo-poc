---
layout: "image"
title: "Malmaschine21"
date: "2008-03-28T16:39:51"
picture: "malmaschine07_2.jpg"
weight: "19"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14140
- /details6b8a-2.html
imported:
- "2019"
_4images_image_id: "14140"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-28T16:39:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14140 -->
Detail Akkuhalterung