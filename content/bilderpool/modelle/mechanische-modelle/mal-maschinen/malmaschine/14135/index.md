---
layout: "image"
title: "Malmaschine16"
date: "2008-03-28T16:39:51"
picture: "malmaschine02_2.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14135
- /detailsfa60.html
imported:
- "2019"
_4images_image_id: "14135"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-28T16:39:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14135 -->
"Das Endprodukt"