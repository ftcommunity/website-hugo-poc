---
layout: "image"
title: "Malmaschinev3 Mini-06"
date: "2009-01-07T21:22:47"
picture: "malmaschinevmini6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16954
- /detailsa410.html
imported:
- "2019"
_4images_image_id: "16954"
_4images_cat_id: "1526"
_4images_user_id: "729"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16954 -->
von der Seite