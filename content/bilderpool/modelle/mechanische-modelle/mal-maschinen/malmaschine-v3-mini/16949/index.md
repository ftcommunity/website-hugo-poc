---
layout: "image"
title: "Malmaschinev3 Mini-01"
date: "2009-01-07T21:22:47"
picture: "malmaschinevmini1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16949
- /detailsd3a7.html
imported:
- "2019"
_4images_image_id: "16949"
_4images_cat_id: "1526"
_4images_user_id: "729"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16949 -->
von rechts vorne