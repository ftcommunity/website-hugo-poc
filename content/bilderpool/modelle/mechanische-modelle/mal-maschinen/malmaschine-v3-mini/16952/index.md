---
layout: "image"
title: "Malmaschinev3 Mini-04"
date: "2009-01-07T21:22:47"
picture: "malmaschinevmini4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16952
- /details0138.html
imported:
- "2019"
_4images_image_id: "16952"
_4images_cat_id: "1526"
_4images_user_id: "729"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16952 -->
von rechts hinten