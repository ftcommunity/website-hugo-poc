---
layout: "overview"
title: "Malmaschine V5"
date: 2020-02-22T08:18:58+01:00
legacy_id:
- /php/categories/1332
- /categories51ba.html
- /categoriese228.html
- /categories4c63.html
- /categories79aa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1332 --> 
Malmaschine mit 2 Planetengetrieben, 3 Drehkränze Z58 sorgen für Regelmäßigkeit :-) , alles modular.