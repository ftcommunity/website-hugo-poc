---
layout: "image"
title: "Neuer Zeichenarm"
date: "2008-05-04T12:53:13"
picture: "PICT4085.jpg"
weight: "22"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14450
- /detailsa7df-2.html
imported:
- "2019"
_4images_image_id: "14450"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-05-04T12:53:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14450 -->
Neuer Zeichenarm aus Aluprofilen