---
layout: "image"
title: "Malmaschine V5"
date: "2008-04-28T22:27:55"
picture: "malmaschinevplaneto07.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14396
- /detailsbefc.html
imported:
- "2019"
_4images_image_id: "14396"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-04-28T22:27:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14396 -->
Zeichenarm