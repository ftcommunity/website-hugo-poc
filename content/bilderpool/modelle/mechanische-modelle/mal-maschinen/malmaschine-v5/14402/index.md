---
layout: "image"
title: "Malmaschine V5"
date: "2008-04-28T22:27:56"
picture: "malmaschinevplaneto13.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14402
- /detailsc18c-2.html
imported:
- "2019"
_4images_image_id: "14402"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-04-28T22:27:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14402 -->
Ergebnis01