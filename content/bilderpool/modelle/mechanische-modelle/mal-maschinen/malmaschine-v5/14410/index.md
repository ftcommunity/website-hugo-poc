---
layout: "image"
title: "Malmaschine V5"
date: "2008-04-28T22:27:56"
picture: "malmaschinevplaneto21.jpg"
weight: "21"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14410
- /detailsc3c2-2.html
imported:
- "2019"
_4images_image_id: "14410"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-04-28T22:27:56"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14410 -->
Ergebnis09