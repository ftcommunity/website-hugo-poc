---
layout: "overview"
title: "Malmaschine V3"
date: 2020-02-22T08:18:54+01:00
legacy_id:
- /php/categories/1322
- /categories6429.html
- /categories7b27.html
- /categoriesd547.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1322 --> 
Ähnlich wie V2, aber kompakter. Kleinere Bauplatte 500, nur ein Powermotor, geändertes Malmodul.