---
layout: "image"
title: "Antrieb"
date: "2008-04-18T21:08:55"
picture: "malmaschinevderkompaktograph06.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14282
- /details701a-2.html
imported:
- "2019"
_4images_image_id: "14282"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-18T21:08:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14282 -->
Auf Anregung eines einzelnen Herren treibt nun ein Powermotor den Maltisch und das Ablenkmodul an.
Macht den Kasten "Super Drawing Machines" nicht so teuer :-)