---
layout: "comment"
hidden: true
title: "6249"
date: "2008-04-19T10:16:24"
uploadBy:
- "pvd"
license: "unknown"
imported:
- "2019"
---
Auf website hieroben steht bei links eine Bauanleitung für eine Maschine in Papstkarton (!) :

http://www.taitographs.co.uk/beanograph.pdf

Die Baugedanken darin lassen sich sicherlich übertragen auf fischertechnik. Am Ende sagt der Autor "it is hoped that the above helps you to enjoy making a simple machine and encourages you to make your own designs" . 
Das "sun- and planet mechanism" öffnet viele Perspektive für Malmaschinen V4 bis V...