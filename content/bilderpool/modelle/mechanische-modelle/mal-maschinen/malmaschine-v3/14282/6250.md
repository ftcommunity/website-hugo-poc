---
layout: "comment"
hidden: true
title: "6250"
date: "2008-04-19T10:38:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Genau. Nur 1 Motor macht die Sache nicht nur kleiner und billiger, sondern führt auch dazu, dass die gezeichneten Kurven geschlossen sind. Sehr hübsch das alles hier! Ich bin ebenfalls erstaunt über die Qualität der Linienführung.

Gruß,
Stefan