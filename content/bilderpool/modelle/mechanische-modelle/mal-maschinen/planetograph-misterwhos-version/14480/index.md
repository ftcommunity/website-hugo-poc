---
layout: "image"
title: "Gesamtansicht"
date: "2008-05-06T16:31:08"
picture: "planetographmwversion1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/14480
- /detailsc8ef.html
imported:
- "2019"
_4images_image_id: "14480"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-06T16:31:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14480 -->
