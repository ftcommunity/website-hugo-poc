---
layout: "image"
title: "Sicht von oben"
date: "2008-05-06T16:31:08"
picture: "planetographmwversion3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/14482
- /details9516.html
imported:
- "2019"
_4images_image_id: "14482"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-06T16:31:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14482 -->
