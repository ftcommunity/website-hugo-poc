---
layout: "comment"
hidden: true
title: "6444"
date: "2008-05-07T00:25:43"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Warum ist z.B. bei der Meccanokonstruktion der Motor so weit weg von der Zeichenebene? Die Ursachen bei den ft-Konstruktionen sind statische Instabilitäten mit Verwindungen, mechanisches Spiel, nicht im Wälzpunkt laufende Zahntriebe, ruckelnde Kettentriebe mit Leertrum, Laufvibrationen des Systems usw.
Das alles sieht man schwer an der laufenden Maschine, den Beweis dazu aber auf dem Papier.
Gruß, Udo2