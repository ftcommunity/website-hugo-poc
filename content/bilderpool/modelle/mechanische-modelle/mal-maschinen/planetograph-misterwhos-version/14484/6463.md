---
layout: "comment"
hidden: true
title: "6463"
date: "2008-05-10T07:49:53"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo,

so rein maschinenbaulich meine ich, daß vorgespannte Getriebe helfen könnten, damit das Umkehrspiel bei einem Richtungswechsel nicht mehr auftritt. Dann sollte die Maschine recht langsam laufen, um Massenkräfte durch Beschleunigungen zu vermeiden. Dazu Kugellager (finden sich an anderer Stelle) und die Lagerungen ebenfalls für Spielfreiheit spannen. Alle Hebel auf statisch exakte Lagerung hin untersuchen. Und als letztes Tuschestift auf Transparent mit Glasplatte darunter, damit der Stift nahezu reibungsfrei in perfekter Ebene läuft.

Dann sollte das Geruckel der Vergangenheit angehören.

Remadus