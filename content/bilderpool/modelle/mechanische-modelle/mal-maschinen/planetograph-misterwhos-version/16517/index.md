---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:03"
picture: "bildergalerie10.jpg"
weight: "19"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/16517
- /details1590.html
imported:
- "2019"
_4images_image_id: "16517"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16517 -->
Ignoriert die Schrift im Hintergrund ;-) Mir ist passendes Papier ausgegangen und habe einfach die Rückseite benutzt.
