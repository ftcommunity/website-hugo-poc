---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:02"
picture: "bildergalerie06.jpg"
weight: "15"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/16513
- /detailsd44c.html
imported:
- "2019"
_4images_image_id: "16513"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16513 -->
Gleiche Einstellungen wie ein Figuren zuvor, aber früher abgebrochen.
