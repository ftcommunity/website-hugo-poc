---
layout: "overview"
title: "Planetograph - MisterWhos Version"
date: 2020-02-22T08:19:04+01:00
legacy_id:
- /php/categories/1336
- /categories5f71.html
- /categoriese5cd.html
- /categoriesec4b.html
- /categories9ac1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1336 --> 
Meine Version des Planetographen. Der Aufbaut ist etwas anders, aber er hat überall die gleichen Übersetzungen wie Knarfs Bokaj \"Malmaschine - Der Planetograph II\"



Er hat noch drei kleine Probleme:

- Alle erzeugten Figuren haben ein leichtes Ei. Ich vermute, dass das am leichten Durchhängen des Stiftes liegt.

- Wird der Tisch mitgedreht, kommen ziemlich merkwürde Figuren raus.

- Irgendwo zittert es im Getriebe