---
layout: "image"
title: "Malmaschine2-11"
date: "2008-04-07T07:56:04"
picture: "malmaschinev11.jpg"
weight: "11"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14190
- /detailsef46.html
imported:
- "2019"
_4images_image_id: "14190"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14190 -->
Stiftablenk-Modul