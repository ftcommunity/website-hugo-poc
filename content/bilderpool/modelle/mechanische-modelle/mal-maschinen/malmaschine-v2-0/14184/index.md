---
layout: "image"
title: "Malmaschine2-05"
date: "2008-04-07T07:56:04"
picture: "malmaschinev05.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/14184
- /detailsefe4.html
imported:
- "2019"
_4images_image_id: "14184"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14184 -->
Zeichentisch-Modul

Läßt sich aus der Maschine herausziehen