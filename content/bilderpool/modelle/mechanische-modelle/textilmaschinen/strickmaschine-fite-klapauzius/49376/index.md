---
layout: "image"
title: ""
date: 2022-01-12T18:59:10+01:00
picture: "Strickmaschine_07.jpeg"
weight: "7"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Einrichtung der Maschine:
1 bis 5: Einfädeln
6 bis 7: Erste Maschen (siehe auch Anleitungen für Standard-Strickmühlen)
7: Bis der Strickschlauch die Einheit für den Abtransport erreicht hat, sollte mit leichtem Zug auf den Faden (am Ausgang) gestrickt werden.