---
layout: "image"
title: ""
date: 2022-01-12T18:59:13+01:00
picture: "Strickmaschine_05.jpeg"
weight: "5"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Teilzerlegte Maschine