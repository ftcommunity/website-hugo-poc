---
layout: "image"
title: ""
date: 2022-01-12T18:59:14+01:00
picture: "Strickmaschine_04.jpeg"
weight: "4"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Teilzerlegte Maschine und Ansicht der "Einfädeleinheit".