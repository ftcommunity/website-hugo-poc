---
layout: "image"
title: ""
date: 2022-01-12T18:59:15+01:00
picture: "Strickmaschine_03.jpeg"
weight: "3"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Ansicht Strickeinheit. Die Nadeln sind Produkte der Firma Sheens (Ersatznadeln für die Strickmaschinen KR830, KR838, KR850). Wahrscheinlich geht es auch mit Nadeln anderer Hersteller für dieselben Strickmaschinen.