---
layout: "image"
title: ""
date: 2022-01-12T18:59:18+01:00
picture: "Strickmaschine_01.jpeg"
weight: "1"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Strickmaschine im Betrieb und mit weggeklappter Motorisierungseinheit. Die Wolle sollte mit möglichst geringer Gegenkraft vom Knäul gezogen werden können. Andernfalls entstehen große Kräfte auf den 
Nadeln, die zu Strickfehlern führen.