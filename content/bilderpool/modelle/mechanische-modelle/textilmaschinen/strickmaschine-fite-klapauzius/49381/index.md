---
layout: "image"
title: ""
date: 2022-01-12T18:59:17+01:00
picture: "Strickmaschine_02.jpeg"
weight: "2"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Abtransport des gestrickten Schlauches. Der Schlauch sollte immer unter einem (geringen) Zug stehen. 