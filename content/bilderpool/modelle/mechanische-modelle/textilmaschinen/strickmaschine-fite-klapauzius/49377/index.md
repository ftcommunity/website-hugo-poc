---
layout: "image"
title: ""
date: 2022-01-12T18:59:11+01:00
picture: "Strickmaschine_06.jpeg"
weight: "6"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Strickmaschine", "Strickliesl", "Strickmühle"]
uploadBy: "Website-Team"
license: "unknown"
---

Teilzerlegte Maschine