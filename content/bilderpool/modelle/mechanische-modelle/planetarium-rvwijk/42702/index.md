---
layout: "image"
title: "Gears for orbit around the sun"
date: "2016-01-10T17:44:13"
picture: "planetarium03.jpg"
weight: "3"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42702
- /details9bbf.html
imported:
- "2019"
_4images_image_id: "42702"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42702 -->
Here you can see the the differential gear in combination with the others