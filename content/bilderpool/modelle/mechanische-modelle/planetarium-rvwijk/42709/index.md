---
layout: "image"
title: "The Earth and Moon mechanics 3"
date: "2016-01-10T17:44:13"
picture: "planetarium10.jpg"
weight: "10"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42709
- /detailsb593-2.html
imported:
- "2019"
_4images_image_id: "42709"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42709 -->
Some further detail from the Earth / Moon mechanics, copied from Triceratops