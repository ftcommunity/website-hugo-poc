---
layout: "image"
title: "Sun Orbit connect / deconnect 2"
date: "2016-01-10T17:44:13"
picture: "planetarium08.jpg"
weight: "8"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42707
- /details685d.html
imported:
- "2019"
_4images_image_id: "42707"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42707 -->
Detail of the mechanism to connect and deconnect the rotation around the sun from / to the motor. 
In this detail it is deconnected (wormwheel down). The whole system can now be turned manually / by hand