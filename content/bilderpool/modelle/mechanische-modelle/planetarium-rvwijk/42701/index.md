---
layout: "image"
title: "Rotation around the sun"
date: "2016-01-10T17:44:13"
picture: "planetarium02.jpg"
weight: "2"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42701
- /detailse2e4.html
imported:
- "2019"
_4images_image_id: "42701"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42701 -->
The delay for the orbit around the sun is through a series of gears including a differential to create an unusual division of 1.574.
The total division for an orbit around the sun is: (4 x 3 x 1.574 x 58) / 3 = 365.168