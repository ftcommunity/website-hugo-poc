---
layout: "image"
title: "Sun Orbit connect / deconnect 3 & ALU Profil connection"
date: "2016-01-10T17:44:13"
picture: "planetarium09.jpg"
weight: "9"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42708
- /details18ed.html
imported:
- "2019"
_4images_image_id: "42708"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42708 -->
Here you can see the gear just below the ALU profile that makes the connection to the top of the model.