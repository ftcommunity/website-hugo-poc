---
layout: "overview"
title: "Planetarium - rvwijk"
date: 2020-02-22T08:20:58+01:00
legacy_id:
- /php/categories/3179
- /categoriesd2a8.html
- /categoriesc53c.html
- /categories4d87.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3179 --> 
Yet another model of a planetarium with Sun, Earth and Moon. The design was partially copied from Triceratops, however, I gave myself some additional challenges to come to a new design.