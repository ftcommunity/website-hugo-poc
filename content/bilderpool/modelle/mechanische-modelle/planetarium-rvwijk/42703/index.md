---
layout: "image"
title: "The Earth and Moon mechanics 1"
date: "2016-01-10T17:44:13"
picture: "planetarium04.jpg"
weight: "4"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- /php/details/42703
- /details33a8-2.html
imported:
- "2019"
_4images_image_id: "42703"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42703 -->
The beautiful design of Triceratops copied! With his 22 and 7 tooth gears!