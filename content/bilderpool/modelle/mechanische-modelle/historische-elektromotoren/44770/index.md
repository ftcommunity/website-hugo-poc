---
layout: "image"
title: "Jacobi Motor Funktionsmodell von vorn"
date: "2016-11-21T17:35:48"
picture: "Jacobi_Motor_Funktionsmodell_von_vorn.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44770
- /detailsc237.html
imported:
- "2019"
_4images_image_id: "44770"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-11-21T17:35:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44770 -->
