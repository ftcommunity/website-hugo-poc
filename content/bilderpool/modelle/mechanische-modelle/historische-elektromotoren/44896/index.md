---
layout: "image"
title: "Motor von Davenport 1837, Funktionsmodell in Betrieb"
date: "2016-12-13T18:12:41"
picture: "Motor_in_Betrieb.jpg"
weight: "8"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44896
- /details1e44.html
imported:
- "2019"
_4images_image_id: "44896"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-12-13T18:12:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44896 -->
