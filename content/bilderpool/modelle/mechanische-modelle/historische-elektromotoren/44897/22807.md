---
layout: "comment"
hidden: true
title: "22807"
date: "2016-12-13T19:48:22"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Rüdiger,

Wieder ein sehr schönes Funktionsmodell !

Statt eines &#8222;großen Ringmagneten&#8220; im Original hast du nur 2 kleine Magnete in N-S-Ausrichtung installiert.  

- Sind das Neodym- Super-Magneten ?

- Eines &#8222;großen Ringmagneten&#8220; nur eine N-  und  eine S-  Seite.
   Ist das der grund das es mit 4  Magneten nicht funktionieren kann ?

Gruss,

Peter Poederoyen NL