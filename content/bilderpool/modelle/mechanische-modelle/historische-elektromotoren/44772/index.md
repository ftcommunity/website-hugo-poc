---
layout: "image"
title: "Jacobi Motor Funktionsmodell halbrechts"
date: "2016-11-21T17:35:48"
picture: "Jacobi_Motor_Funktionsmodell_halbrechts.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/44772
- /detailse820.html
imported:
- "2019"
_4images_image_id: "44772"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-11-21T17:35:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44772 -->
