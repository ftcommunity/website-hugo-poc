---
layout: "overview"
title: "Kugelpumpe"
date: 2021-12-21T17:45:42+01:00
---

Kugelpumpe - Ballenpump

Der eigentliche Konstrukteur ist Marc mjmpetit; Hmtwerner hat das Modell nachgebaut und hier präsentiert.

Es werden 12 Kugeln mit einem Durchmesser von 32 mm bewegt.

Mehr Informationen: https://forum.ftcommunity.de/viewtopic.php?f=6&t=7095
