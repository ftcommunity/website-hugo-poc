---
layout: "image"
title: "Wiederankunft unten"
date: 2020-05-10T11:38:02+02:00
picture: "2019-10-20 Ball-Bahn10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unten schließlich kommen die Bälle aus zwei Richtungen in den Sammelbereich, um wieder einer nach dem anderen in den Turm zu rollen.
