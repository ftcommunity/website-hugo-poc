---
layout: "image"
title: "Sicherheitsmechanik"
date: 2020-05-10T11:38:00+02:00
picture: "2019-10-20 Ball-Bahn03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Falls doch mal ein Ball zu einem ganz ungünstigen Zeitpunkt in den Turm gelangt, kann der federnde unterste Querbalken den Lauf retten und die sich sonst verkantenden Bälle sauber im Aufzug halten.
