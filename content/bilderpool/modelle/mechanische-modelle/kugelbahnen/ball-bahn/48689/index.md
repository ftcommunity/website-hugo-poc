---
layout: "image"
title: "Kreuzungen"
date: 2020-05-10T11:37:54+02:00
picture: "2019-10-20 Ball-Bahn08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kreuzungen einigermaßen zuverlässig zu bekommen, war echt schwierig, und sie könnten auch gerne zuverlässiger sein. Die letzte von bestimmt zweistellig vielen getesteten Varianten sieht man hier: Alle Kreuzungen außer der obersten heben die Bälle beim Eingang sanft an und lassen sie ansonsten über die die Mittelspur laufen, bis sie am anderen Ende hoffentlich wieder sauber in die Führungsschienen laufen.

Manchmal passiert es aber doch, dass ein Ball mit seinem Drehmoment und der nicht völlig gleichen Masseverteilung (schon durch die Naht, an der die beiden Hälften der Bälle zusammengefügt sind) ungünstig in die Kreuzung einläuft, gegen einen der schwarzen Bausteine an den Ecken der Kreuzung prallt und so seine Energie lieber zum hin- und her-Prallen anstatt zum Lauf verwendet. Dann bleibt er als Störenfried liegen und behindert die nächsten Bälle. Nach einiger Zeit greift dann die Sicherheitsschaltung und legt den Aufzug still, sodass wenigstens nicht noch viel mehr Bälle in den Stau rollen.
