---
layout: "image"
title: "Turmeingang"
date: 2020-05-10T11:38:01+02:00
picture: "2019-10-20 Ball-Bahn02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kommen die Bälle an und werden von einer Lichtschranke detektiert - kommt nämlich zu lange kein Ball an, wird das Modell stillgesetzt und eine Warnlampe (eine ft-Rainbow-LED) leuchtet bzw. blinkt und zeigt so die Störung an.
