---
layout: "image"
title: "Turmausgang"
date: 2020-05-10T11:37:56+02:00
picture: "2019-10-20 Ball-Bahn06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Oben stoßen die Bälle gegen das Rollenpaar und werden so aus dem Turm herausgetrieben.
