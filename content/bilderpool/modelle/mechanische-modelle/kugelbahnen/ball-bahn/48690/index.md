---
layout: "image"
title: "Verteiler-Wippe"
date: 2020-05-10T11:37:55+02:00
picture: "2019-10-20 Ball-Bahn07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Durch diese Wippe gelangen die Bälle immer abwechselnd auf eine der beiden sich kreuzenden Bahnen.
