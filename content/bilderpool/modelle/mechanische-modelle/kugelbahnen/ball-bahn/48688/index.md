---
layout: "image"
title: "Bahngeometrie"
date: 2020-05-10T11:37:52+02:00
picture: "2019-10-20 Ball-Bahn09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick von oben in die frei in die Luft ragenden Kurven.
