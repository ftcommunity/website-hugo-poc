---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-10T11:38:03+02:00
picture: "2019-10-20 Ball-Bahn01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nachbarn hatten eine große Tasche voll dieser Bälle, und natürlich musste da sofort eine Bahn drum rum gebaut werden. Die Bälle laufen rechts im Turm hoch und werden oben auf zwei kurvenreiche Bahnen verteilt, die sich im Turm links in jedem Stockwerk in einer Kreuzung treffen.

Ein Video gibt es unter https://youtu.be/nqzcdBkefpI
