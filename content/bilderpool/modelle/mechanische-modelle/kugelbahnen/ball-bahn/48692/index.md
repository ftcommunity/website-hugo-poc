---
layout: "image"
title: "Sicherheitsschaltung"
date: 2020-05-10T11:37:57+02:00
picture: "2019-10-20 Ball-Bahn05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn man den Motor durch nach außen Schieben auskuppelt, kann man den Aufzug auch mit der Kurbel manuell antreiben. Das ist angenehme für die Justage der Kettenpaare, denn die müssen ja links und rechts die Mitnehmer in derselben Höhe haben.

Das Electronics-Modul ist als nachtriggerbares Monoflop geschaltet. Sollte eine 0-1-Flanke (also das wieder Freigeben der Lichtschranke) länger als einige Sekunden ausblieben, geht das Modell von einer Störung aus, schaltet den Motor ab und stattdessen die Warn-Blink-LED an.
