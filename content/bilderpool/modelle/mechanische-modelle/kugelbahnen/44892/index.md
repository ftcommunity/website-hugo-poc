---
layout: "image"
title: "Hauptprogramm Teil 1"
date: "2016-12-11T16:13:02"
picture: "PRG_HauptProgramm01_2.png"
weight: "23"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44892
- /details39f0-2.html
imported:
- "2019"
_4images_image_id: "44892"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-11T16:13:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44892 -->
Das Hauptprogramm zur Steuerung der Kugelbahn