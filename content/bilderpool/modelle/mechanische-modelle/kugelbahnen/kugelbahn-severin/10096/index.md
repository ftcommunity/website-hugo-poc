---
layout: "image"
title: "Kugelbahn"
date: "2007-04-19T20:05:30"
picture: "severinkugelbahn6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/10096
- /details017f-2.html
imported:
- "2019"
_4images_image_id: "10096"
_4images_cat_id: "914"
_4images_user_id: "558"
_4images_image_date: "2007-04-19T20:05:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10096 -->
