---
layout: "image"
title: "Kugelbahn"
date: "2007-04-19T20:05:30"
picture: "severinkugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/10092
- /details2ab8.html
imported:
- "2019"
_4images_image_id: "10092"
_4images_cat_id: "914"
_4images_user_id: "558"
_4images_image_date: "2007-04-19T20:05:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10092 -->
Der Aufzug stammt aus dem Fanclubmodell, der rest von mir.