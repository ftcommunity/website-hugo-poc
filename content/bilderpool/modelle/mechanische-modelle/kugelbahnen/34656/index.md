---
layout: "image"
title: "Knikkerlift"
date: "2012-03-18T11:56:16"
picture: "IMG_0070.jpg"
weight: "6"
konstrukteure: 
- "Jaap Bosscha"
fotografen:
- "Jaap Bosscha"
schlagworte: ["lifter", "knikkerlift", "marble", "Kugellift", "Murmelrad", "Kugelbahn"]
uploadBy: "JaBoss"
license: "unknown"
legacy_id:
- /php/details/34656
- /details58ca-2.html
imported:
- "2019"
_4images_image_id: "34656"
_4images_cat_id: "1030"
_4images_user_id: "1381"
_4images_image_date: "2012-03-18T11:56:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34656 -->
Aufzug fuer Kugeln. http://youtu.be/xEoHujkzzqk
Ball lifter
