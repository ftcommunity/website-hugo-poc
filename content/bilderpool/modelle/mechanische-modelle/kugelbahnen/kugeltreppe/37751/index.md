---
layout: "image"
title: "Kugeltreppe"
date: "2013-10-22T16:40:22"
picture: "kugeltreppe8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/37751
- /detailsf411.html
imported:
- "2019"
_4images_image_id: "37751"
_4images_cat_id: "2803"
_4images_user_id: "162"
_4images_image_date: "2013-10-22T16:40:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37751 -->
Die Hohe der Treppenstufen ist mittels Bausteine 5, Bauplatten 38241 und Abdecksteine 32330 auf der richtiger Hohe bebracht damit jede Treppenstufe genau passt zum nachsten Treppenstufe wann beide zusammen kommen und den Kugel ein Stufe wieter rollt.