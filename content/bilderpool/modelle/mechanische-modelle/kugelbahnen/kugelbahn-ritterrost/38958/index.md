---
layout: "image"
title: "Kugelbahn mit Pendelaufzug"
date: "2014-06-19T19:13:42"
picture: "kugelbahnmitpendelaufzug09.jpg"
weight: "9"
konstrukteure: 
- "Ritterrost"
fotografen:
- "Ritterrost"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/38958
- /detailsd322.html
imported:
- "2019"
_4images_image_id: "38958"
_4images_cat_id: "2916"
_4images_user_id: "1129"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38958 -->
Gegrenzung für die Kugel in den Etagen