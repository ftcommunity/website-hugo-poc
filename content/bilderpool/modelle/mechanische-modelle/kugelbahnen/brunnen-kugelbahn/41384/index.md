---
layout: "image"
title: "Blick von oben ins leere Magazin"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme05.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41384
- /details569e.html
imported:
- "2019"
_4images_image_id: "41384"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41384 -->
Von links kommen Kugeln an und fallen dann auf die Schwerter.
