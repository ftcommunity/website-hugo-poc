---
layout: "image"
title: "Mehrere Kugeln im Magazin"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme07.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41386
- /detailsae25.html
imported:
- "2019"
_4images_image_id: "41386"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41386 -->
Im Echtbetrieb gibt es hier nützliches Gedränge: Die nachfolgenden Kugeln sorgen dafür, dass die erste zuverlässig ins Innere der Aufnahme gedrückt wird.
