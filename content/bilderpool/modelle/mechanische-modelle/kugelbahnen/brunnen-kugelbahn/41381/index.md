---
layout: "image"
title: "Antrieb (2)"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme02.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41381
- /details2d47-2.html
imported:
- "2019"
_4images_image_id: "41381"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41381 -->
Die Achse des Z30 treibt direkt ein Z10 mit der Kugelaufzugskette an. Man beachte: Es wurden ausschließlich Teile verwendet, die ausschließlich aus Kunststoff bestehen, damit nichts rosten kann. Nicht nur auf Metallachsen wurde verzichtet, nicht mal ein normaler Baustein (mit einem Metallstift-verankerten ft-Zapfen nämlich) wurde verwendet.
