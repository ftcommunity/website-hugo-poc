---
layout: "image"
title: "Befestigung der Kugelaufnahme"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme04.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41383
- /details3882.html
imported:
- "2019"
_4images_image_id: "41383"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41383 -->
Um die richtigen Abstände hinreichend genau zu realisieren, brauchte es diesen Teileverhau.
