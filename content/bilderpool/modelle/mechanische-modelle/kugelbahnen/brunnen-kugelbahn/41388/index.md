---
layout: "image"
title: "Turminneres"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme09.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41388
- /details94d4.html
imported:
- "2019"
_4images_image_id: "41388"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41388 -->
... in dem die Kugeln sich durch diese Gruppe von Winkelsteinen schlängeln muss. Das klappert dann ganz nett.
