---
layout: "image"
title: "Kugelaufnahme von unten"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme03.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41382
- /details875e-2.html
imported:
- "2019"
_4images_image_id: "41382"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41382 -->
Auf den Förderkettengliedern sitzen BS7,5, und darauf Rollenlager 37636. Durch die hindurch gehen diese Schwerter, die aber Kugeln tragen können. So können die Rollenlager Kugel für Kugel aufnehmen und hochziehen, und nachfolgende Kugeln fallen trotzdem nicht nach unten durch.
