---
layout: "image"
title: "1ste Pneumatikweiche---der Kugelbahn V5"
date: "2013-02-09T11:50:06"
picture: "Kugelbahn3.jpg"
weight: "3"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
schlagworte: ["Kugelbahn", "Jonas", "Weiche"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36591
- /details8eb6.html
imported:
- "2019"
_4images_image_id: "36591"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-09T11:50:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36591 -->
Hier ist die erste Pneumatikweiche.
Gerade eben habe ich eine Motorisirte Weiche gebaut, aber noch nicht fotografiert.