---
layout: "image"
title: "Kugeldreher der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Kugeldreher.jpg"
weight: "7"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37308
- /detailsef2c.html
imported:
- "2019"
_4images_image_id: "37308"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37308 -->
Der Kugeldreher der Kugelbahn STRIKE 1. 
Die Kugeln kommen auf die drehbare Bahn, werden nach links gedreht und fallen dann wieder auf die Bahn.