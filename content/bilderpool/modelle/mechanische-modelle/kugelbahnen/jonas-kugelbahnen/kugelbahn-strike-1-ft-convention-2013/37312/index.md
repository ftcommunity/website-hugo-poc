---
layout: "image"
title: "Weiche 2 der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Weiche-2.jpg"
weight: "11"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37312
- /details690d-2.html
imported:
- "2019"
_4images_image_id: "37312"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37312 -->
Die Weiche 2 der Kugelbahn STRIKE 1.
Diese Weiche funktioniert mechanisch. Eigentlich kann man das keine Weiche nennen, weil die Kugeln unmittelbar danach wieder auf die gleiche Spur kommen.