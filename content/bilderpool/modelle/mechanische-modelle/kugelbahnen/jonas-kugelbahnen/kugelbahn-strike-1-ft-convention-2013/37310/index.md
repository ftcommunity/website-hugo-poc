---
layout: "image"
title: "Spirale der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Spirale.jpg"
weight: "9"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/37310
- /details475b.html
imported:
- "2019"
_4images_image_id: "37310"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37310 -->
Die Spirale der Kugelbahn STRIKE 1
Die Kugeln kommen in die Spirale und werden wieder zurück auf die Bahn gedreht.