---
layout: "image"
title: "Treppe der Kugelbahn V5"
date: "2013-02-11T20:43:15"
picture: "Treppe.jpg"
weight: "6"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
schlagworte: ["Kugelbahn", "Treppe", "Jonas"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- /php/details/36602
- /detailsda9b.html
imported:
- "2019"
_4images_image_id: "36602"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-11T20:43:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36602 -->
Hier ist eine Kleine Treppe.
Die 30er Winkel sorgen dafür, dass die Kugel in die Flexischiene geleitet wird.