---
layout: "image"
title: "Kronenkonstruktion"
date: "2015-11-02T21:20:35"
picture: "P1040190.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42207
- /detailscbab.html
imported:
- "2019"
_4images_image_id: "42207"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T21:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42207 -->
Wie stellt man für 15mm breite ft-Bausteine einen Abstand ein, der einen kleinen Hauch breiter ist, so daß nichts scheuert oder klemmt?

Mit zwei WS7,5° gegeneinander. Das spreizt die Geometrie um etwa 0,5mm je Seite. Jetzt hat der Hebel Luft und wackelt trotzdem nicht zu sehr. Weiter unten sitzt dann das Gegenstück und voilà ist alles wieder im Raster.

----

Detail of the top. Two angular parts 7.5° per side, assembled together with 180° offset, widen the original 15mm to roughly 16mm. Now there is enough room for the lever to swing without rubbing at the sides carrying the bearings.