---
layout: "image"
title: "Der Umlenkhebel mit dem kompletten Bogen"
date: "2015-11-02T21:20:35"
picture: "P1040188.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42206
- /details49b2.html
imported:
- "2019"
_4images_image_id: "42206"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T21:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42206 -->
So sieht das oben aus. Das Hebelwerk erzeugt den nötigen Hub von knapp 60mm je Seite und die Kette erlaubt die Umlenkung auf die zwei Hubstangen direkt nebeneinander. Je länger der Hebel umso kleiner ist dessen Schwenkwinkel, was für die Kraftübertragung sehr günstig ist. Das Pleuel zur Kurbelwelle ist hier links zu sehen.

Die Idee zum Hebel mit dem Halbbogen fand ich in einem Büchlein namens "Bewegungsmechanismen" von 1925.

----

The top side with the complete lever mechanism. The lever swings nearly 60mm up and down and the chains provide this movement 1:1 to the two lifting rods. The lifting rods are close to each other (and not visible in the picture). The longer the lever, the less angular rotation is needed. This detail has an positive effect on force transmission from the piston rod. The piston rod is the small metall rod on the left hand side.