---
layout: "image"
title: "Das Getriebe - formschlüssige Kurbelwelle für harten Dauereinsatz"
date: "2015-11-02T18:00:14"
picture: "P1040187.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42203
- /details92a7-2.html
imported:
- "2019"
_4images_image_id: "42203"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T18:00:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42203 -->
Die alte Hebekunst (http://www.ftcommunity.de/categories.php?cat_id=3106) war ja nun nicht so besonders toll geraten. Die Kurbelwelle war zu instabil. Hier ist die verbesserte Version. Diese Kurbelwelle stemmt alles auf die Kurbel was der Motor hergibt. Der Prototyp verwendet einen 'alten' Encodermotor (135484), aber mit Powermotor 50:1 wird ebenfalls das VOLLE Motormoment übertragen - bis die Brocken fliegen! Die Kraftübertragung erfolgt formschlüssig.

Die Kurbel ist symmetrisch gebaut und besteht im Kern je Seite aus:
Metallachse, Länge nach Bedarf
31015 - Flachnabe mit Mutter
31022 - Z40
31019 - Drehscheibe
32316 - Verbindungsstopfen, 3x
32881 - BS15, 2x
37237 - BS5

Die Drehscheibe sitzt auf der Klemmnabe und ist auf der Metallachse befestigt. Die Metallachse ragt in die Quernut des inneren BS15. Ein BS5 stellt den Abstand zum äußeren BS15 her, dessen Zapfen in einer Nut der Drehscheibe sitzt. In einer Längsnut des äußeren BS15 sitzt eine 40er-K-Achse (38414) als Kurbelzapfen. Das Z40 ist mit den Verbindungsstopfen an der Drehscheibe befestigt. Die Motorleistung wird direkt in beide Z40 eingekoppelt.

Für härtere Beanspruchungen wird eine 40er-Metallachse (non-ft) sowie Kugellagerung des Pleuelauges empfohlen. Mit dem Encodermotor langt noch Vaseline.

Nachtrag: Der KR (Kurbel-Radius) ist etwa 30mm. Die Kurbel dreht mit etwa 60/min.

----

After having severe trouble with the first version (http://www.ftcommunity.de/categories.php?cat_id=3106) a complete new gear and crankshaft have been developed. Due to form-locking drive (where necessary) and much more massive crank the motors full torque is available now. This means a powerful motor (like a PM 50:1) can ruin parts.

The current version operates with roughly 60/min of the crankshaft. This gives up to 120 marbles/min output rate.

The picture shows the protoype version of the gear and crank core.