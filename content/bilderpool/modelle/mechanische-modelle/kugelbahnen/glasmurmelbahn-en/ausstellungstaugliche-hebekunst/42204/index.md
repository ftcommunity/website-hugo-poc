---
layout: "image"
title: "Der Umlenkhebel"
date: "2015-11-02T21:20:35"
picture: "P1040196.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42204
- /detailsd5a6-3.html
imported:
- "2019"
_4images_image_id: "42204"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2015-11-02T21:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42204 -->
Eine Symphonie aus BS5 und WS15° ergibt einen schönen und stabilen Halbbogen. Auf diesem Halbbogen laufen zwei Ketten zu den Hubstangen. Die Verbindung zum Pleuel erledigen 45er-Loch-Streben (L-Streben) die doppelseitig gesenkte Löcher aufweisen. Der BS15 mit zwei runden Zapfen hält die Kette fest.

----

An arrangement of building parts 5mm together with angular parts 7.5° give a stable arc. This arc supports a chain on each side leading them to the lifting mechanisms.