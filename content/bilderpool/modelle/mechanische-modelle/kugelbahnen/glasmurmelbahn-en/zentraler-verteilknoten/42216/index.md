---
layout: "image"
title: "Zentraler Zufallsverteiler"
date: "2015-11-06T21:29:57"
picture: "pic1.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42216
- /details2457.html
imported:
- "2019"
_4images_image_id: "42216"
_4images_cat_id: "3148"
_4images_user_id: "1557"
_4images_image_date: "2015-11-06T21:29:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42216 -->
Zwei Teilkreisläufe treffen im zentralen Verteiler zusammen (im Bild von links hinten und rechts vorne). Nur durch den statistischen Zufall werden die ankommenden Murmeln zu zwei Hebeaggregaten verteilt (im Bild die Strecken nach rechts hinten und links vorne).

Die neue Konstruktion dieser Baugruppe hat etwas weniger Höhenverlust und baut deutlich kleiner als der Vorgänger. Dafür ist die Funktion noch erheblich verbessert. Und weil nicht genug graue Teile im Haus waren, ist er halt hauptsächlich schwarz in der Unterkonstruktion. Rote Eckknotenplatten habe ich nicht und so sind die Auslaßportale dann grau.

In der oberen linken Bildecke sieht man die neue Gelenkwelle zum Hebeaggregat der Tischtennisballstrecke. Die Kupplung aus V-Stein und halbem Kardangelenk dient minimal dem Längenausgleich (für wechselnde Tische) und gestattet es die Welle schnell zu trennen. Ein Vorzug, den schon Harald entdeckte, oder war es steffalk?

Nachtrag: Weder Harald, noch steffalk, so wie es aussieht ist es Peter's Initiative: http://www.ftcommunity.de/details.php?image_id=33933

----

Two tracks join in the central distributor. One is coming in from the upper left and the other one from the lower right. Just by chance the marbles then get out to one of the two tracks (upper right, lower left) leading to the lifters.

The new design gives less loss of height, is smaller and has improved reliability.