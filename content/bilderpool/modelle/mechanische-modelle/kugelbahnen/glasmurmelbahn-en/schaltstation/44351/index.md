---
layout: "image"
title: "Schaltstation 2015 - Innereien"
date: "2016-09-10T14:26:54"
picture: "schaltstation3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
schlagworte: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44351
- /details99b1-2.html
imported:
- "2019"
_4images_image_id: "44351"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44351 -->
Das Innenleben der Schaltstation. Verteilerplatten und Verdrahtungskanäle sorgen für eine gewisse Ordnung. Die Adern sind auf Maß angepaßt.

---

The internals of the switching station. Electrical distributors and wiring ducts keep it tidy. The wires are cut to size.