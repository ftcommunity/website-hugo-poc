---
layout: "comment"
hidden: true
title: "21570"
date: "2016-01-15T16:55:44"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Danke H.A.R.R.Y für die Detailbilder.
Das schürt die die Vorfreude den Porsche bzw. Benz endlich live zu sehen.
Mein Sohn liegt mir schon länger in den Ohren, dass wir das auch mal probieren sollen und er sie "in echt" sehen möchte, nicht nur auf YouTube.

Hast du evtl. schon in einem Beitrag beschrieben, wie du die Alustangen gebogen hast?

Konstruktive Grüße
Kai