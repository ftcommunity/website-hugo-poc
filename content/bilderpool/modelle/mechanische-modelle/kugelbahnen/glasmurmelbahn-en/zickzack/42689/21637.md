---
layout: "comment"
hidden: true
title: "21637"
date: "2016-02-01T07:33:25"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Also bei dem ganzen Vergleich fällt mir immer spontan dieser Refrain ein:
"Oh Lord, could you buy me a Mercedes-Benz. My friends all drive Porsches I must make an emance [...]" (Joan Baez wenn ich mich recht entsinne)

Zu den Alustangen gibt es nicht viel zu sagen. Die sind deswegen Alu und nicht Stahl, weil ich das mit Haushaltsmitteln (z.B. Kochtopf als Hilfe) und handwerklichem Geschick selbst biegen kann. Für Stahl braucht man erheblich mehr Kraft, die ich nicht mit der nötigen Präzision aufbringen kann..

Die Vorgehensweise:
Zuerst mache ich wenigstens eine grobe Skizze, die mir die Kurve zeigt (Bahnmitte reicht, aber am Anfang und am Ende der Kurve gibt es gerade Überstände). Die nötigen Maße (alle Längen, der Winkel und der Radius) sind da eingetragen. Dann muß ein Excel-Sheet aus der Bahmittengeometrie die innere und äußere Schienenlänge ausrechnen - da ist die Überhöhung der Steilkurve mit drin und noch ein paar Feinheiten. Geht auch schriftlich oder mit Taschenrechner, ist aber fehleranfällig weil umfangreich. Man muß aber für die optimale Steilkurve auch wissen wie schnell die Murmeln durchflitzen.

Wenn die beiden Schienenlängen bekannt sind, kommen zuerst Maßstab und Bleistift, danach Handsäge und Schlüsselfeile (zum Entgraten) zum Einsatz. Zweimal messen, einmal sägen - nicht umgekehrt. Beim Anzeichnen auch die Grenzen der geraden Stücke markieren, dazwischen sitzt der Bogen.

Biegen: naja, dafür nehme ich einen Kochtopf mit kleinerem Durchmesser (weil das Alu wieder auffedert) und biege in kleinen Abschnitten immer schön glatt an die Topfwand. Wenn es die Größe gestattet mache ich eine Zeichnung 1:1 mit Lineal und Zirkel an der ich das Ergebnis ausrichten kann. Meist hantiere ich erst an einer Schiene bis sie in die Halter paßt, danach die andere bis sie ebenfalls in die Halter paßt UND überall den gleichen Abstand zur ersten Schiene hat. Das ist ein bißchen fummelig, und am zeitaufwendigsten bei der ganzen Aktion. Je mehr Zeit ich mir lasse, umso besser wird das Ergebnis. Und es kann sein, daß ich beim Probelauf nach ein paar Minuten ausschalte, die Schienen ausbaue und wieder nachbiege.

Grüße
H.A.R.R.Y.