---
layout: "image"
title: "Turmspitze - linker Turm"
date: "2016-01-08T13:30:55"
picture: "zickzack6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42694
- /details5141.html
imported:
- "2019"
_4images_image_id: "42694"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42694 -->
Oben einfach offen lassen geht natürlich auch, aber mit Deko gibt er mehr für's Auge her. Wieder sorgen BS15 RZ (31059) für die Anpassung des Winkels an die Turmkrone. Die Radachsen mit den roten Hülsen sind ebenfalls nur schmückendes Beiwerk.

Die Murmeln kommen von links hinten über die Kurve angerollt und treten so in der obersten Ebene ins ZickZack ein.

----

Top of the tower

Just leaving the top open would be a simpler way but some decorations give it a better look. Again elements with roung tongues (31059) are used to get the right angles. The red parts are also just decorations.

The marbles are coming from the left side entering the top level of the ZigZag.