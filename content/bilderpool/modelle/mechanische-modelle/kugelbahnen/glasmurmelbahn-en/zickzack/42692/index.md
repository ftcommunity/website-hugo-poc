---
layout: "image"
title: "Mittelstück - linker Turm"
date: "2016-01-08T13:30:55"
picture: "zickzack4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42692
- /detailsdf83.html
imported:
- "2019"
_4images_image_id: "42692"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42692 -->
Gemäß der Fundamentgeometrie zieht sich die Gitterkonstruktion (X-Streben waagerecht, I-Streben diagonal) bis zur Spitze hin. Die Murmelumlenkungen sind im Inneren des Turmes versteckt. Auf halber Höhe quert eine andere Bahn den Turm in einer Lücke.

----

Middle section - left side tower

According to the geometry of the foundation the construction raises to the top end (X-struts horicontally, I-struts as diagonals). The marble guiding is hidden inside the towers. The left side tower supports another track transversing it at roughly half of the height through one of its gaps.