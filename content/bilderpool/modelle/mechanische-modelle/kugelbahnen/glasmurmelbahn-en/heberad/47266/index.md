---
layout: "image"
title: "Der Motor"
date: "2018-02-11T21:48:50"
picture: "heberad6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47266
- /detailse859-2.html
imported:
- "2019"
_4images_image_id: "47266"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47266 -->
Ganz konventionell per XS-Motor und U-Getriebe. Dann noch via Z10 und Z40 auf die erste Antriebswelle untersetzt und schon ist der Antrieb erledigt.
Insgesamt ergibt sich eine Hubleistung von etwa 80 Murmeln pro Minute bei 6 V. Der XS-Motor läuft etwa am Nennarbeitspunkt für 6 V und ist weit von jeglicher Überlastung entfernt. Das U-Getriebe freut sich trotzdem sehr über einen Hauch Kugellagervaseline auf der Motorschnecke.