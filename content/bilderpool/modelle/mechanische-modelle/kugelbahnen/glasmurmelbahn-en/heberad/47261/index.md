---
layout: "image"
title: "Frontansicht"
date: "2018-02-11T21:48:50"
picture: "heberad1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47261
- /details5e87-3.html
imported:
- "2019"
_4images_image_id: "47261"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47261 -->
Das Heberad - die fischertechnik-Version des üblichen Rades zur Murmelförderung: http://www.kugelbahn.info/deutsch/rad/steck.html
Damit fing es eigentlich an. Anstelle eines grossen Rades mit Deckscheibe (damit die Murmeln nicht hinten rausfallen) gibt es ein großes Hohlrad. Etwa so wie eine Fahrradfelge ohne Speichen. Allerdings ist dieses Rad nicht richtig rund sondern ein 96-eck basierend auf 48 Winkelsteinen 7,5° und ebensovielen schwarzen Bausteinen 15. Oben sitzen zwei Wellen auf denen das Rad aufliegt. Auf der Aussenseite des Rades läuft eine Kette um, die an jeder der 24 Trennwände mittels Förderkettengliedern befestigt ist. Es ergeben sich so 24 Kammern zum Murmeltransport - und ein Massengrab für BS15 sowie WS7,5°.