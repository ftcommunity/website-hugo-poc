---
layout: "image"
title: "Das ist Spitze"
date: "2018-02-11T21:48:50"
picture: "heberad8.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47268
- /detailsab34-2.html
imported:
- "2019"
_4images_image_id: "47268"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47268 -->
Also genauer gesagt ist das die Fußlagerung der Turmspitze. Die gelenkigen Teile stellen sich selbst auf die Vorgabe durch die Statikstreben ein, die wiederum einen Abschlußstein tragen - pure Dekoration.

Die im Bild rollende Murmel ist auf einer zweiten Strecke unterwegs, die da einfach nur die Stützen ausnutzt. Mit dem Heberad hat die nichts weiter zu tun.