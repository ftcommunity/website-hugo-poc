---
layout: "image"
title: "Ausstiegsbereich"
date: "2018-02-11T21:48:50"
picture: "heberad4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47264
- /detailsab64-3.html
imported:
- "2019"
_4images_image_id: "47264"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47264 -->
Sind die Murmeln oben angekommen, rollen sie über die grosse, rote Bauplatte (90 x 30) weiter zu den Schienen. Diese Bauplatte ist um 7,5° zu den Schienen geneigt.