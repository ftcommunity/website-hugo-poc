---
layout: "image"
title: "Einstiegsbereich"
date: "2018-02-11T21:48:50"
picture: "heberad2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47262
- /details1dc9-2.html
imported:
- "2019"
_4images_image_id: "47262"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47262 -->
Zuerst mal ist da eine in zwei Richtungen geneigte Bauplatte 90 x 30. Die führt die Murmeln freundlich zum Rad. Weitere Bauplatten und Flachsteine bilden die Umzäunung. Aus der Warteschlange rollt dann (fast) immer eine Murmel in eine vorbeiziehende freie Kammer. Zunächst liegt jede Murmel auf der Kette auf. Die Form der Kettenglieder hält eine Murmel sicher in der Kammer und ist zeitglich die dünnst mögliche Konstruktion der Aussenwand. Zwischen Kette und Grundplatte sind nur etwa 2 mm Luft.

Von vorne und von hinten verhindern seitliche Führungsrollen ein Wegpendeln des frei hängenden Rades.