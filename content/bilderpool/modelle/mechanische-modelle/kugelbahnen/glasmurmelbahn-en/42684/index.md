---
layout: "image"
title: "Glasmurmelbahn 'Jubiläumsedition'"
date: "2016-01-08T13:30:54"
picture: "uebersichten2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42684
- /details6f48.html
imported:
- "2019"
_4images_image_id: "42684"
_4images_cat_id: "3146"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42684 -->
Erweitert aus "A Tribute To Imperfection" anläßlich der Ausstellung zur 50 Jahresfeier von fischertechnik in Waldachtal Tumlingen 2015.

Enhancement from "A Tribute To Imperfection" made for the fan exhibition at the 50 years anniversary of fischertechnik at Waldachtal-Tumlingen in 2015.