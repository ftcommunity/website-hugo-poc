---
layout: "image"
title: "Festgemauert in der Statik ..."
date: "2016-01-08T13:30:54"
picture: "trichter2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42686
- /details6351.html
imported:
- "2019"
_4images_image_id: "42686"
_4images_cat_id: "3176"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42686 -->
... oder so ähnlich beginnt ein bekanntes Gedicht von Friedrich Schiller. Festgemauert ist aber das wichtigste Stichwort um die optimale Wirkung aus dem Trichter herauszuholen. Wenn er wackelt macht er keine Freude. Für die Unterkonstruktion sind bewußt die ungewöhnlichen Spitzbögen ausgewählt worden. Sie sind sehr stabil, selten gezeigt und ergeben ein passendes Maß für die Halterung des Trichters.

----

Fixed into its housing ...

... the funnel resides in a pretty stable construction. Stability is a must otherwise the effect of the funnel is reduced dramatically. The construction consists of pointed archs which are really stable. Those archs also give the right dimensions to support the funnel.