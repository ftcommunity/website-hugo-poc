---
layout: "overview"
title: "Trichter"
date: 2020-02-22T08:16:48+01:00
legacy_id:
- /php/categories/3176
- /categories887f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3176 --> 
Kein gewöhnlicher Trichter sondern ein sogenannter Exponential- oder Potentialtrichter. Manchmal muß es eben ein Fremdteil sein.

----

**Funnel**

Not an ordinary funnel but a very special one. A so called exponential or potential funnel. Sometimes it has to be a non-ft part.