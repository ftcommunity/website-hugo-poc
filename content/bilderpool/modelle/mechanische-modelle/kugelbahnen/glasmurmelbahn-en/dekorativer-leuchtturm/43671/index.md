---
layout: "image"
title: "Der Leuchtturm"
date: "2016-06-03T15:50:44"
picture: "leuchtturm1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["lighthouse", "octogonal"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43671
- /detailsfd7b-2.html
imported:
- "2019"
_4images_image_id: "43671"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43671 -->
Als Zierwerk auf dem Oktogon der Glasmurmelbahn angebracht.

Der Turmsockel ist also ebenfalls achteckig ausgeführt. Mechanisch ist er weitgehend von den Schienenstützen entkoppelt. Selbstverständlich gibt es eine Plattform für den Leuchtturmwärter.

----

The lighthouse.

It is just a decorative element. Due to its position it needs to have an ocotogonal shaft. This way it can reside on the already mounted octogonal track support. The track support and the lighthouse shaft are mechanically decoupled. This reduces vibrations.

Of course there is a platform for the lighthouse keeper.