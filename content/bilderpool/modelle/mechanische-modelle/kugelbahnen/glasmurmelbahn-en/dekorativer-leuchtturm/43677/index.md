---
layout: "image"
title: "Wartungsplattform von unten"
date: "2016-06-03T15:50:44"
picture: "leuchtturm7.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43677
- /details8eb3-2.html
imported:
- "2019"
_4images_image_id: "43677"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43677 -->
Hier noch ein Blick unter die Wartungsplattform. Zwei Statikscharniere je Träger bilden den Längenausgleich. Die Strebenpakete stützen nach unten ab und die Lagerböcke / Kupplungsstücke bilden den "Gitterrost".

----

A shot below the platform to give an idea how to get the necessary measures with the discrete lenght fischertechnik elements. Two so called "static hinges" allow for length adjustment. Diagonal studs support the outer frame and some well selected elements form the catwalk.