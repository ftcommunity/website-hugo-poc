---
layout: "overview"
title: "Dekorativer Leuchtturm"
date: 2020-02-22T08:16:50+01:00
legacy_id:
- /php/categories/3232
- /categoriesc31e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3232 --> 
Es gab schon lange keinen Leuchtturm mehr im Bilderpool. Also war es Zeit mal einen zu bauen. Inspiriert vom Original aus 1976 (http://ft-datenbank.de/details.php?ArticleVariantId=0f71a422-cf0f-4bfe-a1f3-b0f2ed40b181) kommt hier meine Interpretation des Themas.