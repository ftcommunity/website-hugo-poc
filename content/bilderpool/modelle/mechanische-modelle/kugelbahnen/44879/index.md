---
layout: "image"
title: "Kugelbahn Fördermechanismus"
date: "2016-12-10T11:17:30"
picture: "P1040709.jpg"
weight: "10"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn", "Fördermechanismus"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44879
- /details28e1.html
imported:
- "2019"
_4images_image_id: "44879"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T11:17:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44879 -->
Der Schieber nimmt die Kugel von der Verteilerrinne ab und befördert sie auf die Gabel des Turmaufzugs.

Rechts und Links vom Turm sind zwei Motoren zu sehen.
Links der Motor für die Haltestange (Motor_04), die den Ballstapel solange in Position hält bis der Aufzug (Motor 03) einen neuen Ball unter den Stapel gebracht hat.