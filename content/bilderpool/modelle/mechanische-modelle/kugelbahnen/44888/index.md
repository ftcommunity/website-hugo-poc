---
layout: "image"
title: "Beschriftung Heber"
date: "2016-12-10T21:29:02"
picture: "P1040719.jpg"
weight: "19"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn", "Impulszähler"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44888
- /details94a5-2.html
imported:
- "2019"
_4images_image_id: "44888"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44888 -->
Der Heber wird über den Motor Nr. 3 gesteuert.
Die Inputschalter sind D2=E5_HEB (Impulszähler) und D13=E6_HEB (Endschalter).
Die zufahrenden Position sind Unten, Mitte und Oben.