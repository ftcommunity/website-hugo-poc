---
layout: "image"
title: "Korbwerfer (8)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46816
- /details8855-2.html
imported:
- "2019"
_4images_image_id: "46816"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46816 -->
Die eine Umdrehung pro Wurf geht über eine lange Achse mit Kardangelenk auf Z30 nach Z20 (also um den Faktor 1,5 übersetzt!) auf die Nockenscheiben im Hintergrund. Die heben abwechselnd Hebel an, die die Bälle daran hindern, aus dem Korb hinaus zum nächsten Modul zu gelangen.
