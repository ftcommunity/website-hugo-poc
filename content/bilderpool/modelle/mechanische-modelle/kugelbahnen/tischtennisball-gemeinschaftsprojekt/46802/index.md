---
layout: "image"
title: "Überblick (2)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46802
- /detailsea28.html
imported:
- "2019"
_4images_image_id: "46802"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46802 -->
Letztlich konnte ich alles mit über 10 Bällen stundenlang laufen lassen, ohne dass irgendeine Fehlfunktion auftrat, ein Ball verloren ging oder dergleichen.
