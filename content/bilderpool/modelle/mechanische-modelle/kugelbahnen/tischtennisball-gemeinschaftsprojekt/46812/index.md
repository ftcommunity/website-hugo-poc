---
layout: "image"
title: "Korbwerfer (4)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46812
- /details1a96-2.html
imported:
- "2019"
_4images_image_id: "46812"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46812 -->
Damit das Zurückschnalzen des Hebels keinen Schaden und vor allem keinen Krach verursacht, und damit sich die Mechanik durch die Belastung nicht verjustiert, wird es beim Aufprallen auf das aufgewickelte Gummi und die biegsamen Platten 15x90 gedämpft.
