---
layout: "image"
title: "Korbwerfer (7)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46815
- /details60c1.html
imported:
- "2019"
_4images_image_id: "46815"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46815 -->
Hach ja, die Silberlinge! In den 1970ern hatte das Wort "Elektronik" halt noch ein geheimnisvolles, zauberhaftes Flair.

Die Schaltung ist einfach: Der h4-Grundbaustein links ist ein einstellbar empfindlicher Operationsverstärker und Schmitt-Trigger, der das Signal der Fotozelle aufnimmt und damit das Relais rechts steuert. Das schaltet den Motor ein, wenn die Lichtschranke unterbrochen wurde, und solange der Schalter über der Kurvenscheibe losgelassen ist, wird der Relaiskontakt einfach überbrückt. Der Ball kommt also in die Lichtschranke, die Kurvenscheibe dreht, und sobald sie den Taster losgelassen hat, läuft sie weiter, auch wenn die Lichtschranke durch den hochgekickten Ball wieder frei ist - bis der Taster nach einer Umdrehung wieder von der Kurvenscheibe betätigt wird.
