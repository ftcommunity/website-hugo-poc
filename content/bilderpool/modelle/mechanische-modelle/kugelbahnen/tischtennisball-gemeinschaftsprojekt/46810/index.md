---
layout: "image"
title: "Korbwerfer (2)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46810
- /detailsf2b9-4.html
imported:
- "2019"
_4images_image_id: "46810"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46810 -->
Die Gummizüge sind mit folgenden Gedanken gebaut:

- Wir verteilen die Last auf mehrere (4) Gummis. Falls eine reißen sollte, kommen die Bälle so zumindest noch sauber in den Auffangkorb und bleiben also in der Anlage.

- Die Gummis sind über lange Strecken geführt, damit sich bei Drehung ihre Kraft nur wenig ändert, sprich: möglichst konstant bleibt.

- Aus demselben Grund sind sie um Räder mit kleinem Radius gewickelt, nämlich einfach über ft-Naben (in denen sie einfach festgeschraubt wurden) und nicht etwa um ft-Drehscheiben.

- Die Räder sind nicht knallfest angezogen, sodass man die benötigte Kraft bei Bedarf durch Verdrehen der Räder justieren kann. Schwerere Bälle benötigen mehr Kraft als leichtere.

- Die Gummis ziehen fast symmetrisch von links und rechts, damit möglichst wenig Zug auf die Lager der Achse kommt.

Man sieht auch gut die Lichtschranke, die einen ankommenden Ball detektiert.

Auch die Bleche über dem Eingang sind wichtig: Falls mehrere Bälle pep nacheinander ankommen, könnte der zweite sonst vom ersten nach hinten aus dem Modell gekickt werden, wenn dieser erste hochgeworfen wird.
