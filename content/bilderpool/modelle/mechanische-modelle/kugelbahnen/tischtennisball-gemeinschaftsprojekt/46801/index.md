---
layout: "image"
title: "Überblick (1)"
date: "2017-10-16T19:45:35"
picture: "ttballmaschinen01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46801
- /details8b05.html
imported:
- "2019"
_4images_image_id: "46801"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46801 -->
Das rechte Modul schiebt Tischtennisbälle mit drei pneumatisch betätigten Hebeln um vier Ecken, das linke wirft die Bälle in den Korb. In dieser Konfiguration liefen die beiden Module bei mir viele viele Stunden, um auch das letzte Quäntchen Unzuverlässigkeit auszumerzen.
