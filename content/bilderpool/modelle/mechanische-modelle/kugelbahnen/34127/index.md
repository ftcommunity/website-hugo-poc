---
layout: "image"
title: "Flexschienen-Aufhängung mittels Kugelgelenk"
date: "2012-02-09T11:52:14"
picture: "Flexschienen-Aufhngung.jpg"
weight: "5"
konstrukteure: 
- "Karl Tillmetz (charlie2700)"
fotografen:
- "Karl Tillmetz (charlie2700)"
schlagworte: ["Kugelgelenk", "Flexschienen-Aufhängung"]
uploadBy: "charlie2700"
license: "unknown"
legacy_id:
- /php/details/34127
- /details8cf1.html
imported:
- "2019"
_4images_image_id: "34127"
_4images_cat_id: "1030"
_4images_user_id: "529"
_4images_image_date: "2012-02-09T11:52:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34127 -->
Kugelgelenk als effektive Flexschienen-Aufhängung ohne Winkelsteine
