---
layout: "image"
title: "Neuer Ball im Aufzug"
date: "2016-12-10T21:29:02"
picture: "P1040716.jpg"
weight: "17"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn", "Aufzug"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44886
- /details5e53-3.html
imported:
- "2019"
_4images_image_id: "44886"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44886 -->
