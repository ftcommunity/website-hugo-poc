---
layout: "image"
title: "Verladepunkt 2"
date: "2012-10-04T20:21:08"
picture: "fishserstekugelbahn6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35787
- /details1732.html
imported:
- "2019"
_4images_image_id: "35787"
_4images_cat_id: "2645"
_4images_user_id: "1113"
_4images_image_date: "2012-10-04T20:21:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35787 -->
Hier ist der Stoppstein entfernt damit man die Neigung von 7,5° besser sehen kann.