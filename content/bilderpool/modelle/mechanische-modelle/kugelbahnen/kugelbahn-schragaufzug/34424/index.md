---
layout: "image"
title: "Die Rückseite Flexschienenbahn"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34424
- /detailse10a.html
imported:
- "2019"
_4images_image_id: "34424"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34424 -->
Die Rückseite Flexschienenbahn