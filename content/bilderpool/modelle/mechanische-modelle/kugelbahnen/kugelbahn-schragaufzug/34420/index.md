---
layout: "image"
title: "Kugelbahn mit Schrägaufzug Gesamtansicht"
date: "2012-02-26T12:53:07"
picture: "kugelbahnmitschraegaufzug01.jpg"
weight: "1"
konstrukteure: 
- "RitterRost"
fotografen:
- "RitterRost"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34420
- /detailsaa39-4.html
imported:
- "2019"
_4images_image_id: "34420"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34420 -->
Gesamtansicht