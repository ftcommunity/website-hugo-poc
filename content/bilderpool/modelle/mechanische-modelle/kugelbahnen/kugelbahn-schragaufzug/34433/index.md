---
layout: "image"
title: "Ausgang Schräfaufzug"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34433
- /detailsbe66-2.html
imported:
- "2019"
_4images_image_id: "34433"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34433 -->
Ausgang Schräfaufzug