---
layout: "image"
title: "Geöffnete Umlenkung"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34429
- /details7b66.html
imported:
- "2019"
_4images_image_id: "34429"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34429 -->
Geöffnete Umlenkung