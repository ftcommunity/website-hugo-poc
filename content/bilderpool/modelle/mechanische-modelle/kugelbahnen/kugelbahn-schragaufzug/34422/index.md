---
layout: "image"
title: "Obererteil der Kubelbahn mit 'Aufsichtsperson'"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- /php/details/34422
- /detailse4b9-3.html
imported:
- "2019"
_4images_image_id: "34422"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34422 -->
Obererteil der Kubelbahn mit "Aufsichtsperson"