---
layout: "image"
title: "07 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35398
- /detailsdf2f-2.html
imported:
- "2019"
_4images_image_id: "35398"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35398 -->
Das ist die Sperre, die die Kugel stoppt. Eigentlich überflüssig, da der Aufzug vor der Kugel unten ist. 

Im Video ab ca. 1:12 zu sehen: http://www.youtube.com/watch?v=dtplrOt1AjY