---
layout: "image"
title: "04 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35395
- /detailsaf16-2.html
imported:
- "2019"
_4images_image_id: "35395"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35395 -->
Von oben