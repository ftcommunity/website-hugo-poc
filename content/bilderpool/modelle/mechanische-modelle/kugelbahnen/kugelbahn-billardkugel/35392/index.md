---
layout: "image"
title: "01 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35392
- /details0790.html
imported:
- "2019"
_4images_image_id: "35392"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35392 -->
Diese Kugelbahn habe ich mal gebaut, um zu zeigen, dass man auch ohne die Flexschienen auskommt (wovon ich übrigens absolut kein Fan bin!). 

Auf ihr rollt eine Billardkugel, die immerhin 100g Gewicht hat.

Das hier ist Version 1, ich plane noch eine weitere zu bauen.

Ein Video gibt es hier: http://www.youtube.com/watch?v=dtplrOt1AjY
(jetzt auch in vernünftiger Qualität)