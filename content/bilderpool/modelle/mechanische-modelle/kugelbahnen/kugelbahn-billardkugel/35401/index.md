---
layout: "image"
title: "10 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn10.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35401
- /details568a-2.html
imported:
- "2019"
_4images_image_id: "35401"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35401 -->
Der Turm