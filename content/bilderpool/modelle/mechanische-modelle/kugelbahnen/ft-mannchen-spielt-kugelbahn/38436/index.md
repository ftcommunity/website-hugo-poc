---
layout: "image"
title: "Die Kugelbahn"
date: "2014-03-07T10:45:14"
picture: "ftmaennchenspieltkugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38436
- /details660e.html
imported:
- "2019"
_4images_image_id: "38436"
_4images_cat_id: "2864"
_4images_user_id: "2138"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38436 -->
Die "Klein-Kind-Kugel-Schienen-Abfahrt" war zuerst da und einen Drehaufzug hatte ich mal als Video gesehen.

http://www.youtube.com/watch?v=3fEk4EjfYXA