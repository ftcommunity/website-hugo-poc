---
layout: "image"
title: "Gemeinschaftsmodell - Kugelbahn"
date: "2012-05-29T10:02:15"
picture: "kugelbahn7.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
- "Tobias Horst (tobs9578)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35022
- /detailsbb49-2.html
imported:
- "2019"
_4images_image_id: "35022"
_4images_cat_id: "2591"
_4images_user_id: "1162"
_4images_image_date: "2012-05-29T10:02:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35022 -->
Schild: Vorderseite
