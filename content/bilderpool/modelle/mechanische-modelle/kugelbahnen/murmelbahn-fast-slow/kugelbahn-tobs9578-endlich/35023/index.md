---
layout: "image"
title: "Gemeinschaftsmodell - Kugelbahn"
date: "2012-05-29T10:02:15"
picture: "kugelbahn8.jpg"
weight: "9"
konstrukteure:
- "Marcel Endlich (Endlich)"
- "Tobias Horst (tobs9578)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35023
- /details9fd0.html
imported:
- "2019"
_4images_image_id: "35023"
_4images_cat_id: "2591"
_4images_user_id: "1162"
_4images_image_date: "2012-05-29T10:02:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35023 -->
Schild: Rückseite

Ich hoffe euch gefällt die Kugelbahn. Wir sind froh über jeden Kommentar, egal ob Lob oder Kritik ;-) 

Video http://www.youtube.com/watch?v=_MtKTozVDhk&feature=g-u-u
