---
layout: "image"
title: "Ansicht von oben"
date: "2016-12-10T21:29:01"
picture: "P1040711.jpg"
weight: "12"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn", "Tennisbälle"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44881
- /details9305-2.html
imported:
- "2019"
_4images_image_id: "44881"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44881 -->
Die Ansicht der Bahn von oben.