---
layout: "image"
title: "Aufzugsmechanik"
date: "2012-03-02T15:36:26"
picture: "kugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34508
- /details66d6.html
imported:
- "2019"
_4images_image_id: "34508"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34508 -->
Angebtrieben von einem S-Motor.
