---
layout: "image"
title: "Verriegelung"
date: "2012-03-02T15:36:26"
picture: "kugelbahn3.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34509
- /details6120.html
imported:
- "2019"
_4images_image_id: "34509"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34509 -->
Die Lochstrebe verriegelt die Strecke.
