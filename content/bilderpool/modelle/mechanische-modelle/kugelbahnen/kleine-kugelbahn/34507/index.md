---
layout: "image"
title: "Übersicht"
date: "2012-03-02T15:36:26"
picture: "kugelbahn1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34507
- /detailsafdb-2.html
imported:
- "2019"
_4images_image_id: "34507"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34507 -->
Hier seht ihr meine neuste Kugelbahn
http://youtu.be/MXwxZdUYiw8.
