---
layout: "image"
title: "Kugelabgabe"
date: "2012-03-02T15:36:26"
picture: "kugelbahn6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34512
- /details4684.html
imported:
- "2019"
_4images_image_id: "34512"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34512 -->
Der Aufzugsarm ist oben angekommen, die Strebe wird die Kugel gleich aus dem Fach drücken.
