---
layout: "image"
title: "Kugel im Auswurf von unten - etwas weiter"
date: "2015-07-29T11:02:34"
picture: "kbmsa09.jpg"
weight: "9"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41656
- /details12fb-2.html
imported:
- "2019"
_4images_image_id: "41656"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41656 -->
Etwas weiter gefördert endet die hintere Bausteinreihe und die Kugel verlässt den Förderturm