---
layout: "image"
title: "Bahn als ganzes"
date: "2015-07-29T11:02:34"
picture: "kbmsa02.jpg"
weight: "2"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41649
- /details2574.html
imported:
- "2019"
_4images_image_id: "41649"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41649 -->
Die ganze Bahn aus einer anderen Ansicht