---
layout: "image"
title: "Kugel vor dem Auswurf"
date: "2015-07-29T11:02:34"
picture: "kbmsa06.jpg"
weight: "6"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41653
- /detailsd049-2.html
imported:
- "2019"
_4images_image_id: "41653"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41653 -->
Oben angekommen, benhindert ein Winkelstein 10x15x15 den Weg nach oben.