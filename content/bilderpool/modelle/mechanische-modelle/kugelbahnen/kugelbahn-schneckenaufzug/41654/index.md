---
layout: "image"
title: "Kugel im Auswurf von unten"
date: "2015-07-29T11:02:34"
picture: "kbmsa07.jpg"
weight: "7"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41654
- /details4962-2.html
imported:
- "2019"
_4images_image_id: "41654"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41654 -->
Durch die Rotation der Schnecke wird die Kugel aus der Nut raus nach hinten gedrückt, nach oben ist der Weg ja versperrt