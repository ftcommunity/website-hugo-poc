---
layout: "image"
title: "Kugel vor Aufnahme"
date: "2015-07-29T11:02:34"
picture: "kbmsa03.jpg"
weight: "3"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41650
- /details28aa.html
imported:
- "2019"
_4images_image_id: "41650"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41650 -->
Je nach Schneckenstellung lande die Kugel vor dieser oder läuft gleich hinein.
Hier steht die Schecke so, dass es nicht passt.