---
layout: "overview"
title: "Kugelbahn mit Schneckenaufzug"
date: 2020-02-22T08:16:44+01:00
legacy_id:
- /php/categories/3104
- /categories0cea.html
- /categories2f2e-2.html
- /categoriesa549.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3104 --> 
Eine kleine Kugelbahn als Machbarkeitsstudie für einen Aufzug aus einer ft-Schnecke