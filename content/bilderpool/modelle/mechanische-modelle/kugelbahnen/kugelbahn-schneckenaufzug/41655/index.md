---
layout: "image"
title: "Kugel im Auswurf von oben"
date: "2015-07-29T11:02:34"
picture: "kbmsa08.jpg"
weight: "8"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/41655
- /details9b37.html
imported:
- "2019"
_4images_image_id: "41655"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41655 -->
Der gleiche Stand wie vorher, nur von oben.