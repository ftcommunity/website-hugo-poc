---
layout: "image"
title: "11 both switches"
date: "2014-04-16T15:10:50"
picture: "11.jpg"
weight: "10"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/38560
- /detailsa3ee.html
imported:
- "2019"
_4images_image_id: "38560"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38560 -->
Close-up of the connection between switches A and B, again from the side.