---
layout: "image"
title: "Fuß (3)"
date: 2022-01-11T11:50:58+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die S-Riegel, die die Winkelsteine halten.