---
layout: "image"
title: "Stapelbarkeit"
date: 2022-01-11T11:51:04+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man könnte beliebig viele solcher Module übereinander stapeln.