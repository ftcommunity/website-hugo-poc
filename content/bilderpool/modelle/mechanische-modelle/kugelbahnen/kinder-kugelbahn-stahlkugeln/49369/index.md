---
layout: "image"
title: "Übergänge (2)"
date: 2022-01-11T11:51:02+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier eine Sicht von oben auf einen Übergang.