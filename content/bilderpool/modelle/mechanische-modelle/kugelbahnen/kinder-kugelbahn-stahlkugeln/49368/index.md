---
layout: "image"
title: "Fuß (1)"
date: 2022-01-11T11:51:01+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier landet die Kugel sicher und zuverlässig gefangen.