---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-11T11:51:05+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine kleine, beliebig hoch baubare Kugelbahn für die kleinen Stahlkugeln. Oben legt man eine Kugel ein, unten wird sie aufgefangen.