---
layout: "image"
title: "Fuß (3)"
date: 2022-01-11T11:50:59+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind 2 Winkelsteine 60° mit S-Riegeln am Fuß-U-Träger befestigt. Sie tragen zwei Winkelsteine 60° mit drei Nuten.