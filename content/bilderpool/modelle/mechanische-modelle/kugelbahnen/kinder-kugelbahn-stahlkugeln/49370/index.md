---
layout: "image"
title: "Übergänge (1)"
date: 2022-01-11T11:51:03+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Stahlkugeln3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenige Teile bilden den Übergang zwischen zwei Flexschienen.