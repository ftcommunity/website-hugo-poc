---
layout: "image"
title: "Steuercomputer Arduino Uno R3"
date: "2016-12-10T21:29:01"
picture: "P1040712.jpg"
weight: "13"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Arduino", "Kugelbahn", "Steuerung", "Interface"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44882
- /details5527.html
imported:
- "2019"
_4images_image_id: "44882"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44882 -->
Die Anlage wird durch einen Arduino Uno R3 Computer gesteuert.

Auf dem Arduino Computer ist zunächst das Interface Modul Grove Base Shield von SeeedStudio aufgesetzt. Daran an schließen sich zwei einfach Klemmleisten und zum Schluss das Motor-Shield von Adafruit.

Fünf der insgesamt 8 digitalen Inputschalter werden über die analog Anschlussklemmen des Grove Base I/O Shield's angeschlossen.
Ein Digitalanschluss wird an den Analogeingang A05 des Adafruit Motorshields angeklemmt.
Die verbleibenden zwei digitalen Inputschalter (D2/D13) werden über Klemmleiste dem Computer zugeführt.

Die 4 DC-Motoren sind direkt an den entsprechenden Adafruit Motor-Shield Anschlussklemmen angeschlossen und werden über die externe Stromversorgung des Arduinos mit einer Gleichspannung von 12V versorgt.

Die gelben Lochrasterplatine ist ein selbstgebautes Interface.
Hier werden zunächst alle 8 digitalen Schaltsignale auf Bananensteckerbuchsen entgegengenommen, bevor sie an die Schnittstellen des Arduinos weitergeleitet werden.
Für jeden Kanal signalisiert die dazu gehörige rote Leuchtdiode den Zustand des Schalters.