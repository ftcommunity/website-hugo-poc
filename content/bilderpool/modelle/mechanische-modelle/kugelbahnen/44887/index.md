---
layout: "image"
title: "Beschriftung der Eingabeschalter"
date: "2016-12-10T21:29:02"
picture: "P1040717.jpg"
weight: "18"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44887
- /details8f1e.html
imported:
- "2019"
_4images_image_id: "44887"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44887 -->
Schieber auf Position LINKS. Schalter über Analogeingang A1 mit Arduino verbunden.
In der Software ist dieses Signal der Variablen E4_SCH zugeordnet.