---
layout: "image"
title: "Kugelbahn 1"
date: "2005-11-16T14:14:13"
picture: "Kugelbahn_1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/5343
- /detailsf95d.html
imported:
- "2019"
_4images_image_id: "5343"
_4images_cat_id: "459"
_4images_user_id: "381"
_4images_image_date: "2005-11-16T14:14:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5343 -->
