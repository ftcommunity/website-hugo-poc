---
layout: "image"
title: "Das Haputprogramm"
date: "2016-12-11T10:30:59"
picture: "PRG_HauptProgramm01.png"
weight: "21"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
schlagworte: ["Kugelbahn", "Arduino", "Software"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- /php/details/44890
- /details2060.html
imported:
- "2019"
_4images_image_id: "44890"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-11T10:30:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44890 -->
Das ganze Programm teilt sich auf zwei Dateien auf. Diese Datei zeigt das Hauptprogramm.