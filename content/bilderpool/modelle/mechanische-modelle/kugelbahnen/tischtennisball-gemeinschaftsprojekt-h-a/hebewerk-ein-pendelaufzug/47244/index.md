---
layout: "image"
title: "Hebewerk - Antriebstechnik"
date: "2018-02-04T20:58:46"
picture: "hebewerk3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
schlagworte: ["Gemeinschaftsprojekt", "Tischtennisballmaschine", "Hebewerk", "Pendelaufzug"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47244
- /details392c-3.html
imported:
- "2019"
_4images_image_id: "47244"
_4images_cat_id: "3496"
_4images_user_id: "1557"
_4images_image_date: "2018-02-04T20:58:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47244 -->
Der Seilzug wird von der Trommel wechselseitig umgespult. Beide Trommeln (die hintere ist hier vom Getriebe verdeckt) drehen gleichsinning und die Wickelrichtung des Seiles entscheidet ob der Trog sich hebt oder senkt. Der S-Motor treibt die Hubbewegung an (ein XS-Motor hätte es auch getan, war aber gerade keiner mehr verfügbar). Der XS-Motor treibt die Steuerung (also die Schaltwelle) an. Danke an Andreas Tacke (TST) für seinen wunderbaren kompakten Stromverteiler.

Wegen des symmetrischen Aufbaus sind die Kräfte auf das Wellenlager der Seiltrommeln sehr gering; die Seilkräfte kompensieren sich gegenseitig. Aber das hatten wir ja schon damals beim Kettwiesl.