---
layout: "overview"
title: "Hebewerk - ein Pendelaufzug"
date: 2020-02-22T08:16:55+01:00
legacy_id:
- /php/categories/3496
- /categories3904.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3496 --> 
Inspiriert von den grossen Schiffshebewerken (daher der Name) hebt dieser Aufzug wechselseitig bis zu 3 Tischtennisbälle hoch. Die Steuerung erfolgt dabei elektromechanisch und die restliche Mechanik ist recht minimalistisch gehalten.