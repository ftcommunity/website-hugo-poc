---
layout: "image"
title: "Kugelbahn Version 1 Ansicht Verladung #2"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv14.jpg"
weight: "14"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40872
- /details806e.html
imported:
- "2019"
_4images_image_id: "40872"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40872 -->
Hier dieSeitenansicht der Verladung auf der rechten Seite. Aus der Nähe. DIe Elektronik beschreibe ich im Bild #18.