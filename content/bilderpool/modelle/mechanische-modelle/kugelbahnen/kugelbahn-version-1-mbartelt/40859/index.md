---
layout: "image"
title: "Kugelbahn Version 1 Gesamtansicht #1"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv01.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40859
- /details3c22.html
imported:
- "2019"
_4images_image_id: "40859"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40859 -->
Die Kugelbahn ist auf einer Spanplatte 100 x 50 cm montiert.