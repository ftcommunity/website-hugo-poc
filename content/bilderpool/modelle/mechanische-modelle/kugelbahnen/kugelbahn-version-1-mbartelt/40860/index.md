---
layout: "image"
title: "Kugelbahn Version 1 Gesamtansicht #2"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv02.jpg"
weight: "2"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40860
- /details59a2.html
imported:
- "2019"
_4images_image_id: "40860"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40860 -->
Ich hatte angefangen zu bauen: Verladung rechts und dann grob geschätzt. Leider stehen nun einige Teile etwas über.