---
layout: "image"
title: "Kugelbahn Version 1 Verladung der Kugel rechte Seite #1"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv16.jpg"
weight: "16"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40874
- /detailsfbc4-2.html
imported:
- "2019"
_4images_image_id: "40874"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40874 -->
Eine Drehscheibe mit drei Magneten dient zur Weitergabe. Der linke Taster stoppt den Motor nach genau drei Kugeln und der rechte Taster löst die Verladerampe aus.