---
layout: "image"
title: "Kugelbahn Version 1 Elektronik linke Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv11.jpg"
weight: "11"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40869
- /details40ec.html
imported:
- "2019"
_4images_image_id: "40869"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40869 -->
Die verbaute Elektronik auf der linken Seite. Hier wird durch ein Reekkontakt die Einfahrt des Zuges registriert. Dann wird die Entladung eingeleitet. Nachdem dies erfolgt ist, setzt sich der Aufzug in Bewegung und der Zug fährt zur Beladung zurück. Der Aufzug wird nach rund 45 Sekunden wieder ausgeschaltet.