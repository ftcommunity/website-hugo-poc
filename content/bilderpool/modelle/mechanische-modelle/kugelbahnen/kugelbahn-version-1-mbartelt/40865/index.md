---
layout: "image"
title: "Kugelbahn Version 1 Entladevorrichtung BSB-Lorenwagen"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv07.jpg"
weight: "7"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40865
- /detailsbccd-2.html
imported:
- "2019"
_4images_image_id: "40865"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40865 -->
Die Mechanik der Entladung der Lorenwagen habe ich beim Schoonhooven Video abgeschaut.