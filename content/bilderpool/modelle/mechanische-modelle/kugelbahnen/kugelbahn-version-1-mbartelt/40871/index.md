---
layout: "image"
title: "Kugelbahn Version 1 Ansicht Verladung #1"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv13.jpg"
weight: "13"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40871
- /details7f52-2.html
imported:
- "2019"
_4images_image_id: "40871"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40871 -->
Hier dieSeitenansicht der Verladung auf der rechten Seite.