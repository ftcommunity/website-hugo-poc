---
layout: "image"
title: "Kugelbahn Version 1 Elektronik mitte"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv12.jpg"
weight: "12"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/40870
- /detailsa1ae.html
imported:
- "2019"
_4images_image_id: "40870"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40870 -->
Die Elektronik mit den Silberlingen dient zum Einschalten des mittleren Aufzuges und zum ansteuern des Zählwerks. Der Monoflop schaltet den Aufzug auch wieder aus.