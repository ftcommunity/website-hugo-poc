---
layout: "image"
title: "Stufe 8"
date: "2012-06-07T13:08:18"
picture: "kippaufzug10.jpg"
weight: "10"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35049
- /details2e4b-2.html
imported:
- "2019"
_4images_image_id: "35049"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35049 -->
Der Verbinder drückt die Kugel nach rechts aus dem Aufzug, in die grüne Schiene.
