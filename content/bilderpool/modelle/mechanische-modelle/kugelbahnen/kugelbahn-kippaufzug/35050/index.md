---
layout: "image"
title: "kippaufzug11.jpg"
date: "2012-06-07T13:08:29"
picture: "kippaufzug11.jpg"
weight: "11"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35050
- /details15f0-3.html
imported:
- "2019"
_4images_image_id: "35050"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35050 -->
