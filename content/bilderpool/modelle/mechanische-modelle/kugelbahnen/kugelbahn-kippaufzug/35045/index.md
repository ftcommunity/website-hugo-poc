---
layout: "image"
title: "Stufe 4"
date: "2012-06-07T13:08:18"
picture: "kippaufzug06.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35045
- /details3192.html
imported:
- "2019"
_4images_image_id: "35045"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35045 -->
