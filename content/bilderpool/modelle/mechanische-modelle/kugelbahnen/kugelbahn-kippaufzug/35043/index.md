---
layout: "image"
title: "Stufe 3"
date: "2012-06-07T13:08:18"
picture: "kippaufzug04.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35043
- /details509b.html
imported:
- "2019"
_4images_image_id: "35043"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35043 -->
Die Kugel kann über die herunter gedrückte Lochstrebe in den Aufzug laufen.
