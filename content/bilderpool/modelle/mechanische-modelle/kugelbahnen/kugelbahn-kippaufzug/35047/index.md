---
layout: "image"
title: "Stufe 6"
date: "2012-06-07T13:08:18"
picture: "kippaufzug08.jpg"
weight: "8"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35047
- /details4957.html
imported:
- "2019"
_4images_image_id: "35047"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35047 -->
Über die Flexschiene läuft die Kugel auf die andere Seite und wird durch die Kante am zurück laufen gehindert, damit der Aufzug auf der rechten Seite wieder nach oben kippen kann.
