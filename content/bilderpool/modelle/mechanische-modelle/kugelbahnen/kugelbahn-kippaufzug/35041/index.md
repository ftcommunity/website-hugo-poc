---
layout: "image"
title: "Stufe 1"
date: "2012-06-07T13:08:18"
picture: "kippaufzug02.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35041
- /details7417.html
imported:
- "2019"
_4images_image_id: "35041"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35041 -->
Die Lochstrebe hält die Kugeln zurück, bis der Aufzug links nach unten kippt.
