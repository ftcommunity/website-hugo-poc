---
layout: "image"
title: "Detailblick (1)"
date: 2022-01-12T11:01:48+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Tischtennisbaelle2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Winkelsteine sind WS 7,5°. Die Streben sind I-75 gelocht und X-84,4.