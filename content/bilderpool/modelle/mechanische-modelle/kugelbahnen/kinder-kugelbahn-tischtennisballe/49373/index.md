---
layout: "image"
title: "Detailblick (2)"
date: 2022-01-12T11:01:47+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Tischtennisbaelle3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das sollte leicht nachbaubar sein. Kinder hatten viel Spaß daran.