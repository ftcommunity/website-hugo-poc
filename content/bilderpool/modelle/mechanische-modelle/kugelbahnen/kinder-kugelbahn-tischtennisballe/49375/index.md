---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-12T11:01:49+01:00
picture: "2021-08-09_Kinder-Kugelbahn_fuer_Tischtennisbaelle1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenige Bauteile und ein bisschen Statik. Fertig.