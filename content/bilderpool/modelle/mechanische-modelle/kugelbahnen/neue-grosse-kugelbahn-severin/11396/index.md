---
layout: "image"
title: "Kugelbahn"
date: "2007-08-20T16:42:05"
picture: "neuegrossekugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/11396
- /details8704.html
imported:
- "2019"
_4images_image_id: "11396"
_4images_cat_id: "1021"
_4images_user_id: "558"
_4images_image_date: "2007-08-20T16:42:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11396 -->
