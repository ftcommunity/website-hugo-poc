---
layout: "image"
title: "Kugelbahn"
date: "2007-08-20T16:42:05"
picture: "neuegrossekugelbahn4.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/11398
- /detailsbff0-2.html
imported:
- "2019"
_4images_image_id: "11398"
_4images_cat_id: "1021"
_4images_user_id: "558"
_4images_image_date: "2007-08-20T16:42:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11398 -->
