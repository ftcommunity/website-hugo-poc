---
layout: "image"
title: "Kartenmischer"
date: "2007-06-04T17:06:47"
picture: "v2.jpg"
weight: "8"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10713
- /details781d.html
imported:
- "2019"
_4images_image_id: "10713"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-04T17:06:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10713 -->
Hier die größte veränderung:das "auffangbecken" wurde tiefer und breiter gemacht