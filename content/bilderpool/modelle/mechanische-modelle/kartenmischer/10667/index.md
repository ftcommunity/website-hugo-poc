---
layout: "image"
title: "Kartenmischer"
date: "2007-06-03T11:06:52"
picture: "kartenmischer4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10667
- /detailsad1e.html
imported:
- "2019"
_4images_image_id: "10667"
_4images_cat_id: "968"
_4images_user_id: "557"
_4images_image_date: "2007-06-03T11:06:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10667 -->
mit Karten