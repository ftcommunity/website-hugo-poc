---
layout: "image"
title: "Pneumatisches Tier 8"
date: "2010-04-27T12:02:09"
picture: "P1080565_-_Kopie.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27006
- /detailsb252.html
imported:
- "2019"
_4images_image_id: "27006"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-27T12:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27006 -->
Hier sieht man die befestigung der Füße, weil aber ein ganzes Stück in den Seilrollen steckt, können die Füße leider nicht sehr weit aus und ein fahren. Zum laufen reicht es aber.