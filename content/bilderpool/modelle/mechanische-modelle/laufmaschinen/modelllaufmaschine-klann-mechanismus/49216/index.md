---
layout: "image"
title: "Klann-Laufmaschine mit 8 Beinen"
date: 2021-05-31T11:30:43+02:00
picture: "SchraegOben.jpg"
weight: "1"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

„Mechanische Spinne“ mit 8 Beinen nach dem Prinzip des Klann-Koppelgetriebes (https://de.wikipedia.org/wiki/Klann-Mechanismus).