---
layout: "image"
title: "Seitenansicht ohne Motor"
date: 2021-05-31T11:30:36+02:00
picture: "SeiteOhneAntrieb.jpg"
weight: "6"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Um sich der Klann-Geometrie anzunähern, sind die waagerechten Beinsegmente leicht abgewinkelt (7.5°). Die gelben V-Achsen in den Kurbeln sind mit 28 mm etwas kürzer als andere Achsen und ermöglichen einen Abstand zweier Beinpaare von 3,5 Steinen (52,5 mm) ohne dass es klemmt.