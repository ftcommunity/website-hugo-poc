---
layout: "image"
title: "Vorderansicht"
date: 2021-05-31T11:30:41+02:00
picture: "Vorne.jpg"
weight: "2"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Es wird von zwei Encodermotoren (oder Traktormotoren) angetrieben und ist mit der Standardfernbedienung lenkbar. Die Kurbeln der zwei Beinpaare links und rechts sind jeweils um 180° versetzt. Beim Geradeauslauf tendieren die Beinpaare dazu, sich durch die Belastung zu synchronisieren, sodass die äußeren oder inneren Füße jeweils zusammen aufsetzen.