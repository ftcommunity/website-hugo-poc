---
layout: "image"
title: "Sicht von Oben"
date: 2021-05-31T11:30:39+02:00
picture: "Oben.jpg"
weight: "4"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Als Kurbeln dienen zwei Z40 pro Beinpaar. Damit die Laufmaschine lenkbar ist, läuft eines der Z40 in der Mitte auf einer Freilaufnabe und die Antriebsachse ist unterhalb der Kabel unterbrochen. Die zwei Achsen der oberen Aufhängung tragen wesentlich zur Stabilität bei. 