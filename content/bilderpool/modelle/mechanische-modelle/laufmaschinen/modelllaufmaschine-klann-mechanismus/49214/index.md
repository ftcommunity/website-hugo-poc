---
layout: "image"
title: "Seitenansicht"
date: 2021-05-31T11:30:40+02:00
picture: "Seite.jpg"
weight: "3"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine aus Statikteilen sind jeweils an zwei fixen Achsen aufgehangen und benötigen weniger bewegliche Verbindungen als das Strandbeest.