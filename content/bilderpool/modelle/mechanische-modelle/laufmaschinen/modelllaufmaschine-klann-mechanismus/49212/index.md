---
layout: "image"
title: "Beingeometrie"
date: 2021-05-31T11:30:37+02:00
picture: "Geometrie.jpg"
weight: "5"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bewegungskurve des Fußes wurde mit Hilfe von geogebra (www.geogebra.org) simuliert. Die Maße wurden am Modell abgemessen. Ein Bein besitzt zwei starre abgewinkelte Segmente (waagerecht und senkrecht durch gestrichelte Linien verbunden) und ist über Kurbeln an drei Fixpunkten aufgehängt (A, B und C). Das Ergebnis zeigt eine unten relativ flache Bewegungskurve mit großer Amplitude.