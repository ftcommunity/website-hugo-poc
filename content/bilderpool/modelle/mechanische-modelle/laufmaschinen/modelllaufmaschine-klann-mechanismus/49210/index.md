---
layout: "image"
title: "Aufbau seitliches Lager mit Motor"
date: 2021-05-31T11:30:35+02:00
picture: "Lager.jpg"
weight: "7"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die fünf Lager sind möglichst verwindungssteif aufgebaut. Kugellager können optional verwendet werden.