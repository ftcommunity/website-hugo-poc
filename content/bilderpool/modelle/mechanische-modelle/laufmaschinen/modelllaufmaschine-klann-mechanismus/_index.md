---
layout: "overview"
title: "Laufmaschine mit Klann-Mechanismus"
date: 2021-05-31T11:30:35+02:00
---

Dies ist eine Laufmaschine mit 8 Beinen nach dem Prinzip des Klann-Koppelgetriebes. Der Aufbau des Rahmens und Antrieb ist sehr ähnlich zum [Strandbeest ohne Statikstreben](https://www.ftcommunity.de/bilderpool/modelle/mechanische-modelle/laufmaschinen/strandbeest-ohne-statikstreben). Im Vergleich zum Strandbeest bewegt es sich aber eher "spinnen-artig" mit großer Amplitude der Bewegungskurve und setzt die Füße sehr senkrecht und "energisch" auf. Dadurch läuft es etwas besser auf weichen Untergründen wie Teppich. Es wippt aber stärker und benötigt mehr Kraft zum Antrieb. Ein Video findet ihr bei [youtube](https://youtu.be/nVf-Nk55zdc).
