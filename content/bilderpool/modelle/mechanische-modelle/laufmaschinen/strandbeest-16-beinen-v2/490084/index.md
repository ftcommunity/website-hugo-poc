---
layout: "image"
title: "Aufbau 4 Beine"
date: 2021-03-29T15:49:38+02:00
picture: "4beine.jpg"
weight: "6"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Segment aus 4 Beinen.