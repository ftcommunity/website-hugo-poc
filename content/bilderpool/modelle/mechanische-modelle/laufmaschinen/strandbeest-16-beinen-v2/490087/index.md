---
layout: "image"
title: "Oben ohne Antriebseinheit"
date: 2021-03-29T15:49:41+02:00
picture: "obenohneantrieb.jpg"
weight: "3"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Aufbau ohne Antriebseinheit. Für guten Geradeauslauf wird jedes Z30 um 60° weitergedreht.