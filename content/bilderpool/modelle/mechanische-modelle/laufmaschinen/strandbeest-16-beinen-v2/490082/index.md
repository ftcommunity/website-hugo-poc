---
layout: "image"
title: "Detail Rahmen ohne Beine"
date: 2021-03-29T15:49:35+02:00
picture: "rahmen.jpg"
weight: "8"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Aufbau des Rahmens mit Achsen 30 mm als Kurbeln.