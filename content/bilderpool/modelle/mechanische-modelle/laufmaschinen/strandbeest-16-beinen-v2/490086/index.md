---
layout: "image"
title: "Antriebseinheit"
date: 2021-03-29T15:49:40+02:00
picture: "antrieb.jpg"
weight: "4"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb erfolgt über 2 synchronisierte S-Motoren. Alternativ kann eine Fernsteuerung eingesetzt werden und die mittlere Achse entfernt werden. Dann ist das Strandbiest auf glattem Boden lenkbar.