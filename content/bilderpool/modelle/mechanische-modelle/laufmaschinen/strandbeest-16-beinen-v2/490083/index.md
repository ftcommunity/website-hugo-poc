---
layout: "image"
title: "Detail Lager"
date: 2021-03-29T15:49:36+02:00
picture: "lager.jpg"
weight: "7"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Eines von 4 Kurbellagern. Optionale Metallachsen in Speziallänge 40 mm  und Kugellager von www.fischerfriendsman.de.