---
layout: "image"
title: "Detail Bein"
date: 2021-03-29T15:49:39+02:00
picture: "bein.jpg"
weight: "5"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine sind aus Statikstreben gebaut. Doppelte Strebenverbindungen tragen zur Stabilität bei. Die Verbindungen sollten geschmiert werden.