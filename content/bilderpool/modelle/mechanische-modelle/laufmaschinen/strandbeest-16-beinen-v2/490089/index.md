---
layout: "image"
title: "Schräg von Vorne"
date: 2021-03-29T15:49:43+02:00
picture: "schraegvorn.jpg"
weight: "1"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist eine Laufmaschine mit 16 Beinen nach dem Prinzip von Theo Jansens Strandbiestern (www.strandbeest.com).