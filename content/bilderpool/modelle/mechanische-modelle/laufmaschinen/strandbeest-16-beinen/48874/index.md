---
layout: "image"
title: "Ansicht von Vorne"
date: 2020-10-21T16:14:01+02:00
picture: "VorneSchraeg.jpg"
weight: "5"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Als Antrieb dienen 3 Minimotoren, die durch Zahnräder synchronisiert sind.