---
layout: "image"
title: "Aufbau eines Beines"
date: 2020-10-21T16:14:05+02:00
picture: "BeinDetail.jpg"
weight: "10"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Bein benötigt unter anderem 3 gerade Streben 60, davon eine mit Löchern um den Fuß zu fixieren, eine X-Strebe 63.6, 1x 42.4, 5x 30 und vor Allem S-Riegel (1x Länge 4, 1x 6 und 3x 8). Der V-Bolzen an der Verbindung der 4 Streben ist eine weitere Schwachstelle und neigt dazu sich aus der Klemmbuchse zu drücken. Bei mir hat es geholfen die V-Achse mit etwas Pappe zu verstopfen.