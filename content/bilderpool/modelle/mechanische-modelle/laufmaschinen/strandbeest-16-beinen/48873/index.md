---
layout: "image"
title: "Ansicht von Oben"
date: 2020-10-21T16:14:00+02:00
picture: "ObenTotal.jpg"
weight: "6"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine werden paarweise durch ein Kurbelpaar aus einer Rastkurbel mit Zahnrad und einer Gelenkkurbel angetrieben. Die fixe Achse ist aus Rastachsen gestückelt. Alternativ könnte man eine Metallachse zur besseren Stabilität verwenden. Nachteil ist das höhere Gewicht.