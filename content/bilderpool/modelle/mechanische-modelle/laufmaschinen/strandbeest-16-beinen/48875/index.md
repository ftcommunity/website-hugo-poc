---
layout: "image"
title: "Seitenansicht"
date: 2020-10-21T16:14:02+02:00
picture: "GeradeSeite.jpg"
weight: "4"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Im Gegensatz zu Theo Jansens Prinzip verwende ich nicht eine gemeinsame Kurbelwelle für alle Beine sondern ein Kurbelpaar mit Schneckenantrieb für je 2 Beine. Dadurch sieht es etwas breit aus, aber dafür ist genügend Platz für das Getriebe und die Motoren und die Konstruktion ist sehr symmetrisch.