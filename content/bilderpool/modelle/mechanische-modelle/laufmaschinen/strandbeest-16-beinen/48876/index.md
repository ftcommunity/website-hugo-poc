---
layout: "image"
title: "Detail der Beine"
date: 2020-10-21T16:14:03+02:00
picture: "SchraegVonUnten.jpg"
weight: "3"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Auf ebener Fläche läuft das Strandbeest langsam aber einigermaßen gleichmäßig. Großes Gewicht kann es natürlich nicht tragen. Daher wahrscheinlich auch keinen Original-Akku.