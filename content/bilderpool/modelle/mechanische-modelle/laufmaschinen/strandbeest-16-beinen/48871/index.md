---
layout: "image"
title: "Detailansicht des Getriebes"
date: 2020-10-21T16:13:58+02:00
picture: "GetriebeDetail.jpg"
weight: "8"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Entscheidend für einen gleichmäßigen Gang ist die Stellung der Kurbeln. Bei der Version mit 16 Beinen hat es am Besten mit jeweils um 90° versetzte Kurbeln geklappt.