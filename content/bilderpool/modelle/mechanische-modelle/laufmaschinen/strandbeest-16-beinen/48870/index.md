---
layout: "image"
title: "Detailansicht eines Beinpaares"
date: 2020-10-21T16:13:57+02:00
picture: "KurbelDetail.jpg"
weight: "9"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die ganzen Klemmbuchsen und Distanzringe dienen dazu die Streben so zu fixieren, dass sie gerade bezüglich der Laufrichtung stehen, damit das Bein auch unter Belastung möglichst senkrecht steht und nicht seitlich weg knickt. Die Befestigung der Gelenkwürfel-Klaue, durch die die Kurbelachse führt, ist eine Schwachstelle und verbiegt sich leicht.  Die Bauplatte 1x1 hilft etwas dabei sie einzuklemmen, sodass sie sich nicht auf dem Winkelstein 60 verdreht.