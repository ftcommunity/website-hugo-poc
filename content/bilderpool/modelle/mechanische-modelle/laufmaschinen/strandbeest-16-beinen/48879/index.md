---
layout: "image"
title: "Schräg von oben"
date: 2020-10-21T16:14:07+02:00
picture: "SchraegVonOben.jpg"
weight: "1"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist eine Laufmaschine mit 16 Beinen nach dem Prinzip von Theo Jansens Strandbiestern.