---
layout: "image"
title: "Ansicht von Unten"
date: 2020-10-21T16:13:59+02:00
picture: "UntenTotal.jpg"
weight: "7"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Zusammen mit dem stabilen Rahmen aus Klassiksteinen sorgen die Achsen für Stabilität und verhindern, dass sich die große Konstruktion verbiegt. Als Stromversorgung dient ein leichter Fremdakku.