---
layout: "image"
title: "Ansicht der Beine"
date: 2020-10-21T16:14:04+02:00
picture: "SchraegHinten.jpg"
weight: "2"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine sind aus Statikstreben gebaut.