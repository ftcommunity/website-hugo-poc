---
layout: "image"
title: "Seitenansicht"
date: 2021-04-19T20:08:58+02:00
picture: "seite.jpg"
weight: "3"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Es wird von zwei Encodermotoren (oder Traktormotoren) angetrieben und ist mit der Standardfernbedienung lenkbar. Es läuft und lenkt auf glatter Fläche hervorragend.