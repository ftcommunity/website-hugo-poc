---
layout: "image"
title: "Sicht von Oben"
date: 2021-04-19T20:08:57+02:00
picture: "oben.jpg"
weight: "4"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Je vier Beine werden von einer Kurbelwelle angetrieben (in der Mitte unterbrochen).