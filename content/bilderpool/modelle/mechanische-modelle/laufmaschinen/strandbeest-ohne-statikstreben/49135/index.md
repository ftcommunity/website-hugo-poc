---
layout: "image"
title: "Beingeometrie"
date: 2021-04-19T20:08:55+02:00
picture: "geometrie.jpg"
weight: "6"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bewegungskurve des Fuß wurde mit Hilfe von geogebra (www.geogebra.org) simuliert. Das Ergebnis zeigt, dass sich trotz des zusätzlichen Gelenks für einen Winkelbereich der Kurbel von >180° eine optimale, unten möglichst flache Bewegungskurve erreichen lässt.