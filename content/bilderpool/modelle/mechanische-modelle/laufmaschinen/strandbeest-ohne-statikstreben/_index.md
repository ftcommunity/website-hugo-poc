---
layout: "overview"
title: "Strandbeest ohne Statikstreben"
date: 2021-04-19T20:08:52+02:00
---

ich möchte euch die dritte Version meiner Fischertechnik Strandbiester präsentieren. Dieses wurde mit dem Ziel gebaut, auf Statikstreben zu verzichten, um eine stabilere Konstruktion zu erreichen. Während die vorherigen Versionen möglichst leicht konstruiert wurden, soll dieses Strandbeest besonders stabil sein. (Vorbild ist auch das Strandbeest Rhinoceros von Theo Jansen.
Es verwendet soweit wie möglich stabile Gelenkbausteine, Metallachsen und zwei Traktor- (oder Encoder) Motoren für je vier Beine und lässt sich dadurch noch besser lenken.

Der Kurbelradius wurde durch die Verwendung von Zahnrädern Z40 noch mal vergrößert. Der Aufbau der Beine aus Gelenk- und Winkelsteinen erlaubte sogar eine noch etwas bessere Annäherung an die optimale Geometrie (Vergleich mit Abb. 2 S. 34, ft-pedia 1-2021). In einem Detail weiche ich vom dem Original-Design ab, indem ich ein sechstes Gelenk verwende, um die schwer zur realisierende bewegliche Verbindung von drei Beinsegmenten im Punkt C zu vermeiden.
Weil die Kurbellänge CK sich im Bereich der Bodenberührung relativ wenig ändert, wirkt sich das zusätzliche Gelenk nicht nachteilig aus.

Besprochen wird diese Version im Forum unter https://forum.ftcommunity.de/viewtopic.php?f=6&t=6769.

Ein Video findet sich unter https://www.youtube.com/watch?v=9oy6ta2p9pY.
