---
layout: "image"
title: "Vorderansicht"
date: 2021-04-19T20:08:59+02:00
picture: "vorne.jpg"
weight: "2"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine sind stabil aus Winkel und Gelenkbausteinen gebaut. Je zwei Zahnräder Z40 mit Hülsen und Achse 30 bilden eine Kurbel zum Antrieb eines Beinpaares.