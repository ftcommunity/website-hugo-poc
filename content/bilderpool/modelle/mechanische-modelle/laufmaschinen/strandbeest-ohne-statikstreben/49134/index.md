---
layout: "image"
title: "Detail Lager"
date: 2021-04-19T20:08:53+02:00
picture: "lager.jpg"
weight: "7"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die fünf Beinlager sind möglichst stabil gebaut, um eine Verwindung der Kurbel zu vermeiden.