---
layout: "image"
title: "Strandbeest mit 8 Beinen"
date: 2021-04-19T20:09:01+02:00
picture: "schraegvorne.jpg"
weight: "1"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist eine Laufmaschine mit 8 Beinen nach dem Prinzip von Theo Jansens Strandbiestern. (www.strandbeest.com)