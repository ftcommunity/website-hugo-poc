---
layout: "image"
title: "Detail Bein"
date: 2021-04-19T20:08:56+02:00
picture: "bein.jpg"
weight: "5"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beine wurden auf der Grundlage der Originalgeometrie von Theo Jansen entwickelt, die auf den Kurbelradius des Zahnrads Z40 skaliert wurde (siehe ft:pedia 1/2021, S. 32ff).  Abweichend wurde ein sechstes Gelenk verwendet, um an der unteren Kurbel drei Teile beweglich zu verbinden.