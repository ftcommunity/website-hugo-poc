---
layout: "image"
title: "Detail Rahmen ohne Beine"
date: 2021-04-19T20:08:52+02:00
picture: "rahmen.jpg"
weight: "8"
konstrukteure: 
- "Klingel"
fotografen:
- "Klingel"
uploadBy: "Website-Team"
license: "unknown"
---

Zur Aufhängung der Beine werden Metallachsen verwendet, die wesentlich zur Stabilität beitragen. Die Kurbeln (aus zwei Z40) sind um jeweils 180° verdreht, sodass links und rechts immer jeweils 2 Füße am Boden sind. Damit gelenkt werden kann, ist die Kurbelwelle in der Mitte (hinter der Fernsteuerung) unterbrochen und eines der Zahnräder Z40 läuft auf einer Freilaufnabe.