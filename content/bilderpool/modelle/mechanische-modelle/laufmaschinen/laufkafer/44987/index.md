---
layout: "image"
title: "Ansicht von schräg unten"
date: 2022-01-26T11:10:05+01:00
picture: "Laufkaefer5.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Laufmaschine", "6-Beiner", "Fernsteuerung"]
uploadBy: "Website-Team"
license: "unknown"
---

Die grauen Streben sind S-Streben 63,6.