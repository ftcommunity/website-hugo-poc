---
layout: "image"
title: "Ansicht von unten"
date: 2022-01-26T11:10:06+01:00
picture: "Laufkaefer4.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Laufmaschine", "6-Beiner", "Fernsteuerung"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Batteriekasten ist abmontiert