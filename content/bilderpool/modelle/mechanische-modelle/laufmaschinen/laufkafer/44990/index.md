---
layout: "image"
title: "Schräg von vorne"
date: 2022-01-26T11:10:08+01:00
picture: "Laufkaefer2.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Laufmaschine", "6-Beiner", "Fernsteuerung"]
uploadBy: "Website-Team"
license: "unknown"
---

Zwei XS-Motoren, jeweils mit U-Getriebe, Rastritzel und Kurbel bilden den Antrieb.