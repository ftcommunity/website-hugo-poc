---
layout: "image"
title: "Frontansicht"
date: 2022-01-26T11:10:09+01:00
picture: "Laufkaefer1.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Laufmaschine", "6-Beiner", "Fernsteuerung"]
uploadBy: "Website-Team"
license: "unknown"
---

Vor einiger Zeit hatte Primoz Cebulj eine kleine, aber feine Laufmaschine vorgestellt (kleines Bild im großen), die müsste doch auch mit zwei Motoren und Fernsteuerung zu bauen sein.