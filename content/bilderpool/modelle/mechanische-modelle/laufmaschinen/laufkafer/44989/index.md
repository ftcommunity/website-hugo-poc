---
layout: "image"
title: "Seitenansicht"
date: 2022-01-26T11:10:07+01:00
picture: "Laufkaefer3.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Laufmaschine", "6-Beiner", "Fernsteuerung"]
uploadBy: "Website-Team"
license: "unknown"
---

Am Empfänger die Raupenfunktion einstellen und der Laufkäfer lässt sich prima geradeaus, rechts herum, linksherum und rückwärts bewegen.