---
layout: "overview"
title: "Huumi 3.2"
date: 2020-02-22T08:19:46+01:00
legacy_id:
- /php/categories/1032
- /categories9865.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1032 --> 
Eine \"neue\" Version des humanoiden Robotters von mir (Ma-gi-er)