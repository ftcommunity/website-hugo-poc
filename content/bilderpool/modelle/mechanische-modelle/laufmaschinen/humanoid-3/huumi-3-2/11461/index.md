---
layout: "image"
title: "Neues 2"
date: "2007-09-13T15:41:08"
picture: "huumi3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/11461
- /detailsf666-2.html
imported:
- "2019"
_4images_image_id: "11461"
_4images_cat_id: "1032"
_4images_user_id: "445"
_4images_image_date: "2007-09-13T15:41:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11461 -->
Die stabilisierung der Beine von der Seite.