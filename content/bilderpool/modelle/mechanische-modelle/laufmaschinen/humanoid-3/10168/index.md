---
layout: "image"
title: "Teststand 1"
date: "2007-04-26T15:36:22"
picture: "humanoid1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10168
- /details1ce8.html
imported:
- "2019"
_4images_image_id: "10168"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10168 -->
Inzwischen schaut der Teststand nach unten und die beiden grauen X-Streben wurden durch eine "Strebe 105" ersetzt.