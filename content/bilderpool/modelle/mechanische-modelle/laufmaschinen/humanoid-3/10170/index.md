---
layout: "image"
title: "Von vorne"
date: "2007-04-26T15:36:22"
picture: "humanoid3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10170
- /detailsa8c9.html
imported:
- "2019"
_4images_image_id: "10170"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10170 -->
Die Schwarzen Platten sind jetzt gelb und etwas kleiner...