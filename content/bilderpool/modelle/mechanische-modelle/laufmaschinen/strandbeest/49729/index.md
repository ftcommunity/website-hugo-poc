---
layout: "image"
title: "Laufmachine 1"
date: 2023-03-29T13:15:28+02:00
picture: "Heinz_4.jpeg"
weight: "17"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

design by Heinz Jansen