---
layout: "image"
title: "Laufmachine 1"
date: 2023-03-29T13:15:32+02:00
picture: "Heinz_1.jpeg"
weight: "14"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

design by Heinz Jansen