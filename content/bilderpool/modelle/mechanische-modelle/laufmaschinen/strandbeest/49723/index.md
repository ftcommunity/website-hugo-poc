---
layout: "image"
title: "Strandbeest 2"
date: 2023-03-29T13:15:20+02:00
picture: "strandbeest_7.jpeg"
weight: "7"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

design by Klingel8