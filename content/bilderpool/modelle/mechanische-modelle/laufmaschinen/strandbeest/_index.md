---
layout: "overview"
title: "Strandbeest"
date: 2023-03-29T13:15:17+02:00
---

Der niederländische Künstler Theo Jansen stellt seit über 20 Jahren sogenannte Strandbeests her. Es gibt mehrere FT-Versionen von „Klingel8“, die auf YouTube sichtbar sind und die Grundlage für meine Sammlung bilden. Heinz Jansen hat auch ein schönes Modell gebaut, bei dem ich nicht widerstehen konnte, es nachzubauen

