---
layout: "image"
title: "Strandbeest 3"
date: 2023-03-29T13:15:35+02:00
picture: "strandbeest_12.jpeg"
weight: "12"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

design by Klingel8