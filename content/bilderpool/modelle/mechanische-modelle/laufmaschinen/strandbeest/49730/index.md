---
layout: "image"
title: "Laufmachine 1"
date: 2023-03-29T13:15:29+02:00
picture: "Heinz_3.jpeg"
weight: "16"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

design by Heinz Jansen