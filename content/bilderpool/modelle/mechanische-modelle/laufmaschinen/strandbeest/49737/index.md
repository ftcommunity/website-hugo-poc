---
layout: "image"
title: "Strandgruppe"
date: 2023-03-29T13:15:39+02:00
picture: "strandbeest_1.jpeg"
weight: "1"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Strandbeest", " walking"]
uploadBy: "Website-Team"
license: "unknown"
---

