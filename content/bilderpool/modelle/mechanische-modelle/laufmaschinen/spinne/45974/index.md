---
layout: "image"
title: "Kinematik"
date: "2017-06-19T19:47:06"
picture: "spinne03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45974
- /details8ef2-2.html
imported:
- "2019"
_4images_image_id: "45974"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45974 -->
Die Mechanik funktioniert ähnlich wie die der originalen großen ft-Spinne: Die untere Rastachse mit Platte wedelt durch den WS7,5° etwas, und über die Fixierung in Längsrichtung durch die Mimik aus zwei Gelenksteinen und einem Kardanverbund wird das Bein beim simplen Drehen des Z20 gezwungen, eine Gehbewegung zu machen.
