---
layout: "image"
title: "Zu schwerer Prototyp mit PowerMotoren (1)"
date: "2017-06-19T19:47:06"
picture: "spinne10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45981
- /details7453.html
imported:
- "2019"
_4images_image_id: "45981"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45981 -->
Diese Variante der Spinne hat viel Kraft und läuft sehr schnell, aber das Gewicht der Motoren und des Akkus ist zu viel für die Beine.
