---
layout: "image"
title: "Steuereinheit (2)"
date: "2017-06-19T19:47:06"
picture: "spinne08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45979
- /details04a1.html
imported:
- "2019"
_4images_image_id: "45979"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45979 -->
Der Joystick ist wieder mit einem Kardangelenkt aufgehängt. Je zwei gegenüberliegende Taster sind einfach als Polwendeeinheit ausgeführt (siehe "Motorsteuerungen (1)" in ft:pedia 2011-1) und für je einen Motor zuständig.
