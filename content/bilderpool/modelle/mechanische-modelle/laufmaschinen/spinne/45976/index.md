---
layout: "image"
title: "Gesamter Antrieb von unten"
date: "2017-06-19T19:47:06"
picture: "spinne05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45976
- /detailsed90.html
imported:
- "2019"
_4images_image_id: "45976"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45976 -->
Hier ein schräger Blick auf die Mechanik. Die vielen Federnocken verhindern ein Herausrutschen der Zapfen aus den Nuten.
