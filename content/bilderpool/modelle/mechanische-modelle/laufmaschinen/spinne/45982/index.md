---
layout: "image"
title: "Zu schwerer Prototyp mit PowerMotoren (2)"
date: "2017-06-19T19:47:06"
picture: "spinne11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45982
- /details3c1b-2.html
imported:
- "2019"
_4images_image_id: "45982"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45982 -->
Auf einem Träger rennt sie super, aber eben nicht auf einem Tisch.
