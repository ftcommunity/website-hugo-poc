---
layout: "image"
title: "Laufmaschine03"
date: "2009-01-31T00:06:29"
picture: "laufmaschinealienimfruehstadium3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17209
- /details8b14-2.html
imported:
- "2019"
_4images_image_id: "17209"
_4images_cat_id: "1543"
_4images_user_id: "729"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17209 -->
Laufmaschine von links hinten