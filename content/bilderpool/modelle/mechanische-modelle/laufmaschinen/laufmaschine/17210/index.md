---
layout: "image"
title: "Laufmaschine04"
date: "2009-01-31T00:06:29"
picture: "laufmaschinealienimfruehstadium4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/17210
- /detailsd6a9.html
imported:
- "2019"
_4images_image_id: "17210"
_4images_cat_id: "1543"
_4images_user_id: "729"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17210 -->
Laufmaschine von rechts hinten