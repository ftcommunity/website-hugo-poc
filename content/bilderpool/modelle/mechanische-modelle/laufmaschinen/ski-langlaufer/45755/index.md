---
layout: "image"
title: "Ansicht von unten"
date: 2023-01-17T19:57:07+01:00
picture: "2022-11-19_Ski-Langlaeufer_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man ganz gut, dass die beiden waagerecht liegenden I-Streben 30 mit Löchern in der Mitte mit 31848 S-Strebenadaptern mit dem Körper drehbar verbunden sind. Und dass ein Klemmring bei den grauen K-Achsen 15 fehlt ;-)