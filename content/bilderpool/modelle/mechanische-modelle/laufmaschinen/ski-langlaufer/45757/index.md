---
layout: "image"
title: "Ansicht von links hinten"
date: 2023-01-17T19:57:09+01:00
picture: "2022-11-19_Ski-Langlaeufer_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die grünen Streben stellen Arme und Beine dar. Ellenbogengelenke gibt es keine - ich hatte nur wenig Zeit vor der Party. Die schwarzen Streben sind die Skistöcke.