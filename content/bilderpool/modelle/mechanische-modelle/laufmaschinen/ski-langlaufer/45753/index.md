---
layout: "image"
title: "Hintere Rollen"
date: 2023-01-17T19:57:04+01:00
picture: "2022-11-19_Ski-Langlaeufer_7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hinten sind einfach freilaufende Rollen angebracht, und zwar nur außen. Innen würden sich bei der Bewegung ins Gehege kommen.