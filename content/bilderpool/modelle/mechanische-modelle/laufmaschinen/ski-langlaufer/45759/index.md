---
layout: "image"
title: "Ansicht von rechts vorne"
date: 2023-01-17T19:57:12+01:00
picture: "2022-11-19_Ski-Langlaeufer_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist ein Ski-Langläufer, der auf einer geeigneten Unterlage (Papier oder Tischdecke) tatsächlich vorwärts kommt.

Ein Video gibt's unter https://youtu.be/pu59ObvhntQ