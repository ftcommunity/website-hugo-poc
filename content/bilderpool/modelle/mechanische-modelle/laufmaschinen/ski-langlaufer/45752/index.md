---
layout: "image"
title: "Vordere Rollen von vorne"
date: 2023-01-17T19:57:03+01:00
picture: "2022-11-19_Ski-Langlaeufer_8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Trick, warum der Skiläufer vorwärts kommt, liegt in den Rollen unter den Skis und den Sperrklinken über den vorderen Rollen. Das sind die BS7,5, die die Seilrollen-Klemmräder nur in eine Richtung - nach vorne - drehen lassen.