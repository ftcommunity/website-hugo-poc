---
layout: "image"
title: "Vordere Rollen von der Seite"
date: 2023-01-17T19:57:02+01:00
picture: "2022-11-19_Ski-Langlaeufer_9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Bewegt sich ein Ski nach vorne, kann dessen Rolle frei drehen und über den Boden rollen. Die Rolle des anderen wird aber durch die Sperrklinken daran gehindert, nach hinten zu rollen. So stößt sich diese Rolle am Untergrund ab und der Läufer bewegt sich nach vorne.