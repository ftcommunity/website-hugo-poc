---
layout: "image"
title: "Ansicht von rechts vorne"
date: 2023-01-17T19:57:08+01:00
picture: "2022-11-19_Ski-Langlaeufer_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Einige der S-Riegel sind S-Riegel 8, damit die Streben genügend Spiel haben. Schließlich bewegen sich die Ski nicht nur nach vorne und hinten, sondern zwangsweise auch etwas nach innen und außen.