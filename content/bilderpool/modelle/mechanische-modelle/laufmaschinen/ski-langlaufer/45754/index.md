---
layout: "image"
title: "Detailblick auf die Ski"
date: 2023-01-17T19:57:06+01:00
picture: "2022-11-19_Ski-Langlaeufer_6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man nochmal die Ski-Mechanik.