---
layout: "image"
title: "Ansicht von rechts hinten"
date: 2023-01-17T19:57:11+01:00
picture: "2022-11-19_Ski-Langlaeufer_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor schiebt einen der Ski per Exzenter vor und zurück. Die beiden Ski sind über waagerecht liegende Streben miteinander und in der Mitte mit dem "Körper" verbunden. Geht ein Ski vor, geht der andere zurück, und umgekehrt.