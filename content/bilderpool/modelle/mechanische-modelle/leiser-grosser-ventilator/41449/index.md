---
layout: "image"
title: "Untere Befestigung des Sicherheitsrings"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41449
- /detailseb3f.html
imported:
- "2019"
_4images_image_id: "41449"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41449 -->
Auf die gezeigte Art passt der Ring um den Ventilator genau zentriert ins Modell. Wer nachrechnet, kommt drauf, dass der Abstand zur oberen Befestigung gleich ist.

Wenn man den Ventilator mit den für die M-Motoren zugelassenen 9 V betreibt, muss man die Kombinationen aus Winkelstein 30° und Ventilatorflügeln sorgfältig auswählen, damit die Flügel nicht abgehen. Aber selbst wenn das mal passieren sollte, knallen sie sofort an den Sicherheitsring und bilden keine Gefahr.
