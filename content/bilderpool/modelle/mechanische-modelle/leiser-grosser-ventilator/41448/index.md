---
layout: "image"
title: "Schwenkantrieb (2)"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41448
- /details1c52.html
imported:
- "2019"
_4images_image_id: "41448"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41448 -->
... die eine Schnecke/Z30-Kombination und ein Exzenter zum Hin- und Herschwenken bildet.
