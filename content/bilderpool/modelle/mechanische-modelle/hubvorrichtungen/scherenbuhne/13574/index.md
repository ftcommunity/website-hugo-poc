---
layout: "image"
title: "Scherenbühne 4"
date: "2008-02-06T17:15:25"
picture: "DSCN0022.jpg"
weight: "4"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13574
- /details295c.html
imported:
- "2019"
_4images_image_id: "13574"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13574 -->
