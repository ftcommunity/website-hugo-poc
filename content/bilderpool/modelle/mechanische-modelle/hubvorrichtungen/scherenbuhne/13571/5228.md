---
layout: "comment"
hidden: true
title: "5228"
date: "2008-02-06T21:43:29"
uploadBy:
- "bodo42"
license: "unknown"
imported:
- "2019"
---
Das Problem ist auch die Lagerung der Gelenke. Die grossen Toleranzen addieren sich dermassen, daß viel Kraft nur in die Verbiegung der Elemente zueinander geht. Mit besseren Lagern und steiferen Streben wär das wohl einfacher. Geht aber mit reinem ft wohl nur schwer. Alu-Streben mit 15er Lochsteinen wären vielleicht noch die beste Lösung.