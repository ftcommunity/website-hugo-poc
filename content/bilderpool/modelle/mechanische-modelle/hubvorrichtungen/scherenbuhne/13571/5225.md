---
layout: "comment"
hidden: true
title: "5225"
date: "2008-02-06T20:21:58"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Hm.

So, wie ich es sehe, muss man nur auf dem ersten Stück eine große Kraft aufwenden. Wenn man das mit einem Seilzug macht, dehnt sich das Seil dabei, und sobald man über den ersten Punkt hinaus ist, entspannt es sich und die Hebebühne schnellt nach oben. 

Da brauchst du also Drachenschnur, Anglerschnur oder sonstein statisches Seil.

Oder Du behilfst Dir mit einer Schubkonstruktion, die die Plattform auf dem ersten Stück von unten hochdrückt. Später übernimmt dann der Seilzug die Kraft. Damit umgehst Du das Totpunkt-Problem. Vielleicht reicht schon eine Feder, die die Plattform auf den letzten Zentimetern nach unten spannt. Dann muss der Seilzug nur loslassen und schon geht es los. Das hätte immerhin den Vorteil, dass Du nicht zwei Antriebe aufeinander abstimmen musst.