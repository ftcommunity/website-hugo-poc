---
layout: "overview"
title: "Scherenbühne"
date: 2020-02-22T08:18:12+01:00
legacy_id:
- /php/categories/1246
- /categories8292.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1246 --> 
Hier ist mein Versuch, eine Scherenbühne, bzw. Arbeitsplatform zu bauen. Der überwindbare Höhenunterschied ist enorm, und die Höhe der Vorrichtung im eingeklappten Zustand ist sehr niedrig. Leider sind die nötigen Kräfte um das Teil hochzufahren extrem hoch, deshalb ist mir die Motoriesierung noch nicht gelungen.