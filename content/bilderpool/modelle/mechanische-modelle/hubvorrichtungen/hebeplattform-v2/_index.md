---
layout: "overview"
title: "Hebeplattform v2"
date: 2020-02-22T08:18:13+01:00
legacy_id:
- /php/categories/1247
- /categories1613.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1247 --> 
Die zweite Version der Hebeplattform, die sich schon wesentlich besser zusammenfaltet. Das wurde möglich durch einen modifizierten Mini-Motor, bei dem ich die Feder an der Seite abgeschnitten habe. Dadurch ist er genau 15mm breit und kann in den Träger versenkt werden.