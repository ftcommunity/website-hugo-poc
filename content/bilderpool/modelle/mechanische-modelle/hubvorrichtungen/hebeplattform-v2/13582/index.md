---
layout: "image"
title: "Hebeplatform 3"
date: "2008-02-07T09:48:06"
picture: "DSCN0032.jpg"
weight: "3"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13582
- /details1fa0-2.html
imported:
- "2019"
_4images_image_id: "13582"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13582 -->
Die Platform im höchsten Zustand. Höhe der roten Platte: 53.5cm. Überwundener Höhenunterschied: 42cm.