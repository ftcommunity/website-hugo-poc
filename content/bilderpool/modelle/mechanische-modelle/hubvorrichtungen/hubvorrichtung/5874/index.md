---
layout: "image"
title: "Hub04.JPG"
date: "2006-03-12T13:47:20"
picture: "Hub04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5874
- /detailsfe1d.html
imported:
- "2019"
_4images_image_id: "5874"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:47:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5874 -->
Das Problem der kurzen Zahnstangen lässt sich umgehen, indem man längere Zahnstangen auf ft-Achsen zusammensetzt.
