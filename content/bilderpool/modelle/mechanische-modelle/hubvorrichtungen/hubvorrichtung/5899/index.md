---
layout: "image"
title: "Flipper01.JPG"
date: "2006-03-16T18:24:51"
picture: "Flipper01.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["35981", "Flipper", "Zahnstange"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5899
- /details070e.html
imported:
- "2019"
_4images_image_id: "5899"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-16T18:24:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5899 -->
Nachdem einmal heraus ist, dass man mit der "Hülse mit Scheibe 35981" auch Zahnstangen machen kann, kann's von da aus weitergehen: das hier könnte man für einen pneumatischen Flipper hernehmen, etwa als Weiche, um den Ball ins Aus zu lenken.
