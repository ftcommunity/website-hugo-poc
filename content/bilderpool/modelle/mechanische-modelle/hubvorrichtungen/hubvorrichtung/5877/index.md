---
layout: "image"
title: "Hub08.JPG"
date: "2006-03-12T13:49:54"
picture: "Hub08.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5877
- /details0db0-2.html
imported:
- "2019"
_4images_image_id: "5877"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5877 -->
