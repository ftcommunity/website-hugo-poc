---
layout: "image"
title: "Hub02.JPG"
date: "2006-03-12T13:40:15"
picture: "Hub02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5872
- /detailsea39-2.html
imported:
- "2019"
_4images_image_id: "5872"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5872 -->
