---
layout: "image"
title: "Hebemechanismus"
date: "2009-02-14T09:50:29"
picture: "hebebuehne03.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17394
- /details4a4f.html
imported:
- "2019"
_4images_image_id: "17394"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17394 -->
