---
layout: "image"
title: "Hebeplatform (Vorlage)"
date: "2008-02-06T20:44:15"
picture: "Vorlage_2.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "bodo42"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13579
- /details5a4d.html
imported:
- "2019"
_4images_image_id: "13579"
_4images_cat_id: "1244"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T20:44:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13579 -->
Hier die Vorlage zur mobilen Hebeplatform aus dem Abenteuer Bau Buch.
