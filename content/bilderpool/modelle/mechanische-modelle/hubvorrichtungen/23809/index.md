---
layout: "image"
title: "hv02.jpg"
date: "2009-04-25T10:28:40"
picture: "Hubvorrichtung02.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23809
- /details96ab.html
imported:
- "2019"
_4images_image_id: "23809"
_4images_cat_id: "1244"
_4images_user_id: "4"
_4images_image_date: "2009-04-25T10:28:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23809 -->
Das Rastkegelrad ist mittlerweile ersetzt worden durch eine Rastachse 30 plus Rastkupplung. Das Kegelrad berührt nämlich die Zahnstange, wenn sie ganz "drinnen" ist.
