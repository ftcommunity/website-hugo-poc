---
layout: "overview"
title: "Hebeplattform"
date: 2020-02-22T08:18:11+01:00
legacy_id:
- /php/categories/1245
- /categories55de.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1245 --> 
Hier ist mein Versuch, eine möglichst mobile Hebeplattform zu bauen. Leider ist sie im zusammengefalteten Zustand noch viel zu groß. Auch die zu hebende Last kann nicht sehr groß sein, weil das untere Schneckengetriebe sonst nicht mehr mitspielt.