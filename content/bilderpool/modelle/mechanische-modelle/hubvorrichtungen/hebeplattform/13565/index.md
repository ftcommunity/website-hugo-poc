---
layout: "image"
title: "Hebeplatform 4"
date: "2008-02-06T17:15:05"
picture: "DSCN0007.jpg"
weight: "4"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/13565
- /detailsdb3d.html
imported:
- "2019"
_4images_image_id: "13565"
_4images_cat_id: "1245"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13565 -->
Die Platform im höchsten Zustand. Abstand der kleinen roten Platte zum Boden: 50cm. Überwundener Höhenunterschied: 43cm.