---
layout: "image"
title: "Draufsicht 4"
date: "2008-07-15T22:17:21"
picture: "zylindermotor10.jpg"
weight: "11"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14884
- /details0fa3.html
imported:
- "2019"
_4images_image_id: "14884"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14884 -->
