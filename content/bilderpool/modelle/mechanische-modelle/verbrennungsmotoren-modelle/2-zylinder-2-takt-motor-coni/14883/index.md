---
layout: "image"
title: "Draufsicht 3"
date: "2008-07-15T22:17:21"
picture: "zylindermotor09.jpg"
weight: "10"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
uploadBy: "Coni"
license: "unknown"
legacy_id:
- /php/details/14883
- /details3b51-4.html
imported:
- "2019"
_4images_image_id: "14883"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14883 -->
Ich habe ein Getriebe an das Schwungrad  angebaut, damit ich mit dem Motor etwas antreiben kann. (Z.B. eine Seilwinde)