---
layout: "image"
title: "Kolbenmotor"
date: "2008-03-19T16:00:26"
picture: "kolbenmotor1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13956
- /detailsc4b8.html
imported:
- "2019"
_4images_image_id: "13956"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13956 -->
Auf diesem Bild sieht man den gesamten Kolbenmotor. Der Kolben ist in der Mitte.