---
layout: "image"
title: "Nockenscheiben"
date: "2008-03-19T16:00:26"
picture: "kolbenmotor4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13959
- /details3955.html
imported:
- "2019"
_4images_image_id: "13959"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13959 -->
Hier sieht man die Nockenscheiben,die die Ventile steuern. Die Nockenscheiben sind ja nicht ganz rund und deshalb drücken sie einmal pro Umdrehung die Ventile auf. Wenn sie dann nicht mehr aufgedrückt werden, federn sie durch die Federungen zurück.