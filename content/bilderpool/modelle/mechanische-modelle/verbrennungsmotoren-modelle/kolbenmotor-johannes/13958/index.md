---
layout: "image"
title: "Übertragung"
date: "2008-03-19T16:00:26"
picture: "kolbenmotor3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/13958
- /detailsf160.html
imported:
- "2019"
_4images_image_id: "13958"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13958 -->
Auf diesem Bild sieht man die Übertragung von der vom Kolben angetrieben Achse und der Achse, die die Nockenscheiben antreibt.