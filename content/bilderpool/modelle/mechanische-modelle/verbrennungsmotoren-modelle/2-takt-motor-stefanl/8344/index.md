---
layout: "image"
title: "2-Takt-Motor 2"
date: "2007-01-08T17:16:10"
picture: "taktmotor2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8344
- /details614a.html
imported:
- "2019"
_4images_image_id: "8344"
_4images_cat_id: "768"
_4images_user_id: "502"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8344 -->
Zur Steuerung: die Magnetventile werden über die Taster gesteuert die wiederum von den hin und her wippenden Zylindern betätigt werden.
