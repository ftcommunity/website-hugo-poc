---
layout: "image"
title: "Hobby 1 Band 1 S. 38"
date: "2015-04-03T15:14:58"
picture: "IMG_0013.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Ostereier-Katapulte"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40713
- /details3f29.html
imported:
- "2019"
_4images_image_id: "40713"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T15:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40713 -->
Ostereier-Katapulte
