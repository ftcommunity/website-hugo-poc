---
layout: "image"
title: "Ostereier-Katapulte"
date: "2015-04-03T15:14:58"
picture: "IMG_0010.jpg"
weight: "6"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Ostereier-Katapulte"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40717
- /detailsc9c0.html
imported:
- "2019"
_4images_image_id: "40717"
_4images_cat_id: "3059"
_4images_user_id: "1359"
_4images_image_date: "2015-04-03T15:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40717 -->
hier nochmal beide Katapulte
