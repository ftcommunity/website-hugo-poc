---
layout: "image"
title: "Drehimpuls-Prüfstand (9/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse09.jpg"
weight: "9"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15603
- /details5ab7.html
imported:
- "2019"
_4images_image_id: "15603"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15603 -->
Mechanischer Drehzahlzähler:
Von den rosa eingefärbten Baugruppen können die beiden Wellen hochgestellt und die Impulsbausteine "Taster" sowie "Impulsrad" zurückgenommen werden.

Die Antriebswelle wird dann .....
