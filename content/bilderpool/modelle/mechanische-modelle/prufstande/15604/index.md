---
layout: "image"
title: "Drehimpuls-Prüfstand (10/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse10.jpg"
weight: "10"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15604
- /details4c4f.html
imported:
- "2019"
_4images_image_id: "15604"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15604 -->
Mechanischer Drehzahlzähler:

... zu einer Geberwelle, die nur noch die geringe mechanische Reibung der beiden geschmierten Lager zu überwinden hat.

Die Lampe 9V/0,1A der Lichtschranke verursacht allerdings zunächst nur über Interface gesteuert einen merkbaren Spannungsabfall.
Die Drehzahlen dagegen mit dem Impulsbaustein "Taster" gemessen liegen höher.
