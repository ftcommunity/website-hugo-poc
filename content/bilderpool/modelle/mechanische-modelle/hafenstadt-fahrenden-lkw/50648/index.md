---
layout: "image"
title: "Seilführung (5)"
date: 2024-11-09T22:25:30+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos72.jpg"
weight: "72"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht hier zwei schräg angeordnete "Leitbleche" aka Platten 15x45. Wenn die Brückenplatten eingebaut sind, drücken die die Seile herunter und verlaufen direkt oberhalb dieser Platten. Die Platten zwingen ankommendes Seil in die richtige Fahrspur für die nächstfolgenden Umlenkrollen und zur Vermeidung von Kollisionen.

Der Magnet oberhalb der Bildmitte sitzt auf beiden Enden des Seils. Das Seil wird nämlich nicht etwa zusammengeknotet, sondern liegt mit beiden Enden einfach gemeinsam in einem der Magnetaufsätze. Das klemmt dann erst recht stark, aber es erlaubt die Justierung der gesamten Seillänge innerhalb gewisser Grenzen. Der Rest ist ein Werk der Schere, mit der überschüssiges Seil schlicht abgeschnitten wurde.