---
layout: "image"
title: "Seilführung (6)"
date: 2024-11-09T22:25:29+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos73.jpg"
weight: "73"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Seilführung der beiden sich kreuzenden Fahrspuren.