---
layout: "image"
title: "Frachtschiff - Antriebsschrauben"
date: 2024-11-09T22:25:07+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Ansicht von unten