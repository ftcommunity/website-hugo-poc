---
layout: "image"
title: "Sternwarte - Mechanik unterhalb der Kuppel"
date: 2024-11-09T22:25:45+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos61.jpg"
weight: "61"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Exzenter hinten treibt den langen grünen Hebel an, der den roten BS15 mit Bohrung hebt und senkt. Darin steckt die senkrechte Achse der Kuppel, die wie beschrieben (oben und unten durch Klemmringe mit dem BS15 verbunden) die Schwenkbewegung des "Fernrohrs" bewirkt.

Über zwei Schnecken sehr viel langsamer wird die ganze Kuppel gedreht. Das geht über das angetriebene schwarze Z20. Die anderen drei Z20 dienen nur der Führung der Z40/Drehscheibe-Kombination der Kuppel. Das Z40 der Kuppel (hier ja nicht im Bild) liegt auf den Grundbausteinen auf.