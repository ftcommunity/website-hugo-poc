---
layout: "image"
title: "Drehbare Aussichtsplattform"
date: 2024-11-09T22:26:48+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Z20 greift bei Anbau der Plattform ins Z10 beim eigentlichen Fahrspurantrieb ein.