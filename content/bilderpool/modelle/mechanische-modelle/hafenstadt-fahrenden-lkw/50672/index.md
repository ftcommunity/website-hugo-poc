---
layout: "image"
title: "Baukran - Sicherheitsschienen"
date: 2024-11-09T22:25:59+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos50.jpg"
weight: "50"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Baukran fällt bei Stößen leicht um, weil sein Schwerpunkt recht weit oben ist. Das war ich leid, und also gabs ergänzend zu den Schienen, auf denen er läuft, obenauf nochmal Statikstreben, die den Kran am Umkippen hindern.