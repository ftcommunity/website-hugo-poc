---
layout: "image"
title: "Haus"
date: 2024-11-09T22:26:20+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein kleines Haus mit Giebeldach und Schornstein (mit den alten Giebel-Teilen gebaut) soll der Arbeitsplatz der Radar-Leute sein.