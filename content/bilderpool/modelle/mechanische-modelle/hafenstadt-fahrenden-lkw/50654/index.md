---
layout: "image"
title: "Seilführung (1)"
date: 2024-11-09T22:25:38+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos67.jpg"
weight: "67"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wir gehen vom Antrieb unterhalb der Aussichtsplattform durch die gesamte Bahn (und zwar Bild für Bild entlang der Fahrrichtung der LKW auf dem Seil bei der Windmühle).

Hier ist eine besonders kritische Stelle: Das im Bild untere Seil kommt von links mit Magneten (und LKW obendrüber) an. Bei der unteren Freilaufnabe muss der Magnet nach außen - also hier nach rechts unten - zeigen, wenige cm weiter muss es aber links vom nächsten Reifen landen, und zur Drehscheibe hin muss es schon wieder seine Lage ändern. Deshalb gibt es zwischen den beiden Freilaufnaben rechts unten im Bild eine schrägstehende Platte 15x15, die den Magneten nach oben drückt. So kann der nächste Reifen den Magneten leicht nach außen drücken (das Seil läuft ja gespannt innen).