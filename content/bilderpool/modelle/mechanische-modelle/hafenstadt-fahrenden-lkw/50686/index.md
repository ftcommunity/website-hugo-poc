---
layout: "image"
title: "Haus von der Seite"
date: 2024-11-09T22:26:17+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos38.jpg"
weight: "38"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die eingefügten Federnocken sind nur als Abstandshalter da (dann ist's im Haus nicht so dunkel 