---
layout: "image"
title: "Seilführung (4)"
date: 2024-11-09T22:25:32+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos71.jpg"
weight: "71"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist die nächste Kurve, rechts davon beginnt die Brücke.