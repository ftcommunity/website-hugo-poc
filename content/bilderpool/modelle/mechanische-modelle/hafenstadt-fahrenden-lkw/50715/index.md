---
layout: "image"
title: "Drehbare Aussichtsplattform"
date: 2024-11-09T22:26:52+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Damit man ihn nicht gleich entdeckt, sitzt über dem Antrieb einer LKW-Fahrspur und von diesem angetrieben eine Drehplattform. Die dreht sich1:2 * 1:3 = 1/6 so schnell wieder Antrieb. Obenauf dreht sich das Werbeschild in entgegengesetzter Richtung 1/2 so schnell wie der Antrieb, also 3 x so schnell wie die Drehterrasse.

Das Innenzahnrad liegt einfach nur auf und wird vom im Bild rechten Z10 angetrieben. Die drei anderen Z10 dieser Ebene werden hingegen vom Innenzahnrad angetrieben und dienen nur dessen Führung.

Die Terrassenmechanik ist nur mit dem einen Baustein 7,5 (mit BS5 obendrauf zur Verstärkung des Klemmeffekts) links oberhalb des XM-Motors befestigt. So lässt sie sich leicht abnehmen, wenn man doch mal etwas am Antrieb machen muss (weil das Antriebsseil absprang oder so).