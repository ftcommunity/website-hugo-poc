---
layout: "image"
title: "Windrad"
date: 2024-11-09T22:25:55+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos54.jpg"
weight: "54"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Neben der Sternwarte war noch Platz für das Windrad, dessen Solarmotor von der Solarzelle angetrieben wird, sofern sie hinreichend beleuchtet wird.

Die Flügelschraube sitzt mit reingestopftem Papier so auf der Solarmotor-Achse, dass sie nicht gleich herunterfällt. Allerdings ist die Unwucht doch so groß, dass die kleine schwarze Aufsteckachse des Solarmotors an zwei Stellen leicht eingerissen ist. Insofern ist dieser Aufbau zum Nachbau NICHT ZU EMPFEHLEN.