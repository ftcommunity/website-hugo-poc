---
layout: "image"
title: "Hafenstadt vom Meer aus"
date: 2024-11-09T22:26:54+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zunächst wird man hoffentlich von den vielen Teil-Modellen abgelenkt, bevor man sieht, dass da ja kleine Autos fahren.

Außer 8 Neodym-Magneten, zwei Haushaltsgummis, etwas farbig bedrucktem Papier und einer von Ralf Knobloch golden lackiertem ft-Männchen ist alles unverändertes Original-ft.