---
layout: "image"
title: "Seilenden"
date: 2024-11-09T22:25:23+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos78.jpg"
weight: "78"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der andere Magnet an den beiden Seilenden seiner Fahrspur.