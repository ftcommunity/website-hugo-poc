---
layout: "image"
title: "Haus"
date: 2024-11-09T22:26:19+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Haus sitzt mit seiner Bodenplatte lose auf diesen Ständern, damit man es zur Wartung der Bahn leicht wegnehmen kann.