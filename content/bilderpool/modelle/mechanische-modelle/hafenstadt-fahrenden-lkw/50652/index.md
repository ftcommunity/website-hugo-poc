---
layout: "image"
title: "Seilführung (3)"
date: 2024-11-09T22:25:35+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos69.jpg"
weight: "69"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ab gehts in die erste Kurve bzw. Ecke. Alles muss so sitzen, dass zwei entgegenkommende Autos sich keinesfalls ins Gehege kommen.