---
layout: "image"
title: "Sternwarte"
date: 2024-11-09T22:25:50+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos58.jpg"
weight: "58"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die rote "Hülse mit Scheibe" in der Mitte der Flexschienen-Kuppel schwenkt periodisch in ihrem "Schlitz" hin und her. Sehr viel langsamer dreht sich gleichzeitig die gesamte Kuppel um ihre Senkrechte. Angetrieben wird all das von dem einzigen S-Motor links unterhalb der Bildmitte.