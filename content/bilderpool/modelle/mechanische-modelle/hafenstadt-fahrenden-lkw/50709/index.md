---
layout: "image"
title: "Antrieb und Wartungswerkzeuge"
date: 2024-11-09T22:26:45+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Polwendeschalter dient zum Ein- und Ausschalten der gesamten Anlage.

Rechts davon stecken zwei Rastachsen mit Rastadapter. Die werden verwendet, wenn man das Seil an schwer zugänglichen Stellen einfädeln muss - die Rastadapter sind dann sozusagen die Hand mit den Fingern.

Untendrunter eine Führungsplatte für E-Magnetventil für ähnliche Zwecke.

Links vor der Fahrbahn und ganz wichtig: Zwei Clipsplatten 15x30 mit Clips Längsrichtung. Die werden gebraucht, wenn das Seil komplett neu gespannt werden muss. Das Seil wird dazu an die waagerechte Drehscheibe gebracht und mit einer dieser Clipsplatten fixiert. So rutscht das nicht mehr ab, bis man das Seil Umlenkrolle für Umlenkrolle neu verlegt und gespannt hat und schließlich wieder bei der Drehscheibe ankommt.