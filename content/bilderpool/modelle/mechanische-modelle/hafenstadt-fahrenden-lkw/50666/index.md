---
layout: "image"
title: "Windrad - Stromversorgung"
date: 2024-11-09T22:25:52+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos56.jpg"
weight: "56"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von der Solarzelle kommt hier der Strom an einem Leuchtstein an.

Die schrägen Streben auf der Fahrbahnplatte im Hintergrund dienen übrigens dazu, die LKW vor Einfahrt unter die Brücke etwas in die Fahrbahnmitte zu zwingen. Auf dieser Seite kommen nämlich gleich die Pfeiler der Brücke und die oberen Rollen für die Seilführung direkt unter der Brücke, an denen die LKW nicht hängenbleiben sollen.