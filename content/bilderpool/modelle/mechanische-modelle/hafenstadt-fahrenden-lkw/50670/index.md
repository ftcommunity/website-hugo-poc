---
layout: "image"
title: "Photovoltaik"
date: 2024-11-09T22:25:57+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos52.jpg"
weight: "52"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da war noch Platz für eine Solaranlage. Die treibt, technisch ja nicht ganz realistisch, das Windrad neben der Sternwarte an.