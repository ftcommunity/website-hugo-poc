---
layout: "image"
title: "Haus von vorne"
date: 2024-11-09T22:26:18+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Winkelsteine unter dem Dach sind alte Winkelsteine 30° rechtwinklig, sowohl die zwei mittig als auch die beiden außen, mit denen das Dach mit der Hauswand verbunden ist.