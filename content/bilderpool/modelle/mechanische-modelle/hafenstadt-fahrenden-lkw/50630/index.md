---
layout: "image"
title: "Besondere Seilführungsplatte"
date: 2024-11-09T22:25:09+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos89.jpg"
weight: "89"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die schon auf vorherigen Bildern vorkommende Führung direkt vor den Antriebsdrehscheiben zur Dokumentation einzeln.