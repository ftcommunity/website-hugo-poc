---
layout: "image"
title: "Leuchtturm - Lampe und Antrieb"
date: 2024-11-09T22:26:35+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In die Lampe (ein älterer 6-V-Leuchtstein, weil die Anlage ja nur 6 V bekommt) führen direkt ohne Stecker die Litzen, eingeklemmt mittels eines 36495 Leuchtstein-Stopfens. Die Stecker wären nämlich den sich drehenden Linsen im Weg.