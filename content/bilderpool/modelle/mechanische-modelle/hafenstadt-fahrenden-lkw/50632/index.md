---
layout: "image"
title: "Antriebsblock (3)"
date: 2024-11-09T22:25:11+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos87.jpg"
weight: "87"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der Antriebsblock, von der Bodenplatte abgenommen, und zwar "von unten" aufgenommen. Er sitzt nur mit den beiden Zapfen rechts oben im Bild und mit dem BS15 ganz links (der jetzt halt nach unten fiel) auf der Bauplatte. Alles andere ist um die senkrecht stehende Metallachse drehbar.