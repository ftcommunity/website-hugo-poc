---
layout: "image"
title: "Fahrspurantrieb unterhalb der Drehplattform"
date: 2024-11-09T22:26:46+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So sieht es aus, wenn die abgenommene Aussichtsplattform den Blick auf den eigentlichen Fahrzeugantrieb einer Fahrspur freigibt.

Der gesamte Antriebsblock mit XM-Motor, Getriebe und der Drehscheibe ist um die rechts unterhalb der Bildmitte sichtbare Metallachse drehbar aufgehängt. Die Feder im Hintergrund drückt den Antrieb weg von der Fahrbahn, damit die Seilspannung in etwa konstant bleibt - dazu später mehr.

Vornedran sieht man Führungsteile, die zum Zuverlässigen Ein- und Ausfahren der am Seil angebrachten Neodym-Magnete notwendig sind. Auch dazu später mehr.