---
layout: "image"
title: "Leuchtturm - Linsenkorb"
date: 2024-11-09T22:26:32+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die BS5 15 x30 und die Platten 15x30 darauf sind nur Zierde, genauso wie die Platten oben und die Verbindungsstücke 30.