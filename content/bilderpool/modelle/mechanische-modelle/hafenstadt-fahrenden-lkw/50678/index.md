---
layout: "image"
title: "Windmühle von unten"
date: 2024-11-09T22:26:07+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos45.jpg"
weight: "45"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick von unten in die Mühle.