---
layout: "image"
title: "Gesamte Seilführung"
date: 2024-11-09T22:25:39+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos66.jpg"
weight: "66"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Blick mit allen Fahrbahnplatten abgenommen. In diesem Zustand ist der Antrieb nicht funktionsfähig. Die Seile würden spätestens bei den Steigungen bei der Brücke sofort von den Reifen abspringen.