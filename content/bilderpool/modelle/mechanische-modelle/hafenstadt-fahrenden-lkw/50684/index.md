---
layout: "image"
title: "Frachtschiff"
date: 2024-11-09T22:26:14+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Hafen liegt dieses Schiff. Vorne eine Fahne und zwei einfach quer aufgelegte Elemente. Die Laderäume sind mit ft-Teilen gefüllt. Die Brücke mit den runden Fenstern besteht im Wesentlichen aus den Schneckenbausteinen für die kleine schwarze Schnecke.