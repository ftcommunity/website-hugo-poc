---
layout: "image"
title: "Seilführung"
date: 2024-11-09T22:25:42+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos63.jpg"
weight: "63"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Seile werden im Wesentlichen von Reifen 30 auf Freilaufnaben um die Ecken geführt. Einige Seilrollen davor und/oder danach helfen, dass das Seil nicht von den Reifen 30 herunterspringt. Das funktioniert nur, wenn alle Straßenbelag-Statikplatten aufgelegt und mit den 31848 Statik-Strebenadaptern fixiert sind.

Die Reifen 30 sind die neuere Variante, die nur zwei feine Erhebungen an den beiden äußeren Rändern hat. Die Ur-Version davon hatte mehrere solche "Rillen". Die beiden Rillen hier helfen auch, das Seil auf dem Reifen zu halten.

Warum Reifen 30 und nicht einfach nur Naben oder Seilrollen? Nun, irgendwann kommen die Magnete bei der Kurve an. Das sind kleine Neodym-Magnete, mit 31597 Abstandsringen 3 mm aufs Seil geklemmt. In einer Nabe oder einer Seilrolle würde das Seil weit innerhalb des Rades - in der umlaufenden Nut nämlich - geführt. Kommt der Magnet, muss das Seil aber "raus", und das verkürzt die fürs Seil verfügbare Gesamtstrecke, bzw. verlängert es den Teil des Seiles, der tatsächlich unterhalb der Fahrbahn verläuft.

Das ginge so schwer - gefederte Antriebsräder hin oder her - dass das Seil hängen bleiben würde. Man kann die Seilspannung auch nicht beliebig klein oder beliebig groß machen, weil sonst das Seil herunterfiel oder zu schwergängig gezogen werden müsste.

Der Unterschied zwischen "nur Seil" und "Seil mit Magnet" ist sehr viel kleiner, wenn das Seil um die Reifen 30 geführt wird, weil es dann immer "außen" und nicht in einer Rille verläuft.