---
layout: "image"
title: "Drehbare Aussichtsplattform"
date: 2024-11-09T22:26:51+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man die Drehscheibe mit dem Fahr-Antriebsseil. Auf dessen Achse sitzt auch das Z10, dass in das Rast-Z20 der Drehplattform eingreift.