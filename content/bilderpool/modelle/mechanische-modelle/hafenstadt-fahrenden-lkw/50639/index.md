---
layout: "image"
title: "Führungsplatten"
date: 2024-11-09T22:25:19+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos80.jpg"
weight: "80"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Noch ein Detailblick auf die verschiedenen Zusatz-Führungen für Seil und Magnet bei den beiden Antrieben. Nahe der Drehscheibe befindet sich eine 32455 "E-Magnet-Führungsplatte 15x20", die hilft, den Magneten richtig auszurichten und das Seil zuverlässig genau in die Rille der Drehscheibe zu leiten.

In die Antriebs-Drehscheiben ist übrigens je ein Haushaltsgummi, zweifach gelegt, eingelegt, um die Reibung mit dem Seil zu erhöhen. Den Trick habe ich von Ralf Geerken gelernt.

Hier, beim Antrieb unter der Aussichtsplattform, gibt es zusätzlich die schrägstehende Platte 15x15, weil die beiden Freilaufräder so nahe beieinander stehen und deshalb ein Seitenwechsel der Magnete gegenüber dem Seil auf kurzer Strecke notwendig wird.