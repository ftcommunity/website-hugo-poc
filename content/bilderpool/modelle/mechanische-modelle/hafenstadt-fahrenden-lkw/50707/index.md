---
layout: "image"
title: "Fahrantrieb - Schneckengetriebe"
date: 2024-11-09T22:26:42+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XM-Motor geht auf eine der alten mot.-1-Schnecken und treibt das Z40 damit schön langsam und kräftig an.

Die gesamte Anlage wird mit nur 6 Volt Versorgungsspannung betrieben, sonst liefen die Antriebe schon zu schnell.

Rechts oben ist unerwünscht mein Daumen im Bild. Das ist kein ft-Originalteil ;-)