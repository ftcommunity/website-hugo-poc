---
layout: "image"
title: "Radarschirm 2"
date: 2024-11-09T22:26:23+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Beim zweiten Schirm sind die Achshalter anders ausgerichtet.