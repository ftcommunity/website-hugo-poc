---
layout: "image"
title: "Brückenteil"
date: 2024-11-09T22:25:41+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos64.jpg"
weight: "64"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Unterseite der Brücke.