---
layout: "image"
title: "Radarschirme"
date: 2024-11-09T22:26:30+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor unten dreht den rechten Schirm. Der linke wird über die Kette mitgedreht, die auf selbstklemmenden Z15 sitzt.