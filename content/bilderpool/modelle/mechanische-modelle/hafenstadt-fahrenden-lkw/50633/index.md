---
layout: "image"
title: "Antriebsblock (2)"
date: 2024-11-09T22:25:12+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos86.jpg"
weight: "86"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Feder (eine aus dem Robotics Smarttech) drückt den Antriebsblock weg von der Fahrspur und spannt so das Seil, während aber Bewegungsspielraum beim Durchgang der Magnete durch Kurven bleibt.