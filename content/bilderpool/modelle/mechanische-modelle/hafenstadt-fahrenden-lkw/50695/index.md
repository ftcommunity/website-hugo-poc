---
layout: "image"
title: "Freiheitsstatue"
date: 2024-11-09T22:26:28+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Meer steht eine ft-Figur auf einem ft-Reifen, seinerzeit von Ralf Knobloch golden lackiert, und trägt eine Fackel ;-)