---
layout: "image"
title: "Frachtschiff - Auflegeteile im Bug"
date: 2024-11-09T22:26:01+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese zwei kleinen Baugruppen passen in auf den Bootsbug.