---
layout: "image"
title: "Brücke"
date: 2024-11-09T22:25:40+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos65.jpg"
weight: "65"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist nur das Stückchen Fahrbahn unterhalb der Brücke noch aufgesetzt. Die Seile werden also tatsächlich hochgeführt, verlaufen über die Rollenblöcke und werden von einigen "Hilfsblechen" in der richtigen Position gehalten.