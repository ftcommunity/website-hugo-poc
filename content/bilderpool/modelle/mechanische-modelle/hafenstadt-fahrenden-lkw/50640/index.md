---
layout: "image"
title: "Frachtschiff - Antriebsschrauben"
date: 2024-11-09T22:25:21+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die zwei Schrauben bestehen aus je einer Hälfte der neuen, schwergängigen Gelenke, einem Statikstopfen und einer der schwarzen Unterlegscheiben. Oben sitzen zwei Winkelsteine rechtwinklig, die heute nicht mehr hergestellt werden.