---
layout: "image"
title: "Seilführung (7)"
date: 2024-11-09T22:25:28+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos74.jpg"
weight: "74"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der anderen Seite der Brücke gibt es ähnliche Führungsbleche. Die senkrecht stehenden Federnocken rechts am Bild (einer ist verdeckt) halten die schrägen Brückenfahrbahnen zuverlässig unten - schließlich drückt das gespannte Seil hier ja von unten nach oben.