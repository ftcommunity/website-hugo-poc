---
layout: "image"
title: "Fahrzeugseite ohne Räder"
date: 2024-11-09T22:25:15+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos84.jpg"
weight: "84"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick mit zwei Rädern abgenommen.