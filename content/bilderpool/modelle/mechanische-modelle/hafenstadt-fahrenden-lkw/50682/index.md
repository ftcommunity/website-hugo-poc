---
layout: "image"
title: "Windmühle - Fuß"
date: 2024-11-09T22:26:12+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb kommt vom zweiten Fahrbahn-Seilantrieb. Auch die Windmühle ist, wie die Drehterrasse, nur mit einem einzigen BS7,5 mit dem Antrieb verbunden. Das rote Z20 auf der Antriebsachse treibt das Rast-Z10, das über die Kardan-Strecke (mit einer Federnocke zwischen den beiden Kardangelenken) nach oben zum Mühlenrad führt.