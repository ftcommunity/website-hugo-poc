---
layout: "image"
title: "Radarschirme - Antrieb"
date: 2024-11-09T22:26:29+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor soll langsamer drehen, als er das bei den 6 V Versorgungsspannung tun würde. Deshalb sind zwei parallel geschaltete ft-Lämpchen (ein normales und eine Linsenlampe) als Vorwiderstand in Serie mit dem Motor geschaltet.

Die Befestigung unten mit den je 3 Winkelsteinen 30° passt rasterkonform, weil es die neuere Bauform der Winkelsteine ist.