---
layout: "image"
title: "Sternwarte - Mechanik in der Kuppel"
date: 2024-11-09T22:25:49+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos59.jpg"
weight: "59"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der BS7,5 in Bildmitte sitzt genau über der Drehachse und wird von der Hebelmechanik untendrunter periodisch angehoben und wieder abgesenkt. Die beiden senkrecht daran angebrachten Bausteine 30 werden mit angehoben und abgesenkt.