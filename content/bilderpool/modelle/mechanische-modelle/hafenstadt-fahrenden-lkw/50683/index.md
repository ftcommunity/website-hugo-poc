---
layout: "image"
title: "Windmühle"
date: 2024-11-09T22:26:13+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Windmühlenflügel bestehen aus 35772/35771 Statik-Aufnahmestreifen 120 bzw. 60 aus dem hobby-S bzw. u-t S.