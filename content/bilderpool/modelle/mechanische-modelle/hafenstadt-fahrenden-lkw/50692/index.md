---
layout: "image"
title: "Radarschirm 1"
date: 2024-11-09T22:26:24+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Rückseite. In den BS7,5 stecken Federnocken, und in denen sitzen die Achshalter.