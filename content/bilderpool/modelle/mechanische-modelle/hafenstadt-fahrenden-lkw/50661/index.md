---
layout: "image"
title: "Sternwarte - Mechanik in der Kuppel"
date: 2024-11-09T22:25:46+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos60.jpg"
weight: "60"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Am hier linken BS30 neben dem BS7,5 hängt auch eine links von den unteren beiden Rastadaptern gerade noch erkennbare Zahnstange 30 mit feinen Zähnen. Die treibt etwas ähnliches an wie die alte 31050 "Metallachse 50 + Zahnrad Z44 m0,5 [rot]" aus dem Ur-mot.2 an. Allerdings wäre deren Achse zu lang. Deshalb gibts hier eine normale Kunststoffachse, auf der nur ein feines 35133 "Zahnrad Z44 m0,5 [rot]" sitzt. Das habe ich vermutlich mal bei irgend einem Konvolut mit bekommen, und es klemmt gut auf der Achse und tut hier wunderbaren Dienst.

So dreht die Achse mit dem feinen Z44 also immer hin und her und nimmt so das "Fernrohr" - von dem nur die Spitze durch die rote "Hülse mit Scheibe" dargestellt wird - in einer Schwenkbewegung mit.

Der rechte BS30 neben dem BS7,5 ist etwas in Richtung Bild-Front verschoben. Dadurch werden die BS30 etwas verdreht, und so greift die feine Zahnstange sauber ins Z44 ein.