---
layout: "image"
title: "Seilführung (2)"
date: 2024-11-09T22:25:36+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos68.jpg"
weight: "68"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Weiter also zum Antrieb unterhalb der Windmühle.

Unten und ganz links sieht man, wie die Magnete mit den zwei Abstandsringen auf dem Seil aufgefädelt und eingeklemmt sind.