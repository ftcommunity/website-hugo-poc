---
layout: "image"
title: "Fahrantrieb unterhalb der Windmühle"
date: 2024-11-09T22:26:06+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos46.jpg"
weight: "46"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da die LKW auch bei diesem Seilantrieb kurz anhalten, wenn der Magnet auf dem Seil aus der Bahn heraus und um die Drehscheibe geführt wird, gibt es hier ein Parkplatz-Schild, analog zur Aussichts-Drehterrasse. Die Schilder sind auf einem Farblaser gedruckt, mit Pritt-Stift aufgeklebt und zusätzlich an den Enden gesichert.