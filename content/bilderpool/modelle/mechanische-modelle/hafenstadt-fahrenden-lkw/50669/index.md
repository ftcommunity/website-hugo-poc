---
layout: "image"
title: "Photovoltaik"
date: 2024-11-09T22:25:56+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos53.jpg"
weight: "53"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Solarpanel kann durch den BS15 mit rotem runden Zapfen unten und dem schwergängigen Gelenk oben beliebig ausgerichtet werden.