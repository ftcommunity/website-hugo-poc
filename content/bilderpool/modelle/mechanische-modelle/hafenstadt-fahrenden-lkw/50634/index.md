---
layout: "image"
title: "Antriebsblock (1)"
date: 2024-11-09T22:25:13+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos85.jpg"
weight: "85"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der gesamte Antrieb incl. Motor, Getriebe, Drehscheibe und auch den darauf aufsetzenden Teilmodellen Drehplattform bzw. Windmühle ist in einem stabilen, aber kleinen Gestell drehbar an einer Metallachse gelagert.