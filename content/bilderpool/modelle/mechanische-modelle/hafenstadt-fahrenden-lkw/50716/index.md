---
layout: "image"
title: "Hafenkran"
date: 2024-11-09T22:26:53+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist einfach nochmal ein Nachbau von https://ftcommunity.de/bilderpool/modelle/kleinmodelle-zum-nachbauen/miniatur-hafenkran/gallery-index/ mit Federnocken auf den Grundplatten befestigt. Er ist voll funktionstüchtig.