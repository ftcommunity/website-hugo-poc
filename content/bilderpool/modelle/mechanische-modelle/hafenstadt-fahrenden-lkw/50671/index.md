---
layout: "image"
title: "Baukran - Sicherheitsschienen"
date: 2024-11-09T22:25:58+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos51.jpg"
weight: "51"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind die oberen Hilfsschienen mal abgenommen.