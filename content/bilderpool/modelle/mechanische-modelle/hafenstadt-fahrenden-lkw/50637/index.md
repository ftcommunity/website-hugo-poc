---
layout: "image"
title: "Fahrzeugboden"
date: 2024-11-09T22:25:17+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos82.jpg"
weight: "82"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Neodym-Magnet ist bei allen Fahrzeugen gleich ausgerichtet und gleich eingebaut. Er steht der Vorderachse näher als der Hinterachse, damit das Fahrzeug möglichst gerade auf der Spur läuft. Schienen gibt's ja keine und braucht's auch keine.