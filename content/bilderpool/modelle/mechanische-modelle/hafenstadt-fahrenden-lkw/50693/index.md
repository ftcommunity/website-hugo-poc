---
layout: "image"
title: "Radarschirm 1"
date: 2024-11-09T22:26:25+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Schirme sitzen beide auf Rastadaptern und bestehen außer aus ein paar Winkelsteinen und Verkleidungsplatten aus zwei 31081 Achshaltern 30x60, in denen in den 1970er-Jahre-hobby-Kästen die Achsen verstaut waren.