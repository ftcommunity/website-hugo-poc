---
layout: "image"
title: "Fahrzeugseite"
date: 2024-11-09T22:25:16+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos83.jpg"
weight: "83"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Räder sind wieder auf einfache K-Achsen 30 aufgesteckte "Hülse mit Scheibe".