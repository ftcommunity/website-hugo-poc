---
layout: "image"
title: "Fahrbahn"
date: 2024-11-09T22:25:44+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos62.jpg"
weight: "62"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Jetzt aber endlich zum Gag des Modells. Die Fahrbahn besteht, wie man sieht, aus lauter Statikplatten 90x180, einschließlich einer Brücke in der Mitte. Auf zwei gegenläufigen Fahrspuren werden je 3 kleine "LKW" bewegt, also mit Gegenverkehr.

Hier sind Aussichts-Drehplattform, Leuchtturm-Linsenkorb, Windmühle, Windrad und Sternwartenkuppel abgenommen.