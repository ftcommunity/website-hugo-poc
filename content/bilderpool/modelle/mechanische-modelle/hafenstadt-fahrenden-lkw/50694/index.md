---
layout: "image"
title: "Radarschirme - Antriebskette"
date: 2024-11-09T22:26:26+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Z15 der Kette sind selbstklemmende.