---
layout: "overview"
title: "Hafenstadt mit fahrenden LKW"
date: 2024-11-09T22:25:06+01:00
---

Sechs winzige LKW werden in Form einer "8" auf zwei Spuren mit Gegenverkehr incl. Brücke gefahren. Zur Ablenkung davon gibt es eine Reihe von "Modellen im Modell".