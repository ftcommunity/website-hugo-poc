---
layout: "image"
title: "Windmühle - Befestigung der Flügel"
date: 2024-11-09T22:26:09+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos43.jpg"
weight: "43"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb kommt von einem Rastkegelrad mit Achse auf einen Rastadapter. Auf dem sitzt ein BS15, and dem die Flügel angebracht sind. Damit das ganze stabil ist, gibt es die vier Verbindungsstücke 15 drumrum.