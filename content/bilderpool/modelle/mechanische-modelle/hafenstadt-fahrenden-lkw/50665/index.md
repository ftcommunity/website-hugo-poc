---
layout: "image"
title: "Windrad - Papier im Propeller"
date: 2024-11-09T22:25:51+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos57.jpg"
weight: "57"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man das Papierstücken im Propeller, damit der besser auf der Solarmotor-Achse sitzt.