---
layout: "image"
title: "Leuchtturm - Linsenkäfig von unten"
date: 2024-11-09T22:26:34+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In einem Z30 stecken Klemmstifte 15, die 3 Winkelsteine 60° halten. An denen sind per BS7,5 die Linsen befestigt. Dieser Korb kann einfach von oben auf den Leuchtturm-Antrieb aufgesetzt (und von ihm abgenommen) werden.