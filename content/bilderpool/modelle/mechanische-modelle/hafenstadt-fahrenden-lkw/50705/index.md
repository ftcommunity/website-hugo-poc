---
layout: "image"
title: "Fahrantrieb - Führungen"
date: 2024-11-09T22:26:40+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Seil kommt im Bild "unten" rein und geht "oben" wieder raus. Die "Führungsbleche" sollen dafür sorgen, dass die Magnete richtigrum ausgerichtet in die Drehscheibe einreisen und richtigrum bei der Umlenkrolle oben rausgehen. Die müssen nämlich immer "weg von den Umlenkrollen" liegen, hier also schon zwei Mal ihre Lage relativ zum Seil ändern.