---
layout: "image"
title: "Windmühle - Befestigung"
date: 2024-11-09T22:26:10+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nur an diesem BS7,5 hängt die gesamte Windmühle, damit man sie bei Bedarf leicht vom Antrieb abnehmen kann.