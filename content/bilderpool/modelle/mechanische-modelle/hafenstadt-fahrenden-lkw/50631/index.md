---
layout: "image"
title: "Antriebsblock (4)"
date: 2024-11-09T22:25:10+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos88.jpg"
weight: "88"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Detailblick auf die Drehaufhängung.