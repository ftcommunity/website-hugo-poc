---
layout: "image"
title: "Drehbare Aussichtsplattform"
date: 2024-11-09T22:26:49+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die abgenommene Drehplattform. Rechts unten sieht man den BS7,5 zum Anbauen an den Antrieb.