---
layout: "image"
title: "Frachtschiff - Antennen"
date: 2024-11-09T22:25:34+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In den Antennen sind Rastadapter, Federnocken, deren Fassung für Statikstreben und Klemmachsen 15 verbaut.