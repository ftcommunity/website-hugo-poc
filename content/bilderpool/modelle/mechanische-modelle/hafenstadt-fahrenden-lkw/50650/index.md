---
layout: "image"
title: "Magnetbefestigung"
date: 2024-11-09T22:25:33+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos70.jpg"
weight: "70"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf das Seil werden also 6 Abstandsringe gefädelt. In je zwei davon wird ein 10 mm langer Neodym-Magnet mit 4 mm Durchmesser mit Kraft eingedrückt. Der sitzt da bombenfest. Alle Magnete sind in Fahrtrichtung gleich ausgerichtet, die der LKW genau andersherum, sodass sich LKW und Seilmagnet jeweils anziehen.

Die Strebenadapter, in die die Fahrbahn eingeclipst wird, sitzen übrigens genau richtig. Die Statikplatten sind nämlich leider deutlich länger als 120 mm, und dieser Fehler addiert sich bei mehreren hintereinander liegenden Platten deutlich.