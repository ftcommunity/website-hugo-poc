---
layout: "image"
title: "Stadt von oben"
date: 2024-11-09T22:26:41+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Aus der Vogelperspektive sieht man die "8"er-Fahrbahn, über die sich die LKW bewegen. Warum sie das tun, sieht man noch nicht.

Rechts unten im Meer die "Freiheitsstatue", ein Frachtschiff, ein Hafenkran, links davon eine sich drehende Aussichtsplattform und ein Leuchtturm. Rechts hinten zwei sich drehende Radarschirme mit einem kleinen Gebäude daneben. Links vorne eine Windmühle, ein Baukran