---
layout: "image"
title: "Seilenden"
date: 2024-11-09T22:25:22+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos79.jpg"
weight: "79"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Detailblick auf die beide eingeklemmten Seilenden einer Spur.