---
layout: "image"
title: "Drehbare Aussichtsplattform"
date: 2024-11-09T22:26:47+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Detailblick auf die Z10. Der Antrieb läuft hier über die rechte Achse, die zwei Z10 trägt.