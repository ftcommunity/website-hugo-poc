---
layout: "image"
title: "Fahrantrieb - gefederte Aufhängung"
date: 2024-11-09T22:26:43+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier noch ein anderer Blick auf die federnde Spann-Aufhängung des Antriebs.