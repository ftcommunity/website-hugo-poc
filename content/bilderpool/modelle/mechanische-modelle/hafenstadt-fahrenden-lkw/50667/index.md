---
layout: "image"
title: "Windrad - Fuß"
date: 2024-11-09T22:25:53+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos55.jpg"
weight: "55"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch das Windrad ist mit einem roten BS15 mit rotem Zapfen drehbar auf der Bauplatte befestigt.