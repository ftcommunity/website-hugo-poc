---
layout: "image"
title: "Windmühle - Flügelbefestigung"
date: 2024-11-09T22:26:08+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos44.jpg"
weight: "44"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf dem BS15 sitzt aus Ziergründen der Zapfen aus einem Grundbaustein, der mal irgendwann geplatzt war. Die Zapfen habe ich schön aufgehoben.