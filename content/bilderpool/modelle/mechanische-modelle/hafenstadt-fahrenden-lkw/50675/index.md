---
layout: "image"
title: "Fahrantrieb von oben"
date: 2024-11-09T22:26:03+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos48.jpg"
weight: "48"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der drehbare und gefederte Antriebsblock zur Seilspannung, links oben die Photovoltaik ;-)