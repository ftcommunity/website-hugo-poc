---
layout: "image"
title: "Haus von hinten"
date: 2024-11-09T22:26:15+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch von hinten ist das Haus eher unspektakulär.