---
layout: "image"
title: "Fahrzeugflotte"
date: 2024-11-09T22:25:18+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos81.jpg"
weight: "81"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das sind die sechs "LKW". Sie haben alle dieselbe Geometrie und bis auf Verzierungen exakt denselben Aufbau. Die auf einer Fahrspur tragen rote "Frontscheinwerfer", die der anderen Fahrspur orangefarbene. Das dient aber nur der leichteren Kontrolle, bei welcher Fahrspur denn da ein Auto fehlt. Sie wären beliebig auf beiden Fahrspuren einsetzbar.