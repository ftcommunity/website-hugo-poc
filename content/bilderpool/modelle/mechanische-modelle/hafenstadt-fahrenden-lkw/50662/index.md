---
layout: "image"
title: "Frachtschiff - Brücke"
date: 2024-11-09T22:25:47+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Wesentlichen 10 Schneckenbausteine.