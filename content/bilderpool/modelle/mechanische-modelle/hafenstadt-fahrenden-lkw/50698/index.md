---
layout: "image"
title: "Leuchtturm - Linsenkorb von oben"
date: 2024-11-09T22:26:31+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von oben könnte man die Lampe im Turm sehen, wenn der Korb gerade aufgesetzt wäre.