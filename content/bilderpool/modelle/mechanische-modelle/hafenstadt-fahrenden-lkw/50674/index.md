---
layout: "image"
title: "Baukran"
date: 2024-11-09T22:26:02+01:00
picture: "2024-09-12_hafenstadt_mit_fahrenden_autos49.jpg"
weight: "49"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch dieser Kran ist nur aufgewärmt. Es ist der hier: https://ftcommunity.de/bilderpool/modelle/kleinmodelle-zum-nachbauen/miniatur-turmdrehkran/gallery-index/