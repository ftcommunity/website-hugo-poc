---
layout: "overview"
title: "Kastanien- & Bälle-Mühle"
date: 2020-02-22T08:20:49+01:00
legacy_id:
- /php/categories/3064
- /categories61ae.html
- /categories631d.html
- /categories902f.html
- /categories1900.html
- /categoriesa2e8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3064 --> 
Im Sandkasten hatte bestimmt der Eine oder Andere im Kindesalter eine Sandmühle.
Oben wird rieselfähiger Sand eingefüllt. Dieser  fließt durch einen Trichter.
Der herunterfallende Sand treibt ein oder zwei Mühlräder an.

Das Prinzip, wurde einfach auf Kastanien und Bälle angewandt.

Anforderungen die erfüllt werden sollten:

+ es soll ein "Lern(äahhh) Spielzeug" für Kinder im Alter von 1-99 Jahren sein
+ hohe Stabilität
+ Unfallsicher (verschluckbare Kleinteile)
+ Umfallsicher (umsturzsicher)