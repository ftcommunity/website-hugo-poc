---
layout: "image"
title: "Kastanienmühle Seite 02"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle15.jpg"
weight: "15"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40782
- /details9f65.html
imported:
- "2019"
_4images_image_id: "40782"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40782 -->
