---
layout: "image"
title: "Kastanienmühle Vorne 03"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle09.jpg"
weight: "9"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40776
- /details8fed.html
imported:
- "2019"
_4images_image_id: "40776"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40776 -->
Damit der "Prozess des Antreibens" besser sichtbar ist dient eine bläulich- klare Statikplatte als Sichtfenster.
