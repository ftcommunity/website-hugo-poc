---
layout: "image"
title: "Oben 02"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle04.jpg"
weight: "4"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40771
- /detailsd9e0.html
imported:
- "2019"
_4images_image_id: "40771"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40771 -->
Kastanien fallen von oben durch den Trichter,
 fallen auf die Schaufeln und treiben das Schaufelrad an.
