---
layout: "image"
title: "Kastanienmühle Schaufelrad 02"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle02.jpg"
weight: "2"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40769
- /detailsd7ee.html
imported:
- "2019"
_4images_image_id: "40769"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40769 -->
