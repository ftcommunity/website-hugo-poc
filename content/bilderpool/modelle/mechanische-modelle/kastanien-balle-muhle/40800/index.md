---
layout: "image"
title: "kastanienbaellemuehle33.jpg"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle33.jpg"
weight: "33"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40800
- /details7f0f-2.html
imported:
- "2019"
_4images_image_id: "40800"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40800 -->
