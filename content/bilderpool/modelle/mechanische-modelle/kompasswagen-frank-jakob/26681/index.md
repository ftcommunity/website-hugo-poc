---
layout: "image"
title: "PICT4957"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26681
- /details55a1.html
imported:
- "2019"
_4images_image_id: "26681"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26681 -->
Ansicht von vorne