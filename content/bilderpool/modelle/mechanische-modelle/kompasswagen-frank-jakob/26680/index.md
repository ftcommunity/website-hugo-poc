---
layout: "image"
title: "PICT4956"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26680
- /details6977.html
imported:
- "2019"
_4images_image_id: "26680"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26680 -->
Ansicht von links