---
layout: "overview"
title: "Zweizylinder-Pneumatikmotor 2.1"
date: 2020-02-22T08:14:33+01:00
legacy_id:
- /php/categories/3208
- /categoriesf52e.html
- /categoriesb5cf.html
- /categories7c2e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3208 --> 
Die verbesserte Version des Einzylinder-Pneumatikmotors 2.0 (http://ftcommunity.de/categories.php?cat_id=3173).

Verbesserungen:
stabilere Kurbelwelle
bessere Ventile...
und zwei Zylinder

Dank der Zylinder mit Federr ist der Motor selbsanlaufend.

Ein kurzes Video gibt es unter: https://youtu.be/OiWAKAY1sT0