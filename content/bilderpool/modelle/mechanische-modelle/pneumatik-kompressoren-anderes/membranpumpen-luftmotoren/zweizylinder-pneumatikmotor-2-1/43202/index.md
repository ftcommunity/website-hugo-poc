---
layout: "image"
title: "Ventil 3"
date: "2016-03-21T13:33:55"
picture: "pneumatikmotor09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43202
- /detailsfd92.html
imported:
- "2019"
_4images_image_id: "43202"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43202 -->
Da die zwei Zylinder versetzt auf der Kurbelwelle sitzen, muss die Ansteuerung vom Zylinder zum Ventil auch außermittig statt finden.