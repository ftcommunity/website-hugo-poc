---
layout: "image"
title: "Ventil 2"
date: "2016-03-21T13:33:55"
picture: "pneumatikmotor08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43201
- /details8faa.html
imported:
- "2019"
_4images_image_id: "43201"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43201 -->
Das Ventil in seiner anderen Endposition.