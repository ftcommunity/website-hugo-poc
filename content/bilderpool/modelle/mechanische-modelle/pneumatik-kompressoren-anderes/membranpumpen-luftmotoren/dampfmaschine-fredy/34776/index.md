---
layout: "image"
title: "Dampfmaschine 05"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine5.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34776
- /details0305.html
imported:
- "2019"
_4images_image_id: "34776"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34776 -->
Kurbelwelle
