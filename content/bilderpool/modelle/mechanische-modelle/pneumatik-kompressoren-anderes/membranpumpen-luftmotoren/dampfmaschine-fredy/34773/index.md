---
layout: "image"
title: "Dampfmaschine 02"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34773
- /details414e.html
imported:
- "2019"
_4images_image_id: "34773"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34773 -->
Zylinder und Führung
