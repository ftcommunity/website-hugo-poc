---
layout: "image"
title: "Dampfmaschine 03"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine3.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34774
- /details0a8d.html
imported:
- "2019"
_4images_image_id: "34774"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34774 -->
Schwungrad, die bs15 habe ich dort rein gemacht, damit es etwas schwerer wird.
