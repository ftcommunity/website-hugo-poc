---
layout: "image"
title: "Dampfmaschine 01"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/34772
- /detailsc614.html
imported:
- "2019"
_4images_image_id: "34772"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34772 -->
Die Maschine wird von dem neuen fischertechnik Kompressor mit Luft versorgt.
http://youtu.be/B9Q1QKYxNqQ
