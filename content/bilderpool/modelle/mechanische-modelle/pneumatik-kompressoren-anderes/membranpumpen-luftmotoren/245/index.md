---
layout: "image"
title: "Luftmotor"
date: "2003-04-21T20:26:47"
picture: "luftmotorprototype01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Luftmotor", "Membranpumpe", "Silikonschlauch"]
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/245
- /details68b4.html
imported:
- "2019"
_4images_image_id: "245"
_4images_cat_id: "30"
_4images_user_id: "7"
_4images_image_date: "2003-04-21T20:26:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=245 -->
erstes Prototype von Luftmotor mit Membranzylinder und Silikonschlauche als Ventile dabei.  Ein wirkendes Modell  wird allerdings zwei Membranzylinder und vier abklemmbare Silikonschlauche brauchen