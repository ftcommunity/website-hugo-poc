---
layout: "image"
title: "Dampfmaschine 2"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_01.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23227
- /details8783.html
imported:
- "2019"
_4images_image_id: "23227"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23227 -->
Gesamtansicht von Dampfmaschine und Kompressor.