---
layout: "image"
title: "Dampfmaschine 1"
date: "2009-02-27T20:00:19"
picture: "Dampfmaschine_04.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/23226
- /details4d47.html
imported:
- "2019"
_4images_image_id: "23226"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23226 -->
Hier seht Ihr meine funktionierende Dampfmaschine. Es ist im Prinzip ein 1-Zylinder-Zweitakt-Luftmotor, aber ich habe mich funktional und optisch an einer historischen Dampfmaschine orientiert, so dass ich das Modell so benannt habe.

Ziel des Modells war es, im Gegensatz zu den hier bereits gezeigten Luftmotoren nur mit der Technik des aktuellen „Profi Pneumatik II“-Kastens zu arbeiten, das heißt:

- keine modifizierten Bauteile
- keine FT-fremden Bauteile
- keine Elektrik-Komponenten
- keine Pneumatik-Bauteile aus früheren Kästen (z.B. die alten Ventile)
- keine externe Druckluftversorgung
- Verwendung des aktuellen Original-FT-Kompressors

Hinten rechts ist der Druckluftanschluss, wo der Original-FT-Kompressor (optisch leicht modifiziert) über einen Schlauch angeschlossen wird. Das Handventil vorn rechts startet die Dampfmaschine. Stoppt man Sie an der richtigen Stelle (Steuerventil gerade geöffnet und Kurbelwelle über einem Totpunkt), ist sie selbstanlaufend. Im Normalfall muss sie aber – wie jeder 1-Zylinder-Motor – per Hand am Schwungrad angeworfen werden.

Der Zylinder ist doppeltwirkend und überträgt seinen Hub über einen Pleuel auf das Zahnrad Z30, das gleichzeitig als Kurbel und zur Übertragung der Drehbewegung über einen Kettentrieb auf das Schwungrad dient.

Mit dem Schwungrad, seiner Größe/Masse und dem Übersetzungsverhältnis habe ich mich sehr lange beschäftigt. Ich habe kleinere Schwungräder ausprobiert, was aber wenig erfolgreich war. Auch Übersetzungsverhältnisse von 1:1 und 1:2 haben nicht ausgereicht, um das Schwungrad am Laufen zu halten. Das dargestellte Schwungrad mit dem Übersetzungsverhältnisse 1:3 funktioniert prima; ein größeres/schwereres Schwungrad bringt keinen zusätzlichen Mehrwert.

An der Lagerwelle des Schwungrades sitzt ein Zahnrad Z20, das als Abtrieb der Dampfmaschine dient.

Die Kurbelwelle hat am anderen Ende eine Drehscheibe 60, die zur Ansteuerung des Steuerventils dient. Die Winkellage zur Zylinder-Kurbel und die Kurbellänge mussten vielfach optimiert und eingestellt werden, damit die Dampfmaschine sauber und rund läuft.

Das Steuerventil ist ein Handventil mit Ansteuerung per Hebel, was gut funktioniert.

Beim Bauen und Ausprobieren hat sich gezeigt, dass es extrem wichtig ist, die Reibung im gesamten System so niedrig wie möglich zu halten. Schon die Verwendung von normalen Gelenkwürfel-Klauen mit Lagerhülsen zur Lagerung von Kurbelwelle und Schwungrad hat die Reibung so stark erhöht, dass die Dampfmaschine nicht rund lief und stehen blieb.

Die Dampfmaschine erzeugt ein recht brauchbares Drehmoment am Abtrieb vom Schwungrad. Mit dem Luftmassenstrom vom Original-FT-Kompressor habe ich Kurbelwellen-Drehzahlen von 24 u/min erreicht.

Alles in allem ein – so finde ich – technisch faszinierendes und optisch ansprechendes Modell einer Dampfmaschine. Ich kann dem Ding stundenlang beim Arbeiten zusehen. Es rattert, zischt und knarzt – traumhaft... ;o)

Zur besseren Veranschaulichung gibt es hier ein Video zu sehen:

http://www.youtube.com/watch?v=9dAV4oZq6OY

Man beachte, mit wieviel Kraft die Maschine aus dem Stand loslegt!