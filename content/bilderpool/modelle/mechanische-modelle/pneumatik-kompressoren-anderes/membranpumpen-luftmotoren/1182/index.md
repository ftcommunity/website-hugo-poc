---
layout: "image"
title: "Die kettengesteuerte 'DOHC'"
date: "2003-06-09T17:14:51"
picture: "V-Mot3.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1182
- /details96a0-2.html
imported:
- "2019"
_4images_image_id: "1182"
_4images_cat_id: "30"
_4images_user_id: "27"
_4images_image_date: "2003-06-09T17:14:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1182 -->
Fast wie ein Originalmotor :-)