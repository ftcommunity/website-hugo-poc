---
layout: "image"
title: "V2 Motor mit DOHC"
date: "2003-06-09T17:14:50"
picture: "v-mot1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/1180
- /details1490.html
imported:
- "2019"
_4images_image_id: "1180"
_4images_cat_id: "30"
_4images_user_id: "27"
_4images_image_date: "2003-06-09T17:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1180 -->
Hier ein V2 Luftmotor mit doppelter obenliegender Nockenwelle