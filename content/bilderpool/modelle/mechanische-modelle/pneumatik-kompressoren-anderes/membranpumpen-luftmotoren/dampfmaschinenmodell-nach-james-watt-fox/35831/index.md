---
layout: "image"
title: "Kurbelwelle und Umschalter (Dampfmaschine)"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35831
- /detailsfa86.html
imported:
- "2019"
_4images_image_id: "35831"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35831 -->
Die Kurbelwelle betätigt über eine I-Strebe 75 den Druckluft-Umschalter, der die Druckluft abwechselnd in die vordere bzw. hintere Kolbenkammer einleitet.