---
layout: "image"
title: "Umschalter (Dampfmaschine)"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35832
- /detailsa6e2.html
imported:
- "2019"
_4images_image_id: "35832"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35832 -->
Die I-Strebe 75 bewegt einen Gelenkbaustein 45, der über einen Baustein 7,5 und zwei daran befestigte Bausteine 15x30 ein (sehr leichtgängiges) Pneumatik-Handventil betätigt.