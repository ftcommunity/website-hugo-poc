---
layout: "image"
title: "Ventil 1"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42662
- /details0cda-3.html
imported:
- "2019"
_4images_image_id: "42662"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42662 -->
Hier ist der Ablusftschlauch (rechte Schlauchschleife) abgeknickt und der Druckluftschlauch (linke Schlauchschleife) offen.