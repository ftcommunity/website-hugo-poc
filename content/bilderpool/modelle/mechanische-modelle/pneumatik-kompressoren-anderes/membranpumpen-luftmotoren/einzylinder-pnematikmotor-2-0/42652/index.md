---
layout: "image"
title: "Gesamtansicht 2"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42652
- /detailsa8a7.html
imported:
- "2019"
_4images_image_id: "42652"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42652 -->
Innsgesammt ist das Modell relativ stabil und der Motor läuft bis jetzt ohne Probleme.