---
layout: "overview"
title: "4-Zylinder-Axialkolbenmotor PDP + Festoventilen"
date: 2020-02-22T08:14:38+01:00
legacy_id:
- /php/categories/3471
- /categoriese72e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3471 --> 
Mir hat Alfred Petteras 6-Zylinder-Axialkolbenmotor auch fasziniert. 
Die 4-Zylinder-Axialkolbenmotor Stefan Reinmueller sieht sehr gut aus.
Ich habe dieser nachgebaut  mit  (auch) eine: Antriebseinheit (4 pneumatikzylinder) + "Taumelkreuz" + Schwungrad. 
Statt das Ventilinsel habe ich die Festo 3/2-Wegeventilen + Nocken genutzt.
