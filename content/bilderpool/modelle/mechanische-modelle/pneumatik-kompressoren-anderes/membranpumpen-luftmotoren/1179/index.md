---
layout: "image"
title: "luftmotor01(Pettera)"
date: "2003-06-06T15:14:08"
picture: "luftmotor01.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Luftmotor", "Membran"]
uploadBy: "pvd"
license: "unknown"
legacy_id:
- /php/details/1179
- /details4488.html
imported:
- "2019"
_4images_image_id: "1179"
_4images_cat_id: "30"
_4images_user_id: "7"
_4images_image_date: "2003-06-06T15:14:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1179 -->
Luftmotor gebaut nach einem Modell aus Philips Mechanical Engineer Baukasten (1966).
Der Zylinder hat ein Gehause (PVC Teil , AbwasserRohr) worüber ein Balon gespannt ist.  Am Balon ist eine 4 mm Stahlachse verbunden ; bei Drückänderungen bewegt diese Achse (Kolben) , das gibt die Drehbewegung . 
Und damit wird dann auch die Ventilwirkung gesteuert :  vorne im Modell sieht man das graue Rad : damit werden die dünnwandige Silikonschlauche abgequetscht , und das gibt die benötigte Ventilwirkung.  Das Motormodell ist selbstanlaufend.