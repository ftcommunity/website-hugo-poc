---
layout: "comment"
hidden: true
title: "21421"
date: "2015-12-20T10:33:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Mein Erklärungsversuch war natürlich Quatsch, aber bei genauem Hinsehen sieht man vom Zylinder *zwei* Schläuche weggehen. Der Zweite ist die Abluft, geht auch - im Bild links vom Zuluftschlauch - unter der Abklemmmechanik entlang und wird also freigegeben, wenn die Zuluft gesperrt ist.

Gruß,
Stefan