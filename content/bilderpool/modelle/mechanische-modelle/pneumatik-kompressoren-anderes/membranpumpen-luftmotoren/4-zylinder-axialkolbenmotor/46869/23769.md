---
layout: "comment"
hidden: true
title: "23769"
date: "2017-11-05T15:00:05"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

Mit die "Öffsungs-Winkel" der Ventile meine ich die DrehWinkel wie lange das Ventil offen bzw. geschlossen ist.  Dieser zum richtigen Nocken-Winkel-Ventil-öffnung.
Is dieser 30, 45 oder 90 grad ?

Gruss, 

Peter 
Poederoyen NL