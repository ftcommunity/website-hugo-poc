---
layout: "comment"
hidden: true
title: "23765"
date: "2017-11-05T10:10:33"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich bin schwer beeindruckt und bin noch immer am Sinnieren:

Da ist eine Menge Aufwand drin, um dafür zu sorgen,dass die Zylinder nur in Längsrichtung beaufschlagt werden, gerade weil sie so bombenfest im Gehäuseblock verankert sind. Wenn jetzt die Motorachse nach dem Knick wieder zurück verschwenkt und dann rechts nochmal steif lagert, könnte man die P-Zylinder hinten und vorne zwischen zwei Kardangelenke packen und der Fall ist erledigt. Oder was übersehe ich da?

Gruß,
Harald