---
layout: "image"
title: "Ausgleich"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46867
- /detailsce71.html
imported:
- "2019"
_4images_image_id: "46867"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46867 -->
Vergleicht man dieses mit dem vorhergehenden Bild, wird klar, dass dieser Ausgleich nötig ist.