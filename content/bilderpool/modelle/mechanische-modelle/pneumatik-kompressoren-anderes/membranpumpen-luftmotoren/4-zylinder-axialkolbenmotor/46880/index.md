---
layout: "image"
title: "Ventile"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46880
- /detailsfeb5-2.html
imported:
- "2019"
_4images_image_id: "46880"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46880 -->
Die "Exzernterwelle" zum Antrieb der Ventile ist, wie hier zu sehen, aus zwei Drehscheiben aufgebaut, die durch eine exzentriesch liegende Kunstsoffachse verbunden sind. Auf die Drehscheiben sind Z40 aufgesteckt, die über die obenliegende Welle (mit einem Z10) angetrieben werden und dafür sorgen, dass sich die Drehscheiben synchron bewegen (Ohne diesen "Zusatzantrieb" jeder Exzenterhälfte würde sich alles verkeilen und nicht einwandfrei funktionieren).