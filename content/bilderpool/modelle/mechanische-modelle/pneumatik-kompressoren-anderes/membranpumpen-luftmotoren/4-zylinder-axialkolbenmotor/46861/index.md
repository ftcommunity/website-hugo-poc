---
layout: "image"
title: "Frontansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46861
- /details5d5d.html
imported:
- "2019"
_4images_image_id: "46861"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46861 -->
Die zwei Alus unten versteifen die Konstruktion zusätzlich.