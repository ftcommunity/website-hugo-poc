---
layout: "image"
title: "Gesamtansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46854
- /details4468.html
imported:
- "2019"
_4images_image_id: "46854"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46854 -->
Um das Modell stabiler (verwindungssteifer) und transportfähig zu bekommen, sind die zwei Grundplatten mit jeweils 4 Senkschrauben M4 auf einer Spanholzplatte befestigt.