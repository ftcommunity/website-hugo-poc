---
layout: "image"
title: "Ausgleich"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46866
- /details9d9d.html
imported:
- "2019"
_4images_image_id: "46866"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46866 -->
Da die Antriebszylinder starr befestigt sind und radial zur Haupachse des Schwungrades keinen Ausgleich ermöglichen, sind die Außenteile des Kreuzes durch eine Rastachse mit platte und zwei wetere Kunststoffachsen verschiebbar gelagert.