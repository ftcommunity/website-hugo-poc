---
layout: "image"
title: "Ventile"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/46881
- /details0e4a.html
imported:
- "2019"
_4images_image_id: "46881"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46881 -->
Eine der langwierigsten Arbeiten bei dem Modell war das Einstellen der Ventile. Um möglichst wenig Druck(luft)verlust und einen rhuigen Lauf zu erhalten waren einige Stunden an Feinjustierung notwendig... das Ergebnis war es meiner Meinung nach auf jeden Fall wert ;-)