---
layout: "overview"
title: "Dreizylinder-Druckluftmotor mit Schlauchlogik"
date: 2020-02-22T08:14:30+01:00
legacy_id:
- /php/categories/3153
- /categories55cf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3153 --> 
Dieser Motor wird von selbstgebauten Pneumatikventilen in "Schlauch-Logik" (siehe ft:pedia) gesteuert.