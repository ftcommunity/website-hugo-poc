---
layout: "image"
title: "Variable Ausrichtung"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42380
- /detailscb28.html
imported:
- "2019"
_4images_image_id: "42380"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42380 -->
Der Motor kann auf allen drei Seiten stehen, oder wie hier senkrecht, und sogar auf den zwei "Zwischenseiten" (überall da, wo der Kompressor nicht ist) und funktioniert auch da tadellos.
