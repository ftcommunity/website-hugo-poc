---
layout: "image"
title: "Ventilsteuerung"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42375
- /detailsa56c.html
imported:
- "2019"
_4images_image_id: "42375"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42375 -->
Als Exzenter für die Ventilsteuerung dient eine Kompressorkurbel vom älteren ft-Selbstbaukompressor. Danke an H.A.R.R.Y. für den Tipp (siehe http://www.ftcommunity.de/details.php?image_id=42175).
