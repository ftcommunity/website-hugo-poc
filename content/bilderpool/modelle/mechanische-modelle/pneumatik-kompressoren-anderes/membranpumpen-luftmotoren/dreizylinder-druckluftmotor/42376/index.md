---
layout: "image"
title: "Ventilaufbau"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42376
- /details9b23.html
imported:
- "2019"
_4images_image_id: "42376"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42376 -->
Am äußeren Anschluss kommt die Druckluft an. Der mittlere, bewegliche Anschluss geht zum jeweiligen Zylinder (in diesem Motor werden die Zylinder nur einfachwirkend angesteuert). Das untere T-Stück ist der Abluftausgang. Je nach Stellung des mittleren T-Stücks ist durch das geeignete Abknicken der kurzen Schläuche (30 mm) der Zylinder also entweder mit der Druckluft oder mit der Abluft verbunden.
