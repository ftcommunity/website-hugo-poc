---
layout: "image"
title: "Gesamtansicht"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42374
- /detailse0a1-2.html
imported:
- "2019"
_4images_image_id: "42374"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42374 -->
Ein ft-Kompressor bringt Druckluft in ein Puffervolumen. Von dort geht je ein Schlauch zu einem von drei Selbstbau-Ventilen (siehe nächstes Bild). Die steuern die drei Zylinder im Inneren an. Der Motor ist häufig selbstanlaufend.
