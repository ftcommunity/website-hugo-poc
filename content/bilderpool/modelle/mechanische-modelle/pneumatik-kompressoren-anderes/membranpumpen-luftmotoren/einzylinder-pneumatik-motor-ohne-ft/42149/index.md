---
layout: "image"
title: "Innenleben-Steuerung"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42149
- /detailsc7d5.html
imported:
- "2019"
_4images_image_id: "42149"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42149 -->
Die Steuerung funktioniert ganz einfach durch abwechselndes Abklemmen der zwei Schläuche (aus dem Fire Trucks-Set) die durch die Lenkklauen verlegt sind. Damit nie gleichzeitig Abluft-Schlauch und Druckluft-Schlauch offen sind und die Druckluft ungehindert ins Freie strömen kann sind die zwei Schaltscheiben um einen Zahn versetzt.
Von rechts unten kommt der Schlauch mit der Druckluft und links oben ist der Abluft-Schlauch, der blaue Schlauch rechts oben geht durch den BS15 mit Loch zum Zylinder.
Die Federnocken auf den BS7,5 vor den Lenkklauen fixieren die Schläuche.
Für möglichst wenig Reibung zwischen Schlatscheibe und Lenkhebel ist alles mit ordentlich Varseline eingefettet.

Bis alles richtig eingestellt ist dauert es zwar eine Weile, dann läuft der Motor aber einwandfei!