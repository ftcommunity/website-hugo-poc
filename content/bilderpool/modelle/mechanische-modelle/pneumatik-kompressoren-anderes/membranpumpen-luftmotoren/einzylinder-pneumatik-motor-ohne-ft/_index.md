---
layout: "overview"
title: "Einzylinder-pneumatik-Motor ohne ft-Ventile"
date: 2020-02-22T08:14:27+01:00
legacy_id:
- /php/categories/3140
- /categoriesbf5a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3140 --> 
Die meisten kennen sicherlich die "Lego switchless pneumatic engines" auf YouTube... solch einen Motor wollte ich nun auch aus fischertechnik bauen.
Nach vielen Fehlversuchen und langen Pausen ist es nun doch zu einem voll funktionsfähigen Pneumatikmotor gekommen.
Ein Vorteil dieser Variante ohne richtige Ventile ist, dass sie "relativ" kompakt baut und auch weniger Drehmoment benötigt wird, weil die schwergängigen fischertechnik Ventile weckfallent. Aus diesem Grund kann auch die Schwungscheibe recht klein gehalten werden.
Funktionsweise: die Schlatscheiben klemmen abwechselnd den Druckluftanschluss- und den Auslass-Schlauch ab.

Ein Video zum Funktionsbeweis gibt es hier: https://youtu.be/0luH46fEqSE

Der Motor läuft mit dem normalen fischertechnik Kompressor (ca. 0,8 bar) etwas langsam, aber tadellos. Wenn man den Druck auf ca. 1,5 bar erhöht läuft er dann wirklich perfekt.