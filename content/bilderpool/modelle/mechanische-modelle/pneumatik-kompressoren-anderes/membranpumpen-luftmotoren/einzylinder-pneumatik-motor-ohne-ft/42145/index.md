---
layout: "image"
title: "Vorderseite"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/42145
- /details83ec.html
imported:
- "2019"
_4images_image_id: "42145"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42145 -->
Hier schön zu sehen die Kurbelwelle, die vone kugelgelagert ist (würde auch ohne Kugellager einwandfrei fuktionieren). Damit bei höheren Drehzahlen bzw. bei höherer Krafteinwirkung  nicht alles verrutsch habe ich zwischen die Teile Taschenücherstücke geklemmt.
