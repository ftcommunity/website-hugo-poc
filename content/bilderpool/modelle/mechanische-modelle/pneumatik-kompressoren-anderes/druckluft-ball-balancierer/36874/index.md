---
layout: "image"
title: "Ansicht von oben"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36874
- /detailsa13e.html
imported:
- "2019"
_4images_image_id: "36874"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36874 -->
Dieses Modell geht aber noch einen Schritt weiter: Die Düse ist drehbar gelagert. Der Motor geht über zwei Anbauschnecken auf ein Z20, auf dessen Achse eine (leider nicht mehr hergestellte) Segmentscheibe 12 sitzt. Die hat 12 Zähne und greift bei einem Teil seiner Umdrehung in das Z40 im Bildhintergrund ein.
