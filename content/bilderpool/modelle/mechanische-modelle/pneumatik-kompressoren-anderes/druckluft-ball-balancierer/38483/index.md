---
layout: "image"
title: "Anschlag"
date: "2014-03-23T22:27:44"
picture: "Druckluft-Ball-Balancierer9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38483
- /details3bdf.html
imported:
- "2019"
_4images_image_id: "38483"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2014-03-23T22:27:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38483 -->
Das Z40 der Schwenkachse stößt, durch die Gummi-Kraft getrieben, mit einem seiner drei Zapfen an einen Verbinder 15. Auf diese Weise wird die senkrechte Positionierung der Düse nach jedem Schwenk wieder genau genug hergestellt.
