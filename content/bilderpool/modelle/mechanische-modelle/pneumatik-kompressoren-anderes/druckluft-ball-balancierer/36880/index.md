---
layout: "image"
title: "Momentaufnahme 4"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36880
- /details3560.html
imported:
- "2019"
_4images_image_id: "36880"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36880 -->
Wenn man den Antrieb abschaltet, kann man den Ball auch sehr schön von Hand durch Verdrehen der Düse beliebig im Raum bewegen.
