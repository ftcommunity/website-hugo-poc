---
layout: "image"
title: "Momentaufnahme 2"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36878
- /detailse897.html
imported:
- "2019"
_4images_image_id: "36878"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36878 -->
Bei der weiteren Drehung der Düse wandert der Tischtennisball in praktisch konstanter Höhe nach links frei schwebend durch die Luft. Er wird getragen von der an seiner Oberseite entlang strömenden Luft - die ihn außerdem dazu bringt, sich sehr schnell um seine eigene Achse zu drehen.
