---
layout: "image"
title: "Schiebetür offen"
date: "2010-05-02T22:08:20"
picture: "schiebetuer1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27041
- /detailsc4f3.html
imported:
- "2019"
_4images_image_id: "27041"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27041 -->
