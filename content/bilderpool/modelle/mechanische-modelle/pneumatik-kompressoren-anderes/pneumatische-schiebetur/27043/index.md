---
layout: "image"
title: "Schiebetür von oben"
date: "2010-05-02T22:08:20"
picture: "schiebetuer3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27043
- /details445a.html
imported:
- "2019"
_4images_image_id: "27043"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27043 -->
Rechts sieht man den blauen Zylinderkolben mit den beiden Zahnstangen und links das Interface.