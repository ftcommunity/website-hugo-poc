---
layout: "image"
title: "Schiebetür von hinten"
date: "2010-05-02T22:08:20"
picture: "schiebetuer4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27044
- /details23ec-2.html
imported:
- "2019"
_4images_image_id: "27044"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27044 -->
In der Mitte sieht man die Ventile, rechts davon den Luftspeicher und daneben den Kompressor.