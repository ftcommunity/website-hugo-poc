---
layout: "image"
title: "Programmbild"
date: "2010-05-02T22:08:21"
picture: "schiebetuer5.jpg"
weight: "5"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/27045
- /details5245.html
imported:
- "2019"
_4images_image_id: "27045"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27045 -->
Das Programm kann unter http://www.ftcommunity.de/data/downloads/robopro/schiebetr.rpp heruntergeladen werden.