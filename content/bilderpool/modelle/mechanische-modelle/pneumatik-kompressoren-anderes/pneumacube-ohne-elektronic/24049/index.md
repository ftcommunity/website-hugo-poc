---
layout: "image"
title: "PneumaCube ohne Elektronic"
date: "2009-05-17T23:35:27"
picture: "2009-PneumaCube-ohne-Elektronic_008.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24049
- /detailsd0df-2.html
imported:
- "2019"
_4images_image_id: "24049"
_4images_cat_id: "1649"
_4images_user_id: "22"
_4images_image_date: "2009-05-17T23:35:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24049 -->
