---
layout: "image"
title: "Zylinderansicht"
date: "2010-02-08T23:27:50"
picture: "funktionierenderzylindermotor04.jpg"
weight: "4"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- /php/details/26238
- /details52f3.html
imported:
- "2019"
_4images_image_id: "26238"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:27:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26238 -->
Version 1