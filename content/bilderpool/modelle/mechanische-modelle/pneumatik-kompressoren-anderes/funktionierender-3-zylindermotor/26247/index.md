---
layout: "image"
title: "Die neue Version..."
date: "2010-02-08T23:28:06"
picture: "funktionierenderzylindermotor13.jpg"
weight: "13"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- /php/details/26247
- /detailsb613-3.html
imported:
- "2019"
_4images_image_id: "26247"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:28:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26247 -->
etwas kompakter