---
layout: "image"
title: "ein kleiner Film..."
date: "2010-02-08T23:28:06"
picture: "funktionierenderzylindermotor12.jpg"
weight: "12"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- /php/details/26246
- /detailscfbf.html
imported:
- "2019"
_4images_image_id: "26246"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:28:06"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26246 -->
