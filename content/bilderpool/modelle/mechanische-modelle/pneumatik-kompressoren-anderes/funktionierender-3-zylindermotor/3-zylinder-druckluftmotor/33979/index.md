---
layout: "image"
title: "3-Zylinder Druckluftmotor"
date: "2012-01-21T14:02:48"
picture: "zylinderdruckluftmotor2.jpg"
weight: "2"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33979
- /detailsfb43.html
imported:
- "2019"
_4images_image_id: "33979"
_4images_cat_id: "2515"
_4images_user_id: "1361"
_4images_image_date: "2012-01-21T14:02:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33979 -->
Detailansicht der Kurbelwelle