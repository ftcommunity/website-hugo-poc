---
layout: "image"
title: "Der Antrieb"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine07.jpg"
weight: "7"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33142
- /details268d.html
imported:
- "2019"
_4images_image_id: "33142"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33142 -->
Dieser Motor treibt das Förderband an.
