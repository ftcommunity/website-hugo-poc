---
layout: "image"
title: "Druckabschaltung und Magnetventile"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine06.jpg"
weight: "6"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33141
- /details913d.html
imported:
- "2019"
_4images_image_id: "33141"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33141 -->
Der Zylinder, welcher direkt mit dem Lufttank verbunden ist, Betätigt den Taster, welcher mit einer Feder verstärkt ist sobald der Betriebsdruck erreicht ist.