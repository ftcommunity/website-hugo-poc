---
layout: "image"
title: "Gesamtansicht"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine01.jpg"
weight: "1"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33136
- /details9570.html
imported:
- "2019"
_4images_image_id: "33136"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33136 -->
Die Anlage in der Gesamtansicht