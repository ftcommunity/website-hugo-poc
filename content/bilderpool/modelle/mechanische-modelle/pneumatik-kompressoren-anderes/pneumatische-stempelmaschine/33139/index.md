---
layout: "image"
title: "Stapelmagazin"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine04.jpg"
weight: "4"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33139
- /details5859.html
imported:
- "2019"
_4images_image_id: "33139"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33139 -->
Der Zylinder hat gerade ein Werkstück auf das Förderband geschoben.