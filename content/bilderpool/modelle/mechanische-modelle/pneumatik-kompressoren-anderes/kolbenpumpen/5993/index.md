---
layout: "image"
title: "Kompressor_neu_2.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00609_resize.jpg"
weight: "2"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5993
- /details3359.html
imported:
- "2019"
_4images_image_id: "5993"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5993 -->
Damit der Luftdruck auch bei Belastung halbwegs aufrechterhalten werden kann, wurden 2 Kompressoren gekoppelt. Die Kolben werden wechselseitig bewegt, was einen gleichmäßigeren Luftstrom und eine geringere Belastung der Motoren ergibt. Die dritte Drehscheibe dient zur Kopplung und als weiteres Schwungelement. Die Seilrollen mussten leicht aufgebohrt werden, die Originalvariante war mir etwas zu breit :)
