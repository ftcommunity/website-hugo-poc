---
layout: "image"
title: "Kompressor_alt_3.jpg"
date: "2006-03-30T07:52:34"
picture: "dsc00619_resize.jpg"
weight: "6"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["kolben", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5997
- /details542d.html
imported:
- "2019"
_4images_image_id: "5997"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5997 -->
Leider sind viele Spezialteile davon nur noch schwer bzw. teuer zu bekommen...
