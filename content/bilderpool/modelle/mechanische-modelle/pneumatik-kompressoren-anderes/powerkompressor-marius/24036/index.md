---
layout: "image"
title: "Kolben"
date: "2009-05-17T17:56:43"
picture: "powerkompressor3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24036
- /detailsf8bb.html
imported:
- "2019"
_4images_image_id: "24036"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24036 -->
Der Kompressor ist ziehmlich laut.