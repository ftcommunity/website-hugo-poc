---
layout: "image"
title: "Fuß zur Schalldämpfung"
date: "2009-05-17T17:57:41"
picture: "powerkompressor7.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24040
- /details05d5.html
imported:
- "2019"
_4images_image_id: "24040"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:57:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24040 -->
Ich habe einfach ein Stück Korken mit doppelseitigem Klebeband auf den Stein geklebt.