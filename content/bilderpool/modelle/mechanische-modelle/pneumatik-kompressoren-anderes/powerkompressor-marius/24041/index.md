---
layout: "image"
title: "Luftspeicher"
date: "2009-05-17T17:57:41"
picture: "powerkompressor8.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24041
- /details6cf2-2.html
imported:
- "2019"
_4images_image_id: "24041"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:57:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24041 -->
Wenn man will kann man den Luftspeicher auch in das Modell einbauen.