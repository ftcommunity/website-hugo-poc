---
layout: "image"
title: "Gesamtansicht"
date: "2009-05-17T17:56:43"
picture: "powerkompressor5.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/24038
- /details8922.html
imported:
- "2019"
_4images_image_id: "24038"
_4images_cat_id: "1648"
_4images_user_id: "845"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24038 -->
