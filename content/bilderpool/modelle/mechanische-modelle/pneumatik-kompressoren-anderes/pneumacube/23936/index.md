---
layout: "image"
title: "1 - PneumaCube geschlossen"
date: "2009-05-08T23:33:19"
picture: "1_-_PneumaCube_geschlossen.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["Pneumatischer", "Würfel"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23936
- /detailsc1a6.html
imported:
- "2019"
_4images_image_id: "23936"
_4images_cat_id: "1643"
_4images_user_id: "724"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23936 -->
