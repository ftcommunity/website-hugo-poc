---
layout: "image"
title: "3 - PneumaCube Ventilsteuerung von oben"
date: "2009-05-08T23:36:37"
picture: "3_-_PneumaCube_Ventilsteuerung_von_oben.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23939
- /detailsd371.html
imported:
- "2019"
_4images_image_id: "23939"
_4images_cat_id: "1643"
_4images_user_id: "724"
_4images_image_date: "2009-05-08T23:36:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23939 -->
5 Motoren steuern 5 Handventile mit RoboPro.
