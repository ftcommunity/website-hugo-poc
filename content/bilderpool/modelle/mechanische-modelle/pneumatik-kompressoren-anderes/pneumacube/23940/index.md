---
layout: "image"
title: "4 - PneumaCube Ventilsteuerung von der Seite"
date: "2009-05-08T23:41:20"
picture: "4_-_PneumaCube_Ventilsteuerung_von_der_Seite.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/23940
- /details5244-2.html
imported:
- "2019"
_4images_image_id: "23940"
_4images_cat_id: "1643"
_4images_user_id: "724"
_4images_image_date: "2009-05-08T23:41:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23940 -->
