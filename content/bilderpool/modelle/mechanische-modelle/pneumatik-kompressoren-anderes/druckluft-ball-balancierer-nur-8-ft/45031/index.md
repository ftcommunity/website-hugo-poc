---
layout: "image"
title: "Taumelscheibe"
date: 2022-07-17T00:16:12+02:00
picture: "2022-07-15_Druckluft-Ball-Balancierer_mit_nur_8 ft-Kompris_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Taumelmechanik ist gegenüber dem letzten Modell verbessert. In der Mitte sitzt ein normales Rast-Differenzial. Die Scheibe oben ist jetzt aber keine ft-Drehscheibe mehr (mit ihren vielen Schlitzen und Löchern), sondern ein der alten Pneumatik-"Schwungscheiben". Die sind auf einer Seite ganz glatt. Das führt zu einem deutlich ruhigeren Lauf mit viel weniger Ruckeln. Ansonsten wäre das Modell nicht dauerbetriebs-zuverlässig - der Luftdurchsatz der 8 Kompressoren würde nicht ausreichen, auch nur kleine Ruckler zuverlässig auszugleichen.