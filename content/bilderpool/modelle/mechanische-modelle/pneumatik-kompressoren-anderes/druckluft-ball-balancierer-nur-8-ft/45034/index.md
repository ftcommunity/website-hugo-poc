---
layout: "image"
title: "Fuß und Kompressoren"
date: 2022-07-17T00:16:17+02:00
picture: "2022-07-15_Druckluft-Ball-Balancierer_mit_nur_8 ft-Kompris_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kompressoren sind an zwei Ringen aus Winkelsteinen 15° und Bausteinen 7,5° befestigt. Sie werden von je einer ft-Feder getragen, damit möglichst wenig Brummgeräusche auf eine Tischplatte übertragen werden.

In der Box 1000 liegt eine mehrfach gefaltete Tischdecke. Die dämmt nicht nur den Schall noch mehr, sondern sie dient vor allem dazu, einen ggf. herabfallenden Ball sanft aufzufangen. Ansonsten könnte der auf dem Boden der Box bzw. der Tischplatte abspringen und sich wer weiß wo hin verkrümeln. Einmal am Tisch wackeln genügt nämlich, um den Ball zu Fall zu bringen.