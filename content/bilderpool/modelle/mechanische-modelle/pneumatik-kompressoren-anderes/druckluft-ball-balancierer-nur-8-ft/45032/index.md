---
layout: "image"
title: "Drucktanks"
date: 2022-07-17T00:16:14+02:00
picture: "2022-07-15_Druckluft-Ball-Balancierer_mit_nur_8 ft-Kompris_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Über den Kompressoren sind vier Drucklufttanks so angebracht, dass sie immer zwischen zwei Kompressoren stehen. Jeder Tank führt zu einer der guten alten Pneumatik-Düsen ganz oben.