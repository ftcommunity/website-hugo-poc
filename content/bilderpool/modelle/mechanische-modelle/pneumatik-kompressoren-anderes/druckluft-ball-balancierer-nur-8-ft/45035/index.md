---
layout: "image"
title: "Gesamtansicht"
date: 2022-07-17T00:16:18+02:00
picture: "2022-07-15_Druckluft-Ball-Balancierer_mit_nur_8 ft-Kompris_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Anders als das vorherige Modell, bei dem ich alle 11 verfügbaren ft-Kompressoren (aka "brute force" ;-) verbaut hatte, läuft diese Variante mit "nur" acht davon. Das reicht gerade so.

Obenauf tanzt die kleine grüne Kunststoff-Hohlkugel aus dem Robotics-Smarttech-Kasten einfach so ohne weitere Regelung oder Steuerung auf dem Luftstrahl. Die Mechanik "taumelt" die Düsen langsam herum; der Ball folgt dem.

Ein Video gibt's unter https://youtu.be/_dpZifJCJDM