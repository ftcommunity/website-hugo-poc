---
layout: "image"
title: "Schalter und Ballvorrat"
date: 2022-07-17T00:16:15+02:00
picture: "2022-07-15_Druckluft-Ball-Balancierer_mit_nur_8 ft-Kompris_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Je zwei Kompressoren hängen an einem Netzteil. Deshalb gibt es vier Taster als getrennte, aber gemeinsam betätigte Ein-/Ausschalter. Der Batteriehalter links dient als Behälter für die insgesamt 3 Bälle, dich ich habe.