---
layout: "image"
title: "Der Moment der Ballübergabe"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42032
- /details4c2d.html
imported:
- "2019"
_4images_image_id: "42032"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42032 -->
Der ankommende Luftstrahl - in diesem Bild der von der linken Düse - muss gerade im richtigen Moment schlagartig genug abgedeckt werden. Geschieht das zu früh, fällt der Ball einfach herunter. Geschieht es zu spät, würden die beiden Luftstrahlen den Ball wegpusten, und wieder fiele er.

Um diesen Zeitpunkt genau einstellen zu können, sieht man auf dem linken Düsenträger auf dem Winkelstein 30° Haralds Lieblingsteil. Dasselbe gibt es auch rechts, aber auf der Convention 2015, auf der dieses Bild entstand, ergab es sich, dass die Maschine mit nur einer der beiden "Luftleitbleche" besser funktionierte. Das hängt vom Luftzug und vielen anderen Faktoren ab und muss also je nach Standort und Tageslaune nachjustiert werden.
