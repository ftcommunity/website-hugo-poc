---
layout: "image"
title: "Mitnehmer-Mechanik"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42035
- /detailsdc09-2.html
imported:
- "2019"
_4images_image_id: "42035"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42035 -->
Die tatsächlichen Drehachsen der Düsen sind um 15° gegenüber der Senkrechten gedreht - warum, sieht man auf den weiteren Bildern noch. Beide Düsen verfügen über die hier sichtbare Mechanik, und ein simples Haushaltsgummi ist zwischen ihnen gespannt, um das Zurückschnalzen zu realisieren.

Der Stift im Z30 nimmt den Eckstein mit und dreht also die obere Achse gegen die Kraft des gespannten Gummis. Die Düse obendrauf bewegt sich deshalb.
