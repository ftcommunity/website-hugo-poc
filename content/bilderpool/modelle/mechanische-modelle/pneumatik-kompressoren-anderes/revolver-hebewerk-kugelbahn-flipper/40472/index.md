---
layout: "image"
title: "Hub, Verriegelung"
date: "2015-02-08T12:54:27"
picture: "IMG_0070.jpg"
weight: "15"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40472
- /detailsdc85.html
imported:
- "2019"
_4images_image_id: "40472"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40472 -->
zur Verdeutlichung habe ich hier die Einlass-Flex-Schiene abgenommen
