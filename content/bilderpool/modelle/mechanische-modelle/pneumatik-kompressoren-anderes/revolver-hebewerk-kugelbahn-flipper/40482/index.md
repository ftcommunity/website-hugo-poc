---
layout: "image"
title: "Schritt 4"
date: "2015-02-08T12:54:27"
picture: "IMG_0074.jpg"
weight: "19"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40482
- /details38df.html
imported:
- "2019"
_4images_image_id: "40482"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40482 -->
Die Kugel hat den Blockiermechanismus passiert und kann nicht mehr herunterfallen, der Zylinder fährt zurück in Ausgangslage
