---
layout: "image"
title: "Auslass"
date: "2015-02-08T12:54:27"
picture: "IMG_0051.jpg"
weight: "10"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40467
- /detailsa96a-2.html
imported:
- "2019"
_4images_image_id: "40467"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40467 -->
hier oben kommen die Kugeln raus. (immer jeweils die oberste Kugel der Säule).
Die Glas- Steine sorgen dafür, daß man den Auswurf gut sieht und dass die Kugeln nicht herausfallen, sondern sicher auf der Flex-Schiene landen
