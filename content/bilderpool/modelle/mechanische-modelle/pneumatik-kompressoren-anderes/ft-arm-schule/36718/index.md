---
layout: "image"
title: "ft-Arm 'Muskeln' und Gelenk"
date: "2013-03-07T13:30:37"
picture: "bild06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36718
- /details0d13.html
imported:
- "2019"
_4images_image_id: "36718"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36718 -->
Detailaufnahme der "Muskeln" (P-Zylinder), die beide ziehend wirken, so wie die menschlichen Muskeln. Auf dem nächsten Bild sieht man das Gelenk noch deutlicher.