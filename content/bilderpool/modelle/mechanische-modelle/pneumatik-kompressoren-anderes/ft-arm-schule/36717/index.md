---
layout: "image"
title: "ft-Arm mit Hand 2"
date: "2013-03-07T13:30:37"
picture: "bild05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36717
- /details096d.html
imported:
- "2019"
_4images_image_id: "36717"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36717 -->
Mit Hand in der oberen Stellung.