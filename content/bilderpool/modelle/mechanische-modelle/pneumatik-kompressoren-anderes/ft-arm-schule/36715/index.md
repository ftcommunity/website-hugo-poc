---
layout: "image"
title: "ft-Arm Stellung oben"
date: "2013-03-07T13:30:37"
picture: "bild03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36715
- /detailsab0f.html
imported:
- "2019"
_4images_image_id: "36715"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36715 -->
Ohne Hand in der oberen Stellung.