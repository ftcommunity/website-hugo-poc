---
layout: "image"
title: "Gesamtbild"
date: "2008-04-27T13:35:53"
picture: "kompressor4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14389
- /details514e.html
imported:
- "2019"
_4images_image_id: "14389"
_4images_cat_id: "1331"
_4images_user_id: "747"
_4images_image_date: "2008-04-27T13:35:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14389 -->
Das ist ein Gesamtbild meines Kompressors. Auf meiner Homepage ist dazu eine Anleitung.