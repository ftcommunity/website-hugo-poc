---
layout: "image"
title: "Übertragung"
date: "2008-04-27T13:35:53"
picture: "kompressor2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14387
- /details71cf.html
imported:
- "2019"
_4images_image_id: "14387"
_4images_cat_id: "1331"
_4images_user_id: "747"
_4images_image_date: "2008-04-27T13:35:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14387 -->
Hier sieht man die Übertragung vom Motor zum Zylinder.