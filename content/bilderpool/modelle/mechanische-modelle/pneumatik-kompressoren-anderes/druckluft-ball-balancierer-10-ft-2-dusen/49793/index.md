---
layout: "image"
title: "Kompressoren von vorne"
date: 2023-03-30T21:40:59+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Drei Netzteile werden verwendet um die drei Kompressoren links vorne, recht vorne sowie die vier ganz hinten zu versorgen. Dadurch bekommt jeder Kompressor noch volle 9 V und kann die benötigte Leistung bringen. Die drei Kontroll-LEDs zeigen an, ob überall Strom anliegt - die silbrigen Buchsen hier haben nämlich Wackelkontakte, und bei Ausfall einer Kompressorgruppe genügt schon die Druckluft nicht mehr, um die Styroporkugel in der Luft zu halten.