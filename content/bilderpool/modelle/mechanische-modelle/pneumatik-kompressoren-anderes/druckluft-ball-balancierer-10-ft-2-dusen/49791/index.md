---
layout: "image"
title: "8/2-Wegeventil"
date: 2023-03-30T21:40:56+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von unten kommen die vier Druckluftschläuche durch die Nuten in der Bauplatte 500. Sie kommen von oben in dieses Ventil, können auf der Unterseite entweder zu-geknickt oder geöffnet sein und gehen dann wieder nach oben zu den Düsen.

Das ist die "Schlauch-Logik", die auch schon mehrfach in ft:pedia-Artikel verwendet wurde. Der Schieber in der Mitte kann von der rechts exzentrisch gelagerten gelben I-Strebe 60 nach links oder nach rechts verschoben werden. Hier sieht man das rechte Schlauchpaar zugeknickt und das linke offen. Das linke Düsenpaar führt also momentan Druckluft.