---
layout: "image"
title: "Ventil - Rechte Düsen aktiv"
date: 2023-03-30T21:41:02+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist die andere Ventilposition. Exzenter und damit der Schlitten stehen links, das linke Schlauchpaar ist verschlossen, das rechte offen. Das rechte Düsenpaar trägt also gerade den Ball.