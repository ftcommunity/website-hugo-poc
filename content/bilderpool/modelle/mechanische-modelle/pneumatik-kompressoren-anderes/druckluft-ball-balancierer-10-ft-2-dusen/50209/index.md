---
layout: "image"
title: "Convention-Fangkorb (1)"
date: 2023-12-11T21:10:40+01:00
picture: "Convention-Fangkorb_1.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf Conventions gibt es immer wieder interessierte Leute, vor allem erfreulich neugierige Kinder, die ins Modell fassen und den Ball fangen oder auf dem Luftstrahl ablegen wollen. Damit ich nicht ständig Bällen hinterher laufen muss, die dann doch gerade von jemandem zertreten wurden, wurde das Modell um einen Ball-Fangkorb ergänzt.