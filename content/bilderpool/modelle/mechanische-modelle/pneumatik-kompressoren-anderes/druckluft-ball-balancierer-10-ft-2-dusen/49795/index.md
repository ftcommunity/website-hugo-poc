---
layout: "image"
title: "Rückansicht"
date: 2023-03-30T21:41:01+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das senkrecht stehende Z40 wird vom zentral liegenden XM-Motor via Schnecke angetrieben und geht seitlich auf die Schnecken, die zu den je zwei variablen Getrieben führen.