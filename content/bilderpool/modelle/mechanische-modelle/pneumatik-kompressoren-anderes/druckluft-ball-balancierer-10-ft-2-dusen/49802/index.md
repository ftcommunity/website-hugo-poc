---
layout: "image"
title: "Getriebestrang auf der rechten Seite"
date: 2023-03-30T21:41:09+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Getriebestrang funktioniert genauso wie der auf der gegenüberliegenden Seite.

Unten links auf der langen Achse des XM-Motors rechts sieht man die Schaltscheibe, die den vorhin schon erwähnten ft-Taster betätigt.