---
layout: "image"
title: "Rechte Seite"
date: 2023-03-30T21:41:04+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht im Vordergrund - von rechts nach links gesehen:

- Ganz rechts das große, senkrecht stehende Z40. Es wird von einem XM-Motor mit Schnecke permanent und immer gleich schnell drehend angetrieben.

- Von dessen Achse geht es vorne und hinten (hier verdeckt) auf eine weitere Schnecke.

- Ab hier folgen zwei kaskadierte zyklisch ungleichförmig übersetzende Getriebe (siehe https://ftcommunity.de/ftpedia/2022/2022-4/ftpedia-2022-4.pdf#page=12 und https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/getriebe/zyklisch-variable-getriebe/gallery-index/ und https://youtu.be/DtUbYV0oiWE).

- Vorne schließlich dreht sich das linke Z40, oder es ruht, während das des hinteren Getriebestrangs dreht.

- Darauf ist eine taumelnde Scheibe montiert, die die Düsen in einer weiten Schwenkbewegung einmal im Kreis führen.

- Darauf ist das jeweilige Düsenpaar montiert, dessen ausgestoßener Luftstrom die Styropor-Kugel trägt.