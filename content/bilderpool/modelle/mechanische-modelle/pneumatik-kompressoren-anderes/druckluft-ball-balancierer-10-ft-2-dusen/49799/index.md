---
layout: "image"
title: "Ventilsteuerung"
date: 2023-03-30T21:41:06+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das E-Tec steuert über den Taster den zweiten XM-Motor so an, dass er immer eine halbe Umdrehung vollführt, bevor er wieder anhält. In jeder Halbdrehung wird also der Taster von der Schaltscheibe entweder gerade gedrückt oder gerade freigegeben.