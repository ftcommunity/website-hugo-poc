---
layout: "image"
title: "Der gesteuerte XM-Motor"
date: 2023-03-30T21:41:14+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XM-Motor im Vordergrund läuft - anders als der zentrale im Hintergrund - nicht die ganze Zeit durch. Er wird über die Kombination von E-Tec und Taster gesteuert. Letztlich dreht er sich, wenn er sich denn dreht, immer in dieselbe Richtung, aber wegen des Tasters und einer Schaltscheibe auf der Achse des Motors immer nur um eine halbe Umdrehung.