---
layout: "image"
title: "Getriebestrang auf der linken Seite"
date: 2023-03-30T21:41:12+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zweimal hintereinander geschaltet gibt es immer die Kombination eines exzentrisch gedrehten Z20 (links oben und mittig unten im Bild), gefolgt von einem beweglich gelagerten Zwischen-Z20 und einem auf einer fest gelagerten Achse drehenden letzten Z20. Das rechte oben führt also zur zweiten Exzenter-Achse unten.

Die letzte Achse trägt das Z40, dass schließlich das Z40 und die Drehscheibe der Taumelmechanik antreibt.