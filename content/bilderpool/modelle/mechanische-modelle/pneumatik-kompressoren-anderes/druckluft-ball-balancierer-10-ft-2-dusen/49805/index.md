---
layout: "image"
title: "Blick von hinten in die Getriebe"
date: 2023-03-30T21:41:13+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht die völlig gleich aufgebauten, aber gegeneinander um 180° verdreht justierten Getriebestränge.