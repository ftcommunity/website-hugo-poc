---
layout: "image"
title: "Linke Seite"
date: 2023-03-30T21:41:00+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf dieser Seite liegt ein genau gleich aufgebauter Getriebestrang wie auf der rechten.