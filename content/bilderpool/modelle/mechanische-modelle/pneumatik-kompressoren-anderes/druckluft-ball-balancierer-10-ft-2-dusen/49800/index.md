---
layout: "image"
title: "Beide Taumelscheiben und die Düsen"
date: 2023-03-30T21:41:07+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn die Taumelscheiben zueinander zeigen, stehen sie beide still. Das ist die Zeit, in der die bisher Druckluft führende Düse den Ball durch Umschalten der Druckluft auf die andere Düse dieser "übergibt".

Damit das funktioniert, muss die Druckluft *schlagartig* aufs andere Düsenpaar umgeleitet werden. Würde der Schaltvorgang zu lange dauern, würde der Ball bereits herunterfallen, bevor der Luftstrom der anderen Düse ihn auffangen kann. Im Video sieht man auch gut, dass der Ball sehr wohl schon fällt - aber eben noch sicher rechtzeitig von der anderen Düse aufgefangen und weiter getragen wird.