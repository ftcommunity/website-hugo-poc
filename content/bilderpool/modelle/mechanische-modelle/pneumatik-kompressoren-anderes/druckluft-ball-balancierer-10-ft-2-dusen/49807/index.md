---
layout: "image"
title: "Blick von oben"
date: 2023-03-30T21:41:15+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Mitte sitzt der XM-Motor, der das senkrecht stehende Z40 (rechts im Bild) via Schnecke antreibt. Links davon sitzt ein E-Tec-Modul in seinem "Basisprogramm" (alle DIP-Schalter nach unten). Die Reed-Kontakte gehen auf I1 bzw. I2, sodass der Motor-Ausgang dadurch umgepolt wird.

Die Getriebe sind sozusagen um 180° verdreht gegeneinander justiert. Beendet eines gerade die Kreisbewegung der Taumelscheibe, steht die andere kurz davon, ihre Bewegung zu beginnen. Die jeweiligen Reed-Kontakte werden ausgelöst, wenn eine Umdrehung vollendet ist. Das schaltet das E-Tec um. Dessen beide Motorausgänge gehen auf Arbeits- und Ruhekontakt des senkrecht stehenden Tasters (im Bild unterhalb des E-Tecs zu sehen). Der Zentralkontakt des Tasters geht zum ganz rechts (hier nicht gut sichtbaren) XM-Motor, sein andere Pol einfach nach "-" der Stromversorgung.

In einem späteren Bild sieht man noch, wozu genau das dient.