---
layout: "image"
title: "Zentraler Antriebsmotor"
date: 2023-03-30T21:40:54+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser XM-Motor treibt über die schon genannte Schnecke das erste Z40 an, das dann seitlich zu den beiden Getriebesträngen weiterführt.