---
layout: "image"
title: "Ventil - Linke Düsen aktiv"
date: 2023-03-30T21:41:05+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das von der Steuerachse in Halbdrehungen angetriebene Exzenter rechts im Bild zeigt gerade nach außen. Der auf einer K-Achse bewegliche Schlitten in der Mitte des Ventils steht deshalb auch ganz rechts. Deshalb werden die von rechts ankommenden Druckluftschläuche abgeknickt und zuverlässig verschlossen - das rechte Düsenpaar führt gerade keine Druckluft.

Das linke Schlauchpaar hingegen hat Platz genug, damit sich die Schläuche "entfalten" und Druckluft mit hinreichend großem Durchfluss durchlassen. Das linke Düsenpaar führt also gerade Druckluft.