---
layout: "overview"
title: "Druckluft-Ball-Balancierer mit 10 ft-Kompressoren und 2 beweglichen Düsen"
date: 2023-03-30T21:40:54+02:00
---

Dieses Modell balanciert eine ca. 1 g leichte Styropor-Kugel in der Luft. Sie wird zwischen zwei Düsen "übergeben", ohne jemals etwas anderes als Luft zu berühren. Die Steuerung besteht aus vier zyklisch variabel übersetzenden Getrieben, zwei Taumelschreiben, zwei Magnet/Reedkontakt-Paaren, einem E-Tec-Modul und einem selbstgebauten 8/2-Wege-Ventil in Schlauch-Logik.