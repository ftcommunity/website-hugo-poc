---
layout: "image"
title: "Taumelscheibe der linken Seite"
date: 2023-03-30T21:41:11+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ein Detailblick in die so angetriebene Taumelmechanik. Man beachte, dass die Achse, um die sich die Drehscheibe dreht, fest angebracht ist und sich *nicht* dreht. Die Drehscheibe dreht sich mit ihrer Freilaufnabe nur um sie herum. Auch das Kardan wird nur ausgelenkt, dreht sich aber nicht, ebenso wenig wie die Bausteingruppe oben dran. Das führt zu einer Taumelbewegung der Düse darüber.