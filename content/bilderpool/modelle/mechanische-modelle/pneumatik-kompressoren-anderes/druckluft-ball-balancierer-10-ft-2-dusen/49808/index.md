---
layout: "image"
title: "Frontansicht"
date: 2023-03-30T21:41:17+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zwei Bauplatten 500 sind übereinander montiert. Die untere trägt zehn fischertechnik-Kompressoren, die alle in einem gemeinsamen Druckverteiler münden. Die obere trägt zwei Stränge doppelt ausgeführter zyklisch ungleichförmig übersetzender Getriebe, die je eine Taumelscheibe antreiben. Dreht sich eine, steht die andere still - nur durch die Getriebe.

Die Taumelscheiben tragen je zwei fischertechnik-Pneumatik-Düsen. Die Druckluft aller 10 Kompressoren, auf eines der Düsenpaare gegeben, reicht *nicht* aus, um damit einen Tischtennisball zu tragen, der ca. 2 g wiegt. Aber sie genügt zum Tragen einer Styroporkugel, die nur ca. 1 g wiegt.

Da die Luft nicht ausreicht, um beide Düsenpaare gleichzeitig zu betreiben und den Ball immer noch zu tragen, muss zwischen den beiden Düsen umgeschaltet werden. Das macht die Schlauch-Abknick-Ventilkonstruktion auf der Frontseite der oberen Bauplatte 500.

Ein Video gibt's unter https://youtu.be/xVMt7Vs7j1s