---
layout: "image"
title: "Kompressoren von der Seite"
date: 2023-03-30T21:40:58+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die vier nach oben laufenden Druckluftschläuche sind lang genug bemessen, dass man die obere Bauplatte abnehmen und nebendran legen kann. So kommt man an die Stromversorgung und Verschlauchung der Kompressoren heran.

Der Druckluftverteiler in der Mitte besteht aus zwei der Ur-Pneumatik-Druckverteiler mit einem dazugehörigen Schlauchstück dazwischen. Jeder Verteiler hat 8 Schlauchanschlüsse, insgesamt gibt es also 16. 10 werden von den Kompressoren verwendet, 4 für die ins Obergeschoss verlaufenden Schläuche, und die zwei übrigen sind einfach mit einem Stückchen Schlauch miteinander verbunden - das spart P-Stopfen.