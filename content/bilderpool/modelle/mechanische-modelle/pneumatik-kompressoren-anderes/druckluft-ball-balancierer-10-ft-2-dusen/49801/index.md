---
layout: "image"
title: "Taumelmechanik der rechten Seite"
date: 2023-03-30T21:41:08+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht auch gut den Magneten auf der Drehscheibe und den fest angebrachten Reedkontakt, der damit das Ende einer Umdrehung feststellt und dem E-Tec-Modul meldet.