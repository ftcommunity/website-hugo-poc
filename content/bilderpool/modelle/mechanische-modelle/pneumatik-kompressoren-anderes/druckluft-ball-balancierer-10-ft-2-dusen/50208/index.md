---
layout: "image"
title: "Convention-Fangkorb (2)"
date: 2023-12-11T21:10:39+01:00
picture: "Convention-Fangkorb_2.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Korb ist leicht abnehmbar. Insgesamt hat die Maschine damit brav mehrere Ausstellungen durchgehalten.