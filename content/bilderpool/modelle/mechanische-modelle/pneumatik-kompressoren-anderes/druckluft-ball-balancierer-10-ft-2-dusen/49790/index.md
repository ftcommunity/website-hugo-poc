---
layout: "image"
title: "Ventil, Taumelscheiben und Düsen"
date: 2023-03-30T21:40:55+02:00
picture: "2023-03-12_Druckluft-Ball-Balancierer_10_Kompressoren_2_Duesen08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Taumelscheiben sind ähnlich aufgebaut wie in https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/pneumatik-kompressoren-anderes/druckluft-ball-balancierer-nur-8-ft/45035/ bzw. https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/druckluft-ball-balancierer-nur-ft/gallery-index/. Z40 und Drehscheibe sitzen via (schwarzer) Freilauf-Nabe auf einer feststehenden (!) Achse - einer "Rastachse mit Platte". Die Achse dreht sich also *nicht*. Nach oben trägt sie ein Rastkardangelenk, das in einer weiteren Rastachse mit Platte endet. Auf der ist eine alte Pneumatik-Schwungscheibe montiert, die von den drei Rädern darunter - die sich ja mit der Drehscheibe um die Achse drehen - in verschiedene Richtungen gebracht. Die Bausteine obendrauf drehen sich aber nicht um ihre eigene Achse (sonst würden sich die Schläuche ja aufwickeln), sondern "taumeln" nur.

Die Drehscheiben tragen je einen Neodym-Magnet (10 mm lang), der die Reed-Kontakte ganz links und rechts außen (die senkrecht stehenden schwarzen Röhrchen) ansteuern.