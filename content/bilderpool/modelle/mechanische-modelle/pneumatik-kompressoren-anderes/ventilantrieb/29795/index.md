---
layout: "image"
title: "Ventilantrieb (5)"
date: "2011-01-24T19:23:40"
picture: "ventilantrieb5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29795
- /details6353.html
imported:
- "2019"
_4images_image_id: "29795"
_4images_cat_id: "2190"
_4images_user_id: "729"
_4images_image_date: "2011-01-24T19:23:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29795 -->
Detailansicht
S-Motor mit Getriebebock 31069