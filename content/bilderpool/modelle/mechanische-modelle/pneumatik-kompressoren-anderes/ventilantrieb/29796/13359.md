---
layout: "comment"
hidden: true
title: "13359"
date: "2011-01-25T07:29:00"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Was fehlt denn dem Getriebe? Normalerweise sammeln die nur etwas Staub und Luftfeuchtigkeit ein und werden dann schwergängig. Das lässt sich durch Zerlegen und Putzen wieder richten: mit einem abgefeilten (d.h.: stumpfen) Nagel und einem kleinen Hämmerchen die Achse herausklopfen, säubern, wieder reinklopfen. Dabei vorsichtig dosieren, damit das Zahnrad wieder hinreichend viel Spiel auf der Rändelachse nach links und rechts hat.

Alternative: Achse mit Schnecke rausklopfen und in ein 4mm-Messingrohr mit passendem Innendurchmesser einkleben. Für das Zahnrad findet sich auch noch was. So ein "abgesetzt betreibbarer S-Motor" steht bei mir schon länger auf der Wunschliste. Ich habe aber just erst letzte Woche ein paar Solo-Schnecken bestellt; die kompletten Getriebe werden alle gebraucht.

Gruß,
Harald