---
layout: "image"
title: "Pelamis"
date: "2007-10-25T18:14:17"
picture: "Pelamis_0011.jpg"
weight: "1"
konstrukteure: 
- "Paulchen"
fotografen:
- "Paulchen"
schlagworte: ["Pelamis", "Seeschlange"]
uploadBy: "Paulchen"
license: "unknown"
legacy_id:
- /php/details/12310
- /detailsc0e3.html
imported:
- "2019"
_4images_image_id: "12310"
_4images_cat_id: "613"
_4images_user_id: "599"
_4images_image_date: "2007-10-25T18:14:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12310 -->
Erzeugt Strom durch Wellenenergie