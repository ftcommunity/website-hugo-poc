---
layout: "overview"
title: "Kompressor (uhen)"
date: 2020-02-22T08:14:54+01:00
legacy_id:
- /php/categories/1926
- /categories30ca.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1926 --> 
Ich habe hier einen Standard Motor modifiziert. Ich habe die Schnecke vorsichtig mit einer Mini-Trennscheibe gelöst und durch ein Zahnrad ersetzt. Die Kraftübertragung ist beeindruckend. Die Luftmenge reicht aus, um ein \"Zylinder Kolben Pneumatik Motor\" zu betreiben.