---
layout: "image"
title: "Antrieb"
date: "2012-10-10T15:13:52"
picture: "luft6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/35875
- /details7d40.html
imported:
- "2019"
_4images_image_id: "35875"
_4images_cat_id: "2679"
_4images_user_id: "453"
_4images_image_date: "2012-10-10T15:13:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35875 -->
