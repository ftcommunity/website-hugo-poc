---
layout: "image"
title: "Lage des Drehtisches"
date: "2010-10-06T17:21:06"
picture: "Lage.jpg"
weight: "7"
konstrukteure: 
- "werner"
fotografen:
- "werner"
schlagworte: ["Drehtisch"]
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/28936
- /details1053.html
imported:
- "2019"
_4images_image_id: "28936"
_4images_cat_id: "613"
_4images_user_id: "1196"
_4images_image_date: "2010-10-06T17:21:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28936 -->
Zunächst nur zum Verlinken