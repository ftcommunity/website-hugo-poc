---
layout: "image"
title: "Ventilinsel (2)"
date: "2011-01-22T16:48:49"
picture: "ventilinselfrankjakob2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29741
- /details4a7c-2.html
imported:
- "2019"
_4images_image_id: "29741"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29741 -->
Ansicht von vorne rechts