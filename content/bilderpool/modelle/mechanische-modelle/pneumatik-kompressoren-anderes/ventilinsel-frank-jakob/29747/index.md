---
layout: "image"
title: "Ventilinsel (8)"
date: "2011-01-22T16:48:49"
picture: "ventilinselfrankjakob8.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29747
- /details9dc4.html
imported:
- "2019"
_4images_image_id: "29747"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29747 -->
Zu Beginn werden die Ventile in Mittelstellung gebracht, dann werden die Lufttanks gefüllt.
Anschließend lassen sich die Ventile am Bildschirm oder über Taster steuern.
