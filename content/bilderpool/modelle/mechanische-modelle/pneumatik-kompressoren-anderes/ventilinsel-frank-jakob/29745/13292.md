---
layout: "comment"
hidden: true
title: "13292"
date: "2011-01-22T17:56:28"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Frank,
Es gibt zwei weitere Möglichkeiten dieses zu verhindern.
1.) links und rechts des Reedkontakthalters je eine Federnocke aufschieben.
2.) die alte Variante des BS 7,5 (ohne die seitlichen Nuten) verwenden.

Gruß Ludger