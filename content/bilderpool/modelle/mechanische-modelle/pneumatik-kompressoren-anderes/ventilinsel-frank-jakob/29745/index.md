---
layout: "image"
title: "Ventilinsel (6)"
date: "2011-01-22T16:48:49"
picture: "ventilinselfrankjakob6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29745
- /detailsb7dc-2.html
imported:
- "2019"
_4images_image_id: "29745"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29745 -->
Leider verschiebt sich hier öfter mal der BS7.5 und der Reedkontakthalter. Etwas Heißkleber
könnte hier helfen, ist aber nichts für ft-Puristen :-)