---
layout: "image"
title: "Der Zylinder"
date: "2010-10-25T19:15:04"
picture: "pkomp4.jpg"
weight: "4"
konstrukteure: 
- "Jan Hils"
fotografen:
- "Jan Hils"
uploadBy: "ft-Jan"
license: "unknown"
legacy_id:
- /php/details/29054
- /detailse3de-2.html
imported:
- "2019"
_4images_image_id: "29054"
_4images_cat_id: "2111"
_4images_user_id: "1181"
_4images_image_date: "2010-10-25T19:15:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29054 -->
Hier sieht man nochmal die Aufhängung des Zylinders