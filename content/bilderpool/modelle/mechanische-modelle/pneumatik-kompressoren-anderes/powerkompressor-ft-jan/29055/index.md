---
layout: "image"
title: "Die Problemzone"
date: "2010-10-25T19:15:04"
picture: "pkomp5.jpg"
weight: "5"
konstrukteure: 
- "Jan Hils"
fotografen:
- "Jan Hils"
uploadBy: "ft-Jan"
license: "unknown"
legacy_id:
- /php/details/29055
- /details447d.html
imported:
- "2019"
_4images_image_id: "29055"
_4images_cat_id: "2111"
_4images_user_id: "1181"
_4images_image_date: "2010-10-25T19:15:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29055 -->
Hier sieht man den Übergang von Zylinder zur Kurbelwelle nochmal im Detail.
Diese Version ist leider sehr Wartungsaufwendig und vielleicht nicht sehr verschleißarm
Ich würde mich über tipps freuen :)