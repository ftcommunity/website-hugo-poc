---
layout: "image"
title: "Kompressorabschaltung"
date: "2006-12-17T16:23:48"
picture: "Kompressorstation4.jpg"
weight: "9"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/7917
- /details7b82.html
imported:
- "2019"
_4images_image_id: "7917"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2006-12-17T16:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7917 -->
Das ist die Kompressorabschaltung. Wenn der Luftspeicher voll ist drückt der Zylinder gegen die Feder an und den Taster ein. Wenn etwas Luft verbraucht wurde, pumpt der Kompressor woeder nach(wegen der Feder).
