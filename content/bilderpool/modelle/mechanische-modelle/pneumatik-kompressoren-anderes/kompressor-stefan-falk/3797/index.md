---
layout: "image"
title: "Kompressor - Nahansicht des Exzenters"
date: "2005-03-13T13:21:34"
picture: "Kompressor_002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3797
- /details9281.html
imported:
- "2019"
_4images_image_id: "3797"
_4images_cat_id: "578"
_4images_user_id: "104"
_4images_image_date: "2005-03-13T13:21:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3797 -->
Das Exzenter ergibt eine Auslenkung, die den Hub der kleinen Kompressorzylinder bis auf einen winzigen Rest komplett ausnutzt. Die überflüssig aussehenden schwarzen Teile dienen zur Fixierung. Ohne diese würde das Exzenter nach kurzer Zeit zu einer sauber fluchtenden Achse ohne Hub zusammengeschoben. Die Verankerung der Zylinder an der Grundplatte braucht tatsächlich die vier Anschlussstelle (sprich: 4 Zapfen greifen in die Grundplatte), sonst fliegen sie einem nach einiger Zeit raus und um die Ohren.