---
layout: "image"
title: "Kompressor"
date: "2007-02-10T23:44:57"
picture: "Kompressor3.jpg"
weight: "17"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8923
- /details2e42.html
imported:
- "2019"
_4images_image_id: "8923"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-02-10T23:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8923 -->
Antrieb der Kurbel. Ich habe einen 8:1 Power-Motor benutzt, mit zusätzlicher 2:1 Untersetzung.
