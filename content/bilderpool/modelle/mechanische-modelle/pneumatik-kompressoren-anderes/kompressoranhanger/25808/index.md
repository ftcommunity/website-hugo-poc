---
layout: "image"
title: "Kompressoranhänger 4"
date: "2009-11-20T17:55:37"
picture: "kompressoranhaenger4.jpg"
weight: "4"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
schlagworte: ["Kompressor", "Pneumatik", "TST", "Betätiger", "Anhänger"]
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25808
- /details5917.html
imported:
- "2019"
_4images_image_id: "25808"
_4images_cat_id: "1810"
_4images_user_id: "1027"
_4images_image_date: "2009-11-20T17:55:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25808 -->
...die "klassische" Druckabschaltung mittels Betätiger.