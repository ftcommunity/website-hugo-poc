---
layout: "image"
title: "Kompressoranhänger 6"
date: "2009-11-20T17:55:38"
picture: "kompressoranhaenger6.jpg"
weight: "6"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
schlagworte: ["Kompressor", "Pneumatik", "TST", "Betätiger", "Anhänger"]
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- /php/details/25810
- /details3b6f.html
imported:
- "2019"
_4images_image_id: "25810"
_4images_cat_id: "1810"
_4images_user_id: "1027"
_4images_image_date: "2009-11-20T17:55:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25810 -->
...die TST-Bauplatte hält den Powermotor dank 3 Schrauben sehr fest, so dass keine weitere Fixierung des Motors erforderlich ist.