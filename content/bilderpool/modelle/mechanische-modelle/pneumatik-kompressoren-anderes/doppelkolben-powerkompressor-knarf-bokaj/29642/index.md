---
layout: "image"
title: "Kompressor PICT5007"
date: "2011-01-09T15:18:50"
picture: "doppelkolbenpowerkompressorknarfbokaj3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29642
- /detailsa64d.html
imported:
- "2019"
_4images_image_id: "29642"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-09T15:18:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29642 -->
Ansicht hinten rechts