---
layout: "comment"
hidden: true
title: "13171"
date: "2011-01-13T07:50:06"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
"Systemfremde Pilotventilfeder"??? Du meinst sicher diesen braunen Haushaltsgummi, oder? Ja, der stört mich auch.

Wenn schon Gummi, dann ein Original-FT-Teil. Der wirklich tolle Kompressor hat's verdient!

Gruß, Thomas