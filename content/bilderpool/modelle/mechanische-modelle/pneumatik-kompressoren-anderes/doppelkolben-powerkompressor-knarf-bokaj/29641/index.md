---
layout: "image"
title: "Kompressor PICT5006"
date: "2011-01-09T15:18:50"
picture: "doppelkolbenpowerkompressorknarfbokaj2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/29641
- /details01a5.html
imported:
- "2019"
_4images_image_id: "29641"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-09T15:18:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29641 -->
Ansicht vorne links