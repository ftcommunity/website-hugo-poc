---
layout: "image"
title: "Funktionsweise (3)"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28762
- /detailsef7c.html
imported:
- "2019"
_4images_image_id: "28762"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28762 -->
Irgendwann muss das Papier in die hier noch nicht existenten nächsten Walzen geraten, die es dann selber weiter ziehen. Der Motor des Schachtes wird dann ausgeschaltet. Das Papier wird trotzdem von außen weitergezogen, und durch den Freilauf können sich die Rollen weiter drehen. In dem Moment, in dem das Papier ganz unter den Rollen weggezogen ist, hört diese Drehung mangels Antrieb auf, und das nächste Blatt Papier wird kein bisschen mit gezogen.
