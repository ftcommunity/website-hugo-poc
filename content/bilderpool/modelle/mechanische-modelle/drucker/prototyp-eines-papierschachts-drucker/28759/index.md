---
layout: "image"
title: "Von unten (1)"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28759
- /details0c8f.html
imported:
- "2019"
_4images_image_id: "28759"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28759 -->
Eine andere Ansicht auf eine der Ecken auf der Papierausgangsseite von unten.
