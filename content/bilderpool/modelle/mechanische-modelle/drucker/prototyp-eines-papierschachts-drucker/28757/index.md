---
layout: "image"
title: "Freilauf"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28757
- /detailsfb12.html
imported:
- "2019"
_4images_image_id: "28757"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28757 -->
Hier nur zum manuellen Probieren angebracht, ist dieser Freilauf ein wichtiges Element des Einzugs. Es ist dieses Modell: http://www.ftcommunity.de/details.php?image_id=27354. Im fertigen Drucker wird dieser Freilauf bestimmt unter dem Fach liegen und nicht so unerwünscht weit herausstehen. Warum der Freilauf so wichtig ist, wird später noch beschrieben.
