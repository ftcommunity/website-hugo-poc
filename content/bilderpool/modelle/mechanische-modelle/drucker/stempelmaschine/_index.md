---
layout: "overview"
title: "Stempelmaschine"
date: 2020-02-22T08:20:11+01:00
legacy_id:
- /php/categories/2536
- /categories1180.html
- /categoriesc23e.html
- /categoriesacce.html
- /categoriesae63-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2536 --> 
Die Stempelmaschine bestempelt handelsübliche Notizzettel der Größe 9 x9 cm. Diese werden in ein Magazin gelegt und von dort einzeln über ein Transportrollensystem der Stempeleinrichtung zugeführt. Sobald der Notizzettel auf dem Stempeltisch liegt, wird dies durch einen Farb- bzw. Helligkeitssensor erkannt, die Stempeleinrichtung in Gang gesetzt und der Notizzettel bestempelt. Hierfür führt der Stempel, der in Ruheposition auf dem Stempelkissen ruht eine Schwenkbewegung zum Notizzettel hin durch. Ein Umschalter sorgt dafür, dass der Stempel dann wieder auf das Stempelkissen geschwenkt wird. Ein Endschalter beendet den Stempelvorgang und sorgt gleichzeitig dafür, dass der bestempelte Zettel ins Ablagefach transportiert wird. Der Farbsensor über dem Stempeltisch erkennt nun am veränderten Untergrund, dass der Stempeltisch wieder frei ist, und sorgt nun dafür, dass der nächste Zettel auf den Weg geschickt wird. Sobald sich kein Zettel mehr im Magazin befindet, sorgt ein zweiter Farbsensor dafür, dass die Maschine abgeschaltet wird.
Beim Umsetzen der Idee einer Stempelmaschine ergaben sich zwei Probleme. Das erste hat mit dem Einzug der Zettel zu tun. Es hat einige Versuche gekostet, einen Einzug zu konstruieren, der garantiert immer nur einen Zettel auf den Weg befördert. Versuche mit einem ft-Vakuumsauger waren nicht zielführend, weil das Papier der Notizzettel so porös ist, dass immer gleich zwei oder drei Zettel angesaugt wurden. Auch der Einzug über Gummirollen machte zunächst Probleme, weil hier keine Einzelblätter, sondern immer gleich ganze Zettelpakete eingezogen wurden. Einzelblätter einzuziehen gelang aber dadurch, dass das Magazin und damit die Notizzettel fast senkrecht gestellt wurden und die Gummirollen des Einzugmotors mit einem ganz geringen Druck auf die Zettel einwirken. Dieser äußerst geringe Druck wird dadurch gewährleistet, dass sich das Magazin mit den darin befindlichen Zetteln nur ganz leicht an die Einzugsrollen anlehnt. Trotzdem passiert es immer wieder, dass die nachfolgenden Zettel mit angehoben werden und dann irgendwann in das Transportsystem gelangen, so dass dann zwei oder drei Zettel gleichzeitig befördert werden, wobei dann nur der oberste Zettel bestempelt wird. Um dies zu verhindern, wird der Einzugmotor so gesteuert, dass er, sobald der erste Zettel auf den Weg gebracht wurde, eine kurze Rückwärtsdrehung macht, so dass ein eventuell angehobener Folgezettel wieder in die Ausgangsstellung zurück geführt wird.
Das zweite Problem war die Erzeugung des nötigen Stempeldruckes in schneller Folge. Für größere Stempel, die einen recht hohen Druck benötigen, habe ich keine Lösung gefunden, aber mit kleinen Stempeln arbeitet meine Schwenkstempler sehr zuverlässig. Auch das Stempelbild ist sehr deutlich und vollständig.
Ein Video, das die Stempelmaschine in Betrieb zeigt, kann auf YouTube unter "fischertechnik-Stempelmaschine" angesehen werden.