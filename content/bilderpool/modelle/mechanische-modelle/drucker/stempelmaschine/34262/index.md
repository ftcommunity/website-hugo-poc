---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine18.jpg"
weight: "18"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34262
- /details4da3.html
imported:
- "2019"
_4images_image_id: "34262"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34262 -->
Die Stempelmechanik beim Schwenken - kurz vorm Stempeln.