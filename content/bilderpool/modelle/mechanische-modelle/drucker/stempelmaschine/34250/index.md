---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:10"
picture: "stempelmaschine06.jpg"
weight: "6"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34250
- /detailse247.html
imported:
- "2019"
_4images_image_id: "34250"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34250 -->
Einzugsrollen, davor die Notizzettel. Unter den Einzugsrollen befindet sich der Farbsensor, der erkennt, wann das Magazin leer ist und die Maschine abschaltet.