---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:10"
picture: "stempelmaschine08.jpg"
weight: "8"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/34252
- /details6742.html
imported:
- "2019"
_4images_image_id: "34252"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34252 -->
Der Antrieb für dei Transportrollen.