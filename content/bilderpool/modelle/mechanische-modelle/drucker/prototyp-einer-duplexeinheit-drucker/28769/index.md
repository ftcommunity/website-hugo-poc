---
layout: "image"
title: "Wirkungsweise"
date: "2010-09-29T20:02:42"
picture: "prototypeinerduplexeinheitfuereinendrucker03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28769
- /details84b1.html
imported:
- "2019"
_4images_image_id: "28769"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28769 -->
Von (hier) rechts kommt das Papier an (1), und zwar in einer Höhe etwas über der rechten Spitze des dreieckigen Balkens. Es trifft also auf die rechte obere Fläche und wird nach oben abgelenkt. Oben wird es von Walzen wiederum gepackt und aus dem Bereich des Balkens heraus gezogen. Die Geometrie ist so, dass es jetzt etwas links von der oberen Spitze ankommt, wenn es (2) durch Umpolen der Motoren wieder nach unten geschoben wird. Es trifft also auf die linke Fläche und wird nach unten abgelenkt. Dieser Vorgang wiederholt sich dann ein drittes Mal, so dass das Papier wieder nach rechts ausgeworfen wird (3) - jetzt aber mit der Oberseite nach unten gedreht.
