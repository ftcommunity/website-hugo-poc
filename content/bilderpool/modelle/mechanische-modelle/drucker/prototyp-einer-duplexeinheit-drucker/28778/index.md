---
layout: "image"
title: "Blick durch die Einheit ohne Dreiecksbalken"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28778
- /detailsb5ed-2.html
imported:
- "2019"
_4images_image_id: "28778"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28778 -->
Hier ein Blick durch die Einheit bei ausgebautem Dreiecksbalken. In diesem Zustand gäbe es nichts als zerknülltes Papier. ;-)
