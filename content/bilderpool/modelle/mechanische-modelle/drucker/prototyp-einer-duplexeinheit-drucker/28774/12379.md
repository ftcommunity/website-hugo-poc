---
layout: "comment"
hidden: true
title: "12379"
date: "2010-09-29T22:09:58"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

zwei dieser so genannten Planspiegel (Art.-Nr. 31368) waren auch im 
ft-Kasten "ec 3" (Art.-Nr. 30252) zu finden. 
Der "ec 3" war übrigens von 1975 bis 1980 im Handel.

Gruß

Lurchi