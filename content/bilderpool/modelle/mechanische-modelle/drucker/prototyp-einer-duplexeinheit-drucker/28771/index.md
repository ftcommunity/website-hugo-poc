---
layout: "image"
title: "Blick durch den Tunnel"
date: "2010-09-29T20:02:42"
picture: "prototypeinerduplexeinheitfuereinendrucker05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/28771
- /detailsa46f.html
imported:
- "2019"
_4images_image_id: "28771"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28771 -->
Hier blickt man von der Seite durch die Einheit. Der BS15 in Bildmitte trägt zusammen mit seinem Kumpel auf der gegenüberliegenden Seite den dreieckigen Balken. Woanders kann man den nicht aufhängen, weil an allen seinen drei Seiten das Papier entlanggleiten können muss.
