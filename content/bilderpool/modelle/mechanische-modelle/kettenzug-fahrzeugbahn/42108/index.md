---
layout: "image"
title: "Ansicht von oben"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42108
- /details6ed9-2.html
imported:
- "2019"
_4images_image_id: "42108"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42108 -->
Die insgesamt sieben Kettenzüge (vier in den Schrägaufzügen und drei waagerechte in Kurven) müssen synchron laufen, damit sich die Mitnehmer an den Übergabestellen zwischen zwei Kettenzügen nicht ins Gehege kommen. Deshalb werden alle sieben Kettenzüge exakt gleich schnell von einem einzigen Zentralmotor (unten in der Mitte) angetrieben. Aus demselben Grund ist die Anzahl von Kettengliedern zwischen zwei Mitnehmern bei sämtlichen Ketten immer gleich.
