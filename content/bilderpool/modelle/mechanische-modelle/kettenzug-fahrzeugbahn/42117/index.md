---
layout: "image"
title: "Übergang zwischen Kettensegmenten"
date: "2015-10-24T21:46:36"
picture: "kettenzugfahrzeugbahn11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42117
- /details147d.html
imported:
- "2019"
_4images_image_id: "42117"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42117 -->
Diese Übergangsstellen müssen von Kette zu Kette so justiert werden, dass a) die Mitnehmer sich nicht verhaken und b) die Fahrzeuge sauber übergeben werden.
