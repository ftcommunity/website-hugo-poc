---
layout: "image"
title: "Antrieb der zwei übereinanderliegenden Kurven (2)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42116
- /details986b-2.html
imported:
- "2019"
_4images_image_id: "42116"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42116 -->
... und ein Stockwerk höher geht's zurück zum Antriebs-Z20 für die waagerechte Kette darüber.
