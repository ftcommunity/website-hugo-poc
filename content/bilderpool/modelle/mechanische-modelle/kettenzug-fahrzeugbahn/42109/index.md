---
layout: "image"
title: "Antrieb (1)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42109
- /details3520.html
imported:
- "2019"
_4images_image_id: "42109"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42109 -->
Der PowerMotor treibt die beiden waagerechten langen Achsen an. Die laufen sehr schnell; erst direkt vor den Ketten gibt es jeweils eine Schecke/Zahnrad-Kombination, die die hohe Drehgeschwindigkeit in hohes Drehmoment umsetzt. Alle Lager sind mit Vaseline geschmiert, damit das Modell langlauffähig wird.
