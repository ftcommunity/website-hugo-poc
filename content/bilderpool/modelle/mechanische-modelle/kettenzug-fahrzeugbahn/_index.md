---
layout: "overview"
title: "Kettenzug-Fahrzeugbahn"
date: 2020-02-22T08:20:54+01:00
legacy_id:
- /php/categories/3136
- /categoriesf2b9-2.html
- /categories8eb8.html
- /categoriesb24a.html
- /categories9eb8.html
- /categories371c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3136 --> 
Eine Bahn, auf der mehrere Fahrzeuge eine längere Strecke hochgezogen werden und wieder hinabrollen.