---
layout: "image"
title: "Fahrzeug 5 - Betonmischer"
date: "2015-10-24T21:46:36"
picture: "kettenzugfahrzeugbahn16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42122
- /detailsca2d-2.html
imported:
- "2019"
_4images_image_id: "42122"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42122 -->
Das komische Führerhaus hat die Figur fest eingeklebt - ich habe keine Ahnung, ob das Original war oder ob ein Vorbesitzer dieses Gebrauchtteils das gemacht hatte.
