---
layout: "image"
title: "Rundtakttisch - Seite"
date: "2008-01-03T19:07:20"
picture: "rundtakttisch2.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/13220
- /details20ed.html
imported:
- "2019"
_4images_image_id: "13220"
_4images_cat_id: "1196"
_4images_user_id: "1"
_4images_image_date: "2008-01-03T19:07:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13220 -->
Hier der Rundttaktisch von der Stirnseite.
Man kann gut sehen das ich als Grundplatte ne 1000er Platte genommen habe.
Der Drehteller selber ist auch 2 gegeneinander befestigte 500er Platten montiert.
