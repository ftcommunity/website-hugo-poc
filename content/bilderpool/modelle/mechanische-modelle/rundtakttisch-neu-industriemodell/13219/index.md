---
layout: "image"
title: "Rundtakttisch - oben"
date: "2008-01-03T19:07:19"
picture: "rundtakttisch1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/13219
- /details39f7-2.html
imported:
- "2019"
_4images_image_id: "13219"
_4images_cat_id: "1196"
_4images_user_id: "1"
_4images_image_date: "2008-01-03T19:07:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13219 -->
Hier sieht man den neuen Rundtakttisch für mein Industriemodell (bin grade dabei das auf Module umzubauen, ist für Transport besser!).
Bisher hatte ich ja Tischtennisbälle, ab sofort steige ich um auf Felgen.

Später sollen die Felge (besser gesagt die Farben der Felgen) erkannt werden.
Mir schwebt auch vor, die Felgen und die Reifen einzelnd zu zuführen und auf dem Rundtakttisch automatisch zu montieren.
Das braucht allerdings etwas Kraft! Der Tisch ist sehr stabil und würde das aushalten.

Die Befestigung des Powermotors (Kappe abgemacht und in Rohrhüls gepresst!) ist noch etwas schief.
Da muss ich nochmal ran!
