---
layout: "comment"
hidden: true
title: "22538"
date: "2016-09-25T17:14:18"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Rüdiger,
sensationell. Toll. Doch ein richtiger Motor - auch wenn das Drehmoment klein ist.
Nach einem Hinweis von geometer (zu seinen Uhrgetriebe-Lagerungen) kannst Du die Verluste durch Reibung weiter verringern, wenn Du die Bausteine mit Loch durch Kupplungsstücke (wie vorne an der Motorachse) ersetzt.
Beste Grüße,
Dirk