---
layout: "image"
title: "Ansicht von Unten"
date: 2022-03-25T19:38:56+01:00
picture: "Kettenschaltung_Bottomansicht.JPG"
weight: "8"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Werfer von unten mit Parallelogramm-MEchanismus
