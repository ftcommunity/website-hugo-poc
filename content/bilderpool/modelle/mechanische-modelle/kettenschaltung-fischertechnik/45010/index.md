---
layout: "image"
title: "Großes Ritzel"
date: 2022-03-25T19:39:03+01:00
picture: "Kettenschaltung_Top_gross.JPG"
weight: "4"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Kette auf großem Ritzel
