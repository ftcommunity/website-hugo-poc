---
layout: "image"
title: "Schalthebel"
date: 2022-03-25T19:38:59+01:00
picture: "Kettenschaltung-Schalthebel.JPG"
weight: "6"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Schalthebel mit Bowdenzug
