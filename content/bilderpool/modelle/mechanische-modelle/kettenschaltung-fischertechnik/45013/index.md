---
layout: "image"
title: "Kettenschaltung Seitenansicht"
date: 2022-03-25T19:39:07+01:00
picture: "Kettenschaltung-Seitenansicht2.JPG"
weight: "1"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Seitenansicht der 2-Gang ft-Kettenschaltung
