---
layout: "image"
title: "Kettenschaltung Hinteransicht"
date: 2022-03-25T19:39:06+01:00
picture: "Kettenschaltung-Rueckseite.JPG"
weight: "2"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Rückansicht der 2-Gang ft-Kettenschaltung
