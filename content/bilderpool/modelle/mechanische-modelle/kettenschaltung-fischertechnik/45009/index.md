---
layout: "image"
title: "Kleines Ritzel"
date: 2022-03-25T19:39:01+01:00
picture: "Kettenschaltung_Top_klein.JPG"
weight: "5"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Kette auf großem Ritzel
