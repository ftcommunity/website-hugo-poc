---
layout: "image"
title: "Werfer mit Rückstellgummi"
date: 2022-03-25T19:39:04+01:00
picture: "Kettenschaltung-Rueckseite2.JPG"
weight: "3"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Blick auf den Werfer mit Rückstellgummi
