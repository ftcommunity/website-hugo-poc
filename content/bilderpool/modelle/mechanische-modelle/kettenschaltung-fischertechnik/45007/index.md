---
layout: "image"
title: "Bowdenzug"
date: 2022-03-25T19:38:58+01:00
picture: "Kettenschaltung-Bowdenzug.JPG"
weight: "7"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
uploadBy: "Website-Team"
license: "unknown"
---

Bowdenzug mit Pneumatikteilen
