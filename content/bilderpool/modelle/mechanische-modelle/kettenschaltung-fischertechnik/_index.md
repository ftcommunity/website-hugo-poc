---
layout: "overview"
title: "Kettenschaltung mit Fischertechnik"
date: 2022-03-25T19:38:56+01:00
---

Dies ist ein Modell einer 2-Gang Fahrrad-Kettenschaltung mit Fischertechnik, die über einen Bowdenzug geschaltet wird. 
Der Werfer ist ein Parallelogramm-Mechanismus, der das Laufrad parallel verschiebt. Die Rückstellung des Werfers erfolgt mittels eines Haushaltsgummis.
