---
layout: "image"
title: "Containergreifer 12"
date: "2008-07-26T16:23:18"
picture: "containergreifer12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14958
- /details1c2e-2.html
imported:
- "2019"
_4images_image_id: "14958"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14958 -->
Hier nochmals der geschlossene Zustand.

Als Anmerkung: Beide Öffnungstaster und beide Schließtaster sind jeweils parallel geschaltet d.h.
erst wenn jeweils beide Taster gedrückt sind stoppt der Motor.