---
layout: "image"
title: "Containergreifer 06"
date: "2008-07-26T16:23:13"
picture: "containergreifer06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14952
- /detailsbb67.html
imported:
- "2019"
_4images_image_id: "14952"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14952 -->
Hier sieht man den Greifer von unten und erkennt ganz deutlich zwei Taster.
Diese dienen für Bedingungen die ich an elektrische Funktion stelle.

1. Wenn der Greifer ohne Box am Haken hängt darf der Motor nicht angestellt werden können.
    und beide Lampen sind aus.
2. Wenn der Greifer richtig auf der Box steht kann erst dann diese verriegelt werden und
    die rote Lampe geht an.(siehe Bild 07)