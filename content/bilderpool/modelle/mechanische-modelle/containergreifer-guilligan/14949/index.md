---
layout: "image"
title: "Containergreifer 03"
date: "2008-07-26T16:23:13"
picture: "containergreifer03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14949
- /details82dc.html
imported:
- "2019"
_4images_image_id: "14949"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14949 -->
Ich habe den Greifer einmal gedreht und man sieht die Lampenfassungen die
der Stromversorgung dienen.