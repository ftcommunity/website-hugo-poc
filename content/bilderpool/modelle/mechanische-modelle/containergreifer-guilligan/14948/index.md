---
layout: "image"
title: "Containergreifer 02"
date: "2008-07-26T16:23:13"
picture: "containergreifer02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/14948
- /details3ee3.html
imported:
- "2019"
_4images_image_id: "14948"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14948 -->
Das "Fähnchen" dient dazu das die Kabel vom Kran dort eingehängt werden.