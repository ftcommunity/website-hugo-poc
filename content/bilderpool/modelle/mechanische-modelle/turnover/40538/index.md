---
layout: "image"
title: "Original aus Papier"
date: "2015-02-14T19:15:14"
picture: "4_Turnover_Papier.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40538
- /detailsb990.html
imported:
- "2019"
_4images_image_id: "40538"
_4images_cat_id: "3038"
_4images_user_id: "724"
_4images_image_date: "2015-02-14T19:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40538 -->
Hier das Video vom Papier-Modell:
https://www.youtube.com/watch?v=EXkbQzNZXK8
