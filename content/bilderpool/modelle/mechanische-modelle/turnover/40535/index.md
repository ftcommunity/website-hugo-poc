---
layout: "image"
title: "Turnover"
date: "2015-02-14T19:15:14"
picture: "1_Turnover.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/40535
- /details3591.html
imported:
- "2019"
_4images_image_id: "40535"
_4images_cat_id: "3038"
_4images_user_id: "724"
_4images_image_date: "2015-02-14T19:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40535 -->
Mathematische Spielerei
Das Modell kann beleibig oft durch die Mitte gedreht werden
Nachempfunden dem Original aus Papier

Hier kann man sich das Teil in Aktion ansehen:
https://www.youtube.com/watch?v=6m_JG7swx54&list=UUN792LdoJAkQi9-rPmfzWIQ
