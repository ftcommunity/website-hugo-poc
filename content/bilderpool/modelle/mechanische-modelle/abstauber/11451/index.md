---
layout: "image"
title: "'Abstauber'"
date: "2007-09-09T15:10:00"
picture: "d_bilder_010.jpg"
weight: "13"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- /php/details/11451
- /details519c.html
imported:
- "2019"
_4images_image_id: "11451"
_4images_cat_id: "1031"
_4images_user_id: "637"
_4images_image_date: "2007-09-09T15:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11451 -->
Der weg des steines