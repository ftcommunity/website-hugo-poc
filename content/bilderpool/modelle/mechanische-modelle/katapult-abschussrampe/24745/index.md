---
layout: "image"
title: "Turm mit Verriegelung"
date: "2009-08-12T09:44:30"
picture: "abschussrampe08.jpg"
weight: "8"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24745
- /details3edd.html
imported:
- "2019"
_4images_image_id: "24745"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24745 -->
Die Verriegelung hällt die Bälle vom Wegrutschen ab.