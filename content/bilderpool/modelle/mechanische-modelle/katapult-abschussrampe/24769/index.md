---
layout: "image"
title: "Munition7"
date: "2009-08-12T09:44:30"
picture: "abschussrampe32.jpg"
weight: "32"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24769
- /details2b42-2.html
imported:
- "2019"
_4images_image_id: "24769"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24769 -->
Mit diesen Bällen schiesst die Maschiene.