---
layout: "image"
title: "Abschussrampe mit Rädern"
date: "2009-08-12T09:44:29"
picture: "abschussrampe01.jpg"
weight: "1"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24738
- /details9499.html
imported:
- "2019"
_4images_image_id: "24738"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24738 -->
Abschussräder