---
layout: "image"
title: "Luftank"
date: "2009-08-12T09:44:30"
picture: "abschussrampe20.jpg"
weight: "20"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24757
- /details6e3a-2.html
imported:
- "2019"
_4images_image_id: "24757"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24757 -->
Dort ist die Luft drin