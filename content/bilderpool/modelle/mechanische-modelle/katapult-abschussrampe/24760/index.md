---
layout: "image"
title: "Zylinder von Oben"
date: "2009-08-12T09:44:30"
picture: "abschussrampe23.jpg"
weight: "23"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24760
- /detailsd767.html
imported:
- "2019"
_4images_image_id: "24760"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24760 -->
Sicherheitsnetz Teil 1