---
layout: "image"
title: "Munition2"
date: "2009-08-12T09:44:30"
picture: "abschussrampe27.jpg"
weight: "27"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24764
- /detailsc98c-2.html
imported:
- "2019"
_4images_image_id: "24764"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24764 -->
Mit diesen Bällen schiesst die Maschiene.