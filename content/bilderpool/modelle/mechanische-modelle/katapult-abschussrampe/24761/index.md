---
layout: "image"
title: "Zylinder"
date: "2009-08-12T09:44:30"
picture: "abschussrampe24.jpg"
weight: "24"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24761
- /details6967.html
imported:
- "2019"
_4images_image_id: "24761"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24761 -->
Sicherheitsnetz Teil 1.1