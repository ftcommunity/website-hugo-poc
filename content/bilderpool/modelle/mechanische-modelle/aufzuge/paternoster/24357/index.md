---
layout: "image"
title: "Paternoster vorne"
date: "2009-06-14T15:49:09"
picture: "Paternoster_vorne.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/24357
- /details279e-2.html
imported:
- "2019"
_4images_image_id: "24357"
_4images_cat_id: "1668"
_4images_user_id: "724"
_4images_image_date: "2009-06-14T15:49:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24357 -->
Auch Umlauf-Aufzug genannt.
Vorteil ist wie bei einer Rolltreppe die stetige Personenbeförderung (2 Personen pro Kabine), die immer in beide Richtungen möglich ist.
Wird eingesetzt meist in Behörden, da dort das Publikum weitestgehend gleich ist. Ist nämlich am Anfang etwas gewöhnungsbedürftig. Wenn auch nicht gefährlich.
In den Farbwerken von Hoechst gibt es noch einen (über 30 Stockwerke, wenn ich mich recht erinnere).
Und im Buddhistischen Standesamt (äh nein, Statistischen Bundesamt in Wiesbaden) gibt es auch noch einen. 
Es gibt noch viel mehr, aber ich kenne nicht alle Standorte...
Zwischen den Kabinen sieht man Platten, die verhindern, daß man zwischen die Kabinen gerät.
Die Weiterfahrt durch Dachgeschoß und Keller ist ungefährlich und vorgesehen. Die Kabine bleibt dabei gerade.

Film des Paternosters in Aktion:
http://www.youtube.com/watch?v=4BzfRe18lYA&feature=channel

Film vom Umsetzen des Paternosters im Dachboden:
http://www.youtube.com/watch?v=OsY4mIReGs4&feature=channel_page
