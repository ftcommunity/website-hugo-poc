---
layout: "comment"
hidden: true
title: "23932"
date: "2018-01-26T16:37:30"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Das sind offensichtlich Achsen 30, die in Bausteinen 30 mit Bohrung festgeklemmt sind (mit Papier, Seil oder ähnlichem). Die Seilrollen laufen darauf frei.