---
layout: "comment"
hidden: true
title: "23102"
date: "2017-02-24T21:22:37"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Die Achsen in die seitlich liegenden Löcher eingehängt, Seil drum und die Rollenböcke direkt zusammengeschoben gäbe etwa den gleichen Abstand und die Gefahr eine Achse rauszureissen wäre gar nicht erst da. Das Seil könnte bei der Variante ohne dritte Rolle frei laufen.

Ob sich noch eine Erklärung findet warum das Dingens genau so gebaut wurde?

Grüße
H.A.R.R.Y.