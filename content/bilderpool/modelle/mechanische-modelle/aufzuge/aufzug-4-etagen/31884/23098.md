---
layout: "comment"
hidden: true
title: "23098"
date: "2017-02-24T15:44:08"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich könnte mir auch folgendes vorstellen: Wenn man nur die beiden Rollenböcke hätte, würden die auch nach innen zusammengezogen bei dem Gewicht des Aufzugs. Also muss da ein Stein dazwischen. Dann passt aber das Seil wieder schlecht durch (und noch einen Rollenbock wollten sie offenbar nicht nehmen). Deshalb die Umlenkung über die mittlere Rolle nach oben hin, sodass die Rollenböcke praktisch nur in senkrechter Richtung auf Zug, aber nicht in Querrichtung beansprucht werden. Vielleicht verträgt das ihr Zapfen besser?

Gruß,
Stefan