---
layout: "image"
title: "Seiltrommeln Fahrkorbantrieb (vorne)"
date: "2011-09-23T11:45:36"
picture: "etagenaufzug34.jpg"
weight: "34"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31905
- /details87eb.html
imported:
- "2019"
_4images_image_id: "31905"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:36"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31905 -->
