---
layout: "image"
title: "Teilansicht von hinten"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug24.jpg"
weight: "24"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31895
- /details7056.html
imported:
- "2019"
_4images_image_id: "31895"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31895 -->
