---
layout: "image"
title: "Fahrkorb (rechts) verkleidet"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug14.jpg"
weight: "14"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31885
- /details22f8.html
imported:
- "2019"
_4images_image_id: "31885"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31885 -->
