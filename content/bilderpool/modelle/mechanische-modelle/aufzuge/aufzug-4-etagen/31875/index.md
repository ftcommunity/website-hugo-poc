---
layout: "image"
title: "Aufzug Gesamtansicht"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug04.jpg"
weight: "4"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/31875
- /detailsb614.html
imported:
- "2019"
_4images_image_id: "31875"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31875 -->
