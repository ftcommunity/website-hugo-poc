---
layout: "image"
title: "Selektor"
date: "2006-06-04T12:56:40"
picture: "paternoster_2.jpg"
weight: "2"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6415
- /detailsbe82.html
imported:
- "2019"
_4images_image_id: "6415"
_4images_cat_id: "556"
_4images_user_id: "162"
_4images_image_date: "2006-06-04T12:56:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6415 -->
