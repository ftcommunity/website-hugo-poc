---
layout: "image"
title: "Oben"
date: "2006-06-04T12:56:41"
picture: "paternoster_6.jpg"
weight: "7"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6420
- /detailsf328-4.html
imported:
- "2019"
_4images_image_id: "6420"
_4images_cat_id: "556"
_4images_user_id: "162"
_4images_image_date: "2006-06-04T12:56:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6420 -->
Hier seht mann den Plank mit denn RESET stein. Dies ist der 8. Plank. Immer wann dieser Plank denn Taster betatigt, dann steht Plank 1 vorne und das ist der Start position.
