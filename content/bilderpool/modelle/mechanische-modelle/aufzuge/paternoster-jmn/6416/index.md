---
layout: "image"
title: "Selektor hinten seite"
date: "2006-06-04T12:56:41"
picture: "paternoster_2a.jpg"
weight: "3"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6416
- /details2265.html
imported:
- "2019"
_4images_image_id: "6416"
_4images_cat_id: "556"
_4images_user_id: "162"
_4images_image_date: "2006-06-04T12:56:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6416 -->
Es gibt 12 Widerstande. Jede von 470 Ohm.
Mit jede Schaltung kommt 470 Ohm dazu. Zb: nummer 1 hat 470 Ohm, nummer 2 hat 940 Ohm etc. Dieser wert wir dan im Interface gelesen und wann Start gepresst wird, dan dreht der Schrank erst nach sein RESET position und dreht sich dan nach Plank 2 wann der Selektor auf 2 geschaltet ist.
Leider ist es mit die Widerstande nicht immer 100% damit denn Schrank nicht den Richtigen Plank nach vorne drehen kann. 
Das muss ich noch verbessern. 
Auch will ich auch das dieser Schrank ein "memory" funktion hat. Zb: Plank nummer 3 steht vorne und denn selektor wird auf 5 gedreht, dann will ich das der Schrank gleich nach 5 geht und nicht erst rund dreht zum RESET und dann nach 5. 
Ich bin aber noch nicht so weit mit den RoboInterface.
