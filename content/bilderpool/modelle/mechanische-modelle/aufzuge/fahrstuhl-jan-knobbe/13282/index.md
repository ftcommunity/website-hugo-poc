---
layout: "image"
title: "fahrstuhl jan knobbe 3"
date: "2008-01-06T20:09:44"
picture: "fahrstuhlvonjanknobbe3.jpg"
weight: "3"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13282
- /details0ae9-2.html
imported:
- "2019"
_4images_image_id: "13282"
_4images_cat_id: "1200"
_4images_user_id: "713"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13282 -->
Oben sind zwei Stop-Taster nötig.

Im Downloadbereich befindet sich der Schaltplan (Kategorie Software).