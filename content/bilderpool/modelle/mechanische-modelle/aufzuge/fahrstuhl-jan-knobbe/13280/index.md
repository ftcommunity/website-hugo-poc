---
layout: "image"
title: "fahrstuhl jan knobbe 1"
date: "2008-01-06T20:09:44"
picture: "fahrstuhlvonjanknobbe1.jpg"
weight: "1"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13280
- /details448c.html
imported:
- "2019"
_4images_image_id: "13280"
_4images_cat_id: "1200"
_4images_user_id: "713"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13280 -->
Die unregelmäßige Anordnung der Etagen erklärt sich daraus, dass der Fahrstuhl die Stockwerke der Puppenstube meiner Tochter ansteuern sollte. Im Downloadbereich befindet sich der Schaltplan (Kategorie Software).