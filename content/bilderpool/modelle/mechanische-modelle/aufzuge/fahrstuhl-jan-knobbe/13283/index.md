---
layout: "image"
title: "fahrstuhl jan knobbe 4"
date: "2008-01-06T20:09:44"
picture: "fahrstuhlvonjanknobbe4.jpg"
weight: "4"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
uploadBy: "jan"
license: "unknown"
legacy_id:
- /php/details/13283
- /details8b36.html
imported:
- "2019"
_4images_image_id: "13283"
_4images_cat_id: "1200"
_4images_user_id: "713"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13283 -->
Ein Stoptaster in einer der mittleren Etagen (mit Federgelenkbaustein).Im Downloadbereich befindet sich der Schaltplan (Kategorie Software).