---
layout: "overview"
title: "Sackaufzug (Mühlenaufzug)"
date: 2020-02-22T08:18:07+01:00
legacy_id:
- /php/categories/3278
- /categories6747.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3278 --> 
Angeregt durch die Besichtigung einer original erhaltenen, 500 Jahre alten und nach wie vor Mehl produzierenden Wassermühle in Lothringen konstruierte Konrad nach unserer Rückkehr diesen Sackaufzug.