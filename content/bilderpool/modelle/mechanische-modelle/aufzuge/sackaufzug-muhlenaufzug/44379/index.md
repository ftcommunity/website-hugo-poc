---
layout: "image"
title: "Sackaufzug - ausgekuppelt"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug3.jpg"
weight: "3"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/44379
- /details78b6.html
imported:
- "2019"
_4images_image_id: "44379"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44379 -->
Im Bild sieht man den "ausgekuppelten" Sackaufzug: der Transmissionsriemen hängt lose durch.