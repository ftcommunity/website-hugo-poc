---
layout: "image"
title: "'Kupplung' des Sackaufzugs"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug2.jpg"
weight: "2"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/44378
- /details1875-2.html
imported:
- "2019"
_4images_image_id: "44378"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44378 -->
Der Müller hatte uns das Geheimnis enthüllt: Der Seilzug spannte über ein Führungsrad einen lose sitzenden Transmissionsriemen.