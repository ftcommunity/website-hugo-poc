---
layout: "image"
title: "Schnecke"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl10.jpg"
weight: "10"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13475
- /details8ba8.html
imported:
- "2019"
_4images_image_id: "13475"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13475 -->
