---
layout: "comment"
hidden: true
title: "5161"
date: "2008-01-31T12:16:43"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Ist die Rolle rechts schon aus der Halterung gerutscht? Für große Lasten in der Einbau-Position ist die eigentlich nicht so gut geeignet!

Thomas