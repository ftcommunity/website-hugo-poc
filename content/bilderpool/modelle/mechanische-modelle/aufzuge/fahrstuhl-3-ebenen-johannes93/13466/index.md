---
layout: "image"
title: "Gesamtansicht"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13466
- /detailsbe88.html
imported:
- "2019"
_4images_image_id: "13466"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13466 -->
