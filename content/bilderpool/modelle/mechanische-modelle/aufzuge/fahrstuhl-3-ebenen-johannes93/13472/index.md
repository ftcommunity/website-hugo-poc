---
layout: "image"
title: "Kabine auf der 3. Ebene - Gesamtansicht"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl07.jpg"
weight: "7"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13472
- /details5fa1.html
imported:
- "2019"
_4images_image_id: "13472"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13472 -->
