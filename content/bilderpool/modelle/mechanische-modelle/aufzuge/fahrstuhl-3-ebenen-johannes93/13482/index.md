---
layout: "image"
title: "Akku für die Innenbeleuchtung"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal (Johannes93)"
fotografen:
- "Johannes Westphal (Johannes93)"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- /php/details/13482
- /detailsfd03-2.html
imported:
- "2019"
_4images_image_id: "13482"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13482 -->
