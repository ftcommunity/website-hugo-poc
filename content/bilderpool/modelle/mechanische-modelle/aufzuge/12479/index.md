---
layout: "image"
title: "Aufzug"
date: "2007-11-05T15:54:01"
picture: "IMG_0226.jpg"
weight: "1"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12479
- /details1f9c.html
imported:
- "2019"
_4images_image_id: "12479"
_4images_cat_id: "1085"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12479 -->
Mehrgeschossiger Aufzug