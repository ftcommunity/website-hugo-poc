---
layout: "image"
title: "Aufzug Front"
date: 2023-02-10T18:20:08+01:00
picture: "pic15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Front mit Beschriftung