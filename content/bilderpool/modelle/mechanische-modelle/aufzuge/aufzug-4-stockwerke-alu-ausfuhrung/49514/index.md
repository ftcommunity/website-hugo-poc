---
layout: "image"
title: "Bodenplatten"
date: 2023-02-10T18:19:55+01:00
picture: "pic25.jpg"
weight: "25"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Für den Transport lässt sich das Model mittig trennen.