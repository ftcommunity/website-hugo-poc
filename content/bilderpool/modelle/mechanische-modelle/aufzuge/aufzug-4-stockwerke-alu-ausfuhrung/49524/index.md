---
layout: "image"
title: "Rückseite Front"
date: 2023-02-10T18:20:07+01:00
picture: "pic16.jpg"
weight: "16"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Motoren, Lichtschranken, Ruftaster sind beschriftet worden.