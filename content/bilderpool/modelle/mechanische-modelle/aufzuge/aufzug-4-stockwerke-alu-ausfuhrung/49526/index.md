---
layout: "image"
title: "Aufzug Seite"
date: 2023-02-10T18:20:09+01:00
picture: "pic14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man 2x TXT-Controller für die Ansteuerung der Lichtschranken, Ruftaster und den Motoren.
Der TX-Controller steuert die Digitale Anzeige.
