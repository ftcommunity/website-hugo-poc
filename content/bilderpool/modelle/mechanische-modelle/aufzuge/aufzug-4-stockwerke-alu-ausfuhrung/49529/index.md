---
layout: "image"
title: "Aufzug Front"
date: 2023-02-10T18:20:12+01:00
picture: "pic11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die Front bereits am Alu-Gerüst drehbar verbaut.