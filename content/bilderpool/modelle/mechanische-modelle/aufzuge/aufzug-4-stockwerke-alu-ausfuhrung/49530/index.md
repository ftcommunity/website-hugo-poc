---
layout: "image"
title: "Antrieb Aufzug"
date: 2023-02-10T18:20:13+01:00
picture: "pic10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Aufzug wird über einen Encoder-Motor mit Kette angetrieben.
Über den Encoder weiß der Aufzug immer wo dieser steht.