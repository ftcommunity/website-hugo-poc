---
layout: "image"
title: "Kabine"
date: 2023-02-10T18:19:50+01:00
picture: "pic05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Erster Entwurf der Kabine.
Vorne die Führungsschienen. Hinten die Schnecke für die Auf- und Abbewegung.