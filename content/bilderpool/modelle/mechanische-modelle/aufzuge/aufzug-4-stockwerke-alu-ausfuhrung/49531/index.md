---
layout: "image"
title: "Stockwerk Türen"
date: 2023-02-10T18:20:14+01:00
picture: "pic01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier seht ihr den Teleskopantrieb der Türen.
Zwei gegenläufige Schnecken, angetrieben vom S-Motor mit Getriebe.

Hier findet ihr ein Video dazu:
https://1drv.ms/v/s!AnRUc7zxfYRNxlj24ttQN-sEKR-L?e=AZVmKu