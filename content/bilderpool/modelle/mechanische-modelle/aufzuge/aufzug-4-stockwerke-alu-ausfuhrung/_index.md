---
layout: "overview"
title: "Aufzug 4 Stockwerke Alu-Ausführung"
date: 2023-02-10T18:19:45+01:00
---

Hallo zusammen,

hier möchte ich euch meinen Aufzug mit 4 Stockwerken mit Aluprofilen vorstellen.

Dieser beruht auf der vorherigen Version:
https://ftcommunity.de/bilderpool/modelle/mechanische-modelle/aufzuge/aufzug-4-stockwerke-dirkw/

Eine Firma ist an mich herangetreten mit dem Ziel, das der Aufzug einen digitalen Zwilling abbilden soll. 
Das heißt die Sensoren, Motoren, Lichtschranken sollen digital über einen Browser abgerufen werden. 
Der Overhead ist eine SAP-Software, welche die Sensoren überwacht. Bei Störungen werden die Fehler 
in der Software angezeigt. Hintergrund ist, die Wartung des Aufzugs dezentral und kostengünstig zu abzubilden.
Das Modell wird dann später auf Ausstellungen gezeigt, um die Anwendung der SAP-Wartungssoftware zu zeigen.

Im Gegensatz zum Vörgängermodell ist, das keine Seile für den Aufzug verwendet wurden, da er liegend zu Ausstellungen transportiert wird.
Auch die Türen sind ebenfalls wartungsfreier über Schecken konstruiert worden. Als Schnittstelle werden alle Signale von den Controllern abgegriffen.
Diese werden in der SAP-Software digital weiterverarbeitet.
