---
layout: "image"
title: "Breadboard"
date: 2023-02-10T18:20:04+01:00
picture: "pic18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Ein- und Ausgänge des Aufzugs sind über das Flachbandkabel herausgeführt
zum Abgreifen der Signale.