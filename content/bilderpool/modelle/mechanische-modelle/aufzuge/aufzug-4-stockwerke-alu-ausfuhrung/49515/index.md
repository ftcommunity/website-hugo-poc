---
layout: "image"
title: "Bodenplatten"
date: 2023-02-10T18:19:56+01:00
picture: "pic24.jpg"
weight: "24"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Aufzug wurde auf zwei hochwertige Bodenplatten montiert