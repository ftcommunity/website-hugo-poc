---
layout: "image"
title: "Stockwerk Front"
date: 2023-02-10T18:20:02+01:00
picture: "pic02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier seht ihr das Stockwerk mit Rufanzeige und Stockwerkanzeige

Es wird die Funktion der Türen getestet.

Hier findet ihr ein Video dazu:
https://1drv.ms/v/s!AnRUc7zxfYRNxlbDkjMtazpltKYR?e=jGhI09
