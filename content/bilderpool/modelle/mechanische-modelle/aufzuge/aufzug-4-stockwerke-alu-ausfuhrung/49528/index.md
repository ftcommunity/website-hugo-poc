---
layout: "image"
title: "Front Drehlager"
date: 2023-02-10T18:20:11+01:00
picture: "pic12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Front wurde drehbar gelagert. Dies macht die Wartung einfacher.