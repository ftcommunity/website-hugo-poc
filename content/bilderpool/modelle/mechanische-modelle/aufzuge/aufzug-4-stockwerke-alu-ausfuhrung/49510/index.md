---
layout: "image"
title: "Prüfstand Stockwerk"
date: 2023-02-10T18:19:51+01:00
picture: "pic04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Zum Prüfen der Funktionen der einzelnen Stockwerke wurde ein kleiner Prüfstand gebaut