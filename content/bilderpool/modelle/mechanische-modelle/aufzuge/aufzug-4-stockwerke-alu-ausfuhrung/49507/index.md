---
layout: "image"
title: "Rohbau Aufzug vorne"
date: 2023-02-10T18:19:47+01:00
picture: "pic07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier seht ihr den Rohbau des Aufzugs. Der Aufzug wird links und rechts über Stromschienen mit Strom versorgt.