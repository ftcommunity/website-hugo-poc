---
layout: "image"
title: "Aufzug + 4 Stockwerke"
date: 2023-02-10T18:20:01+01:00
picture: "pic20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Es wurde ein zweites Gerüst mit Alu-Stockwerken gebaut.