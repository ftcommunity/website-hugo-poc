---
layout: "image"
title: "Aufgeklappte Front"
date: 2023-02-10T18:20:03+01:00
picture: "pic19.jpg"
weight: "19"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht am die aufgeklappte Front für die bessere Wartung