---
layout: "image"
title: "Kabine Detail"
date: 2023-02-10T18:19:46+01:00
picture: "pic08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

In der Kabine ist ein Lampenbaustein verbaut.