---
layout: "image"
title: "Stockwerk Front Anzeige rollierend"
date: 2023-02-10T18:19:52+01:00
picture: "pic03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier wird die rollierende Anzeige getestet.

Hier findet ihr ein Video dazu:
https://1drv.ms/v/s!AnRUc7zxfYRNxln0yKgo9Bs66gK6?e=DjDYDM