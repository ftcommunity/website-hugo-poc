---
layout: "image"
title: "PICT4871"
date: "2010-02-24T21:35:37"
picture: "fangvorrichtung3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26537
- /details51b8.html
imported:
- "2019"
_4images_image_id: "26537"
_4images_cat_id: "1890"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26537 -->
Fangvorrichtung an der Schiene in Fanglage.
Der bewegliche Keil wird an die Fahrkorbschiene
gepresst.