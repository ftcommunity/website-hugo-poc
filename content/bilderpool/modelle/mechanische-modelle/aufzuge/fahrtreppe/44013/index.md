---
layout: "image"
title: "Funktionsmodell Fahrtreppe - Prototyp"
date: "2016-07-26T10:47:33"
picture: "fahrtreppe1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
schlagworte: ["escalator", "ft-escalator", "roltrap", "ft-roltrap", "ft-Rolltreppe", "fischertechnik", "fischertechnik-Rolltreppe", "fischertechnik-Fahrtreppe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44013
- /detailsbddc.html
imported:
- "2019"
_4images_image_id: "44013"
_4images_cat_id: "3257"
_4images_user_id: "1557"
_4images_image_date: "2016-07-26T10:47:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44013 -->
Der "Stairway to Heaven" ist das jetzt zwar nicht, aber immerhin hat das Ding alles was eine vorbildgerechte Fahrtreppe auszeichnet:
* Konkave Krümmung im unteren Bereich,
* Konvexe Krümmung im oberen Bereich,
* Stufenverhalten im 'öffentlich zugänglichen' Bereich,
* Bidirektionaler Betrieb.

Als Kompromiss haben wir dann:
* Angedeuteter Handlauf,
* Antriebsräder / Umlenkungen oben und unten sind keine Kettenräder,
* Seitenhilfsführungen (wobei nicht klar ist ob sich das mit vollbestückten Trittstufen von alleine erledigt),
* Frei hängendes unteres Kettentrum.

Der Antrieb geht direkt per Handkurbel und dank des alten 45er Rades läßt sich jeder Aspekt der Stufenführung sehr griffig erfahren.

Ach so: Kein Modding, kein 3D-Druck und keine Fremdteile! Alles original ft-Spritzguß-Teile; ja und die Metallachsen sind natürlich kein Spritzguß.