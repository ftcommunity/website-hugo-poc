---
layout: "comment"
hidden: true
title: "22612"
date: "2016-10-21T00:36:34"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Ganz großes Kino für mich als Purist.
Da wären einige Detailaufnahmen und Videos sinnvoll (YouTube und hier verlinken).
Vielleicht baue ich dann mal eine große Ausgabe mit der Umsetzug der "Kompromisse". Danke jedenfalls für die Inspiration.