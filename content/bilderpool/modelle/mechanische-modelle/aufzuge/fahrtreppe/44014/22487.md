---
layout: "comment"
hidden: true
title: "22487"
date: "2016-09-04T18:21:54"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo Peter, Hallo Fans,

mit dem Video ist das so eine Sache. Ich habe (noch?) keinen Account bei yout* und 'Gurgel' schätze ich generell nicht besonders.

Aber was wichtiger ist: Die Fahrtreppe war für mich eine Fingerübung, ob da mit ft überhaupt eine Chance besteht - das Gestell war noch ziemlich wacklig und letztes Wochenende ist sie auch noch vom Tisch gefallen. Falls ich noch mal drangehe, dann richtig. Also stabil und mit entsprechend robuster Handkurbel (ThanksForTheFish weiß wohl am besten was ich meine). Aber bis dahin kann es dauern ... sorry Peter.

Grüße
H.A.R.R.Y.