---
layout: "image"
title: "Funktionsmodell Fahrtreppe - Prototyp"
date: "2016-07-26T10:47:34"
picture: "fahrtreppe2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44014
- /details0648.html
imported:
- "2019"
_4images_image_id: "44014"
_4images_cat_id: "3257"
_4images_user_id: "1557"
_4images_image_date: "2016-07-26T10:47:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44014 -->
Leider fehlen derzeit Kleinteile und so ziehen hier nur zwei von zweiunddreißig Trittstufen ihre Bahn, Riegelscheiben ersetzen notdürftig die Räder.

Was es bislang so im Netz zum Thema ft-Rolltreppe gibt, ist aber ebenso spärlich und lückenhaft:
http://ftcommunity.de/categories.php?cat_id=1016 (Harald's legendärer Fehlversuch - immerhin hat er sich getraut!)
http://blog.goo.ne.jp/ftblock/m/201106 (Die japanische "dreiviertel Version"  -  aber trotz der fehlenden konkaven Krümmung eine ganz tolle Leistung weil hier immerhin die Handläufe mitlaufen können!)
http://www.ftcommunity.de/details.php?image_id=43933 (Fan-Club-Tag-Modell 2016, von Harald - the only way is up ;) )
http://www.ftcommunity.de/details.php?image_id=44019 (noch ein bißchen mehr über Harald's Version 2016)
http://fischertechnik-ag.de/Aktuelles (Suchen nach "Rolltreppe" oder "15.3.2014" - leider ist dazu kein Foto zu finden - ähem, räusper, Diiiii-hiiiiirk??)