---
layout: "image"
title: "Fahrtreppe (totale)"
date: "2016-07-26T18:01:04"
picture: "fahrtreppe1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44019
- /details4eb5.html
imported:
- "2019"
_4images_image_id: "44019"
_4images_cat_id: "3259"
_4images_user_id: "4"
_4images_image_date: "2016-07-26T18:01:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44019 -->
Gute 8 Jahre nach dem ersten Anlauf (siehe "Fehlversuche", https://ftcommunity.de/categories.php?cat_id=1016 ). Erst kam der Funke ("Teile und Herrsche" löst das Problem!) und nach kurzem Entschluss ist sie innerhalb von zwei Tagen entstanden.
