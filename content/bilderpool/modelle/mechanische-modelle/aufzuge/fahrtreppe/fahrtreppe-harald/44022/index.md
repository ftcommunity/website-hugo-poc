---
layout: "image"
title: "Führungen"
date: "2016-07-26T18:01:04"
picture: "fahrtreppe4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44022
- /details5ee2-2.html
imported:
- "2019"
_4images_image_id: "44022"
_4images_cat_id: "3259"
_4images_user_id: "4"
_4images_image_date: "2016-07-26T18:01:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44022 -->
Die kleinen schwarzen Rollen legen fest, wo die Vorderkanten der Stufen entlang fahren sollen. Sie laufen auf breiterem Radstand und werden vom gelben Handlauf niedergedrückt, bzw. vom Reifen 45, der roten Bauplatte 15*45 und dem Malteserrad nach oben gedrückt.

Die bunten Räder 23 geben an, wo die Hinterkanten der Stufen entlang fahren sollen. Sie laufen auf schmalem Radstand und werden von den schwarzen BS30 nach oben gedrückt.

Das Zusammenspiel von innerer und äußerer Führungsbahn legt dann fest, wo die Stufen waagerecht liegen und wo sie irgendwelche anderen Winkel einnehmen dürfen. Da ist noch etwas Feinarbeit möglich, aber im Ganzen funktioniert sie.
