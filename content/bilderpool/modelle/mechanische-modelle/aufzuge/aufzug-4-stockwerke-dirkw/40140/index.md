---
layout: "image"
title: "Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug45.jpg"
weight: "45"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40140
- /detailsa281.html
imported:
- "2019"
_4images_image_id: "40140"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40140 -->
Zu sehen ist:

Links:     Ruftaste, Lichtschranke
Oben:    Stockwerkanzeige
Rechts: Richtungsanzeige Kabine Auf - Ab
