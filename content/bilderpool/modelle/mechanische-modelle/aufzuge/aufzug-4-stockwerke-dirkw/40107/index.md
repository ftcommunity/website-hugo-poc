---
layout: "image"
title: "Fahrstuhlschacht"
date: "2015-01-02T15:55:46"
picture: "aufzug12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40107
- /detailsf242.html
imported:
- "2019"
_4images_image_id: "40107"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40107 -->
Endschalter der Kabine
