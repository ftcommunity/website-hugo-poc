---
layout: "image"
title: "Stockwerkanzeige vorn"
date: "2015-01-02T15:55:46"
picture: "aufzug46.jpg"
weight: "46"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40141
- /detailsbdf2-3.html
imported:
- "2019"
_4images_image_id: "40141"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40141 -->
Die 7-Segmentanzeige des Stockwerks.
