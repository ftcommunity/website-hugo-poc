---
layout: "image"
title: "Ruftaster Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40110
- /detailsf522-4.html
imported:
- "2019"
_4images_image_id: "40110"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40110 -->
