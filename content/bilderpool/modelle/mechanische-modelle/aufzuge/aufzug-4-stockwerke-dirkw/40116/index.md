---
layout: "image"
title: "Kabine seitlich von oben"
date: "2015-01-02T15:55:46"
picture: "aufzug21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40116
- /detailsf39d.html
imported:
- "2019"
_4images_image_id: "40116"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40116 -->
