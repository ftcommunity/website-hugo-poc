---
layout: "image"
title: "Stockwerkanzeige Detail"
date: "2015-01-02T15:55:46"
picture: "aufzug50.jpg"
weight: "50"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40145
- /details9d80.html
imported:
- "2019"
_4images_image_id: "40145"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40145 -->
Dann habe ich eine 10-polige Buchse mit der Anzeige verlötet und ebenfalls aufgeklebt.
