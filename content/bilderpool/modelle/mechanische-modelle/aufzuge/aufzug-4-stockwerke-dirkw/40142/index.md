---
layout: "image"
title: "Stockwerkanzeige  hinten"
date: "2015-01-02T15:55:46"
picture: "aufzug47.jpg"
weight: "47"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40142
- /details9a70.html
imported:
- "2019"
_4images_image_id: "40142"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40142 -->
Ansteuerung der  7-Segmentanzeige.
