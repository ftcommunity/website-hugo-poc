---
layout: "image"
title: "Ruftaster Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40113
- /details5873-3.html
imported:
- "2019"
_4images_image_id: "40113"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40113 -->
Hier seht ihr die ganze Mimik eingebaut von hinten. Bei gedrücktem Ruftaster wird über eine Flip-Flop Schaltung das Signal gespeichert und 
die Led leuchtet.
