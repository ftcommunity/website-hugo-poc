---
layout: "image"
title: "Steuerung Fahrstuhl"
date: "2015-01-02T15:55:46"
picture: "aufzug27.jpg"
weight: "27"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40122
- /detailsc8a0-3.html
imported:
- "2019"
_4images_image_id: "40122"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40122 -->
Zwei Robo TX Controller für die Auf- und Ab-Steuerung, Rufverarbeitung und Lichtschranken.
Der SAA1064 Treiber wird über EXT2 I2C angesteuert.
