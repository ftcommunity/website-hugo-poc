---
layout: "image"
title: "Führungsschienen Gegengewicht"
date: "2015-01-02T15:55:46"
picture: "aufzug40.jpg"
weight: "40"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40135
- /detailse7bd.html
imported:
- "2019"
_4images_image_id: "40135"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40135 -->
Führungsschienen für das Gegengewicht
