---
layout: "image"
title: "Rückseite Fahrstuhl"
date: "2015-01-02T15:55:46"
picture: "aufzug26.jpg"
weight: "26"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40121
- /details4a08.html
imported:
- "2019"
_4images_image_id: "40121"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40121 -->
Die Rückseite mit der Ansteuerung des Fahrstuhls ohne die Kabine
