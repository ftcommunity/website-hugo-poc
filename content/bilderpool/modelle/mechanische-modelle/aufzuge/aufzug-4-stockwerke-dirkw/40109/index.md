---
layout: "image"
title: "Ruftaster Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug14.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40109
- /details0fb0.html
imported:
- "2019"
_4images_image_id: "40109"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40109 -->
Der Ruftaster leuchtet, wenn er gedrückt wird. Vorrausetzung ist, das der Fahrstuhl sich nicht im gleichen Stockwerk befindet.
