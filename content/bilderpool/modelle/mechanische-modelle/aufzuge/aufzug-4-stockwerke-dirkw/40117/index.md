---
layout: "image"
title: "Kabine von oben"
date: "2015-01-02T15:55:46"
picture: "aufzug22.jpg"
weight: "22"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40117
- /details19bd.html
imported:
- "2019"
_4images_image_id: "40117"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40117 -->
Die Sicht vom 3.Stock ins Erdgeschoß.
