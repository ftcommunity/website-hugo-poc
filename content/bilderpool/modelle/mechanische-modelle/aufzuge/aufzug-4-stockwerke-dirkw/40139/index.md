---
layout: "image"
title: "Fahrstuhl Außenverkleidung vorn"
date: "2015-01-02T15:55:46"
picture: "aufzug44.jpg"
weight: "44"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40139
- /detailsc798.html
imported:
- "2019"
_4images_image_id: "40139"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40139 -->
Man kann die Elektrik der Stockwerke erkennen.
