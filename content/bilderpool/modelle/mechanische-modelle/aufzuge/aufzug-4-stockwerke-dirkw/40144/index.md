---
layout: "image"
title: "Stockwerkanzeige Detail"
date: "2015-01-02T15:55:46"
picture: "aufzug49.jpg"
weight: "49"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40144
- /detailsa900.html
imported:
- "2019"
_4images_image_id: "40144"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40144 -->
Für die Anschlüsse der Anzeige habe ich acht 1 mm Löcher in eine Bauplatte gebohrt.
Die  7-Segmentanzeige habe ich dann anschließend aufgeklebt.

