---
layout: "image"
title: "SAA1064 Treiber"
date: "2015-01-02T15:55:46"
picture: "aufzug28.jpg"
weight: "28"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/40123
- /details71f3.html
imported:
- "2019"
_4images_image_id: "40123"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40123 -->
Darüber ein SAA1064 Treiber auf Steckboard für die 7- Segmentanzeige.
