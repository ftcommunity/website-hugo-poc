---
layout: "image"
title: "Die Kabine"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan14.jpg"
weight: "13"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33198
- /details3e0b-2.html
imported:
- "2019"
_4images_image_id: "33198"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33198 -->
Hier seht man in die Kabine