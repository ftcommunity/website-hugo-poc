---
layout: "image"
title: "Türe im 1. Stockwerk offen"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan10.jpg"
weight: "9"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33194
- /details4a93.html
imported:
- "2019"
_4images_image_id: "33194"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33194 -->
Da die Kabine in diesem Stockwerk ist, kann man die Türe öffnen (Blaue Luftschraube)