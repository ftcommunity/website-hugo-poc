---
layout: "image"
title: "Türe im 2. Stockwerk verriegelt"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan13.jpg"
weight: "12"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33197
- /detailse0bd.html
imported:
- "2019"
_4images_image_id: "33197"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33197 -->
Da die Kabine nicht in diesem Stockwerk ist, kann man die Türe nicht öffnen (Blaue Luftschraube)