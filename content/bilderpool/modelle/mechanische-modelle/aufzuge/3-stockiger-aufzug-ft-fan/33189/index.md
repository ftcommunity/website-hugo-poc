---
layout: "image"
title: "Betriebsstundenzähler"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan04.jpg"
weight: "4"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33189
- /details29e4.html
imported:
- "2019"
_4images_image_id: "33189"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33189 -->
Dieses Zählwerk zeigt die Gesamte Betriebszeit des Aufzuges an.