---
layout: "image"
title: "Antriebseinheit"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan18.jpg"
weight: "16"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33201
- /details42f1.html
imported:
- "2019"
_4images_image_id: "33201"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33201 -->
Der Motor treibt über ein Getriebe die Seilwinde an. 
Der Taster zählt die Umdrehungen der Seilwinde