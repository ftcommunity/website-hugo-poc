---
layout: "image"
title: "Lichtschranke"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan15.jpg"
weight: "14"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- /php/details/33199
- /detailsf1d3.html
imported:
- "2019"
_4images_image_id: "33199"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33199 -->
Solange das Legomännchen in der Lichtschranke steht, fährt der Aufzug nicht los