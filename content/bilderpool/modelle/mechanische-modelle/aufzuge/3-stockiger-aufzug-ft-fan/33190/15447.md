---
layout: "comment"
hidden: true
title: "15447"
date: "2011-10-16T21:44:04"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hm. Wie ein RoboInt im Nebel aussieht ;-) wissen wir ja. Aber erzählst Du noch ein bisschen über die Funktionsweise des ganzen? Wie wird der Aufzug zu einem bestimmten Stockwerk gerufen? Wie steuerst Du die Propeller zur Türverriegelung?

Gruß,
Stefan