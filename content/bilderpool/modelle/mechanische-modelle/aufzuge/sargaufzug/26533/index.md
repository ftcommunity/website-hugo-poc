---
layout: "image"
title: "PICT4865"
date: "2010-02-24T21:35:37"
picture: "sargaufzug13.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26533
- /details9605.html
imported:
- "2019"
_4images_image_id: "26533"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26533 -->
Ansicht der Umlenkrolle und Kettenaufhängung