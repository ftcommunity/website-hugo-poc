---
layout: "image"
title: "18082009367"
date: "2010-02-24T21:35:37"
picture: "sargaufzug02.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26522
- /details6250.html
imported:
- "2019"
_4images_image_id: "26522"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26522 -->
Handantrieb mit Bandbremse.
Der gelbe Eimer diente als Kettenbunker