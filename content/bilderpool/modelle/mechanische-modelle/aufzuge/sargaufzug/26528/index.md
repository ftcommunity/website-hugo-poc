---
layout: "image"
title: "PICT4860"
date: "2010-02-24T21:35:37"
picture: "sargaufzug08.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26528
- /details0ba0-2.html
imported:
- "2019"
_4images_image_id: "26528"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26528 -->
Gesamtansicht von hinten rechts