---
layout: "image"
title: "PICT4859"
date: "2010-02-24T21:35:37"
picture: "sargaufzug07.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26527
- /details934c.html
imported:
- "2019"
_4images_image_id: "26527"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26527 -->
Gesamtansicht von hinten links