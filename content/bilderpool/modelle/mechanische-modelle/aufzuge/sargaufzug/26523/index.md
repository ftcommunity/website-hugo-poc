---
layout: "image"
title: "18082009368"
date: "2010-02-24T21:35:37"
picture: "sargaufzug03.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26523
- /details73e8.html
imported:
- "2019"
_4images_image_id: "26523"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26523 -->
Ansicht von unten