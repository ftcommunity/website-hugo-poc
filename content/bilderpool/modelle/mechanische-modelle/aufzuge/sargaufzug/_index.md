---
layout: "overview"
title: "Sargaufzug"
date: 2020-02-22T08:17:51+01:00
legacy_id:
- /php/categories/1889
- /categoriesf952.html
- /categories1674-2.html
- /categories17ec.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1889 --> 
Ein handbetriebener Sargaufzug mit Bandbremse. Aufhängung 2:1