---
layout: "image"
title: "18082009366"
date: "2010-02-24T21:35:36"
picture: "sargaufzug01.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26521
- /detailscb50-2.html
imported:
- "2019"
_4images_image_id: "26521"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26521 -->
Die folgenden Bilder dienten als Vorlage für das ft-Modell.
Der Aufzug wurde abgerissen und durch eine hydraulische
Scherenhubbühne ersetzt.

Plattform hochgefahren.