---
layout: "image"
title: "PICT4864"
date: "2010-02-24T21:35:37"
picture: "sargaufzug12.jpg"
weight: "12"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26532
- /details6330.html
imported:
- "2019"
_4images_image_id: "26532"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26532 -->
Detail der Umlenkrollen unter der Plattform