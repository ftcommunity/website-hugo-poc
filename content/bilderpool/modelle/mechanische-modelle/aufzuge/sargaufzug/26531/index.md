---
layout: "image"
title: "PICT4863"
date: "2010-02-24T21:35:37"
picture: "sargaufzug11.jpg"
weight: "11"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/26531
- /details6dd7.html
imported:
- "2019"
_4images_image_id: "26531"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26531 -->
Detail der Handkurbel