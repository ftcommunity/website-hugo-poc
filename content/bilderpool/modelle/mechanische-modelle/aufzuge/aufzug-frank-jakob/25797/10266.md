---
layout: "comment"
hidden: true
title: "10266"
date: "2009-11-18T19:38:42"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Die Seilaufnahme ist drehbar gelagert und dient zusätzlich als Seilausgleich. Sollte ein Seil reißen stellt sich die Wippe schräg und der (Fang)Kontakt wird ausgelöst(Aufzug stoppt). Beim echten Aufzug werden zusätzlich die Fangkeile an die Fahrkorbschiene gepresst.

Gruß,
Frank

[Weiterführende Informationen zum "Fangen"](http://www.tuv.com/de/abwaerts.html)