---
layout: "image"
title: "PICT4849"
date: "2009-11-17T21:32:28"
picture: "aufzugfrankjakob14.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25800
- /details0ed2-2.html
imported:
- "2019"
_4images_image_id: "25800"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25800 -->
Steuerung