---
layout: "image"
title: "PICT4848"
date: "2009-11-17T21:32:28"
picture: "aufzugfrankjakob13.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25799
- /detailsf17e-2.html
imported:
- "2019"
_4images_image_id: "25799"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25799 -->
Treibscheibe