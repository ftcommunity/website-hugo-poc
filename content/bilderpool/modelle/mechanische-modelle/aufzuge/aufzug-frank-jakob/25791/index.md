---
layout: "image"
title: "PICT4832"
date: "2009-11-17T21:32:24"
picture: "aufzugfrankjakob05.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25791
- /details94d1.html
imported:
- "2019"
_4images_image_id: "25791"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25791 -->
Kabine mit geöffneter Tür