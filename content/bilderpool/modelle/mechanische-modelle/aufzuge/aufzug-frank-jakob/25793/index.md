---
layout: "image"
title: "PICT4836"
date: "2009-11-17T21:32:24"
picture: "aufzugfrankjakob07.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/25793
- /details8a0d.html
imported:
- "2019"
_4images_image_id: "25793"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25793 -->
Gesamtansicht von hinten