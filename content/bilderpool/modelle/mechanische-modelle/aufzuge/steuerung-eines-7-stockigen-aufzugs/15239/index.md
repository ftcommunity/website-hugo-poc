---
layout: "image"
title: "Der Aufzug (in Legomännchen-Perspektive ;-)"
date: "2008-09-15T16:39:11"
picture: "DSCF1695.jpg"
weight: "8"
konstrukteure: 
- "elko"
fotografen:
- "elko"
uploadBy: "elko"
license: "unknown"
legacy_id:
- /php/details/15239
- /details8cb0.html
imported:
- "2019"
_4images_image_id: "15239"
_4images_cat_id: "1394"
_4images_user_id: "763"
_4images_image_date: "2008-09-15T16:39:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15239 -->
