---
layout: "comment"
hidden: true
title: "7159"
date: "2008-09-16T01:27:21"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Ja, super. Das Modell habe ich auf dem Landeswettbewerb schon live gesehen ... und ich erinnere mich auch düster, Dir die Adresse der ftcommunity aufgeschrieben zu haben. Schön, dass Du jetzt hierher gefunden hast :-)

Ich erinnere mich leider nicht mehr, aus welchem Teil Hessens du stammst. Am kommenden Samstag, den 20., findet im Norden des Bundeslandes die fischertechnik-Convention statt. Wenn Du noch nix vorhast, komm einfach kurzentschlossen vorbei, mit dem HessenTicket schaffst Du's allemal. Würd mich freuen, Dich wieder zu treffen.

Grüße, Heiko