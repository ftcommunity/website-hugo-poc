---
layout: "comment"
hidden: true
title: "7151"
date: "2008-09-15T16:30:35"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr schicke Lösung mit der Codierung, richtig gut. Und die Platinen machen einen sehr professionellen Eindruck. Respekt! Warum hast Du denn die Taster so "enthüllt"? Platzgründe?

Gruß,
Stefan