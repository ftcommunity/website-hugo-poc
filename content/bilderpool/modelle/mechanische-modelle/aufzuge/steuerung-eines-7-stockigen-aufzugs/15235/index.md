---
layout: "image"
title: "Ein mit Dioden codierter Taster"
date: "2008-09-15T16:13:56"
picture: "DSCF1689.jpg"
weight: "4"
konstrukteure: 
- "elko"
fotografen:
- "elko"
schlagworte: ["Diode", "Taster", "USB", "Aufzug", "Platine"]
uploadBy: "elko"
license: "unknown"
legacy_id:
- /php/details/15235
- /details6068.html
imported:
- "2019"
_4images_image_id: "15235"
_4images_cat_id: "1394"
_4images_user_id: "763"
_4images_image_date: "2008-09-15T16:13:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15235 -->
Jeder Taster zum Rufen des Aufzugs hat eine eigene Codierung