---
layout: "image"
title: "Gesamtansicht Aufzug mit Steuerung"
date: "2010-09-13T14:37:24"
picture: "aufzug13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28097
- /details6c60-2.html
imported:
- "2019"
_4images_image_id: "28097"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28097 -->
Für die Steuerung verwende ich einen TX Controller - bis auf einen Counter sind alle Ein- und Ausgänge für die 11 Sensoren und 4 Motoren erforderlich.
Hier die Anschlussbelegung:
I1: Reed-Kontakt Etage 1 (Stockwerkerkennung)
I2: Reed-Kontakt Etage 2
I3: Reed-Kontakt Etage 3
I4: Fototransistor Etage 1 (Lichtschranke)
I5: Fototransistor Etage 2
I6: Fototransistor Etage 3
I7: Polwendetaster oben (Richtungswechsel)
I8: Polwendetaster unten
C1: Taster Etage 1 (Ruftaste)
C2: Taster Etage 2
C3: Taster Etage 3
M1: Mini-Mot Etage 1 (Schiebetüre)
M2: Mini-Mot Etage 2
M3: Mini-Mot Etage 3
M4: Antriebsmotor (Aufzug)