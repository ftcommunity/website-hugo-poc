---
layout: "image"
title: "Gegengewicht im Aufzugschacht (von hinten)"
date: "2010-09-13T14:37:23"
picture: "aufzug07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28091
- /detailsc3e2.html
imported:
- "2019"
_4images_image_id: "28091"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28091 -->
Das Gegengewicht gleitet an zwei separaten Laufschienen über je zwei Seilrollen 12 in Rollenlagern (Alternative: Rollenbock, benötigt jedoch doppelt so viele Seilrollen).
Die Laufschienen habe ich seitlich auf U-Träger montiert. (Auch hier waren einfache Winkelträger zu labil und das Gegengewicht sprang aus der Führung.)