---
layout: "image"
title: "Gesamtansicht Aufzug (von vorne)"
date: "2010-09-13T14:37:21"
picture: "aufzug01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28085
- /detailsec63-2.html
imported:
- "2019"
_4images_image_id: "28085"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28085 -->
Die Gesamtansicht zeigt den (verkleideten) Aufzug von vorne. In jedem Stockwerk befindet sich links ein "Ruftaster" und rechts oben die Stockwerkanzeige.
(Die beiden Handwerker - Gastarbeiter - kümmern sich gerade um die Verkabelung.)