---
layout: "image"
title: "Moving Head Antrieb Hochachse"
date: "2004-06-06T10:30:07"
picture: "MH03-Antrieb_Hochachse.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2472
- /detailsa58d.html
imported:
- "2019"
_4images_image_id: "2472"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2472 -->
Seitenansicht des Hochachsenantriebs. Darunter die Stützkonstruktion und die Stromübergabe für den zweiten Motor.