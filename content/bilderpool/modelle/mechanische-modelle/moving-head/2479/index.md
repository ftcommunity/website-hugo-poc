---
layout: "image"
title: "Moving Head Stromübergabe"
date: "2004-06-06T10:30:08"
picture: "MH07-Strombergabe.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2479
- /details2039.html
imported:
- "2019"
_4images_image_id: "2479"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2479 -->
Das läßt auch kleiner konstruieren, aber mir fehlen die kleinen Metallsteckhülsen mit den runden Köpfen für die Schleifringe.