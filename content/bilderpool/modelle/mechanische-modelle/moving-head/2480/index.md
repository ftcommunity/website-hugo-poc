---
layout: "image"
title: "Moving Head Überblick"
date: "2004-06-06T10:30:08"
picture: "MH01-Gesamtmodell_vorne.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2480
- /details1faa.html
imported:
- "2019"
_4images_image_id: "2480"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2480 -->
Im Prinzip eine leere Gabelmontierung drehbar in 2 Achsen