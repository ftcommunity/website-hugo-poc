---
layout: "image"
title: "Moving Head Antriebseite"
date: "2004-06-06T10:30:07"
picture: "MH02-Gesamtmodell_hinten.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2474
- /detailseb04.html
imported:
- "2019"
_4images_image_id: "2474"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2474 -->
Antrieb mittels hochauflösender Schrittmotore. Erreichbare Schwenkgeschwindigkeit bis 60 Grad/s