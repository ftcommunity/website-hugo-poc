---
layout: "image"
title: "Moving Head Antrieb Querachse"
date: "2004-06-06T10:30:07"
picture: "MH04-Antrieb_Querachse.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/2473
- /details87d3.html
imported:
- "2019"
_4images_image_id: "2473"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2473 -->
Im Prinzip das gleiche, wie die Hochachse