---
layout: "image"
title: "Vertwisten"
date: "2010-03-05T21:54:13"
picture: "Vertwistet.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26596
- /details840c.html
imported:
- "2019"
_4images_image_id: "26596"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-05T21:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26596 -->
Die eine Drehscheibe wird gegenüber der anderen verdreht. Wenn man nicht 6, sondern unendlich viele Gummibänder verwendete, entstünde ein einschaliges Rotationshyperboloid, obwohl alle Gummibänder gerade verlaufen.
