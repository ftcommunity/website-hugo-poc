---
layout: "image"
title: "Versuchsaufbau"
date: "2010-03-05T21:54:12"
picture: "Zylinder2.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26595
- /details71fb.html
imported:
- "2019"
_4images_image_id: "26595"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-05T21:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26595 -->
Das hier ist kein eigentliches Modell, sondern ein Experiment, das die Kraftübertragung zwischen windschiefen Achsen demonstriert. Zwei Drehkränze werden über 6 normale Haushaltsgummis so verbunden, dass zunächst ein Zylinder entsteht.
