---
layout: "image"
title: "Konstruktion von zwei kongruenten Rotationshyperboloiden"
date: "2010-03-11T19:52:41"
picture: "IneinanderOhneTwist.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26669
- /details2dfb.html
imported:
- "2019"
_4images_image_id: "26669"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26669 -->
Zuerst baut man einen kleineres Hyperboloid, indem man zwei Zahnräder Z30 auf eine Achse setzt, drei Gummibänder um jeweils 120° versetzt über die Zähne legt, so dass ein Zylinder angedeutet wird. Dann verdreht man ein Zahnrad gegen das andere, baut zwei Drehscheiben herum und legt ebenfalls drei Gummiringe herum, so dass ein Zylinder entsteht, in dem sich das kleinere Hyperboloid befindet.
