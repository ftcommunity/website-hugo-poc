---
layout: "image"
title: "Konstruktion von zwei kongruenten Rotationshyperboloiden 2"
date: "2010-03-11T19:52:41"
picture: "Ineinander.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["windschiefe", "Achsen"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26670
- /details539b-2.html
imported:
- "2019"
_4images_image_id: "26670"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26670 -->
Jetzt stimmt man die Verdrehungen so ab, dass die Gummibäder direkt aneinander liegen, d.h. das kleine Hyperboloid ist ein Abschnitt des größeren. Dann baut man das äußere Hyperboloid auf einer separaten Achse genauso nach, behält die Kopie und entfernt das Original.
