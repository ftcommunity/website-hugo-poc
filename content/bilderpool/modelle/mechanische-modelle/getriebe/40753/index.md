---
layout: "image"
title: "kleines Zahnradgetriebe"
date: "2015-04-11T18:23:50"
picture: "IMG_0017.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40753
- /details3a88.html
imported:
- "2019"
_4images_image_id: "40753"
_4images_cat_id: "1575"
_4images_user_id: "1359"
_4images_image_date: "2015-04-11T18:23:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40753 -->
eine Anwendung habe ich noch nicht für diese kleine Bastelei, aber vielleicht kanns ja wer brauchen. Es ist NICHT im Raster - aber seht selbst..
