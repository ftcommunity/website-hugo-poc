---
layout: "image"
title: "Stirnraddifferential V2"
date: "2014-03-02T18:43:51"
picture: "Stirnraddifferential_V2_1_bearbeitet.jpg"
weight: "3"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38417
- /details8cec.html
imported:
- "2019"
_4images_image_id: "38417"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38417 -->
Hier nun eine komplette Fassung, die sich auch insgesamt drehen läßt und mit einer zusätzlichen Hülse auch stabiler ist. (Dank an Harald).

Die Nabenmuttern in den Scheiben sind lose. An dem grossen Zahnrad rechts lässt sich das ganze Gestell drehen.

Die obere Reihe von Bausteinen darf nicht ganz auf die Scheibe geschoben werden, da die Zahnräder sonst anstossen. Sie soll dem ganzen etwas mehr Stabilität verleihen und auch die Unwucht etwas ausgleichen (was aber dafür leider nicht ausreicht).

Nun ja. Sie schwimmt also ein wenig lebendiger die Kaulquappe.