---
layout: "image"
title: "Stirnraddifferential V5"
date: "2014-03-08T12:01:42"
picture: "Stirnraddifferential_V5_5_bearbeitet.jpg"
weight: "4"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38440
- /details5155-2.html
imported:
- "2019"
_4images_image_id: "38440"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-03-08T12:01:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38440 -->
Eine andere Version des Stirnraddifferentials.

Sie ist kompakter und auch um einiges weniger unwuchtig als V2.

(Falls sich jemand wundert, wo denn V3 und V4 geblieben sind, kann ich hinzufügen, das dabei sozusagen "unechte" Differentiale herausgekommen sind. Unecht, in dem Sinne, das sich die beiden äusseren Wellen _nicht_ wie beim "echten" Differential gegensinnig drehen.
Ich weiss nichtmal ob diese Art einen eigenen Namen hat. Falls jemand neugierig ist, kann ich die mal posten, aber sie sind deutlich grösser als alle anderen Varianten.
Andererseits wäre mir ein solches unechtes Differential garnicht so unlieb, weil ich es sowieso zum addieren resp. subtrahieren benötige).