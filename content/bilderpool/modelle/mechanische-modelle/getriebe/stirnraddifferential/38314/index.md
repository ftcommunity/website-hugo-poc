---
layout: "image"
title: "Stirnraddifferential"
date: "2014-02-21T12:11:49"
picture: "Stirnraddifferential_3_bearbeitet_klein.jpg"
weight: "1"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/38314
- /details252e.html
imported:
- "2019"
_4images_image_id: "38314"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-02-21T12:11:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38314 -->
Mein Versuch (die Kaulquappe) eines Stirnraddifferentials.

Die gesamte Bauggruppe muss man sich, als um die beiden oberen Achsen drehbar, vorstellen.
Da mir aber leider kurze Achsen (80er) gefehlt haben konnte ich das nicht beenden.
Für meine Zwecke wird das ohnehin zu groß.
Aber möglicherweise kriegt man das noch kleiner.