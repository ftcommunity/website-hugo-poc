---
layout: "image"
title: "Stirnraddifferential - Zangenmutter und Spannzange"
date: "2014-07-21T20:31:28"
picture: "Stirnraddifferential_V7_Bild_3_bearbeitet.jpg"
weight: "6"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
schlagworte: ["Stirnraddifferential", "Zangenmutter", "Spannzange"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39023
- /details7edc-2.html
imported:
- "2019"
_4images_image_id: "39023"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-07-21T20:31:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39023 -->
Dann ist mir aufgefallen das die Zangenmutter - 31915 und die Spannzange 35113 ja auch "gezahnt" sind. Eigentlch ja eher eine Rändelung - aber naja - Versuch mach kluch, dachte ich.
Es geht auch tatsächlich. Aber die Ausrichtung der Achsabstände ist doch recht kitzlig.