---
layout: "image"
title: "Stirnraddifferential - Gesamtansicht, Stirnräder oben"
date: "2014-05-18T19:01:36"
picture: "stirnraddifferential1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38821
- /detailsc585.html
imported:
- "2019"
_4images_image_id: "38821"
_4images_cat_id: "2900"
_4images_user_id: "1126"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38821 -->
Der Differentialkäfig wird von zwei Achsen und einer gegenüberliegenden Stange stabil gehalten.
Als Stirnräder dienen Z10er, der Antrieb erfolgt über zwei Z20er, die durch eine Freilaufnabe die linke bzw. die rechte Achse antreiben.