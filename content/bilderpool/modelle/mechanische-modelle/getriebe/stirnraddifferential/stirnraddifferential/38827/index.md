---
layout: "image"
title: "Andere Variante eines Stirnraddifferentials - Steg/Käfig"
date: "2014-05-19T18:34:26"
picture: "Steg.jpg"
weight: "7"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Differenzial", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38827
- /detailsfb61.html
imported:
- "2019"
_4images_image_id: "38827"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-19T18:34:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38827 -->
Der kompakte Steg/Käfig ist symmetrisch aufgebaut.
