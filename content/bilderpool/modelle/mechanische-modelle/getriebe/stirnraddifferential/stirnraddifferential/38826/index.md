---
layout: "image"
title: "Andere Variante eines Stirnraddifferentials"
date: "2014-05-19T18:34:26"
picture: "StirnradGesamt.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Differenzial", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38826
- /detailsaaa7.html
imported:
- "2019"
_4images_image_id: "38826"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-19T18:34:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38826 -->
Hier noch einmal ordentlich aufgebaut und mit An-/Abtrieb am Steg. Die linke und die rechte zentrale Achse treffen sich in der Mitte der rechten Freilaufnabe. Die Planeten sitzen auf Achsen 50, die in den BS15 und in beiden Drehscheiben gelagert sind. Man sollte drei Z15 mit Klemmring verwenden - ich hatte aber nur zwei, darum habe ich ein normales Z15 mit einem Streifen Papier auf die Achse geklemmt. Das ist auch ein gutes Rezept zum Nachbauen, wenn man keines der seltenen Z15 mit Klemmring besitzt.
