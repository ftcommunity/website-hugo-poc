---
layout: "comment"
hidden: true
title: "19067"
date: "2014-05-19T21:58:10"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Man kann natürlich auch rechts Z10 als Planeten benutzen und ein Z20 zentral. Dann spart man noch einmal Trägheitsmoment und Durchmesser. Warum ich damals die Z20 nach außen gebaut habe, erschließt sich mir nicht mehr.