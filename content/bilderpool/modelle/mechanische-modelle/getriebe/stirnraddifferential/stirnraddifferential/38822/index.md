---
layout: "image"
title: "Stirnraddifferential - Gesamtansicht, Stirnräder unten"
date: "2014-05-18T19:01:36"
picture: "stirnraddifferential2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38822
- /detailsed06-2.html
imported:
- "2019"
_4images_image_id: "38822"
_4images_cat_id: "2900"
_4images_user_id: "1126"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38822 -->
Hier sieht man die Stabilisierungsstange auf der Unterseite; sie wirkt auch als Gegengewicht und sorgt für einen ruhigeren Lauf des Differentialkäfigs.