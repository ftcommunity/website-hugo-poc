---
layout: "image"
title: "Andere Variante eines Stirnraddifferentials - weitere Perspektive"
date: "2014-05-19T18:34:26"
picture: "StrinradGesamt1.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Differenzial", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38829
- /detailse5a5-2.html
imported:
- "2019"
_4images_image_id: "38829"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-19T18:34:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38829 -->
Der gesamte Aufbau noch einmal aus einer leicht verschobenen Perspektive, so dass man insbesondere die Freilaufnaben besser erkennen kann.
