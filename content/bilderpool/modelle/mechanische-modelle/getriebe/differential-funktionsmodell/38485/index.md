---
layout: "image"
title: "Detailansicht des Selbstbau-Differentials"
date: "2014-03-24T10:28:22"
picture: "differentialfunktionsmodell2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/38485
- /details08d5.html
imported:
- "2019"
_4images_image_id: "38485"
_4images_cat_id: "2873"
_4images_user_id: "1126"
_4images_image_date: "2014-03-24T10:28:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38485 -->
Antrieb des Differentials über Kronenrad (Z15 auf Z32).