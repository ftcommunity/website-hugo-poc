---
layout: "image"
title: "Getriebe 1 von vorne"
date: "2015-01-19T07:10:38"
picture: "getriebemmitminimot01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40384
- /detailsdf0d.html
imported:
- "2019"
_4images_image_id: "40384"
_4images_cat_id: "3027"
_4images_user_id: "2321"
_4images_image_date: "2015-01-19T07:10:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40384 -->
Während meiner Tüfteleien für ein Lenkservo sind zwei Getriebe entstanden, die große Untersetzungen haben. Die möchte ich euch vorstellen.