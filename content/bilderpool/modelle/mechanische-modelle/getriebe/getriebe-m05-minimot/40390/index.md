---
layout: "image"
title: "Getriebe 2 (1)"
date: "2015-01-19T07:10:38"
picture: "getriebemmitminimot07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40390
- /details1a2b.html
imported:
- "2019"
_4images_image_id: "40390"
_4images_cat_id: "3027"
_4images_user_id: "2321"
_4images_image_date: "2015-01-19T07:10:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40390 -->
Hier das zweite Getriebe, bestenend aus Minimot plus zwei Schneckengetrieben. Die meisten Teile dienen irgendwie zur Stabilisierung, damit das Ganze unter Last nicht auseinanderbiegt.