---
layout: "image"
title: "Getriebe 3"
date: "2015-01-24T11:00:56"
picture: "Feinzahngetriebe_3_1_klein.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40396
- /details43a9-2.html
imported:
- "2019"
_4images_image_id: "40396"
_4images_cat_id: "3027"
_4images_user_id: "2321"
_4images_image_date: "2015-01-24T11:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40396 -->
Für engere Verhältnisse, dafür mit etwas höherer Drehzahl.
Die Riegelscheibe läuft ein wenig hinter der Schnecke. Ist nicht die feinste Art, aber praktisch.