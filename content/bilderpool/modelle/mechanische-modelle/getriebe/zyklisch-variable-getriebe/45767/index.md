---
layout: "image"
title: "Kaskadierte Getriebe (4)"
date: 2023-01-18T09:40:20+01:00
picture: "Zyklisch_variable_Getriebe_26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Den Effekt sollte man am besten selber ausprobieren - der ist echt ungewöhnlich.