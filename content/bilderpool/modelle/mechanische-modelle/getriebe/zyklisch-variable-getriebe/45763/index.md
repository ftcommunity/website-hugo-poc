---
layout: "image"
title: "Sanftes Schrittschaltwerk (2)"
date: 2023-01-18T09:40:15+01:00
picture: "Zyklisch_variable_Getriebe_06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die sanfte Schrittweise Drehung des Abtriebs geschieht mit extrem wenigen Bauteilen auf kleinem Raum. Vor allem braucht man keinerlei komplexe Schritt-Steuerung mit Elektromechanik, Elektronik oder Computern.