---
layout: "image"
title: "Presse (2)"
date: 2023-01-18T09:40:38+01:00
picture: "Zyklisch_variable_Getriebe_13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb geschieht wieder ungleichförmig, und zwar Z20:Z40 = 1:2 untersetzt.