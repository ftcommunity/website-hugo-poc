---
layout: "image"
title: "Sanftes Schrittschaltwerk (3)"
date: 2023-01-18T09:40:14+01:00
picture: "Zyklisch_variable_Getriebe_07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Gesamtübersetzung ergibt sich immer noch wie bei einem normalen Getriebe. Hier also Z20-Z20-Z40 und Z10-Z30, insgesamt also 1:6. Aber im Abtrieb eben ungleichmäßig.