---
layout: "image"
title: "Kaskadierte Getriebe (1)"
date: 2023-01-18T09:40:24+01:00
picture: "Zyklisch_variable_Getriebe_23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein ganz witziger Effekt ergibt sich, wenn man mehrere solcher Getriebe hintereinander schaltet.