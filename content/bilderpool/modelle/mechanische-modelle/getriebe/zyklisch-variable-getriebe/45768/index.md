---
layout: "image"
title: "Kaskadierte Getriebe (3)"
date: 2023-01-18T09:40:22+01:00
picture: "Zyklisch_variable_Getriebe_25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Jede Stufe mehr bringt eine "Verschärfung" des Bewegungsablaufs: Das jeweilige Fähnchen bleibt noch länger stehen, dreht sich dafür aber in umso kurzer Zeit und also mit umso höherer Drehgeschwindigkeit, um eine vollständige Umdrehung absolviert zu haben, wenn die Antriebskurbel eine Umdrehung vollführt hat.