---
layout: "image"
title: "Grundaufbau (2)"
date: 2023-01-18T09:40:29+01:00
picture: "Zyklisch_variable_Getriebe_02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Am einfachsten geht das mit Zahnrad-Kombinationen, deren Mittelpunktabstand ganzzahlige Vielfache von 15 mm beträgt. Dann können nämlich einfach I-Streben verwendet werden, die dafür sorgen, dass alle Zahnräder korrekt miteinander kämmen.