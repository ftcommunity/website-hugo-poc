---
layout: "image"
title: "Kurbelzahnrad kleiner als Kurbelradius (2)"
date: 2023-01-18T09:40:41+01:00
picture: "Zyklisch_variable_Getriebe_10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das hat zur Folge, dass die Abtriebs-Drehung sogar ein kleines Stückchen *rückwärts* geht.