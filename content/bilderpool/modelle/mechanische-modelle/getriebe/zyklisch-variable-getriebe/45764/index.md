---
layout: "image"
title: "Sanftes Schrittschaltwerk (1)"
date: 2023-01-18T09:40:16+01:00
picture: "Zyklisch_variable_Getriebe_05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Anwendung ist ein sanft in 60°-Schritten gedrehter Drehteller - hier eine senkrecht stehende ft-Drehscheibe.