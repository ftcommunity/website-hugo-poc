---
layout: "image"
title: "Grundaufbau (3)"
date: 2023-01-18T09:40:19+01:00
picture: "Zyklisch_variable_Getriebe_03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Zwischenzahnrad ist hier ein "Kettenzahnrad Z20". Jedes andere geht prinzipiell auch, nur muss man ggf. breiter bauen oder anstatt I-Streben einige Bausteine mit BS7,5 verwenden, um die richtigen Abstände zu bekommen (z.B. bei der Kombination Z10 auf Z40).