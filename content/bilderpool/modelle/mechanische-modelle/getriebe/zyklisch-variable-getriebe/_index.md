---
layout: "overview"
title: "Zyklisch variable Getriebe"
date: 2023-01-18T09:40:11+01:00
---

Diese Getriebe sind anlässlich Workshops für fischertechnik-AGs und AG-Leitende entstanden und in der ft:pedia 4/2022 näher beschrieben. In Ergänzung zu den ft-Designer-Bildern in der ft:pedia gibt es hier noch Fotos der echten Modelle.