---
layout: "image"
title: "Sanfter Linear-Schrittvorschub (3)"
date: 2023-01-18T09:40:30+01:00
picture: "Zyklisch_variable_Getriebe_19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In den Raum unter dem letzten Z10 (auf der Achse des Z30) kann eine Reihe Bausteine mit Zahnstangen eingelegt werden.