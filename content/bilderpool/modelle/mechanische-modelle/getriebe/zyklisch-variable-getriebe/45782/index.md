---
layout: "image"
title: "Presse (1)"
date: 2023-01-18T09:40:39+01:00
picture: "Zyklisch_variable_Getriebe_12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies sieht aus wie eine normale Exzenterpresse, ist es aber nicht.