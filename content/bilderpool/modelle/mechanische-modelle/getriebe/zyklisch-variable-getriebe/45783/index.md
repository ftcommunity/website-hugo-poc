---
layout: "image"
title: "Kurbelzahnrad kleiner als Kurbelradius (3)"
date: 2023-01-18T09:40:40+01:00
picture: "Zyklisch_variable_Getriebe_11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Einsatzmöglichkeiten sind wohl nur durch die Phantasie beschränkt.