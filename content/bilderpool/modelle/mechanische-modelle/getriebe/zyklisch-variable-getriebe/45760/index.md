---
layout: "image"
title: "Kurbelzahnrad kleiner als Kurbelradius (1)"
date: 2023-01-18T09:40:11+01:00
picture: "Zyklisch_variable_Getriebe_09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist ein Z10 an der Kurbel angebracht. Dessen Radius (7,5 mm) ist kleiner als der der Kurbel (14 mm).