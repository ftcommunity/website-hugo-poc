---
layout: "image"
title: "Kaskadierte Getriebe (2)"
date: 2023-01-18T09:40:23+01:00
picture: "Zyklisch_variable_Getriebe_24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man drei Mal genau denselben Aufbau. Lediglich die Lager sind jeweils anders, weil wir genügend Abstand brauchen, sodass die beweglichen und festen Z20 sich nicht berühren.