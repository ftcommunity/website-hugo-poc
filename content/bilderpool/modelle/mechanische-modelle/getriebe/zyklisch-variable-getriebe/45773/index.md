---
layout: "image"
title: "Sanfter Linear-Schrittvorschub (4)"
date: 2023-01-18T09:40:28+01:00
picture: "Zyklisch_variable_Getriebe_20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Verbinder-30-Fähnchen dient nur der Veranschaulichung des Bewegungsablaufs.