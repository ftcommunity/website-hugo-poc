---
layout: "image"
title: "Sanftes Schrittschaltwerk (4)"
date: 2023-01-18T09:40:13+01:00
picture: "Zyklisch_variable_Getriebe_08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das an der Kurbel fixierte Z20 hat ungefähr denselben Radius (15 mm) wie die Kurbel (14 mm). Damit geht die Abtriebs-Drehgeschwindigkeit von ungefähr 0 (Stillstand) bis zu einer Maximalgeschwindigkeit.