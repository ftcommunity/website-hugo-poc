---
layout: "image"
title: "Sanfter Linear-Schrittvorschub (6)"
date: 2023-01-18T09:40:25+01:00
picture: "Zyklisch_variable_Getriebe_22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

... und wird bei gleichförmiger Kurbeldrehung sanft schrittweise durchs Modell gezogen.