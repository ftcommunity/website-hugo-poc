---
layout: "image"
title: "Presse (3)"
date: 2023-01-18T09:40:36+01:00
picture: "Zyklisch_variable_Getriebe_14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das hat zur Folge, dass bei richtiger Justierung die Auf- und Abbewegungen (die ja "unproduktiv" sind) schnell erledigt werden, aber die Presse ganz unten lange stehen bleibt (für eine intensive Pressung) und auch ganz oben (damit viel Zeit zum Werkstückwechsel zur Verfügung steht).