---
layout: "image"
title: "Sanfter Linear-Schrittvorschub (2)"
date: 2023-01-18T09:40:32+01:00
picture: "Zyklisch_variable_Getriebe_18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die ungleichförmige Bewegung entsteht wie in den vorherigen Modellen.