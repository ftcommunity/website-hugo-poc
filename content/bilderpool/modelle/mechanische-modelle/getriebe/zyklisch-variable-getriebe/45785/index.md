---
layout: "image"
title: "Grundaufbau (1)"
date: 2023-01-18T09:40:43+01:00
picture: "Zyklisch_variable_Getriebe_01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Grundidee ist ein am exzentrischen Teil einer Kurbel befestigtes Zahnrad und ein bewegliches Zwischenzahnrad, was auf das fest gelagerte Abtriebszahnrad geht.

Ein Video zu alldem gibt es unter https://youtu.be/DtUbYV0oiWE