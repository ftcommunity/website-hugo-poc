---
layout: "image"
title: "Grundaufbau (4)"
date: 2023-01-18T09:40:18+01:00
picture: "Zyklisch_variable_Getriebe_04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Gag ist nun, dass sich das Abtriebszahnrad bei gleichförmiger Drehung der Antriebskurbel *ungleichförmig* dreht.