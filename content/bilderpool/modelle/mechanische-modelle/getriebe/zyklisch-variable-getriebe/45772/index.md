---
layout: "image"
title: "Sanfter Linear-Schrittvorschub (5)"
date: 2023-01-18T09:40:27+01:00
picture: "Zyklisch_variable_Getriebe_21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So kann die Zahnstange eingelegt werden...