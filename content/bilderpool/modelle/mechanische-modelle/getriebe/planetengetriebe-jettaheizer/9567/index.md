---
layout: "image"
title: "Planetengetriebe02"
date: "2007-03-18T16:24:02"
picture: "Planetengetriebe02.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/9567
- /detailsecb5.html
imported:
- "2019"
_4images_image_id: "9567"
_4images_cat_id: "873"
_4images_user_id: "488"
_4images_image_date: "2007-03-18T16:24:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9567 -->
Die Ansicht von der Motorseite aus. Wichtig ist, daß die Nabe der Drehscheibe nicht festgezogen wird (besser wäre eine Freilaufnabe, aber die hab ich nicht).