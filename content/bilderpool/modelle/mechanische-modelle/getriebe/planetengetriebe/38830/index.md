---
layout: "image"
title: "Gesamtansicht"
date: "2014-05-22T14:33:36"
picture: "Planeten1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Planetengetriebe", "Mini-Zahnräder"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38830
- /details9e67-2.html
imported:
- "2019"
_4images_image_id: "38830"
_4images_cat_id: "2901"
_4images_user_id: "1088"
_4images_image_date: "2014-05-22T14:33:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38830 -->
Ein kompaktes Planetengetriebe. Damit es gut funktioniert, muss man eventuell eine der Planetenseiltrommeln gegen das entsprechende Rast-Z10 leicht verdrehen.
