---
layout: "comment"
hidden: true
title: "21357"
date: "2015-11-30T13:42:09"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
@schnaggels: genau, ist halt ein Planetengetriebe.

Wie man darauf kommt, ist eigentlich einfach 3/2 + 1 = 5/2.

Wenn man den Steg und damit die rote Achse festhält, drehen sich die schwarzen Z15 einmal, wenn sich die Metallachse 3/2-mal in die andere Richtung dreht - also -3/2-mal. Wenn man nun das gesamte Getriebe starr eine Umdrehung zurückdreht, haben sich die schwarzen Zahnräder nicht bewegt, die rote Achse und der Steg haben sich -1-mal bewegt und die Metallachse -5/2-mal. Also ist die Übersetzung von Metallachse auf die rote Achse mit Vierkant 5/2:1 bei festgehaltenen Z15. Siehe unser Buch http://technikgeschichte-mit-fischertechnik.de, S. 129-130.

Viele Grüße

Thomas