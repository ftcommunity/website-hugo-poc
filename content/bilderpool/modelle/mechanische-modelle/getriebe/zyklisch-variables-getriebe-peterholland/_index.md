---
layout: "overview"
title: "Zyklisch variables Getriebe (peterholland)"
date: 2020-02-22T08:19:11+01:00
legacy_id:
- /php/categories/1185
- /categories7e8c.html
- /categories8196.html
- /categoriesde4c.html
- /categoriesc96b.html
- /categories4e40-2.html
- /categories9d1f.html
- /categoriesa25e.html
- /categoriesa719.html
- /categories37cf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1185 --> 
Aus einem gleichförmigen Antrieb kann ein ungleichförmiger Abtrieb erzeugt werden