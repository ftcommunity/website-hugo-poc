---
layout: "comment"
hidden: true
title: "6924"
date: "2008-08-24T22:00:50"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich würde vermuten, Peter Krijnens Vorlage ab http://www.ftcommunity.de/details.php?image_id=13073 kommt auch nur mit Original-ft-Teilen aus.

Wenn man auf die Drehscheibe hier eine deutliche Markierung (etwa einen gelben Statikbaustein) aufsetzt, stellt man übrigens fest, dass die Abtriebswelle sich bei der Kombination Innenzahnrad Z30 und Abtriebsrad Z10 genau zwei Mal pro Umdrehung der Antriebswelle dreht. Es lohnt wirklich, so ein Getriebe mal selbst zu bauen und zu staunen, was der Herr Klopmeier da Pfiffiges erdacht hat!

Gruß,
Stefan