---
layout: "image"
title: "Ölpumpe mit Zyklisch variables Getriebe"
date: "2007-12-15T12:14:02"
picture: "lpumpe_mit_zyklisch_variablen_Getriebes_Peter_Damen_004_2.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13070
- /detailsbe1f-2.html
imported:
- "2019"
_4images_image_id: "13070"
_4images_cat_id: "1185"
_4images_user_id: "22"
_4images_image_date: "2007-12-15T12:14:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13070 -->
