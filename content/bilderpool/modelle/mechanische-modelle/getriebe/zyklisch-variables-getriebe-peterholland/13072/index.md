---
layout: "image"
title: "Ölpumpe mit Zyklisch variables Getriebe"
date: "2007-12-15T12:14:02"
picture: "lpumpe_mit_zyklisch_variablen_Getriebes_Peter_Damen_006_2.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13072
- /details6c33.html
imported:
- "2019"
_4images_image_id: "13072"
_4images_cat_id: "1185"
_4images_user_id: "22"
_4images_image_date: "2007-12-15T12:14:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13072 -->
