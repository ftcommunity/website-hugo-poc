---
layout: "image"
title: "Rund"
date: "2010-04-14T20:11:09"
picture: "z3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
schlagworte: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26938
- /details2ce4-2.html
imported:
- "2019"
_4images_image_id: "26938"
_4images_cat_id: "1932"
_4images_user_id: "104"
_4images_image_date: "2010-04-14T20:11:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26938 -->
So ist's schon viel besser: Um das Innenzahnrad ist eines der Ur-ft-Raupenbänder gelegt, darum die Kette mit 50 Gliedern. Da die Kette ein kleines bisschen zu wenig straff ums Gummiband liegt, könnte es auf Dauer etwas Schlupf geben, den ich bei diesem Modell nicht gebrauchen kann. Deshalb ist die Kette mit einem kleinen Stück Faden (weiß, hier gerade oben) gesichert. Selbst wenn es Schlupf geben sollte, ist der irgendwann durch den gestrafften Faden begrenzt. Die andere Seite sieht ganz genau so aus - es steckt nur ein Z30 in den zwei Innenzahnrädern, die gemeinsam das Gummi und die Kette tragen. Die Strebe hier und ihr Gegenstück auf der anderen Seite verhindert wieder das seitliche Verrutschen.
