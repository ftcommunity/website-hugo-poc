---
layout: "image"
title: "Prototyp 1 - nicht so toll"
date: "2010-04-14T20:11:08"
picture: "z1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
schlagworte: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26936
- /details8c30.html
imported:
- "2019"
_4images_image_id: "26936"
_4images_cat_id: "1932"
_4images_user_id: "104"
_4images_image_date: "2010-04-14T20:11:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26936 -->
Dieser Entwurf kommt ausschließlich mit Original-ft-Teilen aus: Ein Innenzahnrad, mit 12 Federnocken (mit den Nocken nach außen) bestückt, trägt eine Kette mit 50 Gliedern. Damit nichts verrutscht, sind an zwei gegenüberliegenden Stellen je zwei Förderkettenglieder mit den Zapfen nach innen angebracht. Die schwarzen Teile sind welche von ft-Lenkungen und verhindern das seitliche Herausrutschen des Innenzahnrads vom Z30.
