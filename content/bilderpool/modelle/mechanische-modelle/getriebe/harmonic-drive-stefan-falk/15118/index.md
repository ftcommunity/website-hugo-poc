---
layout: "image"
title: "Detailansicht (1)"
date: "2008-08-27T23:17:56"
picture: "harmonicdrive3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15118
- /details0a5f.html
imported:
- "2019"
_4images_image_id: "15118"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-27T23:17:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15118 -->
Herzstück der Hebeträger ist je ein doppeltes Unterbrecherstück aus dem guten alten Elektromechanik-Programm von fischertechnik. Der graue Zapfen darin wurde herausgenommen, damit er nicht verloren geht. Diese Unterbrecher sind oben abgerundet (was hier entscheidend für einen ruhigen Lauf ist) und haben an ihrer Unterseite zwei kleine Stege, mit denen sie auf einen Baustein 7,5 gesteckt wurden.

Damit das nicht hängenbleibt, wurde von dem großen Drehkranz das rote Teil entfernt (das geht völlig zerstörungsfrei mit der Hand). Mit je einem BS5 mit zwei Zapfen, einem weiteren BS7,5 und ein paar Bausteinen ist das Ganze mit der Drehscheibe verbunden. Bei der Drehung umkreisen diese Hebeteile also das schwarze Zahnrad und heben an der jeweiligen Stelle die Kette etwas ab. Da das Wiederauflegen um je einen Zahn versetzt geschieht, dreht sich die Kette also schrittweise (zahnweise) und damit sehr stark untersetzt um das schwarze Rad.
