---
layout: "comment"
hidden: true
title: "6983"
date: "2008-08-29T00:07:35"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Modell im O-Gestell und gemäß Service Pack 1 fertig zunächst ohne Freilaufnabe. Das O-Gestell besteht aus zwei Säulenpaaren verbunden mit 2 Grundplatten 180x90, beidseitig auf 45mm verlängerte Achsenführung mit BS15 mit Bohrung. Das  Gestell kann auf vier Seiten gestellt werden. Der Stern ist zwischen den BS15 an der Scheibe 60 zusätzlich mit sechs Winkelsteinen 60° stabilisiert. Wenn das Modellfunktionsprinzip gemäß Service Pack 1 funktioniert, ist bis auf Kinetikversionen keine Entwicklung notwendig. Die Scheibe 60 habe ich mit dem Zahnkranzoberteil durch zwei Federnocken verbunden.
Gruß, Udo2