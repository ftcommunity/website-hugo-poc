---
layout: "image"
title: "Service Pack 1"
date: "2008-08-28T21:12:51"
picture: "harmonicdriveservicepack1.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15122
- /detailse405.html
imported:
- "2019"
_4images_image_id: "15122"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-28T21:12:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15122 -->
Hier sieht man zwei der drei Verbesserungen:

1. Die Unterbrecherstücke sind gegen Abspringen geschützt.
2. Der "Stern" ist durch die Platten 15x30 stabiler. Nebeneffekt: Alle Federstäbe können leicht sehr genau auf denselben Abstand von der Mittelachse justiert werden.
