---
layout: "image"
title: "hypozyk0598"
date: "2014-04-21T21:43:54"
picture: "IMG_0598.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38579
- /details8a60.html
imported:
- "2019"
_4images_image_id: "38579"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T21:43:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38579 -->
Hier die EInzelteile: die Exzenterwelle beruht auf eine Rastachse mit Platte 130593. Das gelbe ist eine Freilaufnabe. Die schwarzen "Knubbel" im Z45 sind Verbindungsstopfen 32316. 
Die Rastachse mit Platte sitzt außermittig, deshalb gilt das gleiche auch für das Z40 innerhalb vom Z42. Die drei Kettenbeläge und die Verbindungsstopfen übertragen nur die Drehung, aber nicht die exzentrische Umlaufbewegung der Achse, auf der sich das Z40 dreht.
