---
layout: "comment"
hidden: true
title: "21648"
date: "2016-02-06T10:21:50"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Bei der Übersetzung des Getriebes da oben fällt auf, dass beide Stufen den Faktor 7 aufweisen. Wenn der sich einmal herauskürzt, verbleibt 3 * 2 * 7 = 42. Da fehlen aber noch immer die theoretischen Grundlagen.


Vom Symmetrischen ist schon noch etwas übrig, und gar Edles noch hinzu: bei den Planetengetrieben täte sich ein Universum auf, wenn man nur die Teile hätte. Oder sie sich druckte. Langer Gedankenstrich --------- wieso eigentlich nicht? Hmmm...

Bei der Ein-Wellen-Uhr ist Stefan doch schon so gut wie angekommen :-)

Gruß, Harald