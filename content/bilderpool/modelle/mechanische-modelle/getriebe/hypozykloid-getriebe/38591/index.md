---
layout: "image"
title: "hypozyk572.jpg"
date: "2014-04-21T22:49:03"
picture: "IMG_0572.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38591
- /detailsb401.html
imported:
- "2019"
_4images_image_id: "38591"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:49:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38591 -->
Es tritt auf: die Ausgangsseite der zweiten Stufe: das ist ein Innen-Z30 aus Kette+Z30, und einem Z40 als dem Ende des Antriebsstrangs. Die Kettenteile passen schon formschlüssig in die Drehscheibe. Die V-Achsen 4*17 (35404) müssten also nur noch das Z30 an der Drehscheibe fixieren -- nun ist es halt doppelt gemoppelt. Das gelbe Teil ist eine Freilaufnabe. Die gesamte rechte Seite der Exzenterwelle dreht also nur lose mit: die drei großen Teile darauf werden als Block vom Z28 angetrieben.
