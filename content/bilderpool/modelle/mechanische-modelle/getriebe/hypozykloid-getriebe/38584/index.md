---
layout: "image"
title: "hypozyk599.jpg"
date: "2014-04-21T22:07:24"
picture: "IMG_0599.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Kronenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38584
- /details7455.html
imported:
- "2019"
_4images_image_id: "38584"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:07:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38584 -->
Hier das krönende Ergebnis Nummer 2: ein zweistufiges Hypozykloid-Getriebe. Es besteht aus unveränderten ft-Bauteilen, zuzüglich zweier Fremdteile, die aber in jedem Bastlerhaushalt zu finden sind: zwei kleine Kabelbinder.

Video: http://tinypic.com/player.php?v=2ap73t%3E&s=8
