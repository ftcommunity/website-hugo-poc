---
layout: "image"
title: "hypozyk568"
date: "2014-04-21T22:03:45"
picture: "IMG_0568mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38583
- /details2b61.html
imported:
- "2019"
_4images_image_id: "38583"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:03:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38583 -->
Im Vorgriff auf den weiteren Werdegang kommt hier das erste Ergebnis: ein funktionsfähiges Hypozykloidgetriebe, einstufig. Das zweistufige kommt im nächsten Bild, und danach die Details.

Video (wenn's denn geht): http://www.vidup.de/v/rxlzj/
