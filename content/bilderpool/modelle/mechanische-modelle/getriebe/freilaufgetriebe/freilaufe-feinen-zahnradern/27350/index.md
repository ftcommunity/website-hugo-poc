---
layout: "image"
title: "Variante 4"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27350
- /details311c.html
imported:
- "2019"
_4images_image_id: "27350"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27350 -->
Ganz klein: Die MiniMot-Achse geht durch den BS7,5 in Bildmitte hindurch und ragt noch etwas in den BS30 zum Zentrieren. Das Ganze ist nur mit dem unteren BS7,5 mit dem BS30 verbunden. Federndes Element ist der Verbinder mit seinem Winkelstein 7,5°. Auf dem nächsten Bild sieht man es von der anderen Seite
