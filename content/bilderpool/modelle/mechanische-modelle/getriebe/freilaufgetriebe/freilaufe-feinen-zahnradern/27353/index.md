---
layout: "image"
title: "Variante 5"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27353
- /details701f.html
imported:
- "2019"
_4images_image_id: "27353"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27353 -->
Hier wurde eine Platte 15 * 30 abgenommen und der Blick auf Details freigelegt.

Vorteile:

- Gutes Drehmoment.

Nachteile:

- Nicht wirklich guter Federeffekt.
- Die Achsen fluchten nicht garantiert, sondern nur durch Justage.
