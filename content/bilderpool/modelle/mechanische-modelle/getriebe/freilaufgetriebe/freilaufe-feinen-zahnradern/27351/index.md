---
layout: "image"
title: "Variante 4"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/27351
- /detailsad62.html
imported:
- "2019"
_4images_image_id: "27351"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27351 -->
Die Achse ist hier etwas herausgezogen, damit man es besser sieht.

Vorteile:

- Sehr kompakt - nirgends größer als der BS30.

Nachteile:

- Asymmetrisch
- Ruckelt etwas, weil kein besonders guter Federeffekt entsteht.
