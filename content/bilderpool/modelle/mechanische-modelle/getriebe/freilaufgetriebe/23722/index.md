---
layout: "image"
title: "Rückseite"
date: "2009-04-14T22:21:40"
picture: "freilaufgetriebe5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23722
- /details35cf.html
imported:
- "2019"
_4images_image_id: "23722"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T22:21:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23722 -->
Hier die Rückseite. Man kann natürlich auch kurze Achsen und Klemmringe auf beiden Seiten verwenden.
