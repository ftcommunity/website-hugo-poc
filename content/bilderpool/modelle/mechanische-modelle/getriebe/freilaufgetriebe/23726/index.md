---
layout: "image"
title: "Ansicht von unten"
date: "2009-04-14T23:33:26"
picture: "freilaufgetriebeb3.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23726
- /details6ef9.html
imported:
- "2019"
_4images_image_id: "23726"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T23:33:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23726 -->
Unspektakulär und mit vielen Anbaumöglichkeiten.
