---
layout: "comment"
hidden: true
title: "9001"
date: "2009-04-15T19:15:29"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Ich warte ja schon drauf, dass Du hier ein paar viel raffiniertere Varianten einstellst. Insofern ist diese Kategorie ja nur da, um Dich zu kitzeln, Deine Bilder reinzustellen. :-)

Ein (Auftrags-)Projekt gibts tatsächlich dafür, aber darüber darf ich noch nicht reden (ganz im Ernst). Sobald ich darf, reiche ich natürlich noch was nach. ;-)

Gruß,
Stefan