---
layout: "image"
title: "Kleiner und leichtgängiger (1)"
date: "2009-06-17T23:54:13"
picture: "freilaufnochkleinerundleichtgaengiger1.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24407
- /detailsa2fe-2.html
imported:
- "2019"
_4images_image_id: "24407"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24407 -->
Ich brauche für ein Modell eine sehr kleine und sehr leichtgängige Freilaufkupplung, und das hier war ein erster Versuch. Funktioniert prima, war aber noch zu groß.

Vorteile:
- Recht kompakt
- Sehr wenige Bauteile
- Sehr leichtgängig
- Kein Gummi, was reißen kann

Nachteile:
- Man braucht zwei Federgelenksteine aus dem alten Elektromechanikprogramm
