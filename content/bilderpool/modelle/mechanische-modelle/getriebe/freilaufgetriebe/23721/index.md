---
layout: "image"
title: "Die Königsklasse"
date: "2009-04-14T22:21:40"
picture: "freilaufgetriebe4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23721
- /detailsd1f6.html
imported:
- "2019"
_4images_image_id: "23721"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T22:21:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23721 -->
Max Buiting hat diesen Freilauf schon 2002 gebaut (siehe http://www.ftcommunity.de/details.php?image_id=770 und http://www.ftcommunity.de/details.php?image_id=771). Diese Variante funktioniert geradezu frappierend gut. In den untenliegenden Nuten der BS15 sind wieder die Federkontakte aus dem 1970er Elektromechanikprogramm eingesteckt. Die drücken die 38428 Bauplatte 15 * 30 * 5 mit Nuten gegen das Z10. Das passt wie dafür gemacht: Die Zähne des Z10 rasten perfekt in der Nut ein.

Vorteile:
- Leichter Lauf
- Recht hohes Drehmoment übertragbar
- Kein Teil ragt über den Radius der Drehscheibe hinaus

Nachteile:
- Der Federkontakt wird nicht mehr hergestellt (man bekommt ihn aber noch gebraucht).
- Der Freilauf rastet nur 10 Mal pro Umdrehung (alle 36 °) ein.
