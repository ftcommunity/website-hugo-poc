---
layout: "image"
title: "Gesamtansicht schräg oben"
date: "2015-12-30T19:41:03"
picture: "getriebe4.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42633
- /details5e8b-2.html
imported:
- "2019"
_4images_image_id: "42633"
_4images_cat_id: "3170"
_4images_user_id: "1359"
_4images_image_date: "2015-12-30T19:41:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42633 -->
Seiltrommel 15  (31016) + Klemmring Z36 (31020) + Z28 (31082)

Das Z28 stammt aus dem Getriebe für den Mini-Motor XS .

Das Ganze passt herrlich ins Raster - und läuft "wie geschmiert"

ist es nun eine Kaulquappe, ein Frosch oder gar ein Prinz?
