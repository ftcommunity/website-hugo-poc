---
layout: "comment"
hidden: true
title: "14812"
date: "2011-08-09T19:15:11"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo ihr Beiden,
das Schneckenteil m1,5 mit Z10m1,0 und Z25m0,5 habe ich mir bei Knobloch z.B. auch einzeln gekauft. Die weiß gekennzeichneten Teile dürften im elastischen Dehnungsbereich ohne Rändelung aufgepreßt worden sein. Das beweist z.B. das Teil 31046 Getriebehalter ohne diese Schnecke. Das Aufpressen der Teile erfolgt mit Vorrichtungen auf die Achse. Diese Montagen sind manuell auch nur mit Vorrichtungen z.B. in einer kleinen Handhebelpresse umgekehrt wieder trennbar.
Grüsse euch, Udo2