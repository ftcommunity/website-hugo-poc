---
layout: "image"
title: "Getriebe der Ölförderpumpe"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46731
- /details8213.html
imported:
- "2019"
_4images_image_id: "46731"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46731 -->
Alle drei hier vorgestellten Varianten ungleichförmig übersetzender Getriebe beginnen gleich: Ein Z10 greift in ein um die Drehachse des Z10 schwenkbares Z30, an dem ein Hebel (die gelbe S-Strebe im Hintergrund) exzentrisch befestigt ist und das Z30 so mit einem festen Punkt verbindet.

Danach geht es mit unterschiedlichen Übersetzungsverhältnissen und daraus resultierendem überraschend unterschiedlichen Verhalten weiter und zurück auf die Haupt-Drehachse. Hier ist das die Kombination der beiden Z10 mit Kette. Wir haben also die schwenkbare 1:3-Untersetzung und dann die schwenkende 1:1-Übertragung aufs eigentliche Exzenter der Pumpe.
