---
layout: "image"
title: "Aufhängung des Pumpenstabs"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46732
- /detailsa04f.html
imported:
- "2019"
_4images_image_id: "46732"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46732 -->
Der Pumpenstab (die senkrecht hängende Metallachse) wird durch eine einzelne "S-Kupplung" reibungsarm geführt, sodass sie auch bei der schnellen Abwärtsbewegung nicht klemmt oder hängen bleibt.
