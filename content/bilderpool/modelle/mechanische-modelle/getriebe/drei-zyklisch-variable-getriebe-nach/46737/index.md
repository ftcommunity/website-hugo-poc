---
layout: "image"
title: "Getriebe der zweiten Presse"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46737
- /detailsd029.html
imported:
- "2019"
_4images_image_id: "46737"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46737 -->
Auch dieses Getriebe besteht aus nur wenigen Teilen, hat aber einen völlig überraschenden, verblüffenden Effekt: Nach der Kombination Z10 auf hebelgeführtes Z30 geht es hier von einem weiteren Z10 auf ein Z30, welches über eine Freilaufnabe auf der durchgehenden Achse sitzt. Dieses Z30 dreht sich alle Drittel Runde tatsächlich etwas *zurück*. Durch die Übersetzung aufs Z10 auf der zweiten Achse (in Bildmitte) bewirkt das, dass das Zurückgehen a) um den Faktor 3 verstärkt wird und b) anstatt 3-mal pro Umdrehung nur ein Mal kommt (es werden drei Umdrehungen aus einer des schwarzen Z30).

Wer die Übersetzungsverhältnisse durchrechnet (das kann ganz ohne Berücksichtigung des Schwenkmechanismus gehen), kommt darauf, dass beide Pressen für jeden Zyklus gleich viel Zeit benötigen, aber eben mit gänzlich unterschiedlichem Verlauf. Zur Kontrolle:

Presse 1: Kette auf Z20, sagen wir also mal 1:2 * 1:3 * 1:2 = 1:12.
Presse 2: Kette auf Z40, sagen wir also mal 1:4 * 1:3 * 1:3 * 3:1 = 1:12.
