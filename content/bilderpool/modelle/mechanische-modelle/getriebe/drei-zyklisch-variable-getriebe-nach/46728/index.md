---
layout: "image"
title: "Gesamtansicht"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46728
- /detailse1d3.html
imported:
- "2019"
_4images_image_id: "46728"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46728 -->
Links der Antrieb, dann folgen drei Modelle: Eine Ölförderpumpe und zwei Pressen.
