---
layout: "image"
title: "Antrieb"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46729
- /details58ef.html
imported:
- "2019"
_4images_image_id: "46729"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46729 -->
Obwohl der Motor die ganze Zeit einfach nur durch läuft, ergeben sich durch die zyklisch variablen Getriebe ungleichförmige Bewegungen der drei Modelle.
