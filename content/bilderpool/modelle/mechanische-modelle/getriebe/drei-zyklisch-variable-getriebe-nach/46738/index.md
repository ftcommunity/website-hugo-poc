---
layout: "image"
title: "Aufhängung der Pressen"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46738
- /detailscd91-3.html
imported:
- "2019"
_4images_image_id: "46738"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46738 -->
Die Pressen dienen ja nur zur Demonstration der Getriebe und sind also ganz einfach gehalten, wie man hier sieht. Die Platte 30*45 mit 3 Zapfen bewegt sich letztlich auf und ab und landet unten genau auf der Platte 30*15.
