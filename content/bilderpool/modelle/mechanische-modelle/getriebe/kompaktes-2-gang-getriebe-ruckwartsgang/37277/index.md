---
layout: "image"
title: "Anfänge (3)"
date: "2013-08-27T23:15:00"
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "lukas99h. - Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37277
- /details73f1-2.html
imported:
- "2019"
_4images_image_id: "37277"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37277 -->
Etwas kompakter. Hier ist kein Z30 verbaut, dafür sind die BS15-Loch nicht ganz im Raster.
Gebaut von lukas99h. und phil
