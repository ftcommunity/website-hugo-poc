---
layout: "image"
title: "Kompaktes 2-Gang-Getriebe + Rückwärtsgang verbessert (1)"
date: "2013-09-18T21:59:56"
picture: "bild1_2.jpg"
weight: "8"
konstrukteure: 
- "lukas99h. - Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37412
- /details055c-2.html
imported:
- "2019"
_4images_image_id: "37412"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-09-18T21:59:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37412 -->
Ich habe das Getriebe mal ein bisschen verbessert. Bei der alten Version hakte es beim Schalten, das darf natürlich nicht sein. Hier sind nur die Zahnradabstände etwas verändert, aber das zieht ein Problemchen mit sich (siehe nächstes Bild).
Gebaut von lukas99h. und Phil
