---
layout: "image"
title: "Kompaktes 2-Gang-Getriebe + Rückwärtsgang (3)"
date: "2013-08-27T23:15:00"
picture: "bild7.jpg"
weight: "7"
konstrukteure: 
- "lukas99h. - Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37281
- /details9f99-2.html
imported:
- "2019"
_4images_image_id: "37281"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37281 -->
Von der anderen Seite.
Gebaut von lukas99h. und phil
