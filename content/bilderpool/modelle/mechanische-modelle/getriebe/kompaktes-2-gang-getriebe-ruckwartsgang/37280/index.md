---
layout: "image"
title: "Kompaktes 2-Gang-Getriebe + Rückwärtsgang (2)"
date: "2013-08-27T23:15:00"
picture: "bild6.jpg"
weight: "6"
konstrukteure: 
- "lukas99h. - Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37280
- /details32c1-2.html
imported:
- "2019"
_4images_image_id: "37280"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37280 -->
Hier sieht man die Verbindung der BS15-Loch. Der rechte ist leider etwas aus dem Raster "gerutscht", aber anders passt es nicht, wegen dem Z20.
Gebaut von lukas99h. und phil
