---
layout: "image"
title: "nochmal von OBEN"
date: "2015-04-11T18:23:50"
picture: "IMG_0022.jpg"
weight: "6"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/40757
- /detailsbeb0-2.html
imported:
- "2019"
_4images_image_id: "40757"
_4images_cat_id: "1575"
_4images_user_id: "1359"
_4images_image_date: "2015-04-11T18:23:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40757 -->
