---
layout: "image"
title: "Symmetrische Variante - Innere Hantel"
date: "2014-05-05T23:06:56"
picture: "HantelSymmetrisch1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
schlagworte: ["Differenzial", "Getriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/38745
- /details28b7.html
imported:
- "2019"
_4images_image_id: "38745"
_4images_cat_id: "2895"
_4images_user_id: "1088"
_4images_image_date: "2014-05-05T23:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38745 -->
Innere Hantel der symmetrischen Variante des Differentialgetriebes. Es müssen nur noch zwei Zahnräder Z40 mit Freilaufnaben aufgesetzt werden.
