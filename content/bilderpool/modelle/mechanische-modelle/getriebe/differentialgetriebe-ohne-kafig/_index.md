---
layout: "overview"
title: "Differentialgetriebe ohne Käfig"
date: 2020-02-22T08:19:39+01:00
legacy_id:
- /php/categories/2895
- /categories1fed.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2895 --> 
Um zu verstehen und sichtbar zu machen, wie und warum ein Differentialgetriebe funktioniert, vertauscht man am besten die Rollen des Käfigs und der zentraler Achse(n). Die hier vorgestellten zwei Varianten sind sehr leichtgängig und haben nur wenig Spiel.