---
layout: "image"
title: "Getriebe 1:256"
date: "2007-06-09T14:59:25"
picture: "getriebe3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10767
- /detailsc6ba-2.html
imported:
- "2019"
_4images_image_id: "10767"
_4images_cat_id: "976"
_4images_user_id: "557"
_4images_image_date: "2007-06-09T14:59:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10767 -->
hier beim antriebsrad, damit es nicht durchrutscht