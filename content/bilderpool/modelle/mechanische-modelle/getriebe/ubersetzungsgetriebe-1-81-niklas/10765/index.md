---
layout: "image"
title: "Getriebe 1:256"
date: "2007-06-09T14:59:05"
picture: "getriebe1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10765
- /details7edf-2.html
imported:
- "2019"
_4images_image_id: "10765"
_4images_cat_id: "976"
_4images_user_id: "557"
_4images_image_date: "2007-06-09T14:59:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10765 -->
Hier mal als bewegung zu sehen (belichtungszeit 1/4 sec)