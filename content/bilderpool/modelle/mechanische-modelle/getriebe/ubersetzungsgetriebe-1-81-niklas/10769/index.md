---
layout: "image"
title: "Getriebe 1:256"
date: "2007-06-09T14:59:26"
picture: "getriebe5.jpg"
weight: "5"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10769
- /details7a21.html
imported:
- "2019"
_4images_image_id: "10769"
_4images_cat_id: "976"
_4images_user_id: "557"
_4images_image_date: "2007-06-09T14:59:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10769 -->
hier nochmal die übrsetzung