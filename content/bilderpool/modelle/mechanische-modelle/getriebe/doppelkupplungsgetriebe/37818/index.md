---
layout: "image"
title: "Doppelkupplungsgetriebe 13"
date: "2013-11-11T09:44:44"
picture: "DSCN1028.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/37818
- /detailsdb21.html
imported:
- "2019"
_4images_image_id: "37818"
_4images_cat_id: "2810"
_4images_user_id: "502"
_4images_image_date: "2013-11-11T09:44:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37818 -->
