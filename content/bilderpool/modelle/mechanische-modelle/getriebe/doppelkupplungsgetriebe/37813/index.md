---
layout: "image"
title: "Doppelkupplungsgetriebe 08"
date: "2013-11-11T09:44:43"
picture: "DSCN1016.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/37813
- /detailsc86a.html
imported:
- "2019"
_4images_image_id: "37813"
_4images_cat_id: "2810"
_4images_user_id: "502"
_4images_image_date: "2013-11-11T09:44:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37813 -->
