---
layout: "image"
title: "Detailblick (2)"
date: "2015-12-28T19:08:43"
picture: "rasterkonformeruebertrag3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42629
- /details1901.html
imported:
- "2019"
_4images_image_id: "42629"
_4images_cat_id: "3169"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42629 -->
Der leichte Versatz der Laschen in den BS7,5 ermöglicht das feine Justieren. Das ganze funktioniert nicht nur mit Z10, sondern auch mit größeren Zahnrädern (siehe auch http://www.ftcommunity.de/details.php?image_id=42394 links unten, wo dasselbe mit einem Z30 gemacht wird).
