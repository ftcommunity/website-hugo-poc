---
layout: "image"
title: "Centrifugaal Kupplung"
date: "2010-01-07T08:22:38"
picture: "autotechnik04.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26018
- /detailsdb23-2.html
imported:
- "2019"
_4images_image_id: "26018"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26018 -->
Centrifugaal Kupplung
