---
layout: "image"
title: "Links Centrifugaal Getriebe mit 3 Gänge und eine Freilauf"
date: "2010-01-07T08:22:38"
picture: "autotechnik05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26019
- /detailsce52.html
imported:
- "2019"
_4images_image_id: "26019"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26019 -->
Das Centrifugaal Getriebe hat 3 Gänge und eine Freilauf
