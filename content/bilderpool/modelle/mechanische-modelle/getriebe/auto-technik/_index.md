---
layout: "overview"
title: "Auto-Technik"
date: 2020-02-22T08:19:23+01:00
legacy_id:
- /php/categories/1836
- /categories7b16.html
- /categoriesb9b1.html
- /categories4d84.html
- /categoriesac23.html
- /categories5027.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1836 --> 
- Fischertechnik Kupplung (wie auch im Deutsches Museum München)

- Centrifugaal Kupplung

- Centrifugaal Getriebe mit 3 Gänge und eine Freilauf

- Wechselgetriebe mit 4 Differentialen und 4 Elektromagneten als Kopplung

- ABS-Antiblockiersystem