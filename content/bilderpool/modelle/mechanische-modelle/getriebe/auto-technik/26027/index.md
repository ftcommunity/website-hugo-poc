---
layout: "image"
title: "Wechselgetriebe mit 4 Differentialen und 4 Elektromagneten als Kopplung"
date: "2010-01-07T08:22:39"
picture: "autotechnik13.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26027
- /details97c5-2.html
imported:
- "2019"
_4images_image_id: "26027"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:39"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26027 -->
Gangwechsel ohne "krssss -en"  Problemlos von 1 nach 2 nach 3, und wieder zuruck. 
Selbst von 3 direkt nach R (Ruckwarts) ist keine Problem. 
Nur ein DifferentialDeckel habe ich umgebaut: geklebt mit ein durchbohrter Z-10-Rastritzel. Damit hat die Ruckwarts-Position ein langzames Drehzahl, und bleibt das Wechselgetriebe-Modell Kompakt.

