---
layout: "image"
title: "Fischertechnik Kupplung (wie auch im Deutsches Museum München)"
date: "2010-01-07T08:22:37"
picture: "autotechnik01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26015
- /details0a4e-2.html
imported:
- "2019"
_4images_image_id: "26015"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26015 -->
Fischertechnik Kupplung (wie auch im Deutsches Museum München)
