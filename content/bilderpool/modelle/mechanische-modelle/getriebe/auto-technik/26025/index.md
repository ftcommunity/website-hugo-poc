---
layout: "image"
title: "Centrifugaal Getriebe mit 3 Gänge und eine Freilauf"
date: "2010-01-07T08:22:39"
picture: "autotechnik11.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Speichenrad"]
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26025
- /details7cff.html
imported:
- "2019"
_4images_image_id: "26025"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26025 -->
Detail des Centrifugaal Getriebe mit 3 Gänge und eine Freilauf
Jetzt beim hohen Geschwindigkeit.
