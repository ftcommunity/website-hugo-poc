---
layout: "image"
title: "zylinder"
date: "2017-01-07T20:22:20"
picture: "IMG_1050.jpg"
weight: "5"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45018
- /details0071.html
imported:
- "2019"
_4images_image_id: "45018"
_4images_cat_id: "3349"
_4images_user_id: "1359"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45018 -->
