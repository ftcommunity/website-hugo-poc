---
layout: "image"
title: "Ventilinsel"
date: "2017-01-07T20:22:20"
picture: "IMG_1052.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45014
- /detailsd836-2.html
imported:
- "2019"
_4images_image_id: "45014"
_4images_cat_id: "3349"
_4images_user_id: "1359"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45014 -->
Befindet sich noch im Bau, funktioniert aber prinzipiell. Das schöne bei diesen 3/2 Wege-Ventilen ist, dass der 3. (Abluft)-Anschluss auch nutzbar ist..
