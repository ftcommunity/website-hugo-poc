---
layout: "image"
title: "Gesamtansicht"
date: "2017-01-07T20:22:20"
picture: "IMG_1048.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Pneumatik", "Kugelbahn", "Stufenförderer"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45016
- /details6d7f.html
imported:
- "2019"
_4images_image_id: "45016"
_4images_cat_id: "3349"
_4images_user_id: "1359"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45016 -->
gesamte Kugelbahn, braucht seeehr viel Luft..
