---
layout: "image"
title: "Gesamtansicht"
date: "2012-01-08T18:31:42"
picture: "partyhut1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33861
- /details3ccf.html
imported:
- "2019"
_4images_image_id: "33861"
_4images_cat_id: "2504"
_4images_user_id: "104"
_4images_image_date: "2012-01-08T18:31:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33861 -->
Diesen Hut hatte ich auf einer Geburtstagsparty eines Freundes auf, die unter dem Motto "Hüte!" stand.

Die lange Kardanstrecke geht rechts vom Kopf herunter bis knapp über Hüfthöhe. Mit der linken Hand den schwarzen Griff halten, mit rechts kurbeln, und die beiden Autos (das schwarze fährt nach Belgien, das weiße nach Karlsruhe - aber das ist ein Insider) fahren auf der Kreisbahn, und das Karussell mit dem Herz drehen sich in entgegengesetzter Richtung. Ein- und ausschaltbar blinkt und hupt das Ganze dann noch.

Es gab auch eine Prämierung, und der Hut gewann eine Flasche Prosecco. ;-)
