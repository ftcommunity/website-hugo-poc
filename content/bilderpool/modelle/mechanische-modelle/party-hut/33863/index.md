---
layout: "image"
title: "Oberseite"
date: "2012-01-08T18:31:42"
picture: "partyhut3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33863
- /details9b9c.html
imported:
- "2019"
_4images_image_id: "33863"
_4images_cat_id: "2504"
_4images_user_id: "104"
_4images_image_date: "2012-01-08T18:31:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33863 -->
Hier sieht man den Taster, der von drei BS5 auf der unteren Drehscheibe betätigt wird. Wenn er nicht gedrückt ist, leuchten die vier bunten Lampen am Kopfreifen. Ist er gedrückt, leuchten die Fahrlampen der Autos und die Hupe hupt. Ebenfalls zu sehen ist die Stromzuführung auf den em-Schleifring.
