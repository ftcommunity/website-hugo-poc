---
layout: "image"
title: "Seitenansicht (rechts)"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33930
- /details2e0f-2.html
imported:
- "2019"
_4images_image_id: "33930"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33930 -->
Vor dem nach oben aus dem Chassis ragenden "Schalt-Motor" habe ich den Akku montiert, daher liegt der XS-Motor nicht flach auf dem XM-Motor.