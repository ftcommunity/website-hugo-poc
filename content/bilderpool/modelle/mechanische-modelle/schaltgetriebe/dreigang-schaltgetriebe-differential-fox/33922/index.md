---
layout: "image"
title: "Aufbau des Dreiganggetriebes"
date: "2012-01-14T22:26:03"
picture: "dreigangschaltgetriebemitdifferentialdirkfox03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33922
- /details720f.html
imported:
- "2019"
_4images_image_id: "33922"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33922 -->
Das Getriebe ist links über eine Kette mit dem XM-Motor verbunden (2:1). Die Übersetzung erfolgt durch ein Verschieben der (im Bild unteren) Getriebeachse. Durch die Verwendung des Differentials und der Z10, Z15 und Z20 wird das Getriebe sehr kompakt und passt quer auf die Grundplatte 90x45. Die Verschiebung der Getriebeachse erfolgt durch den senkrechten XS-Motor, der über ein U-Getriebe eine Rastachse mit Schnecke dreht und damit die Schneckenmutter (m=1) mit den beiden Kupplungsstücken - rechts unten im Bild - bewegt.
Die Z15 sind über Riegelscheiben auf der Achse befestigt. Alle Zahnräder müssen stramm sitzen, damit die Motorkraft auch auf der Hinterachse ankommt - wenn eine Nabe nicht gut greift, kann man mit einem dünnen Faden zwischen Achse und Nabe Schlupf verhindern.