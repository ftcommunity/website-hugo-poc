---
layout: "image"
title: "Dritter Gang"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33925
- /detailsefda.html
imported:
- "2019"
_4images_image_id: "33925"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33925 -->
Im dritten Gang erreicht das Getriebe eine Übersetzung von 1:1,5 (der Getriebeteil mit Differential übersetzt 1:3), die Entfaltung liegt bei 38,25 cm.