---
layout: "image"
title: "Erster Gang"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33923
- /details0706.html
imported:
- "2019"
_4images_image_id: "33923"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33923 -->
Im ersten Gang bewirkt der Getriebeteil mit dem Differential eine Untersetzung von 3:1, insgesamt hat das Getriebe also eine Untersetzung von 6:1. Die Entfaltung (zurückgelegte Strecke je Motorumdrehung) liegt mit den Traktorreifen 80 (Umfang mit neuem Profil: 25,5 cm) also bei 4,24 cm.