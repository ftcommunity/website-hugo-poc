---
layout: "image"
title: "Gesamtansicht"
date: "2012-01-14T22:26:03"
picture: "dreigangschaltgetriebemitdifferentialdirkfox01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33920
- /details084f-2.html
imported:
- "2019"
_4images_image_id: "33920"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33920 -->
Ein fernsteuerbares Fahrzeug mit eingebautem Dreiganggetriebe und funktionierender Lenkung (ohne Servo!) stand schon lange auf meiner Wunschliste - meine bisherigen Getriebe-Konstruktionen (siehe z.B. http://www.ftcommunity.de/categories.php?cat_id=2106) waren aber eher Funktionsmodelle und nicht kompakt genug, um in ein halbwegs schlankes ft-Fahrzeugmodell zu passen.