---
layout: "overview"
title: "Dreigang-Schaltgetriebe mit Differential (Dirk Fox)"
date: 2020-02-22T08:17:15+01:00
legacy_id:
- /php/categories/2510
- /categories93eb-2.html
- /categoriesb6f4.html
- /categoriescd61.html
- /categories312b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2510 --> 
Sehr kompaktes Dreigang-Schaltgetriebe mit geringer Bauhöhe unter Verwendung eines Differentialgetriebes in einem lenkbaren, ferngesteuerten Fahrzeug-Chassis auf der Grundplatte 90x45