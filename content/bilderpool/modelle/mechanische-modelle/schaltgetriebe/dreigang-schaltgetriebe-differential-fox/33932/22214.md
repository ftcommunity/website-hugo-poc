---
layout: "comment"
hidden: true
title: "22214"
date: "2016-07-06T22:21:08"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Die Sache mit den Ketten: Auch ich kann bestätigen, daß sie besser laufen, wenn die "rauhe" Seite nicht auf den Zähnen ist. Anderenfalls neigen sie dazu mit den Aufnahmeschlitzen für das nächste Kettenglied auf einer Zahnkrone einzuhaken.