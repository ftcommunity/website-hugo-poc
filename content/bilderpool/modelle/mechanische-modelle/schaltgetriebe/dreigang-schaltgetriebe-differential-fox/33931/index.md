---
layout: "image"
title: "Seitenansicht (links)"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33931
- /details8611.html
imported:
- "2019"
_4images_image_id: "33931"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33931 -->
Da ist noch Luft nach oben für eine Karosserie.