---
layout: "image"
title: "Rückansicht"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33926
- /details01d7.html
imported:
- "2019"
_4images_image_id: "33926"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33926 -->
Blick von hinten auf die Hinterachse.