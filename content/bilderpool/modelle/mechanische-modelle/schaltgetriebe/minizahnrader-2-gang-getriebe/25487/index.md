---
layout: "image"
title: "Rechtes Getriebe 2"
date: "2009-10-03T20:57:27"
picture: "minizahnraedergetriebe5.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
schlagworte: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25487
- /details7fda.html
imported:
- "2019"
_4images_image_id: "25487"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25487 -->
Hier der obere Teil des rechten Getriebes (vom Motor aus gesehen).
