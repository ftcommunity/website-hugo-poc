---
layout: "image"
title: "Schaltung"
date: "2009-10-03T20:57:26"
picture: "minizahnraedergetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
schlagworte: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25484
- /details7a48.html
imported:
- "2019"
_4images_image_id: "25484"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25484 -->
Sicht auf den Umschaltbereich.  Momentan ist es auf des linke Getriebe Geschaltet (man sieht das schwarze Zahnrädchen vom S-Motor).
