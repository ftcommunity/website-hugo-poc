---
layout: "image"
title: "Gesammtansicht"
date: "2009-10-03T20:57:26"
picture: "minizahnraedergetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
schlagworte: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25483
- /details29c0.html
imported:
- "2019"
_4images_image_id: "25483"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25483 -->
Das ganze Getriebe auf einen Blick. es treibt lediglich eine Schneke an, die zum zeigen das es dreht montiert wurde.
