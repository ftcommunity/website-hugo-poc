---
layout: "image"
title: "Linkes Getriebe"
date: "2009-10-03T20:57:26"
picture: "minizahnraedergetriebe3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
schlagworte: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/25485
- /details62bc.html
imported:
- "2019"
_4images_image_id: "25485"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25485 -->
Hier das ganze Getriebe, das vom Motor aus auf der linken Seite ist.
