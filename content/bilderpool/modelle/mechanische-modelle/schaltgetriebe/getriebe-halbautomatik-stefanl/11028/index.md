---
layout: "image"
title: "automatik Getriebe 1"
date: "2007-07-13T12:03:14"
picture: "automatikgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "StefanLehnerer"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11028
- /detailsf577.html
imported:
- "2019"
_4images_image_id: "11028"
_4images_cat_id: "997"
_4images_user_id: "502"
_4images_image_date: "2007-07-13T12:03:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11028 -->
Automatik Getriebe mit 3 Gängen.
