---
layout: "image"
title: "Getriebe mit Bremse"
date: "2007-05-05T08:38:33"
picture: "getriebe1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10283
- /details37cd.html
imported:
- "2019"
_4images_image_id: "10283"
_4images_cat_id: "935"
_4images_user_id: "557"
_4images_image_date: "2007-05-05T08:38:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10283 -->
von oben.Aufklappbare Abdeckung.