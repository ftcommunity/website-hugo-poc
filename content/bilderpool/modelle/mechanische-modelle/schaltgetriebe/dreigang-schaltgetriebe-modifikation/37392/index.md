---
layout: "image"
title: "Leerlauf beim Gangwechsel zwischen 2. und 3. Gang"
date: "2013-09-13T17:16:34"
picture: "Leerlauf2auf3.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37392
- /details841d.html
imported:
- "2019"
_4images_image_id: "37392"
_4images_cat_id: "2779"
_4images_user_id: "1729"
_4images_image_date: "2013-09-13T17:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37392 -->
das selbe wie beim 1. Gangwechsel. 
Durch den Leerlauf schaltet das Getriebe, ohne zu hakeln