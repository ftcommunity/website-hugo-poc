---
layout: "image"
title: "Leerlauf beim Gangwechsel zwischen 1. und 2. Gang"
date: "2013-09-13T17:16:34"
picture: "Leerlauf1auf2.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37391
- /detailsee4b-2.html
imported:
- "2019"
_4images_image_id: "37391"
_4images_cat_id: "2779"
_4images_user_id: "1729"
_4images_image_date: "2013-09-13T17:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37391 -->
durch den Abstand zwischen 1 und 2 können beide Übersetzungen für einen Moment freilaufen.
wenn die Ritzel beider Gänge gleichzeitig greifen, dann ruckelt es, oder das Getriebe verzieht sich (Bauteileklemmung) und mit der Zeit sind die Zahnflanken auch angenagt.