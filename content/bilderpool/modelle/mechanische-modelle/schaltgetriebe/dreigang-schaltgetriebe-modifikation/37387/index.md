---
layout: "image"
title: "Übersicht"
date: "2013-09-13T17:16:33"
picture: "DreiGangGetriebe.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Schaltgetriebe", "3Gang", "Leerlauf"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37387
- /details44a8-2.html
imported:
- "2019"
_4images_image_id: "37387"
_4images_cat_id: "2779"
_4images_user_id: "1729"
_4images_image_date: "2013-09-13T17:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37387 -->
Das reine Getriebe ohne irgendetwas drumherum.
Die untere Achse steht fest, die obere ist verschiebbar zum Wechseln der Gänge.
Für einen echten Einsatz müsste natürlich anstatt der Handkurbeln ein Motor bzw. Räder/Differential verbunden sein.
Beachtet bitte die leicht geänderten Abstände zwischen den Ritzeln im Vergleich zur Originalversion von Dirk Fox