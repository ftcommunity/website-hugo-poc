---
layout: "image"
title: "Rückwärtsgang eingelegt"
date: "2009-03-01T20:05:03"
picture: "ganggetriebemitrueckwaertsgang05.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23305
- /detailsc142.html
imported:
- "2019"
_4images_image_id: "23305"
_4images_cat_id: "1584"
_4images_user_id: "845"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23305 -->
Hier ist die Übersetzung 20.