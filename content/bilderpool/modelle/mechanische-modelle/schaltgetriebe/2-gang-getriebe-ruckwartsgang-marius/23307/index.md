---
layout: "image"
title: "Schalthebel"
date: "2009-03-01T20:05:03"
picture: "ganggetriebemitrueckwaertsgang07.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23307
- /details1c1e.html
imported:
- "2019"
_4images_image_id: "23307"
_4images_cat_id: "1584"
_4images_user_id: "845"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23307 -->
Den Schalthebel habe ich mit einem Gelenkstein, einem 15er-Grundbaustein mit rundem Zapfen und einem Kardangelenk gebaut.