---
layout: "image"
title: "Von der Seite"
date: 2023-01-14T14:25:13+01:00
picture: "Bilder06.jpg"
weight: "6"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Getriebe von der linken Seite.

Ganz vorne im Bild ist der Schalthebel.
Dahinter, wie schon so häufig, das Z40 des ersten Ganges.
Ganz hinten ist die Übersetzung vom Motor zu sehen.
Rechts ist auch noch der Abtrieb/gelbe Platte.


Achtung: 2-Gang