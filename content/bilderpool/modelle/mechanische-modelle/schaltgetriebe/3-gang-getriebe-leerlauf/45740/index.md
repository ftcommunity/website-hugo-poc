---
layout: "image"
title: "Abtrieb"
date: 2023-01-14T14:25:15+01:00
picture: "Bilder04.jpg"
weight: "4"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist der Ausgang/"Abtrieb" (vielleicht heißt es auch ein bisschen anders) zu sehen. Statt der gelben Platte kann man natürlich auch etwas Anderes anschließen.


Achtung: 2-Gang!