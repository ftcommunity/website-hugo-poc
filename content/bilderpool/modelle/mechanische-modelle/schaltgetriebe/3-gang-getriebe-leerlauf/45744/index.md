---
layout: "image"
title: "Erster Gang / Z40"
date: 2023-01-14T14:25:20+01:00
picture: "Bilder15.jpg"
weight: "15"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist noch einmal das Z40 des ersten Ganges. Neben ihm die Kette des zweiten. Ganz hinten ist das Z10 zum Gangeinstellen zu sehen. Gerade ist es in den Leerlauf geschaltet.