---
layout: "image"
title: "Von vorne"
date: 2023-01-14T14:25:09+01:00
picture: "Bilder09.jpg"
weight: "9"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Links ist der Schalthebel.

Ganz vorne links ist das Z40 des ersten Ganges.

Dahinter ist das neue Z30 mit dem Übertragungs-Z20 daneben.
Über die Kette die darüber zum vorderen Z20 neben dem Z40 läuft ist es mit der Abtriebs Welle verbunden. 

Rechts daneben ist die kurze Kette des ersten Ganges.


Anmerkung: Bei diesem und ein paar weiteren Bildern ist der Motor nicht angeschlossen.