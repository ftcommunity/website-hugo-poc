---
layout: "image"
title: "Von oben"
date: 2023-01-14T14:25:23+01:00
picture: "Bilder12.jpg"
weight: "12"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Das leichte Schräglaufen der Kette (im nächsten Bild besser zu sehen) ist nicht so schlimm.

Rechts oben ist noch einmal die Kraftübertragung zu sehen.
Ganz links (teilweise schon abgeschnitten) ist der Schalthebel.

Manchmal sieht es so aus als wären die Bauteile ein bisschen gebogen/schief auf der Platte. Das stimmt nicht, nur durch die Perspektive wirkt es so.