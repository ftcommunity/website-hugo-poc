---
layout: "overview"
title: "3-Gang Getriebe mit Leerlauf"
date: 2023-01-14T14:25:09+01:00
---

Dieses Handschaltgetriebe hat 3 Gänge mit einem Leerlauf.
Es ist nach dem Getriebe im Kasten ADVANCED "Universal 3" gebaut / abgeändert.

Der Leerlauf kann auch mit einem Rückwärtsgang getauscht werden. Er ist ganz links bei der Kraftverteilung (fehlendes Z15).
Alternativ zum Schalthebel kann auch eine Zahnstange mit Motor angebracht werden.
Zum Beispiel für einen ferngesteuerten LKW.


Auf den ersten Bildern (1-7) ist es noch ein 2-Gang Getriebe, der einzige Unterschied zum "Baukasten-Original" ist das statt dem (ich glaube Z30) ein Z40 eingebaut ist. Zusätzlich ist beim 2-Gang noch einmal darauf hingewiesen. Der Leerlauf beim 2-Gang ist das mittige Z15 bei der Kraft-Verteilung.



Anmerkung: Da das hier meine erste Veröffentlichung von Bildern in die Community ist, kann es mit dem Antworten auf die Kommentare etwas dauern. Ich muss nämlich erst einmal herausfinden wie es geht. Tipps und Anleitungen, vor allem dazu werden gerne über die Kommentare                     entgegengenommen.
                    Beim Nachtragen ist es genauso.

Da ich (leider) versucht habe aus dem Getriebe ein 6-Gang zu bauen, es nicht geschafft habe und ich nicht mehr wusste wie es zurück geht habe ich das Getriebe auseinander gebaut.


Ich habe zwei Videos von dem Getriebe gemacht, da ich allerdings über kein YouTube oder sonstiges Filmekonto verfüge und ich mir auch keines erstellen möchte kann ich leider keine Links dafür hereinstellen.
Allerdings waren sie auch nicht so spannend, der Motor drehte nur die Platte in unterschiedlichen Geschwindigkeiten.