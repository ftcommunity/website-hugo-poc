---
layout: "image"
title: "Motor"
date: 2023-01-14T14:25:16+01:00
picture: "Bilder03.jpg"
weight: "3"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist der Motor.

Da ich das Getriebe ursprünglich einmal in einen Liebherr LTM 11200 9.1 einbauen wollte hat es zusätzlich noch eine Übersetzung von einem Z10 zu einem Z40 (4:1) verpasst bekommen.