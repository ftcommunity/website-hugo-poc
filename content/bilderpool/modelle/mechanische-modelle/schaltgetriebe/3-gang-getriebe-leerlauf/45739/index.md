---
layout: "image"
title: "Zahnrad 1. Gang"
date: 2023-01-14T14:25:14+01:00
picture: "Bilder05.jpg"
weight: "5"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Das große Z40 in der Mitte ist für den ersten Gang zuständig.


Achtung: 2-Gang!