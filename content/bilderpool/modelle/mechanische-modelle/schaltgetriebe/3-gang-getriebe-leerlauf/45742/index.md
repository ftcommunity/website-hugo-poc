---
layout: "image"
title: "Getriebe von oben"
date: 2023-01-14T14:25:18+01:00
picture: "Bilder02.jpg"
weight: "2"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Getriebe noch einmal von oben zu sehen.

Rechts oben ist die "Verteilung": Die drei Z15 werden vom Motor angetrieben. Davor (in diesem Bild nicht so gut zu sehen) ist ein Z10. Das überträgt mit dem Z10 links auf der Achse die Kraft auf die weiteren Zahnräder.

Links unten ist das Z40, also der erste Gang (gerade eingelegt)
Rechts ist der zweite Gang.



Achtung: 2-Gang!