---
layout: "image"
title: "Von der rechten Seite"
date: 2023-01-14T14:25:26+01:00
picture: "Bilder10.jpg"
weight: "10"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Links oben ist ein Z20 des ersten Ganges.
Schräg darunter sind mehrere Z10 die die Kraft auf die Abtriebswelle "leiten".

Ganz rechts ist der Antrieb zu sehen (da kommt der Motor dran).