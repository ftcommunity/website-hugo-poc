---
layout: "image"
title: "Zweite Kette"
date: 2023-01-14T14:25:21+01:00
picture: "Bilder14.jpg"
weight: "14"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die zweite Kette.
Sie ist für den dritten Gang zuständig.
Wofür genau weiß ich aber ehrlich gesagt nicht mehr.