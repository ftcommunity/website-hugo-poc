---
layout: "image"
title: "2-Gang Getriebe"
date: 2023-01-14T14:25:27+01:00
picture: "Bilder01.jpg"
weight: "1"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Bild zeigt das 2-Gang Getriebe von vorne.
Rechts ist der Motor zu sehen.


Achtung: 2-Gang!