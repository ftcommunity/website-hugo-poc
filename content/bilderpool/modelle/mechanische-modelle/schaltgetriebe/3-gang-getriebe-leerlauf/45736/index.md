---
layout: "image"
title: "Von der linken Seite"
date: 2023-01-14T14:25:11+01:00
picture: "Bilder08.jpg"
weight: "8"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Direkt eine Anmerkung: Hiervor waren noch einmal ein paar Bilder vom Motor, da sie aber das gleiche wie beim zwei-Gang Getriebe darstellten habe ich sie bereits im Publisher herausgenommen.


Jetzt zur Beschreibung des Bildes:
Hier ist das Getriebe von der linken Seite zu sehen.
Ganz vorne ist natürlich wieder der Schalthebel, da sich dort aber so gut wie nichts geändert hat möchte ich nicht weiter auf in eingehen.

Rechts hat sich mal wieder das Zahnrad des ersten Gangs hereingeschlichen, da sich dort allerdings auch nichts geändert hat erzähle ich über ihn auch nicht weiter.

Links oben ist das Z30 des zweiten Ganges. So ein bisschen sieht man die rote Kette die das Übertragungs Z20 mit dem neben dem Z40 verbindet. Im folgenden Bild sieht man es besser.