---
layout: "image"
title: "Von hinten"
date: 2023-01-14T14:25:24+01:00
picture: "Bilder11.jpg"
weight: "11"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Anmerkung(en): - (Aber nein, ich "konnte" es ja nicht umdrehen)
                          -Das blaue im Hintergrund ist mein Schreibtischstuhl.
                          -Ich weiß nicht mehr wie ich diese Aufnahme gemacht habe!


Bei diesem Bild schreibe ich mal nicht so viel, weil die Funktionen der einzelnen Bauteile schon erklärt wurden.