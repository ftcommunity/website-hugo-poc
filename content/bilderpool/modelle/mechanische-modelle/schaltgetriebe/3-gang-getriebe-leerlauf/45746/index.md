---
layout: "image"
title: "Kette"
date: 2023-01-14T14:25:22+01:00
picture: "Bilder13.jpg"
weight: "13"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Mitten im Bild ist hier die Kette, wie man sieht ein bisschen schräg. Wie ich aber schon im vorherigen Bild gesagt habe ist das nicht schlimm.
Sie verläuft vom Z20 oben, neben dem Z30, über ein Z10 als "Ablenkung" (damit das Schalt-Übertragungs-Z10 darunter herkommt) zum Z20 unten, neben dem Z40, und wieder zurück.
Sie dient zur Übertragung der Kraft des zweiten Ganges auf die Abtriebs Welle.