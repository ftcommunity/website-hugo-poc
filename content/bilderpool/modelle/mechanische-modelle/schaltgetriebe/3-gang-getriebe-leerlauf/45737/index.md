---
layout: "image"
title: "3-Gang Getriebe"
date: 2023-01-14T14:25:12+01:00
picture: "Bilder07.jpg"
weight: "7"
konstrukteure: 
- "Daniel (336025)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Und hier kommt es endlich: DAS 3-GANG GETRIEBE

Ansicht: Von vorne.


Hier ist fast nichts mehr beim alten:

 -Das Getriebe ist stabiler gebaut
 -Es funktioniert besser
 -Es hat einen Gang mehr
 -Die ganzen Ketten sind für den zweiten Gang
 -Den zweiten Gang übernimmt ein Z30
 (-Den dritten Gang übernimmt ein Z15)