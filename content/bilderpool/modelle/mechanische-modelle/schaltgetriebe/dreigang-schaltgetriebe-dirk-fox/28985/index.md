---
layout: "image"
title: "Dreigang-Schaltgetriebe: 2. Gang"
date: "2010-10-11T18:08:07"
picture: "dreigangschaltgetriebe5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28985
- /details05c5.html
imported:
- "2019"
_4images_image_id: "28985"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28985 -->
Im zweiten Gang erfolgt eine 1:1-Übersetzung (über Z20-Zahnräder). Die Entfaltung liegt hier bei 21 cm.