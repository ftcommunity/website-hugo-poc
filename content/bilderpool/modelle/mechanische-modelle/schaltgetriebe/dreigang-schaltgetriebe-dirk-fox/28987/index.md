---
layout: "image"
title: "Detailansicht Schaltmechanismus"
date: "2010-10-12T13:41:45"
picture: "Detailansicht_Schiebemechanismus.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28987
- /details70ad.html
imported:
- "2019"
_4images_image_id: "28987"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-12T13:41:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28987 -->
Nach zahlreichen Experimenten (mit anschließenden Praxistests meiner Söhne im exzessiven Fahrbetrieb ;-)) hat sich der Mini-Motor mit einem Stück Zahnstange 30, stabilisiert durch eine Führungsstange, als der robusteste Antrieb für die Schaltung erwiesen. Anders als das ursprünglich verwendete Hubgetriebe verzieht sich die Konstruktion auch nicht bei Übersteuerung.