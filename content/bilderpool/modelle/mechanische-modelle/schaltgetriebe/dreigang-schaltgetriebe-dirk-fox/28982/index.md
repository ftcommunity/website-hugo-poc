---
layout: "image"
title: "Dreigang-Schaltgetriebe, Aufsicht"
date: "2010-10-11T18:08:06"
picture: "dreigangschaltgetriebe2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28982
- /details3c16-2.html
imported:
- "2019"
_4images_image_id: "28982"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28982 -->
Antrieb: Die Antriebsachse wird von einem XM-Motor über vier Z30-Zahnräder angetrieben, die sich beim Schalten gegen den Motor verschieben; die vier Zahnräder sind mit drei 30er-Achsen miteinander verbunden. Die Vorderachse ist über eine Kette und zwei Z10-Zahnräder mit der Abtriebsache verbunden.
Robustheit: Das Modell verwendet ausschließlich Metallachsen. Alle Achslager wurden verstärkt - da wackelt auch im höchsten Gang nichts.
