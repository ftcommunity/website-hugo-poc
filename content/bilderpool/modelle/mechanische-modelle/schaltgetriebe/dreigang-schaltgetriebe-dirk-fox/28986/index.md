---
layout: "image"
title: "Dreigang-Schaltgetriebe: 1. Gang"
date: "2010-10-11T18:08:07"
picture: "dreigangschaltgetriebe6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28986
- /details1556.html
imported:
- "2019"
_4images_image_id: "28986"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28986 -->
Im ersten Gang wird Z30 auf Z10 geschaltet; damit liegt die Entfaltung bei einer Umdrehung der Antriebsachse bei 7 cm.