---
layout: "comment"
hidden: true
title: "12636"
date: "2010-10-29T00:03:17"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
die 4xZ20 gehen in den 2/3D-Ansichten schon in Ordnung. Sie sind auch mit Art-Nr. und Stückzahl 4 richtig in der "Stückliste", jedoch fehlt hier die Abbildung Z20 ?
Die Kinematik hat 3 ROT-Bindungen: 1. ROT Schnecke - Z30. An dieses Z30 sind alle Teile der Schaltachse fest zu binden, 2. ROT Schaltzahnräderpaar je nach Gang. An das hier getriebene Zahnrad sind alle Teile dieser Achse fest zu binden. Da in der Version 1.0.6 der Kettentrieb noch nicht realisierbar ist, besteht die 3. ROT aus dem getriebenen Zahnrad der 2.ROT mit dem Kettenrad der Räderachse. Alle Teile der Räderachse sind dann fest an das zweite Kettenrad der 3.ROT zu binden ...
Gruss, Ingo