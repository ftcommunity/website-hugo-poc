---
layout: "comment"
hidden: true
title: "12667"
date: "2010-10-31T19:32:13"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
komme erst in den nächsten Tagen dazu, mir das wegen evtl. Bugs mal näher anzusehen. Schau bitte mal in die Orignaldateien vor der Komprimierung in ZIP. Das Fehlen der Z20-Abbildung in der Bauteilellist könnte einer sein. Falsche Drehrichtungen einzelner Zahnräder innerhalb von Rädergetrieben sind mir nicht neu. Ihre Korrektur war stets sehr kreativ ...
Die "Kinematik-Protokolle" der entpackten ftm-Dateien (3 Gänge) lassen sich übrigens auslesen, wie du das in der Reihenfolge gemacht hast. Und da sind allerdings Abweichungen vom Weg, den ich hier mal als eine Variante öffentlich angedacht habe :o)
Gruss, Ingo