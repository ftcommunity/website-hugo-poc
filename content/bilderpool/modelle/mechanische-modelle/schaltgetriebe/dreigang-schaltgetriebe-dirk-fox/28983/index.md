---
layout: "image"
title: "Dreigang-Schaltgetriebe, Seitenansicht (Bauhöhe)"
date: "2010-10-11T18:08:07"
picture: "dreigangschaltgetriebe3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28983
- /detailsbaf8.html
imported:
- "2019"
_4images_image_id: "28983"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28983 -->
Die Seitenansicht zeigt die (relativ) niedrige Bauhöhe des Getriebes. Die ließ sich nur durch einen Verzicht auf Z40-Zahnräder und eine Beschränkung auf zwei Achsen erreichen. Mit einem Fahrgestell statt der Bauplatte kann man die Bodenfreiheit des Fahrzeugs erhöhen.