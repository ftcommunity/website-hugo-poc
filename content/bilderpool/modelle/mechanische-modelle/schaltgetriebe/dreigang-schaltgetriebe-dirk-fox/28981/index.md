---
layout: "image"
title: "Dreigang-Schaltgetriebe (Gesamtansicht, Unterseite)"
date: "2010-10-11T18:08:05"
picture: "dreigangschaltgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28981
- /details1093.html
imported:
- "2019"
_4images_image_id: "28981"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28981 -->
Dreigang-Schiebe-Schaltgetriebe mit zwei Achsen (bewegliche Antriebswelle und fixe Abtriebswelle). Antrieb und Schaltung werden über die Fernbedienung gesteuert.
Das Modell wurde mit einer niedrigen Bauhöhe, hoher Stabilität und Robustheit bei möglichst geringem Bauteileinsatz (Gewicht) konzipiert; daher eignet sich der Aufbau als "Chassis" eines Fahrzeugs (z.B. mit einem Fahrgestell statt der Bauplatte).