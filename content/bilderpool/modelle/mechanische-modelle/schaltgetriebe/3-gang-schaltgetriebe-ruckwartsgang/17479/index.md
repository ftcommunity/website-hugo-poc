---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 11"
date: "2009-02-21T16:56:56"
picture: "porschemakus11.jpg"
weight: "11"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17479
- /details6f73.html
imported:
- "2019"
_4images_image_id: "17479"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17479 -->
