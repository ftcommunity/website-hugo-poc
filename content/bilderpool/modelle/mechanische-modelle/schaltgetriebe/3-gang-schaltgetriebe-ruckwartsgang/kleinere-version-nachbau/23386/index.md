---
layout: "image"
title: "Detail_Motor"
date: "2009-03-06T19:34:34"
picture: "g2.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23386
- /detailsd56e-2.html
imported:
- "2019"
_4images_image_id: "23386"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23386 -->
Hier sieht man den Antriebsmotor
Es sieht so aus als ob sich die Halterung des Motors und die Befestigung des Zahnrades sich berühren würden; das liegt aber an der Optik, in Wirklichkeit sind dort 3-4mm Abstand