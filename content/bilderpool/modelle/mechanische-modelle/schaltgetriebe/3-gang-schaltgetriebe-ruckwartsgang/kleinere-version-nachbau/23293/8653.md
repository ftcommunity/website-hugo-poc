---
layout: "comment"
hidden: true
title: "8653"
date: "2009-03-03T14:43:49"
uploadBy:
- "Mr Smith"
license: "unknown"
imported:
- "2019"
---
Ich bau grad noch weiter, werde bald eine aktuelle Version hochladen. 
Der "Abrieb" ist Graphitpulver, da ist mir leider eine Flasche  umgekippt :(
Das Zahnrad ganz links ist fest mit der Achse verbunden, dann kommen 2 lose, dann wieder ein festes und das letzte ist wieder lose (mit einer Freilaufnabe, sieht man leider nicht)

Zum Glück schleift nichts, auch wenn die Abstände recht knapp sind