---
layout: "image"
title: "Detailansicht2"
date: "2009-03-01T20:05:03"
picture: "DSC00649.jpg"
weight: "7"
konstrukteure: 
- "ich"
fotografen:
- "ich"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23293
- /detailsa69a.html
imported:
- "2019"
_4images_image_id: "23293"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23293 -->
Die Mittelwelle, leider etwas unscharf geworden.