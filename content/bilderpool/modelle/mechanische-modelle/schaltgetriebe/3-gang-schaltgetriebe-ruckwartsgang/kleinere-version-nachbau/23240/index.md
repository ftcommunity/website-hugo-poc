---
layout: "image"
title: "Draufsicht"
date: "2009-02-28T10:27:06"
picture: "dsc00644.jpg"
weight: "2"
konstrukteure: 
- "ich"
fotografen:
- "ich"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23240
- /detailsc32f.html
imported:
- "2019"
_4images_image_id: "23240"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-02-28T10:27:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23240 -->
