---
layout: "image"
title: "Seite"
date: "2009-03-01T20:05:02"
picture: "DSC00651.jpg"
weight: "6"
konstrukteure: 
- "ich"
fotografen:
- "ich"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23292
- /details6c6f-2.html
imported:
- "2019"
_4images_image_id: "23292"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-01T20:05:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23292 -->
Hier sieht man die Achsen. 
ganz rechts der MiniMotor für die Gangschaltung. Dann kommt die Eingangswelle.
Danach die Hauptachse und Gang1.
Ganz links ist Gang2