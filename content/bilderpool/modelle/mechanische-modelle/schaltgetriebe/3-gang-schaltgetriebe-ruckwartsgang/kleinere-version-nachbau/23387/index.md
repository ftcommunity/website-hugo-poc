---
layout: "image"
title: "Unten"
date: "2009-03-06T19:34:34"
picture: "g3.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/23387
- /detailsef64-4.html
imported:
- "2019"
_4images_image_id: "23387"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23387 -->
ansicht von unten. 

oben: Gang 2
Mitte: Mittelwelle
Unten: Abnehmer
