---
layout: "comment"
hidden: true
title: "8575"
date: "2009-02-21T23:18:13"
uploadBy:
- "Porsche-Makus"
license: "unknown"
imported:
- "2019"
---
Noch was zum Rückwärtsgang:
das war am Schluss das Wenigste. Ich glaube, die meisten Getriebebauer vernachlässigen den Rückwärtsgang, weil es viel einfacher ist, den Motor umzupolen und von daher ja auch unnötig. Bei einem Automotor ist das nicht ganz so einfach ;-)