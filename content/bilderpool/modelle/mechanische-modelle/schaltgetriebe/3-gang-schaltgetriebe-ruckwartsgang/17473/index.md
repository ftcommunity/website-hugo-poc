---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 05"
date: "2009-02-21T16:56:56"
picture: "porschemakus05.jpg"
weight: "5"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17473
- /details675b-2.html
imported:
- "2019"
_4images_image_id: "17473"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17473 -->
Links unten im Bild ist die konventionelle Verschiebung der Getriebeausgangswelle über Schnecken und Schneckenmutter zu sehen.

Wollte man an die Getriebeausgangswelle noch ein Differential befestigen, müsste man natürlich den beim Schaltvorgang entstehenden Längenunterschied der Welle entsprechend ausgleichen, z.B. über Stangen in Bausteinen.