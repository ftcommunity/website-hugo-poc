---
layout: "image"
title: "20090223_3-Gang_Schaltgetriebe_mit_Rückwärtsgang_kompakter_03"
date: "2009-02-23T20:21:40"
picture: "ganggetriebekompakter3.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17495
- /details3edb.html
imported:
- "2019"
_4images_image_id: "17495"
_4images_cat_id: "1571"
_4images_user_id: "327"
_4images_image_date: "2009-02-23T20:21:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17495 -->
Gesamtansicht des Getriebes in Aktion