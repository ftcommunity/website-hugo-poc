---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 07"
date: "2009-02-21T16:56:56"
picture: "porschemakus07.jpg"
weight: "7"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17475
- /details4d36.html
imported:
- "2019"
_4images_image_id: "17475"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17475 -->
