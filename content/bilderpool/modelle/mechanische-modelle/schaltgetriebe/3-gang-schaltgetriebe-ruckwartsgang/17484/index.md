---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 16"
date: "2009-02-21T16:56:56"
picture: "porschemakus16.jpg"
weight: "16"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17484
- /details5404.html
imported:
- "2019"
_4images_image_id: "17484"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17484 -->
Zweiter Gang eingelegt (zweites Z20 von links)