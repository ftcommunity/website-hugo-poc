---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 02"
date: "2009-02-21T16:56:56"
picture: "porschemakus02.jpg"
weight: "2"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17470
- /details94eb.html
imported:
- "2019"
_4images_image_id: "17470"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17470 -->
Prinzipiell kann man dieses Getriebe sehr einfach um ein paar Gänge erweitern, indem man einfach alle Achsen und rundherum die entsprechenden Getriebezwischenwellen mit den Zahnrädern platziert.