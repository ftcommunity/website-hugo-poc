---
layout: "image"
title: "Gesamtansicht"
date: "2013-09-15T15:47:05"
picture: "Gesamtansicht.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/37393
- /detailsac49.html
imported:
- "2019"
_4images_image_id: "37393"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37393 -->
Mit dem Ganghebel verschiebt man den Schaltschlitten. 1. und 3. Gang sind jeweils am rechten und linken Anschlag. Der mittlere 2. Gang muß "erfühlt" werden.

Die erste Stufe des Getriebes sind die beiden vorderen Achsen in der Ansicht. An der unteren, vorderen Achse erfolgt die Krafteinleitung. Dort könnte man einen Motor anflanschen.
Die zweite Stufe des Getriebes sind die beiden hinteren Achsen im Bild. Die untere, hintere Achse ist zum Antrieb der Räder gedacht.

Die beiden Ritzel mit den schwarzen Zahnkränzen haben eine Doppelfunktion. Zum Einen sind sie für die 1:1 Übertragung in der jeweiligen Getriebestufe, zum Anderen übertragen sie die Kraft von der ersten auf die zweite Getriebestufe.
 
Die beiden Getriebestufen sind entgegengesetzt zueinender angeordnet. Die vordere Stufe hat die kleine Übersetzung rechts, die große links und die hintere Stufe entsprechend umgekehrt. Dadurch ist es möglich, durch gemeinsames Verschieben der Achsen im Schaltschlitten herauf- bzw. herunterzuschalten.

Die Ritzel der beiden Getriebestufen sind ineinander geschachtelt, ohne sich zu berühren (außer die Verbindung über die schwarzen Ritzel). Dadurch spart man Baubreite ein.