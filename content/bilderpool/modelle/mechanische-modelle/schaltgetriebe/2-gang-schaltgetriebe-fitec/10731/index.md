---
layout: "image"
title: "2-Gang-Schaltgetriebe"
date: "2007-06-05T18:53:59"
picture: "2-Gang-Getriebe4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10731
- /detailsc671-2.html
imported:
- "2019"
_4images_image_id: "10731"
_4images_cat_id: "972"
_4images_user_id: "456"
_4images_image_date: "2007-06-05T18:53:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10731 -->
Der Mini-Motor treibt die Schnecke an, welche zwischen den 2 Gängen schaltet.
