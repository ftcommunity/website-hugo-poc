---
layout: "image"
title: "Getriebefahrzeug"
date: "2007-06-07T22:28:03"
picture: "Getriebefahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
schlagworte: ["Fahrzeug", "Getriebe", "Gänge"]
uploadBy: "Wolly2.0"
license: "unknown"
legacy_id:
- /php/details/10734
- /details5046.html
imported:
- "2019"
_4images_image_id: "10734"
_4images_cat_id: "973"
_4images_user_id: "570"
_4images_image_date: "2007-06-07T22:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10734 -->
Das besagte Getriebefahrzeug mit 4 Gängen: 2 vom IR-Control mal 2 vom mechanischen Getriebe