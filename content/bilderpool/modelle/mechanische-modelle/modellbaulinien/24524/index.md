---
layout: "image"
title: "Balkenwaage in Professional-Line"
date: "2009-07-09T15:50:32"
picture: "IMG_1336b.jpg"
weight: "3"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24524
- /details8a87.html
imported:
- "2019"
_4images_image_id: "24524"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T15:50:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24524 -->
In der Professional-Line werden ausschließlich schwarze (Grundbausteine und Statikteile) und rote Bauteile verbaut. Auf Metallachsen sollte verzichtet werden.