---
layout: "image"
title: "Balkenwaage in Color-Line"
date: "2009-07-09T15:50:32"
picture: "IMG_1346b.jpg"
weight: "4"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24525
- /detailsa812-2.html
imported:
- "2019"
_4images_image_id: "24525"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T15:50:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24525 -->
In der Color-Line werden ausschließlich bunte Bauteile (rot, gelb, blau) verbaut

Absolutes-No-Go: Graue und schwarze Bauteile.