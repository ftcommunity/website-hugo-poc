---
layout: "image"
title: "Rührmaschine in Professional-Line"
date: "2009-07-09T17:06:57"
picture: "IMG_1412b.jpg"
weight: "10"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24531
- /detailscd8a-2.html
imported:
- "2019"
_4images_image_id: "24531"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T17:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24531 -->
Nachbau der Rühr- bzw. Küchenmaschine in Professional-Line mit ausschließlich schwarzen und roten Bauteilen.