---
layout: "image"
title: "Rührmaschine in Actual-Line"
date: "2009-07-09T17:02:19"
picture: "IMG_1401b.jpg"
weight: "9"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- /php/details/24530
- /detailsa1a1.html
imported:
- "2019"
_4images_image_id: "24530"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T17:02:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24530 -->
Rühr- bzw. Küchenmaschine aus der Bauanleitung des Universal-II-Baukastens.