---
layout: "image"
title: "drehantrieb"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer14.jpg"
weight: "14"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25831
- /details19a7-2.html
imported:
- "2019"
_4images_image_id: "25831"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25831 -->
antrieb zum drehen