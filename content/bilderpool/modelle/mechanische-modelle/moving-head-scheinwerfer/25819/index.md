---
layout: "image"
title: "Temperaturanzeige"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer02.jpg"
weight: "2"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25819
- /details4384-2.html
imported:
- "2019"
_4images_image_id: "25819"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25819 -->
die zwei lampen zeigen die Temperatur des Scheinwerfers an
rot=<28°C
grün=>28°C