---
layout: "image"
title: "positionsmesser"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer16.jpg"
weight: "16"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25833
- /details505b.html
imported:
- "2019"
_4images_image_id: "25833"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25833 -->
zähler für die possition