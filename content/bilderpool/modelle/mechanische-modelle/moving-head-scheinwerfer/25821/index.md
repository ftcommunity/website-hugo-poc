---
layout: "image"
title: "von oben"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer04.jpg"
weight: "4"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25821
- /details076d.html
imported:
- "2019"
_4images_image_id: "25821"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25821 -->
ansicht von oben