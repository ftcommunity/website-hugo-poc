---
layout: "image"
title: "betrieb"
date: "2009-11-28T14:30:57"
picture: "movingheadmitscheinwerfer18.jpg"
weight: "18"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25835
- /detailse219.html
imported:
- "2019"
_4images_image_id: "25835"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25835 -->
moving head im betrieb