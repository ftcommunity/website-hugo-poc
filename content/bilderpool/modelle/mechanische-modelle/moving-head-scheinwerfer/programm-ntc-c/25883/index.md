---
layout: "image"
title: "Programm ntc->C"
date: "2009-12-02T16:48:59"
picture: "programm1.jpg"
weight: "1"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25883
- /detailsaacd.html
imported:
- "2019"
_4images_image_id: "25883"
_4images_cat_id: "1819"
_4images_user_id: "1026"
_4images_image_date: "2009-12-02T16:48:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25883 -->
Programm zum umrechnen von ntc zur temperatur