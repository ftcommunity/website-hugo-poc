---
layout: "image"
title: "lamellen geschlossen"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer13.jpg"
weight: "13"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- /php/details/25830
- /details3c2e.html
imported:
- "2019"
_4images_image_id: "25830"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25830 -->
ansicht der geschlossenen lammelen