---
layout: "image"
title: "Die Räder"
date: "2011-03-30T16:49:54"
picture: "IMG_4065_small.jpg"
weight: "1"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- /php/details/30337
- /details297b.html
imported:
- "2019"
_4images_image_id: "30337"
_4images_cat_id: "2257"
_4images_user_id: "986"
_4images_image_date: "2011-03-30T16:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30337 -->
Mit diesem Katapult kann man sogenannte Softairkugeln (Ich hatte nie eine Softair, werde mir auch sicher nie ein kaufen, die Kugeln sind halt nur für "experimetelle Zwecke" recht praktisch :P) schießen kann. Reichweite liegt bei ~10m.