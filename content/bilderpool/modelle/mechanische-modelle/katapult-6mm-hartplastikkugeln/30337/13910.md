---
layout: "comment"
hidden: true
title: "13910"
date: "2011-03-30T17:49:13"
uploadBy:
- "Matthias10"
license: "unknown"
imported:
- "2019"
---
Hallo Simixus
Schönes Modell!
Ich hatte vor ein paar Jahren die gleiche Idee, nur habe ich die Räder einfacher gebaut und statt Hartplastikkugeln einen Papierflieger genommen. Das kannst du ja auch mal ausprobieren, klappt super. 
Ich finde es sehr interessant, dass du genau die gleiche Idee hast wie ich damals : )
Weiter so

Gruß
Matthias