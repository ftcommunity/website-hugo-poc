---
layout: "image"
title: "Stövchen von vorne"
date: "2013-12-26T15:23:58"
picture: "stoevchenVonVorne_800x.jpg"
weight: "2"
konstrukteure: 
- "axel"
fotografen:
- "axel"
uploadBy: "axel"
license: "unknown"
legacy_id:
- /php/details/37942
- /details87f5.html
imported:
- "2019"
_4images_image_id: "37942"
_4images_cat_id: "2824"
_4images_user_id: "2056"
_4images_image_date: "2013-12-26T15:23:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37942 -->
