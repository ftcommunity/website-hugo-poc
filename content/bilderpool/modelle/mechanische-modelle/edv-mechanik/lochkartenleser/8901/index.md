---
layout: "image"
title: "Sensor Einheit"
date: "2007-02-10T15:13:34"
picture: "lochkartenleser6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8901
- /details077a.html
imported:
- "2019"
_4images_image_id: "8901"
_4images_cat_id: "807"
_4images_user_id: "453"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8901 -->
Hier sie man die Halterung von oben.
