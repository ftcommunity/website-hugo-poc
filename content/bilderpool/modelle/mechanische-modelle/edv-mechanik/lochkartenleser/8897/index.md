---
layout: "image"
title: "Lochstreifenleser"
date: "2007-02-10T15:13:34"
picture: "lochkartenleser2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8897
- /detailse431.html
imported:
- "2019"
_4images_image_id: "8897"
_4images_cat_id: "807"
_4images_user_id: "453"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8897 -->
Gesamtansicht
