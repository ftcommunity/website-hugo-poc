---
layout: "image"
title: "Siebensegmentanzeige - Ziffer C"
date: "2004-04-05T18:47:45"
picture: "14_-_ZifferC.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2329
- /details1fea.html
imported:
- "2019"
_4images_image_id: "2329"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2329 -->
