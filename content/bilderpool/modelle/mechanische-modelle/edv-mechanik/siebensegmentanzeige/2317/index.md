---
layout: "image"
title: "Siebensegmentanzeige"
date: "2004-04-05T18:47:25"
picture: "01_-_Siebensegmentanzeige.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Siebensegmentanzeige", "Anzeige", "Display"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2317
- /details76c5.html
imported:
- "2019"
_4images_image_id: "2317"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2317 -->
Die vier Taster rechts vorne werden in beliebiger Komibination gedrückt. Der Motor läuft so lange, bis das durch die vier linken (großen) Taster an der Schaltwalze abgetastete Bitmuster damit übereinstimmt. Die sieben (kleinen) Taster ergeben dann zum als Binärzahl interpretierten Bitmuster je ein Signal für eines der sieben Segmente der Anzeige zur Anzeige von 0123456789AbCdEF.
