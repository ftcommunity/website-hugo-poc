---
layout: "image"
title: "Siebensegmentanzeige - Ziffer 0"
date: "2004-04-05T18:47:26"
picture: "02_-_Ziffer0.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/2318
- /details21b1.html
imported:
- "2019"
_4images_image_id: "2318"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2318 -->
Eine nicht ganz lichtdichte Plastikfolie dient zur Verbesserung (ja, man könnte sie auch zurechtschneiden und fest anbringen).