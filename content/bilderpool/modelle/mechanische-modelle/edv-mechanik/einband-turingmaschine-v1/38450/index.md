---
layout: "image"
title: "Lese-/Schreibkopf"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38450
- /details96a9.html
imported:
- "2019"
_4images_image_id: "38450"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38450 -->
Lese-/Schreibkopf Details: siehe weitere Bilder