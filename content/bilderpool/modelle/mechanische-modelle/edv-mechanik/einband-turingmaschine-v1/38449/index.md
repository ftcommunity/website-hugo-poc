---
layout: "image"
title: "Turingband Antrieb"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38449
- /details11e7.html
imported:
- "2019"
_4images_image_id: "38449"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38449 -->
Turingband Antrieb {links, halt, rechts}