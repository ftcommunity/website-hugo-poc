---
layout: "image"
title: "Übersicht - In Aktion"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev9.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- /php/details/38456
- /details6168.html
imported:
- "2019"
_4images_image_id: "38456"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38456 -->
In Aktion - die Bewegung sieht man nicht ...

V1 - Probleme: 

- Der Hub des Alu-Schlittens ist etwas zu gering.(es klappt aber etwas mehr Hub wäre schön)
- Der Schlitten hakt etwas => besser bauen
- Mechanische Entkopplung der Fotowiderstände und Lampen vom Schlitten (dann wackelt auch nichts)
- Der eine "alte" Fotowiderstand reagiert manchmal (Umgebungslicht?) zu träge.

V2 - 2Do:
- SW: Übergangstabellen für Multiplikation, Subtraktion usw. (RoboPro?)
- Turingband verlängern.