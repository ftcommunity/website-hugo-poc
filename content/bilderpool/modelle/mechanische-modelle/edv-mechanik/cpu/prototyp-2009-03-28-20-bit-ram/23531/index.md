---
layout: "image"
title: "Schreiben eines Bits (2)"
date: "2009-03-28T16:15:47"
picture: "ram6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23531
- /details87b4.html
imported:
- "2019"
_4images_image_id: "23531"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23531 -->
Beim Weiterdrehen wird der Baustein gekippt.
