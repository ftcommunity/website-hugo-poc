---
layout: "image"
title: "Schreiben eines Bits (1)"
date: "2009-03-28T16:15:47"
picture: "ram5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23530
- /detailse599.html
imported:
- "2019"
_4images_image_id: "23530"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23530 -->
Der Wedel dreht sich hier im Uhrzeigersinn, um den durch die aktuelle Position der Bausteinreihe gerade adressierten BS15 nach links zu kippen (ob das dann eine logische 1 oder 0 sein soll, ist Definitionssache).
