---
layout: "image"
title: "Adressierung des gewünschten Bits"
date: "2009-03-28T16:15:46"
picture: "ram2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23527
- /details754f.html
imported:
- "2019"
_4images_image_id: "23527"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23527 -->
Die lange Bausteinstange kann von diesem PowerMotor so nach links und rechts verschoben werden, dass das gewünschte Bit, sprich der gewünschte Baustein 15, in der Schreib-/Leseeinheit in Zugriff steht. Der Taster rechts dient der Initialisierung. Wird der nicht mehr von der Zahnstange gedrückt, ist das Bit Nummer 0 erreicht.
