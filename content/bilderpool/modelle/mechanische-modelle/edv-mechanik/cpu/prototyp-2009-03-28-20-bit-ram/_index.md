---
layout: "overview"
title: "Prototyp 2009-03-28 - 20 Bit RAM"
date: 2020-02-22T08:20:17+01:00
legacy_id:
- /php/categories/1606
- /categories8827.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1606 --> 
Das sind die ersten zaghaften Versuche, Teile der geplanten fischertechnik \"NANO\" CPU zu realisieren. Hier ein Stück Arbeitsspeicher mit sage und schreibe 20 Bit Kapazität. Siehe auch im Forum unter http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3409.