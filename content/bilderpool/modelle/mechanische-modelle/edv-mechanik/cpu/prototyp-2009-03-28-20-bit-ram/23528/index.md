---
layout: "image"
title: "Schreib-/Leseeinheit (1)"
date: "2009-03-28T16:15:46"
picture: "ram3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23528
- /details4e25.html
imported:
- "2019"
_4images_image_id: "23528"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23528 -->
Die lange Stange trägt 20 Bausteine 15, die kippbar auf einer langen Achse gelagert sind. Jeder dieser BS15 repräsentiert ein Bit Speicher. Je nachdem, in welche Richtung ein BS15 gerade gekippt ist, ist das eine logische "1" oder eine "0".

Der Taster rechts auf dem Bild wird immer dann gedrückt, wenn ein Bit richtig positioniert und damit zugreifbar ist.

Das Kippen übernimmt der MiniMot, der mit seinem "Wedel" - einem der früher mal hergestellten Statiklaschen - in die gewünschte Richtung dreht und damit den Baustein (hoffentlich) zuverlässig in den gewünschten Zustand bringt.

Die Lampe rechts oben leuchtet auf die BS15 und bildet mit dem Lichtleiter unten und der Fotozelle daran eine Lichtschranke.
