---
layout: "image"
title: "Auslesen eines Bits (1)"
date: "2009-03-28T16:15:47"
picture: "ram9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23534
- /detailsfa26-2.html
imported:
- "2019"
_4images_image_id: "23534"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23534 -->
Hier sieht man die nicht unterbrochene Lichtschranke, und wie der Lichtleitstab in der Störlichtkappe des Fotowiderstandes steckt.
