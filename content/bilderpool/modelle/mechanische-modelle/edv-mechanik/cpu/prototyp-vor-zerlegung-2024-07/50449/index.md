---
layout: "image"
title: "Einlauf auf den Lesetisch (1)"
date: 2024-08-18T20:56:10+02:00
picture: "prototyp_vor_zerlegung_2024-07_19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Papierband kommt von unten an und läuft um diese Rundung. Außerdem wird es durch die schrägen Winkelsteine sauber zentriert.