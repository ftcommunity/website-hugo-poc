---
layout: "image"
title: "Tape Reder"
date: 2024-08-18T20:55:32+02:00
picture: "prototyp_vor_zerlegung_2024-07_08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Bandleseeinheit, vom Träger heruntergenommen.