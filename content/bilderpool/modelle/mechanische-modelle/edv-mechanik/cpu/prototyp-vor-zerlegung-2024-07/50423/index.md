---
layout: "image"
title: "Träger (4)"
date: 2024-08-18T20:55:37+02:00
picture: "prototyp_vor_zerlegung_2024-07_42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ansonsten sind die Träger in vier Baugruppen mit drei Abständen dazwischen alle identisch gebaut.

Wie einleitend gesagt: Das ist alles nur der unvollendete und mittlerweile zerlegte Prototypen-Stand. Die Maschine war so nie komplett in Betrieb, nur einzelne Baugruppen waren getestet.