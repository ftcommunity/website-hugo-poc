---
layout: "image"
title: "Auswertungselektronik"
date: 2024-08-18T20:56:14+02:00
picture: "prototyp_vor_zerlegung_2024-07_16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unten die insgesamt 4 Schwellwertschalter für die 4 Fotowiderstände, obendrüber 4 Leistungsstufen, rechts daneben das Relais als Polwender geschaltet, damit man steuern kann, ob nun "Plus" oder "Minus" als Instruktionsimpuls benötigt wird. Rechts unten ein 7400 4-fach-NAND.