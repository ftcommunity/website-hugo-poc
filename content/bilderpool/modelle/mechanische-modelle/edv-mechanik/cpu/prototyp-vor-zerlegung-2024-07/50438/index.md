---
layout: "image"
title: "Memory (2)"
date: 2024-08-18T20:55:56+02:00
picture: "prototyp_vor_zerlegung_2024-07_29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

24 kippbare Bits bilden den Speicher. Sie werden nach Bedarf - 1 vor, 1 zurück pro Instruktion - der Schreib-/Leseeinheit zugeführt.