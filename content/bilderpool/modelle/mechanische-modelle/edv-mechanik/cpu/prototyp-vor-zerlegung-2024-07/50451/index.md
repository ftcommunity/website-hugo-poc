---
layout: "image"
title: "Klappstütze der Auswertungselektronik"
date: 2024-08-18T20:56:13+02:00
picture: "prototyp_vor_zerlegung_2024-07_17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die schrägen Winkelträger 120 kann man abnehmen. Dann kann man die gesamte Elektronik nach hinten (hier: nach rechts vorne) herunterklappen. Sie wird dann von den dann senkrecht nach unten hängenden Stützen oben an der Elektronik abgestützt.