---
layout: "image"
title: "Memory (6)"
date: 2024-08-18T20:55:50+02:00
picture: "prototyp_vor_zerlegung_2024-07_33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So ist das Stellrad von unten aufgebaut. Die schwarzen Scheiben machen die Sache spielfreier