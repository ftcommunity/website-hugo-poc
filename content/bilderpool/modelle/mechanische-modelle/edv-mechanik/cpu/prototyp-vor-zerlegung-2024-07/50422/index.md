---
layout: "image"
title: "Memory"
date: 2024-08-18T20:55:36+02:00
picture: "prototyp_vor_zerlegung_2024-07_05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dies hier sind 24 Bits - nach außen oder nach innen klappbare Bausteine , angeordnet auf einem drehbaren Ring. Links hinten ist die Schreib-/Leseeinheit, die das jeweils gerade darin befindliche Bit entweder per Lichtschranke (mit dem hobby-4-Lichtleitwinkel) lesen oder das Bit über den "Wedel" mit der Feder zerstörungsfrei nach links (außen) oder rechts (innen) setzen oder zurücksetzen kann.

Der Grundbaustein vorne dient als Verstärker für die Lichtschranke und sollte elektrisch nah bei dieser stehen.

Rechts ist der Drehantrieb des Rings zu erahnen.

In den folgenden Bildern gibt es Detailbilder zu den Baugruppen.