---
layout: "image"
title: "Memory (9)"
date: 2024-08-18T20:55:46+02:00
picture: "prototyp_vor_zerlegung_2024-07_36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick auf die Stellfeder, die die Bits nach außen oder innen "wischt".