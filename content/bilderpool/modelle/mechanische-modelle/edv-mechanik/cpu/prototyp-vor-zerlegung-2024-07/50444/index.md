---
layout: "image"
title: "Dispatcher (2)"
date: 2024-08-18T20:56:04+02:00
picture: "prototyp_vor_zerlegung_2024-07_23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Rad drückt auf genau einen Taster, um einen Impuls zum benötigten Element zu leiten.