---
layout: "image"
title: "Träger (1)"
date: 2024-08-18T20:55:42+02:00
picture: "prototyp_vor_zerlegung_2024-07_39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf diesem Träger stecken die vier Bauplatten 500, leicht nach oben entnehmbar, aber transportsicher bei einem Autotransport.