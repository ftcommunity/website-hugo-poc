---
layout: "image"
title: "Control & Timing Unit"
date: 2024-08-18T20:55:55+02:00
picture: "prototyp_vor_zerlegung_2024-07_03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hinten ein paar Silberlinge für Logik und Timing.

Im Vordergrund der Dispatcher: Er kann motorisch nach links und rechts bis zu den Endlagentastern verfahren werden und drückt unten jeweils auf genau einen Taster. Die Steuerung soll den Motor um jeweils einen Taster in die gewünschte Richtung schieben, und das so oft (1 x je Taktzyklus), bis der gewünschte Taster gedrückt ist. Die nächste Instruktion "feuert" dann, und der Strom wird vom jeweils gedrückten Taster an das richtige Element der CPU geschickt, um in der eine Aktion auszulösen.

Rechts hinten ist die Elektromechanik für Sprünge (Jumps) im Code: Der Motor kann um max. 12 Schritte verdreht werden. Solange der nicht ganz unten steht, werden alle Instruktionen ignoriert bis auf die zum Verfahren eben dieses Motors, währen das Tape weiter endlos durchläuft. Bei jedem "Label" auf dem Tape (1 Bit ist dafür reserviert) fährt der Motor eine Stufe zurück. Wenn er auf 0 zurück gefahren ist, fängt die Verarbeitung der darauffolgenden Instruktionen wieder an. Wenn der Compiler also ausgerechnet hat, dass hier ein Sprung über N Labels hinweg kommen muss, ergibt das N Instruktionen, die den Jump-Motor verfahren. Alles danach wird ignoriert, und jeder erkannte Label auf dem Tape schaltet eine Sprungebene zurück. Nach dem N-ten Label werden die Instruktionen auf dem Tape nicht mehr ignoriert, sondern wieder verarbeitet.