---
layout: "image"
title: "Memory (10)"
date: 2024-08-18T20:55:45+02:00
picture: "prototyp_vor_zerlegung_2024-07_37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nach Monaten des Nichtstuns sieht man hier aber wenigstens sehr schön am Staub, wie der Speicherring auf der Bauplatte auflag.