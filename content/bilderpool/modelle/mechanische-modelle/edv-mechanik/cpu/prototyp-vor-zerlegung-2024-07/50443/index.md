---
layout: "image"
title: "Jump-Mechanik (1)"
date: 2024-08-18T20:56:03+02:00
picture: "prototyp_vor_zerlegung_2024-07_24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser S-Motor treibt das Rad mit 12 Nocken an. Ein Schritt "weiter" bedeutet, einen Label mehr überspringen. Einen Schritt "zurück" (ausgelöst durch das Passieren eines Labels auf dem Tape) bedeutet "wir sind dem Sprungziel um 1 Label näher gekommen".