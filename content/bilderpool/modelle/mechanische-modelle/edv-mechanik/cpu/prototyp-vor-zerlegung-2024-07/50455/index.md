---
layout: "image"
title: "Seiteneinblick in die Leseeinheit"
date: 2024-08-18T20:56:18+02:00
picture: "prototyp_vor_zerlegung_2024-07_13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht die Bahn, den Antrieb, obendrüber die hochgeklappte Leseeinheit und rechts die Auswertungselektronik. Die Kabel der Fotowiderstände hängen hier gerade frei herum.