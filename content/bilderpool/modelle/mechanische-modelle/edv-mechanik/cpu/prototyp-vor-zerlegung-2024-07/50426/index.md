---
layout: "image"
title: "ALU - Arithmetic & Logic Unit"
date: 2024-08-18T20:55:41+02:00
picture: "prototyp_vor_zerlegung_2024-07_04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da die einzige geplante Rechenoperation das NAND zweier Bits ist, ist die ALU vermutlich sehr unaufwändig: Zwei Flipflops stellen die beiden Akkumulatoren dar, von denen das NAND berechnet wird. Hier kommt also, wenn überhaupt, nur wenig dazu, sollte das jemals zum Laufen gebracht werden.