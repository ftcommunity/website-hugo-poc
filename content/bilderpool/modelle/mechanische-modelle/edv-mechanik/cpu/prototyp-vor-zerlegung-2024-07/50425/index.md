---
layout: "image"
title: "Träger (2)"
date: 2024-08-18T20:55:40+02:00
picture: "prototyp_vor_zerlegung_2024-07_40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hinten stecken im Träger pro Bauplatte 500 zwei Achsstummel nach oben heraus, die in die untenliegenden Löcher der Bauplatten eingreifen und sie somit fixieren.