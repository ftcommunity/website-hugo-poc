---
layout: "image"
title: "Gesamte Anlage"
date: 2024-08-18T20:56:23+02:00
picture: "prototyp_vor_zerlegung_2024-07_01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz links in schwarz ist der Behälter für das angedachte Papierband mit den Maschineninstruktionen. Danach folgen von links nach rechts 4 Bauplatten 500 auf einem gemeinsamen Träger mit folgenden Baugruppen:

1. Tape Reader
2. Control & Timing Unit
3. ALU
4. Memory

HINWEIS: Das ist alles nur der unvollendete und mittlerweile zerlegte Prototypen-Stand. Die Maschine war so nie komplett in Betrieb, nur einzelne Baugruppen waren getestet.