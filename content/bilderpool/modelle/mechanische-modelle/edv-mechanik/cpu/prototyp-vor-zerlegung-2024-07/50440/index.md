---
layout: "image"
title: "Arithmetic & Logic Unit"
date: 2024-08-18T20:55:59+02:00
picture: "prototyp_vor_zerlegung_2024-07_27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die ALU ist noch beliebig simpel: Zwei 1-Bit-Akkus (h4FF) und ein h4 AND/NAND zum Berechnen des NAND der beiden Bits der Akkus.