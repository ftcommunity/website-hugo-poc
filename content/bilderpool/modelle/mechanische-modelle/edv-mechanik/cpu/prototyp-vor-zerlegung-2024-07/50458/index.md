---
layout: "image"
title: "Leseoptik"
date: 2024-08-18T20:56:22+02:00
picture: "prototyp_vor_zerlegung_2024-07_10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Detailblick auf die optischen Elemente.