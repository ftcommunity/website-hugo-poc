---
layout: "image"
title: "Memory (8)"
date: 2024-08-18T20:55:47+02:00
picture: "prototyp_vor_zerlegung_2024-07_35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Umdrehung wird mit diesem Taster detektiert.