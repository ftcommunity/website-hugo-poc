---
layout: "image"
title: "Memory (4)"
date: 2024-08-18T20:55:52+02:00
picture: "prototyp_vor_zerlegung_2024-07_31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der S-Motor dreht Z40 und Drehscheibe mit den 6 Seilrollen. Die Seilrollen betätigen a) den Positionstaster und greifen b) in die WS60°-Zähne des Rings ein, um ihn definiert und mit hinreichend wenig Spiel zu drehen.