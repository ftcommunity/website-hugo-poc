---
layout: "image"
title: "Antrieb"
date: 2024-08-18T20:56:21+02:00
picture: "prototyp_vor_zerlegung_2024-07_11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein M-Motor im Hintergrund treibt den Gummireifen an. Der Motor läuft von Anfang bis Ende des Programms endlos.

Unten die Mimik zum Andocken des Tape-Vorratsbehälters.