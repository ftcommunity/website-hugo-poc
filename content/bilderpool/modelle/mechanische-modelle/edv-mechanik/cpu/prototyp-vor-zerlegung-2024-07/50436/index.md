---
layout: "image"
title: "Memory (3)"
date: 2024-08-18T20:55:54+02:00
picture: "prototyp_vor_zerlegung_2024-07_30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Ring von oben. Die Bits können entweder nach innen (z.B. log. "0") oder nach außen (z.B. log. "1") geklappt sein.