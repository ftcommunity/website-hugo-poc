---
layout: "overview"
title: "Prototyp vor Zerlegung 2024-07"
date: 2024-08-18T20:55:31+02:00
---

Seit vielen Monaten kam ich leider zu nichts und der aktuelle Stand verstaubte mehr und mehr. Außerdem glaube ich, dass ich für die Codeerzeugung auch für ein trivial kleines Progrämmchen dermaßen viel Papierband benötigen würde, dass ich mir dafür etwas anderes einfallen lassen muss. Also zerlegte ich den aktuellen Stand nochmal und gehe für die Codeerzeugung des auf dem PC angedachten Compilers nochmal zurück ans Reißbrett.

Das ist also alles nur der unvollendete und mittlerweile zerlegte Prototypen-Stand. Die Maschine war so nie komplett in Betrieb, nur einzelne Baugruppen waren getestet.