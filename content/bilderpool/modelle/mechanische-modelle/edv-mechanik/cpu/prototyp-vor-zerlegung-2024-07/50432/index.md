---
layout: "image"
title: "Memory (7)"
date: 2024-08-18T20:55:49+02:00
picture: "prototyp_vor_zerlegung_2024-07_34.jpg"
weight: "34"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Gelesen wird das jeweils aktuelle Bit durch Unterbrechen der Lichtschranke bei nach außen geklapptem Bit oder eben bei nach innen geklapptem Bit durch Freigeben der Lichtschranke.

Geschrieben wird durch Lauf des S-Motors in die gewünschte Richtung, um den "Wedel" (die Feder rechts) um eine Umdrehung über das Bit zu Führen. Egal wie es stand, ist das Bit danach also zuverlässig nach außen oder nach innen gekippt und somit eingestellt.