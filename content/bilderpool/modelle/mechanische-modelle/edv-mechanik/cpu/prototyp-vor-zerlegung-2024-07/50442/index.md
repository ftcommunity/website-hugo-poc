---
layout: "image"
title: "Jump-Mechanik (2)"
date: 2024-08-18T20:56:01+02:00
picture: "prototyp_vor_zerlegung_2024-07_25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Unten ist der Nullpositionstaster rechts, der Schritt-Taster links davon zu sehen. Der Nullpositionstaster wird vom schwarzen Rad 14, also nur an einer Stelle des Rades, gedrückt. Wenn der nach einem Sprung wieder erreicht ist, geht die Verarbeitung (anstatt das Ignorieren) von gelesenen Instruktionen weiter.