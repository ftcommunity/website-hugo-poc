---
layout: "image"
title: "Leseeinheit oben arretiert"
date: 2024-08-18T20:56:19+02:00
picture: "prototyp_vor_zerlegung_2024-07_12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine drehbar gelagerte Strebe ermöglicht es ähnlich einer PKW-Motorhaube, die Leseeinheit anzuheben und gegen Herunterklappen/-fallen zu sichern.