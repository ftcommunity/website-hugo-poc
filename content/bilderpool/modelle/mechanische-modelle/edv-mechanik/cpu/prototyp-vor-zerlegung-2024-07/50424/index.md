---
layout: "image"
title: "Träger (3)"
date: 2024-08-18T20:55:39+02:00
picture: "prototyp_vor_zerlegung_2024-07_41.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Befestigung der Achsen. Ein Klemmring 10 und ein Klemmring 5 ergeben den Abstand dazwischen, damit das ohne Spiel sitzt.