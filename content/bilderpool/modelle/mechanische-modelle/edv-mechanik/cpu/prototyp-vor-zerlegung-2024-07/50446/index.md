---
layout: "image"
title: "Control & Timing Unit"
date: 2024-08-18T20:56:06+02:00
picture: "prototyp_vor_zerlegung_2024-07_21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vorne der Dispatcher (Signalverteiler), hinten etwas Logik, rechts hinten die Sprunginstruktions-Mimik.