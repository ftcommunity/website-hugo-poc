---
layout: "image"
title: "Auswertungselektronik heruntergeklappt"
date: 2024-08-18T20:56:12+02:00
picture: "prototyp_vor_zerlegung_2024-07_18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die heruntergeklappte Elektronik zur leichten Wartung und Verkabelung.