---
layout: "image"
title: "Memory (1)"
date: 2024-08-18T20:55:57+02:00
picture: "prototyp_vor_zerlegung_2024-07_28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Man sieht den drehbaren Speicher-Ring, den Drehantrieb rechts und die Schreib-/Leseeinheit links hinten.