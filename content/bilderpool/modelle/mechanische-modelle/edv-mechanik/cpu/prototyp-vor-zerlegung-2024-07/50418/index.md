---
layout: "image"
title: "Leseeinheit hochgeklappt"
date: 2024-08-18T20:55:31+02:00
picture: "prototyp_vor_zerlegung_2024-07_09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das mit hochgeklappte Rad liegt im Betrieb auf dem von unten angetriebenen Rad auf, um das Band endlos durch die Leseeinheit zu ziehen. Zwei Lampen leuchten von oben aufs Tape, vier Lichtleitwinkel leiden das reflektierte Licht zu vier Fotowiderständen.