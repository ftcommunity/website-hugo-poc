---
layout: "image"
title: "Dispatcher (1)"
date: 2024-08-18T20:56:05+02:00
picture: "prototyp_vor_zerlegung_2024-07_22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der S-Motor kann die Stange in beide Richtungen schieben. Die Federnocken an der Stange drücken bei Erreichen einer definierten Position - über einem der Taster unten nämlich.