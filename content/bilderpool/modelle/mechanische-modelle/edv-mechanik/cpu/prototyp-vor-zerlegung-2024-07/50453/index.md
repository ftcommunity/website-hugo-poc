---
layout: "image"
title: "Bandantrieb (2)"
date: 2024-08-18T20:56:15+02:00
picture: "prototyp_vor_zerlegung_2024-07_15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Band wird durch die zwei Reifen gezogen.