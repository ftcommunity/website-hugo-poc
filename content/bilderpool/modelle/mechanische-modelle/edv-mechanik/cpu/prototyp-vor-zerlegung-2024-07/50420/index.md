---
layout: "image"
title: "Tape Storage (2)"
date: 2024-08-18T20:55:33+02:00
picture: "prototyp_vor_zerlegung_2024-07_07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Wand abgenommen. Endlich mal eine sinnvolle Anwendung der abgerundeten Bausteine...

Und: Staub...