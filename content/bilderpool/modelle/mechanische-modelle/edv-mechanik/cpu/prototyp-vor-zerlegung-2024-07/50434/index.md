---
layout: "image"
title: "Memory (5)"
date: 2024-08-18T20:55:51+02:00
picture: "prototyp_vor_zerlegung_2024-07_32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man den Eingriff der Seilrollen in die Zähne.