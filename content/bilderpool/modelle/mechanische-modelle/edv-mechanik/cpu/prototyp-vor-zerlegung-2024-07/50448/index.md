---
layout: "image"
title: "Tape Reader"
date: 2024-08-18T20:56:09+02:00
picture: "prototyp_vor_zerlegung_2024-07_02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Einheit oberhalb des Laufwegs des Papierbands ist hochklappbar und trägt die Beleuchtung sowie die vier Leseeinheiten. Das Papierband sollte 4 Bit breit sein. Es wird von den Rädern links nach links zurück in den Vorratsbehälter gezogen. Untendrunter läuft es nach rechts und dann hoch auf den Lesetisch.

Die 1980er-Jahre Elektronik im Hintergrund ist ebenfalls zur Wartung wegklappbar und soll die vier Bits auswerten.