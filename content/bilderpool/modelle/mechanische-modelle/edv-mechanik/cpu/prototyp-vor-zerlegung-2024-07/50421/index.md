---
layout: "image"
title: "Tape Storage (1)"
date: 2024-08-18T20:55:35+02:00
picture: "prototyp_vor_zerlegung_2024-07_06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieser Kasten kann von links an den Tape Reader angesteckt werden. Darin soll das Endlos-Papierband mit den draufgedruckten 4-Bit-Wörtern für die Maschineninstruktionen liegen, sofern es nicht gerade in der Bandeinheit ist.

Wahrscheinlich ist das aber viel zu klein. Es wird dermaßen viele "RISC"-Instruktionen pro sinnvollem Befehl benötigen, dass ich keine Lust habe km-weise A4-Tape-Abschnitte zusammenzukleben. Da muss noch was anderes her.