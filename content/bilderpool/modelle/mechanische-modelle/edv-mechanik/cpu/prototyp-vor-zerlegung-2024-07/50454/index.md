---
layout: "image"
title: "Bandantrieb (1)"
date: 2024-08-18T20:56:17+02:00
picture: "prototyp_vor_zerlegung_2024-07_14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Bandmotor läuft halt einfach durch und zieht das Tape endlos durch die Leseeinheit.