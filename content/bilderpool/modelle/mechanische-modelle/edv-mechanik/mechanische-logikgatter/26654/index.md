---
layout: "image"
title: "XOR aus drei NAND-Bausteinen 3"
date: "2010-03-07T11:12:24"
picture: "XOR10.jpg"
weight: "10"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26654
- /details7b13-2.html
imported:
- "2019"
_4images_image_id: "26654"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T11:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26654 -->
Hier ist A=1, B=0 und die Mechanik bildet A XOR B = 1.
