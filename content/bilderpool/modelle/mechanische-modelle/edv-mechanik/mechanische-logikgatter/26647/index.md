---
layout: "image"
title: "Grundeinstellung NAND"
date: "2010-03-07T10:57:04"
picture: "Grundeinstellung.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/26647
- /detailsd967.html
imported:
- "2019"
_4images_image_id: "26647"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T10:57:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26647 -->
Hier das fertige NAND-Gatter. Ob NAND- oder NOR-Gatter entscheidet die Stellung des Ritzels 10 zur Segmentscheibe Z12. Man erkennt hier die genaue Einstellung für NAND. Beide Eingänge (rechts) sind 1 (d.h. Kurbeln zeigen nach links), der Ausgang ist 0 (Kurbel zeigt nach rechts).
