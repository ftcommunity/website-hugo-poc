---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:43"
picture: "DSCN5091.jpg"
weight: "50"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36697
- /details466b.html
imported:
- "2019"
_4images_image_id: "36697"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36697 -->
Sicht von vorne.
Hier kann man gut erkennen wie die Finger bewegt werden.
