---
layout: "image"
title: "Greifer (1)"
date: "2013-02-23T11:53:12"
picture: "DSCN4980.jpg"
weight: "33"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36663
- /detailscadf-3.html
imported:
- "2019"
_4images_image_id: "36663"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-23T11:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36663 -->
Ein neuer Greifer, recht schlank aufgebaut und gut funktionierend.
Verwendet habe ich den neuen (kleinen) Zylinder.
