---
layout: "image"
title: "von unten"
date: "2009-08-14T21:37:27"
picture: "fingergreifer4.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24795
- /detailsafd3.html
imported:
- "2019"
_4images_image_id: "24795"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24795 -->
Der Draht verhindert dass die Gelenke aus ihrer Position rutschen können.