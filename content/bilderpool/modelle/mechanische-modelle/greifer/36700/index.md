---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:43"
picture: "DSCN5097.jpg"
weight: "53"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36700
- /details665b-2.html
imported:
- "2019"
_4images_image_id: "36700"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36700 -->
so siehts von hinten aus.
Die beiden mittleren Zylinder steuern Daumen und Mittelfinger.
Sie sind etwas versetzt angeordnet damit noch genügend Platz für die Anschlüsse vorhanden ist. Die beiden äußeren Zylinder sind für die anderen Finger. Sie stehen hier etas vor.
