---
layout: "image"
title: "Greifer geschlossen"
date: "2008-12-30T16:07:44"
picture: "DSCN2581.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16779
- /detailse18f.html
imported:
- "2019"
_4images_image_id: "16779"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16779 -->
Ansicht von unten
