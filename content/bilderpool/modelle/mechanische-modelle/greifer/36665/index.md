---
layout: "image"
title: "Greifer (1)"
date: "2013-02-23T11:53:12"
picture: "DSCN5019.jpg"
weight: "35"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36665
- /details0caa.html
imported:
- "2019"
_4images_image_id: "36665"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-23T11:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36665 -->
Greifer geschlossen.
