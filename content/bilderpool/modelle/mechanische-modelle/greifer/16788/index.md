---
layout: "image"
title: "'6-Finger-Greifer' pneumatisch"
date: "2008-12-30T16:07:44"
picture: "DSCN2570.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16788
- /details2a94.html
imported:
- "2019"
_4images_image_id: "16788"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16788 -->
das kann man hier auch gut erkennen
