---
layout: "image"
title: "Greifer für Schüttgut (pneumatisch)"
date: "2008-12-30T16:07:44"
picture: "DSCN2588.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16797
- /detailsc269.html
imported:
- "2019"
_4images_image_id: "16797"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16797 -->
SCHLAUCH !
