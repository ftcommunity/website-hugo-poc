---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:43"
picture: "DSCN5100.jpg"
weight: "52"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/36699
- /details2187-2.html
imported:
- "2019"
_4images_image_id: "36699"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36699 -->
Auf diesem Bild kann man die um 20mm geringere Breite gut erkennen.
