---
layout: "image"
title: "Hebegreifer ganz geöffnet"
date: "2014-09-27T18:57:33"
picture: "hebegreifer5.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/39371
- /details6730.html
imported:
- "2019"
_4images_image_id: "39371"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39371 -->
Interessanterweise stösst die Bauplatte 15x90 exakt wenn der Greifer, wie auf dem Bild, ganz geöffnet ist am BS15 mit Loch an, der die Drehachse für die Zahnräder hält.
