---
layout: "image"
title: "Hebegreifer geschlossen"
date: "2014-09-27T18:57:32"
picture: "hebegreifer1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/39367
- /detailsa82f.html
imported:
- "2019"
_4images_image_id: "39367"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39367 -->
Die beiden Z30 wandeln die Drehbewegung in eine Zug- oder Schiebebewegung um, womit der Greiffer geschlossen und angehoben wird. In diesem Bild ist der Greifer voll geschlossen und angehoben.
