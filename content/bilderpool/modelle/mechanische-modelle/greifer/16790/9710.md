---
layout: "comment"
hidden: true
title: "9710"
date: "2009-08-10T17:27:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Ludger,

was für Kompressoren hattest Du denn da verwendet, die nicht genug Luft lieferten? Nur Original-ft-Aufbauten oder auch andere?

Gruß,
Stefan