---
layout: "image"
title: "Greifer"
date: "2009-08-14T21:37:27"
picture: "fingergreifer2.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- /php/details/24793
- /detailsd6a6.html
imported:
- "2019"
_4images_image_id: "24793"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24793 -->
Es gibt nur noch 4 Zangen die gefedert sind und noch 2 Führungsschienen