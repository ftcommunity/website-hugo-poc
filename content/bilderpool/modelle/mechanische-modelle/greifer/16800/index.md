---
layout: "image"
title: "Sauerstoff Werk"
date: "2008-12-30T16:07:44"
picture: "DSCN2602.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/16800
- /details8d6d-2.html
imported:
- "2019"
_4images_image_id: "16800"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16800 -->
Selbst diese Anordnung lieferte nicht genug Druckluft für den großen Greifer.
