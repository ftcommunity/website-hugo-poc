---
layout: "image"
title: "Rolltor 6"
date: "2007-03-07T16:00:50"
picture: "rolltor6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9347
- /detailsb1be.html
imported:
- "2019"
_4images_image_id: "9347"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9347 -->
