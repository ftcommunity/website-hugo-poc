---
layout: "image"
title: "Rolltor 7"
date: "2007-03-07T16:00:50"
picture: "rolltor7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9348
- /detailsfcff.html
imported:
- "2019"
_4images_image_id: "9348"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9348 -->
Hier ist das Tor aufgerollt zu sehen.
