---
layout: "image"
title: "Ansicht hinten geschlossen"
date: "2012-05-18T00:39:49"
picture: "schiebetuerfabian2.jpg"
weight: "2"
konstrukteure: 
- "Fabian"
fotografen:
- "Fabian"
uploadBy: "ft-fabi"
license: "unknown"
legacy_id:
- /php/details/34967
- /details0b96-2.html
imported:
- "2019"
_4images_image_id: "34967"
_4images_cat_id: "2588"
_4images_user_id: "1502"
_4images_image_date: "2012-05-18T00:39:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34967 -->
Von hinten sieht man schön die befästigung der Zylinder an der Tür.
Die Stäbe unten sind zur Stabilisation notwendig und die eisenstange oben zur vernünftigen führung der Tür.
Würde mich freuen, wenn ihr die Tür irgendwo nachbaut, ein Bild davon zu sehen ;-)