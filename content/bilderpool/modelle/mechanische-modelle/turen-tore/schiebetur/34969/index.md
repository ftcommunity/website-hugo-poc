---
layout: "image"
title: "Ansicht hinten offen"
date: "2012-05-18T00:39:49"
picture: "schiebetuerfabian4.jpg"
weight: "4"
konstrukteure: 
- "Fabian"
fotografen:
- "Fabian"
uploadBy: "ft-fabi"
license: "unknown"
legacy_id:
- /php/details/34969
- /details2192.html
imported:
- "2019"
_4images_image_id: "34969"
_4images_cat_id: "2588"
_4images_user_id: "1502"
_4images_image_date: "2012-05-18T00:39:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34969 -->
Die Tür geöffnet von hinten.
Die Luftschlauchführung ist nur provisorisch, da ich die Tür nur zu Probezwecken gebaut habe.