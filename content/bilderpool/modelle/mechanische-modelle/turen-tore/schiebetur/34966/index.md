---
layout: "image"
title: "Ansicht vorne geschlossen"
date: "2012-05-18T00:39:49"
picture: "schiebetuerfabian1.jpg"
weight: "1"
konstrukteure: 
- "Fabian"
fotografen:
- "Fabian"
uploadBy: "ft-fabi"
license: "unknown"
legacy_id:
- /php/details/34966
- /detailseed1.html
imported:
- "2019"
_4images_image_id: "34966"
_4images_cat_id: "2588"
_4images_user_id: "1502"
_4images_image_date: "2012-05-18T00:39:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34966 -->
Ich bin neu hier und das ist mein erstes Modell das ich hier hochgeladen habe...
Gebaut habe ich schon mehr als diese Schiebetür die ich nur zu Probezwecken gebaut habe...
Ich bin froh über jeden neuen Kommentar und über Tipps und Tricks...
