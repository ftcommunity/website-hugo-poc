---
layout: "image"
title: "Ansicht vorne offen"
date: "2012-05-18T00:39:49"
picture: "schiebetuerfabian3.jpg"
weight: "3"
konstrukteure: 
- "Fabian"
fotografen:
- "Fabian"
uploadBy: "ft-fabi"
license: "unknown"
legacy_id:
- /php/details/34968
- /detailsca08-2.html
imported:
- "2019"
_4images_image_id: "34968"
_4images_cat_id: "2588"
_4images_user_id: "1502"
_4images_image_date: "2012-05-18T00:39:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34968 -->
Ja... Die geöffnete Tür  in der nah Ansicht.
Gerne Nachbauen!!!