---
layout: "image"
title: "Tür 3"
date: "2007-03-09T19:00:21"
picture: "doppeltuer03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9355
- /details4e90.html
imported:
- "2019"
_4images_image_id: "9355"
_4images_cat_id: "863"
_4images_user_id: "502"
_4images_image_date: "2007-03-09T19:00:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9355 -->
Halb offen.
