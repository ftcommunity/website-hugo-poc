---
layout: "image"
title: "Tuer01-zu.JPG"
date: "2007-01-13T14:02:28"
picture: "Tuer01-zu.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8381
- /details9fcf.html
imported:
- "2019"
_4images_image_id: "8381"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:02:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8381 -->
Die Tür ist 60 mm breit und bewegt sich beim Öffnen um 45 mm nach rechts.
