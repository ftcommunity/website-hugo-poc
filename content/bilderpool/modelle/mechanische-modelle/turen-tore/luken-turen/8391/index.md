---
layout: "image"
title: "Tuer06-zu.JPG"
date: "2007-01-13T14:23:29"
picture: "Tuer06-zu.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8391
- /details7ee8.html
imported:
- "2019"
_4images_image_id: "8391"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:23:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8391 -->
Hier sind es wieder 45 mm Öffnungsweite, allerdings mit etwas Behinderung durch die Klemmhülse 35980, die beim Öffnen in den Weg schwenkt. Die Klemmhülse sorgt dafür, dass man die Tür auch von außen öffnen kann, ohne sich die Fingernägel abzubrechen: wenn man von außen mit einer Achse o.ä. durch das Loch drückt, schwenkt die Tür auf.

Die Scharniere beruhen darauf, dass der Verschlussriegel 37232 prima in den Riegelstein hineinpasst.
