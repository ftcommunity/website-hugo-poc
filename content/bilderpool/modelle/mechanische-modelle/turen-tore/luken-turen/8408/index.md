---
layout: "image"
title: "Luke03-auf2.JPG"
date: "2007-01-13T14:54:00"
picture: "Luke03-auf2.JPG"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8408
- /details2b1f.html
imported:
- "2019"
_4images_image_id: "8408"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:54:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8408 -->
