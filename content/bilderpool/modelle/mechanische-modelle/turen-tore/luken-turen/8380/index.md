---
layout: "image"
title: "Tuer-basic.JPG"
date: "2007-01-13T14:00:01"
picture: "Tuer-basic.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8380
- /details055a.html
imported:
- "2019"
_4images_image_id: "8380"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:00:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8380 -->
Alles, was ich hier mit dem Namen "Tür" belege, beruht auf der Entdeckung, dass man mit der hier gezeigten Anordnung ein nahezu perfekt rechtwinkliges Dreieck hinbekommt. Das gute daran ist, dass alle Kanten im ft-Raster sind.

Dieses gewinkelte Teil dient dazu, eine Tür (vorzugsweise in der Seitenwand einer Flugzeugkabine) auszuschwenken und dabei vom "Loch" wegzubewegen. Die lange Seite (hier 30 mm) liegt bei geschlossener Tür "in" der Seitenwand und legt fest, wie groß die freie Öffnung bei offener Tür wird:
mal 2 genommen und davon 15 mm abgezogen, ergibt die Breite der freigemachten Öffnung (hier 45 mm).
