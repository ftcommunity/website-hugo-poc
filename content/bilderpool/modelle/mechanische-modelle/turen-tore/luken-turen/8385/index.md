---
layout: "image"
title: "Tuer03-zu.JPG"
date: "2007-01-13T14:10:36"
picture: "Tuer03-zu.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8385
- /details5888.html
imported:
- "2019"
_4images_image_id: "8385"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:10:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8385 -->
Gleiches Prinzip wie Tür01, Öffnungbreite 45 mm. Das U-förmige Bauteil dient "eigentlich" dazu, beim M-Motor die Getriebe 90° versetzt anzubauen.
