---
layout: "image"
title: "Tuer04-zu.JPG"
date: "2007-01-13T14:12:55"
picture: "Tuer04-zu.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8387
- /details5ad8.html
imported:
- "2019"
_4images_image_id: "8387"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:12:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8387 -->
So sieht es aus, wenn man auf den Winkel verzichtet: diese Tür braucht die Aussparung im Winkelträger 60, um richtig weit aufzugehen. Auch hier Öffnungsbreite 45 mm.
