---
layout: "image"
title: "Luke03-auf.JPG"
date: "2007-01-13T14:53:35"
picture: "Luke03-auf.JPG"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8407
- /details03a4-2.html
imported:
- "2019"
_4images_image_id: "8407"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8407 -->
Die Kurbel 38447 leistet bei solchen Viergelenk-Getrieben unschätzbare Dienste.
Die Deckel klappen nicht einfach nur hinunter, sondern werden gleichzeitig nach außen und oben geführt. Ähnliche Klapp-Mechanismen findet man auch bei Küchen- und Wohnzimmerschränken.
