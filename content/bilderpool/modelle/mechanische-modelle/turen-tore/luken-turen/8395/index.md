---
layout: "image"
title: "Tuer07-auf2.JPG"
date: "2007-01-13T14:27:22"
picture: "Tuer07-auf2.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8395
- /detailsd6b1.html
imported:
- "2019"
_4images_image_id: "8395"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:27:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8395 -->
Diese Tür hat 45 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden (angedeutet durch das Kardangelenk).
Als Mitnehmer dienen zwei Klemmhülsen unter dem BS7,5 nebst den beiden Rastadaptern beiderseits davon.
