---
layout: "image"
title: "Luke06-zu.JPG"
date: "2007-01-13T15:04:46"
picture: "Luke06-zu.JPG"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8415
- /details5cb8-2.html
imported:
- "2019"
_4images_image_id: "8415"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:04:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8415 -->
Das ist wieder ein Viergelenk-Getriebe, diesmal mit der Gelenkkurbel 35088 aufgebaut. Den rechten Deckel muss man sich dazudenken.
