---
layout: "image"
title: "Luke01-zu.JPG"
date: "2007-01-13T14:45:49"
picture: "Luke01-zu.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8402
- /details46a1.html
imported:
- "2019"
_4images_image_id: "8402"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:45:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8402 -->
Das ist eine symmetrisch aufgebaute Luke, z.B. für ein Flugzeug-Fahrwerk. Die beiden Deckel sind über die Riegelscheiben gekoppelt. Beim Öffnen schwenken die Deckel nach unten, damit links und rechts davon die Fahrwerksbeine herausfahren können.

Die Kurbel 38447 (oder auch 38235, 38239, 38445, die aber *nml* sind) leistet unschätzbare Dienste. In Bildmitte unten sieht man, wie die Kurbel zwischen Verkleidungsplatte und Federnocken stirnseitig am BS7,5 festgeklemmt wird.
