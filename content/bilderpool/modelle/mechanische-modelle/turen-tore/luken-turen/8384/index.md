---
layout: "image"
title: "Tuer02-auf.JPG"
date: "2007-01-13T14:06:28"
picture: "Tuer02-auf.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8384
- /detailsdfca.html
imported:
- "2019"
_4images_image_id: "8384"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:06:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8384 -->
