---
layout: "image"
title: "Tuer08-zu.JPG"
date: "2007-01-13T14:30:23"
picture: "Tuer08-zu.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8396
- /detailse816.html
imported:
- "2019"
_4images_image_id: "8396"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:30:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8396 -->
Diese Tür hat 45 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden.
Die Tür ist so hoch gebaut, dass eine ft-Figur hindurchgehen kann. Das geht bei den Türen 01 bis 07 natürlich auch, wenn man die Ober/Unterteile auseinander setzt.
