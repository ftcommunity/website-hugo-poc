---
layout: "image"
title: "Luke04-zu.JPG"
date: "2007-01-13T14:54:45"
picture: "Luke04-zu.JPG"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8409
- /details6b65.html
imported:
- "2019"
_4images_image_id: "8409"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:54:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8409 -->
