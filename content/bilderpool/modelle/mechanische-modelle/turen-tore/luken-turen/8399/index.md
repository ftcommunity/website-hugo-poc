---
layout: "image"
title: "Tuer09-auf.JPG"
date: "2007-01-13T14:34:48"
picture: "Tuer09-auf.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8399
- /detailsbcc1.html
imported:
- "2019"
_4images_image_id: "8399"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:34:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8399 -->
Diese Tür hat 75 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden. Als Mitnehmer für die Drehbewegung dient die Konstruktion mit den beiden Rastadaptern mittendrin.
