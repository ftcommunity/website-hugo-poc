---
layout: "image"
title: "Schiebetuer05-zu.JPG"
date: "2007-01-14T12:57:23"
picture: "Schiebetuer05-zu.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8451
- /details1a41-2.html
imported:
- "2019"
_4images_image_id: "8451"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:57:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8451 -->
