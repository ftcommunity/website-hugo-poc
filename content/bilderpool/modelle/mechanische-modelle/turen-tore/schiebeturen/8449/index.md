---
layout: "image"
title: "Schiebetuer04-zu.JPG"
date: "2007-01-14T12:56:34"
picture: "Schiebetuer04-zu.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8449
- /details3396.html
imported:
- "2019"
_4images_image_id: "8449"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:56:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8449 -->
Dieser Aufbau ist etwas ungeschickt und braucht mehr Platz als nötig. Wenn man die Führung der Türen nur innen anbringt, kann die Achse kürzer gewählt werden.
