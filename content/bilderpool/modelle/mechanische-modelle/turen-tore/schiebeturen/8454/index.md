---
layout: "image"
title: "Schiebetuer06-auf.JPG"
date: "2007-01-14T13:07:54"
picture: "Schiebetuer06-auf.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8454
- /details1929.html
imported:
- "2019"
_4images_image_id: "8454"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T13:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8454 -->
Nach diesem Prinzip kann man die anderen Schiebetüren natürlich auch betätigen.

Das Seil läuft einmal im Kreis herum durch beide Türflügel, und wird durch die beiden BS15 lose hindurchgefädelt. Die eine Seite des Seils wird um das linke Verbindungsstück geschlungen, die andere Seite um das rechte. Das Justieren des Seils, so dass die Tür genau mittig schließt, ist ein bisschen fummelig.
