---
layout: "image"
title: "Schiebetuer09c.JPG"
date: "2007-11-18T23:05:02"
picture: "Schiebetuer09c.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12786
- /details1412-2.html
imported:
- "2019"
_4images_image_id: "12786"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T23:05:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12786 -->
Das könnte mal ein Fahrwerksschacht für ein Flugzeug werden.
