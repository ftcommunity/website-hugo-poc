---
layout: "image"
title: "Schiebetuer03-auf.JPG"
date: "2007-01-14T12:54:06"
picture: "Schiebetuer03-auf.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8448
- /details9b24.html
imported:
- "2019"
_4images_image_id: "8448"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:54:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8448 -->
