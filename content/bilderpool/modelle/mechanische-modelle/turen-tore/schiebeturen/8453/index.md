---
layout: "image"
title: "Schiebetuer06-zu.JPG"
date: "2007-01-14T12:59:17"
picture: "Schiebetuer06-zu.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8453
- /detailse748.html
imported:
- "2019"
_4images_image_id: "8453"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:59:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8453 -->
Die Seilführung wird auf dem nächsten Bild deutlicher (06-auf). Nach diesem Prinzip kann man die anderen Schiebetüren natürlich auch betätigen.
