---
layout: "image"
title: "Schiebetuer09a.JPG"
date: "2007-11-18T23:03:21"
picture: "Schiebetuer09a.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12785
- /detailscf0f.html
imported:
- "2019"
_4images_image_id: "12785"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T23:03:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12785 -->
Die Flach- und Bogenstücke (35055, 36330, 36327) haben natürlich auch eine breite Seite, um die ein 32455 herumgreifen kann.
