---
layout: "image"
title: "Schiebetuer10.JPG"
date: "2007-11-18T23:05:31"
picture: "Schiebetuer10.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12787
- /details12ea.html
imported:
- "2019"
_4images_image_id: "12787"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T23:05:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12787 -->
Außer als Schiebetür lässt sich solch eine Gleitführung natürlich auch z.B. in einem Gabelstapler einsetzen.
