---
layout: "image"
title: "Schiebetuer02-auf.JPG"
date: "2007-01-14T12:52:28"
picture: "Schiebetuer02-auf.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8446
- /details1847-2.html
imported:
- "2019"
_4images_image_id: "8446"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8446 -->
