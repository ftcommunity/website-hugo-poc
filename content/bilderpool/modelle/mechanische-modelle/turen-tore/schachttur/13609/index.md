---
layout: "image"
title: "Schachttür01"
date: "2008-02-09T13:45:47"
picture: "schachttuer1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13609
- /detailsfd7e.html
imported:
- "2019"
_4images_image_id: "13609"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13609 -->
Schachttür von vorne links mit Verkleidung