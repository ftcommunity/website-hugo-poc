---
layout: "image"
title: "Schachttür08"
date: "2008-02-09T13:45:47"
picture: "schachttuer8.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13616
- /details1586.html
imported:
- "2019"
_4images_image_id: "13616"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13616 -->
Detail der Mechanik - geöffnete Türen