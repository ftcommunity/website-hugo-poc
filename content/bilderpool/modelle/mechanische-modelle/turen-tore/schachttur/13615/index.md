---
layout: "image"
title: "Schachttür07"
date: "2008-02-09T13:45:47"
picture: "schachttuer7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13615
- /details4eec.html
imported:
- "2019"
_4images_image_id: "13615"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13615 -->
Frontalansicht vom "Aufzugsvorraum"