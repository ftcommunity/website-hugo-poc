---
layout: "image"
title: "Lamellentor(zu)"
date: "2007-08-09T22:02:25"
picture: "lamellentor1.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11324
- /detailsbcd7-2.html
imported:
- "2019"
_4images_image_id: "11324"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11324 -->
Das hier ist mein Lamellentor. Es hat 2 Taster zum Öffnen bzw. Schließen(einer außen, einer innen). Gesteuert wird es per Interface.