---
layout: "image"
title: "Auf(von oben)"
date: "2007-08-09T22:02:25"
picture: "lamellentor3.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11326
- /details3924-2.html
imported:
- "2019"
_4images_image_id: "11326"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11326 -->
Das hier ist die Ansicht von oben. Angetrieben wird es mit einem Powermotor 1:8. Es hat 2 Taster; einen für jede Position des Tores.