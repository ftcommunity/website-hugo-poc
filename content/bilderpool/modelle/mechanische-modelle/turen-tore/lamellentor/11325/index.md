---
layout: "image"
title: "Lamellentor(auf)"
date: "2007-08-09T22:02:25"
picture: "lamellentor2.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11325
- /details62e6-2.html
imported:
- "2019"
_4images_image_id: "11325"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11325 -->
So ist das Lamellentor auf.