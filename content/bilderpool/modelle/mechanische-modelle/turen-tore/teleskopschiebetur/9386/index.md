---
layout: "image"
title: "Tür 6"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9386
- /details2dad.html
imported:
- "2019"
_4images_image_id: "9386"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9386 -->
Die Türen hängen alle an Ketten die mit einer 2:1 Übersetzung verbunden sind damit die hinteren doppelt so schnell laufen.
