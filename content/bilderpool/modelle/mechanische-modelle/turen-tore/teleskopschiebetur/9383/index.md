---
layout: "image"
title: "Tür 3"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9383
- /detailse315.html
imported:
- "2019"
_4images_image_id: "9383"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9383 -->
