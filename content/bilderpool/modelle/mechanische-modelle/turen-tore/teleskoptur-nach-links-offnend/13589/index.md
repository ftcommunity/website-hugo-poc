---
layout: "image"
title: "Teleskoptür02"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13589
- /details343b.html
imported:
- "2019"
_4images_image_id: "13589"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13589 -->
Gesamtansicht von hinten rechts