---
layout: "image"
title: "Teleskoptür01"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13588
- /detailse0b3-2.html
imported:
- "2019"
_4images_image_id: "13588"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13588 -->
Gesamtansicht von hinten links