---
layout: "image"
title: "Teleskoptür06"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13593
- /details084a.html
imported:
- "2019"
_4images_image_id: "13593"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13593 -->
Antrieb