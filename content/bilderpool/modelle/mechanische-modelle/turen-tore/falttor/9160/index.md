---
layout: "image"
title: "Falttor 3"
date: "2007-02-28T19:25:41"
picture: "falttor03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9160
- /detailsc553-2.html
imported:
- "2019"
_4images_image_id: "9160"
_4images_cat_id: "846"
_4images_user_id: "502"
_4images_image_date: "2007-02-28T19:25:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9160 -->
Hier siegt man die ein Teil der Öffnungsmechanik. Hier wird das Tor nur ein bisschen geöffnet da der andere Antrieb dies nicht schafft.
