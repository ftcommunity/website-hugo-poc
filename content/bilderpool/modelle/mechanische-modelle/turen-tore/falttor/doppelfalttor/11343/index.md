---
layout: "image"
title: "Doppelfalttor  8"
date: "2007-08-10T17:07:41"
picture: "doppelfalttor08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11343
- /details11fe-2.html
imported:
- "2019"
_4images_image_id: "11343"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11343 -->
