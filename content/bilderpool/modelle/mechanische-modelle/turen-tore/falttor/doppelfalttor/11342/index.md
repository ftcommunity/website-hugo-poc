---
layout: "image"
title: "Doppelfalttor  7"
date: "2007-08-10T17:07:05"
picture: "doppelfalttor07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11342
- /details6968-2.html
imported:
- "2019"
_4images_image_id: "11342"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11342 -->
So sieht es dann aus wenn es offen ist.
