---
layout: "image"
title: "Doppelfalttor 14"
date: "2007-08-11T17:21:31"
picture: "doppelfalttor4.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11352
- /details7d99.html
imported:
- "2019"
_4images_image_id: "11352"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11352 -->
