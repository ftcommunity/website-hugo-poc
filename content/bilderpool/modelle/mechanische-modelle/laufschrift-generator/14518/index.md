---
layout: "image"
title: "Bild 05 - Detail Lichtschranke"
date: "2008-05-16T07:15:06"
picture: "Bild_05_-_Detail_Lichtschranke.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14518
- /details6ea6.html
imported:
- "2019"
_4images_image_id: "14518"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14518 -->
Die Lichtschranke sorgt dafür, daß die Anzeige immer an derselben Stelle beginnt.
