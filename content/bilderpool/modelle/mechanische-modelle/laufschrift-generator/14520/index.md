---
layout: "image"
title: "Bild 07 - Sekundenanzeige Zahl 47"
date: "2008-05-16T07:15:06"
picture: "Bild_07_-_Sekundenanzeige_Zahl_47.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14520
- /detailsf4b9.html
imported:
- "2019"
_4images_image_id: "14520"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14520 -->
Auf dem Foto kann man die Qualität leider nicht so gut erkennen. Aber in Wirklichkeit steht die Anzeige auf einer Stelle.
