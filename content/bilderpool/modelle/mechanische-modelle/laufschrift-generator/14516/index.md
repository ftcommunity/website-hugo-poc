---
layout: "image"
title: "Bild 03 - Detail LEDs und IC"
date: "2008-05-16T07:15:05"
picture: "Bild_03_-_Detail_LEDs_und_IC.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14516
- /detailsae2e-2.html
imported:
- "2019"
_4images_image_id: "14516"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14516 -->
Rechts im Bild ist der IC, der mir aus nur 3 Datenleitungen 7 Ausgänge für die LEDs kombiniert.
