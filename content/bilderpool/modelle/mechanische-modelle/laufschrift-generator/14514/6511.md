---
layout: "comment"
hidden: true
title: "6511"
date: "2008-05-23T22:43:28"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hi Andreas,

das klingt super - endlich mal einer der was mit den "alten" schnellen Interfaces an Rechnern unter DOS macht :) 

Was mich wundert, das sind II also welche mit Serieller Schnittstelle und du schreibst was von schneller LPT - umgebaut? 

Die Buchse auf dem Interface ist für ein Flachbandkabel wie beim Vorgängermodell?

Was für ein Rechner ist das genau? 486'er oder Pentium xxx?

Nähere Angaben dazu würden mich sehr interessieren :)