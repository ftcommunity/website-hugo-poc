---
layout: "image"
title: "von oben"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40070
- /details7a4e.html
imported:
- "2019"
_4images_image_id: "40070"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40070 -->
