---
layout: "image"
title: "Trichter"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40071
- /detailsa007.html
imported:
- "2019"
_4images_image_id: "40071"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40071 -->
