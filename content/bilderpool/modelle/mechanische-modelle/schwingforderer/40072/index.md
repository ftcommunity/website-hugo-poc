---
layout: "image"
title: "Förderrinne"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40072
- /detailseda1.html
imported:
- "2019"
_4images_image_id: "40072"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40072 -->
