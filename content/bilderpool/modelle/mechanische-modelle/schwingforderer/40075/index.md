---
layout: "image"
title: "Antrieb 1"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40075
- /detailsd779.html
imported:
- "2019"
_4images_image_id: "40075"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40075 -->
Durch den Antrieb wird die Rinne in Kreisschwingungen versetzt und wirft somit das Fördergut nach vorne.
Damit der Ausschlag der Schwingung der Rinne klein ist, musste ich den Ausschlag der Kurbel durch den Hebel verkleinern (sonst springt das Fördergut wild auf der Rinne umher).