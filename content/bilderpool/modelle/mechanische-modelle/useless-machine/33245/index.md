---
layout: "image"
title: "useless1"
date: "2011-10-19T16:49:06"
picture: "DSC01615.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/33245
- /details7330-2.html
imported:
- "2019"
_4images_image_id: "33245"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33245 -->
Hallo,
hier seht Ihr meine "useless machine", Wenn man den Schalter auf "On" stellt kommt ein Arm aus dem Kasten und stellt den Schalter auf "Off" und fährt wieder zurück. Ich habe die Steuerung rein elektromechanisch gelöst und zwar so: Der Arm ist mit einer Nockenscheibe verbunden, deren antrieb von einem Taster unterbrochen wird, betätigt man nun den Schalter wird der Taster überbrückt und der Arm kommt heraus macht den Schalter aus. Da der Taster nur dann betätigt wird, wenn der Arm innen ist, fährt der Arm wieder hinein.

LG
Lukas