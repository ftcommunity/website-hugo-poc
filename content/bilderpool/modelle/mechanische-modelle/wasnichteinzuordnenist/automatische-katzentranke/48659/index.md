---
layout: "image"
title: "Die Tränke"
date: 2020-05-09T17:24:27+02:00
picture: "2020-05-05 Automatische Katzentränke1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kann die Katze ihren Kopf unters Wasser halten. Dadurch unterbricht sie die Lichtschranke mit einer ft-LED und einem ft-Fotowiderstand. Das wiederum bewirkt, dass ca. 30 Sekunden lang sanft Wasser fließt. Das Wasser stammt aus dem der Box 500 selbst und läuft also im Kreis.
