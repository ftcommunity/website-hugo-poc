---
layout: "image"
title: "Die Pumpeinheit"
date: 2020-05-09T17:24:25+02:00
picture: "2020-05-05 Automatische Katzentränke3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Pumpe ist eine Membranpumpe von Lemo Solar, die ich schon lange für fischertechnik - vornehmlich als Kompressor - nutze. In der rein pneumatisch gesteuerten Abfüllanlage (auch auf der ftc zu finden) durfte sie schon einmal Wasser pumpen.

Das Prinzip, das gepumpte Wasser zunächst oben in einen Pneumatik-Tank zu pumpen und erst aus diesem unten in Richtung Tränke laufen zu lassen, bewährt sich auch hier: Die Pumpe pumpt "in Schlägen", und der Luftinhalt des Tanks dämpft das wunderbar, sodass eins stetiger Strom am Ausgang entsteht.

Am Schalter kann man die Anlage ein- und ausschalten.
