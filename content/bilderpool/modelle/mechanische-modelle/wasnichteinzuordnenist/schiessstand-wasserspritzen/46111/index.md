---
layout: "image"
title: "Getroffen!"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/46111
- /details04dd.html
imported:
- "2019"
_4images_image_id: "46111"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46111 -->
Bei einem richtigen Treffer klappt das Fallziel nach hinten und der Anzeiger ist oben.