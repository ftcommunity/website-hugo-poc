---
layout: "image"
title: "Die Trefferanzeigen"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen7.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/46116
- /detailsba26.html
imported:
- "2019"
_4images_image_id: "46116"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46116 -->
X-Streben 169,6 tragen je einen WT15 in gelb. Klappt das Ziel hinter, schwingt die Strebe mit dem WT15 nach oben und zeigt den Treffer an. Eigentlich sieht man es schon an den Bauplatten selbst. Aber: Das Gimmick hat den Zweck den Schwerpunkt des aufgerichteten Ziels noch etwas mehr nach vorne zu verlagern. So steht es noch etwas stabiler. Deswegen auch die langen 169,6-Streben.