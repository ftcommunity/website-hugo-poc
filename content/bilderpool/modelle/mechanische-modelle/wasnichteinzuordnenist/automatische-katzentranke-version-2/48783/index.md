---
layout: "image"
title: "3/4-Ansicht der Anlage"
date: 2020-07-09T12:54:32+02:00
picture: "Katzentraenke2_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So ungefähr muss das aus Katzenperspektive aussehen.
