---
layout: "image"
title: "Detailblick auf die Elektronik"
date: 2020-07-09T12:54:36+02:00
picture: "Katzentraenke2_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nur weil's so schön aussieht ;-) Der Schwellwertschalter überwacht einstellbar empfindlich die Lichtschranke. Bei Unterbrechung derselben wird das Monoflop im mittleren IC-Baustein getriggert, das über die Leistungsstufe rechts das Netzschaltgerät ansteuert.
