---
layout: "image"
title: "Die Ewigkeitsmaschine"
date: "2012-10-28T22:56:05"
picture: "Ewigkeitsmaschine_Kopie.jpg"
weight: "1"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Ewigkeitsmaschine"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/36075
- /detailsf67e.html
imported:
- "2019"
_4images_image_id: "36075"
_4images_cat_id: "2685"
_4images_user_id: "46"
_4images_image_date: "2012-10-28T22:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36075 -->
Das Bild ist nicht wirklich toll, aber es gibt ein kleines Video zu der Maschine. Es ist nicht viel, aber der Mensch freut sich und es ist jetzt schon mein zweites.

http://youtu.be/AZ3EDa-qM34

Viel Spass.
