---
layout: "image"
title: "Gesamtansicht 2"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/38572
- /details31a6.html
imported:
- "2019"
_4images_image_id: "38572"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38572 -->
So weit kann der Kranarm ausgefahren werden. Der Kranhaken selbst verfügt ebenfalls über einen einfachen Flaschenzug. Um die Proportionen nicht kaputt zu machen, habe ich beim Haken auf eine Umlenkrolle verzichtet - es funktioniert auch so.
