---
layout: "image"
title: "Liegestütz"
date: "2017-03-17T14:52:34"
picture: "liegestuetz1.jpg"
weight: "45"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/45553
- /details5edd.html
imported:
- "2019"
_4images_image_id: "45553"
_4images_cat_id: "335"
_4images_user_id: "1557"
_4images_image_date: "2017-03-17T14:52:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45553 -->
- "Wie viele Liegestütze schaffst Du?"
- "Alle! ... und noch Hundert dazu!"