---
layout: "image"
title: "Give-away 12"
date: "2015-04-22T16:44:16"
picture: "20150418_123744.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/40857
- /detailsb9d8.html
imported:
- "2019"
_4images_image_id: "40857"
_4images_cat_id: "3061"
_4images_user_id: "328"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40857 -->
Gabelstapler, 16 Teile