---
layout: "image"
title: "Give-away 1"
date: "2015-04-09T21:06:06"
picture: "20150407_175523.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/40738
- /detailsd44e.html
imported:
- "2019"
_4images_image_id: "40738"
_4images_cat_id: "3061"
_4images_user_id: "328"
_4images_image_date: "2015-04-09T21:06:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40738 -->
Radlader mit klappbarer Schaufel, 15 Teile