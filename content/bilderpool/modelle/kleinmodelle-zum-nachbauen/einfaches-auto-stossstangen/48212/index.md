---
layout: "image"
title: "Gesamtansicht"
date: "2018-10-14T22:59:16"
picture: "einfachesautomitstossstangen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48212
- /details4687.html
imported:
- "2019"
_4images_image_id: "48212"
_4images_cat_id: "3538"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:59:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48212 -->
Das "Dach" besteht aus Überrollbügeln, und die Stoßstangen sind gefedert (hier sind zwei Winkelsteine 60° mit 3 Nuten verbaut; wer die nicht hat, nimmt normale und setzt z.B. BS7,5 dazwischen). Das Auto kann man ruhigen Gewissens gegen Hindernisse oder die Wand fahren lassen. Der Spoiler war ebenfalls eine Anforderung der Stake Holder ;-)
