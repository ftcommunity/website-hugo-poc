---
layout: "image"
title: "FTMann  -  einer (skiff)"
date: "2017-05-22T13:32:39"
picture: "ze-skiff.jpg"
weight: "52"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- /php/details/45921
- /detailsda77.html
imported:
- "2019"
_4images_image_id: "45921"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-05-22T13:32:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45921 -->
