---
layout: "image"
title: "Seitenansicht"
date: "2009-02-01T19:35:37"
picture: "propellerauto1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17229
- /details0491.html
imported:
- "2019"
_4images_image_id: "17229"
_4images_cat_id: "1545"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17229 -->
