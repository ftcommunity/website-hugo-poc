---
layout: "image"
title: "Gesamtansicht"
date: "2009-02-01T19:35:38"
picture: "propellerauto4.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17232
- /detailsc73a-2.html
imported:
- "2019"
_4images_image_id: "17232"
_4images_cat_id: "1545"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17232 -->
Hier sieht man den Motor, den Empfänger und den Batteriekasten.