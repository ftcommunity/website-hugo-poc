---
layout: "image"
title: "Gesamtansicht"
date: "2009-02-01T19:35:38"
picture: "propellerauto3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/17231
- /details5e62.html
imported:
- "2019"
_4images_image_id: "17231"
_4images_cat_id: "1545"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17231 -->
