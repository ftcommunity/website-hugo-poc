---
layout: "image"
title: "Paddelboot"
date: "2008-01-31T14:31:37"
picture: "DSCN2071.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13485
- /details4a74.html
imported:
- "2019"
_4images_image_id: "13485"
_4images_cat_id: "1231"
_4images_user_id: "184"
_4images_image_date: "2008-01-31T14:31:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13485 -->
drei Mann in einem Boot .....
