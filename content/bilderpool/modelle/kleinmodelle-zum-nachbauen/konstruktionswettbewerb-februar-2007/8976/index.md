---
layout: "image"
title: "Jettaheizer (2)"
date: "2007-02-12T06:12:49"
picture: "konstruktionswettbewerb4.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8976
- /detailsc0d5.html
imported:
- "2019"
_4images_image_id: "8976"
_4images_cat_id: "817"
_4images_user_id: "104"
_4images_image_date: "2007-02-12T06:12:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8976 -->
5 Teile mit Seil
