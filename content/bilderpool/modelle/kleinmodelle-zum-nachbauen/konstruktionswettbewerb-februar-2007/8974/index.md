---
layout: "image"
title: "Fredy (2)"
date: "2007-02-12T06:12:49"
picture: "konstruktionswettbewerb2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/8974
- /detailsc668.html
imported:
- "2019"
_4images_image_id: "8974"
_4images_cat_id: "817"
_4images_user_id: "104"
_4images_image_date: "2007-02-12T06:12:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8974 -->
6 Teile mit Seil (mit Fredy per Mail geeinigt; eingereicht waren 5)
