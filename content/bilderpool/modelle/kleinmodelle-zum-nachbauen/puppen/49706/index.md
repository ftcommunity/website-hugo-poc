---
layout: "image"
title: "Maschinenraum"
date: 2023-03-28T17:43:21+02:00
picture: "Puppen_16.jpeg"
weight: "16"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " tanzen", " diorama"]
uploadBy: "Website-Team"
license: "unknown"
---

5 Motoren ergeben fünf verschiedene Tanzgruppen