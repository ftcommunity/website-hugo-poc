---
layout: "image"
title: "Ma-gi-er (4)"
date: "2007-05-01T19:08:42"
picture: "wettbewerb4.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10270
- /detailse2b4-2.html
imported:
- "2019"
_4images_image_id: "10270"
_4images_cat_id: "931"
_4images_user_id: "104"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10270 -->
