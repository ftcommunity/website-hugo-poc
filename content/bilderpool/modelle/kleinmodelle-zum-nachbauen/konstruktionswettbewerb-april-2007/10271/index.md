---
layout: "image"
title: "Ma-gi-er (5)"
date: "2007-05-01T19:08:42"
picture: "wettbewerb5.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10271
- /details3a3f.html
imported:
- "2019"
_4images_image_id: "10271"
_4images_cat_id: "931"
_4images_user_id: "104"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10271 -->
