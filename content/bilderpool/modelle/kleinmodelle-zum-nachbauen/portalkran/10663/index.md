---
layout: "image"
title: "Von der Seite"
date: "2007-06-02T21:36:02"
picture: "portalkran8.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10663
- /detailse574.html
imported:
- "2019"
_4images_image_id: "10663"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10663 -->
Er ist etwas wackelig, hält aber auf den Schienen.