---
layout: "image"
title: "Rollen"
date: "2007-06-02T21:36:01"
picture: "portalkran3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10658
- /details0aab.html
imported:
- "2019"
_4images_image_id: "10658"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10658 -->
Diese werden dann auf ft Schienen gestellt.