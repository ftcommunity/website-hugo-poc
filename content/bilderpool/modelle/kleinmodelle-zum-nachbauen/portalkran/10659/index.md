---
layout: "image"
title: "Gesamtansicht ohne Laufkatze"
date: "2007-06-02T21:36:02"
picture: "portalkran4.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10659
- /detailsaee9-2.html
imported:
- "2019"
_4images_image_id: "10659"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10659 -->
Der Portalkran besteht aus einem einfachen statik Gerüst.