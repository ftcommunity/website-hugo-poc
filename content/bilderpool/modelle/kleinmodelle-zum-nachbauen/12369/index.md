---
layout: "image"
title: "Flieger 01.JPG"
date: "2007-10-28T13:06:38"
picture: "Flieger_01.jpg"
weight: "2"
konstrukteure: 
- "Andreas Diehlmann"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12369
- /details3e2d.html
imported:
- "2019"
_4images_image_id: "12369"
_4images_cat_id: "335"
_4images_user_id: "4"
_4images_image_date: "2007-10-28T13:06:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12369 -->
Den Flieger hat mein Neffe Andreas (7) ganz alleine gebaut. Irgend etwas hat da abgefärbt, vermute ich :-)
