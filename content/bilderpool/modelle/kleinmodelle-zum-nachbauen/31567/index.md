---
layout: "image"
title: "Standfuß für Festplattengehäuse"
date: "2011-08-11T00:09:57"
picture: "standfussfuerfestplattengehaeues1.jpg"
weight: "32"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/31567
- /detailsb600-2.html
imported:
- "2019"
_4images_image_id: "31567"
_4images_cat_id: "335"
_4images_user_id: "9"
_4images_image_date: "2011-08-11T00:09:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31567 -->
:-)