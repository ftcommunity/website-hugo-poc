---
layout: "image"
title: "Halterung für den Schraubendreher"
date: "2013-08-06T19:48:58"
picture: "zieher1.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37231
- /details3a00-2.html
imported:
- "2019"
_4images_image_id: "37231"
_4images_cat_id: "2770"
_4images_user_id: "453"
_4images_image_date: "2013-08-06T19:48:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37231 -->
35973 ;)
