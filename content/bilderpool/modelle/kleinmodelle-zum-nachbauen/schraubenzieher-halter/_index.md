---
layout: "overview"
title: "Schraubenzieher Halter"
date: 2020-02-22T08:33:08+01:00
legacy_id:
- /php/categories/2770
- /categories5bed.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2770 --> 
Halter für die kleinen gelben Fischertechnik-Schraubenzieher.