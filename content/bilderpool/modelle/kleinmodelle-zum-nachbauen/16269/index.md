---
layout: "image"
title: "Helicopter from"
date: "2008-11-14T00:31:45"
picture: "Amelia_helicopter.jpg"
weight: "16"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Helicopter"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16269
- /detailsfcd9.html
imported:
- "2019"
_4images_image_id: "16269"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-11-14T00:31:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16269 -->
This is my daughter with the Junior Starter Jumbo Pack's helicopter.