---
layout: "image"
title: "Halterung 'Freilauachsen'"
date: "2006-12-10T13:25:13"
picture: "magi04.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7809
- /details77aa.html
imported:
- "2019"
_4images_image_id: "7809"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7809 -->
Das ist von Profi Solar abgeguckt.