---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-10T13:25:13"
picture: "magi01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7806
- /details33e8.html
imported:
- "2019"
_4images_image_id: "7806"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7806 -->
Dieses Fahrzeug kann nicht umfliegen ( sofern man es gut nachbaut...)