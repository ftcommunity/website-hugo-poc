---
layout: "image"
title: "Tow Truck"
date: "2008-10-16T00:46:14"
picture: "sm_towtruck.jpg"
weight: "14"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["tow", "truck"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15997
- /details7ce2.html
imported:
- "2019"
_4images_image_id: "15997"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15997 -->
My variation on a tow truck. This one uses a  Large Pulley for the winch. Thought to share.