---
layout: "image"
title: "Hafenkran"
date: 2020-05-13T16:53:47+02:00
picture: "hafenkran05j9e.jpg"
weight: "3"
konstrukteure: 
- "Box 125-1 Advanced Mini Cranes"
fotografen:
- "Pilami"
uploadBy: "Website-Team"
license: "unknown"
---

Der Osterhase beim anstrengenden Verladen der Osterware
