---
layout: "overview"
title: Ostergewinnspiel 2020
date: 2020-05-04T21:17:10+02:00
---
Zu Ostern 2020 [veranstaltete](https://forum.ftcommunity.de/viewtopic.php?f=23&t=5985#p44318) der ftc Modellbau e.V. für seine Mitglieder ein Gewinnspiel.
Die Aufgabe war es, ein Modell aus den Teilen des [Community-Kasten Advanced Mini Cranes](https://ftcommunity.de/knowhow/bauanleitungen/osterei/) zu konstruieren und ein Bild davon zu posten.
Unter den Teilnehmern wurden drei Exemplare der 3D-Druck-Sortierwanne 125 mit Grundplatte 125 verlost.
Die Ziehung der Gewinner ist in diesem [Video](https://youtu.be/xSHSxK0L79I) dokumentiert.



