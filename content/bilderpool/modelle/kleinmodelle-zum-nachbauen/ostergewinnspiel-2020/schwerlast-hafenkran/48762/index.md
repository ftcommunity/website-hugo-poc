---
layout: "image"
title: "Seilzug"
date: 2020-05-14T17:57:30+02:00
picture: "Seilzug.jpg"
weight: "3"
konstrukteure: 
- "Matthias Dettmer (fitenerd)"
fotografen:
- "Marie Cecile Dettmer"
uploadBy: "Website-Team"
license: "unknown"
---

Seilzug, festes Ende Seil, eine weitere Rolle des Flaschenzugs