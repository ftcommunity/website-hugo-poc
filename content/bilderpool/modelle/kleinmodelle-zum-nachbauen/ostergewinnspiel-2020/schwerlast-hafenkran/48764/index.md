---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-14T17:57:32+02:00
picture: "Hafen-Schwerlastkran.jpg"
weight: "1"
konstrukteure: 
- "Matthias Dettmer (fitenerd)"
fotografen:
- "Marie Cecile Dettmer"
uploadBy: "Website-Team"
license: "unknown"
---

