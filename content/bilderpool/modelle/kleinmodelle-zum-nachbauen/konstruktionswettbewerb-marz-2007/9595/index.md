---
layout: "image"
title: "Masked 1"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb25.jpg"
weight: "25"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9595
- /details6055.html
imported:
- "2019"
_4images_image_id: "9595"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9595 -->
Originalbeschreibung:

Die Stückliste:
1x U-Träger 150
6x Nabenmutter mit Scheibe (mit 5 wird's unsymmetrisch)
6x Flachnabenzange ( " )
2x Zahnrad Z 20
4x Zahnrad Z 40
2x Achse beliebige Länge
2x Baustein 5 (als Motorhalterung)
2x Federnocken (als Motorhalterung)
1x Minimotor
1x Getriebe
1x "U-Achse 60 mit Zahnrad Z 28"
2x Ketten

Macht dann alles zusammen genau 30 Teile (wenn ich mich nicht verzählt habe).
Falls Ketten nicht als 1 Teil gelten sollten, bin ich weit weit weg.
Eine Bedingung gibt es, ohne die das Ganze nicht funktioniert: Das "Fahrzeug" muss mit Motor voran über das Hindernis fahren.
