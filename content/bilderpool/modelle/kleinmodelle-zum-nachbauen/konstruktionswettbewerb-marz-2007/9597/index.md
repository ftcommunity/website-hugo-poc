---
layout: "image"
title: "Masked 3"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb27.jpg"
weight: "27"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9597
- /details051d.html
imported:
- "2019"
_4images_image_id: "9597"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9597 -->
