---
layout: "image"
title: "Jettaheizer 4"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb07.jpg"
weight: "7"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9577
- /detailsbcaa.html
imported:
- "2019"
_4images_image_id: "9577"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9577 -->
