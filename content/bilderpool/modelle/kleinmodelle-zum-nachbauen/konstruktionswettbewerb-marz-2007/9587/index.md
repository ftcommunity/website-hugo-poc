---
layout: "image"
title: "Joker 3"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb17.jpg"
weight: "17"
konstrukteure: 
- "Joker"
fotografen:
- "Joker"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9587
- /detailsa7f1-2.html
imported:
- "2019"
_4images_image_id: "9587"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9587 -->
