---
layout: "image"
title: "Jiriki 3"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb12.jpg"
weight: "12"
konstrukteure: 
- "Jiriki"
fotografen:
- "Jiriki"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9582
- /details9a0d.html
imported:
- "2019"
_4images_image_id: "9582"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9582 -->
