---
layout: "image"
title: "Jettaheizer 5"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb08.jpg"
weight: "8"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9578
- /details489c.html
imported:
- "2019"
_4images_image_id: "9578"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9578 -->
