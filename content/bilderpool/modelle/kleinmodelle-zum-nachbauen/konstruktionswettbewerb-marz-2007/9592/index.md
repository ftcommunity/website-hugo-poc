---
layout: "image"
title: "Ma-gi-er 3"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb22.jpg"
weight: "22"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9592
- /detailsa861.html
imported:
- "2019"
_4images_image_id: "9592"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9592 -->
