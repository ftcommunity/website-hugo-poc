---
layout: "image"
title: "Jettaheizer 1"
date: "2007-03-19T15:08:21"
picture: "konstruktionswettbewerb04.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9574
- /details2b6c.html
imported:
- "2019"
_4images_image_id: "9574"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9574 -->
Originalbeschreibung: 

- 2x 31023 Klemmbuchse 10 mit Federring rot
- 2x 31057 Raupenband 230mm lang
- 2x 31063 U-Achse 60 mit Zahnrad Z28
- 2x 31078 U-Getriebe mini schwarz
- 4x 31879 Felge 43 rot
- 1x 32064 Baustein 15 mit Bohrung rot
- 2x 32293 S-Motor 6-9V schwarz
- 2x 32850 Riegelstein 15x15 schwarz
- 1x 32854 U-Träger 150 schwarz
- 4x 35033 Nabenmutter rot
- 4x 36323 S-Riegel 4mm rot
- 2x 36334 Riegelscheibe rot
- 2x 37238 Baustein 5 mit zwei Zapfen rot
- 1x 37384 Achse 80 Metall d=4mm
- 4x 38190 Reifen 65 "G30 FT41"
- 2x 38538 I-Strebe 30 mit Loch gelb

Gesamt: 37 Teile
