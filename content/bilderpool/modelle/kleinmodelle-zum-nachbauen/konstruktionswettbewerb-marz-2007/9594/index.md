---
layout: "image"
title: "Ma-gi-er 5"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb24.jpg"
weight: "24"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/9594
- /detailsb5cf-2.html
imported:
- "2019"
_4images_image_id: "9594"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9594 -->
