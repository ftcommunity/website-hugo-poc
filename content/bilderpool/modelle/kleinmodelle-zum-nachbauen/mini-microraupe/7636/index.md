---
layout: "image"
title: "Miniraupe Grundmodell"
date: "2006-11-27T19:12:54"
picture: "Miniraupe02b.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7636
- /detailsc19a.html
imported:
- "2019"
_4images_image_id: "7636"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7636 -->
Hier mal mit abgenommener Kette. Gleicher Aufbau auf der anderen Seite. Die Leitungen müssen hier von der Seite angesteckt und nach oben geführt werden, weil sonst die Kette drin verhakt.

Hatte bei diesem und dem vorherigen Bild leider den falschen Weißabgleich an der Cam eingestellt und besser bekam ich die Bilder nicht mehr hin... 
.oO(Asche auf mein Haupt)