---
layout: "image"
title: "Miniraupe Grundmodell"
date: "2006-11-27T19:12:54"
picture: "Miniraupe01b.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7635
- /detailse8a0.html
imported:
- "2019"
_4images_image_id: "7635"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7635 -->
Heute habe ich mal eine Mini-Raupe gebaut. 
Ein winziges Modell, aus dem man viel machen kann.

Nur laufen will das kleine Biest nicht so wie es soll. Mit einem 6V-Powerblock tut es sich sehr schwer, und auch mit 6,8V aus dem FT-Trafo läuft das Teilchen nur widerwillig. Außerdem zieht die Miniraupe trotz weicher Kettenglieder bei Fahrt mit Getriebe voraus immer leicht nach rechts weg.