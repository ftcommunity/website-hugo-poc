---
layout: "image"
title: "Miniraupe Variante 4"
date: "2006-11-27T19:13:05"
picture: "Miniraupe06b.jpg"
weight: "6"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7640
- /details3cc7.html
imported:
- "2019"
_4images_image_id: "7640"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:13:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7640 -->
Oder auch einen Mini-Panzer (wem´s denn gefällt...).