---
layout: "image"
title: "Microraupe"
date: "2006-11-27T19:13:05"
picture: "Microraupe01b.jpg"
weight: "7"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7641
- /details99c2.html
imported:
- "2019"
_4images_image_id: "7641"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:13:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7641 -->
Und wer denkt, kleiner geht nimmer, sei hier eines besseren belehrt!

Ich präsentiere die wahrscheinlich kleinste motorisierte Fischertechnik-Raupe der Welt!

Bei diesem Modell kann man wirklich nichts mehr weglassen...

Erstaunlicherweise rennt die Microraupe besser als die Miniraupe, sogar mit einem 4,5V-Batteriestab. Allerdings zieht das Modell wie auch schon die Miniraupe auf glattem Boden bei Fahrt mit Getriebe voraus immer nach rechts, allerdings mangels Haftung stärker als die Miniraupe.