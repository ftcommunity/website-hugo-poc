---
layout: "image"
title: "Segway"
date: 2023-03-27T16:03:58+02:00
picture: "sport_11.jpeg"
weight: "11"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " Kleinmodelle"]
uploadBy: "Website-Team"
license: "unknown"
---

