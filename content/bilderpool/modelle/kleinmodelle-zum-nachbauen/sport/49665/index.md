---
layout: "image"
title: "Pogo-stick"
date: 2023-03-27T16:03:59+02:00
picture: "sport_10.jpeg"
weight: "10"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " Kleinmodelle"]
uploadBy: "Website-Team"
license: "unknown"
---

