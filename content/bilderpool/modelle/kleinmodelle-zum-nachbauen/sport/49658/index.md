---
layout: "image"
title: "Turnen - Schwebebalken"
date: 2023-03-27T16:03:49+02:00
picture: "sport_5.jpeg"
weight: "5"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " Kleinmodelle"]
uploadBy: "Website-Team"
license: "unknown"
---

