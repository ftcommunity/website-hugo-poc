---
layout: "image"
title: "Turnen - Stufenbarren"
date: 2023-03-27T16:03:51+02:00
picture: "sport_4.jpeg"
weight: "4"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " Kleinmodelle"]
uploadBy: "Website-Team"
license: "unknown"
---

