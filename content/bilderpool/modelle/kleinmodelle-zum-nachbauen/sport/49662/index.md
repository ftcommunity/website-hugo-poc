---
layout: "image"
title: "Billard"
date: 2023-03-27T16:03:55+02:00
picture: "sport_13.jpeg"
weight: "13"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["FT Mann", " FT Frau", " Kleinmodelle"]
uploadBy: "Website-Team"
license: "unknown"
---

