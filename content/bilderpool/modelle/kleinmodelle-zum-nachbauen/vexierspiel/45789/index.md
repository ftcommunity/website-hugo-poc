---
layout: "image"
title: "Fertiges Modell"
date: 2023-01-19T12:00:45+01:00
picture: "Vexierspiel_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der drehbare Grundbaustein mit Bohrung dient als Griff. An dem kann man gar lustig kurbeln. Wer das nicht kennt - ausprobieren!