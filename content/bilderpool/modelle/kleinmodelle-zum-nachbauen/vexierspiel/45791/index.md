---
layout: "image"
title: "Baustufe 1"
date: 2023-01-19T12:00:47+01:00
picture: "Vexierspiel_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Original gab's im Clubheft 1976-4 (https://ft-datenbank.de/tickets?fulltext=1976-4). Dieser Nachbau benötigt nur aktuell verfügbare Bauteile.