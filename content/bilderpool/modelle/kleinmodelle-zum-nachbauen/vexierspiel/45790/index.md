---
layout: "image"
title: "Baustufe 2"
date: 2023-01-19T12:00:46+01:00
picture: "Vexierspiel_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In den so hergestellten Schienen können die beiden Gleitstücke bewegt werden. Dafür, dass sich nichts ins Gehege kommt, sorgt der oben aufgesteckte drehbare Abstandshalter.