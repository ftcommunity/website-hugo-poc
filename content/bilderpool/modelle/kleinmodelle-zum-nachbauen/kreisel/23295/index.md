---
layout: "image"
title: "kreisel1.jpg"
date: "2009-03-01T20:05:03"
picture: "kreisel2.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23295
- /details10a4.html
imported:
- "2019"
_4images_image_id: "23295"
_4images_cat_id: "1583"
_4images_user_id: "845"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23295 -->
