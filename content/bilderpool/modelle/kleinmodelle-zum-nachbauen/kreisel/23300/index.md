---
layout: "image"
title: "kreisel3.jpg"
date: "2009-03-01T20:05:03"
picture: "kreisel7.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23300
- /detailsbf5f.html
imported:
- "2019"
_4images_image_id: "23300"
_4images_cat_id: "1583"
_4images_user_id: "845"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23300 -->
<i>Kommentar der ftc-admins: Wir haben da mal ein wenig aussortiert, 3 Bilder reichen bei diesem Modell völlig aus. Aber sehr schöne Idee.</i>