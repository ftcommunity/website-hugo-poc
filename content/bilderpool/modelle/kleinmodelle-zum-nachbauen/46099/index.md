---
layout: "image"
title: "FT-Männer machen Urlaub"
date: "2017-07-25T17:00:51"
picture: "ftmaninurlaub.jpg"
weight: "57"
konstrukteure: 
- "Christian & Stefan"
fotografen:
- "Christian & Stefan"
schlagworte: ["FT-Mann", "Urlaub", "Convention", "Bensem", "Angel", "Fernglas", "Bank"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- /php/details/46099
- /details4eba-2.html
imported:
- "2019"
_4images_image_id: "46099"
_4images_cat_id: "335"
_4images_user_id: "2611"
_4images_image_date: "2017-07-25T17:00:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46099 -->
Während Tim noch beim Angeln entspannt, hält Tom schon Ausschau nach neuen Modellen für die Maker Faire in Bensem und der Süd-Convention in Dreieich.