---
layout: "image"
title: "Modell C - Roland Enzenhofer (1): Doppeldecker"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44360
- /detailsa60d.html
imported:
- "2019"
_4images_image_id: "44360"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44360 -->
Das erste von mehreren von ihm eingereichten Flugmodellen ist der Doppeldecker.
