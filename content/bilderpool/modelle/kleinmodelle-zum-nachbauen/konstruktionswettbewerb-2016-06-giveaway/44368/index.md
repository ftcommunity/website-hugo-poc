---
layout: "image"
title: "Modell G - Roland Enznhofer (5): Hot Buggy"
date: "2016-09-12T10:44:58"
picture: "giveawayfuermakerfaire11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44368
- /detailsd064.html
imported:
- "2019"
_4images_image_id: "44368"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44368 -->
