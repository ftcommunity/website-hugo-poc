---
layout: "image"
title: "Modell A - Jens Lemkamp: Gummiring-Pistole"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44358
- /details49f0.html
imported:
- "2019"
_4images_image_id: "44358"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44358 -->
Jens schlät dieses Modell vor, für das es in der ft:pedia 2016-2 eine ausführliche Beschreibung gibt: https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-2.pdf
