---
layout: "image"
title: "Modell E - Roland Enzenhofer (3): Hubschrauber"
date: "2016-09-12T10:44:52"
picture: "giveawayfuermakerfaire07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/44364
- /details2327.html
imported:
- "2019"
_4images_image_id: "44364"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44364 -->
