---
layout: "image"
title: "Pedale"
date: "2007-04-12T10:05:11"
picture: "velo10.jpg"
weight: "10"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10065
- /details8faa-2.html
imported:
- "2019"
_4images_image_id: "10065"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10065 -->
