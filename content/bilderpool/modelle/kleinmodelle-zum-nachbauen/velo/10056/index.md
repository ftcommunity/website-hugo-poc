---
layout: "image"
title: "Ganzes Velo"
date: "2007-04-12T10:05:10"
picture: "velo01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10056
- /details4566.html
imported:
- "2019"
_4images_image_id: "10056"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10056 -->
Man siet hier die Kette nur im Hintergrund, aber die Übersetzung ist ssowiso nichts wert.