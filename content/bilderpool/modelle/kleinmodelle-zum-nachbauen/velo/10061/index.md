---
layout: "image"
title: "Schutzblech"
date: "2007-04-12T10:05:11"
picture: "velo06.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10061
- /detailse340.html
imported:
- "2019"
_4images_image_id: "10061"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10061 -->
Man kann es auch durch einen Winkelstein ersetzen.