---
layout: "image"
title: "Lenker"
date: "2007-04-12T10:05:11"
picture: "velo08.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10063
- /details1a58.html
imported:
- "2019"
_4images_image_id: "10063"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10063 -->
Die Klemmringe sollten sehr dicht am Bs 30 sein, damit der Lenker nicht wackelt und das Gleichgewicht beiinflusst.