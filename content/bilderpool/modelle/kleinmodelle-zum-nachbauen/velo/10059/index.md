---
layout: "image"
title: "Vorderrad Federung"
date: "2007-04-12T10:05:11"
picture: "velo04.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10059
- /details9615.html
imported:
- "2019"
_4images_image_id: "10059"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10059 -->
Leider gibt es manchmal die Situation dass sie Verklemmt.