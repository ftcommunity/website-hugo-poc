---
layout: "overview"
title: "Ruderboote und Kajaks"
date: 2023-03-28T21:29:22+02:00
---

Mehrere verschiedene Ruderboote und Kajaks für die fischertechnik Familie. Einige der Designs sind inspiriert von früheren Arbeiten von Primoz Cebulj (https://ftcommunity.de/konstrukteure/primoz-cebulj/)

