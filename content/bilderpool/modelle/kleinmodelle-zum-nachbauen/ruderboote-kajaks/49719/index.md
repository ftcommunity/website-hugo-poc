---
layout: "image"
title: "Ruderboot für 2"
date: 2023-03-28T21:29:29+02:00
picture: "Schiff_2.jpeg"
weight: "2"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Ruderboot", " Schiffe", " Kleinmodelle", " Sport", " FT Mann", " FT Frau"]
uploadBy: "Website-Team"
license: "unknown"
---

alte und neue Elemente