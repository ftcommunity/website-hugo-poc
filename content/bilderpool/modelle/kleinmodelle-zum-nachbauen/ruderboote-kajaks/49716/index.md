---
layout: "image"
title: "Ruderboot für 4"
date: 2023-03-28T21:29:25+02:00
picture: "Schiff_5.jpeg"
weight: "5"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Ruderboot", " Schiffe", " Kleinmodelle", " Sport", " FT Mann", " FT Frau"]
uploadBy: "Website-Team"
license: "unknown"
---

Endlich Verwendung der Fahrzeugelemente