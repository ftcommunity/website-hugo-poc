---
layout: "image"
title: "alte und neue Elemente"
date: 2023-03-28T21:29:27+02:00
picture: "Schiff_3.jpeg"
weight: "3"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Ruderboot", " Schiffe", " Kleinmodelle", " Sport", " FT Mann", " FT Frau"]
uploadBy: "Website-Team"
license: "unknown"
---

