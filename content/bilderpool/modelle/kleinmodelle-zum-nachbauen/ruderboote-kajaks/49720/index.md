---
layout: "image"
title: "Kayak für 3"
date: 2023-03-28T21:29:30+02:00
picture: "Schiff_1.jpeg"
weight: "1"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Ruderboot", " Schiffe", " Kleinmodelle", " Sport", " FT Mann", " FT Frau"]
uploadBy: "Website-Team"
license: "unknown"
---

farblich abgestimmt