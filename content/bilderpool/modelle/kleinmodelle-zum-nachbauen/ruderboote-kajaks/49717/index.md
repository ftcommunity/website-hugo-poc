---
layout: "image"
title: "Ruderboot für 4"
date: 2023-03-28T21:29:26+02:00
picture: "Schiff_4.jpeg"
weight: "4"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["Ruderboot", " Schiffe", " Kleinmodelle", " Sport", " FT Mann", " FT Frau"]
uploadBy: "Website-Team"
license: "unknown"
---

