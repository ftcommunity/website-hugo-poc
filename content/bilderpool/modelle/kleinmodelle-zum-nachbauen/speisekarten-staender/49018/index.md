---
layout: "image"
title: "Rückseite"
date: 2021-02-19T22:19:28+01:00
picture: "Speisekarten-Staender2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---Schnell konstruiert, aber hinreichend stabil.