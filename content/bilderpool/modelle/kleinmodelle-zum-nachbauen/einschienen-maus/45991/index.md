---
layout: "image"
title: "Polwendeeinheit - Schieber"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45991
- /detailse2c1.html
imported:
- "2019"
_4images_image_id: "45991"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45991 -->
Der Schieber trägt auf einer Seite eine 15x15-Abdeckung.
