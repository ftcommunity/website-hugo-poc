---
layout: "image"
title: "Polwendeeinheit - Tasterbefestigung"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45993
- /details687c-2.html
imported:
- "2019"
_4images_image_id: "45993"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45993 -->
Die Taster sind stabil befestigt. Diese Baugruppe kann einfach von oben in die Trägereinheit gesetzt werden.
