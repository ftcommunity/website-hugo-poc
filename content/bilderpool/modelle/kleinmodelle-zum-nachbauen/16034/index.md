---
layout: "image"
title: "Gewichtheber"
date: "2008-10-22T21:18:56"
picture: "gewichtheber1.jpg"
weight: "15"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16034
- /detailsc2c4.html
imported:
- "2019"
_4images_image_id: "16034"
_4images_cat_id: "335"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T21:18:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16034 -->
Ein kleines Model zum Nachbauen.