---
layout: "image"
title: "Gesamtansicht"
date: 2023-01-19T11:58:04+01:00
picture: "Giraffe_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit einem kleinen Besuchskind zusammen entstand diese einfache Giraffe.