---
layout: "image"
title: "Beine"
date: 2023-01-19T11:58:02+01:00
picture: "Giraffe_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Damit die Beine in alle Richtungen nach außen zeigen, sind je Bein zwei Winkelsteine 15° verbaut.