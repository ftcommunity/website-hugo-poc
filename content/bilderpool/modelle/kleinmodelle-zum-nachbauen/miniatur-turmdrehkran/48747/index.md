---
layout: "image"
title: "Laufkatze"
date: 2020-05-12T15:56:31+02:00
picture: "2020-01-13 Miniatur-Turmdrehkran6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Sorry, da war die Kamera wohl zu nah am Objekt, um es scharf abzubilden.
