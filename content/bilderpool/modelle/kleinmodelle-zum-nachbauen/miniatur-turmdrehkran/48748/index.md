---
layout: "image"
title: "Tragseilantrieb"
date: 2020-05-12T15:56:33+02:00
picture: "2020-01-13 Miniatur-Turmdrehkran5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Seil gehört oben zwischen die beiden Riegelscheiben. Dass das hier nebendran verläuft, fiel mir erst nach den Aufnahmen auf.
