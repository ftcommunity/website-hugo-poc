---
layout: "overview"
title: "Minimalistisches Rennauto"
date: 2020-02-22T08:33:16+01:00
legacy_id:
- /php/categories/2926
- /categories5bba.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2926 --> 
Ein kleines Rennauto, welches man schnell nachbauen kann und das wenig Ressourcen (Steine) verbraucht.