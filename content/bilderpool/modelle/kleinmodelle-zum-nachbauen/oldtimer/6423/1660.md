---
layout: "comment"
hidden: true
title: "1660"
date: "2006-12-05T21:28:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Naja, es ergab sich einfach so. Als es vorne so altbacken aussah, hab ich hinten die Abschlussplatte schräg gestellt, damit es wenigstens ein bisschen wie ein richtiger Oldtimer und nicht wie ein halber aussieht. Aber die Form war mir bei diesem Modell eigentlich egal - ich wollte ja nur einen Test für das IR-Set.

Gruß,
Stefan