---
layout: "image"
title: "Fahrwerk"
date: "2006-06-13T22:47:53"
picture: "oldtimer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6426
- /detailsd61d.html
imported:
- "2019"
_4images_image_id: "6426"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6426 -->
Antrieb, Lenkung, keine Federung. Alles einfach gehalten.
