---
layout: "image"
title: "Blick von rechts hinten"
date: "2006-06-13T22:47:53"
picture: "oldtimer3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6425
- /details8645.html
imported:
- "2019"
_4images_image_id: "6425"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6425 -->
Der IR-Empfänger guckt zwecks guten Empfangs heraus.
