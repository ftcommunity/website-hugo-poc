---
layout: "image"
title: "Lenkung (1)"
date: "2006-06-13T22:47:53"
picture: "oldtimer5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6427
- /details68d0.html
imported:
- "2019"
_4images_image_id: "6427"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6427 -->
Die Lenkung ist etwa so aufgebaut wie in der Anleitung zum IR Control Set vorgeschlagen. Die kleine rote Achse betätigt in Geradeausstellung den Taster.
