---
layout: "image"
title: "Unterseite"
date: 2023-03-29T18:27:44+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Aufbau ist einfach. Die Vorderachse besteht neben den Rädern aus einem Baustein 7,5 und einer "Rastachse mit Platte". Damit alles gut passt, sitzen auf der erst zwei flache schwarze Distanzscheiben, bevor sie in den BS15 mit Bohrung geht. Oben auf ist einfach eine Rastkupplung quer aufgelegt - das ist der Griff zum Lenken.