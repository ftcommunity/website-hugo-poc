---
layout: "image"
title: "Heckansicht"
date: 2023-03-29T18:27:41+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier nochmal die beiden Haltemagnete. Sie sitzen recht weit außen auf dem BS7,5, damit sie die Achsen 110 genau treffen.