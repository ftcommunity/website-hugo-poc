---
layout: "image"
title: "Drehleiter"
date: 2023-03-29T18:27:46+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Gag ist natürlich die winzige Drehleiter. Sie besteht aus zwei Metallachsen 110 und nach Belieben vielen 10-mm-Neodym-Magneten dazwischen (die gibt es z.B. bei ffm).

ACHTUNG! GEFAHR! Neodym-Magnete sind enorm stark und gehören NICHT IN DIE HÄNDE KLEINER KINDER! Sie könnten zwei davon verschlucken, und dass kann zu Darmverschluss führen!