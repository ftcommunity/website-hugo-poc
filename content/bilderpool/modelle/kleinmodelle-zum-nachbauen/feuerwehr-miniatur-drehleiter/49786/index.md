---
layout: "image"
title: "Aufstell-Halterung"
date: 2023-03-29T18:27:45+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Achsen ragen hinten etwas heraus. Beim Aufstellen über das Gelenk aus der kleinen Klaue und einem der Magneten ziehen die beiden fest montierten Magnete an den Achsen und halten die Leiter so in aufgestellter Position.