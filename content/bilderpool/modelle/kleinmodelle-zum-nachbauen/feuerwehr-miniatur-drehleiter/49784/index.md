---
layout: "image"
title: "Drehmechanik"
date: 2023-03-29T18:27:43+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Drehachse sitzt auf denselben Radhalter-Stiften wie die sechs Räder.