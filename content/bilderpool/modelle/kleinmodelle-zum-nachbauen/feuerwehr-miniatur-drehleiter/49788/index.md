---
layout: "image"
title: "Gesamtansicht in Fahrt"
date: 2023-03-29T18:27:48+02:00
picture: "2023-03-25_Feuerwehr_mit_Miniatur-Drehleiter_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Vorderachse ist gelenkt. Deren Räder sind etwas nach hinten verschoben angebracht. Dadurch stellt sich das Fahrwerk beim Schieben leichter von selbst auf Geradeausfahrt, und der erreichbare Lenkeinschlag wird größer.

ACHTUNG! GEFAHR! Neodym-Magnete sind enorm stark und gehören NICHT IN DIE HÄNDE KLEINER KINDER! Sie könnten zwei davon verschlucken, und dass kann zu Darmverschluss führen!