---
layout: "overview"
title: "Feuerwehr mit Miniatur-Drehleiter"
date: 2023-03-29T18:27:41+02:00
---

Ein kleines Feuerwerauto mit einer kleinen Drehleiter aus Metallachsen und Neodym-Magneten.

ACHTUNG! GEFAHR! Neodym-Magnete sind enorm stark und gehören NICHT IN DIE HÄNDE KLEINER KINDER! Sie könnten zwei davon verschlucken, und dass kann zu Darmverschluss führen!