---
layout: "image"
title: "Die Lenkung"
date: "2006-12-06T23:23:53"
picture: "kleinesautomitlenkungundfederung3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7708
- /details0983.html
imported:
- "2019"
_4images_image_id: "7708"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7708 -->
Die beiden Winkelzahnräder greifen gerade so ineinander, aber die Lenkung funktioniert wunderbar. Da die senkrecht stehenden Achsen, die die vorderen Gelenke bilden, weiter auseinander stehen als die Drehpunkte der Gelenksteine, wird das kurveninnere Rad wie in echt etwas stärker eingeschlagen als das Kurvenäußere. Die beiden senkrechten Achsen könnten auch eine Nummer kleiner gewählt werden, aber davon waren im hobby-1 nicht mehr drin.
