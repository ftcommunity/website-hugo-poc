---
layout: "image"
title: "Maximaler Lenkeinschlag rechts"
date: "2006-12-06T23:23:54"
picture: "kleinesautomitlenkungundfederung6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7711
- /details0996-2.html
imported:
- "2019"
_4images_image_id: "7711"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7711 -->
So weit kann die Lenkung nach rechts eingeschlagen werden...
