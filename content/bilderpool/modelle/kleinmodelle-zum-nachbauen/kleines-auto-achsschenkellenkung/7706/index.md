---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-06T23:23:53"
picture: "kleinesautomitlenkungundfederung1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7706
- /details7926.html
imported:
- "2019"
_4images_image_id: "7706"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7706 -->
Die Teile kommen alle im hobby-1 vor. Etwas exotischere Teile sind höchstens die älteren Klemm-Winkelzahnräder, die älteren Gelenksteine (leicht durch je 2 BS 15 und einen aktuellen Gelenkstein ersetzbar) und die ft Antriebsfeder, die für die Hinterachse verwendet wurde. Die ist leicht durch ein Gummi ersetzbar.
