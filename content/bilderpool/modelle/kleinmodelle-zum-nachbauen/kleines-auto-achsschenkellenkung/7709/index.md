---
layout: "image"
title: "Die Hinterradfederung"
date: "2006-12-06T23:23:54"
picture: "kleinesautomitlenkungundfederung4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7709
- /detailsb368.html
imported:
- "2019"
_4images_image_id: "7709"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7709 -->
Die Hinterräder sind einzeln an Längslenkern aufgehängt und werden durch die ft Antriebsfeder (die hier wie ein Gummi wirkt) gefedert. Die Federung ist schön weich und die Räder federn sehr schön unabhängig von einander ein.
