---
layout: "image"
title: "Seitenansicht"
date: "2009-03-29T17:13:53"
picture: "einschienenflitzer3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23537
- /detailsaf30.html
imported:
- "2019"
_4images_image_id: "23537"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23537 -->
Der Akku ist leicht austauschbar. Der Antrieb erfolgt vorne, Fahrtrichtung ist also auf diesem Bild nach rechts.
