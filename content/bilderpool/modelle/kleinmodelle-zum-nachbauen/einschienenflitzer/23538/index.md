---
layout: "image"
title: "Von unten"
date: "2009-03-29T17:13:53"
picture: "einschienenflitzer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23538
- /details5e59.html
imported:
- "2019"
_4images_image_id: "23538"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23538 -->
Die Räder sind einzeln aufgehängt, damit auch in engen Kurven (sprich: Bogenstücken 60°) keine unnötigen Reibungsverluste durch Starrachsen entstehen. Die roten Rollen dienen der Führung um die Schienen und bewirken, dass die Räder immer hinreichend genau dem Schienenverlauf folgen.
