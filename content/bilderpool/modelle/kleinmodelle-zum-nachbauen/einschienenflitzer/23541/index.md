---
layout: "image"
title: "Antrieb (1)"
date: "2009-03-29T17:13:54"
picture: "einschienenflitzer7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23541
- /details648d.html
imported:
- "2019"
_4images_image_id: "23541"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23541 -->
Der MiniMot treibt das Differential, welches über Reibräder die Vorderräder antreibt. Sowohl das Differential als auch der Motor darüber sind drehbar aufgehängt und werden von einem Gummi (welches über die alten Reifen 45 passte) angedrückt.
