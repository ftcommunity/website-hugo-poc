---
layout: "image"
title: "Antrieb (2)"
date: "2009-03-29T17:13:54"
picture: "einschienenflitzer8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23542
- /details12fe.html
imported:
- "2019"
_4images_image_id: "23542"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23542 -->
Hier sieht man in der Mitte des Bildes, wie das Gummi am unteren Ende aufgehängt ist.
