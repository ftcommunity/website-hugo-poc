---
layout: "image"
title: "Akku abgenommen"
date: "2009-03-29T17:13:54"
picture: "einschienenflitzer5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23539
- /details0864.html
imported:
- "2019"
_4images_image_id: "23539"
_4images_cat_id: "1607"
_4images_user_id: "104"
_4images_image_date: "2009-03-29T17:13:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23539 -->
Es ist wichtig, dass die hintere Lenkaufhängung nicht mit einem BS15 mit Loch, also genau mittig, erfolgt, sondern vor den Achsen. Damit wird die Hinterachse hinterher gezogen und richtet sich sauber aus. Mit einem BS15 mit Loch auch hinten würde die Hinterachse manchmal etwas schräg stehen und damit die Räder über den Boden reiben.
