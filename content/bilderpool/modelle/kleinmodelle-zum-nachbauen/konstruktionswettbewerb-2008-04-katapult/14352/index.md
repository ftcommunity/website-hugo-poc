---
layout: "image"
title: "Trebuchet 3"
date: "2008-04-21T23:33:43"
picture: "sm_iso.jpg"
weight: "33"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["catapult", "trebuchet"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/14352
- /details4c56.html
imported:
- "2019"
_4images_image_id: "14352"
_4images_cat_id: "1327"
_4images_user_id: "585"
_4images_image_date: "2008-04-21T23:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14352 -->
This is my late entry for the catapult contest. 

google translation: Dies ist meine Nachmeldung für die Katapult-Wettbewerb.