---
layout: "image"
title: "Details Fischertechnik Katapult"
date: "2008-04-30T21:15:35"
picture: "Poederoyen-koninginnedag_019.jpg"
weight: "47"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/14429
- /details388c.html
imported:
- "2019"
_4images_image_id: "14429"
_4images_cat_id: "1327"
_4images_user_id: "22"
_4images_image_date: "2008-04-30T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14429 -->
Details Fischertechnik Katapult
