---
layout: "image"
title: "Mirose Vorschlag B (1)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14322
- /details091a.html
imported:
- "2019"
_4images_image_id: "14322"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14322 -->
Miroses Beschreibung zu diesem Vorschlag:

Wirkungsweise:
Wenn der Baustein 30 zwischen die Traktorreifen gelangt, wird er beschleunigt und weggeschleudert.
Wurfweite eines Bausteins 30 bei 9 V: etwa 200 cm. (ca. 82 Bauteile)
