---
layout: "image"
title: "Mirose Vorschlag A (3)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14318
- /detailsbbc3.html
imported:
- "2019"
_4images_image_id: "14318"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14318 -->
