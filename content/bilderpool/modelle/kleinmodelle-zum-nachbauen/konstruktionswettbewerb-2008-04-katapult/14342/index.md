---
layout: "image"
title: "Speedy68 (7)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14342
- /details141e.html
imported:
- "2019"
_4images_image_id: "14342"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14342 -->
