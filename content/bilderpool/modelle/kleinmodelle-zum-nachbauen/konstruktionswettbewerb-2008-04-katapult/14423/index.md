---
layout: "image"
title: "Fischertechnik Pneumatik Katapult"
date: "2008-04-30T14:55:56"
picture: "FT-Launching_Catapult-nov-2007_006.jpg"
weight: "41"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/14423
- /details946b-2.html
imported:
- "2019"
_4images_image_id: "14423"
_4images_cat_id: "1327"
_4images_user_id: "22"
_4images_image_date: "2008-04-30T14:55:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14423 -->
Fischertechnik Pneumatik Katapult
