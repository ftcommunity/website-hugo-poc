---
layout: "image"
title: "Mirose Vorschlag A (4)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14319
- /details3207-2.html
imported:
- "2019"
_4images_image_id: "14319"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14319 -->
