---
layout: "image"
title: "Speedy68 (5)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14340
- /detailse478.html
imported:
- "2019"
_4images_image_id: "14340"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14340 -->
