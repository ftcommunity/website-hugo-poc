---
layout: "overview"
title: "Konstruktionswettbewerb 2008-04: Katapult"
date: 2020-02-22T08:32:55+01:00
legacy_id:
- /php/categories/1327
- /categories68e1.html
- /categories56f6.html
- /categoriesa356.html
- /categories40cf.html
- /categories17b0.html
- /categoriesd044.html
- /categories4950.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1327 --> 
Die Aufgabenstellung war folgende:



1. Es soll ein funktionstüchtiges Katapult gebaut werden, das einen Baustein 30 (z. B. die Art. Nr. 31003) mindestens einen Meter weit schleudert.



2. Mitmachen darf jeder, der Fischertechnikteile zur Verfügung hat. 



3. Es darf aus maximal 250 original Fischertechnikteile bestehen (Motoren, Pneumatik, ... sind erlaubt). Ausnahme: Gummis sind nicht erlaubt. 



4. Modellgröße: Es soll kompakt sein und möglichst auf z. B. eine Bauplatte 259 * 187 (32985) passen (Geringfügige Abweichungen sind kein K.O.-Kriterium, da das nicht gewinnentscheidend ist).



5. Bewertungskriterien sind die raffinierteste, eleganteste, pfiffigste Mechanik - einfach das genialste Modell.