---
layout: "image"
title: "Lagerung des Schwenkhebels"
date: "2015-10-25T17:52:24"
picture: "schwenkventilatorauserjahreteilen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42139
- /details2f77.html
imported:
- "2019"
_4images_image_id: "42139"
_4images_cat_id: "3139"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T17:52:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42139 -->
Wie man rechts im Bild sieht, stehen da einfach zwei Grundbausteine aufeinander, durch die eine einzelne Achse geht. Eine alternative Konstruktion könnte nur mit Achsen, Achskupplungen und den Winkelachsen (die gab es im Ur-ft-400) ausgeführt werden.
