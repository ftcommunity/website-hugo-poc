---
layout: "image"
title: "Unterboden"
date: "2011-11-05T17:50:29"
picture: "minimalistischesservofahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33418
- /detailsf634.html
imported:
- "2019"
_4images_image_id: "33418"
_4images_cat_id: "2475"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33418 -->
Alles ist auf minimalen Teileaufwand ausgelegt, weil ich ja einfach nur mal schnell den Servo antesten wollte.
