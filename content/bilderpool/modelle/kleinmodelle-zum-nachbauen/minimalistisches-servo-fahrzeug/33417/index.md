---
layout: "image"
title: "Seitenansicht"
date: "2011-11-05T17:50:29"
picture: "minimalistischesservofahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33417
- /details1ffd.html
imported:
- "2019"
_4images_image_id: "33417"
_4images_cat_id: "2475"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33417 -->
Gut zu sehen, dass die komplette Vorderachse incl. Lenk-Servo federt. Das ist unelegant, benötigt aber wenige Teile. Der Heckantrieb wird von zwei normalen ft-Federn gefedert. Die kardanische Aufhängung am vorderen Ende des S-Motors erlaubt das unabhängige Einfedern beider Hinterräder. Die Aufhängung wurde aus zwei Stück der älteren Fassung des Gelenksteins sowie dem Kardanwürfel aus einem ganz alten ft-Kardangelenk gebaut (das gab's auf der ftc schon mal wo).
