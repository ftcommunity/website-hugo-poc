---
layout: "image"
title: "FTMann - am kanu"
date: "2017-07-30T15:45:14"
picture: "ze-kanu.jpg"
weight: "58"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- /php/details/46103
- /details74a4.html
imported:
- "2019"
_4images_image_id: "46103"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-07-30T15:45:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46103 -->
