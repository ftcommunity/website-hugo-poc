---
layout: "image"
title: "Walze mit Kotflügeln"
date: "2006-11-26T23:08:57"
picture: "Walze09b.jpg"
weight: "9"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7627
- /detailse3ba.html
imported:
- "2019"
_4images_image_id: "7627"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7627 -->
Habe der Walze jetzt noch ein Paar Kotflügel verpaßt.