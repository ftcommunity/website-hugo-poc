---
layout: "image"
title: "Ansicht von unten"
date: "2006-11-26T12:47:56"
picture: "Walze04b.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7619
- /detailsa443.html
imported:
- "2019"
_4images_image_id: "7619"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7619 -->
