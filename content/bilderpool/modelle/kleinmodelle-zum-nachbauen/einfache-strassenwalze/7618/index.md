---
layout: "image"
title: "Heckansicht"
date: "2006-11-26T12:47:56"
picture: "Walze03b.jpg"
weight: "3"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/7618
- /detailsf81c.html
imported:
- "2019"
_4images_image_id: "7618"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7618 -->
