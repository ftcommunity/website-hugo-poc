---
layout: "image"
title: "Flügelschlagmechanismus"
date: "2011-06-26T19:48:17"
picture: "komoedienvogel3.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30974
- /details3106.html
imported:
- "2019"
_4images_image_id: "30974"
_4images_cat_id: "2314"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30974 -->
Hier ist der Flügelschlagmechanismus gut erkennbar: eine 45er Strebe, die an einer Stange am Zahnrad 30 hebt und senkt den Flügel (am Flügel ist die Strebe an einem Gelenkstein befestigt).