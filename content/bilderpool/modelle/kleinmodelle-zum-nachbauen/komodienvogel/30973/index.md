---
layout: "image"
title: "Gesamt (2)"
date: "2011-06-26T19:48:17"
picture: "komoedienvogel2.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30973
- /detailsf4bb.html
imported:
- "2019"
_4images_image_id: "30973"
_4images_cat_id: "2314"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30973 -->
von hinten mit gehobenen Flügeln