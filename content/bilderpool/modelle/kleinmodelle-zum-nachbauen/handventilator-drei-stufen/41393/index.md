---
layout: "image"
title: "Seitenansicht"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41393
- /detailsb822.html
imported:
- "2019"
_4images_image_id: "41393"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41393 -->
Drei Minitaster sind mit winzigem Abstand untereinander befestigt. Sie sitzen auf drei BS7,5, die am Batteriegehäuse befestigt und mit einem Verbinder 30 auf der (im Bild) linken Seite gesichert sind.
