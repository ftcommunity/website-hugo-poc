---
layout: "overview"
title: "Handventilator mit drei Stufen"
date: 2020-02-22T08:33:21+01:00
legacy_id:
- /php/categories/3095
- /categoriese6f6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3095 --> 
Dieser Ventilator benötigt neben dem 3 Minitaster, 2 Lampen, aber keine Elektronik und bietet drei Kühlstufen.