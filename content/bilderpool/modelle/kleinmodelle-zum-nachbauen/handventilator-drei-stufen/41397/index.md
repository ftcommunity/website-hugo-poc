---
layout: "image"
title: "Schaltbild"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41397
- /detailsb5ff.html
imported:
- "2019"
_4images_image_id: "41397"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41397 -->
Die Taster werden durch unterschiedlich tiefes Eindrücken des obersten nacheinander in der Reihenfolge von oben nach unten geschaltet. Die Lämpchen im Schaltbild dienen als einzelne Vorwiderstände, die durch den zweiten Taster ggf. parallel geschaltet und somit in ihrem Widerstand halbiert werden. Schließt der dritte Taster, liegt die volle Akkuspannung unmittelbar am Motor an. Die endlich zugeführte frische Luft bietet Gelegenheit, Unsinn zu korrigieren, der, ihrer nicht mehr verlustig, als solcher offenbar wird.

Ist natürlich nur ein Modellvorschlag.
