---
layout: "image"
title: "Star Wars is back: Death Star meets his Waterloo. Dem Todesstern geht es an den Kragen."
date: "2017-12-10T19:28:36"
picture: "Luke_Skywalker_im_X-Wing_mit_R2-D2_800x400px.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/46965
- /details17cd-2.html
imported:
- "2019"
_4images_image_id: "46965"
_4images_cat_id: "3474"
_4images_user_id: "2635"
_4images_image_date: "2017-12-10T19:28:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46965 -->
Luke Skywalker im X-Wing, R2-D2 ist natürlich dabei!