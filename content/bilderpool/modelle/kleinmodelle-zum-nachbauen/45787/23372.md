---
layout: "comment"
hidden: true
title: "23372"
date: "2017-04-23T11:06:40"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tolle Idee, und schick gemacht!

Wenn diese Flügel nur eine richtige Bohrung da hätten, wo sie beim BS7,5 auch sind (anstelle dieses irgendwie geformte Loches), und dann noch eine Nut am äußeren Ende, dann wären die Teile auch jenseits von Deko zu etwas nütze.

Was mich noch wundert: was ist das schwarze Teil, das da den Handgriff ergibt?

Gruß,
Harald