---
layout: "image"
title: "Bokalift 2"
date: 2023-04-18T18:17:55+02:00
picture: "bokalift_21.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
uploadBy: "Website-Team"
license: "unknown"
---

details from Schiff: https://boskalis.com/media/cafdcmsf/bokalift-2.pdf