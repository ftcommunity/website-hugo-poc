---
layout: "image"
title: "Ähnlicher kleiner Kran"
date: "2015-12-28T19:08:43"
picture: "kleinerkran2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/42620
- /details7ee5.html
imported:
- "2019"
_4images_image_id: "42620"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42620 -->
Dieser Kran ist mit aktuellen Teilen hergestellt und kaum größer. Er ist ebenfalls manuell drehbar, kann das Seil aufwickeln und den Kranarm neigen.
