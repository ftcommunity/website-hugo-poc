---
layout: "image"
title: "Helicopter"
date: "2009-01-14T16:41:25"
picture: "sm_ft-heliocopter.jpg"
weight: "19"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["helicopter"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16989
- /details70b4.html
imported:
- "2019"
_4images_image_id: "16989"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-01-14T16:41:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16989 -->
A small model constructed out of the  	   	Basic Cranes (96778) kit.