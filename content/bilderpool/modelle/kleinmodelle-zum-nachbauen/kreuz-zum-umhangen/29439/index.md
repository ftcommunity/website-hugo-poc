---
layout: "image"
title: "Kreuz zum Umhängen"
date: "2010-12-08T17:15:11"
picture: "kreuz2.jpg"
weight: "2"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29439
- /details8a86.html
imported:
- "2019"
_4images_image_id: "29439"
_4images_cat_id: "2140"
_4images_user_id: "1162"
_4images_image_date: "2010-12-08T17:15:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29439 -->
