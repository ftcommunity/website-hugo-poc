---
layout: "image"
title: "Hebel"
date: "2008-06-23T10:56:26"
picture: "draisine06.jpg"
weight: "10"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14765
- /details029e-2.html
imported:
- "2019"
_4images_image_id: "14765"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14765 -->
