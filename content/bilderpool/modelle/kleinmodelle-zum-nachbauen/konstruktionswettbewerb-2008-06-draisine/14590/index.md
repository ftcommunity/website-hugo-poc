---
layout: "image"
title: "Draisine vorne"
date: "2008-05-30T07:03:25"
picture: "Draisine_vorne.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14590
- /detailsf53f.html
imported:
- "2019"
_4images_image_id: "14590"
_4images_cat_id: "1351"
_4images_user_id: "724"
_4images_image_date: "2008-05-30T07:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14590 -->
