---
layout: "image"
title: "Draisine Mirose A8"
date: "2008-06-23T10:56:27"
picture: "draisine19.jpg"
weight: "23"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14778
- /details92b5.html
imported:
- "2019"
_4images_image_id: "14778"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:27"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14778 -->
