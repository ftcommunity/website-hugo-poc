---
layout: "image"
title: "Gleise"
date: "2008-06-23T10:56:26"
picture: "draisine07.jpg"
weight: "11"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14766
- /details6ac3.html
imported:
- "2019"
_4images_image_id: "14766"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14766 -->
Auf diesem Bild sieht man die Gleise. Die Spurweite beträgt 11,3 cm.