---
layout: "image"
title: "Masked-1"
date: "2008-06-23T10:56:26"
picture: "draisine09.jpg"
weight: "13"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14768
- /details6963.html
imported:
- "2019"
_4images_image_id: "14768"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14768 -->
Hier noch einige Daten:
Spurweite 82,5 mm
Länge 200 mm
Breite 110 mm
Teile: nicht gezählt, aber geschätzt um die 200-250
