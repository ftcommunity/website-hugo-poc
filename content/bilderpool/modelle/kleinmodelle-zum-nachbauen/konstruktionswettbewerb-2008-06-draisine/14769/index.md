---
layout: "image"
title: "Masked-2"
date: "2008-06-23T10:56:26"
picture: "draisine10.jpg"
weight: "14"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14769
- /detailse9ff.html
imported:
- "2019"
_4images_image_id: "14769"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14769 -->
