---
layout: "image"
title: "Draisine Mirose A7"
date: "2008-06-23T10:56:26"
picture: "draisine18.jpg"
weight: "22"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14777
- /details7cd9-2.html
imported:
- "2019"
_4images_image_id: "14777"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14777 -->
