---
layout: "image"
title: "Draisine Mirose B4"
date: "2008-06-23T10:56:27"
picture: "draisine23.jpg"
weight: "27"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14782
- /detailsaf96.html
imported:
- "2019"
_4images_image_id: "14782"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:27"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14782 -->
