---
layout: "image"
title: "Draisine Mirose B2"
date: "2008-06-23T10:56:27"
picture: "draisine21.jpg"
weight: "25"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/14780
- /detailsf8cc.html
imported:
- "2019"
_4images_image_id: "14780"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:27"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14780 -->
