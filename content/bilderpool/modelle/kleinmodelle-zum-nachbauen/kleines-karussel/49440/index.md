---
layout: "image"
title: "Kleines Karussel"
date: 2022-01-17T10:41:24+01:00
picture: "2022-01-09_Kleines_Karussel1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Rahmen hat vier BS15 mit Bohrung, durch die die Achsen gehen. Die Kette sitzt auf Z20 auf Flachnaben, die die Nabenmutter nach oben tragen und so angebracht sind, dass sie oben anstoßen. Dadurch ist unten Platz genug, dass die Kette nicht schleift.