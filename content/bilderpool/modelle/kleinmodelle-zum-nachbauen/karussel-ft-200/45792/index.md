---
layout: "image"
title: "Kleines Karussell aus ft 200 + mot. 1 (2)"
date: 2023-01-20T09:37:07+01:00
picture: "Karussel_aus_ft_200_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Erst wurde gekurbelt, aber dann wurde die Idee, das mit einem Motor zu ergänzen, begeistert aufgenommen.