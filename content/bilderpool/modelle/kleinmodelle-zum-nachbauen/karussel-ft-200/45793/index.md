---
layout: "image"
title: "Kleines Karussell aus ft 200 + mot. 1 (1)"
date: 2023-01-20T09:37:08+01:00
picture: "Karussel_aus_ft_200_1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein kleines Besuchskind wollte beschäftigt sein. Da kam der gute alte fischertechnik 200 + Ur-Motor aus diesem Kasten https://ftcommunity.de/bilderpool/sonstiges/eure-fischertechnikkasten/fischertechnik-200-mot-1-einer-box-500/gallery-index/ gerade recht.