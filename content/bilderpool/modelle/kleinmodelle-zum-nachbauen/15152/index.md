---
layout: "image"
title: "Wally the Robot"
date: "2008-08-31T08:58:52"
picture: "ft_wally_a.jpg"
weight: "9"
konstrukteure: 
- "Amelia Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Wally", "the", "Robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15152
- /detailscdce.html
imported:
- "2019"
_4images_image_id: "15152"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-08-31T08:58:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15152 -->
This was a robot built by my daughter out of elements found in the "Basic Cranes" kit.