---
layout: "image"
title: "Rakete"
date: 2022-01-05T15:53:01+01:00
picture: "2021-12-31_Rakete2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da die Rakete unsanft landen wird, muss sie aus wenigen, leichtgewichtigen Teilen bestehen, die einen Aufprall auf dem Boden problemlos überstehen.