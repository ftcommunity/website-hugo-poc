---
layout: "image"
title: "Rakete in entspanntem Zustand"
date: 2022-01-05T15:52:58+01:00
picture: "2021-12-31_Rakete5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Rakete sitzt hier locker auf der Antriebsfeder.