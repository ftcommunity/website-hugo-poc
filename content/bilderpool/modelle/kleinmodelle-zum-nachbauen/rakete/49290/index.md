---
layout: "image"
title: "Gesamtansicht"
date: 2022-01-05T15:53:02+01:00
picture: "2021-12-31_Rakete1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Rakete sollte ja tatsächlich fliegen können. Also wird sie mit Federkraft eingespannt und dann abgefeuert.