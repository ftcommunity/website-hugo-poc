---
layout: "image"
title: "Rakete in gespanntem Zustand"
date: 2022-01-05T15:52:56+01:00
picture: "2021-12-31_Rakete6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Rakete ist bereit zum Abschuss.

Die Austrittsgeschwindigkeit konnte ich bei ca. 2 m Flughöhe zu v = sqrt(2*g*h) auf ca. 6 m/s bestimmen. Hält man die Abschussrampe 45° geneigt, genügt das für 3 - 4 m Flugstrecke über Grund.