---
layout: "image"
title: "Aufbau der Abschussrampe"
date: 2022-01-05T15:53:00+01:00
picture: "2021-12-31_Rakete3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Eine fischertechnik-Antriebsfeder aus den 1960er/1970er Jahren ist durch die Löcher zweier gegenüberstehender Winkelträger 120 gesteckt. Die Feder wird dann geschlossen (diese Federn kann man an einer Stelle auftrennen und schließen - genial).

Die Rakete wird dann von oben hineingedrückt und durch den oberen Teil der Feder beim Loslassen hochgeschleudert.