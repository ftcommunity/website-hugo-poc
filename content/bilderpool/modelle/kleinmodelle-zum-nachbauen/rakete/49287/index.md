---
layout: "image"
title: "Abschussmechanik"
date: 2022-01-05T15:52:59+01:00
picture: "2021-12-31_Rakete4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese Stange kann entweder eingeschoben sein - bis in den freien Schlitz des Flachträgers der Rakete. Die Anleitung ist also:

1. Stange herausziehen.
2. Rakete einschieben (mit dem Schlitz in Richtung Stange).
3. Abschussrampe mit der einen Hand an den Grundbausteinen festhalten.
4. Stange mit der anderen Hand herausziehen

NICHT DAS AUGE ÜBER DIE RAKETE HALTEN - VERLETZUNGSGEFAHR!

5. Dadurch wird die Rakete freigegeben und per Federkraft bis zur Decke geschleudert.