---
layout: "image"
title: "Minilaster"
date: "2014-10-12T16:54:26"
picture: "MiniLaster.jpg"
weight: "33"
konstrukteure: 
- "Mein Sohn (6 Jahre)"
fotografen:
- "rito"
schlagworte: ["mini", "Lastwagen", "klein"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- /php/details/39709
- /details8735.html
imported:
- "2019"
_4images_image_id: "39709"
_4images_cat_id: "335"
_4images_user_id: "2179"
_4images_image_date: "2014-10-12T16:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39709 -->
Mein Sohn (6) liebt es gerade aus ganz wenig Teilen die abenteuerlichsten Konstruktionen zu erstellen. Hier ein schönes Beispiel!