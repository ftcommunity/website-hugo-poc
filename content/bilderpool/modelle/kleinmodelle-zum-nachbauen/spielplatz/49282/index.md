---
layout: "image"
title: "Bank"
date: 2022-01-03T13:18:52+01:00
picture: "2021-12-31_Spielplatz3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bank für die offensichtlich in guter Laune befindlichen Eltern.