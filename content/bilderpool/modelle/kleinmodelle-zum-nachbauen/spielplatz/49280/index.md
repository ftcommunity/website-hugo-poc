---
layout: "image"
title: "Rutsche"
date: 2022-01-03T13:18:50+01:00
picture: "2021-12-31_Spielplatz5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Rutsche wurde zuerst gebaut, und zwar bewusst nur aus wenig verschiedenen Teilen (sprich: lauter Grundbausteinen), damit das Kind möglichst viel selber bauen konnte. Erst später wurde sie auf der Bauplatte befestigt.