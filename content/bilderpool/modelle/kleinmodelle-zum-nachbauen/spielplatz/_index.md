---
layout: "overview"
title: "Spielplatz"
date: 2022-01-03T13:18:48+01:00
---

Für ein Besuchskind gebaut: Schaukel, Rutsche, Sandkasten und Bank für einen Spielplatz.