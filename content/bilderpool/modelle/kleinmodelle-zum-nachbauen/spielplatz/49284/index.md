---
layout: "image"
title: "Gesamtansicht 1"
date: 2022-01-03T13:18:55+01:00
picture: "2021-12-31_Spielplatz1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Modelle waren zunächst einzeln gebaut, dann aber zum besseren Transport alle auf eine Bauplatte 500 gesetzt.