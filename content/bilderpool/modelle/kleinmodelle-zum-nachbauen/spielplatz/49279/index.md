---
layout: "image"
title: "Schaukel (1)"
date: 2022-01-03T13:18:49+01:00
picture: "2021-12-31_Spielplatz6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch die Schaukel ist schnell aufgebaut. Der Sitz des Männchens ist auf drei waagerecht liegenden und zwischen zwei senkrecht stehenden BS7,5 befestigt, die alle mit Verbindern 15 zusammenhalten.