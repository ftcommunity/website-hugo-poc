---
layout: "image"
title: "Sandkasten"
date: 2022-01-03T13:18:51+01:00
picture: "2021-12-31_Spielplatz4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vier Eck-Steine, vier Platten 60x30, Schüttgut rein, fertig.