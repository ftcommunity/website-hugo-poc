---
layout: "image"
title: "Schaukel (2)"
date: 2022-01-03T13:18:48+01:00
picture: "2021-12-31_Spielplatz7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Durch die leichtgängige Aufhängung schwingt die Schaukel wunderbar. Es ist sogar Platz genug für Überschläge.