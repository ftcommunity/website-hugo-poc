---
layout: "comment"
hidden: true
title: "721"
date: "2005-10-11T10:20:15"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
StahlfedernDanke für das Kompliment! Die Stahlfedern sind Original-Teile, die ich neu über Knobloch bezogen habe. Hatte in den 80ern davon welche in einem Motor-Kasten, diese sind aber inzwischen nicht mehr vorhanden.

Gruß, Thomas