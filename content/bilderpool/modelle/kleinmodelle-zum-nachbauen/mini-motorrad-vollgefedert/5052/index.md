---
layout: "image"
title: "Mini-Motorrad vollgefedert"
date: "2005-10-01T16:14:47"
picture: "Mini-Motorrad_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5052
- /details4977.html
imported:
- "2019"
_4images_image_id: "5052"
_4images_cat_id: "715"
_4images_user_id: "328"
_4images_image_date: "2005-10-01T16:14:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5052 -->
Ziel war das Erstellen eines möglichst kleinen Motorrads, das allerdings vollständig gefedert sein soll. Ich denke, das ist gut gelungen, und das Kleine sieht echt hübsch aus... ;o)