---
layout: "image"
title: "Werkzeuge"
date: "2007-03-01T16:56:01"
picture: "werkzeuge1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9198
- /details7472-2.html
imported:
- "2019"
_4images_image_id: "9198"
_4images_cat_id: "851"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9198 -->
Von links oben nach rechts unten: Schlüssel für verschiedene sachen, sSchlüssel für Muttern, Kreutzschraubendreher, normaler Schraubendreher, Bohrer.