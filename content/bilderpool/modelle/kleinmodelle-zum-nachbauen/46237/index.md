---
layout: "image"
title: "FTMann - mit Rikscha"
date: "2017-09-05T14:56:08"
picture: "ze-riksa.jpg"
weight: "60"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- /php/details/46237
- /details0fa2.html
imported:
- "2019"
_4images_image_id: "46237"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-09-05T14:56:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46237 -->
