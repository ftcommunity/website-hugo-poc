---
layout: "image"
title: "Rubber Band Powered Vehicle"
date: "2009-04-14T18:57:54"
picture: "sm_ft-rubber_band_2.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Rubber", "Band", "Propeller", "Vehicle"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23717
- /details073d-2.html
imported:
- "2019"
_4images_image_id: "23717"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-14T18:57:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23717 -->
This is my entry into the instructables rubber band powered contest. (See http://www.instructables.com/contest/rubberband/ ) Thought to share.