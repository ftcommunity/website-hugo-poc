---
layout: "image"
title: "Ventilator 1"
date: "2017-06-25T17:04:57"
picture: "zweiwegventilator1.jpg"
weight: "1"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45995
- /detailscce9.html
imported:
- "2019"
_4images_image_id: "45995"
_4images_cat_id: "3419"
_4images_user_id: "1355"
_4images_image_date: "2017-06-25T17:04:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45995 -->
von vorne.
Mittels Power-Controller sind die Motoren stufenlos Steuerbar