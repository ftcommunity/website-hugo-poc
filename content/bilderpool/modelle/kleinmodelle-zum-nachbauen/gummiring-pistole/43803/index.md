---
layout: "image"
title: "Gummi-Ring-Pistole"
date: "2016-06-27T13:52:54"
picture: "gummiringpistole10.jpg"
weight: "10"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43803
- /detailsba0d.html
imported:
- "2019"
_4images_image_id: "43803"
_4images_cat_id: "3244"
_4images_user_id: "1359"
_4images_image_date: "2016-06-27T13:52:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43803 -->
alternative Auslöser-Teile
