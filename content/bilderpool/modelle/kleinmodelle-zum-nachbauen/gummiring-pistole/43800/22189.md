---
layout: "comment"
hidden: true
title: "22189"
date: "2016-06-28T11:27:54"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Die Kritik - die ich auch als Rückmeldung sehr schätze - war durchaus global gemünzt.
Sven schätze ich sehr und ich respektiere ihn und Seine Meinung, wie die aller anderen auch. Mich hat ein durchaus respektlos formulierter Folgebeitrag sehr geärgert und dabei bleibt es.

Mein Kommentar zur Fotostrecke mußte sein.

Und mit "Newcomern" meine ich tatsächlich die ft-Novizen und ft-Eleven, die, wie ich damals als Anfänger übrigens auch, die Winkelsteine solange rumdrehen bis sie die Aufschrift lesen können um herauszufinden was das für einer ist. Anhand des Fotos ist das schon etwas schwierig, wenn man den Trick nicht kennt: WS15° hat eine runde Nut, WS7,5° hat eine flache Nut.

Laß uns das hier nicht weiter vertiefen - dazu ist der Thread da - sondern einfach weiter Spaß haben.

Grüße
H.A.R.R.Y.