---
layout: "image"
title: "Munition für Gummi-Ring-Pistole"
date: "2016-06-27T13:52:54"
picture: "gummiringpistole02.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43795
- /detailsb23b.html
imported:
- "2019"
_4images_image_id: "43795"
_4images_cat_id: "3244"
_4images_user_id: "1359"
_4images_image_date: "2016-06-27T13:52:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43795 -->
