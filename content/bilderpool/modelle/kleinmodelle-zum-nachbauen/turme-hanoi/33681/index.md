---
layout: "image"
title: "Hanoi 06"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi06.jpg"
weight: "6"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33681
- /details6a0f.html
imported:
- "2019"
_4images_image_id: "33681"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33681 -->
In dieser Baustufe wird der Drehkranz mit seinem Antrieb, einer Rastschnecke, und dem Impulsrad zur Betätigung des Tasters I1 montiert. Aus der Sicht des Betrachters wird hinter dem Drehkranz ein Baustein 30 mit Verriegelung zur Bildung eines Kabelträgers des Kabelbaumes für den Hubtisch errichtet.
