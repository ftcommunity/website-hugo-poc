---
layout: "image"
title: "Hanoi 14"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi14.jpg"
weight: "14"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33689
- /details6bdf.html
imported:
- "2019"
_4images_image_id: "33689"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33689 -->
In der Baustufe 13 werden alle Teilkomponenten miteinander verbunden und die Verbindungen werden komplett eingedrückt bzw. aufgeschoben (siehe Hinweise im Bild). 

Das Hubtisch-"Gegenlager" mit den "Gleitsteinen" wird am noch offenen Ende vom Hubtisch-Träger aufgebaut und zunächst dort belassen. Nach dem Einfädeln des Hubtisches wird das Gegenlager an der mittelhohen Stützsäule des Drehkranzes ausgerichtet.
