---
layout: "image"
title: "Hanoi 18"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi18.jpg"
weight: "18"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33693
- /details1173.html
imported:
- "2019"
_4images_image_id: "33693"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33693 -->
Die Baustufe ist unverändert die Baustufe 16. 

Alle für die kontrastreiche Foto-Darstellung verwendeten grauen Bausteine wurden durch schwarze Bausteine ersetzt.
