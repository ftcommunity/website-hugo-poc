---
layout: "image"
title: "Hanoi 17"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi17.jpg"
weight: "17"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33692
- /details285a.html
imported:
- "2019"
_4images_image_id: "33692"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33692 -->
"Die Türme von Hanoi" sind nun fertig aufgebaut. Das aufgenommene Bild entspricht dem Bild aus der Bauanleitung von 1984 auf Seite 1 dieser Bauanleitung, zumindest, was den Blickwinkel und die Perspektive angeht.

"Die Türme von Hanoi" sind einsatzbereit.
