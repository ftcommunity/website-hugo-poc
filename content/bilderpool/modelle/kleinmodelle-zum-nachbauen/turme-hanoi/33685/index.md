---
layout: "image"
title: "Hanoi 10"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi10.jpg"
weight: "10"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33685
- /detailsc322.html
imported:
- "2019"
_4images_image_id: "33685"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33685 -->
Nach dem Entfernen des Zeigers, mit dessen Hilfe die Mittelpositionen der Ablagefelder für die Scheiben ermittelt wurden, wird die kleine Säule neben dem Taster I4 und die hohe Säule mit den Zahnstangen mit "Bauplatten zum Clipsen" abgedeckt.

Die mittelhohe Stützsäule wird mit einer "Bauplatte mit 2 Zapfen" abgedeckt und mit dem Stützpfeiler stabil verbunden.

Die Grundplatte mit dem Drehkranz ist jetzt fertig montiert und verkabelt.
