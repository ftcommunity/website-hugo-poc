---
layout: "image"
title: "Hanoi 13"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi13.jpg"
weight: "13"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33688
- /details447c.html
imported:
- "2019"
_4images_image_id: "33688"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33688 -->
In der Baustufe 12 werden die Teilkomponenten der Baustufen 10c, 11a und 11c zusammengefügt.
