---
layout: "image"
title: "Hanoi 08"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi08.jpg"
weight: "8"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33683
- /detailsa40e.html
imported:
- "2019"
_4images_image_id: "33683"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33683 -->
Nach der Baustufe 6 können schon die Zählimpulse des Drehkranzes für die Ablagefelder der Scheiben ermittelt werden. Dazu wird ein "Zeigestab" gebaut, der den Mittelpunkt des späteren Elektromagneten nachbildet. Der Drehkranz wird soweit gedreht, bis sich die Mittellinie des Zeigers (ft# 38414) über dem Mittelpunkt des Feldes befindet. Die gezählten Impulse von der "Nullstellung" (ausgelöst durch den Taster I2 im Uhrzeigersinn ganz links) bis zur Feldmitte des angesteuerten Feldes werden in das RPP-Programm "Die Türme von Hanoi" übernommen. Der "Vollausschlag" (ausgelöst durch den Taster I3 im Uhrzeigersinn ganz rechts) wird bei diesen Versuchen ebenfalls ermittelt.

Bei dem Zeigestab ist wichtig, daß der Mittelpunkt des Zeigers (ft# 38414) genau 90 mm von der Hinterkante des Zeigestabes entfernt ist (siehe Skizzierung in Baustufe 7a).

Nach den Versuchen wird der Zeigestab wieder entfernt.
