---
layout: "image"
title: "Hanoi 16"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi16.jpg"
weight: "16"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33691
- /details53e0-2.html
imported:
- "2019"
_4images_image_id: "33691"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33691 -->
Der Hubtisch ist fertig montiert, verkabelt (der schmale rote Stecker kommt in die mittlere Buchse des Tasters I6) und elektrisch mit der Verteilerplatte und der 28-poligen Buchsenplatte verbunden und kann mit Hilfe des Hilfs-Programmes "Hubtisch einfädeln.rpp" auf die Stützsäulen des Drehkanzes "eingefädelt" werden. Nach dem "Einfädel-Vorgang" werden die "Gleitsteine" am Ende des Hubtisches an die mittelhohe Stützsäule angepasst, so daß diese Stützsäule leichtgängig umschlossen wird und der Hubtisch sich frei auf und ab bewegen kann.
