---
layout: "image"
title: "Hanoi 02"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi02.jpg"
weight: "2"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33677
- /details5600.html
imported:
- "2019"
_4images_image_id: "33677"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33677 -->
Unter die Grundplatte, ft# 32985, wurde eine Sperrholzplatte 18 x 25 cm aus Pappelholz (18 mm dick) mittig geschraubt und unter dieser 5 Filzgleiter zur Möbelschonung befestigt. Durch das Maß 18 x 25 cm für die Sperrholzplatte passt die gesamte Einheit noch als "Deckel" in eine 500er Sammelbox. 

Die Federnocken, ft# 31982, und das Verbindungsstück, ft# 31060, wurden zur Verdeutlichung nur in die Nuten der Bausteine "eingesetzt". Die Federnocken und das Verbindungsstück werden dann jeweils bis zum Anschlag in die Nuten eingeschoben. Die Federnocken, die zur Arretierung der Bausteine vorgesehen sind, lassen sich tiefer eindrücken und werden bis in die Grundplatte hinein eingedrückt und zwar so tief bis der Nocken auf der Grundplatte aufzuliegen kommt. Dadurch wird der Baustein in seiner Position auf der Grundplatte verriegelt.

Schwarze Bausteine auf schwarzer Grundplatte sind die "Wunschkombination" eines jeden Fotografen. Für die Fotoaufnahmen wurden, wenn aus Gründen des Kontrastes notwendig, die für den Aufbau vorgesehenen schwarzen Bausteine durch graue Bausteine ersetzt.
