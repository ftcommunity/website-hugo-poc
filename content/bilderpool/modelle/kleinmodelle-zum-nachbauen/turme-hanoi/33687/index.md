---
layout: "image"
title: "Hanoi 12"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi12.jpg"
weight: "12"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33687
- /detailsb307.html
imported:
- "2019"
_4images_image_id: "33687"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33687 -->
Baustufe 11a zeigt den Zusammenbau des Elektromagneten mit seiner Trägereinheit. Deutlich zu sehen sind die beiden Klebepunkte an den Polen des Elektromagneten, die verhindern sollen, daß die Scheiben nach Abschalten des Haltestromes wegen einer Aufmagnetisierung am Elektromagneten "kleben" bleiben.

Die Baustufen 11b und 11c verdeutlichen den Aufbau des Antriebes für Hubtisch und Taster I6 der Impulszählung. Hier kommen auch den beiden Teile der abgesägten Bauplatten (Abdeckplatten) ft# 32330 zum Einsatz. Die durchgehende Nut ist erforderlich, damit der Verbinder 45 (im Bild leicht überstehend) die beiden Teile mit Hilfe der Nachbarbausteine tragen kann.

Zwei Antriebsmotore und zwei Hubgetriebe tragen das Gewicht des Hubtisches besser als die "einfache" Ausführung beim Original von 1984. Bei dem damaligen Modell rutschte der Hubtisch wegen seines Gewichtes gern durch. Die Zahnwalzen der heutigen "schwarzen" Hubgetriebe scheinen auch etwas breiter (und damit belastbarer) zu sein als die der damaligen "grauen" Hubgetriebe.
