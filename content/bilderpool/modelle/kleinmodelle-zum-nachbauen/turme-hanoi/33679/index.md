---
layout: "image"
title: "Hanoi 04"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi04.jpg"
weight: "4"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/33679
- /details1a13.html
imported:
- "2019"
_4images_image_id: "33679"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33679 -->
Die gesamte Baustufe 3 zeigt die Montage der "elektrischen" Komponenten auf der Grundplatte.

Die beiden Stecker (rot und grün) am S-Motor ermöglichen die Bildung einer "Eck-Steck-Verbindung" (aus Platzgründen) an dieser Stelle am S-Motor.

Das Teil ft# 75140 ist ein 26-poliger Pfostenstecker mit 26-poligem, farbcodiertem  Flachbandkabel und einer 28-poligen Buchsenplatte durch die "Die Türme von Hanoi" an das Robo-Interface angeschlossen werden.
