---
layout: "image"
title: "Torkler, Ansichten und zerlegt"
date: "2017-03-03T21:38:24"
picture: "Torkler_Ansichten_und_zerlegt.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45448
- /detailse0b3.html
imported:
- "2019"
_4images_image_id: "45448"
_4images_cat_id: "3379"
_4images_user_id: "2635"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45448 -->
Oben zwei weitere Ansichten des Torklers.
Unten der Aufbau aus 2 Halbringen von je 12 Winkelsteinen 15°, vier Bauplatten 3,75 (32330) und 3 Federnocken.