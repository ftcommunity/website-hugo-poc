---
layout: "image"
title: "Der Torkler oder Wobbler"
date: "2017-03-03T21:38:24"
picture: "Torkler_1_800px.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45449
- /detailsfd2f.html
imported:
- "2019"
_4images_image_id: "45449"
_4images_cat_id: "3379"
_4images_user_id: "2635"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45449 -->
Mit torkelnden Bewegungen rollt der Torkler oder Wobbler eine schiefe Ebene hinunter.
Weitere hübsche und lustige Dinge gibt es hier:
http://www.wundersamessammelsurium.info/mathematisches/torkler/index.html