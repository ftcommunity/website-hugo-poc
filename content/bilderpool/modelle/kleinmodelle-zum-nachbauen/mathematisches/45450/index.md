---
layout: "image"
title: "Aufbau des universellen Torklers"
date: "2017-03-04T16:22:10"
picture: "Aufbau_des_universellen_Torklers_800px.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45450
- /details163f.html
imported:
- "2019"
_4images_image_id: "45450"
_4images_cat_id: "3379"
_4images_user_id: "2635"
_4images_image_date: "2017-03-04T16:22:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45450 -->
Im unteren Bild die beiden Ringteile, bestehend jeweils aus 16 Winkelsteinen 15°, 2 Winkelsteinen 60°, 2 BS5 und einem Federnocken. Hinzu kommt der BS30, auf dem sich die beiden Ringteile gegeneinander verschieben lassen.