---
layout: "image"
title: "Der universelle Torkler"
date: "2017-03-04T16:22:10"
picture: "Der_universelle_Torkler_1_800px.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
schlagworte: ["Torkler", "Wobbler"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- /php/details/45451
- /details58d4.html
imported:
- "2019"
_4images_image_id: "45451"
_4images_cat_id: "3379"
_4images_user_id: "2635"
_4images_image_date: "2017-03-04T16:22:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45451 -->
Der Mittelpunkabstand der beiden Ringe lässt sich jetzt verändern.
Der Durchmesser der Ringe beträgt 60 mm, der Radius also 30 mm. Beim einfachen &#8222;Torkler oder Wobbler&#8220; treffen sich die beiden Mittelpunkte in einem Punkt. 
Ein Spezialfall ist der Abstand r * Wurzel2, also 42,4 mm. Der gemeinsame Schwerpunkt hat jetzt eine gleichbleibende Höhe über der Unterlage. Dieser spezielle Torkler rollt die schiefe Ebene auf einer Schlangenlinie ohne Auf- und Ab-Bewegung hinunter.

Hier nochmal der Link zu den Erklärungen:
http://www.wundersamessammelsurium.info/mathematisches/torkler/index.html
Insbesondere die Artikel &#8222;Wobbler, Torkler oder Zwei- Scheiben- Roller&#8220; und &#8222;Zwei-Scheiben-Roller&#8220; sind lesenswert.