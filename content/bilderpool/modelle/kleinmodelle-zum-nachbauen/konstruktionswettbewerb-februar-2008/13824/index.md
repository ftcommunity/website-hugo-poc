---
layout: "image"
title: "Harald (4)"
date: "2008-03-03T12:39:06"
picture: "wettbewerbfebruar4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/13824
- /details96d7.html
imported:
- "2019"
_4images_image_id: "13824"
_4images_cat_id: "1268"
_4images_user_id: "104"
_4images_image_date: "2008-03-03T12:39:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13824 -->
