---
layout: "image"
title: "Go-Kart 3"
date: "2007-05-31T21:33:02"
picture: "Go-Kart_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10644
- /detailsddfe.html
imported:
- "2019"
_4images_image_id: "10644"
_4images_cat_id: "963"
_4images_user_id: "328"
_4images_image_date: "2007-05-31T21:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10644 -->
Die Hinterachse mit Einzelradaufhängung und Federung.