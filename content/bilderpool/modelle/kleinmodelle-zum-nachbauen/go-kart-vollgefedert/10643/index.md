---
layout: "image"
title: "Go-Kart 2"
date: "2007-05-31T21:33:02"
picture: "Go-Kart_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10643
- /details7820.html
imported:
- "2019"
_4images_image_id: "10643"
_4images_cat_id: "963"
_4images_user_id: "328"
_4images_image_date: "2007-05-31T21:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10643 -->
Die Vorderachse mit Einzelradaufhängung und Federung.