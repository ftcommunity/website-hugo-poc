---
layout: "image"
title: "Ein Ventilator für unterwegs"
date: "2015-07-04T16:11:48"
picture: "handventilatormitzweigeschwindigkeitsstufen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41375
- /details0f89.html
imported:
- "2019"
_4images_image_id: "41375"
_4images_cat_id: "3093"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T16:11:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41375 -->
Dieser Ventilator passt auch in eine Aktentasche. Dank des hervorragenden Motors steht nicht zu befürchten, dass sein Besitzer (anstatt des Ventilators) durchdreht. Besonders bei akuten Fällen schwerer Kopfüberhitzung zur Besinnung auf das Wesentliche also sehr zu empfehlen, insbesondere in sonnennahen Büroräumen.
