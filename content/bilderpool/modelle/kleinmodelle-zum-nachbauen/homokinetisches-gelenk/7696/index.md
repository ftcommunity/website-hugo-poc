---
layout: "image"
title: "Detailansicht"
date: "2006-12-04T00:02:54"
picture: "homokinetischesgelenk2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7696
- /detailse978.html
imported:
- "2019"
_4images_image_id: "7696"
_4images_cat_id: "726"
_4images_user_id: "104"
_4images_image_date: "2006-12-04T00:02:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7696 -->
Hier sieht man, wie die Winkelachsen eingreifen. Ein Video dazu gibt's auch.
