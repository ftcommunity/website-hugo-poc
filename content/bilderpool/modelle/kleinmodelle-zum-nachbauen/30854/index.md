---
layout: "image"
title: "FAKE Security Metalldetektor"
date: "2011-06-13T15:21:44"
picture: "metallfakesec1.jpg"
weight: "31"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/30854
- /details0713.html
imported:
- "2019"
_4images_image_id: "30854"
_4images_cat_id: "335"
_4images_user_id: "430"
_4images_image_date: "2011-06-13T15:21:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30854 -->
Der FAKE Security Metalldetektor, wird bei Personenkontrollen eingesetzt. Durch seinen lauten Signalton wird jeder Terrorist entlarvt.