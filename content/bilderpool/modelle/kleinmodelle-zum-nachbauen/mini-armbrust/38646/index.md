---
layout: "image"
title: "Armbrust_012"
date: "2014-04-23T15:49:08"
picture: "012.jpg"
weight: "12"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
schlagworte: ["Armbrust", "Mini", "Kleinmodell", "Bogen", "Katapult", "Schießen"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38646
- /details7250-3.html
imported:
- "2019"
_4images_image_id: "38646"
_4images_cat_id: "2885"
_4images_user_id: "2167"
_4images_image_date: "2014-04-23T15:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38646 -->
Hier die Armbrust nochmal im Fischertechnik Designer