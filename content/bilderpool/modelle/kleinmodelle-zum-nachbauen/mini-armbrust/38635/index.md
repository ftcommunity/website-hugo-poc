---
layout: "image"
title: "Armbrust_001"
date: "2014-04-23T15:49:08"
picture: "001.jpg"
weight: "1"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
schlagworte: ["Armbrust", "Mini", "Kleinmodell", "Bogen", "Katapult", "Schießen"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38635
- /detailsbcd6-2.html
imported:
- "2019"
_4images_image_id: "38635"
_4images_cat_id: "2885"
_4images_user_id: "2167"
_4images_image_date: "2014-04-23T15:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38635 -->
Eine kleine Armbrust:

Den Bogen der Armbrust bildet eine 15x90-Bauplatte, die über einen Rollenbock mit dem Rest verbunden ist. Es wurde keine einfache Verbindungsplatte verwendet, da über den Rollenbock mehr Flexibilität gewährleistet ist. Außerdem ist die Verbindungsfläche der Achsadapter deutlich kleiner, wodurch der Bogen gleichmäßiger belastet werden kann.
Der Bogen ist mit einem 10 cm Pneumatikschlauch verbunden, der als Sehne dient.
Die Klemmbuchsen sollen den Schlauch nur zusätzlich fixieren.
Der Lauf für die Bolzen besteht lediglich aus der Nut eines Bausteins.
Der Abzug der Armbrust besteht aus einer Adapterlasche(aus dem Statikbereich) die durch Druck eine Stange, die die Sehne hält, nach unten zieht und so die Sehne freigibt.