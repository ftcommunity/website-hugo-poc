---
layout: "image"
title: "Armbrust_014"
date: "2014-04-23T15:49:08"
picture: "014.jpg"
weight: "14"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
schlagworte: ["Armbrust", "Mini", "Kleinmodell", "Bogen", "Katapult", "Schießen"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- /php/details/38648
- /details43f5.html
imported:
- "2019"
_4images_image_id: "38648"
_4images_cat_id: "2885"
_4images_user_id: "2167"
_4images_image_date: "2014-04-23T15:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38648 -->
Hier die Armbrust nochmal im Fischertechnik Designer