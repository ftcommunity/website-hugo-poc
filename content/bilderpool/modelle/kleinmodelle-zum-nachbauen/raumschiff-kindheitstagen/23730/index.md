---
layout: "image"
title: "Vereint"
date: "2009-04-15T06:35:06"
picture: "raumschiff4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23730
- /details0db4.html
imported:
- "2019"
_4images_image_id: "23730"
_4images_cat_id: "1621"
_4images_user_id: "104"
_4images_image_date: "2009-04-15T06:35:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23730 -->
Das Tochterschiff kann in das große Raumschiff eingesteckt werden und ab geht's. Mit diesem simplen Modell habe ich damals viele Stunden gespielt, alleine oder mit meinem Bruder oder Freunden.
