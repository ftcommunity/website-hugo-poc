---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 2"
date: "2005-02-25T22:51:26"
picture: "Frontdetail.jpg"
weight: "2"
konstrukteure: 
- "Chevyfahrer"
fotografen:
- "Chevyfahrer"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3651
- /details213a.html
imported:
- "2019"
_4images_image_id: "3651"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3651 -->
kleine Arktisraupe mit Wohnmodul u.Bergewinde