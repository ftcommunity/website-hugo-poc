---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 8"
date: "2005-02-25T23:17:36"
picture: "Wohnmodul.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3657
- /details2b13.html
imported:
- "2019"
_4images_image_id: "3657"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T23:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3657 -->
Das Modul besteht aus 2 Grundplatten 30x90 verbunden durch 2 15er in jedem Eck.Seiten mit 30x90 Verkleidungsplatten.