---
layout: "image"
title: "Rückansicht"
date: "2015-07-04T11:50:08"
picture: "ventilatormitschwenkmechanik2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41372
- /details425c.html
imported:
- "2019"
_4images_image_id: "41372"
_4images_cat_id: "3092"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T11:50:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41372 -->
Die Schwenkmechanik ist ein einfacher Exzenter. Die Federn unter der Bauplatte verhindern a) dass die Tischplatte zum Lautsprecher wird und b) dass der Ventilator sich durch seine Vibration langsam auf dem Tisch fortbewegt.
