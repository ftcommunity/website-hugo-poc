---
layout: "image"
title: "Wagenheber - Mini - gestreckt"
date: "2016-04-02T17:06:56"
picture: "IMG_4021.jpg"
weight: "37"
konstrukteure: 
- "Techum"
fotografen:
- "Techum"
uploadBy: "techum"
license: "unknown"
legacy_id:
- /php/details/43227
- /detailsad60.html
imported:
- "2019"
_4images_image_id: "43227"
_4images_cat_id: "335"
_4images_user_id: "1217"
_4images_image_date: "2016-04-02T17:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43227 -->
Wagenheber