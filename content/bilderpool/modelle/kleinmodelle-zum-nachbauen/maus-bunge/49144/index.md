---
layout: "image"
title: "Mausbunge im Einsatz"
date: 2021-04-21T14:28:52+02:00
picture: "Pictures1.jpg"
weight: "1"
konstrukteure: 
- "Christian Pütter"
fotografen:
- "Christian Pütter"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Mausbunge im Einsatz zu sehen, es ist einfach auf die Tischplatte geklemmt.
Das Kabel der Maus ist dabei schön zurückgezogen.