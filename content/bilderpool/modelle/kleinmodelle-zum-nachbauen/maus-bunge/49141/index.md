---
layout: "image"
title: "Gewicht für das Kabel"
date: 2021-04-21T14:28:47+02:00
picture: "Pictures4.jpg"
weight: "4"
konstrukteure: 
- "Christian Pütter"
fotografen:
- "Christian Pütter"
uploadBy: "Website-Team"
license: "unknown"
---

Damit das ganze dann auch ordentlich funktioniert wird dieses kleine Gewicht an das Mauskabel gehangen, damit es zurückgezogen wird, Das Gewicht muss natürlich an die eigenen Bedürfnisse angepasst werden.