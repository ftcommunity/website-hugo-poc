---
layout: "image"
title: "Ansicht von unten"
date: 2021-04-21T14:28:49+02:00
picture: "Pictures3.jpg"
weight: "3"
konstrukteure: 
- "Christian Pütter"
fotografen:
- "Christian Pütter"
uploadBy: "Website-Team"
license: "unknown"
---

Die Beiden Rollen sind so eingestellt, dass sie sicht so gerade eben nicht berühren. Außerdem sind sie so montiert, dass sie sich leichtläufig drehen können.