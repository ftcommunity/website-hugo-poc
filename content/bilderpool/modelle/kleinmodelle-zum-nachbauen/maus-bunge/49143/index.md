---
layout: "image"
title: "Seitenansicht"
date: 2021-04-21T14:28:50+02:00
picture: "Pictures2.jpg"
weight: "2"
konstrukteure: 
- "Christian Pütter"
fotografen:
- "Christian Pütter"
uploadBy: "Website-Team"
license: "unknown"
---

Die Feder ist auf Beiden seiten mit einem Gelenkwürfel montiert. Die Kugelbahnschiene sowie die Klemmbereiche sind imt einer Schicht Isolierband versehen, die Schiene damit das Kabel leichter "rutscht" weil die unebenheiten weg sind und der KLemmbereich damit es etwas mehr Reibung gegen den Tisch da ist.