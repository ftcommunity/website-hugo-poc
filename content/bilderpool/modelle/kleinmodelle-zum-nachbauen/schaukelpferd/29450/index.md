---
layout: "image"
title: "Schaukelpferd 1"
date: "2010-12-12T16:03:09"
picture: "schaukelpferd1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29450
- /detailsdd07.html
imported:
- "2019"
_4images_image_id: "29450"
_4images_cat_id: "2143"
_4images_user_id: "1162"
_4images_image_date: "2010-12-12T16:03:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29450 -->
Ich habe einmal ein Mini-Modell entworfen. Und es ist ein Schaukelpferd geworden. 
Es würde sich auch prima für einen Fischertechnik- Adventskalender eignen.
