---
layout: "image"
title: "Beispiel 2: Das Kreuz einer Kreuzblende"
date: 2020-05-11T16:09:18+02:00
picture: "2020-01-06 Optische Bank11.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Linse, Kreuzblende und noch einer Linse kann man dieses Bild erhalten, dass das Kreuz der Kreuzblende vergrößert darstellt.
