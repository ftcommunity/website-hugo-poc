---
layout: "image"
title: "Magazin gefüllt"
date: 2020-05-11T16:09:17+02:00
picture: "2020-01-06 Optische Bank02.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Drei verschiedene Ur-Linsenstärken sind je zweifach vertreten (die Linsenstärke ist durch 1, 2 oder 3 aufgesteckte Federnocken gekennzeichnet). Außerdem stehen Spiegel, Hohlspiegel, Schlitzblende mit senkrechtem und waagerechtem Schlitz, Kreuz- und Lochblende zur Verfügung.

Alle Elemente sind leicht herausnehmbar und finden auch leicht ihren Platz. In den Strahlengang gesetzt, sitzen sie zuverlässig, lassen sich aber leicht verschieben.

Links davon der Ein-/Aus-Schalter, die Lagerung für die Metallachsen, die Stromzufuhr auf die Achsen und die frei verschiebbare (verdeckte) Lampe.
