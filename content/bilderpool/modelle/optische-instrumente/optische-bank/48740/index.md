---
layout: "image"
title: "Die Bildplatte (1)"
date: 2020-05-11T16:09:22+02:00
picture: "2020-01-06 Optische Bank08.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist eine 31376 Zentrierplatte 98x90 licht-elektronik und ein rares Teil. Selbstverständlich kann man sich sowas auch aus Pappe o.ä. selber herstellen.
