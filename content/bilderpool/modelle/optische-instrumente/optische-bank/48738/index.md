---
layout: "image"
title: "Beispiel 1: Abbildung des Glühfadens"
date: 2020-05-11T16:09:20+02:00
picture: "2020-01-06 Optische Bank10.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist nur eine Linse im Strahlengang, und man erhält durch Verschieben eine recht gute Abbildung des Glühfadens der Lampe. Man beachte, dass das Bild durch die Linse sowohl in x- als auch in y-Richtung seitenverkehrt ist, das obere Ende des Bildes steht also für das untere Ende des Glühfadens.
