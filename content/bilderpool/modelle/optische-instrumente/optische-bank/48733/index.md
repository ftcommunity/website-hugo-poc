---
layout: "image"
title: "Stromversorgung"
date: 2020-05-11T16:09:14+02:00
picture: "2020-01-06 Optische Bank05.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Versorgung der Metallachsen ist nicht spektakulär, aber diese Form der Versorgung finde ich zu Unrecht etwas in Vergessenheit geraten.
