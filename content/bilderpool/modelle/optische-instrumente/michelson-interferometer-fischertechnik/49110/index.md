---
layout: "image"
title: "Interferogramm einer LED-Taschenlampe mit ft-Spiegeln"
date: 2021-04-18T13:47:55+02:00
picture: "MIF-White-Light-Interferogram1.JPG"
weight: "6"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Verstellung lässt sich so genau einstellen, dass man Weißlichtinterferenz mit einer LED-Taschenlampe beobachten kann