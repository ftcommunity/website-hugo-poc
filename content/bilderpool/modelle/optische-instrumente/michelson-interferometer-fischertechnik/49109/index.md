---
layout: "image"
title: "Weißlicht-Interferogramme mit Vorderflächenspiegeln"
date: 2021-04-18T13:47:54+02:00
picture: "White-light-Interference-patterns.png"
weight: "7"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Verschiedene Weißlicht-Streifen-Interferogramme mit Vorderflächenspiegeln