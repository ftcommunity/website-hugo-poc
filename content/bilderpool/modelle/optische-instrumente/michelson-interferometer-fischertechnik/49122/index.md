---
layout: "image"
title: "Spiegel-Verstelleinheit – von links"
date: 2021-04-18T13:48:08+02:00
picture: "MIF-Mirror-Stage-left.JPG"
weight: "19"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

