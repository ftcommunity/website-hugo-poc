---
layout: "image"
title: "Linear-Verstelleinheit – von schräg vorne"
date: 2021-04-18T13:48:13+02:00
picture: "MIF-Linear-Stage-right2.JPG"
weight: "15"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

