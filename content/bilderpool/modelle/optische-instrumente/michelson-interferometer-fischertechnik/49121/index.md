---
layout: "image"
title: "Michelson Interferometer – von oben"
date: 2021-04-18T13:48:07+02:00
picture: "MIF-Interferometer-Top.JPG"
weight: "2"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Fischertechnik-Michelson-Interferometer von oben. Links unten sieht man die Spiegelverstelleinheit, rechts oben die Linearverstellung. Der Drehkranz oben ist für den Strahlteiler