---
layout: "image"
title: "Linear-Verstelleinheit – Aufsicht"
date: 2021-04-18T13:48:17+02:00
picture: "MIF-Linear-Stage-top.JPG"
weight: "11"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Linear-Verstelleinheit besteht aus einer Flexure-Konstruktion mit den 4 gelben Platten, die einen einfachen Spiegelhalter trägt und einem mehrstufigem Untersetzungs-Mechanismus (Hebel, Zahnrad und Schnecke), der eine sehr feinfühlige Einstellung des Abstands des Spiegels erlaubt.