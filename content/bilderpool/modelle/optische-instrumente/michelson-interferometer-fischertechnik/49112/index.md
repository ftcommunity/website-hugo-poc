---
layout: "image"
title: "Interferogramm einer roten LED mit ft-Spiegeln"
date: 2021-04-18T13:47:57+02:00
picture: "MIF-LED-Interferogram3.JPG"
weight: "4"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Ring-Interferogramm einer LED mit ft-Spiegeln und kleinst möglichem Spiegelabstand.