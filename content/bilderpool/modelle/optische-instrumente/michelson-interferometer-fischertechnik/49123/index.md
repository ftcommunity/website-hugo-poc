---
layout: "image"
title: "Spiegel-Verstelleinheit – Frontalansicht"
date: 2021-04-18T13:48:10+02:00
picture: "MIF-Mirror-Stage_front.JPG"
weight: "18"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Mit dieser Spiegel-Verstellung kann man die Strahlen der beiden Interferometerarme zur Deckung bringen. Auch hier wird über Kombination von 2 Hebeln und Zahnrad-Schnecke eine große Untersetzung erreicht.