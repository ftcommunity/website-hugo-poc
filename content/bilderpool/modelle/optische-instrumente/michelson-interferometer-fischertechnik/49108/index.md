---
layout: "image"
title: "Fundament des Interferometers"
date: 2021-04-18T13:47:53+02:00
picture: "MIF-Baseplane.JPG"
weight: "8"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Fundament des Interferometers mit Strahlteilerdrehkranz, Aufnahmeschiene für die Linearverstellung und Anker für die Spiegelverstellung