---
layout: "image"
title: "Interferogramm einer roten LED mit Vorderflächenspiegeln"
date: 2021-04-18T13:47:56+02:00
picture: "MIF-LED-Interferogram4.JPG"
weight: "5"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Streifen-Interferogramm mit Vorderflächenspiegeln