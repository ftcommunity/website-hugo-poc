---
layout: "image"
title: "Spiegel-Verstelleinheit – von oben"
date: 2021-04-18T13:48:05+02:00
picture: "MIF-Mirror-Stage-top2.JPG"
weight: "21"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

