---
layout: "image"
title: "Spiegel-Verstelleinheit – von hinten"
date: 2021-04-18T13:48:06+02:00
picture: "MIF-Mirror-Stage_rear2.JPG"
weight: "20"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

