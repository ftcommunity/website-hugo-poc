---
layout: "image"
title: "Optische Bank"
date: 2021-04-18T13:48:03+02:00
picture: "MIF-Optical-bench.JPG"
weight: "23"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Lichtquellen können auf einer optischen Bank auf einer separaten Bauteil-Box montiert werden