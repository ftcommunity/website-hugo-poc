---
layout: "image"
title: "Linear-Verstelleinheit – Frontalansicht"
date: 2021-04-18T13:48:15+02:00
picture: "MIF-Linear-Stage-front.JPG"
weight: "13"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

