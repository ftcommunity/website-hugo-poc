---
layout: "image"
title: "Positioniervorrichtung für Laser und Blende"
date: 2021-04-18T13:48:01+02:00
picture: "MIF-Laser-Mount.JPG"
weight: "25"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

