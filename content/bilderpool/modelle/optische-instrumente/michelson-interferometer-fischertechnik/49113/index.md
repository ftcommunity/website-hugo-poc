---
layout: "image"
title: "Interferometer im Einsatz"
date: 2021-04-18T13:47:58+02:00
picture: "MIF-LED-Interferogram2.JPG"
weight: "3"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Das Interferometer mit Strahlteilerwürfel im Einsatz. Lichtquelle ist eine superhelle rote LED, die durch eine Linse mit f=35mm strahlt.