---
layout: "image"
title: "Strahlteiler-Turm – Ansicht Eintrittsebene"
date: 2021-04-18T13:48:19+02:00
picture: "MIF-Beamsplitter-Mount-Entry.JPG"
weight: "10"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

