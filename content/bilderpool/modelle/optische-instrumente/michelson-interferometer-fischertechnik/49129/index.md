---
layout: "image"
title: "Linear-Verstelleinheit – von unten"
date: 2021-04-18T13:48:16+02:00
picture: "MIF-Linear-Stage-bottom.JPG"
weight: "12"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

