---
layout: "image"
title: "Linear-Verstelleinheit – Ankopplung Antriebshebel an Flexure"
date: 2021-04-18T13:48:12+02:00
picture: "MIF-Linear-Stage-Flexure-Drive.JPG"
weight: "16"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Antriebshebel drückt die Flexure-Verstellung nach vorne, ist aber nicht fest mit ihr verbunden