---
layout: "image"
title: "Positioniervorrichtung für LED-Taschenlampe"
date: 2021-04-18T13:48:02+02:00
picture: "MIF-Torch-Mount.JPG"
weight: "24"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

