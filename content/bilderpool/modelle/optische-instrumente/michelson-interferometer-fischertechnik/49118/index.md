---
layout: "image"
title: "Spiegel-Verstelleinheit – von schräg hinten"
date: 2021-04-18T13:48:04+02:00
picture: "MIF-Mirror-Stage_rear-right.JPG"
weight: "22"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

