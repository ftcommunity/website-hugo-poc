---
layout: "image"
title: "Michelson Interferometer mit Fischertechnik"
date: 2021-04-18T13:48:20+02:00
picture: "MIF-Interferometer-Birdview.JPG"
weight: "1"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Ein Michelson-Interferometer aufgebaut mit Fischertechnik. Das Modell ist beschrieben in ft-pedia 1/2021.