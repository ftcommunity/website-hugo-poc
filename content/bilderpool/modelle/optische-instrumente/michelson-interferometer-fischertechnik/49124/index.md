---
layout: "image"
title: "Spiegelhalter der Linear-Verstelleinheit von unten"
date: 2021-04-18T13:48:11+02:00
picture: "MIF-Linear-Stage-Mirror-Mount-bottom.JPG"
weight: "17"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

