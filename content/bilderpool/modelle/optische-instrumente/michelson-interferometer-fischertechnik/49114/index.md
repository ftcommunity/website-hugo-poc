---
layout: "image"
title: "LED-Halterung"
date: 2021-04-18T13:47:59+02:00
picture: "MIF-LED-Mount.JPG"
weight: "26"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

