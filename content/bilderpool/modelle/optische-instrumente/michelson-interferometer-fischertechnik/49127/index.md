---
layout: "image"
title: "Linear-Verstelleinheit – von rechts"
date: 2021-04-18T13:48:14+02:00
picture: "MIF-Linear-Stage-right.JPG"
weight: "14"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer", "Linearverstellung", "Flexure"]
uploadBy: "Website-Team"
license: "unknown"
---

