---
layout: "image"
title: "Strahlteiler-Turm – Ansicht von Seite der Spiegelverstellung"
date: 2021-04-18T13:47:51+02:00
picture: "MIF-Beamsplitter-Mount1.JPG"
weight: "9"
konstrukteure: 
- "Florian Bauer"
fotografen:
- "Florian Bauer"
schlagworte: ["Optik", "Michelson-Interferometer"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Kippung des Strahlteilers kann durch Verschiebung der Streben entlang der Nut-Verbinder eingestellt werden.