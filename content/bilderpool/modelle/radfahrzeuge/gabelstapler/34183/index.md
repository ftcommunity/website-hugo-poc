---
layout: "image"
title: "Ferngesteuerter Gabelstapler, 2. Version"
date: "2012-02-14T23:29:34"
picture: "ftstapler33.jpg"
weight: "3"
konstrukteure: 
- "mattnik"
fotografen:
- "mattnik"
uploadBy: "mattnik"
license: "unknown"
legacy_id:
- /php/details/34183
- /details8f03.html
imported:
- "2019"
_4images_image_id: "34183"
_4images_cat_id: "354"
_4images_user_id: "1447"
_4images_image_date: "2012-02-14T23:29:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34183 -->
Die 2. Version, von der 1. gibt's ein Video im Forum. Die Foto's sind mit diesem Pool leider nicht kompatibel...