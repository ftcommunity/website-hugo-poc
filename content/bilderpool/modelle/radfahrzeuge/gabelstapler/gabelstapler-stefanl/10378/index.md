---
layout: "image"
title: "Gabelstapler 5"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10378
- /details7fe2.html
imported:
- "2019"
_4images_image_id: "10378"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10378 -->
Ganz ausgefahren.
