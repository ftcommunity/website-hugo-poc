---
layout: "image"
title: "Gabelstapler 8"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10381
- /details1c53.html
imported:
- "2019"
_4images_image_id: "10381"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10381 -->
