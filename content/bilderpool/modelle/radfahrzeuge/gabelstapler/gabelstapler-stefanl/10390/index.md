---
layout: "image"
title: "Gabelstabler 17"
date: "2007-05-12T21:51:43"
picture: "gabelstabler3.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10390
- /details48d8.html
imported:
- "2019"
_4images_image_id: "10390"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T21:51:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10390 -->
