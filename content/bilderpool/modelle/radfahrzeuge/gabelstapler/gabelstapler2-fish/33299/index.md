---
layout: "image"
title: "Gabelstapler gesamt"
date: "2011-10-23T16:00:07"
picture: "gabelstaplerfish1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/33299
- /detailsc323-2.html
imported:
- "2019"
_4images_image_id: "33299"
_4images_cat_id: "2464"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33299 -->
Der komplette Gabelstapler, hier gut zu sehen ist die Blendverkleidung rechts des Farbsensors.