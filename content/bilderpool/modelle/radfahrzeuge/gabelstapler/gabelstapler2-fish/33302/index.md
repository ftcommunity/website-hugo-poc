---
layout: "image"
title: "Gabelstapler oben"
date: "2011-10-23T16:00:07"
picture: "gabelstaplerfish4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/33302
- /details12ba-2.html
imported:
- "2019"
_4images_image_id: "33302"
_4images_cat_id: "2464"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33302 -->
-