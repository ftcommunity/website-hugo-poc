---
layout: "image"
title: "Stapler02.jpg"
date: "2009-02-05T21:22:19"
picture: "Stapler02.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17317
- /detailsa030-3.html
imported:
- "2019"
_4images_image_id: "17317"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:22:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17317 -->
Die Vorderseite des Teleskopschubs. Der Motor treibt über die Schnecken den mittleren Rahmen an. Dieser strafft die Ketten (die mit einer Seite am äußeren Rahmen fest sind) und zieht damit den inneren Rahmen auf den Zahnstangen nach oben.
