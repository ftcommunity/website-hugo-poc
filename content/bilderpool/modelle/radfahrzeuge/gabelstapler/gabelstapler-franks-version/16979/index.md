---
layout: "image"
title: "Gabelstabler_02"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16979
- /detailsb04f.html
imported:
- "2019"
_4images_image_id: "16979"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16979 -->
von links hinten