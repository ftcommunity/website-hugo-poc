---
layout: "image"
title: "Gabelstabler_05"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16982
- /details19a2.html
imported:
- "2019"
_4images_image_id: "16982"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16982 -->
von der Seite