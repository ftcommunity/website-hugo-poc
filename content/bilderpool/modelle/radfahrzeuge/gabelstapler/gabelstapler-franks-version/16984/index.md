---
layout: "image"
title: "Gabelstabler_07"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/16984
- /details4778-3.html
imported:
- "2019"
_4images_image_id: "16984"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16984 -->
von unten