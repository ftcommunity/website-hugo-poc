---
layout: "image"
title: "Gabelstabler4"
date: "2005-05-15T17:48:16"
picture: "Gabelstabler_005.jpg"
weight: "4"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/4147
- /details73db-3.html
imported:
- "2019"
_4images_image_id: "4147"
_4images_cat_id: "1556"
_4images_user_id: "332"
_4images_image_date: "2005-05-15T17:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4147 -->
Hier sieht man deutlich, warum die Schnecke der Gabel mit einer Kette angetrieben wird. Ansonsten bleibt die Schneckenmutter an dam Motorgetriebe hängen.
