---
layout: "image"
title: "Gabelstapler von hinten"
date: "2010-04-20T21:29:28"
picture: "gabelstapler4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/26976
- /details9e85-2.html
imported:
- "2019"
_4images_image_id: "26976"
_4images_cat_id: "1938"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26976 -->
Vorne im Bild sind Akku und Interface zu sehen.