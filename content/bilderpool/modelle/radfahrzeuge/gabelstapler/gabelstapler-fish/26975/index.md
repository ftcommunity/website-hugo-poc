---
layout: "image"
title: "Gabelstapler"
date: "2010-04-20T21:29:28"
picture: "gabelstapler3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/26975
- /details7f3b-2.html
imported:
- "2019"
_4images_image_id: "26975"
_4images_cat_id: "1938"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26975 -->
Gabel oben.