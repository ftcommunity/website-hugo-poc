---
layout: "image"
title: "Gabelstapler (verladefertig)"
date: "2007-04-06T15:10:45"
picture: "Gabelstapler_016.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Gabelstapler"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9993
- /details36cf-2.html
imported:
- "2019"
_4images_image_id: "9993"
_4images_cat_id: "903"
_4images_user_id: "504"
_4images_image_date: "2007-04-06T15:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9993 -->
Das alles müsste man halt abmachen um ihn so kleinzumachen, dass er hinten an einem LKW nicht übersteht. Die Hubfunktion der Gabel kann übreigens einfach so mit ein paar Zahnstangen, Kettengliedern und einer längeren Stange nach oben hin erweitert werden.