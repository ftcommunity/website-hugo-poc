---
layout: "image"
title: "Antrieb"
date: "2007-04-06T15:10:45"
picture: "Gabelstapler_013.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Gabelstapler"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9996
- /detailse727-2.html
imported:
- "2019"
_4images_image_id: "9996"
_4images_cat_id: "903"
_4images_user_id: "504"
_4images_image_date: "2007-04-06T15:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9996 -->
Hier ein Blick in die Antriebsmechanik. War gar nicht so einfach das so schmal hinzubekommen. Ist wahrscheinlich nicht die Beste läuft bisher aber ganz gut.