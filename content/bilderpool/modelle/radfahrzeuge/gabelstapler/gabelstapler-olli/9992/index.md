---
layout: "image"
title: "Gabelstapler"
date: "2007-04-06T15:10:45"
picture: "Gabelstapler_018.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
schlagworte: ["Gabelstapler"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/9992
- /details7c9b-2.html
imported:
- "2019"
_4images_image_id: "9992"
_4images_cat_id: "903"
_4images_user_id: "504"
_4images_image_date: "2007-04-06T15:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9992 -->
Das ist mein Gabelstapler. Eigentlich sollte er mal ein Verladegabelstapler werden. Also ein Gabelstapler den man am LKW hochheben kann und dann rumfahren kann. Vielleicht bau ich nochmal einen passenden LKW dazu. Er ist ferngesteuert kann die Gabel hoch- und runterfahren und hat halt diese Raupenfunktion am Empfänger mit der er sich auf der Stelle drehen kann.