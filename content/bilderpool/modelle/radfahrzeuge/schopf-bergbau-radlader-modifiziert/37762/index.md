---
layout: "image"
title: "Schopf-Bergbau-Radlader   -modifiziert"
date: "2013-10-23T21:50:46"
picture: "bergbauradlader10.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37762
- /details5ae5.html
imported:
- "2019"
_4images_image_id: "37762"
_4images_cat_id: "2805"
_4images_user_id: "22"
_4images_image_date: "2013-10-23T21:50:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37762 -->
SCHOPF Mining - Lösungen für die  Tunnel- und Bergbau-Industrie
