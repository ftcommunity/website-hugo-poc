---
layout: "image"
title: "Schopf-Bergbau-Radlader   -modifiziert"
date: "2013-10-23T21:50:46"
picture: "bergbauradlader01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37753
- /details5aa2.html
imported:
- "2019"
_4images_image_id: "37753"
_4images_cat_id: "2805"
_4images_user_id: "22"
_4images_image_date: "2013-10-23T21:50:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37753 -->
Ferngesteuerter Schopf-Bergbau-Radlader mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung

modifiziert mit:

- pneumatik Ventilen 3-way, 2 position  NO + NC & Distributor 

- 15x15 Alu-Profilen OpenBeam

Inspiration:  Erbes-Budeheim-2013   -Jörg und Erik Busch
