---
layout: "image"
title: "Gesamtansicht der Kehrmaschiene"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine04.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31856
- /details7620.html
imported:
- "2019"
_4images_image_id: "31856"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31856 -->
Aufbau des Modells gemäß beiliegender Anleitung. Ansicht von der anderen Seite.