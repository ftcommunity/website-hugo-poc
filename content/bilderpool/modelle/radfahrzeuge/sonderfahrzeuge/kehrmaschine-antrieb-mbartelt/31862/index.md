---
layout: "image"
title: "Servo und Lenkung"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine10.jpg"
weight: "10"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31862
- /details683e.html
imported:
- "2019"
_4images_image_id: "31862"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31862 -->
Hier sieht man, wie ich den Servo eingebaut habe. Im Hintergrund ist die schwenkbare Halterung des Auffangbeälters zu sehen. Leider steuert der Servo so falsch herum. Links und rechts sind vertauscht. Hat jemand eine Idee wie das zu lösen wäre? Oder kann jemand sagen, wie das Sevokabel umgepohlt werden müßte.