---
layout: "image"
title: "Ansicht von unten"
date: "2011-09-20T17:04:50"
picture: "kehrmaschiene6.jpg"
weight: "17"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31869
- /details4ab1-2.html
imported:
- "2019"
_4images_image_id: "31869"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31869 -->
Hier noch mal eine gesamt Ansicht von unten.