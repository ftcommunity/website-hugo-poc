---
layout: "image"
title: "1. Antriebsmodifikation"
date: "2011-09-20T17:04:50"
picture: "kehrmaschiene3.jpg"
weight: "14"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31866
- /detailsc53a.html
imported:
- "2019"
_4images_image_id: "31866"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31866 -->
Antriebsachse, Differential und Reinigungsbürste.