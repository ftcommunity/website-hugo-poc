---
layout: "overview"
title: "Kehrmaschine mit Antrieb (mbartelt)"
date: 2020-02-22T07:57:23+01:00
legacy_id:
- /php/categories/2376
- /categoriesff8b.html
- /categories61e4.html
- /categoriesba2e.html
- /categoriesed9f.html
- /categories41d2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2376 --> 
Hier wurde versucht in das Modell Nr. 5 aus dem Baukasten "Kehrmaschinen", Art.Nr. 500878 einen Antriebsmotor, den Servo für die Lenkung, das Light & Sound Mudul sowie einen Akku einzubauen. Da meine Sammlung überwiegend aus Teilen der 70er Kästen stammt und besteht, sind alle Ergänzugen mit grauen Steinen vorgenommen worden.