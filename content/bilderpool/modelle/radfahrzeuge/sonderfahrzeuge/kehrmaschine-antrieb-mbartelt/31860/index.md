---
layout: "image"
title: "Innenraum"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine08.jpg"
weight: "8"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31860
- /details198e.html
imported:
- "2019"
_4images_image_id: "31860"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31860 -->
Hier sieht man den Akku und den Empfänger des Control-Sets sowie die Blinkleuchten.