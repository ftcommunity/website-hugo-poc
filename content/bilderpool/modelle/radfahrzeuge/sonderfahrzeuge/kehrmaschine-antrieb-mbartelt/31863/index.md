---
layout: "image"
title: "Servo und Lenkung"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine11.jpg"
weight: "11"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31863
- /details97fc.html
imported:
- "2019"
_4images_image_id: "31863"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31863 -->
Hier noch mal der Einbau des Servos vom nahen.