---
layout: "image"
title: "1. Lenkungsmodifikation"
date: "2011-09-20T17:04:50"
picture: "kehrmaschiene5.jpg"
weight: "16"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31868
- /detailsd0ac-2.html
imported:
- "2019"
_4images_image_id: "31868"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-20T17:04:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31868 -->
Ein wenig gübeln und ein paar andere Teile und schon ist der Servo anders herum eingebaut. Nun stimmt auch die Lenkung. Das Soundmodul ist schon mal gut untergebracht.