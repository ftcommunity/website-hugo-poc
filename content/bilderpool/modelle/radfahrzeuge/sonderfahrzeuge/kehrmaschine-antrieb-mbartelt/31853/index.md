---
layout: "image"
title: "Gesamtansicht der Kehrmaschiene"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine01.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31853
- /detailsed9d-2.html
imported:
- "2019"
_4images_image_id: "31853"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31853 -->
Aufbau des Modells gemäß beiliegender Anleitung.