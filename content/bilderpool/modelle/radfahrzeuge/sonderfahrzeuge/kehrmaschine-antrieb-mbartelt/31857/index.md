---
layout: "image"
title: "Antrieb untere Reinigungsbürste"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine05.jpg"
weight: "5"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- /php/details/31857
- /detailsf2ea.html
imported:
- "2019"
_4images_image_id: "31857"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31857 -->
Hier sieht man, das die untere Reinigungsbürste über die Kette von der Vorderachse angetrieben wird.