---
layout: "comment"
hidden: true
title: "11628"
date: "2010-06-06T20:40:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das Modell ist schön, aber die Fotos könnten noch etwas schärfer sein. Tipp: Mach doch mehrere Bilder von jedem Motiv und sortiere die unscharfen aus, behalte nur das Beste. Dann erkennt man auch die Details noch besser.

Gruß,
Stefan