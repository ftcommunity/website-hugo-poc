---
layout: "image"
title: "Windmill Car"
date: "2010-06-04T10:54:30"
picture: "sm_windmill_car.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["wind-powered", "car"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/27356
- /details7503.html
imported:
- "2019"
_4images_image_id: "27356"
_4images_cat_id: "2234"
_4images_user_id: "585"
_4images_image_date: "2010-06-04T10:54:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27356 -->
A small wind-powered vehicle. I liked how this one turned out.
