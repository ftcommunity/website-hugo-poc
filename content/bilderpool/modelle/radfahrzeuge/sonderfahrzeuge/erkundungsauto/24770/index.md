---
layout: "image"
title: "Gesamtansicht"
date: "2009-08-13T20:03:29"
picture: "erkundungsauto01.jpg"
weight: "1"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- /php/details/24770
- /details8184.html
imported:
- "2019"
_4images_image_id: "24770"
_4images_cat_id: "1704"
_4images_user_id: "987"
_4images_image_date: "2009-08-13T20:03:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24770 -->
Gesamtansicht