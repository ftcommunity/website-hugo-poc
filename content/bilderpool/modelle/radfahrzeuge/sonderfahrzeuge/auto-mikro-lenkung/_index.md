---
layout: "overview"
title: "Auto mit Mikro-Lenkung"
date: 2020-02-22T07:57:25+01:00
legacy_id:
- /php/categories/2554
- /categories2a92.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2554 --> 
Aufgabenstellung: Servolenkung mit nur 1 Bausteinbreite zwischen den Achsschenkeln