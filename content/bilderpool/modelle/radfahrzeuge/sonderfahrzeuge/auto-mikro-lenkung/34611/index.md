---
layout: "image"
title: "Unterboden"
date: "2012-03-07T22:55:11"
picture: "automitmikrolenkung3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34611
- /detailsc016.html
imported:
- "2019"
_4images_image_id: "34611"
_4images_cat_id: "2554"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:55:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34611 -->
Um den Servohebel klemmen zwei BS7,5, die lediglich oben und unten von je 5 Kettengliedern zusammengehalten werden: Immer ein Rastkettenglied und dann wieder ein normales. Auch zwischen den BS7,5 liegt also eine "Rasteinheit".

Die silbernen Teile sind ArtNr. 35307, "Steckerbuchse 21" aus dem alten fischertechnik-Elektromechanik-Programm (http://www.ft-datenbank.de/search.php?keyword=35307). Ich brauchte ja irgendwas, was ein hinreichend kleines Loch besitzt, um sich mit möglichst wenig Spiel über die Zäpfchen der Achsschenkel zu stülpen. Irgendwas mit 4-mm-Löchern wäre viel zu groß gewesen.
