---
layout: "image"
title: "3/4-Ansicht"
date: "2012-03-07T22:55:11"
picture: "automitmikrolenkung2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34610
- /detailsa29f-2.html
imported:
- "2019"
_4images_image_id: "34610"
_4images_cat_id: "2554"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:55:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34610 -->
Die Lenkung ist schwer abenteuerlich, aber sie funktioniert besser als befürchtet.
