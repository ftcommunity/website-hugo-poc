---
layout: "image"
title: "Packung"
date: "2012-03-07T22:55:11"
picture: "automitmikrolenkung4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34612
- /detailsd48e.html
imported:
- "2019"
_4images_image_id: "34612"
_4images_cat_id: "2554"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:55:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34612 -->
Das ist alles so klein wie mir in kurzer Zeit möglich hingefriemelt. Die Batterie wird zwischen Servo und IR-Empfänger eingeklemmt.
