---
layout: "image"
title: "unten seitlich"
date: "2010-05-27T12:28:06"
picture: "dreiraedigerseitlichfahrer7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/27307
- /detailsec25.html
imported:
- "2019"
_4images_image_id: "27307"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27307 -->
wenn man eine Strebe entfernt, kann man das Getriebe besser sehen.
Die geschwungenen Streben sorgen nicht nur für ein sportliches Design, eine gewisse Steifheit ist auch wichtig dass sich nichts verzieht und keine Zahnräder "springen" können. Sonst zeigen die Räder irgendwann nicht mehr in die gleiche Richtung.
Die Übersetzung ist ziemlich gross dh die Drehteller drehen langsam aber dafür zuverlässig. Da man ja aber maximal 90° lenken muss kann man trotzdem relativ flott manöverieren.
Natürlich kann man die Räder auch endlos im Kreis rotieren lassen ;)