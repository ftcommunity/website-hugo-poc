---
layout: "image"
title: "06 Tankbefüllung"
date: "2012-05-15T22:00:23"
picture: "turboloescher03.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34948
- /details8427.html
imported:
- "2019"
_4images_image_id: "34948"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34948 -->
Das ganze ist drehbar, da sonst die Ventile und Schläuche zu breit wären und das ganze nicht passen würde.