---
layout: "image"
title: "13 Wasserkanone"
date: "2012-05-15T22:00:23"
picture: "turboloescher10.jpg"
weight: "13"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34955
- /details3d0e-2.html
imported:
- "2019"
_4images_image_id: "34955"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34955 -->
Das Ventil öffnet die zweite Düse. Dann ist aber der Druck zu gering und es kommt unterschiedlich viel Wasser raus, deshalb ist es eigentlich überflüssig.