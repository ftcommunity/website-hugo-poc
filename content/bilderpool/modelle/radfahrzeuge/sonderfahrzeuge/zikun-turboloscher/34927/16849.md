---
layout: "comment"
hidden: true
title: "16849"
date: "2012-05-17T15:48:37"
uploadBy:
- "-Matthias-"
license: "unknown"
imported:
- "2019"
---
Der Seilzug dient zum Kippen, er geht aber nicht durch den Drehkranz. Er wird von dem 50:1 Powermotor angetrieben, der S-Motor treibt die Welle an, die den ganzen Aufsatz dreht.