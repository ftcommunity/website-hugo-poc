---
layout: "image"
title: "19 Gesamt"
date: "2012-05-15T22:00:23"
picture: "turboloescher16.jpg"
weight: "19"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34961
- /details89c2-4.html
imported:
- "2019"
_4images_image_id: "34961"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34961 -->
Nochmal ein Gesamtbild zum Abschluss

Alle Lichter in dem Modell sind LEDs, keine ft-Lampen. (Die sind natürlich viel heller, als hier im Bild zu sehen ist)

Video: http://www.youtube.com/watch?v=kqJoE2jjxog