---
layout: "image"
title: "08 Unterseite"
date: "2012-05-15T22:00:23"
picture: "turboloescher05.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/34950
- /detailse413.html
imported:
- "2019"
_4images_image_id: "34950"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34950 -->
Das ist die Unterseite. Hier erkennt man auch den Schlauch, der direkt zum Drucklufttank geht.