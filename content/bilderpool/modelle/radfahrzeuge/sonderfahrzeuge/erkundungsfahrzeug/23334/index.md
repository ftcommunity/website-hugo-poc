---
layout: "image"
title: "Vorderradfederung"
date: "2009-03-02T18:07:36"
picture: "erkundungsfahrzeug05.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23334
- /detailsc6e3-2.html
imported:
- "2019"
_4images_image_id: "23334"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23334 -->
