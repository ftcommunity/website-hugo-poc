---
layout: "image"
title: "Fronansicht"
date: "2009-03-02T18:07:37"
picture: "erkundungsfahrzeug14.jpg"
weight: "14"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23343
- /detailsb689-3.html
imported:
- "2019"
_4images_image_id: "23343"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23343 -->
