---
layout: "image"
title: "Federung 3"
date: "2009-03-02T18:07:37"
picture: "erkundungsfahrzeug13.jpg"
weight: "13"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23342
- /details016c-6.html
imported:
- "2019"
_4images_image_id: "23342"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23342 -->
