---
layout: "overview"
title: "Erkundungsfahrzeug"
date: 2020-02-22T07:57:15+01:00
legacy_id:
- /php/categories/1586
- /categories4c11.html
- /categories8966.html
- /categories2811.html
- /categories4bd4.html
- /categories364b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1586 --> 
Dieses Erkundungsfahrzeug ist vollgefedert, hat eine Greifzange und eine Kamerahalterung um Bilder zu schießen.