---
layout: "image"
title: "Vorderräder"
date: "2009-03-02T18:07:37"
picture: "erkundungsfahrzeug12.jpg"
weight: "12"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23341
- /detailsbdd7-2.html
imported:
- "2019"
_4images_image_id: "23341"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23341 -->
