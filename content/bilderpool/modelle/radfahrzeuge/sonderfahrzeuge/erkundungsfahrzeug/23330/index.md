---
layout: "image"
title: "Gesamtansicht"
date: "2009-03-02T18:07:35"
picture: "erkundungsfahrzeug01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/23330
- /details907f-2.html
imported:
- "2019"
_4images_image_id: "23330"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23330 -->
