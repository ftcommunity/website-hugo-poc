---
layout: "image"
title: "monowheel_2"
date: "2013-09-13T11:07:03"
picture: "monowheel_2.jpg"
weight: "2"
konstrukteure: 
- "steve"
fotografen:
- "steve"
schlagworte: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37379
- /details5aa2-3.html
imported:
- "2019"
_4images_image_id: "37379"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37379 -->
Hier ein Blick auf das 2-Gang Getriebe. Zum Schalten wird eine Achse mit den enptr. Zahnrädern drauf verschoben. Das ganze hat jedoch noch eher Prototyp Charakter. Die Beschleunigung im  ersten Gang ist zwar ok,  der zweite Gang ist aber eindeutig zu lang übersetzt. Vermutlich wäre eine 20er/30er Zahnradkombination besser statt der 10er/20er.