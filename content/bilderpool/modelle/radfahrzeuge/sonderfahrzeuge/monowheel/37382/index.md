---
layout: "image"
title: "monowheel_5"
date: "2013-09-13T11:07:03"
picture: "monowheel_5.jpg"
weight: "5"
konstrukteure: 
- "steve"
fotografen:
- "steve"
schlagworte: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- /php/details/37382
- /details1e95-3.html
imported:
- "2019"
_4images_image_id: "37382"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37382 -->
Das Wheel ohne Antrieb. Innen ist das Teil praktisch hohl, um Platz für den Antrieb zu haben. Die Verstrebungen machen das Ganze aber trotzdem ziemlich stabil.