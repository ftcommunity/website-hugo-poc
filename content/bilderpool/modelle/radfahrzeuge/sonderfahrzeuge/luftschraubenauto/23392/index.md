---
layout: "image"
title: "Luftschraubenauto (1)"
date: "2009-03-06T19:34:34"
picture: "luftschraubenauto1.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/23392
- /details6c6c.html
imported:
- "2019"
_4images_image_id: "23392"
_4images_cat_id: "1595"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23392 -->
Ich habe neulich die Idee bekommen ein Auto mal mit Luftschrauben anzutreiben. Damit es nicht so schnell langweilig wird, sollte das Auto auch lenkbar und fernsteuerbar sein.

Das ist das Ergebnis. Vier Minimotoren mit zwei großen und zwei kleinen Propellern hinten, die mit dem Raupenkettenmodus des IR-Control Sets gesteuert werden.

Das Fahren damit geht erstaunlich gut! Das Auto reagiert schnell und ist sehr wendig, es kann sogar auf der Stelle drehen. Nur Rückwärtsfahren geht nicht so gut, die Propeller wirken ja nur in eine Richtung.