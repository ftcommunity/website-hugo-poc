---
layout: "comment"
hidden: true
title: "8797"
date: "2009-03-19T22:04:56"
uploadBy:
- "nula"
license: "unknown"
imported:
- "2019"
---
Erste Tests verliefen sehr positiv. Das Boot ist ziemlich flott und extrem wendig, da wirds in der Badewanne schnell langweilig :-)
Ich habe das ganze in eine Sortierwanne aus der Box 1000 reingebastelt, einmal mit Folie überzogen damit die Schlitze für die Sortierstege dicht sind und fertig. Ich kann ja mal Bilder machen.