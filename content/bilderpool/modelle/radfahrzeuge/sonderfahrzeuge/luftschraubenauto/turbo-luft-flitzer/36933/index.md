---
layout: "image"
title: "Luft-Flitzer von Nele&Noah OBEN (Motor aus)"
date: "2013-05-26T09:50:17"
picture: "turboluftflitzer3.jpg"
weight: "3"
konstrukteure: 
- "Nele & Noah"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36933
- /detailsc6ce-4.html
imported:
- "2019"
_4images_image_id: "36933"
_4images_cat_id: "2745"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36933 -->
