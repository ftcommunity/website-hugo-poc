---
layout: "image"
title: "Luft-Flitzer von Nele&Noah  ENDE"
date: "2013-05-26T09:50:17"
picture: "turboluftflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Nele & Noah"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36936
- /details08a0.html
imported:
- "2019"
_4images_image_id: "36936"
_4images_cat_id: "2745"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36936 -->
