---
layout: "image"
title: "Luft-Flitzer von Nele&Noah OBEN (Motor an)"
date: "2013-05-26T09:50:17"
picture: "turboluftflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Nele & Noah"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36932
- /detailsdedd-3.html
imported:
- "2019"
_4images_image_id: "36932"
_4images_cat_id: "2745"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36932 -->
