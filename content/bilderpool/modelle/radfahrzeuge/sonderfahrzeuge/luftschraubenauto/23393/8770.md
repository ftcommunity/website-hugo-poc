---
layout: "comment"
hidden: true
title: "8770"
date: "2009-03-12T20:44:57"
uploadBy:
- "nula"
license: "unknown"
imported:
- "2019"
---
Die Kamera gibts [hier](http://tinyurl.com/bwbg39).

Kostet nur 35€ mit Empfänger und allem.

2,4Ghz
Scharfstellrädchen
380 TVL
1 Lux
Mikrofon
braucht 9 Volt, ist also ft-kompatibel.
Größe: 20 Cent Münze

Die Funktechnik ist komplett eingebaut in der Kamera.

Die Auflösung ist für solche Spaßzwecke ausreichend, es braucht nur viel Licht da das Bild sonst grün und grisselig wird.

Für 35€ ist es ein schöner Spaß :-)