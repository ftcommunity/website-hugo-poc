---
layout: "image"
title: "Auto mit Kompressor (Größenvergleich)"
date: "2010-01-15T20:38:44"
picture: "automitkompressor1.jpg"
weight: "1"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26097
- /details33ac.html
imported:
- "2019"
_4images_image_id: "26097"
_4images_cat_id: "1847"
_4images_user_id: "1057"
_4images_image_date: "2010-01-15T20:38:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26097 -->
Ein Auto mit Kompressor und Druckluftabschaltung und 20:1 Motor