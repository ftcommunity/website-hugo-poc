---
layout: "image"
title: "Baumstammgreifer 02"
date: "2012-12-01T18:27:05"
picture: "greifer2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36245
- /details4417.html
imported:
- "2019"
_4images_image_id: "36245"
_4images_cat_id: "2689"
_4images_user_id: "453"
_4images_image_date: "2012-12-01T18:27:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36245 -->
Lampen
