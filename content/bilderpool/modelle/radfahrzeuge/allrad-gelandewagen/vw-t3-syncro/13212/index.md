---
layout: "image"
title: "VW T3 Syncro 1"
date: "2008-01-03T20:24:22"
picture: "VW_T3_Syncro_02.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/13212
- /detailsc338.html
imported:
- "2019"
_4images_image_id: "13212"
_4images_cat_id: "1194"
_4images_user_id: "328"
_4images_image_date: "2008-01-03T20:24:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13212 -->
Hier mal wieder Experimente mit einem kleinen kompakten Drehschemel-Allrad. Angetrieben wird der Bus durch einen Power-Motor über 3 Differenziale, gelenkt wird per Mini-Motor, und der Akku ist auch mit an Bord. Ziel waren MAXIMALE Geländetauglichkeit, Kraft und Stabilität auf kleinstmöglichem Raum!

Aufgrund der Gewichtsverteilung musste der Akku nach vorn, so dass die (zunächst als PKW mit flacher Haube geplante) Karosserie ein Bus wurde. Klar ist dieser VW-Bus nicht maßstabsgetreu, aber ich finde, er sieht ganz nett aus. Hinten ist sogar noch viel Platz für den IR-Empfänger (den ich nicht habe) oder andere Nutzlast. Auf dem Dach habe ich hinten – wie beim echten T3 möglich – einen großen Gepäckträger installiert.

Die Bodenfreiheit ist nicht so die Wucht, denn der Antrieb sollte so flach wie möglich bauen und so tief wie möglich angeordnet werden. Aber durch den Verzicht auf Kardangelenke und andere „Schwachstellen“ ist das auf die Räder übertragene Drehmoment enorm, und der Bus wühlt sich wie selbstverständlich überall hoch und durch. In dieser kompakten Größe dürfte dieser Bus das geländegängigste Rad-Fahrzeug sein, dass mit Fischertechnik möglich ist!

Die Hinterachse hatte ich zunächst als Pendelachse aufgebaut, um die Geländetauglichkeit noch weiter zu steigern, aber beim Beschleunigen und im Gelände nahm die seitliche Kippneigung stark zu, so dass ich wieder darauf verzichtet habe.