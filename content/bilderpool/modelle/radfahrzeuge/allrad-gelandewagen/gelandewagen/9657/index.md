---
layout: "image"
title: "Geländewagen 4"
date: "2007-03-23T19:26:05"
picture: "gelaendewagen04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9657
- /detailsd05d.html
imported:
- "2019"
_4images_image_id: "9657"
_4images_cat_id: "878"
_4images_user_id: "502"
_4images_image_date: "2007-03-23T19:26:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9657 -->
Hier die Lenkung die per Seilzug bewegt wird.
