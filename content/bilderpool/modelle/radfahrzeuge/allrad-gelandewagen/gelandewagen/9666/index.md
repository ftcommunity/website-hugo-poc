---
layout: "image"
title: "Geländewagen 13"
date: "2007-03-23T19:30:25"
picture: "gelaendewagen13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9666
- /detailsbf23-2.html
imported:
- "2019"
_4images_image_id: "9666"
_4images_cat_id: "878"
_4images_user_id: "502"
_4images_image_date: "2007-03-23T19:30:25"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9666 -->
Die Seilbefestigung.
