---
layout: "image"
title: "Auto mit zwei verschiedenen Motoren (hinten mit vergleich zu Kitt)"
date: "2010-01-17T13:21:26"
picture: "zweimotoren1.jpg"
weight: "1"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26106
- /details8926.html
imported:
- "2019"
_4images_image_id: "26106"
_4images_cat_id: "1849"
_4images_user_id: "1057"
_4images_image_date: "2010-01-17T13:21:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26106 -->
die Räder sind cirka so groß wie Kitt