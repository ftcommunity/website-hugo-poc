---
layout: "comment"
hidden: true
title: "3579"
date: "2007-06-28T20:13:57"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Ja Untersetzt wäre das sicher stärker, aber dann ist es auch sehr langsam. Eigentlich baue ich ja immer geländefahrzeuge, aber hier, das sollte eher für die Straße sein. Der Allradantrieb ist halt eher zum Spaß, aber Fahrzeuge ohne Allrad, an denen kann ich einfach nicht so recht Gefallen finden ;-) Mal schauen ich werde es sicher noch umbauen. Davon wird es natürlich auch Fotos geben.

Gruß fitec