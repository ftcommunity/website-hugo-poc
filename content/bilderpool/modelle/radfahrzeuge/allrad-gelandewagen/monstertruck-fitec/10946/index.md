---
layout: "image"
title: "Antrieb"
date: "2007-06-27T18:35:09"
picture: "Monstertruck7.jpg"
weight: "7"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10946
- /details8d7f.html
imported:
- "2019"
_4images_image_id: "10946"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:35:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10946 -->
Der Antrien des Zentraldifferenzials durch einen 20:1 Power-Motor.
