---
layout: "image"
title: "Unteransicht"
date: "2007-06-27T18:35:09"
picture: "Monstertruck6.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10945
- /detailsfe0b-2.html
imported:
- "2019"
_4images_image_id: "10945"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:35:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10945 -->
Ansicht von unten. Man sieht die Vorder- und Hinterachsen, die Kardanwellen, sowie das Zentraldifferenzial mit 20:1 Power-Motor als Antrieb.
