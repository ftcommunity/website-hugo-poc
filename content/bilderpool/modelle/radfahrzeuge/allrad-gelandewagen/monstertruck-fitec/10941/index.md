---
layout: "image"
title: "Frontantrieb"
date: "2007-06-27T18:34:59"
picture: "Monstertruck2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10941
- /detailsce4a.html
imported:
- "2019"
_4images_image_id: "10941"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10941 -->
Hier sieht man den Frontantrieb.
