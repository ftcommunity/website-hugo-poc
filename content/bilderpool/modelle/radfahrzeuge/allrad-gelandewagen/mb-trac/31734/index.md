---
layout: "image"
title: "MB-Trac"
date: "2011-08-31T21:22:46"
picture: "MB-Trac_003.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/31734
- /detailsaf76.html
imported:
- "2019"
_4images_image_id: "31734"
_4images_cat_id: "2365"
_4images_user_id: "22"
_4images_image_date: "2011-08-31T21:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31734 -->
MB-Trac 
Eine Antrieb mit ein Z10 und ein Innenzahnrad gegen die Reifen funtioniert auch immer gut.
