---
layout: "image"
title: "Moster-Truck Chassis 23"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis5_2.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12210
- /details9ab2-2.html
imported:
- "2019"
_4images_image_id: "12210"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12210 -->
