---
layout: "image"
title: "Moster-Truck Chassis 20"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis2_2.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12207
- /detailsef43.html
imported:
- "2019"
_4images_image_id: "12207"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12207 -->
