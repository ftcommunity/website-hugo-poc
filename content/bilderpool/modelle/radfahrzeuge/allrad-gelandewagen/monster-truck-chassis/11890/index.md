---
layout: "image"
title: "Moster-Truck Chassis 13"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis05.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11890
- /details92a9-2.html
imported:
- "2019"
_4images_image_id: "11890"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11890 -->
