---
layout: "image"
title: "Moster-Truck Chassis 19"
date: "2007-10-14T15:54:17"
picture: "mostertruckchassis1_2.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12206
- /detailsa62d.html
imported:
- "2019"
_4images_image_id: "12206"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12206 -->
Hab wieder den Monster-Truck gebaut, nur diesmal mit Allrad. Insgesamt sind 4 Power-Motoren für den Antrieb der Räder verbaut, für jedes Rad einer!! Dadurch dass jeweils 2 Motoren in Reihe geschalten sind, fährt er ziemlich langsam aber mit ordentlich Schmackes :-)