---
layout: "image"
title: "Moster-Truck Chassis 11"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis03.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/11888
- /detailsc462-2.html
imported:
- "2019"
_4images_image_id: "11888"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11888 -->
Mit allen Rädern kommt er gerade noch hoch.
