---
layout: "image"
title: "Monster-Truck Allrad 15"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/17154
- /detailsd80d.html
imported:
- "2019"
_4images_image_id: "17154"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17154 -->
