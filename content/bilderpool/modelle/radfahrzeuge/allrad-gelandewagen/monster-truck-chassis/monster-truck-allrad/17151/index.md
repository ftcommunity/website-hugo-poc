---
layout: "image"
title: "Monster-Truck Allrad 12"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/17151
- /details1b2c-2.html
imported:
- "2019"
_4images_image_id: "17151"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17151 -->
