---
layout: "image"
title: "Monster-Truck Allrad 16"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/17155
- /details198f-2.html
imported:
- "2019"
_4images_image_id: "17155"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17155 -->
