---
layout: "image"
title: "Moster-Truck Chassis 27"
date: "2007-11-03T22:08:56"
picture: "mostertruckchassis1_3.jpg"
weight: "27"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12384
- /details1887-4.html
imported:
- "2019"
_4images_image_id: "12384"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-11-03T22:08:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12384 -->
Hier die "endgültige" Version. Der Allradantrieb hat sich nicht durchgesetzt da er zu langsam war. Hab wieder den alten Antrieb genommen aber diesmal mit ein Z40 am Reifen.
