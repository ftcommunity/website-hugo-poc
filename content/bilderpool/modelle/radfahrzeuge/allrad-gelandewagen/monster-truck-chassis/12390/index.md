---
layout: "image"
title: "Moster-Truck Chassis 33"
date: "2007-11-03T22:08:57"
picture: "mostertruckchassis7_3.jpg"
weight: "33"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12390
- /details7ec9-3.html
imported:
- "2019"
_4images_image_id: "12390"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-11-03T22:08:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12390 -->
