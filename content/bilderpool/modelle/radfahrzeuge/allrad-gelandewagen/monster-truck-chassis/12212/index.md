---
layout: "image"
title: "Moster-Truck Chassis 25"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis7_2.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12212
- /detailscf43.html
imported:
- "2019"
_4images_image_id: "12212"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12212 -->
