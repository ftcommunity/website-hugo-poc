---
layout: "image"
title: "Moster-Truck Chassis 22"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis4_2.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12209
- /detailscf45.html
imported:
- "2019"
_4images_image_id: "12209"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12209 -->
