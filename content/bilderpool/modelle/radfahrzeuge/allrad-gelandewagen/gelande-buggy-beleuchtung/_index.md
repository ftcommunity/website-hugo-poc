---
layout: "overview"
title: "Gelände-Buggy mit Beleuchtung"
date: 2020-02-22T07:54:33+01:00
legacy_id:
- /php/categories/682
- /categories792d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=682 --> 
8 Leuchtbausteine wurden dem Fahrzeug hinzugefügt. Da es keine Befestigungspunkte gab, habe ich vorne eine Art \"Motorraum\" und hinten eine Art \"Kofferraum\" angebracht. Leuchtbausteine sind parallel geschaltet, daher der \"Kabelwust\".