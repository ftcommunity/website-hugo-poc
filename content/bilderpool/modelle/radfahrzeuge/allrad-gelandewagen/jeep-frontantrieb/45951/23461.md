---
layout: "comment"
hidden: true
title: "23461"
date: "2017-06-18T12:55:57"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Die Lenkklaue als federnde Hinterradaufhängung  habe ich auch noch nicht gesehen, vermutlich weil es im Bilderpool (fast) keine Fahrzeuge mit reinem Vorderradantrieb gibt.

Klasse gemacht!
David