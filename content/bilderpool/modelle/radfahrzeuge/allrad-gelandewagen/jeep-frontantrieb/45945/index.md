---
layout: "image"
title: "Heckansicht"
date: "2017-06-18T11:55:32"
picture: "jeepfa3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45945
- /details5226-2.html
imported:
- "2019"
_4images_image_id: "45945"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45945 -->
Dank butterweicher Federung merkt man nicht so viel vom leicht verwindbaren Rahmen.
