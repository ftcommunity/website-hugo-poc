---
layout: "image"
title: "Antrieb von oben"
date: "2017-06-18T11:55:32"
picture: "jeepfa5.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45947
- /detailsc85f.html
imported:
- "2019"
_4images_image_id: "45947"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45947 -->
Das Differenzial liegt zwei Etagen über den Radachsen. Der Motor auch. Über die Z20 + darunter liegende Z10 wird der Antrieb zuerst eine Etage nach unten geleitet und dann nach außen auf die weißen Kegelzahnräder (die stammen aus einem schwarzen ft-Differenzial) auf die "alten" Reifen 60, die am Außenrand die passende Verzahnung aufweisen. Links kann man eins der weißen Kegelräder durch die Löcher in der tragenden Lasche erahnen.
