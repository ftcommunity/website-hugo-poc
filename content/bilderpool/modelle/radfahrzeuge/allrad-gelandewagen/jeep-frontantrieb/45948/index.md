---
layout: "image"
title: "Antrieb von unten"
date: "2017-06-18T11:55:32"
picture: "jeepfa6.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45948
- /detailsb8cc.html
imported:
- "2019"
_4images_image_id: "45948"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45948 -->
Von hier sind die weißen Kegelzahnräder direkt sichtbar. Mit den Zahnradkombinationen kann man noch "spielen: es passen Z20 / Z10; Z15 / Z15 und Z10 / Z20 da hinein (letzteres mit etwas Umbau); zusammen mit den Motorvarianten (grau oder schwarz) kann man dem Fahrzeug mehr Ackergaul- oder Rennpferd-Qualitäten verpassen.

An den Radaufhängungen sind zwei gemoddete Teile verbaut, und ohne die ist hier nichts zu machen: ein BS7,5 ist quer zu den Nuten durchgesägt, und die beiden halben BS7,5 verstecken sich jeweils außerhalb von einem "ganzen" BS7,5 und halten jetzt die Lenkhebel an den Rädern fest.
