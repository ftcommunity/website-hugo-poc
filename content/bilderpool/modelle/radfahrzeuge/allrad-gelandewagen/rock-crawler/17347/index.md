---
layout: "image"
title: "Rock Crawler 7"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_07_klein.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17347
- /detailsfaa5.html
imported:
- "2019"
_4images_image_id: "17347"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17347 -->
