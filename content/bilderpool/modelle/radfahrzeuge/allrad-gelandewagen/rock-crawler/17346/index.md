---
layout: "image"
title: "Rock Crawler 6"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_06_klein.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17346
- /details4f19-2.html
imported:
- "2019"
_4images_image_id: "17346"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17346 -->
Hier sieht man die extreme Verschränkung der Achsen. Diesen Fahrzustand hat das Fahrzeug - wie auf allen Bildern - selbst aus eigener Kraft erreicht!