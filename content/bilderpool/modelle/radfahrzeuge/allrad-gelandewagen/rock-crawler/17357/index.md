---
layout: "image"
title: "Rock Crawler 17"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_17_klein.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17357
- /details0270-2.html
imported:
- "2019"
_4images_image_id: "17357"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17357 -->
Blick auf die Vorderachse von vorn/unten. Es ist deutlich zu sehen, dass ich JEDEN Freiheitsgrad der Steine blockiert habe, der nicht dem Antrieb der Räder dient. Die Achse ist sehr stabil; es rattert und springt nichts!