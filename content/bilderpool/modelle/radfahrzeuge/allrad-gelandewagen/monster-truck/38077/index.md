---
layout: "image"
title: "Detail zur Vorderachse"
date: "2014-01-17T00:49:35"
picture: "DetailFrontAxle.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38077
- /details3824-2.html
imported:
- "2019"
_4images_image_id: "38077"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T00:49:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38077 -->
wie bereits erwähnt, ist die Vorderachse eine Starrachse.
Die Radaufhängung (1) ist durch Verschränkung, Gegenläufigkeit der Nuten und durch Verschachtelung der Bausteine so konstruiert, daß sie sich eigentlich nicht von selbst lösen kann. Auch bei hohen Kräften verzieht sich die Aufhängung höchstens, es geht aber nichts auf.Es gibt einen Schlüsselstein (Pfeil), den muß man ziehen, um die Konstruktion wieder auseinandernehmen zu können.
Mit (2) habe ich die Achsaufhängung markiert.
Im realen Leben werden Starrachsen mit einem Panhardstab fixiert. Ich bin aber auf eine ganz nette andere Lösung gekommen. Ich verwende 90° Winkelstangen. Oben ist die Metallachse so fixiert, daß sie sich zwar seitlich drehen kann, aber sich nicht nach vorne/hinten neigen kann. Die andere Seite ist verschiebbar in einem Lochstein und führt damit die Starrachse. Damit kann die Achse nach oben und unten federn, auch einseitig (da die Aufhängung in dieser Richtung ja drehbar ist. In Verbindung mit den Federn funktioniert diese Lösung eigentlich sehr gut. 
(3) Normalerweise wird ein Stoßdämpfer drehbar gelagert. Das geht aber mit den ft Federn nicht, da diese keine Führung haben. Wenn Gelenke an beiden Seiten sind, dann weicht die Feder aus (biegt sich durch) und federt nicht
(4) Der Endschalter für die Lenkung (in diesem Bild nicht verdrahtet). Dieser begrenzt zur Zeit den maximalen Lenkeinschlag. Die Mechanik der Lenkung könnte noch einen etwas größeren Lenkeinschlag. Für die Schalter habe ich aber noch keine platzsparende Lösung gefunden