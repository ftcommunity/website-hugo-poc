---
layout: "image"
title: "Detail Motorhaube"
date: "2014-01-17T00:49:34"
picture: "Bonnet.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38076
- /details01c2-2.html
imported:
- "2019"
_4images_image_id: "38076"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T00:49:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38076 -->
ich fürchte, daß ich extra darauf hinweisen muß, daß diese Konstruktion einen BigBlock darstellen soll.
in echt ist so etwas natürlich viel schöner:
http://bilder.4ever.eu/tag/11226/big-block?pg=2