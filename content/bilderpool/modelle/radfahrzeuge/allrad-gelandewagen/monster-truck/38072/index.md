---
layout: "image"
title: "Rückansicht"
date: "2014-01-16T15:59:48"
picture: "RearView.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38072
- /detailsa571-2.html
imported:
- "2019"
_4images_image_id: "38072"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38072 -->
Die hintere Achse ist ebenfalls eine Starrachse. Hinter den Bausteinen in der Mitte ist das Differential versteckt.

Federung und Aufhängung ist ähnlich gelöst wie bei der Vorderachse. 
Das zeige ich später noch im Detail.