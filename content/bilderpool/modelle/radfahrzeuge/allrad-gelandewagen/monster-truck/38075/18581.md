---
layout: "comment"
hidden: true
title: "18581"
date: "2014-01-16T18:17:37"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
das ist ein guter Tipp! Daran habe ich bisher nicht gedacht. Das würde das Modell evtl. fahrfähig machen, auch wenn die Optik etwas darunter leidet.
Das werde ich als Zwischenlösung angehen, aber final werde ich den Antriebsstrang wohl mit einem Metall-Differential inklusive passendem Kegelzahnrad aus einem RC Modellauto ersetzen.