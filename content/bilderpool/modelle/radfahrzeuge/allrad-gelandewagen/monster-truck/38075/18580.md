---
layout: "comment"
hidden: true
title: "18580"
date: "2014-01-16T17:52:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Beim Antrieb gibt's imho mit ft nur eine Variante, die bei so großen Reifen noch funktioniert: Du musst *spät* untersetzen, z.b. durch Z40 direkt *am* Reifen und Z10, was die dann antreibt. Dann hast Du bis dahin eine höhere Drehzahl anstatt hohes Drehmoment.

Gruß,
Stefan