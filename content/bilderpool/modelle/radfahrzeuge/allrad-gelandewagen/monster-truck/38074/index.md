---
layout: "image"
title: "Chassis von oben"
date: "2014-01-16T15:59:48"
picture: "ChassisTopView.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38074
- /detailscb8b.html
imported:
- "2019"
_4images_image_id: "38074"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38074 -->
Ansicht von oben nach Entfernen von Karosserie, Remote Control und Akkus.