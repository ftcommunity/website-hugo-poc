---
layout: "image"
title: "CAD-Modell"
date: "2014-12-21T09:02:59"
picture: "CAD_Modell_DxO.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39950
- /details81de.html
imported:
- "2019"
_4images_image_id: "39950"
_4images_cat_id: "2999"
_4images_user_id: "502"
_4images_image_date: "2014-12-21T09:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39950 -->
