---
layout: "image"
title: "Allrad mit Knicklenkung 7"
date: "2014-12-13T19:42:17"
picture: "DSCN1887_DxO.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/39928
- /detailsd240.html
imported:
- "2019"
_4images_image_id: "39928"
_4images_cat_id: "2999"
_4images_user_id: "502"
_4images_image_date: "2014-12-13T19:42:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39928 -->
