---
layout: "image"
title: "Schalter"
date: "2007-07-01T12:32:57"
picture: "PICT0021.jpg"
weight: "6"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/11012
- /detailsd083.html
imported:
- "2019"
_4images_image_id: "11012"
_4images_cat_id: "996"
_4images_user_id: "590"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11012 -->
Ich hab noch einen Schalter eingebaut um den Empfänger ein und aus zuschalten,das man nicht immer mit den Steckern rumm ziehen muss.
