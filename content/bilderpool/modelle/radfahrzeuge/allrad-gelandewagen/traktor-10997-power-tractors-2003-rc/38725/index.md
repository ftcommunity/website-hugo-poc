---
layout: "image"
title: "Gesamtansicht"
date: "2014-04-27T20:38:31"
picture: "IMG_0008.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38725
- /detailsa19e-2.html
imported:
- "2019"
_4images_image_id: "38725"
_4images_cat_id: "2891"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T20:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38725 -->
