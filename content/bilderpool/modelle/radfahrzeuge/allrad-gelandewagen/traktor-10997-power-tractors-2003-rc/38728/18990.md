---
layout: "comment"
hidden: true
title: "18990"
date: "2014-04-30T09:20:25"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Moin,

solche Regler mit Bremsfunktion, allerdings von einer anderen Firma (ich meine Graupner), hatte ich auch schon mal.
Ich habe dann auf die Nutzung verzichtet weil sie mir nicht genug geeignet schienen.
Weil:
Der Regler mit Bremse ist wirklich nur für einen Antrieb geeignet.
Zum Lenken kann man ihn nicht nutzen.
Als Raupenantrieb (2 Stück davon) war er mir auch nicht genug geeignet.
Ich habe dann auf den Einbau und Betrieb verzichtet.

Damals habe ich mir, so meine ich, 3 oder 4 Stück gekauft.
Falls jemand Interesse daran haben sollte .....
.... einfach mal melden.

Gruß ludger