---
layout: "overview"
title: "Jeeps mit Infrarot- und Funk-Fernbedienung"
date: 2020-02-22T07:56:31+01:00
legacy_id:
- /php/categories/3494
- /categoriesba3d.html
- /categories6e36.html
- /categories038f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3494 --> 
Zwei leicht umgebaute Jeeps aus dem Baukasten "Advanced Automobile" mit zwei unterschiedlichen, selbst gebauten Fernbedienungen auf Basis eines Arduino-Nano-Boards:
- einmal mit Funkfernbedienung (NRF24, mit Eigenbau-Handsender)
- zum anderen mit Infrarot-Fernbedienung (passend zur FT-Fernbedienung aus 500881) 
- beide mit Motorcontroller MX1508 (PCB für 0,60 Euro aus China) für jeweils 2 Motoren
- beide mit Steuerung für je 2 Servos