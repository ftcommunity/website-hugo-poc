---
layout: "image"
title: "Modifikation der Bodenbleche und der Sitzhalterung"
date: "2018-01-30T16:23:37"
picture: "J8.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47220
- /detailsa94e-2.html
imported:
- "2019"
_4images_image_id: "47220"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47220 -->
Nun haben zwei Personen Platz im Jeep