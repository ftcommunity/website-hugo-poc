---
layout: "image"
title: "Stapelaufbau von der Seite"
date: "2018-01-30T16:23:43"
picture: "J11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Servo", "Anschluss"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47223
- /detailsd694.html
imported:
- "2019"
_4images_image_id: "47223"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47223 -->
Man sieht die beiden Servoanschlüsse