---
layout: "image"
title: "Ansicht beider Jeeps von oben"
date: "2018-01-30T16:23:37"
picture: "J1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Jeep", "Infrarot", "Funk", "Fernbedienung", "Arduino", "Nano"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47213
- /detailsbcfe.html
imported:
- "2019"
_4images_image_id: "47213"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47213 -->
Betrieb mit leichten 2s-LiPo Akkus. Man kann gut Autorennen gegeneinander fahren, da sich die beiden Fernbedienungen nicht gegenseitig stören.