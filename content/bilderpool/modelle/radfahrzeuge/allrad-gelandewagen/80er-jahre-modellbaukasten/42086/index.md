---
layout: "image"
title: "Universalfahrzeug von schräg vorn"
date: "2015-10-18T15:42:43"
picture: "universalfahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42086
- /details340c-2.html
imported:
- "2019"
_4images_image_id: "42086"
_4images_cat_id: "3132"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42086 -->
mit ausgefahrener Bühne, ergänzt durch grüne Lampen (Sohn Hannes  seine Lieblingsfarbe ist halt grün) :-)