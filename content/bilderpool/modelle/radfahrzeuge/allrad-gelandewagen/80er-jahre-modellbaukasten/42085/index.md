---
layout: "image"
title: "Universalfahrzeug 30481 (1984)"
date: "2015-10-18T15:42:43"
picture: "universalfahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42085
- /detailse5a5.html
imported:
- "2019"
_4images_image_id: "42085"
_4images_cat_id: "3132"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42085 -->
Hier ein Originalmodell Universalfahrzeug (30481, 1984) nach Bauplan gebaut. Das Universalfahrzeug war ein Kasten mit Fahrzeugteilen, konzipiert als kompletter Modellbaukasten.
Die Idee war, das Fahrzeug als Grundaufbau zu betrachten und durch verschiedene Aufbauten (hier eine Hebebühne) mittels der Grundkästen (z.B. Start 100, 30141) oder Erweiterungskästen (Statik 30149, Motor-Set 39119  zu erweitern / modifizieren.

einige Sonderteile wie z.B.: 
Drehkranz Z58 (31393, 1983)
Seilwinde (31195, 1983)
Aufliegerkupplung (31263+31264, 1983)
lagen schon dem Kasten bei, damit man mit weiteren vorhanden Teilen schicke Modelle bauen konnte.
Besonders  schön zu kombinieren war der Grundaufbau auch mit anderen Kästen, z.B. dem Kompressoranhänger (30458, 1984) oder Hydraulik-Kran (30441, 1984).
Dies wurde teilweise in der Anleitung schon beschrieben.

hier ein paar Links in unsere Datenbank, dort findet man auch entsprechende Bauanleitungen:

Universalfahrzeug (30481, 1984)
http://ft-datenbank.de/details.php?ArticleVariantId=457bbaee-42da-42b9-b81e-64a23467ebfe
Kompressoranhänger (30458, 1984)
http://ft-datenbank.de/details.php?ArticleVariantId=706ad4cb-9996-4deb-88de-19e114156b54
Hydraulik-Kran (30441, 1984)
http://ft-datenbank.de/details.php?ArticleVariantId=929d92f3-fb88-4580-b96c-07c704b43eec
Aufliegerkupplung (31263+31264, 1983)
http://ft-datenbank.de/details.php?ArticleVariantId=6d39cda1-fa8e-41c5-9295-6b7430577063
Seilwinde (31195, 1983)
http://ft-datenbank.de/details.php?ArticleVariantId=e03c9c16-7669-4287-bb5b-610fe359f1c5
Drehkranz Z58 (31393, 1983)
http://ft-datenbank.de/details.php?ArticleVariantId=23992546-4ad0-435e-b619-c7b057d8c09a

