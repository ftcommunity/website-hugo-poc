---
layout: "image"
title: "Die Lenkung"
date: "2014-04-27T16:09:00"
picture: "unimog14.jpg"
weight: "14"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38681
- /details1883-2.html
imported:
- "2019"
_4images_image_id: "38681"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38681 -->
