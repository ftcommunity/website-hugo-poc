---
layout: "image"
title: "Der Kran"
date: "2014-04-27T16:09:00"
picture: "unimog05.jpg"
weight: "5"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38672
- /details4940.html
imported:
- "2019"
_4images_image_id: "38672"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38672 -->
...hier eingeklappt.