---
layout: "image"
title: "Die Seilwinde"
date: "2014-04-27T16:09:00"
picture: "unimog15.jpg"
weight: "15"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38682
- /details9ac3.html
imported:
- "2019"
_4images_image_id: "38682"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38682 -->
Hiter der Rolle sieht man den Motor.