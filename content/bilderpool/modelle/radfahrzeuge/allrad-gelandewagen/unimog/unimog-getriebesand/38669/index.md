---
layout: "image"
title: "Von vorne"
date: "2014-04-27T16:09:00"
picture: "unimog02.jpg"
weight: "2"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38669
- /detailsdf1c.html
imported:
- "2019"
_4images_image_id: "38669"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38669 -->
Im Führerhaus sieht man die Elektrik/Pneumatik, unten die Seilwinde.