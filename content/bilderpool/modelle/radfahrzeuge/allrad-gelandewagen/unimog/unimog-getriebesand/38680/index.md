---
layout: "image"
title: "Ohne Karosserie"
date: "2014-04-27T16:09:00"
picture: "unimog13.jpg"
weight: "13"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- /php/details/38680
- /details9882-2.html
imported:
- "2019"
_4images_image_id: "38680"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38680 -->
Hier sieht man die Technik gut: Oben ist der S-Motor, der den Krahn dreht. Links das Lenkservo, rechts das Schaltgetriebe mit Powermotor.
Der XS-Motor Verschiebt den Motor.