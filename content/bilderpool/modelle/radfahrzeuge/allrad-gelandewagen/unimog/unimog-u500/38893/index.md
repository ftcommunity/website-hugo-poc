---
layout: "image"
title: "Unimog U500"
date: "2014-06-01T15:26:33"
picture: "Foto_6.jpg"
weight: "7"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38893
- /detailsba0e-2.html
imported:
- "2019"
_4images_image_id: "38893"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-06-01T15:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38893 -->
