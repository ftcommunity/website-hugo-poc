---
layout: "comment"
hidden: true
title: "19109"
date: "2014-06-04T12:04:53"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hä? Beim Lenken verspannt sich doch alles? Der Spurstange fehlt doch ein Gelenk. Oder ist alles bewusst "locker" ausgelegt, um derartige Verachiebungen zuzulassen?

Gruß, Thomas