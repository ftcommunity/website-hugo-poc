---
layout: "image"
title: "Unimog U500, Vorderachse Detail"
date: "2014-06-01T18:57:44"
picture: "Foto_10.jpg"
weight: "15"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38901
- /detailsa1dd-3.html
imported:
- "2019"
_4images_image_id: "38901"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-06-01T18:57:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38901 -->
Wie bei der Hinterachse, mussten auch hier nicht ft Kardanwellen verwendet werden, sowie 2 Federn pro Rad.