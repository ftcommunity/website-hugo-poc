---
layout: "image"
title: "Unimog U500 im Gelände"
date: "2014-05-31T22:28:35"
picture: "Bild_003.jpg"
weight: "1"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38887
- /details027e.html
imported:
- "2019"
_4images_image_id: "38887"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-05-31T22:28:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38887 -->
Man sieht, er ist voll geländegängig.