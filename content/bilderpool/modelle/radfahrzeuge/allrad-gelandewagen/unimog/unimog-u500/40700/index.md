---
layout: "image"
title: "Die Rückseite"
date: "2015-04-03T10:00:06"
picture: "Foto_21.jpg"
weight: "22"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/40700
- /details13b6-3.html
imported:
- "2019"
_4images_image_id: "40700"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40700 -->
Beide Räder werden getrennt mittels einer Rastachse angetrieben.