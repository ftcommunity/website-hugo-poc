---
layout: "image"
title: "Unimog 06"
date: "2013-09-29T13:29:22"
picture: "DSCN0913.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/37439
- /details921a.html
imported:
- "2019"
_4images_image_id: "37439"
_4images_cat_id: "2783"
_4images_user_id: "502"
_4images_image_date: "2013-09-29T13:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37439 -->
