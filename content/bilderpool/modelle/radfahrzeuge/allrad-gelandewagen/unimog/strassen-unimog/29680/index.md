---
layout: "image"
title: "Frontansicht"
date: "2011-01-14T13:47:47"
picture: "strassenunimog2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- /php/details/29680
- /details5029.html
imported:
- "2019"
_4images_image_id: "29680"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29680 -->
Das Lenkrad ist direkt mit der Lenkung verbunden.