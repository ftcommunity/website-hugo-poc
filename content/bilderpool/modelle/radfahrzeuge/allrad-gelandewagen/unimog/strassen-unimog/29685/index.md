---
layout: "image"
title: "Oben"
date: "2011-01-14T13:47:47"
picture: "strassenunimog7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- /php/details/29685
- /detailsbac0.html
imported:
- "2019"
_4images_image_id: "29685"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29685 -->
Das ganze nochmal von oben.