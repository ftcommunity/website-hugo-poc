---
layout: "image"
title: "Kabine"
date: "2011-01-14T13:47:47"
picture: "strassenunimog4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- /php/details/29682
- /details3c3d.html
imported:
- "2019"
_4images_image_id: "29682"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29682 -->
Sobald der Schalter nach hinten gelegt wird, fährt der Unimog los.