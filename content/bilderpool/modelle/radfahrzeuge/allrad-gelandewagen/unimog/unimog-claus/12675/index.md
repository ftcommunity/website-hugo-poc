---
layout: "image"
title: "modellevonclauswludwig15.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig15.jpg"
weight: "30"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12675
- /details5c53-2.html
imported:
- "2019"
_4images_image_id: "12675"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12675 -->
