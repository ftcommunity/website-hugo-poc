---
layout: "image"
title: "cu049.JPG"
date: "2007-09-21T20:19:04"
picture: "cu049.JPG"
weight: "9"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11885
- /details7706-2.html
imported:
- "2019"
_4images_image_id: "11885"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11885 -->
Unter der vorderen Stoßstange gibt es 
- zwei Steckdosen für Anbaugeräte, 
- einen Motor mit Hubgetriebe (im Moment flach eingeklappt), 
- einen Druckluftanschluss (der Kompressor sitzt zwischen den linken Rädern in der roten Box mit Klarsichtdeckel).
Über der Stoßstange:
- zwei Zapfwellen
