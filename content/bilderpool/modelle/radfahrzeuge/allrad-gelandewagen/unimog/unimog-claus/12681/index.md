---
layout: "image"
title: "modellevonclauswludwig21.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig21.jpg"
weight: "36"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12681
- /detailsebbe-2.html
imported:
- "2019"
_4images_image_id: "12681"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12681 -->
