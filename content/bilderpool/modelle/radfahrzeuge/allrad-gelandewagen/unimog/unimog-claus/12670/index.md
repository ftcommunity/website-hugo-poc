---
layout: "image"
title: "modellevonclauswludwig10.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig10.jpg"
weight: "25"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12670
- /detailsdfaf.html
imported:
- "2019"
_4images_image_id: "12670"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12670 -->
