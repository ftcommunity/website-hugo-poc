---
layout: "image"
title: "modellevonclauswludwig18.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig18.jpg"
weight: "33"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12678
- /detailsaa5c.html
imported:
- "2019"
_4images_image_id: "12678"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12678 -->
