---
layout: "image"
title: "Unimog Front"
date: "2007-09-18T11:26:25"
picture: "PICT5724.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11825
- /details1c35.html
imported:
- "2019"
_4images_image_id: "11825"
_4images_cat_id: "1065"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:26:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11825 -->
Man beachte: Wenn man die Kupplung abmontiert, kann man einen Ferngesteuerten S-Motor mit Hubgetriebe herausdrehen, um dort etwa eine Schaufel anzubringen.