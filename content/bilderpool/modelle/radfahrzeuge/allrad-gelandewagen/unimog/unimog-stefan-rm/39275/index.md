---
layout: "image"
title: "Lenkung-2"
date: "2014-08-24T10:52:09"
picture: "unimog13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39275
- /details87a2.html
imported:
- "2019"
_4images_image_id: "39275"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39275 -->
Hier schön zu sehen ist der Lenkzylinder und die Federung. Insgesamt ist die Lenkung recht verwindungssteif.