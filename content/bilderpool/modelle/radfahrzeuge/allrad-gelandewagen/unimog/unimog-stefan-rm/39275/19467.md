---
layout: "comment"
hidden: true
title: "19467"
date: "2014-08-29T19:49:55"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Es müsste irgendwie gehen, aber dann schlagen die Räder bei jedem Einfedern der Vorderachse ein, da die Schnecke ja nicht genau senkrecht über der Drehachse der Vorderachse liegt(was vorallem bei so großem Federweg sicherlich zu beachten ist).Das hatt dann wahrscheindlich ein instabiles Fahrverhalten zur Folge hat... und deswegen ist mir dann nur noch die Lenkung via Pneumatikzylinder eingefallen.

Schöne Grüße,
Stefan