---
layout: "image"
title: "linke Seite"
date: "2014-08-24T10:52:09"
picture: "unimog06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39268
- /details7008-2.html
imported:
- "2019"
_4images_image_id: "39268"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39268 -->
mit geschlossener Tür