---
layout: "image"
title: "Antrieb"
date: "2014-08-24T10:52:09"
picture: "unimog10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39272
- /details5707-3.html
imported:
- "2019"
_4images_image_id: "39272"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39272 -->
Hier zu sehen der XM-Motor mit Getriebe zum Mitteldifferential.