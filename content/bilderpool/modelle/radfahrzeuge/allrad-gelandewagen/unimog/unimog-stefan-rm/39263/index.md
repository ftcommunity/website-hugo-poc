---
layout: "image"
title: "Gesamtansicht-1"
date: "2014-08-24T10:52:09"
picture: "unimog01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39263
- /details102d.html
imported:
- "2019"
_4images_image_id: "39263"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39263 -->
Der Unimog verfügt über Allradantrieb, gefederte Pendelachse vorne und hinten, wobei die Hinterachse eine Portalachse ist.Die Lenkung funktioniert mit zwei durch Schleuche miteinander verbundene Pneumatikzylinder, von denen einer mit einer Schnecke (Angetrieben von einem Minimotor in der Front des Fahrzeugs) ein und ausfahrbar ist.
Er schafft vorwärts und rückwärts eine Steigung von ca. 30°, dann geben die Kardangelenke in der Lenkung auf.
Hier sieht man auch schön den Empfänger in der Motorhaube.