---
layout: "image"
title: "von oben"
date: "2014-08-24T10:52:09"
picture: "unimog04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/39266
- /detailsb5ae.html
imported:
- "2019"
_4images_image_id: "39266"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39266 -->
