---
layout: "image"
title: "Unimog 20"
date: "2007-01-02T14:58:38"
picture: "unimog20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8280
- /details17b3-2.html
imported:
- "2019"
_4images_image_id: "8280"
_4images_cat_id: "763"
_4images_user_id: "502"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8280 -->
