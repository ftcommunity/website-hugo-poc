---
layout: "image"
title: "Unimog 6"
date: "2006-12-29T22:04:52"
picture: "unimogfastfertig6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8193
- /details6b08.html
imported:
- "2019"
_4images_image_id: "8193"
_4images_cat_id: "756"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T22:04:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8193 -->
