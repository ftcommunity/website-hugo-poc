---
layout: "image"
title: "Unimog 3"
date: "2007-01-02T14:58:38"
picture: "unimog03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8263
- /detailsc6d0-2.html
imported:
- "2019"
_4images_image_id: "8263"
_4images_cat_id: "763"
_4images_user_id: "502"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8263 -->
Der Seilzug für den Kipper.
