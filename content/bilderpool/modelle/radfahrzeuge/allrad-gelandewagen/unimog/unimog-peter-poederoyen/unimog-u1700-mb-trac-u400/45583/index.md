---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T16:43:41"
picture: "eisenbahndraisineunimogfuerrangierarbeitenb1.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45583
- /detailsb8c5.html
imported:
- "2019"
_4images_image_id: "45583"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T16:43:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45583 -->
