---
layout: "image"
title: "Eisenbahn-Draisine"
date: "2017-03-19T16:36:53"
picture: "unimogfuerrangierarbeiteneisenbahndraisine14.jpg"
weight: "36"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45578
- /details6105.html
imported:
- "2019"
_4images_image_id: "45578"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T16:36:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45578 -->
Een draisine is een klein aangedreven railvoertuig, meestal te licht om voor het trekken van wagons benut te kunnen worden. Draisines worden gebruikt voor inspectieritten, om de spoorarbeiders naar de plaats van de spoorwerken te brengen en dergelijke. Het vervoermiddel werd uitgevonden door Karl von Drais.  De eerste draisines waren met spierkracht aangedreven vierwielige railvoertuigen. Ze werden gebruikt door onderhoudsmonteurs van de spoorwegen. Wat later kwamen draisines die door middel van pedalen werden voortgestuwd. Die noemt men ook wel spoorfiets.
Een draisine dient niet verward te worden met een lorrie, die laatste is namelijk niet aangedreven. Tegenwoordig worden bij de spoorwegen uitsluitend nog benzine- en diesel-draisines gebruikt.
