---
layout: "image"
title: "Radaufhängung -Details"
date: "2016-08-13T18:13:24"
picture: "radaufhaengungdetailsunimogu1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44299
- /details350f-3.html
imported:
- "2019"
_4images_image_id: "44299"
_4images_cat_id: "3268"
_4images_user_id: "22"
_4images_image_date: "2016-08-13T18:13:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44299 -->
Rechts oben ein grauer Baustein 15 mit Senkung + M4 Gewinde + feste Achse + Z10 ohne Antrieb-Funktion.
Untere Z10 dient zum Antrieb Innen-Z30 .
Gelenkwürfel-Zunge 31426 durch bohrt + M4 Befestigung
