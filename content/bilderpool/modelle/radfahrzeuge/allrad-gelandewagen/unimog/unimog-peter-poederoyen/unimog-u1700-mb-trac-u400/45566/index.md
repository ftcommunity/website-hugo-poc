---
layout: "image"
title: "Unimog 1400 als Zweiwegefahrzeug"
date: "2017-03-19T14:19:17"
picture: "unimogfuerrangierarbeiteneisenbahndraisine02.jpg"
weight: "24"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45566
- /details3e90.html
imported:
- "2019"
_4images_image_id: "45566"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T14:19:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45566 -->
Een Unimog 1400 trekt een werktrein tijdens de bouw van het viersporig treinviaduct door Leidsche Rijn. Deze Unimog is voorzien van zowel wielen met luchtbanden als spoorwieltjes met flenzen.
