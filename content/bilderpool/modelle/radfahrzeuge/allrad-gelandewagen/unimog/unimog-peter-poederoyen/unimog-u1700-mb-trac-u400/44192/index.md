---
layout: "image"
title: "U400"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu18.jpg"
weight: "18"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44192
- /detailsb991.html
imported:
- "2019"
_4images_image_id: "44192"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44192 -->
Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit .
