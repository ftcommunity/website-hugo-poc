---
layout: "image"
title: "Unimog Geschichte"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu03.jpg"
weight: "3"
konstrukteure: 
- "Gebr.Böhringer GMBH +  MB"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44177
- /details385a-2.html
imported:
- "2019"
_4images_image_id: "44177"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44177 -->
Link zum Unimog-Museum : 

http://www.unimog-museum.com/ 

Unimog-Museum Betriebs GmbH 
An der B 462 
Ausfahrt Schloss Rotenfels 
76571 Gaggenau
