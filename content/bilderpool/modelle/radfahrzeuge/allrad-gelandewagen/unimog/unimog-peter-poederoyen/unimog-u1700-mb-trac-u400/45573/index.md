---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T14:19:17"
picture: "unimogfuerrangierarbeiteneisenbahndraisine09.jpg"
weight: "31"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45573
- /details1a37-3.html
imported:
- "2019"
_4images_image_id: "45573"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T14:19:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45573 -->
Militair Anwendung
