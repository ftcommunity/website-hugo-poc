---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T16:36:53"
picture: "unimogfuerrangierarbeiteneisenbahndraisine11.jpg"
weight: "33"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45575
- /details6185.html
imported:
- "2019"
_4images_image_id: "45575"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T16:36:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45575 -->
Bahn-arbeit...
