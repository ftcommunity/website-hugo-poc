---
layout: "image"
title: "Radaufhängung -Details"
date: "2016-08-13T18:13:24"
picture: "radaufhaengungdetailsunimogu4.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44302
- /details1e84-2.html
imported:
- "2019"
_4images_image_id: "44302"
_4images_cat_id: "3268"
_4images_user_id: "22"
_4images_image_date: "2016-08-13T18:13:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44302 -->
Rechts oben ein grauer Baustein 15 mit Senkung + M4 Gewinde + feste Achse + Z10 ohne Antrieb-Funktion.
Untere Z10 dient zum Antrieb Innen-Z30 .
Gelenkwürfel-Zunge 31426 durch bohrt + M4 Befestigung

As met schroefeind 4 x 95mm (10 stuks) o.a. bij :
http://flecnederlandvo.mamutweb.com/Shop/List/Techniekonderdelen/171/3
