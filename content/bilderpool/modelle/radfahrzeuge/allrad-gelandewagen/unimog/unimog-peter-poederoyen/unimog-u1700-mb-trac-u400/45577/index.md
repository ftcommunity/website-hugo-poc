---
layout: "image"
title: "Eisenbahn-Draisine"
date: "2017-03-19T16:36:53"
picture: "unimogfuerrangierarbeiteneisenbahndraisine13.jpg"
weight: "35"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/45577
- /detailsb83e-2.html
imported:
- "2019"
_4images_image_id: "45577"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T16:36:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45577 -->
Als Draisine wird ein meist vierrädriges oder dreirädriges Bahndienstfahrzeug bezeichnet, das, mit Muskel- oder mit Motorantrieb ausgestattet, als Hilfsfahrzeug zur Inspektion von Eisenbahnstrecken sowie zum Transport von Arbeitern und Werkzeug verwendet wird.  
https://de.wikipedia.org/wiki/Eisenbahn-Draisine
