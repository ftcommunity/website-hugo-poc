---
layout: "image"
title: "Classic-Unimog 6"
date: "2011-01-15T20:46:34"
picture: "Classic-Unimog_08.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29693
- /detailsc1fc-3.html
imported:
- "2019"
_4images_image_id: "29693"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29693 -->
Der Lenkeinschlag der Vorderräder ist ganz ordentlich.

Die Bodenfreiheit leidet natürlich unter dem großen Zahnrad Z20 in der Achsmitte. Aber das war mir bei diesem Modell nicht so wichtig ...