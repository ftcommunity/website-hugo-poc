---
layout: "image"
title: "Classic-Unimog 1"
date: "2011-01-15T20:46:33"
picture: "Classic-Unimog_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29688
- /detailsb957.html
imported:
- "2019"
_4images_image_id: "29688"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29688 -->
Das vorliegende Modell ist ein kleiner Allradler, optisch und technisch inspiriert von klassischen FT-Modellen aus den 70ern.

Eigentlich war ich nur auf der Suche nach der kleinstmöglichen angetriebenen Vorderachse mit Achsschenkel-Lenkung, aber es wurde dann letztendlich (wie so oft) ein Allradler.

Der Abtrieb erfolgt per Mini-Motor über eine Kette starr auf beide Achsen. Auch in den Achsen sitzen aus Platzgründen keine Differenziale, so dass alle 4 Räder starr miteinander verbunden sind. Geradeaus klappt das noch ganz gut, aber bei Kurvenfahrt verspannt sich alles, und das Fahrzeug schiebt stark über die Vorderräder. Bloß gut, dass die klassischen FT-Reifen (ohne Gummiringe) auf glatten Oberflächen keine so große Haftung haben ...

Aufgrund des kleinen Motors geht im Gelände natürlich auch nicht viel (eher gar nichts) ...

Sontige Details:

- Lenkung per Servo
- Steuerung per Control Set
- Platz für 2 FT-Männchen
- Lenkrad dreht beim Lenken mit
- Stromversorgung per 9V-Blockakku