---
layout: "image"
title: "Classic-Unimog 13"
date: "2011-01-15T20:46:47"
picture: "Classic-Unimog_16.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29700
- /details1a98-2.html
imported:
- "2019"
_4images_image_id: "29700"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29700 -->
Trotz moderner Zutaten (Kardangelenke, Batteriekasten, Control Set mit Servo, schwarze Bausteine) sieht es doch irgendwie sehr klassisch aus, oder? Liegt wahrscheinlich an den Rädern und der einfachen Mechanik. Mir gefällt's ... ;o)