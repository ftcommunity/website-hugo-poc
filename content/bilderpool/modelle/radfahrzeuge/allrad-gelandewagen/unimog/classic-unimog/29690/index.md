---
layout: "image"
title: "Classic-Unimog 3"
date: "2011-01-15T20:46:34"
picture: "Classic-Unimog_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29690
- /details9cf4.html
imported:
- "2019"
_4images_image_id: "29690"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29690 -->
