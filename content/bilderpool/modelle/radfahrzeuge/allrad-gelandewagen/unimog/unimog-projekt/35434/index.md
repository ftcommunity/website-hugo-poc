---
layout: "image"
title: "Unimog V2 12"
date: "2012-09-03T10:24:20"
picture: "Unimog_14_2.jpg"
weight: "29"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35434
- /details5971-2.html
imported:
- "2019"
_4images_image_id: "35434"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35434 -->
Das Fahrgestell ohne Ladefläche (vorn links ist mir beim Abheben eine der Anbindungen der Ladefläche mit abgegangen, ohne dass ich es gemerkt habe). Alles ist deutlich leichter und übersichtlicher als bei der ursprünglichen Version.

Der Akku sitzt nun im Heck, was sich positiv auf die Gewichtsverteilung auswirkt.