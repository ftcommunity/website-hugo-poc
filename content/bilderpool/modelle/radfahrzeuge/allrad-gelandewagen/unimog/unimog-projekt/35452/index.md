---
layout: "image"
title: "Unimog V2 30"
date: "2012-09-03T10:24:21"
picture: "Unimog_33.jpg"
weight: "47"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35452
- /detailsefa8.html
imported:
- "2019"
_4images_image_id: "35452"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35452 -->
Die Vorderachse mit Lenkung im Detail. Sehr stabil und - mit dem integrierten Servo - sehr kompakt.

Die Räder hängen nach wie vor nur an den Rastkupplungen, weswegen das Fahrzeug nicht zu schwer sein sollte, um ein Abfallen der Räder zu vermeiden.