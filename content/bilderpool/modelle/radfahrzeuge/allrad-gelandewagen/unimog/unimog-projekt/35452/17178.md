---
layout: "comment"
hidden: true
title: "17178"
date: "2012-09-03T20:07:19"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Uff, das Servoteil habe ich ja ganz übersehen - das ist aber wirklich geschickt untergebracht. Und bei dem ganzen Modell ist echt viel Statik dabei - das ist ja fast durchsichtig! Great.

Gruß,
Stefan