---
layout: "image"
title: "Unimog V2 28"
date: "2012-09-03T10:24:21"
picture: "Unimog_31.jpg"
weight: "45"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35450
- /details24fb.html
imported:
- "2019"
_4images_image_id: "35450"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35450 -->
Die Front teilzerlegt.