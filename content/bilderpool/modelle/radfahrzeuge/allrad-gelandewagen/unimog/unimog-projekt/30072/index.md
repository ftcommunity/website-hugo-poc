---
layout: "image"
title: "Unimog 06"
date: "2011-02-18T23:20:35"
picture: "Unimog_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30072
- /details8370.html
imported:
- "2019"
_4images_image_id: "30072"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30072 -->
Die Ladefläche ist eine Bauplatte 500, die ich von Anfang an in die Dimensionen des Fahrzeugs mit eingeplant hatte.

Die hintere Bordwand ist klappbar gestaltet.