---
layout: "image"
title: "Unimog V2 06"
date: "2012-09-03T10:24:20"
picture: "Unimog_08_2.jpg"
weight: "23"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35428
- /details79ba.html
imported:
- "2019"
_4images_image_id: "35428"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35428 -->
Der Mercedes-Stern ist das einzige gelbe Teil.