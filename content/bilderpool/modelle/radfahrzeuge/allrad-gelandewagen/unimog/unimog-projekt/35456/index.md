---
layout: "image"
title: "Unimog V2 34"
date: "2012-09-03T10:24:21"
picture: "Unimog_37.jpg"
weight: "51"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35456
- /detailsa7c0-2.html
imported:
- "2019"
_4images_image_id: "35456"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35456 -->
Die Hinterachse von unten. Durch die Metallstange ist sie sehr verwindungssteif, die Statiksteine sorgen für eine verringerte Reibung.