---
layout: "image"
title: "Unimog 07"
date: "2011-02-18T23:20:35"
picture: "Unimog_07.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30073
- /details6515-2.html
imported:
- "2019"
_4images_image_id: "30073"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30073 -->
Die Verriegelung der hinteren Bordwand erreiche ich über 2 drehbar gelagerte Statikwinkel pro Seite, die bei hoch geklappter Bordwand hinter die Stange des Spriegels gedreht werden. Damit dies spielfrei funktioniert, ist dort im unteren Bereich noch eine schwarze Hülse drüber geschoben. Klappt perfekt! :o)