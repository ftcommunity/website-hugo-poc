---
layout: "image"
title: "Unimog 01"
date: "2011-02-18T23:20:34"
picture: "Unimog_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/30067
- /details170a.html
imported:
- "2019"
_4images_image_id: "30067"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30067 -->
Nach einigen vergeblichen Versuchen, einen großen, starken und stabilen Allrad-LKW auf Basis der großen Traktor-Reifen zu bauen, wollte ich mich an dieses Thema eigentlich nicht mehr ran trauen. Ich war der Meinung, dass so etwas mit FT nur unbefriedigend möglich ist.

Ich habe mich aber nun trotzdem noch mal an dieses Thema gewagt. Der hier gezeigte Unimog ist aber in der jetzigen Form alles andere als perfekt – Details dazu später. Daher werde ich ihn noch mehrfach umbauen müssen, wollte aber diesen ersten Stand hier dokumentieren. Daher auch der Name „Das Unimog-Projekt“.

Dieses Mal sollte alles besser werden: Für einen starken und stabilen Antrieb sorgen 2 Power-Motoren, ausschließlich formschlüssige Verbindungen von den Motoren zu den Rädern (keine auf Achsen rutschenden Zahnräder mehr möglich) und der Verzicht auf die kleinen und wenig stabilen Rastkegelzahnräder und Kardan-Gelenke. Zusätzlich wollte ich Federung vorn und hinten und eine schaltbare Differenzialsperre für das mittlere Differenzial. Und das Ganze sollte natürlich auch noch gut aussehen und eine ansprechende Ladefläche bekommen. Licht vorn und hinten darf natürlich auch nicht fehlen. Es sollte der perfekte Allrad-LKW werden!

Basis des Modells ist die bekannte angetriebene Vorderachse mit den Kegelradsätzen, allerdings mit mehr Bodenfreiheit, da die komplette Radaufhängung über der Radachse angeordnet ist. Das bedeutet automatisch den Verzicht auf die kleinen Rastkegelzahnräder (35062), die dort sonst radseitig angebracht werden, da das Ganze sonst viel zu instabil werden würde. Ich habe stattdessen die Kegelzahnräder mit Rastachse (35061) gewählt. Die Räder werden dadurch nur durch eine Rastkupplung gehalten …

Und da wäre ich schon beim größten Problem des Fahrzeugs: Mit all der Technik, den vielen Verkleidungsplatten und der schönen Ladefläche wiegt das Modell fette 2,2 kg, die sich auf dem Arm noch schwerer anfühlen! Allein die Ladefläche wiegt 313 g. Bei meinem ersten Prototyp der Achse war alles noch in Ordnung und die Vorderräder hielten perfekt – beim fertigen schweren Modell kann es aber passieren, dass sich die Räder auf grobem Untergrund seitlich weg biegen und abfallen. Na toll. :o/

Um das leere Fahrzeug ohne Beladung halbwegs waagerecht zu halten, benötige ich 3 Federn pro Rad, also insgesamt 12! Viel zu viel – das kann so nicht bleiben …

Die Kraftübertragung der beiden Motoren erfolgt ausschließlich formschlüssig auf die Räder, was auch gut funktioniert: Nichts rattert oder springt, perfekt! In der Ebene fährt das Fahrzeug schön schnell und überwindet kleine Hindernisse spielend. Aber leider ist die Steigfähigkeit durch das hohe Gewicht stark eingeschränkt! Schon an leichten Steigungen blockieren die Motoren und der Empfänger schaltet ab. Sehr schlecht! Weiterhin vermute ich, dass die Reibung im System sehr hoch ist und somit viel Kraft auf der Strecke bleibt.

Die Differenzialsperre nutzt das einzige geklemmte Zahnrad Z20 auf der vorderen Kardanwelle, was über einen Schiebemechanismus mit dem mittleren Differenzial verbunden wird. Das Ergebnis ist klar: Komme ich mit dem Fahrzeug in eine Situation, wo die Sperre tatsächlich mal gebraucht wird, rutscht das Zahnrad hilflos auf der Welle! Thema Gewicht … :o( Die hohe Masse verbunden mit den verschränkbaren Achsen sorgt aber dafür, dass eigentlich immer alle Räder am Boden sind, so dass die Sperre sehr selten gebraucht wird.

Erstes Fazit: Das Modell hat Potenzial, aber muss in folgenden Punkten verbessert werden:

- Gewicht stark runter
- die aufwendige und wenig nutzbare Differenzialsperre raus
- Verringerung der Reibung im System

Fortsetzung folgt …