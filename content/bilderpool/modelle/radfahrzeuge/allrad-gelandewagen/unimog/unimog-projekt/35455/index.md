---
layout: "image"
title: "Unimog V2 33"
date: "2012-09-03T10:24:21"
picture: "Unimog_36.jpg"
weight: "50"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35455
- /detailsedd4-2.html
imported:
- "2019"
_4images_image_id: "35455"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35455 -->
Die neue leichte Hinterachse ist sehr stabil und funktioniert prächtig.