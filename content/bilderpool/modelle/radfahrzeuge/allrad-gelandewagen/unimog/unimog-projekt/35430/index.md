---
layout: "image"
title: "Unimog V2 08"
date: "2012-09-03T10:24:20"
picture: "Unimog_10_2.jpg"
weight: "25"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35430
- /details4275-2.html
imported:
- "2019"
_4images_image_id: "35430"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35430 -->
Nochmal ein besseres Foto von der Heckklappe (zum ersten Stand unverändert).