---
layout: "comment"
hidden: true
title: "13626"
date: "2011-02-18T23:42:29"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tüfteln macht halt Spaß, nur weiter! Die Bilder machen Lust auf die nächste Entwicklungsstufe. :-)

Ich habe aber noch nicht verstanden, was das rote Rast-Z20 ganz vorne macht.

Gruß,
Stefan