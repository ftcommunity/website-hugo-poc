---
layout: "image"
title: "Unimog V2 07"
date: "2012-09-03T10:24:20"
picture: "Unimog_09_2.jpg"
weight: "24"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/35429
- /details03ca-2.html
imported:
- "2019"
_4images_image_id: "35429"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35429 -->
Das Cockpit mit neuem Schalter zum Aktivieren des Empfängers. Das Licht lässt sich nun mit der Fernbedienung ein- und ausschalten.

Das Lenkrad hat keine Funktion.