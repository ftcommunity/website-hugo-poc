---
layout: "image"
title: "Unimog-Kran"
date: "2010-12-22T15:27:25"
picture: "DSCF4559.jpg"
weight: "12"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29497
- /detailsf5af-2.html
imported:
- "2019"
_4images_image_id: "29497"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29497 -->
Seit einiger Zeit habe ich diesen Unimog-Kran aufgebaut. Dafür habe ich nochmal so einen ähnlichen wie den anderen in dieser Kategorie vorgestellten Unimog gebaut und darauf dann den Kran gesetzt. Dabei habe ich die Idee mit einer Feder bei der Pendelachse in dem Kommentar realisiert. Das verhindert auch erheblich das Kippen. Der ganze Unimog ist sehr geländefähig durch die Feder, der kompakten Bauweise und dem kurzen Radstand. Ich habe 2 Empfänger benutzt.