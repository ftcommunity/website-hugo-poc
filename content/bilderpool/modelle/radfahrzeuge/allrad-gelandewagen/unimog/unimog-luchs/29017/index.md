---
layout: "image"
title: "Unimog Antrieb"
date: "2010-10-16T21:50:52"
picture: "DSCF3624.jpg"
weight: "5"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29017
- /details22ac.html
imported:
- "2019"
_4images_image_id: "29017"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29017 -->
Nach langem rumprobieren, den Motor so zu befestigen, dass nichts rattert habe ich schließlich das Spezialteil von TST gekauft. Nun habe ich keine Beschwerden mehr. Der Motor hat genau die richtige Üersetzung (1:50). Ich habe auch schon 1:20 (Encodermotor) oder 1:8 ausprobiert. Die haben aber eindeutig zu wenig Kraft.