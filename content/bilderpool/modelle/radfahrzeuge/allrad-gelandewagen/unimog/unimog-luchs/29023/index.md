---
layout: "image"
title: "Unimog Gelände 3"
date: "2010-10-16T21:50:53"
picture: "DSCF3638.jpg"
weight: "11"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29023
- /detailse67a.html
imported:
- "2019"
_4images_image_id: "29023"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29023 -->
Und wenn es zu steil wird, kann er sich immer noch mit der Seilwinde helfen . . .