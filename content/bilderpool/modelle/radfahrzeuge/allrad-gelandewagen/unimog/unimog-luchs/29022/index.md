---
layout: "image"
title: "Unimog Gelände 2"
date: "2010-10-16T21:50:52"
picture: "DSCF3636.jpg"
weight: "10"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29022
- /detailsf084.html
imported:
- "2019"
_4images_image_id: "29022"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29022 -->
Hier ist eine etwas stärkere beanspruchung. Wenn das nicht so ein guter Untergrund wäre, würden die Räder auch durchdrehen. Wenn der Unimog auf dieser Steigung seitlich stehen würde, wär er schon längst umgekippt.