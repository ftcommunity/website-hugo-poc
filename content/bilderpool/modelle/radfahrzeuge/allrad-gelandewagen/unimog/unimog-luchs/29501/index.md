---
layout: "image"
title: "Kranarm"
date: "2010-12-22T15:27:27"
picture: "DSCF4566.jpg"
weight: "16"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29501
- /detailscd6d.html
imported:
- "2019"
_4images_image_id: "29501"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29501 -->
Den Kranarm habe ich zuerst mit Hubgetrieben ausgestattet. Nachdem diese zu schwach waren habe ich ihn wie bei dem anderem Hauptarm mit einer Schnecke betrieben.