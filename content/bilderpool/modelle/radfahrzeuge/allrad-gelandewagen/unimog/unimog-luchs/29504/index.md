---
layout: "image"
title: "Motor und Verkabelung"
date: "2010-12-22T15:27:27"
picture: "DSCF4571.jpg"
weight: "19"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29504
- /details3379-2.html
imported:
- "2019"
_4images_image_id: "29504"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29504 -->
Unterm Fahrzeug geht es ziemlich wild zu da ich die Kabel die von den Empfängern und LEDs an den Akku angeschlossen werden müssen erst mal unterm Fahrzeug zusammenkommen lassen hab damit nur noch ein Kabel an den Akku geht. Hier sieht man auch einen Teil des versteckten Empfängers unter dem Drehkranz