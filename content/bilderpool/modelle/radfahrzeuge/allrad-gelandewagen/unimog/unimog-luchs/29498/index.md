---
layout: "image"
title: "Kran"
date: "2010-12-22T15:27:26"
picture: "DSCF4562.jpg"
weight: "13"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29498
- /detailsf361.html
imported:
- "2019"
_4images_image_id: "29498"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29498 -->
Hier sieht man den Antrieb von dem Kran. Das ganze habe ich mit Schnecken gemacht, da ich mit Hubgetrieben schon schlechte Erfahrungen gemacht habe weil sie mit der Zeit ausgeleiert sind. Die 2 Motoren sind parallel an einen Motor ausgang geschaltet. Ich hatte noch nie Probleme, dass diese zu schwach waren, davor ist der ganze Unimog umgekippt.