---
layout: "image"
title: "Unimog Pendelachse 2"
date: "2010-10-16T21:50:52"
picture: "DSCF3628.jpg"
weight: "7"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
schlagworte: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- /php/details/29019
- /details740a.html
imported:
- "2019"
_4images_image_id: "29019"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29019 -->
Hier sieht man nochmal die Pendelachse im Einsatz.