---
layout: "image"
title: "Unimog 2"
date: "2007-10-10T19:44:37"
picture: "Unimog_07.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12175
- /detailsfead.html
imported:
- "2019"
_4images_image_id: "12175"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12175 -->
Man beachte seitlich neben dem Power-Motor die Verwendung des Innenteils eines defekten "Baustein 15 mit 2 Zapfen". Ich find die Teile extrem praktisch, wenn es um reine Versteifungsaufgaben geht und ein normaler Stein nicht reinpasst.

Klar, ein Baustein 7,5 mit zwei Federnocken hätte hochkant auch reingepasst, aber so finde ich's eleganter. Ist mal was anderes...