---
layout: "image"
title: "Mini-Unimog 1"
date: "2007-04-28T15:22:26"
picture: "miniunimog1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10174
- /details7a5a.html
imported:
- "2019"
_4images_image_id: "10174"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-04-28T15:22:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10174 -->
Ein schneller, kleiner und follgefederter Unimog.
