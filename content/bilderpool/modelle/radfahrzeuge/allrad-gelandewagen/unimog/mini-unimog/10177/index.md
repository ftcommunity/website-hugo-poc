---
layout: "image"
title: "Mini-Unimog 4"
date: "2007-04-28T15:22:26"
picture: "miniunimog4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10177
- /details9ca8.html
imported:
- "2019"
_4images_image_id: "10177"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-04-28T15:22:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10177 -->
