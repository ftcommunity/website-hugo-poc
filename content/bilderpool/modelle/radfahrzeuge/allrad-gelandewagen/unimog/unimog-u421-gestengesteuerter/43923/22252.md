---
layout: "comment"
hidden: true
title: "22252"
date: "2016-07-24T09:05:53"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Mei-o-mei!

Du willst es echt wissen. Einen Motortreiber, dessen Strombegrenzung erst so um die 30A einsetzt und dann die dünnen ft-Litzen in Zuleitung und Motorleitung! Das ist hart. Das ist beinhart!

Sonst gefällt mir Dein Unimog und das Steuerungskonzept aber gut - nur das keine Mißverständnisse aufkommen.

Grüße
H.A.R.R.Y.