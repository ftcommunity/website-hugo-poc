---
layout: "image"
title: "Heckansicht"
date: 2020-11-25T19:00:17+01:00
picture: "Buggy3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Die Messingachse ist nur Versteifung und musste genau abgelängt werden.