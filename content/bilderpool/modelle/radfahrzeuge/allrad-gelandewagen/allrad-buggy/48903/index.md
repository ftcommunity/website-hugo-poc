---
layout: "image"
title: "Schrägansicht"
date: 2020-11-25T19:00:19+01:00
picture: "Buggy1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Akku bleibt er unter 1 kg. Die Rastachsen mit Kardangelenken und Rastadaptern setzen der Formgebung Grenzen.
Die Federung in der Hinterachse ist mit einem Stück Silikonschlauch versteift.