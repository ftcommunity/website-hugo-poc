---
layout: "image"
title: "von unten"
date: 2020-11-25T19:00:16+01:00
picture: "Buggy4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Der Servohebel hat sich vorm Fotografieren verdrückt.
Damit die Räder nicht so leicht von den Rastaufnahmen hüpfen, sind kleine Pappstücke eingeklemmt, die die Rastzungen aufspreizen.