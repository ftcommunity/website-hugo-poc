---
layout: "image"
title: "Radaufhängung vorne"
date: 2020-11-25T19:00:12+01:00
picture: "Buggy8.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Räder sitzen mit Freilaufnaben auf 'Rastachse mit Platte' (130593) und werden von Kegelzahnrädern angetrieben. Vorne sind das die weißen, die aus einem Rast-Differenzial stammen.