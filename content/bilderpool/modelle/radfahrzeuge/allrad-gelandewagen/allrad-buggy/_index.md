---
layout: "overview"
title: "Allrad-Buggy"
date: 2020-11-25T19:00:11+01:00
---

Dieser leichte Flitzer war auf der ft-Convention 2017 in Dreieich mit dabei. In der Zwischenzeit ist er gut abgehangen und hat etwas mehr abgespeckt. Und Staub gefangen. 
Die Vorderachse samt Motor stammt vom 'A-Team reloaded' https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/pkw-busse/a-team-reloaded/45961/
Da wurde schon angedeutet, dass es zum Allrad nicht mehr weit ist. Diesen Allrad gibt es beim Allrad-SUV hier: https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/allrad-gelandewagen/suv-allradantrieb/gallery-index/

Die Vorderachse wurde im Citroen-Type H wiederverwendet, wie auch in der Deese II (régénerée, die wiedergeborene). Hier kommt also noch so'n Aufguss...
