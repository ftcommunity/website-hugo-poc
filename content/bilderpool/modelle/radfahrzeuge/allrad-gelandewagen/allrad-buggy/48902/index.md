---
layout: "image"
title: "Draufsicht"
date: 2020-11-25T19:00:18+01:00
picture: "Buggy2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Die Geländigkeit kommt zu einem guten Teil daher, dass sich die Karosse wenig verwindungssteif ist.