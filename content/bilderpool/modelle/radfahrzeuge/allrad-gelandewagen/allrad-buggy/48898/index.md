---
layout: "image"
title: "Antrieb zur Hinterachse"
date: 2020-11-25T19:00:14+01:00
picture: "Buggy6.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Wo am 'A-Team reloaded' ein Rast-Z20 verbaut war, kommt hier das Mitteldifferenzial hin. Von da geht der Antrieb mitten durch den Innenraum nach hinten. Eine elegantere Variante wäre wieder schwerer geworden.