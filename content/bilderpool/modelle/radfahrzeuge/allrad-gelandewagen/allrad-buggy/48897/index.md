---
layout: "image"
title: "Hinterachse"
date: 2020-11-25T19:00:13+01:00
picture: "Buggy7.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Hier musste gestückelt werden, um die Spurbreite richtig hin zu kriegen. Die Rastachsen führen zu außen liegenden Rastkegel Z10, die in die Verzahnung am Reifen 60 greifen.