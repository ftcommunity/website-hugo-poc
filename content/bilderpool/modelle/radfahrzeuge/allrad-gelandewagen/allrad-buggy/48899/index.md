---
layout: "image"
title: "Front zerlegt"
date: 2020-11-25T19:00:15+01:00
picture: "Buggy5.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Website-Team"
license: "unknown"
---

Vorderachse und Motor sind vom 'A-Team reloaded' übernommen:
https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/pkw-busse/a-team-reloaded/45961/

Vor dem Motor ist wenig Platz, deshalb sind da abgesägte Stecker verbaut. Man kann natürlich auch ganz ohne Stecker arbeiten.