---
layout: "image"
title: "Land Rover 4x4 7"
date: "2006-12-08T10:42:04"
picture: "Land_Rover_4x4_08.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/7724
- /details808d.html
imported:
- "2019"
_4images_image_id: "7724"
_4images_cat_id: "730"
_4images_user_id: "328"
_4images_image_date: "2006-12-08T10:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7724 -->
Hier ist der ganze Antriebsstrang inkl. der Lenkung gut zu sehen.

Da der Rahmen oben fehlt, klappt das Fahrzeug in der Mitte zusammen, da die beiden gelben Zugstreben unten mehr Kraft ausüben, als erforderlich ist. Aber so ist der ganze Zusammenbau extrem fest und haltbar.