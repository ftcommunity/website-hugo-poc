---
layout: "image"
title: "Jeep2-05.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-05.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4551
- /details1b33.html
imported:
- "2019"
_4images_image_id: "4551"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4551 -->
Unter der Haube sieht es genauso aus wie im Jeep No.1, aber nur wenn man von oben hineinschaut. Unter dem IR-Empfänger befindet sich jetzt der Motor für die Seilwinde.
