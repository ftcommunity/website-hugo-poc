---
layout: "image"
title: "Jeep2-21.JPG"
date: "2005-08-12T14:07:18"
picture: "Jeep2-21.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4567
- /detailsf0c2.html
imported:
- "2019"
_4images_image_id: "4567"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4567 -->
Erste Baustufe der Hinterachse. Die Achsen der Kegelzahnräder sind etwas zu kurz und rasten nicht im Differenzial ein. Das macht aber nichts, der Antrieb funktioniert trotzdem.
