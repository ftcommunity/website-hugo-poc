---
layout: "image"
title: "Jeep2-28.JPG"
date: "2005-08-12T14:07:28"
picture: "Jeep2-28.jpg"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4574
- /details4c84.html
imported:
- "2019"
_4images_image_id: "4574"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:07:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4574 -->
Das ist das Zwiebel- ääh, Gepäcknetz.

Die S-Riegel sind mit 4mm-Bohrung versehen und dann geschlitzt worden. Der Schlitz sollte etwa 0,5mm breit sein, dann kann man auch prima Kabel einclipsen. Der S-Riegel im Bild ist etwas zu weit aufgeschnitten.
