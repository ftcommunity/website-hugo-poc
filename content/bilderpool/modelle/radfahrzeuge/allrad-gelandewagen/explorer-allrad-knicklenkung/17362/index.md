---
layout: "image"
title: "ExplorerMk2-08.JPG"
date: "2009-02-11T18:31:11"
picture: "ExplorerMk2-08.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17362
- /details074f.html
imported:
- "2019"
_4images_image_id: "17362"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-11T18:31:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17362 -->
Die Motoren passen schön symmetrisch links und rechts außen hin. Die Antriebsachse kann zwischen den beiden Rastadaptern 36227 am Verbinder 15 getrennt werden. Anders müsste beim Zusammenbauen und Herumprobieren jedesmal das ganze Chassis auseinander gerupft werden. Nach hinten und nach vorne lässt sich nichts mehr verschieben, wenn die Achsdifferenziale einmal drin sind.
