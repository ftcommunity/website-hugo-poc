---
layout: "image"
title: "Explorer23.JPG"
date: "2009-02-04T16:08:55"
picture: "Explorer23.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17308
- /details17d3.html
imported:
- "2019"
_4images_image_id: "17308"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T16:08:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17308 -->
Hier ist der Fahrmotor abgezogen und der Blick aufs Mitteldifferenzial ist frei.

Die BS7,5 vor dem vertikal gestellten Drehkranz sind wichtig, um den richtigen Abstand für den Antrieb des Hinterachs-Differenzials mit den Rastachsen hin zu bekommen.
