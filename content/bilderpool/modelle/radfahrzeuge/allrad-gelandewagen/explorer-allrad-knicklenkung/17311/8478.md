---
layout: "comment"
hidden: true
title: "8478"
date: "2009-02-08T10:15:35"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Eure Adleraugen sehen einfach alles! Das sind die Reste von den gekappten Z10, wo ich auch die verbliebenen Stege noch abgehobelt habe. Zwei weitere sind unter den Z30 in der Lenkung zu sehen und halten die I-Strebe am Platz.

Nachdem sich dieses Teil als nützlich erwiesen hat, habe ich auch noch die eine oder andere Achskupplung ugewandelt. 

Gruß,
Harald