---
layout: "image"
title: "Explorer44.jpg"
date: "2009-02-05T20:27:28"
picture: "Explorer44.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17315
- /details649d-2.html
imported:
- "2019"
_4images_image_id: "17315"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:27:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17315 -->
Blick von unten aufs Mitteldifferenzial. Die zweite Befestigungslasche baumelt ungenutzt herum (es geht auch ohne sie).
