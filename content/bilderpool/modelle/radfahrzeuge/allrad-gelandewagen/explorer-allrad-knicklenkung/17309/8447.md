---
layout: "comment"
hidden: true
title: "8447"
date: "2009-02-05T15:22:05"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was ein saustarkes Modell wiedermal. Danke!

Rattern die Ketten nicht, wenn sie über die K-Achse gelenkt werden? Oder geht das langsam genug, dass es keiner merkt?

Gruß,
Stefan