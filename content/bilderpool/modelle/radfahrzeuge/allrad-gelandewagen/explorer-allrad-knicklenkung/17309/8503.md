---
layout: "comment"
hidden: true
title: "8503"
date: "2009-02-13T16:57:50"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
"Verkünstelt" - ja, in der Tat. Ich weiß jetzt, dass die Lenkung auch deutlich simpler zu machen ist. 

Die Differenzialsperren sollten eigentlich überflüssig sein. Aber auf der schief gestellten Platte drehen immer zwei Räder durch (eins hinten, eins vorn). Es könnte auch am Gewicht oder dessen Verteilung liegen. Da müsste der Schwerpunkt tiefer kommen (zu Lasten der Bodenfreiheit) oder ich müsste mal die Reifen mit Sand füllen.

Auf die neuen Motoren bin ich auch schon mächtig gespannt.

Gruß,
Harald