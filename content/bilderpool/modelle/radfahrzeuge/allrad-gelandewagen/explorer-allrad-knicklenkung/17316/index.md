---
layout: "image"
title: "Explorer47.jpg"
date: "2009-02-05T20:31:33"
picture: "Explorer47.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17316
- /details700f-2.html
imported:
- "2019"
_4images_image_id: "17316"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:31:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17316 -->
Der Antriebsstrang "unplugged". Die Z10 sind etwas bearbeitet worden.
