---
layout: "image"
title: "Explorer15.JPG"
date: "2009-02-04T15:57:31"
picture: "Explorer15.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/17305
- /details71f0.html
imported:
- "2019"
_4images_image_id: "17305"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T15:57:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17305 -->
Dank 1:4-Untersetzung im Endantrieb kommt das ft-Clips-Kardangelenk mitten im Antriebsstrang noch nicht an seine Grenzen.
Die vielen Ketten gehören alle zur Lenkung.
