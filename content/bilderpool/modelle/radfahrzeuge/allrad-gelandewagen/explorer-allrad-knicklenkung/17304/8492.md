---
layout: "comment"
hidden: true
title: "8492"
date: "2009-02-11T15:59:26"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Danke! Hmm, ja... Böschungswinkel: der ist leicht zu verbessern, weil ich vor der Vorderachse alle Freiheiten habe. Das wird in "Mark II" gleich mit gemacht, neben der vereinfachten Lenkung (frage mich immer noch, warum ich keine Schnecken quer eingebaut hab. Ich hab immer nur an Längseinbau gedacht und den dann verworfen).

Gruß,Harald