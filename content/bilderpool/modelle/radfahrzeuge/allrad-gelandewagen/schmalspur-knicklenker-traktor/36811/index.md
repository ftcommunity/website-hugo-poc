---
layout: "image"
title: "Schmalspur-Knicklenker-Traktor"
date: "2013-03-24T17:18:24"
picture: "holder2.jpg"
weight: "2"
konstrukteure: 
- "DL8MA + Max"
fotografen:
- "DL8MA"
uploadBy: "DL8MA"
license: "unknown"
legacy_id:
- /php/details/36811
- /detailsbb18.html
imported:
- "2019"
_4images_image_id: "36811"
_4images_cat_id: "2731"
_4images_user_id: "1613"
_4images_image_date: "2013-03-24T17:18:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36811 -->
Schmallspur-Knicklenker-Traktor für Weinberge usw.   (Holder)