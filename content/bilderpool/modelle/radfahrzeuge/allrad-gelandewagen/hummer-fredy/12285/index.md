---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:47"
picture: "ersteversuchefuereinenhummer6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12285
- /details1f99.html
imported:
- "2019"
_4images_image_id: "12285"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12285 -->
Nur der Lenkmotor.
