---
layout: "image"
title: "01-Seitenansicht"
date: "2007-07-15T17:48:59"
picture: "01-Seite.jpg"
weight: "1"
konstrukteure: 
- "unbek."
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11068
- /details273d-4.html
imported:
- "2019"
_4images_image_id: "11068"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:48:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11068 -->
Hier das Beispiel eines echten Monster-Trucks als ausgeführtes Konstruktionsbeispiel.

Insgesamt ist diese Konstruktion außerordentlich leicht und wirkt nur durch die gewaltigen Räder so wuchtig. Die Karosserie ist ein Kunststoffnichts und dient nur zur Unterstützung des Airbrush-Motivs.

Die Rahmenkonstruktion darunter dürften 2 Personen wohl leicht anheben dürfen. Das ist nur normales Wasserrohr. Der Antriebsmotor im hinteren Teil ist völlig unspektakulär und dürfte nicht allzuviel Leistung freisetzen. Am Motor angeflanscht ist ein Automatikgetriebe. Von dort aus geht es per Kardanwelle auf das mittlere Verteilergetriebe. Von dort ohne Mitteldifferential zur Vorder- und Hinterachse.

Interessant ist die Tatsache, daß das Fahrwerk vollkommen symmetrisch gebaut ist. Beide Achsen sind gelenkt, wobei die Hinterachse selten gelenkt wird (es war Schmutz auf der Hydraulikstange), beide Achsen sind gebremst und auf die vollkommen gleiche Art angebaut. Man könnte also jederzeit die Karosserie umdrehen.

Der Raddurchmesser dürfte ca. 1,5 m sein.
