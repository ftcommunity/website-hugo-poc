---
layout: "image"
title: "04-Unterwaagen"
date: "2007-07-15T17:49:00"
picture: "04-Unterwaagen.jpg"
weight: "4"
konstrukteure: 
- "unbek."
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11071
- /details099e.html
imported:
- "2019"
_4images_image_id: "11071"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11071 -->
Hier der Blick von hinten links zur Vorderachse. Gut erkennbar das Verteilergetriebe. Das sieht nach Eigenbau aus. Die Wälzlager sind in angeschraubten Normlagerschilden untergebracht, wie man sie von der Stange kaufen kann. Das Gehäuse ist mit Riffelstahlblech verkleidet - ungewöhnlich. Wahrscheinlich ist dahinter ein popeliger Kettenantrieb, auf jeden Fall kein Leistungsgetriebe, das würde hinter dem glänzenden Blech ohne Kühlrippen einem raschen Hitzetod sterben.

Der Rohrrahmen selbst ist eine Tragbrückekonstruktion, die aus zwei Teilen besteht. Oben eine flaches Tragwerk, das die Karosserie trägt, darunter eine Mittelkonsole, die die Achsschübe auffängt. Da hatte jemand Lust am Schweißen.
