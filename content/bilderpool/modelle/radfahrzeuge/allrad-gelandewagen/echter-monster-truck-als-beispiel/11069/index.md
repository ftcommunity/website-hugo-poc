---
layout: "image"
title: "02-Vorderachse"
date: "2007-07-15T17:49:00"
picture: "02-Vorderachse.jpg"
weight: "2"
konstrukteure: 
- "unbek."
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11069
- /detailsc3aa-2.html
imported:
- "2019"
_4images_image_id: "11069"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11069 -->
Jetzt wird der maschinenbauliche Notstand in aller Schärfe sichtbar: Lenkzylinder und Spurstange sind doch sehr filigran und überhaupt nicht mit einer LKW-Vorderachse vergleichbar. Ebenso zart die Federn, die ja praktisch nichts zu tragen haben.

Interessant ist die leicht schräg verlaufende Querstange (mit kleiner Aufschrift) über dem Differential. Bei genauem Hinsehen ist erkennbar, daß sie links an der Achse angeschlagen ist und rechts am Rahmen. Sie verhindert, daß die Achse seitwärts auspendelt.

Davor sind zwei Gurtbänder zu sehen. Deren Funktion ist mir nicht klar, aber womöglich verhindern sie das Herausfallen der Achsen, wenn der Truck hochgehoben wird. (Transportsicherung)

Oben auf dem Differential sieht man noch zwei Stangen schräg nach hinten verlaufen. Das ist die Drehmomentstütze, damit sich die Achse nicht dreht, sondern die Räder.
