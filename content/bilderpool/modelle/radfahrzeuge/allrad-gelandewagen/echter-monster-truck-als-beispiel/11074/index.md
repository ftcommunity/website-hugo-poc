---
layout: "image"
title: "07-Differential hinten"
date: "2007-07-15T17:49:00"
picture: "07-Differential_hinten.jpg"
weight: "7"
konstrukteure: 
- "unbek."
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/11074
- /details7e61-2.html
imported:
- "2019"
_4images_image_id: "11074"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11074 -->
Hier das hintere Differential. Von unten nach oben: rote Spurstange, weil ja auch die Hinterachse lenkbar ist, darüber in braun eine Stütze, die den Achsschub auf den Rahmen gibt und ganz oben die Drehmomentstütze des Differentialgetriebes.

Interessant ist die Bremse, die auf die Antriebswelle wirkt. Eigentlich würde eine Bremse reichen, weil es ja kein Mitteldifferential gibt. Problematisch an dieser Bremse ist, daß sie ihre Wirksamkeit verliert, sowie ein Rad vom Boden abhebt. Tja...

Fazit: So ein Monster-Truck ist echter Leichtbau, eher extremer Leichtbau. Die Räder könnten womöglich die Hälfte des Gesamtgewichts ausmachen. Als Arbeitsmaschine wäre sie vollkommen ungeeignet und dürfte einer Dauerbeanspruchung keinesfalls standhalten. Dauernde Spurverstellung und wahrscheinlich häufige Radlagerwechsel wären da wohl die Folge.

Aber es geht ja um die Show...
