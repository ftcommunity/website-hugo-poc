---
layout: "image"
title: "4x4"
date: "2007-04-11T09:59:11"
picture: "4x46.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10052
- /details80ad.html
imported:
- "2019"
_4images_image_id: "10052"
_4images_cat_id: "908"
_4images_user_id: "456"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10052 -->
Auch vorne.
