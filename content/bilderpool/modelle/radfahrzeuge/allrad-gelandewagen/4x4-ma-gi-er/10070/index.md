---
layout: "image"
title: "Stromversorgung"
date: "2007-04-12T10:05:11"
picture: "x05.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10070
- /details71dd.html
imported:
- "2019"
_4images_image_id: "10070"
_4images_cat_id: "911"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10070 -->
