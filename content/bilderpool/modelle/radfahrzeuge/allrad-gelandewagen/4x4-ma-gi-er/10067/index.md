---
layout: "image"
title: "Federung"
date: "2007-04-12T10:05:11"
picture: "x02.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10067
- /details8553.html
imported:
- "2019"
_4images_image_id: "10067"
_4images_cat_id: "911"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10067 -->
Das ist meine "Mitte-Knickfederung"