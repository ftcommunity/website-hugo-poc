---
layout: "image"
title: "Vorne"
date: "2007-04-12T10:05:11"
picture: "x01.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10066
- /detailsd831.html
imported:
- "2019"
_4images_image_id: "10066"
_4images_cat_id: "911"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10066 -->
