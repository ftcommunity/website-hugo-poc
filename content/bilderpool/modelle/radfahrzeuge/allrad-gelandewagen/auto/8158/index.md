---
layout: "image"
title: "Seitenansicht"
date: "2006-12-26T21:04:39"
picture: "Allradauto2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8158
- /details63da-2.html
imported:
- "2019"
_4images_image_id: "8158"
_4images_cat_id: "748"
_4images_user_id: "456"
_4images_image_date: "2006-12-26T21:04:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8158 -->
Das ist das Auto von der Seite. Ich habe Metallstangen in die Grundbausteine, damit sie stabiler sind.
