---
layout: "image"
title: "Antrieb"
date: "2006-12-26T21:04:39"
picture: "Allradauto5.jpg"
weight: "5"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8161
- /details5410.html
imported:
- "2019"
_4images_image_id: "8161"
_4images_cat_id: "748"
_4images_user_id: "456"
_4images_image_date: "2006-12-26T21:04:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8161 -->
Hier sieht man den Antrieb. Der ist von thomas004.
