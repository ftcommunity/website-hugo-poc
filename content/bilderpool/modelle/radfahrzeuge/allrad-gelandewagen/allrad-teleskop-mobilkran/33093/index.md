---
layout: "image"
title: "Überlastschutz (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran62.jpg"
weight: "62"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33093
- /details5e7e.html
imported:
- "2019"
_4images_image_id: "33093"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33093 -->
Das Hakenseil wird aber erst über diese Umlenkrolle geführt, welche gerade diesen Taster betätigt. Nur solange der gedrückt ist, kann man den Kranhaken heben lassen, sonst nur noch senken.
