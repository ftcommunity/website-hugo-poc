---
layout: "image"
title: "Detailaufnahme des Bedienpults"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran65.jpg"
weight: "65"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33096
- /detailsdb07.html
imported:
- "2019"
_4images_image_id: "33096"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33096 -->
- Links oben der graue ft-Schalter: Schaltet das Blinklicht ein bzw. aus.
- Etwas links von der Bildmitte, im linken Teil des angedeuteten schräg aufgestellten Kranarms: Nach oben drücken/unten ziehen => Arm wird aufgestellt/herunter geklappt
- Etwas weiter rechts oben: Nach rechts oben/links unten schieben => Teleskoparm fährt aus/ein
- Rechts unten der Kranhaken: Nach unten/oben schieben => Kranhaken wird abgesenkt/angehoben.
