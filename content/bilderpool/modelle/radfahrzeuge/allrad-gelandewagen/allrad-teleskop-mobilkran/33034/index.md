---
layout: "image"
title: "3/4 Heckansicht"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33034
- /detailsb103.html
imported:
- "2019"
_4images_image_id: "33034"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33034 -->
Schwerpunkt bei diesem Modell ist ganz klar nicht exakte Originaltreue zu einem bestimmten Krantyp, sondern die gleiche Funktionalität mit unveränderten fischertechnik-Teilen hinzubekommen.
