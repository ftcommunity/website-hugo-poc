---
layout: "image"
title: "Der Arm ganz ausgefahren (6)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran57.jpg"
weight: "57"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33088
- /detailsd315.html
imported:
- "2019"
_4images_image_id: "33088"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33088 -->
Hier oben schließlich ist es dann wohl sehr einsam. ;-)
