---
layout: "image"
title: "Drehplattform"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33063
- /details353f.html
imported:
- "2019"
_4images_image_id: "33063"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33063 -->
Der eigentliche Kranarm sitzt auf einer drehbaren Plattform. Alle Kabel wurden (mit je einem zeitweise abmontiertem Stecker) durch eines der Löcher der schwarzen Bauplatten und durch das Loch des darunter befindlichen ft-Drehkranzes geführt. Es gibt keine Schleifringe, aber dennoch kann man die Plattform ruhig mehrfach in dieselbe Richtung drehen lassen.
