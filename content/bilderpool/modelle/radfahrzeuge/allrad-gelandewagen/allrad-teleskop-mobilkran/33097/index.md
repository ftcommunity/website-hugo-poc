---
layout: "image"
title: "Transporthilfe"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran66.jpg"
weight: "66"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33097
- /details2677.html
imported:
- "2019"
_4images_image_id: "33097"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33097 -->
Für den Transport zur Convention wurde das Fahrzeug in diesen Rahmen eingesetzt. Die rote Bauplatte in der Mitte kann auch so verwendet werden, um das Heck des Fahrzeugs von unten abzustützen. Das ist z. B. zum Hochklappen des gesamten Fahrzeugs nützlich, wenn man etwas an seiner Unterseite warten muss.
