---
layout: "image"
title: "Spanabhebender Drehkranzantrieb"
date: "2011-10-29T17:04:57"
picture: "bildervomabbaudesteleskopmobilkrans1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33354
- /detailsb17c.html
imported:
- "2019"
_4images_image_id: "33354"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:04:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33354 -->
So sah es aus, als der PowerMotor mit der Schnecke abgebaut wurde, der den Drehkranz antrieb. Das Z15 hatte wohl wirklich viel arbeiten müssen.
