---
layout: "image"
title: "Radaufhängung und Antrieb"
date: "2011-10-29T17:10:20"
picture: "bildervomabbaudesteleskopmobilkrans6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33359
- /details1d6f.html
imported:
- "2019"
_4images_image_id: "33359"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:10:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33359 -->
Auf diesem Bild sieht man nun endlich, dass das Kegelzahnrad tatsächlich etwas unorthodox das Z30 antreibt.
