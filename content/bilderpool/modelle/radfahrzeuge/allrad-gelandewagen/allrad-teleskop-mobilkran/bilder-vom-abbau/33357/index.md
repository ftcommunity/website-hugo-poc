---
layout: "image"
title: "Lenkung"
date: "2011-10-29T17:04:58"
picture: "bildervomabbaudesteleskopmobilkrans4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33357
- /details11ed.html
imported:
- "2019"
_4images_image_id: "33357"
_4images_cat_id: "2470"
_4images_user_id: "104"
_4images_image_date: "2011-10-29T17:04:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33357 -->
Ein Blick von unten auf die Lenkkette sowie auf die nur noch auf der angetriebenen Seite eingehängten Antriebsketten. Was da unten so schräg heraus hängt, ist die Befestigung des S-Motors für das vordere Stützenpaar.
