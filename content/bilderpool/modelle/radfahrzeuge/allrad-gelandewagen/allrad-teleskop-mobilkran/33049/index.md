---
layout: "image"
title: "Kraftfluss zu den Hinterrädern"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33049
- /details2b4f-2.html
imported:
- "2019"
_4images_image_id: "33049"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33049 -->
Der Blick geht von der rechten Fahrzeugseite vor dem ersten Hinterrad vorbei und zeigt ein weiteres Winkelzahnrad, welches auf das Z30 Richtung Hinterachsen geht. Das ist kein Z10, damit die Vorder- und Hinterräder mit demselben Untersetzungsverhältnis angetrieben werden (bei den Vorderrädern gibt es ja auch eine Kombination Winkelzahnrad/Z30). Dadurch muss sich das hier sichtbare Antriebsdifferential nur drehen, aber nicht "differenzieren" - beide Abgangswellen drehen gleich schnell.
