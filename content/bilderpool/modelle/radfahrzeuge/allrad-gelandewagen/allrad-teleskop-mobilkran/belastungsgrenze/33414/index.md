---
layout: "image"
title: "Das Ergebnis"
date: "2011-11-05T17:50:29"
picture: "teleskopmobilkranbelastungsgrenze2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33414
- /details5a07.html
imported:
- "2019"
_4images_image_id: "33414"
_4images_cat_id: "2474"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33414 -->
Das Modell hält (ich hatte stark befürchtet, der Drehkranz würde dem nicht standhalten). Das Fahrwerk verwindet sich, die Stützen auf der abgewandten Seite sowie diese beiden Hinterräder hängen längst frei in der Luft. Nur das Vorderrad hat noch Bodenkontakt.

Insgesamt war damit die "Konstruktionsstrategie" erfolgreich, Dinge nur so stabil zu machen, wie nötig. Es gibt bis auf das Führerhaus und die über den Hinterrädern verlaufenden Zier-Statikträger praktisch keine überflüssigen Teile am Modell, und die verbauten Teile erreichen oftmals gleichzeitig ihre Grenzen. Es ist also nicht eine Baugruppe umsonst unnötig stark (und damit schwer), weil eine andere Baugruppe dann das schwächste Glied der Kette ist. Ein weiteres Beispiel ist vielleicht, dass der Kranarm an nur drei normalen Gelenksteinen aufgehängt ist - die genügen, die Belastungen sogar beim Aufstellen des ausgefahrenen Armes zu tragen.

Bis auf eine Sache stellte sich die Gesamtkonstruktion also als "ausgewogen gleichmäßig" stabil oder instabil heraus. Die eine Ausnahme ist wohl die Schnecke/Z15-Kombination beim Antrieb des Drehkranzes, die wohl unterdimensioniert war.

Alles in allem staune ich, was man fischertechnik-Teilen zumuten kann, insbesondere, aber nicht nur dem Drehkranz. Die halten echt was aus.
