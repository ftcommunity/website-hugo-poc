---
layout: "image"
title: "Der Test"
date: "2011-11-05T17:50:28"
picture: "teleskopmobilkranbelastungsgrenze1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33413
- /details9964-2.html
imported:
- "2019"
_4images_image_id: "33413"
_4images_cat_id: "2474"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33413 -->
Kurz vor dem Abbau hatte ich noch eine Grenze ausloten wollen: Die Stützen ausgefahren, den Arm 90° quer zum Fahrzeug gedreht (also so ungünstig wie möglich, was ein Umkippen des Fahrwerks angeht), so tief wie möglich aufgestellt (etwas weniger als waagerecht ist möglich) und voll ausgefahren. Ich wollte wissen, ob der Drehkranz irgendwann doch auseinander springt oder ob der Kranwagen umkippt.

Wie man sieht, brach oder riss nichts, es biegt sich alles mehr oder weniger beängstigend, aber das fischertechnik macht das alles noch mit. Und das Fahrwerk hebt ein Bein...
