---
layout: "image"
title: "Aufstellen des Kranarms"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33070
- /details406e-3.html
imported:
- "2019"
_4images_image_id: "33070"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33070 -->
Der Arm kann von "auf dem Führerhaus aufliegend" (siehe die ersten drei Bilder dieser Kategorie) bis fast senkrecht aufgestellt werden.
