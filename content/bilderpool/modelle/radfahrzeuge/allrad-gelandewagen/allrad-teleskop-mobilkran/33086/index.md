---
layout: "image"
title: "Der Arm ganz ausgefahren (4)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran55.jpg"
weight: "55"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33086
- /details2afe-2.html
imported:
- "2019"
_4images_image_id: "33086"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33086 -->
Hier sieht man gut, wie das Zugseil von außen über die Umlenkrollen das zweitinnerste Segment bis zur Endlage des Arms hochgezogen hat.
