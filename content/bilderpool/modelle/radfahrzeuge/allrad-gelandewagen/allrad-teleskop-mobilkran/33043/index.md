---
layout: "image"
title: "Vorderachse"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33043
- /detailsd8c3.html
imported:
- "2019"
_4images_image_id: "33043"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33043 -->
Wir blicken hier unter die Vorderachskonstruktion (links im Bild ist vorne am Fahrzeug). Die äußeren Ketten an den Z30 bilden den Antrieb. Die gelben Streben führen die Räder und dienen zusammen mit der mittleren Kette der Lenkung.

Rechts sieht man einen schräg eingebauten S-Motor für das vordere Stützenpaar.
