---
layout: "image"
title: "Einstieg"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33039
- /detailsb303.html
imported:
- "2019"
_4images_image_id: "33039"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33039 -->
Die Türen können entriegelt und geöffnet werden.
