---
layout: "image"
title: "Ausfahren des Kranarms"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33071
- /details53ea.html
imported:
- "2019"
_4images_image_id: "33071"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33071 -->
Der Arm kann dreifach ausgefahren werden, und alle drei Segmente fahren gleichzeitig und gleichmäßig aus. Bei komplett aufgestelltem und ausgefahrenem Arm misst das Modell insgesamt 1,25 m in der Höhe von der Aufstandsfläche der Stützen aus gemessen.
