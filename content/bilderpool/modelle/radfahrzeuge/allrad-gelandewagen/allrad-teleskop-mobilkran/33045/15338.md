---
layout: "comment"
hidden: true
title: "15338"
date: "2011-10-04T22:39:03"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, da hat's mir auch den Nucki rausgehauen. Kein Gedöns mit der Spurstange, die immer im Weg ist und nie die richtige Geometrie schafft. Kardanfehler? Hammwa nich! Aber dass diese Viergelenkmechanik den Drehpunkt konstant hält (und obendrein innerhalb der Aufstandsfläche), ist ja wohl der HAMMER. 

Gruß,
Harald