---
layout: "image"
title: "Lenkung"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33045
- /detailsfde2.html
imported:
- "2019"
_4images_image_id: "33045"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33045 -->
Die Lenk-Kette zieht einen Lenkhebel nach vorne und den anderen gleichzeitig in die andere Richtung. Hier ist auch eine Verbesserung gegenüber dem Stand der Convention 2011 eingebaut: Über den BS7,5, die via Förderkettenglied an der Kette hängen, sitzt jetzt eine "Hülse mit Scheibe" anstatt eines Klemmrings. Wenn das Fahrzeug nämlich, wie auf der Convention geschehen, oft bei aufgestelltem Kranarm auf der Stelle gelenkt wird, ist die Vorderachse relativ weit ausgefedert und die BS7,5 rutschten aus der Kette einfach heraus. Das wird durch die Hülse mit Scheibe zuverlässig verhindert.

Die Platten 15*15 an den BS7,5 dienen dazu, die Endlagentaster vorne zuverlässig zu betätigen.
