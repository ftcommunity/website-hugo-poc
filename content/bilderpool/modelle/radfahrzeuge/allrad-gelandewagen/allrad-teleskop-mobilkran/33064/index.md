---
layout: "image"
title: "Drehplattform"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33064
- /detailsa7c3-3.html
imported:
- "2019"
_4images_image_id: "33064"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33064 -->
Der PowerMotor geht über eine Schnecke auf ein Z15, welches die hier rechts vom Drehkranz sichtbare Schnecke antreibt. Gut zu sehen ist auch nochmal die Befestigung der Plattform am Drehkranz.
