---
layout: "image"
title: "Führerhaus"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33038
- /details830c.html
imported:
- "2019"
_4images_image_id: "33038"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33038 -->
Der Kranhaken wird während der Fahrt magnetisch festgehalten.
