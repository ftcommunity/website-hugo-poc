---
layout: "image"
title: "Frontseite"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33037
- /detailse725-2.html
imported:
- "2019"
_4images_image_id: "33037"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33037 -->
Die Fahrer müssten stehen; Sitze gibt's keine.
