---
layout: "image"
title: "Verseilung zum Ausfahren (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran44.jpg"
weight: "44"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33075
- /details40eb.html
imported:
- "2019"
_4images_image_id: "33075"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33075 -->
Hier sieht man den mittleren der drei interessanten Bereiche der Unterseite des Arms. Um ihn möglichst leicht und flach zu halten, führen nur BS7,5 mit aufgesetzten BS5 ein jeweils inneres Segment so, dass es nicht einfach herausfällt. Das ergibt nämlich eine Überlappung, die aber hinreichend klein ist, dass die Seilrollen des nächstinneren Segments immer noch vorbei können. Die gelben Streben sorgen dafür, dass die Segmente nicht auseinander driften und der Halt der inneren damit verloren ginge. Das graue Strebenkreuz hilft gegen Verbiegen des ganzen Arms nach links und rechts.

Die Seilkombinationen zum Ausfahren (neben dem eigentlichen Zugseil) sind jeweils separate Schnüre, die um Seilrollen eines Segmentes gehen, an den demgegenüber nächst äußeren und nächstinneren aber fest angebracht sind. So werden die richtigen Geschwindigkeitsverhältnisse beim Aus- und Einfahren zwangsweise eingehalten. Diese Seile gehen auch durch die U-Träger hindurch auf die andere Seite, wo dieselbe Mimik nochmal existiert.

Die S-Riegel mit den Riegelscheiben dienen dazu, die Schnüre, nachdem sie einmal verlegt wurden, hinreichend zu spannen. Dazu werden einzelne Seile ggf. ein oder zweimal um den Riegelfuß gewickelt, bevor der Riegel eingesetzt wird.
