---
layout: "image"
title: "Detailaufnahme des Bedienpults"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran64.jpg"
weight: "64"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33095
- /details6806.html
imported:
- "2019"
_4images_image_id: "33095"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33095 -->
- Links unten die vier Räder: Nach rechts/links schieben => Fahrzeug fährt vorwärts/rückwärts
- Rechts daneben die zwei Räder: Nach oben/unten drücken => Fahrzeug lenkt nach links/rechts.
- Links oben: Nach unten ziehen/oben drücken => Stützen fahren nach unten aus/nach oben ein.
- Rechts oben der angedeutete schräg aufgestellte Kranarm: Nach oben drücken/unten ziehen => Plattform dreht sich nach links/rechts
