---
layout: "image"
title: "Verseilung zum Einfahren (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran46.jpg"
weight: "46"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/33077
- /detailsa46e.html
imported:
- "2019"
_4images_image_id: "33077"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33077 -->
Die Seile auf der Oberseite des Kranarms sind fürs wieder Einfahren zuständig. Hier wird einfach wieder am zweitäußersten Segment nach unten gezogen.

Das rote Seil ist das für den Kranhaken. Es wird erst mal nach weit oben geführt, dort am zweitinnersten Segment wieder nach unten zu der Seilrolle in Bildmitte geführt, und erst von dort geht's rauf zur Kranspitze. Das bewirkt, dass beim Ausfahren des Arms automatisch Seil nachgegeben wird, sodass man wenn überhaupt nur ganz wenig Seil selber nachgeben muss (bei ausgefahrenem Arm muss das Hakenseil ja eine größere Strecke überspannen).
