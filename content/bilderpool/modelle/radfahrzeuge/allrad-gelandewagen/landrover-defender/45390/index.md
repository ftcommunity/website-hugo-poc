---
layout: "image"
title: "Landrover Defender"
date: "2017-03-03T21:38:24"
picture: "landrover01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45390
- /detailsfc3e.html
imported:
- "2019"
_4images_image_id: "45390"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45390 -->
Diese Wagen hat Federung,  Vor- und Achterlichter, und unter den Hauben ein Seilwinde. Er ist ferngesteuert mit dem IR-set. Die Türen können geöffnet werden.
Leider kein richtiges Allradantrieb, das wäre Mich vermutlich zu technisch, und dafür muss man wohl ein Großer Model bauen.
Ich hab mich inspirieren lassen durch den Landrover Defender, wovon es viele Ausfühfrungen gibt. Man kann also auch in fischertechnik viele Varianten bedenken. Für die Leiter war am Hintenseite leider kein Platz, also hab ich die an die Seite gemacht.