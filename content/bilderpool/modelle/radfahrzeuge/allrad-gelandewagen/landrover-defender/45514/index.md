---
layout: "image"
title: "landrover02.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover02_2.jpg"
weight: "57"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45514
- /details13ad-2.html
imported:
- "2019"
_4images_image_id: "45514"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45514 -->
Bei Photo 17 (und 23):
1. Das Teil ist ein Bißchen abgespeckt.
2. Für den Lenkrad ist ein Winkelstein 30 grad (statt 15 grad) verwendet.
3. Schwartzes Dashboard 15x45