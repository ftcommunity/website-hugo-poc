---
layout: "comment"
hidden: true
title: "23224"
date: "2017-03-16T11:36:49"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, das kommt immer aufs gleiche heraus: entweder wackelig und leicht (das ist aber dann auch schon mehr als 1 kg, sobald man Akku und 2 Motoren verbaut), oder stabil und um die 2 kg schwer. Mit viel Statik anstelle der BS30/BS15/Bauplatten kommt die Optik nur mit etwas Wohlwollen "schön" heraus.

Gruß,
Harald