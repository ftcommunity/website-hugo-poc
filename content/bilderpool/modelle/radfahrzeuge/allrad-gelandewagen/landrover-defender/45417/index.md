---
layout: "image"
title: "landrover28.jpg"
date: "2017-03-03T21:38:24"
picture: "landrover28.jpg"
weight: "28"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45417
- /detailse677.html
imported:
- "2019"
_4images_image_id: "45417"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45417 -->
Das ist das fertige Zweiten Teil.