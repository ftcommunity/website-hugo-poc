---
layout: "image"
title: "Radaufhängung"
date: 2020-05-10T11:38:56+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick (noch während der Entwicklung) auf das rechte Vorderrad von vorne gesehen: Das Rad sitzt per Freilaufnabe auf einer 130593 Rastaufnahmeachse. Gesichert wird das Rad mit einem Rastkegelzahnrad auf der Außenseite (hier nicht sichtbar) und zwei 105195 Scheibe 4 15.

Innen sitzt auf der Rastaufnahmeachse ein Gelenkstein, der gleichzeitig die Dreh- und senkrechte Antriebsachse darstellt. Von innen kommt das Rastkegelzahnrad vom Differenzial aus an.

Die Lenkung greift an einer 35980 Klemmhülse 7,5 an, die gerade noch auf die Rastaufnahmeachse gesteckt werden kann. An der sitzen zwei BS7,5 als "Hebel", und daran die Querstangen zur Lenkung.

So sind alle 6 lenkbaren Räder aufgebaut. Bei den unlenkbaren der dritten Achse sitzen die Rastaufnahmeachsen der Räder einfach in der Nut eines Statikträgers.
