---
layout: "image"
title: "Fehlgeschlagene Vorversuche (4)"
date: 2020-05-10T11:38:45+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch nicht kräftig genug, solange die Mulde unten liegt: Ein waagerechter Seilzug zieht an dem schräg stehenden Stab. Der ist wiederum überall gelenkig, sodass die Mulde in alle Richtungen kippen könnte - aber sie ist einfach zu schwer.

Man sieht hier ganz gut, wie umlaufende Achsen nur in Halterungen abgesenkt werden sollten, aber nirgends wirklich befestigt sind (außer an der Mulde). Durch weitere Mechanik sollten immer eine der Achsen (links, rechts oder hinten) am Abheben gehindert werden. Dadurch sollte die Mulde um die so fixierte Achse drehen und sich aufstellen. Es funktionierte allerdings leider nicht. Die Mulde verwand sich, und nichts funktionierte wie gewünscht, weil die notwendigen Kräfte einfach zu groß sind.

Man bräuchte einen sehr kompakten "Hydraulikzylinder", der mehrfach ausfahrbar ist (man braucht viel Strecke), und der in das bisschen freien Bereich vor dem Antriebs-XM-Motor passt - und zwar auch bei eingefedertem Fahrwerk.
