---
layout: "image"
title: "Fehlgeschlagene Vorversuche (4)"
date: 2020-05-10T11:38:43+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die in Längsrichtung per Seilzug verschiebbare Aufhängung des "Hydraulikzylinders".

Irgendwann verwarf ich das alles und es blieb beim letztlichen Aufstellen der Mulde nur nach hinten.
