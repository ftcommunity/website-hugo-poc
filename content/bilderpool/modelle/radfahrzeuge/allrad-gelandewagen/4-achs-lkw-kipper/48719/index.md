---
layout: "image"
title: "Blick auf die Unterseite"
date: 2020-05-10T11:39:01+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die vier Achsen hängen links und rechts von der Fahrzeugmitte an je einem gelenkigen Strang zusammen. So können die Achsen nicht nur unterschiedlich stark einfedern, sondern das Fahrwerk kann sich auch problemlos mehrfach verwinden.

Alle Räder sind kräftig à la Harald mit den Innenzahnrädchen von Rastdifferenzialen angetrieben, die in die Kränze der Reifen greifen. Die Räder selbst sitzen allesamt auf Freilaufnaben.

Nur die dritte Achse ist ungelenkt. Die Lenkung geschieht über die lange Strebe, die von ganz hinten bis ganz vorne läuft. Vorne verschiebt sie der Lenkmotor nach links oder rechts, sodass die erste Achse stark, die zweite etwas weniger, die dritte gar nicht und die vierte (man beachte die Aufhängung der Querstreben vor anstatt hinter den Rädern) leicht in die entgegengesetzte Richtung ausgelenkt werden. Das Ganze ist allerdings hinreichend schwach, sodass die Lenkung im Stand kaum zu gebrauchen ist.
