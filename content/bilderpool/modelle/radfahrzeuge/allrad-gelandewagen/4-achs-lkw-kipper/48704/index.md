---
layout: "image"
title: "Mulde aufgestellt"
date: 2020-05-10T11:38:42+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Mulde wird sehr kräftig von einem S-Motor mit einem Teleskopantrieb angehoben bis zu etwa 45° Aufstellwinkel. Unten im Fahrwerk sieht man den XM-Motor zum Antrieb. Das gesamte Fahrwerk incl. Motor ist "ungefedert", aber in sich hochbeweglich. Der Rahmen aus den vielen BS30 sitzt per ft-Kunststofffedern auf diesem Fahrwerk, und da drauf sitzen Mulde und Führerhaus.
