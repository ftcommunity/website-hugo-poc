---
layout: "image"
title: "Hebemechanik"
date: 2020-05-10T11:39:25+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Untersetzungshebel sind stabil gebaut, denn sie müssen ja den schweren LKW nicht nur tragen, sondern sogar anheben können. Es sind je 3 BS30, seitlich versteift mit je zwei Bauplatten 15x90 mit 6 Zapfen. Oben sitzt ein Gelenkstein in einem Federnocken. Damit der sich nicht verschiebt, ist er beidseitig mit je einem Verbinder 45 und einem Verbinder 30 gesichert.

Die langen Metallachsen gleiten so ausschließlich senkrecht durch die Löcher zweier möglichst weit auseinander liegenden Statikträger.
