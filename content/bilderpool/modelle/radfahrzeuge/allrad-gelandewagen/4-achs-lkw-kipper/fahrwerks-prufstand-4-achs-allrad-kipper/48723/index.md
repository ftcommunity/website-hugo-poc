---
layout: "image"
title: "Power-Motoren 20:1"
date: 2020-05-10T11:39:27+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Da ich keine 8 Power-Motoren mit roter Kappe besitze, kamen insgesamt 2 mit grauer Kappe und eingebauter 20:1-Untersetzung zum Einsatz. Damit sich deren Exzenter im grundsätzlich genauso schnell dreht wie die anderen, bedarf es einer Untersetzung 50:20 = 5:2. Das leistet die Kombination Z10 auf Z40, Z32 (vom inneren Teil des Z40) auf Z20 und Z10 auf Z40: 40:10 * 20:32 = 5:2.
