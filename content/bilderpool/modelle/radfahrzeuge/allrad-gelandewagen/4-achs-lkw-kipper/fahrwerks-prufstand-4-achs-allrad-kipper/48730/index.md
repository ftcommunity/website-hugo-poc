---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-10T11:39:36+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Obenauf kommen die 8 Räder des 4-Achs-LKW in die drehbaren Aufnahmen. Jede davon wir von einem eigenen Power-Motor per Exzentergetriebe angehoben und abgesenkt. Da die Motoren nicht exakt gleich schnell drehen, kommen über die Zeit so ungefähr alle Kombinationen von verschieden weit ein- und ausgefederten Rädern vor. Die Maschine walkt das Fahrwerk sehr ordentlich durch.

Ein Video gibt's unter https://youtu.be/C8JO7x_Njfg
