---
layout: "image"
title: "Blick in 'Fahrtrichtung'"
date: 2020-05-10T11:39:31+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Exzenter treiben Hebel an, die den Hub untersetzen, und die gehen nach oben auf die drehbar gelagerten Radaufnahmen.
