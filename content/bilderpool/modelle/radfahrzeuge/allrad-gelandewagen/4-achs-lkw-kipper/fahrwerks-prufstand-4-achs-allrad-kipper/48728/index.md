---
layout: "image"
title: "Antrieb auf der Steuerungsseite"
date: 2020-05-10T11:39:34+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper und Fahrwerks-Prüfstand02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Je vier Power-Motoren treiben je einen Exzenter für je ein Rad des LKW. Auf dieser Seite befinden sich auch zwei Schalter. Jedes Exzenter drückt nämlich einen Taster, wenn ganz oben ist, und lässt ihn gerade los, wenn es ganz unten ankommt. Je ein Pol der Motoren ist direkt mit der Stromversorgung verbunden. Der jeweils andere Pol ist mit dem Zentralkontakt seines Tasters verbunden. Alle Arbeits- und alle Ruhekontakte aller 8 Taster sind über einen der beiden Schalter mit dem anderen Pol der Stromversorgung verbunden.

Schaltet man den unteren Schalter ein, laufen die Motoren, solange sie sich nicht unten befinden. Schaltet man den oberen ein, laufen sie, solange die Exzenter nicht oben sind. Da wir nur einen "Endlagen"-Taster je Motor haben, ist das Prozedere zum Erreichen einer definierten Position so:

1. Die Exzenter stehen zufällig irgendwo zwischen ganz unten und ganz oben.

2. Man schaltet nur den unteren Schalter ein. Die Motoren, die nicht "unten" stehen, laufen, bis sie unten sind. Einige laufen gar nicht, weil ihr Taster schon "unten" anzeigt, obwohl sie nicht *ganz* unten sind.

3. Man schaltet den unteren Schalter aus und den oberen ein. Alle Motoren drehen jetzt, bis die Exzenter ganz oben stehen. Nun sind alle synchron.

4. Man schaltet den oberen wieder aus und den untern ein. Alle Exzenter fahren definiert ganz herunter.

5. Man schaltet beide Schalter ein. Die Anlage läuft los, und nach kurzer Zeit laufen die Exzenter nicht mehr synchron (was ja erwünscht ist).

Zum definierten Herunterfahren der Anlage geht man entsprechend umgekehrt vor: Die Sequenz Herunterfahren, Hochfahren, Herunterfahren bringt die Anlage in die tiefste Ruheposition.
