---
layout: "image"
title: "Führerhaus"
date: 2020-05-10T11:38:35+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Tür lässt sich aufklappen und gibt den Blick ins leere Innere frei. Was man da untern sieht, ist der Lenkantrieb mit Mitteltaster. Die Ansteuerung ist ganz laut Standard-Schaltung der IR-Fernsteuerung: Ein Motor und ein Mittel-Taster.

Es fanden sich keine freiwilligen Fahrer, die in dem miserabel ausgestattetem Führerhaus auf der Lenkung stehend Dienst tun wollten.
