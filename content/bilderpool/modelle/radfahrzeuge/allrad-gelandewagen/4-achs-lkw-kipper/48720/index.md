---
layout: "image"
title: "Gesamtansicht"
date: 2020-05-10T11:39:02+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein LKW mit Kippmulde. Außer den Rückspiegeln (echte ft-Spiegel vom hobby-4) und zu öffnenden Türen ist das Führerhaus leer und "schlicht". Interessanter sind das Fahrwerk und die Kippmechanik. Es sollte ursprünglich ein Kipper werden, der die Mulde nach drei Seiten (links, rechts, hinten) kippen kann. Das habe ich aber nicht hinbekommen - mehr dazu in späteren Bildern.
