---
layout: "image"
title: "Knickbarer Aufbau"
date: 2022-01-17T16:18:03+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die außenliegenden Achsen können gegenüber den jeweils nächsten stark angehoben oder gesenkt werden. Zusammen mit der Verschränkung (nächstes Bild) ergibt das recht große Federwege.