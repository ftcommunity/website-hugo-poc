---
layout: "image"
title: "Linke Seite"
date: 2022-01-17T16:17:56+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sitzt der IR-Empfänger zwischen den Achsen. Alle Räder sind angetrieben und gefedert, alle Achsen bis auf die dritte sind gelenkt.