---
layout: "image"
title: "Gelenkte Achsen"
date: 2022-01-17T16:18:01+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Differenzial kommt am schwarzen Rastkegelzahnrad an (das Fahrwerk liegt hier auf dem Kopf), geht über die weißen Innenzahnräder eines Rastdifferenzials (das ist Haralds grandiose Idee) auf die Verzahnung der Reifen 60. Die sitzen auf Freilaufnaben auf einer Rastachse mit Platte, die in einem Gelenkstein steckt.

Am Rest der herausstehenden Feder der Platte greift die Lenkung an. Das ist die Achillesferse des Aufbaus, denn die löst sich gerne bei Belastung.