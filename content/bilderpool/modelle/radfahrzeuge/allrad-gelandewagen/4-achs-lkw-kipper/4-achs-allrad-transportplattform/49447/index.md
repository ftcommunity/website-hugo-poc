---
layout: "image"
title: "Rückseite"
date: 2022-01-17T16:17:55+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit den Löchern der Statikstreben der Plattform könnte man die Ladung verzurren.

Die "Stoßstange" dient als Aufhängung der bis nach vorne durchgehenden Strebenstange für die Lenkung.