---
layout: "image"
title: "Rechte Seite"
date: 2022-01-17T16:18:06+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Fahrwerk aus https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/allrad-gelandewagen/4-achs-lkw-kipper/gallery-index/ existierte noch. Der LKW darauf war trotz Leichtbau recht schwer, sodass ich etwas Leichteres darauf haben wollte.