---
layout: "image"
title: "Kurz vorm Zerlegen"
date: 2022-01-17T16:17:57+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Beim Abbau entstand noch dieses Bild. Die oben aufliegenden Querstreben verhindern, dass sich die Radaufhängungen nach vorne oder hinten verdrehen. Die untenliegenden Querstreben verhindern, dass die Räder bei Belastung mit Gewicht die Grätsche machen.