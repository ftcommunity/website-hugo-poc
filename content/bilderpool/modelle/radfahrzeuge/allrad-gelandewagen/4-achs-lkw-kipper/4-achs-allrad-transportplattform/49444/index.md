---
layout: "image"
title: "Abnehmen der Plattform"
date: 2022-01-17T16:17:51+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Statikplatten können nach Entfernen des hinteren Abschlusses nach hinten herausgeschoben werden - sie liegen ansonsten lose auf und werden nur seitlich geführt.