---
layout: "image"
title: "Verschränkung"
date: 2022-01-17T16:18:02+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man zwei Achsen miteinander verschränkt. Die längslaufende Antriebsachse ist das einzig Feste daran. Die Längsstreben halten das Ganze nur locker zusammen.