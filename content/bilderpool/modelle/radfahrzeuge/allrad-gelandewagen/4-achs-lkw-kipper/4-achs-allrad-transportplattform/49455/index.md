---
layout: "image"
title: "Antrieb"
date: 2022-01-17T16:18:04+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Fahrwerk ist recht wabbelig gebaut: Nur mit den Laschen um die Differenziale, längs verlaufenden Streben und der Antriebsachse selbst wird es zusammengehalten.