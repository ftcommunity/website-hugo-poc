---
layout: "image"
title: "Das reine Fahrwerk"
date: 2022-01-17T16:17:48+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Fahrwerk völlig ungeschminkt. Der Motor treibt ein Mitteldifferenzial an. Jede Achse hat ihr eigenes Differenzial.