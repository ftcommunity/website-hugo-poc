---
layout: "image"
title: "Die nicht gelenkte Achse"
date: 2022-01-17T16:18:00+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die dritte Achse ist nicht gelenkt und hat anstelle eines Gelenksteins einen Winkelträger 15, der, was Zapfen und Nuten angeht, dieselbe Geometrie wie ein gelenkstein aufweist und auch ein passendes Loch für die senkrecht stehende Achse bietet.