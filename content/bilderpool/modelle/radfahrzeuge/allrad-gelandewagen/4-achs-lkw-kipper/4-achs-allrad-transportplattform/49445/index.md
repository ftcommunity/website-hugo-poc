---
layout: "image"
title: "Lenkung"
date: 2022-01-17T16:17:53+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der S-Motor mit Hubgetriebe lenkt, der Taster in seiner Verfahrstreckenmitte geht zum IR-Modul.