---
layout: "image"
title: "Führerhaus abgenommen"
date: 2022-01-17T16:17:54+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Führerhaus ist nur an zwei Stellen mit dem Fahrzeug verbunden.