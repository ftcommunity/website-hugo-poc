---
layout: "image"
title: "Proportionallenkung"
date: 2022-01-17T16:17:58+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Lange Achse von ganz hinten bis zur Querverschiebung ganz vorne lenkt die vorderste Achse (ganz links im Bild) stark, die zweite schon weniger, und die hinterste Achse leicht in entgegengesetzter Richtung. Allerdings ist der Strebenverbund nicht sehr starr, was die Lenkweite doch arg einschränkt. Man sieht ja auch, dass die Räder nicht allzu ordentlich stehen.