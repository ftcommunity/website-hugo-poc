---
layout: "image"
title: "Blick aufs Fahrwerk"
date: 2022-01-17T16:17:50+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Nach Herausnehmen der Statikplatten kommt man an den Motorraum. Der hätte zur Not auch noch Platz für den Akku, aber mit den Steckern würde es knifflig.