---
layout: "image"
title: "Abgenommene Plattform"
date: 2022-01-17T16:17:49+01:00
picture: "2022-01-08_4-Achs-Allrad-Transportplattform08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Plattform ist nur mit 8 Federn bei ihren vier BS30 am Fahrwerk befestigt.