---
layout: "image"
title: "Aufstellmechanik"
date: 2020-05-10T11:38:38+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der XS-Motor ist gelenkig befestigt. Die letzte Achse seines U-Getriebes wird direkt beidseitig als Seilwinde verwendet und zieht das schwarze Seil über die oberen Umlenkrollen. Von da geht es herunter zu den Umlenkrollen am Fuß des Teleskoparms und von da wieder nach ganz oben in den querliegenden BS7,5. Es ist also nur ein einziges Stück Faden, was da letztlich von einer Achsenseite des U-Getriebes bis zur anderen verläuft.

Die Kette des Teleskoparms ist in einem BS7,5 direkt unterhalb der oberen Umlenkrollen per Rastkettenglied befestigt (der BS7,5 ist von einer Platte 15x15 verdeckt).
