---
layout: "image"
title: "Teleskopantrieb"
date: 2020-05-10T11:38:37+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kette wiederum ist auf der anderen Seite des Teleskoparms an der Unterseite der Kippmulde befestigt und heb sie so an.

Für beiden Endlagen gibt es Endlagentaster mit Diode, so dass man mit einem einzigen Ausgang der IR-Fernsteuerung dafür auskommt.
