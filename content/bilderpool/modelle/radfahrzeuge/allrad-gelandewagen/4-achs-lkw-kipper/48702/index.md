---
layout: "image"
title: "Antrieb"
date: 2020-05-10T11:38:39+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Vom XM-Motor geht es über ein längs angeordnetes Differential nach vorne und hinten. Jede Achse verfügt nochmal über ein quer liegendes Rastdifferenzial.
