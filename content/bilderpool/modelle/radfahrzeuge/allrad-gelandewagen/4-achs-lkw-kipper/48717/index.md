---
layout: "image"
title: "Fehlgeschlagene Vorversuche (1)"
date: 2020-05-10T11:38:58+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Um die Mulde nach drei Seiten zu kippen, gab es eine ganze Reihe Vorversuche, die allesamt fehlschlugen. Die auftretenden Kräfte sind einfach drastisch zu groß.

Variante 1: Hubgetriebe mit kardanischem Ende. Keine Chance. Biegt sich lieber durch.
