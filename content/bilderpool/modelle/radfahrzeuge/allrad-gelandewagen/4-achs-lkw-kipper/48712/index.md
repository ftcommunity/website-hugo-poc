---
layout: "image"
title: "Fehlgeschlagene Vorversuche (3)"
date: 2020-05-10T11:38:52+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ebenfalls völlig chancenlos: Ein kompakter Scherenhub aus I-Streben 60 mit Löchern. Das verbiegt sich viel lieber, als Last zu schieben.
