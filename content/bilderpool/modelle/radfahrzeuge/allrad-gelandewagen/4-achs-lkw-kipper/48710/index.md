---
layout: "image"
title: "Fehlgeschlagene Vorversuche (3)"
date: 2020-05-10T11:38:49+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier die Aufhängung des Scherenhubs. Nett, aber halt bei weitem nicht kräftig genug für die große Mulde. Man beachte, dass der Scherenhub kardanisch aufgehängt ist: Der Baustein 30 mit Bohrung ist ein Gelenk, und die Wurzel des Scherenhubs hängt auch drehbar in den beiden roten S-Kupplungen.
