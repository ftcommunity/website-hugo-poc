---
layout: "image"
title: "Radantrieb"
date: 2020-05-10T11:38:59+02:00
picture: "2019-05-20 4-Achs-Allrad-Kipper11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sieht man den Antrieb eines Rades. Unter dem weißen Zahnrad kommt ein Rastkegelrad an, dass es antreibt.
