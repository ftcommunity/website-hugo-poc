---
layout: "comment"
hidden: true
title: "18614"
date: "2014-01-26T10:32:26"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Bei den Klemmzahnrädern habe ich Taschentücher  bzw. Mitnehmer  (hier bei den zwei Z20 zu sehen)  mitgeklemmt. 
Die Rastachsen halten die Belastungen des Modells problemlos.
Das Modell wiegt etwas mehr als 3 kg.

Deine Lösung des Planetengetriebes gefällt mir sehr gut!