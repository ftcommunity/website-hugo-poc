---
layout: "image"
title: "Rechte Seite"
date: "2014-01-12T17:58:03"
picture: "pickup08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller, Sebastian Erler"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38044
- /detailsec88.html
imported:
- "2019"
_4images_image_id: "38044"
_4images_cat_id: "2829"
_4images_user_id: "1924"
_4images_image_date: "2014-01-12T17:58:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38044 -->
