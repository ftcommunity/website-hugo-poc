---
layout: "image"
title: "Verwindung2"
date: "2014-01-12T17:58:03"
picture: "pickup13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Reinmüller, Sebastian Erler"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38049
- /details9179.html
imported:
- "2019"
_4images_image_id: "38049"
_4images_cat_id: "2829"
_4images_user_id: "1924"
_4images_image_date: "2014-01-12T17:58:03"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38049 -->
