---
layout: "image"
title: "Fernsteuerung"
date: "2006-03-15T21:07:36"
picture: "Allrad_6.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5897
- /detailsa2df-3.html
imported:
- "2019"
_4images_image_id: "5897"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5897 -->
Zur Steuerung von allen motorisierten Fahrzeugen mit 2 Motoren für Antrieb und Lenkung sowie eigener Stromversorgung (Akku-Pack) am Fahrzeug nutze ich diese selbstgebaute Kabelfernsteuerung. Ich denke, dass ist wirklich ne gute Alternative zur teuren Infrarot-Anlage! Vielleicht gefällt sie Euch ja...

Im Inneren des original Fischertechnik-Spiralschlauchs (1 Meter; gibt's bei Knobloch) liegen 3 Kabel (1 vom Akku-Pack zur Fernsteuerung und 2 von der Fernsteuerung zu den beiden Motoren). Lenken lässt sich mit den beiden Tastern, gefahren wird mit dem Schiebeschalter. Das Ding liegt überraschend gut in der Hand und funktioniert überraschend perfekt und komfortabel. Und ist ALLES original Fischertechnik!