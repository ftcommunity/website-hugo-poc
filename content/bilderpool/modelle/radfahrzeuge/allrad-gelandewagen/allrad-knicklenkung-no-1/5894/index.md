---
layout: "image"
title: "Allrad 4"
date: "2006-03-15T21:07:36"
picture: "Allrad_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/5894
- /details91db-2.html
imported:
- "2019"
_4images_image_id: "5894"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5894 -->
Die Knicklenkung muss im Alltag sehr hohe Kräfte aufnehmen bzw.übertragen und musste daher sehr stabil gebaut werden. Das Zahnrad habe ich am Vorderwagen mit 2 roten Winkelsteinen 38423 in den Zähnen fixiert, nicht sehr elegant... Hat jemand ne bessere Lösung?