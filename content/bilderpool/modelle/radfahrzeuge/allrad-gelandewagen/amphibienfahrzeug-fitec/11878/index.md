---
layout: "image"
title: "Amphi"
date: "2007-09-19T19:50:45"
picture: "Amphi1.jpg"
weight: "1"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11878
- /detailsd1ed.html
imported:
- "2019"
_4images_image_id: "11878"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-09-19T19:50:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11878 -->
Ich benutze den Boatsrumpf. Wahrscheinlich werde ich im Wasser mit den Rädern Lenken und mit einem Luftpropeller antreiben.
