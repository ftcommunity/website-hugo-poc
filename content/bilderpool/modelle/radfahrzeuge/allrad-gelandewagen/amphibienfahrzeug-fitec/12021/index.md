---
layout: "image"
title: "Antrieb"
date: "2007-09-26T15:51:59"
picture: "Amphi4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12021
- /details989b-2.html
imported:
- "2019"
_4images_image_id: "12021"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-09-26T15:51:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12021 -->
Man sieht die Kette, die die Kraft vom Getriebe nach unten auf die Räder leitet. Die Kette ist auch mein derzeitiges Problem, da sie immer rattert. Allerdings nur unter Belastung, also wenn ich das Fahrzeug in die Luft halte geht alles.
