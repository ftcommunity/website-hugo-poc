---
layout: "image"
title: "Amphi"
date: "2007-09-19T19:50:46"
picture: "Amphi2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11879
- /details8bd5.html
imported:
- "2019"
_4images_image_id: "11879"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-09-19T19:50:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11879 -->
Der Antrieb wird hinten über eine Kette hochgeleitet. Ich habe vor ein Getriebe mit mehreren Gängen einzubauen. Mindestens einer zum Fahren und einer für Luft(propeller). Vielleicht aber auch mehr.
