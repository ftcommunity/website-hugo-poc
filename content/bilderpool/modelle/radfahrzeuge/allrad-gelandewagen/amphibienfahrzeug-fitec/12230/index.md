---
layout: "image"
title: "Amphi Explorer"
date: "2007-10-15T19:57:55"
picture: "Amphi6.jpg"
weight: "6"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12230
- /details91ea.html
imported:
- "2019"
_4images_image_id: "12230"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-15T19:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12230 -->
Das ist die neue Version. Der Radantrieb war mit Getriebe schlecht realisierbar und auch nicht besonders geländetauglich, da ich wenn ich aus dem wasser raus fahre eine Steigung hoch muss und das geht so nicht. Deswegen jetzt mit Ketten. Ohne Gleichlaufgetriebe. Es hat einen Schlüsselschalter, der das Interface einschaltet. An jeder Kette ist durch eine 1:4 Übersetzung und 2 Kegelzahnräder eine selbstgebaute Schiffsschraube. Der Wasserantrieb ist eine Gemeinschatsproduktion fitec/tim-tech Das funktioniert wirklich!!! Auch beidiesem Modell ist der ft-boatsrumpf verbaut. Das Interface ist zur Fernsteuerung über den Handsender des IR-Control-Stets. Immoment folgende Funtionen:
-Vorwärts und Rückwärts
-Links und rechts
-Automatische Erkennung vom Lenken, also wenn es still steht, lenken die Ketten entgegengesetzt, wenn ich fahre wird ein Motor gestoppt und der andere läuft weiter
-Licht ein- und ausschaltbar per Taste
Geplant:
-Summer der Signal gibt wenn Betriebsspannung stark sinkt
-Elektrode die Wasser erkennt und Positionslampen ein- und ausschaltet
-evtl. Fotowiderstand zum einschalten der Scheinwerfer
-evtl. Seilwinde
-programmisierter Gleichlau beider Ketten
