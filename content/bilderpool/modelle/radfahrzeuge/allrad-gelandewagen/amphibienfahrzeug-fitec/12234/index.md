---
layout: "image"
title: "Amphi Explorer"
date: "2007-10-15T19:57:56"
picture: "Amphi10.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12234
- /details0bb6-3.html
imported:
- "2019"
_4images_image_id: "12234"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-15T19:57:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12234 -->
Von hinten. Die roten Schutzbleche schützn vor Spritzwasser.
