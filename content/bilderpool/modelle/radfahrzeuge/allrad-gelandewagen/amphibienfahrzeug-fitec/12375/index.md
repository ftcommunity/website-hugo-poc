---
layout: "image"
title: "Bodenabstand"
date: "2007-11-02T08:49:08"
picture: "Amphi16.jpg"
weight: "16"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12375
- /details778e.html
imported:
- "2019"
_4images_image_id: "12375"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-11-02T08:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12375 -->
