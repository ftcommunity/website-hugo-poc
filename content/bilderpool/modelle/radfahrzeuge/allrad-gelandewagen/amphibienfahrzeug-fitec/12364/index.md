---
layout: "image"
title: "Bodenabstand"
date: "2007-10-28T12:45:57"
picture: "Amphi12.jpg"
weight: "12"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/12364
- /details694f-2.html
imported:
- "2019"
_4images_image_id: "12364"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-28T12:45:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12364 -->
Ganz ordentlich, finde ich.
