---
layout: "image"
title: "Heckantrieb und Aufhänung"
date: "2016-03-02T12:54:17"
picture: "achseinzel6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/42973
- /details8676.html
imported:
- "2019"
_4images_image_id: "42973"
_4images_cat_id: "3195"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42973 -->
Doppelt ausgeführt um am Ende auch Auflieger tragen zu können