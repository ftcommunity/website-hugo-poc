---
layout: "image"
title: "Einzelradaufhängung Front und Lenkmechanik"
date: "2016-03-02T12:54:17"
picture: "achseinzel8.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/42975
- /details7611.html
imported:
- "2019"
_4images_image_id: "42975"
_4images_cat_id: "3195"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42975 -->
Größe Schwierigkeit war es ein verbiegen der der Befestigung am Mini-Achsschenkel zu verhindern. Interessanterweise klipsen die Innenteile der Gelenke wunderbar drauf und bieten die m.E. beste Lösung.