---
layout: "overview"
title: "roter Jeep mit alter IR Fernbedienung 30344"
date: 2020-02-22T07:56:28+01:00
legacy_id:
- /php/categories/3405
- /categories9255.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3405 --> 
Hab zwei Stück davon in einer Sammlung auf ebay erworben.
Ausstattung: 

- mit Bausteinen 15 und 30 in roter Farbe
- Fahrmotor = Power-Motor mit Differential
- Lenkmotor = S-Motor mit Taster an Mittelstellung
- Stromversorgung = roter NiMH Akku mit 1500 mAh
- alte IR Fernbedienung 30344

Kennt jemand dieses Modell aus irgendeinem Baukasten?
Oder war das mal ein Promotion Modell von Fischertechnik?