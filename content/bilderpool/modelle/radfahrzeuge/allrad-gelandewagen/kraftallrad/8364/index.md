---
layout: "image"
title: "Von unten"
date: "2007-01-11T18:56:54"
picture: "Kraftallrad4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8364
- /details9fdf.html
imported:
- "2019"
_4images_image_id: "8364"
_4images_cat_id: "771"
_4images_user_id: "456"
_4images_image_date: "2007-01-11T18:56:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8364 -->
Von unten. Man sieht alle 3 Differebziale und die Lenkung.
