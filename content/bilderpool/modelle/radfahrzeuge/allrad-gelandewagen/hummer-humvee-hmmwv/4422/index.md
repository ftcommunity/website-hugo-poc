---
layout: "image"
title: "Hummer-12.JPG"
date: "2005-06-12T14:29:14"
picture: "Hummer-12.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4422
- /details0b22.html
imported:
- "2019"
_4images_image_id: "4422"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:29:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4422 -->
Die Vorderachse in voller Pracht.

In fast dieser Form hatte ich das Thema 'Allrad' beim Modell 'Fendt' beiseite gelegt. Mit den dicken Reifen, die der Siggi da aufgetan hat, kann man sich aber den voluminösen Aufbau wieder leisten.

Die weißen Kegelzahnräder stammen aus schwarzen ft-Differenzialen. Die grauen Scheiben sind auch Original-ft, aber keine Ahnung, wozu die mal gedacht waren. 

Metallachsen sind unbedingt nötig, sonst biegt sich alles auseinander.

Die Antriebswelle ist in einer Schwinge gelagert, die um die Diff-Achsen herum...naja, ...schwingen kann.