---
layout: "image"
title: "Hummer-07.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-07.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4417
- /details7816.html
imported:
- "2019"
_4images_image_id: "4417"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4417 -->
Das Mitteldifferenzial sitzt auf einem Schlitten, der in den Winkelträgern 120 vor- und zurückgleiten kann. Das ist einmal notwendig zum Zusammenbauen (gibt sonst graue Haare), aber auch, weil nur die Welle zur Hinterachse ein Teleskopstück drin hat. Wenn das Fahrzeug also vorne tief einfedert, schiebt es den Schlitten etwas nach hinten. Dabei bleiben aber die Zahnräder immer noch im Eingriff.