---
layout: "image"
title: "Hummer-31.JPG"
date: "2005-10-06T17:25:26"
picture: "Hummer-31.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad", "modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5058
- /details6285-3.html
imported:
- "2019"
_4images_image_id: "5058"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5058 -->
Der BS15-Loch wurde auf 10,5 mm Höhe reduziert. Dadurch wird das krumme Maß im ft-Differenzial ausgeglichen und jetzt rasten die Rastachsen richtig ein. Die Nut hat zwar noch beide Seitenwände, aber *klemmen* tut ein ft-Zapfen darin nicht mehr.
