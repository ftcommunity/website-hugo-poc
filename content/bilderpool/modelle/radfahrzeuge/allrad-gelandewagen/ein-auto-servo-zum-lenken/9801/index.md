---
layout: "image"
title: "Ferngesteuertes Auto"
date: "2007-03-27T14:05:50"
picture: "auto3.jpg"
weight: "3"
konstrukteure: 
- "1958230dermitdemhut"
fotografen:
- "1958230dermitdemhut"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9801
- /details1207.html
imported:
- "2019"
_4images_image_id: "9801"
_4images_cat_id: "884"
_4images_user_id: "453"
_4images_image_date: "2007-03-27T14:05:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9801 -->
In der Mitte sieht man das Servo, welches die Lenkung antreibt.