---
layout: "image"
title: "federung"
date: "2006-12-30T09:56:29"
picture: "gelaendewagen06.jpg"
weight: "6"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8213
- /detailsc29a.html
imported:
- "2019"
_4images_image_id: "8213"
_4images_cat_id: "757"
_4images_user_id: "366"
_4images_image_date: "2006-12-30T09:56:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8213 -->
das auto wird mit insgesamt 12 federn gefedert