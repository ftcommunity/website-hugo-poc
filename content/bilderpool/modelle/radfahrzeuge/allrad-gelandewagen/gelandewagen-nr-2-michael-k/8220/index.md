---
layout: "image"
title: "lenkung"
date: "2006-12-30T09:56:36"
picture: "gelaendewagen13.jpg"
weight: "13"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8220
- /details105d-3.html
imported:
- "2019"
_4images_image_id: "8220"
_4images_cat_id: "757"
_4images_user_id: "366"
_4images_image_date: "2006-12-30T09:56:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8220 -->
hier ist der Antrieb der Lenkung zu sehen