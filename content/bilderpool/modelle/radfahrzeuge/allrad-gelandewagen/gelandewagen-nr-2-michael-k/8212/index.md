---
layout: "image"
title: "kraftübertragung"
date: "2006-12-30T09:56:29"
picture: "gelaendewagen05.jpg"
weight: "5"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8212
- /detailsbe94.html
imported:
- "2019"
_4images_image_id: "8212"
_4images_cat_id: "757"
_4images_user_id: "366"
_4images_image_date: "2006-12-30T09:56:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8212 -->
Hier sieht man die Kraftübertragung vom rad zum Diff.