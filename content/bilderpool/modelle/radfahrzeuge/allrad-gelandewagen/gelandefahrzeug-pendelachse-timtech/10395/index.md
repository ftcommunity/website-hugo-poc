---
layout: "image"
title: "Auto"
date: "2007-05-13T16:52:37"
picture: "PICT0032.jpg"
weight: "3"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10395
- /detailsc368.html
imported:
- "2019"
_4images_image_id: "10395"
_4images_cat_id: "947"
_4images_user_id: "590"
_4images_image_date: "2007-05-13T16:52:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10395 -->
Hier sieht man den Motor und etwas von der Penndelachse.
