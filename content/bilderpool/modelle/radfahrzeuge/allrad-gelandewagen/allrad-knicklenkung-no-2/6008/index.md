---
layout: "image"
title: "Knick-4x4c-04.JPG"
date: "2006-04-02T14:08:21"
picture: "Knick-4x4c-04.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6008
- /detailsfd2c-2.html
imported:
- "2019"
_4images_image_id: "6008"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:08:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6008 -->
