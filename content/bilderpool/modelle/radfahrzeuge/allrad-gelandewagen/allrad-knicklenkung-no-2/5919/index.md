---
layout: "image"
title: "Knick-4x4-11.JPG"
date: "2006-03-26T12:35:22"
picture: "Knick-4x4-11.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5919
- /detailse073.html
imported:
- "2019"
_4images_image_id: "5919"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5919 -->
