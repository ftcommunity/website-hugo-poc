---
layout: "image"
title: "Knick-4x4b-01.JPG"
date: "2006-04-02T13:59:24"
picture: "Knick-4x4b-01.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6003
- /details01a7.html
imported:
- "2019"
_4images_image_id: "6003"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T13:59:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6003 -->
Das ist Variante B, mit alltagstauglicher Lenkung. Das Führerhaus sitzt jetzt auf den Motoren, der Akku hat noch keinen Platz gefunden.
