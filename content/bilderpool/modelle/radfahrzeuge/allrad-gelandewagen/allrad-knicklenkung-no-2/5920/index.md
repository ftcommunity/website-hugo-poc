---
layout: "image"
title: "Knick-4x4-12.JPG"
date: "2006-03-26T12:35:47"
picture: "Knick-4x4-12.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5920
- /detailse063-2.html
imported:
- "2019"
_4images_image_id: "5920"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:35:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5920 -->
