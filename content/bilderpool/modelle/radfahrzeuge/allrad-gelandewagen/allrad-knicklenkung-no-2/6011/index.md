---
layout: "image"
title: "Knick-4x4c-09.JPG"
date: "2006-04-02T14:10:31"
picture: "Knick-4x4c-09.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6011
- /details5f86.html
imported:
- "2019"
_4images_image_id: "6011"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:10:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6011 -->
Das Gefährt von unten, bei vollem Lenkeinschlag.
