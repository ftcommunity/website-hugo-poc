---
layout: "image"
title: "Knick-4x4b-04.JPG"
date: "2006-04-02T14:01:49"
picture: "Knick-4x4b-04.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6005
- /details0200-2.html
imported:
- "2019"
_4images_image_id: "6005"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:01:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6005 -->
