---
layout: "image"
title: "Holder Knicklenker 06"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34157
- /detailsa96b-2.html
imported:
- "2019"
_4images_image_id: "34157"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34157 -->
Die Ladefläche ist glatt und gut nutzbar.

Rechts im Bild der Schalter für das Blinklicht.