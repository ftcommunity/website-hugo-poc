---
layout: "image"
title: "Holder Knicklenker 03"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34154
- /details0cde.html
imported:
- "2019"
_4images_image_id: "34154"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34154 -->
Das Kardangelenk sitzt genau mittig zwischen den Achsen, so dass der Allradantrieb auch ohne Mitteldifferenzial kinematisch sauber ist.

Zur besseren Gewichtsverteilung musste der 9V-Block zwingend über die Vorderachse, was gut gelungen ist.