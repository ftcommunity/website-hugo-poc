---
layout: "image"
title: "Holder Knicklenker 07"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_07.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34158
- /detailsaacc.html
imported:
- "2019"
_4images_image_id: "34158"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34158 -->
Das Kabelgewirr ist nicht so schön, aber kaum zu vermeiden.