---
layout: "image"
title: "Holder Knicklenker 09"
date: "2012-02-12T16:56:23"
picture: "Holder_Knicklenker_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34160
- /details0fc3-2.html
imported:
- "2019"
_4images_image_id: "34160"
_4images_cat_id: "2530"
_4images_user_id: "328"
_4images_image_date: "2012-02-12T16:56:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34160 -->
Kürzer lässt sich ein Knicklenker mit 2 Differenzialen wohl kaum darstellen.

Ich wollte eine schmale realistische Spurweite, so dass die Lagerung der Achsen vor allem hinten etwas exotisch geworden ist.

Die Abstandsringe an der Vorderachse sorgen dafür, dass die Scheinwerfer zwischen die Räder passen.