---
layout: "comment"
hidden: true
title: "8520"
date: "2009-02-16T07:40:03"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Der Schalensitz 31984 und das dazu passende FT-Männchen funktionieren nicht in Kombination mit dem Reifen 50 bei PKW-Proportionen.

Klar habe ich die Sitze ausprobiert, aber der Reifen ist 2/3 so hoch wie das Männchen; das passt nicht zusammen. Sitz und Reifen sind nahezu gleich groß. Das sieht zwar witzig aus, ist dann aber eher ein Monster-Truck mit überzeichneten Proportionen.

In meinem Modell war durch die Anorndung der Radnabenmotoren auch die Breite vorgegeben bzw. fix. Die Schalensitze waren dan auch viel zu schmal.

Männchen/Sitz und Reifen 50 passen meiner Meinung nach nur in LKW-Modellen zusammen, aber nicht bei PKWs.

Gruß, Thomas