---
layout: "comment"
hidden: true
title: "8528"
date: "2009-02-17T08:08:43"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Harald, Du erkennst sicher deutlich meine Inspiration: Das absolut gelungene Fahrerhaus von Deinem "Explorer"!

Ich hatte zunächst einen anderen Aufbau, dachte aber angesichtst Deines Modells, dass ich das sicher auch kann. Naja, das ist dann das Ergebnis.

Harald, meine Muse! ;o)

Gruß, Thomas