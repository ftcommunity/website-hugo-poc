---
layout: "image"
title: "City-Allrad 1"
date: "2009-02-15T18:22:37"
picture: "City-Allrad_09.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/17416
- /details17af.html
imported:
- "2019"
_4images_image_id: "17416"
_4images_cat_id: "1567"
_4images_user_id: "328"
_4images_image_date: "2009-02-15T18:22:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17416 -->
Das ist das Modell eines zukünftigen 2-sitzigen Stadtfahrzeugs mit Elektroantrieb, konzeptionell ähnlich dem heutigen Smart Fortwo.

Der Antrieb erfolgt über 4 Radnabenmotoren, dargestellt durch Mini-Motoren, auf denen die Felgen 30 festgeklemmt sind. Die Fahrleistungen sind naturgemäß nicht die Wucht, aber das Fahrzeug kommt überraschend gut voran und wird – einmal in Bewegung – rasend schnell.

Um die Fahrleistungen zu verbessern, war konsequenter Leichtbau das oberste Gebot. Daher dient als Spannungsquelle nur ein leichter 9V-Blockakku. Die 4 parallel geschalteten Motoren ziehen natürlich viel Strom und saugen ihn in wenigen Minuten leer. Bloß gut, dass ich einige Wechselakkus habe, die sich – dank einfacher Zugänglichkeit im Heck – schnell und einfach wechseln lassen.

Um im Großstadtdschungel passend unterwegs zu sein, habe ich dem Fahrzeug eine robuste und geländewagenähnliche Optik verpasst.

Die Wendigkeit des Fahrzeugs wird durch die Allradlenkung erhöht, die die Hinterräder mit halbem Lenkwinkel entgegengesetzt zu den Vorderrädern einschlägt. Die Steuerung erfolgt per Servo (mit verlängertem Lenkhebel) und aktuellem Control Set. Beim Lenken dreht sich das Lenkrad natürlich mit – so, wie es sein soll!

Zwischen den Sitzen ist ein „Schalthebel“ integriert, der zum realitätsnahen Starten des Fahrzeugs dient. Legt man ihn nach vorn um, wird über einen Mini-Taster die Spannungsversorgung des Empfängers hergestellt, und der Fahrspaß beginnt... ;o)