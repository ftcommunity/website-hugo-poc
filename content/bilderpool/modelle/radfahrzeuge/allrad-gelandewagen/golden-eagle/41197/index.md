---
layout: "image"
title: "Golden Eagle Details 4"
date: "2015-06-16T21:06:00"
picture: "goldeneagle14.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41197
- /details2b63.html
imported:
- "2019"
_4images_image_id: "41197"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41197 -->
Fahrer und Beifahrer müssen ein wenig auf ihre Finger achten, damit sie nicht in das Getriebe geraten.