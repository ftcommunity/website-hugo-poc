---
layout: "image"
title: "Golden Eagle Ansichten 3"
date: "2015-06-16T21:06:00"
picture: "goldeneagle08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41191
- /details333e-2.html
imported:
- "2019"
_4images_image_id: "41191"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41191 -->
Ansicht von oben...