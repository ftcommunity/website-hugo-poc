---
layout: "image"
title: "Golden Eagle Ansichten 2"
date: "2015-06-16T21:06:00"
picture: "goldeneagle07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41190
- /details2b87.html
imported:
- "2019"
_4images_image_id: "41190"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41190 -->
Vorne unter dem Kotflügel ist etwas Ballast befestigt, weil das linke Vorderrad in schwerem Gelände trotz ASR manchmal durchdreht. Im Motorraum ist weiterer Ballast, dazu kommt noch ein Bild.