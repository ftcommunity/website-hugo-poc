---
layout: "image"
title: "Golden Eagle Details 7"
date: "2015-06-16T21:06:00"
picture: "goldeneagle16.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41199
- /details043c.html
imported:
- "2019"
_4images_image_id: "41199"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41199 -->
Motorhaube abgebaut