---
layout: "image"
title: "Golden Eagle Gesamtansicht 2"
date: "2015-06-16T21:06:00"
picture: "goldeneagle02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41185
- /detailsea05.html
imported:
- "2019"
_4images_image_id: "41185"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41185 -->
Als Basis dient das Chassis http://www.ftcommunity.de/categories.php?cat_id=3079
Hat Allrad und ein ASR und wird über eine Kabelfernbedienung gesteuert. Die Akkus sind aber mit an Bord.