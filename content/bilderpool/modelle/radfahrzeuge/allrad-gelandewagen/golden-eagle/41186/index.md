---
layout: "image"
title: "Golden Eagle Gesamtansicht 3"
date: "2015-06-16T21:06:00"
picture: "goldeneagle03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41186
- /detailsbae0.html
imported:
- "2019"
_4images_image_id: "41186"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41186 -->
Geländewagen- und Allradmodelle gibt es ja schon sehr viele und sehr schöne hier in der ftc. Mich hat das Thema trotzdem gereizt, vor allem wegen dem von Dirk Fox ausgeschriebenen Wettbewerb (http://forum.ftcommunity.de/viewtopic.php?f=19&t=441&p=2992&hilit=rampe#p2981). Das hier ist natürlich außer Konkurrenz wegen der geschnitzten Kardangelenke, aber man muss sich ja erst mal rantasten.