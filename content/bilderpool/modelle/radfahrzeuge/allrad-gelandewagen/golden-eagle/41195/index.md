---
layout: "image"
title: "Golden Eagle Details 2"
date: "2015-06-16T21:06:00"
picture: "goldeneagle12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41195
- /details9c7e-2.html
imported:
- "2019"
_4images_image_id: "41195"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41195 -->
Die Karosserie ist modular und man kann sie leicht abnehmen, ...