---
layout: "image"
title: "Mini-Allrad 3"
date: "2006-06-01T21:29:46"
picture: "Mini-Allrad_10.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/6351
- /detailsbea4.html
imported:
- "2019"
_4images_image_id: "6351"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2006-06-01T21:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6351 -->
Hier der Antrieb und die variable/teilsperrende Differenzialsperre im Detail:

Der Power-Motor treibt über ein Z10, Z15 und Z30 die Achse an, die außen parallel zur Differenzialachse liegt. Dort sind zwei Rast-Z10 STARR befestigt. Eines dieser Z10 treibt das mittlere Differenzial an, das die Kraft zu Vorder- und Hinterachse verteilt. Über je zwei Rast-Kegelräder wird das jeweilige Differenzial angetrieben. Vor dem mittleren Differenzial ist ein Z20 VOLLSTÄNDIG LOCKER auf die Welle geschoben, dass von dem zweiten Z10 der Antriebsachse angetrieben wird. Das Z20 neben dem mittleren Differenzial hat also immer die gleiche Geschwindigkeit, egal, wie die Kraft durch das mittlere Differenzial nach vorn und/oder hinten verteilt wird. Allein die Reibung des LOCKEREN Z20 reicht aus, um PERMANENT ein recht starkes Drehmoment an die durchdrehende Achse zu verteilen! Ich war selbst verblüfft, aber es funktioniert wirklich wunderbar! Das Auto fährt ohne Reifen-Rutschen problemlos um enge Kurven, um dann im Gelände trotzdem weit voranzukommen, da immer beide Achsen angetrieben werden. Man muss das Z20 neben dem Differenzial auch nie anziehen, um die Sperrwirkung zu verstärken; die Reibung reicht aus.

Alles in allem ist die Sperre auch noch superkompakt, was zum Konzept des Mini-Allrads passt.