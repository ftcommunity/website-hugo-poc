---
layout: "image"
title: "Lagerung der Antriebswelle"
date: "2017-07-12T23:40:59"
picture: "suvx10.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46093
- /detailsd9a3.html
imported:
- "2019"
_4images_image_id: "46093"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46093 -->
Die schwarze Achse wird im unteren BS7,5 gelagert. Der S-Riegel musste abgeflacht werden, damit er in die schmale Nut passt.

Der Antrieb 1:1 mittels zweier Z15 hat zuviel Schlupf, deswegen sind die Umstände mit den Z10 hier nötig geworden.
