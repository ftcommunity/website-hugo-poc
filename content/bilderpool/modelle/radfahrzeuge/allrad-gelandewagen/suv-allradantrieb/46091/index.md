---
layout: "image"
title: "Heckpartie von unten"
date: "2017-07-12T23:40:59"
picture: "suvx08.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46091
- /details91a8.html
imported:
- "2019"
_4images_image_id: "46091"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46091 -->
Die Hinterachse besteht aus zwei Schwingen (ab dem roten Gelenkstein nach hinten), die über die Messingachse miteinander verbunden sind. Die Hinterräder (auf Freilaufnabe müssen im genau richtigen Abstand zum Rast-Z10 angebracht sein -- und auch bleiben. Da ist hier noch Bedarf für Verbesserung, weil diese Anordnung mit der Zeit wandert.

Der Lattenrost aus S-Streben und Flachträgern kann hinten gelöst und dann am Stück heruntergeschwenkt werden. Er bildet eine Mulde, die den Akku aufnimmt.
