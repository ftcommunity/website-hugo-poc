---
layout: "image"
title: "Ladebalken rechts"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39476
- /details3ab4.html
imported:
- "2019"
_4images_image_id: "39476"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39476 -->
Die rechte Seilwinde wird durch einen eigenen Taster auf Zug überwacht. Die vier Gelenksteine am rechten Rand gehören zur Mechanik, die die Containerschlösser (S-Riegel 8, unter den Achsaufnahmen) dreht.