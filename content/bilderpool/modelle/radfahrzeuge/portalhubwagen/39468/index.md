---
layout: "image"
title: "Totale von links"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39468
- /details7323.html
imported:
- "2019"
_4images_image_id: "39468"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39468 -->
Ein Portalhubwagen (straddle carrier) dient dazu, Standard-Container in Überseehäfen zu rangieren. Dazu fahren sie genau über einen Container (oder einen Stapel davon), senken einen Ladebalken auf den obersten Container, verriegeln einen Satz von Containerschlössern, die den Ladebalken mit dem Container verbinden, und können dann den Container anheben und auf einem anderen Stapel oder einem LKW absetzen. Portalhubwagen werden je nach Stapelhöhe gekennzeichnet: "3+1" bedeutet, dass das Gerät auf einen Stapel von drei Containern noch einen weiteren drauf setzen kann. Mein Portalhubwagen ist ein  "2+1"-Typ. 

Portalhubwagen haben typischerweise Allradantrieb und Allradlenkung. Dieser hier hat das auch.