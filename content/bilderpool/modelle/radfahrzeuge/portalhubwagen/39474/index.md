---
layout: "image"
title: "Ladebalken"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39474
- /details0af2.html
imported:
- "2019"
_4images_image_id: "39474"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39474 -->
Dieses Element wird zwischen den Fahrwerkshälften auf und ab bewegt, mit und ohne untergehängtem Container. Es ist auf den Metallachsen links und rechts schwimmend gelagert, d.h. die Lagerungen erlauben freie Bewegung in der horizontalen Ebene, um den Ladebalken passend auf den Container zu zentrieren und dann die Containerschlösser (S-Riegel) zu verriegeln.

Die beiden Seile (hinten und vorn) werden mit je einem Taster überwacht: nur wenn beide Taster "Seil ohne Zug" angeben, bekommt der Motor in Bildmitte Strom und kann den Container ent- oder verriegeln. Wenn nicht, geht der Strom auf die rote Lampe (unterhalb vom Motor).