---
layout: "image"
title: "Totale, halb links"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39469
- /details9c82.html
imported:
- "2019"
_4images_image_id: "39469"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39469 -->
Es fällt auf, dass die ft-Fahrerkabine einmal mehr *richtig* herum verwendet wird -- die breite Seite gehört einfach nach oben!! ;-)
