---
layout: "image"
title: "straddlecarrier6.jpg"
date: "2015-08-03T22:21:04"
picture: "IMG_1897mit.JPG"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41714
- /detailsd08f-2.html
imported:
- "2019"
_4images_image_id: "41714"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:21:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41714 -->
Das ist die Frosch-auf-einem-Container-Perspektive, wenn der Portalhubwagen kommt. Durch die zusätzliche Kette ist der Innenraum noch enger geworden und die Container stoßen beim heben mal links und mal rechts irgendwo an.
