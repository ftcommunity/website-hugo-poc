---
layout: "image"
title: "Container.jpg"
date: "2014-10-02T16:40:44"
picture: "Pistenraupe05.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39497
- /details94eb-2.html
imported:
- "2019"
_4images_image_id: "39497"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T16:40:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39497 -->
Die Container haben innen je 6 Platten 15x30, die dafür sorgen, dass die Stützen an den äußeren Positionen verbleiben.

Auf der Unterseite (rechts oben) befinden sich 6 weitere solcher Platten: die mittleren halten die beiden Bodenplatten zusammen, die äußeren sorgen für gleichmäßige Auflage.

Der Container links oben hat zwei Bodenplatten 90x90 und ist deswegen etwas anders ausgeführt.

Bei allen Containern gilt: 
- das dritte Loch auf der Oberseite muss frei bleiben für das Containerschloss
- die S-Riegel seitlich davor müssen entweder von innen gesteckt sein oder es müssen dort Verschlussriegel 37232 eingesetzt werden.
- die S-Riegel im Deckel (hier nicht gezeigt, sie gehören auf die Winkelsteine 15 auf den "zweiten" Löchern) fixieren den Containerstapel seitlich und sollen deshalb die Griffe von außen montiert haben.
