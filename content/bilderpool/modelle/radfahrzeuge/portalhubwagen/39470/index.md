---
layout: "image"
title: "Unterseite, Lenkung"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39470
- /details3a2a.html
imported:
- "2019"
_4images_image_id: "39470"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39470 -->
Die Räder links und rechts oben wirken 1:1 auf die Messingachsen, die hier nur kopfseitig zu sehen sind (und in die Bildtiefe hinein ragen, zur Ackermann-Lenkung oben auf dem Fahrzeug). 
Das nächste Räderpaar wird mit einer Untersetzung von 2:1 gelenkt, weil der zugehörige Hebelarm auf den hier waagerecht sichtbaren Streben 30/Loch nur halb so groß ist wie derjenige zum obersten Räderpaar.
Das Räderpaar auf der anderen Hälfte, aber noch in Fahrzeugmitte wird beim Lenken mit 1:1 in die Gegenrichtung eingeschlagen. Dafür sorgt die Hebel, die von der linken Seite des einen Rades (zweite Reihe) zur rechten Seite des anderen (dritte Reihe) führen und gleichzeitig einen Endtaster enthalten, der den Lenkmotor bei maximalem Lenkeinschlag sitll setzt.
Der Lenkeinschlag der vierten Reihe (hier unten) wird wieder mittels passender Hebelarme mit 1:2 übersetzt. Die vierte Reihe ist auch diejenige, an der die Lenkmotoren (je einer links und rechts, hinter den Leuchtbausteinen versteckt) eingreifen.