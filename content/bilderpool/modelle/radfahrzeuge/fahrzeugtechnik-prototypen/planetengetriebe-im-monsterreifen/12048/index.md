---
layout: "image"
title: "Getriebe 6"
date: "2007-09-29T15:48:13"
picture: "planetengetriebeimmonsterreifen06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12048
- /detailsa44e.html
imported:
- "2019"
_4images_image_id: "12048"
_4images_cat_id: "1077"
_4images_user_id: "502"
_4images_image_date: "2007-09-29T15:48:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12048 -->
Hier wurde einiges abgeschnibbelt damit es auch reinpasst. Der schwarze Karton verhindert dass die Z10 herausfliegen.