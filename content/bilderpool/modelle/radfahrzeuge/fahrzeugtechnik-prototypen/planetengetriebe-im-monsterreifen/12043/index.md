---
layout: "image"
title: "Getriebe 1"
date: "2007-09-29T15:48:12"
picture: "planetengetriebeimmonsterreifen01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12043
- /details9b76.html
imported:
- "2019"
_4images_image_id: "12043"
_4images_cat_id: "1077"
_4images_user_id: "502"
_4images_image_date: "2007-09-29T15:48:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12043 -->
Man glaubt es nicht aber es ist tatsächlich ein Planetengetriebe im Reifen versteckt. Es gibt nur ein Problem, nämlich die Stabilität :-(
Die Drehscheibe auf der Antriebsseite darf den Reifen nicht berühren und der hat damit keinen Halt und wabbelt herum.