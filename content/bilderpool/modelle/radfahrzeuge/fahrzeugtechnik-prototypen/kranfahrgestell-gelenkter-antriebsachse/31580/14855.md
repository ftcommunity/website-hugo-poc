---
layout: "comment"
hidden: true
title: "14855"
date: "2011-08-16T20:26:54"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Heiko,
die Felgenanordnung bleibt wie bei dir mit dem Gewindestück jeweils nach außen, ebenso die schon eingesetzten Gelenkklauen. Da man Dmr. 7 in Dmr.10 axial einarbeiten kann, besteht die Möglichkeit die Klaue axial weiter außen anzuordnen. Es verbleibt eine Wandung von rechnerisch 1,5mm. Wenn man die Gelenkklaue im Dmr. absetzt, kann man noch eine loslaufende Rohrhülse zwischen Felge und Gelenkklaue zwischensetzen, an der die Lenkung vor den Innenseiten der Felgen angesetzt ist. Frage jetzt aber falls du noch nicht durchblickst bitte nicht weiter, denn ich will irgendwann ausnahmsweise auch mal was 4-rädriges bauen. Das vielleicht anläßlich meines 70. Geburtstages, was ich ja hier mal Harald "versprochen" habe.
Gruß, Ingo