---
layout: "image"
title: "Allrad, ungefedert, kurzer Radstand"
date: "2010-02-15T23:12:33"
picture: "kleinfahrwerke1_2.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26454
- /details77c3.html
imported:
- "2019"
_4images_image_id: "26454"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T23:12:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26454 -->
Noch ein kleines Allradfahrwerk mit besonders kurzem Radstand. Es fährt ziemlich flott.
