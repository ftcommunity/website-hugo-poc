---
layout: "comment"
hidden: true
title: "11013"
date: "2010-02-24T11:13:32"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan!

Danke für Deine Beschreibung!

Aber wäre der Verbinder 45 nicht, würde sich der Baustein 5 nach oben und unten bewegen, wenn die Vordersachse ein- und ausfedert, richtig?

Im Grunde ist das linke Vorderrad doch nur am vorderen Baustein 5 aufgehängt. Der hintere Baustein 5 verhindert nur das verdrehen der Statikstrebe, hängt aber wiederum über den Verbinder 45 nur am vorderen Baustein 5. Korrekt?

Gruß, Thomas