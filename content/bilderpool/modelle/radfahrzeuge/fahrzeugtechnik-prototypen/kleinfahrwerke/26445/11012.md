---
layout: "comment"
hidden: true
title: "11012"
date: "2010-02-24T10:52:21"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hi Thomas,

der BS5 ist ja mit der Nut waagerecht eingebaut und hat einen Statikadapter drin, der in der Strebe steckt. Nach oben oder unten kann er also schon mal nicht. Dann kommt noch der Verbinder 45, der verhindert, dass er grad so nach vorne oder hinten rausrutscht. Dieser BS5 hat keine andere Funktion, als den vorderen BS5 vor Verdrehung zu bewahren. Die Hinterachse ist nicht mit ihm verbunden. Die hängt tatsächlich nur über den BS5 mit zwei Zapfen am Motor, dann ein BS30, dann die Vorstuferäder, und auch das genügt völlig, weil das Fahrzeug so leicht ist.

Gruß,
Stefan