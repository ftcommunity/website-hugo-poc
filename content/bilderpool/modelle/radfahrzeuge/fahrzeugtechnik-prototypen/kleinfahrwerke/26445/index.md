---
layout: "image"
title: "Eine angetriebene Achse, ungefedert"
date: "2010-02-15T21:05:54"
picture: "kleinfahrwerke1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26445
- /detailsdc79.html
imported:
- "2019"
_4images_image_id: "26445"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26445 -->
Recht kompakt und immerhin mit Differential, wenn auch ungelenkt.
