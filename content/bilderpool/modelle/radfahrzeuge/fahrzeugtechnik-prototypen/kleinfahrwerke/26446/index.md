---
layout: "image"
title: "Eine angetriebene Achse, ungefedert"
date: "2010-02-15T21:05:55"
picture: "kleinfahrwerke2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26446
- /details027f.html
imported:
- "2019"
_4images_image_id: "26446"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26446 -->
In 3/4-Ansicht.
