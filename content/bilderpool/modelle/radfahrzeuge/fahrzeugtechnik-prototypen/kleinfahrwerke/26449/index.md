---
layout: "image"
title: "Allradantrieb, gefedert"
date: "2010-02-15T21:05:55"
picture: "kleinfahrwerke5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26449
- /details782a.html
imported:
- "2019"
_4images_image_id: "26449"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26449 -->
Zur Abwechslung mit größeren Reifen, es gingen aber auch die Räder 23 mit den Gummibelägen. Dieses Teil kann ziemlich große Unebenheiten überwinden, weil alle Räder angetrieben sind und durch recht große Federwege auch in hügeligem Gelände guten Bodenkontakt haben.
