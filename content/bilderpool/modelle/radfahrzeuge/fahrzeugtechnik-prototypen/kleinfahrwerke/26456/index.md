---
layout: "image"
title: "Allrad, ungefedert, kurzer Radstand"
date: "2010-02-15T23:12:33"
picture: "kleinfahrwerke3_2.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26456
- /details1845.html
imported:
- "2019"
_4images_image_id: "26456"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T23:12:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26456 -->
Die Spurkränze auf der Unterseite werden für das Modell benötigt, für das der Wagen gedacht ist. Ansonsten sieht man, wie der Motor über einen BS5 mit einem Statikadapter mit den waagerecht verlaufenden Längsträger-Streben verbunden ist. Er steht dann zwar nicht präzise senkrecht, aber so bleibt genug Platz für die Stecker.
