---
layout: "image"
title: "01"
date: "2008-03-22T20:53:43"
picture: "01.jpg"
weight: "1"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14009
- /detailsa6eb.html
imported:
- "2019"
_4images_image_id: "14009"
_4images_cat_id: "1139"
_4images_user_id: "327"
_4images_image_date: "2008-03-22T20:53:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14009 -->
Hier folgen nun einige Versuche, ein Federbein, wie es in heutigen PKW's zum Einsatz kommt, möglichst kompakt und auf jeden Fall mit Pneumatikzylindern, die die Stoßdömpfer darstellen sollen, nachzubauen. Mit der Zeit habe ich gefallen daran gefunden, auch die ein oder andere sagen wir "lustige" Variante zu konstruieren.

Schön ist halt, daß man über den Druck im Pneumatikzylinder die Federhärte wunderbar regulieren kann. Die Feder selbst brauchts eigentlich nicht wirklich, aber so kann man auch komplett ohne Überdruck im Zylinder fahren und hat halt dann die typische butterweiche Federung, wie man sie auch von den Standardmodellen her kennt. Ein bißchen weicher vielleicht noch, wegen der Umlenkung.