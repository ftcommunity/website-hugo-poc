---
layout: "image"
title: "06"
date: "2008-03-22T20:53:45"
picture: "06.jpg"
weight: "6"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14014
- /details1cc7.html
imported:
- "2019"
_4images_image_id: "14014"
_4images_cat_id: "1139"
_4images_user_id: "327"
_4images_image_date: "2008-03-22T20:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14014 -->
