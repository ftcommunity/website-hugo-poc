---
layout: "image"
title: "08"
date: "2008-03-22T20:53:45"
picture: "08.jpg"
weight: "8"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14016
- /details7681.html
imported:
- "2019"
_4images_image_id: "14016"
_4images_cat_id: "1139"
_4images_user_id: "327"
_4images_image_date: "2008-03-22T20:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14016 -->
