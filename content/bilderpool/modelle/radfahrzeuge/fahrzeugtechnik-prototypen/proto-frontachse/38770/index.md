---
layout: "image"
title: "Gesamtansicht"
date: "2014-05-09T17:28:26"
picture: "protofrontachse1.jpg"
weight: "1"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38770
- /detailsaaaa-2.html
imported:
- "2019"
_4images_image_id: "38770"
_4images_cat_id: "2898"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38770 -->
Querträger und drehpunkte.
Eine Weiterentwicklung habe Ich Im 2WD Fahrgestell gebaut.
Es dreht im jeden Fall sehr leicht.