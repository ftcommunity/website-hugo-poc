---
layout: "image"
title: "Gesamtansicht"
date: "2014-05-09T17:28:26"
picture: "protofrontachse2.jpg"
weight: "2"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38771
- /details7609-2.html
imported:
- "2019"
_4images_image_id: "38771"
_4images_cat_id: "2898"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38771 -->
Es ist noch nicht deutlich was oben und was unten ist