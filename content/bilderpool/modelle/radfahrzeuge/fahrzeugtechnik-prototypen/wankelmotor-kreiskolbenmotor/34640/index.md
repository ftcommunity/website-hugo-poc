---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor8.jpg"
weight: "8"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/34640
- /details7bc3-2.html
imported:
- "2019"
_4images_image_id: "34640"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34640 -->
Hier sieht man der 50:1 Powermot, der den Kolben dreht. Der Stab der nach oben zeigt ist eine Art Kupplung durch die ich den Kraftschluss aufheben kann, um von hand den Kolben zu bewegen, wenn die Drehung nur sehr gering oder sehr langsam sein soll.