---
layout: "overview"
title: "Wankelmotor (Kreiskolbenmotor)"
date: 2020-02-22T07:53:01+01:00
legacy_id:
- /php/categories/2556
- /categoriesb309.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2556 --> 
Modell eines Wankelmotors (Kreiskolbenmotors) wie er im NSU ro80 eingetzt wurde. Ich habe das Modell für meine Abitur Präsentationsprüfung gebaut um den Bewegungsablauf zu erläutern.