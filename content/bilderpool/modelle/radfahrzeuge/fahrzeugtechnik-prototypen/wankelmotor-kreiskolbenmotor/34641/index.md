---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor9.jpg"
weight: "9"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- /php/details/34641
- /details69d6.html
imported:
- "2019"
_4images_image_id: "34641"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34641 -->
Vor dem Modell ist eine Plexiglasscheibe.