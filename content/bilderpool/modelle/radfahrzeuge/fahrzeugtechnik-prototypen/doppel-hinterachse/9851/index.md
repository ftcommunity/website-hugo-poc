---
layout: "image"
title: "Hinterachse01.JPG"
date: "2007-03-30T17:09:31"
picture: "Hinterachse01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/9851
- /detailsaaa0-2.html
imported:
- "2019"
_4images_image_id: "9851"
_4images_cat_id: "889"
_4images_user_id: "4"
_4images_image_date: "2007-03-30T17:09:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9851 -->
Diese Achskonstruktion ist eine Nummer kleiner als die vom knickgelenkten Muldenkipper ( http://www.ftcommunity.de/categories.php?cat_id=860 ) - funktioniert aber nach dem gleichen Prinzip. 
Der Antrieb ist noch nicht drangebaut, aber das kommt auch noch.
