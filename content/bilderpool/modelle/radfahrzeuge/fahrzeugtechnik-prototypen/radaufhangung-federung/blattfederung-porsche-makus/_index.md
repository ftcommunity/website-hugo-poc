---
layout: "overview"
title: "Blattfederung (Porsche-Makus)"
date: 2020-02-22T07:52:46+01:00
legacy_id:
- /php/categories/1288
- /categories288a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1288 --> 
Hallo,

weiß nicht, ob das schon mal da war. Ich habe einfach aus einigen Statikstreben eine Blattfederung nachgebaut. Zusätzlich brauchts aber noch die schwarze Feder, damit das ganze wieder zurückfedert. Aber nett anzusehen, wie ich finde und dem Original schon sehr nah!