---
layout: "comment"
hidden: true
title: "6046"
date: "2008-03-24T08:05:00"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Porsche-Makus,
deine Blattfederung ist eine gute Idee. Ein Ende der großen und alle in der Achsmitte paketiert müssen aber gleiten können. Das andere Ende der großen Feder kann dabei drehbar gelagert sein. Dann mußt du dich noch  entscheiden, ob es eine Blatt- oder Schraubenfederung sein soll. Wenn das Fahrzeuggewicht die große Blattfeder zu sehr niederdrückt, kannst du auch mehr große einsetzen.
Gruß Udo2