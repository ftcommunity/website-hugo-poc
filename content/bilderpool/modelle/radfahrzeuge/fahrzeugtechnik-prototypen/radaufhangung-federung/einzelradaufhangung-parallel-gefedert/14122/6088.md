---
layout: "comment"
hidden: true
title: "6088"
date: "2008-03-26T23:54:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Es baut ein bisschen hoch, finde ich. Man könnte den Zylinder vielleicht waagerecht über eine Dreiecksumleitung federn lassen.

Und: Der Knackpunkt dürfte m. E. sein, den Zylinder ordentlich zu steuern, z. B. für gleichbleibende Bodenfreiheit. Und da habe ich Bedenken a) wegen des potentiellen Gewichts eines solchen vollständigen Fahrzeugs und b) wegen der inneren Reibung: Ich fürchte, das federt dann mit Hysterese zu weit aus oder zu weit ein, aber nicht genau auf Niveau.

Willst Du es nicht doch mal zu Ende probieren?

Gruß,
Stefan