---
layout: "image"
title: "01"
date: "2008-03-24T20:28:32"
picture: "01.jpg"
weight: "1"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14088
- /detailsfc57.html
imported:
- "2019"
_4images_image_id: "14088"
_4images_cat_id: "1292"
_4images_user_id: "327"
_4images_image_date: "2008-03-24T20:28:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14088 -->
