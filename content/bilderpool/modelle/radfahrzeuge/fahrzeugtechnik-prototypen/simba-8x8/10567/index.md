---
layout: "image"
title: "Simba04.JPG"
date: "2007-05-30T19:10:21"
picture: "Simba04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/10567
- /details0b0f.html
imported:
- "2019"
_4images_image_id: "10567"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2007-05-30T19:10:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10567 -->
Ein etwas verstärkter Radschemel, von der Innenseite aus gesehen. In Bildmitte unten ist das vordere "Halb-Mittendifferenzial" zu sehen. Das Z20 sitzt auf der Haupt-Antriebswelle, die zum "großen" Mittendifferenzial führt. Die Schnecke rechts treibt das Achsdifferenzial (eins von vieren seiner Art) an.
