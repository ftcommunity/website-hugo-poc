---
layout: "image"
title: "Lenkung auseinandergenommen"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39960
- /detailsa057-4.html
imported:
- "2019"
_4images_image_id: "39960"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39960 -->
hier noch mal der Verweis auf die Räder:  http://www.ftcommunity.de/categories.php?cat_id=2998