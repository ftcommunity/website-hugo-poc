---
layout: "image"
title: "mit Powermotor, von hinten"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_5_klein.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40084
- /details7ccc-2.html
imported:
- "2019"
_4images_image_id: "40084"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40084 -->
Die Statikteile werden wohl bei weiterem Ausbau noch normalen Steinen weichen.