---
layout: "comment"
hidden: true
title: "19833"
date: "2014-12-23T15:11:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Also, die An/Einbauerei bei Power-Motor ist doch nur eine Krankheit. Man kann ihn nur dort (gescheit) festmachen, wo auch die Antriebsachse herauskommt. Er passt nur an einer Seite ins Raster; der Getriebekasten steht an drei Seiten drüber hinaus. Die Achse hat 7,5 mm oder so, und passt mit nichts zusammen außer den beiden Adapterstücken. Wer eine Kardanwelle anbauen will, hat schon mal 30 mm Einbaulänge gegenüber dem XM "gewonnen", weil da eine Rastachse 30 dazwischen muss. Soll ich noch weiter machen? Nimmt man den Adapter mit Z15, sind alle vier Positionen auf der Anbauplatte für BS15 verloren; da passen nur noch Winkel und sonstiges krummes hin. Die Stecker kann man nur an einer Seite einstecken, und zwar so, dass die Baulänge noch erhöht wird (OK, letzteres gilt auch für den XM, macht aber den PowerMot nicht besser).

Ich hab ja auch von jeder Sorte mindestens zwei, aber deswegen bin ich noch lange nicht begeistert von den Dingern.

Gruß,
Harald