---
layout: "overview"
title: "Chassis für Sportwagen"
date: 2020-02-22T07:53:09+01:00
legacy_id:
- /php/categories/3003
- /categoriesbe54.html
- /categoriesf9c5.html
- /categoriesa267.html
- /categories1bf9-2.html
- /categories0584.html
- /categories93cf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3003 --> 
besonders flaches Chassis mit Alufelgen, Lenkung und Powermotor