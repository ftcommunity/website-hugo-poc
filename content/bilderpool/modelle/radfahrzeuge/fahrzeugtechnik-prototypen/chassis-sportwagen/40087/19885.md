---
layout: "comment"
hidden: true
title: "19885"
date: "2014-12-31T17:08:52"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Rattenscharfes Chassis mit niedrigem Eigengewicht... Damit sollten neue Rekorde möglich sein. Fehlen höchtens noch die Federung und die i8-Flügeltüren...
Viel Spaß beim Weitertüfteln.
Gruß, Dirk