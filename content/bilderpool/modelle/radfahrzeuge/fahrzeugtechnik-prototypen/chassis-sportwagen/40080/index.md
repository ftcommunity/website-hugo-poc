---
layout: "image"
title: "mit Powermotor"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_1_klein.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40080
- /detailsb62c.html
imported:
- "2019"
_4images_image_id: "40080"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40080 -->
Den Powermotor hat nun also das Christkind gebracht. Die Sitze mussten einen Millimeter nach vorne (dazu musste ich den Baustein 5 unter dem Winkelbaustein um 90 Grad drehen) und man muss Kabel mit kleinen Steckern verwenden, aber im Großen und Ganzen passt es. Erste Ansätze für Kühlerhaube und Tür sind auch schon sichtbar.