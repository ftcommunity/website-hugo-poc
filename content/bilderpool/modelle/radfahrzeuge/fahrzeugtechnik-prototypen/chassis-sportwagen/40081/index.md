---
layout: "image"
title: "mit Powermotor, Details"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_2_klein.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40081
- /details401f.html
imported:
- "2019"
_4images_image_id: "40081"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40081 -->
Man kann sehen, dass ich Seilrollen mit Schnur verwendet habe, damit sie auch bei größerem Drehmoment nicht rutschen.