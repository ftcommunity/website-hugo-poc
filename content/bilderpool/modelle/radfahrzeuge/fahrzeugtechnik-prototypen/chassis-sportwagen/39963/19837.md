---
layout: "comment"
hidden: true
title: "19837"
date: "2014-12-23T23:03:16"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Naja - wirklich Zufall ist es nicht. Es ist deshalb ein Rechtslenker geworden. Ich habe erst die Lenkung spiegelverkehrt gebaut, dann war es zwar ein vernünftiger Linkslenker, aber der Drehsinn war falsch.