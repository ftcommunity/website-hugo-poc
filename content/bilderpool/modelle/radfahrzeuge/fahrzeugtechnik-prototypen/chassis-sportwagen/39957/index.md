---
layout: "image"
title: "Lenkung von oben"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39957
- /detailsd0f1-2.html
imported:
- "2019"
_4images_image_id: "39957"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39957 -->
Man kann ganz gut sehen, dass die Lenkachsen innerhalb des Rades liegen.