---
layout: "image"
title: "Lenkung"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39956
- /details60c2-2.html
imported:
- "2019"
_4images_image_id: "39956"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39956 -->
Bei der Lenkung habe ich mit der Schnecke experimientiert, und sie ist damit ziemlich kompakt gelungen.