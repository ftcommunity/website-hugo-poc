---
layout: "image"
title: "Lenkung Details"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39961
- /details9fa9.html
imported:
- "2019"
_4images_image_id: "39961"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39961 -->
Die Verbindung zwischen der Schneckenmutter und der Spurstange erfolgt über einen Klemmkontakt, der in eine Nut der Schneckenmutter gesteckt ist und an der Spurstange mit einem Klemmadapter und einem Raubenbelag befestig ist, siehe auch weiter vorne das Bild mit dem flachen Profil. Die Lenkung funktioniert ganz gut.