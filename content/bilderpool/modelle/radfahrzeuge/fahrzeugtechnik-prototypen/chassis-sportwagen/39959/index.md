---
layout: "image"
title: "Lenkung maximaler Rechtseinschlag"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39959
- /details97b6.html
imported:
- "2019"
_4images_image_id: "39959"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39959 -->
Auch hier berührt das Rad etwas die Feder des Getriebes. Wenn man das neue Getriebe verwenden würde, das anstelle der Feder einen Zapfen hat, würde das Rad drunter kommen und nichts mehr berühren. Dann kann man vielleicht den Servo insgesamt ein klein wenig Richtung Zapfen verschieben, so ca. 1mm, damit auf der anderen Seite auch noch etwas mehr Platz ist. Die Winkelzahnräder machen das mit, das habe ich bereits probiert.