---
layout: "image"
title: "mit Powermotor, noch mal von unten"
date: "2014-12-31T07:49:08"
picture: "8_mit_Powermotor_4_klein.jpg"
weight: "17"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40083
- /details6b0d.html
imported:
- "2019"
_4images_image_id: "40083"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40083 -->
Hm... sogar Unterbodenverkleidung... verspricht einen guten cw-Wert.