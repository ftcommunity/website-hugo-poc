---
layout: "image"
title: "Flaches Profil"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/39955
- /detailsf12d-2.html
imported:
- "2019"
_4images_image_id: "39955"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39955 -->
Im Wesentlichen hat das Fahrwerk eine Dicke von nur 30mm. Das es so flach ist ist wichtig, um es unter einer ebenfalls flachen Sport-Karosserie verwenden zu können.