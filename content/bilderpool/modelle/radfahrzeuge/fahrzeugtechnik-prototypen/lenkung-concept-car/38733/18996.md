---
layout: "comment"
hidden: true
title: "18996"
date: "2014-04-30T18:44:35"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Richard,

zu den Fragen: ich kaue halt schon eine ziemlich Weile (ohne Ergebnis) auf solchen Sachen herum. Bei einem Planetengetriebe in der Nabe kommt typischerweise die Antriebsachse genau in die Mitte, dann sitzt ein Teil drehfest auf dem Achsträger, und zuletzt kommt die Felge, die bezüglich der Mittelachse frei laufen muss (sonst bräuchte man den Planetensatz nicht). Also muss da ein Achszapfen außen heraus stehen, oder die Felge hat eine große Innenbohrung und wird auf dem Gehäuse vom Planetengetriebe gelagert. Und irgend ein Dingsda braucht man noch, um die Kraft auf die Felge zu bekommen, z.B. eine Innenverzahnung, in der die Planeten abrollen.

Damit das auch Allrad-tauglich wird, muss natürlich noch ein Gleichlaufgelenk mit hinein, ist ja klar ;-)

Gruß,
Harald