---
layout: "image"
title: "Größenvergleich"
date: "2014-04-30T12:23:43"
picture: "GroessenVergleich.jpg"
weight: "11"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38733
- /details8365-2.html
imported:
- "2019"
_4images_image_id: "38733"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-30T12:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38733 -->
Um die Größenverhältnisse ins rechte Licht zu rücken, hier mal ein Bild mit bekannten Vergleichsgrößen.
Vorne der allseits bekannte ft Reifen. 
Etwas größer der RC Car Reifen, den ich bei dem Modell verwende.
Das Männchen ist eigentlich für alle diese Reifen zu klein!
Der Reifen ist aus dem RC 1:10 Modellbau, dann müsste das Männchen ja schon ca. 18 cm groß sein!
und dahinter ein 132 mm Reifen, mit dem ich eine größere Modellidee umsetzen möchte. Dieser ist eher 1:8, die Besatzung müsste dann schon 22,5 cm groß sein, um ins Größenverhältnis zu passen.

Das dargestellte Modell ist eigentlich ein Versuchsträger und den hab ich bewusst so klein wie möglich gebaut. Dadurch sind die Reifen viel zu groß.

Den Riesenreifen für meine nächste Modellidee habe ich deswegen ausgewählt, da er einen ziemlich großen Felgen-Innenraum hat. Da lässt sich eventuell mal einiges IN das Rad einbauen! Ich möchte damit mal ein richtiges SCALE-Modell machen. Durch den großen Reifen wird das aber einen Maßstab geben zwischen 1:7 und 1:8.....das wird einiges Kopfzerbrechen bereiten in Punkto Stabilität, Gewicht, Antrieb, Kosten....