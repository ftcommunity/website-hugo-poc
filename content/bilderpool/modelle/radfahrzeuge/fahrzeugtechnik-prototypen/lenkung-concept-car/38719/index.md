---
layout: "image"
title: "Frontansicht"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar05.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38719
- /details71f8.html
imported:
- "2019"
_4images_image_id: "38719"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38719 -->
Bodenfreiheit: Der tiefste Teil des Chassis ist der 9V Block, der höchste der Kopf des Fahrers ;-)
