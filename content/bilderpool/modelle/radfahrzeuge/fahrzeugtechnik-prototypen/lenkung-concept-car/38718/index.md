---
layout: "image"
title: "Seitenansicht"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar04.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38718
- /detailsbf8d-2.html
imported:
- "2019"
_4images_image_id: "38718"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38718 -->
Hier sieht man sehr gut die verbliebene Bodenfreiheit, obwohl ich Schalter und 9V-Batterie unten eingebaut habe.