---
layout: "image"
title: "Draufsicht"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar07.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38721
- /detailsb3ea.html
imported:
- "2019"
_4images_image_id: "38721"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38721 -->
Durch den extremen Lenkeinschlag, den die Lenkung möglich macht, benötigen die Reifen sehr viel Platz.
Es geht um wenige Millimeter, ob die Räder irgendwo schleifen oder nicht.
Außerdem sieht man, daß die Lenkung ein bisschen in den Felgen-Innenraum reinragt.
Der Drehpunkt ist allerdings nicht im Radmittelpunkt. Wenn man das machen würde, wäre der Lenkeinschlag stark eingeschränkt!