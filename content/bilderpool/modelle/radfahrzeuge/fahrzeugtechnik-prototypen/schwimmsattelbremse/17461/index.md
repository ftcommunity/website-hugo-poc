---
layout: "image"
title: "Schwimmsattelbremse 15"
date: "2009-02-19T21:15:15"
picture: "Porsche-Makus_Schwimmsattelbremse_15.jpg"
weight: "15"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17461
- /details8cf8.html
imported:
- "2019"
_4images_image_id: "17461"
_4images_cat_id: "1569"
_4images_user_id: "327"
_4images_image_date: "2009-02-19T21:15:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17461 -->
