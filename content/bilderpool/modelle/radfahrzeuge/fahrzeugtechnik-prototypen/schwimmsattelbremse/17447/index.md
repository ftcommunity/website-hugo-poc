---
layout: "image"
title: "Schwimmsattelbremse 01"
date: "2009-02-19T21:15:14"
picture: "Porsche-Makus_Schwimmsattelbremse_01.jpg"
weight: "1"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
schlagworte: ["Scheibenbremse", "Schwimmsattelbremse", "Faustsattelbremse", "Einkolbenbremse."]
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/17447
- /details087b.html
imported:
- "2019"
_4images_image_id: "17447"
_4images_cat_id: "1569"
_4images_user_id: "327"
_4images_image_date: "2009-02-19T21:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17447 -->
Hallo, heute habe ich eine kompakte Einkolbenschwimmsattelscheibenbremse gebaut (langes Wort, aber nichts als die reine Wahrheit ;-)).

Das besondere daran ist, dass diese Bremse genau so funktioniert wie die, die in den meisten Autos eingebaut ist und zudem so kompakt ist, dass man sie wirklich in das ein oder andere Modell einbauen könnte.

Insgesamt drei Teile sind schwimmend gelagert, angefangen mit dem blauen Pneumatikbetätiger (?), dann der Faustsattel und zuletzt ist auch noch die ganze Einheit schwimmend über die Achsen gelagert.

Die Bremse funktioniert recht ordentlich, den Motor konnte ich damit nicht ganz aber fast abbremsen.

Alles ist mit originalen, nicht modifizierten Fischertechnikteilen gebaut.