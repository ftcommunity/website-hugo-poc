---
layout: "comment"
hidden: true
title: "123"
date: "2003-10-08T17:45:15"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
.Leider ein wenig verwackelt und das bisher einzige Foto. Ich habe versucht, den Kipper so klein wie möglich zu halten, heraus kam eine Allradlenkung, die mangels Lenktrapez ziemlich rutscht. Aber zumindest sieht der LKW vernünftig aus :-)