---
layout: "image"
title: "Lenkung der letzten Achse"
date: "2005-10-26T15:29:35"
picture: "Zwangslenkung_-_3.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5120
- /detailse18d.html
imported:
- "2019"
_4images_image_id: "5120"
_4images_cat_id: "185"
_4images_user_id: "9"
_4images_image_date: "2005-10-26T15:29:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5120 -->
Das Konzept stammt nicht von mir, mein Vater hat es mir irgendwann einmal gezeigt - er muss es wohl seinerzeit erfunden haben, als er mit dem Stabilbaukasten Autos gebaut hat. Wie es funktioniert, könnt ihr euch wahrscheinlich vorstellen - wie es wirkt, seht ihr auf den nächsten Fotos.

Dass die Länge der Strebe so gut passt, ohne Bausteine 5, 7,5 und Herumschieben, ist purer Zufall.