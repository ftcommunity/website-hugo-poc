---
layout: "image"
title: "Bremsteil von oben"
date: "2007-05-03T19:12:32"
picture: "scheibenbremse3.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10281
- /details8ec9-2.html
imported:
- "2019"
_4images_image_id: "10281"
_4images_cat_id: "934"
_4images_user_id: "445"
_4images_image_date: "2007-05-03T19:12:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10281 -->
