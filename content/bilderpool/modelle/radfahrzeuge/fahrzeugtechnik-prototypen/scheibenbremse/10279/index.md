---
layout: "image"
title: "Gasamtansicht"
date: "2007-05-03T19:12:32"
picture: "scheibenbremse1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10279
- /details93d6-3.html
imported:
- "2019"
_4images_image_id: "10279"
_4images_cat_id: "934"
_4images_user_id: "445"
_4images_image_date: "2007-05-03T19:12:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10279 -->
Vorne das Pedal, und hinten ist die Bremse.