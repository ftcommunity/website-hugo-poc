---
layout: "comment"
hidden: true
title: "20409"
date: "2015-04-03T16:58:47"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ja, die Pneumatikadapter passen wunderbar ins Raster. Nur leider sind sie für die runden "Zapfen" der Kolbenstangen gemacht. Auf den eckigen Zapfen sitzen sie nicht ganz exakt - und nach einer Weile verformen sie sich in Richtung "eckig", dann halten sie nicht mehr auf den Zylinderstangen.

Super gut klappt das auf BS mit runden Zapfen!

Grüße
H.A.R.R.Y.