---
layout: "image"
title: "Innenansicht der Felge"
date: "2014-06-07T15:21:00"
picture: "Foto_4.jpg"
weight: "7"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38918
- /details68b0-2.html
imported:
- "2019"
_4images_image_id: "38918"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2014-06-07T15:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38918 -->
Nächste Projekte:

&#8226;	Konstruktion der Lenkachse
&#8226;	Konstruktion des Fahrwerks mit den Achsen