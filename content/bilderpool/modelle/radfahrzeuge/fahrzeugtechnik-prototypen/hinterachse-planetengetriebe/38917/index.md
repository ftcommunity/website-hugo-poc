---
layout: "image"
title: "Reifen, Detailansicht"
date: "2014-06-07T15:21:00"
picture: "Foto_3.jpg"
weight: "6"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- /php/details/38917
- /detailseefa.html
imported:
- "2019"
_4images_image_id: "38917"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2014-06-07T15:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38917 -->
Aus Platzgründen ist das Planetengetriebe im Felgen-Innenraum untergebracht. Um den Standplatten entgegen zuwirken und die Stabilität des Reifes zu erhöhen, wurde dieser mit Schaumstoff ausgekleidet.