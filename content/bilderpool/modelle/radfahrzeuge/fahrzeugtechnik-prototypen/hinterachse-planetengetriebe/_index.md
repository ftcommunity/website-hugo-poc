---
layout: "overview"
title: "Hinterachse mit Planetengetriebe"
date: 2020-02-22T07:53:07+01:00
legacy_id:
- /php/categories/2910
- /categories7494.html
- /categoriesd06e.html
- /categories6086.html
- /categories2f19.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2910 --> 
Hallo, 
seit längerem experimentiere ich mit den großen Traktorreifen von Conrad Elektronik. Immer wieder tauchten folgende Probleme auf:
 
&#8226;	Dauerstandplatten 
&#8226;	Hohes Anfahrdrehmoment

Mein Lösungsvorschlag: