---
layout: "image"
title: "Raupenfahrwerk (Bauweise mit Z30) 2.Teil"
date: "2006-04-14T18:57:50"
picture: "DSCN0704.jpg"
weight: "33"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6092
- /detailsd3e5.html
imported:
- "2019"
_4images_image_id: "6092"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-14T18:57:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6092 -->
Der Aufbau ist der gleiche geblieben, nur halt größer
