---
layout: "image"
title: "Prototyp I Ansicht von unten"
date: "2006-05-01T18:57:20"
picture: "DSCN0720.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6194
- /details7a0b-3.html
imported:
- "2019"
_4images_image_id: "6194"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-05-01T18:57:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6194 -->
"P" steht nicht etwa für Power Motor sondern für Pollin.
Ich habe so viele verschiedene PM das ich sonst den Überblick verliere....
