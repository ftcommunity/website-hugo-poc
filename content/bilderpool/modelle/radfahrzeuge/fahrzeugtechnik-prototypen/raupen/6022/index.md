---
layout: "image"
title: "Hier eine Variante mit Z30 (Detail)"
date: "2006-04-05T11:41:21"
picture: "DSCN0689.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6022
- /details6562-2.html
imported:
- "2019"
_4images_image_id: "6022"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-05T11:41:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6022 -->
