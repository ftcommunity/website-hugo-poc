---
layout: "image"
title: "Variante mit Z20, Ansicht von außen"
date: "2006-04-12T19:52:48"
picture: "DSCN0691.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6072
- /detailsfcb1-2.html
imported:
- "2019"
_4images_image_id: "6072"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-12T19:52:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6072 -->
Dieses mal habe ich ein Zahnrad Z20 genutzt. Sieht gut aus und funktioniert auch gut. Das "hoppeln" ist, auf Grund des neuen Ausbaues zwischen den Zahnrädern, weg.
