---
layout: "image"
title: "Prototyp I Kette"
date: "2006-05-01T18:57:20"
picture: "DSCN0721.jpg"
weight: "41"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/6195
- /detailsdacd.html
imported:
- "2019"
_4images_image_id: "6195"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-05-01T18:57:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6195 -->
Bei diesem Modell habe ich die Ketten umgebaut.
Die beiden Klemmbuchsen 5 habe ich durch eine, die zwischen den Rollenböcken sitzt, ersetzt.
