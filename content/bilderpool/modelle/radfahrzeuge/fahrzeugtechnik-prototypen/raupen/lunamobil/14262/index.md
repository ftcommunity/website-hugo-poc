---
layout: "image"
title: "Batteriekappe mit ft Fisch"
date: "2008-04-14T20:47:47"
picture: "battkap.jpg"
weight: "1"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/14262
- /details4832.html
imported:
- "2019"
_4images_image_id: "14262"
_4images_cat_id: "1320"
_4images_user_id: "764"
_4images_image_date: "2008-04-14T20:47:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14262 -->
