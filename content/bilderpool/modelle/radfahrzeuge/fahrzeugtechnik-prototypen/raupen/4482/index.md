---
layout: "image"
title: "raupen_7"
date: "2005-06-19T16:06:51"
picture: "raupen_7.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/4482
- /details73f2.html
imported:
- "2019"
_4images_image_id: "4482"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:06:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4482 -->
Hier ist das innere einer der antrieben zu sehen.
Die gelenkwürfel-Klaue(31436) + Lagerhülse(36819) und der Clipsachse(32870) zusamen mit Kufe(31602), tragen dazu bei das das antriebsteil sich in Länsrichtung verschieben kan, ohne das der kipt oder schief gehen kan.