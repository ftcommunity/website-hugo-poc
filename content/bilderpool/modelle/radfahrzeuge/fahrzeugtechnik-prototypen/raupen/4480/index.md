---
layout: "image"
title: "raupen_5"
date: "2005-06-19T16:06:51"
picture: "raupen_5.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/4480
- /details3364.html
imported:
- "2019"
_4images_image_id: "4480"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:06:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4480 -->
Die seite an dem der motor angebaut werden soll.
Bodenplatt 30x90 (32859) mus noch bearbeitet worden damit er die selbe dicke hat wie der anbauplatte für die Power Motoren.
Die Power Motoren sind die 312er Conrads.