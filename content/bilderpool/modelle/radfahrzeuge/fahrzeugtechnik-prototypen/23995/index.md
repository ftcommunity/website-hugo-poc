---
layout: "image"
title: "Zwillingsachse64.JPG"
date: "2009-05-10T16:05:47"
picture: "Zwillingsachse64.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23995
- /details5185.html
imported:
- "2019"
_4images_image_id: "23995"
_4images_cat_id: "297"
_4images_user_id: "4"
_4images_image_date: "2009-05-10T16:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23995 -->
Die gelben Teile sind Freilaufnaben. Die Räder werden am äußeren Umfang über die Rastkegelzahnräder angetrieben, deren Achsen durch die Mittellöcher der Gelenksteine führen. Die Lagerung erfolgt in Schneckenmuttern mit Kugellager auf der Innenseite. Das war nötig, weil sonst die Gesamtbreite nicht mehr richtig gepasst hätte. Außen vor den Rädern sitzen Rastklemmbuchsen (nicht original-ft; sowas bleibt übrig, wenn man die Rast-Z10 flach abschneidet).
