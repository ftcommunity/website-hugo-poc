---
layout: "image"
title: "Baustufe 2"
date: "2009-01-01T13:18:31"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/16831
- /detailsc281-2.html
imported:
- "2019"
_4images_image_id: "16831"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:18:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16831 -->
Nun wird auf beiden Seiten ein Grundbaustein 15 am Baustein 15 mit Bohrung (32064)befestigt. Ein zusätzlicher Federnocken (31982, s. Markierung) stellt eine Verbindung zum Rahmen her.
