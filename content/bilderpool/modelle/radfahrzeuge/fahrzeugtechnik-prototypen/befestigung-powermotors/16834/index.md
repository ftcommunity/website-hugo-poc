---
layout: "image"
title: "Mit Differential"
date: "2009-01-01T13:24:33"
picture: "bild7.jpg"
weight: "7"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/16834
- /details2e90.html
imported:
- "2019"
_4images_image_id: "16834"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:24:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16834 -->
Zum Abschluss noch das Bild mit eingebautem Differential.
Als Grundlage dient der "Fire Truck", aber diese Lösung lässt sich - mit der einen oder anderen Modifikation - sicherlich auch auf andere Modelle anwenden.
