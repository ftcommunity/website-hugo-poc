---
layout: "image"
title: "Mit ft Original Z10"
date: "2014-02-01T16:00:15"
picture: "Planetengetriebe-OrgFT.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38153
- /details1fce.html
imported:
- "2019"
_4images_image_id: "38153"
_4images_cat_id: "2832"
_4images_user_id: "1729"
_4images_image_date: "2014-02-01T16:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38153 -->
Der Aufbau des Planetengetriebes komplett mit Original-Fischertechnik. Das Bild zeigt es eingebaut in der fertigen Achse. Es baut etwas breiter als die Variante mit der Messingnabe.