---
layout: "image"
title: "Minitaster mit Dioden"
date: 2020-12-28T20:46:57+01:00
picture: "56TastermitDioden.jpg"
weight: "57"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier wurde ein einfache 1N4001 Diode verbaut. Die Theorie der Endlagenschaltung ist in der ft:pedia 3/2011 sehr gut beschrieben.