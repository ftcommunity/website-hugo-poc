---
layout: "image"
title: "Lagerbock Zwischenwelle vorne"
date: 2020-12-28T20:48:00+01:00
picture: "18LagerbockZwischenwellevorne.jpg"
weight: "18"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Wahlweise können hier auch kugelgelagerte Schneckenmuttern 37925 eingesetzt werden