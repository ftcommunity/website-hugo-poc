---
layout: "image"
title: "Grundaufbau"
date: 2020-12-03T20:35:25+01:00
picture: "10. Grundaufbau.jpg"
weight: "10"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Das Grundchassis dient als Basis zum Experimentieren mit unterschiedlichen Schaltgetrieben