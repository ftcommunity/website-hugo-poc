---
layout: "image"
title: "Zwischenwelle komplett"
date: 2020-12-28T20:47:55+01:00
picture: "21Zwischenwellekomplett140mmAchse.jpg"
weight: "21"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Die vier Zahnräder müssen exakt über die Innenverzahnung ausgerichtet werden, damit sich das Z30 Schaltzahnrad einwandfrei axial bewegen kann.