---
layout: "image"
title: "Seitenansicht links"
date: 2020-12-03T20:35:11+01:00
picture: "2. Seitenansicht links.jpg"
weight: "2"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Schaltwelle und Abtriebswelle sind gut zu erkennen. Ebenfalls die Endlagentaster und Servoanbindung. Für eine Dauerbelastung ist das Servo sicherlich nicht ausgelenkt, aber es funktioniert was es soll. 