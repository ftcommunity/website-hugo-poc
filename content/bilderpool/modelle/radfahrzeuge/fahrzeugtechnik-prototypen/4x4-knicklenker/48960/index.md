---
layout: "image"
title: "Übersicht Einzelteile der Zwischenwelle"
date: 2020-12-28T20:47:53+01:00
picture: "22ZwischenwelleEinzelteile.jpg"
weight: "22"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Achslänge 140/150mm. Das Z20 ist sehr fest anzuziehen.