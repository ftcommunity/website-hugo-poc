---
layout: "image"
title: "Lagerbock vorne montiert"
date: 2020-12-28T20:47:37+01:00
picture: "31Lagerbockvornemontiert.jpg"
weight: "32"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Alles kann mit kugelgelagerten Schneckenmuttern ausgestattet werden