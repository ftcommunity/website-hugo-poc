---
layout: "image"
title: "Schaltung für die Endlagentaster"
date: 2020-12-28T20:46:52+01:00
picture: "59SchaltungfürEndlagentaster.jpg"
weight: "60"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Bitte auf die richtige Polung der Dioden achten