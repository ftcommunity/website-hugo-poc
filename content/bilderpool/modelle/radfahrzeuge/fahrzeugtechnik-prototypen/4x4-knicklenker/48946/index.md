---
layout: "image"
title: "Übersicht Einzelteile Lagerbock hinten"
date: 2020-12-28T20:47:31+01:00
picture: "35LagerbockhintenEinzelteile.jpg"
weight: "36"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen