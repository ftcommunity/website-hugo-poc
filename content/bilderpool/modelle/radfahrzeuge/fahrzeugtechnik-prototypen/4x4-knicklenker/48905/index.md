---
layout: "image"
title: "Detail Zwischenwelle"
date: 2020-12-03T20:35:01+01:00
picture: "8. Detail Zwischenwelle.jpg"
weight: "8"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Beim Walzenzahnrad 4x Z15 müssen die Innenzahnräder genau ineinandergesteckt werden, damit die zu den äußeren Zähnen fluchten