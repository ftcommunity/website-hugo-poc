---
layout: "image"
title: "Montage Schaltwelle und Abtriebswelle"
date: 2020-12-28T20:47:28+01:00
picture: "37MontageSchaltwelleundAbtriebswelle.jpg"
weight: "38"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier entscheidet sich, wer bisher sehr genau gebaut hat. Ansonsten sind kleine Korrekturen erforderlich.