---
layout: "image"
title: "Schaltriegelstange montiert"
date: 2020-12-28T20:46:58+01:00
picture: "55Schaltriegelmontiert.jpg"
weight: "56"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Später ist eine Feinjustierung mit den Minitastern erforderlich