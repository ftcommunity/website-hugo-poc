---
layout: "image"
title: "Detail Übersetzung"
date: 2020-12-03T20:35:21+01:00
picture: "12. Detail Übersetzung.jpg"
weight: "12"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Es sollte auf festen Sitz der Teile geachtet werden, da sie sich leicht verschieben können