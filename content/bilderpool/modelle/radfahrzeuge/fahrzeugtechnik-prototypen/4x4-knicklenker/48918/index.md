---
layout: "image"
title: "Detail Mittelwelle"
date: 2020-12-03T20:35:23+01:00
picture: "11. Detail Mittelwelle.jpg"
weight: "11"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Mittelstrang mit Kardangelenk und Drehscheibe. Der Drehpunkt des Kardangelenkes muss identisch sein mit den grauen Gelenksteinen