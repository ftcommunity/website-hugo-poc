---
layout: "image"
title: "Übersicht Einzelteile für Lagerbock zwischenwelle vorne"
date: 2020-12-28T20:47:57+01:00
picture: "20LagerbockZwischenwelleEinzelteile.jpg"
weight: "20"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen