---
layout: "image"
title: "Fernsteuerung montiert"
date: 2020-12-28T20:46:55+01:00
picture: "57Fernsteuerungmontiert.jpg"
weight: "58"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier kann die IR oder BT Fernsteuerung gleichermaßen verbaut werden. Der 4x4 Knicklenker wäre nun fertig.