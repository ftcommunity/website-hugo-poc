---
layout: "image"
title: "Manueller Kurbeltrieb"
date: 2020-12-28T20:47:03+01:00
picture: "52KurbelSperrwelle.jpg"
weight: "53"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Falls keine Dioden, Minitaster und Minimotoren mehr verfügbar sind, kann hier auch einfach manuell das Sperrdifferential betätigt werden.