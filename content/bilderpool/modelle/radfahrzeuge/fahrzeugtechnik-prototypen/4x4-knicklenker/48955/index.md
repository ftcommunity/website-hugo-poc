---
layout: "image"
title: "Powermotor Montage"
date: 2020-12-28T20:47:45+01:00
picture: "27aPowermotorMontageDetail.jpg"
weight: "27"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Bauplatte 3x1 38242 dient zur Abstützung des Powermotors