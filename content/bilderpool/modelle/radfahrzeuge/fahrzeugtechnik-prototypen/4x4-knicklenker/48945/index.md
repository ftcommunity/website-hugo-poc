---
layout: "image"
title: "Lagerbock komplett"
date: 2020-12-28T20:47:29+01:00
picture: "36Lagerbockhintenkomplett.jpg"
weight: "37"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Für Einstellarbeiten kann später der komplette Lagerbock einfach nach hinten gezogen werden.