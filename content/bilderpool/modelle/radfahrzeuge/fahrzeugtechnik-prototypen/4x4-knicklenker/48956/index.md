---
layout: "image"
title: "Powermotor mit Einzelteilen zur Befestigung"
date: 2020-12-28T20:47:47+01:00
picture: "26PowermotorEinzelteile.jpg"
weight: "26"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Das Z10 stützt sich im Zusammenbau axial ab, sodass es nicht von der Welle abfällt.