---
layout: "image"
title: "Abtriebswelle komplett"
date: 2020-12-28T20:47:34+01:00
picture: "33Abtriebswellekomplett.jpg"
weight: "34"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Achslänge 140/150mm. Auf sehr feste Nabenverbindung ist zu achten, eventuell muss man mehrere Naben ausprobieren, um die mit der größten Klemmkraft zu finden.