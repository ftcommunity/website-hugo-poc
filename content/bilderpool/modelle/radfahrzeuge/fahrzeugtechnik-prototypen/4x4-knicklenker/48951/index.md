---
layout: "image"
title: "Übersicht Einzelteile Lagerbock vorne"
date: 2020-12-28T20:47:39+01:00
picture: "30LagerbockvorneEinzelteile.jpg"
weight: "31"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen