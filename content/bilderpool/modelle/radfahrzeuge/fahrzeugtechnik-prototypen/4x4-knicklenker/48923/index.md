---
layout: "image"
title: "Halter für Taster"
date: 2020-12-28T20:46:53+01:00
picture: "58HalterfürTaster.jpg"
weight: "59"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind bereits die Halter für die motorisierte Version des Sperrdifferentials vorgesehen.