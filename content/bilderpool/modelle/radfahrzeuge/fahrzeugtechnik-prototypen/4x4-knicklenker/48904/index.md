---
layout: "image"
title: "Antriebsstrang"
date: 2020-12-03T20:34:59+01:00
picture: "9. Antriebsstrang einzeln.jpg"
weight: "9"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Fahrachsen 75mm Rastachse. Am Mitteldifferential je 60mm und vor dem Kardangelenk eine 45mm Rastachse verbaut