---
layout: "image"
title: "Detail vordere Übersetzung"
date: 2020-12-03T20:35:18+01:00
picture: "14. Detail vordere Übersetzung.jpg"
weight: "14"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Bauteile sind sauber auszurichten, damit ein spielfreies Zusammenspiel aller Komponenten möglich ist. 