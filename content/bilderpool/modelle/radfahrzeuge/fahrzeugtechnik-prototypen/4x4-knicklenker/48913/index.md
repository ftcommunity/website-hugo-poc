---
layout: "image"
title: "Übersicht Einzelteile für Grundaufbau vorne"
date: 2020-12-03T20:35:15+01:00
picture: "16. Übersicht Einzelteile für Grundaufbau vorne.jpg"
weight: "16"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen