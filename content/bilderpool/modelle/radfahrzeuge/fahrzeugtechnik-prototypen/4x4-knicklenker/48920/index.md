---
layout: "image"
title: "Seitenansicht rechts"
date: 2020-12-03T20:35:27+01:00
picture: "1. Seitenansicht rechts.jpg"
weight: "1"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Im Nachhinein ist ein Mitteldifferential bei einem Knicklenker gar nicht notwendig, da es sich hier um einen symmetrischen Achsabstand zum Knickgelenk handelt. Aus didaktischen Gründen ist es aber dringeblieben, ansonsten würde eine Sperre keinen Sinn machen. Danke an dieser Stelle den Hinweisen aus dem Forum. Ein Powermotor (8:1) mit Z10 auf Z20 treibt die Kugelgelagerte Zwischenwelle an.