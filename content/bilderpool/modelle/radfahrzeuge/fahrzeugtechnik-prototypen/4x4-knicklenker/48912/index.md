---
layout: "image"
title: "Übersicht Einzelteile für Grundaufbau hinten"
date: 2020-12-03T20:35:13+01:00
picture: "17. Übersicht Einzelteile für Grundaufbau hinten.jpg"
weight: "17"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen