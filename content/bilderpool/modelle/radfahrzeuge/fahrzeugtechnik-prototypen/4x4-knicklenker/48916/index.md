---
layout: "image"
title: "Detail Gelenkscheibe"
date: 2020-12-03T20:35:20+01:00
picture: "13. Detail Gelenkscheibe.jpg"
weight: "13"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Kurze 45mm Rastachse mit 15mm Distanzhülse schafft ein spielfreies und positionsgenaues Kardangelenk. Die Gelenkscheibe ist mit Verbindern gut zu befestigen.