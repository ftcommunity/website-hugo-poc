---
layout: "image"
title: "Übersicht Einzelteile der Schaltmechanik"
date: 2020-12-28T20:47:19+01:00
picture: "42SchlatmechanikEinzelteile.jpg"
weight: "43"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Schnecke muss später spielfrei sich an der linken Rasthülse abstützen und mit der Spannhülse verspannt werden, ansonsten geht der Gewindegang beim Drehen auf.