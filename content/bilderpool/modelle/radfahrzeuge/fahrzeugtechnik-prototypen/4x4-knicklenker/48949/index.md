---
layout: "image"
title: "Schaltwelle komplett"
date: 2020-12-28T20:47:36+01:00
picture: "32Schaltwellekomplett.jpg"
weight: "33"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Achslänge 140/150mm. Auf sehr feste Nabenverbindung ist zu achten, eventuell muss man mehrere Naben ausprobieren, um die mit der größten Klemmkraft zu finden.