---
layout: "image"
title: "Ansicht unten"
date: 2020-12-03T20:35:08+01:00
picture: "4. Ansicht unten.jpg"
weight: "4"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Antriebskette auf Mitteldifferential und Verstärkungsachsen in den BS 30.