---
layout: "image"
title: "Lagerbock Zwischenwelle hinten Einzelteile"
date: 2020-12-28T20:47:52+01:00
picture: "23LagerbockZwischenwellehintenEinzelteile.jpg"
weight: "23"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Einfach gehaltener Grundaufbau. Fehlende Längenangaben sind dem Raster zu entnehmen