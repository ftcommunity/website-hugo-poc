---
layout: "image"
title: "Sperrwelle mit manueller Betätigung"
date: 2020-12-28T20:47:10+01:00
picture: "48Sperrwellemanuell.jpg"
weight: "49"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Kann später in einem zusätzliche Bauschritt auf Motorbetrieb erweitert werden.