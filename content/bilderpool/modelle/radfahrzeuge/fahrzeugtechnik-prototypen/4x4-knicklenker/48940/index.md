---
layout: "image"
title: "Schaltmechanik mit Motor"
date: 2020-12-28T20:47:21+01:00
picture: "41SchaltmechanikmitMotor.jpg"
weight: "42"
konstrukteure: 
- "Wilhelm (wlh70)"
fotografen:
- "Wilhelm (wlh70)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier können beide Minitoren Type XS oder S verbaut werden