---
layout: "image"
title: "neuer Aufbau"
date: "2012-02-06T17:05:46"
picture: "Bild.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34101
- /detailsa43b.html
imported:
- "2019"
_4images_image_id: "34101"
_4images_cat_id: "2506"
_4images_user_id: "184"
_4images_image_date: "2012-02-06T17:05:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34101 -->
Ich habe hier den "alten Mitnehmer" getauscht. Drei Winkelsteine 15 Grad passen genau.
