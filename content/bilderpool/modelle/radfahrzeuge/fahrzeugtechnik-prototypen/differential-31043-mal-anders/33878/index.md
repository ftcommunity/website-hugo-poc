---
layout: "image"
title: "Einzelteile des Mitnehmers"
date: "2012-01-09T16:34:50"
picture: "DSCN4231.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/33878
- /details0f0f-2.html
imported:
- "2019"
_4images_image_id: "33878"
_4images_cat_id: "2506"
_4images_user_id: "184"
_4images_image_date: "2012-01-09T16:34:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33878 -->
lediglich 3 kleine Teile ermöglichen den funktionierenden Umbau.
