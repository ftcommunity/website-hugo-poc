---
layout: "image"
title: "überarbeitet"
date: "2012-02-09T13:12:13"
picture: "Blattfeder_02.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34129
- /detailsff24.html
imported:
- "2019"
_4images_image_id: "34129"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-02-09T13:12:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34129 -->
auf die Bauplatte 30x30 (hier nur von der Seite zu sehen) muss noch ein 32315 aufgeschoben werden.
