---
layout: "comment"
hidden: true
title: "16310"
date: "2012-02-09T14:55:11"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Na also, das wird doch! Sieht prima aus! Und sehr elegant!

Ich wäre aber nach wie vor dafür, auf die zusätzliche seitliche Befestigung der schwarzen Achsen am schwarzen Baustein 15 gänzlich zu verzichten! Ist doch technisch nicht notwendig.

Gruß, Thomas