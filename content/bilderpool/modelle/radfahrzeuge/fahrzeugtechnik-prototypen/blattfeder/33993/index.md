---
layout: "image"
title: "Blattfeder für LKW Anhänger"
date: "2012-01-23T14:53:33"
picture: "Blattfeder_1.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/33993
- /details3779-2.html
imported:
- "2019"
_4images_image_id: "33993"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-23T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33993 -->
Hier habe ich eine Blattfeder für einen LKW Anhänger gebaut.
Für Schwerlasten kann zusätzlich noch eine Feder 26 oder Feder 20 auf den inneren BS 15 mit Loch eingebaut werden.
