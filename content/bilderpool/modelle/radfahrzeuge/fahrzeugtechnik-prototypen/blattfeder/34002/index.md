---
layout: "image"
title: "Ansicht von unten"
date: "2012-01-24T11:51:12"
picture: "Blattfeder_7.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34002
- /details3362.html
imported:
- "2019"
_4images_image_id: "34002"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-24T11:51:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34002 -->
Die Konstruktion ist etwas aufwendiger als vorher, aber so ist der Halt nur in der Mitte.
