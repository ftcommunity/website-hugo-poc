---
layout: "image"
title: "neue Variante"
date: "2012-01-24T15:15:06"
picture: "Blattfeder_9.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34005
- /details79b1-2.html
imported:
- "2019"
_4images_image_id: "34005"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-24T15:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34005 -->
