---
layout: "overview"
title: "Kupplung mit pneumatischem Ausrücklager (Porsche-Makus)"
date: 2020-02-22T07:51:50+01:00
legacy_id:
- /php/categories/1562
- /categories2c07-2.html
- /categoriesb51f.html
- /categories6807.html
- /categories09fc.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1562 --> 
Ich habe eine Kupplung mit pneumatischem Ausrücker gebaut, die nach langem Experimentieren jetzt endlich sehr gut funktioniert.

Statt der beiden Pneumatikzylinder könnte man natürlich auch Hydraulische verwenden, dann kommt es der Realität noch ein Stück näher.

Über die Schnecke links neben der Kupplungsscheibe lässt sich der Schleifpunkt der Kupplung sehr fein und einfach einstellen.

Im entlasteten Zustand der Zylinder drücken die Federn die Kupplungsscheibe <schwarz/auf Welle 2 fixiert> voll gegen die Kupplungsdruckplatte <rot/auf Welle 1 fixiert>.

Die rote Scheibe links neben der Kupplungsscheibe trägt die Federn und ist NICHT mit der Stange verbunden, sondern nur durch ein freilaufendes Z15 auf der Welle zentriert.

Beaufschlagt man nun die Zylinder mit Druck, schieben diese das Ausrücklager gegen die ganz linke Scheibe <Schwarz/auf Welle 2 fixiert>, wodurch die ganz rechts Scheibe <schwarz/auf Welle 2 fixiert> allmählich nach links gezogen und gegen die Kraft der Sechs Federn ankämpfen muss.

Der Schleifpunkt lässt sich damit recht gut erzeugen.

Das mögliche übertragbare Drehmoment ist abhängig von der Anzahl, der Stärke und der Vorspannung der Federn. In diesem Modell habe ich sechs kurze Federn verwendet.

Auf den Scheiben ist KEIN extra Reibbelag oder so, alles ist mit nicht modifizierten, originalen Fischertechniktteilen gebaut.