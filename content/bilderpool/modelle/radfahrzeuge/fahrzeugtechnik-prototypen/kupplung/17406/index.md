---
layout: "image"
title: "Fischertechnik Kupplung wie im Deutsches Museum München"
date: "2009-02-14T14:51:04"
picture: "Mnchen-Obersalzberg-2-2007_087.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/17406
- /details042f.html
imported:
- "2019"
_4images_image_id: "17406"
_4images_cat_id: "848"
_4images_user_id: "22"
_4images_image_date: "2009-02-14T14:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17406 -->
Fischertechnik Kupplung wie im Deutsches Museum München
