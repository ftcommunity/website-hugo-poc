---
layout: "overview"
title: "Angetriebene, gefederte Vorderachse mit Differential und Standard-Servo"
date: 2020-02-22T07:53:16+01:00
legacy_id:
- /php/categories/3117
- /categories2aa6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3117 --> 
Eine 45er-Statikstrebe wurde gemodded, um das Standard-Servo einfach anzubauen