---
layout: "image"
title: "von oben"
date: "2015-09-30T15:41:37"
picture: "Vorderachse5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Vorderradantrieb"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/41912
- /details0cae-2.html
imported:
- "2019"
_4images_image_id: "41912"
_4images_cat_id: "3117"
_4images_user_id: "579"
_4images_image_date: "2015-09-30T15:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41912 -->
