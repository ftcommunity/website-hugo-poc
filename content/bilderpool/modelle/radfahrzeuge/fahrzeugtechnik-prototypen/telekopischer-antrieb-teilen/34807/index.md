---
layout: "image"
title: "Ganz lang innen"
date: "2012-04-21T16:46:54"
picture: "laengsverschieblicherantrieb2.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34807
- /details3f4e.html
imported:
- "2019"
_4images_image_id: "34807"
_4images_cat_id: "2511"
_4images_user_id: "104"
_4images_image_date: "2012-04-21T16:46:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34807 -->
Ansonsten ist der Aufbau beliebig simpel. Das Teil führt sich auch sehr brav selbst und kann in jeder Lage ordentlich Drehmoment übertragen. Weit aufgezogen ist etwas Verdrehung/Dämpfung dabei, aber nicht schlimm viel, weil die Zapfen an der Gegenseite sich dann doch gut verhaken. Auch ganz lang kann das Teil also ordentlich was ab.
