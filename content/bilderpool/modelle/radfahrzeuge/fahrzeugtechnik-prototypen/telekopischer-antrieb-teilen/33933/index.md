---
layout: "image"
title: "Doppelten Kardan mit längenausgleig"
date: "2012-01-15T19:08:53"
picture: "telekopischerantriebteilenmitlaengenausgleig1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/33933
- /detailse4ac.html
imported:
- "2019"
_4images_image_id: "33933"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33933 -->
31934: V-stein
35113: Spannzange
35115: Getriebewürfel
35467: Getriebeklaue
36227: Rastadapter
38446: Gelenkklaue