---
layout: "image"
title: "Axialer längenausgleig"
date: "2012-01-15T19:08:53"
picture: "telekopischerantriebteilenmitlaengenausgleig3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/33935
- /details9343.html
imported:
- "2019"
_4images_image_id: "33935"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33935 -->
32850: Riegelstein
36227: Rastadapter
38241: Bauplatte 15x30