---
layout: "image"
title: "Axialer längenausgleig"
date: "2012-01-15T19:08:54"
picture: "telekopischerantriebteilenmitlaengenausgleig8.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/33940
- /detailse1fa-2.html
imported:
- "2019"
_4images_image_id: "33940"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33940 -->
Der Haken kan man erzetsen durch ein kombination aus volgenden teilen:
36227: Rastadapter
38226: Seilklemmstift 
oder
107356: Seilklemmstift 

Der Seilklemmstift wird an beide enden über eine länge von 4mm abgedreht bis auf 2mm dicke, und wird dan in der Rastadapter gesteckt