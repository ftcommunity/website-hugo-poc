---
layout: "image"
title: "Vorne"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk5.jpg"
weight: "5"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33311
- /detailsb1aa.html
imported:
- "2019"
_4images_image_id: "33311"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33311 -->
Hier zu sehen sind die 2 Antriebsmotoren und der S-, bzw Mini-Motor für die Lenkung. Das im Bild linke Rad schaut ziemlich krumm und verbogen aus - in Wirklichkeit ist es aber nicht so. Die Hubstange für das Hubgetriebe ist extra etwas zu weit auf die eine Seite geschoben - somit kann in beide Richtungen gleich weit gelenkt werden, ohne dass das Hubgetriebe von der Zahnstange gleitet.