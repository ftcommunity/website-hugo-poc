---
layout: "image"
title: "Gesamtansicht"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk1.jpg"
weight: "1"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33307
- /details2394-2.html
imported:
- "2019"
_4images_image_id: "33307"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33307 -->
Hier mal ein Fahrwerk für Autos wie zB den Audi A6 etc. Ziel war es, im "Innenraum", bzw da, wo eigentlich einer wäre, möglichst viel Platz und einen ordentlichen Boden zu haben. Gelöst ist das Wie bei echten Autos: Unter der "Motorhaube" sitzen 2 XM-Motoren, die ihre Kraft auf ein möglichst nah an der Vorderachse sitzendes Mitteldifferenzial übertragen. Unter den Motoren ist ein Mini-, bzw S-Motor (wo ist da eigentlich der Unterschied?) mit Hubgetriebe für die Lenkung. Nach hinten geht aus dem Mitteldifferenzial die Achse zur Hinterachse. Die angetriebene Vorderachse ist "Billig und Einfach" mit Kardansgelenken aufgebaut, ähnlich wie bei Fredys "Fahrwerk für Allradfahrzeug" (http://ftcommunity.de/categories.php?cat_id=1007), weil es mir bei diesem Modell nicht um die Vorderachse, sondern um die Platzverhältnisse im zukünftigen Innenraum. 
Noch sind die Reifen zu groß für realistische PKW-Proportionen, aber bei einem größeren Fahrwerk wären mir wahrscheinlich wieder irgendwelche Teile ausgegangen. Das Fahrverhalten ist ganz gut - er beschleunigt ordentlich und erreicht eine recht hohe Endgeschwindigkeit - trotz doppelter 1:2 Untersetztung (von den Motoren aufs Mitteldifferenzial und vom Mitteldifferenzial auf die Hinter-, und Vorderachse). Vor Überlastungen Schützt ein "aufgeschraubtes Z15", oder wie auch immer das jetzt heißt (35112). 
Für einen Ausgiebigen Fahrtest war noch keine Zeit.
---------------------------------------------------------------------------------------------------------------
Hier im Bild ganz rechts sind die 2 XM-Motoren, darunter der Lenkmotor mit Hubgetriebe. Die Geschwungenen Träger, die mit dem Chassis verbunden sind, tragen im Wesentlichen zur Stabilität der Verbindung des vorderwagens mit dem Rest bei. Ich hätte natürlich auch einfach irgend was klobig-eckiges zur Mitte des Chassis bauen können, aber so hat man wieder etwas Platz im Innenraum gewonnen und das Desin wirkt sportlicher. 
Die Bilder sind ´bei weitem nicht die besten - jedoch muss ich erwähnen, dass sie unter grausamen Lichtverhältnissen und per Handy entstanden sind.