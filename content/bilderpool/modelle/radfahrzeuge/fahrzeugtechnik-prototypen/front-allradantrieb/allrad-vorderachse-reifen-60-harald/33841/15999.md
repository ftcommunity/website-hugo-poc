---
layout: "comment"
hidden: true
title: "15999"
date: "2012-01-06T11:14:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die DS hatte aber die Scheibenbremsen innen beim Getriebe, also nicht etwa außen in den Rädern, um die ungefederte Masse gering zu halten. Das kannst Du ja sicher noch eben einbauen... :-)