---
layout: "image"
title: "Allrad-R60-9881.JPG"
date: "2013-10-08T20:55:45"
picture: "Allrad-R60-9881.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37695
- /detailsb061.html
imported:
- "2019"
_4images_image_id: "37695"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T20:55:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37695 -->
Und noch'n Frontantrieb. Alles im Raster, nichts gemoddet. Die Stahlstifte mit zwei Zapfen zähle ich nicht. Sowas bleibt übrig, wenn ein BS15 mit zwei Zapfen in die ewigen Fischergründe geht - und das war ich nicht.



"So soll es sein, ..."
