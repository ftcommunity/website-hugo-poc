---
layout: "image"
title: "Allrad-R60-81.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad5.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33843
- /details3de1-2.html
imported:
- "2019"
_4images_image_id: "33843"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33843 -->
Der erste Gedanke ist natürlich, gleich zwei von den Achsen zu bauen und einen Knicklenker draus zu machen. Das hier ist ein "auf die Schnelle" zusammen gezimmerter Erprobungsträger. Hier fehlt natürlich noch der Antrieb selbst, und ein Mitteldifferenzial könnte man auch noch vorsehen.
