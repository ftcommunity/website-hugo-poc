---
layout: "image"
title: "Allrad-R60-9885.JPG"
date: "2013-10-08T20:58:51"
picture: "Allrad-R60-9885.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37696
- /details0a8b-2.html
imported:
- "2019"
_4images_image_id: "37696"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T20:58:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37696 -->
Die Oberseite. Die Lochstrebe hält den Aufbau seitlich im richtigen Abstand.



"... so kann es bleiben. ..."
