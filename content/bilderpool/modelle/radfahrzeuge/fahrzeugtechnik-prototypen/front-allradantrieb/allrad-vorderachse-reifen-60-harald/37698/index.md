---
layout: "image"
title: "Allrad-R60-9841.JPG"
date: "2013-10-08T21:04:05"
picture: "Allrad-R60-9841.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37698
- /details2c68-3.html
imported:
- "2019"
_4images_image_id: "37698"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2013-10-08T21:04:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37698 -->
Und kompakter geht es nimmer. Die "Göttin" Citroen DS hat ihr Update auch schon gekriegt.


"... Alles passt perfekt zusammen. "
