---
layout: "image"
title: "Allrad-R60-72.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad1.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33839
- /detailsd0f1-3.html
imported:
- "2019"
_4images_image_id: "33839"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33839 -->
Baureihe "70" ist jetzt komplett, mit Antrieb, Spurstange und Lenkung. Nichts ist gemoddet und die ganze Achse liegt im ft-Raster :-)) Der Preis dafür ist allerdings, dass das Differenzial nicht in der Mitte sitzt, was die Ergänzung zu einem Allradfahrzeug etwas komplizierter macht. Sollte doch mal das eine oder andere Teil ganz unglücklich auf ein Teppichmesser fallen (soll ja dem einen oder anderen hier im Forum schon mal passiert sein...), dann könnte auch ein Mitteldifferenzial "drin" sein.
