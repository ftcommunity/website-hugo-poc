---
layout: "image"
title: "Spurstange"
date: "2016-03-21T13:33:37"
picture: "lenkung06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43176
- /detailsf7d5-2.html
imported:
- "2019"
_4images_image_id: "43176"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43176 -->
Wer genau hinschaut kann einen Abstand zwischen Servohelbel und Spurstange erkennen. Dieser Abstand reicht aus, um die Bewegung bei vollem Lenkeinschlag auszugleichen (siehe nächstes Bild).