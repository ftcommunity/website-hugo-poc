---
layout: "image"
title: "Spurstange 3"
date: "2016-03-21T13:33:37"
picture: "lenkung08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43178
- /detailsbff9-3.html
imported:
- "2019"
_4images_image_id: "43178"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43178 -->
Der Sevohebel geht beim Einlenken natürlich etwas nach oben. Daher ist die Spurstange so gelager dass sie rechts und links nach oben verschiebbar ist.