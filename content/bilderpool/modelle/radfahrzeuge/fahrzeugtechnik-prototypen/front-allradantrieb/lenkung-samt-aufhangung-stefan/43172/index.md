---
layout: "image"
title: "Vorne"
date: "2016-03-21T13:33:37"
picture: "lenkung02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43172
- /details6697-2.html
imported:
- "2019"
_4images_image_id: "43172"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43172 -->
Die Lenkung ist so gebaut, dass sich fast nichts mehr verschieben kann und somit funktioniert derzeit alles problemlos (mehr Sicherheit gegen Verschieben der Bauteile wird es sicherlich noch geben-ist ja schließlich noch ein Prototyp)...