---
layout: "image"
title: "Achsschenkel 5"
date: "2016-03-21T13:33:37"
picture: "lenkung14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43184
- /details5a8a.html
imported:
- "2019"
_4images_image_id: "43184"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43184 -->
Die Kraftübertragung vom Differential zum Reifen erfolgt wie zu sehen über die Kegelräder...