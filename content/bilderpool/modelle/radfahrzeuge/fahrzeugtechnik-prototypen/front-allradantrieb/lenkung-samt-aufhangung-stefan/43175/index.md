---
layout: "image"
title: "Unterseite"
date: "2016-03-21T13:33:37"
picture: "lenkung05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43175
- /details4777.html
imported:
- "2019"
_4images_image_id: "43175"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43175 -->
Über dem Diffential fehlt noch eine Statik-Strebe, die dem Ganzen zusätzlich noch etwas Stabilität verleit.