---
layout: "image"
title: "Aufhängung"
date: "2016-03-21T13:33:37"
picture: "lenkung19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43189
- /details39f9-3.html
imported:
- "2019"
_4images_image_id: "43189"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43189 -->
Warum haben ft-Teile so "gutmütige" Toleranzen? Genau, damit man so eine Aufhängung ohne Probleme bauen kann ;-).

Die Anordung der Statik-Streben bewirkt, dass die Lenkung relativ zum Mittelteil mit dem Differntial verdrehbar in längsrichtung des Fahrzeuges und um eine im Bild gedachte, horizontal verlaufende Achse durch das Kardangelenk ist. Es können aber hohe Kräfte in längsrichtugn des Fahrzeuges übertragen werden und ein auch die einseitge Belastung eines Reifens (gegeben z.B. durch ein Hindernis vor einem Reifen) stellt kein Problem dar.