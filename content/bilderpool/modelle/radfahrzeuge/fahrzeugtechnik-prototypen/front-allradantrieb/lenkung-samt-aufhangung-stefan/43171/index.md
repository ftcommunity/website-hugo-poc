---
layout: "image"
title: "Gesamtansicht"
date: "2016-03-21T13:33:37"
picture: "lenkung01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/43171
- /details4934.html
imported:
- "2019"
_4images_image_id: "43171"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43171 -->
Ausgehend von der genialen Idee von Martin Wanke in einen Traktorreifen ein Getriebe zu pflanzen(http://ftcommunity.de/categories.php?cat_id=3166), kam bei mir dieser Prototyp heraus. Bis auf die zwei Kugellager ist alles ft.