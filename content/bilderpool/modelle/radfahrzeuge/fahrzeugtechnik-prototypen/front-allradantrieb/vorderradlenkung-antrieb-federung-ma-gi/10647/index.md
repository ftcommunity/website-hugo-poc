---
layout: "image"
title: "Nah von unten"
date: "2007-06-01T20:08:58"
picture: "vorderradlenkungmitantriebundfederung2.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10647
- /details3dce.html
imported:
- "2019"
_4images_image_id: "10647"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10647 -->
Nicht ganz scharf...