---
layout: "image"
title: "Allrad-R45-70.jpg"
date: "2011-12-31T19:55:10"
picture: "Allrad_Reifen451.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33827
- /details1adf-2.html
imported:
- "2019"
_4images_image_id: "33827"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2011-12-31T19:55:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33827 -->
Lang genug hab ich dran herum probiert: na also - irgendwas MUSSTE einfach noch mit den weißen Kegelzahnrädern machbar sein.
