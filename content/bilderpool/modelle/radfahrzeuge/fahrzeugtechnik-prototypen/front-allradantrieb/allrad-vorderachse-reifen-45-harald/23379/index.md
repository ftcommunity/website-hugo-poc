---
layout: "image"
title: "Allrad-R45-22.jpg"
date: "2009-03-06T10:11:31"
picture: "Allrad-R45-22.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23379
- /details79ad.html
imported:
- "2019"
_4images_image_id: "23379"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-06T10:11:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23379 -->
Hier mal ein wenig in Richtung SUV (M-Klasse?) gesponnen. Es kann aber genau so gut ein Traktor oder ein Unimog draus werden.
