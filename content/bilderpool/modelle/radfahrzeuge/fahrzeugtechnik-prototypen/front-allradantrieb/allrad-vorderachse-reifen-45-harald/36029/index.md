---
layout: "image"
title: "front8419.JPG"
date: "2012-10-21T18:23:02"
picture: "front8419.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36029
- /detailsf0c9.html
imported:
- "2019"
_4images_image_id: "36029"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2012-10-21T18:23:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36029 -->
Das Ganze zusammengesetzt. Die Lenkung ist auf rechtem Anschlag, weil die beiden S-Laschen aneinander stoßen. Die Gesamtbreite ist etwas ungewöhnlich "krumm", weil ich hier passende S-Streben gefunden hatte und damit es möglichst kompakt bleibt. Für die Karosserie wird mit 2x BS7,5 auf ganze Vielfache von 30mm aufgefüttert. Dadurch kommen die Räder auch tief genug nach innen in die Radkästen.
