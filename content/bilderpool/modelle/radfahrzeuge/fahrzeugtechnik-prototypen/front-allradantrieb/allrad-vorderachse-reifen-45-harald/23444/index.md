---
layout: "image"
title: "Mini-Allrad-R45-02.jpg"
date: "2009-03-11T16:33:04"
picture: "Mini-Allrad-R45-02.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23444
- /details0ffb.html
imported:
- "2019"
_4images_image_id: "23444"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-11T16:33:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23444 -->
Es geht auch etwas kleiner :-)
