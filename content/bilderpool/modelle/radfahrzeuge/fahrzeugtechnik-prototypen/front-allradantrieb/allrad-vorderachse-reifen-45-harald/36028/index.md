---
layout: "image"
title: "front8416.JPG"
date: "2012-10-21T18:19:01"
picture: "front8416.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36028
- /detailsb7e3-2.html
imported:
- "2019"
_4images_image_id: "36028"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2012-10-21T18:19:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36028 -->
Das ist die ganze Mechanik. Oben der Antriebsmotor, mitten das Differenzial nebst Achslängenausgleich über V-Stein 15x15x15 und darin das Impulszahnrad Z4, darunter der Lenkmotor mit Hubgetriebe. Wo links und rechts die Rastkegelzahnräder heraussstehen, werden die Achsschenkel der Radaufhängung eingesetzt, dann noch die Lenkung einhängen und fertig.
