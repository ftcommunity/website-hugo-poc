---
layout: "image"
title: "Allrad-R45-27.jpg"
date: "2009-03-06T10:08:53"
picture: "Allrad-R45-27.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23378
- /details7dc8-2.html
imported:
- "2019"
_4images_image_id: "23378"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-06T10:08:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23378 -->
Ansicht von unten. Reifen 45 und Reifen 80 sind einfach hintereinander auf der Achse montiert.
