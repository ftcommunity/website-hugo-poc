---
layout: "image"
title: "front8415.JPG"
date: "2012-10-21T18:15:11"
picture: "front8415.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/36027
- /detailsafed.html
imported:
- "2019"
_4images_image_id: "36027"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2012-10-21T18:15:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36027 -->
Die Radaufhängung ist hier deutlich stabiler als beim Vorgänger mit BS7,5 und Rastadapter. Dafür musste aber wieder scharfe Klingen ran: der BS15-Loch ist um 2 mm schlanker geworden, und die Rastachse mit Platte wurde rund gedreht (war für ein Zwischenstadium notwendig, der aktuelle Aufbau stört sich an den Ecken nicht mehr) und auch um 2 mm an einer Seite gekürzt. Für größeren Lenkeinschlag muss an einer der Statiklaschen 23,3 auch noch eine Ecke ab.
