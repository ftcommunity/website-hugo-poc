---
layout: "image"
title: "Allrad-R45-26.jpg"
date: "2009-03-06T10:06:20"
picture: "Allrad-R45-26.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/23377
- /detailsa50a.html
imported:
- "2019"
_4images_image_id: "23377"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-06T10:06:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23377 -->
Dank der Untersetzung direkt am Rad lassen sich recht ordentliche Kräfte übertragen.

Der Gelenkwürfel ist mit Absicht so eingebaut, dass er nach unten aus der Platte 15x45 herausrutschen könnte. Die Konstruktion da außen herum soll nämlich nur das Rad führen, aber kein Gewicht tragen. Das erledigt der Statikadapter 35975, auf dessen einem "Nubbel" die Rastkegelachse aufsitzt.
