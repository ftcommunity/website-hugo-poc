---
layout: "comment"
hidden: true
title: "13698"
date: "2011-02-26T10:33:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da macht einer Ernst mit dem Rampe-Befahren. :-) Müsste der Akku aber nicht eigentlich aufs Fahrzeug, oder ist das für Stromzufuhr von außen gedacht?

Gruß,
Stefan