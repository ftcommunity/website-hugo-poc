---
layout: "image"
title: "Detailblick 1 auf das Getriebe"
date: "2007-05-28T19:32:44"
picture: "allradprototyp08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10549
- /details8633-3.html
imported:
- "2019"
_4images_image_id: "10549"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10549 -->
Die hier gut sichtbare Achse mit einem Z15 und einem Z10 darauf wird von einem MiniMot bewegt, so dass sie in die davon verdeckten Z15 und Z20 eingreifen können.
