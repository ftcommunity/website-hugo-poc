---
layout: "image"
title: "Antrieb der Hinterachse"
date: "2007-05-28T19:32:44"
picture: "allradprototyp05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10546
- /detailsbe75.html
imported:
- "2019"
_4images_image_id: "10546"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10546 -->
Der funktionierte überraschenderweise besser, als dieses Foto ist ;-)
