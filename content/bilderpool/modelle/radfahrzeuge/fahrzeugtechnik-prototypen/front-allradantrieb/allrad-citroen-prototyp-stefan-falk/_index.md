---
layout: "overview"
title: "Allrad-Citroën Prototyp  (Stefan Falk)"
date: 2020-02-22T07:51:56+01:00
legacy_id:
- /php/categories/959
- /categoriesb2b7-2.html
- /categories63fd.html
- /categories10e7.html
- /categories78be.html
- /categoriesc0a0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=959 --> 
Alte Prototypen-Bilder des Fahrzeugs mit Lenkung, Federung, Niveauausgleich (deshalb Citroën) und Allradantrieb.