---
layout: "comment"
hidden: true
title: "4621"
date: "2007-11-20T20:32:59"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Feder drückte die verschiebliche Achse zurück auf den einen Gang, solange sie nicht von dem Hebel links in den anderen Gang gedrückt wurde. Das Getriebe überlebte den Prototypenstatus des Fahrwerks aber nicht und blieb im endgültigen Fahrzeug nicht enthalten.