---
layout: "image"
title: "Kraftverteilung für die Vorderräder"
date: "2007-05-28T19:32:44"
picture: "allradprototyp04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/10545
- /details337e.html
imported:
- "2019"
_4images_image_id: "10545"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10545 -->
Das hier sichtbare Differential wird angetrieben von dem hier nicht sichtbaren Differential, welches die Kraft zwischen Vorder- und Hinterachse verteilt. Das sichtbare Differential hier teilt diese Kraft zwischen linkem Vorderrad (der hier "rechte" Ausgang des Differentials) und rechtem Vorderrad (der hier "hintere" Ausgang des Differentials). Über mehrere Zwischen-Z10 gelangt diese Kraft zum Vorderbau.

Diese Mimik wurde für das endgültige Fahrzeug vereinfacht. Da gab es pro Achse einfach einen eigenen Motor. Beide Motoren waren in Serie geschaltet. Damit spart man sich das Zentraldifferential und einen ganzen Berg Reibungsverluste.
