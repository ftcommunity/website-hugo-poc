---
layout: "image"
title: "Ansicht 2"
date: "2007-04-30T18:47:24"
picture: "vorderradlenkung9.jpg"
weight: "9"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10247
- /details6f45.html
imported:
- "2019"
_4images_image_id: "10247"
_4images_cat_id: "929"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10247 -->
