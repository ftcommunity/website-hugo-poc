---
layout: "image"
title: "Differential"
date: "2007-04-30T18:47:24"
picture: "vorderradlenkung5.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10243
- /details5b29.html
imported:
- "2019"
_4images_image_id: "10243"
_4images_cat_id: "929"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10243 -->
Sonnst kann man ja nicht fahren.... Vorne im Bild die Lenkstange.