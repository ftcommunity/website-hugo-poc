---
layout: "image"
title: "Detailblick 1 auf das Getriebe"
date: "2006-05-21T17:52:59"
picture: "Allrad5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6282
imported:
- "2019"
_4images_image_id: "6282"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6282 -->
Die hier gut sichtbare Achse mit einem Z15 und einem Z10 darauf wird von einem MiniMot bewegt, so dass sie in die davon verdeckten Z15 und Z20 eingreifen können.
