---
layout: "image"
title: "Blick auf die Frontpartie"
date: "2006-05-21T17:52:59"
picture: "Allrad2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6279
imported:
- "2019"
_4images_image_id: "6279"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6279 -->
Antrieb: Der Antrieb kommt für das linke Rad (im Bild rechts oben) von hinten, für das rechte Rad durch eine der Achsen, an denen das Fahrwerk selbst aufgehängt ist vorne an. Über die Kardanwellen gelangt die Kraft bis zu den über den Vorderrädern angebrachten Reibrädern, die mit den Rädern federn und einlenken.
