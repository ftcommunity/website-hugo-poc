---
layout: "comment"
hidden: true
title: "1105"
date: "2006-05-22T19:30:58"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja wie denn, keine Automatik??!

;-)

Ein sehr schönes Teil! Und als echter "Franzose" natürlich mit Niveauregulierung  :-D


Aber bei den vielen Differenzialen blick ich nicht ganz durch. Wie kommt denn das Drehmoment aus dem Diff-Körper links wieder heraus?
Das vordere schwarze Diff (mit Z20 drauf) liegt wohl "zwischen" den Vorderrädern, das andere (nur Kegelkranz) ist das Längsdiff zwischen Vorder- und Hinterachse. Aber was passiert dazwischen, vom Schaltgetriebe her?


Damit die Niveauregulierung automatisch funktioniert, müsste doch noch irgendwo ein Neigungs/Federwegsensor versteckt sein, oder?

Gruß,
Harald