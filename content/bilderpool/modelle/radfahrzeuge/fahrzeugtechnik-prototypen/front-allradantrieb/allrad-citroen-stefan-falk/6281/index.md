---
layout: "image"
title: "Blick auf die Unterseite"
date: "2006-05-21T17:52:59"
picture: "Allrad4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6281
imported:
- "2019"
_4images_image_id: "6281"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6281 -->
Man sieht die fast schon primitive, aber höchst wirkungsvolle Hinterachskonstruktion, deren Antrieb wie bei der Vorderachse über Reibräder geschieht. Von links nach rechts folgen das Getriebe, der MiniMot zum Schalten des Getriebes, die Vorderachskonstruktion sowie der MiniMot für die automatische Niveauregulierung vorne.
