---
layout: "image"
title: "Frontantrieb II, Gesamtansicht 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42578
- /detailsfcd2-2.html
imported:
- "2019"
_4images_image_id: "42578"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42578 -->
Ich wollte eine lenkbare, angetriebene Portalachse haben mit Federung und halbwegs schmaler Spurbreite. Und das ganze nur mit original ft-Teilen. 
War 'ne echte Herausforderung (und deswegen habe ich auch so lange nichts von mir hören lassen).