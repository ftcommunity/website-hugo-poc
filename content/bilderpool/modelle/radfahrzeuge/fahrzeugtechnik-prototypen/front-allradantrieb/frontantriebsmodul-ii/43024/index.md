---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Lenkung 1"
date: "2016-03-10T12:37:42"
picture: "98_nochmal_verbesserte_Lenkung_1.jpg"
weight: "65"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43024
- /details9f49.html
imported:
- "2019"
_4images_image_id: "43024"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43024 -->
Die Lenkung ist auch nochmal verbessert. In der vorherigen Version war der Servo nich richtig fest und hat sich bei großen Lenkkräften seitlich wegbeweg. Jetzt sitzt er fest. Auch der Lenkbalken ist nochmal verändert.