---
layout: "comment"
hidden: true
title: "21700"
date: "2016-02-16T22:35:15"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Harald, bequem ist echt was anderes - Der Stuhl am Basteltisch kann gar nicht so bequem sein dass man ohne Rückenschmerzen und Frust sowas hinbekommt, oder hast du einen Luftkissen-Supersessel? Aber zum Glück gibts ja hartnäckige Menschen hier, die sich nicht unterkriegen lassen - dazu zähle ich mich auch - nur gibt es sicher auch viele Kunden die dazu keine Lust haben.