---
layout: "image"
title: "Frontantrieb II verbessert, Lenkung 4"
date: "2016-02-15T17:21:21"
picture: "96_verbesserte_Lenkung_4.jpg"
weight: "58"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42875
- /details5929.html
imported:
- "2019"
_4images_image_id: "42875"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-02-15T17:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42875 -->
Nebenbei habe ich noch die Radaufhängung stablisiert mit zwei Statkisteinen anstelle der Winkelsteine und einer Flügelachse dazwischen. Das engt zwar das Differential etwas mehr ein, aber nciht so stark, dass es klemmt.