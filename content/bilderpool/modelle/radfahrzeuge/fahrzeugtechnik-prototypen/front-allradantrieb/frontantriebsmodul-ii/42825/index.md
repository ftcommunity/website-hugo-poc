---
layout: "image"
title: "Frontantrieb II verbessert, Rad-Innenansicht 2"
date: "2016-01-27T21:37:09"
picture: "75_verbessertes_Rad_-_Innenansicht_2.jpg"
weight: "45"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42825
- /details0aee.html
imported:
- "2019"
_4images_image_id: "42825"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42825 -->
Grau und schwarz in Schwarz. Fototechnische Katastrophe.