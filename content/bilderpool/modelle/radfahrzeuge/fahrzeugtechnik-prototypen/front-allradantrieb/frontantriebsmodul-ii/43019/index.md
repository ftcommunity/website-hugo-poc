---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Radaufhängung 1"
date: "2016-03-10T12:37:42"
picture: "97_nochmal_verbesserte_Radaufhngung_1.jpg"
weight: "60"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43019
- /details96ae.html
imported:
- "2019"
_4images_image_id: "43019"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43019 -->
So ist die Radaufhängung jetzt dauerbetriebstauglich und läuft auch noch etwas leichter als vorher. Als Felge verwende ich jetzt eine Scheibe, die im Gegensatz zu der Knoteplatte fester mit dem Innenzahnrad verbunden ist.