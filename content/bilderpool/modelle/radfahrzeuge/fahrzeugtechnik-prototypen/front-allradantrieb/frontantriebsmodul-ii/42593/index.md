---
layout: "image"
title: "Frontantrieb II, Versatzgetriebe 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul19.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42593
- /details322f-3.html
imported:
- "2019"
_4images_image_id: "42593"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42593 -->
Zur Veranschaulichung des Versatzgetriebes habe ich den Reifen mal durchsichtig gemacht. Die roten V-Achsen muss man sich wegdenken, die stecken nur zur Veranschaulichung drin.