---
layout: "comment"
hidden: true
title: "21447"
date: "2015-12-28T12:57:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ah, dieselbe Technologie wie in Haralds Stealth-Flieger :-)

Ich gratuliere, was eine Anhäufung von Ideen pro cm³! Da trübt die Tatsache, dass man offenbar so außerplanmäßig arbeiten muss, um eine vernünftige Portalachse mit ft zu bauen, nur mäßig ;-)

Wann kommt der Unimog oben drauf?

Gruß,
Stefan