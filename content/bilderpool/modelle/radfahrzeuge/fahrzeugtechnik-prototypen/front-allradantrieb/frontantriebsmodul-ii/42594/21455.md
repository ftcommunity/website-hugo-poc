---
layout: "comment"
hidden: true
title: "21455"
date: "2015-12-28T22:37:56"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Auseinanderdrücken muss man das nicht wirklich. Es ist darauf ausgelegt, dass man die welle mit dem ersten Ritzel rauszieht, ich nehme an, damit man das Zahnrad wechseln kann, wenn es verschlissen ist. Man kann das Z10 übrigens auch einzeln kaufen.