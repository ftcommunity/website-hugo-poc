---
layout: "image"
title: "Frontantrieb II, Gesamtansicht 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42576
- /details271a.html
imported:
- "2019"
_4images_image_id: "42576"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42576 -->
Der Winter ist da (welcher WInter?) - Zeit für ft. Und im Winter kann man Allradantrieb gut brauchen, also Zeit für eine neue Variante angetriebener Vorderachsen.