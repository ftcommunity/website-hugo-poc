---
layout: "image"
title: "Frontantrieb II, Radaufhängung 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul15.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42589
- /detailsd4fe-2.html
imported:
- "2019"
_4images_image_id: "42589"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42589 -->
Das Innere der Radnabe ist von ganz tief unten aus der Kreativkiste.