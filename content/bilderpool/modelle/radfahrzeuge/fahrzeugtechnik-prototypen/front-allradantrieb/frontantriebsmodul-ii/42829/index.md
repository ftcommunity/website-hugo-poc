---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung-Details 4"
date: "2016-01-27T21:37:09"
picture: "80_verbesserte_Radaufhngung_-_Details_4.jpg"
weight: "49"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42829
- /detailsa991.html
imported:
- "2019"
_4images_image_id: "42829"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42829 -->
Der Verbindungsstopfen hält relativ fest in der Spurstange und kann sich in dem inneren Loch der schwarzen Statikstrebe frei drehen. So ergibt das wieder ein halbwegs gutes Lenktrapez.