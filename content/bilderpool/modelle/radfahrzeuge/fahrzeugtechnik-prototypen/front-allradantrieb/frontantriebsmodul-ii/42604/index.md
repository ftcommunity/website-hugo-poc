---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 6"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul30.jpg"
weight: "30"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42604
- /detailsab5a.html
imported:
- "2019"
_4images_image_id: "42604"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42604 -->
Ich fürchte, dass die Spurstange abrutscht, wenn die Lenkkräfte zu groß werden. Aber das wird erst der Einsatz in einem Geländewagen zeigen.