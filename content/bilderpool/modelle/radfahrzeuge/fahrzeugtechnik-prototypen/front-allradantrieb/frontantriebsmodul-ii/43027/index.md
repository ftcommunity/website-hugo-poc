---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Lenkung 4"
date: "2016-03-10T12:37:42"
picture: "98_nochmal_verbesserte_Lenkung_4.jpg"
weight: "68"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43027
- /detailsbfa5.html
imported:
- "2019"
_4images_image_id: "43027"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43027 -->
Der Lenkbalken von unten gesehen.
Die &#180;BSB-Pleuelstangen sind jetzt mit vier Riegelscheiben verbunden, weil zwei bei großen Lenkkräften nicht gehalten haben.