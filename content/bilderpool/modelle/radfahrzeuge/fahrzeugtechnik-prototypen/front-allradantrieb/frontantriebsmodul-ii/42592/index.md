---
layout: "image"
title: "Frontantrieb II, Rad-Innenansicht"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul18.jpg"
weight: "18"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42592
- /detailse752-2.html
imported:
- "2019"
_4images_image_id: "42592"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42592 -->
Im Reifen sind zwei Innenzahnräder, die durch vier Klemmen auseinandergehalten werden. In den Klemmen sind keine Achsen, sie sind einfach so zwischen die Zahnräder geschoben und halten durch den Anpressdruck. Es ist eine riesen Fummelei, das alles in den Reifen zu reinzukriegen :-(. Das eine Zahnrad ist Teil des Versatzgetzriebes, das andere dient nur dazu, den Reifen aufzuspannen.
Die Außenseite (oben im Bild) des Reifens ist durch einen Kranz aus Riegelscheiben noch etwas weiter ausgestellt, sonst passen die ganzen folgenden Innrerein des Getriebes nicht hinein.
