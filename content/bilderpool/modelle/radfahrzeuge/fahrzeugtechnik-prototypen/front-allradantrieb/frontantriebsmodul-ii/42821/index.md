---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung 1"
date: "2016-01-27T21:37:09"
picture: "70_verbesserte_Radaufhngung_1.jpg"
weight: "41"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42821
- /details3eeb.html
imported:
- "2019"
_4images_image_id: "42821"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42821 -->
Wie versprochen, hier eine verbesserte Version, die auch unter Last gut funktioniert. Das erste provisorische Testfahrzeug kann jetzt ordentliche Rampen und Hindernisse überwinden.