---
layout: "image"
title: "Frontantrieb II verbessert, Rad-Innenansicht 1"
date: "2016-01-27T21:37:09"
picture: "75_verbessertes_Rad_-_Innenansicht_1.jpg"
weight: "44"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42824
- /details505d.html
imported:
- "2019"
_4images_image_id: "42824"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42824 -->
Im Reifen ist jetzt nur noch ein Innenzahnrad und er wird durch sechs K-Achsen-30 aufgespannt, die auch die Sternlasche zentrieren. Auf drei der Achsen sind Lagerhülsen geschoben als Abstandshalter zwischen Sternlasche und Zahnrad. Es ist leider auch in dieser verbesserten Version  immer noch eine Fummelei, das alles in den Reifen zu reinzukriegen.