---
layout: "image"
title: "Frontantrieb II, Lenkeinschlag 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42585
- /detailsdeec.html
imported:
- "2019"
_4images_image_id: "42585"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42585 -->
Wenn die Lenkung nicht voll eingeschlagen ist, gibt es auch ein Lenktrapez (wirklich trapezförmig, kein Parallelogramm), das äußere Rad schlägt nämlich etwas weniger ein als das innere. Leider nicht mehr bei vollem Einschlag, siehe nächstes Bild.