---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul25.jpg"
weight: "25"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42599
- /details0793-2.html
imported:
- "2019"
_4images_image_id: "42599"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42599 -->
Das Kardangelenk und das Z10 werden auf dem Achsschenkel in einer Gelenkwürfel-Klaue gelagert und mit einem Stück Kette fixiert.