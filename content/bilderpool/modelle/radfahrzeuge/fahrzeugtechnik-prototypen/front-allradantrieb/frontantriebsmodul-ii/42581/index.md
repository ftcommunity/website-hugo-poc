---
layout: "image"
title: "Frontantrieb II, Bodenfreiheit 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42581
- /details6f63.html
imported:
- "2019"
_4images_image_id: "42581"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42581 -->
Fast überall ist die Bodenfreiheit größer als der halbe Raddurchmeser.