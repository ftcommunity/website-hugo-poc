---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul26.jpg"
weight: "26"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42600
- /details1c7a-3.html
imported:
- "2019"
_4images_image_id: "42600"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42600 -->
Das Förder-Kettenglied unten dient zur Befestigung am BS15, aber nur mit einer Seite, die andere Hängt in der Luft. Wenn man es ganz in die Nut schiebt, wird die Kette zu schräg. Ein weiteres Förder-Kettenglied weiter oben dient dazu, die Kette an der Glenkwürfel-Klaue zu fixieren.