---
layout: "image"
title: "Frontantrieb II nochmal verbessert, Lenkung 2"
date: "2016-03-10T12:37:42"
picture: "98_nochmal_verbesserte_Lenkung_2.jpg"
weight: "66"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/43025
- /detailsaf79-3.html
imported:
- "2019"
_4images_image_id: "43025"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-03-10T12:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43025 -->
Der Servo ist jetzt ist einen Käfig aus vier Statiklaschen "eingepackt". Hier habe ich ihn vorne mal geöffnet.
Der Lenkbalken wird jetzt doch wieder direkt vom Lenkhebel bewegt, und nicht mehr mit Hilfe des Lochs. So ist der Lenkweg größer, und weil jetzt der Servo fest sitzt geht das auch präzise genug.