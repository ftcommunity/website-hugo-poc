---
layout: "image"
title: "Allradantrieb 1"
date: "2007-04-29T11:33:10"
picture: "allradantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10187
- /details5115-2.html
imported:
- "2019"
_4images_image_id: "10187"
_4images_cat_id: "1589"
_4images_user_id: "502"
_4images_image_date: "2007-04-29T11:33:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10187 -->
Das Z20 ganz oben wird festgehalten damit sich der Antrieb nich auflöst. An die Achse daneben kommt der Antrieb.
