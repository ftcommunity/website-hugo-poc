---
layout: "image"
title: "Gesamtansicht"
date: "2011-02-25T22:29:44"
picture: "A1_.jpg"
weight: "1"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Offroad", "Achse", "Gelände", "Kardan", "Antrieb"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/30128
- /detailsc65a-2.html
imported:
- "2019"
_4images_image_id: "30128"
_4images_cat_id: "2227"
_4images_user_id: "41"
_4images_image_date: "2011-02-25T22:29:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30128 -->
Eine kompakte und stabile(!) angetriebene Vorderachse. Auf ein Differential wurde bewusst verzichtet, weil das später noch "drumrum" zu bauende Modell geländegängig werden soll. Die Spurstange ist ein klein wenig zu kurz, dadurch ergibt sich eine Vorspur - die in der Realität auch vorhanden ist und ein stabileres Fahrverhalten bewirkt.
