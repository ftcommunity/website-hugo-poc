---
layout: "image"
title: "Detail Achsschenkel"
date: "2011-02-25T22:43:34"
picture: "A3_.jpg"
weight: "3"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/30130
- /details83bb.html
imported:
- "2019"
_4images_image_id: "30130"
_4images_cat_id: "2227"
_4images_user_id: "41"
_4images_image_date: "2011-02-25T22:43:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30130 -->
Detail des linken Achsschenkels, die Spurstange wurde entfernt
