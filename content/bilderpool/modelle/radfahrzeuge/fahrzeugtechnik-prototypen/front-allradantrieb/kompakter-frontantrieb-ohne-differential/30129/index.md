---
layout: "image"
title: "Ansicht ohne Räder"
date: "2011-02-25T22:43:34"
picture: "A2_.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/30129
- /details8ec0.html
imported:
- "2019"
_4images_image_id: "30129"
_4images_cat_id: "2227"
_4images_user_id: "41"
_4images_image_date: "2011-02-25T22:43:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30129 -->
Ohne Räder sieht man den Aufbau der Achsschenkel. Ich liebe den Baustein 7,5... ;-)
