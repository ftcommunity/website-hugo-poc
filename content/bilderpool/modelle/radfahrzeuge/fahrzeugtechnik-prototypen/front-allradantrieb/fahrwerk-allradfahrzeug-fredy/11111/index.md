---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug06.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11111
- /detailsa108.html
imported:
- "2019"
_4images_image_id: "11111"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11111 -->
Der Endschalter für die Endposition der Lenkung, es ist auf beiden Seiten einer angebracht.
