---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug04.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11109
- /details4ba9.html
imported:
- "2019"
_4images_image_id: "11109"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11109 -->
Auf diesem Bild ist der zu sehen wie der Power Motor an dem Diff angebracht ist, man sieht auch an welcher Stelle die Welle nach vorne angerarcht ist.
