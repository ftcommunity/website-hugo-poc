---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug03.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11108
- /details2afd.html
imported:
- "2019"
_4images_image_id: "11108"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11108 -->
Hier sieht man den Antrieb der Lenkung und die Kardan Welle.
