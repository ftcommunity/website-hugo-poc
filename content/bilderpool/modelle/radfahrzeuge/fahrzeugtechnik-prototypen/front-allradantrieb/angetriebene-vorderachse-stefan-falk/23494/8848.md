---
layout: "comment"
hidden: true
title: "8848"
date: "2009-03-27T16:42:47"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Ohne Zweifel als Start zum Thema eine gelungene Konstruktion. Was du aber selber auch siehst, ist sie noch nicht das Ende der "Fahnenstange" in der modellhaften Nachbildung der Realität. 
1. Bei Geradeausfahrt ist quer zur Fahrtrichtung der Radstand am grössten und beim max. Lenkeinschlag links oder  rechts am kleinsten.
2. Der Antrieb kommt nicht von innen.
3. Es ist noch keine konzipierte Federung vorhanden.
Das packst du auch noch. Ich weiss es!
Gruss, Ingo