---
layout: "image"
title: "Lenkung (1)"
date: "2009-03-26T21:25:13"
picture: "vorderachse4.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23523
- /details2588.html
imported:
- "2019"
_4images_image_id: "23523"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23523 -->
Hier die Ansicht von unten. Man sieht, wie der Motor immer senkrecht zur Differentialachse und zu den schwarzen Statikstreben bleibt.
