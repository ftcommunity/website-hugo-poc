---
layout: "image"
title: "Noch eine Gesamtansicht"
date: "2009-03-26T21:25:13"
picture: "vorderachse6.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23525
- /details8a30-3.html
imported:
- "2019"
_4images_image_id: "23525"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23525 -->
Evtl. gingen auch I-Streben 90 anstatt 120, aber ob das dann ohne die Rastkupplung und den Federnocken auf beiden Seiten auch gerade so gut aufgeht, müsste man noch probieren.
