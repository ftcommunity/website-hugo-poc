---
layout: "image"
title: "Lenkeinschlag (1)"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23496
- /details1c71-2.html
imported:
- "2019"
_4images_image_id: "23496"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23496 -->
Beim Lenken bleibt so ungefähr überhaupt nichts an seiner Stelle. Die Reifen drehen so aber sehr schön über ihrem Aufstandsmittelpunkt. Vergleiche das nächste Bild.
