---
layout: "image"
title: "Gesamtansicht (2)"
date: "2009-03-24T00:03:23"
picture: "angetriebenevorderachse2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/23495
- /details6d4a.html
imported:
- "2019"
_4images_image_id: "23495"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23495 -->
Der Antrieb sollte ordentlich Schmackes haben. Hier habe ich mal eine Variante mit Kettenantrieb probiert.

Die Z30 drehen hier zwar nur an der Achse, auf dem der Reifen auch aufgespannt ist, was eine Drehmomentgrenze in sich birgt, weil das irgendwann an der Achse selbst durchdreht. Allerdings kann man das Z30 und die Radnabe bombenfest miteinander verbinden, wenn man in die drei Löcher des Z30 je eine kurze Achse steckt. Die Nabe ist nämlich eine Spur zu klein, als dass diese Achsen hineinpassen würden, und so klemmen die sehr fest an der Nabe. Damit sollte heftig Drehmoment übertragen werden (wobei dann die Kardangelenke wohl wieder das schwächste Glied wären).

Die von unten von der schwarzen Bauplatte her kommenden Metallachsen deuten die Punkte an, die auch in einem echten Fahrzeug zur Aufhängung der Achse verwendet werden müssten, da wirklich alles andere sich beim Lenken verdreht.
