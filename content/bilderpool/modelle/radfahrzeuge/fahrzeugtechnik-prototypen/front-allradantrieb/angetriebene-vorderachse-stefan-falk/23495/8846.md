---
layout: "comment"
hidden: true
title: "8846"
date: "2009-03-26T21:17:12"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich weiß nicht, ob ich Dich richtig verstanden habe. Sicherlich kann man beliebig hoch Lochstreben über den jetzigen Anbringen, aber es beim Lenken sind diese beiden Löcher imho tatsächlich die einzigen Punkte, die wenigstens einigermaßen ortsfest bleiben (ganz genau tun sie's nämlich nicht, weil ihr Abstand geringfügig variiert). Ich glaube, man sieht es besser auf den Fotos mit Aufhängung der Achse, die ich eben hochgeladen habe.

Gruß,
Stefan