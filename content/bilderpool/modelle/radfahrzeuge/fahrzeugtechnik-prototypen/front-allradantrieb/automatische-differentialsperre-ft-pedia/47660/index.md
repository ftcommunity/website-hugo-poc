---
layout: "image"
title: "Automatische Differentialsperre aus der ft:pedia"
date: "2018-05-18T18:40:11"
picture: "sperrefuermitteldifferential01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47660
- /detailscbcb-2.html
imported:
- "2019"
_4images_image_id: "47660"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47660 -->
In der ft:pedia   ( https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-1.pdf )
und auf Youtube   ( https://www.youtube.com/watch?v=OPzr2LrQjrI )
hatte ich diese automatische Differentialsperre vorgestellt, zu der hier noch ein paar weitere Ideen hinzukommen.