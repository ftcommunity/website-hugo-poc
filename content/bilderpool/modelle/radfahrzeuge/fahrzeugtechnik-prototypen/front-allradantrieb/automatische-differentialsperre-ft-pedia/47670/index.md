---
layout: "image"
title: "Sperre für Mitteldifferential 9"
date: "2018-05-18T18:41:08"
picture: "sperrefuermitteldifferential11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47670
- /detailsfef6-2.html
imported:
- "2019"
_4images_image_id: "47670"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:41:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47670 -->
Leider komische Farben und unscharf :-(

An dem Funkitonsmodell kann man fühlen, dass das Drehmoment an der Welle für die Hinterachse größer wird, wenn man sie mit der Hand blockieren will.