---
layout: "overview"
title: "Automatische Differentialsperre aus der ft:pedia - es geht weiter"
date: 2020-02-22T07:52:31+01:00
legacy_id:
- /php/categories/3514
- /categoriesc3b4.html
- /categoriesc119.html
- /categoriesc584.html
- /categories3c6d.html
- /categoriesbd0e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3514 --> 
In der ft:pedia   ( https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-1.pdf )
und auf Youtube   ( https://www.youtube.com/watch?v=OPzr2LrQjrI )
hatte ich eine automatische Differentialsperre vorgestellt, zu der hier noch ein paar weitere Ideen hinzukommen.