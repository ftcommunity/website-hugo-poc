---
layout: "image"
title: "Sperre für Mitteldifferential 3"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47664
- /detailsd36e.html
imported:
- "2019"
_4images_image_id: "47664"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47664 -->
Die Welle unter dem Motor geht vom Mitteldifferential zur Vorderachse. Das hier sichtbare Diefferential ist das "Rechendifferential".