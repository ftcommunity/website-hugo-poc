---
layout: "image"
title: "Sperre für Mitteldifferential 8"
date: "2018-05-18T18:40:13"
picture: "sperrefuermitteldifferential10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/47669
- /details0165.html
imported:
- "2019"
_4images_image_id: "47669"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47669 -->
Der Klemmadapter hat keine wichtige Funktion, er dient nur als zusätzliche Führung.