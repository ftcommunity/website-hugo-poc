---
layout: "image"
title: "Lenkeinschlag nach links"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11201
- /details6d19-2.html
imported:
- "2019"
_4images_image_id: "11201"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11201 -->
Hier sieht man, wie es von unten aussieht, wenn die Lenkung ganz nach links eingeschlagen wurde.
