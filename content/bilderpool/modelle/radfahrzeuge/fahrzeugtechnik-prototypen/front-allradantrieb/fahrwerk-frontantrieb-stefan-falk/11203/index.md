---
layout: "image"
title: "Fahrwerk von unten"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11203
- /details5b2d-2.html
imported:
- "2019"
_4images_image_id: "11203"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11203 -->
Ein Blick auf das Gesamtfahrwerk von unten mit nach links eingeschlagener Lenkung. Man sieht ganz gut, dass die als Querträger fungierenden schwarzen Streben beim Lenken nicht mehr parallel zum MiniMot stehen.
