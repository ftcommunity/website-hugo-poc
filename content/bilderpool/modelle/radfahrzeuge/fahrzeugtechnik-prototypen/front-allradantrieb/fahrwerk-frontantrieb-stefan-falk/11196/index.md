---
layout: "image"
title: "Gesamtansicht"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11196
- /details7f06.html
imported:
- "2019"
_4images_image_id: "11196"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11196 -->
Der Antrieb wurde nochmal geändert: Eine Schnecke ist's jetzt, denn beim Vorderachsen-Prototypen störten die auf beiden Seiten herausragenden Achsen. Außerdem hat das Fahrwerk eine Lenkung, federt vorne auch etwas und hat eine schön weiche Hinterradfederung.
