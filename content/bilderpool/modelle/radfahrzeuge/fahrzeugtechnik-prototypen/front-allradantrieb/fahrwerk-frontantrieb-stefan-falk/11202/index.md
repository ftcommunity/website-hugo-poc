---
layout: "image"
title: "Hinterachse"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11202
- /detailsbdd8.html
imported:
- "2019"
_4images_image_id: "11202"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11202 -->
Noch eine Einzelradaufhängung an Längslenkern, Marke "Primitiv".
