---
layout: "image"
title: "Achsschenkel 3"
date: "2007-10-02T08:32:54"
picture: "Achsschenkel_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12060
- /details3994.html
imported:
- "2019"
_4images_image_id: "12060"
_4images_cat_id: "1078"
_4images_user_id: "328"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12060 -->
Die Schneckenmutter mit dem Abstandsring 31597 von außen. Passt natürlich nicht exakt wie ein Kugellager, aber meiner Meinung nach reicht es.