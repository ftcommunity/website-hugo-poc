---
layout: "image"
title: "Achsschenkel 2"
date: "2007-10-02T08:32:54"
picture: "Achsschenkel_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12059
- /details930b.html
imported:
- "2019"
_4images_image_id: "12059"
_4images_cat_id: "1078"
_4images_user_id: "328"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12059 -->
Zerlegt sieht man gut die Riegelscheibe 36334 und die beiden Abstandsringe 31597, die in die Schneckenmutter geschoben werden und die Lagerung der Achse übernehmen.