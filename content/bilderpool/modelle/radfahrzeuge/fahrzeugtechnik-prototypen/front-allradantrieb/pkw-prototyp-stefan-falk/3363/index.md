---
layout: "image"
title: "Fahrwerks-Prototyp (1)"
date: "2004-12-02T17:50:39"
picture: "Fahrwerk_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3363
- /details92e8-2.html
imported:
- "2019"
_4images_image_id: "3363"
_4images_cat_id: "642"
_4images_user_id: "104"
_4images_image_date: "2004-12-02T17:50:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3363 -->
Das ist der Prototyp einer Achse eines Fahrwerks für einen geplanten Kranwagen. Er soll an jedem Rad angetrieben, gefedert und lenkbar sein. Man sieht hier den ausgefederten Zustand (weil der Kran mit seinem Gewicht oben drauf noch fehlt). Die Federstärke ist aber durch anpassen der hier 2 grauen Grundbausteine in der Länge justierbar. Die "Federn" sind übrigens die Gummiringe, die über die guten alten Reifen 45 passen. Alles Original ft unmodifiziert.