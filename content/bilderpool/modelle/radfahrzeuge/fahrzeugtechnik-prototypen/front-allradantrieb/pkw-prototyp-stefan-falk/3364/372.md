---
layout: "comment"
hidden: true
title: "372"
date: "2004-12-07T15:18:20"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Danke für den Kommentar!Ich weiß, bei dieser Bauart wird das Fahrwerk ganz schön breit (deshalb liegt das Differential auch außerhalb der eigentlichen Antriebsachse, sonst wäre es noch breiter). Und es endet in einer Kreisbewegung des Rades, solange man nur ein Kardan pro Rad verbauen kann. Das ganze wird dann, wie auf den Bildern zu sehen, klobig und alles andere als elegant. Allerdings soll der Kranarm auch mindestens zweifach, lieber dreifach ausfahrbar sein - und wird damit auch heftig groß werden. Ich bin aber einer von denen, die es nicht mal übers Herz bringen, ein Selbstbauteil auch nur auszudenken, geschweigedenn ft-Teile zu zerschnippeln oder sonstwie zu vergewaltigen. Alles soll mit Standard-ft-Teilen laufen. Ich will auch noch eine Variante mit Rastachsen versuchen, und gerade fällt mir ein, mann könnte doch auch die vom Rad entfernte Seite der Kardans ganz auf die gegenüberliegende Fahrzeugseite bringen. Damit könnten zwei Kardans je Rad verbaut werden, nur müsste der Antrieb auf beide Fahrzeugachsen auf die dann in Längsachse des Fahrzeugs versetzten Kardanenden gebracht werden. Die Radaufhängung könnte dann so McPershon-mäßig sein, so dass das Rad (fast) nicht schwenkt, sondern wirklich rauf/runter geht. Vielleicht habe ich zwischen den Jahren mal Zeit dazu.

Gruß,
Stefan