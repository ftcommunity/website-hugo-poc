---
layout: "image"
title: "Fahrwerks-Prototyp (5)"
date: "2004-12-02T17:50:39"
picture: "Fahrwerk_007.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3367
- /details0872-2.html
imported:
- "2019"
_4images_image_id: "3367"
_4images_cat_id: "642"
_4images_user_id: "104"
_4images_image_date: "2004-12-02T17:50:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3367 -->
Bis der Kran (irgendwann nächstes Jahr) mal fertig wird, sieht das Fahrwerk vielleicht schon wieder ganz anders aus...
