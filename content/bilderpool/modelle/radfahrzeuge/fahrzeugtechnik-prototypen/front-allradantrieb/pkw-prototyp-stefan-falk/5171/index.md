---
layout: "image"
title: "Vorderachse Gesamtansicht"
date: "2005-11-01T19:53:07"
picture: "Vorderachse_008.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5171
- /details11b6.html
imported:
- "2019"
_4images_image_id: "5171"
_4images_cat_id: "642"
_4images_user_id: "104"
_4images_image_date: "2005-11-01T19:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5171 -->
Hier noch ein Beitrag und Vorschlag zum Thema "angetriebene, gelenkte und gefederte Achse". Das hier ist eine erste Vorab-Umsetzung folgender Idee: Das Problem ist doch a) dass die Kardangelenke so große Lenkradien bewirken und b) nicht wirklich viel Kraft auf die Räder bringen können. Also dachte ich, warum kein Reibradantrieb, der mitlenkt. Hier also das Prinzipmodell: Jedes Rad ist an drei Querlenkern stabil gegen Verschiebung in Fahrtrichtung aufgehängt. Die Federn fehlen hier noch. Genauso federn die aus zwei Kardangelenken bestehenden Antriebswellen (siehe auch nächstes Bild). Genau auf dem extrem nah am Rad liegenden Drehpunkt liegt nun die Mitte eines dritten Kardans, welches nur Lenk-, aber keine Federbewegungen mitmachen muss und das Reibrad antreibt. Durch das Gewicht des späteren Fahrzeugs wird der Reifen auch noch gut ans Reibrad gedrückt. Außerdem haben wir so eine Untersetzung: Die Kardans brauchen weniger Kraft übertragen, weil ja letztlich nur die Umdrehungsgeschwindigkeit eines Vorstuferades auf die Straße gebracht wird.
