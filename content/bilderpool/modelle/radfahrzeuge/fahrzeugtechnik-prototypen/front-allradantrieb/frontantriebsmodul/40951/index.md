---
layout: "image"
title: "Frontantrieb von vorne oben"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40951
- /detailsaa3c.html
imported:
- "2019"
_4images_image_id: "40951"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40951 -->
http://www.ftcommunity.de/details.php?image_id=40627 habe ich weiterentwickelt zu einem Frontantriebsmodul. Die Lenkung erfolgt über einen Schneckenantrieb, allerdings fehlt vor dem Getriebe noch ein Minimotor "Micro" (ft-Nr. 31062). Den muss ich noch bestellen, aber den Einbauplatz kann man schon erahnen.