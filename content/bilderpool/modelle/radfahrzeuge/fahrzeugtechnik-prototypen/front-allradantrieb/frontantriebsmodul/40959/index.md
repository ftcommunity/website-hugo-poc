---
layout: "image"
title: "Frontantrieb aufgeklappt von der Seite"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40959
- /detailsda5a-2.html
imported:
- "2019"
_4images_image_id: "40959"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40959 -->
Und wieder zuklappen. Die Riegelscheibe hat zusammen mit der Klemmbuchse genügend Reibung, um die Lenkung anzutreiben. Sie dreht erst durch, wenn die Schneckenmutter am Anschlag ist.