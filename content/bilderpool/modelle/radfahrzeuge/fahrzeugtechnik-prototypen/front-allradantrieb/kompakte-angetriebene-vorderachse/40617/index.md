---
layout: "image"
title: "Linkseinschlag"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung6.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40617
- /detailsed86-3.html
imported:
- "2019"
_4images_image_id: "40617"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40617 -->
Aber auch bei dieser Lösung führt die Längsverschiebung dazu, dass der Drehpunkt unter dem Rad ist.