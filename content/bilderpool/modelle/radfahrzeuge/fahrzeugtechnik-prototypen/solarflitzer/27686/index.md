---
layout: "image"
title: "9"
date: "2010-07-05T16:39:58"
picture: "solarflitzer9.jpg"
weight: "9"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27686
- /details6640.html
imported:
- "2019"
_4images_image_id: "27686"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27686 -->
Man sieht, dass das Auto ziehmlich klein ist.