---
layout: "image"
title: "4"
date: "2010-07-05T16:39:58"
picture: "solarflitzer4.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27681
- /details648d-2.html
imported:
- "2019"
_4images_image_id: "27681"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27681 -->
Flach von der Seite. 
Hier sieht man gut das da kaum was dran ist. Solarzellen, Motor, Räder, Draht und Klebeband, mehr eigentlich nicht. Deshalb wiegt es auch nur knapp 125 Gramm.