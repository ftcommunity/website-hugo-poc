---
layout: "image"
title: "16"
date: "2010-07-06T17:38:34"
picture: "solarflitzer7.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27706
- /detailsbc61-3.html
imported:
- "2019"
_4images_image_id: "27706"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27706 -->
Damit es schneller geht, habe dann noch große Räder dran gebaut. Es bleibt aber dabei, ohne Schneckengetrieb geht´s schneller, auch die großen Räder haben kaum geholfen.