---
layout: "image"
title: "17"
date: "2010-07-06T17:38:34"
picture: "solarflitzer8.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27707
- /detailsccd8.html
imported:
- "2019"
_4images_image_id: "27707"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27707 -->
Die Großen Räder haben auch nicht viel gebracht, das erste Modell war schneller.