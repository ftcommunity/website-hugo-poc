---
layout: "image"
title: "10"
date: "2010-07-06T17:38:33"
picture: "solarflitzer1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27700
- /detailsf7bc.html
imported:
- "2019"
_4images_image_id: "27700"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27700 -->
Solarflitzer 2, Gesamtansicht. 
Die unterschiede: Ich habe zum Antrieb das Schneckengetriebe verwendet, was ich aber nicht wieder machen werde, weil es so viel Kraft schluckt. 
Es hat kleinere Räder, weil ich dachte sie würden sich schneller drehen, hab mich aber getäuscht. Weniger Draht und Klebeband wurde verbaut was einen Gewichtsvorteil von immerhin 9 Gramm brachte. Das Auto ist flacher, obwohl der Luftwiederstand wohl nur wenig bremste. Die Solarzellen sind jetzt auch fast gerade, was den Vorteil hat, dass das Auto immer gleich viel Licht einfängt, egal in welche Richtung es fährt.