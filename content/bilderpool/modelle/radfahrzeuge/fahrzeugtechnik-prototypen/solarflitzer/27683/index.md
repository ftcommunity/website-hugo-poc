---
layout: "image"
title: "6"
date: "2010-07-05T16:39:58"
picture: "solarflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27683
- /details71e9.html
imported:
- "2019"
_4images_image_id: "27683"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27683 -->
Die übertragung vom Motor zur Achse.