---
layout: "image"
title: "3"
date: "2010-07-05T16:39:58"
picture: "solarflitzer3.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27680
- /detailsb441.html
imported:
- "2019"
_4images_image_id: "27680"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27680 -->
Ein Foto flach von hinten. Hier sieht man, dass ich das Gestell aus Gewichtsgründen nur aus Draht gemacht habe. Fischertechnik sind eigentlich nur die drei Räder und die Narbe auf der Hinterachse, sowie die Achsen und Achsenklemmen.