---
layout: "image"
title: "2"
date: "2010-07-05T16:39:58"
picture: "solarflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27679
- /details6e10-2.html
imported:
- "2019"
_4images_image_id: "27679"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27679 -->
Einmal von oben.