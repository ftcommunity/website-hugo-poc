---
layout: "image"
title: "Ansicht unten hinten"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48334
- /details13b6.html
imported:
- "2019"
_4images_image_id: "48334"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48334 -->
Der Gelenkstein ermöglicht das Schwenken der Hinterachse.