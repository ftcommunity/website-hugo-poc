---
layout: "image"
title: "Vorderachse 1"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii24.jpg"
weight: "24"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48345
- /detailsd3f4.html
imported:
- "2019"
_4images_image_id: "48345"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48345 -->
Kommen wir zur Vorderachse.
Wie bereits erwähnt führt die graue S-Strebe die Spurstange. Wenn man genau hinschaut sieht man, dass die Spurstange, passend zum Servo, unsymmetrisch aufgebaut ist. Die beiden Pleuelstangen links und rechts sind unterschiedlich lang.
Die Verbindungsstopfen gehen erst durch die schwarze S-Strebe, dann durch die Pleuelstange (zum Durchstecken braucht man etwas Kraft), dann durch die S-Lasche. Und sie sitzen dann, nicht grade regelgerecht, in den Nuten der BS7,5.