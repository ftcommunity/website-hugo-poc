---
layout: "image"
title: "Vorderradaufhängung 1"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii31.jpg"
weight: "31"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48352
- /details1028-2.html
imported:
- "2019"
_4images_image_id: "48352"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48352 -->
Ich gebe zu, das sieht etwas komisch aus, Die Konstruktion im Vordergrund ist die Radaufhängung und dreht sich nicht mit, ebenso wie die Rastachse in der Mitte.