---
layout: "image"
title: "Vorderachse 7"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii30.jpg"
weight: "30"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48351
- /details5009.html
imported:
- "2019"
_4images_image_id: "48351"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48351 -->
Genau wie hinten auch hier natürlich ein Getriebe im Rad, bestehend aus einem Z10 und alten Ketten-Fördergliedern als Innenzahnrad.