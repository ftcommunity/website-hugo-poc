---
layout: "image"
title: "Hinterrad"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii20.jpg"
weight: "20"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48341
- /details66f2.html
imported:
- "2019"
_4images_image_id: "48341"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48341 -->
Das Rad an sich besteht aus meiner Speichenfelge https://www.ftcommunity.de/categories.php?cat_id=2998, dem Reifen 65 und einem Inenzahnrad aus alten Ketten-Fördergliedern.