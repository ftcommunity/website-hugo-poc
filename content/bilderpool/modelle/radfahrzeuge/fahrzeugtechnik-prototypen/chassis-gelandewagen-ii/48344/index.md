---
layout: "image"
title: "Aufhängung Servo"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii23.jpg"
weight: "23"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48344
- /details3cfd.html
imported:
- "2019"
_4images_image_id: "48344"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48344 -->
Ich habe eine Version des Servos, in die man die Metall-Zwichenstecker relativ fest reinstecken kann. Ich glaube, das ist nicht bei allen ft-Servos so.