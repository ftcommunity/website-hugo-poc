---
layout: "image"
title: "Ansicht unten"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48333
- /details342e-2.html
imported:
- "2019"
_4images_image_id: "48333"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48333 -->
Drei Differentiale, wie es sich für einen Geländewagen gehört.
Die S-Strebe unter dem vorderen Differential führt die Spurstange der Lenkung. Durch die Geometrie ergibt es sich sogar, dass das innere Rad stärker eingelenkt wird als das äußere.
Man ahnt aber auch, dass die Kette die Antriebswelle am Mitteldifferential bei hoher Last durchbiegt und an den Motor heranzieht, wodurch die Kette springen kann. Das bessere ich noch nach, Foto folgt.