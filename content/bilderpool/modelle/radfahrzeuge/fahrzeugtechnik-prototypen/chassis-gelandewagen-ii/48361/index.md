---
layout: "image"
title: "Nachtrag ft-pur"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii40.jpg"
weight: "40"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48361
- /detailsdab5.html
imported:
- "2019"
_4images_image_id: "48361"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48361 -->
Und hier noch die versprochenen Dioden aus dem Elektronik-Praktikum - jetzt ist es wirklich ft-pur.