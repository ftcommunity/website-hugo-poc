---
layout: "image"
title: "Bettengebirge 2"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48325
- /details536e.html
imported:
- "2019"
_4images_image_id: "48325"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48325 -->
Durch das Bettengebirge wühlt er sich ganz anständig.