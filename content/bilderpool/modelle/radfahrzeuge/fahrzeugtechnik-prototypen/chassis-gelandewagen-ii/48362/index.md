---
layout: "image"
title: "Versuch mit S-Motor - Gesamtansicht"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii41.jpg"
weight: "41"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48362
- /details712f-3.html
imported:
- "2019"
_4images_image_id: "48362"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48362 -->
Das war mein erster Versuch - mit S-Motor. Damit ist das Chassis noch etwas kürzer, aber der Motor war zu schwach.