---
layout: "image"
title: "Vorderachse 6"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii29.jpg"
weight: "29"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48350
- /details5c93.html
imported:
- "2019"
_4images_image_id: "48350"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48350 -->
Das ist echt schwer zu fotografieren, für mich zumindest.