---
layout: "image"
title: "Vorderradaufhängung 5"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii35.jpg"
weight: "35"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48356
- /details9329.html
imported:
- "2019"
_4images_image_id: "48356"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48356 -->
Die schwarze Rolle auf der Rastachse mit Platte stellt den richtigen Abstand der Z10 zur Fahrzeug-Mitte her, und damit die richtige Spurweite der Vorderräder. Die Rolle sitzt eigentlich bündig an der Platte, ich habe sie hier nur etwas abgezogen, damit man sie besser sieht.