---
layout: "image"
title: "Rampe 1"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48328
- /details8a80.html
imported:
- "2019"
_4images_image_id: "48328"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48328 -->
Und dann noch die 45°-Rampe aus http://forum.ftcommunity.de/viewtopic.php?f=19&t=441.
