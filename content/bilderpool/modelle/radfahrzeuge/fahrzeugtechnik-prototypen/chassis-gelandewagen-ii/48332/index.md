---
layout: "image"
title: "Ansicht oben"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48332
- /details74d1.html
imported:
- "2019"
_4images_image_id: "48332"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48332 -->
Ich habe mich bemüht, die funktionalen Teile möglichst auch als tragende Teile zu verwenden, damit das Modell leicht wird.