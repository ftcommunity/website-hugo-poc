---
layout: "image"
title: "Bodenfreiheit vorne"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii16.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48337
- /details62f4.html
imported:
- "2019"
_4images_image_id: "48337"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48337 -->
Ein Bild zur Bodenfreiheit von vorne. Die Perspektive täuscht etwas, der tiefste Punkt ist die Kette am Mitteldifferential.