---
layout: "image"
title: "Hinterachse 2"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii21.jpg"
weight: "21"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48342
- /details8a4e.html
imported:
- "2019"
_4images_image_id: "48342"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48342 -->
Die Hinterradaufhängung ist ziemich einfach. Die Rastkupplungen verhindern, dass sich das Rad unter Last verbiegt und nach oben ausweicht, und die Kettenglieder dann über das Z10 rutschen.