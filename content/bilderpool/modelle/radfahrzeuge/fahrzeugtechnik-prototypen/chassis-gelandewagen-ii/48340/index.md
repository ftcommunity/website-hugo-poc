---
layout: "image"
title: "Hinterachse 1"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii19.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48340
- /detailsc658.html
imported:
- "2019"
_4images_image_id: "48340"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48340 -->
Das ist der unsichtbare Teil in den Hinterrädern.
Wichtig ist die schwarze Scheibe und das klitzekleine Stück Pneumatikschlauch. Die Scheibe verhindert das Abrutschen des Rades von der Achse, und der Schlauch verhindert, zwischen die Klauen der Rastachsen gesteckt, das Abrutschen der Scheibe.