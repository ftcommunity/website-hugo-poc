---
layout: "image"
title: "Gesamtansicht 2"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48323
- /detailsab6c.html
imported:
- "2019"
_4images_image_id: "48323"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48323 -->
Man erkennt schon auf den ersten Blick die etwas merkwürdige Vorderradaufhängung, dazu später mehr.