---
layout: "image"
title: "Vorderradaufhängung 2"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii32.jpg"
weight: "32"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/48353
- /details4627.html
imported:
- "2019"
_4images_image_id: "48353"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48353 -->
Hier ist die Vorderradaufhängung abgenommen. Und auch hier wieder der Trick mit dem Stückchen Pneumatikschlauch, das verhindert, dass die S-Laschen von der Rastachse (schwer zu erkennen) abrutscht.