---
layout: "image"
title: "Das Papa-Mobil"
date: "2018-04-19T20:19:22"
picture: "gummibandauto3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47467
- /details06ef.html
imported:
- "2019"
_4images_image_id: "47467"
_4images_cat_id: "3506"
_4images_user_id: "1557"
_4images_image_date: "2018-04-19T20:19:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47467 -->
"Außerdem denken dann alle du hättest das gebaut." - pffff. Okay, wenn der Papa sowas baut dann geht da aber noch ein bisschen was - mitsamt Bilderstrecke  als Bonbon.

Optimiert und gnadenlos auf leichtgängige Achslager ausgelegt, reicht der "normierte Tankinhalt" dieses kleinen Renners nun für bis zu 9 Meter, bei etwa 1 Meter Beschleunigungsweg. An der Vorderachse sitzen Freilaufnaben (schwarz). Die Gummireifen auf den Drehscheiben stammen ursprünglich aus dem Baumarkt: Irgendwelche Dichtungsringe passenden Durchmessers.

Ob es in 3eich dereinst ebenbürtige Konkurrenz geben wird?