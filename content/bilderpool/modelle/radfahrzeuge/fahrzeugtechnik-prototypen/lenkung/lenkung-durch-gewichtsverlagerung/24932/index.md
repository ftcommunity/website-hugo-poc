---
layout: "image"
title: "Rechts"
date: "2009-09-19T21:24:10"
picture: "lenkungdurchgewichtsverlagerung3.jpg"
weight: "3"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/24932
- /detailse958-2.html
imported:
- "2019"
_4images_image_id: "24932"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24932 -->
... und umgekehrt