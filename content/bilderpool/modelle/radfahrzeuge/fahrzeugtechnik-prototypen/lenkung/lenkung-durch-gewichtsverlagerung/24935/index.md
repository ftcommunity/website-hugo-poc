---
layout: "image"
title: "Federung (Rückansicht)"
date: "2009-09-19T21:24:11"
picture: "lenkungdurchgewichtsverlagerung6.jpg"
weight: "6"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/24935
- /detailsbbf5-2.html
imported:
- "2019"
_4images_image_id: "24935"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24935 -->
Die Aufhängung von der Innenseite.