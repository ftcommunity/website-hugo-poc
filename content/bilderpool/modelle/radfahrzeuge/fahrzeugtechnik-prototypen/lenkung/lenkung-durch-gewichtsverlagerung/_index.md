---
layout: "overview"
title: "Lenkung durch Gewichtsverlagerung"
date: 2020-02-22T07:51:47+01:00
legacy_id:
- /php/categories/1719
- /categoriesec36-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1719 --> 
Die Lenkung dieses Fahrzeuges funktioniert durch bloßes Verlagern des Gewichtes nach links oder rechts. Um trotz der Fahrzeugneigung den Bodenkontakt der Räder sicherzustellen, sind alle Teile beweglich miteinander verbunden.