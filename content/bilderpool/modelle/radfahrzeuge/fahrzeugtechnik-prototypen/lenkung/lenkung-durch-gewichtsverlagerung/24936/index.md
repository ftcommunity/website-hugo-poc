---
layout: "image"
title: "Detail Vorderrad"
date: "2009-09-19T21:24:11"
picture: "lenkungdurchgewichtsverlagerung7.jpg"
weight: "7"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/24936
- /details5932.html
imported:
- "2019"
_4images_image_id: "24936"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24936 -->
