---
layout: "image"
title: "Gesamtansicht von vorne"
date: "2007-02-16T18:29:43"
picture: "lenkung05.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9040
- /details866b.html
imported:
- "2019"
_4images_image_id: "9040"
_4images_cat_id: "824"
_4images_user_id: "453"
_4images_image_date: "2007-02-16T18:29:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9040 -->
Hier sieht man die Kegelzahnräder und die Kadernwelle für den Antrieb der Räder.
