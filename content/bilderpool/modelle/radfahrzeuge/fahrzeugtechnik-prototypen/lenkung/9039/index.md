---
layout: "image"
title: "Antrieb"
date: "2007-02-16T18:29:42"
picture: "lenkung04.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9039
- /detailsf145.html
imported:
- "2019"
_4images_image_id: "9039"
_4images_cat_id: "824"
_4images_user_id: "453"
_4images_image_date: "2007-02-16T18:29:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9039 -->
Hier kommt noch ein Motor dran für den Antrieb.
