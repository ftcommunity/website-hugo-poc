---
layout: "image"
title: "Führung"
date: "2007-02-16T18:29:42"
picture: "lenkung03.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9038
- /detailsf8ec.html
imported:
- "2019"
_4images_image_id: "9038"
_4images_cat_id: "824"
_4images_user_id: "453"
_4images_image_date: "2007-02-16T18:29:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9038 -->
Die beiden Metallstandstangen führen den Gewindestein auf der Gewindestange.
