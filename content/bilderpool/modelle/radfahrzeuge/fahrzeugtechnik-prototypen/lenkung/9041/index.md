---
layout: "image"
title: "Antrieb"
date: "2007-02-16T18:29:43"
picture: "lenkung06.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9041
- /details7491-2.html
imported:
- "2019"
_4images_image_id: "9041"
_4images_cat_id: "824"
_4images_user_id: "453"
_4images_image_date: "2007-02-16T18:29:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9041 -->
Hier sieht man die beiden Zahnräder die, die Räder antreiben.
