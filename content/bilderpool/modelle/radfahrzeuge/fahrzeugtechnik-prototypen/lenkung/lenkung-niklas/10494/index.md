---
layout: "image"
title: "Lenkungssystem"
date: "2007-05-23T18:51:51"
picture: "lenkung3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10494
- /details2001.html
imported:
- "2019"
_4images_image_id: "10494"
_4images_cat_id: "957"
_4images_user_id: "557"
_4images_image_date: "2007-05-23T18:51:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10494 -->
hier sieht man die übertragung von lenkrad auf lenkwelle