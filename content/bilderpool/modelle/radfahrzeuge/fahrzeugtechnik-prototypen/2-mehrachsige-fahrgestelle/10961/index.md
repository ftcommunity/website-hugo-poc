---
layout: "image"
title: "Vorderradaufbau 2"
date: "2007-06-28T16:08:20"
picture: "DSCN1310.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10961
- /detailsa1d5-2.html
imported:
- "2019"
_4images_image_id: "10961"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-06-28T16:08:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10961 -->
