---
layout: "image"
title: "Minimal Laufkatze"
date: "2008-02-03T09:30:07"
picture: "MeinMiniModell.jpg"
weight: "22"
konstrukteure: 
- "Peter Aeberhard"
fotografen:
- "Peter Aeberhard"
schlagworte: ["Minimalmodell", "Mini-Modell", "wenige", "Teile"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13510
- /details3321.html
imported:
- "2019"
_4images_image_id: "13510"
_4images_cat_id: "989"
_4images_user_id: "660"
_4images_image_date: "2008-02-03T09:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13510 -->
Mit möglichst wenigen Teilen eine Laufkatze zu bauen, war meine Motivation. Als Stomabnehmer dienen ausgebaute Abnehmer von Carrera-Autos, Lotsauglitze ginge wahrscheinlich auch.