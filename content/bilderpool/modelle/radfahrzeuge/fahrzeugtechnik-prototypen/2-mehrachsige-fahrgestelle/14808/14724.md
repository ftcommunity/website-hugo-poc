---
layout: "comment"
hidden: true
title: "14724"
date: "2011-07-28T22:14:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nein, das ist gut so und genau der Gag dieser Bauart! Die Achsen aller Räder sollen sich in gedachter Verlängerung idealerweise alle in ein und demselben Punkt treffen, und das ginge nicht, wenn die beiden gelenkten Achsen gleich start eingschlagen werden - deren verlängerte Achsen verliefen dann ja parallel zueinander und würden sich nie treffen.

Gruß,
Stefan