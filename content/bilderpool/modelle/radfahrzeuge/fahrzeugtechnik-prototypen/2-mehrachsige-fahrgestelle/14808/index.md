---
layout: "image"
title: "Doppelachslenkung unten"
date: "2008-07-07T09:33:46"
picture: "Doppelachslenkung_unten.jpg"
weight: "23"
konstrukteure: 
- "sire_mid"
fotografen:
- "sire_mid"
uploadBy: "SireMiD"
license: "unknown"
legacy_id:
- /php/details/14808
- /details4dfb-2.html
imported:
- "2019"
_4images_image_id: "14808"
_4images_cat_id: "989"
_4images_user_id: "441"
_4images_image_date: "2008-07-07T09:33:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14808 -->
Inspiriert durch die Flughafenfeuerwehr aus dem Baukasten Fire Trucks wollte ich ein vierachsiges Modell bauen. Dazu benötigt man natürlich eine Doppelachslenkung. Ziel der Konstruktion war es, dass das neue Servo aus dem Fernlenkset verbaut werden kann und die Bodenfreiheit nicht allzu sehr durch die Mechanik eingeschränkt wird.
Das ist das erste Mal, dass ich Bilder hochlade, für Anregungen und Kritik bin ich sehr dankbar.