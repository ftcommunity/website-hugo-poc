---
layout: "image"
title: "Lenkmechanik Detail"
date: "2007-06-30T15:51:01"
picture: "DSCN1385.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10975
- /details2aa3-2.html
imported:
- "2019"
_4images_image_id: "10975"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10975 -->
Zuerst dachte ich das der BS 7,5 auf der Zahnstange nicht genug Halt findet. Das hat sich aber später als unbegründet herausgestellt. Die alten Zahnstangen halten besser als die neuen.
