---
layout: "image"
title: "Ansicht"
date: "2007-06-30T15:51:01"
picture: "DSCN1382.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10972
- /detailsf7cb.html
imported:
- "2019"
_4images_image_id: "10972"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10972 -->
Hier die erweiterte Version des 4 - achs Fahrgestelles. Die Endschalter für die Lenkung wurden vom 2 - achs Fahrgestell übernommen. Genau so der Achsschenkelaufbau.
