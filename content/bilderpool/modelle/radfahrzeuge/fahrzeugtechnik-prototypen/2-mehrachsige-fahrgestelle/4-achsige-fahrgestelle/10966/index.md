---
layout: "image"
title: "Doppelachslenkung Ansicht"
date: "2007-06-30T15:51:01"
picture: "DSCN1394.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10966
- /detailsf9be-3.html
imported:
- "2019"
_4images_image_id: "10966"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10966 -->
Bei einer Doppelachslenkung tritt oft das Problem auf das die Reifen "radieren". Dieses kann durch unterschiedliche Lenkradien der Vorderräder vermieden werden. Das habe ich versucht hier umzusetzen.
