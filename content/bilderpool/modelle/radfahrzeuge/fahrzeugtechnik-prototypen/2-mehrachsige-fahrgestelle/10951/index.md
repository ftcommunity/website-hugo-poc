---
layout: "image"
title: "Radträger"
date: "2007-06-28T14:15:48"
picture: "DSCN1317.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/10951
- /details640c.html
imported:
- "2019"
_4images_image_id: "10951"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-06-28T14:15:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10951 -->
Die Bauplatten 15x15 werden nach der Radmontage nach außen geschoben und halten so das Rad fest.
