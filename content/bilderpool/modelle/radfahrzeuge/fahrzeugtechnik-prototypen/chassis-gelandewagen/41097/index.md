---
layout: "image"
title: "30 Grad Steigung 04"
date: "2015-05-30T15:35:02"
picture: "30_Grad_Steigung_04_klein.jpg"
weight: "43"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41097
- /detailse475.html
imported:
- "2019"
_4images_image_id: "41097"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-30T15:35:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41097 -->
Geschafft. Jetzt kommt die Karosserie dran.