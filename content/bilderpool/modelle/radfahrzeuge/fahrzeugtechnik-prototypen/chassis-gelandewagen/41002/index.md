---
layout: "image"
title: "Bodenfreiheit 01"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41002
- /detailsa7d4-2.html
imported:
- "2019"
_4images_image_id: "41002"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41002 -->
Hier eine Seitenansicht. Die Bodenfreiheit ist ok aber nicht überragend, da könnte man wahrscheinlich noch mehr rausholen.