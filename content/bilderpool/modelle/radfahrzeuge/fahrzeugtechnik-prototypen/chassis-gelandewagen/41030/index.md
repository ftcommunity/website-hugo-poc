---
layout: "image"
title: "Getriebe 13"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen33.jpg"
weight: "33"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41030
- /details8998-3.html
imported:
- "2019"
_4images_image_id: "41030"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41030 -->
Hier ist der Abdeckstein ausgebaut.