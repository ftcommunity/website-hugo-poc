---
layout: "image"
title: "Getriebe 10"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen30.jpg"
weight: "30"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41027
- /details253e.html
imported:
- "2019"
_4images_image_id: "41027"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41027 -->
Die Statikstrebe mit der L-Lasche halten das Lager für das Mitteldifferentiel im Raster, es könnte sich sonst nach hinten wegschieben.