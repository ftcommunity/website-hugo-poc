---
layout: "image"
title: "Lenkeinschlag 03"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41007
- /details3c4f.html
imported:
- "2019"
_4images_image_id: "41007"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41007 -->
Beim Lenkeinschlag links stört leider das Zahnrad etwas. Wenn man wirklich bis zum Anschlag dreht, berührt der Reifen das Ritzel und wird gebremst.