---
layout: "image"
title: "ABS 01"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen15.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41012
- /detailsfabe.html
imported:
- "2019"
_4images_image_id: "41012"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41012 -->
Gimmik: ein ASR für die Vorderachse. Wenn man den Magnet einschaltet, zieht er das Ankerblech auf die Welle vom Mitteldifferential zur Vorderachse und bremst diese. Bei Tests hatte ich häufig das Problem, dass bei starken Steigungen die Vorderräder durchdrehen. Das liegt u.a. daran, dass der Batteriekasten weit hinten sitzt, und sich bei Bergfahrten der Schwerpunkt zusätzlich nach hinten schiebt. Durch den Magneten kann man dann (manuell eingeschaltet) das Antriebsmoment ebenfalls nach hinten "schieben". Das graue Scharneir mit der Bauplatte 15x30 verhindert, dass die Ankerplatte bei ausgeschaltetem Magnet nach unten klappt.