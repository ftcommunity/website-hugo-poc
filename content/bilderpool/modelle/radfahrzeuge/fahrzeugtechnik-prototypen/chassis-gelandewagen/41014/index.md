---
layout: "image"
title: "Radaufhängung"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen17.jpg"
weight: "17"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41014
- /detailsdbf0-3.html
imported:
- "2019"
_4images_image_id: "41014"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41014 -->
In den Felgen befinden sich Seilrollen als Abstandshalter.