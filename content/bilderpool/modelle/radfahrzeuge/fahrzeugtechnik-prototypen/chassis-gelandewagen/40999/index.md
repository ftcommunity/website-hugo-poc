---
layout: "image"
title: "Chassis für Geländewagen 02"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40999
- /details375a.html
imported:
- "2019"
_4images_image_id: "40999"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40999 -->
Es sei gleich vorweggenommen, dass die Kardangelenke selbstgebaut sind, 
siehe http://www.ftcommunity.de/categories.php?cat_id=3046.

Scheinbar überflüssige Teile gehören zur Befestigung der Karosserie, die schon in Arbeit ist.