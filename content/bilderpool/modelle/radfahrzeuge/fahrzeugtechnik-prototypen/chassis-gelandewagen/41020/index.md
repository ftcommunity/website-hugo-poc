---
layout: "image"
title: "Getriebe 03"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen23.jpg"
weight: "23"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41020
- /detailsc125-2.html
imported:
- "2019"
_4images_image_id: "41020"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41020 -->
Weil das Getriebe so frickelig ist, habe ich es mal Schritt für Schritt aufgedröselt.