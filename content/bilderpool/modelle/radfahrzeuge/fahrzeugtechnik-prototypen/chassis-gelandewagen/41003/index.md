---
layout: "image"
title: "Bodenfreiheit 02"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41003
- /details79db-2.html
imported:
- "2019"
_4images_image_id: "41003"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41003 -->
Ansicht von vorne. Für mehr Bodenfreiheit wären noch Portalachsen super. Vielleicht beim nächsten Modell.