---
layout: "image"
title: "Getriebe 04"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen24.jpg"
weight: "24"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41021
- /details8afb.html
imported:
- "2019"
_4images_image_id: "41021"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41021 -->
Die Winkelsteine und die Platte sind schon für die Karosserie. Der Reedkontakthalter ist nur für die Kabelführung bestimmt.