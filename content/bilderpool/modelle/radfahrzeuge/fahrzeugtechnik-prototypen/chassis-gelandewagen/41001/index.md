---
layout: "image"
title: "Chassis für Geländewagen 04"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41001
- /details16d0.html
imported:
- "2019"
_4images_image_id: "41001"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41001 -->
Die Fernbedienung erfolgt per Kabel, weil derzeit alle meine IR-Module anderweitig verbaut sind. Ich glaube, ich muss mal wieder ein wenig investieren.