---
layout: "image"
title: "Lenkeinschlag 02"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/41006
- /details7df1.html
imported:
- "2019"
_4images_image_id: "41006"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41006 -->
Lenkeinschlag links. Am Ende schlägt die Schneckenmutter an und die Riegelscheibe auf der Minimot-Getriebewelle dreht durch.