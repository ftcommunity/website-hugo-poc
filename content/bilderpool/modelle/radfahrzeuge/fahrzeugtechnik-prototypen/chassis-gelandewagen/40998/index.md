---
layout: "image"
title: "Chassis für Geländewagen 01"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40998
- /detailsdbdf.html
imported:
- "2019"
_4images_image_id: "40998"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40998 -->
Mit dem Fronantriebsmodul http://www.ftcommunity.de/categories.php?cat_id=3076 
habe ich ein Chassis für einen Geländewagen mit Allradantrieb entworfen.