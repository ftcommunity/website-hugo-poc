---
layout: "image"
title: "Raketenauto inspiriert von Torben Kuhlmanns 'Armstrong'"
date: "2018-06-02T21:00:26"
picture: "MitMaus.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/47685
- /detailsc6e3.html
imported:
- "2019"
_4images_image_id: "47685"
_4images_cat_id: "3516"
_4images_user_id: "1088"
_4images_image_date: "2018-06-02T21:00:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47685 -->
Im Anschluss an eine Mond- und Jupiterbeobachtung im Kindergarten in diesem Winter habe ich das genial gezeichnete Buch "Armstrong" von Torben Kuhlmann vorgelesen. Natürlich musste dann auch eine Art Raketenauto durch den Gruppenraum flitzen. Das abgebildete Gefährt legte mit einem normalen Ballon 10 m zurück und wurde nur durch die Wand gestoppt. Die Maus musste unbedingt mit. Da der Ballon aufgepustet den ganzen Platz auf der Platte einnimmt, sitzt sie sehr weit vor den Rädern. Der Fahrleistung tut das keinen Abbruch.
