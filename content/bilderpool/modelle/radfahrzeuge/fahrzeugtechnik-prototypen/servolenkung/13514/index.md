---
layout: "image"
title: "Ergebnis des Lenkeinschlags"
date: "2008-02-03T20:45:25"
picture: "lenkeinschlag_ergebnis.jpg"
weight: "4"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- /php/details/13514
- /details6d98.html
imported:
- "2019"
_4images_image_id: "13514"
_4images_cat_id: "1238"
_4images_user_id: "708"
_4images_image_date: "2008-02-03T20:45:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13514 -->
Der Motor hat so lange gedreht, bis der Taster wieder freigegeben wurde. Dadurch ist der Motor quasi dem Einschlag des Lenkrads gefolgt. Der Powermotor ist dafür natürlich hoffnungslos überdimensioniert. Da er aber ohne Schneckengetriebe auskommt, hemmt er nicht und man kann die Lenkung noch mit Muskelkraft bedienen, wenn die Servolenkung ausfällt.