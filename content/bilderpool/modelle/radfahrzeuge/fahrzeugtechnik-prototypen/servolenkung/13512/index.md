---
layout: "image"
title: "Draufsicht"
date: "2008-02-03T20:45:25"
picture: "von_oben.jpg"
weight: "2"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- /php/details/13512
- /details0200.html
imported:
- "2019"
_4images_image_id: "13512"
_4images_cat_id: "1238"
_4images_user_id: "708"
_4images_image_date: "2008-02-03T20:45:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13512 -->
Hier das ganze von oben, nur um zu zeigen, dass die obere Achse eigentlich aus zwei Achsen besteht.