---
layout: "image"
title: "endu48-101.JPG"
date: "2007-10-03T21:19:52"
picture: "endu48-101.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12131
- /details60c9-3.html
imported:
- "2019"
_4images_image_id: "12131"
_4images_cat_id: "1086"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T21:19:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12131 -->
Der richtige Abstand zwischen Rastkegelzahnrad und der Radachse wird dadurch eingehalten, dass in der durchgehenden Nut der drei BS7,5 passende Bauteile eingesetzt sind. Hier sind das eine Rastachse 15 (so nenne ich das Teil, mangels richtiger Bezeichnung) und eine Klemmhülse 7,5 (35980).
