---
layout: "image"
title: "endu48-100.JPG"
date: "2007-10-03T21:09:05"
picture: "endu48-100.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12130
- /detailsd901.html
imported:
- "2019"
_4images_image_id: "12130"
_4images_cat_id: "1086"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T21:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12130 -->
Diese Achse ist mit dem Reifen 45 direkt neben dem Traktorreifen aufgebaut und kommt ganz ohne Teile-Modding aus. Die Rastkegelzahnräder greifen in die Riffelung der Reifen 45 ein und liefern eine Untersetzung von 10:48 (also 1:4,8), wenn man nur die Zähnezahlen betrachtet. Wegen der unterschiedlichen Durchmesser von Reifen 45 und den Conrad-Reifen ist das aber nicht die tatsächlich wirksame Untersetzung.

Man ist versucht, daraus eine Federschwinge zu machen, mit Gelenksteinen anstelle der Lagerböcke. Daraus wird aber nichts, denn die Achse braucht Gewicht von oben, damit die Rastkegelzahnräder an die Riffelung der Reifen 45 angedrückt werden.
