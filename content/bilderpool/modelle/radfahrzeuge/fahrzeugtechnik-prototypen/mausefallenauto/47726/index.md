---
layout: "image"
title: "Mausefallenauto 2.0"
date: "2018-07-20T18:39:16"
picture: "P1050044_klein.jpg"
weight: "6"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/47726
- /detailsc68f.html
imported:
- "2019"
_4images_image_id: "47726"
_4images_cat_id: "297"
_4images_user_id: "2781"
_4images_image_date: "2018-07-20T18:39:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47726 -->
Das Mausefallenauto ist jetzt deutlich besser:
als Hinterräder dienen CDs, die mit "Hülse + Scheibe 15&#8220; zentriert werden. Die Hülse wird mit einer Nabe aus dem 3D-Drucker (hellblau) festgehalten. Auf der anderen Seite drücken Flachnabe und -zange dagegen. Dadurch drehen die Räder auf der Antriebsachse nicht durch.
Die Klammern auf der Mausefalle sind durch Sekundenkleber gesichert, damit sich die Feder nicht losarbeitet. Der Metallstift, der normalerweise die Feder in gespanntem Zustand hält, wurde entfernt.