---
layout: "comment"
hidden: true
title: "24097"
date: "2018-06-02T19:34:19"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Hallo Esther,
wie sorgst du dafür, dass die ft-Schnur nicht auf der Achse durchrutscht?
Da habe ich bisher keine Lösung gefunden, ein Gummiring hat da keine Probleme.
Gruß
Rüdiger