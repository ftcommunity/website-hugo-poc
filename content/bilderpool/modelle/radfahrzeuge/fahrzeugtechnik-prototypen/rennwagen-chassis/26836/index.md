---
layout: "image"
title: "Rennwagen-Chassis 2"
date: "2010-03-29T19:03:25"
picture: "Rennwagen-Chassis_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26836
- /details7292.html
imported:
- "2019"
_4images_image_id: "26836"
_4images_cat_id: "1919"
_4images_user_id: "328"
_4images_image_date: "2010-03-29T19:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26836 -->
Spurweite und Radstand ergeben sehr harmonische Proportionen. Denkbar sind für mich kleine (Renn-) LKWs und natürlich PKWs aller Art. Für einen flachen Rennwagen à la Formel 1 ist der Vorbau mit dem Servo leider zu hoch.

Der Übergang vom schmalen auf das breite Chassis hinter der Vorderachse wurde von mir sehr stabil dargestellt.

Nach oben bieten sich viele Anbaumöglichkeiten für Aufbauten.