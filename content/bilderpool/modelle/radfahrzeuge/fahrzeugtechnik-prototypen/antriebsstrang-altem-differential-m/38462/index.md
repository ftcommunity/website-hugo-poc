---
layout: "image"
title: "Gesamte Ansicht von schäg oben"
date: "2014-03-16T17:58:36"
picture: "S1060001.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["M-Motor", "Differenzial", "Lenkung", "Potentiometer", "Antriebsstrang", "präzise", "Sicherung", "Verrutschen"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38462
- /details7930.html
imported:
- "2019"
_4images_image_id: "38462"
_4images_cat_id: "2868"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38462 -->
Hab herumgebastelt und endlich eine befriedigende Lösung gefunden, wie man mit den grauen Motoren und dem alten Differential einen leistungsfähigen Antrieb mit hoher Geschwindigkeit baut, bei dem nichts mehr verrutscht und wie man eine präzise Lenkung mit wenig Schlupf hinbekommt und deren Stellung mit einem Potentiometer abgegegriffen und gesteuert werden kann mit wenig Schlupf. Hier das Ergebnis.