---
layout: "image"
title: "ATeam16.JPG"
date: "2005-01-04T16:46:14"
picture: "ATeam16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3515
- /details872b.html
imported:
- "2019"
_4images_image_id: "3515"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3515 -->
Die Strebe I-105 Loch war einmal eine I-120 Loch.
Vom BS5 links musste eine Ecke abgeschnitten werden, damit der ganze Klotz beim Lenken unter die Längsträger passt.