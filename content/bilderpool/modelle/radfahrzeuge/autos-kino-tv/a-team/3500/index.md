---
layout: "image"
title: "ATeam01.JPG"
date: "2005-01-04T16:28:20"
picture: "ATeam01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3500
- /details58cf.html
imported:
- "2019"
_4images_image_id: "3500"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3500 -->
"Ich liebe es, wenn ein Modell funktioniert" 

(zumindest so ähnlich lautete der Spruch von Hannibal Smith, dem Anführer des legendären A-Team)
