---
layout: "image"
title: "ATeamV-08.JPG"
date: "2005-01-04T17:01:25"
picture: "ATeamV-08.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3518
- /details27ac.html
imported:
- "2019"
_4images_image_id: "3518"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T17:01:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3518 -->
Nochmal der Versuchsträger:
Die Dachneigung ist hier 2x 15°, dadurch wird die Dachbreite "krumm" und es passen keine Verkleidungsplatten mehr, weder über die Breite noch über die Länge. Für die Scharniere der Heckklappe bleibt wohl nur noch Gummi als Baumaterial, so schief sind die Ecken geworden. Die Verwindungssteifigkeit ist trotzdem prima: mehr als die hier sichtbaren Streben plus den Rahmen der Frontscheibe braucht man nicht für die ganze Zelle.