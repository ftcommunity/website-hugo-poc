---
layout: "image"
title: "ATeamV-04.JPG"
date: "2005-01-04T16:46:14"
picture: "ATeamV-04.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3517
- /details71d1-2.html
imported:
- "2019"
_4images_image_id: "3517"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3517 -->
Noch ein Bild vom Versuchsträger: die Hinterachse ist einfacher gehalten und die Schwinge vorn gelagert. Diese Hinterachse ist aber schon *zu* einfach, die Hinterräder stehen immer x-beinig herum.