---
layout: "comment"
hidden: true
title: "13454"
date: "2011-02-02T15:29:14"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Seit wann das denn? Der Van hatte doch ganz normale Türen.

Meinst du den DeLorean aus "Zurück in die Zukunft"? Der hatte Flügeltüren.