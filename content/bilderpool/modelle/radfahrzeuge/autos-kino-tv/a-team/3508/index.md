---
layout: "image"
title: "ATeam09.JPG"
date: "2005-01-04T16:28:30"
picture: "ATeam09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3508
- /detailsc1b4-2.html
imported:
- "2019"
_4images_image_id: "3508"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3508 -->
Bei geschlossener Heckklappe wird der Stromkreis für die rückwärtige Beleuchtung hergestellt, gleichzeitig dienen die Kontakte als 'Schnappverschluss' für die Heckklappe (als Feder dafür dient der ganze Fahrzeugrahmen). Der Lichtschalter befindet sich vorn in der Mittelkonsole.

Das Batteriegehäuse in der Stoßstange ist eins der beiden Geheimfächer, die ein Wagen für das A-Teams natürlich immer hat. Das zweite Fach befindet sich im Längsträger unter der linken Wagenseite.