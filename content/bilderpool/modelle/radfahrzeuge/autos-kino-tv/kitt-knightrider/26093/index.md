---
layout: "image"
title: "Kitt aus knightrider (vorne)"
date: "2010-01-15T16:23:30"
picture: "kittausknightrider2.jpg"
weight: "2"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- /php/details/26093
- /detailsfddc.html
imported:
- "2019"
_4images_image_id: "26093"
_4images_cat_id: "1845"
_4images_user_id: "1057"
_4images_image_date: "2010-01-15T16:23:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26093 -->
Mein Nachbaumodell von kitt hat folgende Merkmale:
Ultraschall,50:1 P-motor,2taster an der stoßstange,3 taster am lenkmotor für die Lenkung,lampen und ein Sprachmodul