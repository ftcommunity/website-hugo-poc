---
layout: "image"
title: "Vorderachse mit Servo (Vorderansicht)"
date: "2014-11-23T19:12:24"
picture: "kipperx04.jpg"
weight: "4"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39856
- /details2cb2-2.html
imported:
- "2019"
_4images_image_id: "39856"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39856 -->
Vorderachse mit Servo von ft (Vorderansicht)
Spurstange demontiert damit die befestigung des Servos besser zu sehen ist.
Ein Trinkhalm leitet die Verkabelung nach oben.
