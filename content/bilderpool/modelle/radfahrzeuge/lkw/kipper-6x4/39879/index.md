---
layout: "image"
title: "Fertig!"
date: "2014-11-23T19:12:24"
picture: "kipperx27.jpg"
weight: "27"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39879
- /details588f.html
imported:
- "2019"
_4images_image_id: "39879"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39879 -->
Eigentlich ist's niemals fertig. Es fehlt noch ein Auspuff, die Hinterachsen sollten in Stahl ausgeführt werden, das Lenkrad könnte mitdrehen. Umbau nach ein Feuerwehr Leiterwagen, Autokran, usw.
