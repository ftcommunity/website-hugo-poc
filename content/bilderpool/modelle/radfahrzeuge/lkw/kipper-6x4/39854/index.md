---
layout: "image"
title: "Alle Teile der Kipper bevor dem Zusammenbau"
date: "2014-11-23T19:12:24"
picture: "kipperx02.jpg"
weight: "2"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39854
- /detailsc9c7.html
imported:
- "2019"
_4images_image_id: "39854"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39854 -->
Bevor dem ersten Zusammenbau sind alle Teile auf dem Tisch.
Rechts oben ist der Montageständer.