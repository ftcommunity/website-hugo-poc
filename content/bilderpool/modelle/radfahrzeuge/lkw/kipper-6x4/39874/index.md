---
layout: "image"
title: "Empfänger und Motorregler im Platz"
date: "2014-11-23T19:12:24"
picture: "kipperx22.jpg"
weight: "22"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/39874
- /details030d.html
imported:
- "2019"
_4images_image_id: "39874"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39874 -->
Schließlich passte der Kabelsalat nicht unter die Motorhaube.
Jetzt ist das Fahrerhaus 5mm hoch gegangen und sind die Kabel unter dem Fahrerhaus. Jetzt sind Kabel locker verlegt und klemmt nichts.
