---
layout: "image"
title: "Lenkung (4)"
date: "2006-01-27T13:58:26"
picture: "DSCN0622.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5695
- /details098e-3.html
imported:
- "2019"
_4images_image_id: "5695"
_4images_cat_id: "489"
_4images_user_id: "184"
_4images_image_date: "2006-01-27T13:58:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5695 -->
Eine Gesamtansicht von unten
