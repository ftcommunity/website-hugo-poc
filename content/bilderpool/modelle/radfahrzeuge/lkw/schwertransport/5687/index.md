---
layout: "image"
title: "das Grundgerüst"
date: "2006-01-27T13:55:56"
picture: "132_3295.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5687
- /details18db.html
imported:
- "2019"
_4images_image_id: "5687"
_4images_cat_id: "489"
_4images_user_id: "184"
_4images_image_date: "2006-01-27T13:55:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5687 -->
