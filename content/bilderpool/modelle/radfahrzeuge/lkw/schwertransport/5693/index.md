---
layout: "image"
title: "Lenkung (2)"
date: "2006-01-27T13:58:26"
picture: "DSCN0627.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5693
- /detailsb54a.html
imported:
- "2019"
_4images_image_id: "5693"
_4images_cat_id: "489"
_4images_user_id: "184"
_4images_image_date: "2006-01-27T13:58:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5693 -->
