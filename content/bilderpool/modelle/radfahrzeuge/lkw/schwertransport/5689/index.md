---
layout: "image"
title: "Antrieb (1)"
date: "2006-01-27T13:55:56"
picture: "DSCN0619.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5689
- /detailsc278-2.html
imported:
- "2019"
_4images_image_id: "5689"
_4images_cat_id: "489"
_4images_user_id: "184"
_4images_image_date: "2006-01-27T13:55:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5689 -->
Jeder Powermotor treibt eine Seite an. Alle Hinterräder werden angetrieben. Dadurch das kein Antriebs-Zahnrad auf einer Achse geschraubt werden muss, ist der LKW SUPER stark....
