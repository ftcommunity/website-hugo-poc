---
layout: "image"
title: "Schneeräumfahrzeug"
date: "2016-10-24T21:20:51"
picture: "schneeraeumfahrzeug06.jpg"
weight: "6"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/44676
- /detailsa832.html
imported:
- "2019"
_4images_image_id: "44676"
_4images_cat_id: "3325"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44676 -->
Fahrregler von CTI, Stromversorgung über vier AA Akkus.
