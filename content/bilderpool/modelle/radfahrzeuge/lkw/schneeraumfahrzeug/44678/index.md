---
layout: "image"
title: "Schneeräumfahrzeug"
date: "2016-10-24T21:20:51"
picture: "schneeraeumfahrzeug08.jpg"
weight: "8"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/44678
- /detailsea7d-2.html
imported:
- "2019"
_4images_image_id: "44678"
_4images_cat_id: "3325"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44678 -->
