---
layout: "image"
title: "Detail"
date: "2012-02-08T14:34:02"
picture: "23_Detail_Vorderachse.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34121
- /details3bd5.html
imported:
- "2019"
_4images_image_id: "34121"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-08T14:34:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34121 -->
oben sieht man das Differential. Links daneben ist die Welle für die Steilwinde.
Darunter ist die Führung (zwei leere Hubgetriebe) für das Zahnstangengetriebe.
Auf dem Hubgetriebe sind die beiden Schalter die den Lenkeinschlag begrenzen.
Etwas tiefen liegen zwei weitere Schalter die die Blinker steuern.
