---
layout: "image"
title: "Radträger"
date: "2012-02-05T20:01:41"
picture: "16_Radtrger_3.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34097
- /detailsfb36-2.html
imported:
- "2019"
_4images_image_id: "34097"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34097 -->
eingebauter Radträger. Die großen Reifen werden zwischen den beiden Drehscheiben eingebaut.
