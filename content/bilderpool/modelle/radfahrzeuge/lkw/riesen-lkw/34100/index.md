---
layout: "image"
title: "mit Belastung"
date: "2012-02-06T17:05:46"
picture: "19_mit_Belastung.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
schlagworte: ["Monsterreifen"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34100
- /detailsd18e.html
imported:
- "2019"
_4images_image_id: "34100"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-06T17:05:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34100 -->
Jetzt habe ich die Frage vom Thomas erst richtig verstanden.....
Die Federn können sich nicht seitlich bewegen weil sie daran gehindert werden.
Wenn man das vorher gezeigte Bild mit diesem vergleicht sieht man auch warum.
Die schwarzen 30er Streben bilden zusammen mit den roten Lenkhebeln ein Parallelogramm (Das Rad muss ja immer senkrecht stehen). Das konnte man auf dem anderen Bild nicht so gut erkennen.
Dieses verhindert es das sich die Feden wegdrücken.
Entferne ich eine dieser Streben verschieben sich die Federn.
