---
layout: "image"
title: "Gesamtansicht"
date: "2012-02-05T20:01:27"
picture: "01_Gesamt.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34082
- /detailse0a2-2.html
imported:
- "2019"
_4images_image_id: "34082"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34082 -->
Ich möchte heute mein neustes Modell vorstellen.
Inspiriert wurde ich von dem hier

http://www.ftcommunity.de/details.php?image_id=14803

Es ist 77 cm lang, 45 cm breit und an der höchsten Stelle 30 cm hoch
und so schwer das ich beim Transport Probleme habe ....
 
Das Modell hat einen permanenten Heckantrieb an zwei Achsen (über Differential),
einen Frontantrieb (ebenfalls mit Differential) der über eine Klauenkupplung zuschaltbar ist,
eine Seilwinde unter der Motorhaube,
richtig funktionierende Beleuchtung aus Fahr- (weiß) und Rückleuchten (rot), Blinker (gelb),
Rückfahrscheinwerfer (weiß) (Dank an Rolf für den Tip mit den Dioden),
das Lenkrad dreht sich beim Lenken mit,
am Heck beträgt der Federweg 30 mm, an der Vorderachse etwas weniger,

Beim Bau habe ich erst einmal gemerkt was die Konstrukteure damals geleistet haben.....
Es wird hier von einem Differential gesprochen. Nur welches war es. 31500 war zu der Zeit (1969) noch nicht auf dem Markt und 31043 (so denke ich mal) ist auch nicht gerade geeignet.
Wurde ein Fremd-Differential genutzt oder wurde selbst eines gebaut..... ???

Ich habe mich dazu entschlossen selbst eines zu bauen. Was auch sehr gut funktioniert.
Einen Prototyp konnte man auf der Modellschau in Münster sehen.
 
Da einige ft Teile nicht so vorhanden waren wie ich sie benötigte musste ich ein paar Änderungen vornehmen (lassen). Für die Differentiale mussten einige Z40 geändert werden (Dank an Andreas).
Für den Antrieb einige Rast Z10 (gekürzt) und Schneckenmuttern mit Kugellagern versehen.
Rastachsen mussten für die Verwendung der Metall Kardans angepasst werden.
