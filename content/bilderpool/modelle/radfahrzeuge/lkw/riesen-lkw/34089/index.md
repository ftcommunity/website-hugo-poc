---
layout: "image"
title: "Steuerung Kupplung"
date: "2012-02-05T20:01:27"
picture: "08_Steuerung_Kupplung.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34089
- /detailsc40b.html
imported:
- "2019"
_4images_image_id: "34089"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34089 -->
Hier die Steuerung der Klauenkupplung. Der Antrieb erfolgt über die Kardanwelle. Der Motor wird mittels Dioden am Ende gestoppt.
