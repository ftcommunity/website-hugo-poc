---
layout: "comment"
hidden: true
title: "16261"
date: "2012-02-06T10:49:59"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Doch, geht!

Es gibt eine klassische Schaltung für Endschalter, die ohne störende Fremdteile (= Dioden) auskommt.

Siehe hier:

http://www.ftcommunity.de/details.php?image_id=26819

Vielleicht kann mir irgendjemand erklären, was die Dioden für einen Sinn machen, wenn es auch ohne geht ... ;o)

Gruß, Thomas