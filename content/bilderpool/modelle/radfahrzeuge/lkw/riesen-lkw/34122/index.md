---
layout: "image"
title: "etwas zu den selbstgebauten Differentialen"
date: "2012-02-08T14:34:02"
picture: "Diff_001.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34122
- /details7909.html
imported:
- "2019"
_4images_image_id: "34122"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-08T14:34:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34122 -->
Ich möchte hier noch etwas zu den Differentialen schreiben:
Der schwarze Teil des Drehkranzes hat ja den Nachteil das man dort keine Nabe eindrehen kann.
Das Loch ist zu groß. Nach langen Überlegungen habe ich das dann so gelöst:
Wenn man einen Spurkranz mit Gummiring nimmt, passt dieser sehr gut dort hinein.
Dann eine Freilaufnabe dazu und der erste Teil ist fertig.
