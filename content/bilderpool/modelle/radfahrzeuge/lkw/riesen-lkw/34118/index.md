---
layout: "image"
title: "Antrieb der Vorderachse"
date: "2012-02-08T14:34:02"
picture: "DSCN4385.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34118
- /details4dc4-3.html
imported:
- "2019"
_4images_image_id: "34118"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-08T14:34:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34118 -->
Auf diesem Bild ist zu sehen wie der Antrieb der Vorderachse geschieht.
An der linken mittleren Bildseite erkennt man so gerade noch das schwarze Zahnrad des Drehkranzes.
Dann weiter  mit dem senkrecht stehenden roten (verkleinerten) Z40.
Dieses greift mit seinen "Kronenzähnen" in die "Stirnzähne" des Drehkranzes. (sagt man das so??)
Weiter über zwei graue Kardangelenke bis zu einem Z10 (hier leider verdeckt).
Vom Z10 über die linke Kette zum anderen (roten Z10) und zur Drehscheibe.
Diese Drehscheibe ist mit ihrer kompletten Achse (mit zwei Kugellagern im Schneckenbaustein) frei beweglich.
Wenn sie nach rechts verschoben wird greifen die Klauen in Federnocken am schwarzen Z30 (rechte Kette).
Wird sie nach links verschoben löst sich die Kupplung und die Vordere Achse wird nicht mehr angetrieben.
Das alles bleibt bei Fahrt in Bewegung. Das heißt die Drehscheibe dreht sich IMMER.
Den Schwung dafür bekommt sie durch die permanente Verbindung mit dem vorderen Differential.
Das Ganze hätte man bestimmt besser hinbekommen aber mir viel´s gerade nicht ein ...
