---
layout: "image"
title: "Kupplung geschlossen"
date: "2012-02-05T20:01:27"
picture: "07_Kupplung_1.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34088
- /detailsb380-2.html
imported:
- "2019"
_4images_image_id: "34088"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34088 -->
Hier sieht man die Mitenhmer die das Z40 bewegen. In der späteren Version wird aus dem Z40 eine Drehscheibe.
