---
layout: "image"
title: "ausgefahren hoch"
date: "2012-03-05T13:18:36"
picture: "09_ausgefahren_hoch.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34576
- /details4a9f.html
imported:
- "2019"
_4images_image_id: "34576"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-03-05T13:18:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34576 -->
selbst wenn die Rampe ausgefahren ist kann sie noch hochgezogen werden.
