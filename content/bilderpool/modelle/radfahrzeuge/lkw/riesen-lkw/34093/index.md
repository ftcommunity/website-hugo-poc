---
layout: "image"
title: "Differential eigenbau erster Versuch"
date: "2012-02-05T20:01:41"
picture: "12_Diff1-1.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34093
- /details489d.html
imported:
- "2019"
_4images_image_id: "34093"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34093 -->
Hier der erste funktionsfähe Versuch ein Differential selbst zu bauen.
