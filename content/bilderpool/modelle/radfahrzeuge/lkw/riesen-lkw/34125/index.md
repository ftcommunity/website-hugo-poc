---
layout: "image"
title: "Einzelteile"
date: "2012-02-08T14:34:02"
picture: "Diff_004.jpg"
weight: "27"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34125
- /details3a53.html
imported:
- "2019"
_4images_image_id: "34125"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-08T14:34:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34125 -->
Das Ganze benötigt man 2 mal. Zusätzlich noch eine Achse für die roten Zahnräder.
Das Z10 auf der Rastachse muss gekürzt werden damit alles kompakt wird.
Der Spurkranz muss fest in den Drehkranz gedrück werden.
Man muss öfters testen ob die Achsen gerade laufen.
