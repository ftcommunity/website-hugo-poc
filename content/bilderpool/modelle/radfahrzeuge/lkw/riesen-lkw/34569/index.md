---
layout: "image"
title: "Rückansicht"
date: "2012-03-05T12:56:13"
picture: "02_Rckansicht.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34569
- /details2bd3-3.html
imported:
- "2019"
_4images_image_id: "34569"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-03-05T12:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34569 -->
