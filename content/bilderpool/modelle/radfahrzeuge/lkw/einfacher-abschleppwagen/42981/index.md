---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Ansicht Fahrerseite"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen02.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42981
- /details340f-2.html
imported:
- "2019"
_4images_image_id: "42981"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42981 -->
Hier noch von der anderen Seite.