---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Seilwinde und Stützgelenk"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen05.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42984
- /details3cd0.html
imported:
- "2019"
_4images_image_id: "42984"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42984 -->
Die Seilwinde ist mittig in den BS30 platziert, die beiden äußeren Zapfen sind nur zur Hälfte in der Nut. Eine Statikgelenk stellt sich auf den passenden Winkel ein. Ein Gelenkwürfel könnte auch funktionieren, dann kommt der Mast steiler rauf.