---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Mastspitze - von vorne"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen07.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42986
- /details5bda.html
imported:
- "2019"
_4images_image_id: "42986"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42986 -->
Nochmal die Mastspitze von der anderen Seite. Zwischen den Kupplungsstücken könnte noch ein Federnocken oder Verbinder 15 in den BS30 um den richtigen Abstand der Kupplungsstücke zu fixieren.