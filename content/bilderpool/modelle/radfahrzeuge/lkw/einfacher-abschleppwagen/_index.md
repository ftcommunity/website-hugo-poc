---
layout: "overview"
title: "Einfacher Abschleppwagen"
date: 2020-02-22T07:51:29+01:00
legacy_id:
- /php/categories/3197
- /categoriesfbea.html
- /categories3100.html
- /categories4965.html
- /categoriesdf9a.html
- /categories8b67.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3197 --> 
Ein einfacher Abschleppwagen, aufbauend - im wahrsten Wortsinne - auf dem einfachen Lastwagen aus "Einfacher Lastwagen mit Anhänger" (http://www.ftcommunity.de/categories.php?cat_id=3162). Optional ist auch noch die Fahrerkabine klappbar. Beide Erweiterungen sind voneinander unabhängig. Und natürlich sind das Anregungen, die nach eigenem Ermessen abgewandelt werden können.

