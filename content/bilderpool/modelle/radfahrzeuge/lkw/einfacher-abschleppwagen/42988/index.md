---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen09.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42988
- /details5cd6.html
imported:
- "2019"
_4images_image_id: "42988"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42988 -->
Am Abschleppwagen ist noch die Fahrerkabine klappbar. Diese Erweiterung ist unabhängig vom Aufbau "Abschleppkran" auch am Grundmodell möglich.