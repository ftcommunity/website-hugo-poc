---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Ansicht Beifahrerseite"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen01.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["einfacher", "Lastwagen", "Spielmodell", "kindgerecht", "universell", "Abschleppwagen", "Kranwagen"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42980
- /detailsb17d.html
imported:
- "2019"
_4images_image_id: "42980"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42980 -->
Ein einfacher Abschleppwagen, aufbauend - im wahrsten Wortsinne - auf dem einfachen Lastwagen aus "Einfacher Lastwagen mit Anhänger" (http://www.ftcommunity.de/categories.php?cat_id=3162). Optional ist auch noch die Fahrerkabine klappbar. Beide Erweiterungen sind voneinander unabhängig. Und natürlich sind das Anregungen, die nach eigenem Ermessen abgewandelt werden können.

Viele Teile sind es nicht, die den Aufbau des Abschleppwagens ausmachen.