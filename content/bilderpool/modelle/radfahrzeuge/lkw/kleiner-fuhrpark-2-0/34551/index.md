---
layout: "image"
title: "zugmaschiene 2"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_02.jpg"
weight: "5"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34551
- /detailsd863.html
imported:
- "2019"
_4images_image_id: "34551"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34551 -->
Unschwer zu erkennen: Es gibt wieder 2 gelenkte Achsen. Allerdings habe ich mein Lieblingskonstrukt im Thema ein wenig variiert: Diesmal wollte ich eine möglichst kompakte Zugamschiene mit 3 Achsen. Damit das bei aller Kürze auch noch vernünftig um die Kurve kommt, ist die "vordere Hinterachse" ebenfalls gelenkt.