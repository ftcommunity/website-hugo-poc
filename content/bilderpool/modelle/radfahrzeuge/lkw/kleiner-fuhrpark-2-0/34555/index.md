---
layout: "image"
title: "zugmaschiene 6"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_06_oben.jpg"
weight: "9"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34555
- /details5000.html
imported:
- "2019"
_4images_image_id: "34555"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34555 -->
