---
layout: "image"
title: "sattelzug_komplett_1"
date: "2012-03-04T19:50:12"
picture: "sattelzug_1.jpg"
weight: "2"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34548
- /details9376.html
imported:
- "2019"
_4images_image_id: "34548"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34548 -->
Totale. Der Sattelzug ist schon spielbereit, aber noch nicht komplett fertig. ÜBer den Aufbau und das finale Aussehen bin ich mir noch nicht ganz schlüssig