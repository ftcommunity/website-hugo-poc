---
layout: "image"
title: "bug5"
date: "2012-03-05T22:09:42"
picture: "buggy_05.jpg"
weight: "16"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34588
- /details2547.html
imported:
- "2019"
_4images_image_id: "34588"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-05T22:09:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34588 -->
