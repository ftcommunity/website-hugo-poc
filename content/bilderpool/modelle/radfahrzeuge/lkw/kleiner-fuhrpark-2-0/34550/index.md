---
layout: "image"
title: "zugmaschiene 1"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_01.jpg"
weight: "4"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- /php/details/34550
- /details1f98.html
imported:
- "2019"
_4images_image_id: "34550"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34550 -->
