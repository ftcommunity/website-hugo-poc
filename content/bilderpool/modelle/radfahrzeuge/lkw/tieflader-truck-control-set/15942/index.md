---
layout: "image"
title: "Tieflader-Truck mit Control-set und (Fremd-) Servo"
date: "2008-10-11T22:48:04"
picture: "Dieplader-Truck-Control-set_003_2.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/15942
- /details8d8b.html
imported:
- "2019"
_4images_image_id: "15942"
_4images_cat_id: "1447"
_4images_user_id: "22"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15942 -->
