---
layout: "image"
title: "Tieflader + Truck + Control-set"
date: "2008-10-11T22:48:03"
picture: "Dieplader-Truck-Control-set_007_2.jpg"
weight: "20"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/15941
- /detailsa677-3.html
imported:
- "2019"
_4images_image_id: "15941"
_4images_cat_id: "1447"
_4images_user_id: "22"
_4images_image_date: "2008-10-11T22:48:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15941 -->
Bei mir ist die FT-Servo schon überdreht weil Kräfte aufgetreten sind die innerhalb eines Modelles "üblicher" Bauart auftreten können. Ein Fischertechnik - Servo muss es normalerweise doch aushalten.....?!?!.....

Mit einen Servo mit Metallgetriebe (nicht FT) gibt es gar keine Problemen mehr.
