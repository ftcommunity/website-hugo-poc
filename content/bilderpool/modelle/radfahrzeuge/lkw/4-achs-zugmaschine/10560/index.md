---
layout: "image"
title: "4-Achs-Zugmaschine Lenkung im Detail"
date: "2007-05-30T15:14:04"
picture: "LKW_-_7.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["Lenkung", "Achsschenkellenkung", "Mehrachslenkung", "Doppelachslenkung", "Schwerlast"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10560
- /details1ccf.html
imported:
- "2019"
_4images_image_id: "10560"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:14:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10560 -->
