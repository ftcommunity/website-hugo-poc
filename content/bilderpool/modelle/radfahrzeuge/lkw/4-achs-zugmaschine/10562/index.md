---
layout: "image"
title: "4-Achs-Zugmaschine Stoßstange"
date: "2007-05-30T15:15:20"
picture: "LKW_-_9.jpg"
weight: "9"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10562
- /details65c0.html
imported:
- "2019"
_4images_image_id: "10562"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:15:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10562 -->
