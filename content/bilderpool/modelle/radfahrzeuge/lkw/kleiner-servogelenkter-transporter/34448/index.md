---
layout: "image"
title: "Unterboden"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34448
- /details7e36.html
imported:
- "2019"
_4images_image_id: "34448"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34448 -->
Der S-Motor ist nur mit zwei Federnocken auf dem Satz von BS7,5 angebracht, und dieser wird lediglich von den zwei Platten 15*60 mit dem Frontbereich verbunden, nämlich mit zwei BS30, zwischen denen BS15 mit zwei Zapfen stecken, an denen die Vorderräder angebracht sind.
