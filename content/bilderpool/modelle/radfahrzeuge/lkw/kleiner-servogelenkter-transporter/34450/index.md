---
layout: "image"
title: "Lenkung"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34450
- /details498e.html
imported:
- "2019"
_4images_image_id: "34450"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34450 -->
Der Hebel des Servos ist abgenommen, auf ihm steckt eine Klemmkupplung 20 (31024), die die kleine Lenkung dann relativ konventionell betätigt. Aufpassen muss man nur, dass man die Lenkung auf Nullstellung justiert, bevor alles zugebaut wird.
