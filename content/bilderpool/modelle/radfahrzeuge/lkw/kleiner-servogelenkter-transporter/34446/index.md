---
layout: "image"
title: "Linke Seite"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34446
- /details0c50.html
imported:
- "2019"
_4images_image_id: "34446"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34446 -->
Vor dem linken Hinterrad sitzt der Ein-/Aus-Schalter.
