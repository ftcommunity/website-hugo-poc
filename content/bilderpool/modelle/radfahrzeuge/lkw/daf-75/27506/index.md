---
layout: "image"
title: "centrale diff"
date: "2010-06-14T14:39:13"
picture: "P6130023.jpg"
weight: "15"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/27506
- /detailsd5ac-4.html
imported:
- "2019"
_4images_image_id: "27506"
_4images_cat_id: "1972"
_4images_user_id: "838"
_4images_image_date: "2010-06-14T14:39:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27506 -->
Centrale aandrijving voor de voor en achteras