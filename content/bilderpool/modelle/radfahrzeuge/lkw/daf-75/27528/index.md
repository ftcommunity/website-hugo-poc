---
layout: "image"
title: "cabine"
date: "2010-06-14T19:42:06"
picture: "P6140015.jpg"
weight: "26"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/27528
- /details3146-2.html
imported:
- "2019"
_4images_image_id: "27528"
_4images_cat_id: "1972"
_4images_user_id: "838"
_4images_image_date: "2010-06-14T19:42:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27528 -->
Close up van wiel plus trap (geheel weegt nu al 2,1 kg, waarvan 1,5 kg op de vooras rust weet niet of de besturing dit nog de bocht om krijgt)