---
layout: "image"
title: "Komplett runtergeklappt"
date: "2011-11-21T22:08:24"
picture: "wlfzetros06.jpg"
weight: "6"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/33540
- /details7b38.html
imported:
- "2019"
_4images_image_id: "33540"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33540 -->
Hier musste ich mein "Fotostudio" anbauen, die Plattform war zu kurz. Wie man sieht, ist der Winkel zwischen schiebender "Strebe" und dem Klapparm sehr ungünstig. Zum Anheben wird wahnsinnig viel Kraft gebraucht, das machte mir auch am Meistne Probleme. Aber die jetzige Lösung schafft es gerade so(siehe nächstes Foto).
