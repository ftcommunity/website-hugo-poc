---
layout: "image"
title: "Fahrzeugantrieb"
date: "2011-11-21T22:08:24"
picture: "wlfzetros09.jpg"
weight: "9"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/33543
- /details1792-2.html
imported:
- "2019"
_4images_image_id: "33543"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33543 -->
Immerhin hats für einen Antrieb beider Hinterachsen gereicht. Daran hängt ein 20:1er PM.
