---
layout: "image"
title: "Antrieb Teleskopmechanik im Arm"
date: "2011-11-21T22:08:24"
picture: "wlfzetros08.jpg"
weight: "8"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/33542
- /detailsfe67.html
imported:
- "2019"
_4images_image_id: "33542"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33542 -->
Die war die zweite Hauptbaustelle. Möglichst flach (sonst sitzt der Container so weit oben), aber trotzdem ordentlich angetrieben. Die untere Schneckenmutter, die mir zufällig gleich drei mal auf das Teppichmesser fiel, schiebt eine Reihe Bausteine hin und her. Auf der sitzt dann der hier nach hinten zeigende Arm, an dem der Haken befestigt ist.
