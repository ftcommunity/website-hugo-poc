---
layout: "image"
title: "Nano RC Truck 04"
date: "2011-12-05T23:19:12"
picture: "04_M_Truck_bfr_w.jpg"
weight: "24"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33617
- /details0216.html
imported:
- "2019"
_4images_image_id: "33617"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33617 -->
...hier ist der Truck mit einer Linkskurve an dem Fotografen auf die Zielgerade eingefahren...