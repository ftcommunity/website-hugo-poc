---
layout: "image"
title: "Nano RC Truck 01"
date: "2011-12-05T23:19:11"
picture: "01_M_Truck_f_w.jpg"
weight: "21"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33614
- /detailsdaa0-2.html
imported:
- "2019"
_4images_image_id: "33614"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33614 -->
Als Antwort auf meine Frage wie klein können RC Fahrzeuge in Ft sein entstand dieses Modell. Hier die Ansicht von der "Fahrerseite", wobei nur eine "Person" Platz fand. Details sind unverkennbar. Die nötigen Kabel lassen sich schwer verstauen. Auf Parkettboden läuft der Truck sehr geräuscharm.