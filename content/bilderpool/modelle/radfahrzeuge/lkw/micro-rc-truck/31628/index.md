---
layout: "image"
title: "Micro-RC-Truck 15"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_15.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31628
- /detailsf87d.html
imported:
- "2019"
_4images_image_id: "31628"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31628 -->
Links im Heck ist die Zug-/Druckstange, die den Schalter zum An- und Ausschalten des Empfängers bedient. Zum Starten muss sie 15 mm nach vorn geschoben werden und betätigt weiter vorn einen Taster. Das Ausschalten ist etwas kompliziert, aber mit dem kleinen Finger klappt es.