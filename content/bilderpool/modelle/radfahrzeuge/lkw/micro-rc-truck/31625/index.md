---
layout: "image"
title: "Micro-RC-Truck 12"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_12.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31625
- /detailse5d5.html
imported:
- "2019"
_4images_image_id: "31625"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31625 -->
Die Statikstrebe greift in den Strebenadapter auf der Zugmaschine. Der Drehpunkt des Aufliegers liegt exakt auf der Drehachse des Servos.