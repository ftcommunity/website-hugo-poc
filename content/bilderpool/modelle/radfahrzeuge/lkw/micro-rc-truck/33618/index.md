---
layout: "image"
title: "Nano RC Truck 05"
date: "2011-12-05T23:19:12"
picture: "05_M_Truck_fv_w.jpg"
weight: "25"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- /php/details/33618
- /details6e50.html
imported:
- "2019"
_4images_image_id: "33618"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33618 -->
...etwas dichter zum Fahrer...