---
layout: "image"
title: "Micro-RC-Truck 09"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31622
- /details1a6f.html
imported:
- "2019"
_4images_image_id: "31622"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31622 -->
Der Auflieger wird mittels eines Strebenadapters an der Zugmaschine befestigt. Die Aufnahmeachse davor greift in das Loch vom Servohebel, damit gelenkt werden kann.