---
layout: "image"
title: "LKW MAN, Volvo und Feuerwehr"
date: 2022-10-08T18:35:36+02:00
picture: "LKW01.jpg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Detailbilder zur Feuerwehr gibt es unter https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/lkw-feuerwehr-mercedes-benz-kofferaufbau/