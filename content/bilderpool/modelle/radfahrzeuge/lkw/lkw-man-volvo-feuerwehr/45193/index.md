---
layout: "image"
title: "LKW MAN, Volvo und Feuerwehr"
date: 2022-10-08T18:35:22+02:00
picture: "LKW03.jpg"
weight: "3"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Detailbilder zur Feuerwehr gibt es unter https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/lkw-feuerwehr-mercedes-benz-kofferaufbau/