---
layout: "comment"
hidden: true
title: "10235"
date: "2009-11-12T19:53:33"
uploadBy:
- "Minneralwasser"
license: "unknown"
imported:
- "2019"
---
Jop, das Z10 ist ganz einfach nur gesteckt, bzw. geklemmt. Die Welle des Motors ist auf einer Seite schon abgeflacht gewesen. So passte das Zahnrad zufällig genau auf die Welle. Allerdings eiert es minimal, dies wird aber durch die Antriebskette Ausgeglichen. Ich hab auch schon überlegt, das Z10 zu kürzen, war mir aber dann doch zu schade, obwohl ich noch genügend hätte ;)