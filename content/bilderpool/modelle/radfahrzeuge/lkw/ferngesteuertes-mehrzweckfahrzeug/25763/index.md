---
layout: "image"
title: "Uhrsprungsposition vom Regler"
date: "2009-11-11T20:20:47"
picture: "ferngesteuerteselektromehrzweckfahrzeug9.jpg"
weight: "9"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25763
- /details2de8.html
imported:
- "2019"
_4images_image_id: "25763"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25763 -->
Hier ist die Uhrsprüngliche Position des Reglers. Deutlich zu erkennen ist, wie nah er am Antriebsritzel ist. Das Zahnrad auf der Motorachse hat ein regelrechtes Loch in die Plastikverkleidung des Reglers Gefräst.