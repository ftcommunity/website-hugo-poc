---
layout: "image"
title: "Motorraum"
date: "2009-11-11T20:20:46"
picture: "ferngesteuerteselektromehrzweckfahrzeug5.jpg"
weight: "5"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- /php/details/25759
- /details4997.html
imported:
- "2019"
_4images_image_id: "25759"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25759 -->
Die Achse des 540er Motors hatte genau den passenden Durchmesser, sodass man ein kleines Zahnrad auf sie stecken konnte.
Über eine Kette wird dann die Kraft auf das Zahnrad an der Kardanwelle übertragen.