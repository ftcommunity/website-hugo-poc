---
layout: "overview"
title: "Ferngesteuertes Mehrzweckfahrzeug"
date: 2020-02-22T07:50:24+01:00
legacy_id:
- /php/categories/1806
- /categoriesd96f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1806 --> 
Die Antriebs- und Fernsteuerkomponenten, sowie der Akku sind nicht-Fischertechnikteile. Diese stammen aus dem RC-Car-Bereich. Das Fahrzeug ist recht flott unterwegs.

Allerdings ist bei den Geschwindigkeiten, die es erreicht schon ein Verschleiß an den Ft-Teilen. Besonders die Achsen und deren Lagerung sind betroffen.



Technische Daten

Motor: Industriebürstenmotor der Baugröße 540 mit 18 Turns

Regler: Robbe Rookie

Servo: Carson

Gewicht: 900 Gramm mit Akku

Geschwindigkeit: geschätzt ca. 15 km/h (mache demnächst einen genauen Test mit dem Navi) 

Fahrzeit: mit dem 3000er Akku etwas über 20 Minuten