---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:01"
picture: "Truck4.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8846
- /details8d10-2.html
imported:
- "2019"
_4images_image_id: "8846"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8846 -->
Zugmaschine von vorne.
