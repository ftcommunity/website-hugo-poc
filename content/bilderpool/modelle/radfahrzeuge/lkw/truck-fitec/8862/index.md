---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:12"
picture: "Truck20.jpg"
weight: "20"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8862
- /detailsac43-2.html
imported:
- "2019"
_4images_image_id: "8862"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8862 -->
Gesamt
