---
layout: "comment"
hidden: true
title: "1761"
date: "2006-12-21T12:26:13"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ein HAMMER-Modell!!! Wahnsinn!!! Tausende Funktionen und dabei - im Gegensatz zu anderen derartig komplexen Projekten - auch hinsichtlich Design und Proportionen ganz weit vorn! Super!

Sag mal, klemmt das nicht, wenn 2 Power-Motoren unsyncron über Schnecken 1 Differenzial antreiben?

Und sag mal, willst Du uns nicht die eine oder andere Funktion erklären? ;o) Ich sehe nur dutzende Motoren, Achsen, Zylinder, etc. und verliere schnell den Überblick, was jetzt was macht...

Gruß, Thomas