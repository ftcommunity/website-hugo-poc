---
layout: "image"
title: "muellwagen17.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen17.jpg"
weight: "17"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7937
- /details5dcf-2.html
imported:
- "2019"
_4images_image_id: "7937"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7937 -->
