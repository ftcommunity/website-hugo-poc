---
layout: "image"
title: "muellwagen22.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen22.jpg"
weight: "22"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7942
- /details97d4-2.html
imported:
- "2019"
_4images_image_id: "7942"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7942 -->
