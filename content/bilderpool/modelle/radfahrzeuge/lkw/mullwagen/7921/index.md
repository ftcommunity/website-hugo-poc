---
layout: "image"
title: "muellwagen01.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen01.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7921
- /details69e8-2.html
imported:
- "2019"
_4images_image_id: "7921"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7921 -->
