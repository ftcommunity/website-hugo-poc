---
layout: "image"
title: "muellwagen06.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen06.jpg"
weight: "6"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7926
- /details8737.html
imported:
- "2019"
_4images_image_id: "7926"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7926 -->
