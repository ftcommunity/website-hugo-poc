---
layout: "image"
title: "muellwagen28.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen28.jpg"
weight: "28"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7948
- /details42a5.html
imported:
- "2019"
_4images_image_id: "7948"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7948 -->
