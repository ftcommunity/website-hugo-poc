---
layout: "image"
title: "muellwagen15.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen15.jpg"
weight: "15"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7935
- /details7538-2.html
imported:
- "2019"
_4images_image_id: "7935"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7935 -->
