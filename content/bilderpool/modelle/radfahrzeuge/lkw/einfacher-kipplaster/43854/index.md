---
layout: "image"
title: "Kragen"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster10.jpg"
weight: "10"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43854
- /detailsc856-2.html
imported:
- "2019"
_4images_image_id: "43854"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43854 -->
Vorne ist dann noch der Kragen, eine Art Schutzdach. Das soll verhindern, daß Ladung auf das Dach des Führerhäuschens fällt; oder bei der Cabrio-Version dem Fahrer auf den Kopf. Für den einigermaßen robusten Look sorgen WS15°.