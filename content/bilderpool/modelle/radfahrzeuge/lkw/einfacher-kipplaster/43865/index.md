---
layout: "image"
title: "Verbesserung - Geänderte Heckpartie"
date: "2016-07-09T08:24:57"
picture: "einfacherkipplasterii3.jpg"
weight: "21"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43865
- /detailsda8e-2.html
imported:
- "2019"
_4images_image_id: "43865"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-09T08:24:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43865 -->
Oben auf der Traverse ist eine Bauplatte 15 x 90 mit 6 Zapfen (38245) eingeschoben. Ausser der Optik stabilisiert sie die 3 Bausteine 30 der Traverse zusätzlich.