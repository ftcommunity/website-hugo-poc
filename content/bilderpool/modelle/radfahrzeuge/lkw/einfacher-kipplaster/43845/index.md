---
layout: "image"
title: "Der Kipplaster von der Fahrerseite aus"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster01.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
schlagworte: ["einfacher", "Lastwagen", "Kipplaster", "Spielmodell", "kindgerecht", "universell", "Muldenkipper"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43845
- /details8e5e.html
imported:
- "2019"
_4images_image_id: "43845"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43845 -->
Es war mal wieder so weit: "Papa können wir den Abschleppwagen umbauen? Ich brauche einen Kipplaster und einen Bagger auch noch!"

Den einfachen Abschleppwagen (https://www.ftcommunity.de/categories.php?cat_id=3197) umzubauen war dann auch nicht sooo schwer. Bloß die ersten Versuche einer Mulde sahen -autozensiert- aus. Hier seht ihr das vorläufige Endergebnis. Zum Spielen langt es jedenfalls wieder.
Das Führerhaus läßt sich auch wegklappen ohne die Mulde anheben zu müssen. Der "Kragen" steht also nicht nur wegen der Optik so schräg.

... und diesmal ist der noch in der Montagehalle fotografiert worden. Die Tüte Gummibären war dann bei der Übergabe an den Kunden als Zugabe in der Kippmulde ;-)