---
layout: "image"
title: "Verbesserung - Geänderte Heckpartie"
date: "2016-07-09T08:24:57"
picture: "einfacherkipplasterii2.jpg"
weight: "20"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43864
- /details0370.html
imported:
- "2019"
_4images_image_id: "43864"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-09T08:24:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43864 -->
Zusätzlich hilft auf der Innenseite der Traverse noch eine Bauplatte 15 x 60 mit 4 Zapfen (38464) gegen Verschieben.

Die Hinterachse ist um eine Position nach hinten gewandert = "langer Radstand". Im optischen Gesamteindruck mit der Mulde wirkt das so etwas besser.