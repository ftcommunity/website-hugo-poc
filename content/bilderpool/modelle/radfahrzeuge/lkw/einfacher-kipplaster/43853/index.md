---
layout: "image"
title: "Klappenscharnier"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster09.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43853
- /detailsf7c0.html
imported:
- "2019"
_4images_image_id: "43853"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43853 -->
Die Mulde hat natürlich eine Klappe! Das Scharnier sitzt oben und das Bild dürfte für sich sprechen. Die Metallachse 90 geht einfach durch und je Seite verhindert eine Klemmbuchse 5 deren Herausrutschen. Alles andere klemmt von alleine ausreichend fest. Dem BS7,5 kann man untendrunter und nach vorne hin je eine Bauplatte 15 x 15 mit Zapfen (!) spendieren, dann verdreht sich nichts.

Die Klappe selbst besteht dann aus einer weiteren Grundplatte 90 x 90. Wegen der Anordnung des Scharniers schließt sie recht gut wenn die Mulde waagerecht liegt.