---
layout: "image"
title: "Montagedetail - Seitenwand"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster12.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43856
- /details0705-2.html
imported:
- "2019"
_4images_image_id: "43856"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43856 -->
So ist die, auf den Bildern vordere, Seitenwand bestückt. Der BS7,5 links oben im Eck wird mittels Federnocken mit der Grundplatte verbunden.