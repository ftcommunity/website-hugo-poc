---
layout: "image"
title: "büssing03.jpg"
date: "2013-07-20T19:18:59"
picture: "IMG_9059mit.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37178
- /details47cf.html
imported:
- "2019"
_4images_image_id: "37178"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:18:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37178 -->
Die Doppel-Hinterachse mit 1 Differenzial stammt von hier
http://www.ftcommunity.de/categories.php?cat_id=889 ab, ist aber jetzt auch ein bisschen geländegängig.
