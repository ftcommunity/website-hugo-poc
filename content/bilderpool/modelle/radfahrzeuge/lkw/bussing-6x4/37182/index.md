---
layout: "image"
title: "büssing07.jpg"
date: "2013-07-20T19:28:40"
picture: "IMG_9065mit.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37182
- /details6b94-2.html
imported:
- "2019"
_4images_image_id: "37182"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:28:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37182 -->
Die Lagerung der Räder ist eher etwas zartes. Die Innenfläche der Felge sorgt dafür, dass der BS7,5 nicht wegrutscht und alles auseinander fällt.
