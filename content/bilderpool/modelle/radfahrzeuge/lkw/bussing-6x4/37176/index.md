---
layout: "image"
title: "büssing01.jpg"
date: "2013-07-20T19:15:36"
picture: "IMG_8985.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37176
- /details001b.html
imported:
- "2019"
_4images_image_id: "37176"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37176 -->
Willst du einen Büssing sehn,
musst du auf die Kirmes gehn.

... könnte man sagen, denn diese alten Kisten gibt es sonst nirgendwo mehr. Der hier ist die Zugmaschine für den "Jumping", auf diesem Bild aber ohne den Antrieb auf der Heckplattform, und die Lenkung ist auch anders geworden.
