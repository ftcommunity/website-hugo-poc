---
layout: "image"
title: "Der Anhänger"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger11.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42544
- /detailsd209.html
imported:
- "2019"
_4images_image_id: "42544"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42544 -->
Ein Einachsanhänger, ebenso primitiv gebaut wie die Zugmaschine.