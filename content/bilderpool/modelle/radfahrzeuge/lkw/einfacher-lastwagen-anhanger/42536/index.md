---
layout: "image"
title: "Gespann von hinten"
date: "2015-12-20T13:46:57"
picture: "einfacherlastwagenmitanhaenger03.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42536
- /detailse796.html
imported:
- "2019"
_4images_image_id: "42536"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42536 -->
Rücklichter gibt's nicht - wer braucht die schon im Kinderzimmer?