---
layout: "image"
title: "Unterboden der Zugmaschine"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger05.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42538
- /details09e9.html
imported:
- "2019"
_4images_image_id: "42538"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42538 -->
Ein paar Winkelträger bilden den Leiterrahem und die Achslager. Die Statikbauplatte stabilsiert das Ganze.
Die Achsen gehen durch, die Räder sind darauf festgeklemmt.