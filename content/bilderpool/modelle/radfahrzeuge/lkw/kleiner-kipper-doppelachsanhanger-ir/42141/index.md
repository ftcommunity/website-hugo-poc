---
layout: "image"
title: "Mit Hydraulik-Anhänger, gekippt"
date: "2015-10-26T11:27:24"
picture: "DSC08330_1.jpg"
weight: "7"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42141
- /details8f5e.html
imported:
- "2019"
_4images_image_id: "42141"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T11:27:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42141 -->
Durch Umstecken einer Hydraulikleitung auf ein T-Stück (mit  Blindstopfen auf dem 3. Anschluß) wird der Übergang zur Pneumatik hergestellt.
