---
layout: "image"
title: "Rechte Seitenansicht"
date: "2015-10-26T11:27:24"
picture: "DSC08331_1.jpg"
weight: "8"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42142
- /details826f.html
imported:
- "2019"
_4images_image_id: "42142"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T11:27:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42142 -->
Im V-Stein 60x30x30, hinter der Klappe, sind die beiden Magnetventile untergebracht
