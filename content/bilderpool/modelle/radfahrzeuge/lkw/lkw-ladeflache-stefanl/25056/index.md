---
layout: "image"
title: "Gesamtansicht 1"
date: "2009-09-22T18:47:58"
picture: "lkwmitladeflaeche01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/25056
- /detailsdd18.html
imported:
- "2019"
_4images_image_id: "25056"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25056 -->
Antrieb durch einen 8:1 Power Motor, Lenkung mit Mini Motor, gekippt wird durch einen 50:1 Power Motor mit Schnecke. 
Reifen gibts hier http://www.ftcommunity.de/categories.php?cat_id=1428 und hier http://www.haertle.de/rc+modellbau/rc+car+zubehoer/reifen+felgen+raeder/tamiya+9445529+reifen+4+umruestsatz+offroad+onroad.html?listtype=search&searchparam=9445529
