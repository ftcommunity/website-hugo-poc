---
layout: "image"
title: "Geländegängig"
date: "2009-09-22T18:47:58"
picture: "lkwmitladeflaeche08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/25063
- /detailsbf9a.html
imported:
- "2019"
_4images_image_id: "25063"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25063 -->
Beide Achsen sind gefederte Pendelachsen.
