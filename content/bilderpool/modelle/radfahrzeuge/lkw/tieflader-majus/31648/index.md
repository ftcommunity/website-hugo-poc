---
layout: "image"
title: "Fotomontage des Trucks"
date: "2011-08-28T15:12:24"
picture: "Unbenannt-1.jpg"
weight: "1"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
uploadBy: "majus"
license: "unknown"
legacy_id:
- /php/details/31648
- /details7c88-3.html
imported:
- "2019"
_4images_image_id: "31648"
_4images_cat_id: "2361"
_4images_user_id: "1239"
_4images_image_date: "2011-08-28T15:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31648 -->
Tieflader

Maße:
Länge mit Rampen:	80 cm
Länge:			72 cm
Höhe:			21 cm
Breite:			18 cm
Länge des Trucks
ohne Auflieger:	          30,5 cm

Beschreibung:
Der LKW besitzt folgende Features:
- Lenkung der Vorderachse auf dem Dach mithilfe des linken Zahnrades von   
  rechts.
- Lenkrad lenkt mit
- Stütze des Aufliegers
- Rampen am Ende elektrisch hoch- und runterfahrbar
- Beleuchtung (noch nicht angeschlossen
Auf ein Motor zum Antrieb habe ich bewusst verzichtet, da ein Akku von FT viel zu schwach wäre. Ebenso ging es mir mit der Lenkung, diese ist auch ohne Motor auf dem Dach für leichstes Lenken angebracht. Die einzige für den FT-Akku zumutbare Last ist das elektrische Anheben der Rampen am Ende des Aufliegers. Zusätzlich habe ich den Innenraum mit FT versucht nachzugestalten. 
In diesem Modell ging es mir also weniger um die technische sondern mehr um die optische Herausforderung.