---
layout: "image"
title: "Nano-RC-Truck 15"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_15.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38513
- /details7202.html
imported:
- "2019"
_4images_image_id: "38513"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38513 -->
Ganz großes Glück, dass die Riegelscheibe so ideal an das Getriebe passt! Oder hat das jemand schon mal so gebaut?