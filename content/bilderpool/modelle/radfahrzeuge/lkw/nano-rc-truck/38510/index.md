---
layout: "image"
title: "Nano-RC-Truck 12"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_12.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38510
- /details350d-2.html
imported:
- "2019"
_4images_image_id: "38510"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38510 -->
Diese Getriebeeinheit auf Basis der Riegelscheiben bedarf einer sehr genauen Justage der Lagen der Riegelscheiben. Sonst klemmt es, oder es rutscht durch.

Je 2 Klemmstifte (107356) klemmen in einer Hülse 15 (31983) und bilden so eine Antriebsachse.

Auf eine zweite Ebene mit Riegelscheiben auf der anderen Seite der K-Achse 30 habe ich bewusst verzichtet. Sie bringt keinen Mehrwert, aber die Riegelscheiben auf den Achsen müssten dann zahngenau ausgerichtet werden, was in dieser Größe kaum möglich ist.

Links im Bild der Schalter, der den Empfänger ein- und ausschaltet.