---
layout: "image"
title: "lkw07.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw07.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45595
- /details862d.html
imported:
- "2019"
_4images_image_id: "45595"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45595 -->
