---
layout: "image"
title: "lkw12.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw12.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45600
- /detailsf068.html
imported:
- "2019"
_4images_image_id: "45600"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45600 -->
