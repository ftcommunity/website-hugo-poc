---
layout: "image"
title: "lkw34.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw34.jpg"
weight: "34"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45622
- /details00ae.html
imported:
- "2019"
_4images_image_id: "45622"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45622 -->
