---
layout: "image"
title: "lkw27.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw27.jpg"
weight: "27"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45615
- /detailsfd62.html
imported:
- "2019"
_4images_image_id: "45615"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45615 -->
