---
layout: "image"
title: "lkwmehr2.jpg"
date: "2017-03-23T14:27:57"
picture: "lkwmehr2.jpg"
weight: "43"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45639
- /details49ce-2.html
imported:
- "2019"
_4images_image_id: "45639"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-23T14:27:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45639 -->
