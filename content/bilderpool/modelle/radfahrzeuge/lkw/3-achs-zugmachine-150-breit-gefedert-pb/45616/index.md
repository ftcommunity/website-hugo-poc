---
layout: "image"
title: "lkw28.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw28.jpg"
weight: "28"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45616
- /details81b6.html
imported:
- "2019"
_4images_image_id: "45616"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45616 -->
Die Stein Rechts unten ist etwas besonderes. In die Kasten aus den Achtziger Jahren würde die benutzt um Stühle an zu schieben. Ich hab nur Einen, aus ein Baukasten mit einen Mars-buggy oder so etwas, die ich mal an Ebay gekauft habe. Am motor ist ein Impulszahnrad befestigt, und das greift dan in diesen Baustein, und das alles hat was Spiel das ausreicht als Ausgleich für die Bewegung der gefederten Hintenblocks, sowie in der Länge als wie eine Art Kardan. Deshalb konnte ich hier nur eine originalen Kardan verwenden, und das spart Raum. Eine Idee von Harald Steinhaus, wenn ich mich nicht verirre. Läuft super.