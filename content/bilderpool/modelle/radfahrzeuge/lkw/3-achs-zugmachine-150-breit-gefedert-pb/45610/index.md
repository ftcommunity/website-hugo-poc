---
layout: "image"
title: "lkw22.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw22.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45610
- /details3662.html
imported:
- "2019"
_4images_image_id: "45610"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45610 -->
