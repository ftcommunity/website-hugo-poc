---
layout: "image"
title: "lkw35.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw35.jpg"
weight: "35"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45623
- /details0c3b-2.html
imported:
- "2019"
_4images_image_id: "45623"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45623 -->
