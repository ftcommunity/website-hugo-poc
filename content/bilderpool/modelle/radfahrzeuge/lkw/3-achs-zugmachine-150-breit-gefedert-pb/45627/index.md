---
layout: "image"
title: "lkw39.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw39.jpg"
weight: "39"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45627
- /details7b17-2.html
imported:
- "2019"
_4images_image_id: "45627"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45627 -->
