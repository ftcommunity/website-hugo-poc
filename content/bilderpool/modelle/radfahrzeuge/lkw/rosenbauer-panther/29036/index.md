---
layout: "image"
title: "15 Antrieb"
date: "2010-10-19T18:24:55"
picture: "rosenbauerpanther04_2.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29036
- /details5fc1.html
imported:
- "2019"
_4images_image_id: "29036"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29036 -->
Beide Achsen sind angetrieben.