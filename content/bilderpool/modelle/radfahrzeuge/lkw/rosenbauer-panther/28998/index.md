---
layout: "image"
title: "11 Kabel"
date: "2010-10-14T17:59:03"
picture: "rosenbauerpanther11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28998
- /detailsd751.html
imported:
- "2019"
_4images_image_id: "28998"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:03"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28998 -->
So sieht das dann aus, wenn das Kabel am Panther angeschlossen ist.