---
layout: "image"
title: "17 Innenleben"
date: "2010-10-19T18:24:55"
picture: "rosenbauerpanther06_2.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29038
- /details1301.html
imported:
- "2019"
_4images_image_id: "29038"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29038 -->
So siehts innen drin aus. 
Der Motor in der Mitte betätigt über Wellen und Zahnräder das Handventil, durch das das Wasser fließt.
Man kann auch das Interface, den Tank und zum Teil den Kompressor sehen.