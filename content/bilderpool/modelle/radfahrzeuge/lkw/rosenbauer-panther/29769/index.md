---
layout: "image"
title: "31 Innen"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther07_3.jpg"
weight: "31"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29769
- /details572c-2.html
imported:
- "2019"
_4images_image_id: "29769"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29769 -->
