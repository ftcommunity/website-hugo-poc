---
layout: "image"
title: "23 Ventil"
date: "2010-10-19T18:24:56"
picture: "rosenbauerpanther12.jpg"
weight: "23"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/29044
- /detailsbe46-3.html
imported:
- "2019"
_4images_image_id: "29044"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:56"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29044 -->
Wenn ich ihn auseinanderbau, kann ich das Ventil nochmal einzeln fotografieren.