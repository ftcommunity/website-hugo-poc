---
layout: "image"
title: "04 Fahrerhaus"
date: "2010-10-14T17:59:02"
picture: "rosenbauerpanther04.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/28991
- /detailse6a7-2.html
imported:
- "2019"
_4images_image_id: "28991"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28991 -->
Tür offen