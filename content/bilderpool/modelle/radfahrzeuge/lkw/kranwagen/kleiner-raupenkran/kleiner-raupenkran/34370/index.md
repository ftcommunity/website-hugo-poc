---
layout: "image"
title: "Kran aufgestellt"
date: "2012-02-23T21:06:55"
picture: "kleinerraupenkran07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34370
- /details3086.html
imported:
- "2019"
_4images_image_id: "34370"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:06:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34370 -->
Hier hat der Kran eine Höhe von 54cm.