---
layout: "image"
title: "Blick in die Fernbedinung"
date: "2012-02-23T21:07:06"
picture: "kleinerraupenkran14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34377
- /details19a1-2.html
imported:
- "2019"
_4images_image_id: "34377"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:07:06"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34377 -->
-