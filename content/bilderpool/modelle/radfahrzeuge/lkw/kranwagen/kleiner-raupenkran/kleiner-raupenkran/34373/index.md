---
layout: "image"
title: "Fernbedinung"
date: "2012-02-23T21:06:55"
picture: "kleinerraupenkran10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34373
- /details2cde.html
imported:
- "2019"
_4images_image_id: "34373"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:06:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34373 -->
Der Dreheknopf ist für die Geschwindigkeitstrimung der Motoren zuständig.