---
layout: "image"
title: "Von Vorne"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran04.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34319
- /details117f.html
imported:
- "2019"
_4images_image_id: "34319"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34319 -->
Hier sieht man gut das Getriebe der Mini-Motoren.