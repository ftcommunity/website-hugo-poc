---
layout: "image"
title: "Kran"
date: "2012-02-20T21:16:19"
picture: "kleinerraupenkran12.jpg"
weight: "12"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34327
- /detailseabf-2.html
imported:
- "2019"
_4images_image_id: "34327"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:16:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34327 -->
Der erste Teil des Auslegers ist hier gut zu sehen.