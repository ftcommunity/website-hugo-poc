---
layout: "comment"
hidden: true
title: "16437"
date: "2012-02-21T07:52:27"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Dein Kran gefällt mir wirklich gut! Schön minimalistisch aufgebaut. Vor allem das Fahrgestell ist Dir gut gelungen.

Die gemischten Farben sind jetzt nicht so meins, aber ich vermute, Du hast (noch) nicht genügend gleichfarbige Teile. Daher akzeptiert. ;o)

Wie steuerst Du die insgesamt 6 Motoren? Per IR-Fernsteuerung mit 2 Empfängern? Ne Stromversorgung müsste ja auch noch ans Modell ...

Oder per Kabelfernsteuerung? Dann bin ich gespannt auf das Steuerpult.

Die Grundfläche des Fahrgestells ist im Verhältnis zum restlichen Kran aber etwas klein, oder? Auch mit der improvisierten Abstützung seitlich der Ketten wird der Kran wohl schnell zum Kippen neigen ...

Ich freue mich auf weitere Bilder und Details!

Gruß, Thomas