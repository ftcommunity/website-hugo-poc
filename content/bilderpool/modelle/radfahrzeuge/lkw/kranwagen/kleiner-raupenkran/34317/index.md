---
layout: "image"
title: "Unterteil"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran02.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34317
- /details6f74-2.html
imported:
- "2019"
_4images_image_id: "34317"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34317 -->
Das Unterteil wird mit 2 Mini-Motoren angetrieben.