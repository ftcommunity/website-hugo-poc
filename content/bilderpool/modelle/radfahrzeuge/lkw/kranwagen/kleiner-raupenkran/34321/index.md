---
layout: "image"
title: "Von oben"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran06.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/34321
- /detailsa8c4-2.html
imported:
- "2019"
_4images_image_id: "34321"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34321 -->
Hier sieht man die Seilwind nochmal etwas besser.