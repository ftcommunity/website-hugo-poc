---
layout: "image"
title: "Kranwagen"
date: "2007-04-30T09:00:18"
picture: "kranwagen1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10224
- /detailsbba1-3.html
imported:
- "2019"
_4images_image_id: "10224"
_4images_cat_id: "926"
_4images_user_id: "557"
_4images_image_date: "2007-04-30T09:00:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10224 -->
von oben.Gegengewicht erkennbar