---
layout: "image"
title: "Kranwagen Neu"
date: "2007-04-04T17:42:41"
picture: "kranwagenneu7.jpg"
weight: "7"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9986
- /details6fa5.html
imported:
- "2019"
_4images_image_id: "9986"
_4images_cat_id: "901"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T17:42:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9986 -->
Kranarm oben