---
layout: "image"
title: "Kranwagen Neu"
date: "2007-04-04T17:42:41"
picture: "kranwagenneu2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9981
- /detailsdf70-2.html
imported:
- "2019"
_4images_image_id: "9981"
_4images_cat_id: "901"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T17:42:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9981 -->
Kranarm