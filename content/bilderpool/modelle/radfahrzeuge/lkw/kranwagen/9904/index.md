---
layout: "image"
title: "Kranwagen"
date: "2007-04-03T17:33:56"
picture: "kranwagen1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9904
- /details10e4.html
imported:
- "2019"
_4images_image_id: "9904"
_4images_cat_id: "896"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9904 -->
Seite