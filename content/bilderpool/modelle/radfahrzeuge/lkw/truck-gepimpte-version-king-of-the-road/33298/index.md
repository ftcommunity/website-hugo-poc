---
layout: "image"
title: "Sattelauflieger - Ansicht von unten"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion7.jpg"
weight: "7"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33298
- /detailsad21-2.html
imported:
- "2019"
_4images_image_id: "33298"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33298 -->
Die Auflieger-Stütze wird mit einem Minimot aus- und eingefahren.