---
layout: "image"
title: "Micro-RC-LKW 03"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31785
- /details9733-2.html
imported:
- "2019"
_4images_image_id: "31785"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31785 -->
