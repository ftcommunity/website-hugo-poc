---
layout: "comment"
hidden: true
title: "15096"
date: "2011-09-13T19:13:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Guck an, wie schön. Du meinst, die U-Getriebe-Achse, die auf beiden Seiten heraussteht, gibt es nicht mehr, oder wie ist das gemeint?

Zum Modell: Wie gewohnt, ganz und gar hervorragend!

Gruß,
Stefan