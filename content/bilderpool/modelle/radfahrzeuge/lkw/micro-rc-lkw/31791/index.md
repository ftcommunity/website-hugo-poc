---
layout: "image"
title: "Micro-RC-LKW 09"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31791
- /details4438.html
imported:
- "2019"
_4images_image_id: "31791"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31791 -->
Jetzt wird es eng! In Höhe und Breite wäre kein Millimeter weniger drin gewesen. Aber es ist trotzdem alles locker verbaut, und nichts klemmt.

Von der Lenkung und vom Servo ist immer noch nichts zu sehen. Wo soll denn das noch stecken? ;o)

Man sieht hier auch gut, dass auf der linken Seite - im Gegensatz zu rechts - keine Bauplatten 30x90 möglich gewesen wären. Die Zapfen hätten bereits dazu geführt, dass der Block aus Akku und Empfänger nicht mehr reingepasst hätte.

Auch die Länge passt haarscharf, den der Empfänger liegt bereits VOR dem Servo, direkt über der Drehachse der Lenkung. Der Empfänger liegt aber vorn gerade noch nicht an der Bauplatte 30x45 an.