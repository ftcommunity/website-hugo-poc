---
layout: "image"
title: "Micro-RC-LKW 08"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_08.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31790
- /detailsfebe.html
imported:
- "2019"
_4images_image_id: "31790"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31790 -->
Der Schalter passte GERADE SO ins Führerhaus ...