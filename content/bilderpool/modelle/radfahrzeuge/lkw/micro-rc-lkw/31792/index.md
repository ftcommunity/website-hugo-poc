---
layout: "image"
title: "Micro-RC-LKW 10"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_10.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/31792
- /detailscb87.html
imported:
- "2019"
_4images_image_id: "31792"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31792 -->
Das Geheimnis um die Lenkung wird gelüftet: Das Servo ließe sich mit Hebel nicht quer im Fahrzeug verbauen, weil es zu lang ist. Also habe ich den Hebel weggelassen und es ganz hinten im Heck längs eingebaut. Die Welle vom Servo lenkt also direkt, ohne Hebel!

Jetzt musste nur noch eine Lösung für die Befestigung von FT-Achsen am Servoritzel her ... ;o)

Das Führerhaus hält nur rechts an dem Baustein 7,5. Links war aufgrund der Stecker am Schalter kein Platz mehr für eine Befestigung.

Die Stromversorgung vom Empfänger zum XS-Motor erfolgt durch die Grundplatten.