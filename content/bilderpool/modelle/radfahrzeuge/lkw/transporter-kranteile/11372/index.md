---
layout: "image"
title: "Nachläufer"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile06.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11372
- /detailsf8d0-2.html
imported:
- "2019"
_4images_image_id: "11372"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11372 -->
Das Nachläuferteil welches hinten an dem/den Transportteilen befestigt wird.