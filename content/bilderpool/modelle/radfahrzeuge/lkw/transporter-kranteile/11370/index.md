---
layout: "image"
title: "Zugfahrzeug (von unten)"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile04.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11370
- /detailsb20b.html
imported:
- "2019"
_4images_image_id: "11370"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11370 -->
Hier mal von unten zu sehen.