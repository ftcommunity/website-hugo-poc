---
layout: "image"
title: "Wippspitzteile"
date: "2007-08-13T17:04:05"
picture: "transporterkranteile08.jpg"
weight: "8"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/11374
- /detailse4c1.html
imported:
- "2019"
_4images_image_id: "11374"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11374 -->
Nochmal die Version mit den beiden "kleinen" Teilen.