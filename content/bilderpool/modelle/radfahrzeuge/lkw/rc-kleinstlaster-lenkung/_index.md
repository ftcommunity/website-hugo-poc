---
layout: "overview"
title: "RC-Kleinstlaster mit Lenkung"
date: 2020-02-22T07:51:16+01:00
legacy_id:
- /php/categories/2814
- /categories1cae.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2814 --> 
Ferngesteuerter Oldtimer-Laster mit minimalen Außenmaßen. Trotzem sehr flott, vorderradgefedert und mit sehr kleinem Wendekreis. Gute Basis für allerlei ferngesteuerte Kleinstmodelle.