---
layout: "overview"
title: "LKW blau"
date: 2023-03-28T13:57:19+02:00
---

Ich sah einen blauen Truck von Thomas Habig und fing an, meine blauen Teile zu sammeln, um einen ähnlichen Truck zu bauen. Es gibt zwar immer noch ein paar rote Teile, aber ich mag das Ergebnis.

 (https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/lkw/5436/)

