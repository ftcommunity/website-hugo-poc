---
layout: "image"
title: "Chassis"
date: 2023-03-28T13:57:25+02:00
picture: "LKWB_4.jpeg"
weight: "4"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["LKW", " blau"]
uploadBy: "Website-Team"
license: "unknown"
---

