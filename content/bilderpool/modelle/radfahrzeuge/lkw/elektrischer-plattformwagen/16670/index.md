---
layout: "image"
title: "Platformwagen 1"
date: "2008-12-20T19:31:34"
picture: "Platformwagen_01_klein.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/16670
- /details5f08.html
imported:
- "2019"
_4images_image_id: "16670"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16670 -->
Das ist der Prototyp des Modells eines elektrisch angetriebenen Platformwagens mit Radnabenmotoren an der gelenkten Vorderachse. Solche kleinen Platformwagen werden in der Regel für kleinere Transportaufgaben innerhalb von Fabriken oder Werkhallen genutzt, dann aber mit normalem elektrischen Hinterachsantrieb.

Bei meinem Modell lag der Fokus auf der Konstruktion einer gelenkten Vorderachse mit zwei Minimotoren als Radnabenmotoren. Die Lenkung erfolgt über ein Servo. Der Servohebel des Servos ist mir im Original aber zu kurz, so dass ich ihn mit FT-Teilen verlängert habe, um einen stark vergrößerten Lenkeinschlag der Räder zu erreichen.

Die beiden Minimotoren an der Vorderachse haben natürlich kein leichtes Spiel mit dem Fahrzeug, so dass ich extremen Leichtbau anwenden musste, damit der Platformwagen von der Stelle kommt. Die Stromversorgung erfolgt daher über einen 9V-Blockakku, der zwischen den Hinterrädern angeordnet wurde, da dort ja kein Antrieb sitzt. Somit ergibt sich eine absolut ebene Ladefläche, die ich aber aus Gewichtsgründen nicht weiter bebaut habe. Auch das Fahrerhaus ist nur grob angedeutet.

Das Fahrverhalten wirkt recht realistisch, da das Fahrzeug sehr langsam beschleunigt und nach Gaswegnahme ohne bremsende Getriebteile noch lange rollt. Es ist extrem wendig und erreicht erstaunlich hohe Geschwindigkeiten. Da kein Getriebe verbaut ist, fährt es superleise - eben wie ein richtiges Elektroauto!

Die angetriebene gelenkte Vorderachse bietet natürlich die ideale Basis für ein Allrad-Fahrzeug mit vier Radnabenmotoren, also zusätzlich noch zwei an der Hinterachse. Leider habe ich aktuell nur drei Minimotoren...