---
layout: "image"
title: "Platformwagen 3"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_03_klein.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/16672
- /details815d.html
imported:
- "2019"
_4images_image_id: "16672"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16672 -->
Hier sieht man den extremen Lenkeinschlag der Vorderräder mit den Radnabenmotoren.

Empfänger, Schalter und Servo sind alle im aus Leichtbaugründen nur grob angedeuteten Fahrerhaus untergebracht.