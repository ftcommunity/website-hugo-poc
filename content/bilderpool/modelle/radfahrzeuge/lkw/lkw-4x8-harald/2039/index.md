---
layout: "image"
title: "4x8Gr01.JPG"
date: "2003-12-23T20:27:52"
picture: "4x8Gr01.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2039
- /detailsf7f4.html
imported:
- "2019"
_4images_image_id: "2039"
_4images_cat_id: "432"
_4images_user_id: "4"
_4images_image_date: "2003-12-23T20:27:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2039 -->
Das Design gewaltig aufgeräumt, neue Hinterachse (natürlich geländegängig), neue Lenkung. Und Reifen, die dazu passen. Das ist die Generation 'Vater' auf dem Weg zum 8x8 Allradantrieb, die aber selbst immer noch 4x8 ist.

Als Nutzlast gibt es einen Betonmischer, zu finden unter 'Baumaschinen', http://www.ftcommunity.de/categories.php?cat_id=126