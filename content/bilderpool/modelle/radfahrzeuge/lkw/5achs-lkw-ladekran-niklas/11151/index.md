---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw09.jpg"
weight: "9"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11151
- /details3d4f.html
imported:
- "2019"
_4images_image_id: "11151"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11151 -->
nochmal der Ausleger