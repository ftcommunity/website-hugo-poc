---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw10.jpg"
weight: "10"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11152
- /detailsfa43.html
imported:
- "2019"
_4images_image_id: "11152"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11152 -->
Die Gewicht in den Tanks der Zugmaschine werden benötigt um ein umkippen der zugmaschine beim manuellen Lenken verhindern