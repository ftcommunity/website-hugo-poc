---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw01.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11143
- /details6dc2-2.html
imported:
- "2019"
_4images_image_id: "11143"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11143 -->
Hier der 5-Achs LKW von oben