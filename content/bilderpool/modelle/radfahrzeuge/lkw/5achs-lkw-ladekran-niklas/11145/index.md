---
layout: "image"
title: "LKW mit Kran"
date: "2007-07-20T14:48:20"
picture: "lkw03.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11145
- /detailsa8dd.html
imported:
- "2019"
_4images_image_id: "11145"
_4images_cat_id: "1009"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T14:48:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11145 -->
Hier der Übergang zwischen Auflieger und Zugmaschine, die Gewind dienen als Stützen