---
layout: "image"
title: "modellevonclauswludwig52.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig52.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12712
- /detailsc659.html
imported:
- "2019"
_4images_image_id: "12712"
_4images_cat_id: "1138"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12712 -->
