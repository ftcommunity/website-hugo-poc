---
layout: "image"
title: "Zugmaschine mit 1-achs Anhänger"
date: "2007-11-05T18:24:55"
picture: "DSCN1972.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12507
- /details3e54-2.html
imported:
- "2019"
_4images_image_id: "12507"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T18:24:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12507 -->
Beide mit Kippmulde.
