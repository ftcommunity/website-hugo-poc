---
layout: "image"
title: "Servicefahrzeug"
date: "2007-11-05T15:54:02"
picture: "DSCN1980.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12500
- /detailsc947.html
imported:
- "2019"
_4images_image_id: "12500"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12500 -->
Anhänger
