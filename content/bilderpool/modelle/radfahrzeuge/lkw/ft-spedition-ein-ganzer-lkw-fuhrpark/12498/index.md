---
layout: "image"
title: "kleiner LKW mit Kippmulde"
date: "2007-11-05T15:54:02"
picture: "DSCN1971.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12498
- /details55a9.html
imported:
- "2019"
_4images_image_id: "12498"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12498 -->
Er hat ganz schön was zu "schalten".
