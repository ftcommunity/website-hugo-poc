---
layout: "image"
title: "Servicefahrzeug"
date: "2007-11-05T15:54:02"
picture: "DSCN1979.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12499
- /detailsa79c-4.html
imported:
- "2019"
_4images_image_id: "12499"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12499 -->
Für den Cat. Er trägt eine Ersatzkette.
