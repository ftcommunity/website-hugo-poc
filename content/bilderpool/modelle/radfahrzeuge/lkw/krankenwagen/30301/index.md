---
layout: "image"
title: "Die Fahrerkabine"
date: "2011-03-21T18:35:36"
picture: "krankenwagen07.jpg"
weight: "6"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30301
- /details1b86.html
imported:
- "2019"
_4images_image_id: "30301"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30301 -->
Es ist sehr geräumig in der Fahrerkabine