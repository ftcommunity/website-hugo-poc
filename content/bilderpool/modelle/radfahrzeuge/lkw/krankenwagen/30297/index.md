---
layout: "image"
title: "Vorne"
date: "2011-03-21T18:35:36"
picture: "krankenwagen02.jpg"
weight: "2"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- /php/details/30297
- /detailse0fd.html
imported:
- "2019"
_4images_image_id: "30297"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30297 -->
Die Warnblinker sind eingeschaltet