---
layout: "image"
title: "modellevonclauswludwig85.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig85.jpg"
weight: "11"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12745
- /details7239.html
imported:
- "2019"
_4images_image_id: "12745"
_4images_cat_id: "1146"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12745 -->
