---
layout: "image"
title: "Muhl containers"
date: "2006-04-08T12:26:05"
picture: "DSCN4428.jpg"
weight: "2"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6026
- /details40fc.html
imported:
- "2019"
_4images_image_id: "6026"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6026 -->
