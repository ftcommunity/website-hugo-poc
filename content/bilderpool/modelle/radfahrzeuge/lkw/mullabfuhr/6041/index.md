---
layout: "image"
title: "Muhlpresse starten 2"
date: "2006-04-08T12:26:13"
picture: "DSCN4444.jpg"
weight: "17"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6041
- /details95d1-3.html
imported:
- "2019"
_4images_image_id: "6041"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6041 -->
