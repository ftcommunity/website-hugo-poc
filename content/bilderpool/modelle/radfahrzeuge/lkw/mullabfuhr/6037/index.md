---
layout: "image"
title: "Leere Containers wieder zuruck"
date: "2006-04-08T12:26:13"
picture: "DSCN4440.jpg"
weight: "13"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/6037
- /detailsda6a-2.html
imported:
- "2019"
_4images_image_id: "6037"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6037 -->
