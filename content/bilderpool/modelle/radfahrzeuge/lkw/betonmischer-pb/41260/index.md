---
layout: "image"
title: "Mixer-lr-27"
date: "2015-06-24T14:11:15"
picture: "betonmischer14.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41260
- /details8d91-3.html
imported:
- "2019"
_4images_image_id: "41260"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41260 -->
