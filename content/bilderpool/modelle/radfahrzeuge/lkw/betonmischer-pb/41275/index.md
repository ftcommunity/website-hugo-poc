---
layout: "image"
title: "Mixer-lr-49"
date: "2015-06-24T14:11:15"
picture: "betonmischer29.jpg"
weight: "29"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41275
- /detailsc9ce.html
imported:
- "2019"
_4images_image_id: "41275"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41275 -->
