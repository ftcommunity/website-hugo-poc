---
layout: "image"
title: "Mixer-lr-01a"
date: "2015-06-24T14:11:15"
picture: "betonmischer02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41248
- /details6d6b.html
imported:
- "2019"
_4images_image_id: "41248"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41248 -->
