---
layout: "image"
title: "Mixer-lr-12"
date: "2015-06-24T14:11:15"
picture: "betonmischer06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41252
- /detailsf0bf.html
imported:
- "2019"
_4images_image_id: "41252"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41252 -->
Nochmal, von Unten.