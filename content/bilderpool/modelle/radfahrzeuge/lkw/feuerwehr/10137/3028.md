---
layout: "comment"
hidden: true
title: "3028"
date: "2007-04-22T18:27:44"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Das Foto kriegst du besser hin. Probier's nochmal, dann wird's scharf. Notfalls benutz den Makromodus deiner Kamera (das Blumen-Symbol normalerweise).