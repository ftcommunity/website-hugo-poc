---
layout: "image"
title: "Feuerwehr"
date: "2007-04-21T14:21:45"
picture: "feuer3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10135
- /details194b-2.html
imported:
- "2019"
_4images_image_id: "10135"
_4images_cat_id: "915"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10135 -->
Hier wird ein gelber Tank angebracht