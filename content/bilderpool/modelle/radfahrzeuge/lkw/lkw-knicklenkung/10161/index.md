---
layout: "image"
title: "LKW Allrad Lenkung"
date: "2007-04-24T22:01:05"
picture: "lkw1_2.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/10161
- /details8a7b.html
imported:
- "2019"
_4images_image_id: "10161"
_4images_cat_id: "898"
_4images_user_id: "557"
_4images_image_date: "2007-04-24T22:01:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10161 -->
Fahrerhaus, zu sehen Lenkung.