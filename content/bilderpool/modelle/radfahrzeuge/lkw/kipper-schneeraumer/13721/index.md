---
layout: "image"
title: "Kipper02"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13721
- /details4f7b.html
imported:
- "2019"
_4images_image_id: "13721"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13721 -->
von vorne