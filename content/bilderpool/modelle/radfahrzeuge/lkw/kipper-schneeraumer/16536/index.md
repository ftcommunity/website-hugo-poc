---
layout: "image"
title: "Vorderansicht mit Schneefräse"
date: "2008-12-01T17:55:46"
picture: "winterdienstfahrzeug05.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16536
- /details309f.html
imported:
- "2019"
_4images_image_id: "16536"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16536 -->
