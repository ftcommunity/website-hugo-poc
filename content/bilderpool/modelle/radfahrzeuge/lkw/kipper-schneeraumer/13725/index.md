---
layout: "image"
title: "Kipper06"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13725
- /details87aa-2.html
imported:
- "2019"
_4images_image_id: "13725"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13725 -->
von hinten