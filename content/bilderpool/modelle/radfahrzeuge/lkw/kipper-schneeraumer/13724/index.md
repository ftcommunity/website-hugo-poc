---
layout: "image"
title: "Kipper05"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13724
- /detailse1c5-3.html
imported:
- "2019"
_4images_image_id: "13724"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13724 -->
Die Kipper-Mechanik