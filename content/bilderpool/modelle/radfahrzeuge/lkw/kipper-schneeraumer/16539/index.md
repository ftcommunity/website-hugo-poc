---
layout: "image"
title: "Unteransicht"
date: "2008-12-01T17:55:46"
picture: "winterdienstfahrzeug08.jpg"
weight: "14"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16539
- /details4c67-2.html
imported:
- "2019"
_4images_image_id: "16539"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16539 -->
Hier sieht man die ganze Technik.