---
layout: "image"
title: "Schneefräsenantrieb"
date: "2008-12-01T17:55:46"
picture: "winterdienstfahrzeug07.jpg"
weight: "13"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16538
- /details4d66-2.html
imported:
- "2019"
_4images_image_id: "16538"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16538 -->
