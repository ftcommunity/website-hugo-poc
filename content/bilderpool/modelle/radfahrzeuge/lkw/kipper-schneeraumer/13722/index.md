---
layout: "image"
title: "Kipper03"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- /php/details/13722
- /details5126.html
imported:
- "2019"
_4images_image_id: "13722"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13722 -->
Die Kipper-Mechanik