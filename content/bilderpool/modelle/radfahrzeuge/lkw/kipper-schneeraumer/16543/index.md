---
layout: "image"
title: "winterdienstfahrzeug12.jpg"
date: "2008-12-01T17:55:46"
picture: "winterdienstfahrzeug12.jpg"
weight: "18"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16543
- /detailsef70-2.html
imported:
- "2019"
_4images_image_id: "16543"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:46"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16543 -->
