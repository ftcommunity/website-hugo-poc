---
layout: "image"
title: "modellevonclauswludwig32.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig32.jpg"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12692
- /details8cc1-3.html
imported:
- "2019"
_4images_image_id: "12692"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12692 -->
