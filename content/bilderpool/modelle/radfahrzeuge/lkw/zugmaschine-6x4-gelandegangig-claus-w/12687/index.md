---
layout: "image"
title: "modellevonclauswludwig27.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig27.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12687
- /detailsa041.html
imported:
- "2019"
_4images_image_id: "12687"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12687 -->
