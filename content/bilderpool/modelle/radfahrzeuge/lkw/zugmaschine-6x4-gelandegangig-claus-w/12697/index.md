---
layout: "image"
title: "modellevonclauswludwig37.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig37.jpg"
weight: "15"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12697
- /details0ec9.html
imported:
- "2019"
_4images_image_id: "12697"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12697 -->
