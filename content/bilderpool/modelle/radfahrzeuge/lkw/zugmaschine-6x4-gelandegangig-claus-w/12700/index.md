---
layout: "image"
title: "modellevonclauswludwig40.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig40.jpg"
weight: "18"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12700
- /details2c96-2.html
imported:
- "2019"
_4images_image_id: "12700"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12700 -->
