---
layout: "image"
title: "modellevonclauswludwig41.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig41.jpg"
weight: "19"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12701
- /details8ed6-2.html
imported:
- "2019"
_4images_image_id: "12701"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12701 -->
