---
layout: "image"
title: "modellevonclauswludwig26.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig26.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12686
- /detailsd16d.html
imported:
- "2019"
_4images_image_id: "12686"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12686 -->
