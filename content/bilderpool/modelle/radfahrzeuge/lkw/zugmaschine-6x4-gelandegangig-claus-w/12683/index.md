---
layout: "image"
title: "modellevonclauswludwig23.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig23.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12683
- /details4fd2.html
imported:
- "2019"
_4images_image_id: "12683"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12683 -->
