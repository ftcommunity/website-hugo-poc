---
layout: "image"
title: "[5/6] Unteransicht"
date: "2009-09-11T21:40:46"
picture: "zugmaschineclaus5.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24913
- /details3d74.html
imported:
- "2019"
_4images_image_id: "24913"
_4images_cat_id: "1717"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24913 -->
