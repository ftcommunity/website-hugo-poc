---
layout: "image"
title: "modellevonclauswludwig31.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig31.jpg"
weight: "9"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12691
- /detailsec9c.html
imported:
- "2019"
_4images_image_id: "12691"
_4images_cat_id: "1147"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12691 -->
