---
layout: "image"
title: "Doppelpendelachse Aufhängung 1"
date: "2014-08-01T17:52:03"
picture: "lkjwmitpendelachse09.jpg"
weight: "9"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39126
- /details6a1f.html
imported:
- "2019"
_4images_image_id: "39126"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39126 -->
hier sieht man die befestigung der zwei Pendelachsen und ihren Drehpunkt, der auf der Antriebsachse liegt.