---
layout: "image"
title: "Aufbau Doppelachse Teil 2"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse14.jpg"
weight: "14"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39131
- /detailsde38-3.html
imported:
- "2019"
_4images_image_id: "39131"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39131 -->
Hier den zweiten Teil ohne die Aufhängung.