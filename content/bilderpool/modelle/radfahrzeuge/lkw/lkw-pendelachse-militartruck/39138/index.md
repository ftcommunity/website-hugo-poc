---
layout: "image"
title: "Aufbau Doppelachse Teil 1: Der Motor Eingebaut 3"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse21.jpg"
weight: "21"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39138
- /detailsdd97-2.html
imported:
- "2019"
_4images_image_id: "39138"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39138 -->
Zur Verstärkung werden noch zwei 2x1 Abdeckplatten angebaut.