---
layout: "overview"
title: "LKW mit Pendelachse (Militärtruck)"
date: 2020-02-22T07:51:18+01:00
legacy_id:
- /php/categories/2925
- /categories27bb.html
- /categories73df.html
- /categoriesdad6.html
- /categories5a3c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2925 --> 
Ich habe einen Militärtruck gebaut, den manoft in alten Kriegsfilmen sieht.
Als Vorbild dafür dienen zum Teil der "King of the Road" und ältere Modelle von mir.