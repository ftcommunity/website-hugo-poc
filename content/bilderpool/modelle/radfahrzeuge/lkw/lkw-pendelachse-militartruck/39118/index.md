---
layout: "image"
title: "Gesamtansicht Rechts"
date: "2014-08-01T17:52:03"
picture: "lkjwmitpendelachse01.jpg"
weight: "1"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- /php/details/39118
- /detailsa35e.html
imported:
- "2019"
_4images_image_id: "39118"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39118 -->
Hier sieht man die Charakteristischen Merkmale eines Militärtrucks aus den 40er Jahren.