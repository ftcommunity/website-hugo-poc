---
layout: "comment"
hidden: true
title: "19318"
date: "2014-08-01T18:00:59"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das ist ein schönes Modell und eine prima Dokumentation: alles beschrieben und abgebildet. Mit dem ft-Publisher könnte man die Bilder noch beschneiden und etwas aufhellen, aber das rangiert auch so schon deutlich im oberen Drittel dessen, was man hier so zu sehen bekommt.

Gruß,
Harald