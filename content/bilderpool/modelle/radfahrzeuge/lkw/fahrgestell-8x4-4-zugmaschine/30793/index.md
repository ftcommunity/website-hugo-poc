---
layout: "image"
title: "Aufhaengung 2. Achse, Blick von unten"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine09.jpg"
weight: "9"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30793
- /details6a7f.html
imported:
- "2019"
_4images_image_id: "30793"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30793 -->
Die obere Befestigung der Achsschenkel ist erheblich steifer als die untere. Das ist keine Absicht, und eigentlich unnuetz. Die Strebe 30 mit Loch, die in die Felge hineinragt, ist einfach ausgefuehrt, die Befestigung am Rahmen dagegen doppelt. Das ist eine schlechte Kopie von Achse 1, wo das gleiche Prinzip mehr Sinn ergibt.