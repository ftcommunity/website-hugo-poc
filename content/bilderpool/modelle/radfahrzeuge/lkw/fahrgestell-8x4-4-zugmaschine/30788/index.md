---
layout: "image"
title: "Rueckansicht Fahrerhaus"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine04.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30788
- /detailsccd4.html
imported:
- "2019"
_4images_image_id: "30788"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30788 -->
Das Dach ist relativ einfach gestrickt, dadurch leider recht dick geraten. Man sieht die Verjuengung des Alu-Rahmens.