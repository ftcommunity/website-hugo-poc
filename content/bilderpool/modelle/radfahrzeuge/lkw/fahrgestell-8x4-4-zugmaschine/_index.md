---
layout: "overview"
title: "Fahrgestell 8x4/4, Zugmaschine, unvollendet"
date: 2020-02-22T07:50:38+01:00
legacy_id:
- /php/categories/2299
- /categories4578.html
- /categoriesecfe.html
- /categories9528.html
- /categories210b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2299 --> 
Unvollendetes Projekt aus 2008 (?). Motorisierung mit Gleichlaufgetriebe, innovative Achsschenkellenkung mit geringem Lenkrollradius, Doppelachsantrieb mit Mittendifferential, gefederte Radaufhaengung, ansprechende Verkleidungsteile. 