---
layout: "image"
title: "Hinterer Teil, von unten gesehen"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine15.jpg"
weight: "15"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30799
- /details5032.html
imported:
- "2019"
_4images_image_id: "30799"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30799 -->
Das zeig ich gar nicht gerne ... die Achsen sind Pendelachsen, und werden aussen durch jeweils zwei Spiralfedern gehalten. Was die Federn nicht abstuetzen, geht ungefedert in den Rahmen, und zwar durch die Antriebswelle hindurch. Die ist nicht gut aufgehaengt, denn zum Mittendifferential fuehren nur die Kegelraeder Z14. Die geben zusammen mit der Breite des Mittendifferentials den Radstand vor (der zu gross ist). 

Auf den Kegelrad-Achsen sitzt bei beiden Achsen nur ein Winkeltraeger 15, weil in dessen Hohlraum noch drei Streben Platz finden muessen, die aufrecht stehen und einen Teil der Hinterachs-Last aufnehmen. Dieser Teil ist nicht gerade stabil geworden. Man haette evtl. einen Gelenkstein nehmen koennen, dessen Zange nach unten zeigt, und von dessen Nut nach aussen gehen ... aber was ist flach genug, dass es nicht gleich auf dem Boden aufsetzt, und trotzdem noch hohe Kraefte aufnehmen kann?

Aussen ist das ein bisschen schoener geloest mit Bausteinen 30 mit Loch und Kupplungsstuecken 30. Aber da muss ja auch keine Antriebskraft durch.