---
layout: "image"
title: "Aufhaengung Vorderraeder, Lenkung"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine07.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30791
- /details4c0d-2.html
imported:
- "2019"
_4images_image_id: "30791"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30791 -->
Die Doppelachslenkung mit Blattfederung. Die Raeder sind oben und unten an quer verlaufenden Streben aufgehaengt, die ein bisschen Federn. Die hintere Achse (links im bild) ist hat ihre untere Aufhaengung an den nach vorn gezogenen Streben ... das gibt extra Elastizitaet, also geringere Belastbarkeit, und ist eher aus der Not heraus entstanden. Im Hintergrund verlaeuft die Verbindungsstrebe zwischen den Achsen und die Verbindungsstrebe zum Servo.

Die gelben Streben verdeutlichen die Funktion und waren leichter zu finden ... eigentlich sollte da aber kein farbiger Akzent hin. 
