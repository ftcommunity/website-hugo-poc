---
layout: "image"
title: "Vorderer Teil ohne Fahrerhaus, Motorblock"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine19.jpg"
weight: "19"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30803
- /details2637.html
imported:
- "2019"
_4images_image_id: "30803"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30803 -->
Die Motoren passen ganz gut rein, und wie im Original auch wirklich nur, wenn der Rahmen Platz macht. Die Motoren liegen leider recht hoch, weil sie komplett ueber der Vorderachse haengen muessen. Vor der Vorderachse und unterhalb der Motoren ist deshalb noch Luft.

Der Kuehlergrill ist so gut positioniert, dass ich am liebsten wirklich etwas zu kuehlen haette.