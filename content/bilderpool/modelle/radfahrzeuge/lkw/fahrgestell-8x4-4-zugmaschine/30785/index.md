---
layout: "image"
title: "Vorderer Teil"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine01.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/30785
- /detailsbcbc.html
imported:
- "2019"
_4images_image_id: "30785"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30785 -->
Kurzes, kippbares Fahrerhaus, Beleuchtung, Kuehler, Schub-Kupplung. Das Lenkrad ist nur ein Dummy. Die Bereifung ist fast massstabsgetreu.

Alles ist krumm und schief, weil das Modell einen Umzug und zwei Jahre Herumstehen hinter sich hat.