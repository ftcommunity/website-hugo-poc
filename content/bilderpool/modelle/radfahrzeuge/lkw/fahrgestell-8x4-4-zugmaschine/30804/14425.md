---
layout: "comment"
hidden: true
title: "14425"
date: "2011-06-08T12:59:03"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Eine Möglichkeit ist immer, mit hoher Drehzahl zu laufen. Dann können die Drehmomente wieder "Rastkardan-kompatibel" niedrig sein. Erst möglichst spät, im Idealfall an den Rädern direkt, muss dann von Drehzahl auf Drehmoment untersetzt werden.

Gruß,
Stefan