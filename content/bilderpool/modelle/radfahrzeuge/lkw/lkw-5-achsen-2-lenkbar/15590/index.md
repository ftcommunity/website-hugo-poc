---
layout: "image"
title: "Draufsicht"
date: "2008-09-25T08:44:42"
picture: "lkwachsenlenkbar3.jpg"
weight: "3"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/15590
- /details8c37.html
imported:
- "2019"
_4images_image_id: "15590"
_4images_cat_id: "1429"
_4images_user_id: "808"
_4images_image_date: "2008-09-25T08:44:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15590 -->
Draufsicht