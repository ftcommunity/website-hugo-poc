---
layout: "overview"
title: "LKW 5 Achsen, 2 lenkbar"
date: 2020-02-22T07:50:21+01:00
legacy_id:
- /php/categories/1429
- /categories9bbf.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1429 --> 
Nach gut 30 Jahren Pause wieder mit ft gebastelt. Dieser LKW war eines meiner letzten Produkte aus meiner Teenie-Zeit. Zwei besonderheiten: zwei lenkbare Voderarchsen, die Ladefläche ist extrem belastbar, quasi als Rollschuh benutzbar. Eventuell lässt sich auf dem Aufbau hinter der Führerhaus ein Kran unterbringen, um Lasten auf den LKW zu beförden.