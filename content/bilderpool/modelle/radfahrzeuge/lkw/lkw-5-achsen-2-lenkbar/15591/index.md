---
layout: "image"
title: "Führerhaus..."
date: "2008-09-25T08:44:42"
picture: "lkwachsenlenkbar4.jpg"
weight: "4"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/15591
- /detailsce55.html
imported:
- "2019"
_4images_image_id: "15591"
_4images_cat_id: "1429"
_4images_user_id: "808"
_4images_image_date: "2008-09-25T08:44:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15591 -->
...mit Inneneinrichtig