---
layout: "image"
title: "Schneepflug von rechts"
date: "2015-02-15T20:38:19"
picture: "IMG_0467_b.jpg"
weight: "1"
konstrukteure: 
- "asdf"
fotografen:
- "asdf"
schlagworte: ["Schneepflug"]
uploadBy: "asdf"
license: "unknown"
legacy_id:
- /php/details/40539
- /details0a29.html
imported:
- "2019"
_4images_image_id: "40539"
_4images_cat_id: "3039"
_4images_user_id: "1490"
_4images_image_date: "2015-02-15T20:38:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40539 -->
Hallo ft community

Nach langer Zeit sind wir nun wieder dazugekommen, eines unserer Modelle zu fotografieren:
Passend zur Jahreszeit einen kleinen LKW mit Schneepflug, gebaut von Flori

Der LKW lässt sich lenken, außerdem kann der Pflug mit Schneckengetrieben angehoben und abgesenkt und zusätzlich gekippt werden.