---
layout: "image"
title: "Der AKKU"
date: "2012-08-29T20:08:53"
picture: "kleinerlkw07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35409
- /detailsae3e.html
imported:
- "2019"
_4images_image_id: "35409"
_4images_cat_id: "2627"
_4images_user_id: "1122"
_4images_image_date: "2012-08-29T20:08:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35409 -->
.