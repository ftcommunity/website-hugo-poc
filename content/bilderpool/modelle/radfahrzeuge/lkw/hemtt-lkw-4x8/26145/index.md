---
layout: "image"
title: "Kabine Draufsicht"
date: "2010-01-25T19:29:25"
picture: "DSCN0243.jpg"
weight: "12"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26145
- /details153a-2.html
imported:
- "2019"
_4images_image_id: "26145"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26145 -->
Mittig die zwei Schnecken zum Heben und Senken des Anbaugerätes