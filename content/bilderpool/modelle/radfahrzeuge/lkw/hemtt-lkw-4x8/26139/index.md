---
layout: "image"
title: "Beleuchtung vorne"
date: "2010-01-24T22:22:18"
picture: "DSCN0223.jpg"
weight: "6"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26139
- /details6159.html
imported:
- "2019"
_4images_image_id: "26139"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-24T22:22:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26139 -->
