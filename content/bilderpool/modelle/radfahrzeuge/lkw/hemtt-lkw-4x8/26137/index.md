---
layout: "image"
title: "Bergekran ausgefahren"
date: "2010-01-24T22:22:18"
picture: "DSCN0218.jpg"
weight: "4"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- /php/details/26137
- /detailsd75b-2.html
imported:
- "2019"
_4images_image_id: "26137"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-24T22:22:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26137 -->
