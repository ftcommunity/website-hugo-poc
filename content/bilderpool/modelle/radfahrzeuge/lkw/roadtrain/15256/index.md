---
layout: "image"
title: "Antrieb"
date: "2008-09-16T18:21:00"
picture: "016.jpg"
weight: "17"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15256
- /details6e31.html
imported:
- "2019"
_4images_image_id: "15256"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15256 -->
So sieht der Antrieb von unten aus. Mit der speziellen Motorhalterung von Andreas "TST" alles kein Problem.
