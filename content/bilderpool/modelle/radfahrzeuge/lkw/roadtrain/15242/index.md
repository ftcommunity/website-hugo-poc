---
layout: "image"
title: "Mein Nachbau"
date: "2008-09-16T18:20:58"
picture: "002.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15242
- /details4976.html
imported:
- "2019"
_4images_image_id: "15242"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15242 -->
