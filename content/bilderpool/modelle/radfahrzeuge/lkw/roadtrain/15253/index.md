---
layout: "image"
title: "Antrieb"
date: "2008-09-16T18:21:00"
picture: "013.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15253
- /details50c5.html
imported:
- "2019"
_4images_image_id: "15253"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15253 -->
Mit dieser Welle werden, über Differentiale, alle Hinterachsen angetrieben.
