---
layout: "image"
title: "Ansicht"
date: "2008-09-16T18:21:00"
picture: "021.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15261
- /detailsb10d.html
imported:
- "2019"
_4images_image_id: "15261"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15261 -->
