---
layout: "image"
title: "und so sieht er aus"
date: "2008-09-19T07:59:26"
picture: "031.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15293
- /details33b0.html
imported:
- "2019"
_4images_image_id: "15293"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-19T07:59:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15293 -->
So wie man ihn hier sieht wird er auch unter dem Rahmen befestigt. Eine Seite ist offen (hier die vordere).
