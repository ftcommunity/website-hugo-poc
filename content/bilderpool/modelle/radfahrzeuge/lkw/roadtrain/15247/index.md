---
layout: "image"
title: "geöffnete Motorhaube"
date: "2008-09-16T18:20:59"
picture: "007.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15247
- /details1b0c.html
imported:
- "2019"
_4images_image_id: "15247"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15247 -->
Der vordere Bereich ist gerade groß genug um einen Akkublock aufzunehmen.
