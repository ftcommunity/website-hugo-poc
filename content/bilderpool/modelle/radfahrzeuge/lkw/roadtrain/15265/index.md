---
layout: "image"
title: "Von dieser Sorte gibt es zwei"
date: "2008-09-16T18:21:00"
picture: "025.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15265
- /details7a73.html
imported:
- "2019"
_4images_image_id: "15265"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15265 -->
einer davon hat eine Anhängerkupplung. Einer hat vorn einen Dolly, so wie dieser.
