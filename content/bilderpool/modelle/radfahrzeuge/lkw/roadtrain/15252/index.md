---
layout: "image"
title: "Druck-Luftanschluss"
date: "2008-09-16T18:20:59"
picture: "012.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/15252
- /details97ce.html
imported:
- "2019"
_4images_image_id: "15252"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15252 -->
Hier werden die Schläuche angeschlossen die (später) zu den Kippermulden führen.
