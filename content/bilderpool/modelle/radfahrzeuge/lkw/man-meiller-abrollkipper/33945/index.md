---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:58:23"
picture: "5.jpg"
weight: "5"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33945
- /details6d33-2.html
imported:
- "2019"
_4images_image_id: "33945"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33945 -->
Der Abrollkipper fährt zunächst rückwärts mit abgelegtem Haken an den Container heran.
