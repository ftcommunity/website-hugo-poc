---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:33:35"
picture: "2.jpg"
weight: "2"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33942
- /details9b4a-2.html
imported:
- "2019"
_4images_image_id: "33942"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:33:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33942 -->
Hakenarm
