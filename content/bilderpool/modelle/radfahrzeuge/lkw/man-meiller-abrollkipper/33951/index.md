---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:58:24"
picture: "11.jpg"
weight: "11"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33951
- /details09a9-2.html
imported:
- "2019"
_4images_image_id: "33951"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:58:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33951 -->
Hier zu sehen die beiden Aufnahmerollen und die Verriegelung (kleine Nägel unter den beiden Bauplatten 15*30)
