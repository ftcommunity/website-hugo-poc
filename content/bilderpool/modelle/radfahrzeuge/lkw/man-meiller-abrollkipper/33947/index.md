---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:58:23"
picture: "7.jpg"
weight: "7"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33947
- /details5f47-2.html
imported:
- "2019"
_4images_image_id: "33947"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33947 -->
Nun ist es wichtig, dass der Fahrer die beiden Laufschienen des Containers über die beiden Aufnahmerollen des Abrollkippers aufnimmt.
