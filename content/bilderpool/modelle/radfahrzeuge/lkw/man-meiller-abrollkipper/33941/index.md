---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:33:34"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "Johannes Weber (Rheingauer01)"
fotografen:
- "Johannes Weber (Rheingauer01)"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- /php/details/33941
- /details035f.html
imported:
- "2019"
_4images_image_id: "33941"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:33:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33941 -->
