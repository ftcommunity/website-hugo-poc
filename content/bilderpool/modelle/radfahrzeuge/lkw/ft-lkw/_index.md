---
layout: "overview"
title: "Ft LKW"
date: 2020-02-22T07:50:28+01:00
legacy_id:
- /php/categories/1956
- /categoriesab8a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1956 --> 
LKW mit Sound und Light, Front  und Rückscheinwerfer selbstgebaut mit LEDs. Unter der Ladefläche ist Platz für Accu, Kabel und weitere Module.