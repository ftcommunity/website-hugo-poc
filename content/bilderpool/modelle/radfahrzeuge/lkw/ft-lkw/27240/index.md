---
layout: "image"
title: "LKW"
date: "2010-05-16T12:29:39"
picture: "ftlkw3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "michelino"
license: "unknown"
legacy_id:
- /php/details/27240
- /detailsbba1-2.html
imported:
- "2019"
_4images_image_id: "27240"
_4images_cat_id: "1956"
_4images_user_id: "876"
_4images_image_date: "2010-05-16T12:29:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27240 -->
Fahrerhaus mit LED- Frontscheinwerfer ( selbstgebaut)