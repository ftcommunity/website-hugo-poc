---
layout: "image"
title: "LKW4x6-01.JPG"
date: "2004-02-15T20:20:31"
picture: "LKW4x6-01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2110
- /details3cde.html
imported:
- "2019"
_4images_image_id: "2110"
_4images_cat_id: "430"
_4images_user_id: "4"
_4images_image_date: "2004-02-15T20:20:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2110 -->
Das LKW-Chassis von PK, ausgestellt in Veghel 2004. Alle Achsen sind gefedert und geländegängig.
