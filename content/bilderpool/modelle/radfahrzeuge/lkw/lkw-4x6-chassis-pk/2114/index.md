---
layout: "image"
title: "LKW4x6-12.JPG"
date: "2004-02-15T20:20:31"
picture: "LKW4x6-12.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2114
- /details817e.html
imported:
- "2019"
_4images_image_id: "2114"
_4images_cat_id: "430"
_4images_user_id: "4"
_4images_image_date: "2004-02-15T20:20:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2114 -->
