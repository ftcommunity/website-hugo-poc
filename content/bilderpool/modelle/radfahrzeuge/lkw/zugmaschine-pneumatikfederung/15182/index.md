---
layout: "image"
title: "Zugmaschine - v.1 - detail der Federung"
date: "2008-09-05T19:36:54"
picture: "IMG_1468_800.jpg"
weight: "3"
konstrukteure: 
- ".zeuz."
fotografen:
- ".zeuz."
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15182
- /detailse1f5.html
imported:
- "2019"
_4images_image_id: "15182"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T19:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15182 -->
Auf diesem Bild gibt es zwei nennenswerte Beobachtungen:
Der Anschlusstopfen des rechten Zylinders ist leider Abgebrochen
Da die pneumatische Federung noch nicht aktiv ist wurde eine Hilfsfeder mittig eingebaut.
