---
layout: "image"
title: "Zugmaschine - v.2 - Unten"
date: "2008-09-06T09:04:28"
picture: "DSCN0065_800.jpg"
weight: "9"
konstrukteure: 
- ".zeuz."
fotografen:
- ".zeuz."
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- /php/details/15188
- /details7042.html
imported:
- "2019"
_4images_image_id: "15188"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15188 -->
So schaut das doch schon viel besser aus.
