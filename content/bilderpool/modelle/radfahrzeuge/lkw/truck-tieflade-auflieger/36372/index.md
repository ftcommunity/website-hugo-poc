---
layout: "image"
title: "Truck mit Tieflade-Auflieger"
date: "2012-12-30T12:38:31"
picture: "truck1.jpg"
weight: "1"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/36372
- /details424b.html
imported:
- "2019"
_4images_image_id: "36372"
_4images_cat_id: "2701"
_4images_user_id: "1419"
_4images_image_date: "2012-12-30T12:38:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36372 -->
Noch aus den 70ern beim Stöbern in den alten Dias wiedergefunden, Leider nicht ganz die Tiefenschärfe wie benötigt. 
In diesem Modell steckte damals fast mein ganzes ft, Daher auch die Ergänzungen aus Papier für den Tank und die "Ketten" am Kuhfänger.
Unter dem Papier"Trafo" war das ft-Netzteil versteckt, Das Kabel kann man im Hintergrund auch sehen. 
