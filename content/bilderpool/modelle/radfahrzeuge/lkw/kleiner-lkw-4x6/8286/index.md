---
layout: "image"
title: "LKW 4x6 4"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8286
- /detailscbce.html
imported:
- "2019"
_4images_image_id: "8286"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8286 -->
Natürlich lassen sich die Türen öffnen!