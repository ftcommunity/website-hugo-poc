---
layout: "image"
title: "LKW 4x6 1"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8283
- /details3637-2.html
imported:
- "2019"
_4images_image_id: "8283"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8283 -->
Hier ein kleines ferngesteuertes vollgefedertes LKW-Fahrgestell 4x6 mit motorisiertem Antrieb, motorisierter Lenkung und Akku-Pack. Auf den ersten Blick nix Besonderes...

..., man beachte aber bitte den sehr kompakten Vorderwagen mit sehr flach bauender gefederter Vorderachse und darüber angeordnetem Power-Motor. Recht stolz bin ich auch auf die "versteckte" Unterbringung des Akku-Packs im Führerhaus. Somit sind alle drei wesentlichen Antriebskomponenten (2 Motoren, Akku-Pack) im Vorderwagen untergebracht, so dass der komplette hintere Teil des Fahrzeugs für Aufbauten verwendet werden kann.