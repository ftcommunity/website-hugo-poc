---
layout: "image"
title: "LKW 4x6 6"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_10.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/8288
- /detailsf64f.html
imported:
- "2019"
_4images_image_id: "8288"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8288 -->
Hier die Vorderachse im Detail. Der Mini-Motor wird nur von einer Strebe 30 gehalten und federt mit der Achse mit.

Die Ansicht täuscht: Die beiden Spriralfedern der Vorderachse drücken in Nominallage das Fahrzeug nach oben, und zwischen den beiden Motoren verbleibt genügend Platz zum federn. Die Strebe 30 liegt also nicht am Power-Motor an.