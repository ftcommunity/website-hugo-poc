---
layout: "image"
title: "von unten"
date: "2015-10-06T17:22:16"
picture: "DSC08202-sc.jpg"
weight: "3"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- /php/details/42056
- /detailsf9ea-3.html
imported:
- "2019"
_4images_image_id: "42056"
_4images_cat_id: "3128"
_4images_user_id: "2488"
_4images_image_date: "2015-10-06T17:22:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42056 -->
hierbei gut zu sehen der Antrieb der beiden Hinterachsen über den Traktormotor
