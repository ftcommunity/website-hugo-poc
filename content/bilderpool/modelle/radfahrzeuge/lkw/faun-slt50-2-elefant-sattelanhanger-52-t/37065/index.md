---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 31"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert31.jpg"
weight: "31"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37065
- /details1c76-3.html
imported:
- "2019"
_4images_image_id: "37065"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37065 -->
Damit das ganse Immer recht unter das Chassis bleibt: Abstützung