---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 15"
date: "2013-06-11T22:27:47"
picture: "sattelanhaengerkaessbohrert04.jpg"
weight: "55"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37089
- /details6b6c.html
imported:
- "2019"
_4images_image_id: "37089"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-11T22:27:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37089 -->
....und von unten.