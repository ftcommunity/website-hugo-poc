---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 3"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37037
- /detailse02f.html
imported:
- "2019"
_4images_image_id: "37037"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37037 -->
Die beide rechte Hinterräder.
Die 17 Räder 83x32 sind von Robbe: #34120010 und passen genau auf die 43er Felge #31879