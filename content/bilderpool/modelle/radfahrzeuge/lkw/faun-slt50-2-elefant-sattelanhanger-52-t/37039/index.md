---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 5"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37039
- /detailsd0f0.html
imported:
- "2019"
_4images_image_id: "37039"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37039 -->
Die Beide gelenkte rechter Voderräder