---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 17"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert17.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37051
- /detailsf2c6.html
imported:
- "2019"
_4images_image_id: "37051"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37051 -->
