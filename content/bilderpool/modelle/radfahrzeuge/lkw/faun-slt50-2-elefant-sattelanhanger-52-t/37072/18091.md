---
layout: "comment"
hidden: true
title: "18091"
date: "2013-06-11T17:10:09"
uploadBy:
- "pk"
license: "unknown"
imported:
- "2019"
---
Der Sattelanhänger ist hydraulisch um +100mm/ -80mm in der Höhe verstelbar.
Wen Du dir mal auf  "Page 11" der bauanleitung von das Revellmodell, die Bilder 59 bis 64 ansiehst, siehst Du das die Hydraulikcylinder #127 + #128(bild 62) über Winkellenker der ladefläche auf die Federpackete, hoch drücken.

Weitere foto's folgen.

Gruß,
Peter