---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 13"
date: "2013-06-11T22:27:47"
picture: "sattelanhaengerkaessbohrert09.jpg"
weight: "60"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37094
- /details8ff0-3.html
imported:
- "2019"
_4images_image_id: "37094"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-11T22:27:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37094 -->
...am Ramen sind auch 2