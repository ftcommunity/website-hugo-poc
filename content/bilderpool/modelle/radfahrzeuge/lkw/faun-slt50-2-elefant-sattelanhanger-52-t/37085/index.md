---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 45"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert51.jpg"
weight: "51"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37085
- /detailsa6ec-2.html
imported:
- "2019"
_4images_image_id: "37085"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37085 -->
