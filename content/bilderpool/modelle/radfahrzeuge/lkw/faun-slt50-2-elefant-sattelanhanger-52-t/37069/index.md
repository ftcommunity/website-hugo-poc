---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 35"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert35.jpg"
weight: "35"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/37069
- /details91db-3.html
imported:
- "2019"
_4images_image_id: "37069"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37069 -->
Ferder:
2 x #130593 - Rastachse mit Platte
2 x #36913 I-Strebe 45 schwarz mit Loch
4 x #36923 I-Strebe 75 schwarz mit Loch
2 x unterlegscheiben
2 x m4 Mutter