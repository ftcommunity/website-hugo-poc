---
layout: "image"
title: "LKW Rampe hoch"
date: "2006-12-20T00:22:35"
picture: "lkwtransporter04.jpg"
weight: "4"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/7966
- /details80b2.html
imported:
- "2019"
_4images_image_id: "7966"
_4images_cat_id: "743"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:22:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7966 -->
LKW Rampe hoch
