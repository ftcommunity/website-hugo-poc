---
layout: "image"
title: "Anhänger ohne LKW, mit Rampe"
date: "2006-12-20T00:22:35"
picture: "lkwtransporter09.jpg"
weight: "9"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/7971
- /details26d7.html
imported:
- "2019"
_4images_image_id: "7971"
_4images_cat_id: "743"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:22:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7971 -->
Anhänger ohne LKW, mit Rampe
