---
layout: "image"
title: "PKW-Transporter"
date: "2007-07-20T16:55:57"
picture: "pkwtrans6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11160
- /details3e94.html
imported:
- "2019"
_4images_image_id: "11160"
_4images_cat_id: "1010"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T16:55:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11160 -->
und hier der Transporter von hinten.