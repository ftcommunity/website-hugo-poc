---
layout: "image"
title: "0x6Tr10.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr10.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2022
- /details658b.html
imported:
- "2019"
_4images_image_id: "2022"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2022 -->
Die Zugmaschine von unten. Motoren hat sie nicht, dafür ist sie aber voll geländegängig und hinten gefedert. Der dicke Kondensator wird für die Blinkelektronik gebraucht, die in der Fotodose versteckt ist.