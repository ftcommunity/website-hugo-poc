---
layout: "image"
title: "0x6Tr40.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr40.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2023
- /details4a4f-2.html
imported:
- "2019"
_4images_image_id: "2023"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2023 -->
Die vordere Stütze des Aufliegers. Die Knie-Mechanik sorgt dafür, dass das Gewicht im ausgefahrenen Zustand nicht auf dem Hubgetriebe lastet.