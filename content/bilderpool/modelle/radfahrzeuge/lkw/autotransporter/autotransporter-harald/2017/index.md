---
layout: "image"
title: "0x6Tr02.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr02.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2017
- /details262a.html
imported:
- "2019"
_4images_image_id: "2017"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2017 -->
Hab mal ein paar ältere Fotos digital abfotografiert. Wenn das Modell schief und verzogen aussieht, dann liegt es daran, dass ich das Foto etwas krumm einspannen mußte, damit der Blitz nicht die Bildmitte versaut.
