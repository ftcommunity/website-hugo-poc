---
layout: "image"
title: "0x6Tr50.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr50.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2025
- /details4602.html
imported:
- "2019"
_4images_image_id: "2025"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2025 -->
Die Verriegelung der oberen Laderampe.