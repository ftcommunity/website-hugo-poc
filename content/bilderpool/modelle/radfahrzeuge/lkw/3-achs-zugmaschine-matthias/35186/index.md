---
layout: "image"
title: "05 Unterseite"
date: "2012-07-17T16:34:37"
picture: "zugmaschine5.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35186
- /details557a-2.html
imported:
- "2019"
_4images_image_id: "35186"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35186 -->
Unter dem Führerhaus ist noch viel Platz, um einen Akku oä. zu verbauen.