---
layout: "image"
title: "04 Führerhaus"
date: "2012-07-17T16:34:37"
picture: "zugmaschine4.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/35185
- /details3991.html
imported:
- "2019"
_4images_image_id: "35185"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35185 -->
Die Führerkabine mit den Türen des Original