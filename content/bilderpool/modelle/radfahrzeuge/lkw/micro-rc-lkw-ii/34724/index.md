---
layout: "image"
title: "Micro-RC-LKW II 03"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34724
- /detailsdc7c.html
imported:
- "2019"
_4images_image_id: "34724"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34724 -->
Das Servo sitzt komplett im Führerhaus, so dass im Aufbau Platz für Empfänger und Akku bleiben.

Der Spoiler auf dem Dach ist nur Show und hat keinen technischen Sinn. Die gelbe Platte könnte auch flach auf dem Dach liegen, denn das Servo passt bündig darunter. Das Fahrzeug könnte also ohne den Spoiler noch flacher sein. Ich habe ihn verbaut, damit die Optik des Dachverlaufs aufgelockerter ist.