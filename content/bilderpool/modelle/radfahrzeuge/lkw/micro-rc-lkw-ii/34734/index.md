---
layout: "image"
title: "Micro-RC-LKW II 13"
date: "2012-03-31T23:32:00"
picture: "Micro-RC-LKW_II_13.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34734
- /details090b.html
imported:
- "2019"
_4images_image_id: "34734"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34734 -->
Die Optik des Fahrzeugs leidet durch das Loch im Dach kaum. Der Spoiler kaschiert diesen "Makel" ein wenig.