---
layout: "image"
title: "Micro-RC-LKW II 08"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_08.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34729
- /details8c91.html
imported:
- "2019"
_4images_image_id: "34729"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34729 -->
Die Vorderachse mit Achsschenkel-Lenkung. Details dazu im nächsten Bild.