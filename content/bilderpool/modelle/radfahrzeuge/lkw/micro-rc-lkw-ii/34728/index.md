---
layout: "image"
title: "Micro-RC-LKW II 07"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_07.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34728
- /detailsdf9b.html
imported:
- "2019"
_4images_image_id: "34728"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34728 -->
Der Antrieb des XS-Motors geht direkt auf die Hinterachse, was das Fahrzeug schön schnell macht. Sogar kleine Drifts sind möglich.

Ein Stecker am Motor musste seiner Hülle beraubt werden, weil er sonst nirgends mehr gepasst hätte.