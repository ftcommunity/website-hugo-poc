---
layout: "image"
title: "Micro-RC-LKW II 09"
date: "2012-03-31T23:31:59"
picture: "Micro-RC-LKW_II_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34730
- /details4d92.html
imported:
- "2019"
_4images_image_id: "34730"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34730 -->
Diese kleine Achsschenkel-Lenkung ist das Ergebnis wochenlanger Tüftelei ... Hier ist der maximale Lenkausschlag nach rechts dargestellt.

Zur Erklärung:

Die kleine Junior-Bauplatte sitzt quer im Fahrzeug, und durch die mittleren Löcher sind je eine Rastachse geführt. Die beiden Bausteine 7,5 halten unten nur durch die Rastnasen der Achsen, was super funktioniert. Platz für ein Verriegelungselement war eh nicht da.

Um zu verhindern, dass sich die Rastachsen verdrehen und die Bausteine 7,5 abfallen und um die Drehbewegung des Servos auf die Lenkung überhaupt zu ermöglichen, sitzt auf jeder Achse ein (Statik-) Mitnehmer 31712, dessen Arm durch die Klemmbuche und die Riegelscheibe geführt ist.

Auf der anderen Seite der Bausteine 7,5 sitzt eine Statikstrebe 15 als Spurstange.

Die ganze Achse ist recht wackelig und hat viel Spiel, funktioniert im Betrieb aber wirklich tadellos und ist überraschend stabil! Einmal sauber justiert, ist auch der Geradeauslauf perfekt. Die Räder sitzen ja leicht hinter den Drehachsen der Achsschenkel, daher ziehen sie sich bei Geradeausfahrt immer automatisch gerade.