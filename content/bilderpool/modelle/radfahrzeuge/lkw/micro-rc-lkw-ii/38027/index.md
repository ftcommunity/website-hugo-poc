---
layout: "image"
title: "Micro-RC-LKW II 18"
date: "2014-01-08T23:15:05"
picture: "DSC09200.jpg"
weight: "18"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38027
- /details9e54-2.html
imported:
- "2019"
_4images_image_id: "38027"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38027 -->
