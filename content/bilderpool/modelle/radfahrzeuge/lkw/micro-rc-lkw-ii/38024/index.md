---
layout: "image"
title: "Micro-RC-LKW II 15"
date: "2014-01-08T23:15:05"
picture: "DSC09193.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38024
- /details620f.html
imported:
- "2019"
_4images_image_id: "38024"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38024 -->
Der 9V-Akku passt genau über/neben dem Empfänger. Es wäre kein Millimeter mehr Platz gewesen!

Leider lässt sich viel unangenehmes Kabelgewirr bei so kleinen Modellen kaum vermeiden ...