---
layout: "image"
title: "Micro-RC-LKW II 19"
date: "2014-01-08T23:15:05"
picture: "DSC09203.jpg"
weight: "19"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/38028
- /detailsc685-3.html
imported:
- "2019"
_4images_image_id: "38028"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38028 -->
Wer das Modell nachbauen möchte, findet hier die entsprechenden Baugruppen.