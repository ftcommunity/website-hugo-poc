---
layout: "image"
title: "und gearbeitet wird hier auch ..."
date: "2008-01-26T17:11:08"
picture: "DSCN2064.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13438
- /details4306.html
imported:
- "2019"
_4images_image_id: "13438"
_4images_cat_id: "1220"
_4images_user_id: "184"
_4images_image_date: "2008-01-26T17:11:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13438 -->
