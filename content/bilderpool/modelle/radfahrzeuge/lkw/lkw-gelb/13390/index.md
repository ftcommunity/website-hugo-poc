---
layout: "image"
title: "LKW mit Anhänger"
date: "2008-01-25T15:36:14"
picture: "DSCN2048.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13390
- /details00c1.html
imported:
- "2019"
_4images_image_id: "13390"
_4images_cat_id: "1220"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13390 -->
Dieser hier hat eine feste Ladefläche.
