---
layout: "image"
title: "Drehschemel"
date: "2008-01-26T17:11:08"
picture: "DSCN2065.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/13437
- /details799e-2.html
imported:
- "2019"
_4images_image_id: "13437"
_4images_cat_id: "1220"
_4images_user_id: "184"
_4images_image_date: "2008-01-26T17:11:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13437 -->
