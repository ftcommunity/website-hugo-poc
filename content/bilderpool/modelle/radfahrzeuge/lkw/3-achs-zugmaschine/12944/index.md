---
layout: "image"
title: "Antrieb"
date: "2007-11-30T19:45:05"
picture: "achszugmaschine4.jpg"
weight: "4"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12944
- /detailsd80c.html
imported:
- "2019"
_4images_image_id: "12944"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12944 -->
Der Antrieb ist etwas ungewöhnlich... Das liegt daran, dass ich beide hinteren Achsen antreiben, aber trotzdem die Proportionen des Modells so halbwegs realistisch halten wollte. Unter dem Führerhaus des Modells befinden sich 4 senkrecht eingebaute S-Motoren. Zwei davon treiben je ein Rad der ersten Hinterachse an, die beiden anderen per Kette die zweite Hinterachse.