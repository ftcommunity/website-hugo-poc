---
layout: "image"
title: "Lenkung"
date: "2007-11-30T19:45:05"
picture: "achszugmaschine5.jpg"
weight: "5"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12945
- /detailsa002-2.html
imported:
- "2019"
_4images_image_id: "12945"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12945 -->
Da ich eine RC-Fernsteuerung verwende, wird hier ein RC-Servo zur Anlenkung der Vorderachse verwendet. Bei Verwendung des IR Control Sets müßte da halt ein S-Motor hin.