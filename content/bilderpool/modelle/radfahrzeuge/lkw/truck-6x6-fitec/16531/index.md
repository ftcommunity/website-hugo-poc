---
layout: "image"
title: "Federung"
date: "2008-11-30T00:35:42"
picture: "6x6Truck2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/16531
- /detailsdb8c-2.html
imported:
- "2019"
_4images_image_id: "16531"
_4images_cat_id: "1494"
_4images_user_id: "456"
_4images_image_date: "2008-11-30T00:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16531 -->
Hier sieht man in wie das ganze mal federn soll.
Die Räder, auf deren Achsen sich ein Z30 befindet, werden wiederum von einem Z30 angetrieben, das ganze ist dan mit Gelenken gelagert, sodass sich jedes rad hoch- und runterbewegen kann.
Ich werde wahrscheinlich keine Federn benutzen sondern Pneumatikzylinder.
Der Antriebsmotor soll dann wie früher im Traktor einen Kompressor antreiben, der sich bei genug Luft abkoppelt. Das ist dann wie beim richtigen LKW - Man schaltet den Motor ein, und langsam hebt die Federung den LKW ein wenig in die Luft.
