---
layout: "image"
title: "PKW"
date: 2023-06-09T17:06:40+02:00
picture: "oldtimer-27.jpeg"
weight: "27"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Hier gut zu sehen der Koffer über dem Motorraum.
