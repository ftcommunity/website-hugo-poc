---
layout: "image"
title: "PKW"
date: 2023-06-09T17:06:27+02:00
picture: "oldtimer-37.jpeg"
weight: "37"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Fahrgestell und Rahmen