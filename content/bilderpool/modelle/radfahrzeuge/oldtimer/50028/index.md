---
layout: "image"
title: "PKW"
date: 2023-06-09T17:06:35+02:00
picture: "oldtimer-30.jpeg"
weight: "30"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
