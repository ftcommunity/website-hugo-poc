---
layout: "overview"
title: "Oldtimer"
date: 2023-06-09T17:06:18+02:00
---

### Der Anfang vom jetzigen Ende

Das Benzin kaufte man in der Apotheke und die Felgen waren aus Holz. Drum nannte man diese Fahrzeuge "Benzinkutschen". Hier jetzt zwei Modelle aus nur fischertechnik-Teilen, nichts geklebt, kein 3D Druck, auch keine Aufkleber und alle Teile sind noch in der Teile-Liste.

Die Modelle sehen trotz allem sehr realistisch aus. Manche behaupten, das sei mit Fischertechnik nicht möglich; jetzt kann sich jeder seine Meinung selber bilden. 

Noch was zum LKW: der besteht aus 577 Teilen. 
