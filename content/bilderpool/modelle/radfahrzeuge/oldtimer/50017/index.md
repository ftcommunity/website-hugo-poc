---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:22+02:00
picture: "oldtimer-06.jpeg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
