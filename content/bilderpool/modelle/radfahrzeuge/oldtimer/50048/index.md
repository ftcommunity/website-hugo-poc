---
layout: "image"
title: "LKW"
date: 2023-06-09T17:07:01+02:00
picture: "oldtimer-12.jpeg"
weight: "12"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---


Auch die Schraubfedern sind beim Original vorhanden.
