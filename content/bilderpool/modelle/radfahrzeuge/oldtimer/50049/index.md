---
layout: "image"
title: "Bild des Originals"
date: 2023-06-09T17:07:02+02:00
picture: "oldtimer-11.jpeg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

