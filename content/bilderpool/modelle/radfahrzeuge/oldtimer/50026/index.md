---
layout: "image"
title: "Vorderhaus"
date: 2023-06-09T17:06:33+02:00
picture: "oldtimer-32.jpeg"
weight: "32"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

