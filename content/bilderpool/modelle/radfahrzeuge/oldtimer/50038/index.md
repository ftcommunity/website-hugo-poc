---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:48+02:00
picture: "oldtimer-21.jpeg"
weight: "21"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
Es gab damals nur zwei Modelle: eins mit 1,5 Tonnen Zuladung und eine Version mit 2 Tonnen Zuladung.

