---
layout: "image"
title: "PKW"
date: 2023-06-09T17:06:45+02:00
picture: "oldtimer-23.jpeg"
weight: "23"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Eine Motorkutsche als PKW auch aus dem Zeitraum von ca. 1884 bis 1889. Hier verbaute man kein Lenkrad, stattdessen einen Lenkhebel. Auch hier wurden Lederriemen zum Antrieb eingesetzt. Das war mit Originalteilen von fischertechnik nicht möglich. Deswegen setze ich eine Kardanwelle ein. Auch ein Differential war vorhanden logischerweise, nur kein Planetengetriebe; im Grunde genommen wie heute auch noch. 

Der Chauffeur hieß damals Kutscher und saß auf dem Kutscherbock. In der Mitte saßen die feinen Leute wie bei der Postkutsche.
