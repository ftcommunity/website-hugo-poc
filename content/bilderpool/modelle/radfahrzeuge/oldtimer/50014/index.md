---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:18+02:00
picture: "oldtimer-09.jpeg"
weight: "9"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
Das Lenkrad dreht mit.
