---
layout: "image"
title: "LKW"
date: 2023-06-09T17:07:04+02:00
picture: "oldtimer-10.jpeg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Wie beim Original hat er zwei Planetengetriebe.
