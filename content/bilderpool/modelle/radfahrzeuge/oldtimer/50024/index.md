---
layout: "image"
title: "Vorderhaus"
date: 2023-06-09T17:06:30+02:00
picture: "oldtimer-34.jpeg"
weight: "34"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

