---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:52+02:00
picture: "oldtimer-19.jpeg"
weight: "19"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
Der Antrieb funktionierte über Lederriemen über ein Differential an die Hinterachse. Die Bremse funktionierte auch über den Lederiemen und war nur ein Handbremshebel.


