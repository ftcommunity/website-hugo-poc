---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:47+02:00
picture: "oldtimer-22.jpeg"
weight: "22"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
Natürlich gab es keinen Kipper. Die waren schon stolz, wenn sie eine Überdachung hatten.

