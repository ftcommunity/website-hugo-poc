---
layout: "image"
title: "PKW"
date: 2023-06-09T17:06:28+02:00
picture: "oldtimer-36.jpeg"
weight: "36"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Batteriehalter"]
uploadBy: "Website-Team"
license: "unknown"
---

Als Stromversorgung habe ich einen Batteriehalter mit Schalter (135719) eingesetzt im Fußraum.