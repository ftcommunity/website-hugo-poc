---
layout: "image"
title: "Bild des Originals"
date: 2023-06-09T17:06:53+02:00
picture: "oldtimer-18.jpeg"
weight: "18"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

