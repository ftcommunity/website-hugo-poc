---
layout: "image"
title: "Vorderhaus"
date: 2023-06-09T17:06:32+02:00
picture: "oldtimer-33.jpeg"
weight: "33"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Der Vorderhaus der beiden Fahrzeuge ist fast gleich.
