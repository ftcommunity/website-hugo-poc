---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:26+02:00
picture: "oldtimer-38.jpeg"
weight: "38"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Hier noch mal die Hinterachse vom LKW mit Planetengetriebe.