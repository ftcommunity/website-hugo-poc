---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:49+02:00
picture: "oldtimer-20.jpeg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---
Das kleine Rad neben dem Lenkrad war zur Regelung der Zündung je nach Belastung.
