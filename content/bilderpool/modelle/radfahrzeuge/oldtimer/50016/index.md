---
layout: "image"
title: "LKW"
date: 2023-06-09T17:06:21+02:00
picture: "oldtimer-07.jpeg"
weight: "7"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Der Empfänger ist kopfüber unter der Sitzbank eingebaut.
