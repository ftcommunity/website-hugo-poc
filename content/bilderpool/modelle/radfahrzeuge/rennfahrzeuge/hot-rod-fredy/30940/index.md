---
layout: "image"
title: "Lenkung"
date: "2011-06-26T16:15:42"
picture: "hotrod07.jpg"
weight: "7"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30940
- /details6b60.html
imported:
- "2019"
_4images_image_id: "30940"
_4images_cat_id: "2312"
_4images_user_id: "453"
_4images_image_date: "2011-06-26T16:15:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30940 -->
