---
layout: "image"
title: "Hot Rod"
date: "2011-06-26T16:15:42"
picture: "hotrod01.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30934
- /detailseaec-2.html
imported:
- "2019"
_4images_image_id: "30934"
_4images_cat_id: "2312"
_4images_user_id: "453"
_4images_image_date: "2011-06-26T16:15:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30934 -->
http://www.youtube.com/watch?v=Ryi0bVb380k
