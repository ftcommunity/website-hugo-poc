---
layout: "image"
title: "Sicht von unten: Antrieb und Servo"
date: "2013-12-02T12:57:36"
picture: "funflitzer3.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37891
- /details0ded.html
imported:
- "2019"
_4images_image_id: "37891"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37891 -->
Das Servo maximiert den Ausschlag der Lenkung. Zusammen mit dem kleinen Radstand hat das Modell einen sehr kleinen Wendekreis. Der Hinterradantrieb wird durch den darüberliegenden Motor mit sehr schneller Drehzahl angetrieben. Damit beschleunigt das Modell recht dramatisch innerhalb 2 Meter auf Höchstgeschwindigkeit.