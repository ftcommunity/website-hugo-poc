---
layout: "image"
title: "F1a-01.jpg"
date: "2008-05-23T17:54:36"
picture: "F1a-01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14570
- /details6337.html
imported:
- "2019"
_4images_image_id: "14570"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T17:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14570 -->
Er ist zwar noch nicht ganz fertig, aber ich musste das kurzzeitig vorhandene "Büchsenlicht" zwischen den Regenwolken ausnutzen.
