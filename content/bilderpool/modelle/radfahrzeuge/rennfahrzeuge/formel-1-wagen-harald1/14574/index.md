---
layout: "image"
title: "F1a-12.JPG"
date: "2008-05-23T18:02:28"
picture: "F1a-12.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14574
- /details1a09.html
imported:
- "2019"
_4images_image_id: "14574"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:02:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14574 -->
Das Chassis ist eigentlich für einen PKW gedacht, aber vielleicht merkt's ja immer noch keiner :-)

Die Hinterachse hat Einzelradfederung. Der Antrieb geht durch die Gelenke hindurch und mit 1:3-Untersetzung auf die Räder. Die Antriebsschnecke ist "schwimmend" gelagert, d.h. sie kann nicht nach vorn oder hinten aber ansonsten könnte sie ums Differenzial herum kreisen.
