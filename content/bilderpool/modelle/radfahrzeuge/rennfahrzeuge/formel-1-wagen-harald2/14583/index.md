---
layout: "image"
title: "F1b-11.JPG"
date: "2008-05-23T18:21:17"
picture: "F1b-11.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14583
- /details4a52.html
imported:
- "2019"
_4images_image_id: "14583"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:21:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14583 -->
Die Schneckenmutter dreht und kippelt noch. Da fehlt noch eine Führungsschiene oder ähnliches.
