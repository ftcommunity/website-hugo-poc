---
layout: "image"
title: "F1b-03.JPG"
date: "2008-05-23T18:18:59"
picture: "F1b-03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14581
- /details7c78-2.html
imported:
- "2019"
_4images_image_id: "14581"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:18:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14581 -->
Das gute Stück von der anderen Seite. Die Räder sind allesamt per S-Streben aufgehängt.
