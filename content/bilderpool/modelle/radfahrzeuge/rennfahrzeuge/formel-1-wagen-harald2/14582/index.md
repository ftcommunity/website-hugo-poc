---
layout: "image"
title: "F1b-10.JPG"
date: "2008-05-23T18:19:41"
picture: "F1b-10.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/14582
- /details42a9.html
imported:
- "2019"
_4images_image_id: "14582"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:19:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14582 -->
