---
layout: "image"
title: "Nase, Lenkung, Aufhängung"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari02.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38652
- /details80a4.html
imported:
- "2019"
_4images_image_id: "38652"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38652 -->
ich weiß nicht, ob es auf den Bildern einen ähnlichen Eindruck vermittelt wie beim realen Modell.
Die Linien sehen sehr dynamisch aus, die Achsaufhängung irgendwie "organisch", da mehrfach geschwungen.
Das ganze ist dann noch ein bisschen verziert mit Aerodynamik-Schnickschnack: Frontspoiler, Luftleitbleche....