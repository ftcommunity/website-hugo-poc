---
layout: "image"
title: "Front Detail"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari04.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38654
- /detailse6c4.html
imported:
- "2019"
_4images_image_id: "38654"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38654 -->
