---
layout: "image"
title: "Spreizung"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari08.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38658
- /detailsdff0.html
imported:
- "2019"
_4images_image_id: "38658"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38658 -->
Eigentlich ging es mir am Anfang nur darum, eine Lenkung mit Spreizung auszuprobieren. Mein Ziel war es, durch die Spreizung den Drehpunkt in den Mittelpunkt der Radaufstandsfläche zu bekommen (siehe Schnittpunkt der blauen Linie mit Reifenunterseite), was zu einem neutralen Lenkverhalten führt. Das ist mit Fischertechnik nicht so leicht zu realisieren (ich verwende hier ja auch ein paar Fremdteile).
Die Spreizung hat auch noch ein paar andere Effekte und ist in modernen PKW durchaus üblich.

Am Besten mal bei Wikipedia nachlesen: http://de.wikipedia.org/wiki/Lenkung
