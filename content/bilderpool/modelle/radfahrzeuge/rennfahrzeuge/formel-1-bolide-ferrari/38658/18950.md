---
layout: "comment"
hidden: true
title: "18950"
date: "2014-04-23T18:55:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
... oder beim "A-Team" http://ftcommunity.de/details.php?image_id=3511

Aber ich sehe, du hast die Achse nicht nur in Querrichtung schräg gestellt, sondern auch in Längsrichtung. Das gibt noch ein paar Kräfte mehr, die am Lenkverhalten mitwirken.

Mir fehlt da noch eine Strebe unten quer von Rad zu Rad. Da biegt sich doch die Radaufhängung beim "Einfedern" und dreht auf dem Zapfen des Winkel 60 unter der schwarzen Platte, oder?

Ein sehr gelungenes Modell!

Gruß,
Harald