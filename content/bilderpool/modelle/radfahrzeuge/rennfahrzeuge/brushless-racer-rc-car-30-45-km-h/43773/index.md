---
layout: "image"
title: "Ein erster Versuch einer Federung der Vorderachse."
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik3.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43773
- /details913b.html
imported:
- "2019"
_4images_image_id: "43773"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43773 -->
Platte unten und Federn oben - wir wollten den Wagen flach halten - würde sogar auch auf dem Kopf noch fahren. Hat Vorteile, wenn er mal wieder wegen der Straßenlage abhebt oder in die Büsche geht.