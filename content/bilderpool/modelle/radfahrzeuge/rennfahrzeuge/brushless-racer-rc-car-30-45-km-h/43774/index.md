---
layout: "image"
title: "Die Motoren bauen sehr flach"
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik4.jpg"
weight: "4"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43774
- /details3b8e.html
imported:
- "2019"
_4images_image_id: "43774"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43774 -->
