---
layout: "image"
title: "Nabenmotor."
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43772
- /detailsff41.html
imported:
- "2019"
_4images_image_id: "43772"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43772 -->
Eine halb abgesächte Nabe, aufgeschraubt auf den Motor reicht, um den Reifen zu halten - braucht keinen Kleber. Der Motor ist damit zur hälfte noch unter dem Reifen, welcher alle drehenden Teile sehr schön schützt. Auch im Dreck macht das kein Problem bisher.