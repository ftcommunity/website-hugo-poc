---
layout: "image"
title: "Die Platte sichert Akku und Elektronik sehr gut"
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/43775
- /detailsc44e.html
imported:
- "2019"
_4images_image_id: "43775"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43775 -->
