---
layout: "image"
title: "Chassis"
date: "2014-03-26T10:34:33"
picture: "rapidracer08.jpg"
weight: "8"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38496
- /detailsfffc.html
imported:
- "2019"
_4images_image_id: "38496"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38496 -->
Ohne beide Räder kann man den Aufbau nochmals gut sehen. Sollte recht schnell zusammengebaut sein.