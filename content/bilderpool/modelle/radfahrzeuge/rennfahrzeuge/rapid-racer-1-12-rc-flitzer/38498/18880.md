---
layout: "comment"
hidden: true
title: "18880"
date: "2014-03-26T20:34:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Auch eine hübsch unorthodoxe Kombination von Winkelzahnrädern. Gratuliere zum Modell! Mein Traum ist ja immer noch eine aus unverfrickeltem ft gebaute zweispurige "Carrera-Bahn", mit der man (möglichst mit Spurwechsel) echte und schnelle Rennen fahren kann...
Gruß,
Stefan