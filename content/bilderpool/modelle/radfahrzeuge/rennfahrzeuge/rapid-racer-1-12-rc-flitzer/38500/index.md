---
layout: "image"
title: "Getriebekasten 4"
date: "2014-03-26T10:34:34"
picture: "rapidracer12.jpg"
weight: "12"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38500
- /details6d85.html
imported:
- "2019"
_4images_image_id: "38500"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38500 -->
Da sind noch längere Querliegende Verbinder unter den Motorzahnrädern zur Versteifung versteckt.