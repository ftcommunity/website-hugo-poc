---
layout: "image"
title: "Rapid Racer vorne"
date: "2014-03-26T10:34:33"
picture: "rapidracer02.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38490
- /details80bd.html
imported:
- "2019"
_4images_image_id: "38490"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38490 -->
Ansicht von vorne. Noch fehlt vorne eine Stoßstange, aber man muß ja auch mit etwas Basteln bestraft werden, wenn man nicht richtig zielt. Größere Reifen gingen auch, aber die schnellen Getriebemotoren (und auch das Getriebe) machen das nur recht schlecht mit. Aber wenn jemand langsamere Motoren hat...