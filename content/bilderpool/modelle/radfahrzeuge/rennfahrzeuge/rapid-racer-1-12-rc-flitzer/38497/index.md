---
layout: "image"
title: "Getriebekasten 1"
date: "2014-03-26T10:34:33"
picture: "rapidracer09.jpg"
weight: "9"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38497
- /detailsc91c.html
imported:
- "2019"
_4images_image_id: "38497"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38497 -->
Am Getriebekasten ist alles mit 5mm oder 7.5mm versetzt gebaut, das ist kein Artefakt, sondern Absicht, um den Kasten noch stabiler hinzubekommen. Geht aber vielleicht auch noch besser, reicht aber momentan gut aus.