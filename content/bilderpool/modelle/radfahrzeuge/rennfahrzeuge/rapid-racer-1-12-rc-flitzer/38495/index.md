---
layout: "image"
title: "Racer Untenansicht"
date: "2014-03-26T10:34:33"
picture: "rapidracer07.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38495
- /detailsdc61.html
imported:
- "2019"
_4images_image_id: "38495"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38495 -->
Zwei Powermotoren, was sonst? Nochmal gut sichtbar, wie die Winkelbausteine die Motoren am Getriebekasten von unten krallen. Die Querliegenden Bausteine vor den Motoren halten die Motoren über die Stecker fest.