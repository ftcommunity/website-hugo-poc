---
layout: "image"
title: "Rapid Racer seite"
date: "2014-03-26T10:34:33"
picture: "rapidracer03.jpg"
weight: "3"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/38491
- /detailsc425.html
imported:
- "2019"
_4images_image_id: "38491"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38491 -->
Die Motoren werden vorne über die Stecker noch extra gehalten. Das ist ein bißchen auf Zug gebaut (könnte man entspannen), aber so hält das ein tacken steifer und der eine zentrale Zapfen vorne zeigt mal wieder, wie gut der FT-Kunsttoff ist.