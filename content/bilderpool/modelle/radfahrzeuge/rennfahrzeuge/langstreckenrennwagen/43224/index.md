---
layout: "image"
title: "Ansicht unten"
date: "2016-04-02T17:06:56"
picture: "lsrph3.jpg"
weight: "3"
konstrukteure: 
- "Patrick Holtz"
fotografen:
- "Patrick Holtz"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43224
- /detailsfad5.html
imported:
- "2019"
_4images_image_id: "43224"
_4images_cat_id: "3210"
_4images_user_id: "2228"
_4images_image_date: "2016-04-02T17:06:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43224 -->
Der Rennwagen fährt ziemlich schnell dank der Übersetzung im Verhältnis 2:1.
