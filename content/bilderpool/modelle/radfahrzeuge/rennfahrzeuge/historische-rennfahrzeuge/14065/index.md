---
layout: "image"
title: "Porsche 908"
date: "2008-03-24T08:40:09"
picture: "P1010878.jpg"
weight: "1"
konstrukteure: 
- "Adrian Raiber"
fotografen:
- "Adrian Raiber"
uploadBy: "adrian"
license: "unknown"
legacy_id:
- /php/details/14065
- /details3ff8.html
imported:
- "2019"
_4images_image_id: "14065"
_4images_cat_id: "1290"
_4images_user_id: "759"
_4images_image_date: "2008-03-24T08:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14065 -->
Ein Porsche 908 nach einer Anleitung der Clubzeitschrift März 1975. Hergestellt mit classic Bausteinen und Styro-Karosserie mit Hobbywelt 1