---
layout: "image"
title: "Porsche 908 Teil 4"
date: "2008-03-24T08:40:09"
picture: "P1010877.jpg"
weight: "4"
konstrukteure: 
- "Adrian Raiber"
fotografen:
- "Adrian Raiber"
uploadBy: "adrian"
license: "unknown"
legacy_id:
- /php/details/14068
- /details12dd-2.html
imported:
- "2019"
_4images_image_id: "14068"
_4images_cat_id: "1290"
_4images_user_id: "759"
_4images_image_date: "2008-03-24T08:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14068 -->
