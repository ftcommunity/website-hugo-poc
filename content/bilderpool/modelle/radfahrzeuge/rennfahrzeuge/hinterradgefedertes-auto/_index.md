---
layout: "overview"
title: "Hinterradgefedertes Auto mit Sturzverstellung"
date: 2020-02-22T07:56:45+01:00
legacy_id:
- /php/categories/1929
- /categoriesd5d9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1929 --> 
Ich habe ein Auto gebaut, bei dem man hinten durch zwei Minimotoren den Sturz verstellen kann. Natürlich ist er hinten noch gefedert! Vorne hat er eine Pendelachse.