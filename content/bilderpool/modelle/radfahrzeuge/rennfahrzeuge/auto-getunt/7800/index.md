---
layout: "image"
title: "Lufteinzug vorne"
date: "2006-12-09T13:38:38"
picture: "gif49.jpg"
weight: "49"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7800
- /details45b9.html
imported:
- "2019"
_4images_image_id: "7800"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:38"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7800 -->
