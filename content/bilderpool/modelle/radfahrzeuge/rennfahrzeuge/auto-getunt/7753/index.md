---
layout: "image"
title: "Auto oben"
date: "2006-12-09T13:37:59"
picture: "gif02.jpg"
weight: "2"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7753
- /details8564.html
imported:
- "2019"
_4images_image_id: "7753"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:37:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7753 -->
