---
layout: "image"
title: "Federung von unten"
date: "2006-12-20T19:22:05"
picture: "magi5.jpg"
weight: "58"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7995
- /details879f-2.html
imported:
- "2019"
_4images_image_id: "7995"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7995 -->
