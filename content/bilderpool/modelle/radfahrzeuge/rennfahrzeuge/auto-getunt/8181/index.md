---
layout: "image"
title: "Filter"
date: "2006-12-29T17:56:18"
picture: "magi8_2.jpg"
weight: "70"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8181
- /details6913.html
imported:
- "2019"
_4images_image_id: "8181"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-29T17:56:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8181 -->
