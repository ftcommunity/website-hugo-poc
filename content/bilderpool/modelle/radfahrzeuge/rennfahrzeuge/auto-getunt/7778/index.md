---
layout: "image"
title: "Prallansicht"
date: "2006-12-09T13:38:20"
picture: "gif27.jpg"
weight: "27"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7778
- /details47bd.html
imported:
- "2019"
_4images_image_id: "7778"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:20"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7778 -->
