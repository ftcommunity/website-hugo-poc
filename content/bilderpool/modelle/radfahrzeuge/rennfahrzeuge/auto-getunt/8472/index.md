---
layout: "image"
title: "Alles was neu ist"
date: "2007-01-15T22:50:01"
picture: "mabi03.jpg"
weight: "74"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8472
- /details144a.html
imported:
- "2019"
_4images_image_id: "8472"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8472 -->
Alle drei sachen di neu sind auf einen Blick. Ganz links ein Rückspiegel, in der Mitte die Fehrnsteuerung und rechts die "neue" Lenkung.