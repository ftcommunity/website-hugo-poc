---
layout: "image"
title: "Lenkung von weither"
date: "2006-12-29T17:56:18"
picture: "magi9_2.jpg"
weight: "71"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8182
- /detailsd665.html
imported:
- "2019"
_4images_image_id: "8182"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-29T17:56:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8182 -->
