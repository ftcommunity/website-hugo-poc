---
layout: "image"
title: "Hupe"
date: "2006-12-09T13:38:44"
picture: "gif52.jpg"
weight: "52"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7803
- /details2409.html
imported:
- "2019"
_4images_image_id: "7803"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:44"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7803 -->
