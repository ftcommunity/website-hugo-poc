---
layout: "image"
title: "Auto vorne- unten"
date: "2006-12-09T13:38:29"
picture: "gif36.jpg"
weight: "36"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7787
- /details3561.html
imported:
- "2019"
_4images_image_id: "7787"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:29"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7787 -->
