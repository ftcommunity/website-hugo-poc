---
layout: "image"
title: "Altes Dach 2"
date: "2006-12-09T13:38:10"
picture: "gif20.jpg"
weight: "20"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7771
- /detailsd774.html
imported:
- "2019"
_4images_image_id: "7771"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:10"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7771 -->
