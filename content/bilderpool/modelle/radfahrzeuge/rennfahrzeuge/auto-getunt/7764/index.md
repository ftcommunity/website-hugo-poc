---
layout: "image"
title: "Auto oben"
date: "2006-12-09T13:38:10"
picture: "gif13.jpg"
weight: "13"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7764
- /details9ca8-2.html
imported:
- "2019"
_4images_image_id: "7764"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:10"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7764 -->
