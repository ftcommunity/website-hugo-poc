---
layout: "image"
title: "Ein grosser auspuff (Links)"
date: "2006-12-09T13:38:29"
picture: "gif38.jpg"
weight: "38"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7789
- /details6337-2.html
imported:
- "2019"
_4images_image_id: "7789"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:29"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7789 -->
