---
layout: "image"
title: "Ansicht von oben"
date: "2006-12-20T19:22:05"
picture: "magi7.jpg"
weight: "60"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7997
- /details4357.html
imported:
- "2019"
_4images_image_id: "7997"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7997 -->
