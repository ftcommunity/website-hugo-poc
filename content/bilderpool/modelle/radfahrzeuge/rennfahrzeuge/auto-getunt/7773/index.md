---
layout: "image"
title: "Sicht von innen nach vorne"
date: "2006-12-09T13:38:20"
picture: "gif22.jpg"
weight: "22"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7773
- /detailsb820-2.html
imported:
- "2019"
_4images_image_id: "7773"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7773 -->
Man sieht noch die Fehrnsteuerung