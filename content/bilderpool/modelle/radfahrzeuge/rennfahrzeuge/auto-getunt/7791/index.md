---
layout: "image"
title: "ohne abdeckung"
date: "2006-12-09T13:38:29"
picture: "gif40.jpg"
weight: "40"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7791
- /detailsfd71.html
imported:
- "2019"
_4images_image_id: "7791"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:29"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7791 -->
