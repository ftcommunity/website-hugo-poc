---
layout: "image"
title: "Kabel (rechts vorne)"
date: "2007-01-15T22:50:26"
picture: "mabi11.jpg"
weight: "82"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8480
- /details51bd-2.html
imported:
- "2019"
_4images_image_id: "8480"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8480 -->
Hier gehts entlang.