---
layout: "image"
title: "Vier Auspüffe gerade"
date: "2006-12-09T13:38:38"
picture: "gif43.jpg"
weight: "43"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7794
- /detailse57c.html
imported:
- "2019"
_4images_image_id: "7794"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:38"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7794 -->
