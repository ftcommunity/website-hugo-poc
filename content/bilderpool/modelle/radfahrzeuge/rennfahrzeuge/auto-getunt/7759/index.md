---
layout: "image"
title: "'Neon Licht' (unten)"
date: "2006-12-09T13:37:59"
picture: "gif08.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7759
- /details94a1.html
imported:
- "2019"
_4images_image_id: "7759"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:37:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7759 -->
