---
layout: "image"
title: "Nochmal Seitlicher Lufteinzug"
date: "2006-12-20T19:22:05"
picture: "magi9.jpg"
weight: "62"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/7999
- /details22a7.html
imported:
- "2019"
_4images_image_id: "7999"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7999 -->
