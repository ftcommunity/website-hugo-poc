---
layout: "image"
title: "Neu von oben"
date: "2007-01-15T22:50:01"
picture: "mabi04.jpg"
weight: "75"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/8473
- /details2b62.html
imported:
- "2019"
_4images_image_id: "8473"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8473 -->
Hier siet man die Befestigung gut.