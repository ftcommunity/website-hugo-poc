---
layout: "image"
title: "fbesser12.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser12.jpg"
weight: "82"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47045
- /details805c.html
imported:
- "2019"
_4images_image_id: "47045"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47045 -->
