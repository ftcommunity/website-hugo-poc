---
layout: "image"
title: "Etwas kürzer 2"
date: "2017-03-25T13:34:58"
picture: "fetwaskuerzer2.jpg"
weight: "56"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45698
- /detailsadfe.html
imported:
- "2019"
_4images_image_id: "45698"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-25T13:34:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45698 -->
