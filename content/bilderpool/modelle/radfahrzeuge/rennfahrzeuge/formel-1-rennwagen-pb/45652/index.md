---
layout: "image"
title: "f10.jpg"
date: "2017-03-24T06:50:47"
picture: "f10.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45652
- /details1389-2.html
imported:
- "2019"
_4images_image_id: "45652"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45652 -->
Was gut ist an der Servo is der unmittelbare Reaktion, die Schnellen Kurzen Lenkbewegungen, was gerade fur ein Racer geeignet ist. Das hier war mein ursprünglicher Versuch den Lenkung mit den Servo zu machen. Leider kaum Einschlag, auch weil den Servo eigentlich an die Falsche Seite der Lenkstange sitzt. Hat vielleicht jemand Ideeën wie es doch mit den Servo in etwa derselben Raum realisiert werden kann?