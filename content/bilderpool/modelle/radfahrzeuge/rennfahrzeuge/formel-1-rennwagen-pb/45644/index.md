---
layout: "image"
title: "f02.jpg"
date: "2017-03-24T06:50:47"
picture: "f02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45644
- /details8a68-2.html
imported:
- "2019"
_4images_image_id: "45644"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45644 -->
