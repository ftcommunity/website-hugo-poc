---
layout: "image"
title: "f50.jpg"
date: "2017-03-24T06:51:52"
picture: "f50.jpg"
weight: "50"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45692
- /details3f61-2.html
imported:
- "2019"
_4images_image_id: "45692"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45692 -->
Und dann die Ferrari