---
layout: "image"
title: "fservo6.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo6.jpg"
weight: "68"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45720
- /details5a8a-2.html
imported:
- "2019"
_4images_image_id: "45720"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45720 -->
Einfacher symmetrischer Einbau der Servo (Oei, den Nase ist etwas weggeschoben vom Mitte...)