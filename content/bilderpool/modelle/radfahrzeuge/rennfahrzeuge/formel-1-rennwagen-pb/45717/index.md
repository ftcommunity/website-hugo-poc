---
layout: "image"
title: "fservo3.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo3.jpg"
weight: "65"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45717
- /detailsd11a.html
imported:
- "2019"
_4images_image_id: "45717"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45717 -->
Die Spurstange sitzt jetzt näher an das Drehpunkt der Räder.

Weil den Servo andersherum eingebaut worden ist, lenkt er auch umgekehrt. Mann kann sich daran gewohnen, oder den IR-Sender auf den Kopf halten (den Motor kann mann ja umpolen)