---
layout: "image"
title: "f07.jpg"
date: "2017-03-24T06:50:47"
picture: "f07.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45649
- /details7058.html
imported:
- "2019"
_4images_image_id: "45649"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45649 -->
Unten