---
layout: "image"
title: "fbesser13.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser13.jpg"
weight: "83"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47046
- /detailsf87c-3.html
imported:
- "2019"
_4images_image_id: "47046"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47046 -->
Die Lenkung ist nog ein Wenig geändert worden, nach ein Idee von Hans Wijnsauw.