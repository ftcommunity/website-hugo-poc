---
layout: "image"
title: "fservo1.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo1.jpg"
weight: "63"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45715
- /details4b07-3.html
imported:
- "2019"
_4images_image_id: "45715"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45715 -->
Weil ich motiviert war den Servo zu verwenden für den Lenkung, hab ich noch einen gebaut und es hat geklappt. Mit den Servo reagiert die Lenkung rasch und zügig, was für ein Racer gewünscht ist. Das 'in der Mitte stellen' der Lenkung ist schon etwas Schwieriger, aber es geht. Hab die Nase auch noch um etwa 10mm kürzen können.

