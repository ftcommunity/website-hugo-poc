---
layout: "image"
title: "f12.jpg"
date: "2017-03-24T06:51:52"
picture: "f12.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45654
- /detailsd0e7-3.html
imported:
- "2019"
_4images_image_id: "45654"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45654 -->
Kein Platz für den Großen Taster ohne das Aussehen zu schaden. Also den Kleinen verwendet mit ein BS 15x30x3,75 als 'Schiebschalter'. Nicht vergessen nach dem Fahren zurück zu schieben.