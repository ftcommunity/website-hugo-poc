---
layout: "image"
title: "f19.jpg"
date: "2017-03-24T06:51:52"
picture: "f19.jpg"
weight: "19"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45661
- /details75a9-2.html
imported:
- "2019"
_4images_image_id: "45661"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45661 -->
Das Selbe Teil in einen anderen Perspektive.