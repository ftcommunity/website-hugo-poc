---
layout: "image"
title: "f41.jpg"
date: "2017-03-24T06:51:52"
picture: "f41.jpg"
weight: "41"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45683
- /details0b84.html
imported:
- "2019"
_4images_image_id: "45683"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45683 -->
