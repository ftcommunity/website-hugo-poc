---
layout: "image"
title: "f17.jpg"
date: "2017-03-24T06:51:52"
picture: "f17.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45659
- /details2a14.html
imported:
- "2019"
_4images_image_id: "45659"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45659 -->
Die Ecksteine 15x15x15 sind dafür, dass Alles gut Schieben bleibt Zwischen den Streben, ohne zu 'Knacken'.