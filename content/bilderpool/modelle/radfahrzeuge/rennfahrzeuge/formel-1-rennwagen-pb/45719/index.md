---
layout: "image"
title: "fservo5.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo5.jpg"
weight: "67"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45719
- /details7153.html
imported:
- "2019"
_4images_image_id: "45719"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45719 -->
