---
layout: "image"
title: "f44.jpg"
date: "2017-03-24T06:51:52"
picture: "f44.jpg"
weight: "44"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45686
- /detailsbdc8-2.html
imported:
- "2019"
_4images_image_id: "45686"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45686 -->
Zum Schluß noch ein paar Bilder vom Fertig Modell