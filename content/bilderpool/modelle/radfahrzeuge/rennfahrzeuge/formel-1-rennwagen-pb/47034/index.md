---
layout: "image"
title: "fbesser01.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser01.jpg"
weight: "71"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47034
- /detailsdcad-2.html
imported:
- "2019"
_4images_image_id: "47034"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47034 -->
Ich hab mich noch bemüht den Empfänger hinten ein zu bauen, was mit den Servo-reverse-kabel möglicht würde. Damit gab es raum für ein 2:1 Übersetzung im Antrieb. Im Praxis macht es nicht so viel Unterschied...