---
layout: "overview"
title: "Rennwagen"
date: 2020-02-22T07:56:43+01:00
legacy_id:
- /php/categories/1560
- /categories31be.html
- /categories4aa8.html
- /categoriesc7f4.html
- /categories164b.html
- /categories6c03.html
- /categoriesc24e-2.html
- /categoriesfff7.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1560 --> 
Dieser Rennwagen hat einen Überrollbügel und wird vom Power Motor angetrieben. Gelenkt wird es durch den Servo. Gesteuert wird das Auto mit dem IR Control Set.