---
layout: "image"
title: "rennwagen6.jpg"
date: "2009-11-01T11:27:02"
picture: "rennwagen6.jpg"
weight: "18"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/25620
- /detailsdd32.html
imported:
- "2019"
_4images_image_id: "25620"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-11-01T11:27:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25620 -->
