---
layout: "comment"
hidden: true
title: "10175"
date: "2009-11-02T16:22:01"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Aber die Vorderreifen sind doch auch "Gummis", sogar Breitreifen :)
Auf der Vorderachse fehlt sicher nur Gewicht, der Akku hinten sorgt ja für genügend Andruck!