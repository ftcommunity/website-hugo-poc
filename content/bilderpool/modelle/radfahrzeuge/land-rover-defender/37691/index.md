---
layout: "image"
title: "IMG_9194.JPG"
date: "2013-10-07T21:01:31"
picture: "IMG_9194mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37691
- /details663d.html
imported:
- "2019"
_4images_image_id: "37691"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T21:01:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37691 -->
Jetzt geht's an den Kern der Sache. Das Differenzial sitzt tiefer und weiter vorn als die schwarzen Rastkegel, die hier verdeckt sind. Die Kopplung dazwischen ist je ein Paar von Z10 (1x Klemm-Z10, 1x Rast-Z10). An der ganzen Anordnung ist nichts gemoddet oder geschnippelt.
