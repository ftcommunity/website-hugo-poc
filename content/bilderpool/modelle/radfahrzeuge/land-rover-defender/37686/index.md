---
layout: "image"
title: "IMG_9185.JPG"
date: "2013-10-07T20:39:40"
picture: "IMG_9185mit.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37686
- /details2221.html
imported:
- "2019"
_4images_image_id: "37686"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T20:39:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37686 -->
Ein 'Defender' mit Allradantrieb. Das Foto zeigt die Vorversion. Zur ft-Convention 2013 wurde das Chassis um 3 cm verlängert, damit gescheite Türen hinein passten, die Haube verkleidet, die Verkleidung der A-Säulen weggenommen und eine Hecktür eingebaut.
