---
layout: "overview"
title: "Motorrad mit Beiwagen groß (ca. 70cm)"
date: 2024-03-24T17:30:11+01:00
---

Diese Bildersammlung zeigt ein Motorrad, genauer gesagt eine Beiwagenmaschine, die ich für die Handpuppenkatze und die Eule (die meine Frau für die Schule verwendet) gebaut habe.
Ursprünglich war geplant, das ganze sogar mit einem Gummiband als "Aufziehmaschine" zu bauen, damit es selbstständig fährt. Das hat leider nicht funktioniert, weil das Modell, samt Katze zu schwer war und sie die Reifen entweder durchgedreht haben oder es gar nichts in Fahren gekommen ist.
Ich hab' dann aus Zeitgründen das ganze Modell auch nicht so stabil bauen, können, dass es nur auf den Hinterrädern und dem Vorderrad steht, sondern hab geschummelt, indem ich unter dem Sitz noch ein Hilfsrad eingebaut haben. Mit ein bisschen überlegen und umbauen, ließe sich das aber lösen.
Gut geworden ist meiner Ansicht nach das große Vorderrad, dass ich extra mit Speichen und einem alten Fahrradschlauch gebaut habe.
Den Beiwagen könnte man auch noch etwas aufhübschen. Aber für unsere Fotos und Spielereien hat es ausgereicht.