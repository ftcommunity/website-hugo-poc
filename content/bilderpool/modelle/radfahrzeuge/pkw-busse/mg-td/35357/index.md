---
layout: "image"
title: "MG TD"
date: "2012-08-26T15:33:25"
picture: "mgtd1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/35357
- /details8b1f-2.html
imported:
- "2019"
_4images_image_id: "35357"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35357 -->
Hier sieht man den MG TD.
Ich habe aufgrund der schwachen Batterieleistung keine Lampen angeschlossen.
Eine Besondere Herausforderung war es den geschwungenen Kotflügel zu konstruieren.