---
layout: "image"
title: "Von Hinten"
date: "2012-08-26T15:33:25"
picture: "mgtd4.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/35360
- /details8b7d.html
imported:
- "2019"
_4images_image_id: "35360"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35360 -->
Hier kann man besonders gut das Ersatzrad und den Gepäckträger erkennen.