---
layout: "image"
title: "Kleinwagen 13"
date: "2010-03-20T18:00:09"
picture: "Kleinwagen_23.jpg"
weight: "13"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26764
- /details5f06.html
imported:
- "2019"
_4images_image_id: "26764"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26764 -->
Unter dem Servo sitzt ein Verbindungsstück 15 als Abstandshalter, damit sich die obere Spurstange problemlos unter dem Servohebel bewegen kann.