---
layout: "image"
title: "Kleinwagen 5"
date: "2010-03-20T18:00:07"
picture: "Kleinwagen_10.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26756
- /detailsdb0f-2.html
imported:
- "2019"
_4images_image_id: "26756"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26756 -->
