---
layout: "image"
title: "pickup04.jpg"
date: "2013-07-20T17:39:47"
picture: "IMG_9074mit.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37172
- /detailsa709.html
imported:
- "2019"
_4images_image_id: "37172"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:39:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37172 -->
Einen derart "aufgeräumten" Unterboden hat noch keins meiner Autos gehabt. Die weißen Kegelräder rechts waren zum fanClub-Day in Tumlingen als Reserve dabei.
Die BS15-Loch, in denen die Vorderräder gelagert sind, sind deutlich gemoddet (seitlich abgehobelt).
