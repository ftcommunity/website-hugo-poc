---
layout: "image"
title: "pickup02.jpg"
date: "2013-07-20T17:35:33"
picture: "IMG_9069mit.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37170
- /details4ef6.html
imported:
- "2019"
_4images_image_id: "37170"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:35:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37170 -->
Auf die Ladefläche kommt noch der Akku als fester Einbau. Der hat nirgendwo drunter oder hinein gepasst.
