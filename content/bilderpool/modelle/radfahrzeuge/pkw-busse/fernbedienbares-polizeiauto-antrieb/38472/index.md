---
layout: "image"
title: "Nochmal ohne Blitz"
date: "2014-03-16T21:08:36"
picture: "S1060013.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38472
- /details9c68-2.html
imported:
- "2019"
_4images_image_id: "38472"
_4images_cat_id: "2869"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T21:08:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38472 -->
