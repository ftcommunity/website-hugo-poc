---
layout: "image"
title: "2 Gangschaltung"
date: "2011-01-22T19:45:38"
picture: "neueautomodelle2.jpg"
weight: "2"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29751
- /details1d67.html
imported:
- "2019"
_4images_image_id: "29751"
_4images_cat_id: "2187"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29751 -->
eine sehr einfach aufgebaute gangschaltung die über das controllset ferns gesteuert werden kann
