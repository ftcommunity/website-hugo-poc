---
layout: "image"
title: "Universal II-Fahrzeug Cockpit"
date: "2009-01-20T16:50:23"
picture: "IMG_5759.jpg"
weight: "6"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- /php/details/17116
- /detailsc911.html
imported:
- "2019"
_4images_image_id: "17116"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17116 -->
Hier noch die Sicht in das Cockpit