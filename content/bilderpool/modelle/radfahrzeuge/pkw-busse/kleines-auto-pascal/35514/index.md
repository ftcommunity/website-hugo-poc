---
layout: "image"
title: "Die Lenkung"
date: "2012-09-12T21:32:11"
picture: "kleinesauto9.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/35514
- /details91fa.html
imported:
- "2019"
_4images_image_id: "35514"
_4images_cat_id: "2633"
_4images_user_id: "1122"
_4images_image_date: "2012-09-12T21:32:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35514 -->
Das Zahnrad ist mit einer Klemmbüchse 31023 am Servo festgemacht.