---
layout: "image"
title: "Akku ausgebaut"
date: "2015-01-07T22:44:05"
picture: "sportwagen11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40207
- /detailsad1d.html
imported:
- "2019"
_4images_image_id: "40207"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40207 -->
So muss man den Akku ausbauen, wenn man die Zellen laden will. Als Akku könnte man auch den original-8,4V-Akku verwenden, aber ich habe mich für den Selbstbau http://www.ftcommunity.de/details.php?image_id=40195 entschieden. Das Original würde optisch mehr hermachen, zumal man die Fischertechnik-Aufschrift vielleicht durch die Lücken in der Klappe sehen würde. Auch die Heckklappe rastet an Bauplatten 3x2 vorbei, was hier aber kaum spürbar ist.