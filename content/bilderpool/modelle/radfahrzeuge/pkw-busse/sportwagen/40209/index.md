---
layout: "image"
title: "Fronthaube demontiert"
date: "2015-01-07T22:44:05"
picture: "sportwagen13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40209
- /detailsd22f.html
imported:
- "2019"
_4images_image_id: "40209"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40209 -->
Hier die demontierte Fronthaube. Das ganze Chassis mit Lenkung, Antrieb und Sitzen findet ihr unter http://www.ftcommunity.de/categories.php?cat_id=3003. Vielleicht ist noch erwänenswert, dass sich das Lenkrad mitdreht wenn man lenkt.