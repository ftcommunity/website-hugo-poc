---
layout: "image"
title: "Ansicht von hinten"
date: "2015-01-07T22:44:05"
picture: "sportwagen06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40202
- /details9262.html
imported:
- "2019"
_4images_image_id: "40202"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40202 -->
Auf den beiden Raupenbelege stützt sich die Heckklappe ab.