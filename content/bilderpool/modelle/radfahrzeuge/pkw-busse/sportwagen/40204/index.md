---
layout: "image"
title: "Ansicht von unten"
date: "2015-01-07T22:44:05"
picture: "sportwagen08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40204
- /details22b0-2.html
imported:
- "2019"
_4images_image_id: "40204"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40204 -->
Man sieht hier, dass die Fronthaube mit einer Bauplatte 3x1 runtergezogen wird.