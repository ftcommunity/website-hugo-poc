---
layout: "image"
title: "geöffnete Türen"
date: "2015-01-07T22:44:05"
picture: "sportwagen09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40205
- /details2247-2.html
imported:
- "2019"
_4images_image_id: "40205"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40205 -->
Die Türen kann man öffnen. Beim Öffnen und Schließen rasten sie an der Bauplatte 2x1 vorbei, was ein sehr schönes Gefühl gibt und vor allem rasten sie dadurch beim Schließen richtig ein und schlackern nicht rum.