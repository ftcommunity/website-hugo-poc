---
layout: "image"
title: "Ente05.JPG"
date: "2006-07-10T17:48:05"
picture: "Ente05.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6615
- /details8d35-2.html
imported:
- "2019"
_4images_image_id: "6615"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:48:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6615 -->
Die Heckklappe rastet oben ein, dank Vierkant-Stummel an den Radachsen 36585, auf denen sie gelagert ist.
