---
layout: "image"
title: "Ente37.JPG"
date: "2006-07-10T18:06:53"
picture: "Ente37.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6622
- /details0371.html
imported:
- "2019"
_4images_image_id: "6622"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6622 -->
Die vorderen Innereien. 
Die Lenkung beruht auf zwei Schnecken 35977 nach Art des Winkelantriebs in diesem Bild: http://www.ftcommunity.de/details.php?image_id=5310 : die Kette wird von einem Kegelrad zwischen den Schnecken angetrieben. Auf der Unterseite ist ein BS7,5 aufgeschoben, der die Spurstange hin- und herschiebt.
