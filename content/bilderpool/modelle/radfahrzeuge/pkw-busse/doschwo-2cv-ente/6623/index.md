---
layout: "image"
title: "Ente41.JPG"
date: "2006-07-10T18:08:03"
picture: "Ente41.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6623
- /detailse23f.html
imported:
- "2019"
_4images_image_id: "6623"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:08:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6623 -->
Der IR-Empfänger ist an seinem Platz, darunter fehlt noch der Akku.
