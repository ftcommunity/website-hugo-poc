---
layout: "image"
title: "Ente67.JPG"
date: "2006-07-10T18:21:02"
picture: "Ente67.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6630
- /details0023-2.html
imported:
- "2019"
_4images_image_id: "6630"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:21:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6630 -->
Die vordere Sitzbank (mit blauen Teilen) zwischen den beiden B-Säulen. Unten im Bild ist die hintere linke Tür zu sehen.
