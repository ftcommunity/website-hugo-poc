---
layout: "image"
title: "Ente65.JPG"
date: "2006-07-10T18:17:27"
picture: "Ente65.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6628
- /detailsd457.html
imported:
- "2019"
_4images_image_id: "6628"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:17:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6628 -->
Vorne drin geht es ziemlich eng zu.
