---
layout: "image"
title: "Frontansicht"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34357
- /details27fd.html
imported:
- "2019"
_4images_image_id: "34357"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34357 -->
Der cw wäre bestimmt schön klein :-)
