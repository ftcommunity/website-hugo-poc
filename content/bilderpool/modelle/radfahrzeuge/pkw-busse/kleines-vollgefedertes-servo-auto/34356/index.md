---
layout: "image"
title: "Seitenansicht"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34356
- /detailsc725-2.html
imported:
- "2019"
_4images_image_id: "34356"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34356 -->
Endlich mal mit meinem Servo rumspielen. :-)  Dieses Auto ist an beiden Achsen butterweich gefedert, servogelenkt und hat Heckantrieb mit Differential. Platz für ft-Männchen ist nicht, aber immerhin ragt nichts im Innenraum über die Fensterlinie hinaus. Karosserie kann ich ja nicht ;-) also wurde es so ein Stück Einheitsseife, wie heute ja viele Autos aussehen.
