---
layout: "image"
title: "Fahrwerk von unten"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34359
- /details4018.html
imported:
- "2019"
_4images_image_id: "34359"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34359 -->
Zwei schwarze gelochte I-Streben 120 laufen von vorne bis hinten durchs Fahrzeug und sind nur an den roten und schwarzen BS15 in der Mitte mittels je zweier Strebenadapter befestigt. Vorne tragen sie federn die konventionell aufgebaute Servolenkung, hinten tragen Sie zur Federung der Hinterachse bei. Dadurch, dass die Federn recht weit innen, nach den Achsmittelpunkten, angreift, wird die Federung schön weich.

Die beiden schwarzen Kunststofffedern halten die Hinterachskonstruktion hinreichend fest, aber so, dass das Kardangelenk schwingen kann. Tatsächlich kann die Hinterachse damit ein wenig nach links und rechts pendeln, Ursprünglich wollte ich zwei Elektromechanik-Federstäbe dafür verwenden, aber so kann man es mit aktuellen Teilen leichter nachbauen.
