---
layout: "image"
title: "Aufsicht ohne Verdeck"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34360
- /details39a9.html
imported:
- "2019"
_4images_image_id: "34360"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34360 -->
Hier sind die Verdeckplatten abgenommen, und man sieht von oben ins Fahrzeug. Vorne trägt eine weitere schwarze Kunststofffeder zur Federung der Vorderachse bei, die einfach ein bisschen aufs Servo drückt.
