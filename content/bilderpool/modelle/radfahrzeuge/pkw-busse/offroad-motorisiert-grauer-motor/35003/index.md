---
layout: "image"
title: "Offroad"
date: "2012-05-27T22:35:08"
picture: "offroadmotorisiertgrauermotorfernsteuerung5.jpg"
weight: "5"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/35003
- /detailse0f8.html
imported:
- "2019"
_4images_image_id: "35003"
_4images_cat_id: "2592"
_4images_user_id: "1355"
_4images_image_date: "2012-05-27T22:35:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35003 -->
Motor und Getriebe