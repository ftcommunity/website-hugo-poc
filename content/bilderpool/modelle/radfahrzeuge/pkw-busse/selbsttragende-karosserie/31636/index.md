---
layout: "image"
title: "Komplett ansicht"
date: "2011-08-22T15:46:51"
picture: "selbsttragendekarosserieauto1.jpg"
weight: "1"
konstrukteure: 
- "Barth Thomas"
fotografen:
- "Barth Thomas"
uploadBy: "choco"
license: "unknown"
legacy_id:
- /php/details/31636
- /details78f5-2.html
imported:
- "2019"
_4images_image_id: "31636"
_4images_cat_id: "2359"
_4images_user_id: "1347"
_4images_image_date: "2011-08-22T15:46:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31636 -->
ist noch nicht fertig