---
layout: "image"
title: "Oldtimer"
date: "2011-02-26T18:08:18"
picture: "oldtimer4.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30135
- /detailsa7fa.html
imported:
- "2019"
_4images_image_id: "30135"
_4images_cat_id: "2228"
_4images_user_id: "453"
_4images_image_date: "2011-02-26T18:08:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30135 -->
