---
layout: "image"
title: "Oldtimer"
date: "2011-02-26T18:08:18"
picture: "oldtimer1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30132
- /details3a98.html
imported:
- "2019"
_4images_image_id: "30132"
_4images_cat_id: "2228"
_4images_user_id: "453"
_4images_image_date: "2011-02-26T18:08:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30132 -->
http://www.youtube.com/watch?v=WkdZKQM5y6Y
