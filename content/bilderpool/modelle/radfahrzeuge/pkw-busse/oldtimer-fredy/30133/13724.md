---
layout: "comment"
hidden: true
title: "13724"
date: "2011-02-27T19:24:19"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Super Modell eines klassischen englischen Rechtslenker-Roadsters!

Aber ob der beinamputierte Mechaniker auf dem Beifahrersitz im Ernstfall soviel helfen kann? ;o)

Gruß, Thomas