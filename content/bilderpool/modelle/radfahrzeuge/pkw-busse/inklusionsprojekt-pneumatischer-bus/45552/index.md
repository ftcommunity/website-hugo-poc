---
layout: "image"
title: "Großes Seitenfenster aus CD Hülle"
date: "2017-03-15T21:16:58"
picture: "IMG_8556.jpg"
weight: "29"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Seitenfenster", "CD", "Hülle"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45552
- /detailsacb9.html
imported:
- "2019"
_4images_image_id: "45552"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45552 -->
