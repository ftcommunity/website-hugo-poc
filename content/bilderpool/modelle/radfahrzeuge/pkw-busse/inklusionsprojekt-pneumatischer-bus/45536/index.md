---
layout: "image"
title: "Ansicht des Busses von oben"
date: "2017-03-15T21:16:58"
picture: "IMG_8519.jpg"
weight: "13"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Bus", "Pneumatik", "Ventil", "Kompressor", "Steuerelektronik"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45536
- /details8da7-3.html
imported:
- "2019"
_4images_image_id: "45536"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45536 -->
Auf dem Dach des Busses haben die Steuerelektronik und die pneumatischen Ventile Platz gefunden.