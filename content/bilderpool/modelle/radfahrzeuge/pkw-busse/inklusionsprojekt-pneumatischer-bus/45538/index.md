---
layout: "image"
title: "Gleitfalttüren geschlossen"
date: "2017-03-15T21:16:58"
picture: "IMG_8525.jpg"
weight: "15"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Gleitfalttür"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45538
- /details0d3b.html
imported:
- "2019"
_4images_image_id: "45538"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45538 -->
