---
layout: "image"
title: "Luftfederung vorne mit Ausgleichsbehälter"
date: "2017-03-15T21:16:58"
picture: "IMG_8547.jpg"
weight: "26"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Luftspeicher", "Ausgleichsbehälter"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/45549
- /details8fb5.html
imported:
- "2019"
_4images_image_id: "45549"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45549 -->
Der kleine Luftspeicher dient dem schnellen Ausgleich von Luftdruckschwankungen zwischen links und rechts.