---
layout: "image"
title: "Strandbuggy mit Allradantrieb"
date: "2012-04-29T13:34:19"
picture: "Strandbuggy_Allrad.jpg"
weight: "4"
konstrukteure: 
- "asdf"
fotografen:
- "asdf"
uploadBy: "asdf"
license: "unknown"
legacy_id:
- /php/details/34835
- /detailsc856.html
imported:
- "2019"
_4images_image_id: "34835"
_4images_cat_id: "2577"
_4images_user_id: "1490"
_4images_image_date: "2012-04-29T13:34:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34835 -->
In der frühen Version hatte er einen Allradantrieb. Später wurde der durch Vorderradantrieb ersetzt, weil wir das zweite Differential für ein anderes Modell brauchten.