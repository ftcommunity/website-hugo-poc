---
layout: "image"
title: "Strandbuggy mit Vorderradantrieb"
date: "2012-04-29T13:34:19"
picture: "Strandbuggy_Vorderrad.jpg"
weight: "5"
konstrukteure: 
- "asdf"
fotografen:
- "asdf"
uploadBy: "asdf"
license: "unknown"
legacy_id:
- /php/details/34836
- /details084d.html
imported:
- "2019"
_4images_image_id: "34836"
_4images_cat_id: "2577"
_4images_user_id: "1490"
_4images_image_date: "2012-04-29T13:34:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34836 -->
Nun mit Vorderradantrieb, aber mit den neuen großen Rädern.