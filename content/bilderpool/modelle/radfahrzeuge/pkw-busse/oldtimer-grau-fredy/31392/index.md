---
layout: "image"
title: "Oldtimer"
date: "2011-07-28T11:42:57"
picture: "graueroldtimer2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/31392
- /details658b-2.html
imported:
- "2019"
_4images_image_id: "31392"
_4images_cat_id: "2335"
_4images_user_id: "453"
_4images_image_date: "2011-07-28T11:42:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31392 -->
