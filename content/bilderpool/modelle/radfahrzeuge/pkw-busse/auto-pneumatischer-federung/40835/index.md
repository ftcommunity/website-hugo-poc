---
layout: "image"
title: "Längslenker"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung26.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40835
- /details0ca3.html
imported:
- "2019"
_4images_image_id: "40835"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40835 -->
Dieses Bild wurde auch beim Zerlegen des Fahrzeugs aufgenommen. Man sieht den Aufbau der Längslenker. Unterhalb des Z30 sieht man gerade noch das Z10-Ritzel des PowerMotors. Über die Schnecke auf der Z30-Achse geht der Antrieb über das (alte rote) Differential. Dessen Abtriebsachsen sind gleichzeitig die Achsen, um die die Längslenker pendeln
