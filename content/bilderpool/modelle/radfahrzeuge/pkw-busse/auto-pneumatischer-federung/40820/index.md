---
layout: "image"
title: "Vorderrad in Sollhöhe"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40820
- /detailsfef8.html
imported:
- "2019"
_4images_image_id: "40820"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40820 -->
Dies ist das Vorderrad mit erreichter Soll-Bodenfreiheit.
