---
layout: "image"
title: "Gesamtansicht (2)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40811
- /details717f-2.html
imported:
- "2019"
_4images_image_id: "40811"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40811 -->
Hier sieht man gut, wie das Fahrzeug fast auf dem Boden aufliegt, solange es ausgeschaltet ist.
