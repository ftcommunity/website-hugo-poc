---
layout: "image"
title: "Lenkungsversuche in erster Variante"
date: "2015-04-20T15:15:14"
picture: "prototypen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40841
- /details8b64.html
imported:
- "2019"
_4images_image_id: "40841"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40841 -->
Das andere Rad würde von der Fahrzeuginnenseite her gelenkt. Das hat sich aber gar nicht bewährt: Das Spiel war viel zu groß, und der Lenkeinschlag zu winzig. Letztlich fiel ich auf das klassische Lenktrapez mit einer Spurstange zurück, die von einem Servo vor der Vorderachse (deshalb die lange Motorhaube) betätigt wird.

Hier sieht man aber schon die niedrig bauende Vorderachskonstruktion.
