---
layout: "image"
title: "Lenkungsversuche in erster Variante"
date: "2015-04-20T15:15:13"
picture: "prototypen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40840
- /details147e.html
imported:
- "2019"
_4images_image_id: "40840"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40840 -->
Zuerst wollte ich die Lenkung vom Fahrzeuginneren, also hinter der Vorderachse antreiben. Eine durchgehende Rastachse sollte über eine Rastkurbel (Bildmitte) ein einigermaßen klobiges Lenkgestänge antreiben.
