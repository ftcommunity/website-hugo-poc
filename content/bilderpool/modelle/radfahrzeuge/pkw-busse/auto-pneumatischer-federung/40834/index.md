---
layout: "image"
title: "Hinterachse"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40834
- /details6697.html
imported:
- "2019"
_4images_image_id: "40834"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40834 -->
Man sieht hier den Antriebs-Powermotor mittig unterhalb des Kompressors, das rechte Hinterrad sowie die Aufhängung seines Pneumatikzylinders für die Federung. Wie bei der Vorderachse auch sind hinten wegen ihrer Leichtgängigkeit Kompressorzylinder verbaut. Oben sieht man die Kontaktachsen für den hinteren Niveauausgleich.
