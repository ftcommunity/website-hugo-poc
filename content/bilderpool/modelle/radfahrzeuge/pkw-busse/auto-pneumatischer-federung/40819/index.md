---
layout: "image"
title: "Vorderrad ganz eingefedert"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40819
- /detailsad31-3.html
imported:
- "2019"
_4images_image_id: "40819"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40819 -->
Hier sieht man das Vorderrad völlig eingefedert bei ausgeschaltetem Fahrzeug - vergleiche das nächste Bild.
