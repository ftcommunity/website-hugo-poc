---
layout: "image"
title: "Aufbau der Vorderachse (1)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40832
- /details60c8.html
imported:
- "2019"
_4images_image_id: "40832"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40832 -->
Die Vorderachse baut sehr angenehm niedrig: Der hier sichtbare Zylinder (wir schauen von vorne aufs halb zerlegte Fahrzeug) ist, wie alle vier Zylinder, ein Kompressorzylinder. Die fahren nämlich besonders leichtgängig aus und ein und ermöglichen eine fein ansprechende Federung.

Der Zylinder für das rechte Rad hier im Bild ist an einer Achse drehbar aufgehängt, die durch das innere Gelenk des linken oberen Querträgers für das linke Rad geht. Der Hebel aus einigen roten Teilen rechts hängt unten an einer solchen Achse, auf der auf der entfernten (hinteren) Seite geradeso der Zylinder für das rechte Fahrzeugrad (hier links im Bild, weil wir von vorne drauf gucken) aufgehängt ist. Die Obere Achse ist eine Hälfte des Steuerkontakts. Der Hebel endet rechts oben am BS7,5, der nämlich am Zapfen des BS15 des oberen Querlenkers sitzt.
