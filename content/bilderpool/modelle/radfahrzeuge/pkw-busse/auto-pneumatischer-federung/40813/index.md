---
layout: "image"
title: "Gesamtansicht (4)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40813
- /detailsbc3e.html
imported:
- "2019"
_4images_image_id: "40813"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40813 -->
Die Bodenfreiheit beträgt hier nur wenige mm. Alle Räder sind komplett eingefedert, da das Federungssystem drucklos ist.
