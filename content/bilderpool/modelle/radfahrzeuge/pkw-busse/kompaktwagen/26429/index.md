---
layout: "image"
title: "Kompaktwagen 12"
date: "2010-02-14T14:01:03"
picture: "Kompaktwagen_12.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26429
- /details058b-2.html
imported:
- "2019"
_4images_image_id: "26429"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26429 -->
Entfernt man das Dach, werden die Proportionen des "Cabrios" stimmiger. Ich wollte aber unbedingt ein geschlossenes Fahrzeug bauen.