---
layout: "image"
title: "Kompaktwagen 15"
date: "2010-02-14T14:01:03"
picture: "Kompaktwagen_15.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26432
- /detailsa246-2.html
imported:
- "2019"
_4images_image_id: "26432"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26432 -->
Die Aufhängung des Power-Motors und die sehr stabile Konstruktion der Kraftübertragung zum Drehschemel.