---
layout: "image"
title: "Kompaktwagen 16"
date: "2010-02-14T17:45:58"
picture: "Kompaktwagen_16.jpg"
weight: "16"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/26435
- /details6df5.html
imported:
- "2019"
_4images_image_id: "26435"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T17:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26435 -->
Nach Abnahme eines Vorderrades zeigt sich eine technische Neuerung:

Mein Drehschemel hatte immer das Problem, dass das Kegelrad am Differenzial unter hoher Belastung ratterte, da es im Gelenkstein nicht ausreichend fest geführt ist. Abhilfe schafft eine Kombination aus 2 Bausteinen 5 mit 2 Verbindungsstücken 15. Das Kegelrad stützt sich unter Last mit seiner Flanke an der Seite des Bausteins 5 ab, und ein Rattern wird vermieden! Das Verbindungsstück 15 verhindert, dass das Kegelrad nach oben ausweichen kann.

Das Abstützen funktioniert überraschend geräuschlos. Der Ergebnis ist eine sensationelle Kraftübertragung auf die Vorderachse - nichts rattert oder springt!

Das Fahrverhalten des Kompaktwagens ist typisch Frontantrieb: Die Kraft des Motors kann schlecht auf die Straße gebracht werden, da die Räder sehr schnell durchdrehen.