---
layout: "image"
title: "Mini-Frontantrieb 8"
date: "2011-01-23T20:20:34"
picture: "Mini-Frontantrieb_8.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/29789
- /details4038.html
imported:
- "2019"
_4images_image_id: "29789"
_4images_cat_id: "2189"
_4images_user_id: "328"
_4images_image_date: "2011-01-23T20:20:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29789 -->
Der Unterboden mit XS-Motor.