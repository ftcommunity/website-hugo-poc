---
layout: "comment"
hidden: true
title: "13330"
date: "2011-01-23T23:24:11"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Schon wieder so ein "rattenscharfes" Minimalmodell... Das darf ich meinen Jungs nicht zeigen, sonst ist meine letzte Fernsteuerung auch noch weg (die letzte steckt schon in Deinem Unimog ;-) ...

Beste Grüße,
Dirk