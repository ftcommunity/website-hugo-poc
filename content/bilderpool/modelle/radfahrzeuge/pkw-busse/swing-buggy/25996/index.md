---
layout: "image"
title: "unterseite"
date: "2009-12-31T13:11:36"
picture: "swingbuggy05.jpg"
weight: "5"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- /php/details/25996
- /details1627.html
imported:
- "2019"
_4images_image_id: "25996"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25996 -->
