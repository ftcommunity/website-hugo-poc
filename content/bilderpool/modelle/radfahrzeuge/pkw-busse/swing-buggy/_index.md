---
layout: "overview"
title: "Swing-Buggy"
date: 2020-02-22T07:53:41+01:00
legacy_id:
- /php/categories/1833
- /categories49ba.html
- /categories933d.html
- /categories2a41.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1833 --> 
ein Fahrzeug mit drehbaren Hinterradaufhängungen und geteiltem, gefederten Rahmen. aufgrund der vielen Bewegungsmöglichkeiten der einzelnen Komponenten zueinander entsteht bei aufeinanderfolgenden gegensätzlichen Lenkbewegungen der eindruck, daß sich das Fahrzeug richtiggehend  in die kurven schmiegt. was auf statischen Fotos leider nicht so gut darstellbar ist...