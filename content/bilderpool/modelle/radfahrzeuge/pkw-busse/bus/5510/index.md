---
layout: "image"
title: "Frontfederung_2"
date: "2005-12-20T18:26:09"
picture: "Neuer_Ordner_016.jpg"
weight: "10"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5510
- /details3280.html
imported:
- "2019"
_4images_image_id: "5510"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5510 -->
