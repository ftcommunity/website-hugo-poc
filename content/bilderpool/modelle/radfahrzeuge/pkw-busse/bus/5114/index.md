---
layout: "image"
title: "Bus1"
date: "2005-10-23T16:15:14"
picture: "IMG_0066_001.jpg"
weight: "1"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5114
- /detailse039.html
imported:
- "2019"
_4images_image_id: "5114"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-10-23T16:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5114 -->
Kurz ein paar Daten: 74cm lang, 15cm breit und 18cm hoch.

An diesem Bus wurde fast 2 Jahre gearbeitet. Die erste Version war noch um einiges schmaler und hatte hinten keine Zwillingsreifen.
