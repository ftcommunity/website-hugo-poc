---
layout: "image"
title: "Bus2"
date: "2005-10-23T16:15:14"
picture: "IMG_0067_002.jpg"
weight: "2"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5115
- /details04d5.html
imported:
- "2019"
_4images_image_id: "5115"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-10-23T16:15:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5115 -->
