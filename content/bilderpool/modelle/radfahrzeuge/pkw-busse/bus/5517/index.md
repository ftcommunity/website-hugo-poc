---
layout: "image"
title: "Tür_2"
date: "2005-12-23T15:19:27"
picture: "Neuer_Ordner_002_2.jpg"
weight: "14"
konstrukteure: 
- "Christopher Wecht (ffcoe)"
fotografen:
- "Christopher Wecht (ffcoe)"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- /php/details/5517
- /detailsef44.html
imported:
- "2019"
_4images_image_id: "5517"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-23T15:19:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5517 -->
Und sie öffnet sich...
