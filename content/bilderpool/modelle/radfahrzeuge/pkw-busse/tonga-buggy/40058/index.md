---
layout: "image"
title: "Detail Links Vorne"
date: "2014-12-30T07:14:11"
picture: "tongabuggy04.jpg"
weight: "4"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/40058
- /detailscfb0.html
imported:
- "2019"
_4images_image_id: "40058"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40058 -->
Stabile Achslagerung mit Kugellager und Schneckenmutter.
Die originale fischertechnik Feder sind oben demontiert. Nachdem die BIC-Feder im Platz sind haben die keine Funktion meher.