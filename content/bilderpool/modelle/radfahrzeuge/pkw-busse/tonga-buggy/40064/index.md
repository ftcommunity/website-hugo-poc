---
layout: "image"
title: "Tonga Buggy"
date: "2014-12-30T07:14:11"
picture: "tongabuggy10.jpg"
weight: "10"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/40064
- /detailsf9c6.html
imported:
- "2019"
_4images_image_id: "40064"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40064 -->
Tonga Buggy
Hinten ein kleines Spoiler.