---
layout: "image"
title: "Detail BIC-Federung"
date: "2014-12-30T07:14:11"
picture: "tongabuggy05.jpg"
weight: "5"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/40059
- /details7bce.html
imported:
- "2019"
_4images_image_id: "40059"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40059 -->
BIC-Federung: Spiralfeder aus einen BIC-Kugelschreiber.