---
layout: "image"
title: "auto04"
date: "2003-04-27T13:28:05"
picture: "auto04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/851
- /detailsca95-3.html
imported:
- "2019"
_4images_image_id: "851"
_4images_cat_id: "25"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T13:28:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=851 -->
Das Dreigang-Getriebe ist hier beschrieben:
http://www.ftcommunity.de/categories.php?cat_id=24
