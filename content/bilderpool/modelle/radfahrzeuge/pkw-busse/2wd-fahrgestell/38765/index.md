---
layout: "image"
title: "Hinterachse, Ansicht von Hinten"
date: "2014-05-09T17:28:26"
picture: "wdfahrgestell14.jpg"
weight: "14"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38765
- /details5c2f.html
imported:
- "2019"
_4images_image_id: "38765"
_4images_cat_id: "2897"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38765 -->
Ich habe versucht mit wenig Teile ein stabile Hinterachse zu bauen, mit wenigst möglich durchbiegung.