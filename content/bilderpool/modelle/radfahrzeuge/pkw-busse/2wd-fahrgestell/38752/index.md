---
layout: "image"
title: "2WD Fahrgestell"
date: "2014-05-09T17:28:25"
picture: "wdfahrgestell01.jpg"
weight: "1"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- /php/details/38752
- /details0681.html
imported:
- "2019"
_4images_image_id: "38752"
_4images_cat_id: "2897"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38752 -->
Diese Fahrzeug soll weiter entwickelt werden und ist gemeint ein Grund zu sein um "Fernsteuer-Teile" zu kaufen.
Es ist auch meine erste Eintrag im Bilderpool.
