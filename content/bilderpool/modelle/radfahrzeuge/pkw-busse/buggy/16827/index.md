---
layout: "image"
title: "Lenkung von innen"
date: "2008-12-31T19:10:26"
picture: "buggy10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16827
- /details8e73.html
imported:
- "2019"
_4images_image_id: "16827"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16827 -->
Hier sieht man, warum die "Motorhaube" so hoch sitzen muss. Andernfalls könnte die etwas groß geratene Lenkung nicht hinreichend einfedern.
