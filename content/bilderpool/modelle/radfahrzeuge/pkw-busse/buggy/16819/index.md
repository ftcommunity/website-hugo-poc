---
layout: "image"
title: "Seitenansicht"
date: "2008-12-31T19:10:24"
picture: "buggy02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16819
- /details1533.html
imported:
- "2019"
_4images_image_id: "16819"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16819 -->
Rechts ist übrigens vorne. ;-) Der Akku ist schräg unterhalb des Motors nur eingehängt, damit man ihn leichter wechseln kann.
