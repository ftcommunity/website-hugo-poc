---
layout: "image"
title: "Gesamtansicht"
date: "2008-12-31T19:10:23"
picture: "buggy01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/16818
- /details99d0.html
imported:
- "2019"
_4images_image_id: "16818"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16818 -->
Kaum etwas an dem Teil ist gerade, aber es fährt, lenkt und federt sehr weich.
