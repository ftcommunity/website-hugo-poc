---
layout: "image"
title: "Pick-up Ente"
date: "2010-08-29T20:56:12"
picture: "Pick-up_Ente_unterbodenleuchte_2.jpg"
weight: "1"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/28020
- /detailse05c.html
imported:
- "2019"
_4images_image_id: "28020"
_4images_cat_id: "2235"
_4images_user_id: "1177"
_4images_image_date: "2010-08-29T20:56:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28020 -->
mein modell einer ente die mit einen handgriff zu einem pick-up umgebaut werden kann 
im dunklen (+unterbodenbeleuchtung)
