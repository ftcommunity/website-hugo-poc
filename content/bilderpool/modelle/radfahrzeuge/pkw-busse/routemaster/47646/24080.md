---
layout: "comment"
hidden: true
title: "24080"
date: "2018-05-18T14:20:21"
uploadBy:
- "PHabermehl"
license: "unknown"
imported:
- "2019"
---
Ein wirklich großartiges Modell, viele tolle Ideen und wunderbar umgesetzt.

Wo ich hier gerade lese "2D-Druck-Technik", das wäre ja noch eine tolle Idee, passende Statikplatten als Formteile zu konstruieren und 3D zu drucken...