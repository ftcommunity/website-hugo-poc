---
layout: "image"
title: "Routemaster"
date: "2018-05-13T08:59:58"
picture: "Routemaster_-_7_of_14.jpg"
weight: "7"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- /php/details/47652
- /details834d.html
imported:
- "2019"
_4images_image_id: "47652"
_4images_cat_id: "3513"
_4images_user_id: "2739"
_4images_image_date: "2018-05-13T08:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47652 -->
