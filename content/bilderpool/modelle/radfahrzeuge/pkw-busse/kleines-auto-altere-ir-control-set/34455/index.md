---
layout: "image"
title: "Blick von oben"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34455
- /details5451.html
imported:
- "2019"
_4images_image_id: "34455"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34455 -->
Das ganze Verdeck ist leicht nach hinten klappbar, wo es nur von zwei Statikstopfen gehalten wird. Der graue Mini-Mot in Bildmitte ist für die Lenkung zuständig; der Taster im Frontbereich tastet die Mittelstellung ab.

Der MiniMot sitzt per zwei BS5 direkt auf dem darunter liegenden S-Motor für den Antrieb. Die vom Lenkmotor angetriebene K-Achse sitzt nicht exakt mittig, sondern etwas weiter in Fahrtrichtung rechts, damit das mit der kleinen Schnecke genau aufgeht.
