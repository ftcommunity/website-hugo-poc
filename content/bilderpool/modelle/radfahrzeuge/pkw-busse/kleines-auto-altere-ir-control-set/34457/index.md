---
layout: "image"
title: "Lenkung (2)"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34457
- /detailsd807.html
imported:
- "2019"
_4images_image_id: "34457"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34457 -->
Für diese Aufnahme ist der linke Vorderreifen abgenommen.

Die K-Achse geht weiter mit einem Abstandshalter, zwei Riegelscheiben und einer Klemmbuchse 8, bevor sie im BS15 mit Loch endet. Die beiden Riegelscheiben sind welche, die stramm sitzen (herausgesucht) und die Go-Kart-Lenkung betreiben.
