---
layout: "image"
title: "Lenkung (3)"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/34458
- /details13b9-2.html
imported:
- "2019"
_4images_image_id: "34458"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34458 -->
Der Klemmring 8 ist wichtig, weil in seiner Öffnung der obenliegende Taster eingreift. Das ist so justiert, dass der Taster in Geradeausstellung der Lenkung gerade losgelassen wird, weil die Öffnung oben liegt. Dadurch kann man die Sache praktisch genau so anschließen wie beim IR-Control-Set vorgesehen.
