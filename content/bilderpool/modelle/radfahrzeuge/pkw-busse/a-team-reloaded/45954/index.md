---
layout: "image"
title: "Draufsicht"
date: "2017-06-18T18:00:36"
picture: "ateamreloaded03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45954
- /details32f9.html
imported:
- "2019"
_4images_image_id: "45954"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45954 -->
Das Original hat zwei Hecktüren, die ich durch eine Heckklappe ersetzt habe (Beleuchtung und Scharniere gehen einfach nicht zusammen an die Heckkante). Rechts gibt es eine Schiebetür und mitten drunter einen doppelten Boden mit einem gewaltig großen Geheimfach.
