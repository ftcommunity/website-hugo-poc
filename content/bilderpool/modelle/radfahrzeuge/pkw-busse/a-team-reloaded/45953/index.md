---
layout: "image"
title: "Frontansicht"
date: "2017-06-18T18:00:36"
picture: "ateamreloaded02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45953
- /details26e2.html
imported:
- "2019"
_4images_image_id: "45953"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45953 -->
Dank Frontantrieb bleibt die Technik vorn konzentriert und lässt reichlich Platz im hinteren Teil.
