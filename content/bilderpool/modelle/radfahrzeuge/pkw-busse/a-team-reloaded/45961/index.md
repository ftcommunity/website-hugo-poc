---
layout: "image"
title: "Motorblock"
date: "2017-06-18T18:00:37"
picture: "ateamreloaded10.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45961
- /detailsf1b7-2.html
imported:
- "2019"
_4images_image_id: "45961"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45961 -->
Der Antrieb ist auf vier Seiten um den Motor herum geschlungen. Die Rast-Z20 sind im Fahrzeug nach hinten weg eingebaut. Die Z20 mit Nabe treiben 2:1 je ein Z10 an, das dann über ein Rastkegelrad auf den Reifen 60 führt.

Wenn man das obere Rast-Z20 durch ein Differenzial ersetzt, hat man schon den Anfang für ein Allrad-Vehikel...
