---
layout: "image"
title: "Von unten (Gesamtansicht)"
date: "2006-10-02T16:16:38"
picture: "allradcitroen07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7089
- /details4cc1-2.html
imported:
- "2019"
_4images_image_id: "7089"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7089 -->
Der hintere PowerMotor treibt die Hinterachse an. Details der Vorderachse sieht man im nächsten Bild.
