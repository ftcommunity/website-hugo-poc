---
layout: "image"
title: "Antrieb vorne"
date: "2006-10-02T16:28:11"
picture: "allradcitroen13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7095
- /details503e-2.html
imported:
- "2019"
_4images_image_id: "7095"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7095 -->
Man blickt hier von rechts durch das Seitenfenster. Das hier sichtbare Differential wird von einem dahinter liegenden, quer zur Fahrtrichtung eingebauten PowerMot angetrieben und verteilt die Kraft auf zwei nach längs zur  Fahrtrichtung nach vorne laufenden Achsen. Die hier rechte Achse des Differentials mündet direkt in der Kardanwelle für den Antrieb des linken Vorderrades, die man im vorigen Bild sah. Die linke Achse des Differentials geht über zwei zwischen-Z10 auf eine der drei Metallachsen, die ganz durch den Vorderbau laufen und dort wieder über zwischen-Z10 auf der Kardanwelle für den Antrieb des rechten Vorderrades landen.

Die beiden PowerMots für die Vorder- und Hinterachse sind in Serie geschaltet und bilden damit ein "elektrisches Differential". Dazu hat chevyfahrer ein Video in die ft Community gestellt - Danke für die Idee an chevyfahrer. Zunächst hatte ich alles mit nur einem Motor angetrieben. Das ging ja noch. Als ich dann auch noch ein elektrisch schaltbares Zweiganggetriebe einbauen wollte, waren die Reibungsverluste aber einfach zu groß. Mit dieser Lösung hier baut es sich nicht nur Reibungs- sondern auch Teile- und damit Gewichtssparender.
