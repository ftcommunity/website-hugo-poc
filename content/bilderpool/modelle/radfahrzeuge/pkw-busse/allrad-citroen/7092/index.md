---
layout: "image"
title: "Vorderachse von unten (2)"
date: "2006-10-02T16:16:38"
picture: "allradcitroen10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7092
- /detailsbf73.html
imported:
- "2019"
_4images_image_id: "7092"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7092 -->
Man sieht hier sehr gut die Achillesferse der Vorderachse: Die roten BS15 mit Loch, an dem die Vorderräder aufgehängt sind, sind unten lediglich mit einem S-Riegel aufgehängt. Damit der bei der Fahrt nicht herausrutscht, stecken noch je 2 Federnocken darin. Allerdings müsste hier etwas Stabileres hin, der Riegel rutscht nämlich doch manchmal heraus.
