---
layout: "image"
title: "Von hinten (2)"
date: "2006-10-02T16:16:38"
picture: "allradcitroen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7087
- /detailsd194-2.html
imported:
- "2019"
_4images_image_id: "7087"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7087 -->
Mit geöffneter Heckklappe. Das gibt einen scheuen Blick auf die hintere Niveauregulierung frei.
