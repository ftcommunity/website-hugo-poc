---
layout: "image"
title: "Lenkung"
date: "2006-10-02T16:28:11"
picture: "allradcitroen12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/7094
- /detailsedd1-2.html
imported:
- "2019"
_4images_image_id: "7094"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7094 -->
Etwa in Bildmitte sieht man eine Schnecke, die vom MiniMot links unten (teilweise sichtbar) angetrieben wird. Die zieht den Block in der Mitte, an der die Lenkstangen (gelbe Streben) aufgehängt sind. Links und rechts Endschalter, fertig.

Ebenfalls sichtbar: Die Kardanwelle für den Antrieb des hier rechten, in Fahrtrichtung gesehen aber linken Reibrades für das linke Vorderrad.

Die Mimik mit den alten Seilrollen (hier am unteren Bildrand) ist kurz vor der Lenkungsschnecke an Gelenken aufgehängt. In den Seilrollen stecken Metallachsen, die vorne über Streben verbunden sind. An diesen Streben hängt mittig der vorher beschriebene Kontakt für die Feststellung der aktuellen Bodenfreiheit. Dort wird auch (ebenfalls mittig in den Streben) mit dem Gummi der vorderen Niveauregulierung gezogen, um die Federung stärker oder schwächer einzustellen.

Kurz vor den Seilrollen (hier gerade noch sichtbar) liegen die dazugehörenden Klemmräder, die sich auf den Radaufhängungen abstützen und so das Rad nach unten drücken können.
