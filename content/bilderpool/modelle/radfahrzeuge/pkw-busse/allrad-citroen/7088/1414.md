---
layout: "comment"
hidden: true
title: "1414"
date: "2006-10-02T16:24:14"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Und rechts unten sieht man, warum die Niveauregulierung in Mörshausen nicht so richtig mitspielen wollte und eher knatterte: das schwarze ft kommt mir deutlich elastischer vor als rot oder grau.

Hut ab vor der unkonventionellen Lösung rund um die Vorderachse. Ich staune immer wieder.