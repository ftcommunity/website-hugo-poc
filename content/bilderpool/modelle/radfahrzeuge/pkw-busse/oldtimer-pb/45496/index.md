---
layout: "image"
title: "oldtimer03.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer03.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45496
- /details3a7b.html
imported:
- "2019"
_4images_image_id: "45496"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45496 -->
