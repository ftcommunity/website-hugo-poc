---
layout: "image"
title: "oldtimer19.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer19.jpg"
weight: "19"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45512
- /detailse863-2.html
imported:
- "2019"
_4images_image_id: "45512"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45512 -->
