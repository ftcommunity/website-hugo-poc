---
layout: "image"
title: "oldtimer04.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45497
- /detailsb8e7-3.html
imported:
- "2019"
_4images_image_id: "45497"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45497 -->
Jetzt würde mann für die Lenkung wohl den Servo nützen. Die gabs damals noch nicht. Die Kette hat 32 Glieder.