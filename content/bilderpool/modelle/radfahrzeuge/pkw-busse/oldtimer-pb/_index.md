---
layout: "overview"
title: "Oldtimer (PB)"
date: 2020-02-22T07:54:13+01:00
legacy_id:
- /php/categories/3382
- /categoriesb826-2.html
- /categorieseb15-2.html
- /categoriesf096-2.html
- /categories98dd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3382 --> 
Ein alteres Model, das damals auch in Niederländischen 'FT-clubblad' (FT-clubmagazin) erschienen ist. Es ist ferngesteuert (mit den älteren IR-set) und gefedert. Die großen Räder der Dampfmaschine gaben mir den Idee hierfür. Die alten Raupen-gummi's (??) passten gut da herum, damit den Auto 'Grip', Traktion, hat.