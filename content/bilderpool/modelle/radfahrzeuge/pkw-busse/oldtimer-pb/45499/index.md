---
layout: "image"
title: "oldtimer06.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/45499
- /details2d91.html
imported:
- "2019"
_4images_image_id: "45499"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45499 -->
Bei mir schiebt ein Einzige BS15 immer hin und wieder auf den Hubstang die hier vor das Lenken eingesetzt ist. Deswegen habe ich hier eine Reihe BS genützt und die an Begin und Ende fest gesetzt mit Klemmstücke 15mm.