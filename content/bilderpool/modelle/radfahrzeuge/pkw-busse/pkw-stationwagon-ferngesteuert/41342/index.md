---
layout: "image"
title: "Station-22"
date: "2015-06-29T23:51:57"
picture: "pkwstation25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41342
- /detailse276.html
imported:
- "2019"
_4images_image_id: "41342"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41342 -->
