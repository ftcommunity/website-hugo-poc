---
layout: "image"
title: "PKW-station-21"
date: "2015-06-29T23:51:57"
picture: "pkwstation18.jpg"
weight: "18"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41335
- /detailsef55-3.html
imported:
- "2019"
_4images_image_id: "41335"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41335 -->
