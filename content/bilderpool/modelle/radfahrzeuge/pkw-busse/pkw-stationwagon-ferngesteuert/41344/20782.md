---
layout: "comment"
hidden: true
title: "20782"
date: "2015-06-30T00:04:43"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hmm... für ein Lenktrapez nach Ackermann (--> https://de.wikipedia.org/wiki/Lenkung#Achsschenkellenkung )  müssten die Anlenkhebel aber anders herum eingebaut werden.

Auf die Idee mit dem längs liegenden U-Träger werde ich bestimmt mal zurückkommen... 

Gruß,
Harald