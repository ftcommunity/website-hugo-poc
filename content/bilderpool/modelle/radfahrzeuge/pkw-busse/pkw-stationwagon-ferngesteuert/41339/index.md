---
layout: "image"
title: "PKW-station-25"
date: "2015-06-29T23:51:57"
picture: "pkwstation22.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41339
- /details738f.html
imported:
- "2019"
_4images_image_id: "41339"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41339 -->
Mit den IR-Augen nach Oben reagiert er gut auf die Signalen der Sender.