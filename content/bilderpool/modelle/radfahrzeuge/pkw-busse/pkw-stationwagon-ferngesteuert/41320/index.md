---
layout: "image"
title: "PKW-station-03"
date: "2015-06-29T23:51:40"
picture: "pkwstation03.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41320
- /details38bf.html
imported:
- "2019"
_4images_image_id: "41320"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41320 -->
