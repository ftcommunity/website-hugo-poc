---
layout: "image"
title: "Von hinten"
date: "2011-11-06T22:46:33"
picture: "ueberschlagfahrzeug7.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/33428
- /details9c5a.html
imported:
- "2019"
_4images_image_id: "33428"
_4images_cat_id: "2477"
_4images_user_id: "1122"
_4images_image_date: "2011-11-06T22:46:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33428 -->
Den Controler konnte ich vorne nicht anbringen, weil der Controler sonst bei starken Bremsen oder Überschlagen am Boden hängen bleiben würde.