---
layout: "image"
title: "Die Direkt-Übertragung"
date: "2011-11-06T22:46:33"
picture: "ueberschlagfahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/33425
- /details6d33.html
imported:
- "2019"
_4images_image_id: "33425"
_4images_cat_id: "2477"
_4images_user_id: "1122"
_4images_image_date: "2011-11-06T22:46:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33425 -->
-