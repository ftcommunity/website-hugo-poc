---
layout: "image"
title: "Von vorne"
date: "2011-11-06T22:46:33"
picture: "ueberschlagfahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/33422
- /details5a28.html
imported:
- "2019"
_4images_image_id: "33422"
_4images_cat_id: "2477"
_4images_user_id: "1122"
_4images_image_date: "2011-11-06T22:46:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33422 -->
Hier kann man die Beiden Mini-Motoren gut sehen.