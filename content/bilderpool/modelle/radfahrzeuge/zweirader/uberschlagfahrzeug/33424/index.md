---
layout: "image"
title: "Ohne Akku"
date: "2011-11-06T22:46:33"
picture: "ueberschlagfahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/33424
- /detailsdb82-2.html
imported:
- "2019"
_4images_image_id: "33424"
_4images_cat_id: "2477"
_4images_user_id: "1122"
_4images_image_date: "2011-11-06T22:46:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33424 -->
Hier kann man die Direkt-Übertragung der Motoren zu den Reifen sehen.