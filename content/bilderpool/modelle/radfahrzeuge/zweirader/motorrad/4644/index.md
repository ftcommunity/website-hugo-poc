---
layout: "image"
title: "Motorrad 7"
date: "2005-08-22T20:07:00"
picture: "Motorrad_7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/4644
- /detailsc4e7-2.html
imported:
- "2019"
_4images_image_id: "4644"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4644 -->
Hier nochmal die Federgabel mit demontiertem Scheinwerfer in ihrer ganzen Pracht. Durch die lose Lagerung gleitet sie sehr leicht.