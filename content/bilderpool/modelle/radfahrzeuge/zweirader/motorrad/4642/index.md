---
layout: "image"
title: "Motorrad 5"
date: "2005-08-22T20:07:00"
picture: "Motorrad_5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/4642
- /detailsae15.html
imported:
- "2019"
_4images_image_id: "4642"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4642 -->
Das Vorderrad ist an einer Federgabel aufgehängt. Um "im Alltag" ein problemlosen Gleiten der beiden Kunststoffachsen in den Bausteinen 30 zu ermöglichen, dürfen diese nicht starr miteinander verbunden werden. Z.B. ein Baustein 15 dazwischen würde auch nach mehrfacher Feinjustage zum Klemmen führen.