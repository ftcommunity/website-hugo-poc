---
layout: "overview"
title: "Motorrad (vollgefedert)"
date: 2020-02-22T07:57:09+01:00
legacy_id:
- /php/categories/2756
- /categories143e.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2756 --> 
Hier sieht man ein vollgefdertes Motorrad, das man fernbedienen kann. Ich wollte alle Bauteile, die man zur Fernsteuerung benötigt, platzsparend unterbringen.
Ein Video kann man hier sehen: http://home.versanet.de/~udohenkel/motorrad.mp4  (Ich habe gefilmt und gesteuert, deshalb die schlechte Bildführung)