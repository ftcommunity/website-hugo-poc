---
layout: "image"
title: "Motorrad (linke Seite)"
date: "2013-07-02T08:30:08"
picture: "motorradvollgefedert2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/37135
- /detailsb6a8-2.html
imported:
- "2019"
_4images_image_id: "37135"
_4images_cat_id: "2756"
_4images_user_id: "1112"
_4images_image_date: "2013-07-02T08:30:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37135 -->
Leider hat es Stützräder. Ohne diese ist es nicht fahrbar, da kein Kreiselsystem (Gyro) vorhanden ist. Vielleicht hat da jemand eine Idee, wie man das bauen könnte. Die Kette wird durch die vertikale und horizontale Feder am Hinterrad gespannt.