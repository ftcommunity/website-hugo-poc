---
layout: "image"
title: "Motorrad (Servo Elektronik)"
date: "2013-07-02T08:30:08"
picture: "motorradvollgefedert6.jpg"
weight: "6"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/37139
- /detailsf6d9.html
imported:
- "2019"
_4images_image_id: "37139"
_4images_cat_id: "2756"
_4images_user_id: "1112"
_4images_image_date: "2013-07-02T08:30:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37139 -->
Um das Motorrad richtig steuern zu können, musste ich das Servo modifizieren. Die Polarität des Motors und der Endschalter musste vertauscht werden. Ansonsten ist es eine gute Übung zu fahren. (nach links steuern und nach rechts abbiegen)