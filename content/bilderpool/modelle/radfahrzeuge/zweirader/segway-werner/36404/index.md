---
layout: "image"
title: "Segway rechts"
date: "2013-01-04T14:44:53"
picture: "segwaymitabstandssensor3.jpg"
weight: "3"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36404
- /detailsed8c-2.html
imported:
- "2019"
_4images_image_id: "36404"
_4images_cat_id: "2702"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T14:44:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36404 -->
Das Empfängermodul dient der manuellen Steuerung
