---
layout: "image"
title: "Segway links"
date: "2013-01-04T14:44:53"
picture: "segwaymitabstandssensor2.jpg"
weight: "2"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36403
- /detailsb19d.html
imported:
- "2019"
_4images_image_id: "36403"
_4images_cat_id: "2702"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T14:44:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36403 -->
Das Problem ist, dass der Abstandssensor nur Werte in 1cm-Schritten ausgibt
Deshalb ist der Abstandssensor an einer langen Stange, um die Genauigkeit zu erhöhen
