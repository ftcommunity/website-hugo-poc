---
layout: "image"
title: "Segway Gesamtansicht"
date: "2013-01-04T14:44:53"
picture: "segwaymitabstandssensor1.jpg"
weight: "1"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36402
- /detailsc0b9.html
imported:
- "2019"
_4images_image_id: "36402"
_4images_cat_id: "2702"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T14:44:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36402 -->
Der kurze V-Träger dient nur als Stütze, wenn das Segway nicht balanciert
An dem längeren V-Träger kann man das Segway nach vorne/hinten fahren lassen
