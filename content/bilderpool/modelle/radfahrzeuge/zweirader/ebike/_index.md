---
layout: "overview"
title: "eBike"
date: 2020-02-22T07:57:10+01:00
legacy_id:
- /php/categories/3042
- /categories91a1.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3042 --> 
Elektrisch angetriebenes Motorrad mit Servo-Lenkung, das sein Gleichgewicht halten soll. In das rote Gepäckteil (9V-Batteriehalter von ft)  kommt die Elektronik bestehend aus: Arduino Nano (18 x 45 mm), GY-86 mit MPU6050 (3-Achs-Gyro + Beschleunigungssensor) und Selbstbau-Board zum Ansteuern des Lenk-Servos und des Antriebsmotors. Kippt das Bike nach rechts, lenkt es nach rechts und beschleunigt etc...