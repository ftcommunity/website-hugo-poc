---
layout: "image"
title: "Gepäckträger mit Steuer-Elektronik"
date: "2015-02-19T18:44:53"
picture: "Gepaecktraeger.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["GY-86", "Arduino", "Nano", "MPU6050", "I2C", "TWI", "Servo", "Bike", "Motorrad", "eBike"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/40573
- /details7ef1.html
imported:
- "2019"
_4images_image_id: "40573"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-02-19T18:44:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40573 -->
Hier sieht man den Gepäckträger (9V-Batteriehalter von FT), der die Elektronik zur Steuerung aufnehmen soll und die Elektronik bestehend aus:
- Arduino Nano (18 x 45 mm)
- GY-86 mit MPU6050 (3-Achs-Gyro + Beschleunigungssensor)
- Selbstbau-Board zum Ansteuern des Lenk-Servos und des Antriebsmotors
Das GY-86 ist hier schon Huckepack auf den Arduino gelötet, Verbindung über I2C-Bus.
Das Selbstbauboard hat einen 5V-Spannungsregler (unten im Bild) für den Servo-Motor, einen MOSFET und Freilaufdiode für den Antriebsmotor und einen Infrarot-Fernbedienungsempänger (Anschluß an einem Interrupt-Pin des Arduino). Der Servo-Motor und der Antriebsmotor werden mit 16-bit-PWM über Timer1 vom Arduino-Board gesteuert.