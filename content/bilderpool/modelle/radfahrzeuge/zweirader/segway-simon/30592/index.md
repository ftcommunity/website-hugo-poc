---
layout: "image"
title: "Programm2 von Segway"
date: "2011-05-21T18:37:28"
picture: "neues_Hauptprogramm2_Segway.jpg"
weight: "4"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
schlagworte: ["Segway", "Programm", "2"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- /php/details/30592
- /details6168-2.html
imported:
- "2019"
_4images_image_id: "30592"
_4images_cat_id: "2281"
_4images_user_id: "-1"
_4images_image_date: "2011-05-21T18:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30592 -->
weiterentwikeltes Programm