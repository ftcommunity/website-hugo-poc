---
layout: "image"
title: "Antrieb"
date: "2017-12-10T22:55:39"
picture: "moechtegernsegwayfuerakkutestundspiel3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46977
- /details6beb.html
imported:
- "2019"
_4images_image_id: "46977"
_4images_cat_id: "3476"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:55:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46977 -->
Der Antrieb ist simpel: Z10, Z30 und drauf aufs Speichenrad. Das ist ein 1:10-Powermotor, der dreht auch flott.
