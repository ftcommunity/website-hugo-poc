---
layout: "overview"
title: "Möchtegern-Segway für Akku-Test und Spiel"
date: 2020-02-22T07:57:12+01:00
legacy_id:
- /php/categories/3476
- /categories3c74.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3476 --> 
Ein Gerät für Geschicklichkeitsspiele, gebaut für den Test eines Fan-Selbstbau-Akkus.