---
layout: "image"
title: "IR-Modul"
date: "2017-12-10T22:55:39"
picture: "moechtegernsegwayfuerakkutestundspiel4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46978
- /detailscf04.html
imported:
- "2019"
_4images_image_id: "46978"
_4images_cat_id: "3476"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:55:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46978 -->
Oberhalb (wir schauen ja gerade "von unten") des anderen Motors sitzt der IR-Empfänger.
