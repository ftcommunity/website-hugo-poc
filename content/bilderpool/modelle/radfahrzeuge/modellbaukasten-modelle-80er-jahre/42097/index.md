---
layout: "image"
title: "Maschinerie Kranteil Seilwinden"
date: "2015-10-18T18:20:08"
picture: "IMG_1002.jpg"
weight: "7"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Supermodell", "Teleskop", "Mobil-Kran", "30474", "1983", "Seilwinde"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42097
- /detailsd4a6.html
imported:
- "2019"
_4images_image_id: "42097"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T18:20:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42097 -->
Seilwinden und Aufbau des Kranteils
