---
layout: "image"
title: "Hebebühne ausgfahren (von hinten)"
date: "2015-10-18T15:42:43"
picture: "IMG_1083_copy.jpg"
weight: "1"
konstrukteure: 
- "Jens bzw FT"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["80er","Universalfahrzeug", "30481", "1984"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42090
- /details7eec.html
imported:
- "2019"
_4images_image_id: "42090"
_4images_cat_id: "3133"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42090 -->
Universalfahrzeug (30481, 1984)
