---
layout: "comment"
hidden: true
title: "21131"
date: "2015-10-20T19:42:49"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
Ist ja interessant - ich konnte den Teleskop Mobilkran immer nur nachbauen, von daher bin ich echt überrascht, welche Feinheiten da verbaut wurden. Auch das weiße Seil war mir bis dato unbekannt, und die Halbschalen für die Seilwinde waren da auch nie drin, sondern im Container-Kran verbaut.
Das ist ja das Schöne an so einer Comunity, dass man dieses gesammelte Wissen bündelt :-) also ausdrücklich "Danke" für jede Information!  

Gruß
Jens