---
layout: "comment"
hidden: true
title: "21126"
date: "2015-10-19T18:20:10"
uploadBy:
- "PHabermehl"
license: "unknown"
imported:
- "2019"
---
Hallo,

die grauen Seilwinden-Halbschalen hat meiner nicht, und in Stückliste und Bauanleitung in der Datenbank finde ich sie auch nicht, auf Deinen Bildern auch nicht. Was hat es damit auf sich?

Außerdem gehört zu dem Modell weiße 1-mm-Leine für die gesamte Bespannung, die ist auch recht rar.

Ist zugegebenermaßen jetzt Kümmelspalterei, aber bei den alten Modellen ist es wichtig, auf Originalität zu achten, weil da bei eBay und Co. leider oft versucht wird, bunt zusammengestoppelte Replikas teuer an den Mann zu bringen....

Gruß
Peter