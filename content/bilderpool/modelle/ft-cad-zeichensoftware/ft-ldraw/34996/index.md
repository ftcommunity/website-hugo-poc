---
layout: "image"
title: "Geheimschloss 3"
date: "2012-05-22T16:10:24"
picture: "Geheimschloss_3.jpg"
weight: "133"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34996
- /details979f-2.html
imported:
- "2019"
_4images_image_id: "34996"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-05-22T16:10:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34996 -->
aus "Elektromechanik" S.48ff