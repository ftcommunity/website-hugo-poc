---
layout: "image"
title: "Stirnradgetriebe 1 (aus Hobby 2 Band 1 S.13)"
date: "2011-07-30T20:48:16"
picture: "Stirnradgetriebe_01.jpg"
weight: "33"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31484
- /details9930.html
imported:
- "2019"
_4images_image_id: "31484"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-30T20:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31484 -->
