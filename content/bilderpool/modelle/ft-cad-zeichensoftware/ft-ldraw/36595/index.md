---
layout: "image"
title: "Spielautomat 4"
date: "2013-02-10T15:48:13"
picture: "Spielautomat_Inet4.jpg"
weight: "203"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36595
- /detailse3ce-2.html
imported:
- "2019"
_4images_image_id: "36595"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-02-10T15:48:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36595 -->
Nachbau des Spielautomaten "Club-Modell 3/1979" mit einigen Modifikationen insbesondere beim Antrieb.