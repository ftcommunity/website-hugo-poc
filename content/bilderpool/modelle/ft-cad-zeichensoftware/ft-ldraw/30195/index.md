---
layout: "image"
title: "3Achs Rob_2"
date: "2011-03-04T09:23:07"
picture: "3Achs_Rob_2_2.jpg"
weight: "2"
konstrukteure: 
- "??"
fotografen:
- "con.barriga"
schlagworte: ["3Achs", "Rob_2"]
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30195
- /detailsa090.html
imported:
- "2019"
_4images_image_id: "30195"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-03-04T09:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30195 -->
Teile einer Bauanleitung für einen 3-Achs Rob