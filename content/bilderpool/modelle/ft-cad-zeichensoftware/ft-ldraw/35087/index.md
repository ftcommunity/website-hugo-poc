---
layout: "image"
title: "4-Takt-Motor Übersicht"
date: "2012-07-01T12:53:11"
picture: "Vier-TaktMotor_05.jpg"
weight: "141"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35087
- /details328b.html
imported:
- "2019"
_4images_image_id: "35087"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-07-01T12:53:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35087 -->
Modell nach der (nicht ganz so deutlichen) ft-Club Bauanleitung von 1978 

(Deutlichere) Anleitung als PDF (5 MB) kann bei mir per e-mail angefordert werden.