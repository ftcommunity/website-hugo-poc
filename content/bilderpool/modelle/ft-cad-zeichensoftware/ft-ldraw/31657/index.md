---
layout: "image"
title: "Manipulator 10"
date: "2011-08-28T18:26:04"
picture: "Manipulator_LPub_page_10.jpg"
weight: "60"
konstrukteure: 
- "ltam"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31657
- /detailse622-3.html
imported:
- "2019"
_4images_image_id: "31657"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-28T18:26:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31657 -->
