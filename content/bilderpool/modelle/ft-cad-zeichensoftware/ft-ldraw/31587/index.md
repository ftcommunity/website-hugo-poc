---
layout: "image"
title: "Pneum. Bagger 03"
date: "2011-08-16T21:34:57"
picture: "Pneu_Bagger_03.jpg"
weight: "51"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31587
- /details98ae.html
imported:
- "2019"
_4images_image_id: "31587"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-16T21:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31587 -->
