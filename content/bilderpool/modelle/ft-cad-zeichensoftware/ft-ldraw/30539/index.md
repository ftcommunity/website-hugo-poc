---
layout: "image"
title: "HR-Rob Säule_1"
date: "2011-05-08T19:29:19"
picture: "Hochregalrobot_01.jpg"
weight: "8"
konstrukteure: 
- "???"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30539
- /details254e.html
imported:
- "2019"
_4images_image_id: "30539"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-05-08T19:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30539 -->
Alternative für Säule des neuen Hochregal-Roboters. Der Encoder-Motor scheint zu schwach, um die Säule zuverlässig aufwärts bewegen zu können