---
layout: "comment"
hidden: true
title: "15571"
date: "2011-10-28T17:01:01"
uploadBy:
- "con.barriga"
license: "unknown"
imported:
- "2019"
---
@Stefan
Vielen Dank für die Anerkennung und die "moralische" Unterstützung bei den Weglassungen. 
Der Satz von Thomas war aber bestimmt nur als gutmütige Spöttelei gemeint.

Gruß an alle, con.barriga