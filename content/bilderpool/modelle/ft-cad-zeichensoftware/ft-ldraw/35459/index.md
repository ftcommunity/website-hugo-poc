---
layout: "image"
title: "Super Loop 3"
date: "2012-09-04T19:52:53"
picture: "Superloop_03.jpg"
weight: "149"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35459
- /details7e35.html
imported:
- "2019"
_4images_image_id: "35459"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-04T19:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35459 -->
Das Fan-Club Modell Nr. 4 mit kleinen Modifikationen bei Antrieb und Kabine.