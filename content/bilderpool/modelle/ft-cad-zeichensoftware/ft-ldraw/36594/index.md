---
layout: "image"
title: "Warenautomat"
date: "2013-02-10T15:48:13"
picture: "Warenautomat_Inet1.jpg"
weight: "202"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36594
- /detailsa16f.html
imported:
- "2019"
_4images_image_id: "36594"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-02-10T15:48:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36594 -->
Der Warenautomat zum Nachbau des Spielautomaten "Club-Modell 3/1979".