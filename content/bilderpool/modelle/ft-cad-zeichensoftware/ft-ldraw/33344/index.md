---
layout: "image"
title: "Uhr Club Modell 31 1977_01"
date: "2011-10-27T16:02:45"
picture: "Uhr_Club_Modell_1977_01.jpg"
weight: "76"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/33344
- /details3eb9-2.html
imported:
- "2019"
_4images_image_id: "33344"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-10-27T16:02:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33344 -->
Diese "antike" Fischertechnik Uhr fristet in der Kopie des Clubheftes doch ein arg graues Dasein.