---
layout: "image"
title: "Papierhalter für alten Scanner -  Antriebsseite"
date: "2011-06-12T11:17:05"
picture: "Schmalseite_Antrieb_angehoben.jpg"
weight: "19"
konstrukteure: 
- "con.barriga"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30852
- /detailsf44f.html
imported:
- "2019"
_4images_image_id: "30852"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-06-12T11:17:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30852 -->
Mit einem an die gekennzeichneten Bausteine angeklebten Filzstreifen hält das Papier ausreichend fest.