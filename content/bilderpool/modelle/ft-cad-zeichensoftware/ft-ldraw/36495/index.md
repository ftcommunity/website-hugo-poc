---
layout: "image"
title: "modifizierter Warenautomat 7"
date: "2013-01-14T10:57:06"
picture: "Warenautomat_Inet_7.jpg"
weight: "193"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36495
- /details227b-3.html
imported:
- "2019"
_4images_image_id: "36495"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-14T10:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36495 -->
Geändert wurden:
    der Warenturm (es passen jetzt süße kleine Schokotäfelchen hinein), 
    der Schieber, 
    der Münzprüfer (lässt nur 5 Centmünzen durch) und
    die Steuerung (durch ein Interface).

Der komplette Aufbau. Die vierte Lichtschranke dient in Kombination mit der sich über dem rechten Behältnis befindlichen der Positionierung des Hubmotors.
Diese Lichtschranke über dem rechten Behältnis erkennt außerdem, ob eine "falsche" Münze eingeworfen wurde.