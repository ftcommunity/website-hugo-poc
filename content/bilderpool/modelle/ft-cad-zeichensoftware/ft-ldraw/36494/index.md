---
layout: "image"
title: "modifizierter Warenautomat 6"
date: "2013-01-14T10:57:06"
picture: "Warenautomat_Inet_6.jpg"
weight: "192"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36494
- /details0e8d-2.html
imported:
- "2019"
_4images_image_id: "36494"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-14T10:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36494 -->
Geändert wurden:
    der Warenturm (es passen jetzt süße kleine Schokotäfelchen hinein), 
    der Schieber, 
    der Münzprüfer (lässt nur 5 Centmünzen durch) und
    die Steuerung (durch ein Interface).

Wurde eine 5-Cent Münze erkannt, hebt das Hubgetriebe und "Anhang" die Münze aus dem Spalt und sie rollt weiter bis zur Lichtschranke bzw. in das linke Behältnis.