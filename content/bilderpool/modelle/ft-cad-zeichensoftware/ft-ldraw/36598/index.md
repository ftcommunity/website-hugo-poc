---
layout: "image"
title: "Spielautomat 1"
date: "2013-02-10T15:48:13"
picture: "Spielautomat_Inet1.jpg"
weight: "206"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36598
- /detailsc63c-2.html
imported:
- "2019"
_4images_image_id: "36598"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-02-10T15:48:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36598 -->
Nachbau des Spielautomaten "Club-Modell 3/1979" mit einigen Modifikationen insbesondere beim Antrieb.
Zusammen mit dem gezeigten Geldeinwurf, dem Warenautomaten und einem Interface mit Erweiterung eine interessante Programmieraufgabe. (Je nach Inhalt des Warenautomaten mit Belohnung).