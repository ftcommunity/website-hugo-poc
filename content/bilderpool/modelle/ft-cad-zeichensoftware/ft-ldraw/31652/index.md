---
layout: "image"
title: "Manipulator"
date: "2011-08-28T18:26:04"
picture: "Manipulator.jpg"
weight: "55"
konstrukteure: 
- "ltam"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31652
- /detailsa5ac.html
imported:
- "2019"
_4images_image_id: "31652"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-28T18:26:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31652 -->
Auf der Seite  http://www.ltam.lu/fischertechnik/Frameset-1.htm  findet sich (neben vielem anderem) das nachfolgend dargestellte Modell eines Teilewenders.
Neben einer Überblicksdarstellung habe ich auch einzelne Bauabschnitte dargestellt (so etwa jeden 2. Schritt). Vielleicht kann man sich ja die Zwischenschritte "zusammenreimen".