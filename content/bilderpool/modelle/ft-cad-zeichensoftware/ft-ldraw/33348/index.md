---
layout: "image"
title: "Uhr Club Modell 31 1977_05"
date: "2011-10-27T16:02:46"
picture: "Uhr_Club_Modell_1977_05.jpg"
weight: "80"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/33348
- /details723c-2.html
imported:
- "2019"
_4images_image_id: "33348"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-10-27T16:02:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33348 -->
