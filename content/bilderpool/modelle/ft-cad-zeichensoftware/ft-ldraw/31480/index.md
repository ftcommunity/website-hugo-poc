---
layout: "image"
title: "Variante des Stifthalters für ft-Scanner 4"
date: "2011-07-30T00:52:45"
picture: "Schreibkopf_LPub_page_4.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31480
- /details15c1.html
imported:
- "2019"
_4images_image_id: "31480"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-30T00:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31480 -->
