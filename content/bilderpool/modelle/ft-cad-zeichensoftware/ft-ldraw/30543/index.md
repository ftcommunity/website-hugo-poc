---
layout: "image"
title: "HR-Rob Säule_5"
date: "2011-05-08T19:29:19"
picture: "Hochregalrobot_05.jpg"
weight: "12"
konstrukteure: 
- "???"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30543
- /details04ab-2.html
imported:
- "2019"
_4images_image_id: "30543"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-05-08T19:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30543 -->
