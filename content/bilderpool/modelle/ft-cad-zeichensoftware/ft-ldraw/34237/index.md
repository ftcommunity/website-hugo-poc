---
layout: "image"
title: "Buggy 13"
date: "2012-02-18T15:08:22"
picture: "Buggy_13.jpg"
weight: "98"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34237
- /detailsab3c.html
imported:
- "2019"
_4images_image_id: "34237"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-02-18T15:08:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34237 -->
