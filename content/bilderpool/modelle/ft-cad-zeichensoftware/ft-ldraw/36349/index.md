---
layout: "image"
title: "Warenautomat 01"
date: "2012-12-27T18:34:49"
picture: "Warenautomat_I1.jpg"
weight: "181"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36349
- /details13ce-2.html
imported:
- "2019"
_4images_image_id: "36349"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-27T18:34:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36349 -->
Der Warenautomat aus "Club Modell 1977/04".

So ganz zufrieden bin ich allerdings nicht, da der Münzeinwurf für 10 Pf ausgelegt war und die anderen damaligen Münzen sich in der Größe deutlich davon unterschieden.

Mit einem 5 Cent-Stück geht es ganz leidlich, aber eine 10 Cent Münze (die kaum kleiner ist) wird mal abgewiesen, mal wird sie angenommen.

Vielleicht fällt einem ja noch was ein.