---
layout: "image"
title: "Kniehebelpresse 3 (aus Hobby 1 Band 3 S. 42)"
date: "2011-08-13T13:24:57"
picture: "Kniehebelpresse_24.jpg"
weight: "47"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31576
- /details016d.html
imported:
- "2019"
_4images_image_id: "31576"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-13T13:24:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31576 -->
