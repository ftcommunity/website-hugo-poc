---
layout: "image"
title: "3Achs Rob_4"
date: "2011-03-04T09:23:07"
picture: "3Achs_Rob_4_2.jpg"
weight: "4"
konstrukteure: 
- "??"
fotografen:
- "con.barriga"
schlagworte: ["LDraw", "LPub"]
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/30197
- /details213e.html
imported:
- "2019"
_4images_image_id: "30197"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-03-04T09:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30197 -->
Teile einer Bauanleitung für einen 3-Achs Rob