---
layout: "image"
title: "Kameradreher 4"
date: "2012-06-16T15:56:40"
picture: "KameraDreher_28.jpg"
weight: "138"
konstrukteure: 
- "con.barriga"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35066
- /detailsb6d5.html
imported:
- "2019"
_4images_image_id: "35066"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-06-16T15:56:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35066 -->
Drehgestell für eine Fotokamera um z.B. Einzelaufnahmen für ein Panoramabild aufzunehmen.