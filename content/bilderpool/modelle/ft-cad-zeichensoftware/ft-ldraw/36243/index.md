---
layout: "image"
title: "Ranger 05"
date: "2012-11-30T21:52:44"
picture: "Ranger_5.jpg"
weight: "171"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36243
- /details1da5.html
imported:
- "2019"
_4images_image_id: "36243"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-30T21:52:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36243 -->
Ranger. Fan-Club Modell 1988/2. Modell komplett und Arm in Drehung.
Bestimmt einige Abweichungen vom Original, da manche Einzelheiten auf den Scans schlecht erkennbar.

Vollständige Anleitung als PDF wie immer erhältlich.