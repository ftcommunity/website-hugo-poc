---
layout: "image"
title: "Plane Test"
date: "2012-04-13T23:59:20"
picture: "ft_plane_v2.jpg"
weight: "128"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/34791
- /details6c78.html
imported:
- "2019"
_4images_image_id: "34791"
_4images_cat_id: "2243"
_4images_user_id: "585"
_4images_image_date: "2012-04-13T23:59:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34791 -->
A slightly different version of the plane using a different library.