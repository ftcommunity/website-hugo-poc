---
layout: "image"
title: "Selbststeuerndes Fahrzeug 3"
date: "2012-09-26T16:30:42"
picture: "SelbFahrz_3.jpg"
weight: "157"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35541
- /details66c7.html
imported:
- "2019"
_4images_image_id: "35541"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-26T16:30:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35541 -->
Was heute "Spursucher" genannt wird, war 1976 (vor 36 Jahren) ein "sich selbststeuerndes Fahrzeug" (aus Clubheft 29/1976).