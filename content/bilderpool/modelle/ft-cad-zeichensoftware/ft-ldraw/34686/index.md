---
layout: "image"
title: "Strobel-Brücke_04"
date: "2012-03-25T16:44:25"
picture: "Strob_Brueck_Bew_hal1.jpg"
weight: "125"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34686
- /detailsa75e-2.html
imported:
- "2019"
_4images_image_id: "34686"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-25T16:44:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34686 -->
Die "Strobel-Brücke" aus Hobby 1/3 S.77ff in drei verschiedenen Phasen.
(Mit POVRAY gespielt)