---
layout: "image"
title: "Buggy IR-Empfänger 01"
date: "2012-03-03T09:58:00"
picture: "FT_Buggy_IR-Empf_01.jpg"
weight: "104"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34515
- /details568d-2.html
imported:
- "2019"
_4images_image_id: "34515"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-03T09:58:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34515 -->
