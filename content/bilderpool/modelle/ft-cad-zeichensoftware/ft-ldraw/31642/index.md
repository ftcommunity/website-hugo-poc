---
layout: "image"
title: "Kompressor 2"
date: "2011-08-22T23:54:54"
picture: "Pneu_Kompressor_page_3.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/31642
- /details14ed.html
imported:
- "2019"
_4images_image_id: "31642"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-22T23:54:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31642 -->
Drei aufeinanderfolgende Arbeitsschritte bei denen der/die vorhergende(n) "geweißt" wurden.