---
layout: "image"
title: "Kipper"
date: "2012-08-04T18:07:54"
picture: "Kipper_04.jpg"
weight: "145"
konstrukteure: 
- "ft, con.barriga"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/35241
- /details5c1e.html
imported:
- "2019"
_4images_image_id: "35241"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35241 -->
Der modifizierte Hydraulik-Kipper aus den '80ern .
Angetriebene Kippmulde