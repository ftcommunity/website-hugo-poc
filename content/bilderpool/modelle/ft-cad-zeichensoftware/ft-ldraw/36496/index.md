---
layout: "image"
title: "Traktor 6"
date: "2013-01-22T17:34:37"
picture: "Traktor_Inet_06.jpg"
weight: "194"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36496
- /details54b1.html
imported:
- "2019"
_4images_image_id: "36496"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-22T17:34:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36496 -->
Modell und einige Bauschritte, nach einem dieser Tage gesehenen Bild.