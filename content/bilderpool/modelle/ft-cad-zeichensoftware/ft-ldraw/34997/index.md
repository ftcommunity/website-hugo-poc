---
layout: "image"
title: "Geheimschloss 4"
date: "2012-05-22T16:10:24"
picture: "Geheimschloss_4.jpg"
weight: "134"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/34997
- /detailsbbbc-2.html
imported:
- "2019"
_4images_image_id: "34997"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-05-22T16:10:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34997 -->
aus "Elektromechanik" S.48ff