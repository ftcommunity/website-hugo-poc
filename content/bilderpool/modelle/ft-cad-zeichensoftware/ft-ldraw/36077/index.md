---
layout: "image"
title: "Kartenmischer 1"
date: "2012-10-29T20:05:16"
picture: "Kartenmisch_1.jpg"
weight: "158"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- /php/details/36077
- /detailse486-3.html
imported:
- "2019"
_4images_image_id: "36077"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-10-29T20:05:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36077 -->
Fan-Club Modell 1976/3

Die Kartenablage wurde verändert, da die im Original verwendeten "Winkelklammern"
mir nicht zur Verfügung stehen.