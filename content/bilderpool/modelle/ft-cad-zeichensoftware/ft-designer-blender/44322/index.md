---
layout: "image"
title: "Grundbaustein"
date: "2016-08-26T22:05:40"
picture: "fdub1.jpg"
weight: "1"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44322
- /details6e62.html
imported:
- "2019"
_4images_image_id: "44322"
_4images_cat_id: "3270"
_4images_user_id: "2228"
_4images_image_date: "2016-08-26T22:05:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44322 -->
Alle Bauteile stammen aus dem ft Designer. Über die Exportfunktion wurden die Modelle ins extensible 3D Format exportiert und in Blender gerendert.
