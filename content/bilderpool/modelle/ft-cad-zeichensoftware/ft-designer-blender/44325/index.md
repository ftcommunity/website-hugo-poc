---
layout: "image"
title: "Radlader"
date: "2016-08-26T22:05:40"
picture: "fdub4.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44325
- /details08f7.html
imported:
- "2019"
_4images_image_id: "44325"
_4images_cat_id: "3270"
_4images_user_id: "2228"
_4images_image_date: "2016-08-26T22:05:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44325 -->
Modell: https://ftcommunity.de/categories.php?cat_id=3006

Zum Vergleich: Im Modus "Blender Render" mit der Option "Freestile" gerendert, sodass die Bauteilkanten gezeichnet werden. Außerdem Spiel mit Licht und Schatten am Modell.
