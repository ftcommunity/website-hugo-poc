---
layout: "image"
title: "Verbinder 5 kurz Schwarz"
date: "2015-06-08T21:30:57"
picture: "teilekalti15.jpg"
weight: "19"
konstrukteure: 
- "fischertechnik-Kalti"
fotografen:
- "Kalti"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41166
- /details26ea.html
imported:
- "2019"
_4images_image_id: "41166"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41166 -->
