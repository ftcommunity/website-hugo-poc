---
layout: "image"
title: "Abdeckplatte 1 Rot"
date: "2015-06-08T21:30:57"
picture: "teilekalti17.jpg"
weight: "21"
konstrukteure: 
- "fischertechnik-Kalti"
fotografen:
- "Kalti"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41168
- /detailsaa2c.html
imported:
- "2019"
_4images_image_id: "41168"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41168 -->
