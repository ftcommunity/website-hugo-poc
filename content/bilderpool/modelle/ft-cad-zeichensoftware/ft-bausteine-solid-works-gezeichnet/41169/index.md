---
layout: "image"
title: "Winkelstein 3 Seitig Rot"
date: "2015-06-08T21:30:57"
picture: "teilekalti18.jpg"
weight: "22"
konstrukteure: 
- "fischertechnik-Kalti"
fotografen:
- "Kalti"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41169
- /details0995.html
imported:
- "2019"
_4images_image_id: "41169"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41169 -->
