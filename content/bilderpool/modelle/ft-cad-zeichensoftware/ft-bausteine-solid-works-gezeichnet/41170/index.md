---
layout: "image"
title: "Grundbaustein 30 Rot"
date: "2015-06-08T21:30:57"
picture: "teilekalti19.jpg"
weight: "23"
konstrukteure: 
- "fischertechnik-Kalti"
fotografen:
- "Kalti"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41170
- /detailsee21.html
imported:
- "2019"
_4images_image_id: "41170"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41170 -->
