---
layout: "image"
title: "Grundbaustein 15 2 Zapfen Schwarz"
date: "2015-06-08T21:30:57"
picture: "teilekalti10.jpg"
weight: "14"
konstrukteure: 
- "fischertechnik-Kalti"
fotografen:
- "Kalti"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/41161
- /details6481-3.html
imported:
- "2019"
_4images_image_id: "41161"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41161 -->
