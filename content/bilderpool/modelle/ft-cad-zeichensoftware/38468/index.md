---
layout: "image"
title: "Grundbaustein Beispiel"
date: "2014-03-16T17:58:36"
picture: "Grundmodell.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "Thomas Kaltenbrunner (Kalti)"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/38468
- /detailsc758.html
imported:
- "2019"
_4images_image_id: "38468"
_4images_cat_id: "2271"
_4images_user_id: "1342"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38468 -->
Anordnung von Grundbausteinen wie es auf alten Bauanleitungen war allerdings mit roten Bausteinen
