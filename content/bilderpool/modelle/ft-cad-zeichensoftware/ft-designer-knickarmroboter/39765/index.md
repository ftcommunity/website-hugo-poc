---
layout: "image"
title: "Knickarmroboter 4"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter4.jpg"
weight: "4"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- /php/details/39765
- /details8242.html
imported:
- "2019"
_4images_image_id: "39765"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39765 -->
Von hinten
 erstellt mit Ft-Designer