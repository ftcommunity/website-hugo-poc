---
layout: "image"
title: "Knickarmroboter 3"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter3.jpg"
weight: "3"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- /php/details/39764
- /details2fdf.html
imported:
- "2019"
_4images_image_id: "39764"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39764 -->
Von oben
 erstellt mit Ft-Designer