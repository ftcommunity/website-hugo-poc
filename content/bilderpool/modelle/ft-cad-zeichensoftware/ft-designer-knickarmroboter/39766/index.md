---
layout: "image"
title: "Knickarmroboter 5"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter5.jpg"
weight: "5"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- /php/details/39766
- /details86d9.html
imported:
- "2019"
_4images_image_id: "39766"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39766 -->
Von vorne
 erstellt mit Ft-Designer