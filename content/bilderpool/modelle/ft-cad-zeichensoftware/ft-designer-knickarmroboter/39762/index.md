---
layout: "image"
title: "Knickarmroboter 1"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter1.jpg"
weight: "1"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- /php/details/39762
- /details45e4.html
imported:
- "2019"
_4images_image_id: "39762"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39762 -->
Knickarmroboter von der Seite
 erstellt mit Ft-Designer