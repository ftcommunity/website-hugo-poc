---
layout: "image"
title: "1"
date: "2010-11-09T22:27:43"
picture: "ftzeichnungen1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/29216
- /details708a.html
imported:
- "2019"
_4images_image_id: "29216"
_4images_cat_id: "2120"
_4images_user_id: "1082"
_4images_image_date: "2010-11-09T22:27:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29216 -->
Eigentlich steht in der Kategorien-Beschreibung schon alles. Wo der Fluchtpunkt ist sieht man, und was hier dargestellt wird, kann man vielleicht erraten. Der Motor ist etwas missglückt, weil es einfach nur ein Kasten ist, und die entsprechenden Ecken fehlen.