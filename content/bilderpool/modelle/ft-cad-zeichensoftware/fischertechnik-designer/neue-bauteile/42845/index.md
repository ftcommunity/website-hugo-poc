---
layout: "image"
title: "121640 Schlauchanschluss Kompressor"
date: "2016-01-30T17:19:51"
picture: "ftdesigner3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42845
- /detailsd0e7.html
imported:
- "2019"
_4images_image_id: "42845"
_4images_cat_id: "3185"
_4images_user_id: "2303"
_4images_image_date: "2016-01-30T17:19:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42845 -->
neues Bauteil für den ftdesigner

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip
