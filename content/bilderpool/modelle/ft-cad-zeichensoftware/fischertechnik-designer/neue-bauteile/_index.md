---
layout: "overview"
title: "Neue Bauteile"
date: 2020-02-22T08:40:08+01:00
legacy_id:
- /php/categories/3185
- /categories35cc.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3185 --> 
Hallo zusammen,

zur Dokumentation meiner Modelle verwende ich denn fischertechnik-Designer. 
Ein tolles Produkt, es macht viel Spaß damit zu arbeiten. Leider ist die Bibliothek nicht mehr so aktuell. 

Daher habe ich mir mit AutoCAD Inventor viele fehlende Teile neu konstruiert und in .stl konvertiert. 
Hier das Ergebnis.

Bei den Bauteilen handelt sichi um neue und modifizierte Bauteile für den ftdesigner.

Die Bauteile habe ich bereits an Michael Samek (ftdesigner) weitergeleitet. Sie sollten mit dem nächsten Update kommen.
Wer dies noch nicht abwarten möchte, kann diese Bauteile vorab installieren.

Die neuen Bauteile befinden sich noch in der Testphase. Es kann sein das diese Bauteile noch vereinzelt Fehler enthalten können.
Alle Bauteile sind ohne Gewähr auf volle Funktion und Vollständigkeit.

Eine Installation geschieht immer auf eigene Gefahr. Es emphiehlt sich eine Sicherheitskopie vom ftdesigner anzulegen.

Hier könnt ihr die neuen Bauteile downloaden:

https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip

**** Bitte unbedingt die Installtionshinweise im Download beachten.****



