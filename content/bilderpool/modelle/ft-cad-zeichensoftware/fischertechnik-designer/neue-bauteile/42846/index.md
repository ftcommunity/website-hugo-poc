---
layout: "image"
title: "130589 Sound and Lights"
date: "2016-01-30T17:19:51"
picture: "ftdesigner4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42846
- /details1062.html
imported:
- "2019"
_4images_image_id: "42846"
_4images_cat_id: "3185"
_4images_user_id: "2303"
_4images_image_date: "2016-01-30T17:19:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42846 -->
neues Bauteil für den ftdesigner

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip
