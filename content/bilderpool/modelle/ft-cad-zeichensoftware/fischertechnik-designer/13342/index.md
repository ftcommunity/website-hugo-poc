---
layout: "image"
title: "BF1 2D1 Bohr- und Fräsmaschine"
date: "2008-01-17T22:48:18"
picture: "BF1-00-00_Statik-071206.jpg"
weight: "2"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "2D-Kopie  3D-Fenster"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13342
- /detailsea39.html
imported:
- "2019"
_4images_image_id: "13342"
_4images_cat_id: "1213"
_4images_user_id: "723"
_4images_image_date: "2008-01-17T22:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13342 -->
Konstruktion mit dem ft-Designer.
Erste eigene Neuentwicklung (ft-Neuling ab 12/2006) mit dem Ziel des Kennenlernens der statischen Bauteileigenschaften und der modellgemässen Umsetzbarkeit praktischer mechanischer Funktionen. Das Modell mit 685 Bauteilen hat ein 2-stufig schaltbares Rädergetriebe, Bohrvorschub von Hand und eine Fräszustellung. Mit dem hier nicht abgebildeten Zubehör besitzt das Maschinenmodell 6 Achsen. Aus Studien- und Wartungsgründen wurde der Getriebekopf in einen C-Rahmen mit 2-flügeliger Öffnung der Verkleidung konzipiert. Mit der Verdrehsicherung der "Spindelpinole" wird der Getrieberahmen zum O-Rahmen. Die konstruktive Entwicklung erfolgte im 1.Quartal 2007 und wurde bis zur Beschaffbarkeit der Bauteile abgelegt. Das Modell ist nunmehr aufgebaut und vorerst gemäss dem abgebildeten Stand mit insgesamt 10 Std. Laufzeit unter max. Spindeldrehzahl erprobt. Die während der Konstruktion bereits qualitativ erkannten statischen Notwendigkeiten an Fuss und Säule wurden im Ergebnis der Erprobung quantitativ ermittelt und umgesetzt.

Nachtrag 21.10.2008:
Zweite Veröffentlichung mit detailierten Fotos
http://www.ftcommunity.de/details.php?image_id=15967
