---
layout: "image"
title: "BF1 Foto 1 Gesamtmodell"
date: "2008-01-19T08:34:57"
picture: "BF1_Gesamtmodell.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13343
- /detailsf04a.html
imported:
- "2019"
_4images_image_id: "13343"
_4images_cat_id: "1213"
_4images_user_id: "723"
_4images_image_date: "2008-01-19T08:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13343 -->
Das Foto des Gesamtmodells zeigt auch das komplette Zubehör. Hier z.B. auf dem Kreuztisch ein kleiner Flachspanner, links hinten ein Aufsatzdrehtisch (Drehachse C) wahlweise mit Platte für prismatische oder Teller für rotationssymmetrische Teile und links vorn ein Teilkopf (Drehachse A). Am Gegenlager fehlt allerdings die Spindel. Da ft-Fans ständig? tüfteln , sind die Teile schon wieder woanders eingesetzt. Der Aufbau des Gesamtmodells vom ersten bis zum letzten Bauteil benötigte im Stück vom 29. zum 30.11.2007 ausgeführt 11,5 Std. Das Modell enthält keine Heinzelmännchen wie z.B. fremde oder bearbeitete Teile, Klebepunkte und Papierzwischenlagen. Eine notwendige Ausnahme bilden die Drehkränze von Spindelkopf und Aufsatzdrehtisch. Ihr Spiel zwischen Ober- und Unterteil musste zur Beseitigung des Ankippens durch Zwischenlagen eliminiert werden.
