---
layout: "image"
title: "Pneumatik-Roboter 5"
date: "2008-03-07T07:03:15"
picture: "Pneumatik_Roboter_5_web.jpg"
weight: "17"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
schlagworte: ["FT Experimenta Schulprogramm"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13875
- /detailsacd8-2.html
imported:
- "2019"
_4images_image_id: "13875"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13875 -->
In die Mulden paßt ein Tischtennisball.
