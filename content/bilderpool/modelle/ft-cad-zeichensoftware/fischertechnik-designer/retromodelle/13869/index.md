---
layout: "image"
title: "Material-Aufzug"
date: "2008-03-07T07:03:15"
picture: "Materialaufzug_web.jpg"
weight: "11"
konstrukteure:
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
schlagworte: ["Experimenta Computing Schulprogramm"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13869
- /details67c3-3.html
imported:
- "2019"
_4images_image_id: "13869"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13869 -->
Mit den 3 Tastern (rechts) kann der Aufzug in das jeweilige Stockwerk gerufen werden. Dieser funktioniert mit Hubgetriebe. Ist er im Stockwerk angekommen, wird der Motor über den jeweils zweiten Taster (links) ausgeschaltet.

.
.
.
