---
layout: "image"
title: "TrainingsRoboter von 1985"
date: "2008-03-07T07:03:15"
picture: "TrainingsRobot_web.jpg"
weight: "20"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13878
- /details65b6.html
imported:
- "2019"
_4images_image_id: "13878"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13878 -->
Ein sehr schönes Modell, daß früher mit Gabelleichtschranken positioniert wurde. Heute werden 4er Impulsrädchen verwendet.
Statt der Alu-Profile kann man natürlich auch normale Steine verwenden.
