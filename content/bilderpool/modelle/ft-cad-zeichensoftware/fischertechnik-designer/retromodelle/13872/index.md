---
layout: "image"
title: "Pneumatik-Roboter 2"
date: "2008-03-07T07:03:15"
picture: "Pneumatik_Roboter_2_web.jpg"
weight: "14"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
schlagworte: ["FT Experimenta Schulprogramm"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13872
- /details1796.html
imported:
- "2019"
_4images_image_id: "13872"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13872 -->
In die Mulden paßt ein Tischtennisball.
