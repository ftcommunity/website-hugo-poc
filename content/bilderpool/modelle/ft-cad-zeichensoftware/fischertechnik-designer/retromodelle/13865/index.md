---
layout: "image"
title: "Codekartenleser mit automatischem Einzug"
date: "2008-03-07T07:03:14"
picture: "Codekartenleser_mit_automatischem_Einzug_web.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
schlagworte: ["Profi Computing"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13865
- /details4968.html
imported:
- "2019"
_4images_image_id: "13865"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13865 -->
Man kann schön die Funktionsweise eines solchen Lesegerätes ergründen.
Das Modell hat ein großes Experimentier-Potential.
Man kann es auch schön mit dem Tresor verbinden.

Wenn die Pappkarte eingeschoben wird, startet über einen Taster der Transportmotor. 2 Lichtschranken werten dann die Löcher in der Karte aus. Die eine Reihe Löcher dient zur Positionsermittlung. Dann wird geschaut, ob auf der anderen Reihe ein Loch ist, oder nicht. Wenn alle 5 Löcher ausgewertet sind, kann die Karte wieder ausgegeben werden und z.B. ein Safe geht auf.
