---
layout: "image"
title: "Codekarte"
date: "2008-03-07T07:03:14"
picture: "Codekarte_2.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Fischertechnik-Designer"
schlagworte: ["Profi Computing"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/13864
- /details6b22.html
imported:
- "2019"
_4images_image_id: "13864"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13864 -->
Codekarte für Lesegerät
