---
layout: "comment"
hidden: true
title: "5495"
date: "2008-03-08T12:17:23"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Ich bitte ebenfalls darum die ftcommunity nicht mit diesen 3D Bildern zu überschwemmen. Das ist alles Theorie und hat mit der Praxis, die ja doch anders aussieht nur bedingt zu tun. Wenn jemand im 3D konstruiert und dann das Modell auch 1:1 baut ist das vielleicht etwas anderes. Aber auch in einem solchen Fall sollten die gebauten Modelle im Vordergrund stehen. 
Ich frage mich. was diese 3D Konstruktuionen eigentlich in der ftcommunity zu suchen haben?. Sind wir jetzt schon soweit das wir nur noch am PC arbeiten. 
Ich schlage vor wir legen den Taschenrechner aus der Hand und rechnen im Kopf oder schreiben mit dem Füllfederhalter. 
Aus meiner Sicht könne wir auf diese Art des Modellbaus verzichten...

Gruß Jürgen