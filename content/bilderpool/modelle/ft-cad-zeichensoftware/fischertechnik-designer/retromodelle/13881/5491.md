---
layout: "comment"
hidden: true
title: "5491"
date: "2008-03-07T20:50:31"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Udo2,

ich habe alle Modelle auch schon gebaut. 

Und ich fände es schade, wenn diese tollen Modelle irgendwo in Vergessenheit geraten. 

Wie Du schon sagst, man muß ja nichts neu erfinden, was es schon gibt. Ich finde es sehr beeindruckend, was es alles schon gibt. Und wenn man einfach mal ein Modell nachbaut (ohne immer was neues zu erfinden), ist das auch schon sehr faszinierend. 

Bei vielen Modellen versteht man eben durch das Modell, wie das Ganze in der Realität funktioniert. Und darum geht es doch. Das man die Realität nachbaut. Sicher ist es auch immer wieder beeindruckend, was es alles neues gibt. Aber eine Mechanik, die nach 20 (oder sogar teilweise 40) Jahren wieder entdeckt wird, ist doch wie ein Neubau...   ;c)

Doch hier in der Community gibt es eben vom Anfänger bis zum Profi alles. Der Anfänger sucht nach etwas, was er bauen kann, oder Inspiration. Der Profi teilt sich anderen mit.

Deswegen hat alles - denke ich - seinen Platz. Neues und Altes.

Wie denken die anderen darüber?

Viele Grüße, Andreas.