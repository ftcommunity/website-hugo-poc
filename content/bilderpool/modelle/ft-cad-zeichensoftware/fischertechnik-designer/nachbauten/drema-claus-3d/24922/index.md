---
layout: "image"
title: "[8/9] Reitstock 2/2"
date: "2009-09-11T21:41:20"
picture: "dremavonclausind8.jpg"
weight: "8"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 - 2D aus 3D-Sichtfenster"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24922
- /details91a0.html
imported:
- "2019"
_4images_image_id: "24922"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:41:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24922 -->
Die Mechanik seiner Verschiebung und Arretierung, aus Sichtgründen Teileausblendungen am Reitstockgehäuse (Profilkantendarstellung)
