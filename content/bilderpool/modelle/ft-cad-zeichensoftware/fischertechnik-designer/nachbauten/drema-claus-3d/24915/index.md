---
layout: "image"
title: "[1/9] Gesamtansicht"
date: "2009-09-11T21:40:46"
picture: "dremavonclausind1.jpg"
weight: "1"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 - 2D aus 3D-Sichtfenster"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24915
- /details802d.html
imported:
- "2019"
_4images_image_id: "24915"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24915 -->
Das zur Convention 2008 von Claus-Werner Ludwig vorgestellte ft-Modell war für mich ein Thema damit in 3D weiterführende Erfahrungen mit dem "fischertechnik-designer" zu sammeln. Da dieses Großmodell wegen der Modelltiefe per Foto nicht ausreichend einsehbar ist, hat mir Claus sein Modell für ein paar Wochen zur Verfügung gestellt. Dafür lieber Claus an dieser Stelle nochmals meinen Dank für deine Unterstützung.
Hier die Gesamtansicht des ft-Modells (3D-Schattierung ohne Bauteilkanten) soweit mit dem ftD aktuell machbar nun auch detailgetreu in 3D. Die 3D-Stückliste weist 2290 Bauteile aus. Der Nachbau ist zweistufig in 10 Hauptbaugruppen mit fast 100 Baufasen (Baugruppen) strukturiert. Eine nochmalige Vorstellung des Modells ist hier nicht vorgesehen. Es soll nur eine Wiedergabe weniger aufklärender Ansichten sein, die per Foto vom Original so nicht möglich sind.
Sobald es meine Zeit erlaubt und die Teile im ftD verfügbar sind werde ich dazu auch noch einen Kabelplan ähnlich der in den ft-Baukästen erstellen. Das Modell der Digitaluhr von Steffan Falk ist ja auch noch in Arbeit.
