---
layout: "image"
title: "Jeep"
date: "2011-12-03T14:03:07"
picture: "jeep1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/33599
- /details7548.html
imported:
- "2019"
_4images_image_id: "33599"
_4images_cat_id: "2488"
_4images_user_id: "1"
_4images_image_date: "2011-12-03T14:03:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33599 -->
Habe seit 2 Tagen den fischertechnik-Designer.

Nun bin ich ein wenig am üben und habe mir mal die Bauanleitung von einem älteren fischertechnik Baukasten zu Übungszwecken zur Hand genommen.
So nach und nach wird es!
