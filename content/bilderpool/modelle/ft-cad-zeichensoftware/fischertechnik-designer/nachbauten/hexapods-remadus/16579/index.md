---
layout: "image"
title: "[2/2] Flugsimulator, elektromechanisch"
date: "2008-12-11T00:56:25"
picture: "hexapod2.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "3D-Nachbau, Udo2"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16579
- /detailsd94f-2.html
imported:
- "2019"
_4images_image_id: "16579"
_4images_cat_id: "1501"
_4images_user_id: "723"
_4images_image_date: "2008-12-11T00:56:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16579 -->
... veröffentlichter Modellentwicklungen von Martin Romann [Remadus].
