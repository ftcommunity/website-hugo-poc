---
layout: "comment"
hidden: true
title: "7972"
date: "2008-12-11T21:16:14"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
schau mal bitte genauer hin. Jeder Gelenkfuß hat einen zur Platte rechtwinkligen BS15 mit dem Zapfen nach unten in der Arbeitsplatte. Die jeweils anderen BS15 (links oben und unten) sitzen an diesem bzw. am daran anliegenden WS60° oder (rechts) an einem WS60° direkt mit den Zapfen nach oben. Okay? Schummeln gilt also auch in 3D nicht!
Gruß, Ingo