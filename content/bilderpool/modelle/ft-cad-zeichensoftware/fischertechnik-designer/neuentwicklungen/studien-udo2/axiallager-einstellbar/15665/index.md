---
layout: "image"
title: "Wälzlager, axial (4/4)"
date: "2008-09-30T09:58:22"
picture: "axiallagereinstellbar4.jpg"
weight: "4"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/15665
- /details3b43.html
imported:
- "2019"
_4images_image_id: "15665"
_4images_cat_id: "1439"
_4images_user_id: "723"
_4images_image_date: "2008-09-30T09:58:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15665 -->
Dieser dritte Lösungsansatz beseitigt komplett den zweiten Fehler.
Die Lösung kann als Bauvorlage zu ihrer Erprobung dienen.
Beim Aufbau sollte aber beachtet werden, dass die Verbindungen der Rädereinstellungen von etwas festerem Sitz sein müssen.
