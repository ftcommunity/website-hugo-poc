---
layout: "comment"
hidden: true
title: "7360"
date: "2008-09-30T13:38:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,

ich würde sagen, ein Vollkreis aus den ft-Bogenstücken ist immer etwas "überspannt", und die Übergänge sind immer etwas eckig. Der Kreis will nämlich *nicht* genau vollständig schließen und man muss ihn umbiegen. Irgendwie scheinen mir das eher 59°-Bogenstücke als solche mit 60° zu sein. Insofern kann ich aus den Übergängen an Remadus' Drehkranz noch nicht ableiten, dass die Rollen z. B. zu weit außen säßen.

Gruß,
Stefan