---
layout: "image"
title: "[5/7] BR 003 131-1 Kulissensteinlagerung gefedert"
date: "2011-11-29T11:56:36"
picture: "dscalestudieschnellzuglokbrwitte5.jpg"
weight: "5"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/33586
- /details4efb.html
imported:
- "2019"
_4images_image_id: "33586"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33586 -->
Hier wurden mal das Treibrad B und eine Bauplatte 15x15 ausgeblendet. Erkennbar wird damit die gefederte Kulissenlagerung der 6 Treib- und Kuppelräder. Die Federung der Kulissensteine erfolgt beim Vorbild mit Blattfedern. Ich habe hier vorläufig "Rundstäbe" eingesetzt. Zu erkennen sind auch die "vorbereiteten" Bohrungen zur Aufnahme der Schwingen für die Bremsen der C-Räder.




