---
layout: "comment"
hidden: true
title: "10107"
date: "2009-10-21T22:00:48"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

merkwürdig, du bist bis jetzt der Einzige, der sich für diese Studie interessiert. Nach meinem Wissen ist das eine der wenigen korrekten Blattfederungen in ft.

Nun zu deinen Fragen

1. Die Achse macht durch die Federung eine Vertikalbewegung zum Rahmen. Zur Führung des Federpaketes und damit zur Führung des Achsschenkels dienen in der Tat der Federnocken am Federpaket und der Baustein 5 fast am Rahmen (U-Träger), der hier aus Sichtgründen ausgeblendet ist.
2. Die Statikstreben als Blattfedern sind in 35979 U-Träger Adapter mit 07356 Klemmstift schön fest "aufgefedelt". Das 3D-Teil U-Träger Adapter ist geometrisch leider nicht korrekt, deshalb die weisse Beilage. Sie dient allerdings als eine Art Horizontalklemme, damit sich die Federn nicht um ihre zentrische Klemmung drehen.

Die Baugruppe hatte ich vor der Veröffentlichung hier in der ftC aufgebaut und als diese probiert. Deshalb gibt es noch die verbesserte Lösung im Bild [2].

Gruss, Ingo