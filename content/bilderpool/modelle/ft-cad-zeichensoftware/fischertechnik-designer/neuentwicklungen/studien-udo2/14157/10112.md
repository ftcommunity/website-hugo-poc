---
layout: "comment"
hidden: true
title: "10112"
date: "2009-10-22T07:57:07"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Danke für Deine Ausführungen, Udo, äh, Ingo!

Was mir immer noch nicht klar ist: Der Federnocken in der Mitte gleitet also im Fahrzeugbetrieb im Ein- und Ausfedern im Baustein 5??? Das ist meiner Meinung nach aber keine "saubere" FT-Lösung.

FT-Verbindungen mittels Nut/Feder/Zapfen sind doch per Definition "fest"! Wäre eine Achse, die in einer rahmenfesten Bohrung gleitet nicht die bessere Lösung?

Ansonsten: Sauber aufgebaut! Wobei ich mit dieser digitalen Designer-Geschichte immer noch gar nix anfangen kann... ;o)

Gruß, Thomas