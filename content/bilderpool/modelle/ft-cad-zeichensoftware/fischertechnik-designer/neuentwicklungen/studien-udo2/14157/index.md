---
layout: "image"
title: "[1] LKW-Achse mit Blattfeder"
date: "2008-04-03T17:49:49"
picture: "studienvonudo1.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D-Kopie aus 3D-Ansicht"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/14157
- /detailscd62.html
imported:
- "2019"
_4images_image_id: "14157"
_4images_cat_id: "1308"
_4images_user_id: "723"
_4images_image_date: "2008-04-03T17:49:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14157 -->
3D-Studie zur Hinterradaufnahme eines LKW mit Blattfederung. Zur besseren Sicht wurde das Rahmenteil  U-Träger 150 ausgeblendet. An ihm befestigt sind von oben nach unten 3x Baustein 5, 2x Adapterlasche und der Verbindungsstopfen mit Klemmbuchse 5. Modellmäßig umgesetzt sind die Funktionen Federpaket mit senkrechter Führung in Achshöhe, Lastübertragung an den Federenden, Überlastanschlag, Achsgelenk und Blattfedersicherung bei Kranhub. Die Entwicklung wird über den praktischen Aufbau weitergeführt mit dem Ziel einer höheren Federkraft. Dazu sind verschiedene Lösungen angedacht, die erprobt werden sollen.
Bild [2] zeigt eine verstärkte Ausführung des Federpaketes.
