---
layout: "image"
title: "[5/7] 3D-XYZ-G"
date: "2008-03-05T18:08:59"
picture: "dxyzlinearrobotervorabvorstellungd5.jpg"
weight: "5"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/13842
- /details13ff.html
imported:
- "2019"
_4images_image_id: "13842"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-05T18:08:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13842 -->
Hier noch eine der weiter konstruktiv vorbereiteten Getriebevarianten Schrittmotorübersetzung 3:1 mit Kettentrieb 2:1. Das Modell erreicht rechnerisch 0,208mm/Schritt und 10mm/Motorumdrehung als ein Kompromiß zwischen Schrittauflösung und Lineargeschwindigkeit.
Diese Schrittauflösung könnte als niedrig angesehen werden. Aus der Praxis der Technik sei hierzu aber vermerkt, daß eine rechnerisch ermittelte Genauigkeitsstufe praktisch erst erreicht wird, wenn das "Drumherum" des technischen Gebildes wie etwa Statik und Mechanik um eine Zehnerpotenz bzw. um eine Kommastelle genauer ist.
Da bei Schrittmotoren mit der Erhöhung der Ansteuerfrequenz die Drehzahl sich mit abfallendem Drehmonent erhöht, ist man gut beraten am Abgang des Motors bei vorhandener Drehzahlreserve ein geeignetes Getriebe zu platzieren.
In der Z-Achse wurde der Schneckentrieb belassen. Schrittmotoren dieser Achse werden vergleichsweise heiß, weil sie z.B. im Plotterbetrieb während der gesamten Zeichenlänge zur vertikalen Sicherung des Schreibstiftes eingeschaltet bleiben. Die Praxis wird zeigen, ob die mechanische Selbsthemmung des Schneckentriebs ein Abschalten des Schrittmotors ermöglicht. Das soll eine noch angedachte Eliminierung des Spindeltriebspiels unterstützen.
