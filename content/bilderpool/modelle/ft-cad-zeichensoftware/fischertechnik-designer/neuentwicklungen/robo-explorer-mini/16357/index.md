---
layout: "image"
title: "(4/7) Fahrwerk mit Ketten"
date: "2008-11-21T10:10:16"
picture: "roboexplorerminiind4.jpg"
weight: "4"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16357
- /details81a2.html
imported:
- "2019"
_4images_image_id: "16357"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16357 -->
Technische Daten des Fahrwerks:

- Achsstand oben 120mm
- Achstand unten 85mm
- Fahrwerklänge 150mm
- Fahrwerkbreite 105mm
- Fahrwerkhöhe 55mm
- Auffahrschräge vorn ca. 50°

Die Raupenketten bestehen neben dem Rastkettenglied 36248 (auch Kettenglied  36263 möglich) noch aus 37192 Förderkettenglied und 37210 Raupenbelag 14,5. Die beiden letzten Teile werden leider nicht mehr hergestellt. Ich denke aber, daß sich das mal mit entsprechenden Baukästen wieder ändern kann. Die aktuellen Raupenbeläge 31762 und 31790 erscheinen mir mit ihrer Größe nicht universell. Eine weitere Größe kürzerer Raupenbelege ebenfalls zum Anbau an die Rastkettenglieder 36248 und 128659 ist da vielleicht denkbar.

Der praktische Aufbau ergibt je Kette 33 Ketten- oder Rastkettenglieder sowie 32 Förderkettenglieder und Raupenbelege! 

Die Führung der Ketten mit Auffahrschräge erlaubt so auch dazwischen das beidseitige Anbauen von Impulstastern und Ultraschallsensoren. An den unteren Ritzeln Z10 will ich eventuell erstmal die Lösung eines klemmbaren Impulsrades 5 versuchen. In die Bodenöffnung des Rahmens passen vorerst genau der IR-Spurensensor oder der Farbsensor.
