---
layout: "image"
title: "(6/7) Fahrwerk mit ROBO Interface"
date: "2008-11-21T10:10:16"
picture: "roboexplorerminiind6.jpg"
weight: "6"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16359
- /details0b81.html
imported:
- "2019"
_4images_image_id: "16359"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16359 -->
Das aktuelle ROBO Interface erdrückt optisch fast das kleine Fahrzeug.
Unter [3/5] => wird die aktuelle Befestigung des Interface vorgestellt.
