---
layout: "image"
title: "(7/7) Fahrwerk mit Interface 4"
date: "2008-11-21T10:10:16"
picture: "roboexplorerminiind7.jpg"
weight: "7"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16360
- /details5d46.html
imported:
- "2019"
_4images_image_id: "16360"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16360 -->
Der "ROBO Explorer mini" symbolisch mit dem ft-Interface der 4. Generation. Ich habe es mal längs mittig angeordnet ohne die noch unbekannten Befestigungsmöglichkeiten. So ist vorn und hinten innerhalb der Fahrzeuglänge noch je ca. 30mm Platz für Sensoren und Signalgeber.
Das erlaubt dann z.B. hinten im Austausch dieses für das ROBO Interface gedachten Mignon-Akkupacks auch den Aufbau von ein oder zwei ft-Akkus in senkrechter Anordnung und einen um fast 18mm niedrigeren Aufbau des ft-Interface 4.
