---
layout: "image"
title: "(1/7) Rahmen mit Achslager und Motorkonsole"
date: "2008-11-21T10:10:14"
picture: "roboexplorerminiind1.jpg"
weight: "1"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16354
- /detailse06d.html
imported:
- "2019"
_4images_image_id: "16354"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16354 -->
Erste Infos zum ft-Interface der 4. Generation haben uns mit den Abmessungen 90mm x 90mm x 15mm erreicht. Wird es dann auch mal einen Baukasten ähnlich dem aktuellen "ROBO Explorer" mit einem kleineren Basisfahrzeug geben? Also habe ich mich mal mit so einem kleinen Fahrzeug versucht, bei dem zunächst die Sensoren und die Steuerprogramme des "ROBO Explorer" mit seinen Fahrzeugvarianten verwendbar bleiben sollen.
Das hier ist erstmal eine Vorabveröffentlichung aus der 3D-Entwicklung. Das Modell selbst befindet sich im Aufbau leider mit zeitlichen Stolpersteinen bei der Beschaffung von Teilen und Werkzeugen.

Dieses Chassis (Kfz-Fahrgestell) hat mich während der bisherigen 3D-Entwicklung ständig aktualisiert aufgebaut zur Kontrolle begleitet. Schließlich sind hier die Anforderungen an die Festigkeit der Verbindungen und an die Verwindungssteife hoch. Der Rahmen als nach oben offenes C-Gestell ist gleichzeitig auch der Kettenspanner.
