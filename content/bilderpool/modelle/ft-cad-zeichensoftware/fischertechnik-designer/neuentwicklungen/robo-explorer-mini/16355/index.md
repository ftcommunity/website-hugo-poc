---
layout: "image"
title: "(2/7) Getriebe und Achsen"
date: "2008-11-21T10:10:15"
picture: "roboexplorerminiind2.jpg"
weight: "2"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16355
- /detailscaf9.html
imported:
- "2019"
_4images_image_id: "16355"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-21T10:10:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16355 -->
Technische Ausstattung der Mechanik:

- Zwei separate Frontantriebe für je eine Kette
- Power Motoren 20:1 oder 50:1
- Getriebeübersetzung 1:1 
- Metallachsen mit 2-fach-Lagerung

Bei der Motorbefestigung will ich mal das "Patent" von Herrn Brickwedde mit in der Länge halbierten Rohrhülsen versuchen. Das ist hier in 3D leider nicht korrekt darstellbar.

Der Antrieb des ersten Kegelrades Z12 ist auch über 35142 Abtriebshülse und 35063 Rastachse 30 möglich. Eine Spezialanfertigung aus Metall ergibt jedoch axial bessere Platzverhältnisse.
Den Kern der Antriebswelle bildet eine Metallachse 90. Die beiden Zahnradblöcke Kegelrad Z12 und Ritzel Z10 werden wieder lösbar auf je eine Hülse 15 zum Freilauf auf der Kernachse aufgezogen.

Die drei freilaufenden Kuppelachsen bestehen aus je zwei Metallachsen 50. Ihre axiale Lage wird durch je einen Klemmring 5 zwischen Innen- und Außenlager gesichert.
