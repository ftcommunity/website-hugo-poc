---
layout: "image"
title: "[1/5] Bestückter 'ROBO Explorer mini' von rechts vorn"
date: "2008-11-23T12:49:03"
picture: "roboexplorermini1.jpg"
weight: "8"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2 - 2D aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/16472
- /details56db-2.html
imported:
- "2019"
_4images_image_id: "16472"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-23T12:49:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16472 -->
Der "ROBO Explorer mini" vorläufig mit dem aktuellen ROBO Interface und mit gleichzeitiger Komplettbestückung aller Sensoren und Signalgeber, die bei den fünf Fahrzeugvarianten Spursucher, Tunnelroboter, Farberkenner, Explorer und Rescue Robot des aktuellen Baukastens "ROBO Explorer" vorkommen. Aus Gründen einer flexiblen Verwendung der Sensoren IR-Spurensucher und Farberkenner wurde von einer Bestückung der Bodenöffnung Abstand genommen.

Die Verkabelung dazu kann dann wahlweise aus der Bauanleitung dieses Baukastens entnommen werden.

Die Steuerprogramme können unter http://www.fischertechnik.de/robopro/update.html runtergeladen werden. Die Beispielprogramme sind im aktuellen Update 1.2.1.33 enthalten.

Die Komponenten Ultraschallsensor, IR-Spurensucher, Farbsensor und Fotowiderstand sind im ftd als 3D-Teil leider noch nicht vorrätig. Ich habe diese deshalb simuliert abgespeichert und kann sie so bei Bedarf hinzugeladen rationell anordnen.
