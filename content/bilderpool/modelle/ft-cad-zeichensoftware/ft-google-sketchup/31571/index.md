---
layout: "image"
title: "Sieben ft-Teile [3/5]"
date: "2011-08-12T22:54:27"
picture: "ftundgooglesketchup3.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Udo2, 2D-Screenshot aus 3D"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/31571
- /detailse7ce.html
imported:
- "2019"
_4images_image_id: "31571"
_4images_cat_id: "2351"
_4images_user_id: "723"
_4images_image_date: "2011-08-12T22:54:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31571 -->
Die Flächendarstellung "Röntgen chattiert mit Kantenlinien" zeigt anschaulich auf einen Blick alle Konturen und Kantenlinien, die in 3D zu diesen roten ft-Bauteilen gehören.
Den Quadratzapfen habe ich nur einmal erstellt und für die verschiedenen Bauteilseiten separat in seinen Koordinaten modifiziert. Er wird zur Arbeitsersparnis bei Bedarf hinzuimportiert und manuell angedockt um dann mit dem  jeweiligen 3D-Teil gruppiert zu werden. Das muß aber mit der erforderlichen Genauigkeit gemeistert werden, weil sonst die Gruppierungen und auch Konvertierungen in andere CAD-Formate scheitern können!
