---
layout: "image"
title: "Grundbaustein"
date: "2014-03-16T17:58:36"
picture: "Grundbaustein.jpg"
weight: "1"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "Thomas Kaltenbrunner (Kalti)"
schlagworte: ["CAD", "Grundbaustein", "SolidWorks"]
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/38467
- /detailscc30.html
imported:
- "2019"
_4images_image_id: "38467"
_4images_cat_id: "2271"
_4images_user_id: "1342"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38467 -->
Grundbaustein mit SolidWorks gezeichnet.
