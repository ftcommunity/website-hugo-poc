---
layout: "image"
title: "Schienenkran"
date: "2004-01-15T21:31:52"
picture: "Kran2.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- /php/details/2071
- /details8c7a.html
imported:
- "2019"
_4images_image_id: "2071"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2071 -->
Ein ft-Schienenkran auf einem Playmobil-Fahrgestell. Hier hebt er gerade einen schweren LGB-Personenwagen, ist allerdings auch mit ein paar hundert Gramm Gegengewicht bestückt.