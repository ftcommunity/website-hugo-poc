---
layout: "image"
title: "Front mit Fahrer"
date: "2005-05-19T21:51:30"
picture: "PICT1254.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["ALWEG", "Monorail", "Einschienenbahn"]
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/4155
- /details3f84.html
imported:
- "2019"
_4images_image_id: "4155"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:51:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4155 -->
Gut Ding will Weile haben!

Lange hab ich mich nicht mehr gemeldet - ich hatte einfach zuviel Arbeit an den Backen gehabt.

Nun, gibt es endlich wieder neue Fotos von meinen Monorailprojekt!