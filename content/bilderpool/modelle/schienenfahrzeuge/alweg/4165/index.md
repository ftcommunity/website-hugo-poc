---
layout: "image"
title: "eine weitere Seitenansicht"
date: "2005-05-19T21:52:54"
picture: "PICT1263.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/4165
- /detailsf884.html
imported:
- "2019"
_4images_image_id: "4165"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4165 -->
Zum Weichenbau bin ich leider immer noch nicht gekommen, obwohl ich inzwischen auf dafür alle Teile zusammen habe (für die Flexweiche). Die Butterfly-Weiche läßt sich übrigens nicht mit FT realisieren (zumindest nicht für die Baugröße meines Wagens) - ich denke ich werde da auch auf Baumarktmaterial zurückgreifen müssen.