---
layout: "image"
title: "Ein Blick durch den Wagenkasten..."
date: "2005-11-23T17:29:51"
picture: "PICT2225.jpg"
weight: "23"
konstrukteure: 
- "Daniel Nowicki"
fotografen:
- "Daniel Nowicki"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- /php/details/5354
- /detailsc5ad.html
imported:
- "2019"
_4images_image_id: "5354"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-11-23T17:29:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5354 -->
