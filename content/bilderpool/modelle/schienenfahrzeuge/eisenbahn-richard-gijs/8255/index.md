---
layout: "image"
title: "close up"
date: "2007-01-02T14:58:38"
picture: "fischertechnik_025.jpg"
weight: "7"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/8255
- /details9d51.html
imported:
- "2019"
_4images_image_id: "8255"
_4images_cat_id: "761"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8255 -->
