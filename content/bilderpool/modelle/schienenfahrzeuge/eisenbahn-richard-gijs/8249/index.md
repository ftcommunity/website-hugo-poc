---
layout: "image"
title: "1"
date: "2007-01-02T14:58:38"
picture: "fischertechnik_006.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/8249
- /details5818.html
imported:
- "2019"
_4images_image_id: "8249"
_4images_cat_id: "761"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8249 -->
