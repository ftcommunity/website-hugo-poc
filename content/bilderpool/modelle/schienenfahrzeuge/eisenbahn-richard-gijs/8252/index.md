---
layout: "image"
title: "curve"
date: "2007-01-02T14:58:38"
picture: "fischertechnik_010.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/8252
- /details91bd.html
imported:
- "2019"
_4images_image_id: "8252"
_4images_cat_id: "761"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8252 -->
