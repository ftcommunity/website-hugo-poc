---
layout: "image"
title: "Hängebahn mit Aufzug"
date: "2006-03-15T14:44:11"
picture: "Fischertechnik-Bilder_001.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5887
- /details070b-2.html
imported:
- "2019"
_4images_image_id: "5887"
_4images_cat_id: "506"
_4images_user_id: "420"
_4images_image_date: "2006-03-15T14:44:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5887 -->
