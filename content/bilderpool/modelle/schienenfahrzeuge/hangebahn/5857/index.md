---
layout: "image"
title: "Bahn"
date: "2006-03-12T12:15:55"
picture: "Fischertechnik_018.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5857
- /detailsc9c5.html
imported:
- "2019"
_4images_image_id: "5857"
_4images_cat_id: "506"
_4images_user_id: "420"
_4images_image_date: "2006-03-12T12:15:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5857 -->
