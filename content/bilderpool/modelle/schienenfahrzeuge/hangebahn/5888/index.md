---
layout: "image"
title: "Hängebahn mit Aufzug"
date: "2006-03-15T14:44:12"
picture: "Fischertechnik-Bilder_002.jpg"
weight: "3"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5888
- /details1db0-2.html
imported:
- "2019"
_4images_image_id: "5888"
_4images_cat_id: "506"
_4images_user_id: "420"
_4images_image_date: "2006-03-15T14:44:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5888 -->
