---
layout: "image"
title: "Steuerung"
date: "2006-03-15T14:44:12"
picture: "Fischertechnik-Bilder_003.jpg"
weight: "4"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5889
- /details1873-2.html
imported:
- "2019"
_4images_image_id: "5889"
_4images_cat_id: "506"
_4images_user_id: "420"
_4images_image_date: "2006-03-15T14:44:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5889 -->
