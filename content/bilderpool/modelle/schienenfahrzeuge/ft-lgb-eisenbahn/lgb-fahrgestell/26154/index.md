---
layout: "image"
title: "von unten"
date: "2010-01-25T22:35:42"
picture: "vonunten.jpg"
weight: "3"
konstrukteure: 
- "kilo70"
fotografen:
- "kilo70"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/26154
- /details4ee0.html
imported:
- "2019"
_4images_image_id: "26154"
_4images_cat_id: "1854"
_4images_user_id: "1071"
_4images_image_date: "2010-01-25T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26154 -->
die Stromabnehmer dürften die von der BSB sein. Passen jedenfalls genau in die Lücke zwischen den Rädern.