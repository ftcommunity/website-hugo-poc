---
layout: "image"
title: "von oben"
date: "2010-01-25T22:35:42"
picture: "vonoben.jpg"
weight: "2"
konstrukteure: 
- "kilo70"
fotografen:
- "kilo70"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- /php/details/26153
- /detailsf201.html
imported:
- "2019"
_4images_image_id: "26153"
_4images_cat_id: "1854"
_4images_user_id: "1071"
_4images_image_date: "2010-01-25T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26153 -->
naja, das alte Problem: es gibt keine passenden Zahnrädern die sich mittig anbauen lassen. So ist aber die Antriebswelle mittig. Dass das wenig bringt fiel mir erst später auf.