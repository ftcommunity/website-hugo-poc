---
layout: "image"
title: "Langholzwagen"
date: "2009-11-02T21:41:41"
picture: "bumpf6.jpg"
weight: "6"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/25630
- /details1b9e-2.html
imported:
- "2019"
_4images_image_id: "25630"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25630 -->
