---
layout: "comment"
hidden: true
title: "15800"
date: "2011-12-01T14:12:41"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Walter, 
das Lauf- und Spurprofil an der Drehscheibe 60 dürfte nunmehr geklärt sein :o)
Der Schienenkopf läuft etwas zum Spurkranz versetzt? Welchen Schienentyp verwendest du?
Gruß, Ingo