---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf1.jpg"
weight: "1"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31496
- /details181d.html
imported:
- "2019"
_4images_image_id: "31496"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31496 -->
Bahnhof mit Abstellgleiss
