---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf7.jpg"
weight: "7"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31502
- /detailsed81-2.html
imported:
- "2019"
_4images_image_id: "31502"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31502 -->
Schnellzuglok
gleicher Antrieb wie Rangierlok
