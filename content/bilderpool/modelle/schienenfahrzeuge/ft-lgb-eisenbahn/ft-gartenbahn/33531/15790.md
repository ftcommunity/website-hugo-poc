---
layout: "comment"
hidden: true
title: "15790"
date: "2011-11-30T15:35:10"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Walter,
mit den Räderbezeichnungen bin ich leider nicht mehr auf dem Laufendem. 2'B1' bedeutet dann zwischen den Anführungsstrichen feste nicht schwenkbare Räder? Interessant wäre mal zu wissen, welchen Schienentyp du hier genau verwendest. Vielleicht brauchen wir mal Kompatibilität fürs rollende Material :o) Ich bin von dem üblichen Spurkranzspiel ausgegangen. Wenn natürlich die Schienenlichte enge Toleranzen hat und das Modell mit enger laufenden Spurkränzen gefahren wird reicht natürlich eine Drehscheibenbreite, wenn sie nicht nur für den Laufkranz sondern dann evtl. auch noch axial am Spurkranz bearbeitet wird.
Gruß, Ingo