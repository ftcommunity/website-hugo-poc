---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-11-20T18:07:00"
picture: "waltermariograf1.jpg"
weight: "12"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33531
- /detailsfd75.html
imported:
- "2019"
_4images_image_id: "33531"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-20T18:07:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33531 -->
Dampflok
Erster Prototyp einer Dampflok für die ft-Gartenbahn.
Batteriebetrieben läuft Sie schon auf meiner Teststrecke im Keller.
