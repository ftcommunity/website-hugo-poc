---
layout: "image"
title: "Stellpult"
date: "2011-12-18T21:03:16"
picture: "waltermariograf1_4.jpg"
weight: "26"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33717
- /details1b17-2.html
imported:
- "2019"
_4images_image_id: "33717"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33717 -->
Stellpult für Entkupplunggleis oder Weichen mit Positionsanzeige.
