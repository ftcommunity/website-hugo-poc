---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-11-30T22:24:35"
picture: "waltermariograf1_2.jpg"
weight: "16"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33589
- /details9fe9-2.html
imported:
- "2019"
_4images_image_id: "33589"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-30T22:24:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33589 -->
Lok 2'B1'
