---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-02T21:43:02"
picture: "ftgartenbahn3.jpg"
weight: "10"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31516
- /detailse832.html
imported:
- "2019"
_4images_image_id: "31516"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-02T21:43:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31516 -->
Ansicht von unten
