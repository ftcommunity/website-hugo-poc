---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-11-20T18:07:00"
picture: "waltermariograf4.jpg"
weight: "15"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/33534
- /details6dd9-2.html
imported:
- "2019"
_4images_image_id: "33534"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-20T18:07:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33534 -->
