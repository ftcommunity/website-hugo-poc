---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-02T21:43:02"
picture: "ftgartenbahn1.jpg"
weight: "8"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31514
- /details7fe6.html
imported:
- "2019"
_4images_image_id: "31514"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-02T21:43:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31514 -->
Innenleben bei beiden Loks gleich
