---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-02T21:43:02"
picture: "ftgartenbahn2.jpg"
weight: "9"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31515
- /details8164-2.html
imported:
- "2019"
_4images_image_id: "31515"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-02T21:43:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31515 -->
Drehschemel abmontiert
In der Mitte sieht man die Antriebswelle die durch den Drehschemel hindurch führt
