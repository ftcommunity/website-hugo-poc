---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf3.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/31498
- /details92e3.html
imported:
- "2019"
_4images_image_id: "31498"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31498 -->
Steuerung mit selbstgebasteltem Flip-Flop mit Relais
