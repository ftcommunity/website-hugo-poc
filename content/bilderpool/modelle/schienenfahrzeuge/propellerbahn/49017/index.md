---
layout: "image"
title: "Der Wagen"
date: 2021-01-25T17:43:47+01:00
picture: "2021-01-10 Propellerbahn1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zwei S-Motoren (XS-Motoren erwiesen sich als nicht kräftig genug) pusten jeweils in die selbe Richtung. Der Wagen rollt einfach auf dem Tisch auf einer Schiene aus U-Trägern 150, wie das Video unter https://youtu.be/QxTXcjlOnNw zeigt.

Ähnlichkeiten mit der Magnetschwebebahn des Clubheftes 1973-1 sind nicht mehr viele vorhanden, aber es funktioniert.