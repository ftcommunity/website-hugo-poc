---
layout: "image"
title: "Stromabnehmer"
date: 2021-01-25T17:43:45+01:00
picture: "2021-01-10 Propellerbahn3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Stromabnehmer sind "Federkontakte" aus dem älteren fischertechnik-Elektromechanik-Programm. Metallachsen 30 könnten evtl. auch funktionieren.

Hier habe ich endlich mal einen Einsatzzweck für die komischen Ringe gefunden, von denen ich mal einen Schwung gekauft hatte. Die gehören aber nicht zu fischertechnik, sondern zum fischerform-Programm. Hier dienen sie dazu, einen gerade richtigen Anpressdruck der Kontakte auf die darunter liegenden Stromschienen aufzubauen. Mit Federn oder dergleichen wäre die Reibung so hoch, dass der Wagen nicht vom Fleck kommt.