---
layout: "image"
title: "Hauptbahnhof und Steuerung (1)"
date: 2021-01-25T17:43:42+01:00
picture: "2021-01-10 Propellerbahn6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wenn der Wagen hier ankommt, knallt er einfach gegen die recht weiche Federung. Der XS-Motor rechts treibt ein Paar der älteren fischertechnik-Schleifringe an, die die richtige Stromzufuhr steuert.