---
layout: "image"
title: "Entfernter Bahnhof"
date: 2021-01-25T17:43:38+01:00
picture: "2021-01-10 Propellerbahn9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der entfernten Seite liegt einfach nochmal die federnde Haltestation.