---
layout: "image"
title: "Steuerung (3)"
date: 2021-01-25T17:43:39+01:00
picture: "2021-01-10 Propellerbahn8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf der anderen Seite sitzen ebenfalls zwei Schleifkontakte, aber andersherum gepolt. Wenn die also Kontakt bekommen, führen sie den Stromschienen Strom in anderer Polung zu - die Bahn fährt zurück zum Hauptbahnhof.

Die aufgesteckten Unterbrecher sorgen dafür, dass immer nur einer der Abnehmerpaare Kontakt mit den Schleifringen hat - ansonsten gäbe es einen Kurzschluss. Die Zeit, in der beide Abnehmer unterbrochen sind, ist die Zeit, in der die Motoren ausgeschaltet sind - die Wartezeit im Bahnhof vor der Weiterfahrt.