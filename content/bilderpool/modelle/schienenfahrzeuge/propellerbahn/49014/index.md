---
layout: "image"
title: "Stromschienen (1)"
date: 2021-01-25T17:43:44+01:00
picture: "2021-01-10 Propellerbahn4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auf diese Schienen wird der Wagen aufgesetzt. Die Stromschienen sind lange Strom-Achsen, ebenfalls aus dem älteren fischertechnik-Elektromechanik-Programm. Die haben an einem Ende Buchsen und am anderen Stifte, die wiederum in Buchsen passen.