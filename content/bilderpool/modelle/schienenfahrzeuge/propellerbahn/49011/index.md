---
layout: "image"
title: "Steuerung (2)"
date: 2021-01-25T17:43:40+01:00
picture: "2021-01-10 Propellerbahn7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Kontakte der rechten der beiden BS15 führen Strom auf die Schleifringe. Intern sind die so verbunden, dass die beiden rechten bzw. die beiden linken Bahnen miteinander verbunden sind. Ganz rechts kommt also "-" auf die rechte Bahn und "+" auf die linke.

Der zweie BS15 (links vorne) bekommt also ebenfalls auf der rechten Bahn "-" und auf der linken "+" und führt das gerade zu den Stromschienen - die Bahn fährt vom Hauptbahnhof weg.