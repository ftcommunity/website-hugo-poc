---
layout: "image"
title: "Die Unterseite des Wagens"
date: 2021-01-25T17:43:46+01:00
picture: "2021-01-10 Propellerbahn2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die äußeren Rollen führen den Wagen, auf den inneren rollt er auf dem Tisch. In der Mitte sieht man die Stromabnehmer, die von zwei durchgehenden Metallachsen den Strom zu den Motoren bringen.