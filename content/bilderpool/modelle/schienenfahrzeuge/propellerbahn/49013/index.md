---
layout: "image"
title: "Stromschienen (2)"
date: 2021-01-25T17:43:43+01:00
picture: "2021-01-10 Propellerbahn5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dadurch können die prinzipiell beliebig verlängert werden und so Strom über weite Strecken transportieren.

Jede Übergangsstelle hat aber wohl etwas Übergangswiderstand, sodass weiter entfernt weniger Spannung ankommt als ganz am Anfang (wenn man nicht gerade Strom von beiden Seiten zuführt).