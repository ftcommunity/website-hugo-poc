---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:15"
picture: "lgbcablecar26.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47081
- /details9f85.html
imported:
- "2019"
_4images_image_id: "47081"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:15"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47081 -->
Youtube-link :
https://www.youtube.com/watch?v=vPeT26sIquk&t=


Link to a Ride on Wellington's Cable Car - New Zealand
https://www.youtube.com/watch?v=1SV7WUQtvko
https://www.wellingtoncablecar.co.nz/DEUTSCHE/STARTSEITE

Link zum Geheimnisse der Karlsruhe Turmbergbahn  (Ralf Geerken) :
https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2015-4.pdf

Link zum LGB -Unimog als Zweiwegefahrzeug für Rangier-arbeiten :
https://www.ftcommunity.de/details.php?image_id=45567#col3

Link zum LGB -Eisenbahn-Draisine
https://www.ftcommunity.de/details.php?image_id=45577
