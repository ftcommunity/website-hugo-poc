---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar07.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47062
- /details628a-2.html
imported:
- "2019"
_4images_image_id: "47062"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47062 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=
