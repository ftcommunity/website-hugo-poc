---
layout: "image"
title: "Wellington's Cable Car - New Zealand"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47056
- /detailsdd7d.html
imported:
- "2019"
_4images_image_id: "47056"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47056 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t= 



Link to a Ride on Wellington's Cable Car - New Zealand   ( seen in 1998 )

https://www.youtube.com/watch?v=1SV7WUQtvko

https://www.wellingtoncablecar.co.nz/DEUTSCHE/STARTSEITE


Link zum Geheimnisse der Karlsruhe Turmbergbahn  (Ralf Geerken) :

https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2015-4.pdf
