---
layout: "image"
title: "Wellington's Cable Car - New Zealand"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar04.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47059
- /detailsada5-2.html
imported:
- "2019"
_4images_image_id: "47059"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47059 -->
Link to a Ride on Wellington's Cable Car - New Zealand

https://www.youtube.com/watch?v=1SV7WUQtvko

https://www.wellingtoncablecar.co.nz/DEUTSCHE/STARTSEITE
