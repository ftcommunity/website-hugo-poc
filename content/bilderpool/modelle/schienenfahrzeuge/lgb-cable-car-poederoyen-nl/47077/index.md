---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:15"
picture: "lgbcablecar22.jpg"
weight: "22"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47077
- /detailse2c4-2.html
imported:
- "2019"
_4images_image_id: "47077"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:15"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47077 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=
