---
layout: "image"
title: "LGB -Unimog als Zweiwegefahrzeug für Rangier-arbeiten :"
date: "2018-01-10T19:17:15"
picture: "lgbcablecar25.jpg"
weight: "25"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/47080
- /details8923-2.html
imported:
- "2019"
_4images_image_id: "47080"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:15"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47080 -->
Link zum LGB -Unimog als Zweiwegefahrzeug für Rangierarbeiten :

https://www.ftcommunity.de/details.php?image_id=45567#col3
