---
layout: "comment"
hidden: true
title: "7775"
date: "2008-11-16T20:38:30"
uploadBy:
- "flyingcat"
license: "unknown"
imported:
- "2019"
---
Ja, das sind die 8:1-er Powermotoren. Wenn man wie ich noch die alten grauen Motoren gewohnt ist, dann ist die Kraft der neuen Powermotoren wirklich sagenhaft.

Höchste Geschwindigkeit, die ich per Lichtschranke gemessen habe, waren 7,4 km/h, das sind etwa 206 cm/s oder 2060 mm/s.