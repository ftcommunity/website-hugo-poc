---
layout: "overview"
title: "Highspeed-Monorail"
date: 2020-02-22T08:35:10+01:00
legacy_id:
- /php/categories/1468
- /categories1ebd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1468 --> 
Eigentlich wollte ich ein Transportsystem für Modell-Paletten bauen. Und das sollte eigentlich nicht auf Schienen laufen. Und dann wurde mir klar, dass es kaum zu schaffen ist, präzises Navigieren im Raum zu ermöglichen, wenn man nur zwei Impulsräder mit jeder Menge Schlupf und Spiel hat. Also habe ich mit einem Monorail-System experimentiert - der Antrieb wurde durch Powermotoren mit roter Kappe erledigt. MIt viel Experimentieren hat das dann recht gut geklappt, und dann.... ja dann kam die Frage auf, was wohl passiert, wenn ich die Motoren durch die mit schwarzer Kappe ersetze. Noch mehr herumprobiert und geboren war die Highspeed-Variante auf einer Schiene. Wichtig war mir, dass das System sich auch über längere Laufzeit bewährt, und das hat es - bis jetzt wurden insgesamt 8,3 km zurückgelegt.



Mit vollem Akku komme ich auf gemessene 7,4 km/h Höchstgeschwindigkeit. Um das zu erreichen, wird mit Beschleunigungs- und Bremsrampen in der Software gearbeitet. Die Akkuspannung wird ständig überwacht, sobald sie unter 7.8 V fällt, wird vollautomatisch eine dreiminütige Pause zum Akkuladen eingelegt (wie an den Akkulader angedockt wird folgt auf den Bildern). Dadurch ergibt sich fast genau eine Wechsel zwischen 3 Minuten laden und 3 Minuten fahren.



Das Fahrwerk ermöglich hohe Geschwindigkeit, hat aber einen grossen Nachteil: Kurven sind nicht möglich.



Achja, ich höre schon den Ruf nach einem Video... bitte sehr: www.flying-cat.de/ft/Monorail.wmv (3,6 MB).