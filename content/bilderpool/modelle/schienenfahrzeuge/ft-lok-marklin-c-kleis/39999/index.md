---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis07.jpg"
weight: "7"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/39999
- /detailsbcdc.html
imported:
- "2019"
_4images_image_id: "39999"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39999 -->
Den Motor befestigt und die Drähte von den beiden Schleifer (FT und Märklin) angeschlossen