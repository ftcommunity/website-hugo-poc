---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis10.jpg"
weight: "10"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/40002
- /details30f7.html
imported:
- "2019"
_4images_image_id: "40002"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40002 -->
Teilweise zusammen gebaute Lok. Die Kabeln werden im vorder Teil versteckt.