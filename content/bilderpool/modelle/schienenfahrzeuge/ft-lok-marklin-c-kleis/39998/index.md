---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis06.jpg"
weight: "6"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/39998
- /details8ce3.html
imported:
- "2019"
_4images_image_id: "39998"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39998 -->
Das Kabel wird an den Original-Schleifer von der FT angesteckt.