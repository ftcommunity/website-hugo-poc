---
layout: "image"
title: "Waggon"
date: "2009-03-07T14:35:52"
picture: "DSCN2635.jpg"
weight: "27"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23416
- /details4926.html
imported:
- "2019"
_4images_image_id: "23416"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23416 -->
so sieht er von unten aus
