---
layout: "image"
title: "Weiche links"
date: "2009-04-05T15:40:19"
picture: "DSCN2641.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23592
- /details9e60-2.html
imported:
- "2019"
_4images_image_id: "23592"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-04-05T15:40:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23592 -->
erste elektrifizierte Weiche - Prototyp -
mit Endschaltern auf jeder Seite.
