---
layout: "image"
title: "Gesamtansicht"
date: "2009-02-19T11:53:06"
picture: "DSCN2619.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17446
- /details8a87-3.html
imported:
- "2019"
_4images_image_id: "17446"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17446 -->
Hier der erste Zug.....
