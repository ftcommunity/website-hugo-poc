---
layout: "image"
title: "Weiche"
date: "2009-04-05T15:40:20"
picture: "DSCN2643.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23594
- /details14b6.html
imported:
- "2019"
_4images_image_id: "23594"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-04-05T15:40:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23594 -->
Mechanik am Drehpunkt.
Sicht von unten.
