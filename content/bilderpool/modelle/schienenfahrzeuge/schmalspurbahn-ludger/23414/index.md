---
layout: "image"
title: "Lorenwaggon"
date: "2009-03-07T14:35:52"
picture: "DSCN2638.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23414
- /detailscb37-2.html
imported:
- "2019"
_4images_image_id: "23414"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23414 -->
von unten
