---
layout: "comment"
hidden: true
title: "8552"
date: "2009-02-19T18:17:08"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo ludger-ftc,
dein Modellthema nun mit ersten Fotos vorliegend ist für mich sehr interessant, weil ich mich auch mal später in etwa dieser Grössenordnung versuchen will.
Möchte hier aber nicht schon Gesagtes wiederholen, dennoch: Ich würde das Gelenk bei der grossen Lok wie bei der kleinen besser in Höhe der Kupplung anordnen, senkrechte Antriebsmotoren ob ein oder zwei bei dieser Fahrzeugbreite und -höhe nein, starre Spurführung nein.
Das mit der Kurve - welches Bogenstück nimmst du? - rechne ich trotzdem mal nach.
Gruss, Udo2