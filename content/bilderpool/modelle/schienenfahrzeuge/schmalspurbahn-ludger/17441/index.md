---
layout: "image"
title: "große Dampflok"
date: "2009-02-19T11:53:06"
picture: "DSCN2614.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17441
- /details2c29.html
imported:
- "2019"
_4images_image_id: "17441"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17441 -->
Hier habe ich eine Aufnahme gemacht auf der man die Drehpunkte gut erkennen kann.
