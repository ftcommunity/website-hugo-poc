---
layout: "image"
title: "Loren Waggon von unten"
date: "2009-02-19T11:53:06"
picture: "DSCN2618.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17445
- /details8544.html
imported:
- "2019"
_4images_image_id: "17445"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17445 -->
Hier sieht man gut wie ich die Unterkonstruktion gebaut habe.
Hat sehr lange gedauert bis es so aussah wie auf diesem Foto.
