---
layout: "image"
title: "ein Waggon"
date: "2009-02-19T11:53:06"
picture: "DSCN2615.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/17442
- /details7963.html
imported:
- "2019"
_4images_image_id: "17442"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17442 -->
dieser mit zwei Drehgestellen
