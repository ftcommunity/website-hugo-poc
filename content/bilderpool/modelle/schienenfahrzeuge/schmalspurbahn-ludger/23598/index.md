---
layout: "image"
title: "sehr große Dampflok"
date: "2009-04-05T15:40:20"
picture: "DSCN2675.jpg"
weight: "34"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23598
- /detailsbc17.html
imported:
- "2019"
_4images_image_id: "23598"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-04-05T15:40:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23598 -->
Ansicht von vorn
