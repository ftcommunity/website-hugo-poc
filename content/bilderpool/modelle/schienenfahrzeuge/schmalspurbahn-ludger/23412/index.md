---
layout: "image"
title: "Die kleine Dampflok"
date: "2009-03-07T14:35:51"
picture: "DSCN2636.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/23412
- /detailsddec.html
imported:
- "2019"
_4images_image_id: "23412"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-03-07T14:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23412 -->
so sieht sie von unten aus
