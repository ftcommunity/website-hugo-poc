---
layout: "comment"
hidden: true
title: "6164"
date: "2008-04-06T04:06:38"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Johannes,
dann sind ja meine Tipps für den Umgang mit deiner Digicam nicht umsonst. Für den sicher vorhandenen Autofocus mußt du im Nahbereich mit geringerer Schärfentiefe dich auf einen Entfernungspunkt am Objekt zur Einstellung der Schärfe entscheiden. Eine Nahbrille für die Kontrolle der Schärfe auf dem LCD-Color-Monitor wirst du ja wohl noch nicht benötigen, oder? Sollte dir die Digicam mal runtergefallen sein, könnte auch eine Dejustierung des Objektivs die Ursache sein.
Also dann mal los.
Gruß Udo2