---
layout: "image"
title: "Antrieb (2)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48223
- /details8c77.html
imported:
- "2019"
_4images_image_id: "48223"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48223 -->
Der Trick mit dem Gummi in der Drehscheiben-Nut zum besseren Antrieb des Seils ist schamlos von Ralf Geerken übernommen - Danke Dir, Ralf! Der XM-Motor treibt das Z40 darüber über eine mot-1-Schnecke an. Damit die trotz der recht großen auftretenden Kräfte sauber gelagert und geführt wird, steckt in ihr von der Gegenseite ein mot-1-Getriebeaufsatz ohne Schnecke.
