---
layout: "image"
title: "Fahrgestelle (1)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48231
- /detailsfb0b.html
imported:
- "2019"
_4images_image_id: "48231"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48231 -->
Jeder Wagen sitzt auf zwei solcher Fahrgestelle, die drehbar gelagert sind. Die jeweils oberen haben noch ein paar Bauteile mehr für die Seilanknüpfung.
