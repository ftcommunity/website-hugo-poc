---
layout: "image"
title: "Türen offen in der Bergstation"
date: "2018-10-15T16:10:18"
picture: "standseilbahn23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48241
- /details4da2-2.html
imported:
- "2019"
_4images_image_id: "48241"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48241 -->
Bei der Bergstation gilt analog dasselbe.
