---
layout: "image"
title: "Kurvenfahrt (3)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48235
- /detailsd72b-2.html
imported:
- "2019"
_4images_image_id: "48235"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48235 -->
Der rechte Wagen bei der Bergfahrt kurz vor der oberen Weiche.
