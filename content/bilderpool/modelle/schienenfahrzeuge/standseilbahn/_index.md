---
layout: "overview"
title: "Standseilbahn"
date: 2020-02-22T08:35:51+01:00
legacy_id:
- /php/categories/3539
- /categories5d67.html
- /categoriescc4c.html
- /categoriesbcb4.html
- /categories0174.html
- /categoriesba09-2.html
- /categoriesf394.html
- /categoriese728.html
- /categories5f55-3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3539 --> 
Die Vorlagen von Peter Damen (https://www.ftcommunity.de/suche.php?suchbegriff=cable-car&R1=search_all&action=suche) und insbesondere die wunderbare Turmbergbahn von Ralf Geerken (https://www.ftcommunity.de/suche.php?suchbegriff=turmberg&R1=search_all&action=suche) weckten den Wunsch in mir, auch so eine Bahn zu bauen.