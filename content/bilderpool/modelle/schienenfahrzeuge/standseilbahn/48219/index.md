---
layout: "image"
title: "Gesamtansicht"
date: "2018-10-15T16:10:17"
picture: "standseilbahn01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48219
- /detailsdab4.html
imported:
- "2019"
_4images_image_id: "48219"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48219 -->
Die Vorlagen von Peter Damen (https://www.ftcommunity.de/suche.php?suchbegriff=cable-car&R1=search_all&action=suche) und insbesondere die wunderbare Turmbergbahn von Ralf Geerken (https://www.ftcommunity.de/suche.php?suchbegriff=turmberg&R1=search_all&action=suche) mit ihrer technisch ausgefeilten Schienenkonstruktion weckten den Wunsch in mir, auch so eine Bahn zu bauen.
