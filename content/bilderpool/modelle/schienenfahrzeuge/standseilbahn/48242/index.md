---
layout: "image"
title: "Transport- und Wartungssicherungen"
date: "2018-10-15T16:10:18"
picture: "standseilbahn24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48242
- /details53dd.html
imported:
- "2019"
_4images_image_id: "48242"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48242 -->
Diese beiden Sicherungshaltungen kann man anstecken, sodass man bei praktisch nicht gespanntem Seil Wartung machen kann (z.B. Einfetten der Metallachsen beim Antrieb, Justieren der Seillänge etc.).
