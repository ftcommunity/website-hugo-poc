---
layout: "image"
title: "Blick von unten unter die Wagen"
date: "2018-10-15T16:10:18"
picture: "standseilbahn12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48230
- /detailsae96-3.html
imported:
- "2019"
_4images_image_id: "48230"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48230 -->
Von Talseite unter die Wagen geblickt sieht man die drehbaren Fahrgestelle. Die sind eine Weiterentwicklung gegenüber den starr angebrachten Rädern, wie ich sie auf der Convention 2018 noch hatte.
