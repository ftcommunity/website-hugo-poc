---
layout: "image"
title: "Schienenführung"
date: "2018-10-15T16:10:18"
picture: "standseilbahn10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48228
- /details7544.html
imported:
- "2019"
_4images_image_id: "48228"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48228 -->
Die Schienenführung ist viel weniger ausgefeilt als bei Ralfs Modell, aber sie funktioniert.
