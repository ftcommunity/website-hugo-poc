---
layout: "image"
title: "Fixierung der Bauplatten 500"
date: "2018-10-15T16:10:18"
picture: "standseilbahn09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48227
- /details6f74.html
imported:
- "2019"
_4images_image_id: "48227"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48227 -->
Die Bauplatten 500 sitzen in Stiften der Vorstufe-Radaufhängungen, die in regelmäßigen Abständen im Statik-"Berg" sitzen. "Berg" trifft es auch insofern, dass in diesem Modell exakt alle meine grauen Statikträger 120 verbaut sind.
