---
layout: "image"
title: "Personenwagen in rot"
date: "2009-10-29T19:19:11"
picture: "personenwagen1.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/25582
- /details22c3-2.html
imported:
- "2019"
_4images_image_id: "25582"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T19:19:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25582 -->
Deutlich schöner als der Vorgänger. Es leben die roten Bausteine(Ich hätte nie gedacht, dass ich das mal schreibe).
Natürlich mit hohen Fensterscheiben, passend zum Glacier-Express ;-)
Stühle sind auch drin. Innenbeleuchtung fehlt noch, kommt aber vermutlich Morgen.
