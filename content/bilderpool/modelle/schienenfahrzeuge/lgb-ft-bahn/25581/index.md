---
layout: "image"
title: "Ansicht von Unten"
date: "2009-10-29T11:59:32"
picture: "2009_10_291.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/25581
- /detailsa1e2.html
imported:
- "2019"
_4images_image_id: "25581"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T11:59:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25581 -->
Falls die vorhandenen Bilder nicht zum Nachbau reichen, mache ich auch gerne noch ein Paar im halbzerlegten Zustand.
Achja, natürlich sind beim linken Drehgestell die Achsen zu lang, fragt mich nicht, wieso ich da nicht die Kürzeren genommen habe...
