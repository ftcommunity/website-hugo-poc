---
layout: "comment"
hidden: true
title: "10148"
date: "2009-10-29T20:49:57"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Man könnte allerdings auch einen Draht durch den Spurkranz hindurch
an die Achse führen. Und da es ja immer mindestens zwei Achsen gibt,
hätte man die nötige Stromzufuhr, die man dann dezent von der Mitte
abnehmen und in die Elektrik führen kann. Natürlich muß bei der einen
Achse der rechte und bei der anderen der linke Spurkranz entsprechend
modifiziert werden.

Gruß, Thomas