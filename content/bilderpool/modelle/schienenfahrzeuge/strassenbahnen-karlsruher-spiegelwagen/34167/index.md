---
layout: "image"
title: "Inneneinrichtung Spiegelwagen"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen2.jpg"
weight: "2"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34167
- /details0b1b.html
imported:
- "2019"
_4images_image_id: "34167"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34167 -->
Der Spiegelwagen verdankt seinen Namen ovalen Spiegeln, die innen zwischen den fünf Fenstern des Fahrgastraums angebracht waren. Später wurden die Spiegel zu Gunsten eines sechsten Fensters entfernt. Der Spiegelwagen besaß erstmals eine Heizung im Fahrgastraum, mit Leder bezogene, gepolsterte Sitze, Schiebetüren zwischen Fahrgastraum und den beiden Plattformen sowie eine Fußbodenheizung für den Fahrer. Der Spiegelwagen verfügte über 24 Sitz- und 20 Stehplätze.