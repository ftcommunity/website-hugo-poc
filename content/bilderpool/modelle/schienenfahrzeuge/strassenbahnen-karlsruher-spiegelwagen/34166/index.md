---
layout: "image"
title: "Karlsruher Spiegelwagen (Triebwagen 100)"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen1.jpg"
weight: "1"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34166
- /details2ae1-2.html
imported:
- "2019"
_4images_image_id: "34166"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34166 -->
Der Karlsruher Spiegelwagen (http://de.wikipedia.org/wiki/Spiegelwagen) löste 1929 den Residenzwagen (1913-1929) als Straßenbahnwagen ab. Er war mit 94 kW deutlich stärker als der Residenzwagen und konnte bis zu drei Beiwagen ziehen. Auf gerader Strecke erreichte er eine Geschwindigkeit von 40 km/h. Er wurde bis 1941 in der Waggonfabrik Rastatt gebaut und war auf ausgewählten Strecken bis 1972 im Einsatz. Die auf dem Bild zu sehende "Nr. 100" fährt heute noch zu besonderen Anlässen.