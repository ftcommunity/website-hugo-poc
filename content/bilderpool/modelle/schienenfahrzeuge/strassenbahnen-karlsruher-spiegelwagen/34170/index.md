---
layout: "image"
title: "Stromabnehmer Spiegelwagen"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen5.jpg"
weight: "5"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34170
- /details10cb.html
imported:
- "2019"
_4images_image_id: "34170"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34170 -->
Der Scherenstromabnehmer hat im Modell noch keine Funktion; es fehlen noch die zugehörigen Oberleitungen ...