---
layout: "image"
title: "Zielfilmkasten 4"
date: "2012-02-18T19:58:49"
picture: "Zielfilmkasten_Deckel_oben.jpg"
weight: "12"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/34242
- /details08b2.html
imported:
- "2019"
_4images_image_id: "34242"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-18T19:58:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34242 -->
Das ist der obere Deckel, auf dem die "Laterne" montiert ist, die die Nummer der Linie anzeigt.