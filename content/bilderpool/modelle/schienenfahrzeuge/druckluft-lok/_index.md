---
layout: "overview"
title: "Druckluft-Lok"
date: 2020-02-22T08:35:39+01:00
legacy_id:
- /php/categories/2370
- /categoriesaf12.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2370 --> 
2 Zylinder  4 Takt Druckluft-Lok

Achsfolge: 2'B
Spurweite: 45mm
Kesseldruck: ?
Zugkraft: 1,5l Wasserflasche auf einem 4 Achser
Nur eine Fahrtrichtung
Weichen können noch nicht befahren werden
(nächste Ausbaustufe: el.Ventile, zwei Fahrtrichtungen)
