---
layout: "image"
title: "Lokomotive 008"
date: "2004-12-05T10:22:02"
picture: "Lokomotive_008.JPG"
weight: "8"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3392
- /details30ed-2.html
imported:
- "2019"
_4images_image_id: "3392"
_4images_cat_id: "294"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:22:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3392 -->
