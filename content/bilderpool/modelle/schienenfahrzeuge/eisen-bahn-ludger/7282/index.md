---
layout: "image"
title: "Gleisanlage (1)"
date: "2006-10-30T18:55:43"
picture: "DSCN1075.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7282
- /details7f1d.html
imported:
- "2019"
_4images_image_id: "7282"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-30T18:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7282 -->
ich habe die Anlage nochmal komplett überarbeitet und farblich abgestimmt. Hier die Gesamtansicht der Anlage (eine Fliese ist 33 x 33 cm).
Coesfeld kann kommen ....
