---
layout: "image"
title: "Loren Waggon (Ansicht)"
date: "2005-12-27T15:28:59"
picture: "DSCN0490.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5540
- /details31dd.html
imported:
- "2019"
_4images_image_id: "5540"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:28:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5540 -->
