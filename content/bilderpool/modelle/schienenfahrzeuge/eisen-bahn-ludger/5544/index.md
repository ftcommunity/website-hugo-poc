---
layout: "image"
title: "Loren Waggon (Mechanik 2)"
date: "2005-12-27T15:32:28"
picture: "DSCN0494.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5544
- /detailsa80d.html
imported:
- "2019"
_4images_image_id: "5544"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:32:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5544 -->
Auf eine Metallstange wird das Band aufgewickelt
