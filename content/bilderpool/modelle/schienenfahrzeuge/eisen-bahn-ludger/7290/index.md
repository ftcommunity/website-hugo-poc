---
layout: "image"
title: "Ein Turm ?"
date: "2006-10-30T18:55:43"
picture: "DSCN1077.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7290
- /detailse5a0-2.html
imported:
- "2019"
_4images_image_id: "7290"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-30T18:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7290 -->
Ihr fragt euch bestimmt "Was hat er denn da für ein Ding hingebaut"

Dieses Ding erspart mir die Akkus. Das gelbe Kabel liefert Strom von einem Transformator. Oben auf den Trum ist ein Schleifring montiert, darauf der schwarze Ausleger. Über den Ausleger wird ein zweites Kabel zur Lok geführt. Die schleppt das Kabel mit und dreht den Ausleger so das sich das Kabel nicht um den Turm wickeln kann.

Ich hoffe das alle das verstanden haben ??!!??

Ach ja, gesteuert werden die Loks per IR-Ferbedienung.
