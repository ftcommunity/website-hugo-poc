---
layout: "image"
title: "Gleisanlage (2)"
date: "2006-10-30T18:55:43"
picture: "DSCN1076.jpg"
weight: "33"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7283
- /detailsa403.html
imported:
- "2019"
_4images_image_id: "7283"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-30T18:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7283 -->
Eine andere Perspective
