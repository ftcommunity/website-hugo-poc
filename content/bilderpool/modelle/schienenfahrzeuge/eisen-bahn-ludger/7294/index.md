---
layout: "image"
title: "Waggon"
date: "2006-10-31T19:17:48"
picture: "DSCN1083.jpg"
weight: "43"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7294
- /details6eff.html
imported:
- "2019"
_4images_image_id: "7294"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-31T19:17:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7294 -->
Habe noch einen weiteren Waggon gebaut
