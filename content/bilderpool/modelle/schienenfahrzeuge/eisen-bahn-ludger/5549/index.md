---
layout: "image"
title: "Lok mit 'Akku' Tender"
date: "2005-12-29T17:20:42"
picture: "DSCN0520.jpg"
weight: "27"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5549
- /detailsc1bc.html
imported:
- "2019"
_4images_image_id: "5549"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-29T17:20:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5549 -->
Die Stromversorgung für die Lok ist gesichert.
Das Ganze funktioniert mit der IR Fernedienung.
