---
layout: "image"
title: "gerades Gleis"
date: "2005-12-27T15:08:57"
picture: "DSCN0519.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5522
- /detailsfb33.html
imported:
- "2019"
_4images_image_id: "5522"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5522 -->
Spurweite 9 cm ...
