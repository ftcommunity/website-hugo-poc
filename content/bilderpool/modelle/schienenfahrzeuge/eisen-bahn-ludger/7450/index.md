---
layout: "image"
title: "elektrische Weiche mit Signal"
date: "2006-11-12T18:26:46"
picture: "DSCN1116.jpg"
weight: "45"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7450
- /details8e2e-2.html
imported:
- "2019"
_4images_image_id: "7450"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7450 -->
Das Signal zeigt nicht die Weichenstellung an sondern ist "nur" eine Hilfe für mich damit ich die richtige Weiche umstellen kann.
