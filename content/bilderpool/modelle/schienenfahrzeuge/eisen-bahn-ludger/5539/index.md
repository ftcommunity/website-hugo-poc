---
layout: "image"
title: "Die Dampflok von unten"
date: "2005-12-27T15:28:59"
picture: "DSCN0489.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5539
- /details1720-3.html
imported:
- "2019"
_4images_image_id: "5539"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:28:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5539 -->
gut zu erkennen ist das IR Empfängermodul im Kessel
