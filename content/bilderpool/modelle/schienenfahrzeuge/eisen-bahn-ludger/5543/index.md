---
layout: "image"
title: "Loren Waggon (Mechanik 1)"
date: "2005-12-27T15:29:06"
picture: "DSCN0491.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/5543
- /detailse0f3-2.html
imported:
- "2019"
_4images_image_id: "5543"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:29:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5543 -->
