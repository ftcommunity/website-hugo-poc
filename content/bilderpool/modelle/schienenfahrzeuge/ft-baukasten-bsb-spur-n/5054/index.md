---
layout: "image"
title: "ft_BSB_N_Variante"
date: "2005-10-02T14:23:35"
picture: "ft_BSB_N_Variante.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Derks"
schlagworte: ["BSB", "N-Spur", "Baukasten", "Anleitung"]
uploadBy: "peterderks"
license: "unknown"
legacy_id:
- /php/details/5054
- /details2f85.html
imported:
- "2019"
_4images_image_id: "5054"
_4images_cat_id: "369"
_4images_user_id: "363"
_4images_image_date: "2005-10-02T14:23:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5054 -->
Die Bauspielbahn N gab es in 2 Varianten. Hier die Ausführung, bei der die Bauteile in 2 Spritzguß-Stücken zusammenhängen.