---
layout: "image"
title: "Deckel BSB-Spur N"
date: "2005-08-12T10:41:25"
picture: "BSB-Spur_N_Front.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "olliillo"
license: "unknown"
legacy_id:
- /php/details/4536
- /detailsc405.html
imported:
- "2019"
_4images_image_id: "4536"
_4images_cat_id: "369"
_4images_user_id: "331"
_4images_image_date: "2005-08-12T10:41:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4536 -->
