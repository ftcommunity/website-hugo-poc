---
layout: "image"
title: "Gondel Seite"
date: "2005-11-30T22:50:57"
picture: "gondel_seite.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/5441
- /detailsbaa3-2.html
imported:
- "2019"
_4images_image_id: "5441"
_4images_cat_id: "470"
_4images_user_id: "373"
_4images_image_date: "2005-11-30T22:50:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5441 -->
Die Gondel von der Seite
