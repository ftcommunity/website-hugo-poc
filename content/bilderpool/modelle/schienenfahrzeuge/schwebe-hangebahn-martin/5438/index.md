---
layout: "image"
title: "Aufhängung"
date: "2005-11-30T22:50:56"
picture: "aufhaengung.jpg"
weight: "1"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
schlagworte: ["Schwebebahn", "Hängebahn"]
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/5438
- /details851e.html
imported:
- "2019"
_4images_image_id: "5438"
_4images_cat_id: "470"
_4images_user_id: "373"
_4images_image_date: "2005-11-30T22:50:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5438 -->
Die Aufhängung meiner ersten Schwebebahn, vorerst ohne Motor.
