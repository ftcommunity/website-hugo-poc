---
layout: "image"
title: "Schwebebahn komplett"
date: "2005-11-30T22:50:57"
picture: "schwebebahn1.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/5442
- /details6abd-2.html
imported:
- "2019"
_4images_image_id: "5442"
_4images_cat_id: "470"
_4images_user_id: "373"
_4images_image_date: "2005-11-30T22:50:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5442 -->
Ein Überblick über die ganze Schwebebahn, wegen Teilemangels ist sie leider nich größer geworden.
