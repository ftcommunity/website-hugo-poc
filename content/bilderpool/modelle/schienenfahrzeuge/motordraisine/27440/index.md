---
layout: "image"
title: "Seitenansicht"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_01.jpg"
weight: "1"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27440
- /details8429-3.html
imported:
- "2019"
_4images_image_id: "27440"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27440 -->
Video: [Motordraisine](http://www.youtube.com/watch?v=AnGz6IlAC4k)
Maßstab: 1:6
Spurweite: 127 mm (5")
Probefahrt auf der Gartenbahn.

Vorbild: Motordraisine X 614 der ÖBB für Schmalspur 760 mm
