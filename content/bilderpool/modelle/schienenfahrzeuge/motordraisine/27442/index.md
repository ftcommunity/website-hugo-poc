---
layout: "image"
title: "Kupplung"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_03.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27442
- /details013a-2.html
imported:
- "2019"
_4images_image_id: "27442"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27442 -->
