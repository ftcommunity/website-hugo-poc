---
layout: "image"
title: "Vorder- / Seitenansicht"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_02.jpg"
weight: "2"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27441
- /detailsd5d7-2.html
imported:
- "2019"
_4images_image_id: "27441"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27441 -->
