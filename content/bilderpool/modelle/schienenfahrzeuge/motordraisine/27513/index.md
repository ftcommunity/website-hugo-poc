---
layout: "image"
title: "Kleinwagen fertig"
date: "2010-06-14T14:39:14"
picture: "Motordraisine_11.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/27513
- /details7335-3.html
imported:
- "2019"
_4images_image_id: "27513"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-14T14:39:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27513 -->
Die Seitenteile der Draisine bestehen aus Papier. Es hat zufällig den gleichen Farbton wie ft.
Neu dazugekommen sind Spitzensignal und Zugschluß an beiden Stirnseiten, unabhängig voneinander zu schalten und eine "Hupe" (= Summer). Sie wird über die Fernsteuerung betätigt (Servo, der einen Taster betätigt).
Auch habe ich das Fahrwerk verstärkt.
