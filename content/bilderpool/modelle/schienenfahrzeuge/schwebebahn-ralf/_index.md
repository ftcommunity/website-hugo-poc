---
layout: "overview"
title: "Schwebebahn von Ralf"
date: 2020-02-22T08:35:02+01:00
legacy_id:
- /php/categories/529
- /categories2117.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=529 --> 
Ein Schwebebahnsystem das sich sehr leicht erweitern lässt. Eine Schiene ist sehr einfach aufgebaut. Die Bahn kann die Kurven in beiden Richtungen (S-Kurve) durchlaufen und auch kleine Steigungen überwinden.