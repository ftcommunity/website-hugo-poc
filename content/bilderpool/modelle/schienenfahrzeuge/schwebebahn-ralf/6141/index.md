---
layout: "image"
title: "In einer Kurve"
date: "2006-04-26T16:02:09"
picture: "Schwebebahn04.jpg"
weight: "4"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Schwebebahn"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/6141
- /detailsc7d9-2.html
imported:
- "2019"
_4images_image_id: "6141"
_4images_cat_id: "529"
_4images_user_id: "381"
_4images_image_date: "2006-04-26T16:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6141 -->
