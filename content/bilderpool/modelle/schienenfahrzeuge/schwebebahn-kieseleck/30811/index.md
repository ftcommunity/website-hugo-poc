---
layout: "image"
title: "Radaufhängung(1)"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck05.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30811
- /detailsf58f.html
imported:
- "2019"
_4images_image_id: "30811"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30811 -->
Die Räder sind in einem 60 Grad Winkel zur Schiene aufgehängt.