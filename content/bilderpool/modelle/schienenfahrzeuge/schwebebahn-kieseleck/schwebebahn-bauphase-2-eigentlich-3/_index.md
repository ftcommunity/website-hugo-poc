---
layout: "overview"
title: "Schwebebahn Bauphase 2 (eigentlich 3)"
date: 2020-02-22T08:35:35+01:00
legacy_id:
- /php/categories/2313
- /categories050f.html
- /categoriesf874.html
- /categories8bb6.html
- /categories5292.html
- /categories9fba.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2313 --> 
Leider war ich so töricht die Bilder der Bauphase drei zu Löschen bevor sie hochgeladen waren. So könnt ihr nur die Bauphase 3-Bilder bewundern. Am Anfang der Bauphase drei standen Probleme mit dem Wagen der dritten Generation (v. a. bei Rückwärtsfahrten in den neuen 60 Grad Kurven (die Drehgestellaufhängung funktionierte nicht einwandfrei)) und selbst der neue Wagen funktioniert nicht immer (aber meistens) störungsfrei. Da der neue Wagen nicht so flach ist wie der ältere es war passte er nicht in die damals neu dazu gekommene "Central Station" ,die unterm Bett stand. Deshalb musste ich die "Central Station" aufgeben. Ich änderte also den Streckenverlauf, erhöhte die "Central Station", bei der Station 1 (siehe Bauphase 1(http://www.ftcommunity.de/details.php?image_id=30807)) musste ich den "Bahnsteig" heraus nehmen und die Station 2 (noch von Bauphase 2) konnte ich unverändert lassen. So und jetzt schaut euch die Bilder an!