---
layout: "image"
title: "Empfänger"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich22.jpg"
weight: "22"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30967
- /detailsdfdd.html
imported:
- "2019"
_4images_image_id: "30967"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30967 -->
Der Empfänger (aus dem IR Control Set) und eine Batteriebox.