---
layout: "image"
title: "Steuerung der Beleuchtung und des Signals"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich07.jpg"
weight: "7"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30952
- /detailsee53.html
imported:
- "2019"
_4images_image_id: "30952"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30952 -->
Rechts ist der Ein/Aus-Schalter (weder das Signal noch die Beleuchtung kann man seperat Ein/Aus-schalten), links der Taster für das Signal. Dahinter sieht man die Verteilerplatten.