---
layout: "image"
title: "Von Oben"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich13.jpg"
weight: "13"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30958
- /detailsfd13.html
imported:
- "2019"
_4images_image_id: "30958"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30958 -->
Das ganze von Oben.