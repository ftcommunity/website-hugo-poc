---
layout: "image"
title: "Die Aufhängung der Drehgestelle beim Wagen"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich24.jpg"
weight: "24"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30969
- /details6c40.html
imported:
- "2019"
_4images_image_id: "30969"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30969 -->
Auch sie ist neu.