---
layout: "image"
title: "Wagen in Station1"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck04.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30810
- /details1c2e-3.html
imported:
- "2019"
_4images_image_id: "30810"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30810 -->
