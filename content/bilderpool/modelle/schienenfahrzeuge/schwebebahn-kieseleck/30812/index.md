---
layout: "image"
title: "Treibachsenaufhängung"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck06.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30812
- /detailsd6b7-2.html
imported:
- "2019"
_4images_image_id: "30812"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30812 -->
Die Treibachsen (und die Motoren) sind auch in einem 60 Grad Winkel zur Schiene aufgehängt.