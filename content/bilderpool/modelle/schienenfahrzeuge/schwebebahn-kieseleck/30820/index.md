---
layout: "image"
title: "Die Kurve"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck14.jpg"
weight: "14"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30820
- /detailse76b-2.html
imported:
- "2019"
_4images_image_id: "30820"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30820 -->
besteht aus zwei 7,5 Winkelsteinen im Abstand von einem Statikelement.