---
layout: "image"
title: "Station1"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck01.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- /php/details/30807
- /detailsdff1-2.html
imported:
- "2019"
_4images_image_id: "30807"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30807 -->
Sie wurde ursprünglich für die Achterbahn gebaut, ich konnte sie hier aber noch verwenden.