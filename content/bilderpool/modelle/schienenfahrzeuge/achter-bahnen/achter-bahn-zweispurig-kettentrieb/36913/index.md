---
layout: "image"
title: "Ansicht von unten"
date: "2013-05-12T21:59:19"
picture: "achterbahnzweispurigmitkettentrieb13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36913
- /details6391-2.html
imported:
- "2019"
_4images_image_id: "36913"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:19"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36913 -->
Hier sieht man, wie Fahrbahn, Antrieb und die Spur-Räder zusammenspielen. Dieses Bild entstand auf der abschüssigen Rampe. Dort drängt das Fahrzeug von alleine nach unten, weshalb der Faden auf diesem Bild bremst und nicht zieht.
