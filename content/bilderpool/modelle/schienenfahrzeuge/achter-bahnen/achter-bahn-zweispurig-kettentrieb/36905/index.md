---
layout: "image"
title: "LKW (1)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36905
- /detailsc2dc-2.html
imported:
- "2019"
_4images_image_id: "36905"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36905 -->
Eines der Fahrzeuge ist ein kleiner Laster.
