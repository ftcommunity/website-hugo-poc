---
layout: "image"
title: "PKW (2)"
date: "2013-05-12T21:59:19"
picture: "achterbahnzweispurigmitkettentrieb11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36911
- /detailsdf7c-2.html
imported:
- "2019"
_4images_image_id: "36911"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36911 -->
Auch der PKW in 3/4-Ansicht.
