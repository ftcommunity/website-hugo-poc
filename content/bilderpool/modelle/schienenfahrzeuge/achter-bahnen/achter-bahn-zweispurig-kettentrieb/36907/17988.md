---
layout: "comment"
hidden: true
title: "17988"
date: "2013-05-21T19:22:02"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hi Thomas,

ja, nach einigem Tüfeln, was die Radabstände und Anzahl (erst hatte ich zwei pro Auto) und Befestigung der Fäden angeht, läuft das so wie hier gezeigt sehr schön und durchaus Convention-dauerbetriebstauglich. Siehe das Video.

Gruß,
Stefan