---
layout: "image"
title: "Geometrie"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/36902
- /details10d3-2.html
imported:
- "2019"
_4images_image_id: "36902"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36902 -->
Alle Kurven gehen um 90°. Eine Kette wird in der Spurmitte jeweils von Z40 in den Kurven geführt. Vor der Brücke gibt es jeweils 7,5° Steigung. Die dadurch notwendige Verwindung der Fahrbahn erledigen locker einige Statik-Flachträger.
