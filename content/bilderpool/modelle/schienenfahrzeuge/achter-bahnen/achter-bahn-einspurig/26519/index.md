---
layout: "image"
title: "Das Stellglied in der Rechtskurve"
date: "2010-02-23T21:27:17"
picture: "achterbahneinspurig6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26519
- /detailsdaa9.html
imported:
- "2019"
_4images_image_id: "26519"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26519 -->
Die Beweglichkeit der "Hand" lässt die Bahn in Rechtskurven nicht hängen oder entgleisen.
