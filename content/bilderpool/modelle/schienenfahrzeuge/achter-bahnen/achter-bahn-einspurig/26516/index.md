---
layout: "image"
title: "Der Wagen (Antriebsseite)"
date: "2010-02-23T21:27:17"
picture: "achterbahneinspurig3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26516
- /details63e4.html
imported:
- "2019"
_4images_image_id: "26516"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26516 -->
Der Wagen fährt auf nur zwei Gummireifen und wird quergeführt von insgesamt 6 Vorstuferädern - 4 vorne und 2 hinten beim Antrieb. Der MiniMot passt so genau ins Z15.  Außerdem hängt er tief genug, damit der Schwerpunkt des Wagens tief genug unterhalb der Schiene sitzt. Deshalb kippt der Wagen nicht herunter.
