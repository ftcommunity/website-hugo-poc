---
layout: "image"
title: "Gesamtansicht der Bahn"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40845
- /details8010-2.html
imported:
- "2019"
_4images_image_id: "40845"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40845 -->
Das Auto befährt die Wendeschleifen immer in abwechselnder Richtung. Dadurch wird die Stromleitung mal linksherum, mal rechtsherum verdrillt - und netto also überhaupt nicht.

Mittig sieht man eine Schikane mit einem Buckel, rechts davor eine, in der das Fahrzeug sehr schnell abwechselnd nach links und rechts lenken muss. Das funktioniert beides völlig problemlos (siehe Video).
