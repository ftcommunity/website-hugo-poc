---
layout: "comment"
hidden: true
title: "20542"
date: "2015-04-26T00:04:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das käme wohl auf einen Versuch an. Allerdings würden die Antriebsräder dann in den Kurven auf jeden fall schleifen, weil wir ja den Ackermann so gar nicht mehr einhalten würden. Wenn man genau hinschaut bzw. hinhört, stellt man auch so schon fest, dass die Kurvengeschwindigkeit etwas geringer ist als auf der Geraden - vermutlich, weil keine Differentiale im Fahrzeug stecken.
Gruß,
Stefan