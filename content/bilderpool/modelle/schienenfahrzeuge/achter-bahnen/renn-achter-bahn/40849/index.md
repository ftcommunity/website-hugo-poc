---
layout: "image"
title: "Aufbau der Bahn"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40849
- /details05ce-2.html
imported:
- "2019"
_4images_image_id: "40849"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40849 -->
Die Bahn besteht aus simplen Statikträgern mit 30 mm Abstand dazwischen. Auf ihnen sitzen die Reifen des Rennwagens, und zwischen ihnen das Lenkungs-Vorstuferad.
