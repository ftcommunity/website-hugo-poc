---
layout: "image"
title: "Fahrzeug"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/40846
- /details1f35.html
imported:
- "2019"
_4images_image_id: "40846"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40846 -->
Das Fahrzeug ist klein und leicht und hat einen recht direkten Antrieb auf die Hinterachse. Es soll ja schließlich *schnell* fahren.
