---
layout: "overview"
title: "Achter-Bahn nullgleisig"
date: 2020-02-22T08:35:27+01:00
legacy_id:
- /php/categories/1894
- /categoriesfeae.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1894 --> 
Hier fährt ein selbstlenkendes Fahrzeug relativ frei eine "8" und wir so von einem sich nicht verdrillenden Kabel von außen mit Strom versorgt.