---
layout: "comment"
hidden: true
title: "11075"
date: "2010-02-28T23:12:27"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
mit deiner interessanten Modellösung "nullgleisig" bin ich etwas gespalten. Wenn man von der Definition zur Radführung durch Gleise ausgeht, laufen hier auch zumindest abschnittsweise Räder mit paralleler Achse auf einem "Gleisrücken" :o) Die Fragen zum Spurkranz überlasse ich mal den Anderen ...
Gruss, Ingo