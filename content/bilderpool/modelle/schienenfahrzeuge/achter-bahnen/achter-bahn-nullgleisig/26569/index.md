---
layout: "image"
title: "Blick von hinten links"
date: "2010-02-28T22:06:56"
picture: "achterbahnnullgleisig5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26569
- /details0612.html
imported:
- "2019"
_4images_image_id: "26569"
_4images_cat_id: "1894"
_4images_user_id: "104"
_4images_image_date: "2010-02-28T22:06:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26569 -->
Hier sieht man ganz gut die Anordnung der beiden Motoren. Jeder treibt nur "sein" Rad an.
