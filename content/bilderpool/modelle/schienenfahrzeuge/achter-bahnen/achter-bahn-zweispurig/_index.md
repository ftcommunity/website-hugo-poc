---
layout: "overview"
title: "Achter-Bahn zweispurig"
date: 2020-02-22T08:35:24+01:00
legacy_id:
- /php/categories/1885
- /categoriese5c8.html
- /categories4abb-2.html
- /categories3abc.html
- /categories56d4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1885 --> 
Diese zweigleisige Bahn fährt endlos eine \"8\", ohne dass Akkus oder Stromschienen benötigt würden.