---
layout: "image"
title: "Sicherung"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26497
- /details86d8.html
imported:
- "2019"
_4images_image_id: "26497"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26497 -->
Damit die gebogenen Flachträger nicht durch die Kraft des Wagens aus den Bausteinen über den Stützen geschoben werden können, sind die alle mit je einem BS5 gesichert.
