---
layout: "image"
title: "Der Wagen"
date: "2010-02-21T13:26:37"
picture: "achterbahnzweispurig03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26492
- /details2d6b.html
imported:
- "2019"
_4images_image_id: "26492"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26492 -->
Das ist die Power-Motor-Variante von http://www.ftcommunity.de/details.php?image_id=26454 mit einem Erdbebensicheren Getriebe (in den Kurven treten nämlich ganz schön große Kräfte auf) und Schutzblechen, damit das Kabel nicht unter die Räder kommt.
