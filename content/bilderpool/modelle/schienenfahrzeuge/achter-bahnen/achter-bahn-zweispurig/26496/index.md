---
layout: "image"
title: "Detail der Schwinge"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26496
- /details4654-2.html
imported:
- "2019"
_4images_image_id: "26496"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26496 -->
Hier sieht man, dass der BS30, der als unteres Gewicht zum Zurückschwingen dient, ganz leicht (nach außen nämlich) versetzt angebracht ist. Das lenkt den Schwerpunkt etwas aus, und die Schwinge hängt wie gewünscht ganz leicht geneigt in der Kreuzung.
