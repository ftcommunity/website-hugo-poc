---
layout: "image"
title: "Die Steuerzentrale"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/26499
- /detailsfeb9-2.html
imported:
- "2019"
_4images_image_id: "26499"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26499 -->
Ganz oben im Kabelturm kann die Bedienmannschaft die ganze Anlage steuern: Sie schaltet halt ein oder aus. ;-)
