---
layout: "image"
title: "GTW 4"
date: "2012-05-29T02:50:20"
picture: "bumpf4.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/35011
- /detailsd5d5.html
imported:
- "2019"
_4images_image_id: "35011"
_4images_cat_id: "2593"
_4images_user_id: "424"
_4images_image_date: "2012-05-29T02:50:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35011 -->
Auch der enge Kurvenradius bereitet dem GTW keine Mühe
