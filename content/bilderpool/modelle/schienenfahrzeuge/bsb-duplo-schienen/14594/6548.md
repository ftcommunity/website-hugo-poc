---
layout: "comment"
hidden: true
title: "6548"
date: "2008-06-01T16:10:57"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

wie auch schon in einem anderen Kommentar geschrieben, finde ich Deine Bahn ausgesprochen schmuck und hübsch!
Fast schon serientauglich!


Frage sind die Lokomotiven elektrisch angetrieben?
Wenn ja, könntest Du uns mal einen Einblick auf den Antrieb geben?

Gruß

Lurchi