---
layout: "image"
title: "Front"
date: "2009-01-02T21:08:44"
picture: "bumpf5.jpg"
weight: "5"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/16845
- /details4197.html
imported:
- "2019"
_4images_image_id: "16845"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16845 -->
Mit den Fototransistoren könnte ich, wenn ich das IR Contol Set mit dem Doppel Flip-Flop austausche, die Lok über ein Lichtsignal steuern.
