---
layout: "image"
title: "Antrieb"
date: "2009-01-02T21:08:44"
picture: "bumpf4.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/16844
- /details1887-3.html
imported:
- "2019"
_4images_image_id: "16844"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16844 -->
Zwei M-Motoren treiben die Lok an
