---
layout: "image"
title: "Innenleben"
date: "2009-01-02T21:08:44"
picture: "bumpf3.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/16843
- /details3ead.html
imported:
- "2019"
_4images_image_id: "16843"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16843 -->
Steuerung über das alte IR Control Set
