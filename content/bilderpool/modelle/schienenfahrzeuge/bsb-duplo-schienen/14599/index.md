---
layout: "image"
title: "Einsatz in Industrieanlage"
date: "2008-05-30T22:39:35"
picture: "bsb8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/14599
- /details8266.html
imported:
- "2019"
_4images_image_id: "14599"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14599 -->
