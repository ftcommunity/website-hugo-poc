---
layout: "image"
title: "Lokis Bruder 1"
date: "2008-06-02T22:44:33"
picture: "bauspielbahn1.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/14611
- /detailsea91.html
imported:
- "2019"
_4images_image_id: "14611"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-06-02T22:44:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14611 -->
Loki hat gestern einen kleinen Bruder bekommen.