---
layout: "image"
title: "Test Lok 'Dufte 1'"
date: "2007-01-19T08:31:22"
picture: "PICT1212.jpg"
weight: "5"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8509
- /details437c.html
imported:
- "2019"
_4images_image_id: "8509"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-19T08:31:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8509 -->
Die ersten Tests sind erfolgreich abgeschlossen. Kann die Lok anhalten und ihr wieder freie Fahrt geben. Hab ihr noch einen Spannungswandler verpasste,(er befindet sich in der blauen Box) weil sie sonst wie ein ICE unterwegs ist.

Gruss Bumpf
