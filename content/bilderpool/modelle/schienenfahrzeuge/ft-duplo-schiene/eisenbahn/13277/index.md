---
layout: "image"
title: "Achsantrieb"
date: "2008-01-05T06:40:38"
picture: "eisenbahn2_2.jpg"
weight: "7"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13277
- /details7c29.html
imported:
- "2019"
_4images_image_id: "13277"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-05T06:40:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13277 -->
Hier musste ich ein wenig nachhelfen, indem ich ein Rastkegelzahnrad auf 4mm aufgebohrt habe um es über die Kunststoffachse zu schieben.
