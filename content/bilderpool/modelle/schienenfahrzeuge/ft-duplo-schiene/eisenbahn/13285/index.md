---
layout: "image"
title: "Güterzug"
date: "2008-01-06T20:09:44"
picture: "eisenbahn2_3.jpg"
weight: "11"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13285
- /details938a.html
imported:
- "2019"
_4images_image_id: "13285"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13285 -->
Länger geht's nicht mehr. Nicht die die Lok ist das Problem sondern die Wagen die es in der Kurve nach innen zieht.
