---
layout: "image"
title: "Antriebsrad"
date: "2008-01-05T06:40:38"
picture: "eisenbahn4_2.jpg"
weight: "9"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13279
- /details9562.html
imported:
- "2019"
_4images_image_id: "13279"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-05T06:40:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13279 -->
Da das Rad 23 auf der Kunststoffachse keinen Halt hat, musste ich auch da ein bisschen nachhelfen. Loch durch Rad und Achse bohren, Büroklammer auf Länge abschneiden, hindurchstossen, Gummiring montieren und fertig ist das Ganze. Hält super.
