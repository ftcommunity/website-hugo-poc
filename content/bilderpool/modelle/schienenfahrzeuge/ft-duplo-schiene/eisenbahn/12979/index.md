---
layout: "image"
title: "Eisenbahn 3"
date: "2007-12-02T15:46:17"
picture: "eisenbahn3.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/12979
- /detailsaddd.html
imported:
- "2019"
_4images_image_id: "12979"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2007-12-02T15:46:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12979 -->
Langholzwagen und Tieflader
