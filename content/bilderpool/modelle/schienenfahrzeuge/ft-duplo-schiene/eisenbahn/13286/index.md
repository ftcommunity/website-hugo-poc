---
layout: "image"
title: "Güterzug"
date: "2008-01-06T20:09:44"
picture: "eisenbahn3_3.jpg"
weight: "12"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13286
- /detailsd510.html
imported:
- "2019"
_4images_image_id: "13286"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13286 -->
Musste den Tiefladewagen ein bisschen höher setzen damit er an der Weiche nicht anhängt
