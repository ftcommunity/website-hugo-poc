---
layout: "image"
title: "Güterzug"
date: "2008-01-06T20:09:44"
picture: "eisenbahn1_3.jpg"
weight: "10"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter Mario Graf"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/13284
- /details63ae-2.html
imported:
- "2019"
_4images_image_id: "13284"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13284 -->
Güterzug auf Probefahrt. Ausser dem Tiefladewagen hab ich an allen Wagen die Räder gewechselt. Hatten zuviel Reibungswiderstand. Lok läuft hervorragend ohne durchzudrehen.
