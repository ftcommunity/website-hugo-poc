---
layout: "image"
title: "Schaltgeleise"
date: "2007-01-17T22:54:38"
picture: "PICT1206.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8495
- /details7de3.html
imported:
- "2019"
_4images_image_id: "8495"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-17T22:54:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8495 -->
Mit dieser Schiene könnte man den Zug steuern.
