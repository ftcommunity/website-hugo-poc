---
layout: "image"
title: "Wagen"
date: "2007-01-28T08:55:14"
picture: "PICT1243.jpg"
weight: "11"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/8709
- /detailsc246.html
imported:
- "2019"
_4images_image_id: "8709"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8709 -->
