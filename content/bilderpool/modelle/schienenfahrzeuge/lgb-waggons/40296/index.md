---
layout: "image"
title: "Langholzwagen mit Ladung"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40296
- /detailsdea1.html
imported:
- "2019"
_4images_image_id: "40296"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40296 -->
Langholzwagen auf LGB-Schienen
