---
layout: "image"
title: "Langholzwagen im Einsatz"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40297
- /details122a.html
imported:
- "2019"
_4images_image_id: "40297"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40297 -->
Langholzwagen mit Bahnpersonal
