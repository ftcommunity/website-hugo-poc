---
layout: "image"
title: "Langholzwagen - Fahrgestell (von oben)"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40300
- /detailse165.html
imported:
- "2019"
_4images_image_id: "40300"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40300 -->
Fahrgestell des Langholzwagens
