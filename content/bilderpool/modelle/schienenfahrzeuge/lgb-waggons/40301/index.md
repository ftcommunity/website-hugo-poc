---
layout: "image"
title: "Langholzwagen - Fahrgestell (von unten)"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons06.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40301
- /details9e06-2.html
imported:
- "2019"
_4images_image_id: "40301"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40301 -->
Unterseite des Fahrgestells
