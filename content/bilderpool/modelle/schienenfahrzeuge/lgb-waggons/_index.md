---
layout: "overview"
title: "LGB-Waggons"
date: 2020-02-22T08:35:45+01:00
legacy_id:
- /php/categories/3022
- /categories2a73.html
- /categoriesbcd9.html
- /categories4959.html
- /categories1afa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3022 --> 
Die LGB-Schienen passen genau ins fischertechnik- und Playmobil-Maß - ideale Voraussetzungen für die Entwicklung von Modellen, die beide Welten verbinden. Hier einige LGB-Waggons aus unserer jüngsten Fertigung.