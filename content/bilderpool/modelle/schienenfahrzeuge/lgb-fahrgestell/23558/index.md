---
layout: "image"
title: "Fahrgestell II"
date: "2009-03-29T21:05:07"
picture: "lgb2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/23558
- /detailsb1e5.html
imported:
- "2019"
_4images_image_id: "23558"
_4images_cat_id: "1609"
_4images_user_id: "373"
_4images_image_date: "2009-03-29T21:05:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23558 -->
