---
layout: "image"
title: "Fahrgestell III"
date: "2009-03-29T21:05:07"
picture: "lgb3.jpg"
weight: "3"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/23559
- /details9933.html
imported:
- "2019"
_4images_image_id: "23559"
_4images_cat_id: "1609"
_4images_user_id: "373"
_4images_image_date: "2009-03-29T21:05:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23559 -->
Leider geht die Schnecknekonstruktion nicht hundertprozentig mittig. Das ist aber nicht tragisch, es funktioniert gut so.
