---
layout: "image"
title: "Kipper 003"
date: "2004-11-03T11:41:06"
picture: "Kipper_003.JPG"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/2774
- /detailsa843.html
imported:
- "2019"
_4images_image_id: "2774"
_4images_cat_id: "271"
_4images_user_id: "119"
_4images_image_date: "2004-11-03T11:41:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2774 -->
