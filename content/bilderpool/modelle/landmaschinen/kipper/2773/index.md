---
layout: "image"
title: "Kipper 002"
date: "2004-11-03T11:41:06"
picture: "Kipper_002.JPG"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/2773
- /detailsbff6.html
imported:
- "2019"
_4images_image_id: "2773"
_4images_cat_id: "271"
_4images_user_id: "119"
_4images_image_date: "2004-11-03T11:41:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2773 -->
