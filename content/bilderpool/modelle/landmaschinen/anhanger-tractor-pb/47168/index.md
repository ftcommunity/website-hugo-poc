---
layout: "image"
title: "anhaengertractorpb5.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb5.jpg"
weight: "5"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47168
- /details4565.html
imported:
- "2019"
_4images_image_id: "47168"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47168 -->
Unter den Boden hängt ein Minimotor fürs hochziehen der Heckklappe. Das geht einfach mit zwei Seilchen an eine Achse, festgesetzt mit eine Klemmbüchse.