---
layout: "image"
title: "anhaengertractorpb6.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb6.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47169
- /detailsd117.html
imported:
- "2019"
_4images_image_id: "47169"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47169 -->
