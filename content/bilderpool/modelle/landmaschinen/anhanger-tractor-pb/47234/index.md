---
layout: "image"
title: "Anh-f08"
date: "2018-02-01T15:19:19"
picture: "anhaenger07.jpg"
weight: "16"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47234
- /details248a-2.html
imported:
- "2019"
_4images_image_id: "47234"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47234 -->
