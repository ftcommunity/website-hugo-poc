---
layout: "image"
title: "anhaengertractorpb7.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb7.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47170
- /details18b0.html
imported:
- "2019"
_4images_image_id: "47170"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47170 -->
Noch mal ein Totalbild.