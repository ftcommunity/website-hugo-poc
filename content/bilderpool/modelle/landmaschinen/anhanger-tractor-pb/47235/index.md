---
layout: "image"
title: "Anh-f09-10"
date: "2018-02-01T15:19:19"
picture: "anhaenger08.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47235
- /details7144.html
imported:
- "2019"
_4images_image_id: "47235"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47235 -->
Aufbau vom Fahrgestell, teil 1