---
layout: "image"
title: "Anh-f01-02"
date: "2018-02-01T15:19:18"
picture: "anhaenger03.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47230
- /details6103.html
imported:
- "2019"
_4images_image_id: "47230"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47230 -->
