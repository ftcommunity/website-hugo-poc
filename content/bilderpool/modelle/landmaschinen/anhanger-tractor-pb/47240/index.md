---
layout: "image"
title: "Anh-f19-20"
date: "2018-02-01T15:19:24"
picture: "anhaenger13.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47240
- /details5a9c.html
imported:
- "2019"
_4images_image_id: "47240"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47240 -->
