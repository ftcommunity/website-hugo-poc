---
layout: "image"
title: "Anh-11-13"
date: "2018-02-01T15:19:19"
picture: "anhaenger09.jpg"
weight: "18"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47236
- /detailse820-2.html
imported:
- "2019"
_4images_image_id: "47236"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47236 -->
Teil 2