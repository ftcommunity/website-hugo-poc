---
layout: "comment"
hidden: true
title: "19508"
date: "2014-09-14T10:56:24"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Marten,

ursprünglich hatte ich geplant, dass die Track sich links und rechts unabhängig schwenken lassen, das ist auch beim Original so. In der Praxis stellt dies aber Probleme da: Sind die Tracks einzeln aufgehängt, wird das Modell instabil.
Nach mehrern "Geländetests" hat sich bei dem Modell herausgestellt, dass diese Funktion nicht unbedingt erforderlich ist, daher habe ich sie mit einer Strebe verbunden.

Gruß, David

PS: Sobald ich das Modell wieder zerlege, (was ja eigentlich schade ist), mache ich noch ein paar Fotos vom Innenleben, auf denen man dan den Antrieb besser erkennen kann.