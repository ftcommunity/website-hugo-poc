---
layout: "image"
title: "Drehgelenk 2"
date: "2014-09-12T11:45:42"
picture: "qtrac08.jpg"
weight: "8"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39342
- /detailsd7ec.html
imported:
- "2019"
_4images_image_id: "39342"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39342 -->
Hier wird der Hinterwagen angebaut. Die drei scharzen Stifte sorgen dafür, dass das schwarze Zahnrad unbeweglich mit dem Hinterwagen verbunden ist.
