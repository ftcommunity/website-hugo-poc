---
layout: "comment"
hidden: true
title: "19510"
date: "2014-09-14T11:06:46"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Marten,

Ja, das Herausziehen wird lediglich durch Klemmhülzen verhindert. Allerdings sehe ich gerade, dass in meinem Nachbau ein kleiner Fehler steckt. Im Original habe ich die Kunstruktion mittels zwei Klemmhülsen (einer großen und einer kleinen) zusammengehalten. Bisher hat das aber keinen Schwachpunkt dargestellt.

Gruß, David