---
layout: "image"
title: "Beleuchtung"
date: "2014-09-12T11:45:54"
picture: "qtrac14.jpg"
weight: "14"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39348
- /detailsb974-2.html
imported:
- "2019"
_4images_image_id: "39348"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:54"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39348 -->
Dank Beleuchtung ist ein Arbeitseinsatz auch bei Nacht möglich.
