---
layout: "image"
title: "Quadtrac"
date: "2014-09-12T11:45:42"
picture: "qtrac02.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39336
- /details649d.html
imported:
- "2019"
_4images_image_id: "39336"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39336 -->
Der Quadtrac kann per Fischertechnik Control Set gesteuert werden
