---
layout: "overview"
title: "Quadtrac"
date: 2020-02-22T08:26:26+01:00
legacy_id:
- /php/categories/2947
- /categoriesffa5.html
- /categories3215.html
- /categories6069.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2947 --> 
Dies ist mein Nachbau des leistungsfähigen Traktors Quadtrac Case IH 620, der in den USA gebaut wird. Besonders an diesem Traktor ist, dass er an Stelle von Rädern Ketten besitzt und über eine Knicklenkung gesteuert werden kann. Ich habe in mein Modell zahlreiche Funktionen wie "Allradantrieb", eine Knicklenkung und diverse Gelenke zum Geländeausgleich eingebaut...