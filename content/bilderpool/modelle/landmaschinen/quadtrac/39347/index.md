---
layout: "image"
title: "Fahrwerk (Ansicht ohne Ketten)"
date: "2014-09-12T11:45:54"
picture: "qtrac13.jpg"
weight: "13"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39347
- /detailsa675.html
imported:
- "2019"
_4images_image_id: "39347"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:54"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39347 -->
So sieht das Fahrzeug ohne Ketten aus
