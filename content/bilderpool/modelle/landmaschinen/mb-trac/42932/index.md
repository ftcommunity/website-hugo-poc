---
layout: "image"
title: "MB Trac - Gesamtansicht 3"
date: "2016-02-29T21:09:00"
picture: "mbtrac03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42932
- /details0b09.html
imported:
- "2019"
_4images_image_id: "42932"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42932 -->
Thomas004, den Stern habe ich von deinem Unimog geklaut.