---
layout: "image"
title: "MB Trac - Hinterradaufhängung 1"
date: "2016-02-29T21:09:00"
picture: "mbtrac13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42942
- /detailsc405-2.html
imported:
- "2019"
_4images_image_id: "42942"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42942 -->
Das Z10 "taucht" in die Statiksteine ein.