---
layout: "image"
title: "MB Trac - Antriebsstrang"
date: "2016-02-29T21:09:00"
picture: "mbtrac15.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42944
- /details9039.html
imported:
- "2019"
_4images_image_id: "42944"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42944 -->
Das rote Z20 ist schon mal für eine spätere Differentialsperre. Ist halt noch ein Prototyp.
Spezielle Lagerung der Kardanwelle, damit das Fahrzeug kurz wird:
- vor dem Mitteldifferential läuft die Rastachse in einer Schneckenmutter
- hiner dem Mitteldifferential läuft das Kardangelenk in einem Haken
- in der Vorder- und der Hinteradschwinge sind die Kardangelenke mit Federnocken kurzgekuppelt und in halben Gelenksteinen gelagert
Besonders bei dem Haken muss man rumprobieren, bis man einen möglichst leichtgängigen findet.