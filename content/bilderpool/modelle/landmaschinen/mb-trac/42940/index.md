---
layout: "image"
title: "MB Trac - Hinterrad 1"
date: "2016-02-29T21:09:00"
picture: "mbtrac11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42940
- /detailsda9e-3.html
imported:
- "2019"
_4images_image_id: "42940"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42940 -->
Das ist ein Hinterrad ohne Reifen. In den Vorderrädern ist ein zusammengebautes Z7 verbaut. Die Mühe habe ich mir hier erspart und ein fertiges Z10 genommen. Wegen der unterschiedlichen Übersetzung muss das Mitteldifferential bei Gradeausfahrt immer differentieren. Ist vielleicht nich elegant, hat aber auch einen positiven Effekt: hinten ist die Antriebsleistung größer. Das spüre ich auf weichem Untergrund, verstehe es aber nicht ganz. Das Mitteldifferential wirkt wahrscheinlich aufgrund der inneren Reibung ziemlich stark als Sperrdifferential, versucht also die langsame Seite (hinten) zu beschleunigen.