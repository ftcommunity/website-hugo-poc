---
layout: "image"
title: "MB Trac - Befestigung Kotflügel 2"
date: "2016-02-29T21:09:00"
picture: "mbtrac21.jpg"
weight: "21"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42950
- /details669a.html
imported:
- "2019"
_4images_image_id: "42950"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42950 -->
In dem Klemmdingsbums oben am Kotflügel steckt noch ein Seilklemmstift, der wiederum in dem BS7,5 steckt.