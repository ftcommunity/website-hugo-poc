---
layout: "image"
title: "MB Trac - Prototyp auf Rampe 6"
date: "2016-02-29T21:09:00"
picture: "mbtrac24.jpg"
weight: "24"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/42953
- /details62e4.html
imported:
- "2019"
_4images_image_id: "42953"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42953 -->
... aber bergab gings anders.