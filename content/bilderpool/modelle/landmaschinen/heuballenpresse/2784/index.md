---
layout: "image"
title: "Heuballenpresse 003"
date: "2004-11-03T12:23:14"
picture: "Heuballenpresse_003.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2784
- /details9f44.html
imported:
- "2019"
_4images_image_id: "2784"
_4images_cat_id: "273"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2784 -->
von Claus-W. Ludwig