---
layout: "image"
title: "Heuballenpresse 012"
date: "2004-11-03T12:23:14"
picture: "Heuballenpresse_012.JPG"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2793
- /detailsd57b.html
imported:
- "2019"
_4images_image_id: "2793"
_4images_cat_id: "273"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2793 -->
von Claus-W. Ludwig