---
layout: "image"
title: "Perspektive Rechts"
date: "2012-01-07T19:42:39"
picture: "k-100_0389.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/33848
- /details6b0b.html
imported:
- "2019"
_4images_image_id: "33848"
_4images_cat_id: "2502"
_4images_user_id: "119"
_4images_image_date: "2012-01-07T19:42:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33848 -->
Im Stand mit Sound (Dieselgenerator von Conrad), Kolbenbewegung, Antrieb von Riemenscheibe und Kühlerventilator.
