---
layout: "image"
title: "Seitenansicht mit Details zu den Rädern"
date: "2012-01-07T19:42:39"
picture: "k-100_0375.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/33847
- /details2a6b.html
imported:
- "2019"
_4images_image_id: "33847"
_4images_cat_id: "2502"
_4images_user_id: "119"
_4images_image_date: "2012-01-07T19:42:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33847 -->
Vorderräder Tamiya LKW-Reifen ca. 80mm mit Orginal Felge. Passt genau auf 45er ft Reifen. Hinten: Große Bruder Treckerreifen 120mm. Beiden in Rot lackiert.
