---
layout: "image"
title: "Bulldog.jpg"
date: "2012-01-07T20:15:41"
picture: "IMG_2970.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33859
- /detailsd4c3-2.html
imported:
- "2019"
_4images_image_id: "33859"
_4images_cat_id: "2502"
_4images_user_id: "4"
_4images_image_date: "2012-01-07T20:15:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33859 -->
Nur mal so als Vergleich. Da sieht man, WIE genau der Claus den Bulldog getroffen hat. Fantastico!

Gruß,
Harald
