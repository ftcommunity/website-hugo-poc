---
layout: "overview"
title: "Lanz-EilBulldog Poederoyen"
date: 2020-02-22T08:26:20+01:00
legacy_id:
- /php/categories/2624
- /categoriesdc41.html
- /categorieseab9.html
- /categoriesb1a4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2624 --> 
De Lanz Bulldog is een type tractor ontwikkeld door de Duitse fabrikant Lanz.
Ontworpen door de Duitse ingenieur Fritz Huber, had de tractor een liggende één-cilinder tweetakt gloeikopdieselmotor. Door gebruik van deze ééncilinder had die een karakteristiek geluid, een staccato "boem-boem" geluid.
