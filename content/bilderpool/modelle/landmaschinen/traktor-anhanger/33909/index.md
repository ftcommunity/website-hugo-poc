---
layout: "image"
title: "Traktor"
date: "2012-01-13T19:03:10"
picture: "traktormitanhaenger11.jpg"
weight: "11"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33909
- /details38ce-2.html
imported:
- "2019"
_4images_image_id: "33909"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33909 -->
Ansicht von schräg vorne.