---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:02:59"
picture: "traktormitanhaenger06.jpg"
weight: "6"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33904
- /details7167.html
imported:
- "2019"
_4images_image_id: "33904"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33904 -->
Anhängerladefläche gekippt.