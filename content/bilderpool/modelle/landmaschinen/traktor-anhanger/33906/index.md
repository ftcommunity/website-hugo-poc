---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:02:59"
picture: "traktormitanhaenger08.jpg"
weight: "8"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33906
- /details2acf.html
imported:
- "2019"
_4images_image_id: "33906"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:02:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33906 -->
Traktor von unten.