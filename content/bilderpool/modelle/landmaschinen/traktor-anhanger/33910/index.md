---
layout: "image"
title: "Traktor mit Anhänger"
date: "2012-01-13T19:03:10"
picture: "traktormitanhaenger12.jpg"
weight: "12"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33910
- /details39bd-2.html
imported:
- "2019"
_4images_image_id: "33910"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33910 -->
Ansicht von schräg oben.