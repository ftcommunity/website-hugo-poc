---
layout: "image"
title: "Anhänger"
date: "2012-01-13T19:03:10"
picture: "traktormitanhaenger17.jpg"
weight: "17"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- /php/details/33915
- /details97f1-2.html
imported:
- "2019"
_4images_image_id: "33915"
_4images_cat_id: "2508"
_4images_user_id: "1361"
_4images_image_date: "2012-01-13T19:03:10"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33915 -->
Detailansicht Kippmechanik