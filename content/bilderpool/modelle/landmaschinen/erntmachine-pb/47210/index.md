---
layout: "image"
title: "Ernter-fase09"
date: "2018-01-30T16:23:29"
picture: "ernter09.jpg"
weight: "16"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47210
- /details25dc.html
imported:
- "2019"
_4images_image_id: "47210"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47210 -->
Den Cilinder enthält ein Stückchen Blei als Gegengewicht.