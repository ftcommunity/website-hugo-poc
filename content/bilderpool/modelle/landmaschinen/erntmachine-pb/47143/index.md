---
layout: "image"
title: "erntmachine3.jpg"
date: "2018-01-17T18:01:36"
picture: "erntmachine3.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47143
- /detailsae57.html
imported:
- "2019"
_4images_image_id: "47143"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47143 -->
Hier kann mann das drehbare kleine unterstutzungs-Räd an Vorderseite Rechts erkennen. Links gibt's auch eines, fast gerade unten den Gegengewicht-cilinder. Mann sieht noch ein kleines Stückchen davon.