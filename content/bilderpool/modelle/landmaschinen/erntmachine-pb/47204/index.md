---
layout: "image"
title: "Ernter-gesamt03"
date: "2018-01-30T16:23:29"
picture: "ernter03.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47204
- /details4125.html
imported:
- "2019"
_4images_image_id: "47204"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47204 -->
