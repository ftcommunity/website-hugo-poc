---
layout: "image"
title: "Ernter-unten"
date: "2018-01-30T16:23:29"
picture: "ernter04.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47205
- /detailsb55a.html
imported:
- "2019"
_4images_image_id: "47205"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47205 -->
