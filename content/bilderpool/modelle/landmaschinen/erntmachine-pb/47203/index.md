---
layout: "image"
title: "Ernter-gesamt02"
date: "2018-01-30T16:23:29"
picture: "ernter02.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47203
- /details1f54.html
imported:
- "2019"
_4images_image_id: "47203"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47203 -->
