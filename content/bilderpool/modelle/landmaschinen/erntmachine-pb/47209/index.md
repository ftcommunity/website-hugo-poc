---
layout: "image"
title: "Ernter-fase07-08"
date: "2018-01-30T16:23:29"
picture: "ernter08.jpg"
weight: "15"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47209
- /detailsd496.html
imported:
- "2019"
_4images_image_id: "47209"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47209 -->
