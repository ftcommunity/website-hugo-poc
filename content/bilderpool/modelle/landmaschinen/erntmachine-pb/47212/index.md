---
layout: "image"
title: "Ernter-fase13"
date: "2018-01-30T16:23:37"
picture: "ernter11.jpg"
weight: "18"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47212
- /detailsb80a.html
imported:
- "2019"
_4images_image_id: "47212"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47212 -->
