---
layout: "image"
title: "erntmachine2.jpg"
date: "2018-01-17T18:01:36"
picture: "erntmachine2.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47142
- /details42c8.html
imported:
- "2019"
_4images_image_id: "47142"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47142 -->
