---
layout: "comment"
hidden: true
title: "17686"
date: "2013-01-03T17:07:28"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ein sehr imposantes Modell!

Nur, aus den trockenen Worten und den Bildern werde ich nicht recht schlau.

Ich fände es sehr hilfreich, wenn das Getriebe mal in einer Gesamtaufnahme zu sehen wäre. Ein paar beschreibende Worte kämen auch ganz recht. Ist der Korb vom linken Differenzial irgendwo angetrieben oder festgehalten? 
Wenn das das Längsdifferenzial zum Ausgleich der Drehzahlen zwischen vorn und hinten ist, wo ist dann die Automatik?
Die "freischwebend" verbauten Kegelräder sehen mir nicht danach aus, als ob damit das Fahrzeug auch auf dem Boden angetrieben werden kann. Stimmt das?

Gruß,
Harald