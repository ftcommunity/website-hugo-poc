---
layout: "comment"
hidden: true
title: "17687"
date: "2013-01-03T18:45:21"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Harald + Jack,

Das Prinzip mit Lego gibt es unter :

http://www.ftcommunity.de/details.php?image_id=35387

This Lego CVT enables to have : 
. A constant speed of the motor 
. A infinity of variable ratio 
. A variable speed between 200rm (1:1) and 40 rpm (1:5) 
. A variable torque between 50N.cm (1:1 = motor torque) and arround 250N.cm (1:5) 
Of course, some torque are lost because of the friction pin but the torque is increased by 5 so the loss is negligible regarding the increase. 
Es gibt mit 2 Differentiale noch mehr möglichkeiten: 

http://www.nico71.fr/continuously-variable-transmission/ 

It enables a variable ratio on the output from 1:1 to 1:5. The CVT chooses the most suitable ratio functions of the resistive torque on the output. The principle is based on two differentials which are connected side by side with a first gear : 1:1 and a second gear on the other side : 1:5. The side with the 1:5 gear has also a friction pin in order to limit the rotation.

Interessante Video gibt es unter: 

http://www.youtube.com/watch?v=uF5jSjTmugw 

und 

http://www.nico71.fr/continuously-variable-transmission/ 

Sehr interessant für Nachbua ist:
http://www.youtube.com/watch?v=54biG55um5o

Mein FT-Nachbau gibt es unter:
http://www.ftcommunity.de/details.php?image_id=35390

Grüss, 

Peter 
Poederoyen NL