---
layout: "image"
title: "seilkranmitfahrzeug09.jpg"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug09.jpg"
weight: "9"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/9403
- /detailse988.html
imported:
- "2019"
_4images_image_id: "9403"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9403 -->
