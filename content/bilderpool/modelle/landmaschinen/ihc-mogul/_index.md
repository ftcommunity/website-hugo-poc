---
layout: "overview"
title: "IHC Mogul"
date: 2024-04-09T18:42:37+02:00
---

## Der Anfang vom jetzigen Ende, Teil 2

Der Verbrennermotor wurde von Anfang an nicht nur zur Fortbewegung und zum Transport eingesetzt, sondern hielt in der ganzen Technik Einzug. Daher zeige ich jetzt nach den [Benzinkutschen und dem LKW](https://ftcommunity.de/bilderpool/modelle/radfahrzeuge/oldtimer/gallery-index/) Modelle aus der Landwirtschaft. Sie zeigen, wie die Industrie diesem Markt die entsprechenden Entwicklungen zu Nutze machte.

Das erste Modell ist der erste Traktor, den IHC (International Harvester Company) in Serie gebaut und angeboten hat: ein IHC Mogul, Baujahr 1911, im Maßstab ca. 1:16. Leistung: 21 PS. 

Der Traktor war sehr einfach aufgebaut. Es gab nur zwei Motorvarianten: einen Verdampfer und einen Motor mit einem geschlossenen Kühlkreislauf. Das Modell zeigt den Verdampfer, also ein ganz einfaches Prinzip. Er hatte nur drei Räder. Ein Rad wurde über Kette angetrieben, also kein Differential. 
Gestartet wurde er mit Benzin und dann auf Petroleum umgestellt. Man hat dafür zwei Tanks eingebaut. Schweröl-Verbrenner müssen zum Starten nämlich erst vorglühen. Daher ist man bei Schweröl und Petroleum so vorgegangen: man hat Benzin beigemischt (fast das Doppelte) und den Motor mit dem Benzin-Gemisch angeworfen. Wenn er einen Moment gelaufen ist, war er so warm, dass man ganz auf Petroleum umschalten konnte.
