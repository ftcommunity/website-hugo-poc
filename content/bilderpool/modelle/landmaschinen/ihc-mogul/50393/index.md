---
layout: "image"
title: "IHC Mogul, Bj. 1911"
date: 2024-04-09T18:42:43+02:00
picture: "ihc-004.jpeg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Golden Line"]
uploadBy: "Website-Team"
license: "unknown"
---

Ich habe die Modelle jetzt auf einem mit Goldstreifen versehenen Holzbrett befestigt und und somit zu einer Serie gemacht mit der Bezeichnung Golden Line, weil ich noch mehr Modelle in der Form bauen werde.
