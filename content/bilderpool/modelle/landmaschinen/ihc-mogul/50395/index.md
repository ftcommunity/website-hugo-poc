---
layout: "image"
title: "Ansicht von oben"
date: 2024-04-09T18:42:46+02:00
picture: "ihc-002.jpeg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

