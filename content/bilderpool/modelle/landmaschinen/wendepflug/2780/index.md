---
layout: "image"
title: "Wendepflug 005"
date: "2004-11-03T11:53:15"
picture: "Wendepflug_005.JPG"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/2780
- /detailsf957.html
imported:
- "2019"
_4images_image_id: "2780"
_4images_cat_id: "272"
_4images_user_id: "119"
_4images_image_date: "2004-11-03T11:53:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2780 -->
