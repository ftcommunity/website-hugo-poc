---
layout: "image"
title: "Wendepflug 001"
date: "2004-11-03T11:53:15"
picture: "Wendepflug_001.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/2776
- /detailsff87.html
imported:
- "2019"
_4images_image_id: "2776"
_4images_cat_id: "272"
_4images_user_id: "119"
_4images_image_date: "2004-11-03T11:53:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2776 -->
