---
layout: "image"
title: "Forwader 01"
date: "2006-05-07T15:16:33"
picture: "Forwader_01.jpg"
weight: "1"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/6223
- /details72e9.html
imported:
- "2019"
_4images_image_id: "6223"
_4images_cat_id: "537"
_4images_user_id: "10"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6223 -->
Totalansicht des Prototyps von vorn
benutzt habe ich Räder von der Firma "Bruder"
