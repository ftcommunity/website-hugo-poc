---
layout: "image"
title: "Forwader 04"
date: "2006-05-07T15:16:33"
picture: "Forwader_04.jpg"
weight: "4"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/6226
- /details2ce4.html
imported:
- "2019"
_4images_image_id: "6226"
_4images_cat_id: "537"
_4images_user_id: "10"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6226 -->
Blick auf die "Steuerelektrik". Über dieses Modul muss noch der drehbare Fahrersitz montiert. Die Original-Forwader können die Fahrersitze, incl. der Bedienungseinrichtungen um 180° drehen.
