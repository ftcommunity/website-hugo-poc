---
layout: "image"
title: "Ein Anhänger"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern04.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42549
- /detailsf764.html
imported:
- "2019"
_4images_image_id: "42549"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42549 -->
Rolle hießen früher diese Zweiachsanhänger mit Deichsel.