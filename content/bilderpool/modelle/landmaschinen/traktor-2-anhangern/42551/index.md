---
layout: "image"
title: "Anhänger von unten"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern06.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42551
- /details7980.html
imported:
- "2019"
_4images_image_id: "42551"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42551 -->
Die Vorderachse entspricht noch weitegehend der Anleitung ab S. 39, aber hinten ist es dann doch anders. Da ist eine Anhängerkupplung mit Stoßstange dran.