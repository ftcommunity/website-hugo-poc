---
layout: "image"
title: "Ruebenroder 23"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_23.JPG"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2915
- /details2647.html
imported:
- "2019"
_4images_image_id: "2915"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2915 -->
von Claus-W. Ludwig