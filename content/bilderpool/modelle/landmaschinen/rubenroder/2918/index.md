---
layout: "image"
title: "Ruebenroder 26"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_26.JPG"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2918
- /details36a5.html
imported:
- "2019"
_4images_image_id: "2918"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2918 -->
von Claus-W. Ludwig