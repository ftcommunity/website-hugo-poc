---
layout: "image"
title: "Ruebenroder 19"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_19.JPG"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2911
- /details236f-2.html
imported:
- "2019"
_4images_image_id: "2911"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2911 -->
von Claus-W. Ludwig