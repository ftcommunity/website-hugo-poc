---
layout: "image"
title: "Ruebenroder 24"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_24.JPG"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2916
- /detailsbaf7.html
imported:
- "2019"
_4images_image_id: "2916"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2916 -->
von Claus-W. Ludwig