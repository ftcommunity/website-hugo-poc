---
layout: "image"
title: "Ruebenroder 15"
date: "2004-11-05T14:38:17"
picture: "Ruebenroder_15.JPG"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/2907
- /detailsabd1.html
imported:
- "2019"
_4images_image_id: "2907"
_4images_cat_id: "276"
_4images_user_id: "5"
_4images_image_date: "2004-11-05T14:38:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2907 -->
von Claus-W. Ludwig