---
layout: "image"
title: "Mähdrescher02"
date: "2014-11-23T09:32:19"
picture: "maehdrescher2.jpg"
weight: "2"
konstrukteure: 
- "lukas"
fotografen:
- "lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39840
- /detailse32a.html
imported:
- "2019"
_4images_image_id: "39840"
_4images_cat_id: "2989"
_4images_user_id: "1631"
_4images_image_date: "2014-11-23T09:32:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39840 -->
Hier kann man nun gut das Arbeitslicht (gelber Blinker) und die Arbeitsscheinwerfer sehn welche per Taster eingeschalten werden können.