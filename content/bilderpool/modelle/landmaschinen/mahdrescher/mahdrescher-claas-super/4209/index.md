---
layout: "image"
title: "claas super 028"
date: "2005-05-26T21:11:25"
picture: "claas_super_028.JPG"
weight: "28"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/4209
- /detailseeb3.html
imported:
- "2019"
_4images_image_id: "4209"
_4images_cat_id: "356"
_4images_user_id: "119"
_4images_image_date: "2005-05-26T21:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4209 -->
