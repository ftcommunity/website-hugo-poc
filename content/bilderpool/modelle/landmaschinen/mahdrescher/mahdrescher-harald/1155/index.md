---
layout: "image"
title: "MMM30.jpg"
date: "2003-05-31T20:09:26"
picture: "MMM30.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Mähdrescher", "Großreifen", "Monsterreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1155
- /details0e1f.html
imported:
- "2019"
_4images_image_id: "1155"
_4images_cat_id: "113"
_4images_user_id: "4"
_4images_image_date: "2003-05-31T20:09:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1155 -->
So wird er wohl aussehen, wenn er fertig ist.
