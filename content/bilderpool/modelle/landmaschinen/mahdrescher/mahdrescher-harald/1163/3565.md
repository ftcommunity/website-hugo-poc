---
layout: "comment"
hidden: true
title: "3565"
date: "2007-06-26T07:40:18"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das ist eine Möglichkeit, die ft-Taster als Schalter zu verwenden: hier werden zum Schalten die kompletten Gehäuse hin- und hergeschoben, die Betätigung geschieht durch die Verkleidungsplatte. Das ganze wurde im Mähdrescher versteckt, nur die Clipachsen mit den Federnocken schauen beiderseits (2 links, 2 rechts) vom Fahrer heraus.