---
layout: "image"
title: "MMM11"
date: "2003-05-05T21:01:24"
picture: "MMM11.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Monsterreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1115
- /detailse4f3.html
imported:
- "2019"
_4images_image_id: "1115"
_4images_cat_id: "113"
_4images_user_id: "4"
_4images_image_date: "2003-05-05T21:01:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1115 -->
Die Dreschtrommel ist rundherum mit ft-Zahnstangen vom Hubgetriebe bestückt. Das Förderband vom Schneidwerk ist auch gleichzeitig dessen Antrieb. Die Montage ist eine ganz elende Fummelei.
