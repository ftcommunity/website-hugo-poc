---
layout: "image"
title: "FT-Mahdrescher"
date: "2003-07-25T11:07:07"
picture: "FT-MD23.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/1254
- /details546d.html
imported:
- "2019"
_4images_image_id: "1254"
_4images_cat_id: "192"
_4images_user_id: "22"
_4images_image_date: "2003-07-25T11:07:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1254 -->
Fischertechnik Mahdrescher 

Einen ”New Holland” , ein “Case-International”, oder eine “Massey Ferguson -Mahdrescher” zu bauen war nicht unbedingt meine Absicht.  Es ist schliesslich eine “Fischertechnik Mahdrescher“.
Für den Straßentransport habe ich ein Wagen hinzu, auf dem das Schneidwerk hinterhergezogen wird.

Der Mähdrescher ist sozusagen selbsttragend. Es gibt ein Gerüst aus U-Trägern, Winkelträgern und viele Alu-Profilen, an dem alle beweglichen Teile befestigt sind. 

Meine Fischertechnik Mahdrescher hat insgesamt 5 Motoren:

1. Fahr-Antrieb (Conrad Powermot 110 min-1 und grosse Conrad-Reifen)
2. Lenkung (S-Motor)
3. Heben Schneidwerk (S-Motor)
4. Korntankschnecke-entlerung (Minimot)
5. Dreschwerk (Conrad PowerMot 110 min-1): eine kombinierter Antrieb fur 10 Funkionen

Das Dreschwerk enthalt die folgende 10 Sachen: schneiden(1), hasplen(2), zusammen fugen mit eine Einzugsschnecke(3), eine Schrägförderer(4), eine Dreschtrommel(5), eine Abnehm/Wendetrommel(6), eine lange Strohschuttelrutsche(7), eine Siebenschuttelrutsche(8), Reinigung mit Ventilator(9), und Korn zusammeln mit eine Elevator(10). Mit eine Leuchtstein kann man drinnen Alles anschauen. 
Der Schrägförderer dient gleichzeitig als Kettenantrieb für das Schneidwerk, Haspel und Einzugsschnecke. Meine Mahdrescher hat keine Strohhäcksler hinten am Mähdrescher.

Fahrantrieb, Lenkung und die Korntankschnecke-entlerung werden über das IR-Control gesteuert. Damit kann man fahren und entlehren, ohne daß Schalter am Mähdrescher gebraucht werden. Das heben des Schneidwerk ist semi-IR-Control gesteuert; nur fur die Drehrichtung braucht man eine Schalter hinter dem Fahrersitz.  Auch fur das Dreschwerk und die Beleuchtung habe ich Schalter.
Ich brauche 8 st NiMH-1.2V-Akkus für die Stromzufuhr. Das reicht nur gerade genug.

Die Lenkung hat eine S-Motor und Klemmschnecke-antrieb. Die hat eine langsame Übersetzung und braucht nicht soviel Höhe zum Einbau. 
 
Mit ein M-4-Gewinde (316L) wird das Schneidwerk langsam gehoben und gesenkt. Das Schneidwerk ist ziemlich schwer, vorallem wegen die viele 316L-Metallachse fur die Haspel.
Die Haspel mit Pneumatik Zylinder kann nur mit Handbetrieb gehoben werden. Ich habe keine speziale Kompressor dazu eingebaut.

Die Mahdrescher hat eine Reinigungs-Ventilator unter dem Dreschtrommel, Abnehmtrommel und Siebboden. Die Reinigungs-Ventilator habe ich gebaut mit 2 Kreuzknotenplatten (31665) und 4 st  
I-Strebe im jedem Loch der Kreuzknotenplatte. 

Fur dem Elevator habe ich kleine Rastraupenbelag (37210) und ein Alu-profil-20x40mm vom Baumarkt genutzt. Das Alu-profil-20x40mm ist innen 16x36 mm. Das reicht genau. Das selbe billige (nicht FT-) Alu-Profil, habe ich auch fur meine Autokran genutzt. Mit eine Zahnstangen-getriebe (37272) ist Alles sehr kompakt und Stark.

Die Korntank-Entleerungsschnecke kann nur mit Handbetreib ausgeschwenkt und eingeklappt werden. Die Entleerungsschnecke darf nur dann laufen, wenn die Schnecke ausgeschwenkt ist, sonst gibt es Bruch im Getriebe.

Die Mahdrescher von Harald Steinhaus war eine sehr gute Leidfaden fur mich gewesen.