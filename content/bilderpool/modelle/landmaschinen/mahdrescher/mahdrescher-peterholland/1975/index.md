---
layout: "image"
title: "Maehdr21.JPG"
date: "2003-11-11T19:15:06"
picture: "Maehdr21.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1975
- /detailsb676.html
imported:
- "2019"
_4images_image_id: "1975"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1975 -->
Blick von oben auf den oberen Schüttler und die Wendetrommel. Fahrtrichtung ist nach links.