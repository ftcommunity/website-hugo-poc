---
layout: "image"
title: "Maehdr18.JPG"
date: "2003-11-11T19:15:06"
picture: "Maehdr18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1976
- /detailsaf79-2.html
imported:
- "2019"
_4images_image_id: "1976"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1976 -->
Nochmal von unten reingeschaut. Die gelben Streben stecken in den Löchern von Kreuzknotenplatten, das ergibt den "Wind", das Gebläse zum Trennen von Spreu und Körnern.