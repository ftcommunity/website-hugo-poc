---
layout: "image"
title: "Maehdr09.JPG"
date: "2003-11-11T20:15:24"
picture: "Maehdr07.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1977
- /detailsa0eb-2.html
imported:
- "2019"
_4images_image_id: "1977"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1977 -->
Blick von oben in die rechte Seite (Fahrtrichtung ist nach rechts). Vorne liegen die Akkus, darunter der Power-Mot, der Dresch- und Schneidwerk antreibt. In Bildmitte (im Dunkeln) liegt die Wendetrommel, dahinter der obere Schüttler.