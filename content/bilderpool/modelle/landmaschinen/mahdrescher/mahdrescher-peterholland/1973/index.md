---
layout: "image"
title: "Maehdr09.JPG"
date: "2003-11-11T19:15:06"
picture: "Maehdr09.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1973
- /details4774-3.html
imported:
- "2019"
_4images_image_id: "1973"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1973 -->
Die linke Seite. Die Dreschtrommel sitzt hinter dem rechten der drei Z10, die das Schneidwerk über den Schrägförderer antreiben.