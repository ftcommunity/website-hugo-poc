---
layout: "overview"
title: "Lanz HL 12"
date: 2024-04-15T19:31:20+02:00
---

## Der Anfang vom jetzigen Ende, Teil 3

Ein weiteres Modell zur Geschichte der Verbrennermotoren: ein Lanz HL 12 im Maßstab ca. 1:12. Baujahr 1922, Leistung 12 PS.
