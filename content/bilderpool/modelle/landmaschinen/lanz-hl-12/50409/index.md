---
layout: "image"
title: "Lanz HL 12"
date: 2024-04-15T19:31:33+02:00
picture: "lanz-001.jpeg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
schlagworte: ["Golden Line"]
uploadBy: "Website-Team"
license: "unknown"
---

