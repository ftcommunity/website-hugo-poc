---
layout: "image"
title: "Aufstellung"
date: 2024-04-15T19:31:32+02:00
picture: "lanz-010.jpeg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Die Modelle sind bei Austellung mit einem Taster und Steuerblock verbunden. Wenn man den Taster betätigt, werden die Modelle für eine Minute in Bewegung gesetzt. Hier ein Bild mit der Aufstellung und dem Taster. Auf der Ausstellung in Dortmund DASA Maker Fair Ruhr 2024 waren die Modelle schon zu sehen.