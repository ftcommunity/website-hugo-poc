---
layout: "image"
title: "Transportverpackung"
date: 2024-04-15T19:31:31+02:00
picture: "ihc-012.jpeg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

 Wenn alles klappt, sind diese beiden Modelle mit der Bezinkutsche und dem LKW am 20 April 2024 an der Nordconvention ldeA in Mellendorf zu sehen. Hier sind sie schon zum Transport verpackt.