---
layout: "image"
title: "tim carlo maishexler"
date: "2010-11-07T21:56:31"
picture: "maishexler__48.jpg"
weight: "10"
konstrukteure: 
- "tim carlo"
fotografen:
- "tim carlo"
uploadBy: "carlo"
license: "unknown"
legacy_id:
- /php/details/29211
- /detailsba7d.html
imported:
- "2019"
_4images_image_id: "29211"
_4images_cat_id: "2119"
_4images_user_id: "893"
_4images_image_date: "2010-11-07T21:56:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29211 -->
Maishexler nach Vorbild von John Deere und Siku-Modell