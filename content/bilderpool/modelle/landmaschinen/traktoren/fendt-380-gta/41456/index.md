---
layout: "image"
title: "Fendt380GTA06.jpg"
date: "2015-07-19T20:41:59"
picture: "P1110124mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41456
- /detailscd1e.html
imported:
- "2019"
_4images_image_id: "41456"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:41:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41456 -->
Der vordere Teil von unten. Die Achse in den gelben Winkelträgern erlaubt das Verschränken der Vorderachse gegenüber dem Rest. In der Lenkung (rechtes Vorderrad, im Bild unten) wurde ein Winkelstein 30° auf Minimal-Abmessungen zusammengeschnitzt, damit er in den Lenkwürfel hineinpasst.
