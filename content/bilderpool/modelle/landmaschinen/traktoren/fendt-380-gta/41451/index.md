---
layout: "image"
title: "Fendt380GTA01.jpg"
date: "2015-07-19T20:28:43"
picture: "P1050111mit.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41451
- /details9daf.html
imported:
- "2019"
_4images_image_id: "41451"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:28:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41451 -->
Dieser Geräteträger ist aus 2003 und war (mit anderem Verdeck) in Mörshausen mit dabei: http://ftcommunity.de/categories.php?cat_id=166 .

Jetzt sind mir die Bilder wieder über den Weg gelaufen.
