---
layout: "image"
title: "Mini-Traktor 1"
date: "2007-05-22T21:04:55"
picture: "Traktor_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10484
- /detailsa0f9.html
imported:
- "2019"
_4images_image_id: "10484"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-22T21:04:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10484 -->
Hier ein kleiner und - meiner Meinung nach - besonders hübscher Traktor mit Antrieb, Lenkung, Pendelachse, eigener Stromversorgung und Kabelfernbedienung.