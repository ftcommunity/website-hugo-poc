---
layout: "image"
title: "Mini-Traktor 5"
date: "2007-05-23T13:17:38"
picture: "Traktor_5.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10488
- /details8007.html
imported:
- "2019"
_4images_image_id: "10488"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-23T13:17:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10488 -->
