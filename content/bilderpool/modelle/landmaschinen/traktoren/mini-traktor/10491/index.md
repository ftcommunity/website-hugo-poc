---
layout: "image"
title: "Mini-Traktor 8"
date: "2007-05-23T13:17:38"
picture: "Traktor_8.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10491
- /details978e-2.html
imported:
- "2019"
_4images_image_id: "10491"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-23T13:17:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10491 -->
Zum Schluss noch ein Detailfoto vom Antriebspackage.