---
layout: "image"
title: "[5/5] Vorderachse"
date: "2009-09-10T21:27:16"
picture: "traktorclaus5.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/24908
- /details3cb7-4.html
imported:
- "2019"
_4images_image_id: "24908"
_4images_cat_id: "1716"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24908 -->
