---
layout: "image"
title: "Schlueter Traktor 011"
date: "2004-12-05T10:28:42"
picture: "Schlueter_Traktor_011.JPG"
weight: "11"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3403
- /detailscb64.html
imported:
- "2019"
_4images_image_id: "3403"
_4images_cat_id: "295"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3403 -->
