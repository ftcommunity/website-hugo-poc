---
layout: "image"
title: "Schlueter Traktor 002"
date: "2004-12-05T10:28:42"
picture: "Schlueter_Traktor_002.JPG"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3394
- /detailsfeec.html
imported:
- "2019"
_4images_image_id: "3394"
_4images_cat_id: "295"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3394 -->
