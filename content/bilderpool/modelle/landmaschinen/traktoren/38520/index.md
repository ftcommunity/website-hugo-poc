---
layout: "image"
title: "Traktor mit pneumatischem Heuballengreifer 4"
date: "2014-03-29T23:32:29"
picture: "109_5268.jpg"
weight: "7"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
uploadBy: "Nordmann"
license: "unknown"
legacy_id:
- /php/details/38520
- /detailsf28d-2.html
imported:
- "2019"
_4images_image_id: "38520"
_4images_cat_id: "605"
_4images_user_id: "2159"
_4images_image_date: "2014-03-29T23:32:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38520 -->
