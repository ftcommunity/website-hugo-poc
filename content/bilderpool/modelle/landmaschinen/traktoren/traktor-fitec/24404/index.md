---
layout: "image"
title: "Achse"
date: "2009-06-16T17:17:10"
picture: "traktor8.jpg"
weight: "74"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24404
- /detailsf418.html
imported:
- "2019"
_4images_image_id: "24404"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24404 -->
Die Acshe für Anbaugeräte. Sie lässt sich ausfahren. (ausgefahren)
