---
layout: "image"
title: "Heck"
date: "2008-04-29T18:13:32"
picture: "Traktor61.jpg"
weight: "61"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/14420
- /detailsfb23-5.html
imported:
- "2019"
_4images_image_id: "14420"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-04-29T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14420 -->
Das Heck ist zu sehen. Was fehlt ist noch eine Anhängerkupplung.
