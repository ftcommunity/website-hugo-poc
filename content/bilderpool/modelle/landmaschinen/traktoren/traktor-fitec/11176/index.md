---
layout: "image"
title: "Traktor"
date: "2007-07-21T14:04:56"
picture: "Traktor31.jpg"
weight: "31"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11176
- /details969c-3.html
imported:
- "2019"
_4images_image_id: "11176"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:04:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11176 -->
Das ist mein neuer Traktor. Er ist noch nicht fertig, das ist nur so ein Zwischenstand. Er hat Allradantrieb, Achsschenkellenkung, genug Bodenfreiheit, Pendelachse, Hebehydraulik am Heck mit 2 Pneumatikzylindern. Geplant ist noch ein Kompressor, der sich per Zylinder, welcher gegen eine Feder drückt, bei genug Druck im Lufttank den Kompressor auskoppelt vom Fahrmotor. Ich habe schon mal einen Test gemacht wie es fährt. Es fährt sehr gut. Nichts rattert, springt usw. Auch die Pendelchse funktioniert prächtig. :-) Die Pendelachse ist diesmal ohne Kardangelenke gebaut, somit ist der Drehpunkt, direkt auf der Antriebsachse. der große Nachteil, den eine Pendelachse mit Kardangelenken hat, ist dass die Kardangelenke bei hoher belastung auseinander springen. Man kann das noch verbessern indem man einen kleinstmöglichen Winkel der Kardangelenke macht, aber im richtigen Gelände hilft das nichts. Jetzt passiert das nicht mehr. :-)
