---
layout: "image"
title: "Neu"
date: "2007-05-25T21:23:34"
picture: "Traktor12.jpg"
weight: "12"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10498
- /details9ce4.html
imported:
- "2019"
_4images_image_id: "10498"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10498 -->
Ich habe es etwas umgebaut. Die Kompressorabschaltung ist ganz unten neben dem Motor. Es ist alles sehr platzsparend. Nur der Empfänger muss noch eingebaut werden.
