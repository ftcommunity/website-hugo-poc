---
layout: "image"
title: "Pendelachse"
date: "2008-04-29T18:13:32"
picture: "Traktor54.jpg"
weight: "54"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/14413
- /details29f0-2.html
imported:
- "2019"
_4images_image_id: "14413"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-04-29T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14413 -->
Die Pendelchse kann weit genug pendeln.
