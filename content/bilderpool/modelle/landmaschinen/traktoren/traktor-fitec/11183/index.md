---
layout: "image"
title: "Frontantrieb"
date: "2007-07-21T14:04:57"
picture: "Traktor38.jpg"
weight: "38"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11183
- /details0671-4.html
imported:
- "2019"
_4images_image_id: "11183"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:04:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11183 -->
Das ist der Frontantrieb. er hat unten jetzt doch Gelenke, wegen der Stabilität, aber sie sind anders genbaut. So hat man immer noch sehr viel Bodenabstand.
