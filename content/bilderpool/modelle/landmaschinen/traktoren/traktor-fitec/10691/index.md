---
layout: "image"
title: "Mähwerk"
date: "2007-06-04T15:48:00"
picture: "Traktor21.jpg"
weight: "21"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10691
- /details38f5.html
imported:
- "2019"
_4images_image_id: "10691"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10691 -->
Mähwerk.
