---
layout: "image"
title: "Bodenabstand"
date: "2007-07-15T17:49:00"
picture: "Traktor26.jpg"
weight: "26"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11079
- /detailsb4c6.html
imported:
- "2019"
_4images_image_id: "11079"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11079 -->
Hier sieht man den extrem hohen Bodenabstand, der bei Geländefahrzeugen dringend gebraucht wird.
