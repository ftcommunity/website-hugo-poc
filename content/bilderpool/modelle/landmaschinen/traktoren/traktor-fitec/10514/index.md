---
layout: "image"
title: "Kompressorabschaltung"
date: "2007-05-27T18:23:23"
picture: "Traktor16.jpg"
weight: "16"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10514
- /details4979.html
imported:
- "2019"
_4images_image_id: "10514"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-27T18:23:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10514 -->
Die Kompressorabschaltung sitzt ganz unten neben dem Motor. Das spart Platz.
