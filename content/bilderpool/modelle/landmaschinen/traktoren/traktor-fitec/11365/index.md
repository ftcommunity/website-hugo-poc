---
layout: "image"
title: "Kompressor"
date: "2007-08-12T15:24:19"
picture: "Traktor49.jpg"
weight: "49"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11365
- /details0f30-2.html
imported:
- "2019"
_4images_image_id: "11365"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-08-12T15:24:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11365 -->
Der Kompressor ist oben an einem Gelenk aufgehängt. Der leine Zylinder, in dem eine Feder ist, koppelt bei zuviel Druck den Kompressor mit seinem Z20 vom Fahrmotor aus, sodass dieser entlastet ist.
