---
layout: "image"
title: "Heckantrieb"
date: "2007-07-21T14:04:57"
picture: "Traktor40.jpg"
weight: "40"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11185
- /details7cd3.html
imported:
- "2019"
_4images_image_id: "11185"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:04:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11185 -->
Mal ein Foto, des Heckantriebs. Er sieht etwas kompliziert aus, ist er auch. Er ist aber extrem stabil. Im Gegensatz zu meinem alten Traktor, kann ich mit aller Kraft hinten auf meinen Traktor drücken, ohne dass sich etwas durchbiegt.
