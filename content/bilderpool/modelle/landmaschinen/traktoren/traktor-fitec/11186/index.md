---
layout: "image"
title: "Lenkung"
date: "2007-07-21T14:05:01"
picture: "Traktor41.jpg"
weight: "41"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11186
- /detailsd4d3.html
imported:
- "2019"
_4images_image_id: "11186"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:05:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11186 -->
Hier sieht man die Lenkung. Man könnte denken, dass je nach lenkeinschlag der Minimotor die Pendelchse blockiert, aber ich habe es getestet und in jeder lenkposition kann die Pendelachse noch sehr weit pendeln.
