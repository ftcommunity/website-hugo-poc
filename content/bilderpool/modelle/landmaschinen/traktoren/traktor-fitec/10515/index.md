---
layout: "image"
title: "Kompressor"
date: "2007-05-27T18:23:23"
picture: "Traktor17.jpg"
weight: "17"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10515
- /detailsf61d.html
imported:
- "2019"
_4images_image_id: "10515"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-27T18:23:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10515 -->
Der Kompressor ist klappbar. So kann man auch mal etwas am Motor reparieren ohne alles wegbauen zu müssen.
