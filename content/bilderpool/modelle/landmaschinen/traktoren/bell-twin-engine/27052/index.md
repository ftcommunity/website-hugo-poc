---
layout: "image"
title: "achteraanzicht"
date: "2010-05-03T11:26:30"
picture: "P5020149.jpg"
weight: "7"
konstrukteure: 
- "ruurd"
fotografen:
- "chef8"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/27052
- /details8e43.html
imported:
- "2019"
_4images_image_id: "27052"
_4images_cat_id: "1948"
_4images_user_id: "838"
_4images_image_date: "2010-05-03T11:26:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27052 -->
Onderkant van de achterkant