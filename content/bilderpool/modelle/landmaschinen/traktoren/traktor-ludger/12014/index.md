---
layout: "image"
title: "Hinterrad Montageblock (Prototyp)"
date: "2007-09-26T15:07:38"
picture: "DSCN1509.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12014
- /detailsaa54-3.html
imported:
- "2019"
_4images_image_id: "12014"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12014 -->
Hier sieht man wie das mit dem Rastzahnrad (Bild vorher) gemeint ist. Die linke Schneckenmutter trägt zwei Kugellager für die Hinterachse. Läuft "superleicht".
