---
layout: "image"
title: "Ansicht 1"
date: "2007-07-13T12:03:15"
picture: "DSCN1429.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/11044
- /details173c-2.html
imported:
- "2019"
_4images_image_id: "11044"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-13T12:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11044 -->
Wenn alles klappt könnte er so aussehen
