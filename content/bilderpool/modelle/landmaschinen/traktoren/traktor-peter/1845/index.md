---
layout: "image"
title: "FT-MB-traktor + Anhanger mit A2-M4 Antrieb"
date: "2003-10-28T20:55:14"
picture: "FT-MB-tractorAnhanger0001.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/1845
- /details693e-2.html
imported:
- "2019"
_4images_image_id: "1845"
_4images_cat_id: "118"
_4images_user_id: "22"
_4images_image_date: "2003-10-28T20:55:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1845 -->
Genutzt habe ich eine LEMO-getreibemotor (GM 1637), 3-15V, Getreibe 30:1, 1665 Upm, Motot und Getreibe 16 mm, Lange 38mm