---
layout: "image"
title: "Traktor01.JPG"
date: "2003-11-11T20:15:25"
picture: "Traktor01.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1978
- /details8776.html
imported:
- "2019"
_4images_image_id: "1978"
_4images_cat_id: "118"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1978 -->
