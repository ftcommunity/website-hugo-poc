---
layout: "image"
title: "Fendt300-09"
date: "2004-11-01T10:20:22"
picture: "Fendt300-09.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2727
- /details0ae2.html
imported:
- "2019"
_4images_image_id: "2727"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2727 -->
Blick von unten unter das Kabinendach.

Zum Kompressor gibt es hier mehr Details:
http://www.ftcommunity.de/categories.php?cat_id=18