---
layout: "image"
title: "Fend300all01.JPG"
date: "2005-01-15T12:30:18"
picture: "Fend300all01.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Allrad", "Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3527
- /details4ecb-2.html
imported:
- "2019"
_4images_image_id: "3527"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T12:30:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3527 -->
Die Vorderachse ist mit dem Chassis über die Bodenplatte 90x30 verbunden. Die beiden Gelenksteine links unten halten die beiden Z15 auf richtigem Abstand, obwohl ihre Achsen schräg übereinander angeordnet liegen.

Das Längsdifferenzial ist unbedingt notwendig, weil die Antriebsstränge zu Vorder- und Hinterachse unterschiedliche Übersetzungen haben. Um das Z15 vor dem Hinterachsdifferenzial einbauen zu können, musste ein Winkelträger 15 etwas bearbeitet werden.