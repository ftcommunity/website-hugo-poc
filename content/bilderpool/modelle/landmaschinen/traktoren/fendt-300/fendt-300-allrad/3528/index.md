---
layout: "image"
title: "Fendt300all05.JPG"
date: "2005-01-15T12:30:18"
picture: "Fendt300all05.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Allrad", "Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3528
- /details315d.html
imported:
- "2019"
_4images_image_id: "3528"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T12:30:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3528 -->
So 'technisch' das ganze auch aussehen mag, *taugen* tut es nichts :-(

Die Vorderachse zieht einfach nichts. Wenn ein bißchen Kraft gebraucht wird, verzieht sich das ganze Teil und die Kegelrädchen knattern aufeinander herum.