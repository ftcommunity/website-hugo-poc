---
layout: "image"
title: "Allrad01.JPG"
date: "2005-01-15T12:30:17"
picture: "Allrad01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3525
- /details12f0-2.html
imported:
- "2019"
_4images_image_id: "3525"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T12:30:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3525 -->
Mit etwas Inspiration durch Heikos Roboter-Arm: 
Kardangelenke kann man ersetzen durch Kegelradsätze. Die funktionieren auch bis fast 90° Knickwinkel, bis dann die Zähne von Ein- und Ausgangswelle in Eingriff kommen. Die ft-Kardangelenke bauen 30 mm lang, mit Kegelzahnrädern wird es etwas kürzer.

Das hier ist ein Versuchsaufbau, der Aufbau krankt aber daran, dass man einfach nicht genügend Drehmoment rüberbringt.