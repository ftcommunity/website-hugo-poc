---
layout: "image"
title: "Monster02"
date: "2004-11-01T10:20:22"
picture: "Monster02.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Traktor", "Monsterreifen", "Großreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2745
- /detailsaafb-2.html
imported:
- "2019"
_4images_image_id: "2745"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2745 -->
Jetzt hab ich den Reifen halt doch aufgemacht :-)
