---
layout: "image"
title: "MB-Truck 1"
date: "2007-06-10T20:09:31"
picture: "mbtruck02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10795
- /details08eb.html
imported:
- "2019"
_4images_image_id: "10795"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10795 -->
