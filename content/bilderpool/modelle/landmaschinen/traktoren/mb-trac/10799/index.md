---
layout: "image"
title: "MB-Truck 5"
date: "2007-06-10T20:09:31"
picture: "mbtruck06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10799
- /details47f2.html
imported:
- "2019"
_4images_image_id: "10799"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10799 -->
