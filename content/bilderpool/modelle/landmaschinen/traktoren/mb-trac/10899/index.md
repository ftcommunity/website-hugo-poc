---
layout: "image"
title: "MB-Truck 14"
date: "2007-06-20T17:16:07"
picture: "mbtruck1.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10899
- /detailsf72b.html
imported:
- "2019"
_4images_image_id: "10899"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10899 -->
