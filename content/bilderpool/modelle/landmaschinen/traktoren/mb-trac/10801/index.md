---
layout: "image"
title: "MB-Truck 7"
date: "2007-06-10T20:09:31"
picture: "mbtruck08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10801
- /details49e5.html
imported:
- "2019"
_4images_image_id: "10801"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10801 -->
