---
layout: "image"
title: "MB-Truck 17"
date: "2007-06-20T17:16:08"
picture: "mbtruck4.jpg"
weight: "18"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10902
- /detailsb19f.html
imported:
- "2019"
_4images_image_id: "10902"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10902 -->
