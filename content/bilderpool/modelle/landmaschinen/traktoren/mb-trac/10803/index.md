---
layout: "image"
title: "MB-Truck 9"
date: "2007-06-10T20:09:31"
picture: "mbtruck10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/10803
- /details125f.html
imported:
- "2019"
_4images_image_id: "10803"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10803 -->
