---
layout: "image"
title: "Das ganze Gespann von Oben"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor10.jpg"
weight: "10"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43134
- /details4a5f-2.html
imported:
- "2019"
_4images_image_id: "43134"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43134 -->
Der Traktor ist sehr stabil und gleichzeitig einfach und ist auch mal zum spielen gut geeignet, was bei den jungen Fischertechnikern wohl noch dazugehört. Habe meine erste Fischertechnik im Alter von 9 Jahren bekommen. Das war 1972 und ich musste meine Modelle mit der Hand schieben und lenken. Da gab es noch keine Funkfernsteuerung und auch nur eine einfache Achsschenkellenkkung war da möglich. Und auch das hat schon viel Spaß gemacht, weil wir unsere Fahrzeuge selbst bauen konnten.