---
layout: "image"
title: "Traktor von unten"
date: "2016-03-27T19:15:48"
picture: "2222_klein.jpg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43215
- /details6d4a-3.html
imported:
- "2019"
_4images_image_id: "43215"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-27T19:15:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43215 -->
Keine Beschreibung