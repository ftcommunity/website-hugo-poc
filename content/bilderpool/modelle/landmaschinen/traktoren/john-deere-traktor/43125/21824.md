---
layout: "comment"
hidden: true
title: "21824"
date: "2016-03-14T17:15:54"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ein wirklich sehr schönes Modell, das mal zeigt, dass man auch mit Fischertechnik realistische Farbkombinationen erstellen kann (und auch die kontroversen grünen Statikteile sinnvoll verwenden kann)!

Außerdem natürlich sehr viel Liebe zum Detail, um nur mal das Gewicht vorne oder das kreative Geländer aus einem Kardangelenk.

Gruß, David

@ Masked: Ich glaube, die Motorhaube wird in den Junior Kästen als Kippmulde verwendet: http://www.fischertechnik.de/desktopdefault.aspx/tabid-17/35_read-136/usetemplate-2_column_pano/