---
layout: "image"
title: "Traktor von vorn mit Hänger"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor02.jpg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43126
- /detailsbadb-2.html
imported:
- "2019"
_4images_image_id: "43126"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43126 -->
Dieser Traktor ist mit den wenigen grünen Teilen, welche  es erst jetzt  im Ft.-Program gibt, gebaut. Mit dem Gelb und dem Grün passt er wohl eindeutig zur Welt bekannten Marke JOHN DEERE. Oder seid ihr anderer Meinung???