---
layout: "image"
title: "Traktor von vorn 3"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor05.jpg"
weight: "5"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43129
- /details75d9.html
imported:
- "2019"
_4images_image_id: "43129"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43129 -->
Hier noch mal ein Bild mit Lenkeischlag links.