---
layout: "image"
title: "Traktor von Hinten"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor07.jpg"
weight: "7"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43131
- /details29af-2.html
imported:
- "2019"
_4images_image_id: "43131"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43131 -->
Hier habe ich die aus dem Ft.-Program schon bekannte Hebetechnik der (Ackerschiene) Geräteträger mit Handbetrieb eingesetzt, Auch den etwas versteckten Accu kann man sehen. Beim Laden des Accus ziehe ich nur den Kotflügel nach oben ab. Der Accu muss zum laden nicht herausgenommen werden, man trägt ja auch nicht den Tank zur Tankstelle.