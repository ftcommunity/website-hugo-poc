---
layout: "image"
title: "Traktor von vorn 2"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor04.jpg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43128
- /details8e21.html
imported:
- "2019"
_4images_image_id: "43128"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43128 -->
Der Antrieb ist mit einem Powermotor 9V 20:1 ca. 280 u/min ausgestattet und läuft über ein Rast-Differential 75218 auf die Hinterräder.