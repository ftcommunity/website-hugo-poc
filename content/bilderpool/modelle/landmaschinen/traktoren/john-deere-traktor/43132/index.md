---
layout: "image"
title: "Traktor von Oben mit abgebauter Motorhaube"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor08.jpg"
weight: "8"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43132
- /details770e.html
imported:
- "2019"
_4images_image_id: "43132"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43132 -->
Hier nun der Blick auf die Pendelachse mit Integriertem Microservo, und einem Gelenkwürfel mit Hülse 31436 dem ich mit etwas Papier zu einem festeren sitz verholfen habe.