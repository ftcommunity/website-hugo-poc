---
layout: "image"
title: "knikpunt"
date: "2010-03-07T10:12:48"
picture: "P3060376.jpg"
weight: "13"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26635
- /details2ca6-3.html
imported:
- "2019"
_4images_image_id: "26635"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26635 -->
foto onderzijde knikpunt