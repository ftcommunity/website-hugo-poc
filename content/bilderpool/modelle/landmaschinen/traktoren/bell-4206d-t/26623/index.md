---
layout: "image"
title: "Bell 4206D-T"
date: "2010-03-07T10:12:46"
picture: "P3060362.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26623
- /details8530.html
imported:
- "2019"
_4images_image_id: "26623"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26623 -->
Vooraangezicht knik tractor