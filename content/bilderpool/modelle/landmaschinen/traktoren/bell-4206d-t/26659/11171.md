---
layout: "comment"
hidden: true
title: "11171"
date: "2010-03-07T23:25:44"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wie werden die dreieckigen Raupenbänder davor geschützt, sich bei einem Hindernis einfach um sich selbst zu drehen? So dass die Grundseite auch wirklich unten bleibt?

How are the triangles protected form just turning around themselves when hitting something? So that the ground side stays at the bottom?

Stefan