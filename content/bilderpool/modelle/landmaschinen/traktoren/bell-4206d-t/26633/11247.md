---
layout: "comment"
hidden: true
title: "11247"
date: "2010-03-22T07:53:18"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das Modell ist Spitzenklasse! Technik, Design -- alles passt. Mit der Übersetzung direkt am Rad ist der Antrieb auch einsatztauglich.

Gruß,
Harald