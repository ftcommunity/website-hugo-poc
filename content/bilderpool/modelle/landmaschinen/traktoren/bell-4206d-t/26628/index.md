---
layout: "image"
title: "knik punt cardanas"
date: "2010-03-07T10:12:47"
picture: "P3060368.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/26628
- /detailse33d.html
imported:
- "2019"
_4images_image_id: "26628"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26628 -->
cardan gewrichten zitten direct aan elkaar. Moest een asje bewerken om dit zo te krijgen. Je moet 2 cardan gewrichten gebruiken omdat anders tijdens het knikken de as niet eenparig ronddraait. Danzij de 2 cardan stukken kun je ook scherper sturen/knikken, zonder dat het schokkerig gaat.