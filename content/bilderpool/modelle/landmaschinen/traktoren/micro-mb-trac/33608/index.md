---
layout: "image"
title: "Unten"
date: "2011-12-03T19:52:32"
picture: "micrombtrac6.jpg"
weight: "6"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33608
- /detailsb0e2.html
imported:
- "2019"
_4images_image_id: "33608"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33608 -->
Hier sieht man das Chassis. Eigentlich ziemlich unspektakulär.