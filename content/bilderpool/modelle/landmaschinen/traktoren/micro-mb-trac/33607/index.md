---
layout: "image"
title: "Mit 'geöffneter Motorhaube'"
date: "2011-12-03T19:52:32"
picture: "micrombtrac5.jpg"
weight: "5"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33607
- /details151d.html
imported:
- "2019"
_4images_image_id: "33607"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33607 -->
Nein, ich mache keine Schleichwerbung für die Batterienmarke ;-)