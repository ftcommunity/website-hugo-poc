---
layout: "image"
title: "Vorne"
date: "2011-12-03T19:52:32"
picture: "micrombtrac2.jpg"
weight: "2"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- /php/details/33604
- /detailsdb0f-3.html
imported:
- "2019"
_4images_image_id: "33604"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33604 -->
Im Hintergrund unser Balkon und die Tanne (oder Fichte oder was weiß ich was) unserer Nachbarn.