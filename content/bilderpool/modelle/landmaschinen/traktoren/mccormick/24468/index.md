---
layout: "image"
title: "Von vorne"
date: "2009-06-29T23:27:02"
picture: "mccormick1.jpg"
weight: "1"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24468
- /detailsdf6b.html
imported:
- "2019"
_4images_image_id: "24468"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24468 -->
