---
layout: "image"
title: "Der Akku und Anschlüsse"
date: "2009-06-29T23:27:02"
picture: "mccormick6.jpg"
weight: "6"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24473
- /details28de.html
imported:
- "2019"
_4images_image_id: "24473"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24473 -->
Hier sieht man die Stromversorgung un den Anschluss der Lampen und einem Anschluss für einen Zusatzmotor auf einem Gerät(Dar der Traktor keine Zapfwelle hat)