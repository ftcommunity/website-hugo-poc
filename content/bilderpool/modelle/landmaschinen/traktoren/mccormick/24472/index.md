---
layout: "image"
title: "oben rechts"
date: "2009-06-29T23:27:02"
picture: "mccormick5.jpg"
weight: "5"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24472
- /details982e.html
imported:
- "2019"
_4images_image_id: "24472"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24472 -->
