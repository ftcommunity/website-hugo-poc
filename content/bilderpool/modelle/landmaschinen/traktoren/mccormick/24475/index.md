---
layout: "image"
title: "Hinten rechts"
date: "2009-06-29T23:27:02"
picture: "mccormick8.jpg"
weight: "8"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- /php/details/24475
- /details9fa0.html
imported:
- "2019"
_4images_image_id: "24475"
_4images_cat_id: "1680"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24475 -->
