---
layout: "image"
title: "Hydraulik"
date: "2007-05-27T18:23:24"
picture: "PICT0006.jpg"
weight: "3"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10521
- /detailsaf7f.html
imported:
- "2019"
_4images_image_id: "10521"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-05-27T18:23:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10521 -->
Hier kan man die Magnetventiele und die 
Heck Hydraulik sehen.Wenn ich etwas anhängen möchte dann klipse ich ein BS 7,5 in die 
Achse.
