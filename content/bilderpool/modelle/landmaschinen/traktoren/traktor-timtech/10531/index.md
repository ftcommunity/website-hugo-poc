---
layout: "image"
title: "Gesammtansicht"
date: "2007-05-27T18:23:25"
picture: "PICT0015.jpg"
weight: "13"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/10531
- /detailsb721-2.html
imported:
- "2019"
_4images_image_id: "10531"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-05-27T18:23:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10531 -->
Hier sieht man noch einmal den ganzen Trecker.
