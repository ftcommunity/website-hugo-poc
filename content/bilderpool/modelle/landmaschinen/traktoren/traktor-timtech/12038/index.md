---
layout: "image"
title: "Pendelachse"
date: "2007-09-29T12:37:09"
picture: "fischertechnik_002.jpg"
weight: "18"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Tim Ronellenfitsch (timtech)"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- /php/details/12038
- /detailsd871-2.html
imported:
- "2019"
_4images_image_id: "12038"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-09-29T12:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12038 -->
Hier sieht man die Pendelachsen aufhängung.
