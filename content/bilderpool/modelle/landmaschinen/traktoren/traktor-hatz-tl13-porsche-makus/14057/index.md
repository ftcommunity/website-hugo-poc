---
layout: "image"
title: "06"
date: "2008-03-23T23:40:40"
picture: "06.jpg"
weight: "6"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14057
- /detailsae52-2.html
imported:
- "2019"
_4images_image_id: "14057"
_4images_cat_id: "1289"
_4images_user_id: "327"
_4images_image_date: "2008-03-23T23:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14057 -->
