---
layout: "image"
title: "Tractor-non-IR"
date: "2018-01-23T16:33:17"
picture: "tractornonir01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47173
- /detailseb9a.html
imported:
- "2019"
_4images_image_id: "47173"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-23T16:33:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47173 -->
