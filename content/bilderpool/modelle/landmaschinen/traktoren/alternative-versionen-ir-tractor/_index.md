---
layout: "overview"
title: "Alternative versionen IR-Tractor"
date: 2020-02-22T08:25:58+01:00
legacy_id:
- /php/categories/3490
- /categories188f.html
- /categories9269.html
- /categories3928.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3490 --> 
Hier werde ich Baufasen zeigen fur alternative Versionen von den Tractor aus den Tractor-IR-set; nämlich ein Version ohne Motoren und ein Version mit den Powermotor 50:1 statt den grauen Tractormotor.