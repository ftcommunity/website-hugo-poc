---
layout: "image"
title: "Tractor-PM-fase06-07"
date: "2018-01-24T17:32:34"
picture: "tractorpm7.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47189
- /details1895.html
imported:
- "2019"
_4images_image_id: "47189"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-24T17:32:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47189 -->
