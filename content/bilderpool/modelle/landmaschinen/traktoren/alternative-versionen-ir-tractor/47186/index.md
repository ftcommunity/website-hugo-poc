---
layout: "image"
title: "Tractor-PM-fase03"
date: "2018-01-24T17:32:34"
picture: "tractorpm4.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47186
- /details4fa7-3.html
imported:
- "2019"
_4images_image_id: "47186"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-24T17:32:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47186 -->
Die kleine Kette ist für extra Sicherung der Motor, man sieht nog ein kleines Stükchen davon. Man sieht's besser in das Bild von Unten (2 Bilder zurück). Die Kette hat 15 Glieder.