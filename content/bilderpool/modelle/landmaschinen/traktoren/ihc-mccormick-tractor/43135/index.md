---
layout: "image"
title: "IHC Anblick vorn rechts"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor01.jpg"
weight: "1"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43135
- /details3b18.html
imported:
- "2019"
_4images_image_id: "43135"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43135 -->
Hier seht ihr das erste Modell bei dem mir die Vollfunktion absolut gelungen war und nicht, wie öfters eben "irgendwie fast perfekt, aber doch nicht ganz perfekt".