---
layout: "image"
title: "IHC noch mal von vorn 2"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor26.jpg"
weight: "26"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43160
- /details57e8.html
imported:
- "2019"
_4images_image_id: "43160"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43160 -->
ohne Beschreibung