---
layout: "image"
title: "IHC Schaltechnik 1 Nullstellung"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor31.jpg"
weight: "31"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43165
- /details89f8-2.html
imported:
- "2019"
_4images_image_id: "43165"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43165 -->
Hier eine Beschreibung von einem Schaltvorgang mit dem Microservo 132292 und einem Minitaster 37780, den ich mir für den IHC ausgedacht habe. Somit konnte ich die Zapfwelle ein- und ausschalten. Die Bauplatte 5  mit den zwei Federnocken bleibt am Schaltnocken vom Minitaster hängen, wie man in den folgenden Bildern sehen wird, Somit standen mir noch drei Funktionen am Control-Set zur Verfügung für den Fahrmotor,den Lenkmotor und das Heben und Senken der Ackerschiene. Das Bild zeigt die Nullstellung.