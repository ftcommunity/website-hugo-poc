---
layout: "image"
title: "IHC Lenkung 1"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor07.jpg"
weight: "7"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43141
- /details8324.html
imported:
- "2019"
_4images_image_id: "43141"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43141 -->
ohne Beschreibung