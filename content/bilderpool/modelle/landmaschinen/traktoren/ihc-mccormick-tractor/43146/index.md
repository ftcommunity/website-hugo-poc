---
layout: "image"
title: "IHC Hinterachse 3"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor12.jpg"
weight: "12"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43146
- /detailsc755-2.html
imported:
- "2019"
_4images_image_id: "43146"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43146 -->
ohne Beschreibung