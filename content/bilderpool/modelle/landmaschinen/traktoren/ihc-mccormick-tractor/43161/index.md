---
layout: "image"
title: "IHC von der seite hinten"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor27.jpg"
weight: "27"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- /php/details/43161
- /detailscbbf.html
imported:
- "2019"
_4images_image_id: "43161"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43161 -->
ohne Beschreibung