---
layout: "image"
title: "AllTrac 1"
date: "2005-10-30T16:57:11"
picture: "AllTrac_01.jpg"
weight: "1"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5151
- /detailse1f4.html
imported:
- "2019"
_4images_image_id: "5151"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5151 -->
Heckansicht mit Anbaumöglichkeiten für Geräte. Der Zapfwellenanschluss ist unten in der Mitte
