---
layout: "image"
title: "Das Original"
date: 2023-03-12T12:09:58+01:00
picture: "DO-Porsche-01.jpeg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
uploadBy: "Website-Team"
license: "unknown"
---

Bild-Quelle: http://www.fahrzeugseiten.de/Traktoren/Porsche_Diesel/Standard_T_217/standard_t_217.html