---
layout: "overview"
title: "Traktor Porsche Diesel Standard T 217"
date: 2023-03-12T12:09:49+01:00
---

 Traktor Porsche Diesel Standard T 217

 Produktionszeitraum: 1960 bis 1962. 

 Ein Model in der Größe des fischertechenik-Baukastens ADVANCED.
 Mal ein kleineres Modell, um zu zeigen, dass auch kleine Modelle ziemlich realistisch gebaut werden können.
