---
layout: "image"
title: "ferngesteuerter Traktor"
date: "2012-05-19T20:22:22"
picture: "ferngesteuertertraktor5.jpg"
weight: "5"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34975
- /detailsf2f8.html
imported:
- "2019"
_4images_image_id: "34975"
_4images_cat_id: "2589"
_4images_user_id: "1476"
_4images_image_date: "2012-05-19T20:22:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34975 -->
