---
layout: "image"
title: "ferngesteuerter Traktor"
date: "2012-05-19T20:22:22"
picture: "ferngesteuertertraktor3.jpg"
weight: "3"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
uploadBy: "coini"
license: "unknown"
legacy_id:
- /php/details/34973
- /detailsc0c3.html
imported:
- "2019"
_4images_image_id: "34973"
_4images_cat_id: "2589"
_4images_user_id: "1476"
_4images_image_date: "2012-05-19T20:22:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34973 -->
