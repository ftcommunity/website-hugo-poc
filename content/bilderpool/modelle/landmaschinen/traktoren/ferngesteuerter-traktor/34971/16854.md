---
layout: "comment"
hidden: true
title: "16854"
date: "2012-05-19T20:37:53"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die Geometrie am Kraftheber hinten gehört nochmal überarbeitet, damit es ein "oben" und ein "unten" gibt. 

Sehr schickes Teil! Die Proportionen und Formen stimmen, und da ist kein Teil zuviel verbaut. Eine Wegzapfwelle ist auch noch dran. Selbst die Lenkgeometrie scheint hinzukommen, obwohl nach der Ackermann-Regel (http://de.wikipedia.org/wiki/Lenkung) die roten Hebel eher anders herum gehören würden. 

Gruß,
Harald