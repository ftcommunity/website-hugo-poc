---
layout: "image"
title: "Mini-RC-Traktor V2 13"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv13.jpg"
weight: "13"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37925
- /details9d81-2.html
imported:
- "2019"
_4images_image_id: "37925"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37925 -->
Ansicht von unten. Recht glatter Unterboden.