---
layout: "image"
title: "Mini-RC-Traktor V2 4"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv04.jpg"
weight: "4"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37916
- /details2ab4.html
imported:
- "2019"
_4images_image_id: "37916"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37916 -->
Vorderradaufhängung (eigentlich Vordermotoraufhängig - wie bei Fendt Geräteträger...) in action.