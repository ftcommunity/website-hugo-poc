---
layout: "image"
title: "Mini-RC-Traktor V2 8"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv08.jpg"
weight: "8"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37920
- /details5442.html
imported:
- "2019"
_4images_image_id: "37920"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37920 -->
Ist glaube ich zu V1 etwas kürzer geworden. Proportionen der Schutzbleche passen jetzt einigemaßen.