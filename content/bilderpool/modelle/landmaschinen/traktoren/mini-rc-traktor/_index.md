---
layout: "overview"
title: "Mini-RC Traktor"
date: 2020-02-22T08:25:46+01:00
legacy_id:
- /php/categories/2819
- /categories46e6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2819 --> 
Ein Versuch, wie man auf kleinstem Raum mit Fernsteuerung einen Traktor bauen kann. Die Vorderachse lenkt direkt mit dem Servo und kann auch Hindernissen ausweichen. Hochkanter Einbau von Motor und Empfänger macht die Konstruktion kurz - ein Direktgetriebe den Antrieb schnell. Mit kleinem Radstand geht es theoretisch wndig ums eck - wenn sich das fehlende Differenzial nicht bemerkbar machen würde.....