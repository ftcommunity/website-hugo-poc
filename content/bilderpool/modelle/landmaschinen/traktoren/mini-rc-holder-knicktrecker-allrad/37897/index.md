---
layout: "image"
title: "Holder Mini mit maximalem Lenkeinschlag"
date: "2013-12-08T11:56:43"
picture: "minircholderknicktreckermitallrad1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37897
- /detailsd310.html
imported:
- "2019"
_4images_image_id: "37897"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T11:56:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37897 -->
Die Reifen berühren sich gerade nicht, durch den kleinen Achsabstand ist der Trecker wie sein Vorbild sehr wendig. Antrieb vorne und hinten gibt ihm viel Power, trotz der recht schnellen Übersetzung.