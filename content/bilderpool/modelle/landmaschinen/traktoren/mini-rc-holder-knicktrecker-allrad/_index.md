---
layout: "overview"
title: "Mini-RC Holder KnickTrecker mit Allrad"
date: 2020-02-22T08:25:45+01:00
legacy_id:
- /php/categories/2818
- /categories613f.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2818 --> 
Wenn man den Servoarm direkt mit einem 7.5er 37468 und unter Druck mit einer 20er Rastachse 31690 verbindet, kann man kompakte Lenkungen bauen. Beispiel hier ein Allrad Holder Traktor, der sehr antriebsstark ist. Fährt flott und präzise. Wiederum: soviel Motor wie möglich. Aber - ogott - ein Reifen ist falsch montiert und die Kabel. Die Kabel! Viel Spaß, Dieter.