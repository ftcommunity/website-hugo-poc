---
layout: "image"
title: "Zugmaschine 2"
date: "2013-12-08T11:56:44"
picture: "minircholderknicktreckermitallrad7.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37903
- /detailsff88.html
imported:
- "2019"
_4images_image_id: "37903"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T11:56:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37903 -->
... und ich meinte: viele Anhänger.