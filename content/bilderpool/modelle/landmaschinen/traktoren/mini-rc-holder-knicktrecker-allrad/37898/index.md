---
layout: "image"
title: "Von Hinten"
date: "2013-12-08T11:56:44"
picture: "minircholderknicktreckermitallrad2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37898
- /detailsb7a9.html
imported:
- "2019"
_4images_image_id: "37898"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T11:56:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37898 -->
Eine Anhängekupplung muss schon sein. Ging leider nicht mehr, den Empfänger weiter nach vorne zu schieben.