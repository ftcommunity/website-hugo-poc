---
layout: "image"
title: "Trecker aus der Nähe"
date: "2013-12-08T11:56:44"
picture: "minircholderknicktreckermitallrad5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- /php/details/37901
- /details61d7.html
imported:
- "2019"
_4images_image_id: "37901"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T11:56:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37901 -->
Ein Sitz mußte ja schon noch drauf. [Oder schafft man eine Hydraulik hinten hinzubekommen statt dem Antriebsmotor? Hm...]