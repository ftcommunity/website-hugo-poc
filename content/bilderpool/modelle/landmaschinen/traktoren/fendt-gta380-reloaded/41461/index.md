---
layout: "image"
title: "Fendt380GTA Mk2 2015.jpg"
date: "2015-07-19T20:54:30"
picture: "IMG_1882.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41461
- /details412d.html
imported:
- "2019"
_4images_image_id: "41461"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:54:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41461 -->
Und hier der Grund, warum ich auf die alten Fotos gestoßen bin. Der "neue" hat Allrad, und eine durchgehende Zapfwelle, abklappbare Kabine, den neuen Kompressor, und ...
