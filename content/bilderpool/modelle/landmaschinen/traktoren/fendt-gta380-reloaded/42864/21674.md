---
layout: "comment"
hidden: true
title: "21674"
date: "2016-02-12T08:00:03"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
hmm,...  nicht ganz. Das sind zwei Impulsräder 4 (eins hinten, eins vorn), die in den V-Steinen 15 stecken. Das ganze ist dann ein Gleichlaufgelenk (im Gegensatz zu einem Kardan) und hat Längenausgleich, weil die Impulszahnräder hin und her rutschen können. Solange keins heraus fällt, kann man also die Vorderachse noch weiter verschränken.

Gruß,
Harald