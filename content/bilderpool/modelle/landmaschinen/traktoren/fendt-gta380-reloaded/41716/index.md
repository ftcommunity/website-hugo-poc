---
layout: "image"
title: "Schrägansicht"
date: "2015-08-05T21:06:56"
picture: "gtamod02.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41716
- /details9a8e-2.html
imported:
- "2019"
_4images_image_id: "41716"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41716 -->
Die Pneumatik ist fast komplett unterm Kabinendach untergebracht.
