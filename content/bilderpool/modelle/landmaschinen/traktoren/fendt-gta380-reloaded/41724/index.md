---
layout: "image"
title: "Innereien unterm Dach"
date: "2015-08-05T21:06:56"
picture: "gtamod10.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41724
- /details572f.html
imported:
- "2019"
_4images_image_id: "41724"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41724 -->
Die Dach-Innereien nochmal im Zoom.
