---
layout: "image"
title: "Frontlader von unten"
date: "2015-08-05T21:07:02"
picture: "gtamod11.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41725
- /detailsa27c.html
imported:
- "2019"
_4images_image_id: "41725"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:07:02"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41725 -->
