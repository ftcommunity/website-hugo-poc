---
layout: "image"
title: "Mk3: Blockbauweise (02-2016)"
date: "2016-02-08T00:03:38"
picture: "gtamk2.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42860
- /details92b6.html
imported:
- "2019"
_4images_image_id: "42860"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42860 -->
Der neue Antriebsblock enthält gleich fünf Motoren und ein bisschen mehr:
- Antrieb für die Heckzapfwelle (schwarz, oben mitte)
- Antrieb für die Frontzapfwelle (schwarz, unten mitte). Der Antrieb wird um 90° abgewinkelt mittels zweier Z15, von denen eins eine schwarze Kette trägt.
- Antrieb für Geräte im Zwischenachs-Anbau (schwarz, rechts unten)
- IR-Empfänger (rot, oben rechts)
- Antrieb für Kraftheber hinten (verdeckt, unter dem IR-Empfänger)
- Längsdifferenzial (Bildmitte, unterhalb der Stahlachse)
- Hinterachsdifferenzial (hier verdeckt)
- Antriebsmotor für den Allrad (XM, verdeckt unten mitte, treibt über die rote Kette das Längsdifferenzial an)
