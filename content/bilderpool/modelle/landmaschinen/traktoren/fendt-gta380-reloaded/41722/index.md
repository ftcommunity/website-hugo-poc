---
layout: "image"
title: "Kabine abgeklappt"
date: "2015-08-05T21:06:56"
picture: "gtamod08.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41722
- /detailse729.html
imported:
- "2019"
_4images_image_id: "41722"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41722 -->
Wenn die zwei Verkleidungsteile vor den Motoren entfernt sind, kann man die gesamte Kabine hinten etwas anheben, ausrasten und dann nach vorn auf die Vorderachse schwenken. Hier ist auch der Akku noch heraus genommen.
