---
layout: "image"
title: "Unterseite"
date: "2015-08-05T21:06:56"
picture: "gtamod06.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/41720
- /details0751.html
imported:
- "2019"
_4images_image_id: "41720"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41720 -->
Zentrales Element ist die lange Messingachse (die Steckachse aus hobby4 ist ein wenig zu kurz). Sie ist Drehachse für die Vorderachse und gleichzeitig eine (missglückte) Zapfwelle. Missglückt deshalb, weil sie zu tief liegt (Kardangelenke würden beinahe auf dem Boden aufliegen) und durch die Querbelastung dreht sie schwer. Außerdem sitzt der rote Encodermotor, der sie antreibt, nicht in passender Höhe und die Kette läuft schräg.

Die beiden Streben I-120 sorgen dafür, dass die Vorderachse nicht alleine wegfährt.

Hinten gibt es eine Drossel (aus Rastadapter mit m4-Helicoil-Gewindeeinsatz drin), damit der hintere Kraftheber sich schön langsam absenkt. Der Frontheber hat sowas auch eingebaut.

Der schwarze Motor unten treibt über eine Kette das Mitteldifferenzial an.
