---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-24T22:43:10"
picture: "Fischertechnik-modellen-Fendt_003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/2749
- /detailsd077.html
imported:
- "2019"
_4images_image_id: "2749"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-24T22:43:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2749 -->
