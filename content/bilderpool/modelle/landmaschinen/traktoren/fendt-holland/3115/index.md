---
layout: "image"
title: "Case-International"
date: "2004-11-14T21:52:44"
picture: "Fischertechnik-modellen-Fendt_023.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/3115
- /details4f3b.html
imported:
- "2019"
_4images_image_id: "3115"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-14T21:52:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3115 -->
