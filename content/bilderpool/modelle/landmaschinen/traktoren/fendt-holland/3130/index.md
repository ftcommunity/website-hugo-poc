---
layout: "image"
title: "Case-International"
date: "2004-11-15T11:18:21"
picture: "Fischertechnik-modellen-Fendt_038.jpg"
weight: "36"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/3130
- /details2cf7-2.html
imported:
- "2019"
_4images_image_id: "3130"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-15T11:18:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3130 -->
