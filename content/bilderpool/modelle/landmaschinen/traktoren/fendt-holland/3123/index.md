---
layout: "image"
title: "Case-International"
date: "2004-11-14T21:52:45"
picture: "Fischertechnik-modellen-Fendt_031.jpg"
weight: "29"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/3123
- /details125d.html
imported:
- "2019"
_4images_image_id: "3123"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-14T21:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3123 -->
