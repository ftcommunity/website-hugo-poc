---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-24T22:43:10"
picture: "Fischertechnik-modellen-Fendt_004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/2750
- /details388b.html
imported:
- "2019"
_4images_image_id: "2750"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-24T22:43:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2750 -->
