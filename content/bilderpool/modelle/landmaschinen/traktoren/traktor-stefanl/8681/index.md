---
layout: "image"
title: "Traktor 16"
date: "2007-01-25T17:58:40"
picture: "traktor16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8681
- /detailsa5a1-2.html
imported:
- "2019"
_4images_image_id: "8681"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8681 -->
