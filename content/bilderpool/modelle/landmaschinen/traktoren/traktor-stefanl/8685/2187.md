---
layout: "comment"
hidden: true
title: "2187"
date: "2007-01-26T16:40:50"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

ich meine die Vorrichtung vorne, an die man Zusatzballast hängen kann, wenn man hinten schwere Geräte wie z.B. einen großen Wendepflug anbaut. Das sind einfach nur Gewichte, die verhindern, daß der Traktor Männchen macht.

Gruß,
Franz