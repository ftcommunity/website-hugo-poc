---
layout: "comment"
hidden: true
title: "2179"
date: "2007-01-25T23:22:47"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

ist doch ein schickes Teil geworden! Jetzt noch ein paar Details dran (Kotflügel über den Hinterrädern, Trittstufen, Ballasthalter vorne, ...) und das Ding ist perfekt!

Gruß,
Franz