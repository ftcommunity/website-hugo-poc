---
layout: "image"
title: "Traktor 13"
date: "2007-01-25T17:58:40"
picture: "traktor13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8678
- /details68fa-3.html
imported:
- "2019"
_4images_image_id: "8678"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8678 -->
Ganz krummes Maß von der Breite aber das musste so damit die kleinen Zahnräder genau passen.
