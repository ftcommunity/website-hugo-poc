---
layout: "image"
title: "Traktor 21"
date: "2007-01-25T17:58:40"
picture: "traktor21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/8686
- /detailscf7c-2.html
imported:
- "2019"
_4images_image_id: "8686"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8686 -->
Der power Motor ist tatsächlich leicht schräg eingebaut weil er sonst das Differential streifen würde,
