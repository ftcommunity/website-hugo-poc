---
layout: "overview"
title: "Nano-Traktor"
date: 2020-02-22T08:25:38+01:00
legacy_id:
- /php/categories/1312
- /categories4daa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1312 --> 
Nachdem der Bau des motorisierten Mini-Traktors mit Anhänger so viel Spass gemacht hat, wollte ich natürlich wissen, ob es noch kleiner geht (allerdings ohne Motor).