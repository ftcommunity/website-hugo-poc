---
layout: "image"
title: "Motorisierter Mini-Traktor 01"
date: "2008-04-04T21:55:04"
picture: "20080404_Fischertechnik_Mini-Traktoren_01.jpg"
weight: "1"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- /php/details/14171
- /detailsc355.html
imported:
- "2019"
_4images_image_id: "14171"
_4images_cat_id: "1311"
_4images_user_id: "327"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14171 -->
