---
layout: "image"
title: "tarnkapp7779.jpg"
date: "2012-10-07T15:30:02"
picture: "IMG_7779.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35807
- /details1e31-2.html
imported:
- "2019"
_4images_image_id: "35807"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:30:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35807 -->
Das Heck mit dem bisschen noch verbliebenen Platz für die Antriebe von Quer- und Höhenruder, Fahrwerksklappen und Bombenschacht. Die Triebwerksgondeln könnte man noch mit Dummies bestücken.
