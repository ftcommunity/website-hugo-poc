---
layout: "image"
title: "tarnkapp7775.jpg"
date: "2012-10-07T15:26:59"
picture: "IMG_7775.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35806
- /detailsbc38.html
imported:
- "2019"
_4images_image_id: "35806"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:26:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35806 -->
Blick von hinten auf die Rumpfmitte. Obenauf thront der Antriebsmotor des Hauptfahrwerks. Die beiden Kranhaken sollten zum Aufhängen des Modells dienen (ha, *einmal* von vornherein auch daran gedacht!); aber unter der Last des eigenen Gewichts war damit doch "Essig".

Die beiden Taster unten sind die Endanschläge fürs Hauptfahrwerk. In den Z30 links und rechts stecken ft-Kurbelachsen, die jeweils einen dieser Taster betätigen. Funktioniert einwandfrei, wird aber wieder ausgebaut, weil ich den Platz für anderes benötige: der S-Motor quer unten drin, nebst der gerade so erkennbaren Angelschnur, sollte den Bombenschacht öffnen und schließen (das sind die beiden roten Klappen, links halb geöffnet), was aber nichts geworden ist.

Ganz oben erkennt man, dass die Pilotensitze umgeklappt werden können. Der rechte ist gerade in flacher Position und gibt den Zugriff auf die Mechanik darunter frei.
