---
layout: "image"
title: "tarnkapp7550"
date: "2012-10-07T15:35:22"
picture: "IMG_7550.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35808
- /details2da7.html
imported:
- "2019"
_4images_image_id: "35808"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35808 -->
Bug- und Hauptfahrwerk sind aus der ft:pedia 02/2012 bekannt. Es sind nur noch Endschalter für beide hinzu gekommen.
