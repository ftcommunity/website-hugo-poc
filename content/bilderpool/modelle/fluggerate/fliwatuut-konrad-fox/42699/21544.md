---
layout: "comment"
hidden: true
title: "21544"
date: "2016-01-10T15:47:47"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ja, das ist prima gebaut und hat schöne Details! Vor allem sind bei solchen Modellen viele Hebel wichtig, damit in der Phantasie noch mehr Sachen eingeschaltet werden können, als sowieso schon im Modell stecken.

Wie wäre es mit einem kleinen Puppenfilm mit diesem Modell als Held?

Gruß,
Stefan