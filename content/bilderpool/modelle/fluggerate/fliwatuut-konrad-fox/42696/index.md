---
layout: "image"
title: "Gesamtansicht des FliWaTüüts"
date: "2016-01-10T14:29:38"
picture: "dasfliwatueuetkonradfox2.jpg"
weight: "2"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/42696
- /detailse2d5.html
imported:
- "2019"
_4images_image_id: "42696"
_4images_cat_id: "3178"
_4images_user_id: "1126"
_4images_image_date: "2016-01-10T14:29:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42696 -->
Bis auf kleinere konstruktionsbedingte Abweichungen (vier statt drei Rotorblätter, Verzicht auf die gefederte Radaufhängung) hier der Nachbau von Konrad (9 Jahre).