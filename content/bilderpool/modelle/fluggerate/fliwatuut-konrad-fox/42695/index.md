---
layout: "image"
title: "Original-Modell des FliWaTüüt (WDR)"
date: "2016-01-10T14:29:38"
picture: "dasfliwatueuetkonradfox1.jpg"
weight: "1"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "unknown"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/42695
- /details6348.html
imported:
- "2019"
_4images_image_id: "42695"
_4images_cat_id: "3178"
_4images_user_id: "1126"
_4images_image_date: "2016-01-10T14:29:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42695 -->
Hier eine Seitenansicht des Original-Modells der WDR-Verfilmung.