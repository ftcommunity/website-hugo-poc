---
layout: "image"
title: "TIE-Abfang-Bomber"
date: "2018-06-02T13:41:02"
picture: "TIE_Abfang_Bomber.jpg"
weight: "6"
konstrukteure: 
- "ForscherTeam"
fotografen:
- "Rito"
schlagworte: ["Raumschiff", "Star Wars", "Minimodell"]
uploadBy: "ForscherTeam"
license: "unknown"
legacy_id:
- /php/details/47683
- /details1989.html
imported:
- "2019"
_4images_image_id: "47683"
_4images_cat_id: "3503"
_4images_user_id: "2864"
_4images_image_date: "2018-06-02T13:41:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47683 -->
Ein kleines Minimodell als Anregung für Lucasfilm.
Konstruiert von F(10 Jahre)
