---
layout: "image"
title: "Kleinmodelle aus dem All"
date: "2018-04-15T18:11:30"
picture: "weltraumodelle1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/47407
- /detailsedde.html
imported:
- "2019"
_4images_image_id: "47407"
_4images_cat_id: "3503"
_4images_user_id: "968"
_4images_image_date: "2018-04-15T18:11:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47407 -->
