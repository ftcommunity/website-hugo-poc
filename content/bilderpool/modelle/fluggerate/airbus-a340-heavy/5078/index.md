---
layout: "image"
title: "A340H_236.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_236.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5078
- /detailsdaaa.html
imported:
- "2019"
_4images_image_id: "5078"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5078 -->
about to land... (3 of 5)

Althought it might seem that way, the main landing gear has not yet reached its final position. This will be when the nose landing gear also has reached its end position.
