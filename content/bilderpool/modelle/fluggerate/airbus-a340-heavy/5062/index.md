---
layout: "image"
title: "A340H_102.JPG"
date: "2005-10-06T17:25:26"
picture: "A340H_102.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5062
- /detailsbba4.html
imported:
- "2019"
_4images_image_id: "5062"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5062 -->
This is outdated but still shows the basic idea of the main landing gear and how it fits into body and wings. The current design is somewhat different. 

It's based on landing gear model FWH12 ( http://www.ftcommunity.de/details.php?image_id=4382 ) with the wheels replaced by more (and smaller) ones and the apparatus rotated by 90°.
