---
layout: "comment"
hidden: true
title: "722"
date: "2005-10-15T11:18:52"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Deine Bilder sind einfach ein Genuss!So viel Technik. So viel Frickelei. Und Deine selbsttragende Bauweise lässt mich immer wieder staunen.

Gruß,
Stefan