---
layout: "image"
title: "A340H_286.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_286.jpg"
weight: "25"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Speichenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5083
- /details250d-2.html
imported:
- "2019"
_4images_image_id: "5083"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5083 -->
Jet engine (bottom) and dummy jet engine (top). The black axle is all that keeps the left and right halfs of these engines together.
