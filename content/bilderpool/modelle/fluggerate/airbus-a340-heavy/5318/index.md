---
layout: "image"
title: "Querruder35.JPG"
date: "2005-11-11T13:06:57"
picture: "Querruder35.JPG"
weight: "41"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5318
- /details2ca6-2.html
imported:
- "2019"
_4images_image_id: "5318"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T13:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5318 -->
Das Getriebe, das die beiden Querruder gegenläufig ansteuert und den Motor in den Endlagen stillsetzt. Der Motor sitzt vorn zwischen Bug- und Hauptfahrwerk; die Antriebswelle (links unten, mit Klemm-Z10) geht weiter nach hinten zum Seitenruder.

Detailfotos vom Getriebe gibt es hier:
http://www.ftcommunity.de/categories.php?cat_id=456
