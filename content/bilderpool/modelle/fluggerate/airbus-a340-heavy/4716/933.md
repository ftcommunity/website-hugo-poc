---
layout: "comment"
hidden: true
title: "933"
date: "2006-03-20T09:06:29"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Hast du inzwischen schon einen Test gemacht? 
Angebrochene Flaschen lassen sich ohne weiteres lagern. Du musst nur vor dem nächsten gebrauch den ausgehärteten Schaum aus dem Röhrchen entfernen, evtl. mit ner Schraube, das geht ganz einfach. Wenn der Test wieder so ausfällt wie bei deinem ersten Mal, dann würde ich mal zum OBI Markt hinfahren. So ist das nämlich **nicht**OK.