---
layout: "image"
title: "A340Hz-foam01.JPG"
date: "2005-09-20T19:46:54"
picture: "A340-foam01.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4716
- /details72b3.html
imported:
- "2019"
_4images_image_id: "4716"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-09-20T19:46:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4716 -->
Das kommt dabei heraus, wenn man meint, mal eben auf die Schnelle eine Transportschale für ein Modell aufschäumen zu können.

Auf der PU-Schaumflasche stand etwas von "Ergibt 45 Liter Schaum". Naja. 4,5 Liter (vier komma fünf) käme wohl eher hin. Auf dem Bild sieht man das Ergebnis von DREI Flaschen.
