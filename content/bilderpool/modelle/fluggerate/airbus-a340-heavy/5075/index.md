---
layout: "image"
title: "A340H_226.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_226.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5075
- /details14ae.html
imported:
- "2019"
_4images_image_id: "5075"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5075 -->
The cargo hatch on the left side opened, with its lock (black strut-15) disengaged.
