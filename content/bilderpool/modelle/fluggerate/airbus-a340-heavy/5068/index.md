---
layout: "image"
title: "A340H_170.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_170.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5068
- /details069c.html
imported:
- "2019"
_4images_image_id: "5068"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5068 -->
Ok, here's one last 'outdated' photograph: this arrangement (shown with chains not mounted and worm gear out of place) did not work satisfactorily.
