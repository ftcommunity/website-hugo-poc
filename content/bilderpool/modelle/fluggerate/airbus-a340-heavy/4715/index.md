---
layout: "image"
title: "A340Hz_A340heavy-04.JPG"
date: "2005-09-18T20:35:33"
picture: "A340heavy-04.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Speichenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4715
- /details17da.html
imported:
- "2019"
_4images_image_id: "4715"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-09-18T20:35:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4715 -->
Das hier sind die Triebwerke (die inneren mit Motor, die äußeren sind Dummies).

Den Rest gibt es aber erst am Samstag in Mörshausen zu sehen...
