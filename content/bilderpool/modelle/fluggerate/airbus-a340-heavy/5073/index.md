---
layout: "image"
title: "A340H_220.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_220.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5073
- /detailsb8e5.html
imported:
- "2019"
_4images_image_id: "5073"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5073 -->
The ramp is operated manually.
