---
layout: "image"
title: "A340H_266.JPG"
date: "2005-10-14T18:19:46"
picture: "A340H_266.jpg"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5091
- /details23e3.html
imported:
- "2019"
_4images_image_id: "5091"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-14T18:19:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5091 -->
This is the light electronics and the joystick (with the actual stick missing but simply imagine an axle 30 in the mid of the switches). The cockpit is too small for it, so the joystick had to move to the rear.
