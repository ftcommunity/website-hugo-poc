---
layout: "image"
title: "BugFW06.JPG"
date: "2005-11-11T12:53:45"
picture: "BugFW06.JPG"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5312
- /detailsbbda.html
imported:
- "2019"
_4images_image_id: "5312"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5312 -->
Der Antrieb kommt über die Achse unten, dann die beiden Rast-Z10, zwei Kegelzahnräder und über ein Rast-Z10 auf der gegenüberliegenden Seite auf die Achse mit den Z30.
Der rote Mitnehmer in Bildmitte steckt in einem Rollenbock 32085, der schräg abgeschnitten wurde und hier fast wie ein Winkelstein aussieht.
