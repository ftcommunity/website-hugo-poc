---
layout: "image"
title: "A340H_240.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_240.jpg"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5080
- /details5e87.html
imported:
- "2019"
_4images_image_id: "5080"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5080 -->
about to land... (5 of 5)

Ready for landing. This is when all landing gears have passed a 'dead center' position and can carry weight without running the risk that this weight might drive the gear back into the fuselage.
