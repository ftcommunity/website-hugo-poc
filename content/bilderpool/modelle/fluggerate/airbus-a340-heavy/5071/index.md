---
layout: "image"
title: "A340H_210.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_210.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5071
- /detailsebd6-2.html
imported:
- "2019"
_4images_image_id: "5071"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5071 -->
Clockwise from top-left:

top-left: two E-Tec modules that control the position lights (white, on top of the yaw rudder; red+green, on left+right wings).
top-right: the gear that opens the front by lifting up the cockpit.
bottom-right: the nose landing gear in retracted position where it actuates two switches (of which only the upper, gray one is visible).
mid-left: the main landing gear.
