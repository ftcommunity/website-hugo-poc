---
layout: "image"
title: "A340H_254.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_254.jpg"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5082
- /details3090.html
imported:
- "2019"
_4images_image_id: "5082"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5082 -->
Main landing gear retracted, seen from the inside. The cargo compartment flooring was removed to make this picture.
