---
layout: "image"
title: "Airbus A300"
date: "2011-04-02T12:47:37"
picture: "airbusa5.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30371
- /details5c9e.html
imported:
- "2019"
_4images_image_id: "30371"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-02T12:47:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30371 -->
Die Sicht auf den linken Flügel.