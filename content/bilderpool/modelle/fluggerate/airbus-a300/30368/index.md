---
layout: "image"
title: "Airbus A300"
date: "2011-04-02T12:47:37"
picture: "airbusa2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30368
- /detailsacd8.html
imported:
- "2019"
_4images_image_id: "30368"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-02T12:47:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30368 -->
Vorderansicht des Airbus