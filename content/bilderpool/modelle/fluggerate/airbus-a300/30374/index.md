---
layout: "image"
title: "Airbus A300"
date: "2011-04-02T12:47:37"
picture: "airbusa8.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30374
- /details4059.html
imported:
- "2019"
_4images_image_id: "30374"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-02T12:47:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30374 -->
Hier sieht man das Schneckengetriebe des linken Heckfahrwerks. Nach demselben Prinzip funktioniert das Bugfahrwerk auch.