---
layout: "image"
title: "Ladeluke"
date: "2011-04-06T16:48:02"
picture: "a1.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/30429
- /details176f.html
imported:
- "2019"
_4images_image_id: "30429"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-06T16:48:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30429 -->
Hier sieht man die Ladeluke in geschlossenem Zustand.