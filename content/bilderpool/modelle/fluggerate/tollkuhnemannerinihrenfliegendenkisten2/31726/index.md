---
layout: "image"
title: "Das Speichenrad als Grundlage 1"
date: "2011-08-30T23:21:42"
picture: "rollenlager1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/31726
- /details7f1f.html
imported:
- "2019"
_4images_image_id: "31726"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31726 -->
An dem Speichenrad lässt sich alles mögliche mit runder ft-Nut befestigen.
Seitlich draufschieben und um 90 Grad drehen, - fertig.
