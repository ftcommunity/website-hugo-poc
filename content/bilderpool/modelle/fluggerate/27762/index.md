---
layout: "image"
title: "Tragfläche1"
date: "2010-07-16T11:15:24"
picture: "IMG_3123_Tragflche.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/27762
- /details7aff.html
imported:
- "2019"
_4images_image_id: "27762"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:15:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27762 -->
Hier kommen auch noch Verstrebungen drauf. Aber dazwischen kommt noch...
