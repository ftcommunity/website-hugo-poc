---
layout: "image"
title: "Libelle met vleugeldetails"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle31.jpg"
weight: "23"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39071
- /details776e-3.html
imported:
- "2019"
_4images_image_id: "39071"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39071 -->
Netkousen simuleren de verharde aderen-net-structuur die voor de stabiliteit van de libelle-vleugel zorgt.  
Hieronder een koolstofvezelstaaf 4mm  om extra stijfheid te verkrijgen in de vleugels. 
Kunststof stelringen met M4-schroefdraad zijn zeer praktisch en degelijk om assen, koolstof-vezelstaven of draadeinden vast te zetten of de geleiden.  

Samenvattend overall-overzicht +  inzicht in de werking + ontwikkeling Robot-Libelle : 

http://www.festo.com/net/SupportPortal/Files/248133/Festo_BionicOpter_de.pdf

http://www.festo.com/rep/de_corp/assets/pdf/GEO_Infografik_Roboterlibelle.pdf

Youtube-link Fischertechnik Libelle :

https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1
