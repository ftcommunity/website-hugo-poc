---
layout: "image"
title: "Fischertechnik Libelle"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39049
- /detailsfc0f.html
imported:
- "2019"
_4images_image_id: "39049"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39049 -->
In de zomervakantie-2014 heb ik van Fischertechnik een Libelle met 4 onafhankelijk bewegende vleugels gemaakt. Een en ander geïnspireerd op de Festo-Bionic-Opter.  
Zie:     https://www.youtube.com/watch?v=wZl0LUWHbCw

Libellen hebben zich in de loop van hun 300 miljoen jaar lange evolutionaire geschiedenis gespecialiseerd. Hun vliegtechniek is uniek. Ze bewegen zich moeiteloos in alle richtingen in de ruimte, blijven vrij in de lucht hangen en glijden over water zonder één vleugelslag. Dankzij hun twee paar onafhankelijk van elkaar te bewegen vleugels kunnen ze plotseling stoppen en omdraaien, snel versnellen en zelfs achteruit vliegen.


Voor het overall-overzicht én het inzicht in de werking + ontwikkeling zijn vooral de volgende 2 weblinks van Festo zeer verhelderend : 

http://www.festo.com/net/SupportPortal/Files/248133/Festo_BionicOpter_de.pdf

http://www.festo.com/rep/de_corp/assets/pdf/GEO_Infografik_Roboterlibelle.pdf

De BionicOpter beheerst als eerste model meer vliegtechnieken dan helikopters, motor- en zweefvliegtuigen samen.
Lichtgewichtconstructie De kunstmatige libel is een echt lichtgewicht. Met een spanwijdte van 63 cm en een lengte van 44 cm weegt de libel slechts 175 gram. En dit dankzij de consistente lichtgewichtconstructie van de BionicOpter. De vleugels bestaan uit een fijne carbonframe en een ultradunne foliebedekking. De structuur van de behuizing en de mechanische componenten uit elastisch polyamide en teerpolymeer verschaft een zeer flexibel, ultralicht maar robuust totaalsysteem. Op een smal gebied van de borst bevindt zich de batterij, acht servomotoren, een krachtige ARM  microcontroller, sensoren en draadloze modules. Geminiaturiseerd en geïntegreerd De unieke vliegeigenschappen van BionicOpter worden mogelijk gemaakt dankzij de lichte constructie en de hoge mate van functionele integratie. Voor de kunstmatige libel betekent dit dat onderdelen zoals sensoren, actuatoren en mechanica, alsook communicatie-, stuur- en regeltechniek in een beperkte ruimte geïnstalleerd en gecoördineerd moeten worden. 

Zo toont Festo de mogelijkheden en perspectieven van functionele integratie in miniatuur. De op afstand bestuurbare libel communiceert draadloos in real-time en wisselt permanent informatie uit. Het combineert de meest uiteenlopende sensorwaarden, detecteert zelfstandig complexe gebeurtenissen en kritische situaties. 


De "truuk" om de amplitude van de vleugel-uitslag per vleugel te kunnen verstellen in Fischertechnik gebeurd door de 4mm vleugelslag-aandrijfas (met een 40 graden hoek) vrij door te voeren via een 5mm messing-buisprofiel met een binnendiameter van 4,1 mm. Met de vleugel-uitslag-amplitude kan de stuwkracht worden geregeld.

Het verdraaien van de vleugelhoek bepaalt de stuwrichting. De vleugelhoek-verstelling per vleugel vindt plaats door  XS-motoren via een RoboPro-programma afhankelijk of de Libelle naar boven of beneden vliegt.

Youtube-links :
https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1      = Libelle Fischertechnik

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2    = Pijlstaartrog Fischertechnik

https://www.youtube.com/watch?v=NtDFw9uO-bs&index=4&list=UUvBlHQzqD-ISw8MaTccrfOQ       = Qualle Fischertechnik

https://www.youtube.com/watch?v=RjhEi15VK-4&index=7&list=UUvBlHQzqD-ISw8MaTccrfOQ    = Smartbird Earthflight Fischertechnik

https://www.youtube.com/watch?v=6kFnCIemQq8&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=19    = Pneumatik Muskel Fischertechnik

https://www.youtube.com/watch?v=3w9-xQq0GwI&index=25&list=UUvBlHQzqD-ISw8MaTccrfOQ    = Pneumatik Yoyo Fischertechnik

https://www.youtube.com/watch?v=NZpHLLx2ANE&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=20   = Pneuma-cube Fischertechnik
