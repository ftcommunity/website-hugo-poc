---
layout: "image"
title: "Kabeltrommel-aandrijving waarmee de Libelle kan 'stijgen' en 'dalen' 2"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle20.jpg"
weight: "12"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39060
- /details60df.html
imported:
- "2019"
_4images_image_id: "39060"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39060 -->
Deze  Vishay 10-slags 5K-potmeter is bij conrad verkrijgbaar onder o.a. artikelnr.  429287.   
Zie:  http://www.conrad.nl/ce/nl/product/429287/Vishay-10-slags-potmeter-type-534-2-W-5-?ref=list  
