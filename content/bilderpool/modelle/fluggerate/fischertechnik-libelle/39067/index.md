---
layout: "image"
title: "1mm dik transparant Polycarbonaat-plaat."
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle27.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39067
- /details4eb6-3.html
imported:
- "2019"
_4images_image_id: "39067"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39067 -->
De 4 vleugels heb ik geknipt uit 1mm dik transparant Polycarbonaat-plaat. Dikker is niet goed meer knipbaar met een huishoudschaar en dunner heeft onvoldoende stijfheid. 
Deze plaat is bij Conrad verkrijgbaar onder artikelnummer 229803.   Zie :    http://www.conrad.nl/ce/nl/product/229803/Polycarbonaat-plaat-400-mm-500-mm-1-mm
