---
layout: "image"
title: "Fischertechnik Libelle"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle02.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39050
- /details1cf0.html
imported:
- "2019"
_4images_image_id: "39050"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39050 -->
In de zomervakantie-2014 heb ik van Fischertechnik een Libelle met 4 onafhankelijk bewegende vleugels gemaakt. Een en ander geïnspireerd op de Festo-Bionic-Opter.  
Zie:     https://www.youtube.com/watch?v=wZl0LUWHbCw
Evenals m'n FT-smart-bird kan ik de Libelle wederom met de oude IR-afstand-bediening alle kanten op laten "vliegen".   
Zie:   https://www.youtube.com/watch?v=RjhEi15VK-4

Het werkingsprincipe van de Smartbird, de Pijlstaart-rog  én de BionicOpter van Festo heb ik nagebouwd met Fischertechnik. Ik heb elk model met 3 draden aan het plafond bevestigd. 
Voorts  uitgerust met de oude IR-afstandsbediening voor inschakeling van de centrale vleugelaandrijving (1), naar links- en naar rechts vliegen (2) en positionering van lijf- en staarthoogte (3).
