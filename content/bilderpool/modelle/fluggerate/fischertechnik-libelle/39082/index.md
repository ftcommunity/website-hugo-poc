---
layout: "image"
title: "Stijgen van Libelle met vleugelhoek-verstelling door XS-motoren (137096) via RoboPro"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle42.jpg"
weight: "34"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39082
- /details9be4-2.html
imported:
- "2019"
_4images_image_id: "39082"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39082 -->
Het draaien van de vleugelhoek bepaalt de stuwrichting. De vleugelhoek-verstelling per vleugel vindt plaats door  XS-motoren (137096) via een RoboPro-programma door de TX-computer-interface.  Voor herkenning van de 0-Positie gebruik ik reed-sensoren  met een buitendiameter van 3,8mm die direct ín een fischertechnik-bouwsteen geschoven kan worden. Losse reedcontacthouders kunnen hiermee achterwege blijven. De 3,8mm reedsensoren zijn bij Conrad verkrijgbaar onder nummer 503548.     
Zie :    http://www.conrad.nl/ce/nl/product/503548/PIC-MS-213-3-Cilindrische-reedsensor-MS-2XX-180-VDC-130-VAC-1-NO-700-mA-10-W

Een 4mm diametraal gemagnetiseerd Neodymium-staafmagneet (hoogte 10mm en 1,1 kg houdkracht)  in het uiteinde van een draaischijf-60 triggert de reedsensor. Deze zijn o.a. verkrijgbaar bij :   www.supermagnete.nl    
Zie :      http://www.supermagnete.nl/S-04-10-N        
Let op: de meeste staafmagneten zijn axiaal gemagnetiseerd en zijn, afhankelijk van de opstelling van het reed-contact ten opzichte van de staafmagneet, ook geschikt.  

Als Pulsteller gebruik ik normale minitasters  37783.  Een veranderende Potmeter-weerstandswaarde zorgt voor een vleugelhoek-verstelling om de vereiste stuwrichting te blijven houden.  
