---
layout: "image"
title: "Fischertechnik Libelle in aanbouw 4      -onderaanzicht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle14.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39054
- /details8ef2.html
imported:
- "2019"
_4images_image_id: "39054"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39054 -->
Midden-rechts de centrale aandrijf-Powermotor M1 (50:1) waarmee de 4 vleugels middels grote tandwielen 40  (31022) tegelijk op- en -neer kunnen bewegen.   
Net als bij Festo hebben de voorvleugels een vaste fase-verschuiving ten opzichte van de achtervleugels
