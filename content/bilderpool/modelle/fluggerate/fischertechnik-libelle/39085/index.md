---
layout: "image"
title: "Libelle met Smartbird en Pijlstaartrog"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle45.jpg"
weight: "37"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39085
- /details5278.html
imported:
- "2019"
_4images_image_id: "39085"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39085 -->
Op de achtergrond zowel de Smartbird als de Pijlstaartrog.

Het werkingsprincipe / kinematik van resp. de Smartbird, de Pijlstaart-rog  én de BionicOpter van Festo heb ik nagebouwd met Fischertechnik. Ik heb elk model met 3 draden aan het plafond bevestigd. Voorts  uitgerust met de oude IR-afstandsbediening voor inschakeling van de centrale vleugelaandrijving (1), naar links- en naar rechts vliegen/zwemmen (2) en positionering van lijf- en staarthoogte (3).  Ik heb bewust de oude IR-afstandsbediening gebruikt ivm het hogere schakelbare vermogen én de automatische midden-positie terug-stel-mogelijkheid.  De nieuwe afstandbesturingen zijn software-matig beveiligd en schakelen bij iets hogere piek vermogens (te) snel uit.
