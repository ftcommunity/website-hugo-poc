---
layout: "image"
title: "Vleugels + 1mm Polycarbonaat-plaat + Koolstofvezel-staaf 4mm + Netkousen"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle29.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39069
- /details4c0c-2.html
imported:
- "2019"
_4images_image_id: "39069"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39069 -->
De 4 vleugels heb ik geknipt uit 1mm dik transparant Polycarbonaat-plaat. Dikker is niet goed meer knipbaar met een huishoudschaar en dunner heeft onvoldoende stijfheid. Deze plaat is bij Conrad verkrijgbaar onder artikelnummer 229803.  
Zie :    http://www.conrad.nl/ce/nl/product/229803/Polycarbonaat-plaat-400-mm-500-mm-1-mm

Netkousen uit de feestwinkel over de transparante  polycarbonaat-vleugels simuleren de verharde aderen-net-structuur die voor de stabiliteit van de libelle-vleugel zorgt.  

Koolstofvezelstaaf 4mm heb ik gebruikt om extra stijfheid te verkrijgen in de vleugels. Deze zijn verkrijgbaar bij Conrad onder :  http://www.conrad.nl/ce/nl/product/220467/Koolvezelstaafjesbuisjes-massief-500-mm-4-mm   

Aluminium- en kunststof stelringen met M4-schroefdraad zijn zeer praktisch en degelijk om assen, koolstof-vezelstaven of draadeinden vast te zetten of de geleiden.  Deze zijn o.a. verkrijgbaar bij FLEC Nederland te  Heerle (NB).  


Samenvattend overall-overzicht +  inzicht in de werking + ontwikkeling Robot-Libelle : 

http://www.festo.com/net/SupportPortal/Files/248133/Festo_BionicOpter_de.pdf

http://www.festo.com/rep/de_corp/assets/pdf/GEO_Infografik_Roboterlibelle.pdf

Youtube-link Fischertechnik Libelle :

https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1
