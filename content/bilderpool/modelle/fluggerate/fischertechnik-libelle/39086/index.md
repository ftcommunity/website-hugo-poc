---
layout: "image"
title: "Smartbird en Pijlstaartrog met Youtube-links"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle46.jpg"
weight: "38"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39086
- /details980f-3.html
imported:
- "2019"
_4images_image_id: "39086"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39086 -->
Op de achtergrond zowel de Smartbird als de Pijlstaartrog.

https://www.youtube.com/watch?v=RjhEi15VK-4&index=7&list=UUvBlHQzqD-ISw8MaTccrfOQ       = Smartbird Earthflight Fischertechnik

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2        = Pijlstaartrog Fischertechnik
