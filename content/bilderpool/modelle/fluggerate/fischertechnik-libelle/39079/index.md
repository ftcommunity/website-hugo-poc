---
layout: "image"
title: "Normale Amplitude-vleugel-uitslag"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle39.jpg"
weight: "31"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39079
- /details5b27.html
imported:
- "2019"
_4images_image_id: "39079"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39079 -->
Normale Amplitude-vleugel-uitslag bij rechtdoor vliegen
