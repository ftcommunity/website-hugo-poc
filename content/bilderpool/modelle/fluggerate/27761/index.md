---
layout: "image"
title: "Schiefe Verstrebungen 2"
date: "2010-07-16T11:13:49"
picture: "IMG_3121_Winkelverbindung.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/27761
- /details6ed6.html
imported:
- "2019"
_4images_image_id: "27761"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:13:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27761 -->
