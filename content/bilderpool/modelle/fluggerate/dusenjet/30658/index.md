---
layout: "image"
title: "Düsenjet15"
date: "2011-05-29T12:09:36"
picture: "duesenjet15.jpg"
weight: "15"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30658
- /details0b52.html
imported:
- "2019"
_4images_image_id: "30658"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:36"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30658 -->
Fahrwerk vorne ausgefahren.
Das vordere Fahrwerk wird im Vergleich zum hinteren Fahrwerk nur mit einem Mini-Motor ein-und ausgefahren.
