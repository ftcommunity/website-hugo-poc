---
layout: "image"
title: "Düsenjet7"
date: "2011-05-29T12:09:22"
picture: "duesenjet07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30650
- /details97f1.html
imported:
- "2019"
_4images_image_id: "30650"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30650 -->
Das Hinterteil des Jets.