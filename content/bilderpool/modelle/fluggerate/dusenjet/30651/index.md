---
layout: "image"
title: "Düsenjet8"
date: "2011-05-29T12:09:22"
picture: "duesenjet08.jpg"
weight: "8"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- /php/details/30651
- /detailsd940.html
imported:
- "2019"
_4images_image_id: "30651"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:22"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30651 -->
Hier ist der Motor fuer das linke Ruder.
Jedes Ruder hat einen eigenen Motor.