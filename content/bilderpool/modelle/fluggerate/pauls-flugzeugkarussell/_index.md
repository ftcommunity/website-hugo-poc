---
layout: "overview"
title: "Pauls Flugzeugkarussell"
date: 2020-02-22T08:34:40+01:00
legacy_id:
- /php/categories/2729
- /categoriescfa6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2729 --> 
Paul, junger Sohn von Freunden, war zu Besuch und baute dieses Fliegerkarussell bis auf die Schleifringe ganz alleine. Das Modell durfte auch mit zur Convention 2012.