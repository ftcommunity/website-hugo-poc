---
layout: "image"
title: "Erlkönig09.jpg"
date: "2008-01-04T18:57:36"
picture: "EK009.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13269
- /details41c5.html
imported:
- "2019"
_4images_image_id: "13269"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13269 -->
Mit den Riegelsteinen 32850 kann man schöne Luken bauen, mit denen Schaltpulte (hier nicht zu sehen) und P-Ventile (vierte Luke, öffnet nach unten) herausgeklappt werden können.
