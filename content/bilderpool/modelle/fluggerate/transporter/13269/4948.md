---
layout: "comment"
hidden: true
title: "4948"
date: "2008-01-07T17:49:14"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Michael,

kann sein, kann auch nicht sein! Tatsache ist, dass ich eigentlich mit einer Transall als Vorlage angefangen habe (dann aber "passte" deren Fahrwerk nicht), dann eine Hercules C-130 ausgeguckt habe (da war aber auch nicht alles recht), dann von der C-5a Galaxy die Heckklappe genommen hab und vielleicht noch ein bisschen AN-124 vom letzten Mal mit drin steckt. Es ist also im Prinzip von allen etwas dabei, und der Rest hat noch Gelegenheit, sich in den Flügeln zu verewigen. Die gibt es nämlich noch gar nicht. Es ist noch nicht mal sicher, ob ich bis zum Stichtag 17.02. die Propeller fertig bekomme oder doch die bekannten Triebwerke dranmachen muss.