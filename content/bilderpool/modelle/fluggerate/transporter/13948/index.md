---
layout: "image"
title: "Landeklappe184.JPG"
date: "2008-03-19T11:21:04"
picture: "Landeklappe184.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13948
- /detailsec19.html
imported:
- "2019"
_4images_image_id: "13948"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:21:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13948 -->
Eine Landeklappe im eingefahrenen Zustand. Wenn man zwischen den Bildern "Landeklappe184" und "Landeklappe185" hin-und herwechselt, gibt das ein kleines Daumenkino.
