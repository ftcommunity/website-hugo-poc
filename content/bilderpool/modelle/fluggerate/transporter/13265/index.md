---
layout: "image"
title: "Erlkönig05.jpg"
date: "2008-01-04T18:36:16"
picture: "EK005.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13265
- /detailse5f2.html
imported:
- "2019"
_4images_image_id: "13265"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:36:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13265 -->
"Stellprobe" im Cockpit. Eigentlich sollen alle Funktionen mit IR und Robo-Int gesteuert werden, aber ohne Schalter macht ein Cockpit auch nicht viel her.
