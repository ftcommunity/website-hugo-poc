---
layout: "image"
title: "Landeklappe187.JPG"
date: "2008-03-19T11:22:54"
picture: "Landeklappe187.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13950
- /detailsbe0b.html
imported:
- "2019"
_4images_image_id: "13950"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:22:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13950 -->
Die Unterseite der Landeklappe. Der richtige Antriebsmechanismus wird noch gesucht.
