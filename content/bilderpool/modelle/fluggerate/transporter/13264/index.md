---
layout: "image"
title: "Erlkönig04.jpg"
date: "2008-01-04T18:33:31"
picture: "EK004.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/13264
- /detailsa59b.html
imported:
- "2019"
_4images_image_id: "13264"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:33:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13264 -->
Das Hauptfahrwerk ist ausgefahren. Der Radträger baumelt immer noch unkontrolliert herum (Gelenksteine auf dem längs liegenden BS30, dessen Zapfen zwischen den Rädern zu sehen ist), d.h. beim Einfahren muss ich mit der Hand nachhelfen und die Räder nach oben kippen.
