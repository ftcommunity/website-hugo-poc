---
layout: "image"
title: "Heck vom Hubschrauber"
date: "2005-08-26T17:19:30"
picture: "motorisierte_Roboter_077.jpg"
weight: "3"
konstrukteure: 
- "Marius (mari)"
fotografen:
- "Marius (mari)"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4653
- /details9709.html
imported:
- "2019"
_4images_image_id: "4653"
_4images_cat_id: "583"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T17:19:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4653 -->
