---
layout: "image"
title: "Segler12.JPG"
date: "2006-08-15T15:37:58"
picture: "Segler12.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6685
- /details25cf.html
imported:
- "2019"
_4images_image_id: "6685"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:37:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6685 -->
