---
layout: "image"
title: "Chipmunk17.JPG"
date: "2006-09-17T20:58:45"
picture: "Chipmunk17.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6877
- /details5eeb.html
imported:
- "2019"
_4images_image_id: "6877"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:58:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6877 -->
