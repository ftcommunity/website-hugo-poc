---
layout: "image"
title: "Chipmunk03.JPG"
date: "2006-09-17T20:53:46"
picture: "Chipmunk03.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6872
- /detailsf8ad.html
imported:
- "2019"
_4images_image_id: "6872"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:53:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6872 -->
