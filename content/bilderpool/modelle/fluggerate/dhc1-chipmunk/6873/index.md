---
layout: "image"
title: "Chipmunk04.JPG"
date: "2006-09-17T20:55:08"
picture: "Chipmunk04.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6873
- /details5a19-2.html
imported:
- "2019"
_4images_image_id: "6873"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:55:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6873 -->
