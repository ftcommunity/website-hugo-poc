---
layout: "image"
title: "Chipmunk13.JPG"
date: "2006-09-17T20:57:37"
picture: "Chipmunk13.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6875
- /detailsa7e2.html
imported:
- "2019"
_4images_image_id: "6875"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:57:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6875 -->
Das Fahrwerk ist draußen --- und es ist ein ziemlich wackeliges. Ein gescheiter Endanschlag und/oder Verriegelung fehlt da noch.
