---
layout: "image"
title: "FWH13_03.JPG"
date: "2006-03-12T13:20:40"
picture: "FWH13_03.JPG"
weight: "51"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5864
- /details83a9.html
imported:
- "2019"
_4images_image_id: "5864"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:20:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5864 -->
