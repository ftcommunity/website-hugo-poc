---
layout: "image"
title: "FWL05S_03.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL05S_03.jpg"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4377
- /detailsdd51.html
imported:
- "2019"
_4images_image_id: "4377"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4377 -->
