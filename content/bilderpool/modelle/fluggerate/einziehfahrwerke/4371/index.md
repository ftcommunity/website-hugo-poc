---
layout: "image"
title: "FWL05D_01.JPG"
date: "2005-06-08T11:23:04"
picture: "FWL05D_01.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Fahrwerk", "Kniegelenk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4371
- /details36bd.html
imported:
- "2019"
_4images_image_id: "4371"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4371 -->
Fahrwerk (leicht) Nummer 5 gibt es als 'Double'- (2 Räder) und als 'Single'-Variante (1 Rad).

Die Knie-Knickmechanik ist hier mit Lenkwürfel, Lenkklaue, Verbinder 15 und Bauplatte 15x15 realisiert.
