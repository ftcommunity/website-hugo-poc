---
layout: "image"
title: "FWL08_03.JPG"
date: "2006-03-12T13:16:37"
picture: "FWL08_03.JPG"
weight: "48"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5861
- /details5287-2.html
imported:
- "2019"
_4images_image_id: "5861"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:16:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5861 -->
