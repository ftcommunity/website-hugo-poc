---
layout: "image"
title: "FWL06_02.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL06_02.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4379
- /details42e3.html
imported:
- "2019"
_4images_image_id: "4379"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4379 -->
Nummer 6 halb ausgefahren. Auf den BS7,5 (in dem die Kurbel gelagert ist) kommt noch der Landescheinwerfer drauf.