---
layout: "image"
title: "FWH12_04.JPG"
date: "2005-06-08T22:02:08"
picture: "FWH12_04.jpg"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4385
- /detailsf00b.html
imported:
- "2019"
_4images_image_id: "4385"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4385 -->
und jetzt eingerastet. Mit Motor wirkt das Ausfahren richtig majestätisch :-)
