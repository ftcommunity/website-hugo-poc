---
layout: "image"
title: "FWL07_04.JPG"
date: "2005-08-21T11:14:36"
picture: "FWL07_04.jpg"
weight: "42"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4613
- /details5b51.html
imported:
- "2019"
_4images_image_id: "4613"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-08-21T11:14:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4613 -->
