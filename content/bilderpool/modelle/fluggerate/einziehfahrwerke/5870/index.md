---
layout: "image"
title: "FWH13_38.JPG"
date: "2006-03-12T13:29:47"
picture: "FWH13_38.JPG"
weight: "57"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5870
- /detailsb215.html
imported:
- "2019"
_4images_image_id: "5870"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:29:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5870 -->
Geschafft!

Das Fahrwerk ist ausgefahren und verriegelt: der Hebel ist über den Knickpunkt hinaus gestreckt.
