---
layout: "image"
title: "FWH11_01.JPG"
date: "2005-06-08T22:02:08"
picture: "FWH11_01.jpg"
weight: "36"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4387
- /details4305.html
imported:
- "2019"
_4images_image_id: "4387"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4387 -->
Nummer 11 (heavy) ganz ausgefahren. Das Teil ist zwar nicht so kompakt wie Nr. 12, dafür kann man es aber besser in einen Rumpf einbauen. Nr. 12 ist eher für Gondeln unter dem Flügel geeignet. 

Trotzdem müßte dieser Flugzeugrumpf schon ziemlich mächtig sein...
