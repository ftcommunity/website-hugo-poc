---
layout: "image"
title: "FWL05D_02.JPG"
date: "2005-06-08T11:23:04"
picture: "FWL05D_02.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4372
- /details9eca-2.html
imported:
- "2019"
_4images_image_id: "4372"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4372 -->
Fahrwerk eingezogen. Das Bein ist leicht abgewinkelt, weil die Flügel bei Tiefdeckern nach außen hin ansteigen, das Rad aber trotzdem vertikal auf dem Boden stehen soll. Außerdem passen so die Räder in die Statik hinein und um das Z15 herum.