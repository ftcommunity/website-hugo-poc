---
layout: "image"
title: "Luftkissen 2"
date: "2007-02-18T20:57:36"
picture: "2.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["Luftkissen", "fliegen"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9054
- /details089b.html
imported:
- "2019"
_4images_image_id: "9054"
_4images_cat_id: "825"
_4images_user_id: "34"
_4images_image_date: "2007-02-18T20:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9054 -->
