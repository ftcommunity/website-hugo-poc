---
layout: "comment"
hidden: true
title: "2436"
date: "2007-02-19T22:59:20"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Wenns auf Land schwebt dann auch auf Wasser. Warum sollte es nur auf Land schweben?