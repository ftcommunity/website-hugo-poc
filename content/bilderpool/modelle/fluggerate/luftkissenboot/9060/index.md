---
layout: "image"
title: "Luftkissen 12"
date: "2007-02-18T20:57:36"
picture: "12.jpg"
weight: "8"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9060
- /detailse5ec.html
imported:
- "2019"
_4images_image_id: "9060"
_4images_cat_id: "825"
_4images_user_id: "34"
_4images_image_date: "2007-02-18T20:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9060 -->
