---
layout: "image"
title: "Tür"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk5.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/40584
- /details1846.html
imported:
- "2019"
_4images_image_id: "40584"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40584 -->
Tür geöffnet.
Rechts unten (an der Tür) kann man den Magnet erahnen.
Außerdem sieht man die Positionslichter.