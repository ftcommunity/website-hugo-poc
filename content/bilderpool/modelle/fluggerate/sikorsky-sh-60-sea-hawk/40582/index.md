---
layout: "image"
title: "Größenvergleich"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk3.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/40582
- /details4177.html
imported:
- "2019"
_4images_image_id: "40582"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40582 -->
Größenvergleich mit Kater Micky.