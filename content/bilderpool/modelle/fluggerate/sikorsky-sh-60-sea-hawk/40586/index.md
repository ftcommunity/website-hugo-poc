---
layout: "image"
title: "Heckrotor"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk7.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/40586
- /detailsd2a8.html
imported:
- "2019"
_4images_image_id: "40586"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40586 -->
Hier sieht man den Heckrotor. 
Er wird direkt von einem Minimotor angetrieben.
Darüber ist ein Positionslicht angeordnet.