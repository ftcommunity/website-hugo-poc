---
layout: "image"
title: "1  Fahrwerk"
date: "2009-09-27T23:59:11"
picture: "fahrwerkfuermodellflugzeug1.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25371
- /details7dc8.html
imported:
- "2019"
_4images_image_id: "25371"
_4images_cat_id: "1776"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25371 -->
Vor ein paar Tagen kam mir die Idee für meine 1m Piper ein Fahrwerk zu bauen. Da war zwar ein umgekehrtes Federstahldraht-U mit Rädern und einem Spornrad hinten dran dabei, das war aber nicht sonderlich stabil und die Räder waren auch viel zu klein.
Also habe ich eins aus fischertechnik gebaut :)
Ich habe mich dann auch für ein Bugrad entschieden, ich persönlich finde sowas schöner.