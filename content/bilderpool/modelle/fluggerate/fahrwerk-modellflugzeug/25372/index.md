---
layout: "image"
title: "2  angeklebtes Fahrwerk"
date: "2009-09-27T23:59:12"
picture: "fahrwerkfuermodellflugzeug2.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/25372
- /detailsfd9e.html
imported:
- "2019"
_4images_image_id: "25372"
_4images_cat_id: "1776"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25372 -->
Die grauen Sortierwannentrennwände habe ich mit Heißklebe am Rest des Fahrwerks unten dran geklebt. Das wird dann einfach mit ein paar Streifen Tesafilm an der Unterseite des Fliegers befestigt.