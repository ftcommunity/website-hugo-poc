---
layout: "image"
title: "Wing Landing Gear"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk7.jpg"
weight: "7"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/17414
- /detailsecf0.html
imported:
- "2019"
_4images_image_id: "17414"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17414 -->
Die Kinematik ist aus Platzgründen nicht mit Schneckengetriebe o.ä. umsetzbar. Ich habe Schnüre verwendet. Auch die Verrasterung muss durch eine Schnur entkoppelt werden. Die genaue Steuerung habe ich aber nicht aufgebaut.
