---
layout: "image"
title: "Wing Landing Gear ausgefahren"
date: "2009-03-06T21:42:10"
picture: "Wing_Landing_Gear_ausgefahren.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/23399
- /detailsef1c.html
imported:
- "2019"
_4images_image_id: "23399"
_4images_cat_id: "1587"
_4images_user_id: "107"
_4images_image_date: "2009-03-06T21:42:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23399 -->
Eine absolut sichere und stabile Konstruktion.
