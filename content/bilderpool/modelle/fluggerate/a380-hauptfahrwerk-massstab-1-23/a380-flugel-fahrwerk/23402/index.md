---
layout: "image"
title: "Wing Landing Gear wird ausgefahren_2"
date: "2009-03-06T21:42:10"
picture: "Wing_Landing_Gear_wird_ausgefahren_2.jpg"
weight: "4"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/23402
- /detailsb290.html
imported:
- "2019"
_4images_image_id: "23402"
_4images_cat_id: "1587"
_4images_user_id: "107"
_4images_image_date: "2009-03-06T21:42:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23402 -->
Die Drehachse des Fahrwerkes ist in zwei Ebenen angewinkelt. Dies ist einerseits durch das Flügelprofil und dem Flügelspitzenwinkel an dieser Stelle notwendig. Desweiteren ist zu vermuten, dass der Kraftangriffspunkt deren -weiterleitung auf die Fahrbahn und umgekehrt nicht beliebig gewählt werden kann. Andererseits wirken beim Starten und Landen enorme Kräfte auf das gesamte Fahrwerk. Durch den Anstellwinkel des Flugzeugs müssen die Kräfte im Fahrwerk möglichst momentenfrei weitergeleitet werden. Achtet mal bei einem stehenden Flugzeug auf das leicht geneigte Fahrwerk.
