---
layout: "overview"
title: "A380 Hauptfahrwerk - Maßstab 1:23"
date: 2020-02-22T08:34:13+01:00
legacy_id:
- /php/categories/1566
- /categoriesd35d.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1566 --> 
In diesem Modell wollte ich mich mit dem Maßkonzept, Funktion und Kinematik des Hauptfahrwerkes A380 auseinandersetzen. Allein die Kinematik ist nicht nur sehr interessant sondern für ein Modellbauer sehr anspruchsvoll. Im Internet finden sich nur sehr sehr wenige Informationen und Fotos. Aber die wenigen plus der im Handel erhältlichen DVD \"Airbus A380  History - Technology - Testing\"  haben es mir ermöglicht das Hauptfahrwerk im Maßstab 1:23 zu bauen. Gerne hätte ich die Flügelwurzel und die Tragflächen bis zur ersten Turbine mit Landklappen gebaut, aber der unvorstellbar große Zeitaufwand, Platzbedarf und die fehlenden Informationen der geometrischen Abmessungen im Flügelwurzel- und Traglächenbereich haben mich doch dazu bewogen es bei diesem Bauzustand zu belassen. Vielleicht werde ich später noch ein mal weiterarbeiten ...