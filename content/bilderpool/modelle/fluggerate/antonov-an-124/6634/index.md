---
layout: "image"
title: "Antonov143.JPG"
date: "2006-07-14T18:05:07"
picture: "Antonov143.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6634
- /detailsa405.html
imported:
- "2019"
_4images_image_id: "6634"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:05:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6634 -->
In den Zahnkranz des Reifen 45 greift ein  Kegelzahnrad schräg von rechts unten.
