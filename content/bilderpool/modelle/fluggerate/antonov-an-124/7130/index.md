---
layout: "image"
title: "AN124_164.JPG"
date: "2006-10-03T14:36:39"
picture: "AN124_164.JPG"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7130
- /detailsa676.html
imported:
- "2019"
_4images_image_id: "7130"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:36:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7130 -->
Die drei Stecker unten gehören zum oberen Endanschlag für das Bugfahrwerk (einen unteren Anschlag gibt es noch nicht, dafür hat der Antrieb eine Rutschkupplung).

Die Leiter links führt nach oben, hinter das Cockpit. Endlich mal eine Anwendung für die Schalthebel 31994!

Der schwarze Minitaster (mit grünem und orangem Stecker) ist der untere Endanschlag fürs Hauptfahrwerk. Direkt dahinter (aber verdeckt) sitzt noch ein grauer Minitaster, der als oberer Endanschlag funktioniert.
