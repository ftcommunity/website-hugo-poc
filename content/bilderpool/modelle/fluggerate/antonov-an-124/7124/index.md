---
layout: "image"
title: "AN124_149.JPG"
date: "2006-10-03T14:10:21"
picture: "AN124_149.JPG"
weight: "25"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7124
- /detailsbcea.html
imported:
- "2019"
_4images_image_id: "7124"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:10:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7124 -->
Die Seilwinde am Kran ist gegenüber dem "Erlkönig" geändert worden. Die Achse ist in halbierten (ft-teile-gemoddeten ;-) ) BS7,5 gelagert und hat eine Querbohrung, in der das Seil verknotet ist.
