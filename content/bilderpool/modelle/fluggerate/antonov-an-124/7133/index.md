---
layout: "image"
title: "AN124_173.JPG"
date: "2006-10-03T14:49:40"
picture: "AN124_173.JPG"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7133
- /details318e-2.html
imported:
- "2019"
_4images_image_id: "7133"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:49:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7133 -->
(Fast-) Gesamtansicht von oben.
