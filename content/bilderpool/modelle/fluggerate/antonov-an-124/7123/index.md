---
layout: "image"
title: "AN124_139.JPG"
date: "2006-10-03T14:08:01"
picture: "AN124_139.JPG"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7123
- /details4e83-3.html
imported:
- "2019"
_4images_image_id: "7123"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:08:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7123 -->
Blick von hinten auf den Antrieb der Heckrampe und was sonst noch alles drinnen steckt. Die Energiekette für den Kran hat sich aus der Halterung gelöst und baumelt jetzt senkrecht herum.
