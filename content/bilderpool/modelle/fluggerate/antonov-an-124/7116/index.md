---
layout: "image"
title: "AN124_114.JPG"
date: "2006-10-03T13:27:58"
picture: "AN124_114.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7116
- /detailsde5b-2.html
imported:
- "2019"
_4images_image_id: "7116"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:27:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7116 -->
Blick von vorne links aufs linke Hauptfahrwerk. Die Schnecke hebt und senkt das Fahrwerk. Der weiße Schalter oben steuert die Landescheinwerfer (in der Nase).

Links von oben nach unten: die Antriebsachse für das Alternativ-Hubgetriebe (um den Rollenbock links unten herum; siehe http://www.ftcommunity.de/categories.php?cat_id=649), das die Bugrampe ein- und ausfährt.
