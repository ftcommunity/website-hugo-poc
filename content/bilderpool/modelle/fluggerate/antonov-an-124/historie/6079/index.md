---
layout: "image"
title: "Erlkoenig01.JPG"
date: "2006-04-12T20:41:36"
picture: "Erlkoenig01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6079
- /details1d24.html
imported:
- "2019"
_4images_image_id: "6079"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-04-12T20:41:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6079 -->
Ich habe da Gerüchte über eine angebliche "Ideenpause" gehört. 

Einspruch!!! 

Manchmal versucht man halt, ein "U-Boot"-Projekt zu lancieren und erst im letzten Moment damit aufzutauchen. Bei der A-340 hat's geklappt, aber diesmal muss ich wohl doch den Schleier etwas früher lüften.


"Erlkönig" ist Fachjargon für ein Automodell, das noch in geheimer Erprobung ist und trotzdem jemandem vor die Kamera geriet. Das hier ist  kein Auto, aber der Begriff passt trotzdem.
