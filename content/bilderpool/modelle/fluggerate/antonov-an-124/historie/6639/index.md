---
layout: "image"
title: "Antonov023.JPG"
date: "2006-07-14T18:13:58"
picture: "Antonov023.jpeg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6639
- /details5bc3-2.html
imported:
- "2019"
_4images_image_id: "6639"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:13:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6639 -->
Das "alte" Bugfahrwerk, das mittlerweile durch etwas kleineres ersetzt wurde. Die Geometrie hat nicht 100%ig hingehauen: entweder die Räder "schielen" im ausgefahrenen Zustand (wie hier zu sehen), oder das Fahrwerk lässt sich nicht vollständig einziehen.
