---
layout: "image"
title: "Antonov222.JPG"
date: "2006-07-14T18:27:22"
picture: "Antonov222.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6643
- /details5411.html
imported:
- "2019"
_4images_image_id: "6643"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:27:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6643 -->
Die wesentlichen Teile vom Flugzeugbug:
Laderampe (hier oben und ausgeklappt), Kranschlitten im vorderen Anschlag.

Die Grundplatte unter dem Kran ist der Boden der Pilotenkanzel. Links und rechts davon, erkennbar an den schwarzen I-30-Loch, die Joysticks für Pilot und Copilot (bestehend aus zwei Minitastern, die sich gegenüberstehen, und einer dazwischen elastisch gelagerten Achse).
