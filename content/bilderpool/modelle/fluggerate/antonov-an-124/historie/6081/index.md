---
layout: "image"
title: "Erlkoenig03.JPG"
date: "2006-04-12T20:47:25"
picture: "Erlkoenig03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6081
- /detailsfbc9-2.html
imported:
- "2019"
_4images_image_id: "6081"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-04-12T20:47:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6081 -->
Weiterhin ist es so, dass man auf Erlkönig-Fotos sehr selten die wichtigen Details erkennen kann. Das ist hier auch so. Schade, schade!
