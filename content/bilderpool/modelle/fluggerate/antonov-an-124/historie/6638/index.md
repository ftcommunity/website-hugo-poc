---
layout: "image"
title: "Antonov118.JPG"
date: "2006-07-14T18:11:01"
picture: "Antonov118.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6638
- /details26f1.html
imported:
- "2019"
_4images_image_id: "6638"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:11:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6638 -->
Die erste Ausführung des Fahrwerks hatte nur 4 Räder pro Seite. Damit wäre aus dem Modell eher eine Hercules C-130 geworden.
