---
layout: "image"
title: "AN124_133.JPG"
date: "2006-10-03T14:03:44"
picture: "AN124_133.JPG"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7122
- /detailsc573-2.html
imported:
- "2019"
_4images_image_id: "7122"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:03:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7122 -->
Das Querruder nebst Antrieb. Das kleine ft-Kegelrad passt sauber in den Kranz des ft-Rad 45 hinein.

Im Antrieb war noch eine Richtungsumkehr nötig: im Winkelantrieb mit 2 Kegelzahnrädern sitzt eins "rückwärts" auf der Achse. Den Trick habe ich bei Claus-Werner abgeguckt.

Der Übergang von der vertikalen Antriebsachse zum Querruder wird durch eine Seilwindenbremse 31999 bewerkstelligt. Dieses Teil passt genau zwischen die Bauplatten, die das Ruder darstellen.

Das untere Scharnier des Querruders besteht aus Gelenkwürfelklaue 31436, Lagerhülse 36819 und einem Reedkontakthalter 35969. Darin hat die Achse bessere Führung als in einem Gelenkstein.

Die Dreiecksflächen für das Seitenleitwerk sind durch grobschlächtiges Durchsägen von Statik-Bauplatten 90x180 und 90x90 entstanden. Sorry Thomas (thkais) - feiner habe ich die Schnitte nicht hingekriegt.
