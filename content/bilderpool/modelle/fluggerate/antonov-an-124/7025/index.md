---
layout: "image"
title: "AN124_32.JPG"
date: "2006-10-01T11:53:26"
picture: "AN124_32.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7025
- /detailsb30e-2.html
imported:
- "2019"
_4images_image_id: "7025"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-01T11:53:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7025 -->
Spannweite: 1,25 m
Gesamtlänge: 1,06 m
Gesamtgewicht: 8,2 kg

Verbaut sind 16 Motoren:
4 x Triebwerke
1 x Hauptfahrwerk
1 x Bugfahrwerk
1 x Bugnase heben
1 x Bugrampe
1 x Heckluken öffnen
1 x Heckrampe
2 x Kran (Laufkatze, Seilwinde)
1 x Landeklappen
1 x Querruder + Seitenruder
1 x Höhenruder
1 x Kompressor (eingebaut, aber nicht genutzt. der einzige P-Zylinder könnte das Mittelteil der Heckluke öffnen und schließen, ist aber nicht angeschlossen. Die Druckspeicher in der Nase sind ohne Funktion)
