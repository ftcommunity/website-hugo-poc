---
layout: "image"
title: "AN124_161.JPG"
date: "2006-10-03T14:31:21"
picture: "AN124_161.JPG"
weight: "30"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7129
- /details0ee1.html
imported:
- "2019"
_4images_image_id: "7129"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T14:31:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7129 -->
Blick von unten unter den Cockpitboden. Die grauen Stecker stecken in aufrecht stehenden Minitastern, die vom Copiloten benutzt werden, um die Landeklappen zu betätigten. Der Pilot hat auf der anderen Seite das gleiche nochmal, und fährt damit das Fahrwerk ein und aus.

Der violette Stecker und das gelb-grüne Kabel am oberen Bildrand gehören zum linken Landescheinwerfer (mit schwarzem Sockel.

Der BS7,5 mitte rechts dient als vorderer Endanschlag für den Kran.
