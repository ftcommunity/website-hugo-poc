---
layout: "image"
title: "FT_Star Wars_Millenium Falke"
date: "2016-10-19T16:58:21"
picture: "fischertechnikstarwarsmilleniumfalke19.jpg"
weight: "19"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44644
- /details24d7.html
imported:
- "2019"
_4images_image_id: "44644"
_4images_cat_id: "3322"
_4images_user_id: "1688"
_4images_image_date: "2016-10-19T16:58:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44644 -->
