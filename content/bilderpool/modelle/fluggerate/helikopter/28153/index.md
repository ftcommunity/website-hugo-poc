---
layout: "image"
title: "Rundum13"
date: "2010-09-14T20:16:06"
picture: "rundumblick13.jpg"
weight: "13"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28153
- /details030c.html
imported:
- "2019"
_4images_image_id: "28153"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28153 -->
Ansicht 13