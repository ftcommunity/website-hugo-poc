---
layout: "image"
title: "Blick in das Cockpit1"
date: "2010-09-18T13:36:54"
picture: "DSCF0397.jpg"
weight: "24"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28181
- /detailsfeb5.html
imported:
- "2019"
_4images_image_id: "28181"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28181 -->
Blick zum Steuerpult des Piloten