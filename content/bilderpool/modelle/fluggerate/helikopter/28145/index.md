---
layout: "image"
title: "Rundum05"
date: "2010-09-14T20:16:06"
picture: "rundumblick05.jpg"
weight: "5"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28145
- /details9e82.html
imported:
- "2019"
_4images_image_id: "28145"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28145 -->
Ansicht 5