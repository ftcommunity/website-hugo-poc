---
layout: "image"
title: "Von oben1"
date: "2010-09-14T20:16:06"
picture: "rundumblick16.jpg"
weight: "16"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28156
- /detailsc001.html
imported:
- "2019"
_4images_image_id: "28156"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28156 -->
Ansicht 16