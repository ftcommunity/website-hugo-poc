---
layout: "image"
title: "Von der Seite2"
date: "2010-09-14T20:16:06"
picture: "rundumblick21.jpg"
weight: "21"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28161
- /detailsa90e.html
imported:
- "2019"
_4images_image_id: "28161"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28161 -->
Helikopter von der Seite mit geöffneter Türe