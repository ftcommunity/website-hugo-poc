---
layout: "image"
title: "Rundum02"
date: "2010-09-14T20:16:06"
picture: "rundumblick02.jpg"
weight: "2"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28142
- /details6db2.html
imported:
- "2019"
_4images_image_id: "28142"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28142 -->
Ansicht 2