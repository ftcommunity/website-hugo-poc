---
layout: "image"
title: "Rundum07"
date: "2010-09-14T20:16:06"
picture: "rundumblick07.jpg"
weight: "7"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28147
- /detailsfda7.html
imported:
- "2019"
_4images_image_id: "28147"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28147 -->
Ansicht 7