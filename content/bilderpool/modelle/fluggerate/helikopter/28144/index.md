---
layout: "image"
title: "Rundum04"
date: "2010-09-14T20:16:06"
picture: "rundumblick04.jpg"
weight: "4"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28144
- /details40ce.html
imported:
- "2019"
_4images_image_id: "28144"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28144 -->
Ansicht 4