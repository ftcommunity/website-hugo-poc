---
layout: "image"
title: "Von der Seite1"
date: "2010-09-14T20:16:06"
picture: "rundumblick20.jpg"
weight: "20"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28160
- /details0f92.html
imported:
- "2019"
_4images_image_id: "28160"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28160 -->
Helikopter von der Seite mit geschlossener Türe