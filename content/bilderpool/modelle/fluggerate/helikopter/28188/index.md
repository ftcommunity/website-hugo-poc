---
layout: "image"
title: "Steuermechanismus1"
date: "2010-09-18T13:36:54"
picture: "DSCF0407.jpg"
weight: "31"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- /php/details/28188
- /details1b0b.html
imported:
- "2019"
_4images_image_id: "28188"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28188 -->
Kippmechanismus am Hauptrotor zum Steuern von "vorwärts", "rückwärts", "rechts neigen" und "links neigen"