---
layout: "image"
title: "Fußpedale von unten (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale6.jpg"
weight: "6"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27034
- /details3e77.html
imported:
- "2019"
_4images_image_id: "27034"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27034 -->
Hier sieht man das Gummi-Transportband, damit die Platte auf dem Parkettboden nicht rutschen kann. Das Potentiometer erkennt man rechts.