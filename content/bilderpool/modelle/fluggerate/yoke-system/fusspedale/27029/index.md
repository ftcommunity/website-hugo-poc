---
layout: "image"
title: "Fußpedale Seitengesamtansicht (Yoke System)"
date: "2010-05-02T11:16:08"
picture: "fusspedale1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/27029
- /details46aa.html
imported:
- "2019"
_4images_image_id: "27029"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27029 -->
Es ist ganz schön schwierig, die Konstruktion so zu bauen, dass man es mit den Füßen bedienen kann. Mir ist nun eine robuste Konstruktion gelungen. Lediglich der Hackenrutschschutz ist etwas biegsam.