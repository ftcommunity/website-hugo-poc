---
layout: "image"
title: "'Lenkrad' Seitenansicht"
date: "2010-04-08T17:38:29"
picture: "yokesystem4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26918
- /details3038.html
imported:
- "2019"
_4images_image_id: "26918"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26918 -->
