---
layout: "image"
title: "Anschlussbox für Yoke System"
date: "2010-04-08T17:38:29"
picture: "yokesystem8.jpg"
weight: "8"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26922
- /details7ef7.html
imported:
- "2019"
_4images_image_id: "26922"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26922 -->
Das war mal ein wireless Gamepad, nun optimal für FT einsetzbar