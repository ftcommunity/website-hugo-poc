---
layout: "image"
title: "Yoke System"
date: "2010-04-08T17:38:28"
picture: "yokesystem1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- /php/details/26915
- /details2623-2.html
imported:
- "2019"
_4images_image_id: "26915"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26915 -->
Hier sieht man meinen Sohn im Anflug auf den Eifelturm.