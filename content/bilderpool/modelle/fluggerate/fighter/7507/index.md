---
layout: "image"
title: "Fighter-70.JPG"
date: "2006-11-19T20:03:11"
picture: "Fighter-70.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7507
- /detailsd305-2.html
imported:
- "2019"
_4images_image_id: "7507"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T20:03:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7507 -->
Das Hauptfahrwerk auf halbem Wege. Auf der rechten Seite ist der abgeschnittene S-Riegel 8 zu erkennen, der das Gelenk im ausgefahrenen Zustand begrenzt.

Dummerweise steht der Block um die beiden Z20 herum mitten im Innenraum und damit immer im Weg.

Die Bilder 69, 70, 71 ergeben in Folge betrachtet die Sequenz beim Ein- und Ausfahren (à la Daumenkino).
