---
layout: "image"
title: "Fighter-22.JPG"
date: "2006-11-19T19:41:14"
picture: "Fighter-22.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7500
- /detailsad1a-2.html
imported:
- "2019"
_4images_image_id: "7500"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:41:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7500 -->
Blick von vorne unten, Fahrwerke sind eingezogen und die vordere Luke ist zu.
