---
layout: "image"
title: "Fighter-03.JPG"
date: "2006-11-19T19:40:16"
picture: "Fighter-03.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7499
- /detailsa224.html
imported:
- "2019"
_4images_image_id: "7499"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:40:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7499 -->
