---
layout: "image"
title: "Fighter-76.JPG"
date: "2006-11-19T20:05:15"
picture: "Fighter-76.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7509
- /detailsadc5.html
imported:
- "2019"
_4images_image_id: "7509"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T20:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7509 -->
Ein Fahrwerksbein von vorn gesehen.
