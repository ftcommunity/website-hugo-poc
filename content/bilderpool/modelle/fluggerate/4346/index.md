---
layout: "image"
title: "Solar-Hubschrauber"
date: "2004-09-29T19:02:19"
picture: "Ultra01.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Solar", "Hubschrauber"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/4346
- /detailsece5.html
imported:
- "2019"
_4images_image_id: "4346"
_4images_cat_id: "359"
_4images_user_id: "34"
_4images_image_date: "2004-09-29T19:02:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4346 -->
Ein bißchen Sonnenlicht, und schon hebt er ab.

Das Rotorblatt ist fest mit dem Motorgehäuse verbunden. Somit dreht sich der Motor mit; die Welle ist starr am Rumpf des Hubschraubers befestigt.
