---
layout: "image"
title: "Fallschirmspringer"
date: "2007-03-28T09:08:54"
picture: "121_2124.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9831
- /details2545.html
imported:
- "2019"
_4images_image_id: "9831"
_4images_cat_id: "885"
_4images_user_id: "34"
_4images_image_date: "2007-03-28T09:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9831 -->
Hier ist einfach ein ft-Mänchen, eine Mülltüte, Faden und ein Gummiband.
Mit dem Gummi wird das Mänchen befestigt.
Fallschirm in der Mitte nach oben ziehen, drei mal falten und das ft-Mänchen dranhalten. So nach oben werfen.
