---
layout: "image"
title: "Hubschrauber von 1975"
date: "2009-07-03T09:11:46"
picture: "Hubschrauber_1976.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/24491
- /details8ff0.html
imported:
- "2019"
_4images_image_id: "24491"
_4images_cat_id: "359"
_4images_user_id: "968"
_4images_image_date: "2009-07-03T09:11:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24491 -->
