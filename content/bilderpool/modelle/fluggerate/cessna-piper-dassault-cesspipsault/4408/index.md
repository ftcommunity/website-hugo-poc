---
layout: "image"
title: "Cesspipsault-55.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-55.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4408
- /detailsbb22.html
imported:
- "2019"
_4images_image_id: "4408"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4408 -->
Die Innereien der Flugzeugnase. Das "Hufeisen" aus Flachträgern 120 stützt sich nur durch Berührung auf dem Winkel 30° vorne ab.
