---
layout: "image"
title: "Cesspipsault-65.JPG"
date: "2005-06-09T23:01:53"
picture: "Cesspipsault-65.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4396
- /details6a38.html
imported:
- "2019"
_4images_image_id: "4396"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4396 -->
Die Achse vorn mitten betätigt das komplette Fahrwerk. Links unter dem Pilotensitz (im Bild also rechts) das Z10 fürs linke Höhenruder.
