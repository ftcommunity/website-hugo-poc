---
layout: "image"
title: "Cesspipsault-70.JPG"
date: "2005-06-09T23:01:53"
picture: "Cesspipsault-70.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4398
- /details80da.html
imported:
- "2019"
_4images_image_id: "4398"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4398 -->
Die drei Schalter unter der 2. Klasse sind für Landescheinwerfer+Innenbeleuchtung, Motoren und Positionslichter. Die Lampenfassung ist für die Stromversorgung am Boden da.

Ins Heck würde ein ft-Akku locker hineinpassen, allerdings liegt dann der Schwerpunkt zu weit hinten und der Flieger kippt nach hinten.
