---
layout: "image"
title: "Cesspipsault-47.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-47.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["35072"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4405
- /detailsc41d-3.html
imported:
- "2019"
_4images_image_id: "4405"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4405 -->
Mitten im Bild die Seilwinde mit Ratsch-Griff für die Fahrwerksabspannung. Es ist Absicht, dass das Seil eine volle Schleife macht (d.h. der Kraftfluss ist geschlossen), so dass die Zelle unter Last nicht verbogen/verspannt wird.

Die beiden dicken Schnecken betätigen die Höhenruder links und rechts; die zugehörigen Achsen verlaufen nach vorn bis unters Cockpit. 

In der Mitte zwischen diesen Schnecken sitzt noch eine dritte (hier vom Flachträger verdeckt), die das Hauptfahrwerk über ein Z20 ein- und ausfährt.
