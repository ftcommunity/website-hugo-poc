---
layout: "image"
title: "Cesspipsault-53.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-53.jpg"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4407
- /details517f.html
imported:
- "2019"
_4images_image_id: "4407"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4407 -->
Blick in die 2. Klasse und auf die Innenbeleuchtung. 

Die Antriebswellen für die Leitwerke im Heck sind mittlerweile bis ins Cockpit hinein verlängert (die Bandscheiben des Copiloten bedanken sich sehr :-) ).
