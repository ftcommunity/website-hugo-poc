---
layout: "image"
title: "Cesspipsault-64.JPG"
date: "2005-06-09T23:01:52"
picture: "Cesspipsault-64.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4395
- /details9d8f.html
imported:
- "2019"
_4images_image_id: "4395"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4395 -->
Das gute Stück im Reiseflug. Der Copilot muss die hinteren Ruder betätigen, dazu muss er sich allerdings im Sitz umdrehen, damit er an die Achs-Enden hinter seiner rechten Schulter herankommt (die Gewerkschaft mault auch schon). Als Ausgleich darf er sich aber nach rechts unten bücken, um das Z10 für das rechte Höhenruder (unter seinem Sitz) zu betätigen.
