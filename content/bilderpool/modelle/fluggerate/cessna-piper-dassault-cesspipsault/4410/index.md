---
layout: "image"
title: "Cesspipsault-66.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-66.jpg"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4410
- /detailsf162.html
imported:
- "2019"
_4images_image_id: "4410"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4410 -->
Die Bauplatte 15x13 mit 2+2 Zapfen musste ein ganz klein wenig gestutzt werden.
