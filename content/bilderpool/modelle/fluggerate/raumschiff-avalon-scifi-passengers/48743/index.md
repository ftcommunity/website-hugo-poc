---
layout: "image"
title: "Die Avalon im Flug"
date: 2020-05-11T16:09:43+02:00
picture: "2020-01-13 Raumschiff _Avalon_ aus dem SciFi _Passengers_4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Raumschiff flog einige Wochen durch unser Wohnzimmer, äh, den Sternenhimmel, und drehte sich dabei wie das Original um die eigene Längsachse, wenn man es per IR-Fernsteuerung in Gang setzte.
