---
layout: "image"
title: "Der Drehantrieb"
date: 2020-05-11T16:09:42+02:00
picture: "2020-01-13 Raumschiff _Avalon_ aus dem SciFi _Passengers_5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Motor treibt schön langsam den Faden an, der sich um ein Z20 des Raumschiffs schlingt und es so zum Drehen bringt.
