---
layout: "image"
title: "Prototyp"
date: "2015-10-29T21:09:24"
picture: "P1040331.jpg"
weight: "1"
konstrukteure: 
- "- classified -"
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/42173
- /details925a.html
imported:
- "2019"
_4images_image_id: "42173"
_4images_cat_id: "3143"
_4images_user_id: "1557"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42173 -->
Hinter vorgehaltener Hand wird ja schon ein Weilchen gemunkelt, daß die fischerwerke in den Flugzeugbau einsteigen wollen. Es geht wohl um Militärtransporter für die Nato und ein erster Protoyp soll im Test sein. So recht glauben wollte ich das nicht, aber Ramstein ist ja nicht weit ...

Sorry für die Unschärfe. Er flog ziemlich schnell vorbei.