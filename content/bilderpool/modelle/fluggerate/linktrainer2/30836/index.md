---
layout: "image"
title: "indicator3"
date: "2011-06-10T09:06:27"
picture: "linktrainer06.jpg"
weight: "6"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30836
- /detailsbf69.html
imported:
- "2019"
_4images_image_id: "30836"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30836 -->
A box with a cut plane coverse 3 lens lights.
The plane is projected on the cardboard screen.
