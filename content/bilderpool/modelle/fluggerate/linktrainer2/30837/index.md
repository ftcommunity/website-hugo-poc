---
layout: "image"
title: "indicator4"
date: "2011-06-10T09:06:27"
picture: "linktrainer07.jpg"
weight: "7"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30837
- /details2e4d.html
imported:
- "2019"
_4images_image_id: "30837"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30837 -->
The 3 lens lights.