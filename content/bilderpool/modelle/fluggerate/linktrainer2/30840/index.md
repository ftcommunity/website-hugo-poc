---
layout: "image"
title: "bluebox2"
date: "2011-06-10T09:06:27"
picture: "linktrainer10.jpg"
weight: "10"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30840
- /details1ddf.html
imported:
- "2019"
_4images_image_id: "30840"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30840 -->
Here's how the bluebox can move about two axes.
