---
layout: "image"
title: "plotter1"
date: "2011-06-10T09:06:27"
picture: "linktrainer17.jpg"
weight: "17"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30847
- /details5957.html
imported:
- "2019"
_4images_image_id: "30847"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30847 -->
The destination plotter with photocell above.
The e-tec module is for the light below the flightmap to blink.
