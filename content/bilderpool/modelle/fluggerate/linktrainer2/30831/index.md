---
layout: "image"
title: "fuelmeter1"
date: "2011-06-10T09:06:27"
picture: "linktrainer01.jpg"
weight: "1"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30831
- /detailse3cf-2.html
imported:
- "2019"
_4images_image_id: "30831"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30831 -->
The fuel gauge on the side. 
A paper strip is rolled up and down and is kept taut by a rubber band.

http://www.youtube.com/watch?v=HeMQGV5nDw4&feature=mfu_in_order&list=UL