---
layout: "image"
title: "Rotorkopf02.JPG"
date: "2007-01-25T19:08:13"
picture: "Rotorkopf02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8698
- /detailsaf08.html
imported:
- "2019"
_4images_image_id: "8698"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:08:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8698 -->
Wie beim echten Hubschrauber können die Rotorblätter seitlich (in Drehrichtung) wackeln. Was hier noch fehlt, ist das Schlaggelenk, das den Blättern die Bewegung nach oben und unten erlaubt. Zwischen die Winkelträger 30 und die U-Trägeradapter muss man sich also noch je einen Gelenkwürfel mit horizontal liegender Achse denken.
