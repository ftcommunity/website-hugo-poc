---
layout: "comment"
hidden: true
title: "13586"
date: "2011-02-17T00:45:05"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
gleichen die Gelenke denn wirklich aus, oder machen sie Dein Modell instabil?
Ich glaubte bei meinen Recherchen gelernt zu haben, dass man bei modernen Rotoren keine Schlag- und Schwenkgelenke mehr verwendet, sondern statt dessen einen "starren" Rotorkopf mit Rotoren, deren Werkstoffe die Kräfte ausgleichen (http://de.wikipedia.org/wiki/Rotorkopf). Da fischertechnik ja von Natur aus nur eine begrenzte Steifigkeit mitbringt, haben wir in unserem Modell ebenfalls auf Schlag- und Schwenkgelenke verzichtet. Allerdings haben wir keine Vergleichstests angestellt... Hast Du das mit Deinem Modell untersucht?
Gruß, Dirk