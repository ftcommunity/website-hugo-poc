---
layout: "image"
title: "Rotorkopf01.JPG"
date: "2007-01-25T19:04:27"
picture: "Rotorkopf01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["32455", "35977", "Rotorkopf", "Hubschrauber"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8697
- /detailse60a.html
imported:
- "2019"
_4images_image_id: "8697"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:04:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8697 -->
Zwei gute Bekannte, die Führungsplatte 32455 und die Schnecke 35977, sind mal wieder mitten im Geschehen beteiligt. Neu dabei ist das "Lagerstück 2", 31772 und darin eingeklipst ein "Lagerstück 1", 31771. 

Wobei aber die Abbildungen des letzteren in der Teileliste wie auch bei Knobloch nicht ganz hinhauen: was ich da als 31771 verbaut habe, hat eine kurze Stummelachse mit einem Rast-Wulst, mit dem man das Lagerstück 2 aufklipsen kann. Von diesen Pärchen sind hier 5 verbaut: zwei oben als Kipplager für die Rotorblätter, und drei unten, um die Taumelscheibe gelenkig zu lagern.
