---
layout: "image"
title: "DC9-16_3374.JPG"
date: "2011-02-12T12:54:53"
picture: "DC9-16_3374.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29912
- /detailsc983.html
imported:
- "2019"
_4images_image_id: "29912"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:54:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29912 -->
Die Einzelteile des Triebwerks-Dummys.
