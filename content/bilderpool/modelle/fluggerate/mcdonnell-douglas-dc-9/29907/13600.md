---
layout: "comment"
hidden: true
title: "13600"
date: "2011-02-18T16:16:34"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja richtig, ich brauchte eine noch stärkere Untersetzung als es jedes der beiden Getriebe alleine geschafft hätte. Außerdem musste alles in den begrenzten Raum hineinpassen. Als Nebeneffekt gibt es noch den 'weichen' Endanschlag nach oben: wenn die Zahnstange aus dem Hubgetriebe heraus ist, passiert einfach *nichts*.

Gruß,
Harald