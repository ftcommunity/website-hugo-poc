---
layout: "image"
title: "DC9-08_4102.JPG"
date: "2011-02-12T12:24:24"
picture: "DC9-08_4102.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29904
- /details0804.html
imported:
- "2019"
_4images_image_id: "29904"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29904 -->
Im dritten Feld von außen scheint der Motor nebst U-Getriebe und Schnecke fürs Hauptfahrwerk hindurch. Für den wäre unter der Kabine noch Platz gewesen, aber die Antriebsachse (man sieht das Kardangelenk im Rumpf verschwinden) muss von der Seite kommen. Also war besonders flache Bauart angesagt.

Die grüne Leuchte ist hier falsch - dahin gehört die rote.
