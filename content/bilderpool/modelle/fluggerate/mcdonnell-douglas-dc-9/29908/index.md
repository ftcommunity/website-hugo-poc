---
layout: "image"
title: "DC9-12_4110.JPG"
date: "2011-02-12T12:43:02"
picture: "DC9-12_4110.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29908
- /detailse777.html
imported:
- "2019"
_4images_image_id: "29908"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:43:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29908 -->
Zwischen den Verstrebungen der Flügelunterseite sieht man den Motor nebst Getriebe fürs Hauptfahrwerk. Der U-Träger ist außen mittels einer Winkelachse 31035 am Aluträger befestigt. Die Kardanwelle musste mit einem BS5 auf richtige Länge gebracht werden. Rechts oben, mit weißem und braunem Stecker, befindet sich ein Landescheinwerfer mit LED.
