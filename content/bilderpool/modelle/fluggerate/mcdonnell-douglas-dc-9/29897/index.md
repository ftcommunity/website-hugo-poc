---
layout: "image"
title: "DC9-01_4601.JPG"
date: "2011-02-12T12:05:54"
picture: "DC9-01_4601.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29897
- /details1db1.html
imported:
- "2019"
_4images_image_id: "29897"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:05:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29897 -->
Die DC-9 kommt diesem Flieger noch am nächsten. Die Konfiguration der Heckflosse ist allerdings deutlich anders, das Original hat hier ein T-Leitwerk mit dem Höhenruder ganz oben. Dafür hat hier die Stabilität nicht gereicht. Die Unterbringung der ganzen Mechanik wäre die nächste Frage gewesen.
