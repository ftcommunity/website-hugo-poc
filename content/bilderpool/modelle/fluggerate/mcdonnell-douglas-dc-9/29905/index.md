---
layout: "image"
title: "DC9-09_4101.JPG"
date: "2011-02-12T12:27:19"
picture: "DC9-09_4101.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29905
- /detailsb057.html
imported:
- "2019"
_4images_image_id: "29905"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:27:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29905 -->
In der rechten Tragfläche sind eine Blink-Elektronik und ein eTec-Modul untergebracht. Der Alu-Träger geht quer durch den Rumpf bis nach gegenüber. Den Rest der Stabilität für den Flügel übernimmt der schwarze U-Träger, der etwas angewinkelt zum Alu steht.
