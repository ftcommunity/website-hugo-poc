---
layout: "image"
title: "DC9-21_4627.JPG"
date: "2011-02-12T13:13:00"
picture: "DC9-21_4627.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/29917
- /detailsf312.html
imported:
- "2019"
_4images_image_id: "29917"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T13:13:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29917 -->
Das Bugfahrwerk hat ebenfalls eine Kniehebelmechanik, die im ausgefahrenen Zustand überstreckt ist und damit ein selbsttätiges Einklappen verhindert.
