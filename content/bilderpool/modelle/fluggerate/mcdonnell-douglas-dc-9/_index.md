---
layout: "overview"
title: "McDonnell Douglas DC-9"
date: 2020-02-22T08:34:24+01:00
legacy_id:
- /php/categories/2208
- /categoriesa1f5.html
- /categories8a83.html
- /categoriesa4da.html
- /categories7e88.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2208 --> 
... ist zumindest das nächstliegende 'echte' Flugzeug zu diesem Modell. Die Konfiguration der Heckflosse (Position des Höhenruders) ist allerdings deutlich anders.