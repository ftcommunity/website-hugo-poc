---
layout: "image"
title: "Gesamtansicht 2"
date: "2010-04-18T10:25:24"
picture: "kampfjet2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/26954
- /details2825.html
imported:
- "2019"
_4images_image_id: "26954"
_4images_cat_id: "1935"
_4images_user_id: "791"
_4images_image_date: "2010-04-18T10:25:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26954 -->
