---
layout: "image"
title: "flugz20.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz20.jpg"
weight: "20"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47032
- /detailsb4cd.html
imported:
- "2019"
_4images_image_id: "47032"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47032 -->
