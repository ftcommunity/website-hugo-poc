---
layout: "image"
title: "flugz01.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/47013
- /details0bf1.html
imported:
- "2019"
_4images_image_id: "47013"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47013 -->
