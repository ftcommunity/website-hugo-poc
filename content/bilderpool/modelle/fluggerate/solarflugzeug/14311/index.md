---
layout: "image"
title: "Hintere Flügel"
date: "2008-04-19T14:36:42"
picture: "solarflugzeug4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14311
- /details4583.html
imported:
- "2019"
_4images_image_id: "14311"
_4images_cat_id: "1324"
_4images_user_id: "747"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14311 -->
Hier sieht man die hinteren Flügel.