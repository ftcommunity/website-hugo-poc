---
layout: "image"
title: "Solarflugzeug von oben"
date: "2008-04-19T14:36:42"
picture: "solarflugzeug3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/14310
- /details364f.html
imported:
- "2019"
_4images_image_id: "14310"
_4images_cat_id: "1324"
_4images_user_id: "747"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14310 -->
Auf diesem Bild sieht man die Tragflächen. Die Solarzellen sind ein Teil der Flügel und eine Stromquelle gleichzeitig.