---
layout: "image"
title: "Cockpit back"
date: "2011-05-29T14:42:43"
picture: "cockpit_back.jpg"
weight: "8"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30676
- /details96eb.html
imported:
- "2019"
_4images_image_id: "30676"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30676 -->
A total view on the back of the cockpit. Maybe you have already noticed that all components of the installation with plug cords have to been connected. Therefore it is necessary ofcourseall the connections to mark.