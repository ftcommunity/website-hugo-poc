---
layout: "image"
title: "Back alti meter"
date: "2011-05-29T14:42:43"
picture: "back_altimeter.jpg"
weight: "3"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30671
- /detailsf690.html
imported:
- "2019"
_4images_image_id: "30671"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30671 -->
The decelaration and the angular gears on so little space, I could only solve with my meccano parts. Two clockhands with different speed and a movement indicator at the same time. However it works very nice. If the big clockhand goes one time around the small one moves one digit.