---
layout: "image"
title: "Back horizon"
date: "2011-05-29T14:42:43"
picture: "back_horizon.jpg"
weight: "4"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30672
- /details1f52.html
imported:
- "2019"
_4images_image_id: "30672"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30672 -->
Here you see only a very strong decelaration with fischer parts.
The meccano part is for control the neutral indication with a microswitch.