---
layout: "image"
title: "Stick direction function"
date: "2011-05-29T14:42:43"
picture: "stick_direction.jpg"
weight: "15"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30683
- /details6d30-3.html
imported:
- "2019"
_4images_image_id: "30683"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30683 -->
The disc is for choosing the direction of the plotter and compass.
By turning the stick you can change immediately the direction.
If you had choosen for bad weather Robopro shall influence this compass direction. There are 8 contacts: North, South, West, East, and between.