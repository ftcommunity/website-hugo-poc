---
layout: "image"
title: "Bluebox2"
date: "2011-05-29T14:42:43"
picture: "bluebox2.jpg"
weight: "6"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30674
- /details835b.html
imported:
- "2019"
_4images_image_id: "30674"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30674 -->
A view inside the bluebox.