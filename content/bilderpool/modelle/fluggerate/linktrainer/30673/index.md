---
layout: "image"
title: "Bluebox1"
date: "2011-05-29T14:42:43"
picture: "bluebox1.jpg"
weight: "5"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30673
- /details45d3.html
imported:
- "2019"
_4images_image_id: "30673"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30673 -->
The plane (bluebox) is a free paper model downloaded from www.currell.net.
On the control panel you see three buttons. One for nice weather, one for stormy weather and a start button. Three lights: a warning light for to much rolling, a warning light for height to low and a ready light after initialisation. The two warning lights are also with support of 2 different
sounds from the sound box. (The third sound of the soundbox contains the arrival sound)