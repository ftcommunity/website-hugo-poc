---
layout: "image"
title: "Total stick"
date: "2011-05-29T14:42:43"
picture: "stick.jpg"
weight: "16"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30684
- /detailsf8b5.html
imported:
- "2019"
_4images_image_id: "30684"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30684 -->
Farther the stick works like a real planestick. Left, right, forwards and backwards.