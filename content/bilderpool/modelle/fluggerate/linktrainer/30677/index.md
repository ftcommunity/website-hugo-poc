---
layout: "image"
title: "Cockpit panel"
date: "2011-05-29T14:42:43"
picture: "cockpit.jpg"
weight: "9"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- /php/details/30677
- /details117a-2.html
imported:
- "2019"
_4images_image_id: "30677"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30677 -->
A total view of the cockpit front.