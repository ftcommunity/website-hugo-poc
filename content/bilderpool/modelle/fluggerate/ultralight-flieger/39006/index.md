---
layout: "image"
title: "Ultraleichtflugzeug3"
date: "2014-07-08T20:05:33"
picture: "ultraleichtflugzeug3.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39006
- /details24fc.html
imported:
- "2019"
_4images_image_id: "39006"
_4images_cat_id: "584"
_4images_user_id: "1631"
_4images_image_date: "2014-07-08T20:05:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39006 -->
von Oben