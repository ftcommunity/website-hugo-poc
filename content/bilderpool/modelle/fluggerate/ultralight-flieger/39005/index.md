---
layout: "image"
title: "Ultraleichtflugzeug2"
date: "2014-07-08T20:05:33"
picture: "ultraleichtflugzeug2.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- /php/details/39005
- /details4883.html
imported:
- "2019"
_4images_image_id: "39005"
_4images_cat_id: "584"
_4images_user_id: "1631"
_4images_image_date: "2014-07-08T20:05:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39005 -->
Leider noch ohne Lenkfunktion aber ich probiere schon daran :)