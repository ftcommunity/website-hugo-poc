---
layout: "image"
title: "Ultralight-Mobile - Drachenflieger"
date: "2004-09-29T19:20:20"
picture: "Ultra02.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Mobile", "Ultralight"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/4347
- /details1828.html
imported:
- "2019"
_4images_image_id: "4347"
_4images_cat_id: "584"
_4images_user_id: "34"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4347 -->
Die Idee ist einfach Klasse!

Ein Mobile aus ft-Ultralight-Fliegern. Hier die erste von vier Beteiligten, der Drachenflieger. Nummer 4 war ein Satellit und das Foto ist mir leider verschütt gegangen.

Gehört einfach in jedes Kinderzimmer!
