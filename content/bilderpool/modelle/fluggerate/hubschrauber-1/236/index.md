---
layout: "image"
title: "hubi16"
date: "2003-04-21T19:56:06"
picture: "hubi16.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/236
- /details2cf7.html
imported:
- "2019"
_4images_image_id: "236"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=236 -->
