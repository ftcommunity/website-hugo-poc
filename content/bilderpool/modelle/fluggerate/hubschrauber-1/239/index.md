---
layout: "image"
title: "hubi19"
date: "2003-04-21T19:56:06"
picture: "hubi19.jpg"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Rotorkopf", "Taumelscheibe", "Hubschrauber"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/239
- /detailsf3bd.html
imported:
- "2019"
_4images_image_id: "239"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=239 -->
