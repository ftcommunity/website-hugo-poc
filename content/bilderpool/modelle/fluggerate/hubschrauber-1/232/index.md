---
layout: "image"
title: "hubi12"
date: "2003-04-21T19:56:06"
picture: "hubi12.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/232
- /detailsfc3b.html
imported:
- "2019"
_4images_image_id: "232"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=232 -->
