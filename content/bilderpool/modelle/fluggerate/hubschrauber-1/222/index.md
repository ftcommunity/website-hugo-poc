---
layout: "image"
title: "hubi02"
date: "2003-04-21T19:56:05"
picture: "hubi02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/222
- /detailse892.html
imported:
- "2019"
_4images_image_id: "222"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=222 -->
