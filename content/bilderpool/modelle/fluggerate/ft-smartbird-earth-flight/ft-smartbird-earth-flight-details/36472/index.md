---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails12.jpg"
weight: "12"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36472
- /details2298-2.html
imported:
- "2019"
_4images_image_id: "36472"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36472 -->
Unten -hinten
