---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36465
- /details1ecc.html
imported:
- "2019"
_4images_image_id: "36465"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36465 -->
Der Flügel besteht aus einem zweiteiligen Armflügelholm mit einer Achsaufnahme am Rumpfaustritt, einem Trapezgelenk, wie dies in vergrößerter Form bei Baggern vorkommt, und einem Handflügelholm.
Der Armflügel erzeugt den Auftrieb, der Handflügel nach dem Trapezgelenk den Vortrieb. 

Voor herkenning van de 0-Boven-Positie van de centrale vleugelaandrijving (=2x FT-50:1 Powermotor) gebruik ik schakelaar I-1.
Voor de Positie-herkenning van de centrale vleugelaandrijving gebruik ik Pulsteller I-2.
