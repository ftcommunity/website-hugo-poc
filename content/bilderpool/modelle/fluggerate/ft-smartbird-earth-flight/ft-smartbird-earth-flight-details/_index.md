---
layout: "overview"
title: "FT-Smartbird-Earth-flight-details"
date: 2020-02-22T08:34:36+01:00
legacy_id:
- /php/categories/2708
- /categoriesf29b.html
- /categoriesebdc.html
- /categoriesb111.html
- /categoriesa753.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2708 --> 
Details der Kinematik meiner "Fischertechnik-Smartbird-Earth-Flight".

Der Flügel besteht aus einem zweiteiligen Armflügelholm mit einer Achsaufnahme am Rumpfaustritt, einem Trapezgelenk, wie dies in vergrößerter Form bei Baggern vorkommt, und einem Handflügelholm.
Der Armflügel erzeugt den Auftrieb, der Handflügel nach dem Trapezgelenk den Vortrieb. 
Am Begin des Handflügels befindet sich der FT-Servomotor (mit Potmeter)  für die aktive Torsion. 

