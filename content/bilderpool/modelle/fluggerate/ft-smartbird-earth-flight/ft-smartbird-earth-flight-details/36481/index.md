---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight -Gesammt-Ansicht"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails21.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36481
- /details9324-3.html
imported:
- "2019"
_4images_image_id: "36481"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36481 -->
Styropor-Kopf
