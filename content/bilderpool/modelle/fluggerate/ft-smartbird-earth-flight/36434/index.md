---
layout: "image"
title: "- beide eindvleugels middels de servo's in de -schuin-laag positie."
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight25.jpg"
weight: "25"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36434
- /details2fd8-3.html
imported:
- "2019"
_4images_image_id: "36434"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36434 -->
Het Robopro-programma stuurt de 2 FT-Servo-motoren voor de verdraaiing van de "eindvleugels" aan afhankelijk van de centrale vleugelaandrijving-positie. 

- Nadat de centrale vleugel-aandrijving schakelaar I-1 (=0-boven-positie) heeft bekrachtigd, gaan na 4 pulsovergangen beide eindvleugels in de -horizontaal-hoog positie middels de servo's. 
- De centrale vleugel-aandrijving gaat ononderbroken door. Na 2 verdere pulsovergangen gaan beide eindvleugels middels de servo's in de -schuin-laag positie. 
- De centrale vleugel-aandrijving gaat ononderbroken door tot I-1 (=0-boven-positie) wederom wordt bekrachtigd en bovenstaande cyclus zich herhaald.
