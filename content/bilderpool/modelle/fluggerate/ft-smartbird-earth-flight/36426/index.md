---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight17.jpg"
weight: "17"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36426
- /detailsd8dc-2.html
imported:
- "2019"
_4images_image_id: "36426"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36426 -->
De Robo Interface gebruik ik uitsluitend en alleen voor de bediening van de 2 FT-Servo-motoren ten behoeve van verdraaiing van de "eindvleugels".
Voor herkenning van de 0-Boven-Positie van de centrale vleugelaandrijving gebruik ik schakelaar I-1.
Voor de Positie-herkenning van de centrale vleugelaandrijving gebruik ik Pulsteller I-2.

Vanwege de potmeter-weerstandverschillen en om kapotdraaien te voorkomen, stuur ik elke FT-Servo apart aan met een max. snelheidswaarde 4. 
Om de zaak "eenvoudig" te houden laat ik elke FT-Servo volledig in de uiterste posities bewegen.

De Festo-Smartbird, die ook daadwerkelijk zelf kan vliegen, regelt ook de tussen-eindvleugel-posities afhankelijk van o.a. het toerental van de centrale vleugelaandrijving en de gewenste vlucht.
