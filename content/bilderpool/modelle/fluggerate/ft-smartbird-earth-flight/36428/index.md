---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight19.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/36428
- /details26b9.html
imported:
- "2019"
_4images_image_id: "36428"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36428 -->
Elke vleugel heeft een "ondervleugel" voor voldoende lift, én een FT-Servo-motor met potmeter voor verdraaiing van de "eindvleugel" ten behoeve van de voorwaartse stuwkracht.
