---
layout: "image"
title: "Detailansicht: Verstellung der Taumelscheibe (Konstruktionszeichnung)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor07.jpg"
weight: "7"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29884
- /detailsc86d.html
imported:
- "2019"
_4images_image_id: "29884"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29884 -->
Die Neigung des Unterteils der Taumelscheibe (im Bild die oberste Drehscheibe 60) wird über die drei Schnecken(gewinde) eingestellt. Die Drehscheibe hat keine Nabe, damit sie jeden Neigungswinkel einnehmen kann.