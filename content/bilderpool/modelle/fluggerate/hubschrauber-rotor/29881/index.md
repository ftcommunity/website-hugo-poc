---
layout: "image"
title: "Detailansicht: Rotorkopf mit Taumelscheibe (Konstruktionszeichnung)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor04.jpg"
weight: "4"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29881
- /detailsb2b8.html
imported:
- "2019"
_4images_image_id: "29881"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29881 -->
Die Taumelscheibe (Drehscheibe 60) ist mit den Rotorblättern über zwei I-Streben verbunden; wird die Taumelscheibe angehoben, verstellt sich der Neigungswinkel (Pitch) der Rotorblätter. Die beiden Verbindungen der Taumelscheibe mit dem Rotorkopf über je zwei Kupplungsstücke verhindert, dass sich die Taumelscheibe gegen den Rotor verdreht (und so den Neigungswinkel verfälscht).