---
layout: "overview"
title: "Hubschrauber-Rotor"
date: 2020-02-22T08:34:21+01:00
legacy_id:
- /php/categories/2205
- /categories7536.html
- /categoriesf827.html
- /categories17f4.html
- /categories90e6.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2205 --> 
Haupt- und Heckrotor eines Hubschraubers mit Pitch und Taumelscheibe.