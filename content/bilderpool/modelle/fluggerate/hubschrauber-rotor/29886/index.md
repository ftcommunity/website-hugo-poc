---
layout: "image"
title: "Detailansicht: Heckrotor mit Pitch (Konstruktionszeichnung)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor09.jpg"
weight: "9"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29886
- /detailsdae6.html
imported:
- "2019"
_4images_image_id: "29886"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29886 -->
Beim Heckrotor ließ sich die Verstellung des Pitchs der Rotorblätter mit zwei Lochsteinen (statt der Drehscheibe 60) realisieren; die Verbindung der I-Streben mit dem Lochstein ist so stabil, dass keine zusätzliche Verbindung mit dem Rotorkopf erforderlich ist. Dadurch wird die gesamte Konstruktion deutlich schlanker.