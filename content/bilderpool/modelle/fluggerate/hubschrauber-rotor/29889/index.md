---
layout: "image"
title: "Detailansicht: Verstellhebel des Heckrotor-Pitches"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor12.jpg"
weight: "12"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29889
- /details29ab-2.html
imported:
- "2019"
_4images_image_id: "29889"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29889 -->
Die Pitch-Verstellung erfolgt mechanisch mit einem Seilzug, der über zwei Umlenkrollen mit den Pedalen im Cockpit verbunden wird.