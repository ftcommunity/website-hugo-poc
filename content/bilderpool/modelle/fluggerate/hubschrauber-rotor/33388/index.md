---
layout: "image"
title: "Hubschrauber-Rotor-Alternativ"
date: "2011-11-04T20:30:12"
picture: "Rotorkopf-alternativ_007.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/33388
- /detailscd1c.html
imported:
- "2019"
_4images_image_id: "33388"
_4images_cat_id: "2205"
_4images_user_id: "22"
_4images_image_date: "2011-11-04T20:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33388 -->
Hubschrauber-Rotor-Alternativ
