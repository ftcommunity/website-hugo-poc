---
layout: "image"
title: "Hauptrotor mit drei Rotorblättern"
date: "2011-09-09T07:42:02"
picture: "Gesamtansicht_Hauptrotor_mit_drei_Rotorblttern.jpg"
weight: "14"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/31765
- /details587e.html
imported:
- "2019"
_4images_image_id: "31765"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-09-09T07:42:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31765 -->
Für den Sikorsky VS-300 (auf der Convention 2011 zu sehen) brauchte Johann eine Variante des Rotors mit drei Rotorblättern. Dazu hat er die Mechanik der Taumelscheibe wie beim Heckrotor auf reine Mechanik (Seilzüge statt Schneckengetriebe) umgestellt.