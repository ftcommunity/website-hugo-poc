---
layout: "image"
title: "Stirnseite"
date: "2014-10-02T21:56:48"
picture: "pistenbully07.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39504
- /details444e-2.html
imported:
- "2019"
_4images_image_id: "39504"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39504 -->
Da geht es recht eng zu. Das Gleichlaufgetriebe (Nummer 9, von NBGer) ist längs eingebaut, und ums Eck herum geht es hier mit Kronenrädern aus Z20 und darum geschlungenen schwarzen Ketten. 

Der Motor oben drauf steuert den gegensinnigen Eingang an. Das schwarze Z10 ist von TST ( http://ftcommunity.de/details.php?image_id=32625 ). Das rote Z10 wurde hier mal auf seinem Platz angetroffen, normalerweise hüpft es irgendwo im Raum umher...
