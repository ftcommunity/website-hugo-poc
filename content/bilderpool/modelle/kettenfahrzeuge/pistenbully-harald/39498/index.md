---
layout: "image"
title: "Totale von links"
date: "2014-10-02T21:56:48"
picture: "pistenbully01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39498
- /details0aff.html
imported:
- "2019"
_4images_image_id: "39498"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39498 -->
Hübsch an zu schauen, aber technisch eher misslungen ist diese Pistenraupe. Die Details dazu kommen weiter hinten.
