---
layout: "image"
title: "Totale, im Gelände"
date: "2014-10-02T21:56:48"
picture: "pistenbully02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39499
- /detailsc4da.html
imported:
- "2019"
_4images_image_id: "39499"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39499 -->
Dank Einzelradfederung geländegängig, aber leider bringt der Antrieb nicht viel. Die Packerwalze hinten ist gerade abgebaut.
