---
layout: "image"
title: "Fahrerkabine"
date: "2014-10-02T21:56:48"
picture: "pistenbully03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39500
- /details3ece.html
imported:
- "2019"
_4images_image_id: "39500"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39500 -->
Die Schalter für Blinklicht und Arbeitsscheinwerfer sind oben unter dem Dachhimmel montiert.
