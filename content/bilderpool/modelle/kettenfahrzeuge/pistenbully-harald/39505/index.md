---
layout: "image"
title: "Unterseite mit Ketten"
date: "2014-10-02T21:56:48"
picture: "pistenbully08.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39505
- /details1129.html
imported:
- "2019"
_4images_image_id: "39505"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39505 -->
Die weißen Flecken sind die sichtbaren Abschnitte des Federungs-Hosengummis, das der Reihe nach durch alle Radaufhängungen gefädelt ist. Die Laufrollen an den "Ecken" haben zusätzliche Plastik-Federn.

Die grauen Motoren liegen auf (im Bild natürlich unter) einer "Spur N Grundplatte" 36093 aus der Bauspielbahn (BSB).
