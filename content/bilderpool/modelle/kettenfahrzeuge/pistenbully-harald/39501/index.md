---
layout: "image"
title: "Packerwalze"
date: "2014-10-02T21:56:48"
picture: "pistenbully04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39501
- /details4ad9.html
imported:
- "2019"
_4images_image_id: "39501"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39501 -->
Damit wird der Schnee hinter dem Fahrzeug verdichtet. Die Befestigung der ft-Leiter mittels Federnocken und Klemmhülse 35980 habe ich bei Ludger abgeguckt. 

Die Führungsplatten 32455 verhindern, dass die Spurschienen nach innen, in die Speichenräder hinein wandern. Nach außen hält da nichts und niemand, deshalb müssen die Achsen der Speichenräder auf Zug gehalten werden. Dafür sorgen die Platten 15x90, die unter Federspannung durch die in die Leiter eingeklemmten Flachträger 120 stehen.

Die zwei Führungsplatten 32455 unten im Bild dienen als seitliche Anschläge, weil die Packerwalze pendelnd in einer Dreipunktaufnahme am Fahrzeugheck hängt.
