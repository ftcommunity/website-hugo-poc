---
layout: "image"
title: "ohne Verkleidung"
date: "2014-10-02T21:56:48"
picture: "pistenbully05.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39502
- /details98ab.html
imported:
- "2019"
_4images_image_id: "39502"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39502 -->
Die Verkleidungsplatten mit Clip, genauso wie die Clipplattenverbinder kommen hier mal schön zur Geltung.

Der Empfänger ist eigentlich ein "blauer" (altes Modell), der in ein rotes Gehäuse umgezogen ist. Deswegen kennt er auch den Raupenmodus (Brücke von 1 nach 2) und hat drei Motorausgänge.

Der einzelne rote (1:50) Powermot links treibt den gegensinnigen Eingang des Gleichlaufgetriebes an. Das tut er ganz gut, aber nach 30 bis 45° Drehung auf der Stelle wird regelmäßig eine Kette abgeworfen, falls nicht schon vorher das rote Z10 vom Motor gesprungen ist.

Die zwei Powermotoren 1:20 (graue Kappe) treiben den gleichsinnigen Eingang des Gleichlaufgetriebes an. Die Kraft würde ausreichen, aber trotz Nylodenfäden, die in die Achsverbinder geklemmt sind, schrauben sie eher den Antrieb auseinander als das Fahrzeug zu bewegen.

Das Gleichlaufgetriebe ist die "Variante 9" ( http://ftcommunity.de/categories.php?cat_id=2918 ) von NBGer, die ich unbedingt mal verwenden musste. Das klappt, aber man muss dafür sorgen, dass die großen Drehmomente erst *nach* dem Getriebe aufgebaut werden, d.h. Antrieb eher mit 1:10-Motoren, aber dann mit deutlichen zusätzlichen Untersetzungen auf die Ketten gehen.
