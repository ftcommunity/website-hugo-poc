---
layout: "image"
title: "Kettenfahrzeug 11"
date: "2007-03-17T18:16:58"
picture: "kettenfahtzeugmitfederung11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9550
- /details5f9c.html
imported:
- "2019"
_4images_image_id: "9550"
_4images_cat_id: "872"
_4images_user_id: "502"
_4images_image_date: "2007-03-17T18:16:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9550 -->
Das kleine Zahnrad treibt beide großen Zahräder an.
