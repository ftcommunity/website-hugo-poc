---
layout: "image"
title: "Kettenfahrzeug 12"
date: "2007-03-17T18:16:58"
picture: "kettenfahtzeugmitfederung12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/9551
- /details8230.html
imported:
- "2019"
_4images_image_id: "9551"
_4images_cat_id: "872"
_4images_user_id: "502"
_4images_image_date: "2007-03-17T18:16:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9551 -->
Die schwarzen Platten lassen sich hochklappen.
