---
layout: "image"
title: "Panzer Leopard 2 (05)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven5.jpg"
weight: "5"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- /php/details/46769
- /details03ff.html
imported:
- "2019"
_4images_image_id: "46769"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46769 -->
Die Motorabdeckung lässt sich öffnen. Um genügend Drehmoment auf die Ketten zu bekommen, habe ich die Lösung mit den Rädern gewählt, da ich nur das Differential vom Traktor-Set habe. Diese Übersetzung reichte nicht aus.