---
layout: "image"
title: "Panzer Leopard 2 (01)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven1.jpg"
weight: "1"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- /php/details/46765
- /details3f7a.html
imported:
- "2019"
_4images_image_id: "46765"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46765 -->
Der Panzer ist mit zwei Traktormotoren angetrieben, die über die Raupenfunktion der Fernsteuerung angesteuert werden.