---
layout: "image"
title: "Mini-Panzer 1"
date: "2007-03-05T21:52:11"
picture: "Mini-Panzer_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/9301
- /details1848.html
imported:
- "2019"
_4images_image_id: "9301"
_4images_cat_id: "858"
_4images_user_id: "328"
_4images_image_date: "2007-03-05T21:52:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9301 -->
Da ich (bislang) nur die kurzen Ketten (2x20 schwarze Teile) aus dem kleinen Bulldozer-Kasten (34979) habe, habe ich versucht, mit denen ein motorisiertes lenkbares Kettenfahrzeug zu bauen. Viel ist nicht möglich, aber mein kleiner Panzer sieht ganz nett aus und hat mit dem Batteriekasten sogar seinen Strom mit an Bord!

Da für gutes Vorankommen im Gelände eine möglichst geringe Masse notwendig ist, musste ich auf thematisch andere schwerere Aufbauten (Führerhaus, Kran, etc.) leider verzichten und das Fahrzeug mit einer leichten Kanone versehen. Ich hoffe, das Thema stösst hier nicht allzu sehr auf Missgefallen... Wenn ja, entschuldigt bitte.

Jedenfalls kommt er so ganz ordentlich voran, wobei die Batteriemasse doch recht bremst. Mit externer Stromversorgung geht dann doch deutlich mehr!