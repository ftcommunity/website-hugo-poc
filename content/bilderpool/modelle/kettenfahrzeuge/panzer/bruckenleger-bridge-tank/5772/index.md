---
layout: "image"
title: "Bridge Tank TM-5-5420-203-14"
date: "2006-02-18T21:24:51"
picture: "Bridging_Tank_TM-5-5420-203-14_022_2.jpg"
weight: "37"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5772
- /details0b94.html
imported:
- "2019"
_4images_image_id: "5772"
_4images_cat_id: "486"
_4images_user_id: "22"
_4images_image_date: "2006-02-18T21:24:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5772 -->
Bridge Tank TM-5-5420-203-14 bei einer anderen Fluss.
