---
layout: "image"
title: "Bridge Tank TM-5-5420-203-14"
date: "2006-01-21T19:52:07"
picture: "Bridging_Tank_TM-5-5420-203-14_002.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (alias PeterHolland)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5606
- /details380b.html
imported:
- "2019"
_4images_image_id: "5606"
_4images_cat_id: "486"
_4images_user_id: "22"
_4images_image_date: "2006-01-21T19:52:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5606 -->
