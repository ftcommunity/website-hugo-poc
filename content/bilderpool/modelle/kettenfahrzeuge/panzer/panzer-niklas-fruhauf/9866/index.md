---
layout: "image"
title: "Panzer"
date: "2007-03-31T18:08:14"
picture: "panzer3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9866
- /details52d7.html
imported:
- "2019"
_4images_image_id: "9866"
_4images_cat_id: "891"
_4images_user_id: "557"
_4images_image_date: "2007-03-31T18:08:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9866 -->
Bodenabstand, leider nicht viel, Kurbel links :Links-Rechts, Kurbel rechts oben :Hoch-Runter