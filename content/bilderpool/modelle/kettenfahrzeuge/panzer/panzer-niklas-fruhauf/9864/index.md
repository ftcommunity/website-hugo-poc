---
layout: "image"
title: "Panzer"
date: "2007-03-31T18:08:13"
picture: "panzer1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9864
- /details9df2.html
imported:
- "2019"
_4images_image_id: "9864"
_4images_cat_id: "891"
_4images_user_id: "557"
_4images_image_date: "2007-03-31T18:08:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9864 -->
Schussrohr und aufbau schwekbar(lins rechts, hoch runter)