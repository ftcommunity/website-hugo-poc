---
layout: "image"
title: "Panzer"
date: "2007-03-31T18:08:14"
picture: "panzer4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9867
- /details5793.html
imported:
- "2019"
_4images_image_id: "9867"
_4images_cat_id: "891"
_4images_user_id: "557"
_4images_image_date: "2007-03-31T18:08:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9867 -->
Schussvorrichtung