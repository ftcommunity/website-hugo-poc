---
layout: "image"
title: "Panzer"
date: "2014-10-27T17:53:42"
picture: "tank1.jpg"
weight: "1"
konstrukteure: 
- "Patrick Holtz"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39754
- /detailsa6da.html
imported:
- "2019"
_4images_image_id: "39754"
_4images_cat_id: "2982"
_4images_user_id: "2228"
_4images_image_date: "2014-10-27T17:53:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39754 -->
Der Panzer ist per Control Set fernsteurbar. Er wird von zwei Powermotoren (20:1) angetrieben.
