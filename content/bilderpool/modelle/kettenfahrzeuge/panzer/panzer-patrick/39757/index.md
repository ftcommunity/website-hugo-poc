---
layout: "image"
title: "Aufbau"
date: "2014-10-27T17:53:42"
picture: "tank4.jpg"
weight: "4"
konstrukteure: 
- "Patrick Holtz"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/39757
- /details39f8.html
imported:
- "2019"
_4images_image_id: "39757"
_4images_cat_id: "2982"
_4images_user_id: "2228"
_4images_image_date: "2014-10-27T17:53:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39757 -->
Der Aufbau ist wie bei den meisten Originalen flach gestaltet. Das "Panzerrohr" lässt sich durch Drehen an der schwarzen Schnecke heben senken.
