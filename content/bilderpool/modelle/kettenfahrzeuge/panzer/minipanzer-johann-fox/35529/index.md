---
layout: "image"
title: "Minipanzer - Variante mit Gummis"
date: "2012-09-16T20:52:47"
picture: "minipanzer5.jpg"
weight: "5"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35529
- /detailsbb63.html
imported:
- "2019"
_4images_image_id: "35529"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35529 -->
