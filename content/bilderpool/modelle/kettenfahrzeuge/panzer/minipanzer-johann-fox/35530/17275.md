---
layout: "comment"
hidden: true
title: "17275"
date: "2012-09-23T10:55:09"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Tja... einmal nicht hingeguckt - schon passiert. In meiner Jugend wäre wohl 'ne motorisierte Friedenstaube rausgekommen :)
Jedenfalls gelang es ihm nicht, Deinen Kleinstpanzer (http://www.ftcommunity.de/categories.php?cat_id=858) zu toppen - irgendwie wollte das IR-Modul nicht recht hineinpassen.
Gruß, Dirk