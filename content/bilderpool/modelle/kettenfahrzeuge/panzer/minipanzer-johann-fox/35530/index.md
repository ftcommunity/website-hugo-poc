---
layout: "image"
title: "Minipanzer - Modellvarianten"
date: "2012-09-16T20:52:47"
picture: "minipanzer6.jpg"
weight: "6"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35530
- /details3308.html
imported:
- "2019"
_4images_image_id: "35530"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35530 -->
