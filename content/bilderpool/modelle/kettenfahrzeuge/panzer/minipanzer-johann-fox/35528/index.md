---
layout: "image"
title: "Minipanzer - Konstruktion"
date: "2012-09-16T20:52:47"
picture: "minipanzer4.jpg"
weight: "4"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35528
- /details506b.html
imported:
- "2019"
_4images_image_id: "35528"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35528 -->
