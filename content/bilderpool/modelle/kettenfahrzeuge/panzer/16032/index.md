---
layout: "image"
title: "Seitenansicht"
date: "2008-10-22T18:56:33"
picture: "kampfpanzer3.jpg"
weight: "9"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16032
- /detailse584.html
imported:
- "2019"
_4images_image_id: "16032"
_4images_cat_id: "32"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T18:56:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16032 -->
