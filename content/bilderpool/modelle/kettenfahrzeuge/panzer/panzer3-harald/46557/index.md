---
layout: "image"
title: "der Neue"
date: "2017-09-30T19:42:20"
picture: "pz1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46557
- /details63d5.html
imported:
- "2019"
_4images_image_id: "46557"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46557 -->
... hat eine Laufrolle mehr als sein Vorgänger, hat (noch) keinen Tx eingebaut, und hat innen noch reichlich Platz.
