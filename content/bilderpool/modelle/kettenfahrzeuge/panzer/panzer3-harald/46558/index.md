---
layout: "image"
title: "Federung"
date: "2017-09-30T19:42:20"
picture: "pz2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46558
- /detailsbffb.html
imported:
- "2019"
_4images_image_id: "46558"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46558 -->
Es gibt einen einzigen langen Gummizug, auf den die Tragarme aller Laufräder aufgefädelt sind. Der Gummizug ist hinter der Bugplatte einfach verknotet. Da soll aber noch eine Trommel mit Motor hin, um die Federung einstellbar zu machen.
