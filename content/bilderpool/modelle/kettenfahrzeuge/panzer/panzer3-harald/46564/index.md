---
layout: "image"
title: "Antrieb"
date: "2017-09-30T19:42:20"
picture: "pz8.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46564
- /detailsb895.html
imported:
- "2019"
_4images_image_id: "46564"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46564 -->
Das ist das "Gleichlaufgetriebe (11)" von hier: https://ftcommunity.de/categories.php?cat_id=3346

Die rote Motorhalteplatte von TST hat eine Bohrung mehr, so dass Z10 und Z20 im richtigen Abstand gehalten werden. 

Der graue Winkelträger sitzt noch etwas schief drin. In Dreieich hatte es ein junger Panzerfahrer geschafft, den Panzer vom Tisch herunter purzeln zu lassen. Da hat es einige Sachen verzogen.
