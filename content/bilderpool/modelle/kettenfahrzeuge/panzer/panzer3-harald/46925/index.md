---
layout: "image"
title: "Wannenbug"
date: "2017-11-18T18:01:37"
picture: "pzr3.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46925
- /detailsa198.html
imported:
- "2019"
_4images_image_id: "46925"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-11-18T18:01:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46925 -->
Die Bugräder sind nicht gefedert.Der Gummizug hält die Kette auch beim Einfedern straff genug. Die Spannvorrichtung ist noch etwas provisorisch.
