---
layout: "image"
title: "Rohrwiege"
date: "2017-11-18T18:01:37"
picture: "pzr5.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46927
- /details07bc.html
imported:
- "2019"
_4images_image_id: "46927"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-11-18T18:01:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46927 -->
Der Servo vom Höhenrichtwerk sitzt genau unter der Rohrwiege.
