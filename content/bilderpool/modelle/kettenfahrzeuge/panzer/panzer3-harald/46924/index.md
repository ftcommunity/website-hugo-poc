---
layout: "image"
title: "Tragarme (2)"
date: "2017-11-18T18:01:37"
picture: "pzr2.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46924
- /detailsf460.html
imported:
- "2019"
_4images_image_id: "46924"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-11-18T18:01:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46924 -->
Den weiteren Umbau skizziert der Akku: für seinen Halt sorgt ein Messingrohr, das den Wannenboden darstellt und links/rechts fest eingebaut ist, also mit der Federung nichts zu tun hat.

Weil das ganze Gefährt erheblich abspecken muss, wird eine Laufrolle wieder gehen müssen.
