---
layout: "image"
title: "Turm-Innereien"
date: "2017-11-18T18:01:37"
picture: "pzr4.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46926
- /detailsfb82.html
imported:
- "2019"
_4images_image_id: "46926"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-11-18T18:01:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46926 -->
Der Turm ruht mit vier Rollen auf dem Drehkranz (der hier nicht mit drauf ist). Ins Turmheck soll ein TX einziehen, dann muss der Rest aber woanders hin.
