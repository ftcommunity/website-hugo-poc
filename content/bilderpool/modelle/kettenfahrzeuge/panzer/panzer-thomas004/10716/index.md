---
layout: "image"
title: "Panzer 3"
date: "2007-06-04T21:22:22"
picture: "Panzer_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10716
- /details37b1.html
imported:
- "2019"
_4images_image_id: "10716"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10716 -->
