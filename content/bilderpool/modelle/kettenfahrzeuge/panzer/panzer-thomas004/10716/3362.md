---
layout: "comment"
hidden: true
title: "3362"
date: "2007-06-05T08:39:03"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ach, irgendwie hat doch jeder schonmal einen Panzer gebaut...

Gerade, wenn man mit Kettenfahrwerken experimentiert und Antriebs- und Laufrad zwecks Geländetauglichkeit höher legt, sieht es sofort wie ein Panzer aus! Da kommt man um das Thema kaum drumrum...

Gruß, der friedliebende Thomas