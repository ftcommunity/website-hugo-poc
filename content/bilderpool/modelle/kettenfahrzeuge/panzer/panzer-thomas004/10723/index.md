---
layout: "image"
title: "Panzer 10"
date: "2007-06-04T21:22:22"
picture: "Panzer_15.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/10723
- /detailsbc22.html
imported:
- "2019"
_4images_image_id: "10723"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10723 -->
Der Kopf klemmt fest auf einem Winkelstein 10x15x15, was einen prima Fahrer ergibt! So wirkt das militärische Gefährt doch noch ganz lustig... ;o)