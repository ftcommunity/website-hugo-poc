---
layout: "image"
title: "Panzer03"
date: "2003-04-21T21:15:35"
picture: "Panzer03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/257
- /details400d.html
imported:
- "2019"
_4images_image_id: "257"
_4images_cat_id: "32"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=257 -->
