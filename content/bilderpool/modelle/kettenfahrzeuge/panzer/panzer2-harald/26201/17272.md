---
layout: "comment"
hidden: true
title: "17272"
date: "2012-09-22T22:36:36"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Bennik,

mit dem Seilzug wird die Kanone in der Höhe gerichtet: das Hubgetriebe zieht über zweifache Umlenkung und mit Untersetzung 2:1 am oberen Ende des Lagerblocks, in dem das Rohr steckt. Ein bisschen Maschinengewehr-Sound ist da auch eingebaut: der Motor hat keinen Endschalter. Wenn die Hubzahnstange komplett ausgefahren ist (dann ist das Rohr ganz oben), knattert sie, weil sie vom Gummizug zurückgehalten wird.

Gruß,
Harald