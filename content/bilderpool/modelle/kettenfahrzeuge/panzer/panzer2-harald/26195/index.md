---
layout: "image"
title: "Panzer06.jpg"
date: "2010-02-02T23:07:30"
picture: "Panzer06.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26195
- /details6fed.html
imported:
- "2019"
_4images_image_id: "26195"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:07:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26195 -->
Was soll man sagen? Bei Panzern sieht man von außen eigentlich nie, was alles drin steckt.
