---
layout: "comment"
hidden: true
title: "10706"
date: "2010-02-03T17:38:03"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

das Modell ist dir rundum gelungen! Als ein geeignetes "Sammelbecken" für ein Konvolut von technischen Funktionen kann man das Modellthema schon akzeptieren. Hattest du zur Formgestaltung ein Vorbild? Da werden wir also dazu noch etwas hören, wenn du dem Modell über den ROBO TX Controller Leben einhauchst?

Gruss Ingo