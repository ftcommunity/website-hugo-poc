---
layout: "comment"
hidden: true
title: "10717"
date: "2010-02-04T13:46:20"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Der Grund ist einfach: da wo die Kette umgelenkt wird und unter Zug steht, kommt eine zusätzliche Kraftkomponente mit Richtung "schräg nach oben/innen" zustande. Die Federbeine an den "Ecken" werden also eingedrückt. Beim Beschleunigen käme noch das Einfedern wegen der Trägheitsmomente des ganzen Fahrzeugs dazu (Aufrichten beim Anfahren, Nicken beim Bremsen) - letzteres hätte ich ja liebend gerne! Nur so stark sind  die Motoren nicht, und wenn, dann sind die Ketten das nächstschwächere Glied.

Gruß,
Harald