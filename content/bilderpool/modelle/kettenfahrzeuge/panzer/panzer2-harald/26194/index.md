---
layout: "image"
title: "Panzer04.jpg"
date: "2010-02-02T23:07:05"
picture: "Panzer04.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26194
- /details05c9.html
imported:
- "2019"
_4images_image_id: "26194"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:07:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26194 -->
