---
layout: "image"
title: "Panzer09.jpg"
date: "2010-02-02T23:10:47"
picture: "Panzer09.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/26196
- /details749f.html
imported:
- "2019"
_4images_image_id: "26196"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:10:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26196 -->
Der Robo-Controller im Turmheck hat noch keine Funktion (isch abe keine Ultraschall-Sensor...), aber das kann ja noch kommen. 

Das schwere Teil verlangt nach einem Gegengewicht, und hierzu bietet es sich an, die Batteriefächer vorn am Turm auch wirklich mit Batterien zu bestücken. Damit kann man den Turm auch unabhängig von der Wanne mit Strom versorgen, und die Schleifringe rauswerfen.
