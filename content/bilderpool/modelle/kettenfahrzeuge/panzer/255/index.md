---
layout: "image"
title: "Panzer01"
date: "2003-04-21T21:15:35"
picture: "Panzer01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/255
- /details8d15-2.html
imported:
- "2019"
_4images_image_id: "255"
_4images_cat_id: "32"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=255 -->
