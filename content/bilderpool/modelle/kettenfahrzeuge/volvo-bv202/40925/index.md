---
layout: "image"
title: "Hebebühne-4"
date: "2015-05-01T22:04:59"
picture: "volvobv32.jpg"
weight: "32"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40925
- /details401a.html
imported:
- "2019"
_4images_image_id: "40925"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40925 -->
