---
layout: "comment"
hidden: true
title: "20572"
date: "2015-05-02T12:29:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich bin fasziniert davon, wie viel Technik Du unterhalb der Verkleidungen verstecken konntest. Das muss sehr geschickt gebaut sein.
Gruß,
Stefan