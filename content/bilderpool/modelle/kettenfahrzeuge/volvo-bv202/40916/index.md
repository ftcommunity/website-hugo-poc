---
layout: "image"
title: "Höhenausgeich"
date: "2015-05-01T22:04:59"
picture: "volvobv23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40916
- /detailsabaa.html
imported:
- "2019"
_4images_image_id: "40916"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40916 -->
Der gesamte Mittelteil ist so konstruiert, dass die Lenkkräfte vom Vorderwagen recht gut auf den Hinterwagen übertragen werden können. Es können sich jedoch einige Bausteine bei zu viel Höhenunterschied zwischen Vorder- und Hinterwagen nach oben bzw. unten verschieben.