---
layout: "overview"
title: "Volvo bv202"
date: 2020-02-22T08:37:27+01:00
legacy_id:
- /php/categories/3073
- /categoriesee2e.html
- /categories1ba9.html
- /categories0cb8.html
- /categories49bc-2.html
- /categories2f59.html
- /categories3868.html
- /categories58fd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3073 --> 
Vorne und hinten angetriebenes Kettenfahrzeug mit Knicklenkung.