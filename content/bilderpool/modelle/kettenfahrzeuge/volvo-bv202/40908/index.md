---
layout: "image"
title: "Motorraum-3"
date: "2015-05-01T22:04:59"
picture: "volvobv15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40908
- /details6d4d-2.html
imported:
- "2019"
_4images_image_id: "40908"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40908 -->
