---
layout: "image"
title: "neue Lenkung-Gesamtansicht"
date: "2015-08-28T21:20:28"
picture: "volvobv1.jpg"
weight: "45"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/41868
- /details03bd.html
imported:
- "2019"
_4images_image_id: "41868"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41868 -->
Da bei der ersten Version der Lenkung nach einigen Tests und längerem Betrieb die Zylinder nicht mehr richtig arbeiteten, musste eine neue Lenkung her. Das Problem der alten Lenkung war, dass die Kolbenstangen auf Biegung beansprucht wurden und mit der Zeit die Kolbendichtungen bei dieser Beanspruchung nicht mehr richtig abdichteten. 
Hier nun die Lösung, bei der die Zylinder nicht mehr auf Biegung sondern nur auf Zug und Druck beasprucht werden.

Falls jemand dieses Modell nachbauen will unbedingt diese Version der Lenkung verwenden! ;-)

Hier nocheinmal der Link zum Video (hat auch im Video schon die neue Lenkung): http://youtu.be/SDb3Kyq1PQw