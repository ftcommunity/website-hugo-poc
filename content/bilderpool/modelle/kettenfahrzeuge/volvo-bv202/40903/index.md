---
layout: "image"
title: "Front"
date: "2015-05-01T22:04:59"
picture: "volvobv10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40903
- /details497e.html
imported:
- "2019"
_4images_image_id: "40903"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40903 -->
