---
layout: "image"
title: "Seitenansicht-1"
date: "2015-05-01T22:04:59"
picture: "volvobv04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40897
- /details191c.html
imported:
- "2019"
_4images_image_id: "40897"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40897 -->
