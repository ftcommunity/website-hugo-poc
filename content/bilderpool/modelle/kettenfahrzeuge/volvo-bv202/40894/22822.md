---
layout: "comment"
hidden: true
title: "22822"
date: "2016-12-22T16:06:23"
uploadBy:
- "FT-Modellbau"
license: "unknown"
imported:
- "2019"
---
Gutes Modell, ich habe versucht es nachzubauen und zu verbessern.
Das Ergebnis sah nicht so schön aus aber war immerhin mit 2 XM Motoren, 2 Empfängern, 2 Akku-Packs und leider nur 2 Pneumatik Zylindern
So stark dass mir eine Kette kaputt gegangen ist aber von der Kraft und Geländegängigkeit her ist meinst bestimmt etwas besser für den Outdoorbereich geeignet :)