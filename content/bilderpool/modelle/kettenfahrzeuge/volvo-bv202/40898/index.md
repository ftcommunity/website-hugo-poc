---
layout: "image"
title: "Seitenansicht-2"
date: "2015-05-01T22:04:59"
picture: "volvobv05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40898
- /details3faf.html
imported:
- "2019"
_4images_image_id: "40898"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40898 -->
Um die Kette etwas mehr zu spannen, musste ich beim letzten Achsabstand der Laufräder etwas schummeln...