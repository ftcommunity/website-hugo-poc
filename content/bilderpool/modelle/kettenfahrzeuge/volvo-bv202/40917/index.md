---
layout: "image"
title: "Verdreh-Ausgleich"
date: "2015-05-01T22:04:59"
picture: "volvobv24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40917
- /details28ca.html
imported:
- "2019"
_4images_image_id: "40917"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40917 -->
In den Boxen rechts und links sitzt jeweils ein Magnetventil zur Steuerung der Hebebühne.