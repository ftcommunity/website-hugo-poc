---
layout: "image"
title: "Detail-1"
date: "2015-05-01T22:04:59"
picture: "volvobv39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40932
- /details2e0c.html
imported:
- "2019"
_4images_image_id: "40932"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40932 -->
Zwischen den zwei Rast-Z20 sitzen zwei der Magnetventile für die Lenkung.