---
layout: "image"
title: "Detail-2"
date: "2015-05-01T22:04:59"
picture: "volvobv40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/40933
- /details7264.html
imported:
- "2019"
_4images_image_id: "40933"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40933 -->
