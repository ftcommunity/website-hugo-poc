---
layout: "image"
title: "Gefedertes Kettenfahrwerk"
date: "2016-03-02T12:54:17"
picture: "federkette1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/42977
- /details6b97.html
imported:
- "2019"
_4images_image_id: "42977"
_4images_cat_id: "3196"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42977 -->
Hier meine vorletzte Bastelei. Ziel war ein möglichst flaches, gefedertes Kettenfahrwerk zu bauen. Momentan fehlt noch der Hauptantrieb, der Lenkmotor des Gleichlaufs ist bereits eingebaut.