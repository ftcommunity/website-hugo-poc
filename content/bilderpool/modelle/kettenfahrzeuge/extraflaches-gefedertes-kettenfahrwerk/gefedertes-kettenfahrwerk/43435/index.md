---
layout: "image"
title: "Rohrneigung oben"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk04.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/43435
- /details6690.html
imported:
- "2019"
_4images_image_id: "43435"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43435 -->
Hat ne Weile gedauert, bin aber dann doch noch auf eine kompakte Lösung zur elektrischen Rohrneigung gekommen.