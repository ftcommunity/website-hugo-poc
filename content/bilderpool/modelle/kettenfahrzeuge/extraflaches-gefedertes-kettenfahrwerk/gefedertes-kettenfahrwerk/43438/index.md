---
layout: "image"
title: "Von der Seite mit Turm"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk07.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/43438
- /details1707-2.html
imported:
- "2019"
_4images_image_id: "43438"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43438 -->
Die beiden Empfänger sind für:
1. Geradeaus
2. Lenkung
3. Turmdrehung
4. Licht (später dann)
5. Rohrneigung
6. Ton?