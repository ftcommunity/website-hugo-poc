---
layout: "image"
title: "Seitansicht/Federung"
date: "2010-12-27T16:29:50"
picture: "panzer3.jpg"
weight: "3"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/29549
- /detailse090.html
imported:
- "2019"
_4images_image_id: "29549"
_4images_cat_id: "2150"
_4images_user_id: "456"
_4images_image_date: "2010-12-27T16:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29549 -->
Da das gesamte Fahrzeug jetzt schon recht schwer ist und wohl auch noch schwerer wird, muss ich die Federn noch härter kriegen. Vielleicht durch stopfen mit Silikonschlauch o.Ä.
