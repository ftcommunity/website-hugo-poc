---
layout: "image"
title: "Panzerwanne"
date: "2010-12-27T16:29:48"
picture: "panzer1.jpg"
weight: "1"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/29547
- /detailseeba.html
imported:
- "2019"
_4images_image_id: "29547"
_4images_cat_id: "2150"
_4images_user_id: "456"
_4images_image_date: "2010-12-27T16:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29547 -->
Zu sehen ist der Um-/Weiterbau meiner Panzerwanne, die ich schon auf der Convnetion 2010 dabei hatte. Sie verfügt über Komplettfederung (sieht man auf den weiteren Bildern besser. Der Antrieb besteht aus 3 Motoren. 2 20:1 Powermotoren treiben je ein Differenzial, deren eine Seite je eine Kette antreibt, die andere Seite ist je mit dem anderen Differenzial verbunden. Die Verbindungsachse ist außerdem durch den 3. Motor (50:1) bewegbar. Durch das unterschiedliche Schalten der Motoren ergeben sich sehr interessante Bewegungsmuster, man hat zahlreiche Möglichkeiten zu lenken und außerdem mit unterschiedlichen Motoren Vorzufahren - unterschiedlich schnell. 
Der Akkuhalter fasst 10 AA Akkus, ergibt 12V. Gesteuert werden soll das Fahrzeug mal mit einem AVR Controller.
