---
layout: "image"
title: "Lenkung"
date: "2011-05-23T20:18:11"
picture: "kettenfahrzuge4.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30633
- /details9b8e.html
imported:
- "2019"
_4images_image_id: "30633"
_4images_cat_id: "2283"
_4images_user_id: "453"
_4images_image_date: "2011-05-23T20:18:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30633 -->
