---
layout: "image"
title: "Rups-27"
date: "2015-06-26T19:36:40"
picture: "raupen26.jpg"
weight: "26"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41304
- /details9370.html
imported:
- "2019"
_4images_image_id: "41304"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41304 -->
Ich Habe den Weichen Raupenbelag gewählt weil Ich vor allem Traktion wollte.