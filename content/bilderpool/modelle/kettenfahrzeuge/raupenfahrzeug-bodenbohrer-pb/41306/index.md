---
layout: "image"
title: "Rups-29"
date: "2015-06-26T19:36:40"
picture: "raupen28.jpg"
weight: "28"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41306
- /detailsae00.html
imported:
- "2019"
_4images_image_id: "41306"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41306 -->
