---
layout: "image"
title: "Rups-35"
date: "2015-06-26T19:36:40"
picture: "raupen34.jpg"
weight: "34"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41312
- /details170c.html
imported:
- "2019"
_4images_image_id: "41312"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41312 -->
