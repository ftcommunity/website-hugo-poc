---
layout: "image"
title: "Rups-38"
date: "2015-06-26T19:36:40"
picture: "raupen37.jpg"
weight: "37"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41315
- /details6e87.html
imported:
- "2019"
_4images_image_id: "41315"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41315 -->
