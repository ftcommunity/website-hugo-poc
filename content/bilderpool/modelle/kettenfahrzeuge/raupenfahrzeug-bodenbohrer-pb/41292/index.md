---
layout: "image"
title: "Rups-15"
date: "2015-06-26T19:36:39"
picture: "raupen14.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41292
- /details8d43.html
imported:
- "2019"
_4images_image_id: "41292"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41292 -->
