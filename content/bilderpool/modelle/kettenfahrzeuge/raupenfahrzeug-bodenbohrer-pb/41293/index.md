---
layout: "image"
title: "Rups-16"
date: "2015-06-26T19:36:39"
picture: "raupen15.jpg"
weight: "15"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41293
- /detailsd408.html
imported:
- "2019"
_4images_image_id: "41293"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41293 -->
Aufbau des Motorblocks. Damit das Modell nicht zu breit wurde, hab Ich Kegelräder benutzen mussen. Um die nicht überspringen zu lassen, sollte das Motorblock sehr stark und massif sein. Sonst: krrrr, au an den Ohren. Und an den Kegelrädern...