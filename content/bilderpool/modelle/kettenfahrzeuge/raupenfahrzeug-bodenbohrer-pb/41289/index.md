---
layout: "image"
title: "Rups-12"
date: "2015-06-26T19:36:39"
picture: "raupen11.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
uploadBy: "PB"
license: "unknown"
legacy_id:
- /php/details/41289
- /details6d1c-2.html
imported:
- "2019"
_4images_image_id: "41289"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41289 -->
