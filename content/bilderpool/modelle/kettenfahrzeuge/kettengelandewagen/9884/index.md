---
layout: "image"
title: "Kettengeländewagen+Interface"
date: "2007-04-02T20:48:16"
picture: "gelaendewagen3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9884
- /details7e67.html
imported:
- "2019"
_4images_image_id: "9884"
_4images_cat_id: "894"
_4images_user_id: "557"
_4images_image_date: "2007-04-02T20:48:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9884 -->
Hinten Viele Verstrebungen, da sich der Roboter sonst verdreht und verkantet