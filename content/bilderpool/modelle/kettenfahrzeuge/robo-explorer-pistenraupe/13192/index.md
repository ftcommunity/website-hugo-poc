---
layout: "image"
title: "Pistenraupe 4"
date: "2008-01-01T09:40:37"
picture: "roboexplorerpistenraupe13.jpg"
weight: "13"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- /php/details/13192
- /detailsaae7.html
imported:
- "2019"
_4images_image_id: "13192"
_4images_cat_id: "1191"
_4images_user_id: "642"
_4images_image_date: "2008-01-01T09:40:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13192 -->
