---
layout: "image"
title: "Greifantrieb 2"
date: "2005-08-21T20:36:53"
picture: "Kettenfahrwerk_mit_Arm_006.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4622
- /details8a2c.html
imported:
- "2019"
_4images_image_id: "4622"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:36:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4622 -->
Hier dasselbe von der anderen Seite.