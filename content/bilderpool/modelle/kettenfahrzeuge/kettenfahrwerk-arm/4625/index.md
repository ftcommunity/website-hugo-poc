---
layout: "image"
title: "Der Greifer 1"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_009.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4625
- /detailsc87a.html
imported:
- "2019"
_4images_image_id: "4625"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4625 -->
Hier schließlich kommt die zweite Längsachse am Greifer an. Wieder liegt ein Winkelzahnrad auf der anderen Seite wie im Mittelgelenk, damit auch hier die Position des Greifers von Armbewegungen unbeeindruckt bleibt.