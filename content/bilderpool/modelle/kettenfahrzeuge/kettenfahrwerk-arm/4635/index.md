---
layout: "image"
title: "Akkutausch 2"
date: "2005-08-21T20:54:42"
picture: "Kettenfahrwerk_mit_Arm_019.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4635
- /details4d17-2.html
imported:
- "2019"
_4images_image_id: "4635"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4635 -->
... und gibt den Zugriff auf den Akku frei, ...