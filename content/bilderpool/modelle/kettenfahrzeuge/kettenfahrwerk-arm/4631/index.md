---
layout: "image"
title: "Armpositionen 5"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_015.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4631
- /details4b5d.html
imported:
- "2019"
_4images_image_id: "4631"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4631 -->
... und natürlich auch jede beliebige andere Stellung ist möglich, denn die Armsegmente sind ja unabhängig voneinander.