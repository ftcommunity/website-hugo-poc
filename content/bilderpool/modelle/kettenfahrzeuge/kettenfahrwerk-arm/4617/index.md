---
layout: "image"
title: "Kettenfahrwerk mit Arm"
date: "2005-08-21T20:29:05"
picture: "Kettenfahrwerk_mit_Arm_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4617
- /details3af7.html
imported:
- "2019"
_4images_image_id: "4617"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:29:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4617 -->
Hier nun die Ergänzung zum Kettenfahrwerk um den Arm (siehe Fahrwerke/Prototypen). Zum bereits enthaltenen IR-Empfänger für Fahrmotor, Lenkmotor und Lampen kam der zweite: Zwei Motoren für die Armbewegungen und einer für den Greifer bzw. die alternativ anbaubare Schaufel (siehe späteres Bild).