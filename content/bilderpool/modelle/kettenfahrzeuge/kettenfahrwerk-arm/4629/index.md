---
layout: "image"
title: "Armpositionen 3"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_013.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4629
- /details0fb5.html
imported:
- "2019"
_4images_image_id: "4629"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4629 -->
Hier mal beide Armsegmente etwas nach unten. Das innere könnte noch etwas weiter herunter, bis es am IR-Empfänger anstößt. Das äußere kann auch dann noch ganz nach unten gefahren werden (z. B. über einer Tischkante). Und immer ist der Greifer voll steuerbar, bleibt aber bei den Armbewegungen selbst in konstanter Stellung.