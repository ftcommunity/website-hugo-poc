---
layout: "image"
title: "Armpositionen 2"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_012.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4628
- /details8803-2.html
imported:
- "2019"
_4images_image_id: "4628"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4628 -->
Einmal durchstrecken bitte: Beide Armsegmente waagerecht.