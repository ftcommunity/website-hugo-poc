---
layout: "image"
title: "Kettwiesl 2013 - Maschinenraum von unten"
date: "2013-07-06T16:01:17"
picture: "P1010274.jpg"
weight: "18"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk", "Gleichlaufgetriebe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37149
- /detailsa882-2.html
imported:
- "2019"
_4images_image_id: "37149"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37149 -->
Hier liegt das Kettwiesl auf dem Rücken und gibt den Blick in den Maschinenraum frei. In der linken Hälfte residiert das Überlagerungslenkgetriebe (= Gleichlaufgetriebe); die drei Z10-Ritzel sind unter der Bauplatte 75x15 versteckt. Rechts ist der Motor (32293) für den Hubantrieb. Und wenn man ganz genau hinsieht lugt neben dem U-Getriebe noch ein kleines bißchen vom Inkrementalgeber (Taster) für die Hubhöhe heraus. Der ist so versteckt, daß er auf keinem einzigen Bild wirklich zu sehen ist.

Natürlich sind im Gelände Abdeckungen drauf, sonst kommt der ganze Schmutz hinein und blockiert die Zahnräder.