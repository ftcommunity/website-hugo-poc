---
layout: "image"
title: "Kettwiesl 2013 - Hubmechanik Detail links"
date: "2013-07-06T16:01:17"
picture: "P1010268.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37145
- /details4a2d.html
imported:
- "2019"
_4images_image_id: "37145"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37145 -->
Seilzug als Getriebe. Untersetzung 4:1 und das schön platzsparend, optimal für den Zweck hier.