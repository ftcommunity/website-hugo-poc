---
layout: "image"
title: "Kettenfahrwerk noch ohne Mähwerk"
date: "2012-12-20T17:12:15"
picture: "Kettwiesl_Mechanik.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kettenfahrzeug", "Kettenfahrwerk", "Kettwiesl", "Rasenmäher"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36334
- /detailsf5f9.html
imported:
- "2019"
_4images_image_id: "36334"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36334 -->
Das Kettenfahrwerk ist als universeller Nutzlastträger konzipiert. Zwei Ketten, je 4 voll gefederte Laufrollen (ca. 20mm Federweg), die vordere Umlenkrolle dient als Kettenspanner zum Geometrieausgleich. Im Triebwerk (ein Gleichlaufgetriebe) arbeiten nicht-ft-Motoren aus alten Druckern - wegen der höheren Leistung.

Das Ganze ist auch noch modular aufgebaut, so daß Änderungen schnell durchgeführt werden können (z.B. längere Ketten mit mehr Laufrollen).