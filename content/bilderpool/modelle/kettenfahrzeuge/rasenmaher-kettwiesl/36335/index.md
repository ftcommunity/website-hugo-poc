---
layout: "image"
title: "Kettwiesl als Rasenmäher"
date: "2012-12-20T17:12:15"
picture: "Kettwiesl_Rasenmaeher.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/36335
- /details6db0.html
imported:
- "2019"
_4images_image_id: "36335"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36335 -->
Die Nutzlast "Mähwerk" ist vorne zwischen den Längsträgern des Kettwiesl eingehängt. Ein recht schlanker und doch erstaunlich stabiler Aufbau. Der obere Querträger wird ausschlißelich von den beiden 170mm Wellen rechts und links getragen. Die Höhenverstellung funktioniert einfach per Seilzug, wobei die erwähnten 170mm Wellen als Führung dienen. Ein oberer Endschalter gibt die Nullage vor. Die Schnitthöhe wird dann per Impulsgeber über die Umdrehungen der Seiltrommel eingestelt.