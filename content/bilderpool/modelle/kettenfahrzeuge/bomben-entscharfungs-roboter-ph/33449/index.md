---
layout: "image"
title: "Greifanrm Steuerung"
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter21.jpg"
weight: "21"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33449
- /detailsf70e.html
imported:
- "2019"
_4images_image_id: "33449"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33449 -->
...