---
layout: "image"
title: "Schwenkantreib für den Arm"
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter15.jpg"
weight: "15"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33443
- /details4ffd-2.html
imported:
- "2019"
_4images_image_id: "33443"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33443 -->
von Vorne.