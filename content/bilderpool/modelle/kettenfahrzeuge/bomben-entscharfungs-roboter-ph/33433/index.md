---
layout: "image"
title: "Antrieb für den Greifer"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter05.jpg"
weight: "5"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33433
- /detailsb053.html
imported:
- "2019"
_4images_image_id: "33433"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33433 -->
über zwei Schnecken