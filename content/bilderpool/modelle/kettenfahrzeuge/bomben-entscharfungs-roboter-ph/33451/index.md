---
layout: "image"
title: "An Anschuluss für die Steuerung."
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter23.jpg"
weight: "23"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33451
- /details90b8.html
imported:
- "2019"
_4images_image_id: "33451"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33451 -->
Nur als kompaktes Kabel nicht über Lan.