---
layout: "image"
title: "Schnecke Gesamt"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter06.jpg"
weight: "6"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33434
- /detailse593.html
imported:
- "2019"
_4images_image_id: "33434"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33434 -->
Der obere Moter ist nicht der Abtrieb hierfür.