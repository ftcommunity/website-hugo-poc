---
layout: "image"
title: "Kamera"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter11.jpg"
weight: "11"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/33439
- /details03c5.html
imported:
- "2019"
_4images_image_id: "33439"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33439 -->
Motor