---
layout: "image"
title: "bully47.jpg"
date: "2012-03-16T09:48:06"
picture: "bully47.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34650
- /details27df.html
imported:
- "2019"
_4images_image_id: "34650"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-03-16T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34650 -->
Das sollte eigentlich eine Pistenraupe werden, deswegen ist das Teil um ein oder zwei Laufrollenpaare zu kurz, und eine "Wanne" ist es auch nicht. Ein P-Motor besorgt den Antrieb, gelenkt wird mit einem Servo, der wahlweise das Differenzial sperrt oder eine Kette stillsetzt. Die Federung erfolgt mit zwei Stück "Hosengummi" aus Muttis Schneiderkiste.
