---
layout: "image"
title: "bully68.JPG"
date: "2012-03-16T10:12:10"
picture: "bully68.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34654
- /details416a.html
imported:
- "2019"
_4images_image_id: "34654"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-03-16T10:12:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34654 -->
Das Fahrwerk von unten. Die Gummizüge sind geradeaus durch alle Tragarme hindurch gefädelt. Am Heck (im Bild oben) fehlte ein fester Punkt, deshalb steckt dort noch eine Messingachse über die ganze Fahrzeugbreite drin. Am Bug wird das Gummi durch die schwarze Bauplatte auf die Oberseite gefädelt (im Bild unten rechts) und dort mit Verstellmöglichkeit arretiert.
