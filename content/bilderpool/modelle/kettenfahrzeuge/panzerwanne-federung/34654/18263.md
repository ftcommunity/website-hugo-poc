---
layout: "comment"
hidden: true
title: "18263"
date: "2013-08-28T11:02:13"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo,
welche Fischertechnik-Teile sind die Laufräder (mit dem Gummi) eigentlich genau? Die kenne ich nicht. Sind das Standardteile oder gehören die zu einem Bausatz?  Ich bin selbst auch gerade dabei, ein Kettenfahrzeug zu bauen. Als Laufräder hab ich Seilrollen verwendet. Aber Laufräder mit Gummi würden bestimmt schöner laufen!
Danke und Gruß
Richard