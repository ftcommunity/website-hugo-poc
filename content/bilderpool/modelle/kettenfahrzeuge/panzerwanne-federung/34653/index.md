---
layout: "image"
title: "bully65.JPG"
date: "2012-03-16T10:07:25"
picture: "bully65.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/34653
- /details65cb.html
imported:
- "2019"
_4images_image_id: "34653"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-03-16T10:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34653 -->
Hinten sind gleich vier Kugellager in Schneckenmuttern verbaut. Der Servo schiebt über den Seilzug die Achse mit den vier Z15 in die Positionen links - mitte - rechts. In der Lage "mitte" sind die inneren Z15 im Eingriff mit den Z20 und bilden damit eine starre Kupplung für den Geradeauslauf. Zu sehen ist die Position "links", wo die linke Kette frei ist und die rechte Kette blockiert wird, indem das Z15 links außen auf ein (noch nicht existierendes) Sperr-Element geschoben wird. 

Es darf nur während der Fahrt gelenkt werden. Wenn nämlich Zahn (von einem Z15) direkt vor Zahn (von einem Z20) steht, passiert rein gar nichts. Damit in diesem Fall das "rein gar nichts" auch für den Servo gilt, ist er auf Federn gelagert und kann nachgeben, bis irgendwann die Zahnräder passend stehen.
