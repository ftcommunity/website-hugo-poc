---
layout: "image"
title: "Pz-Wanne02.JPG"
date: "2005-10-31T20:56:43"
picture: "Pz-Wanne02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Panzer", "Wanne", "Fahrgestell"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5168
- /details808e.html
imported:
- "2019"
_4images_image_id: "5168"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T20:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5168 -->
Die Wanne von unten gesehen. Die Kette verläuft hinten sehr steil, aber das muss noch geändert werden.

Die Laufrollen sitzen lose auf je einer Achse 110. Diese Achsen stecken über Klemmbuchsen in den schwarzen Statiksteinen. Diese Verbindung ist flexibel genug, um das Einfedern der Laufrollen zu ermöglichen. Die Streben 120-Loch sollen ein Herausrutschen der Achsen bei Kurvenfahrt verhindern, aber ob das ausreicht, wird sich noch zeigen müssen.
