---
layout: "image"
title: "Sieben Segmentanzeigen"
date: "2011-01-26T15:11:17"
picture: "explorer09.jpg"
weight: "9"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/29810
- /detailsfc35.html
imported:
- "2019"
_4images_image_id: "29810"
_4images_cat_id: "2193"
_4images_user_id: "1275"
_4images_image_date: "2011-01-26T15:11:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29810 -->
Sonderknöpfe:
Licht,
Hupe,
Kamera hoch,
Kamera runter,
und 4 weitere.