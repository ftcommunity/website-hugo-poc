---
layout: "image"
title: "Bedienpult"
date: "2011-01-26T15:11:17"
picture: "explorer07.jpg"
weight: "7"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/29808
- /details5605.html
imported:
- "2019"
_4images_image_id: "29808"
_4images_cat_id: "2193"
_4images_user_id: "1275"
_4images_image_date: "2011-01-26T15:11:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29808 -->
Rechts:
Zwei Geschwindigkeitstufen (Vor 2, Zurück2)
Und Lichttaster
Links:
"Lenkrad"