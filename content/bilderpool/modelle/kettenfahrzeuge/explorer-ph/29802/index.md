---
layout: "image"
title: "Gesamtansicht(Vorne)"
date: "2011-01-26T15:11:16"
picture: "explorer01.jpg"
weight: "1"
konstrukteure: 
- "PH"
fotografen:
- "PH"
uploadBy: "PH"
license: "unknown"
legacy_id:
- /php/details/29802
- /detailsc9a4.html
imported:
- "2019"
_4images_image_id: "29802"
_4images_cat_id: "2193"
_4images_user_id: "1275"
_4images_image_date: "2011-01-26T15:11:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29802 -->
Oben ist die Kamera zusehen. Die Kamera ist nach oben und unten Beweglich.

Die hellen Punkte sind Leds.