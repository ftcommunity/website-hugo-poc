---
layout: "image"
title: "Lenkung1"
date: "2014-02-05T12:24:18"
picture: "haegglund22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- /php/details/38188
- /detailsefa5.html
imported:
- "2019"
_4images_image_id: "38188"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38188 -->
Die Lenkung erfolgt durch zwei "Gewindezylinder" die durch einen XM-Motor angetrieben werden.