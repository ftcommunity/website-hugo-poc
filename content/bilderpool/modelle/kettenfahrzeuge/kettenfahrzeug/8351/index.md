---
layout: "image"
title: "Vorderansicht"
date: "2007-01-08T18:01:30"
picture: "Kettenfahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/8351
- /details54e0.html
imported:
- "2019"
_4images_image_id: "8351"
_4images_cat_id: "769"
_4images_user_id: "456"
_4images_image_date: "2007-01-08T18:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8351 -->
Hier sieht man es von vorne. Für eine Expedition im Dunkeln braucht man natürlich Lichter.
