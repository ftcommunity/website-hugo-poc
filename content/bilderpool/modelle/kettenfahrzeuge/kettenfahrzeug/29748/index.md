---
layout: "image"
title: "Einfaches Kettenfahrzeug"
date: "2011-01-22T16:48:49"
picture: "Kettenfahrzeug_front_01.jpg"
weight: "6"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29748
- /details1ee3.html
imported:
- "2019"
_4images_image_id: "29748"
_4images_cat_id: "769"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29748 -->
zu sehen ist ein einfaches kettenfahrzeug mit dem ft controllset
