---
layout: "image"
title: "Gleichlaufgetriebe"
date: "2007-03-10T15:40:50"
picture: "Gleichlaufgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9364
- /details78aa.html
imported:
- "2019"
_4images_image_id: "9364"
_4images_cat_id: "864"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T15:40:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9364 -->
Das ist ein Gleichlaufgetriebe. Es ist dem Gleichlaufgetriebe von steffalk sehr ähnlich, weil ich ursprünglich auch das besagte bauen wollte. Nur hab ich es nach etwas Tüfftelei verbessert. Ich habe die 3 Z10 und das Z20 durch ein Z15 und ein Z30 ersetzt. Das ist sehr praktisch, weil es nicht so kompliziert ist und die 2:1 Untersetzung bleibt immer noch erhalten. Die naächste Fotos zeigen alles im Detail.
