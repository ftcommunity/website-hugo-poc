---
layout: "image"
title: "Gleichlauf06-04.JPG"
date: "2006-05-05T19:23:28"
picture: "Gleichlauf06-04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6209
- /detailsd942.html
imported:
- "2019"
_4images_image_id: "6209"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:23:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6209 -->
