---
layout: "image"
title: "Gleichlauf03-01.JPG"
date: "2005-08-20T19:32:55"
picture: "Gleichlauf03-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Gleichlauf", "Sigma-Delta", "Summe-Differenz", "Panzer", "Fly-by-Wire"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4607
- /details23d0.html
imported:
- "2019"
_4images_image_id: "4607"
_4images_cat_id: "618"
_4images_user_id: "4"
_4images_image_date: "2005-08-20T19:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4607 -->
Hier sind die Teile versammelt für Gleichlaufgetriebe Type 3. Die beiden S-Motoren sind übereinander montiert; die Z15 passen gerade so am Motorgehäuse vorbei.
