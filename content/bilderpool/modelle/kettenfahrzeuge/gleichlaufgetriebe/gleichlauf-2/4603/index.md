---
layout: "image"
title: "Gleichlauf02-01.JPG"
date: "2005-08-20T19:32:55"
picture: "Gleichlauf02-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Gleichlauf", "Sigma-Delta", "Summe-Differenz", "Panzer", "Fly-by-Wire"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4603
- /detailsd5b7.html
imported:
- "2019"
_4images_image_id: "4603"
_4images_cat_id: "617"
_4images_user_id: "4"
_4images_image_date: "2005-08-20T19:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4603 -->
Das Getriebe hier ist zwar nicht für einen Panzer gedacht, aber die Funktion ist die gleiche: Es gibt einen Motor für exakten Gleichlauf-Antrieb und einen für exakten Umgekehrt-Gleichlauf-Antrieb. 

Wenn man ein Getriebe braucht, 
- das zwei Eingangswellen hat und zwei Ausgangswellen,
- und es soll am einen Ausgang die SUMME der Drehwinkel der Eingangswellen herauskommen,
- und es soll am anderen Ausgang die DIFFERENZ der Drehwinkel herauskommen,

dann führt das auf genau dasselbe Getriebe. Man muss sich nur mit sich selbst einigen, welchen Anschluss man wie herum definiert.

(Wer sich mit Hohlleitern für Mikrowellen auskennt: ein "Magic-T" erfüllt genau die gleiche Funktion, aber mit elektromagnetischen Wellen)

Im vorliegenden Fall brauche ich eine sehr starke Untersetzung, daher die vielen verbauten Schnecken. Als Summen- und Differenzausgänge definiere ich die Metallachsen, die hier nach links weg zeigen. Die Ausgänge für "Links" und "Rechts" liegen unter den Motoren und zeigen nach rechts weg.
