---
layout: "image"
title: "Gleichlauf02-02.JPG"
date: "2005-08-20T19:32:55"
picture: "Gleichlauf02-02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Gleichlauf", "Sigma-Delta", "Summe-Differenz", "Panzer", "Fly-by-Wire"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4604
- /detailsdc5f.html
imported:
- "2019"
_4images_image_id: "4604"
_4images_cat_id: "617"
_4images_user_id: "4"
_4images_image_date: "2005-08-20T19:32:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4604 -->
Au weia, die Beschreibung von Bild 1 gehört HIERHER (hatte das falsch Bild angezeigt). Also: 
nach links weg (Metallachsen) : Summe / Differenz;
nach rechts weg (unter Motoren) : Linker / Rechter Ausgang

Einen BS 7,5 habe ich halbiert, sonst wäre er am Kegelkranz des Differenzials angestoßen.