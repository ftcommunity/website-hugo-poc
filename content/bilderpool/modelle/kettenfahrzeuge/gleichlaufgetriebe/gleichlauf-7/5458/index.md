---
layout: "image"
title: "Baustein-15 (32064) mit Bohrung + 2st Kugellager Conrad 214426  (4x4x9mm)"
date: "2005-12-09T19:51:42"
picture: "FT-Antrieb_007.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5458
- /details254d.html
imported:
- "2019"
_4images_image_id: "5458"
_4images_cat_id: "622"
_4images_user_id: "22"
_4images_image_date: "2005-12-09T19:51:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5458 -->
