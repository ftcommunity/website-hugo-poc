---
layout: "comment"
hidden: true
title: "9024"
date: "2009-04-19T17:27:19"
uploadBy:
- "Mr Smith"
license: "unknown"
imported:
- "2019"
---
An die ausgehenden Achsen werden dann die Antriebszahnräder angeschlossen.

Entweder man nimmt die Achsen links/oben und unten oder rechts/oben und unten.