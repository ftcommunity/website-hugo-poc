---
layout: "comment"
hidden: true
title: "9025"
date: "2009-04-19T18:42:36"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo!
So ganz blick ich noch nicht durch, was da genau passiert. Aber eins fällt mir gerade auf: die Datenfelder "Konstrukteur" und "Fotograf" sind dafür gedacht, dass man mal darin suchen kann. Wenn jeder da "ich" einträgt, wird das nicht so den Erfolg bringen ;-)

Gruß,
Harald