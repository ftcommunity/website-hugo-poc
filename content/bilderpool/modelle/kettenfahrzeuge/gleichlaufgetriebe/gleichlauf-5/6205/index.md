---
layout: "image"
title: "Gleichlauf05-04.JPG"
date: "2006-05-05T19:15:03"
picture: "Gleichlauf05-04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6205
- /detailsfe58.html
imported:
- "2019"
_4images_image_id: "6205"
_4images_cat_id: "620"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:15:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6205 -->
