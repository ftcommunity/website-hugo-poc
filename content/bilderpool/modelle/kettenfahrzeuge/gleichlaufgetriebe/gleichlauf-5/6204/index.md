---
layout: "image"
title: "Gleichlauf05-03.JPG"
date: "2006-05-05T19:14:30"
picture: "Gleichlauf05-03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6204
- /detailsccb2.html
imported:
- "2019"
_4images_image_id: "6204"
_4images_cat_id: "620"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:14:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6204 -->
