---
layout: "image"
title: "Gleichlauf05-01.JPG"
date: "2006-05-05T19:10:09"
picture: "Gleichlauf05-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6202
- /details8644.html
imported:
- "2019"
_4images_image_id: "6202"
_4images_cat_id: "620"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:10:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6202 -->
Etwas schön kompaktes, allerdings wurden die Antriebsräder 31414 hinter dem Kegelansatz abgedreht. Es geht auch mit den Originalteilen, aber dann wird das Getriebe breiter und neigt zum rattern.

Die Streben 22,5 sind selbstgemacht (Plastik 2mm stark, mit 4mm Loch).
