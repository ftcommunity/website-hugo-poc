---
layout: "image"
title: "Gleichlaufgetriebe Abtriebsseite"
date: "2014-06-23T18:54:09"
picture: "gleichlaufvariante1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38972
- /details881c.html
imported:
- "2019"
_4images_image_id: "38972"
_4images_cat_id: "2917"
_4images_user_id: "1729"
_4images_image_date: "2014-06-23T18:54:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38972 -->
Dies ist das zentrale Gleichlaufgetriebe.
Alles was man braucht, sind 2 Differentiale, ein paar Bausteine 7,5 und 15 (mit Loch) bzw. Schneckenmuttern mit Kugellager
eine Achse und ein paar Zahnräder.

Für mich war es wichtig, möglichst wenig Platz in der Höhe und in der Breite zu benötigen, da ich damit eine sehr flache Panzerwanne realisieren möchte.