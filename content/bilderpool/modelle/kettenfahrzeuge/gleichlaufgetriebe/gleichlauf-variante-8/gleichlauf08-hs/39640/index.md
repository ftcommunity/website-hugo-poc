---
layout: "image"
title: "Gleichlauf08_4"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39640
- /detailsf1cd-2.html
imported:
- "2019"
_4images_image_id: "39640"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39640 -->
Die Variante mit 2x15° in der Wanne der Pistenraupe ( http://ftcommunity.de/categories.php?cat_id=2958 ), die um 30 mm verbreitert wurde. Die Kronenräder passen trotzdem noch nicht hinein.
