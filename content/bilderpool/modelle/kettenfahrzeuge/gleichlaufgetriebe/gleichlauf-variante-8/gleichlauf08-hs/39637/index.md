---
layout: "image"
title: "Gleichlauf08_1"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39637
- /detailsf04d.html
imported:
- "2019"
_4images_image_id: "39637"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39637 -->
Ich habe hier das Gleichlaufgetriebe Nr.8 von NBGer zum kompletten Antriebsblock für ein Kettenfahrzeug erweitert. Der Block besteht aus Mittelteil mit den Differenzialen, und daran gelenkig angebaut die beiden Motoren für gegensinnigen und gleichsinnigen Antrieb der Aussgänge. Die vielen Gelenksteine erleichtern den Zusammenbau, außerdem kann der Block ganz einfach an die Einbauverhältnisse angepasst werden: zwischen "breit, aber flach" und "hoch, aber schmal" kann stufenlos jede Zwischenform eingenommen werden.

Hier ist eine mittlere Variante (2x15°-Winkelsteine) beidseitig aufgefaltet: links der Motor für den gegensinnigen Eingang, dessen Z10 die Kette (hier abgenommen) antreibt. Der rechte Motor (gleichsinniger Eingang) treibt über ein frei laufendes Z20 (einteilig schwarz) die beiden Z20 im Mittelteil an. Seine Steckbuchsen sind nur noch mit blanken Kabelenden zu erreichen.

Die Ausgänge (Klemm-Z10 in der Mitte) werden 1:3 untersetzt und können dann mit Kegel- oder Kronenrädern auf die Ketten führen.

Zum Öffnen und Schließen verschiebt man die vier Winkel 15° in Längsrichtung. Je nach Aufbauvariante (breit/schmal) werden an dieser Stelle andere Bauteile eingesetzt
