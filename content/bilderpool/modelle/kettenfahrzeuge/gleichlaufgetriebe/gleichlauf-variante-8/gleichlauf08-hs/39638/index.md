---
layout: "image"
title: "Gleichlauf08_2"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39638
- /details7ef2.html
imported:
- "2019"
_4images_image_id: "39638"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39638 -->
Alle Gelenksteine und durchgehenden Achsen ergeben einen symmetrischen Aufbau. Nur die Motoren sind etwas unterschiedlich angebaut: der "gegensinnige" hier links wird über die Federleiste in seiner unteren Gehäusemitte gelagert (damit der Abstand zwischen Z10 und Z20 stimmt); der "gleichsinnige" auf der rechten Seite wird in einer seitlichen Nut gelagert und kommt daher 7,5 mm weiter vom Mittelteil weg. Dummerweise sind die Buchsen des "gleichsinnigen" Motors nicht mehr mit Steckern zugänglich -- da muss man mit blanken Kabelenden arbeiten.
