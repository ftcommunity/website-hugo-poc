---
layout: "image"
title: "Gleichlauf08_7"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs7.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39643
- /details24eb.html
imported:
- "2019"
_4images_image_id: "39643"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39643 -->
Das ist die Variante 0x15°. Die aufrecht stehenden BS 7,5 gibt es nur auf der rechten Seite. Der Mittelblock hat zusätzliche BS7,5 bekommen, damit der Abstand für das Z20 auf der Stirnseite stimmt. Der Antriebsblock wird schmal, aber hoch.
