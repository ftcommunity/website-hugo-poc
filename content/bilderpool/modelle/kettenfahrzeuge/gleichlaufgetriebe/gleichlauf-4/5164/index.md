---
layout: "image"
title: "Gleichlauf04-01.JPG"
date: "2005-10-31T20:56:42"
picture: "Gleichlauf04-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Sigma-Delta", "Gleichlauf"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5164
- /details4bac.html
imported:
- "2019"
_4images_image_id: "5164"
_4images_cat_id: "619"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T20:56:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5164 -->
Zu den Entwürfen von MarMac (
http://www.ftcommunity.de/categories.php?cat_id=404 )
und Schnaggels ( http://www.ftcommunity.de/categories.php?cat_id=297 ), bei denen die Motoren längs eingebaut sind,
gibt es hiermit noch eine weitere Variante. Ziel war es, die Motoren quer zur Fahrtrichtung zu legen, um möglichst wenig von der verfügbaren Baulänge im Inneren zu verbrauchen.

Die beiden Power-Mots liegen hier übereinander, der untere treibt das Z20 (Mitte rechts) durch einen BS15-Loch hindurch an. Die Motoren sind mit je zwei Stahlstiften+Zapfen (aus kaputten BS15 mit Doppelzapfen) befestigt, von denen nur der oberste hier zu sehen ist.

Die beiden Differenziale liegen in der gleichen Flucht montiert. Das Z10 mittendrin treibt sie beide gleichsinnig an, es wird seinerseits vom oberen Power-Mot über 2x Z10 angetrieben.

Die Käfige (Z20) der Differenziale werden vom unteren Power-Mot gegensinnig angetrieben. Beim linken Differenzial erfolgt dies über die beiden Z20 auf roten Naben, die links unten zu sehen sind. 
Die Drehrichtungsumkehr für den Käfig des rechten Differenzials erfolgt über einen Satz von drei Kegelzahnrädern, der ... in einem weiteren Differenzial verpackt ist. Dieses Differenzial ist eins von den 'alten', mit Z15-Käfig und Metallachsen. Der Käfig ist durch die Bauplatte 15x60 blockiert, deshalb wirkt dieses Differenzial nur als Drehrichtungsumkehr.

Die Gleisketten des Fahrzeugs werden von den Z10 ganz außen angetrieben, ggf. über eine weitere Untersetzung.
