---
layout: "image"
title: "Gleichlauf04-02.JPG"
date: "2005-10-31T20:56:43"
picture: "Gleichlauf04-02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Sigma-Delta", "Gleichlauf"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5165
- /details0746.html
imported:
- "2019"
_4images_image_id: "5165"
_4images_cat_id: "619"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T20:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5165 -->
Der Antriebsstrang für den Lenkmotor, der die Ketten gegensinnig antreibt.

In Bildmitte befindet sich das 'alte' Differenzial, dessen Käfig blockiert ist und das nun zur Drehrichtungsumkehrung zwischen linker und rechter Seite dient. Die Käfige der beiden Rast-Differenziale werden einmal direkt (linke Seite) und einmal in umgekehrter Richtung (rechte Seite) angetrieben.
