---
layout: "image"
title: "Seitenansicht Antriebseinheit"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante6.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38981
- /detailsf5fb.html
imported:
- "2019"
_4images_image_id: "38981"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38981 -->
Die gesamte Antriebseinheit hat nach wie vor einen geringen Höhenbedarf.
(Die schwarzen Steine im Bild sind nur Füße für Demonstration und Testbetrieb)