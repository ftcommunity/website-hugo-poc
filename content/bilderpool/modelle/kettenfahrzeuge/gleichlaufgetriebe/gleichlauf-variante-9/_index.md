---
layout: "overview"
title: "Gleichlauf (Variante 9)"
date: 2020-02-22T08:36:04+01:00
legacy_id:
- /php/categories/2918
- /categoriesd687.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2918 --> 
Ein Nachtrag bzw. eine Variante der Variante 8, die ich bereits gepostet habe:
http://ftcommunity.de/categories.php?cat_id=2917

Dies war eigentlich meine erste Version des Gleichlaufgetriebes, Die Version 8 gefiel mir aber besser, da sie im Gesamtaufbau mit Motoren usw. symmetrisch ist.
Jetzt bin ich aber nochmal auf diese Version zurückgekommen, weil sie einen besonderen Vorteil bietet, den ich bei den entsprechenden Bildern im Detail beschreiben werde.

Generell ist diese Version fast gleich zur Version 8.  Diese Getriebevariante baut sogar etwas schmaler, 
Der Hauptunterschied ist, daß die Einleitung für Hauptantrieb und Lenkungsüberlagerung zueinander vertauscht sind. 
