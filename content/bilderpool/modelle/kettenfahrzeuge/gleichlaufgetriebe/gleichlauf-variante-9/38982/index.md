---
layout: "image"
title: "Funktionsprinzip Antriebseinheit"
date: "2014-06-27T13:03:12"
picture: "Funktionsprinzip_Antriebseinheit_800x600.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Panzer", "Antrieb", "Gleichlaufgetriebe", "Überlagerungslenkgetriebe"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38982
- /detailsfed5.html
imported:
- "2019"
_4images_image_id: "38982"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-27T13:03:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38982 -->
Mit diesem Bild wird es klarer!

Ganz wichtig: An der gelben Linie sind die Achsen NICHT durchgängig. Sie haben aus Platzgründen lediglich den selben Baustein als Lager (jeweils ein Kugellager rechts und links)

Der orange Bereich gehört zum Überlagerungslenkgetriebe, der grüne Bereich zum Schaltgetriebe des Hauptantriebes. 
Dieses Schaltgetriebe wirkt nur auf den Hauptantrieb für Geradeausfahrt vorwärts/rückwärts.

Der Kraftfluß für den Hauptantrieb ist mit roten Pfeilen dargestellt. Im Schaltgetriebe gibt es 3 Übersetzungsvarianten je nach Getriebestellung (grüner Pfeil)
Der Kraftfluß für die Lenkungsüberlagerung ist mit blauen Pfeilen dargestellt. Das ganze war ja nur ein Versuchsaufbau, deswegen fehlt der Lenkmotor!