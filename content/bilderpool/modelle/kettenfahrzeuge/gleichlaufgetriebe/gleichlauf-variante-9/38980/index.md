---
layout: "image"
title: "Panzer-Antriebseinheit Draufsicht"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante5.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38980
- /detailseeb7.html
imported:
- "2019"
_4images_image_id: "38980"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38980 -->
An diesem Bild lässt sich die Funktionsweise erklären:
- Untere Achse rechts zur mittleren Achse rechts: Der Motor ist mit dem Schaltgetriebe mit der Zahnradkombination Z10->Z20 verbunden
- Mittlere Achse recht zur oberen Achse rechts: Je nach Getriebestellung ist die mittlere Achse mit der oberen Achse mit unterschiedlichen Übersetzungen verbunden
- obere Achse: Dies ist die Schaltachse und wird je nach Übersetzung verschoben. Im Gegensatz zu den unteren und mittleren Achsen ist diese durchgängig.
- Die Übertragung auf das Differential (Hauptantrieb) erfolgt über das Z15 oben. Dieses verschiebt sich je nach Schaltstellung. Die Länge des Differentials reicht genau aus, um alle Schaltstellungen abzudecken.
Das ist der eigentliche Grund warum ich dieses Gleichlaufdifferential verwende. Das Differentialgehäuse ist eine Zahnradwalze, über das das Antriebsrad verschoben werden kann.
Das Gleichlaufgetriebe selbst ist dann wie bereits beschrieben. Ich habe nur die Position der Lenkzahnräder in den Getrieberahmen verlegt, damit ich den Platz für das Schaltspiel an der Antriebsachse gewinne.

