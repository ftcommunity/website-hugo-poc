---
layout: "image"
title: "Motorblock für Wall-e 2.0"
date: "2011-05-23T19:39:54"
picture: "Raupenfahrzeug_mit_Gleichlaufgetriebe_Power-Motorblock_III.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30628
- /detailsc5a1.html
imported:
- "2019"
_4images_image_id: "30628"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-05-23T19:39:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30628 -->
Hier der Power-Motor-Block (Übersetzung 50:1) mit einer äußerst stabilen Verbindung - da zuckt nichts mehr.