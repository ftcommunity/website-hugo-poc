---
layout: "image"
title: "Kettenfahrwerk mit Gleichlaufgetriebe 'Classic'"
date: "2010-12-27T15:04:35"
picture: "Fahrwerk_mit_Gleichlaufgetriebe_Classic.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
schlagworte: ["Gleichlaufgetriebe", "Kettenfahrwerk"]
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/29546
- /detailsd095.html
imported:
- "2019"
_4images_image_id: "29546"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2010-12-27T15:04:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29546 -->
Hier mein "Gleichlaufgetriebe für Arme" integriert in ein kompaktes Kettenfahrwerk. Auch hier ist die Achse vorne geteilt - kaschiert durch den roten Baustein 15 mit Bohrung in der Mitte.