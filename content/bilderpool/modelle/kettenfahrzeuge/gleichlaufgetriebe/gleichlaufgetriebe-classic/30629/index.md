---
layout: "image"
title: "Wall-e 2.0 mit neuen Differentialen und Power-Motoren"
date: "2011-05-23T19:39:54"
picture: "Raupenfahrzeug_mit_Gleichlaufgetriebe_neue_Differentiale_mit_Power-Motoren_II.jpg"
weight: "10"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/30629
- /details3e7c.html
imported:
- "2019"
_4images_image_id: "30629"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-05-23T19:39:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30629 -->
Eine ziemlich "stürmische" Variante des Wall-e - fährt und dreht ausgesprochen fix.
Die Kippneigung lässt gegenüber Version 1.0 etwas nach.