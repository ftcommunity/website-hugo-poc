---
layout: "image"
title: "IR Pneumatik Raupen-Löffelbagger von DasKasperle - HINTEN"
date: "2013-05-26T09:50:17"
picture: "irpneumatikraupenloeffelbaggervondaskasperle05.jpg"
weight: "5"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36963
- /details4e33.html
imported:
- "2019"
_4images_image_id: "36963"
_4images_cat_id: "2748"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36963 -->
