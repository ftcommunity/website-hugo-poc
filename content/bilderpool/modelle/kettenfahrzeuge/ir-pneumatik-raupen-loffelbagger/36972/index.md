---
layout: "image"
title: "IR Pneumatik Raupen-Löffelbagger von DasKasperle - UNTEN"
date: "2013-05-26T09:50:17"
picture: "irpneumatikraupenloeffelbaggervondaskasperle14.jpg"
weight: "14"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/36972
- /detailsa167.html
imported:
- "2019"
_4images_image_id: "36972"
_4images_cat_id: "2748"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36972 -->
