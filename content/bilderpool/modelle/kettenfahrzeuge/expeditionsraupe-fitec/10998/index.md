---
layout: "image"
title: "Expeditionsraupe"
date: "2007-07-01T12:32:56"
picture: "Expeditionsraupe1.jpg"
weight: "1"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/10998
- /detailsd4b9.html
imported:
- "2019"
_4images_image_id: "10998"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10998 -->
Das ist meine Expeditionsraupe. Sie hat ein Gleichlaufgetriebe mit 2 50:1 Power-Motoren. Sie fährt sehr gut im Gelände ich werde 100% mal ein Video machen. Sie ähnelt etwas dem Robo Explorer.
