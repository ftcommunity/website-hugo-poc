---
layout: "image"
title: "Raupe im Paprikabeet"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe5.jpg"
weight: "5"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11003
- /details0e51.html
imported:
- "2019"
_4images_image_id: "11003"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11003 -->
Hier sieht man die Raupe im Paprikabeet. Man sieht auch die dreckigen Ketten. Falls ihr nicht glaubt, dass es dort fährt; es wird 100% ein Video geben.
