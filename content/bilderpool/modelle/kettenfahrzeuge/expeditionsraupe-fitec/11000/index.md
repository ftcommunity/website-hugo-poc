---
layout: "image"
title: "Unterseite"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe3.jpg"
weight: "3"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/11000
- /details7079.html
imported:
- "2019"
_4images_image_id: "11000"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11000 -->
Die Streben sind dazu da, damit es sich nicht durchbiegt, sprich; zur Stabiliesierung.
