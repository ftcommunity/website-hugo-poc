---
layout: "image"
title: "Räumrobot_001"
date: "2003-10-14T11:24:04"
picture: "rBot001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Räumroboter"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- /php/details/1825
- /details1d13.html
imported:
- "2019"
_4images_image_id: "1825"
_4images_cat_id: "194"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1825 -->
IR-Ferngesteuerter Räumroboter