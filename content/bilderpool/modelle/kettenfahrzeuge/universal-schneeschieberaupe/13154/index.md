---
layout: "image"
title: "Positions-Schaltung"
date: "2007-12-23T13:36:18"
picture: "Universal-Schneeschieberaupe10.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13154
- /details0566.html
imported:
- "2019"
_4images_image_id: "13154"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-23T13:36:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13154 -->
Hier eine Schaltung mit 2 Dioden. Ich habe sie von irgendeiner Fan-Seite nachgebaut. Ich werde also an beide Endpunkte der Hydraulik einen Taster anbringen, sodass der Motor bei erreichen des Endpunktes abgeschaltet wird und trotzdem noch in die andere Richtung steuerbar ist.
