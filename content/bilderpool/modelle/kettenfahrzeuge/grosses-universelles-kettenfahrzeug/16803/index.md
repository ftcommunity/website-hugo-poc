---
layout: "image"
title: "KF 2.11"
date: "2008-12-30T17:03:01"
picture: "IMG_2904.jpg"
weight: "11"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16803
- /detailsb11c-2.html
imported:
- "2019"
_4images_image_id: "16803"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T17:03:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16803 -->
Ein Schritt weiter