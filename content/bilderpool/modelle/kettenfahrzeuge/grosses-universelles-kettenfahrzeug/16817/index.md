---
layout: "image"
title: "KF 3.11"
date: "2008-12-31T08:21:19"
picture: "IMG_2933.jpg"
weight: "25"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16817
- /details4ad3.html
imported:
- "2019"
_4images_image_id: "16817"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-31T08:21:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16817 -->
Die Halbkugel nochmal in groß. Die sechste Strebe wurde ausgelassen, um Platz für die Mechanik der Kamera, die sich vertikal bewegen können soll, zu bieten.