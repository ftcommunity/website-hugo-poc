---
layout: "image"
title: "KF 3.5"
date: "2008-12-30T18:22:36"
picture: "IMG_2926.jpg"
weight: "16"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16808
- /detailsf906.html
imported:
- "2019"
_4images_image_id: "16808"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16808 -->
Oben Drauf kommt noch ein Greifer mit einer zusätzlichen Bewegungsrichtung und eine angedeutete Halbkugel, die mit Sensoren und einer Mini-Funkkamera bestückt wird. Bewegungsradius 360°. Die elektrische Verbindung zum Greifer übernimmt ein Floppy-Flachbandkabel.