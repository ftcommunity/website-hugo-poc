---
layout: "image"
title: "KF 3.3"
date: "2008-12-30T17:26:03"
picture: "IMG_2928.jpg"
weight: "14"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16806
- /details0503.html
imported:
- "2019"
_4images_image_id: "16806"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T17:26:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16806 -->
Laut Dokumentation von FT soll die Übersetzung der Winkelgetriebe für die alten Motoren 240:1 sein. Kann das jemand bestätigen? Für die Seilwinde habe ich zwei Getriebe hintereinander geschaltet, was theoretisch eine Übersetzung von 57600:1 ergibt. Korregiert mich, wenn ich mich irre. Für eine Seilwinde genug, denke ich.