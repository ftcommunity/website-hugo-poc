---
layout: "image"
title: "KF 3.7"
date: "2008-12-30T18:22:36"
picture: "Bild_007.jpg"
weight: "18"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16810
- /details164d-2.html
imported:
- "2019"
_4images_image_id: "16810"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16810 -->
Die Mini-Funkkamera. Das Bild ist zwar mäßig und die Reichweite unter aller Sau, aber für's erste reichts. Denkbar wäre das ganze als Spielzeug für meine Kinder, es könnte meine Pflanzen gießen, wenn ich im Urlaub bin, als Überwachungsanlage mit Webanbindung, oder einfach nur als Zeitvertreib.