---
layout: "image"
title: "KF 3.10"
date: "2008-12-30T19:18:00"
picture: "IMG_2932_2.jpg"
weight: "24"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16816
- /details9a31-2.html
imported:
- "2019"
_4images_image_id: "16816"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T19:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16816 -->
Mit drei Motoren fährt es schon. Für den vierten fehlen mir mal wieder Bauteile. Allerdings stellt ein dünnes Heftchen schon eine Schwierigkeit dar. Dabei blockieren aber nicht die Motoren, sondern die Achsen drehen halt durch.