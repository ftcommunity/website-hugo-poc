---
layout: "image"
title: "KF 2.8"
date: "2008-12-12T22:54:13"
picture: "IMG_2841.jpg"
weight: "8"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16596
- /detailsb937.html
imported:
- "2019"
_4images_image_id: "16596"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16596 -->
Einer der Antriebe. Auch der Motor überträgt auf zwei Ritzel. Insgesamt 4 Powermotoren mit den 50:1 Getrieben werden das Modell antreiben. Die Synchronisation übernimmt die Software.