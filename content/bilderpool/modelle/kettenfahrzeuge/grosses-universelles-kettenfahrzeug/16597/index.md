---
layout: "image"
title: "KF 2.9"
date: "2008-12-12T22:54:13"
picture: "IMG_2842.jpg"
weight: "9"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16597
- /details98be.html
imported:
- "2019"
_4images_image_id: "16597"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16597 -->
