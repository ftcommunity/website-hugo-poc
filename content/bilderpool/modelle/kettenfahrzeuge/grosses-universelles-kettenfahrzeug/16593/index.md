---
layout: "image"
title: "KF 2.7"
date: "2008-12-12T22:54:13"
picture: "IMG_2839.jpg"
weight: "7"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16593
- /details119d.html
imported:
- "2019"
_4images_image_id: "16593"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16593 -->
