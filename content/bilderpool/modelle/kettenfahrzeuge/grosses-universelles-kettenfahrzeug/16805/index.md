---
layout: "image"
title: "KF 3.2"
date: "2008-12-30T17:26:03"
picture: "IMG_2921.jpg"
weight: "13"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/16805
- /details2d08.html
imported:
- "2019"
_4images_image_id: "16805"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T17:26:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16805 -->
Die Unterkante der gefederten Laufräder liegt etwa 3-4 mm tiefer, als die der Äußeren im unbelasteten Zustand. Die Federung soll Unebenheiten nur dämpfen und nicht ausgleichen, daher die etwas harte Federung. Hinten rechts noch eine Winde.