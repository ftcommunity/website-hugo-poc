---
layout: "image"
title: "Antrieb Gesamtansicht rechts"
date: "2005-10-30T11:29:25"
picture: "Antrieb_001.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5142
- /details17b3.html
imported:
- "2019"
_4images_image_id: "5142"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5142 -->
Gestern und heute habe ich das Kettenfahrwerk wieder auseinandergebaut (nach langer Zeit arbeitsbedingter ft-Abstinenz). Hier nun nachgereicht ein paar Detailfotos vom Antrieb. Das hier ist die rechte Seite (die Motoren zeigen mit ihrer Kappe in Fahrtrichtung).
