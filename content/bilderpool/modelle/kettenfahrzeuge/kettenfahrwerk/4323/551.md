---
layout: "comment"
hidden: true
title: "551"
date: "2005-05-31T18:41:35"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Oh la la, quelle Frickelage!Mein lieber Scholli, Respekt! Mei, was geht's da eng zu.

Was mich dann (getreu dem angeblichen, aber immer heftig dementierten Panzerfahrermotto "breit fahren - schmal denken") zur Frage bringt: warum baust du nicht den Kasten breiter? Mit den schwarzen Differenzialen kann man doch unbegrenzt in die Breite gehen.

Gruß, Harald