---
layout: "image"
title: "Antrieb Getriebe rechts"
date: "2005-10-30T11:29:25"
picture: "Antrieb_004.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5145
- /details577e.html
imported:
- "2019"
_4images_image_id: "5145"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5145 -->
Hier das rechte Getriebe en Detail. Die hier oben sichtbaren (tatsächlich "hinten" befindlichen) Rast-Z10 sitzen eine ft-Zapfenlänge tiefer als der Rest, so dass sie ins Z10  und das Z20 eingreifen.
