---
layout: "image"
title: "Kettenfahrwerk (Blick von unten in den Antrieb)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_012.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4322
- /detailscc4d.html
imported:
- "2019"
_4images_image_id: "4322"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4322 -->
Das ist der untere Motor (das Fahrwerk liegt gerade auf dem Kopf). Der ist für die Vor-/Rückwärtsbewegung verantwortlich.
