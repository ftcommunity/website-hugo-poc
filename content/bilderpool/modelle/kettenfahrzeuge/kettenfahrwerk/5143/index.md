---
layout: "image"
title: "Antrieb Gesamtansicht links"
date: "2005-10-30T11:29:25"
picture: "Antrieb_002.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/5143
- /details2eb5.html
imported:
- "2019"
_4images_image_id: "5143"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5143 -->
Hier die linke Seite. Durch den anderen Getriebeaufbau als auf der rechten Seite ergeben sich die notwendigen Drehrichtungen: Der untere Motor ist der fürs Fahren (beide Ketten werden in die gleiche Richtung angetrieben), der obere der fürs Lenken (beide Ketten werden entgegengesetzt angetrieben, und zwar auch noch halb so schnell wie beim Fahrmotor).
