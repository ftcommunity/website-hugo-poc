---
layout: "image"
title: "Kettenfahrwerk (Unterseite)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_010.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/4328
- /detailsad2a.html
imported:
- "2019"
_4images_image_id: "4328"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4328 -->
Die 2 x 4 Räder sind gefedert aufgehängt. Es bleibt genug Bodenfreiheit für schlimmes Gelände, und der Schwerpunkt des Fahrwerks ist auch noch recht tief (bisher kippte das ganze nie um).