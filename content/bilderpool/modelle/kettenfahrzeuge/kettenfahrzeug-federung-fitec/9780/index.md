---
layout: "image"
title: "Bodenabstand"
date: "2007-03-24T15:06:00"
picture: "Kettenfahrwerk23.jpg"
weight: "3"
konstrukteure: 
- "Umgebaute Federung: fitec -Rest: StefanL"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9780
- /detailsf07f.html
imported:
- "2019"
_4images_image_id: "9780"
_4images_cat_id: "879"
_4images_user_id: "456"
_4images_image_date: "2007-03-24T15:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9780 -->
Hier sieht man den großen Bodenabstand. Ein playmobil-Männchen kann fast darunter laufen.
