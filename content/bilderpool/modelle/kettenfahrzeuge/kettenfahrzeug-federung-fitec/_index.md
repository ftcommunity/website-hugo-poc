---
layout: "overview"
title: "Kettenfahrzeug mit Federung (fitec)"
date: 2020-02-22T08:36:38+01:00
legacy_id:
- /php/categories/879
- /categoriesa9d3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=879 --> 
Dieses Kettenfahrzeug hat eine Federung. Ich habe hauptsächlich von der Federung Bilder gemacht, weil das Kettenfahrzeug dem von StefanL sehr ähnelt. Das einzig andere ist, dass die Federung so gebaut wurde dass das Kettenfahrzeug mehr Bodenabstand hat.