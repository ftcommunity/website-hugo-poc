---
layout: "image"
title: "Federung"
date: "2007-03-24T15:06:00"
picture: "Kettenfahrwerk22.jpg"
weight: "2"
konstrukteure: 
- "Umgebaute Federung: fitec -Rest: StefanL"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9779
- /detailsb6d9.html
imported:
- "2019"
_4images_image_id: "9779"
_4images_cat_id: "879"
_4images_user_id: "456"
_4images_image_date: "2007-03-24T15:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9779 -->
Hier sieht man die Federung im Detail.
