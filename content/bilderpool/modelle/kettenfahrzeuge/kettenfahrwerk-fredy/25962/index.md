---
layout: "image"
title: "Antrieb"
date: "2009-12-16T17:58:51"
picture: "fahrwerk2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/25962
- /detailsc5fe.html
imported:
- "2019"
_4images_image_id: "25962"
_4images_cat_id: "1825"
_4images_user_id: "453"
_4images_image_date: "2009-12-16T17:58:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25962 -->
50:1 Power Motoren.
