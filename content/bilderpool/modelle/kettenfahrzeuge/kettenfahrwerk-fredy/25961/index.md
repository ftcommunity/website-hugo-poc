---
layout: "image"
title: "Kettenfahrwerk"
date: "2009-12-16T17:58:50"
picture: "fahrwerk1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/25961
- /details34f4.html
imported:
- "2019"
_4images_image_id: "25961"
_4images_cat_id: "1825"
_4images_user_id: "453"
_4images_image_date: "2009-12-16T17:58:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25961 -->
Prototyp für ein neues Kettenfahrwerk.
Die Raupenbeläge fehlen, weil die sich unter dem Gewicht lösen.
