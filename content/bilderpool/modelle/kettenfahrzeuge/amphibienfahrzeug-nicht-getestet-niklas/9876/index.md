---
layout: "image"
title: "Amphibienfahrzeug(nicht getestet)"
date: "2007-04-01T17:49:09"
picture: "amphibienfahrzeug4.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/9876
- /detailsa229.html
imported:
- "2019"
_4images_image_id: "9876"
_4images_cat_id: "892"
_4images_user_id: "557"
_4images_image_date: "2007-04-01T17:49:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9876 -->
von oben.