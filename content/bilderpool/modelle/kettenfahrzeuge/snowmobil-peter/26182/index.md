---
layout: "image"
title: "Snowmobil 1"
date: "2010-01-29T18:58:24"
picture: "snowmobil1.jpg"
weight: "1"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26182
- /details5804.html
imported:
- "2019"
_4images_image_id: "26182"
_4images_cat_id: "1857"
_4images_user_id: "998"
_4images_image_date: "2010-01-29T18:58:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26182 -->
<ul>
<li>2 Power Motoren
<li>IR-Empfänger
<li>Lenkung
<li>Relaisversärker