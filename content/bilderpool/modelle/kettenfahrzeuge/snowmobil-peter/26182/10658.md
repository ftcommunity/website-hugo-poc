---
layout: "comment"
hidden: true
title: "10658"
date: "2010-01-29T20:00:17"
uploadBy:
- "hman13"
license: "unknown"
imported:
- "2019"
---
Ganz einfach: der Schnee kommt an den Zahnrädern raus. Denn dort ist ein Abstand zwischen den Kettengliedern.