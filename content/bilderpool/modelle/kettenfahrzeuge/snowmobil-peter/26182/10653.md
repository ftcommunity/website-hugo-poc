---
layout: "comment"
hidden: true
title: "10653"
date: "2010-01-29T19:35:29"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

ein nettes Modell hast du da gebaut. Ab welcher Schneehöhe nimmt denn das Raupenwerk dann Schaden z.B. durch eingeklemmten Schnee.? Wie wäre es dagegen mit einem Schneeepflug oder gar noch ein Schneefräsengebläse davor? Dann könnte noch der Raupenantrieb für eine bessere Querstandfestigkeit verbreitert und die 2 Power-Motoren darüber etwa mittig längs angeordnet werden. Die Antriebskette aussen könnte dann ganz wegfallen und durch reine Zahnradgetriebe (Kegelräder vom Motor von längs auf quer) ersetzt werden.
Das sind aber nur so mal schnelle Gedanken, denn es ist ja dein Modell ... 

Gruss Udo2