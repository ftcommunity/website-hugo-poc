---
layout: "image"
title: "Snowmobil 2"
date: "2010-01-29T18:58:24"
picture: "snowmobil2.jpg"
weight: "2"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26183
- /details207c.html
imported:
- "2019"
_4images_image_id: "26183"
_4images_cat_id: "1857"
_4images_user_id: "998"
_4images_image_date: "2010-01-29T18:58:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26183 -->
Es ist mir gelungen der Schwerpunkt genau in die Mitte zu bringen