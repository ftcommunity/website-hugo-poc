---
layout: "image"
title: "Snowmobil 6"
date: "2010-01-29T18:58:25"
picture: "snowmobil6.jpg"
weight: "6"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26187
- /detailsa2ed.html
imported:
- "2019"
_4images_image_id: "26187"
_4images_cat_id: "1857"
_4images_user_id: "998"
_4images_image_date: "2010-01-29T18:58:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26187 -->
