---
layout: "image"
title: "Pistenbully 3"
date: "2007-10-28T12:45:39"
picture: "pistenbully03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12355
- /details7016.html
imported:
- "2019"
_4images_image_id: "12355"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12355 -->
Der Motor zum heben und senken des Räumschildes, platzsparend verstaut.
