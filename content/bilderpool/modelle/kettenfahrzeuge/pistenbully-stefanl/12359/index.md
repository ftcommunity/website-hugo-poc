---
layout: "image"
title: "Pistenbully 7"
date: "2007-10-28T12:45:39"
picture: "pistenbully07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12359
- /details28cd.html
imported:
- "2019"
_4images_image_id: "12359"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12359 -->
