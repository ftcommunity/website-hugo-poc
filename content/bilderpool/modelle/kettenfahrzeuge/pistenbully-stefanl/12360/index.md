---
layout: "image"
title: "Pistenbully 8"
date: "2007-10-28T12:45:39"
picture: "pistenbully08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
fotografen:
- "Stefan Lehnerer (StefanL)"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- /php/details/12360
- /details9850.html
imported:
- "2019"
_4images_image_id: "12360"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12360 -->
127% Steigung schafft er gut, aber nur mit Anti-rutsch-Matte. Die Echten schaffen ja um die 100% ;-)
