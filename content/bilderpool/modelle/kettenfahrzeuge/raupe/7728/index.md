---
layout: "image"
title: "von vorne"
date: "2006-12-08T18:39:17"
picture: "Raupe_004.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7728
- /details3df9.html
imported:
- "2019"
_4images_image_id: "7728"
_4images_cat_id: "731"
_4images_user_id: "453"
_4images_image_date: "2006-12-08T18:39:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7728 -->
