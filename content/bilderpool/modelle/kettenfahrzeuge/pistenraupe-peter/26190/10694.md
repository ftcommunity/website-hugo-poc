---
layout: "comment"
hidden: true
title: "10694"
date: "2010-02-01T16:06:23"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Wie ist denn die Drehzahl der Motoren? Wenn die nahe der Leerlaufdrehzahl ist, dann geben sie keine Kraft ab. Das maximale Drehmoment gibt es im Stillstand.

Wenn schon vier Power-Motoren, dann will ich auch sehen, dass sie genutzt werden :-)