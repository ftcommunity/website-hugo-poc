---
layout: "image"
title: "Pistenraupe"
date: "2010-01-31T17:46:49"
picture: "pistenraupepeter1.jpg"
weight: "1"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- /php/details/26188
- /details7009.html
imported:
- "2019"
_4images_image_id: "26188"
_4images_cat_id: "1860"
_4images_user_id: "998"
_4images_image_date: "2010-01-31T17:46:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26188 -->
Damit kein Schnee an die Motoren kommt habe ich unten eine Folie befestigt