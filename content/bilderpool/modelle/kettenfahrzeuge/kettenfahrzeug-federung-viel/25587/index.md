---
layout: "image"
title: "Die Federung"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit3.jpg"
weight: "3"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/25587
- /details2008.html
imported:
- "2019"
_4images_image_id: "25587"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25587 -->
Ich fürchte es ist etwas "over-engineered", aber es hat trotzdem Spaß gemacht.