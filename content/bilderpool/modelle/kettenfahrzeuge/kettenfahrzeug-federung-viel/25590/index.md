---
layout: "image"
title: "Federung im Test"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit6.jpg"
weight: "6"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- /php/details/25590
- /details7efe.html
imported:
- "2019"
_4images_image_id: "25590"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25590 -->
Hier sieht man, was ein Hindernis (mit ca. 1,2 cm Höhe) bewirkt. Die betroffene Feder ist noch nicht mal ganz zusammen.