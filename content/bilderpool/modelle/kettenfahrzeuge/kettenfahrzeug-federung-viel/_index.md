---
layout: "overview"
title: "Kettenfahrzeug mit Federung und viel Bodenfreiheit"
date: 2020-02-22T08:37:00+01:00
legacy_id:
- /php/categories/1798
- /categoriesc1b8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1798 --> 
Für meine erste Veröffentlichung hier in ftCommunity wollte ich eigentlich etwas Einfaches bauen. Es sollte nur ein einfaches Kettenfahrzeug werden. Ich hatte eins mit Federung in Erbes-Büdesheim-2009 gesehen gehabt. Da sagte ich mir prima das baust du nach. Es stellte sich leider heraus, daß diese Art von Federung nicht viel Tragkraft hat. Das Modell schleifte fast am Boden (und wog 1,45 kg). Das war sehr ärgerlich, deshalb baute ich das Ganze von Grund auf neu. Jetzt ist (bis auf den Gleichlaufmotor) alles original von mir. Es hat nun gewichtsunabhängige Federung und eine Rekordbodenfreiheit von 8,5 cm. Es ist 31,5 cm Breit sowie 38,5 cm Lang und wiegt 2,25 kg.