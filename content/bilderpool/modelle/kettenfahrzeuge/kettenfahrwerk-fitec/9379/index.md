---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:30:57"
picture: "Kettenfahrwerk10.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9379
- /details5a59.html
imported:
- "2019"
_4images_image_id: "9379"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9379 -->
Wo vorne und wo hinten sein soll weiß ich noch nicht.
