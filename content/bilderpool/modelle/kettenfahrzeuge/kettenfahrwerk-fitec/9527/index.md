---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-15T19:02:22"
picture: "Kettenfahrwerk12.jpg"
weight: "12"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9527
- /details6279.html
imported:
- "2019"
_4images_image_id: "9527"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-15T19:02:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9527 -->
Hier habe ich mal das Kettenfahrzeug fast ganz erneuert. Lediglich das Getriebe ist dasselbe geblieben. Die Gründe für die Erneuerung sind erstes, weil die Kette in starkem Gelände, sprich Rasen(Wiese) immer abgesprungen ist. Außerdem ist es jetzt viel stabiler wie vorher. Ansonsten war das alte Kettenfahrwerk aber sehr gut.
