---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-15T19:02:23"
picture: "Kettenfahrwerk19.jpg"
weight: "19"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9534
- /details3a84.html
imported:
- "2019"
_4images_image_id: "9534"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-15T19:02:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9534 -->
Hier mit Ketten. Die linke Seite ist noch nicht bespannt, weil ich die restlichen Ketten noch woanders ausbauen muss.
