---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:30:57"
picture: "Kettenfahrwerk3.jpg"
weight: "3"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/9372
- /details7368.html
imported:
- "2019"
_4images_image_id: "9372"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9372 -->
Durch die Statik ist es sehr stabil. Der Akku ist auch schon eingebaut. Man sieht ihn vorn unter der Statik. Die Seilrollen sind wegen der Ketten.
