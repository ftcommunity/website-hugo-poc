---
layout: "image"
title: "Pistenraupe 4"
date: "2007-10-16T21:02:20"
picture: "Pistenraupe_06.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12238
- /details864c.html
imported:
- "2019"
_4images_image_id: "12238"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:02:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12238 -->
Das Räumschild ist gelenkig gelagert und schleift somit immer - wie gewünscht - auf dem Boden, äh, dem Schnee.