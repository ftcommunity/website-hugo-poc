---
layout: "image"
title: "Pistenraupe 6"
date: "2007-10-16T21:04:50"
picture: "Pistenraupe_08.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12240
- /details286b-2.html
imported:
- "2019"
_4images_image_id: "12240"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:04:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12240 -->
Das Glättbrett sorgt für eine glatte und feste Schneepiste.