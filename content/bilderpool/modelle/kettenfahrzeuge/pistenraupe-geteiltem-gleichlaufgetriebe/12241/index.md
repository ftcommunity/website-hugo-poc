---
layout: "image"
title: "Pistenraupe 7"
date: "2007-10-16T21:08:15"
picture: "Pistenraupe_09.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12241
- /detailsa18d.html
imported:
- "2019"
_4images_image_id: "12241"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:08:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12241 -->
Die Bodenfreiheit ist für ein Fahrzeug dieser (geringen) Größe beachtlich. Wie bereits geschrieben, handelt es sich auch um ein Gleichlaufgetriebe mit 2 Motoren! Mehr dazu weiter hinten... Es ist zu erkennen, dass der technische Aufbau (der Antrieb) extrem flach baut.