---
layout: "image"
title: "Pistenraupe 5"
date: "2007-10-16T21:04:50"
picture: "Pistenraupe_07.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
schlagworte: ["32455"]
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/12239
- /detailsbef8.html
imported:
- "2019"
_4images_image_id: "12239"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:04:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12239 -->
Das Räumschild lässt sich natürlich auch hochklappen und fixieren.

Das Fahrerhaus ist vor allem nach vorn und seitlich bewusst sehr luftig gehalten, damit der Fahrer jederzeit einen optimalen Überblick hat - wie bei den echten Pistenraupen.