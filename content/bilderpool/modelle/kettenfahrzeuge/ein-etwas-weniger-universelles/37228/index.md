---
layout: "image"
title: "Viel Hunger"
date: "2013-08-06T18:51:14"
picture: "IMG_8718.jpg"
weight: "43"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37228
- /details2821-2.html
imported:
- "2019"
_4images_image_id: "37228"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37228 -->
Die acht Power-Motoren haben ordentlich Appetit.