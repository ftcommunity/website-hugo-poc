---
layout: "image"
title: "Leichtbauweise"
date: "2012-02-19T15:44:28"
picture: "IMG_3880.jpg"
weight: "26"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34294
- /detailsa9cd.html
imported:
- "2019"
_4images_image_id: "34294"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34294 -->
Um Gewicht zu sparen, habe ich wo immer es geht und die Stabilität nicht beeindrächtigt, Statikteile oder hohle Bausteine benutzt. Ein Motorblock wiegt 492gr.