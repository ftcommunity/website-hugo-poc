---
layout: "image"
title: "Antrieb 2"
date: "2013-08-07T18:39:26"
picture: "IMG_9075.jpg"
weight: "47"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37233
- /details1557.html
imported:
- "2019"
_4images_image_id: "37233"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37233 -->
Unterhalb der Schnecken befindet sich eine Achse mit vier 30-er Zahnräder. Drei Alu-Stäbe sorgen für den Abstand zueinander und daß die Klemmverbinder nicht durchrutschen. Diese vier Zahnräder treiben eine weitere Achse gleicher Bauweise an.