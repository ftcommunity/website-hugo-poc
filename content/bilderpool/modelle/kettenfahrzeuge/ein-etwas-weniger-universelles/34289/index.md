---
layout: "image"
title: "Zusätzliche Lagerung"
date: "2012-02-19T15:44:28"
picture: "IMG_3835.jpg"
weight: "21"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34289
- /detailsb6f6.html
imported:
- "2019"
_4images_image_id: "34289"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34289 -->
Es hat sich gezeigt, daß die schnecke heruntergedrückt wird, wenn der Motorblock belastet wird, und der Antrieb aussetzt. Deshalb mußte das lose Ende gelagert werden.