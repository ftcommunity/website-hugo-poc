---
layout: "image"
title: "Von vorne"
date: "2012-02-19T15:44:06"
picture: "IMG_3592.jpg"
weight: "6"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34274
- /details8b45.html
imported:
- "2019"
_4images_image_id: "34274"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34274 -->
So viel muß schon sein.