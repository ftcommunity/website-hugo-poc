---
layout: "image"
title: "Viel Power und keine Haftung"
date: "2012-02-19T15:44:28"
picture: "IMG_3593.jpg"
weight: "34"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34302
- /details70ae.html
imported:
- "2019"
_4images_image_id: "34302"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34302 -->
der nächste punkt, der mich beschäftigte, war, ob die kette genug reibung aufbringt, um auf parkett sauber zu rangieren. und vor allem für ein digitales wesen reproduzierbar zu rangieren. bringt ja nichts, wenn er sich verliert. deshalb werde ich die ketten verdoppeln. ist bei der größe sicher auch optisch ein gewinn.