---
layout: "image"
title: "Es geht in die Höhe"
date: "2012-02-19T15:44:28"
picture: "IMG_3615.jpg"
weight: "11"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34279
- /details4f99.html
imported:
- "2019"
_4images_image_id: "34279"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34279 -->
um beide motoren unterzubringen und den innenraum freizuhalten, mußte ich etwas in die höhe bauen.