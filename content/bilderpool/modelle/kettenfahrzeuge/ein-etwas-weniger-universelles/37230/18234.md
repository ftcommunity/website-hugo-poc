---
layout: "comment"
hidden: true
title: "18234"
date: "2013-08-06T20:05:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr schönes Fahrwerk, sehr schöne Bilder! Wie sieht das mit Dauerlast aus? Bei so viel Reibung durch die Zahnkränze dürfen doch so einige Zahnräder von Schnecken abgefressen werden?

Gruß,
Stefan