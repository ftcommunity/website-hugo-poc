---
layout: "image"
title: "Nicht nur ein Blatt Papier"
date: "2013-08-06T18:51:31"
picture: "IMG_8721.jpg"
weight: "45"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37230
- /details955c.html
imported:
- "2019"
_4images_image_id: "37230"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37230 -->
Nachdem das allererste Modell ( Großes universelles Kettenfahrzeug ) nicht mal über einen dünnen Prospekt fahren konnte, hat dieses Modell genug Motorleistung und ist steif genug, um auch die künftige Ausrüstung zu transportieren. Obwohl es nicht dafür ausgelegt ist, fährt es anstandslos über ein 60cmx60cmx15cm Kissen und auch eine Steigung von 20° sind kein Problem. Nachdem es diese Tests bestanden hatte, ohne daß sich am Fahrwerk etwas verschoben hatte, kann es weiter gehen.