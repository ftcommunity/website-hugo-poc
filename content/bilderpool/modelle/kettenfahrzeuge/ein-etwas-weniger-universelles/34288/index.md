---
layout: "image"
title: "Von vorne"
date: "2012-02-19T15:44:28"
picture: "IMG_3807.jpg"
weight: "20"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34288
- /detailsb545.html
imported:
- "2019"
_4images_image_id: "34288"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34288 -->
von vorne