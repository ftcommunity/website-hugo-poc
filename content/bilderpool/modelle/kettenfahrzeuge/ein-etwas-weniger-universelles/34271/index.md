---
layout: "image"
title: "Der Antrieb"
date: "2012-02-19T15:44:06"
picture: "IMG_3578.jpg"
weight: "3"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34271
- /detailsf9ae.html
imported:
- "2019"
_4images_image_id: "34271"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34271 -->
Ein Power-Motor treibt per Kette ein 40-er Zahnrad an. Auf dessen Welle sitzt ein 30-er Zahnrad, das wiederum deide Zahnkränze antreibt.