---
layout: "image"
title: "Hier ohne Differential"
date: "2012-02-19T15:44:28"
picture: "IMG_3796.jpg"
weight: "17"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34285
- /details6c32.html
imported:
- "2019"
_4images_image_id: "34285"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34285 -->
Eine bauplatte wird als lagerung durchbohrt.