---
layout: "overview"
title: "Ein etwas weniger universelles Kettenfahrzeug"
date: 2020-02-22T08:37:13+01:00
legacy_id:
- /php/categories/2537
- /categoriesf147.html
- /categoriesdc4d.html
- /categoriesb480.html
- /categoriesf9b6.html
- /categories44e5.html
- /categoriescf7d.html
- /categoriesb15c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2537 --> 
Zeit ist vergangen, und das ursprüngliche Monster, das kaum über ein Blatt Papier fahren konnte, hat sich entwickelt.