---
layout: "image"
title: "Zu Plump"
date: "2012-02-19T15:44:28"
picture: "IMG_3631.jpg"
weight: "12"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34280
- /details4dfd-2.html
imported:
- "2019"
_4images_image_id: "34280"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34280 -->
aber letztendlich war mir der entwurf zu plump, nicht elegant genug, zu unpraktisch beim bauen, beim justieren und evtl. späterer reparaturen.