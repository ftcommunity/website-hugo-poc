---
layout: "image"
title: "Gehirn"
date: "2013-08-07T18:39:26"
picture: "IMG_8722.jpg"
weight: "49"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37235
- /detailsbc61.html
imported:
- "2019"
_4images_image_id: "37235"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37235 -->
Da das Fahrzeug im Endstadium völlig autonom funktionieren soll, habe ich vor, sämtliche Elektronik einzusetzen, die ich besitze. Daher ist Geschwindigkeit eher kontraproduktiv. Es zu bauen macht ja schon höllischen Spaß - aber ehrlich gesagt, freue ich mich schon mehr es zu programmieren.