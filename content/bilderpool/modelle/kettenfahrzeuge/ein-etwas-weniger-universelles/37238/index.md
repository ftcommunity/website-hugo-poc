---
layout: "image"
title: "Wartung und Erweiterung 2"
date: "2013-08-07T18:39:27"
picture: "IMG_9079.jpg"
weight: "52"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37238
- /details9dfe.html
imported:
- "2019"
_4images_image_id: "37238"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37238 -->
Die zweite Antriebswelle läßt sich entnehmen.