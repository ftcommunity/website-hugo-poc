---
layout: "image"
title: "Nur einseitig"
date: "2013-08-06T18:51:14"
picture: "IMG_8719.jpg"
weight: "44"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/37229
- /details32b7-2.html
imported:
- "2019"
_4images_image_id: "37229"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37229 -->
Die acht Antriebsräderpaare sind jeweils nur einseitig befestigt.