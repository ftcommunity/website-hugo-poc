---
layout: "image"
title: "Schon relativ schwer"
date: "2012-02-19T15:44:06"
picture: "IMG_3590.jpg"
weight: "4"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34272
- /detailse5a7.html
imported:
- "2019"
_4images_image_id: "34272"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34272 -->
In dieser Form fährt es und überwindet Hindernisse. Da allerdings die untere Federung noch fehlt, verliert die Kette den Kontakt zu den unteren Zahnrädern bei Hindernissen.