---
layout: "comment"
hidden: true
title: "8377"
date: "2009-02-01T08:46:54"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@Masked
Hallo Martin,
wenn du die Power-Motoren 8:1 durch 20:1 (oder 50:1) austauschst und nur harte Belege einsetzt, sollte sich das auf die Stromaufnahme günstig auswirken. Modellgewicht und Raupenauflage dürften dennoch ausreichende Laufeigenschaften des Bullys hergeben.
Gruss, Udo2