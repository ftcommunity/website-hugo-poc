---
layout: "image"
title: "Auf dem Rückzug"
date: "2010-12-18T14:02:48"
picture: "kettenfahrzeugschnee5.jpg"
weight: "5"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/29475
- /details61d6.html
imported:
- "2019"
_4images_image_id: "29475"
_4images_cat_id: "2144"
_4images_user_id: "373"
_4images_image_date: "2010-12-18T14:02:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29475 -->
Wie die fehlenden Fußspuren beweisen, hat das Fahrzeug es selber wieder rausgeschafft, wenn auch mit Mühe.
Das Fazit: Ft kann mit Schnee, aber das Ganze ist noch Ausbaufähig. Insbesondere muss man darauf achten, nirgendwo eine Möglichkeit zu bieten, in die sich der Schnee reinsetzen kann. Weitere Versuche folgen, sobald wieder genügend Pulverschnee da ist. Ab Morgen wird das Ganze ja wieder zu pappig.
