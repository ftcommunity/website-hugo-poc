---
layout: "image"
title: "Glättbrett"
date: "2009-01-18T20:08:01"
picture: "pb4.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/17078
- /detailsb634.html
imported:
- "2019"
_4images_image_id: "17078"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-18T20:08:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17078 -->
Man sieht die Kugellager-Lagerung (Oppermann-Kugellager) der Achsen. Ansonsten ist der Antrieb von den PowerBulldozern übernommen.
