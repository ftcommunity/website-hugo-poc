---
layout: "image"
title: "Industrea Dozer_hinten"
date: "2008-12-14T10:53:30"
picture: "industreadozer4.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16614
- /detailsc8c0.html
imported:
- "2019"
_4images_image_id: "16614"
_4images_cat_id: "1504"
_4images_user_id: "845"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16614 -->
