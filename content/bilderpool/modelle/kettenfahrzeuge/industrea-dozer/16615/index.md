---
layout: "image"
title: "Gabelkippmechanismus"
date: "2008-12-14T10:53:30"
picture: "industreadozer5.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16615
- /detailsd021.html
imported:
- "2019"
_4images_image_id: "16615"
_4images_cat_id: "1504"
_4images_user_id: "845"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16615 -->
