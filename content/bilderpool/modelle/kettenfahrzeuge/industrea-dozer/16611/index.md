---
layout: "image"
title: "Industrea Dozer"
date: "2008-12-14T10:53:30"
picture: "industreadozer1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16611
- /detailse5b0-2.html
imported:
- "2019"
_4images_image_id: "16611"
_4images_cat_id: "1504"
_4images_user_id: "845"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16611 -->
