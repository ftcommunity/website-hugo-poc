---
layout: "image"
title: "Raupenfahrzeug4"
date: "2008-03-11T15:40:57"
picture: "raupenfahrzeugmitpneumatischemgreiferfer4.jpg"
weight: "4"
konstrukteure: 
- "equester"
fotografen:
- "equester"
uploadBy: "equester"
license: "unknown"
legacy_id:
- /php/details/13897
- /details6c90.html
imported:
- "2019"
_4images_image_id: "13897"
_4images_cat_id: "1275"
_4images_user_id: "731"
_4images_image_date: "2008-03-11T15:40:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13897 -->
Zur Zeit des Baus hatte ich noch keine Power-Motoren, daher noch der Antrieb über normalen Motor mit 1:3 Untersetzung.
Ebenso kann man natürlich die manuelle Betätigung der Ventile noch mit Hilfe von Magnetventilen+Interface verbessern.
