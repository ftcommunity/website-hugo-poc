---
layout: "image"
title: "Raupe von vorne"
date: "2011-05-18T14:47:35"
picture: "raupe2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30571
- /details6fe2.html
imported:
- "2019"
_4images_image_id: "30571"
_4images_cat_id: "2278"
_4images_user_id: "1162"
_4images_image_date: "2011-05-18T14:47:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30571 -->
Man sieht den Bodenabstand und die niedrige Bauweise.
