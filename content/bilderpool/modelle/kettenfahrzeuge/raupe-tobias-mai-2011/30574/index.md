---
layout: "image"
title: "Raupe im 'Berchele'"
date: "2011-05-18T14:47:35"
picture: "raupe5.jpg"
weight: "5"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30574
- /details3b49.html
imported:
- "2019"
_4images_image_id: "30574"
_4images_cat_id: "2278"
_4images_user_id: "1162"
_4images_image_date: "2011-05-18T14:47:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30574 -->
