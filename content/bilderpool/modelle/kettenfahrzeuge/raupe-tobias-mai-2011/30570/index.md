---
layout: "image"
title: "Raupe"
date: "2011-05-18T14:47:35"
picture: "raupe1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30570
- /details290b.html
imported:
- "2019"
_4images_image_id: "30570"
_4images_cat_id: "2278"
_4images_user_id: "1162"
_4images_image_date: "2011-05-18T14:47:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30570 -->
Mein Bruder hat eine Raupe gebaut. Die durch das IR Control Set bewegt werden kann. Sein Vorbild war diese Weinbergsraupe Niko HY 20/11 hier: http://www.niko-maschinenbau.de/de/produkte/raupen/HY20_11_HY27_16_HY13_11.htm .

MfG
Endlich bzw. Tobias
