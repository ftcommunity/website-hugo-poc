---
layout: "image"
title: "Pneumatikbagger"
date: "2007-02-03T00:28:46"
picture: "toyfair09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8797
- /details7734.html
imported:
- "2019"
_4images_image_id: "8797"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8797 -->
Wohl ein Bagger der mit dem aktuellen Pneumatikkasten machbar ist.