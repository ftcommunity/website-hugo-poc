---
layout: "image"
title: "Robo explorer"
date: "2007-02-05T17:49:54"
picture: "100_3424.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "tomak"
uploadBy: "tomak"
license: "unknown"
legacy_id:
- /php/details/8891
- /detailsd179.html
imported:
- "2019"
_4images_image_id: "8891"
_4images_cat_id: "802"
_4images_user_id: "548"
_4images_image_date: "2007-02-05T17:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8891 -->
