---
layout: "image"
title: "FischerTipp 2"
date: "2007-02-03T12:02:40"
picture: "toyfair11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8799
- /details8aea.html
imported:
- "2019"
_4images_image_id: "8799"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T12:02:40"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8799 -->
