---
layout: "image"
title: "RC-Cars"
date: "2007-02-03T00:28:46"
picture: "toyfair01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- /php/details/8789
- /details10b3.html
imported:
- "2019"
_4images_image_id: "8789"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8789 -->
Eine einfache Fernbedienen die ausschließlich für die dazugehörigen Chassis gedacht ist.