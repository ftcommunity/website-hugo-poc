---
layout: "image"
title: "Ein Projekt der ft-AG der GSM"
date: "2016-04-08T21:38:56"
picture: "einprojektderftagdergsm05.jpg"
weight: "5"
konstrukteure: 
- "Hanna"
fotografen:
- "giliprimero"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- /php/details/43268
- /details984d.html
imported:
- "2019"
_4images_image_id: "43268"
_4images_cat_id: "3214"
_4images_user_id: "2439"
_4images_image_date: "2016-04-08T21:38:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43268 -->
