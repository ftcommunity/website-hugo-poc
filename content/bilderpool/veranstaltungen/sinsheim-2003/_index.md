---
layout: "overview"
title: "Sinsheim 2003"
date: 2020-02-22T08:58:47+01:00
legacy_id:
- /php/categories/179
- /categoriesf1b8.html
- /categories8aaf.html
- /categories5277.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=179 --> 
Thomas Kaiser war im Technikmuseum in Sinsheim und hat einige Bilder gemacht.
Vielen Dank an Thomas!