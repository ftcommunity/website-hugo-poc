---
layout: "comment"
hidden: true
title: "126"
date: "2003-10-08T19:31:57"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
CVT?Wenn mich nicht alles täuscht, dann hat dieses Modell ein CVT-Getriebe (Continuously variable transmission), d.h. die Geschwindigkeit läßt sich **stufenlos** ändern. Das Antriebsrad befindet sich in Fahrzeugmitte (Achse in Längsrichtung), das angetriebene Rad (mit Gummiring drauf) rollt auf ersterem ab, und zwar wahlweise mehr außen oder mehr innen. Damit ändert sich das Übersetzungsverhältnis und schwupp, wird das Modell schneller oder langsamer.