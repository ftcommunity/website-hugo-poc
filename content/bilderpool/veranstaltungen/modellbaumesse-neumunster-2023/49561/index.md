---
layout: "image"
title: "Hanomag HC 280"
date: 2023-03-08T17:17:33+01:00
picture: "Neumünster_04-05.03.202366.jpg"
weight: "78"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

