---
layout: "image"
title: "Hanomag HC 280"
date: 2023-03-08T17:17:37+01:00
picture: "Neumünster_04-05.03.202362.jpg"
weight: "74"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

