---
layout: "image"
title: "Hanomag HC 280"
date: 2023-03-08T17:18:37+01:00
picture: "Neumünster_04-05.03.202308.jpg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

