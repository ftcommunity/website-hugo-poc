---
layout: "image"
title: "Hanomag HC 280"
date: 2023-03-08T17:17:32+01:00
picture: "Neumünster_04-05.03.202367.jpg"
weight: "79"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

