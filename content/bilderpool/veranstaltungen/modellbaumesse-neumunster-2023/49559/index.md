---
layout: "image"
title: "Hanomag HC 280"
date: 2023-03-08T17:17:30+01:00
picture: "Neumünster_04-05.03.202368.jpg"
weight: "80"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "Website-Team"
license: "unknown"
---

