---
layout: "overview"
title: "Modellbaumesse Neumünster 2023"
date: 2023-03-06T17:20:57+01:00
---
Modellbau Schleswig Holstein vom 04. bis zum 05.03.2023

Das Standteam: Dirk, Marko, Christian, Frank mit Sohn, Detlef mit Frau

Hier noch ein Video dazu: [Youtube](https://www.youtube.com/watch?v=cZrgnm6JsvU&t=1s)
