---
layout: "image"
title: "Schaufenster 1"
date: "2005-11-05T15:49:19"
picture: "142_4251.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["Schaufenster", "Riesenrad"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/5180
- /detailscdf6.html
imported:
- "2019"
_4images_image_id: "5180"
_4images_cat_id: "434"
_4images_user_id: "34"
_4images_image_date: "2005-11-05T15:49:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5180 -->
Hier ist das Schaufenster von inn beim Aufbau zu sehen.
