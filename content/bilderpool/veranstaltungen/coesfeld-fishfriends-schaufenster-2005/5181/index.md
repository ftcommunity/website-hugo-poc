---
layout: "image"
title: "Schaufenster 2"
date: "2005-11-05T15:49:19"
picture: "142_4252.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["Schaufenster", "Riesenrad"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/5181
- /detailse021.html
imported:
- "2019"
_4images_image_id: "5181"
_4images_cat_id: "434"
_4images_user_id: "34"
_4images_image_date: "2005-11-05T15:49:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5181 -->
Schaufenster von innen beim Aufbau.
