---
layout: "image"
title: "Schaufenster 11"
date: "2006-12-20T21:50:26"
picture: "157_5774.jpg"
weight: "11"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8015
- /detailsfe59.html
imported:
- "2019"
_4images_image_id: "8015"
_4images_cat_id: "747"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8015 -->
