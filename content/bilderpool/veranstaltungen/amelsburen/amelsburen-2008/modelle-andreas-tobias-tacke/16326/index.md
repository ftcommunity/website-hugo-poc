---
layout: "image"
title: "Tackes Basteleien"
date: "2008-11-17T21:09:19"
picture: "amel25.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16326
- /details4cd5.html
imported:
- "2019"
_4images_image_id: "16326"
_4images_cat_id: "1474"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:19"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16326 -->
Zum ausprobieren.
