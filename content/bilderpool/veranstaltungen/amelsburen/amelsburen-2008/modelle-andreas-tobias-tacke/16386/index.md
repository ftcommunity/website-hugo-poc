---
layout: "image"
title: "Schöner LKW.."
date: "2008-11-21T16:54:44"
picture: "ftausstellungamelsbueren26.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16386
- /details5b1c.html
imported:
- "2019"
_4images_image_id: "16386"
_4images_cat_id: "1474"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:44"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16386 -->
...mit noch schöneren Felgen.
