---
layout: "image"
title: "Industriemodell"
date: "2008-11-21T17:42:29"
picture: "ft44.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16436
- /detailsc843.html
imported:
- "2019"
_4images_image_id: "16436"
_4images_cat_id: "1475"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16436 -->
