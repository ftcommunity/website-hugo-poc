---
layout: "image"
title: "Steuerung"
date: "2008-11-21T17:42:29"
picture: "ft45.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16437
- /detailsc347.html
imported:
- "2019"
_4images_image_id: "16437"
_4images_cat_id: "1475"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16437 -->
