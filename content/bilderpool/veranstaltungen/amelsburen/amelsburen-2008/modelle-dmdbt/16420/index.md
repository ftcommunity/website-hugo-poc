---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:42:29"
picture: "ft28.jpg"
weight: "4"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16420
- /details0a93.html
imported:
- "2019"
_4images_image_id: "16420"
_4images_cat_id: "1484"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16420 -->
