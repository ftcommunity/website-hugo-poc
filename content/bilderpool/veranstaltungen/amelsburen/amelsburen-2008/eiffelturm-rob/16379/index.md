---
layout: "image"
title: "Blick auf den Aufzug im Pfeiler"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren19.jpg"
weight: "8"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16379
- /detailsd885.html
imported:
- "2019"
_4images_image_id: "16379"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16379 -->
Schön konstruiert, fast wie in echt.