---
layout: "image"
title: "Bild vom Eiffelturm"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren14.jpg"
weight: "3"
konstrukteure: 
- "G. Eiffel"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16374
- /details5faf.html
imported:
- "2019"
_4images_image_id: "16374"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16374 -->
Steht in der Stadt der Liebe.