---
layout: "image"
title: "Ein niedliches Detail"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren16.jpg"
weight: "5"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16376
- /details5c39-2.html
imported:
- "2019"
_4images_image_id: "16376"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16376 -->
Das Restaurant und die Touristen sehen echt klasse aus.