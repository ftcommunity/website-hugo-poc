---
layout: "comment"
hidden: true
title: "11301"
date: "2010-04-02T14:03:39"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Letzte Tage war noch ein Bericht im Fernsehen über den Bau vom Eiffelturm. War ganz interessant. Am liebsten würde ich meine Brücke Brücke sein lassen und mich auch mal am ET versuchen. Wie hoch ist die Halle in Erbes-Büdesheim nochmal? :-D

Soll ich noch Bilder vom Original hier einstellen? War ja 2008 zweimal vor Ort. ;-)