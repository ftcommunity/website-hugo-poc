---
layout: "image"
title: "Noch ein Pfeiler vom Eiffelturm"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren20.jpg"
weight: "9"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16380
- /detailsd841-2.html
imported:
- "2019"
_4images_image_id: "16380"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16380 -->
mit Treppe zum Aufzug.