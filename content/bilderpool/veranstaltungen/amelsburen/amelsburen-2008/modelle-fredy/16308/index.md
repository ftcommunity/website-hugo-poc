---
layout: "image"
title: "Roboter"
date: "2008-11-17T21:08:46"
picture: "amel07.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16308
- /detailsffc7.html
imported:
- "2019"
_4images_image_id: "16308"
_4images_cat_id: "1482"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16308 -->
Fredys Roboter
