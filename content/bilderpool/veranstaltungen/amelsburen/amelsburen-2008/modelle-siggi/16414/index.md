---
layout: "image"
title: "Zange"
date: "2008-11-21T17:42:29"
picture: "ft22.jpg"
weight: "4"
konstrukteure: 
- "Olli"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16414
- /details73e9.html
imported:
- "2019"
_4images_image_id: "16414"
_4images_cat_id: "1487"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16414 -->
