---
layout: "image"
title: "Siggis Truck"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren02.jpg"
weight: "2"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16362
- /detailsb088-2.html
imported:
- "2019"
_4images_image_id: "16362"
_4images_cat_id: "1487"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16362 -->
Sehr schöner Truck von Frickelsiggi, der auch noch fährt, also der Truck nicht der Siggi.  ;-)
