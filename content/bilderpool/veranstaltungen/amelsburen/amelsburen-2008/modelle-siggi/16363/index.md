---
layout: "image"
title: "Siggi ist wieder am frickeln."
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren03.jpg"
weight: "3"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16363
- /detailscb3b.html
imported:
- "2019"
_4images_image_id: "16363"
_4images_cat_id: "1487"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16363 -->
