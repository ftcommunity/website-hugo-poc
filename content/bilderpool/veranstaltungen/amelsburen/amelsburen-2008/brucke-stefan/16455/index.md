---
layout: "image"
title: "Brücke"
date: "2008-11-21T17:42:29"
picture: "ft63.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16455
- /details114b.html
imported:
- "2019"
_4images_image_id: "16455"
_4images_cat_id: "1483"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16455 -->
