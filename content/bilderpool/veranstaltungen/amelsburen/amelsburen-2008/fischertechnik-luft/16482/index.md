---
layout: "image"
title: "Amelsb16220.JPG"
date: "2008-11-23T14:02:14"
picture: "Amelsb16220.JPG"
weight: "15"
konstrukteure: 
- "n-a"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16482
- /details599a.html
imported:
- "2019"
_4images_image_id: "16482"
_4images_cat_id: "1480"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16482 -->
Der Eiffelturm wird bestaunt.
