---
layout: "image"
title: "Spielecke"
date: "2008-11-17T21:09:02"
picture: "amel15.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16316
- /details6629.html
imported:
- "2019"
_4images_image_id: "16316"
_4images_cat_id: "1480"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:02"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16316 -->
