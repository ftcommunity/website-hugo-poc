---
layout: "image"
title: "Spieltisch"
date: "2008-11-21T17:42:29"
picture: "ft66.jpg"
weight: "8"
konstrukteure: 
- "TST der Veranstalter"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16458
- /detailsf18f.html
imported:
- "2019"
_4images_image_id: "16458"
_4images_cat_id: "1480"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16458 -->
