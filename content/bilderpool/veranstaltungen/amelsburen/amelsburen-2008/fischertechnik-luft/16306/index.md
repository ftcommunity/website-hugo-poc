---
layout: "image"
title: "Hände"
date: "2008-11-17T21:08:46"
picture: "amel05.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16306
- /details1998.html
imported:
- "2019"
_4images_image_id: "16306"
_4images_cat_id: "1480"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16306 -->
Manchmal musste der Vater auch noch mithelfen...