---
layout: "image"
title: "Bulldozer-Spiel"
date: "2008-11-21T17:41:36"
picture: "ft13.jpg"
weight: "14"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16405
- /detailsb08c.html
imported:
- "2019"
_4images_image_id: "16405"
_4images_cat_id: "1473"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16405 -->
