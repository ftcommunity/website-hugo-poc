---
layout: "image"
title: "Industrea Dozer"
date: "2008-11-21T17:41:36"
picture: "ft14.jpg"
weight: "15"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16406
- /detailscfb3-2.html
imported:
- "2019"
_4images_image_id: "16406"
_4images_cat_id: "1473"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16406 -->
