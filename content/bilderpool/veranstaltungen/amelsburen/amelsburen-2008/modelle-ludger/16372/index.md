---
layout: "image"
title: "Des Klempners wichtigtes Utensil.."
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren12.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16372
- /details7ee4-2.html
imported:
- "2019"
_4images_image_id: "16372"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16372 -->
...der Schweissbrenner. Hier sieht man die Gasflaschen.
