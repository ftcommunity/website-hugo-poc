---
layout: "image"
title: "Das vordere Ende von Ludgers Roadtrain"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren08.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16368
- /detailsa318.html
imported:
- "2019"
_4images_image_id: "16368"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16368 -->
und hier nun das vordere Teil und noch weitere Modelle von Ludger.
