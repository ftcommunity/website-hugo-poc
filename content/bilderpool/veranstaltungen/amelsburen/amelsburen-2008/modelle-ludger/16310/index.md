---
layout: "image"
title: "ftamel08_0029"
date: "2008-11-17T21:09:02"
picture: "amel09.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16310
- /details72b2-2.html
imported:
- "2019"
_4images_image_id: "16310"
_4images_cat_id: "1473"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:02"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16310 -->
