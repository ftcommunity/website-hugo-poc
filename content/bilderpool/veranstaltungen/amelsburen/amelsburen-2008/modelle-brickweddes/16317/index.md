---
layout: "image"
title: "Riesenrad"
date: "2008-11-17T21:09:02"
picture: "amel16.jpg"
weight: "1"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16317
- /detailsfc00.html
imported:
- "2019"
_4images_image_id: "16317"
_4images_cat_id: "1476"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:02"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16317 -->
