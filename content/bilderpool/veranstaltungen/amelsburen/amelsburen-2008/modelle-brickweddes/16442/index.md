---
layout: "image"
title: "Motor"
date: "2008-11-21T17:42:29"
picture: "ft50.jpg"
weight: "12"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16442
- /details0da8.html
imported:
- "2019"
_4images_image_id: "16442"
_4images_cat_id: "1476"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16442 -->
