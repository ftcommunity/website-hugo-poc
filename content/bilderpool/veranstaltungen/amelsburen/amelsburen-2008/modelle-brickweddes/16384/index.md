---
layout: "image"
title: "Der Antrieb..."
date: "2008-11-21T16:54:44"
picture: "ftausstellungamelsbueren24.jpg"
weight: "6"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/16384
- /detailsb0c8.html
imported:
- "2019"
_4images_image_id: "16384"
_4images_cat_id: "1476"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:44"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16384 -->
...ist auch fast wie beim Original. Schön gelöst..
