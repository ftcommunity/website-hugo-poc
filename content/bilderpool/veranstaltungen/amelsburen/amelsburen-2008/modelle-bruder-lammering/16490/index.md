---
layout: "image"
title: "Amelsb15117.JPG"
date: "2008-11-23T14:23:33"
picture: "Amelsb15117.JPG"
weight: "3"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16490
- /details492d.html
imported:
- "2019"
_4images_image_id: "16490"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:23:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16490 -->
