---
layout: "image"
title: "Amelsb15143.JPG"
date: "2008-11-23T14:26:33"
picture: "Amelsb15143.JPG"
weight: "7"
konstrukteure: 
- "Lammering"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16494
- /details87c8.html
imported:
- "2019"
_4images_image_id: "16494"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16494 -->
Der Antrieb der Schiffschaukel. Der Motor wird mittels der "Garagentorschaltung" des E-Tec umgepolt, wenn die Gondel über einen der Taster fährt.
