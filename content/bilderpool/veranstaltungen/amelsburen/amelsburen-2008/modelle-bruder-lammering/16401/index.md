---
layout: "image"
title: "Kirmes-Modell"
date: "2008-11-21T17:41:15"
picture: "ft09.jpg"
weight: "1"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16401
- /details5e34.html
imported:
- "2019"
_4images_image_id: "16401"
_4images_cat_id: "1490"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16401 -->
