---
layout: "image"
title: "Amelsb16303.JPG"
date: "2008-11-23T14:27:53"
picture: "Amelsb16303.JPG"
weight: "8"
konstrukteure: 
- "Lammering"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/16495
- /detailsa746.html
imported:
- "2019"
_4images_image_id: "16495"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:27:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16495 -->
Et maintenant, eine Variation von Kassenhäuschen.
