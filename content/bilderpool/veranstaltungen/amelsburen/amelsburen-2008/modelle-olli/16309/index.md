---
layout: "image"
title: "joystick"
date: "2008-11-17T21:08:46"
picture: "amel08.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/16309
- /detailsb4e5-2.html
imported:
- "2019"
_4images_image_id: "16309"
_4images_cat_id: "1485"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16309 -->
Damit konnte man den Roboter steuern.