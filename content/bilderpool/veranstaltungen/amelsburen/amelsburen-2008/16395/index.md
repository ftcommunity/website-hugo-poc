---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:41:15"
picture: "ft03.jpg"
weight: "5"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16395
- /details72ea.html
imported:
- "2019"
_4images_image_id: "16395"
_4images_cat_id: "1479"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16395 -->
