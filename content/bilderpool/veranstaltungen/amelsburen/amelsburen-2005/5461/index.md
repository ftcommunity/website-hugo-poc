---
layout: "image"
title: "Fahrbahn mit Publikum"
date: "2005-12-16T16:01:45"
picture: "Bild1987.jpg"
weight: "2"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5461
- /detailsd20c.html
imported:
- "2019"
_4images_image_id: "5461"
_4images_cat_id: "472"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5461 -->
