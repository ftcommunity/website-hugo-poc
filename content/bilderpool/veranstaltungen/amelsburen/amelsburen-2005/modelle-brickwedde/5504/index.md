---
layout: "image"
title: "Amelsbüren 13"
date: "2005-12-17T13:54:29"
picture: "Amelsbren_13.jpg"
weight: "11"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5504
- /detailsa642.html
imported:
- "2019"
_4images_image_id: "5504"
_4images_cat_id: "473"
_4images_user_id: "10"
_4images_image_date: "2005-12-17T13:54:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5504 -->
