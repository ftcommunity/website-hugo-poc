---
layout: "image"
title: "Autogetriebe und Lenkung"
date: "2005-12-16T16:01:58"
picture: "Bild1966.jpg"
weight: "6"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5473
- /details0b68.html
imported:
- "2019"
_4images_image_id: "5473"
_4images_cat_id: "473"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5473 -->
