---
layout: "image"
title: "Amelsbüren 2"
date: "2005-12-17T13:44:47"
picture: "Amelsbren_02.jpg"
weight: "10"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5499
- /detailsd712.html
imported:
- "2019"
_4images_image_id: "5499"
_4images_cat_id: "473"
_4images_user_id: "10"
_4images_image_date: "2005-12-17T13:44:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5499 -->
