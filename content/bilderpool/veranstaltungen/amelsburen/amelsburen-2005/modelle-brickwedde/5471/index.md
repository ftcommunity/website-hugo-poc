---
layout: "image"
title: "Riesenrad"
date: "2005-12-16T16:01:58"
picture: "Bild1962.jpg"
weight: "4"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5471
- /details571d.html
imported:
- "2019"
_4images_image_id: "5471"
_4images_cat_id: "473"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5471 -->
