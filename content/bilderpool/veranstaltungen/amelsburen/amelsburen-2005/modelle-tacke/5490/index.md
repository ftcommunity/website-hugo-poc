---
layout: "image"
title: "4-Takt-Motor 2"
date: "2005-12-16T16:02:17"
picture: "Bild1994.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Reiner Stüven"
schlagworte: ["trike"]
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5490
- /details8652.html
imported:
- "2019"
_4images_image_id: "5490"
_4images_cat_id: "478"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5490 -->
