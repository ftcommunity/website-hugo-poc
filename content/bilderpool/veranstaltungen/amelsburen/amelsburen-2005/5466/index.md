---
layout: "image"
title: "Hubschrauber"
date: "2005-12-16T16:01:46"
picture: "Bild1979.jpg"
weight: "7"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5466
- /detailsed1d.html
imported:
- "2019"
_4images_image_id: "5466"
_4images_cat_id: "472"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5466 -->
