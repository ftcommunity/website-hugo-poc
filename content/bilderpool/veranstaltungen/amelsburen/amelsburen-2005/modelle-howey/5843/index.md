---
layout: "image"
title: "Howey2"
date: "2006-03-06T09:14:42"
picture: "Howey2.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Muensterische Zeitung"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/5843
imported:
- "2019"
_4images_image_id: "5843"
_4images_cat_id: "479"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:14:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5843 -->
