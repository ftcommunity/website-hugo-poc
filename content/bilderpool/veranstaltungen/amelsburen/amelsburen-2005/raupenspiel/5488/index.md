---
layout: "image"
title: "Die Raupe schiebt die Räder zusammen"
date: "2005-12-16T16:02:07"
picture: "Bild1977.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/5488
- /detailsc887.html
imported:
- "2019"
_4images_image_id: "5488"
_4images_cat_id: "477"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5488 -->
