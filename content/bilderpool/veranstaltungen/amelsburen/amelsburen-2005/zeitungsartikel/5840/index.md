---
layout: "image"
title: "Westfaelische Nachrichten"
date: "2006-03-06T09:00:25"
picture: "W._Nachrichten.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Scan"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/5840
- /details715a.html
imported:
- "2019"
_4images_image_id: "5840"
_4images_cat_id: "502"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5840 -->
Artikel über die Ausstellung in der Zeitung Westfälische Nachrichten
