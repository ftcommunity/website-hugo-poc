---
layout: "image"
title: "Dragster"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_084.jpg"
weight: "10"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7555
- /detailsae90.html
imported:
- "2019"
_4images_image_id: "7555"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7555 -->
