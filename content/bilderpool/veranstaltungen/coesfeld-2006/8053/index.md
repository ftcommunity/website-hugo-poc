---
layout: "image"
title: "COE 23"
date: "2006-12-20T21:50:26"
picture: "158_5809.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8053
- /details7246.html
imported:
- "2019"
_4images_image_id: "8053"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8053 -->
