---
layout: "image"
title: "Spielautomat von Ludger"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_057.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7543
- /detailsf62a.html
imported:
- "2019"
_4images_image_id: "7543"
_4images_cat_id: "720"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7543 -->
... und noch ein Stückchen weiter vor ...
