---
layout: "image"
title: "Spielautomat von Ludger"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_056.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7542
- /details14dc.html
imported:
- "2019"
_4images_image_id: "7542"
_4images_cat_id: "720"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7542 -->
