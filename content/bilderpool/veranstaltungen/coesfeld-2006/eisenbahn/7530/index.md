---
layout: "image"
title: "Eisenbahn von Ludger"
date: "2006-11-21T18:08:52"
picture: "Coesfeld_035.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7530
- /details9fd2.html
imported:
- "2019"
_4images_image_id: "7530"
_4images_cat_id: "719"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7530 -->
