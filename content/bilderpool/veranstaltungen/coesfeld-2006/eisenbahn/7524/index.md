---
layout: "image"
title: "Eisenbahn von Ludger"
date: "2006-11-21T17:43:16"
picture: "Coesfeld_010.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7524
- /detailsb2b7.html
imported:
- "2019"
_4images_image_id: "7524"
_4images_cat_id: "719"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7524 -->
