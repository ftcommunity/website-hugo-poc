---
layout: "image"
title: "COE 13"
date: "2006-12-20T21:50:26"
picture: "157_5800.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8043
- /detailse3da.html
imported:
- "2019"
_4images_image_id: "8043"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8043 -->
