---
layout: "image"
title: "Kleindetails am bestaunen"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_138.jpg"
weight: "6"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "stephan\'s Frau"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7589
- /detailse50f.html
imported:
- "2019"
_4images_image_id: "7589"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7589 -->
