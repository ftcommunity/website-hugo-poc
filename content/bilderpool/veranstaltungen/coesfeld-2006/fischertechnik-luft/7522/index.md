---
layout: "image"
title: "beim Modelle bestaunen"
date: "2006-11-21T17:43:16"
picture: "Coesfeld_006.jpg"
weight: "4"
konstrukteure: 
- "diverse"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7522
- /details0982.html
imported:
- "2019"
_4images_image_id: "7522"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T17:43:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7522 -->
