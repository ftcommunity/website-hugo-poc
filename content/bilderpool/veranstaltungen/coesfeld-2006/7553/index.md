---
layout: "image"
title: "Tieflader mit Planierraupe"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_082.jpg"
weight: "8"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7553
- /details51ad.html
imported:
- "2019"
_4images_image_id: "7553"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7553 -->
