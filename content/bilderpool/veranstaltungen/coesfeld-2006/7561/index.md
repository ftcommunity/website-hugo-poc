---
layout: "image"
title: "Ein Geschicklichkeitsspiel"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_095.jpg"
weight: "15"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7561
- /details9839.html
imported:
- "2019"
_4images_image_id: "7561"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7561 -->
Hier ist ein schönes Geschicklichkeitsspiel zu sehen. Es war für kleinere Kinder gedacht und in den Dosen versteckte sich manchmal eine kleine Überraschung.
