---
layout: "image"
title: "Tieflader mit Raupe"
date: "2007-03-23T23:43:25"
picture: "159_5902.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9750
- /detailsb0d2.html
imported:
- "2019"
_4images_image_id: "9750"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9750 -->
