---
layout: "image"
title: "Autofabrik von M. Busch"
date: "2006-11-21T18:08:52"
picture: "Coesfeld_027.jpg"
weight: "4"
konstrukteure: 
- "M. Busch"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7528
- /details85f7.html
imported:
- "2019"
_4images_image_id: "7528"
_4images_cat_id: "1564"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7528 -->
