---
layout: "image"
title: "Waschstrasse von M. Busch"
date: "2006-11-21T18:08:52"
picture: "Coesfeld_019.jpg"
weight: "1"
konstrukteure: 
- "M. Busch"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7525
- /detailsdf49.html
imported:
- "2019"
_4images_image_id: "7525"
_4images_cat_id: "1564"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7525 -->
