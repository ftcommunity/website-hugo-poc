---
layout: "image"
title: "Waschstraße"
date: "2007-03-23T23:43:25"
picture: "159_5932.jpg"
weight: "7"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9777
- /detailsad29.html
imported:
- "2019"
_4images_image_id: "9777"
_4images_cat_id: "1564"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9777 -->
...und läuft alles richtig?
