---
layout: "image"
title: "COE 58"
date: "2006-12-20T21:50:26"
picture: "158_5833.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/8082
- /detailsa433.html
imported:
- "2019"
_4images_image_id: "8082"
_4images_cat_id: "1298"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8082 -->
