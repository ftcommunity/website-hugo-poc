---
layout: "image"
title: "Übersicht SPS"
date: "2007-03-23T23:43:25"
picture: "159_5924.jpg"
weight: "84"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9771
- /detailsd87b.html
imported:
- "2019"
_4images_image_id: "9771"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9771 -->
