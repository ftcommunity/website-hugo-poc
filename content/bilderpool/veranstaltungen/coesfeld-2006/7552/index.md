---
layout: "image"
title: "Teilansicht der Raupe"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_079.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7552
- /details695a.html
imported:
- "2019"
_4images_image_id: "7552"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7552 -->
