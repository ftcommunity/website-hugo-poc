---
layout: "image"
title: "Schiffsschaukel"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_115.jpg"
weight: "9"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7571
- /details4fa7.html
imported:
- "2019"
_4images_image_id: "7571"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7571 -->
