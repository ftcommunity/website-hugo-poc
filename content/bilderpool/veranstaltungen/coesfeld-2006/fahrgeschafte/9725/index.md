---
layout: "image"
title: "Kassenhäuschen"
date: "2007-03-23T23:43:25"
picture: "158_5871.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9725
- /detailsccda.html
imported:
- "2019"
_4images_image_id: "9725"
_4images_cat_id: "721"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9725 -->
