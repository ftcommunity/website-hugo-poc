---
layout: "image"
title: "Ein Rummelplatzmodell"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_104.jpg"
weight: "6"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7568
- /detailsbd06.html
imported:
- "2019"
_4images_image_id: "7568"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7568 -->
Noch ein schönes Rummelplatzmodell.

So eins wollte ich mal bauen aber aus Teilemangel wurde daraus nichts. War auch recht schwer.
