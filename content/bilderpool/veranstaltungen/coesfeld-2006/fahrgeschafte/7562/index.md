---
layout: "image"
title: "Ein Rummelplatzmodell"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_097.jpg"
weight: "1"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7562
- /details969d.html
imported:
- "2019"
_4images_image_id: "7562"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7562 -->
Dieses Modell hab ich in Natura noch nie auf einem Rummelplatz gesehen. Sieht aber sehr interessant aus.
