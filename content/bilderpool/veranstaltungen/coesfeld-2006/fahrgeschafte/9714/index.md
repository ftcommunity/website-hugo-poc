---
layout: "image"
title: "Kimesmodell"
date: "2007-03-23T23:43:25"
picture: "158_5860.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9714
- /details5175.html
imported:
- "2019"
_4images_image_id: "9714"
_4images_cat_id: "721"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9714 -->
