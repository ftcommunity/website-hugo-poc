---
layout: "image"
title: "Kirmesmodelle"
date: "2007-03-23T23:43:25"
picture: "158_5861.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9715
- /details73b2.html
imported:
- "2019"
_4images_image_id: "9715"
_4images_cat_id: "721"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9715 -->
Es ist unglaublich welche Variationen von Kassenhäuschen es gibt...
