---
layout: "image"
title: "ft Männchen mit Presslufthammer"
date: "2006-11-21T18:38:19"
picture: "Coesfeld_092.jpg"
weight: "13"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7558
- /detailsa5fa-2.html
imported:
- "2019"
_4images_image_id: "7558"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:38:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7558 -->
