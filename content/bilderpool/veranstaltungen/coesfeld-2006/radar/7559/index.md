---
layout: "image"
title: "Radar"
date: "2007-03-23T23:43:25"
picture: "Coesfeld_093.jpg"
weight: "1"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7559
- /detailsaef7.html
imported:
- "2019"
_4images_image_id: "7559"
_4images_cat_id: "1299"
_4images_user_id: "130"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7559 -->
