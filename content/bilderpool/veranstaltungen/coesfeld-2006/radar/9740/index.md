---
layout: "image"
title: "Nabe"
date: "2007-03-23T23:43:25"
picture: "158_5891.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9740
- /details3292.html
imported:
- "2019"
_4images_image_id: "9740"
_4images_cat_id: "1299"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9740 -->
