---
layout: "image"
title: "fliegendes Flugzeug"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_121.jpg"
weight: "17"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7576
- /details791e.html
imported:
- "2019"
_4images_image_id: "7576"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7576 -->
