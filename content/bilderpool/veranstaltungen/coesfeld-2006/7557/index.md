---
layout: "image"
title: "ft Männchen mit Presslufthammer"
date: "2006-11-21T18:38:19"
picture: "Coesfeld_089.jpg"
weight: "12"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7557
- /detailsde47.html
imported:
- "2019"
_4images_image_id: "7557"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:38:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7557 -->
