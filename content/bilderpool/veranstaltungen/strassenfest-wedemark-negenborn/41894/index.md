---
layout: "image"
title: "Der Kugelwerfer"
date: "2015-09-23T09:52:30"
picture: "strassenfest01.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/41894
- /details1d42.html
imported:
- "2019"
_4images_image_id: "41894"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41894 -->
Hier werden die Kugeln auf halbe Gelenksteine als Transportmedium geworfen.
