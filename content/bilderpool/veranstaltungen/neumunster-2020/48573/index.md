---
layout: "image"
title: "Autoscooter 6"
date: 2020-04-15T09:00:36+02:00
picture: "NeumünsterAutoscooter06.jpg"
weight: "8"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Figürchen sind hell(auf) begeistert
