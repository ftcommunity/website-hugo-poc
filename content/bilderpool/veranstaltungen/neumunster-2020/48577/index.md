---
layout: "image"
title: "Autoscooter 2"
date: 2020-04-15T09:00:41+02:00
picture: "NeumünsterAutoscooter02.jpg"
weight: "4"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Website-Team"
license: "unknown"
---

Vernebelte Wagen
