---
layout: "image"
title: "Autoscooter 1"
date: 2020-04-15T09:00:42+02:00
picture: "NeumünsterAutoscooter01.jpg"
weight: "3"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Website-Team"
license: "unknown"
---

Im Dunkeln gelassen
