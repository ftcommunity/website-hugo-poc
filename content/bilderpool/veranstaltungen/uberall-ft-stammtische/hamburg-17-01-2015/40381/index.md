---
layout: "image"
title: "Bergbahn"
date: "2015-01-17T22:00:11"
picture: "stammtischhamburg4.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/40381
- /details6abd.html
imported:
- "2019"
_4images_image_id: "40381"
_4images_cat_id: "3026"
_4images_user_id: "373"
_4images_image_date: "2015-01-17T22:00:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40381 -->
Ebenfalls Ralf Geerken, inzwischen mit Silberling-Steuerung.
