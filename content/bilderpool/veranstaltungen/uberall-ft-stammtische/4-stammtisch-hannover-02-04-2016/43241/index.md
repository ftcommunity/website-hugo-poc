---
layout: "image"
title: "stammtisch14.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43241
- /detailse979.html
imported:
- "2019"
_4images_image_id: "43241"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43241 -->
