---
layout: "image"
title: "Herbert H. und Tochter"
date: "2016-04-05T11:46:23"
picture: "stammtisch4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/43256
- /details53e9.html
imported:
- "2019"
_4images_image_id: "43256"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43256 -->
Sie waren erst den Tag davor aus Peking gekommen.
