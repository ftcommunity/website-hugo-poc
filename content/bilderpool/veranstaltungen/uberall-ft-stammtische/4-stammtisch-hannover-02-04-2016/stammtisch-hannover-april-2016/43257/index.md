---
layout: "image"
title: "Winkelachsen einmal anders"
date: "2016-04-05T11:46:23"
picture: "stammtisch5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/43257
- /details7e18.html
imported:
- "2019"
_4images_image_id: "43257"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43257 -->
Das Grundgerüst für den Mikrofon-Ständer
