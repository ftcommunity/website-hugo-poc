---
layout: "comment"
hidden: true
title: "21906"
date: "2016-04-03T19:04:00"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Grundsätzlich gilt: Die Beschleunigung eines Körpers auf der schiefen Ebene (wie in diesem Fall) ist unabhängig von dessen Masse. Demnach müsste Fall 3 eintreten, dass nämlich beide Wagen gleichzeitig ankommen.
Ein Beispiel aus dem Alltag verdeutlicht jedoch das Gegenteil: Man sagt, ein schwerer Radfahrer sei beim Bergabfahren im Vorteil. Dies würde doch dem Gesetz widersprechen, oder? Im Falle des Radfahrers ist der Schwerere im Vorteil, da er bei größerer Masse ein ähnlich stark durch die Luftreibung gebremst wird im Vergleich zu einem leichtereren Fahrer. Die wirkende Hangabtriebskraft des schweren Radfahrers ist größer als die des leichten Fahrers, die Reibungskraft der Luft aber gleich groß. Dadurch wirkt beim schweren Fahrer eine unverhältnismäßg größere beschleunigende Kraft, weshalb dieser stärker beschleunigt und damit im Vorteil ist.

Im Falle der ft Autos schätze ich die Wirkung der Luftreibung als zu gering ein, als dass sie diesen Effekt spürbar machen würde. Es bleibt also dabei: Beide Wagen kommen gleichzeitig unten an.

Mehr dazu in der aktuellen ft:pedia: https://ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-1.pdf

Grüße, David