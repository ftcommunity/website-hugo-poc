---
layout: "image"
title: "stammtisch05.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch05.jpg"
weight: "5"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43232
- /details544a-2.html
imported:
- "2019"
_4images_image_id: "43232"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43232 -->
