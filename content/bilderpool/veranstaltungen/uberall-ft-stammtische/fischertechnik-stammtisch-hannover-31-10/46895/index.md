---
layout: "image"
title: "fischertechnik Fahrturm"
date: "2017-11-06T16:08:24"
picture: "stammtisch09.jpg"
weight: "9"
konstrukteure: 
- "Herbert"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46895
- /details4571.html
imported:
- "2019"
_4images_image_id: "46895"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46895 -->
