---
layout: "image"
title: "Diverse Teile"
date: "2017-11-06T16:08:24"
picture: "stammtisch01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46887
- /detailsca08.html
imported:
- "2019"
_4images_image_id: "46887"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46887 -->
