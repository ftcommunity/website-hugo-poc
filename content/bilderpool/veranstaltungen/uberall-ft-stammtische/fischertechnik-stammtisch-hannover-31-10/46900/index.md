---
layout: "image"
title: "Taster"
date: "2017-11-06T16:08:42"
picture: "stammtisch14.jpg"
weight: "14"
konstrukteure: 
- "Thomas"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46900
- /detailsb227.html
imported:
- "2019"
_4images_image_id: "46900"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46900 -->
