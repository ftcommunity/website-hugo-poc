---
layout: "image"
title: "Magnet 12 Volt"
date: "2017-11-06T16:08:24"
picture: "stammtisch05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46891
- /details861a.html
imported:
- "2019"
_4images_image_id: "46891"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46891 -->
