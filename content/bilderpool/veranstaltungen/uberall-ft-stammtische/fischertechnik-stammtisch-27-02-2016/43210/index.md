---
layout: "image"
title: "ft-kompatible Halterung für µC-Boards"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/43210
- /details4ef4.html
imported:
- "2019"
_4images_image_id: "43210"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43210 -->
