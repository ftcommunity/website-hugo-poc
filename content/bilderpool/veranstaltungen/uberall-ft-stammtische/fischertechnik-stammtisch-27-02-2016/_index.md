---
layout: "overview"
title: "fischertechnik-Stammtisch 27.02.2016  in Karlsruhe"
date: 2020-02-22T09:01:00+01:00
legacy_id:
- /php/categories/3209
- /categories7bc4.html
- /categories55f8.html
- /categoriesbe53.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3209 --> 
Wir trafen uns in Dirk Fox' Büroräumen, aßen gestifteten Kuchen und fachsimpelten :-)