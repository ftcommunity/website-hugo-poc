---
layout: "image"
title: "Illustre Runde"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/43204
- /details407a.html
imported:
- "2019"
_4images_image_id: "43204"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43204 -->
... mit ft, Kaffee und Kuchen - was braucht man mehr?
