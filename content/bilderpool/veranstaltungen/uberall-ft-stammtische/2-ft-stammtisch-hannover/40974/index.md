---
layout: "image"
title: "Kugelbahn"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover08.jpg"
weight: "8"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Rolf B"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- /php/details/40974
- /detailsc681.html
imported:
- "2019"
_4images_image_id: "40974"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40974 -->
Elektronisch gesteuerter Aufzugsarm.
@Thomas: Hast Du da ein Video von? Da könnte man die Funktion ohne vele Worte erfassen.
