---
layout: "image"
title: "Extra für fabse"
date: "2007-04-29T19:59:11"
picture: "hessen2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/10219
- /detailsd6d2.html
imported:
- "2019"
_4images_image_id: "10219"
_4images_cat_id: "925"
_4images_user_id: "373"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10219 -->
Das sind die Reste des Apfelkuchens. Wir ham dir (fabse) wie gefordert was übrig gelassen.
