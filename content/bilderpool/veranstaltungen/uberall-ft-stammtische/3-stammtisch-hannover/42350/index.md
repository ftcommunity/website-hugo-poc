---
layout: "image"
title: "stammtisch53.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch53.jpg"
weight: "53"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42350
- /detailsf7aa.html
imported:
- "2019"
_4images_image_id: "42350"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42350 -->
