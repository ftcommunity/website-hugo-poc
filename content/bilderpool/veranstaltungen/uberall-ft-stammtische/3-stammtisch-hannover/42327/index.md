---
layout: "image"
title: "stammtisch30.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch30.jpg"
weight: "30"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42327
- /detailsed0e-2.html
imported:
- "2019"
_4images_image_id: "42327"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42327 -->
