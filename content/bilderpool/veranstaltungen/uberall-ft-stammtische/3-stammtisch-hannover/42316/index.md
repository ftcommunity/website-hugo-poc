---
layout: "image"
title: "stammtisch19.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch19.jpg"
weight: "19"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42316
- /details67e6.html
imported:
- "2019"
_4images_image_id: "42316"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42316 -->
