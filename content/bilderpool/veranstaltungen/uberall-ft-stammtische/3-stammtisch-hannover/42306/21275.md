---
layout: "comment"
hidden: true
title: "21275"
date: "2015-11-12T12:08:27"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Das Rad ganz hinten rechts am Bildrand, also das mit den Bögen innendrin, soll wohl eher ein Riemenrad darstellen. Der zugehörige Riemen (die Kette mit den schwaren Belägen) hängt daneben auf der zentralen Welle. Ob das mit den Fördergliedern zum Verhaken im Sinne eines Zahnriemens gedacht ist, weiß ich nicht. Aber das ist eine hochinteressante Idee für Antriebe.

Das ganze Bauwerk ist unbeschreiblich gut. Ich wüßte nur auch gerne mal. was das werden soll. Ein altes Clubmodell vielleicht?

Grüße
H.A.R.R.Y.