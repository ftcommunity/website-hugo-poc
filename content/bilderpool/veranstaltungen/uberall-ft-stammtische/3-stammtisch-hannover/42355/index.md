---
layout: "image"
title: "stammtisch58.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch58.jpg"
weight: "58"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42355
- /detailsa440.html
imported:
- "2019"
_4images_image_id: "42355"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42355 -->
