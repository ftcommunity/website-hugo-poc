---
layout: "image"
title: "stammtisch47.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch47.jpg"
weight: "47"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42344
- /details558a.html
imported:
- "2019"
_4images_image_id: "42344"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42344 -->
