---
layout: "image"
title: "stammtisch39.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch39.jpg"
weight: "39"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42336
- /details7c3a.html
imported:
- "2019"
_4images_image_id: "42336"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42336 -->
