---
layout: "image"
title: "stammtisch59.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch59.jpg"
weight: "59"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42356
- /detailsed91.html
imported:
- "2019"
_4images_image_id: "42356"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42356 -->
