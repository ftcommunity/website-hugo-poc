---
layout: "image"
title: "stammtisch31.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch31.jpg"
weight: "31"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42328
- /details1a37.html
imported:
- "2019"
_4images_image_id: "42328"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42328 -->
