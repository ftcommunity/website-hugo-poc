---
layout: "image"
title: "stammtisch36.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch36.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42333
- /detailsb548.html
imported:
- "2019"
_4images_image_id: "42333"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42333 -->
