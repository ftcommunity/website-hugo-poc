---
layout: "image"
title: "stammtisch02.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch02.jpg"
weight: "2"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42299
- /details7b92.html
imported:
- "2019"
_4images_image_id: "42299"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42299 -->
