---
layout: "image"
title: "stammtisch17.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch17.jpg"
weight: "17"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42314
- /detailsac9a-2.html
imported:
- "2019"
_4images_image_id: "42314"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42314 -->
