---
layout: "image"
title: "stammtisch45.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch45.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42342
- /detailsabab.html
imported:
- "2019"
_4images_image_id: "42342"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42342 -->
