---
layout: "comment"
hidden: true
title: "21289"
date: "2015-11-15T18:34:43"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Richtig. Der Erfinderstolz verpufft, wenn man das Werk nicht würdigen kann.

Gruß,
Harald