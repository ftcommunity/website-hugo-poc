---
layout: "image"
title: "stammtisch29.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch29.jpg"
weight: "29"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42326
- /detailscea1.html
imported:
- "2019"
_4images_image_id: "42326"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42326 -->
