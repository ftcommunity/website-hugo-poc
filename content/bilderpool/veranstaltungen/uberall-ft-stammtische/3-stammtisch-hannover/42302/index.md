---
layout: "image"
title: "stammtisch05.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch05.jpg"
weight: "5"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/42302
- /details9f52.html
imported:
- "2019"
_4images_image_id: "42302"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42302 -->
