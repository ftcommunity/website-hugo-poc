---
layout: "image"
title: "Micha's fischertechnik Keller"
date: "2012-08-04T18:07:54"
picture: "DSC01039.jpg"
weight: "6"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/35247
- /details0088.html
imported:
- "2019"
_4images_image_id: "35247"
_4images_cat_id: "2612"
_4images_user_id: "997"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35247 -->
