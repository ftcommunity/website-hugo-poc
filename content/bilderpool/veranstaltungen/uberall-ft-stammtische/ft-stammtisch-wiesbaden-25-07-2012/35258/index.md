---
layout: "image"
title: "fischertechnik Stammtisch Wiesbaden - Die Hütte"
date: "2012-08-04T18:07:54"
picture: "DSC01110.jpg"
weight: "17"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/35258
- /details7516-2.html
imported:
- "2019"
_4images_image_id: "35258"
_4images_cat_id: "2612"
_4images_user_id: "997"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35258 -->
