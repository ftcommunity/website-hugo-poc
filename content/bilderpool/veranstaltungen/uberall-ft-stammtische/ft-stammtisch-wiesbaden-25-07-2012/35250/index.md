---
layout: "image"
title: "fischertechnik Stammtisch Wiesbaden"
date: "2012-08-04T18:07:54"
picture: "DSC01071.jpg"
weight: "9"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/35250
- /details23ab.html
imported:
- "2019"
_4images_image_id: "35250"
_4images_cat_id: "2612"
_4images_user_id: "997"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35250 -->
