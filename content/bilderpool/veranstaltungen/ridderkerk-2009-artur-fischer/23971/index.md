---
layout: "image"
title: "3D-Printer, Gabelstapler (heftruck), Rotatie-display + Wehre-Dokumentation"
date: "2009-05-10T11:50:51"
picture: "2009-RidderkerkArtur-Fischer_064.jpg"
weight: "23"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23971
- /detailse6a4.html
imported:
- "2019"
_4images_image_id: "23971"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:50:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23971 -->
