---
layout: "image"
title: "Gespräch mit Harold Jaarsma"
date: "2009-05-10T11:37:28"
picture: "2009-RidderkerkArtur-Fischer_060.jpg"
weight: "20"
konstrukteure: 
- "Gespräch mit Harold Jaarsma"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23968
- /details303b-2.html
imported:
- "2019"
_4images_image_id: "23968"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23968 -->
