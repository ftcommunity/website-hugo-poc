---
layout: "image"
title: "Artur Fischer......."
date: "2009-05-10T12:05:08"
picture: "2009-RidderkerkArtur-Fischer_100.jpg"
weight: "43"
konstrukteure: 
- "Artur Fischer"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23991
- /details93dd.html
imported:
- "2019"
_4images_image_id: "23991"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T12:05:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23991 -->
