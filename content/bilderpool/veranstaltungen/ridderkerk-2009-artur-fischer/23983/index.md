---
layout: "image"
title: "Fokker-Flugzeug"
date: "2009-05-10T12:04:49"
picture: "2009-RidderkerkArtur-Fischer_090.jpg"
weight: "35"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23983
- /details4982.html
imported:
- "2019"
_4images_image_id: "23983"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T12:04:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23983 -->
