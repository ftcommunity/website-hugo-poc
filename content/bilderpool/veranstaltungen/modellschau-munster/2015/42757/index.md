---
layout: "image"
title: "Modellschau Münster 22.11.15"
date: "2016-01-26T22:16:51"
picture: "muenster17.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/42757
- /detailsf6e5.html
imported:
- "2019"
_4images_image_id: "42757"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42757 -->
