---
layout: "image"
title: "muenster77.jpg"
date: "2016-01-26T22:16:51"
picture: "muenster77.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/42817
- /detailsf2b8.html
imported:
- "2019"
_4images_image_id: "42817"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42817 -->
