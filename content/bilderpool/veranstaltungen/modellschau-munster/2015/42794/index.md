---
layout: "image"
title: "Modellschau Münster 22.11.15"
date: "2016-01-26T22:16:51"
picture: "muenster54.jpg"
weight: "55"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/42794
- /detailsb2da-2.html
imported:
- "2019"
_4images_image_id: "42794"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42794 -->
