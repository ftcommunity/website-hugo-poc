---
layout: "image"
title: "Modellschau Münster 22.11.15 Aufbau"
date: "2016-01-26T22:16:51"
picture: "muenster10.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/42750
- /details265b.html
imported:
- "2019"
_4images_image_id: "42750"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42750 -->
