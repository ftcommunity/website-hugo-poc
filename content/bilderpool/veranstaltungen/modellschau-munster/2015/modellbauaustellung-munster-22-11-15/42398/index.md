---
layout: "image"
title: "Riesenrad"
date: "2015-11-28T11:42:24"
picture: "muenster01.jpg"
weight: "1"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42398
- /detailsf8cb-2.html
imported:
- "2019"
_4images_image_id: "42398"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42398 -->
