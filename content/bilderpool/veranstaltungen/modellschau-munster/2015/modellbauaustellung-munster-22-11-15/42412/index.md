---
layout: "image"
title: "Brückenkran"
date: "2015-11-28T11:42:24"
picture: "muenster15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42412
- /details2737-2.html
imported:
- "2019"
_4images_image_id: "42412"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42412 -->
