---
layout: "image"
title: "Wiener Riesenrad"
date: "2015-11-28T11:42:24"
picture: "muenster58.jpg"
weight: "58"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42455
- /details4a87.html
imported:
- "2019"
_4images_image_id: "42455"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42455 -->
