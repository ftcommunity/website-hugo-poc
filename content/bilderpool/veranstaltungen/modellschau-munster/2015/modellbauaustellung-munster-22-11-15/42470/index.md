---
layout: "image"
title: "Portalkran"
date: "2015-11-28T11:42:24"
picture: "muenster73.jpg"
weight: "73"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42470
- /details410c.html
imported:
- "2019"
_4images_image_id: "42470"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42470 -->
