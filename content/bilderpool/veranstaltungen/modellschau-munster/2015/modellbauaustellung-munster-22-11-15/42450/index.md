---
layout: "image"
title: "Sonderteile"
date: "2015-11-28T11:42:24"
picture: "muenster53.jpg"
weight: "53"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42450
- /details7685.html
imported:
- "2019"
_4images_image_id: "42450"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42450 -->
