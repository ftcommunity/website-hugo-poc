---
layout: "image"
title: "Sägewerk"
date: "2015-11-28T11:42:24"
picture: "muenster36.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42433
- /detailsca0c-2.html
imported:
- "2019"
_4images_image_id: "42433"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42433 -->
