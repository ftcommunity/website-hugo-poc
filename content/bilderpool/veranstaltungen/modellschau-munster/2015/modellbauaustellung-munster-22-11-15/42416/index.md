---
layout: "image"
title: "Kugelbahn"
date: "2015-11-28T11:42:24"
picture: "muenster19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42416
- /details81d1.html
imported:
- "2019"
_4images_image_id: "42416"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42416 -->
