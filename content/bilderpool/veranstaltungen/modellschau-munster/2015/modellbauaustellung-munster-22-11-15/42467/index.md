---
layout: "image"
title: "Radarstation"
date: "2015-11-28T11:42:24"
picture: "muenster70.jpg"
weight: "70"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42467
- /details3391.html
imported:
- "2019"
_4images_image_id: "42467"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42467 -->
