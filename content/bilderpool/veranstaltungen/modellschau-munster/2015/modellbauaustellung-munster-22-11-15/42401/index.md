---
layout: "image"
title: "Pistenbully hinten"
date: "2015-11-28T11:42:24"
picture: "muenster04.jpg"
weight: "4"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42401
- /details310a.html
imported:
- "2019"
_4images_image_id: "42401"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42401 -->
