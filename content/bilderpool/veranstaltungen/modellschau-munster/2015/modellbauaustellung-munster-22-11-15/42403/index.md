---
layout: "image"
title: "Schopf Bergbau Radlader"
date: "2015-11-28T11:42:24"
picture: "muenster06.jpg"
weight: "6"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42403
- /details5c1c.html
imported:
- "2019"
_4images_image_id: "42403"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42403 -->
