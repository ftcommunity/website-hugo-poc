---
layout: "image"
title: "Schachspiel"
date: "2015-11-28T11:42:24"
picture: "muenster21.jpg"
weight: "21"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42418
- /details84ce.html
imported:
- "2019"
_4images_image_id: "42418"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42418 -->
