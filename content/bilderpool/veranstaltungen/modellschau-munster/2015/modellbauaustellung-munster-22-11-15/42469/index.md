---
layout: "image"
title: "Kleinmodelle"
date: "2015-11-28T11:42:24"
picture: "muenster72.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42469
- /details637f-4.html
imported:
- "2019"
_4images_image_id: "42469"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42469 -->
