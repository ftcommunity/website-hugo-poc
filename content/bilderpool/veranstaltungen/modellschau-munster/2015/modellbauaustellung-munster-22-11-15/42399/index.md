---
layout: "image"
title: "Pistenbully"
date: "2015-11-28T11:42:24"
picture: "muenster02.jpg"
weight: "2"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42399
- /details8876.html
imported:
- "2019"
_4images_image_id: "42399"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42399 -->
