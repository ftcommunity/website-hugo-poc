---
layout: "image"
title: "6-Achs Roboterarm Encoder"
date: "2015-11-28T11:42:24"
picture: "muenster50.jpg"
weight: "50"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42447
- /detailsc6cf-2.html
imported:
- "2019"
_4images_image_id: "42447"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42447 -->
