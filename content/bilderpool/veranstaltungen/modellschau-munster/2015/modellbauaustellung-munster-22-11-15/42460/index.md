---
layout: "image"
title: "Wiener Riesenrad Antrieb"
date: "2015-11-28T11:42:24"
picture: "muenster63.jpg"
weight: "63"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42460
- /details6f2d-3.html
imported:
- "2019"
_4images_image_id: "42460"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42460 -->
