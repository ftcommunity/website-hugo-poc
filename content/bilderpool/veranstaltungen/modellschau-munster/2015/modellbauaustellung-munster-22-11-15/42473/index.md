---
layout: "image"
title: "Ball-Roboter"
date: "2015-11-28T11:42:24"
picture: "muenster76.jpg"
weight: "76"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42473
- /details61a9-2.html
imported:
- "2019"
_4images_image_id: "42473"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42473 -->
