---
layout: "image"
title: "Bearbeitungszentrum"
date: "2015-11-28T11:42:24"
picture: "muenster45.jpg"
weight: "45"
konstrukteure: 
- "Heinrich Fuchs"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42442
- /detailsf01f.html
imported:
- "2019"
_4images_image_id: "42442"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42442 -->
