---
layout: "image"
title: "Glücksspiel Rutsche"
date: "2015-11-28T11:42:24"
picture: "muenster40.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42437
- /details0f44-3.html
imported:
- "2019"
_4images_image_id: "42437"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42437 -->
Die Steine werden über eine Rutsche auf das Spielfeld gelegt.
