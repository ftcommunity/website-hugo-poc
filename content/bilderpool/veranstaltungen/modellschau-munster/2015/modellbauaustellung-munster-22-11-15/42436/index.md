---
layout: "image"
title: "Glücksspiel"
date: "2015-11-28T11:42:24"
picture: "muenster39.jpg"
weight: "39"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42436
- /detailsd456.html
imported:
- "2019"
_4images_image_id: "42436"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42436 -->
Man bekommt aus einem Magazin weisse und schwarze Steine.
