---
layout: "image"
title: "Binär-Addierer"
date: "2015-11-28T11:42:24"
picture: "muenster24.jpg"
weight: "24"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42421
- /details6977-2.html
imported:
- "2019"
_4images_image_id: "42421"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42421 -->
