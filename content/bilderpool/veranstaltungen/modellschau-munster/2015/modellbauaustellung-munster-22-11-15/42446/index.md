---
layout: "image"
title: "6-Achs Roboterarm Arm"
date: "2015-11-28T11:42:24"
picture: "muenster49.jpg"
weight: "49"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/42446
- /detailsabd7.html
imported:
- "2019"
_4images_image_id: "42446"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42446 -->
