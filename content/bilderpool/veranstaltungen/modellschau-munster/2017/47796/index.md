---
layout: "image"
title: "kvgftmodellschau32.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau32.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47796
- /detailsb132-3.html
imported:
- "2019"
_4images_image_id: "47796"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47796 -->
