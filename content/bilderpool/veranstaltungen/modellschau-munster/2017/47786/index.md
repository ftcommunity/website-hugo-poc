---
layout: "image"
title: "kvgftmodellschau22.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47786
- /detailsc9eb.html
imported:
- "2019"
_4images_image_id: "47786"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47786 -->
