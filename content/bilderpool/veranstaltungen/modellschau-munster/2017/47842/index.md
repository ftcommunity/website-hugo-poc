---
layout: "image"
title: "kvgftmodellschau78.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau78.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47842
- /details37f3.html
imported:
- "2019"
_4images_image_id: "47842"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47842 -->
