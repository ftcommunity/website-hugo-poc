---
layout: "image"
title: "kvgftmodellschau10.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47774
- /details2704-2.html
imported:
- "2019"
_4images_image_id: "47774"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47774 -->
