---
layout: "image"
title: "kvgftmodellschau07.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47771
- /details9f42.html
imported:
- "2019"
_4images_image_id: "47771"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47771 -->
