---
layout: "image"
title: "kvgftmodellschau64.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau64.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47828
- /details8273-2.html
imported:
- "2019"
_4images_image_id: "47828"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47828 -->
