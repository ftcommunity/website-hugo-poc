---
layout: "image"
title: "kvgftmodellschau59.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau59.jpg"
weight: "59"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/47823
- /detailsb233.html
imported:
- "2019"
_4images_image_id: "47823"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47823 -->
