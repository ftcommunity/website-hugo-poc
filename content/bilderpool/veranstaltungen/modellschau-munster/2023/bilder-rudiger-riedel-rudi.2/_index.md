---
layout: "overview"
title: "Bilder von Rüdiger Riedel (Rudi)"
date: 2023-02-07T20:38:14+01:00
---

Ein herzlicher Dank an Roland Keßelmann für die Ausrichtung dieser wunderhübschen Ausstellung und dass ich dabei sein durfte! Es war ein wunderbarer Tag. Ausgelaugt aber glücklich dürften die Aussteller am Abend gewesen sein. Viele Familien, Eltern und Großeltern kamen mit ihren Kindern. Neben der Freude über die Modelle gab es die Freude über den kulinarischen Bereich.