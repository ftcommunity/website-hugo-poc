---
layout: "image"
title: "Riesenrad, klein"
date: 2023-02-07T20:29:33+01:00
picture: "CIMG3793.JPG"
weight: "21"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist ein Riesenrad aus dem Kasten "FUN PARK". Der Antrieb erfolgt über eine Handkurbel.