---
layout: "image"
title: "Mars-Roboter von hinten"
date: 2023-02-07T20:29:40+01:00
picture: "CIMG3787.JPG"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist der Mars-Roboter noch einmal von hinten.