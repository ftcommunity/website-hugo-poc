---
layout: "image"
title: "Riesenrad, neuestes"
date: 2023-02-07T20:29:37+01:00
picture: "CIMG3789.JPG"
weight: "19"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist die Riesenrad-Galerie: Neuestes (aus dem Kasten "SUPER FUN PARK")