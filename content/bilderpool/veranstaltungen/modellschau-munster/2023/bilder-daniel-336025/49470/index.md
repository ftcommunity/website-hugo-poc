---
layout: "image"
title: "Riesenrad, klein 2"
date: 2023-02-07T20:29:32+01:00
picture: "CIMG3794.JPG"
weight: "22"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Riesenrad ist glaube ich auch aus dem Kasten "FUN PARK". Der Antrieb erfolgt über eine Handkurbel.