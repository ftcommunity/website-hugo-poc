---
layout: "image"
title: "Mars-Roboter"
date: 2023-02-07T20:29:43+01:00
picture: "CIMG3785.JPG"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Mars-Roboter den man selber per Joystick steuern kann. Ziel: Ball einfangen und zum Start bringen. "Garnicht so leicht."