---
layout: "image"
title: "CIMG3782"
date: 2023-02-07T20:29:45+01:00
picture: "CIMG3782.JPG"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Karte vom Konstruckteur des Seilbaggers.