---
layout: "image"
title: "Mixer"
date: 2023-02-07T20:29:23+01:00
picture: "CIMG3773.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Mixer/Rührgerät für die Küche. Davor geschaltet, wie es aussieht, ein zwei-Gang Getriebe.