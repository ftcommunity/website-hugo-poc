---
layout: "image"
title: "CIMG3780"
date: 2023-02-07T20:29:47+01:00
picture: "CIMG3780.JPG"
weight: "10"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Kabine und Motorraum des Seilbaggers.