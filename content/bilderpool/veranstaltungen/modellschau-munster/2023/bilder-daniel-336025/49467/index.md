---
layout: "image"
title: "Riesenrad, sehr alt"
date: 2023-02-07T20:29:29+01:00
picture: "CIMG3797.JPG"
weight: "25"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein ganz altes Riesenrad. Die Gondel sind glaube ich aus Schaumstoff. Aus welchem Kasten es stammt und wie es angetrieben wird weiß ich nicht.