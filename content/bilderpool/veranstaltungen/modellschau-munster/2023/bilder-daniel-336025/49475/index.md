---
layout: "image"
title: "Elefant aus 'Animal friends'"
date: 2023-02-07T20:29:38+01:00
picture: "CIMG3792.JPG"
weight: "18"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist ein Elefant aus dem neuen Kasten "Animal friends". Ob er wirklich aus dem Kasten ist oder nur aus den Bauteilen weiß ich nicht.