---
layout: "image"
title: "CIMG3778"
date: 2023-02-07T20:29:17+01:00
picture: "CIMG3778.JPG"
weight: "9"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Noch einmal der Seilbagger, jetzt leicht nach rechts gedreht (aus Baggerfahrers Sicht).