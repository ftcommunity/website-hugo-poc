---
layout: "image"
title: "Seilbagger"
date: 2023-02-07T20:29:18+01:00
picture: "CIMG3777.JPG"
weight: "8"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein großer roter Seilbagger.