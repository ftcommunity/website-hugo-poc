---
layout: "image"
title: "Mars-Roboter von schräg hinten"
date: 2023-02-07T20:29:39+01:00
picture: "CIMG3788.JPG"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Siehe: "Mars-Roboter von schräg hinten/der Seite" (zwei Bilder vorher).