---
layout: "image"
title: "Murmelbahn"
date: 2023-02-07T20:29:19+01:00
picture: "CIMG3776.JPG"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Eine schöne gelb-grüne Murmelbahn neben der schicken Bühne