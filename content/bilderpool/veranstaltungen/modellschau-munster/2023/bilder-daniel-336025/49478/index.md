---
layout: "image"
title: "Mars-Roboter von schräg hinten/der Seite"
date: 2023-02-07T20:29:42+01:00
picture: "CIMG3786.JPG"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist der Mars Roboter noch einmal von schräg hinten (oder von der Seite, wie man's nimmt).