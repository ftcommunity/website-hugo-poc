---
layout: "image"
title: "Das verrückte Sofa"
date: 2023-02-07T20:29:36+01:00
picture: "CIMG3772.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Arm mit Gondel (Sofa) ähnlich dem "Fighter" oder dem "Flinging Circus" auf Volksfesten.