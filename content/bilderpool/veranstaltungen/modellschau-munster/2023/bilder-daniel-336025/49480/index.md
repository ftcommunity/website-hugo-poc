---
layout: "image"
title: "lustiges Fahrzeug"
date: 2023-02-07T20:29:44+01:00
picture: "CIMG3783.JPG"
weight: "13"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses lustig anmutendes Fahrzeug fuhr von Zeit zu Zeit auf dem Boden herum.