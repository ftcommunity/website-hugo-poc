---
layout: "image"
title: "CIMG3781"
date: 2023-02-07T20:29:46+01:00
picture: "CIMG3781.JPG"
weight: "11"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Dies ist der Text zu dem Seilbagger aus dem vorherigen Bild. Beim folgenden ist auch die Karte des Konstruckteurs zu sehen.