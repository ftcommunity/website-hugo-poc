---
layout: "image"
title: "Waschanlage"
date: 2023-02-07T20:29:22+01:00
picture: "CIMG3774.JPG"
weight: "4"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Eine automatische Waschanlage mit Selbstbedienung und Auto.