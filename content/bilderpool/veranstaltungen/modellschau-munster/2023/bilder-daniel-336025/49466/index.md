---
layout: "image"
title: "Riesenrad, klein und alt"
date: 2023-02-07T20:29:27+01:00
picture: "CIMG3798.JPG"
weight: "26"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Schon wieder so ein ganz kleines und zwar ein altes. Baukasten: Weiß ich nicht. Antrieb: Handkurbel.