---
layout: "image"
title: "Willkommensschild"
date: 2023-02-07T20:29:49+01:00
picture: "CIMG3770.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein lustiges Schild das die Leute auf der Ausstellung begrüßt.