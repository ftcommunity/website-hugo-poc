---
layout: "image"
title: "Riesenrad, neu"
date: 2023-02-07T20:29:35+01:00
picture: "CIMG3790.JPG"
weight: "20"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier sind zwei Riesenräder aus dem Kasten "Oeco energy", er ist noch relativ neu.