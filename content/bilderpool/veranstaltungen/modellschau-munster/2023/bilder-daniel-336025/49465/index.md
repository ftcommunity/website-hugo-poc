---
layout: "image"
title: "Riesenrad, klein und alt 2"
date: 2023-02-07T20:29:26+01:00
picture: "CIMG3799.JPG"
weight: "27"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Wahrscheinlich ist dieses Riesenrad das gleiche wie das auf dem vorherigen Bild. Kasten: ?; Antrieb: Handkurbel. Dieses Riesenrad ist bestimmt das älteste bei diesen hier, denn die noch älteren links daneben habe ich aus welchem Grund auch immer nicht aufgenommen.