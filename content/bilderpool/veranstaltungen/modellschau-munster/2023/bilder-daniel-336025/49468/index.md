---
layout: "image"
title: "und ein weiteres Riesenrad"
date: 2023-02-07T20:29:30+01:00
picture: "CIMG3796.JPG"
weight: "24"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Daniel (336025)"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist noch ein Riesenrad, warscheinlich ist es aus dem "FUN PARK", Vielleicht ist es aber auch ein älterer Kasten. Der Antrieb erfolgt über einen M-Motor.