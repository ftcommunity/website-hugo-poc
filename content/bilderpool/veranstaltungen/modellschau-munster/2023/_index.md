﻿---
layout: "overview"
title: "2023"
date: 2023-02-08T13:48:08+01:00
---

Die Modellschau fand am 29. Januar 2023 von 10:00 - 17:00 Uhr in der Pausenhalle des Kardinal-von-Galen-Gymnasiums in Münster statt.

Ergänzend wurden kostenlose Lötkurse (kleine elektronischen Schaltungen) für Kinder von den Funkamateuren aus Münster angeboten und es gabe eine fischertechnik-Spielecke für Kinder. Außerdem wure mit Waffeln, Kaffee und Würstchen für das leibliche Wohl gesorgt.
