---
layout: "image"
title: "Schöne Modelle"
date: "2012-11-20T21:40:43"
picture: "hbz22.jpg"
weight: "22"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36131
- /details8fd1.html
imported:
- "2019"
_4images_image_id: "36131"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36131 -->
