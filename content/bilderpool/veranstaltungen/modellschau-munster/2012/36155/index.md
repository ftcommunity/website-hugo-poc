---
layout: "image"
title: "Kuchentheke reichlich gefüllt"
date: "2012-11-20T21:40:43"
picture: "hbz46.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36155
- /detailsf6d2.html
imported:
- "2019"
_4images_image_id: "36155"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36155 -->
