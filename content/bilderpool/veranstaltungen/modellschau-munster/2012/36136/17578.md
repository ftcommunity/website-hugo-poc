---
layout: "comment"
hidden: true
title: "17578"
date: "2012-11-27T17:27:44"
uploadBy:
- "Maximilian Ammenwerth"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan.

Das Bild oben zeigt einen Plotter. Der kann in einem Bereich von 7cm mal 5,5cm einzelne Punkte "drucken". Dazu wird ein Zylinder, an dessen Ende sich ein kleiner Schwamm befindet nach unten auf das Papier gedrückt. Seine Position bestimmt er, indem er die Anzahl der Umdrehungen (der Motoren) mit zwei Tastern zählt. Eine Umdrehung bedeutet 4 Tastendrücke. Auf der langen Achse sind das etwa 112 Tastendrücke, und auf der kürzeren 64. 
Mit dem Schwamm habe ich allerdings nur eine Auflösung von 17x13 Pixeln, weil der Schwamm so dick ist. Für ein Smilie reicht das.
Das Motiv kann man ändern, indem man Excel Dateien importiert. In den Dateien müssen dann die Koordinaten der Punkte stehen, die Farbe bekommen sollen.

Viele Grüße,
Maximilian