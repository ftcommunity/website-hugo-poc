---
layout: "image"
title: "Big Roy mit Heuwender"
date: "2012-11-20T21:40:43"
picture: "hbz51.jpg"
weight: "51"
konstrukteure: 
- "Tobias Tacke"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36160
- /details1c2c.html
imported:
- "2019"
_4images_image_id: "36160"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36160 -->
