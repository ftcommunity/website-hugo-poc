---
layout: "image"
title: "wenn der Vater mit dem Sohne"
date: "2012-11-20T21:40:43"
picture: "hbz47.jpg"
weight: "47"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36156
- /details0521-2.html
imported:
- "2019"
_4images_image_id: "36156"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36156 -->
