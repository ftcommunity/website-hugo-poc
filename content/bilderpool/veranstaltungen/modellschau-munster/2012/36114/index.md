---
layout: "image"
title: "beim Aufbau"
date: "2012-11-20T21:40:42"
picture: "hbz05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36114
- /details5a81.html
imported:
- "2019"
_4images_image_id: "36114"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36114 -->
