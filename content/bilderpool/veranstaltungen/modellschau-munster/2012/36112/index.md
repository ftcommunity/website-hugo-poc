---
layout: "image"
title: "Riesenrad"
date: "2012-11-20T21:40:42"
picture: "hbz03.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36112
- /details3de7.html
imported:
- "2019"
_4images_image_id: "36112"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36112 -->
