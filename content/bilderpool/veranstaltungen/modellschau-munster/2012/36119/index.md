---
layout: "image"
title: "Roboter"
date: "2012-11-20T21:40:42"
picture: "hbz10.jpg"
weight: "10"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36119
- /details3d2c.html
imported:
- "2019"
_4images_image_id: "36119"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36119 -->
