---
layout: "image"
title: "Martinum Gymnasium"
date: "2012-11-20T21:40:42"
picture: "hbz19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36128
- /detailsaf38.html
imported:
- "2019"
_4images_image_id: "36128"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36128 -->
