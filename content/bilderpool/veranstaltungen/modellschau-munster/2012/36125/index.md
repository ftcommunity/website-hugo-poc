---
layout: "image"
title: "KVG Hiltrup"
date: "2012-11-20T21:40:42"
picture: "hbz16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36125
- /detailsb61c.html
imported:
- "2019"
_4images_image_id: "36125"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36125 -->
