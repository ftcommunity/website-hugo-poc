---
layout: "image"
title: "Roboter mal anders...."
date: "2012-11-20T21:40:43"
picture: "hbz36.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36145
- /details2b83.html
imported:
- "2019"
_4images_image_id: "36145"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36145 -->
