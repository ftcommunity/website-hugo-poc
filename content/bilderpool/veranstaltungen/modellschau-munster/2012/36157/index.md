---
layout: "image"
title: "wenn der Vater mit dem Sohne"
date: "2012-11-20T21:40:43"
picture: "hbz48.jpg"
weight: "48"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36157
- /details3870.html
imported:
- "2019"
_4images_image_id: "36157"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36157 -->
