---
layout: "image"
title: "landwirtschaftliche Geräte"
date: "2012-11-20T21:40:43"
picture: "hbz23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Meinert"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36132
- /details75f0.html
imported:
- "2019"
_4images_image_id: "36132"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36132 -->
