---
layout: "image"
title: "Tresor von innen"
date: "2012-11-20T21:40:43"
picture: "hbz50.jpg"
weight: "50"
konstrukteure: 
- "Dominik Tacke"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36159
- /detailsc596.html
imported:
- "2019"
_4images_image_id: "36159"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36159 -->
