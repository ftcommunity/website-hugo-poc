---
layout: "image"
title: "Tresor"
date: "2012-11-20T21:40:43"
picture: "hbz53.jpg"
weight: "53"
konstrukteure: 
- "Dominik Tacke"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36162
- /details44e9.html
imported:
- "2019"
_4images_image_id: "36162"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36162 -->
