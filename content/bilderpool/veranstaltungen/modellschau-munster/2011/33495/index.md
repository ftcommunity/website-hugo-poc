---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau13.jpg"
weight: "13"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33495
- /details0692.html
imported:
- "2019"
_4images_image_id: "33495"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33495 -->
