---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau02.jpg"
weight: "2"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33484
- /details5a10.html
imported:
- "2019"
_4images_image_id: "33484"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33484 -->
