---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:17"
picture: "fischertechnikmodellschau28.jpg"
weight: "28"
konstrukteure: 
- "Rob"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33510
- /details196f-2.html
imported:
- "2019"
_4images_image_id: "33510"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:17"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33510 -->
