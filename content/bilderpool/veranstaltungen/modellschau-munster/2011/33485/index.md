---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:16"
picture: "fischertechnikmodellschau03.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/33485
- /detailsf864.html
imported:
- "2019"
_4images_image_id: "33485"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33485 -->
