---
layout: "image"
title: "Flyer zur Modellschau Münster 2016"
date: "2016-09-18T09:26:13"
picture: "flyer1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/44384
- /details7b0f.html
imported:
- "2019"
_4images_image_id: "44384"
_4images_cat_id: "3279"
_4images_user_id: "182"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44384 -->
Hallo zusammen,
am Sonntag, den 20.11.2016, findet wieder eine große Fischertechnik-Modellschau in Münster statt. Veranstaltungsort ist diesmal aber nicht wie in den letzten Jahren das HBZ Münster, sondern das Kardinal-von-Galen Gymnasium in Münster Hiltrup.

In der Aula der Schule werden in der Zeit von 10:00 bis 17:00 Uhr eine Vielzahl von Modellen gezeigt. Neben vielen privaten Ausstellern werden auch mehrere Schulen mit den Produkten ihrer Arbeitsgemeinschaften vertreten sein. Weitere Aussteller (privat oder gerne auch weitere Schulen) sind jederzeit herzlich willkommen!

Der Förderverein des Kardinal-von-Galen Gymnasiums wird an diesem Sonntag alle Aussteller und Gäste mit Kaffee, Kuchen, Waffeln und Würstchen versorgen. Der Eintritt zur Ausstellung ist natürlich frei!

Ort:
Kardinal-von-Galen Gymnasium
Zum Roten Berge 25
48165 Münster
