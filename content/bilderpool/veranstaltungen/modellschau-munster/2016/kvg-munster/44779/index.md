---
layout: "image"
title: "Überblick"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44779
- /detailscf09.html
imported:
- "2019"
_4images_image_id: "44779"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44779 -->
