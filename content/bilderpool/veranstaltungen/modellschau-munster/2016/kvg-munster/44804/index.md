---
layout: "image"
title: "Mäsing"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster28.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44804
- /detailsb1f1.html
imported:
- "2019"
_4images_image_id: "44804"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44804 -->
