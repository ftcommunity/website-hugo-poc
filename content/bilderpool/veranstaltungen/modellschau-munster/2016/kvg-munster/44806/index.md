---
layout: "image"
title: "Mäsing"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster30.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44806
- /details7aa7.html
imported:
- "2019"
_4images_image_id: "44806"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44806 -->
