---
layout: "image"
title: "Ostermann"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster38.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44814
- /details736d.html
imported:
- "2019"
_4images_image_id: "44814"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44814 -->
