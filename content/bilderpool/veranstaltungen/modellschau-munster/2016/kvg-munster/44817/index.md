---
layout: "image"
title: "Püttmann"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster41.jpg"
weight: "41"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- /php/details/44817
- /details6041-2.html
imported:
- "2019"
_4images_image_id: "44817"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44817 -->
