---
layout: "comment"
hidden: true
title: "22775"
date: "2016-11-29T08:39:28"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

ja, das soll es sein. Links das Wasserrad über den sie angetrieben werden soll.
In der Mitte die Übersetzung für den Vortrieb, rechts der Turm mit der Säge (hier Zahnstange).
Der Vortrieb geht, wenn ein bestimmter Hebel am Wagen umgelegt ist, automatisch.
Im Modell wird alles mit Kurbel angetrieben.

Gruß ludger