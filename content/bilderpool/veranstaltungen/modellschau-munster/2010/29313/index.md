---
layout: "image"
title: "Roboterarm04"
date: "2010-11-20T23:44:10"
picture: "Roboterarm04.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29313
- /details607e.html
imported:
- "2019"
_4images_image_id: "29313"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T23:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29313 -->
