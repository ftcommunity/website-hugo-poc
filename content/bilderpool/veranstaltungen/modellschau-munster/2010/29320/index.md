---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:44"
picture: "hbz01.jpg"
weight: "47"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29320
- /details8c28-2.html
imported:
- "2019"
_4images_image_id: "29320"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29320 -->
Modelle von Andreas und Tobias Tacke
