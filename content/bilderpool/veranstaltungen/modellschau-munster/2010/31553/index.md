---
layout: "image"
title: "Radwippe2"
date: "2011-08-08T21:55:42"
picture: "IMG_4219.JPG"
weight: "85"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31553
- /details18e6-2.html
imported:
- "2019"
_4images_image_id: "31553"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31553 -->
