---
layout: "image"
title: "Roboterarm06"
date: "2010-11-20T23:44:10"
picture: "Roboterarm.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29315
- /details7283.html
imported:
- "2019"
_4images_image_id: "29315"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T23:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29315 -->
