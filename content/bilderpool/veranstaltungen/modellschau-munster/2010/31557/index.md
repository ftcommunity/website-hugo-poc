---
layout: "image"
title: "Schranke1"
date: "2011-08-08T22:01:37"
picture: "IMG_4228.JPG"
weight: "89"
konstrukteure: 
- "Ludger"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Leiter", "130925"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31557
- /details97df-2.html
imported:
- "2019"
_4images_image_id: "31557"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:01:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31557 -->
Eine pfiffige Anwendung der ft-Feuerwehrleiter.
