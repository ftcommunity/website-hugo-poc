---
layout: "image"
title: "Indubot"
date: "2010-11-17T20:48:34"
picture: "Indubot.jpg"
weight: "18"
konstrukteure: 
- "Lars Blome"
- "Hendrik H."
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29288
- /details6a21.html
imported:
- "2019"
_4images_image_id: "29288"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:48:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29288 -->
