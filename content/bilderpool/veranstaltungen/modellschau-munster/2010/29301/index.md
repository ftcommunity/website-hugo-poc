---
layout: "image"
title: "Modelle von den Schülern"
date: "2010-11-20T22:09:02"
picture: "Modelle_von_den_Schlern.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29301
- /detailsdc96.html
imported:
- "2019"
_4images_image_id: "29301"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T22:09:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29301 -->
