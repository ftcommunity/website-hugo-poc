---
layout: "image"
title: "Abbau am Ende"
date: "2010-11-17T19:47:54"
picture: "Abbau.jpg"
weight: "2"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29272
- /detailscdc4.html
imported:
- "2019"
_4images_image_id: "29272"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T19:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29272 -->
