---
layout: "image"
title: "Schneidmaschine"
date: "2010-11-17T19:47:54"
picture: "Frser.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29281
- /detailsd431.html
imported:
- "2019"
_4images_image_id: "29281"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T19:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29281 -->
