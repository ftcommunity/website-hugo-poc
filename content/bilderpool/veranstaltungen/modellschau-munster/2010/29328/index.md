---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz09.jpg"
weight: "54"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29328
- /detailse42f.html
imported:
- "2019"
_4images_image_id: "29328"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29328 -->
Der Give Away, immer wieder gerne bespielt
