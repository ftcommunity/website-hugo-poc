---
layout: "image"
title: "Radwippe3"
date: "2011-08-08T21:56:01"
picture: "IMG_4220.JPG"
weight: "86"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31554
- /details2440.html
imported:
- "2019"
_4images_image_id: "31554"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:56:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31554 -->
