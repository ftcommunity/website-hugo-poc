---
layout: "image"
title: "KastenRoboter"
date: "2010-11-17T20:40:39"
picture: "IMGP0172.jpg"
weight: "16"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29286
- /details033d.html
imported:
- "2019"
_4images_image_id: "29286"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29286 -->
