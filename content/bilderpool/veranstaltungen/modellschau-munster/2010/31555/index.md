---
layout: "image"
title: "Autowaschsstraße"
date: "2011-08-08T21:56:40"
picture: "IMG_4221.JPG"
weight: "87"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31555
- /detailsff2e-2.html
imported:
- "2019"
_4images_image_id: "31555"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:56:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31555 -->
