---
layout: "image"
title: "Roter Zug"
date: "2010-11-15T17:29:12"
picture: "schwebebahn09.jpg"
weight: "2"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29260
- /details5e3c.html
imported:
- "2019"
_4images_image_id: "29260"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29260 -->
- Steuerung über IR- Control-Set
- Antrieb:1 XM-Motor
- 3 angetriebene Seilrollen