---
layout: "image"
title: "Roter Zug auf der Stecke"
date: "2010-11-15T17:29:12"
picture: "schwebebahn08.jpg"
weight: "1"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29259
- /details1690.html
imported:
- "2019"
_4images_image_id: "29259"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29259 -->
