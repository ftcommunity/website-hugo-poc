---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:44"
picture: "hbz04.jpg"
weight: "12"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29323
- /detailscd79-3.html
imported:
- "2019"
_4images_image_id: "29323"
_4images_cat_id: "2348"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29323 -->
Die Schwebebahn, echt Klasse
