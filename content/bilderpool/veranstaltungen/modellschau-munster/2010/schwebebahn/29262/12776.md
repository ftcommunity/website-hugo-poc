---
layout: "comment"
hidden: true
title: "12776"
date: "2010-11-16T01:41:49"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Ein sehr schönes Modell der Schwebebahn! Da müsste den Wuppertaler ft-Fans das Herz höher schlagen... Ließe sich mit den Schienen sogar eine Wendeschleife bauen?