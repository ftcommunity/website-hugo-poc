---
layout: "image"
title: "Blauer Zug"
date: "2010-11-15T17:29:12"
picture: "schwebebahn10.jpg"
weight: "3"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- /php/details/29261
- /details1171-2.html
imported:
- "2019"
_4images_image_id: "29261"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29261 -->
- Steuerung über TX- Controller
- Antrieb: 2 Encoder Motoren synchron
- 4 angetriebene Seilrollen
- 2 Reed Sensoren für Start, Beschleunigen, Abbremsen und Halt
- 8 LED Maxilight für die Beleuchtung