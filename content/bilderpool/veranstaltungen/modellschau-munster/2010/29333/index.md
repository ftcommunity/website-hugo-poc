---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz14.jpg"
weight: "59"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29333
- /detailsa59a-3.html
imported:
- "2019"
_4images_image_id: "29333"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29333 -->
