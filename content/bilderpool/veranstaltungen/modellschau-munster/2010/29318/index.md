---
layout: "image"
title: "Speed-star"
date: "2010-11-20T23:44:10"
picture: "Speedstarub-an01.jpg"
weight: "45"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29318
- /details5cda.html
imported:
- "2019"
_4images_image_id: "29318"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T23:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29318 -->
Hier mein speedstar mit farbwechselnder underbodenbeleuchtung
und extra schnelle überstztung
