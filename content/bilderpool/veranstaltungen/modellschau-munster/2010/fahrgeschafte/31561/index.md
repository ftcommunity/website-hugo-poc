---
layout: "image"
title: "Riesenrad und Fallturm"
date: "2011-08-08T22:10:15"
picture: "IMG_4252.JPG"
weight: "4"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31561
- /detailse771.html
imported:
- "2019"
_4images_image_id: "31561"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:10:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31561 -->
