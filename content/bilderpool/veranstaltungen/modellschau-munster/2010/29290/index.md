---
layout: "image"
title: "Lampenrad"
date: "2010-11-17T20:48:34"
picture: "Lampenrad.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29290
- /detailsc7b7.html
imported:
- "2019"
_4images_image_id: "29290"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:48:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29290 -->
