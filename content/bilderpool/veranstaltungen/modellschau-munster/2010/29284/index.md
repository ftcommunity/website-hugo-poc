---
layout: "image"
title: "Huhu Drucker"
date: "2010-11-17T20:24:50"
picture: "HUHU-Drucker.jpg"
weight: "14"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/29284
- /details0d9b-2.html
imported:
- "2019"
_4images_image_id: "29284"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:24:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29284 -->
