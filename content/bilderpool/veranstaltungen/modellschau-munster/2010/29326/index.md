---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz07.jpg"
weight: "52"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29326
- /details82ae-2.html
imported:
- "2019"
_4images_image_id: "29326"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29326 -->
