---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz22.jpg"
weight: "66"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29341
- /details4ce5.html
imported:
- "2019"
_4images_image_id: "29341"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29341 -->
Modelle von Brickwedde
