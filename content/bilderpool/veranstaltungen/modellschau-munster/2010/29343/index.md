---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz24.jpg"
weight: "68"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29343
- /details663b-3.html
imported:
- "2019"
_4images_image_id: "29343"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29343 -->
