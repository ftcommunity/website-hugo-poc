---
layout: "image"
title: "HBZ 2010"
date: "2010-11-22T20:42:45"
picture: "hbz33.jpg"
weight: "77"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29352
- /detailse12b.html
imported:
- "2019"
_4images_image_id: "29352"
_4images_cat_id: "2123"
_4images_user_id: "182"
_4images_image_date: "2010-11-22T20:42:45"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29352 -->
Ein Super Modell
