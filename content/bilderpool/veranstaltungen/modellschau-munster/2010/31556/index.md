---
layout: "image"
title: "Weiche"
date: "2011-08-08T22:00:18"
picture: "IMG_4226.JPG"
weight: "88"
konstrukteure: 
- "Ludger"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/31556
- /details884c.html
imported:
- "2019"
_4images_image_id: "31556"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:00:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31556 -->
Eine Weiche in Ludgers Schmalspurbahn.
