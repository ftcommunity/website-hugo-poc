---
layout: "image"
title: "ft Messestand"
date: "2008-11-14T21:31:57"
picture: "modellsuedbaubahn2.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/16272
- /detailse42e.html
imported:
- "2019"
_4images_image_id: "16272"
_4images_cat_id: "1470"
_4images_user_id: "409"
_4images_image_date: "2008-11-14T21:31:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16272 -->
Messe Modell Süd Bau & Bahn
