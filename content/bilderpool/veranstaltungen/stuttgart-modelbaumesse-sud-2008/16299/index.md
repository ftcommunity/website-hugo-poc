---
layout: "image"
title: "ferngesteuerter Hubschrauber"
date: "2008-11-17T21:08:29"
picture: "modelbaumessesued10.jpg"
weight: "19"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- /php/details/16299
- /details6bb3-2.html
imported:
- "2019"
_4images_image_id: "16299"
_4images_cat_id: "1470"
_4images_user_id: "845"
_4images_image_date: "2008-11-17T21:08:29"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16299 -->
