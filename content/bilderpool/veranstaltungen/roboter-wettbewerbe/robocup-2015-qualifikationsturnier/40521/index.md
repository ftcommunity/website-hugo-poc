---
layout: "image"
title: "Roboter im Testparcours"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40521
- /detailsfaee.html
imported:
- "2019"
_4images_image_id: "40521"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40521 -->
Testlauf am Bumper - eine echte Herausforderung für die kleinen Führungsrädchen (Team RobCross)