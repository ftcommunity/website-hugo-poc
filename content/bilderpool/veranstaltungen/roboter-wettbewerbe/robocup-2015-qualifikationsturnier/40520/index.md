---
layout: "image"
title: "Roboter im Testparcours"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40520
- /detailsa09e.html
imported:
- "2019"
_4images_image_id: "40520"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40520 -->
Testlauf in der Übungs-Arena (Team RobCross)