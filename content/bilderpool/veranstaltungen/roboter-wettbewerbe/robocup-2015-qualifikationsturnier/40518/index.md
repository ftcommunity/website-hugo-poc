---
layout: "image"
title: "Roboter am Wettkampf-Parcours"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40518
- /details4fcd.html
imported:
- "2019"
_4images_image_id: "40518"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40518 -->
Wettkampfarena der "Rescue A Primary"-Liga, vor dem ersten Wettlauf (Team RobCross)

Aufgabenstellung: 
Der entwickelte Roboter muss autonom
&#8226; einer (teilweise auf einer Länge von 10-15 cm unterbrochenen) Linie folgen
&#8226; Hindernisse auf der Linie umfahren und auf der Linie angebrachte &#8222;Bumper&#8220; bewältigen
&#8226; an Kreuzungen in einer (kurz vor dem Wettlauf) festgelegten Richtung abbiegen
&#8226; eine Rampe mit 22,5 % Steigung bewältigen
&#8226; einen offenen Raum (ohne Linienführung) erkennen (Alufolie am Eingang)
&#8226; in dem Raum ein &#8222;Opfer&#8220; (Getränkedose) finden, ergreifen, anheben und auf einem Podest in einer Ecke des Raumes abstellen.
Der Parcours muss innerhalb von acht Minuten bewältigt werden.