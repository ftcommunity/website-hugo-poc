---
layout: "image"
title: "Wettkampfversion des Roboters"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/40522
- /detailsafee.html
imported:
- "2019"
_4images_image_id: "40522"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40522 -->
Team RobCross