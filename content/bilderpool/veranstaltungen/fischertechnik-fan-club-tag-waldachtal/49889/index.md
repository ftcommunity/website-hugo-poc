---
layout: "image"
title: "Binärzähler, Rechengeräte, Astronomie"
date: 2023-05-13T16:11:40+02:00
picture: "Fan-Club-Tag_2023_089.jpg"
weight: "89"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Und ein "selbsttragendes" Innenzahnrad