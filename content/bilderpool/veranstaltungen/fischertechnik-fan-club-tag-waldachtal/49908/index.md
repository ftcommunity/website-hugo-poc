---
layout: "image"
title: "LKW"
date: 2023-05-13T16:12:04+02:00
picture: "Fan-Club-Tag_2023_071.jpg"
weight: "71"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Antrieb über Encodermotoren