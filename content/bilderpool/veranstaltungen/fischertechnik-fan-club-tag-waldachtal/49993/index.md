---
layout: "image"
title: "Das autonome Auto auf der Fahrbahn"
date: 2023-05-13T16:13:48+02:00
picture: "Fan-Club-Tag_2023_117.jpg"
weight: "117"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Jungs haben mir erzählt, dass ein Schiedsrichter die roten Klötze und welche in einer anderen Farbe irgendwo auf die Fahrbahn stellen, und die einen müssen dann automatisch rechts und die anderen links umfahren werden.