---
layout: "image"
title: "Faktorisierer"
date: 2023-05-13T16:11:48+02:00
picture: "Fan-Club-Tag_2023_083.jpg"
weight: "83"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Aus Thomas' Buch "Mathematik verstehen mit fischertechnik". Die Maschine probiert Kombinationen von Faktoren und hält an, wenn sie eine Lösung gefunden hat.