---
layout: "image"
title: "Kreissäge"
date: 2023-05-13T16:13:01+02:00
picture: "Fan-Club-Tag_2023_029.jpg"
weight: "29"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Blick von unten