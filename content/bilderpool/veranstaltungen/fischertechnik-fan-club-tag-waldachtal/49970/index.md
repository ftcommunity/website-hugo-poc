---
layout: "image"
title: "Frau Ottmann nebst Bohrmaschine"
date: 2023-05-13T16:13:19+02:00
picture: "Fan-Club-Tag_2023_015.jpg"
weight: "15"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch Detlefs Frau war mit von der Partie, erklärte und demonstrierte alles.