---
layout: "image"
title: "Mannshohes Kirmesmodell"
date: 2023-05-13T16:14:02+02:00
picture: "Fan-Club-Tag_2023_106.jpg"
weight: "106"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Stromschienen führen den kompletten Turm hinauf