---
layout: "image"
title: "Getränkeautomat"
date: 2023-05-13T16:13:38+02:00
picture: "Fan-Club-Tag_2023_124.jpg"
weight: "124"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Was er hinten in der Hand hält, ist die Becherablage vor der Füllstation hinten, die da aber gerade nicht angebracht war.