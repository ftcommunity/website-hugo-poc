---
layout: "image"
title: "Ein Dixie-Klo!"
date: 2023-05-13T16:12:19+02:00
picture: "Fan-Club-Tag_2023_006.jpg"
weight: "6"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Beleuchtung! Und auf der Tür-Außenseite steht auch "DIXI"! Und mit "Gas-Abzug"! Isses nich grandios?