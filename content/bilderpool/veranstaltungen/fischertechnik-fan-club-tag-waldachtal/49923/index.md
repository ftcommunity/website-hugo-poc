---
layout: "image"
title: "Roboterarme"
date: 2023-05-13T16:12:22+02:00
picture: "Fan-Club-Tag_2023_058.jpg"
weight: "58"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Z15 als Impulsrad verwendet