---
layout: "image"
title: "Selbstenttwister"
date: 2023-05-13T16:11:45+02:00
picture: "Fan-Club-Tag_2023_085.jpg"
weight: "85"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Lampe dreht sich ums Zentralzahnrad, und dennoch sind keine Schleifringe notwendig, um sie von außen mit Strom zu versorgen. Das Lämpchen wird immer so mit gedreht, dass die Litze sich nicht verdrillt.