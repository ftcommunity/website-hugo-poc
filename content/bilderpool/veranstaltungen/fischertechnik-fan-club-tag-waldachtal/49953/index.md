---
layout: "image"
title: "Holzverarbeitung"
date: 2023-05-13T16:12:58+02:00
picture: "Fan-Club-Tag_2023_030.jpg"
weight: "30"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Alles an Werkzeugen, was man da so braucht