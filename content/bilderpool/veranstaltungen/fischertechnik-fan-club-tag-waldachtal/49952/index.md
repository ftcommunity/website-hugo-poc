---
layout: "image"
title: "Gewinn-Angel-Spiel"
date: 2023-05-13T16:12:57+02:00
picture: "Fan-Club-Tag_2023_031.jpg"
weight: "31"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der grüne Knopf vorne unten startet das Spiel. Mit dem Joystick links daneben verfährt man die Greifhand (momentan links vorne oben) über den gewünschten Gewinn (es gab Dinge wie fischerTIP und Süßigkeiten zu erangeln). Druck auf den roten Taster (rechts neben dem grünen) oder 20 s warten senkt den Greifer, und wenn man gut gezielt hat, hat man den Gewinn gefischt.