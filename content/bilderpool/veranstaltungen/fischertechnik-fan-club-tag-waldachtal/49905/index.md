---
layout: "image"
title: "Hafenkran"
date: 2023-05-13T16:12:00+02:00
picture: "Fan-Club-Tag_2023_074.jpg"
weight: "74"
konstrukteure: 
- "Peter"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Beschreibung zum Zweiseil-Antrieb der Schaufel