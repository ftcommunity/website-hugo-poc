---
layout: "image"
title: "Kleine Rechenmaschine"
date: 2023-05-13T16:11:33+02:00
picture: "Fan-Club-Tag_2023_094.jpg"
weight: "94"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ebenfalls aus Thomas' Buch "Mathematik verstehen mit fischertechnik"