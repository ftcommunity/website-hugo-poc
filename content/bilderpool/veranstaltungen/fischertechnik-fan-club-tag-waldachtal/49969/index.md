---
layout: "image"
title: "Kettenglieder der Bohrmaschine"
date: 2023-05-13T16:13:18+02:00
picture: "Fan-Club-Tag_2023_016.jpg"
weight: "16"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Haufen Einzelteile für jedes Glied der Kette!