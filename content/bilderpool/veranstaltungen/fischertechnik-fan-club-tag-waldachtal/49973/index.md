---
layout: "image"
title: "Container"
date: 2023-05-13T16:13:23+02:00
picture: "Fan-Club-Tag_2023_135.jpg"
weight: "135"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die gute alte Statiklasche ist hier genau richtig :-)