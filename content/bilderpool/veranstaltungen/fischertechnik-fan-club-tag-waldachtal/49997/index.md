---
layout: "image"
title: "Riesenkran"
date: 2023-05-13T16:13:53+02:00
picture: "Fan-Club-Tag_2023_113.jpg"
weight: "113"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ich habe die Kamera hier am langen Arm hochgehalten und konnte trotzdem nur von unten fotografieren! Die Krümmung im Ausleger kommt - so vermute ich zumindest - von einem etwas zu stark angezogenen Spannseil.