---
layout: "image"
title: "Kreissäge"
date: 2023-05-13T16:13:02+02:00
picture: "Fan-Club-Tag_2023_028.jpg"
weight: "28"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier der verkleinerte Nachbau von Detlef Ottmanns Kreissäge.