---
layout: "image"
title: "Achterbahn"
date: 2023-05-13T16:12:44+02:00
picture: "Fan-Club-Tag_2023_041.jpg"
weight: "41"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Leider weiß ich den Namen des Erbauers nicht. Jedenfalls besteht die noch unvollendete Achterbahn jetzt schon aus über tausend Grundbausteinen! Und der Wagen rollte prima darauf.