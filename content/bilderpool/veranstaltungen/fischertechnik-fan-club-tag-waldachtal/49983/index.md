---
layout: "image"
title: "Pumpensteuerung"
date: 2023-05-13T16:13:35+02:00
picture: "Fan-Club-Tag_2023_126.jpg"
weight: "126"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Detailblick. Der Zylinder mit der etwas schräg verstärkten Rückholfederung ist ein On-Demand-Druckluft-Puffer.