---
layout: "image"
title: "Autokran"
date: 2023-05-13T16:12:11+02:00
picture: "Fan-Club-Tag_2023_066.jpg"
weight: "66"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Schöne Variante des ausfahrbaren Arms