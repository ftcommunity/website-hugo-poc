---
layout: "image"
title: "Steuerung der 3D-Mess-Maschine"
date: 2023-05-13T16:12:29+02:00
picture: "Fan-Club-Tag_2023_052.jpg"
weight: "52"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von diesem Block geht es zum Notebook daneben. Der kann dann ein Bild des vermessenen Werkstücks anzeigen.