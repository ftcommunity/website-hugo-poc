---
layout: "image"
title: "Riesenkran"
date: 2023-05-13T16:13:56+02:00
picture: "Fan-Club-Tag_2023_110.jpg"
weight: "110"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Monstrum braucht ein richtiges Fundament