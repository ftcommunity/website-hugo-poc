---
layout: "image"
title: "LKW"
date: 2023-05-13T16:12:02+02:00
picture: "Fan-Club-Tag_2023_072.jpg"
weight: "72"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Schöne Details und wieder eine neue Variante von Frontscheibe und Außenspiegel!