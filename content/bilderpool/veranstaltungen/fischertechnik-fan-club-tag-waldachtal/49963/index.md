---
layout: "image"
title: "Blick unter die Bohrmaschine"
date: 2023-05-13T16:13:11+02:00
picture: "Fan-Club-Tag_2023_021.jpg"
weight: "21"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Fahrwerk, Schleifringe