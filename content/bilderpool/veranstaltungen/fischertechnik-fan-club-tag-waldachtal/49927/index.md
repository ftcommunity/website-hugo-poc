---
layout: "image"
title: "Vielrichtungsfahrzeug"
date: 2023-05-13T16:12:27+02:00
picture: "Fan-Club-Tag_2023_054.jpg"
weight: "54"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Räder können gedreht werden.