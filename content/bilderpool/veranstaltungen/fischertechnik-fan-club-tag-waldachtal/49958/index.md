---
layout: "image"
title: "Ab ins Körbchen"
date: 2023-05-13T16:13:04+02:00
picture: "Fan-Club-Tag_2023_026.jpg"
weight: "26"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier fährt die Bohrmaschine nach Ende der Veranstaltung wieder in seinen Transportbehälter.