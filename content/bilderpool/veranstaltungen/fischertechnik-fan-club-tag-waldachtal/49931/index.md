---
layout: "image"
title: "Koordinaten-Messmaschine"
date: 2023-05-13T16:12:32+02:00
picture: "Fan-Club-Tag_2023_050.jpg"
weight: "50"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Tastkopf kann sozusagen die Umkehroperation eines 3D-Druckers ausführen: Er detektiert (wohl induktiv) und vermisst die großen Bohrungen im Metallwerkstück auf der Platte.