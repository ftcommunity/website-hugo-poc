---
layout: "image"
title: "Gewinnvorrat"
date: 2023-05-13T16:12:55+02:00
picture: "Fan-Club-Tag_2023_033.jpg"
weight: "33"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

An Gewinnen war kein Mangel :-)