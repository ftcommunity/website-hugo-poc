---
layout: "image"
title: "3D-Geometrie, Multiplikationshilfe"
date: 2023-05-13T16:11:32+02:00
picture: "Fan-Club-Tag_2023_095.jpg"
weight: "95"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Wie sehen wohl die Gummis aus, wenn man sehr schnell dreht?