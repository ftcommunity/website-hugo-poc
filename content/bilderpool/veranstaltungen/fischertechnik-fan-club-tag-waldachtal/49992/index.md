---
layout: "image"
title: "Autonomes Auto"
date: 2023-05-13T16:13:46+02:00
picture: "Fan-Club-Tag_2023_118.jpg"
weight: "118"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das ist wohl eine andere Variante als die auf der Fahrbahn