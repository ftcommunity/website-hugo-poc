---
layout: "image"
title: "Computersteuerung"
date: 2023-05-13T16:12:38+02:00
picture: "Fan-Club-Tag_2023_046.jpg"
weight: "46"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So soll die Bahn wohl mal aussehen. Wir dürfen gespannt sein!