---
layout: "image"
title: "Band 'die fischers'"
date: 2023-05-13T16:12:54+02:00
picture: "Fan-Club-Tag_2023_034.jpg"
weight: "34"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Von links nach rechts Schlagzeuger, Gitarrist, Pianist am Flügel. Und die bewegen sich tatsächlich. Ganz oben winkt wohl der Manager.