---
layout: "image"
title: "Kreissäge"
date: 2023-05-13T16:12:46+02:00
picture: "Fan-Club-Tag_2023_004.jpg"
weight: "4"
konstrukteure: 
- "Kreissäge"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

So sieht sie von unten aus. Links daneben der verkleinerte Nachbau von Christian Wiechmann.