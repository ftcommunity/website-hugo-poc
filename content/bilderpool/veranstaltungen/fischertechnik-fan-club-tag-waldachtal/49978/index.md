---
layout: "image"
title: "Verfahrbares Kranführerhaus"
date: 2023-05-13T16:13:29+02:00
picture: "Fan-Club-Tag_2023_130.jpg"
weight: "130"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Das Verfahren geht überraschend schnell, weil das Hubgetriebe nur führt und der Antrieb von einem U-Getriebe auf die Zahnstangen geht. Schöne Lösung!