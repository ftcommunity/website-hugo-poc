---
layout: "image"
title: "Leselektüre"
date: 2023-05-13T16:13:14+02:00
picture: "Fan-Club-Tag_2023_019.jpg"
weight: "19"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Dieses Detail *musste* ich einfach von Nahem aufnehmen.