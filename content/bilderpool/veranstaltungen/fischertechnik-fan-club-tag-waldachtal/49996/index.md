---
layout: "image"
title: "Autonomes Fahren"
date: 2023-05-13T16:13:51+02:00
picture: "Fan-Club-Tag_2023_114.jpg"
weight: "114"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Roboterfahrzeug für einen Wettbewerb