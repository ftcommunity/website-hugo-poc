---
layout: "image"
title: "Hafenkran"
date: 2023-05-13T16:11:57+02:00
picture: "Fan-Club-Tag_2023_076.jpg"
weight: "76"
konstrukteure: 
- "Peter"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Zweiseil-Greiferantrieb - ein Motor ist für Auf und Ab, der andere fürs Öffnen und Schließen zuständig