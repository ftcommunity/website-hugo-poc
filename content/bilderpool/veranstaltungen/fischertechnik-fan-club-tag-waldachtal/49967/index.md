---
layout: "image"
title: "Fahrer der Bohrmaschine"
date: 2023-05-13T16:13:15+02:00
picture: "Fan-Club-Tag_2023_018.jpg"
weight: "18"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Sogar seine Pausenlektüre ist dabei! Ein fantastisches Modell. Und es konnte trotz seines Gewichts ferngesteuert selbständig nicht nur in seine Transport-Holzkiste, sondern sogar über deren bestimmt mehr als 5 cm hohe Wand fahren.