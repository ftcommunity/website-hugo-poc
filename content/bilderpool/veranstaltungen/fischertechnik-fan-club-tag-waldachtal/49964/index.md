---
layout: "image"
title: "Rückseite der Bohrmaschine"
date: 2023-05-13T16:13:12+02:00
picture: "Fan-Club-Tag_2023_020.jpg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Im Störlichttubus ist, was eine Rückfahrkamera darstellen soll.