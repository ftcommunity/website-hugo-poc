---
layout: "image"
title: "Mannshohes Kirmesmodell"
date: 2023-05-13T16:14:06+02:00
picture: "Fan-Club-Tag_2023_103.jpg"
weight: "103"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Hier wird nicht gekleckert, hier wird geklotzt :-)