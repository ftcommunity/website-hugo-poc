---
layout: "image"
title: "Unterdruckpumpe"
date: 2023-05-13T16:11:29+02:00
picture: "Fan-Club-Tag_2023_097.jpg"
weight: "97"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Rutschkupplung, sodass man keine Endlagenabschaltung benötigt