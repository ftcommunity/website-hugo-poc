---
layout: "image"
title: "Mein eigener Stand"
date: 2023-05-13T16:13:22+02:00
picture: "Fan-Club-Tag_2023_136.jpg"
weight: "136"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Mit einem Freundes-Kind und meiner Frau. Von links nach rechts: Der aktuelle Druckluft-Ball-Balancierer in einer Convention-Dauerbetriebs-Fassung, die 50-Hz-Uhr mit elektromagnetisch erregtem Pendel, auch nochmal verbessert (Doku dazu folgt noch, ansonsten beides bereits im Bilderpool und auf YouTube), der Betonmischer aus der 1967er-Bauanleitung (der kommt noch in die ft:pedia), der Ur-ft 200 plus mot.1 plus Segmentscheibe (super für nicht ft kennende Besuchskinder, auch im Bilderpool), der Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten (siehe ft:pedia) und die zyklisch ungleichförmigen Getriebe (siehe ft:pedia und YouTube, 4 davon stecken auch im Getriebe des Ball-Balancierers).