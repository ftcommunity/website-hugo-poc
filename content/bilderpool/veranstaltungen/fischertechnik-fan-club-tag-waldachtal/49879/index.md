---
layout: "image"
title: "Lösung linearer Gleichungssysteme"
date: 2023-05-13T16:11:28+02:00
picture: "Fan-Club-Tag_2023_098.jpg"
weight: "98"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Diese Maschine hat Thomas schon mal in der ft:pedia 2/2014 als den "Seilcomputer Kelvin" ausführlich vorgestellt. Sie löst ein System aus zwei linearen Gleichungen mit zwei Unbekannten. Halt einfach fantastisch.