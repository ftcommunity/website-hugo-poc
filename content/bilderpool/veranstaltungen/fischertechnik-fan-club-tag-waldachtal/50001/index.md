---
layout: "image"
title: "Bagger"
date: 2023-05-13T16:13:57+02:00
picture: "Fan-Club-Tag_2023_011.jpg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der baggert Nudeln auf den LKW. Die sind leicht genug, dass der LKW seine Ladeplattform noch anheben kann.