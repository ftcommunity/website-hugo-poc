---
layout: "image"
title: "Oberseite der Bohrmaschine"
date: 2023-05-13T16:13:17+02:00
picture: "Fan-Club-Tag_2023_017.jpg"
weight: "17"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Besen war mal eine Zahnbürste! Das nennt man dann wohl Liebe zum Detail! Das Graue da hinten ist ein alter ft-Störlichttubus und steht symbolisch für eine Rückfahrkamera.