---
layout: "image"
title: "Multiplikationshilfe"
date: 2023-05-13T16:11:30+02:00
picture: "Fan-Club-Tag_2023_096.jpg"
weight: "96"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Auch bekannt als "Konrad, the educated monkey"