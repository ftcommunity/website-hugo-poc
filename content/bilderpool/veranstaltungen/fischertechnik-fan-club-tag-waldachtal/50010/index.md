---
layout: "image"
title: "Mannshohes Kirmesmodell"
date: 2023-05-13T16:14:09+02:00
picture: "Fan-Club-Tag_2023_101.jpg"
weight: "101"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Heavy-Duty-Maschinenbau: Der Ring mit den Personen dreht sich um den Turm und fährt auf und ab. Das runde Teil mit den senkrecht stehenden Streben dreht sich dabei und blinkt. Jede Menge toller Details.