---
layout: "image"
title: "Pumpsteuerung"
date: 2023-05-13T16:13:36+02:00
picture: "Fan-Club-Tag_2023_125.jpg"
weight: "125"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Kompressor pumpt Druckluft in eine Flasche. Deren Flüssigkeit wird daraufhin durch einen zweiten Schlauch heraus gedrückt. Es wurde extra bei ft bezüglich der Lebensmittel-Verwendbarkeit der Schläuche angefragt, sagte man mir. Die ist gegeben, wenn man die mit kochendem Wasser durchspült - was diese Pumperei ja auch erledigen kann.

Man sieht unten den Anschluss für die Flasche herabhängen. Die Flasche fehlt hier gerade.