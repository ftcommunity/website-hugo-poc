---
layout: "image"
title: "Portalkran"
date: 2023-05-13T16:12:13+02:00
picture: "Fan-Club-Tag_2023_064.jpg"
weight: "64"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Laufkatze mit drehbarer Seilaufhängung