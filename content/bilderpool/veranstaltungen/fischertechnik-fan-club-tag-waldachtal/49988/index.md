---
layout: "image"
title: "Getränkeautomat"
date: 2023-05-13T16:13:41+02:00
picture: "Fan-Club-Tag_2023_121.jpg"
weight: "121"
konstrukteure: 
- "ft-Fan"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Der Roboter in der Mitte macht die ganze Arbeit: Becher aus dem Magazin holen, hinten links vollgießen lassen, vorne abstellen, und den leeren Becher schließlich rechts in den Auswurf stellen. Die drei Akkus da sind Gegengewichte.