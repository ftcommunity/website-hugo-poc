---
layout: "image"
title: "Die Bastelecke"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44861
- /details726b.html
imported:
- "2019"
_4images_image_id: "44861"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44861 -->
