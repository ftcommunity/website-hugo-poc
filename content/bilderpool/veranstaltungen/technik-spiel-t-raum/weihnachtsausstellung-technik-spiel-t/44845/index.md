---
layout: "image"
title: "Vorderer Teil"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44845
- /details0563.html
imported:
- "2019"
_4images_image_id: "44845"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44845 -->
