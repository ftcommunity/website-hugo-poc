---
layout: "image"
title: "Schachbrett und Ewigkeitsmaschine"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44856
- /details0e48-2.html
imported:
- "2019"
_4images_image_id: "44856"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44856 -->
