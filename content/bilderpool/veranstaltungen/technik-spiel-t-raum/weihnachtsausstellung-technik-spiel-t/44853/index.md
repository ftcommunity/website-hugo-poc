---
layout: "image"
title: "Planetarium"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum21.jpg"
weight: "21"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44853
- /details130f-2.html
imported:
- "2019"
_4images_image_id: "44853"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44853 -->
