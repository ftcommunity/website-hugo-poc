---
layout: "image"
title: "Laufband für Hugo und Herrmann Roboter"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum14.jpg"
weight: "14"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/44846
- /detailsb929.html
imported:
- "2019"
_4images_image_id: "44846"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44846 -->
