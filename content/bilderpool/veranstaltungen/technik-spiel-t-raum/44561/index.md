---
layout: "image"
title: "Das mittlere Fach"
date: "2016-10-08T13:56:22"
picture: "technikspieltraum1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/44561
- /detailsec8b.html
imported:
- "2019"
_4images_image_id: "44561"
_4images_cat_id: "3314"
_4images_user_id: "381"
_4images_image_date: "2016-10-08T13:56:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44561 -->
Krokodillok + Anhänger
Anleitung licht-elektronik-Ausbaukasten l-e2
Lochkarte aus dem o.g. Baukasten
