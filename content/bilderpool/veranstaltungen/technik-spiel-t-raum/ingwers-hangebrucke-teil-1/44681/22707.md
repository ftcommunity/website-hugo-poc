---
layout: "comment"
hidden: true
title: "22707"
date: "2016-10-29T22:43:45"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Wäre schön eine Gesammtaufnahme in der "blauen Stunde" mit Beleuchtung zu haben. Sicher eine absolut sehenswerte Attraktion in jedem Garten?