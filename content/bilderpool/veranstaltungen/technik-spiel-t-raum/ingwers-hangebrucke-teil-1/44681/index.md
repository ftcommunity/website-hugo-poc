---
layout: "image"
title: "Die ersten Drähte sind gezogen"
date: "2016-10-25T14:48:43"
picture: "ingwershaengebruecketeil1.jpg"
weight: "1"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/44681
- /details7205.html
imported:
- "2019"
_4images_image_id: "44681"
_4images_cat_id: "3326"
_4images_user_id: "381"
_4images_image_date: "2016-10-25T14:48:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44681 -->
Die Brücke ist 4,37 lang und die gesamte Beleuchtung ist zwar schon fertig, aber Ingwer hatte sie an dem Tag abgebaut, dass wir besser die Drähte einziehen konneten.
