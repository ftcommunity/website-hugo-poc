---
layout: "image"
title: "dsc00046"
date: 2020-04-22T15:33:06+02:00
picture: "DSC00046.JPG"
weight: "28"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Ein kontrastreicher Kran
