---
layout: "image"
title: "dsc00085"
date: 2020-04-22T15:32:43+02:00
picture: "DSC00085.JPG"
weight: "47"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Mit Festhalten gilt der Versuch nicht
