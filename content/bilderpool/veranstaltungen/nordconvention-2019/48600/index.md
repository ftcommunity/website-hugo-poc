---
layout: "image"
title: "dsc00069"
date: 2020-04-22T15:32:53+02:00
picture: "DSC00069.JPG"
weight: "39"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Bildhaftes gibt's hier auch von Tobias