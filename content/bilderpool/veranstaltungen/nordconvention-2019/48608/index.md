---
layout: "image"
title: "dsc00053"
date: 2020-04-22T15:33:02+02:00
picture: "DSC00053.JPG"
weight: "31"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Wären es nicht Tischtennisbälle dann könnten sie glatt als Lemminge durchgehen
