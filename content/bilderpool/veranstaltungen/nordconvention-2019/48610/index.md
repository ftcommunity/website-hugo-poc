---
layout: "image"
title: "dsc00049"
date: 2020-04-22T15:33:05+02:00
picture: "DSC00049.JPG"
weight: "29"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Sausende Tischtennisballekens
