---
layout: "image"
title: "dsc00075"
date: 2020-04-22T15:32:50+02:00
picture: "DSC00075.JPG"
weight: "41"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Geduldig werden auch die Fragen der Kleinsten beantwortet