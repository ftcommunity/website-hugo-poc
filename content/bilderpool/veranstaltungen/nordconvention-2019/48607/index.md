---
layout: "image"
title: "dsc00057"
date: 2020-04-22T15:33:01+02:00
picture: "DSC00057.JPG"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Zum Glück steuern nicht beide mit Infrarot, sonst wüsste einer nicht wo der andere hinsteuert