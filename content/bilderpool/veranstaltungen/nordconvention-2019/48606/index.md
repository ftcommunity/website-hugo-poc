---
layout: "image"
title: "dsc00060"
date: 2020-04-22T15:33:00+02:00
picture: "DSC00060.JPG"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Zum Glück steuern nicht beide mit Infrarot, sonst wüsste einer nicht wo der andere hinsteuert