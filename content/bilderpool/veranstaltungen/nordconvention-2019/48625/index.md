---
layout: "image"
title: "dsc00022"
date: 2020-04-22T15:33:24+02:00
picture: "DSC00022.JPG"
weight: "14"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Ob sie wohl wirklich oben war? Diese hier jedenfalls nicht.
