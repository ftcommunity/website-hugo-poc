---
layout: "image"
title: "dsc00016"
date: 2020-04-22T15:33:27+02:00
picture: "DSC00016.JPG"
weight: "11"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Kleiner dürfte sie wirklich nicht sein (lt. Ingwer jedenfalls)
