---
layout: "image"
title: "dsc00019"
date: 2020-04-22T15:33:25+02:00
picture: "DSC00019.JPG"
weight: "13"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Silke Glauberstein"
uploadBy: "Website-Team"
license: "unknown"
---

Die Mühle übertrumpft alles
