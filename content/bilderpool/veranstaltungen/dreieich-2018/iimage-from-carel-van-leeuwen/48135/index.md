---
layout: "image"
title: "FT convention model"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen32.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/48135
- /detailsfd5a.html
imported:
- "2019"
_4images_image_id: "48135"
_4images_cat_id: "3533"
_4images_user_id: "136"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48135 -->
FT-Convention 2018