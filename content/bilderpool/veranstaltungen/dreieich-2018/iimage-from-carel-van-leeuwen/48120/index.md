---
layout: "image"
title: "They are still playing."
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/48120
- /detailsdf30.html
imported:
- "2019"
_4images_image_id: "48120"
_4images_cat_id: "3533"
_4images_user_id: "136"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48120 -->
FT-Convention 2018