---
layout: "image"
title: "FT convention model H. Nijkerk"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen39.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/48142
- /detailse34a.html
imported:
- "2019"
_4images_image_id: "48142"
_4images_cat_id: "3533"
_4images_user_id: "136"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48142 -->
FT-Convention 2018