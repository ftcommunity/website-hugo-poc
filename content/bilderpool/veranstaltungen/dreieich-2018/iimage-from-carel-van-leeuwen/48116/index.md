---
layout: "image"
title: "Mix of table tennis and fischertechnik in the hall."
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/48116
- /detailsa060-2.html
imported:
- "2019"
_4images_image_id: "48116"
_4images_cat_id: "3533"
_4images_user_id: "136"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48116 -->
FT-Convention 2018