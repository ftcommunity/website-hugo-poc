---
layout: "image"
title: "FT convention model P. Niekerk"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen38.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/48141
- /details60e7-2.html
imported:
- "2019"
_4images_image_id: "48141"
_4images_cat_id: "3533"
_4images_user_id: "136"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48141 -->
FT-Convention 2018