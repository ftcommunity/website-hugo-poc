---
layout: "image"
title: "Ablaufbahn, Gezeitenberechnungsmaschine"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention032.jpg"
weight: "32"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47951
- /details4443.html
imported:
- "2019"
_4images_image_id: "47951"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47951 -->
Von letzterer lesen wir hoffentlich noch in der ft:pedia Genaues!
