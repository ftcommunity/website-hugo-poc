---
layout: "image"
title: "Verschiedenes"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention087.jpg"
weight: "87"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48006
- /detailsbbde.html
imported:
- "2019"
_4images_image_id: "48006"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48006 -->
