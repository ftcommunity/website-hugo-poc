---
layout: "image"
title: "Flipper"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention099.jpg"
weight: "99"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48018
- /details132d.html
imported:
- "2019"
_4images_image_id: "48018"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48018 -->
