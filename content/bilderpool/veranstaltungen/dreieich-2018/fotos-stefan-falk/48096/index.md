---
layout: "image"
title: "Geländegängige Raupe"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention177.jpg"
weight: "177"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48096
- /details2bea.html
imported:
- "2019"
_4images_image_id: "48096"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "177"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48096 -->
Die Dreiecksraupen können sich federnd bewegen, das Fahrzeug kann sich verwinden und verfügt über eine pneumatische Knicklenkung.
