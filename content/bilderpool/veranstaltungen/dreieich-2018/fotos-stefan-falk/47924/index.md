---
layout: "image"
title: "Zahnstangenuhr, Kleinst-Motor"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention005.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47924
- /detailsf63a.html
imported:
- "2019"
_4images_image_id: "47924"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47924 -->
