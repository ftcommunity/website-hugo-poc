---
layout: "image"
title: "Traubenzucker-Spender"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention123.jpg"
weight: "123"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48042
- /details3732.html
imported:
- "2019"
_4images_image_id: "48042"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "123"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48042 -->
Links hinten wird Traubezucker geholt (siehe nächstes Bild). Der wird von einem Zug nach rechts gebracht und vom Förderwagen nach vorne. Greift der Besucher und holt sich ihn, unterbricht er eine Lichtschranke, und es geht von vorne los.
