---
layout: "image"
title: "ftDuino-Fahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention083.jpg"
weight: "83"
konstrukteure: 
- "Björn Gundermann"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48002
- /detailsbe6b.html
imported:
- "2019"
_4images_image_id: "48002"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48002 -->
