---
layout: "image"
title: "Originalverpackter Pneumatik-Kasten"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention068.jpg"
weight: "68"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47987
- /details78a3.html
imported:
- "2019"
_4images_image_id: "47987"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47987 -->
