---
layout: "image"
title: "Laufmaschine"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention039.jpg"
weight: "39"
konstrukteure: 
- "Huub van NIekerk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47958
- /details47d6.html
imported:
- "2019"
_4images_image_id: "47958"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47958 -->
