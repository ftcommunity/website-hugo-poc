---
layout: "image"
title: "3D-Scanner"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention054.jpg"
weight: "54"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47973
- /detailse7d2.html
imported:
- "2019"
_4images_image_id: "47973"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47973 -->
Man sieht das Bild der 3D-gescannten Ende
