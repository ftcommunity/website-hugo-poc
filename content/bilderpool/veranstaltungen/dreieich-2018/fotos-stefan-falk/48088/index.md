---
layout: "image"
title: "Ottomotor, Fahrzeuge"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention169.jpg"
weight: "169"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48088
- /details95d0.html
imported:
- "2019"
_4images_image_id: "48088"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "169"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48088 -->
