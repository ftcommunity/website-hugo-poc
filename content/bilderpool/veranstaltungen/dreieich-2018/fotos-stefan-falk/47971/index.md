---
layout: "image"
title: "Spurfolger"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention052.jpg"
weight: "52"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47971
- /details6dc0.html
imported:
- "2019"
_4images_image_id: "47971"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47971 -->
