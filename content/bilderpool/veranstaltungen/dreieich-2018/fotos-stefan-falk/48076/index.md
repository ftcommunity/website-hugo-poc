---
layout: "image"
title: "Pneumatik-Bagger"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention157.jpg"
weight: "157"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48076
- /detailsc34f.html
imported:
- "2019"
_4images_image_id: "48076"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "157"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48076 -->
