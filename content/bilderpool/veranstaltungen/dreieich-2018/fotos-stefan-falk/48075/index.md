---
layout: "image"
title: "Pneumatik-Bagger"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention156.jpg"
weight: "156"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48075
- /details4ca0.html
imported:
- "2019"
_4images_image_id: "48075"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "156"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48075 -->
Sehr pfiffig die so einfach "verlängerten" Pneumatikzylinder.
