---
layout: "image"
title: "Druckluftmotor"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention120.jpg"
weight: "120"
konstrukteure: 
- "Severin"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48039
- /details039a-2.html
imported:
- "2019"
_4images_image_id: "48039"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "120"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48039 -->
Mit einer sehr spannenden Mechanik.
