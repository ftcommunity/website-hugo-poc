---
layout: "image"
title: "Fahrroboter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention053.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47972
- /detailsfcd3.html
imported:
- "2019"
_4images_image_id: "47972"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47972 -->
