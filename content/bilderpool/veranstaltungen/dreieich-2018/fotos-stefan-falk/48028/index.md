---
layout: "image"
title: "Kreiselspiel, Klassik-Haus, Spielauto, nanoFramework-Uhr"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention109.jpg"
weight: "109"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48028
- /details60d3.html
imported:
- "2019"
_4images_image_id: "48028"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "109"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48028 -->
