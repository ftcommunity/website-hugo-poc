---
layout: "image"
title: "Roboter-Parcours"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention048.jpg"
weight: "48"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47967
- /details1e22.html
imported:
- "2019"
_4images_image_id: "47967"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47967 -->
