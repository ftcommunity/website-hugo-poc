---
layout: "image"
title: "Lissajous-Figuren-Malmaschine"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention033.jpg"
weight: "33"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47952
- /detailsd3e0.html
imported:
- "2019"
_4images_image_id: "47952"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47952 -->
Vorne unten wird ein kleines Blatt eingespannt, von oben ein Stift. Die beiden Zahnräder treiben die Mechanik, die sinusförmige Bewegungen des Stiftes in x- und y- Richtung, aber in unterschiedlicher Frequenz, bewirken.
