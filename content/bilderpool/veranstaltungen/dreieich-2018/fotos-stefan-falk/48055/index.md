---
layout: "image"
title: "Teil einer Verseilmaschine"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention136.jpg"
weight: "136"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48055
- /details7caa.html
imported:
- "2019"
_4images_image_id: "48055"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "136"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48055 -->
So richtig von Hand - das kleinere Gegenstück steht einen halben Meter weiter rechts. Hier werden Schnüre eingespannt, aus denen ein Seil geflochten werden kann.
