---
layout: "image"
title: "Kugelbahn-Xylophon"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention023.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47942
- /detailsc3e4.html
imported:
- "2019"
_4images_image_id: "47942"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47942 -->
Die roten Finger im Hintergrund drücken auf "Klaviertasten", die Kugeln auf die blauen Teile stoßen, an deren unterem Ende sie gegen Xylophon-Metallplatten stoßen (siehe nächstes Bild). So kann man eine Melodie spielen.
