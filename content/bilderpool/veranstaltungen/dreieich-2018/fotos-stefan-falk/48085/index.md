---
layout: "image"
title: "Müllwagen"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention166.jpg"
weight: "166"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48085
- /details4f01.html
imported:
- "2019"
_4images_image_id: "48085"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "166"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48085 -->
