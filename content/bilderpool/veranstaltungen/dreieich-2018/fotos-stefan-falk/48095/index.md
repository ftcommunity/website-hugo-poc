---
layout: "image"
title: "50-Hz-Antriebe"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention176.jpg"
weight: "176"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48095
- /details2912.html
imported:
- "2019"
_4images_image_id: "48095"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "176"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48095 -->
