---
layout: "image"
title: "Demonstration stehender Wellen"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention029.jpg"
weight: "29"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47948
- /detailsc36a.html
imported:
- "2019"
_4images_image_id: "47948"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47948 -->
Je nachdem, an welchem Faden man das System erregt, ergeben sich verschiedene stehende Wellen.
