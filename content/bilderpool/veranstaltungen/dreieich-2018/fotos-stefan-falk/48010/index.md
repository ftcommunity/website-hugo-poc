---
layout: "image"
title: "Schiffschaukel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention091.jpg"
weight: "91"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48010
- /details18ad.html
imported:
- "2019"
_4images_image_id: "48010"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48010 -->
