---
layout: "image"
title: "50-Hz-Antriebe"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention174.jpg"
weight: "174"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48093
- /detailsfb4b.html
imported:
- "2019"
_4images_image_id: "48093"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "174"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48093 -->
