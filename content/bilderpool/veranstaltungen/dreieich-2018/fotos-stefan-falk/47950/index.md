---
layout: "image"
title: "Erklärungen"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention031.jpg"
weight: "31"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47950
- /detailseed3-2.html
imported:
- "2019"
_4images_image_id: "47950"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47950 -->
