---
layout: "image"
title: "Kugelbahnmodell-Strecke"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention017.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47936
- /detailsbc31.html
imported:
- "2019"
_4images_image_id: "47936"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47936 -->
