---
layout: "image"
title: "Bahn mit Lego-Zug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention074.jpg"
weight: "74"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47993
- /details856c.html
imported:
- "2019"
_4images_image_id: "47993"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47993 -->
In den Schienen sind Verzahnungen, an denen sich der Zug zieht. Er kippt oben Kugeln auf die Kugelbahn, die er unten wieder aufnimmt.
