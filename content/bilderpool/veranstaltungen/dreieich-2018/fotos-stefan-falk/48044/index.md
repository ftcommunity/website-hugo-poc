---
layout: "image"
title: "ftDuino-Modell"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention125.jpg"
weight: "125"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48044
- /details2680.html
imported:
- "2019"
_4images_image_id: "48044"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "125"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48044 -->
