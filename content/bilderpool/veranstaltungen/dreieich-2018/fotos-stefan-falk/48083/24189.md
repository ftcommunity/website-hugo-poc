---
layout: "comment"
hidden: true
title: "24189"
date: "2018-09-26T00:15:20"
uploadBy:
- "Claus"
license: "unknown"
imported:
- "2019"
---
Als Frontlenker konzipiert, ergänzt er das Kurzhauer-Müllfahrzeug und die Langhauber-Zugmaschiene. RC gesteuert. Funktionen: Fahren, Lenken, ein- und ausfahren der Rampe, heben und senken der hinteren Fahrzeugplattform, alle Lichtfunktionen.