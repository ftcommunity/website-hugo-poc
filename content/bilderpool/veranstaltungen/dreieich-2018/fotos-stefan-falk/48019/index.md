---
layout: "image"
title: "3D-Drucker"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention100.jpg"
weight: "100"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48019
- /details631f.html
imported:
- "2019"
_4images_image_id: "48019"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48019 -->
