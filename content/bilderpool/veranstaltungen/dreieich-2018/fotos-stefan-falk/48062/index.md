---
layout: "image"
title: "Pneumatische Dampflok"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention143.jpg"
weight: "143"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48062
- /details9dc0.html
imported:
- "2019"
_4images_image_id: "48062"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "143"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48062 -->
