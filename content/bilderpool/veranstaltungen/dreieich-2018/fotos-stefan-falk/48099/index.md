---
layout: "image"
title: "Präziser Polarkoordinaten-Plotter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention180.jpg"
weight: "180"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48099
- /details5822.html
imported:
- "2019"
_4images_image_id: "48099"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "180"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48099 -->
