---
layout: "image"
title: "fischertip24.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9503
- /detailscad4.html
imported:
- "2019"
_4images_image_id: "9503"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9503 -->
