---
layout: "image"
title: "fischertip20.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9499
- /details73a3.html
imported:
- "2019"
_4images_image_id: "9499"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9499 -->
