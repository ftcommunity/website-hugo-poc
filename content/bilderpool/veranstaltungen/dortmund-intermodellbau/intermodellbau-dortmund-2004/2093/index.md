---
layout: "image"
title: "Robo Interface mit Model"
date: "2004-02-11T11:20:13"
picture: "109_0923.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Robointerface"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/2093
- /detailseb8c.html
imported:
- "2019"
_4images_image_id: "2093"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2093 -->
Hier ist das Robointerface mit einem einfachen Modell zu sehen. Zwei Taster links/rechts und ein Taster für Zählimpulse.