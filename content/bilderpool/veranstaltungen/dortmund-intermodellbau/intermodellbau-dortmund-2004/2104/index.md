---
layout: "image"
title: "Neuer mobiler Roboter mit Robo Interface"
date: "2004-02-11T11:20:29"
picture: "109_0952.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/2104
- /details7111.html
imported:
- "2019"
_4images_image_id: "2104"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2104 -->
Neuer mobiler Roboter mit Robo Interface. Das Modell hier wurde noch Programmmäßig am Stand verbessert. Im Grunde ist es ein Statikring der "frei" aufgehängt ist. Stößt nun der Roboter an kann er am den gedrückten Taster erkennen wie und wo er das Hinerniss berührt hat. Das Programm gibt dan an wie er reagieren soll - z.B. zurücksetzen und dann drehen ...
Was mich erstaunte war die Genauigkeit der Drehungen (dann parallel zu Kante) und die sehr guten geradeaus Fahreigenschaften.