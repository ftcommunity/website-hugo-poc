---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau28.jpg"
weight: "28"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43303
- /details6456.html
imported:
- "2019"
_4images_image_id: "43303"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43303 -->
Der berühmte 3-D Drucker. Hier einer von den zwei Prototypen.
