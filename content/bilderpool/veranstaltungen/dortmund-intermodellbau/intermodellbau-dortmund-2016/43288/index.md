---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau13.jpg"
weight: "13"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43288
- /details9fc4.html
imported:
- "2019"
_4images_image_id: "43288"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43288 -->
Rechenmaschinen.
