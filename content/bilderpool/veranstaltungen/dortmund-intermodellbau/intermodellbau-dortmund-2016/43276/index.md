---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:09"
picture: "intermodellbau01.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43276
- /details2d09.html
imported:
- "2019"
_4images_image_id: "43276"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43276 -->
