---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43296
- /detailsa47e-2.html
imported:
- "2019"
_4images_image_id: "43296"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43296 -->
Die Ecke von DirkW hinten und Friedrich vorne. Hier konnten die Kinder spielen mit Baukran, Flipper, Wall-E und Sattelauflieger mit Bagger.
