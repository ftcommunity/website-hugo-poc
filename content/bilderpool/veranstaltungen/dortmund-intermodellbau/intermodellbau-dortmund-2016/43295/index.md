---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43295
- /details6fd3.html
imported:
- "2019"
_4images_image_id: "43295"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43295 -->
Der Flipper war Dauermagnet für groß und klein.
