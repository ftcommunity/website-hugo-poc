---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau07.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43282
- /details8fe4.html
imported:
- "2019"
_4images_image_id: "43282"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43282 -->
Neu, jetzt mit Beleuchtung der Segmente.
