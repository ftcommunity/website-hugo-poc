---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau22.jpg"
weight: "22"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43297
- /details8273.html
imported:
- "2019"
_4images_image_id: "43297"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43297 -->
