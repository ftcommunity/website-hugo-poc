---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau29.jpg"
weight: "29"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43304
- /detailsbe46.html
imported:
- "2019"
_4images_image_id: "43304"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43304 -->
