---
layout: "image"
title: "fischertechnik-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau27.jpg"
weight: "27"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43302
- /details46a4-2.html
imported:
- "2019"
_4images_image_id: "43302"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43302 -->
Software vom 3-D Drucker. Diese macht meiner Meinung einen soliden Eindruck und ist intuitiv zu bedienen.
Rechts oben die Druckzeit.
