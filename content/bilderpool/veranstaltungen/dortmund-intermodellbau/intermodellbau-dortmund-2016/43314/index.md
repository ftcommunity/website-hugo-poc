---
layout: "image"
title: "Teilnehmer der Messe"
date: "2016-04-25T20:24:10"
picture: "intermodellbau39.jpg"
weight: "39"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43314
- /details8fb8.html
imported:
- "2019"
_4images_image_id: "43314"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43314 -->
Von links nach rechts.
rob-van-baal, geometer, Dirk K., Joana (fischertechnik),
Tobias Brezing (fischertechnik), Markus W., DirkW.
(Friedrich und TST sind nicht dabei)
