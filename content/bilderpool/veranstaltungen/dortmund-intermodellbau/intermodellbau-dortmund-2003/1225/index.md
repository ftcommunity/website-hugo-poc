---
layout: "image"
title: "Der Parcour"
date: "2003-07-08T17:12:53"
picture: "Der_Parcour_2.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1225
- /detailscb7c.html
imported:
- "2019"
_4images_image_id: "1225"
_4images_cat_id: "141"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1225 -->
