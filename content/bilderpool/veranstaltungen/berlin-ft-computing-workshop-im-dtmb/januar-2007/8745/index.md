---
layout: "image"
title: "berlin07.jpg"
date: "2007-01-29T18:43:39"
picture: "berlin07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/8745
- /details4bfc.html
imported:
- "2019"
_4images_image_id: "8745"
_4images_cat_id: "800"
_4images_user_id: "1"
_4images_image_date: "2007-01-29T18:43:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8745 -->
