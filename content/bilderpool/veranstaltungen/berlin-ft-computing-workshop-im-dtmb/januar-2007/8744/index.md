---
layout: "image"
title: "berlin06.jpg"
date: "2007-01-29T18:43:39"
picture: "berlin06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/8744
- /detailsa6d1.html
imported:
- "2019"
_4images_image_id: "8744"
_4images_cat_id: "800"
_4images_user_id: "1"
_4images_image_date: "2007-01-29T18:43:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8744 -->
