---
layout: "image"
title: "bild19.jpg"
date: "2006-12-10T21:26:21"
picture: "bild19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/7854
- /details0221.html
imported:
- "2019"
_4images_image_id: "7854"
_4images_cat_id: "737"
_4images_user_id: "1"
_4images_image_date: "2006-12-10T21:26:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7854 -->
