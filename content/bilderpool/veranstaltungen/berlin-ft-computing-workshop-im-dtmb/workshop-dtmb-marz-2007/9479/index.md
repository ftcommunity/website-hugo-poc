---
layout: "image"
title: "berlin63.jpg"
date: "2007-03-13T20:02:07"
picture: "berlin63.jpg"
weight: "60"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/9479
- /details4bbc.html
imported:
- "2019"
_4images_image_id: "9479"
_4images_cat_id: "870"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:02:07"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9479 -->
