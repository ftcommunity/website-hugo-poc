---
layout: "image"
title: "FT-Treffen Vlissingen-2008"
date: "2008-02-24T14:33:02"
picture: "Fischertechnik-Vlissingen-2008_001_2.jpg"
weight: "9"
konstrukteure: 
- "Fam. van Baal"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13741
- /details25bb-2.html
imported:
- "2019"
_4images_image_id: "13741"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T14:33:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13741 -->
