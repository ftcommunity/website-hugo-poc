---
layout: "image"
title: "FT-Treffen-Vlissingen-2008  FT-Luft"
date: "2008-02-24T14:33:01"
picture: "Fischertechnik-Vlissingen-2008_051.jpg"
weight: "5"
konstrukteure: 
- "NL-FT-Mitglied"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13737
- /details7b2e.html
imported:
- "2019"
_4images_image_id: "13737"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T14:33:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13737 -->
