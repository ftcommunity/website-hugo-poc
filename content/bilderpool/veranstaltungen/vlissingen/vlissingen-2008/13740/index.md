---
layout: "image"
title: "FT-Treffen Vlissingen-2008"
date: "2008-02-24T14:33:01"
picture: "Fischertechnik-Vlissingen-2008_001.jpg"
weight: "8"
konstrukteure: 
- "Fam. van Baal"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13740
- /detailsb032.html
imported:
- "2019"
_4images_image_id: "13740"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T14:33:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13740 -->
