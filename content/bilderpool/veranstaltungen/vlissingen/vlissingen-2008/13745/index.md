---
layout: "image"
title: "FT-Treffen Vlissingen-2008"
date: "2008-02-24T15:23:08"
picture: "Fischertechnik-Vlissingen-2008_005.jpg"
weight: "13"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13745
- /detailsa85c.html
imported:
- "2019"
_4images_image_id: "13745"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T15:23:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13745 -->
