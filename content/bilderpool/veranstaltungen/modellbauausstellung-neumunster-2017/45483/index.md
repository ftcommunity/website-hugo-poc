---
layout: "image"
title: "Flipper"
date: "2017-03-08T17:29:59"
picture: "neumuenster24.jpg"
weight: "24"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45483
- /details4eca.html
imported:
- "2019"
_4images_image_id: "45483"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45483 -->
