---
layout: "image"
title: "Modellbauaustellung Neumünster"
date: "2017-03-08T17:29:59"
picture: "neumuenster25.jpg"
weight: "25"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45484
- /details887d-3.html
imported:
- "2019"
_4images_image_id: "45484"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45484 -->
