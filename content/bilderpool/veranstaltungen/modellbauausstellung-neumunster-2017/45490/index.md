---
layout: "image"
title: "Strickmaschine"
date: "2017-03-08T17:30:06"
picture: "neumuenster31.jpg"
weight: "31"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45490
- /details86eb.html
imported:
- "2019"
_4images_image_id: "45490"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:30:06"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45490 -->
