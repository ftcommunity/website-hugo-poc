---
layout: "image"
title: "Modellbauaustellung Neumünster"
date: "2017-03-08T16:28:27"
picture: "neumuenster07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45466
- /detailsea66.html
imported:
- "2019"
_4images_image_id: "45466"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T16:28:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45466 -->
