---
layout: "image"
title: "Wall-E und CNC-Fräse"
date: "2017-03-08T17:29:59"
picture: "neumuenster22.jpg"
weight: "22"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45481
- /details5c66.html
imported:
- "2019"
_4images_image_id: "45481"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45481 -->
