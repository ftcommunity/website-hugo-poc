---
layout: "image"
title: "Modellbauaustellung Neumünster"
date: "2017-03-08T17:29:42"
picture: "neumuenster12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45471
- /detailsfed1.html
imported:
- "2019"
_4images_image_id: "45471"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:42"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45471 -->
