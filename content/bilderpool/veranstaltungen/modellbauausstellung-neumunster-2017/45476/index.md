---
layout: "image"
title: "Kleine Kugelbahn"
date: "2017-03-08T17:29:42"
picture: "neumuenster17.jpg"
weight: "17"
konstrukteure: 
- "Grau"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45476
- /details51f1.html
imported:
- "2019"
_4images_image_id: "45476"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45476 -->
