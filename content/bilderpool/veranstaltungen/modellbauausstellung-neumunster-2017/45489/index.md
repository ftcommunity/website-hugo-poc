---
layout: "image"
title: "Strickmaschine"
date: "2017-03-08T17:29:59"
picture: "neumuenster30.jpg"
weight: "30"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45489
- /detailsb132-2.html
imported:
- "2019"
_4images_image_id: "45489"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45489 -->
