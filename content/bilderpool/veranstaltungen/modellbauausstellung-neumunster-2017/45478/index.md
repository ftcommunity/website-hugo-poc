---
layout: "image"
title: "Bulldozer"
date: "2017-03-08T17:29:42"
picture: "neumuenster19.jpg"
weight: "19"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45478
- /details4075.html
imported:
- "2019"
_4images_image_id: "45478"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:42"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45478 -->
