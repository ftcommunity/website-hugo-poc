---
layout: "image"
title: "Schachspiel"
date: "2017-03-08T17:30:06"
picture: "neumuenster32.jpg"
weight: "32"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45491
- /details1ea4.html
imported:
- "2019"
_4images_image_id: "45491"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:30:06"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45491 -->
