---
layout: "image"
title: "Laufband Hugo und Herrmann"
date: "2017-03-08T17:29:59"
picture: "neumuenster28.jpg"
weight: "28"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45487
- /detailsb974.html
imported:
- "2019"
_4images_image_id: "45487"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45487 -->
