---
layout: "image"
title: "Harvester"
date: "2017-03-08T17:29:42"
picture: "neumuenster20.jpg"
weight: "20"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45479
- /details18b8.html
imported:
- "2019"
_4images_image_id: "45479"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:42"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45479 -->
