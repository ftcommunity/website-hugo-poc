---
layout: "image"
title: "Community Schools Class"
date: "2010-01-28T18:48:10"
picture: "sm_build_game.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Boise", "Idaho", "Community Schools"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26177
- /details7e96.html
imported:
- "2019"
_4images_image_id: "26177"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-01-28T18:48:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26177 -->
I am teaching a "Mechanical Engineering with fischertechnik" in a community schools program in Boise. Last night was our first night! Age range 9-20+!!! We had a great time! 

Google Translation:
Ich unterrichte eine "Maschinenbau mit fischertechnik" in einer Gemeinschaft Schulen Programm in Boise. Letzte Nacht war unsere erste Nacht! Altersgruppe 9-20 +! Wir hatten eine tolle Zeit!