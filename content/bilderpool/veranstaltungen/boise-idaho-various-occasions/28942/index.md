---
layout: "image"
title: "Teacher training"
date: "2010-10-08T14:21:05"
picture: "sm_ftTRAINB.jpg"
weight: "58"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["in-service", "teacher", "training"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28942
- /details985e.html
imported:
- "2019"
_4images_image_id: "28942"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-10-08T14:21:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28942 -->
Images from a recent ft teacher in-service session.