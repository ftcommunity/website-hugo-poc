---
layout: "image"
title: "Community Schools Class"
date: "2010-02-04T20:02:51"
picture: "ca_2-a.jpg"
weight: "22"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Community Schools"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26202
- /details3b4e.html
imported:
- "2019"
_4images_image_id: "26202"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-02-04T20:02:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26202 -->
Images of the Community Schools Class in Boise Idaho.