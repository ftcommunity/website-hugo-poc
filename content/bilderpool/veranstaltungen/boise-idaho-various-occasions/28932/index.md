---
layout: "image"
title: "BrickCON Robot"
date: "2010-10-06T07:29:22"
picture: "sm_ftrobotA.jpg"
weight: "51"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["BrickCON", "2010"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28932
- /detailsdf7c.html
imported:
- "2019"
_4images_image_id: "28932"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-10-06T07:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28932 -->
This was my entry at BrickCON 2010 in Seattle WA. The goal was to avoid obstacles and complete laps as quickly as possible. Mine was the only ft robot in the competition. I won the John Force Award!