---
layout: "image"
title: "Community Schools Class"
date: "2010-02-19T19:37:36"
picture: "sm_carousels.jpg"
weight: "41"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "Community Schools"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/26468
- /details800a-2.html
imported:
- "2019"
_4images_image_id: "26468"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-02-19T19:37:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26468 -->
These are images of a community school's class I taught in Boise Idaho.