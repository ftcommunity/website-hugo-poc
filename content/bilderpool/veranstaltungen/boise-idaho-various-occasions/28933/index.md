---
layout: "image"
title: "BrickCON 2010"
date: "2010-10-06T07:29:23"
picture: "sm_ftrobotB.jpg"
weight: "52"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["BrickCON", "2010", "Seattle"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28933
- /details1f3e.html
imported:
- "2019"
_4images_image_id: "28933"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-10-06T07:29:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28933 -->
This was my entry at BrickCON 2010 in Seattle WA. The goal was to avoid obstacles and complete laps as quickly as possible. (There were a total of three different courses). Mine was the only ft robot in the competition. I won the John Force Award!