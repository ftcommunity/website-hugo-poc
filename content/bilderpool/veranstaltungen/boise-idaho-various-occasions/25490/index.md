---
layout: "image"
title: "Seattle BrickCON 2009"
date: "2009-10-03T20:57:27"
picture: "brickcon4.jpg"
weight: "12"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["BrickCON", "2009"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/25490
- /details07e3.html
imported:
- "2019"
_4images_image_id: "25490"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-10-03T20:57:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25490 -->
I gave a presentation on constructing with fischertechnik at BrickCON 2009 in Seattle WA. It was well received...many had never seen fischertechnik before!