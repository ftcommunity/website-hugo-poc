---
layout: "image"
title: "BrickCON ft Presentation"
date: "2010-10-06T20:24:38"
picture: "ftbrickcon_c.jpg"
weight: "55"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["BrickCON", "2010"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28939
- /detailsebbe.html
imported:
- "2019"
_4images_image_id: "28939"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-10-06T20:24:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28939 -->
These are images from my ft building session at BrickCON 2010 in Seattle WA. We had an awesome time!