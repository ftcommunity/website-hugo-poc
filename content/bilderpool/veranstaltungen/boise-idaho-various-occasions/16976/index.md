---
layout: "image"
title: "Mechatronics at BSU 2009"
date: "2009-01-10T01:31:55"
picture: "DSC01250.jpg"
weight: "5"
konstrukteure: 
- "BSU students"
fotografen:
- "BSU"
schlagworte: ["BSU", "Mechatronics", "PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16976
- /detailsa838.html
imported:
- "2019"
_4images_image_id: "16976"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-01-10T01:31:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16976 -->
Photos from the Design and Analysis of Mechatronics Class (Robotics Module) from 
Boise State University. This project combined ft and the PCS Brain controller.  

The students designed, built and programmed a robot which will follow a track with crooked turns and return to the start position without going off the track. At the end we had the drag race to measure the weight ratio of the design.