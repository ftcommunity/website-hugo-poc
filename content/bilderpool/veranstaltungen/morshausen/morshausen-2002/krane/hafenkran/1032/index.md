---
layout: "image"
title: "Hafenkran"
date: "2003-05-03T10:41:17"
picture: "Bild094.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1032
- /details15f0-2.html
imported:
- "2019"
_4images_image_id: "1032"
_4images_cat_id: "99"
_4images_user_id: "1"
_4images_image_date: "2003-05-03T10:41:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1032 -->
Diese Hafenkran wurde mit der Software FreeSpeedSpeech von Markus Mack sprachgesteuert!