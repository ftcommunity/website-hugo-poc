---
layout: "image"
title: "Fertigungsinsel"
date: "2003-05-03T10:32:21"
picture: "Bild100.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "-?-"
schlagworte: ["Fanclubmodell"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1010
- /details7570.html
imported:
- "2019"
_4images_image_id: "1010"
_4images_cat_id: "95"
_4images_user_id: "1"
_4images_image_date: "2003-05-03T10:32:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1010 -->
