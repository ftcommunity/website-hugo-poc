---
layout: "image"
title: "Bild011"
date: "2003-05-03T10:46:25"
picture: "Bild011.jpg"
weight: "1"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1038
- /detailsf848.html
imported:
- "2019"
_4images_image_id: "1038"
_4images_cat_id: "102"
_4images_user_id: "1"
_4images_image_date: "2003-05-03T10:46:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1038 -->
