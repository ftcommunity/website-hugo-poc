---
layout: "comment"
hidden: true
title: "23610"
date: "2017-09-05T07:51:24"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Die taumelnde Drehscheibe könnte gar nicht schräg auf einer Welle klemmen die durch eines der exzentrischen Löcher geht. Und mittels Klemmnabe / Flachnabe geht das auch nicht in einem derartigen Winkel.

Meine Erklärung nach Analyse der beiden Bilder, insbesondere diesem hier:
Alle Drehscheiben sitzen axial zentriert aber nicht auf einer gemeinsamen Welle. Die linke Drehscheibe ist im Rahmen fest verankert, die Zylinder sind ortsfest. Die mittlere Drehscheibe (an der die Zylinderstangen befestigt sind) könnte auf einer gekröpften Welle sitzen. Die mittlere Drehscheibe muss auch nicht drehen sondern nur taumeln. Dadurch wird deren Welle in eine nutierte Bewegung gezwungen (siehe https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2011-1.pdf#page=25, Abbildung 18), dreht sich und nimmt das Schwungrad mit. Vielleicht ist da etwas Entsprechendes im Inneren des großen Schwungrades verborgen um die Metallachse für die taumelnde Drehscheibe entsprechend schräg zu stellen? Das würde funktionieren und ohne Schnitzereien auskommen.