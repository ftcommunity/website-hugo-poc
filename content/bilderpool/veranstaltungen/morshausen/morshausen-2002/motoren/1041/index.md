---
layout: "image"
title: "Bild096"
date: "2003-05-03T10:46:25"
picture: "Bild096.jpg"
weight: "4"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1041
- /details6578.html
imported:
- "2019"
_4images_image_id: "1041"
_4images_cat_id: "102"
_4images_user_id: "1"
_4images_image_date: "2003-05-03T10:46:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1041 -->
