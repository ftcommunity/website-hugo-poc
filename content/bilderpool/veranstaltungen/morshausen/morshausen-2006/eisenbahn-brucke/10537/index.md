---
layout: "image"
title: "Brücke zwischen Tischen"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke5.jpg"
weight: "11"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10537
- /details3641.html
imported:
- "2019"
_4images_image_id: "10537"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10537 -->
Komplette Brücke
