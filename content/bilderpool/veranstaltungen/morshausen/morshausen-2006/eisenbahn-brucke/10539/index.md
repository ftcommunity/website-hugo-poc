---
layout: "image"
title: "Personenzug 2"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke7.jpg"
weight: "13"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10539
- /details60a1-2.html
imported:
- "2019"
_4images_image_id: "10539"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10539 -->
Im Bahnhof
