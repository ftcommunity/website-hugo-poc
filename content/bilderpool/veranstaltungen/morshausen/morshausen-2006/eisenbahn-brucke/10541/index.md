---
layout: "image"
title: "Personenzug auf Brücke"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke9.jpg"
weight: "15"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10541
- /detailse030-3.html
imported:
- "2019"
_4images_image_id: "10541"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10541 -->
Personenzug auf Brücke
