---
layout: "image"
title: "convention1.jpg"
date: "2006-10-03T11:32:49"
picture: "convention1.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/7109
- /details9dd5.html
imported:
- "2019"
_4images_image_id: "7109"
_4images_cat_id: "665"
_4images_user_id: "1"
_4images_image_date: "2006-10-03T11:32:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7109 -->
