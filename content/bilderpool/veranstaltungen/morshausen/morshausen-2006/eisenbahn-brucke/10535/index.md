---
layout: "image"
title: "Personenzug"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke3.jpg"
weight: "9"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10535
- /details81bd.html
imported:
- "2019"
_4images_image_id: "10535"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10535 -->
Im Bahnhof Mörshausen
