---
layout: "image"
title: "Ente"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_004.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9957
- /details4f58.html
imported:
- "2019"
_4images_image_id: "9957"
_4images_cat_id: "664"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9957 -->
