---
layout: "image"
title: "Flugzeuge und andere Modellen"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_054.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6940
- /details5469-2.html
imported:
- "2019"
_4images_image_id: "6940"
_4images_cat_id: "664"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6940 -->
