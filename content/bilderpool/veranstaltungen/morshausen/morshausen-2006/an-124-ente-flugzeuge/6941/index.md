---
layout: "image"
title: "Flugzeuge und andere Modellen"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_056.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6941
- /details7302.html
imported:
- "2019"
_4images_image_id: "6941"
_4images_cat_id: "664"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6941 -->
