---
layout: "comment"
hidden: true
title: "1367"
date: "2006-09-24T19:20:00"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
In welche Richtung wurde hier die Grenze zwischen "Genie" und "Wahnsinn" überschritten ;-)
Ich glaube, man kann sich bei diesem Modell stundenlang aufhalten und findet dann immer noch ein Detail. Überall summt und brummt es und bewegen sich Zahnräder und Wellen... doch eher Wahnsinn (aber im positiven Sinn)... 
Wirklich beeindruckend.
Nächstes Jahr bringen wir aber ft zum Fliegen, oder?