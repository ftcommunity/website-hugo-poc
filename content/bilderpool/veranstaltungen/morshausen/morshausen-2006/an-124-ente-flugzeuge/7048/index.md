---
layout: "image"
title: "Die Antonov in Baugruppen"
date: "2006-10-01T14:11:26"
picture: "Mrshausen_029.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7048
- /details05b2.html
imported:
- "2019"
_4images_image_id: "7048"
_4images_cat_id: "664"
_4images_user_id: "130"
_4images_image_date: "2006-10-01T14:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7048 -->
