---
layout: "image"
title: "Flugzeug_3"
date: "2006-09-24T01:20:13"
picture: "jpeg03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6882
- /detailse075.html
imported:
- "2019"
_4images_image_id: "6882"
_4images_cat_id: "664"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:20:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6882 -->
