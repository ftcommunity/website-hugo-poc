---
layout: "image"
title: "Flugzeug_4"
date: "2006-09-24T01:20:13"
picture: "jpeg04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6883
- /details1b5b.html
imported:
- "2019"
_4images_image_id: "6883"
_4images_cat_id: "664"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:20:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6883 -->
