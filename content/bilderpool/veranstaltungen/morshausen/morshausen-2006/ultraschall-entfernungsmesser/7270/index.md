---
layout: "image"
title: "Ultraschall-Entfernungsmesser"
date: "2006-10-29T19:01:47"
picture: "rb2.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7270
- /detailsb885.html
imported:
- "2019"
_4images_image_id: "7270"
_4images_cat_id: "687"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7270 -->
