---
layout: "image"
title: "RM19"
date: "2006-10-05T20:05:51"
picture: "RoboMax_019.jpg"
weight: "19"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7153
- /detailsbd00.html
imported:
- "2019"
_4images_image_id: "7153"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7153 -->
