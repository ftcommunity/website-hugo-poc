---
layout: "image"
title: "RM6"
date: "2006-10-05T20:05:38"
picture: "RoboMax_006.jpg"
weight: "6"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7140
- /details89f6-2.html
imported:
- "2019"
_4images_image_id: "7140"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7140 -->
