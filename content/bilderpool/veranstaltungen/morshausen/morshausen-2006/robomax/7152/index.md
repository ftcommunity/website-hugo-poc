---
layout: "image"
title: "RM18"
date: "2006-10-05T20:05:51"
picture: "RoboMax_018.jpg"
weight: "18"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7152
- /detailseba4.html
imported:
- "2019"
_4images_image_id: "7152"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7152 -->
