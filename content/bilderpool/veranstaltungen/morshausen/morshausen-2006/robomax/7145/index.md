---
layout: "image"
title: "RM11"
date: "2006-10-05T20:05:50"
picture: "RoboMax_011.jpg"
weight: "11"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7145
- /detailsbb5d.html
imported:
- "2019"
_4images_image_id: "7145"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7145 -->
