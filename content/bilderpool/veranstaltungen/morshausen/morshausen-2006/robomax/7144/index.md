---
layout: "image"
title: "RM10"
date: "2006-10-05T20:05:38"
picture: "RoboMax_010.jpg"
weight: "10"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7144
- /detailse1df.html
imported:
- "2019"
_4images_image_id: "7144"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7144 -->
