---
layout: "image"
title: "Roboterarm"
date: "2006-10-29T19:01:17"
picture: "roboterarm1.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7254
- /details3a13.html
imported:
- "2019"
_4images_image_id: "7254"
_4images_cat_id: "663"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7254 -->
