---
layout: "image"
title: "Roboterarm"
date: "2007-05-31T09:44:39"
picture: "roboarm1.jpg"
weight: "2"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10614
- /details2c96.html
imported:
- "2019"
_4images_image_id: "10614"
_4images_cat_id: "663"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10614 -->
