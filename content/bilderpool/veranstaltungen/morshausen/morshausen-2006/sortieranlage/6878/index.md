---
layout: "image"
title: "Industriemodell_1"
date: "2006-09-24T00:59:47"
picture: "jpeg1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6878
- /details2821.html
imported:
- "2019"
_4images_image_id: "6878"
_4images_cat_id: "678"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T00:59:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6878 -->
