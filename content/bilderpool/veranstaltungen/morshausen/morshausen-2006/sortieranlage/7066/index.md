---
layout: "image"
title: "Sortieranlage04.JPG"
date: "2006-10-02T15:57:53"
picture: "Sortieranlage04.JPG"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7066
- /detailsb482.html
imported:
- "2019"
_4images_image_id: "7066"
_4images_cat_id: "678"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T15:57:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7066 -->
