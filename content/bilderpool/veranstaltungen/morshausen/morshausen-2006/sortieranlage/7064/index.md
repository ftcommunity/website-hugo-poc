---
layout: "image"
title: "Sortieranlage02.JPG"
date: "2006-10-02T15:54:36"
picture: "Sortieranlage02.JPG"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7064
- /details2949.html
imported:
- "2019"
_4images_image_id: "7064"
_4images_cat_id: "678"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T15:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7064 -->
Die Box mit dem reflektierenden Alu im Boden (rechts) wird zuerst aussortiert, die andere läuft durch bis zur zweiten Station.
