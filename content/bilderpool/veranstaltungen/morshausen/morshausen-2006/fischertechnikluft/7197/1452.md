---
layout: "comment"
hidden: true
title: "1452"
date: "2006-10-17T09:32:24"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Ja, wundert mich auch dass das Bild mit meiner kleinen digitalen so gut geworden ist. Gut, irgendjemand hatte 1 Euro eingeworfen, dann ist Licht im Brunnen und so ca. jede Minute wird etwas Wasser nach unten geschüttet, damit man die Tiefe des Brunnens auch 'hören' kann. Erst wird nmlich die Wasserfläche schwarz und ca. 1/2s später hört man das Platsch.
Über dem Brunnen ist übrigens ein sehr starkes Gitter.
Euer Ralf