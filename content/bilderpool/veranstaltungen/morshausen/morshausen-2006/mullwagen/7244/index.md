---
layout: "image"
title: "Die Mülleimer"
date: "2006-10-29T14:54:37"
picture: "cwl2.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7244
- /details7474.html
imported:
- "2019"
_4images_image_id: "7244"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7244 -->
