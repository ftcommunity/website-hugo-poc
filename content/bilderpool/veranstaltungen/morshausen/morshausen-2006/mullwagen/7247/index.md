---
layout: "image"
title: "Die Cabine kann nach vorne"
date: "2006-10-29T14:54:37"
picture: "cwl5.jpg"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7247
- /details9d32.html
imported:
- "2019"
_4images_image_id: "7247"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7247 -->
