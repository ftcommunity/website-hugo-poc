---
layout: "image"
title: "Müllwagen"
date: "2007-05-31T09:44:07"
picture: "muellwagen1.jpg"
weight: "12"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10602
- /details8534-2.html
imported:
- "2019"
_4images_image_id: "10602"
_4images_cat_id: "672"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10602 -->
mit diversen Funktionen
