---
layout: "image"
title: "Müllwagen"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_040.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6932
- /detailsb4be-2.html
imported:
- "2019"
_4images_image_id: "6932"
_4images_cat_id: "672"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6932 -->
