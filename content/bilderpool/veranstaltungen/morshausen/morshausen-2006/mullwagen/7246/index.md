---
layout: "image"
title: "Gesammtmodell"
date: "2006-10-29T14:54:37"
picture: "cwl4.jpg"
weight: "9"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7246
- /details49c5.html
imported:
- "2019"
_4images_image_id: "7246"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7246 -->
