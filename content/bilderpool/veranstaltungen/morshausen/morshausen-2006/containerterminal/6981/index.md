---
layout: "image"
title: "Containerterminal_2"
date: "2006-09-25T22:52:56"
picture: "warwel2.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6981
- /details967d.html
imported:
- "2019"
_4images_image_id: "6981"
_4images_cat_id: "673"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:52:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6981 -->
