---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:24"
picture: "containerterminal04.jpg"
weight: "13"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10579
- /details6156-3.html
imported:
- "2019"
_4images_image_id: "10579"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10579 -->
Steuerung ist Kinderleicht
