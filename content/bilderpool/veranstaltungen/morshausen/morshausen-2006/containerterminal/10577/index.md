---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:07"
picture: "containerterminal02.jpg"
weight: "11"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10577
- /details715d.html
imported:
- "2019"
_4images_image_id: "10577"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10577 -->
