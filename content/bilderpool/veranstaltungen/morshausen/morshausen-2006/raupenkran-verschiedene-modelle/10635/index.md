---
layout: "image"
title: "Schwebebahn"
date: "2007-05-31T09:45:20"
picture: "verschmodelle4.jpg"
weight: "31"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10635
- /details401d.html
imported:
- "2019"
_4images_image_id: "10635"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10635 -->
von Oben
