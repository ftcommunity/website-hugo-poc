---
layout: "image"
title: "Schwebebahn Holger Howey"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_066_2.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6944
- /details0538.html
imported:
- "2019"
_4images_image_id: "6944"
_4images_cat_id: "660"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6944 -->
