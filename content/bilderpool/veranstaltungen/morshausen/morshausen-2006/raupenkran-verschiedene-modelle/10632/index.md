---
layout: "image"
title: "Roboter mit Schrittmotoren"
date: "2007-05-31T09:45:20"
picture: "verschmodelle1.jpg"
weight: "28"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10632
- /details0d41.html
imported:
- "2019"
_4images_image_id: "10632"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10632 -->
von Obenlinks
