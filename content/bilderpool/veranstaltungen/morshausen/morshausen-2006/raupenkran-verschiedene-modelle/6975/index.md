---
layout: "image"
title: "Magnetschwebebahn_1"
date: "2006-09-25T22:45:19"
picture: "holger5.jpg"
weight: "15"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6975
- /detailsacec.html
imported:
- "2019"
_4images_image_id: "6975"
_4images_cat_id: "660"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:45:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6975 -->
