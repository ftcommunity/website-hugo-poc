---
layout: "comment"
hidden: true
title: "1395"
date: "2006-09-26T22:31:54"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Die Styroporschneidemaschine läuft mit dem Programm von der ftComunity (Bressenham). Man kann damit von Corel Draw erstellte Zeichnungen ausgeben.
Als Beispiele kann man z.B. einen kleinen Flieger für Kinder machen. Neben der Hand sieht man eine Zahl die auch mit dem Schneider augegeben wurde. Es ist eine 2 einer Windows Schrift.