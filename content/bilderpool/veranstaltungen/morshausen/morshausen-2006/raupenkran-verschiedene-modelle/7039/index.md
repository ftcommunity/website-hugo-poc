---
layout: "image"
title: "Details von Holgers grossem Kran"
date: "2006-10-01T14:11:26"
picture: "Mrshausen_011.jpg"
weight: "23"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7039
- /details040d-2.html
imported:
- "2019"
_4images_image_id: "7039"
_4images_cat_id: "660"
_4images_user_id: "130"
_4images_image_date: "2006-10-01T14:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7039 -->
