---
layout: "image"
title: "Roboter mit Schrittmotoren"
date: "2007-05-31T09:45:20"
picture: "verschmodelle3.jpg"
weight: "30"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10634
- /details4850.html
imported:
- "2019"
_4images_image_id: "10634"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10634 -->
Frontansicht
