---
layout: "image"
title: "Kran"
date: "2007-05-31T09:45:33"
picture: "verschmodelle7.jpg"
weight: "34"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10638
- /detailsc5af-2.html
imported:
- "2019"
_4images_image_id: "10638"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10638 -->
in voller Größe
