---
layout: "image"
title: "Holzfab_01.JPG"
date: "2006-09-26T18:40:52"
picture: "Holzfab_01.JPG"
weight: "5"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7010
- /details9f21.html
imported:
- "2019"
_4images_image_id: "7010"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:40:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7010 -->
Station 1: Anlieferung per Kran und Transport-LKW
