---
layout: "image"
title: "Sägewerk_2"
date: "2006-09-25T22:48:13"
picture: "jansen2.jpg"
weight: "4"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6979
- /detailsc3d6-2.html
imported:
- "2019"
_4images_image_id: "6979"
_4images_cat_id: "667"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:48:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6979 -->
