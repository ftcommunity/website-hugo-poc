---
layout: "image"
title: "Holzfab_08.JPG"
date: "2006-09-26T18:46:42"
picture: "Holzfab_08.JPG"
weight: "11"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7016
- /details0fae.html
imported:
- "2019"
_4images_image_id: "7016"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:46:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7016 -->
Die Sortierstation etwas genauer.
