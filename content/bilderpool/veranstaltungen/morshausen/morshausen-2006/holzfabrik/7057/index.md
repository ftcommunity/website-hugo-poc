---
layout: "image"
title: "Der Aufbau"
date: "2006-10-02T02:44:55"
picture: "Mrshausen_038.jpg"
weight: "13"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7057
- /details66d2.html
imported:
- "2019"
_4images_image_id: "7057"
_4images_cat_id: "667"
_4images_user_id: "130"
_4images_image_date: "2006-10-02T02:44:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7057 -->
Hier baut Fam. Jansen ihre Holzfabrik auf.
