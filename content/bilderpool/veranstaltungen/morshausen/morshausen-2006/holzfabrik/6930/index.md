---
layout: "image"
title: "Holzfabrik Fam. Janssen"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_044.jpg"
weight: "2"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Peter Damen ("
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/6930
- /detailsb15e.html
imported:
- "2019"
_4images_image_id: "6930"
_4images_cat_id: "667"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6930 -->
