---
layout: "image"
title: "Sortieranlage"
date: "2006-10-02T02:45:00"
picture: "Mrshausen_139.jpg"
weight: "18"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/7062
- /details5441-4.html
imported:
- "2019"
_4images_image_id: "7062"
_4images_cat_id: "667"
_4images_user_id: "130"
_4images_image_date: "2006-10-02T02:45:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7062 -->
Da werden die Klötzchen der Grösse nach aussortiert. Die kleinen dürfen passieren, die grossen nicht.
