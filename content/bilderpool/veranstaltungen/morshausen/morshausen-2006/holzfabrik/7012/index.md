---
layout: "image"
title: "Holzfab_03.JPG"
date: "2006-09-26T18:43:29"
picture: "Holzfab_03.JPG"
weight: "7"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7012
- /details08f4.html
imported:
- "2019"
_4images_image_id: "7012"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:43:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7012 -->
Station 3: Weitertransport per Förderband und Greifer
