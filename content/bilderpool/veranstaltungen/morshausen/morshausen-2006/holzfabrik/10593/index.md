---
layout: "image"
title: "Sägerei"
date: "2007-05-31T09:43:46"
picture: "holzfarbrik1.jpg"
weight: "21"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10593
- /details1c56.html
imported:
- "2019"
_4images_image_id: "10593"
_4images_cat_id: "667"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10593 -->
mit einer Kettensäge werden die Holzstämme zerkleinert
