---
layout: "image"
title: "Roboterarm"
date: "2006-10-29T15:28:42"
picture: "fl1.jpg"
weight: "1"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7252
- /details8c21.html
imported:
- "2019"
_4images_image_id: "7252"
_4images_cat_id: "674"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T15:28:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7252 -->
