---
layout: "image"
title: "Display im 9V Block Kasten"
date: "2006-09-24T22:49:47"
picture: "thkais7.jpg"
weight: "7"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6969
- /details2a16.html
imported:
- "2019"
_4images_image_id: "6969"
_4images_cat_id: "662"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T22:49:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6969 -->
