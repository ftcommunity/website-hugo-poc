---
layout: "image"
title: "Pneumatik Bearbeitungszentrum"
date: "2006-10-29T14:54:37"
picture: "ap2.jpg"
weight: "2"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7241
- /details3b59.html
imported:
- "2019"
_4images_image_id: "7241"
_4images_cat_id: "695"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7241 -->
