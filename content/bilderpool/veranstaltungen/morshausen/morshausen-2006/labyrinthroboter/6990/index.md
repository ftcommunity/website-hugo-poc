---
layout: "image"
title: "Roboter im Labyrint_2"
date: "2006-09-25T23:01:37"
picture: "who3.jpg"
weight: "3"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6990
- /detailsbbc2.html
imported:
- "2019"
_4images_image_id: "6990"
_4images_cat_id: "671"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:01:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6990 -->
