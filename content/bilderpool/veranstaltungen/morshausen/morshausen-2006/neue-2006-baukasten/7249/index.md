---
layout: "image"
title: "Neue Modelle 2006"
date: "2006-10-29T15:28:42"
picture: "ft1.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7249
- /details573f-2.html
imported:
- "2019"
_4images_image_id: "7249"
_4images_cat_id: "696"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T15:28:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7249 -->
