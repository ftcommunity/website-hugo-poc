---
layout: "image"
title: "Gelbe Baustein 30!!!"
date: "2006-10-29T15:28:42"
picture: "ft3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7251
- /detailsec67.html
imported:
- "2019"
_4images_image_id: "7251"
_4images_cat_id: "696"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T15:28:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7251 -->
