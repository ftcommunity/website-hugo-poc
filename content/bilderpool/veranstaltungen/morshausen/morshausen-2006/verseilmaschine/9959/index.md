---
layout: "image"
title: "Verseilmaschine"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_009.jpg"
weight: "7"
konstrukteure: 
- "Sven Engelke (sven)"
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9959
- /details9c36.html
imported:
- "2019"
_4images_image_id: "9959"
_4images_cat_id: "676"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9959 -->
