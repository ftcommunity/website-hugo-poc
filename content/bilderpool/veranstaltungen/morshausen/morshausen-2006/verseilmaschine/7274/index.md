---
layout: "image"
title: "Verseilmaschine"
date: "2006-10-29T19:02:12"
picture: "se2.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke (sven)"
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7274
- /detailsf662.html
imported:
- "2019"
_4images_image_id: "7274"
_4images_cat_id: "676"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:02:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7274 -->
