---
layout: "image"
title: "Labyrinthroboter_1"
date: "2006-09-25T23:17:17"
picture: "remadus1.jpg"
weight: "1"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6995
- /detailsbab6.html
imported:
- "2019"
_4images_image_id: "6995"
_4images_cat_id: "666"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:17:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6995 -->
