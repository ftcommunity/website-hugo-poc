---
layout: "image"
title: "Bagger"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_029.jpg"
weight: "3"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9960
- /detailsacd9-2.html
imported:
- "2019"
_4images_image_id: "9960"
_4images_cat_id: "675"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9960 -->
