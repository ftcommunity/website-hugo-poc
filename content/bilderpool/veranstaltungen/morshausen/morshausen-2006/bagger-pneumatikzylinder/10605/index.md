---
layout: "image"
title: "Pneumatikbagger"
date: "2007-05-31T09:44:07"
picture: "pneumatik2.jpg"
weight: "5"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10605
- /detailse373.html
imported:
- "2019"
_4images_image_id: "10605"
_4images_cat_id: "675"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10605 -->
