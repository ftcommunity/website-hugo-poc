---
layout: "image"
title: "Sigfried Kloster"
date: "2006-10-29T19:01:47"
picture: "sk1.jpg"
weight: "5"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Rob van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/7271
- /detailsa794-2.html
imported:
- "2019"
_4images_image_id: "7271"
_4images_cat_id: "669"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7271 -->
