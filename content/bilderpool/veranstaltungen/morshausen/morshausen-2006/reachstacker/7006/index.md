---
layout: "image"
title: "Stacker90.JPG"
date: "2006-09-26T18:18:52"
picture: "Staker90.JPG"
weight: "2"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7006
- /detailsb9fd.html
imported:
- "2019"
_4images_image_id: "7006"
_4images_cat_id: "669"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:18:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7006 -->
