---
layout: "image"
title: "Stacker92.JPG"
date: "2006-09-26T18:19:50"
picture: "Staker92.JPG"
weight: "4"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7008
- /details7538.html
imported:
- "2019"
_4images_image_id: "7008"
_4images_cat_id: "669"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:19:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7008 -->
