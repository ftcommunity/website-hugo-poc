---
layout: "image"
title: "Kran_29"
date: "2006-09-24T01:43:27"
picture: "kran29.jpg"
weight: "29"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6921
- /detailsfa38-2.html
imported:
- "2019"
_4images_image_id: "6921"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6921 -->
