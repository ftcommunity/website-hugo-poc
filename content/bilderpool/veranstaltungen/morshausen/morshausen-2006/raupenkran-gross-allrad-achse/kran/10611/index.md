---
layout: "image"
title: "Raupenkran groß"
date: "2007-05-31T09:44:39"
picture: "raupenkrangross4.jpg"
weight: "43"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10611
- /details6c1d.html
imported:
- "2019"
_4images_image_id: "10611"
_4images_cat_id: "679"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10611 -->
verdrehter Ausleger
