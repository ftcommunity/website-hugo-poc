---
layout: "image"
title: "Kran_34"
date: "2006-09-24T01:43:34"
picture: "kran34.jpg"
weight: "34"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6926
- /details077e.html
imported:
- "2019"
_4images_image_id: "6926"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:34"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6926 -->
