---
layout: "image"
title: "Kran_28"
date: "2006-09-24T01:43:27"
picture: "kran28.jpg"
weight: "28"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6920
- /details9226-2.html
imported:
- "2019"
_4images_image_id: "6920"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6920 -->
