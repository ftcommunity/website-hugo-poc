---
layout: "image"
title: "Kran_20"
date: "2006-09-24T01:43:20"
picture: "kran20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6912
- /details0fd6.html
imported:
- "2019"
_4images_image_id: "6912"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:20"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6912 -->
