---
layout: "image"
title: "Kran_25"
date: "2006-09-24T01:43:27"
picture: "kran25.jpg"
weight: "25"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6917
- /detailsb664-2.html
imported:
- "2019"
_4images_image_id: "6917"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6917 -->
