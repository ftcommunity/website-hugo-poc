---
layout: "image"
title: "Kran_7"
date: "2006-09-24T01:42:50"
picture: "kran07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6899
- /details6b3a.html
imported:
- "2019"
_4images_image_id: "6899"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6899 -->
