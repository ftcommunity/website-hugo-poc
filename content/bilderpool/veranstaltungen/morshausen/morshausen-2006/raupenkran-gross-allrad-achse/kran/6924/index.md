---
layout: "image"
title: "Kran_32"
date: "2006-09-24T01:43:34"
picture: "kran32.jpg"
weight: "32"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6924
- /details8f19.html
imported:
- "2019"
_4images_image_id: "6924"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:34"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6924 -->
