---
layout: "image"
title: "Kran"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_003.jpg"
weight: "37"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9972
- /details1a42.html
imported:
- "2019"
_4images_image_id: "9972"
_4images_cat_id: "679"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9972 -->
