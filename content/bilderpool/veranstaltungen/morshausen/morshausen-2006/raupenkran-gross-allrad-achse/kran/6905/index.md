---
layout: "image"
title: "Kran_13"
date: "2006-09-24T01:43:20"
picture: "kran13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6905
- /details9eea-2.html
imported:
- "2019"
_4images_image_id: "6905"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:20"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6905 -->
