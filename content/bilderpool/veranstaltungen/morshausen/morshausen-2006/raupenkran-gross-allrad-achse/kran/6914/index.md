---
layout: "image"
title: "Kran_22"
date: "2006-09-24T01:43:27"
picture: "kran22.jpg"
weight: "22"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/6914
- /details5a8f.html
imported:
- "2019"
_4images_image_id: "6914"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6914 -->
