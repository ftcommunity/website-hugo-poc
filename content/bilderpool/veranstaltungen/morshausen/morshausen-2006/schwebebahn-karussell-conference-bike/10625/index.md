---
layout: "image"
title: "Conference Bike"
date: "2007-05-31T09:45:03"
picture: "schwebebahn10.jpg"
weight: "33"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10625
- /details0c4f.html
imported:
- "2019"
_4images_image_id: "10625"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10625 -->
von Vorne
