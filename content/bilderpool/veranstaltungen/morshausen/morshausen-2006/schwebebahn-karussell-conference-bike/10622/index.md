---
layout: "image"
title: "Schwebebahn"
date: "2007-05-31T09:45:03"
picture: "schwebebahn07.jpg"
weight: "30"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10622
- /details8fe2.html
imported:
- "2019"
_4images_image_id: "10622"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10622 -->
Gesammtansicht
