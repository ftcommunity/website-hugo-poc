---
layout: "image"
title: "Das Conference Bike"
date: "2006-09-26T18:05:56"
picture: "P9230064.jpg"
weight: "10"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/7002
- /detailsef0e.html
imported:
- "2019"
_4images_image_id: "7002"
_4images_cat_id: "680"
_4images_user_id: "381"
_4images_image_date: "2006-09-26T18:05:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7002 -->
Da immer wieder sehr viele Leute fragen:
Das Fahrrad für 7 Personen gibt es in echt! Siehe www.conferencebike.de
