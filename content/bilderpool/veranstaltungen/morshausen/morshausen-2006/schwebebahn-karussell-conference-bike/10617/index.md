---
layout: "image"
title: "Propellerflugzeug"
date: "2007-05-31T09:44:40"
picture: "schwebebahn02.jpg"
weight: "25"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10617
- /detailscb0a.html
imported:
- "2019"
_4images_image_id: "10617"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10617 -->
mit 2 Motoren
