---
layout: "image"
title: "Kinderkarussell55.JPG"
date: "2006-10-02T16:13:58"
picture: "Kinderkarussell55.JPG"
weight: "12"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7078
- /detailse297.html
imported:
- "2019"
_4images_image_id: "7078"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:13:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7078 -->
