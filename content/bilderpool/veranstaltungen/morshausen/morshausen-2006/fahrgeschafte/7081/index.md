---
layout: "image"
title: "Riesenrad (klein)53.JPG"
date: "2006-10-02T16:15:05"
picture: "Riesenrad_klein53.JPG"
weight: "15"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7081
- /details0f27-2.html
imported:
- "2019"
_4images_image_id: "7081"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7081 -->
