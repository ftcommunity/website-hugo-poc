---
layout: "image"
title: "Fahrgeschäfte"
date: "2007-04-04T20:14:38"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_011.jpg"
weight: "21"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9991
- /details36aa.html
imported:
- "2019"
_4images_image_id: "9991"
_4images_cat_id: "668"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T20:14:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9991 -->
