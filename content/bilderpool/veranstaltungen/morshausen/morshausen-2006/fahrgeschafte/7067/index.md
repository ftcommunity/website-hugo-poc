---
layout: "image"
title: "Fuhrpark_01.JPG"
date: "2006-10-02T16:06:00"
picture: "Fuhrpark_01.JPG"
weight: "1"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7067
- /details5e32.html
imported:
- "2019"
_4images_image_id: "7067"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7067 -->
Der Fuhrpark für den Transport der Kirmesmodelle ist immer wieder faszinierend. Einfach liebevoll gemacht.
