---
layout: "image"
title: "Kinderkarussell14.JPG"
date: "2006-10-02T16:13:42"
picture: "Kinderkarussell14.JPG"
weight: "11"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7077
- /details0234-2.html
imported:
- "2019"
_4images_image_id: "7077"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7077 -->
