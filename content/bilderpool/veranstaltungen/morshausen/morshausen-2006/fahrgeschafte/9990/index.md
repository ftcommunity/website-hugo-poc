---
layout: "image"
title: "Fahrgeschäfte"
date: "2007-04-04T20:14:37"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_012.jpg"
weight: "20"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9990
- /details969b-3.html
imported:
- "2019"
_4images_image_id: "9990"
_4images_cat_id: "668"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T20:14:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9990 -->
