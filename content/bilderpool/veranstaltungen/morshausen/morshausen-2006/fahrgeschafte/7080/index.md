---
layout: "image"
title: "Riesenrad (klein)05.JPG"
date: "2006-10-02T16:14:44"
picture: "Riesenrad_klein05.JPG"
weight: "14"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7080
- /detailsdb76-3.html
imported:
- "2019"
_4images_image_id: "7080"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:14:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7080 -->
