---
layout: "image"
title: "Fuhrpark_02.JPG"
date: "2006-10-02T16:07:11"
picture: "Fuhrpark_02.JPG"
weight: "2"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7068
- /details1cd6-2.html
imported:
- "2019"
_4images_image_id: "7068"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:07:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7068 -->
Man beachte das hochmoderne Cockpit.
