---
layout: "image"
title: "Fuhrpark_07.JPG"
date: "2006-10-02T16:10:26"
picture: "Fuhrpark_07.JPG"
weight: "6"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/7072
- /detailse121.html
imported:
- "2019"
_4images_image_id: "7072"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:10:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7072 -->
Hier sieht man, warum die Kirmesanlagen "FAHRgeschäfte" heißen - weil alle Teile geFAHREN werden (oder etwa nicht? ;-) )
