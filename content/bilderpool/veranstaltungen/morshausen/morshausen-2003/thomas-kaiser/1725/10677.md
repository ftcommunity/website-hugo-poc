---
layout: "comment"
hidden: true
title: "10677"
date: "2010-01-30T19:34:50"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
Zauberei...

Auf der Platine befindet sich ein Reed-Kontakt, der durch einen feststehenden Magneten ausgelöst wird. So bekommt man 1x pro Umdrehung einen Impuls. Der Rest ist Timing.
Ginge auch mit einem Fototransistor und einer Lampe oder vielen anderen Dingen...