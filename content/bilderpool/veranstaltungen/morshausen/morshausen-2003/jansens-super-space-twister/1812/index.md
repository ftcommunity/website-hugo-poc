---
layout: "image"
title: "Jansen und Bürgermeister Wagner"
date: "2003-10-08T17:53:40"
picture: "kirmes_jansen_wagner.jpg"
weight: "14"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "-?-"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/1812
- /detailsf07e-2.html
imported:
- "2019"
_4images_image_id: "1812"
_4images_cat_id: "168"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1812 -->
