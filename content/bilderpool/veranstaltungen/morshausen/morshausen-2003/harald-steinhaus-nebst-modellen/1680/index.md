---
layout: "image"
title: "DSCF0070"
date: "2003-09-28T09:56:47"
picture: "DSCF0070.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1680
- /details7e55.html
imported:
- "2019"
_4images_image_id: "1680"
_4images_cat_id: "166"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1680 -->
