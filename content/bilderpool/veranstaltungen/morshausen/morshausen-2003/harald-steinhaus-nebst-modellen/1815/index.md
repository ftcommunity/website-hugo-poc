---
layout: "image"
title: "So viel Funktion auf einmal ..."
date: "2003-10-08T17:55:04"
picture: "mhdrescher_harald_rckseite.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Hartmut Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/1815
- /details1c03-3.html
imported:
- "2019"
_4images_image_id: "1815"
_4images_cat_id: "166"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1815 -->
