---
layout: "image"
title: "Siggis Robbi"
date: "2003-10-09T11:40:57"
picture: "Siggis_Robbi_2.jpg"
weight: "10"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1820
- /detailsc684.html
imported:
- "2019"
_4images_image_id: "1820"
_4images_cat_id: "151"
_4images_user_id: "130"
_4images_image_date: "2003-10-09T11:40:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1820 -->
