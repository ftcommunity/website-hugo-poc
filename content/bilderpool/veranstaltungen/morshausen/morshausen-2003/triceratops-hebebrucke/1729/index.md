---
layout: "image"
title: "IMGP3870"
date: "2003-09-28T10:07:21"
picture: "IMGP3870.JPG"
weight: "2"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1729
- /details0e62.html
imported:
- "2019"
_4images_image_id: "1729"
_4images_cat_id: "176"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T10:07:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1729 -->
