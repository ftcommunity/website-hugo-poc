---
layout: "overview"
title: "MarMacs FluSi 2"
date: 2020-02-22T08:53:46+01:00
legacy_id:
- /php/categories/170
- /categoriesb0f8.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=170 --> 
Pädagogisch wertvolles Kinderspielzeug, das viel bespielt, aber leider nur wenig fotografiert wurde.