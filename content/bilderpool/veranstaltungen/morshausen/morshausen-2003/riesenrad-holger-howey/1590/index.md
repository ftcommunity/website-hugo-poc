---
layout: "image"
title: "DCP 0698"
date: "2003-09-28T09:48:23"
picture: "DCP_0698.JPG"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1590
- /detailsbffc.html
imported:
- "2019"
_4images_image_id: "1590"
_4images_cat_id: "153"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1590 -->
