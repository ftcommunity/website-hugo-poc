---
layout: "image"
title: "Bergauf"
date: "2003-10-03T15:01:45"
picture: "P9200019.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1774
- /detailse718-2.html
imported:
- "2019"
_4images_image_id: "1774"
_4images_cat_id: "173"
_4images_user_id: "4"
_4images_image_date: "2003-10-03T15:01:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1774 -->
