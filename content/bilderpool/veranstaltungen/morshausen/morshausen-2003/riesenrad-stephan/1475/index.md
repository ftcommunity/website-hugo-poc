---
layout: "image"
title: "n2-0047-a"
date: "2003-09-21T12:06:20"
picture: "n2-0047-a.jpg"
weight: "1"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "nn"
schlagworte: ["Riesenrad", "Fahrgeschäft"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/1475
- /detailsf431.html
imported:
- "2019"
_4images_image_id: "1475"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-21T12:06:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1475 -->
