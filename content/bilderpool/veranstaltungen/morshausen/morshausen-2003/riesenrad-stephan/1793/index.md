---
layout: "image"
title: "Der Aufgang zu meinem Riesenrad"
date: "2003-10-08T15:42:08"
picture: "Aufgang_vom_RR.jpg"
weight: "11"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1793
- /detailsecf9-2.html
imported:
- "2019"
_4images_image_id: "1793"
_4images_cat_id: "154"
_4images_user_id: "130"
_4images_image_date: "2003-10-08T15:42:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1793 -->
Der Aufgang bestand aus einer Großbauplatte und 18 Grundbauplatten 35129. 12 für die Seitlichen Aufgänge und 6 für die vorderen und hinteren Treppen.