---
layout: "image"
title: "Fanta"
date: "2008-09-22T15:37:55"
picture: "moershausen11.jpg"
weight: "2"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15435
- /details35fe.html
imported:
- "2019"
_4images_image_id: "15435"
_4images_cat_id: "1419"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15435 -->
Brillant und billig, vielleicht ein sponsor ?