---
layout: "image"
title: "Luftbild-1"
date: "2008-09-21T20:41:55"
picture: "conv2008-01.jpg"
weight: "1"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/15363
- /details96f8.html
imported:
- "2019"
_4images_image_id: "15363"
_4images_cat_id: "1399"
_4images_user_id: "41"
_4images_image_date: "2008-09-21T20:41:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15363 -->
Das DGH aus einer ganz anderen Perspektive...
