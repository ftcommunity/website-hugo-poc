---
layout: "image"
title: "Luftbild-2"
date: "2008-09-21T20:41:56"
picture: "conv2008-02.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/15364
- /details891b.html
imported:
- "2019"
_4images_image_id: "15364"
_4images_cat_id: "1399"
_4images_user_id: "41"
_4images_image_date: "2008-09-21T20:41:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15364 -->
Man kann auch Bilder von sich aus der Vogelperspektive machen...
