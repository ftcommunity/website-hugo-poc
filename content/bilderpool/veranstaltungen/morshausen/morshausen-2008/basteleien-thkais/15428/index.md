---
layout: "image"
title: "Quadrokopter 3"
date: "2008-09-22T15:37:55"
picture: "moershausen04.jpg"
weight: "8"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15428
- /detailsfec1.html
imported:
- "2019"
_4images_image_id: "15428"
_4images_cat_id: "1399"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15428 -->
