---
layout: "image"
title: "moershausen01.jpg"
date: "2008-09-22T15:37:55"
picture: "moershausen01.jpg"
weight: "5"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15425
- /details20a7.html
imported:
- "2019"
_4images_image_id: "15425"
_4images_cat_id: "1399"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15425 -->
