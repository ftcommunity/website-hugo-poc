---
layout: "image"
title: "Robot Magazine"
date: "2009-03-16T22:34:58"
picture: "robot_mag_article.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Robot", "Magazine", "Mörshausen", "2008"]
schlagworte: ["Robot", "Magazine", "Mörshausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23469
- /details7049.html
imported:
- "2019"
_4images_image_id: "23469"
_4images_cat_id: "1398"
_4images_user_id: "585"
_4images_image_date: "2009-03-16T22:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23469 -->
This a piece Laura and I authored about Morshausen. Robot Magazine ran an edited version in their May/June issue. Thought to share.