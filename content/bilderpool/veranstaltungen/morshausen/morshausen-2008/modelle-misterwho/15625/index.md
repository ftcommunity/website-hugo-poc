---
layout: "image"
title: "Planetograph"
date: "2008-09-25T17:47:42"
picture: "conv20.jpg"
weight: "4"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15625
- /details54a1-2.html
imported:
- "2019"
_4images_image_id: "15625"
_4images_cat_id: "1424"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:42"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15625 -->
