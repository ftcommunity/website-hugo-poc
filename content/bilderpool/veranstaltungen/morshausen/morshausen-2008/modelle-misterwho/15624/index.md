---
layout: "image"
title: "Labyrinth Roboter"
date: "2008-09-25T17:47:42"
picture: "conv19.jpg"
weight: "3"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15624
- /details2e59.html
imported:
- "2019"
_4images_image_id: "15624"
_4images_cat_id: "1424"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:42"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15624 -->
