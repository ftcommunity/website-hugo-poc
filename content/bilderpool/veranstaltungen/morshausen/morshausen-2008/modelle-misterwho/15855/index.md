---
layout: "image"
title: "Moershausen Models"
date: "2008-10-10T08:58:34"
picture: "m_robots.jpg"
weight: "5"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15855
- /detailsd96d.html
imported:
- "2019"
_4images_image_id: "15855"
_4images_cat_id: "1424"
_4images_user_id: "585"
_4images_image_date: "2008-10-10T08:58:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15855 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.
