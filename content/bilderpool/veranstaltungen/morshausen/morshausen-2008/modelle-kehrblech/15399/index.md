---
layout: "image"
title: "Steuerrad"
date: "2008-09-22T07:43:46"
picture: "Steuerrad_4_Gewinnt_Roboter_-_Links_Rechts_und_Drcken.jpg"
weight: "3"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15399
- /detailsd985.html
imported:
- "2019"
_4images_image_id: "15399"
_4images_cat_id: "1410"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15399 -->
3 Positionen:

1. Nach links drehen
2. Nach rechts drehen
3. drücken
