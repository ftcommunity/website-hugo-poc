---
layout: "image"
title: "Moershausen Models"
date: "2008-10-03T18:59:06"
picture: "m_brick_sorter.jpg"
weight: "6"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15812
- /details1728.html
imported:
- "2019"
_4images_image_id: "15812"
_4images_cat_id: "1410"
_4images_user_id: "585"
_4images_image_date: "2008-10-03T18:59:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15812 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.
