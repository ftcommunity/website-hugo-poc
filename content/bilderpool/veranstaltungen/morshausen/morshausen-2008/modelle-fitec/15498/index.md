---
layout: "image"
title: "Kräne"
date: "2008-09-23T07:43:24"
picture: "convention30.jpg"
weight: "4"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
- "fitec"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15498
- /details8e41.html
imported:
- "2019"
_4images_image_id: "15498"
_4images_cat_id: "1407"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15498 -->
