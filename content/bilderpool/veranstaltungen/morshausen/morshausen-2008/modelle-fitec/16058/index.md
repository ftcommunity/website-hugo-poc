---
layout: "image"
title: "Industriestraße"
date: "2008-10-25T14:26:26"
picture: "fitec01.jpg"
weight: "7"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16058
- /details9e03.html
imported:
- "2019"
_4images_image_id: "16058"
_4images_cat_id: "1407"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16058 -->
