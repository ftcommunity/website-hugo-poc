---
layout: "image"
title: "Hochregallager"
date: "2008-10-25T14:26:41"
picture: "fitec04.jpg"
weight: "10"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16061
- /details38a3-2.html
imported:
- "2019"
_4images_image_id: "16061"
_4images_cat_id: "1407"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16061 -->
