---
layout: "image"
title: "Achterbahn mit Aufzug"
date: "2008-09-22T07:43:46"
picture: "Achterbahn_mit_Aufzug_von_Limit.jpg"
weight: "1"
konstrukteure: 
- "Marius Seider (Limit)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15400
- /details89b2.html
imported:
- "2019"
_4images_image_id: "15400"
_4images_cat_id: "1411"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15400 -->
