---
layout: "image"
title: "Truck mit Wahsager und Speedy68"
date: "2008-09-22T07:43:47"
picture: "Truck_mit_Wahsager_und_Speedy68.jpg"
weight: "1"
konstrukteure: 
- "Wahsager"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15416
- /details923e.html
imported:
- "2019"
_4images_image_id: "15416"
_4images_cat_id: "1418"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15416 -->
