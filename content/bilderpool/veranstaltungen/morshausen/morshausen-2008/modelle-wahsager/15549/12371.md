---
layout: "comment"
hidden: true
title: "12371"
date: "2010-09-29T15:44:36"
uploadBy:
- "sparifankerl"
license: "unknown"
imported:
- "2019"
---
Da würden ja noch 2 60er Zylinder zum Heben der Kabine reinpassen.
Wie viel beträgt denn der Abstand zwischen den bei gelenkten Vorderachsen?
Ich bin z. Zt. auch am Bauen eines 8 x 4. Aber ich habe das Problem, dass
die 2te Lenkachse nicht gerade lenkt. Habe die beiden Anlenkhebel mit 2 übereinandergesteckten I-75 Statikstreben verbunden.  Die 1. Achse wird mit dem Servo gesteuert.  Suche dringend eine Lösung. Bitte um Antwort! Danke.