---
layout: "image"
title: "zwei Motoren auf ein Differenzial"
date: "2008-09-23T09:49:14"
picture: "Heiko_Truck_8Rad59.JPG"
weight: "7"
konstrukteure: 
- "Wahsager"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/15549
- /details8b9b.html
imported:
- "2019"
_4images_image_id: "15549"
_4images_cat_id: "1418"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T09:49:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15549 -->
