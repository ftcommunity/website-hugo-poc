---
layout: "image"
title: "Kugelbahn"
date: "2008-09-22T15:37:55"
picture: "moershausen19.jpg"
weight: "13"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15443
- /detailsd4d2.html
imported:
- "2019"
_4images_image_id: "15443"
_4images_cat_id: "1405"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15443 -->
Der kugelbahn ist leider nicht in bild