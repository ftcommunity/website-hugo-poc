---
layout: "image"
title: "übersicht"
date: "2008-09-22T15:37:55"
picture: "moershausen07.jpg"
weight: "7"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15431
- /detailsb21f.html
imported:
- "2019"
_4images_image_id: "15431"
_4images_cat_id: "1405"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15431 -->
