---
layout: "image"
title: "Modell 3 Dingus"
date: "2008-09-23T07:43:23"
picture: "convention07.jpg"
weight: "16"
konstrukteure: 
- "Tovenaar"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15475
- /details2f44.html
imported:
- "2019"
_4images_image_id: "15475"
_4images_cat_id: "1405"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15475 -->
