---
layout: "image"
title: "Sortieranlage"
date: "2008-09-22T15:37:55"
picture: "moershausen22.jpg"
weight: "1"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15446
- /details1203.html
imported:
- "2019"
_4images_image_id: "15446"
_4images_cat_id: "1398"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15446 -->
