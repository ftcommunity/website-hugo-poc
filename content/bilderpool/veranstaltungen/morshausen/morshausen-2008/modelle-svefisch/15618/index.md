---
layout: "image"
title: "Industriemaschine"
date: "2008-09-25T17:47:42"
picture: "conv13.jpg"
weight: "2"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15618
- /detailsb224.html
imported:
- "2019"
_4images_image_id: "15618"
_4images_cat_id: "1437"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:42"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15618 -->
