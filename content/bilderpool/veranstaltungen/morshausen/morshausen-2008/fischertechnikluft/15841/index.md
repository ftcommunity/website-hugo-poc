---
layout: "image"
title: "Moershausen Model"
date: "2008-10-08T10:48:38"
picture: "m_maze_2.jpg"
weight: "69"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15841
- /details1b52-3.html
imported:
- "2019"
_4images_image_id: "15841"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-08T10:48:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15841 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.