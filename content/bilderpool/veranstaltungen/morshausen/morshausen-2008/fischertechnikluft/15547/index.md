---
layout: "image"
title: "Convention 2008"
date: "2008-09-23T07:43:24"
picture: "convention79.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15547
- /details2ed0.html
imported:
- "2019"
_4images_image_id: "15547"
_4images_cat_id: "1403"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15547 -->
Herr Knobloch (mit Zettel in der Hand) beantwortet geduldig die vielen Fragen. Sein Sohn rechts neben Ihm wird vermutlich das Wort Teilemangel nicht kennen.
