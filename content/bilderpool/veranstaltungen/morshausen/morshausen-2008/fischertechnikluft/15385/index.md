---
layout: "image"
title: "Figur"
date: "2008-09-21T21:34:08"
picture: "conv18.jpg"
weight: "17"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15385
- /details13e9.html
imported:
- "2019"
_4images_image_id: "15385"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15385 -->
Sieht zwar nicht so aus, aber Ralf hat die Dinger wirklich jongliert....Ich glaube da ist mir mein Kettengebundenes Spielzeug deutlich lieber ;-)
