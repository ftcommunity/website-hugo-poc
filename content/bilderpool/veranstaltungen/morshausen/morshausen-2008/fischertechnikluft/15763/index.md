---
layout: "image"
title: "Homberg Dinner"
date: "2008-10-02T16:37:22"
picture: "sm_hom_1.jpg"
weight: "31"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Moershausen", "2008", "Homberg"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15763
- /details9217.html
imported:
- "2019"
_4images_image_id: "15763"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-02T16:37:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15763 -->
On the night of the 22nd, a group of ft enthusiasts had dinner in Homberg. We were honored to attend.