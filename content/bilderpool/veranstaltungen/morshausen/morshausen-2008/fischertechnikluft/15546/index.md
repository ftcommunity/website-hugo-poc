---
layout: "image"
title: "Convention 2008"
date: "2008-09-23T07:43:24"
picture: "convention78.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15546
- /details2d42.html
imported:
- "2019"
_4images_image_id: "15546"
_4images_cat_id: "1403"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15546 -->
Herr Knobloch beantwortet geduldig die vielen Fragen
