---
layout: "image"
title: "Haus Hildegard"
date: "2008-10-02T22:34:19"
picture: "haus_h_group_c.jpg"
weight: "42"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Mörshausen", "2008", "Haus Hildegard"]
schlagworte: ["Mörshausen", "2008", "Haus Hildegard"]
schlagworte: ["Mörshausen", "2008", "Haus Hildegard"]
schlagworte: ["Mörshausen", "2008", "Haus Hildegard"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15775
- /detailsc4b3.html
imported:
- "2019"
_4images_image_id: "15775"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-02T22:34:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15775 -->
On the Sept 21, a group of ft enthusiasts met at the Haus Hildegard. We looked at the new firetruck kits, Frank Linde's new interface, drove the tracked vehicle, and built from the PCS nomenclature baggie. A good time was had by all!


Auf der 21. September, eine Gruppe von Enthusiasten ft erfüllt im Haus Hildegard. Wir haben uns auf die neue firetruck Kits, Frank Linde die neue Schnittstelle, fuhr der Kettenfahrzeug, und gebaut von der PCS Nomenklatur baggie. Eine gute Zeit hatte wurde von allen!