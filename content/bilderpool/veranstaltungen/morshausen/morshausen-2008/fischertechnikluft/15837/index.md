---
layout: "image"
title: "Lothar"
date: "2008-10-07T22:42:03"
picture: "m_lothar.jpg"
weight: "68"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Lothar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15837
- /details82e0.html
imported:
- "2019"
_4images_image_id: "15837"
_4images_cat_id: "1403"
_4images_user_id: "585"
_4images_image_date: "2008-10-07T22:42:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15837 -->
These are some pics I took at Moershausen 2008. Thought to share.