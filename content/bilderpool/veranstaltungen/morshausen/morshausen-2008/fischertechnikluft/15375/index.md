---
layout: "image"
title: "Zuschauer 1"
date: "2008-09-21T21:34:08"
picture: "conv08.jpg"
weight: "7"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/15375
- /details4aaa-2.html
imported:
- "2019"
_4images_image_id: "15375"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15375 -->
Staunendes Publikum
