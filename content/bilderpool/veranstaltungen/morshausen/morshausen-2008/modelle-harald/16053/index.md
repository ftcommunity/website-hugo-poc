---
layout: "image"
title: "Flieger"
date: "2008-10-25T14:26:26"
picture: "harald2.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16053
- /detailsf695.html
imported:
- "2019"
_4images_image_id: "16053"
_4images_cat_id: "1409"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16053 -->
