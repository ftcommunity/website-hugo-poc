---
layout: "image"
title: "Flugzeug"
date: "2008-09-23T07:43:24"
picture: "convention18.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15486
- /details478c-2.html
imported:
- "2019"
_4images_image_id: "15486"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15486 -->
