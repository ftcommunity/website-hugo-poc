---
layout: "image"
title: "Laderaum"
date: "2008-09-23T09:58:30"
picture: "HS-Flieger84.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/15551
- /details4eeb-2.html
imported:
- "2019"
_4images_image_id: "15551"
_4images_cat_id: "1409"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T09:58:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15551 -->
Der Laderaum ist belegt mit 8 Platten 180x90. Die S-Riegel an den Seiten haben 4mm-Löcher in den Griffen, damit man die Fracht verzurren kann.
