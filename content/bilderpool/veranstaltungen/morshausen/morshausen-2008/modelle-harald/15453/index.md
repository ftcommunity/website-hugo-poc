---
layout: "image"
title: "Transporter"
date: "2008-09-22T20:42:14"
picture: "convention1.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/15453
- /details5953-2.html
imported:
- "2019"
_4images_image_id: "15453"
_4images_cat_id: "1409"
_4images_user_id: "592"
_4images_image_date: "2008-09-22T20:42:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15453 -->
