---
layout: "image"
title: "Drehbank"
date: "2008-09-23T07:43:23"
picture: "convention10.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15478
- /detailsed9d.html
imported:
- "2019"
_4images_image_id: "15478"
_4images_cat_id: "1402"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15478 -->
