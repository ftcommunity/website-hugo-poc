---
layout: "image"
title: "Drehbank"
date: "2008-09-21T22:19:54"
picture: "cl2.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15390
- /details51fa-2.html
imported:
- "2019"
_4images_image_id: "15390"
_4images_cat_id: "1402"
_4images_user_id: "130"
_4images_image_date: "2008-09-21T22:19:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15390 -->
Auch ein schönes Modell.
