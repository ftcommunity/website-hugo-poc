---
layout: "image"
title: "Truck"
date: "2008-09-21T22:19:54"
picture: "cl1.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/15389
- /details79f8.html
imported:
- "2019"
_4images_image_id: "15389"
_4images_cat_id: "1402"
_4images_user_id: "130"
_4images_image_date: "2008-09-21T22:19:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15389 -->
Schöner Truck. Klasse gebaut, gefällt mir sehr.
