---
layout: "image"
title: "moershausen24.jpg"
date: "2008-09-22T15:37:55"
picture: "moershausen24.jpg"
weight: "5"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15448
- /detailsb2c5.html
imported:
- "2019"
_4images_image_id: "15448"
_4images_cat_id: "1402"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15448 -->
