---
layout: "image"
title: "Drehmaschine"
date: "2008-09-21T21:34:07"
picture: "con2.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/15367
- /details495e.html
imported:
- "2019"
_4images_image_id: "15367"
_4images_cat_id: "1402"
_4images_user_id: "430"
_4images_image_date: "2008-09-21T21:34:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15367 -->
