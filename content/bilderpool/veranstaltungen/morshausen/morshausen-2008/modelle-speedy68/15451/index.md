---
layout: "image"
title: "Kirmesmodell"
date: "2008-09-22T19:31:19"
picture: "convention1.jpg"
weight: "6"
konstrukteure: 
- "Speedy68"
fotografen:
- "nula"
uploadBy: "nula"
license: "unknown"
legacy_id:
- /php/details/15451
- /details61da.html
imported:
- "2019"
_4images_image_id: "15451"
_4images_cat_id: "1413"
_4images_user_id: "592"
_4images_image_date: "2008-09-22T19:31:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15451 -->
