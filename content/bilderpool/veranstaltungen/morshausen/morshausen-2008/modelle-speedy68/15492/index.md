---
layout: "image"
title: "Top Spin"
date: "2008-09-23T07:43:24"
picture: "convention24.jpg"
weight: "13"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15492
- /detailsa97c.html
imported:
- "2019"
_4images_image_id: "15492"
_4images_cat_id: "1413"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15492 -->
