---
layout: "image"
title: "5-Achs-Roboter"
date: "2008-09-25T17:47:41"
picture: "conv01.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15606
- /detailse143.html
imported:
- "2019"
_4images_image_id: "15606"
_4images_cat_id: "1436"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15606 -->
