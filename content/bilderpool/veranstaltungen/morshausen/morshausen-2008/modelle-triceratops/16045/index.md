---
layout: "image"
title: "Uhr"
date: "2008-10-25T14:26:12"
picture: "triceratops3.jpg"
weight: "13"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16045
- /detailscd12.html
imported:
- "2019"
_4images_image_id: "16045"
_4images_cat_id: "1417"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16045 -->
