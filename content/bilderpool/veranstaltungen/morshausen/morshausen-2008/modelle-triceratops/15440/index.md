---
layout: "image"
title: "Uhr"
date: "2008-09-22T15:37:55"
picture: "moershausen16.jpg"
weight: "3"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15440
- /detailsdb42.html
imported:
- "2019"
_4images_image_id: "15440"
_4images_cat_id: "1417"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15440 -->
