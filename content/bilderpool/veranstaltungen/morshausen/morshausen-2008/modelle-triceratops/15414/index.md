---
layout: "image"
title: "Seilbahn mit Triceratops"
date: "2008-09-22T07:43:47"
picture: "Seilbahn_mit_Triceratops.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15414
- /details2b45-2.html
imported:
- "2019"
_4images_image_id: "15414"
_4images_cat_id: "1417"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15414 -->
