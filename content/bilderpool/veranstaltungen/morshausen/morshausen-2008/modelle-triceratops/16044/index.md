---
layout: "image"
title: "Station"
date: "2008-10-25T14:26:12"
picture: "triceratops2.jpg"
weight: "12"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16044
- /details1e58.html
imported:
- "2019"
_4images_image_id: "16044"
_4images_cat_id: "1417"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16044 -->
