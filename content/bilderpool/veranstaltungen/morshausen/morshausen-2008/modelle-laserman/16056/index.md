---
layout: "image"
title: "Interface"
date: "2008-10-25T14:26:26"
picture: "laserman2.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16056
- /detailsc05e.html
imported:
- "2019"
_4images_image_id: "16056"
_4images_cat_id: "1423"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16056 -->
