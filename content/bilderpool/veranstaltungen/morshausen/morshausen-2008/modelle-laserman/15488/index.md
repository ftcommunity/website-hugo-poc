---
layout: "image"
title: "Bootsschleuse"
date: "2008-09-23T07:43:24"
picture: "convention20.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15488
- /details0270.html
imported:
- "2019"
_4images_image_id: "15488"
_4images_cat_id: "1423"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15488 -->
