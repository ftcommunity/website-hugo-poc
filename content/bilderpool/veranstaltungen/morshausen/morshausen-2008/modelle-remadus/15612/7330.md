---
layout: "comment"
hidden: true
title: "7330"
date: "2008-09-27T16:36:11"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Nachrag2:

Hallo Martin,

mit der Vorstellung deiner Modellkreation Axiallager zur Convention 2008 hast du uns möglicherweise mit einer neuen Modellrubrik beschenkt. Dank der beiden Fotos von Heiko haben alle davon eine uns bereichernde Kenntnis erhalten.
Ich persönlich von der Lösung sehr angesprochen habe sie "etwas" modifiziert.
Der äussere Umkreis der Radialrollen ist jetzt auf den Innendurchmesser der beiden Laufringe einstellbar.
Bilder von dieser Modellvorlage kommen am Montag.
Zwei weitere Ideen gehen mir dazu noch im Kopf herum, deren Umsetzbarkeit ich aber konstruktiv erst mal prüfen muss.

Grüsse dich, Udo2