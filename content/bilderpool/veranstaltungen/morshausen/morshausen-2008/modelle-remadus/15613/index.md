---
layout: "image"
title: "Wälzlager"
date: "2008-09-25T17:47:42"
picture: "conv08.jpg"
weight: "3"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15613
- /detailsfd89-2.html
imported:
- "2019"
_4images_image_id: "15613"
_4images_cat_id: "1435"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15613 -->
