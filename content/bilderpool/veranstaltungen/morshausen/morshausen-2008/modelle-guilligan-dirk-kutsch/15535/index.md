---
layout: "image"
title: "Krangreifarm"
date: "2008-09-23T07:43:24"
picture: "convention67.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15535
- /details8091.html
imported:
- "2019"
_4images_image_id: "15535"
_4images_cat_id: "1408"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15535 -->
miteinzigartigem "Verriegelungssystem"
