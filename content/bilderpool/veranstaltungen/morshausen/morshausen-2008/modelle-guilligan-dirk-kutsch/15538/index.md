---
layout: "image"
title: "Tieflader"
date: "2008-09-23T07:43:24"
picture: "convention70.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15538
- /details2a22.html
imported:
- "2019"
_4images_image_id: "15538"
_4images_cat_id: "1408"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15538 -->
die hinteren Räder lenken auch mit
