---
layout: "image"
title: "Der Roboter muss mal kurz an die seite für das foto"
date: "2008-09-22T15:37:55"
picture: "moershausen14.jpg"
weight: "4"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15438
- /detailsdbdb-2.html
imported:
- "2019"
_4images_image_id: "15438"
_4images_cat_id: "1406"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15438 -->
