---
layout: "image"
title: "Steuerung"
date: "2008-10-25T14:26:56"
picture: "dmdbt2.jpg"
weight: "10"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16070
- /detailsb0a9.html
imported:
- "2019"
_4images_image_id: "16070"
_4images_cat_id: "1406"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16070 -->
