---
layout: "image"
title: "Convention 2008"
date: "2008-09-23T07:43:24"
picture: "convention51.jpg"
weight: "5"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15519
- /details6d1b.html
imported:
- "2019"
_4images_image_id: "15519"
_4images_cat_id: "1406"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15519 -->
rechts vorne: severin - rechts hinten: steffalk - links: fabse
