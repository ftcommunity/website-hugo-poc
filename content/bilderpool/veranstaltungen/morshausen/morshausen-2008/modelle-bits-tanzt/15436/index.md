---
layout: "image"
title: "Roboter 1"
date: "2008-09-22T15:37:55"
picture: "moershausen12.jpg"
weight: "2"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15436
- /details3e93.html
imported:
- "2019"
_4images_image_id: "15436"
_4images_cat_id: "1406"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15436 -->
