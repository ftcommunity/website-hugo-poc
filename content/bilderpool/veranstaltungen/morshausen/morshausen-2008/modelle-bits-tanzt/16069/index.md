---
layout: "image"
title: "Sensor"
date: "2008-10-25T14:26:41"
picture: "dmdbt1.jpg"
weight: "9"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/16069
- /detailsee3a.html
imported:
- "2019"
_4images_image_id: "16069"
_4images_cat_id: "1406"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16069 -->
