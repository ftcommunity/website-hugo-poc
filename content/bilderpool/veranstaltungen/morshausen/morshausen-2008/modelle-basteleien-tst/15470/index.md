---
layout: "image"
title: "LKW"
date: "2008-09-23T07:43:23"
picture: "convention02.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15470
- /details6693.html
imported:
- "2019"
_4images_image_id: "15470"
_4images_cat_id: "1400"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15470 -->
