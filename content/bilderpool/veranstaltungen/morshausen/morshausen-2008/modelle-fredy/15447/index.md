---
layout: "image"
title: "was ist da drin ?"
date: "2008-09-22T15:37:55"
picture: "moershausen23.jpg"
weight: "1"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- /php/details/15447
- /detailse58e.html
imported:
- "2019"
_4images_image_id: "15447"
_4images_cat_id: "1422"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15447 -->
