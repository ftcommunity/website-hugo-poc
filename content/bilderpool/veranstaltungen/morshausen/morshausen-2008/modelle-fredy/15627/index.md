---
layout: "image"
title: "Industriemodell"
date: "2008-09-23T07:43:24"
picture: "convention39.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15627
- /details1697.html
imported:
- "2019"
_4images_image_id: "15627"
_4images_cat_id: "1422"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15627 -->
