---
layout: "image"
title: "Kabelaufwickler"
date: "2008-09-22T07:43:46"
picture: "Kabelaufwickler_von_Thanks_for_the_fish.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15411
- /details475c.html
imported:
- "2019"
_4images_image_id: "15411"
_4images_cat_id: "1416"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15411 -->
