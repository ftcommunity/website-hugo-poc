---
layout: "image"
title: "Transportvorrichtung für FT-Kassetten"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen30.jpg"
weight: "30"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25480
- /detailsd28d.html
imported:
- "2019"
_4images_image_id: "25480"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25480 -->
