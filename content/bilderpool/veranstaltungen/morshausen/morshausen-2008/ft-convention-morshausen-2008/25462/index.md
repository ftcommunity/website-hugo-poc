---
layout: "image"
title: "Kaiser-Wilhelm-Brücke"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen12.jpg"
weight: "12"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25462
- /details966c.html
imported:
- "2019"
_4images_image_id: "25462"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25462 -->
LKW auf der Fahrbahn
