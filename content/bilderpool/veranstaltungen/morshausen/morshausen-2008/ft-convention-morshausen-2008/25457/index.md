---
layout: "image"
title: "Verschiedene Kräne"
date: "2009-10-01T19:18:52"
picture: "ftconventioninmoehrhausen07.jpg"
weight: "7"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25457
- /details8511.html
imported:
- "2019"
_4images_image_id: "25457"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25457 -->
