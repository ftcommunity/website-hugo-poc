---
layout: "image"
title: "Voith-Schneider-Propeller von Harald"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen26.jpg"
weight: "26"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/25476
- /detailsd95d-2.html
imported:
- "2019"
_4images_image_id: "25476"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25476 -->
