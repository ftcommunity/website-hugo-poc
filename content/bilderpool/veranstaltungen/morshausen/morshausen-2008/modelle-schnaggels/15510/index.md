---
layout: "image"
title: "RoboMax"
date: "2008-09-23T07:43:24"
picture: "convention42.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15510
- /detailsf37b-3.html
imported:
- "2019"
_4images_image_id: "15510"
_4images_cat_id: "1421"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15510 -->
Roboter, wo Treppen steigen kann
