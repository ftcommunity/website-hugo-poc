---
layout: "image"
title: "lenkbarer Antrieb"
date: "2008-09-23T07:43:24"
picture: "convention61.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15529
- /details7eca.html
imported:
- "2019"
_4images_image_id: "15529"
_4images_cat_id: "1414"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15529 -->
