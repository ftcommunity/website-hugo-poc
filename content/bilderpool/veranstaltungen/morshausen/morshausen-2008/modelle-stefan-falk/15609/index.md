---
layout: "image"
title: "Klopmeier-Getriebe"
date: "2008-09-25T17:47:41"
picture: "conv04.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/15609
- /details1ff2.html
imported:
- "2019"
_4images_image_id: "15609"
_4images_cat_id: "1414"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15609 -->
