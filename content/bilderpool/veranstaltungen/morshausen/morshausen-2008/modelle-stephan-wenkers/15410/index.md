---
layout: "image"
title: "Aufhängung"
date: "2008-09-22T07:43:46"
picture: "Aufhngung.jpg"
weight: "2"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/15410
- /detailsec60.html
imported:
- "2019"
_4images_image_id: "15410"
_4images_cat_id: "1415"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15410 -->
