---
layout: "image"
title: "Ballweiterreicher"
date: "2008-09-23T07:43:24"
picture: "convention19.jpg"
weight: "3"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15487
- /details6e35-2.html
imported:
- "2019"
_4images_image_id: "15487"
_4images_cat_id: "1412"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15487 -->
