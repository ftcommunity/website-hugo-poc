---
layout: "image"
title: "Mopeds09.JPG"
date: "2008-09-23T10:03:02"
picture: "Mopeds09.jpg"
weight: "3"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/15553
- /detailsaf18.html
imported:
- "2019"
_4images_image_id: "15553"
_4images_cat_id: "1432"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:03:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15553 -->
