---
layout: "image"
title: "Großmodell"
date: "2005-09-26T23:48:51"
picture: "Grossmodell_2.jpg"
weight: "3"
konstrukteure: 
- "M. Pütter"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5025
- /details3e45.html
imported:
- "2019"
_4images_image_id: "5025"
_4images_cat_id: "396"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5025 -->
