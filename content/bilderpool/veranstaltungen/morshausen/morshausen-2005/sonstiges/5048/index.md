---
layout: "image"
title: "LKW mit den Alufelgen"
date: "2005-09-30T21:20:27"
picture: "LKW_Modell_mit_Alufelgen.jpg"
weight: "16"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5048
- /detailsb507.html
imported:
- "2019"
_4images_image_id: "5048"
_4images_cat_id: "402"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5048 -->
Hier ist ein LKW Modell mit den schönen Alufelgen zu sehen. Die müsste es serienmäßig von ft geben.
