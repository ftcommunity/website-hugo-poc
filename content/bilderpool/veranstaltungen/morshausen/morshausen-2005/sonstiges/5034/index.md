---
layout: "image"
title: "Ballweitergabe"
date: "2005-09-27T16:45:41"
picture: "P8252516.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5034
- /details6491.html
imported:
- "2019"
_4images_image_id: "5034"
_4images_cat_id: "402"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5034 -->
