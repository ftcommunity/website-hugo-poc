---
layout: "image"
title: "Schöne Alufelgen"
date: "2005-09-30T21:20:27"
picture: "Alufelgen.jpg"
weight: "17"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5049
- /detailsa74e.html
imported:
- "2019"
_4images_image_id: "5049"
_4images_cat_id: "402"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5049 -->
Fischertechnikfelgen aus Alu
