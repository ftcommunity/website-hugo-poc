---
layout: "image"
title: "Kermis-NN66.JPG"
date: "2005-11-03T14:47:12"
picture: "Kermis-NN66.JPG"
weight: "13"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5179
- /details1244-2.html
imported:
- "2019"
_4images_image_id: "5179"
_4images_cat_id: "433"
_4images_user_id: "4"
_4images_image_date: "2005-11-03T14:47:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5179 -->
Die "Kermis" im Überblick.
