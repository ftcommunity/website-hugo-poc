---
layout: "image"
title: "Ballwurfmaschine"
date: "2005-09-30T21:20:27"
picture: "Ballwurfmaschine.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5041
- /details8f0a.html
imported:
- "2019"
_4images_image_id: "5041"
_4images_cat_id: "387"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5041 -->
