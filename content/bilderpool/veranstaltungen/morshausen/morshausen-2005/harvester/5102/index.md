---
layout: "image"
title: "Harvester57.JPG"
date: "2005-10-19T22:06:57"
picture: "Harvester57.jpg"
weight: "21"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5102
- /detailsf121.html
imported:
- "2019"
_4images_image_id: "5102"
_4images_cat_id: "389"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5102 -->
