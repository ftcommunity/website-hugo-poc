---
layout: "image"
title: "Harvester3"
date: "2005-09-26T22:19:55"
picture: "Harvester_3.jpg"
weight: "7"
konstrukteure: 
- "Herr Kohl"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5009
- /details1705.html
imported:
- "2019"
_4images_image_id: "5009"
_4images_cat_id: "389"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T22:19:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5009 -->
