---
layout: "image"
title: "Der Harvester"
date: "2005-09-26T22:19:55"
picture: "Harvester2.jpg"
weight: "6"
konstrukteure: 
- "Herr Kohl"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5008
- /detailsd3f9.html
imported:
- "2019"
_4images_image_id: "5008"
_4images_cat_id: "389"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T22:19:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5008 -->
