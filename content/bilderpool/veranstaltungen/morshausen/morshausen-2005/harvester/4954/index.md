---
layout: "image"
title: "conv2005 sven017"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven017.jpg"
weight: "5"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4954
- /details0036.html
imported:
- "2019"
_4images_image_id: "4954"
_4images_cat_id: "389"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4954 -->
