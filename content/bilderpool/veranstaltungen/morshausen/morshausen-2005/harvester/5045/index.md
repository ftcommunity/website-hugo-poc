---
layout: "image"
title: "Harvester"
date: "2005-09-30T21:20:27"
picture: "P8252529.jpg"
weight: "11"
konstrukteure: 
- "Albert Kohl (?)"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5045
- /details645d-2.html
imported:
- "2019"
_4images_image_id: "5045"
_4images_cat_id: "389"
_4images_user_id: "8"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5045 -->
