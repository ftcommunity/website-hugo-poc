---
layout: "image"
title: "Harvester13.JPG"
date: "2005-10-19T22:06:57"
picture: "Harvester13.jpg"
weight: "16"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5097
- /details1024.html
imported:
- "2019"
_4images_image_id: "5097"
_4images_cat_id: "389"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5097 -->
