---
layout: "image"
title: "conv2005 sven042"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven042.jpg"
weight: "8"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4977
- /details6c94-3.html
imported:
- "2019"
_4images_image_id: "4977"
_4images_cat_id: "398"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4977 -->
