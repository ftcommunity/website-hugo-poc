---
layout: "image"
title: "conv2005 heiko041"
date: "2005-09-25T14:53:35"
picture: "conv2005_heiko041.jpg"
weight: "2"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Heiko Engelke"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4922
- /details25bb-3.html
imported:
- "2019"
_4images_image_id: "4922"
_4images_cat_id: "398"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T14:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4922 -->
