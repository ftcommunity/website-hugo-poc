---
layout: "image"
title: "Martins Webcam Teleskop"
date: "2005-09-27T16:45:41"
picture: "P8252512.jpg"
weight: "7"
konstrukteure: 
- "remadus"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5031
- /detailsf052.html
imported:
- "2019"
_4images_image_id: "5031"
_4images_cat_id: "503"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5031 -->
Der Strommast in der Ferne.
