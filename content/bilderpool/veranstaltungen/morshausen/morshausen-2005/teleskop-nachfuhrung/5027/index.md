---
layout: "image"
title: "Martins Webcam Teleskop"
date: "2005-09-26T23:48:51"
picture: "P8252508.jpg"
weight: "3"
konstrukteure: 
- "remadus"
fotografen:
- "Joachim Jacobi (MisterWho)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/5027
- /detailsa730-2.html
imported:
- "2019"
_4images_image_id: "5027"
_4images_cat_id: "503"
_4images_user_id: "8"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5027 -->
An dem Teleskop ist eine umgebaut Webcam montiert. Über Schrittmotoren und entsprechender Software kann es dem Mond automatisch nachfahren.
