---
layout: "image"
title: "Lektüre über die Schlüter Trecker"
date: "2005-09-30T21:20:27"
picture: "Buch_ber_Schlter_Trecker.jpg"
weight: "13"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5047
- /details0976.html
imported:
- "2019"
_4images_image_id: "5047"
_4images_cat_id: "388"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5047 -->
