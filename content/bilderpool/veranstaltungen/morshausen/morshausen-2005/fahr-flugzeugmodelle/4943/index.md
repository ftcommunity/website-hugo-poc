---
layout: "image"
title: "conv2005 sven006"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven006.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4943
- /detailsf39c.html
imported:
- "2019"
_4images_image_id: "4943"
_4images_cat_id: "397"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4943 -->
