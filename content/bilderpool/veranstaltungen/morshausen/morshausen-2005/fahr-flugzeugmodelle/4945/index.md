---
layout: "image"
title: "conv2005 sven008"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven008.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4945
- /detailsa506-2.html
imported:
- "2019"
_4images_image_id: "4945"
_4images_cat_id: "397"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4945 -->
