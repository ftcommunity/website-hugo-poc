---
layout: "image"
title: "conv2005 heiko050"
date: "2005-09-25T14:53:35"
picture: "conv2005_heiko050.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Heiko Engelke"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4931
- /details7da7.html
imported:
- "2019"
_4images_image_id: "4931"
_4images_cat_id: "397"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T14:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4931 -->
