---
layout: "image"
title: "conv2005 heiko042"
date: "2005-09-25T14:53:35"
picture: "conv2005_heiko042.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Heiko Engelke"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4923
- /detailsae6c.html
imported:
- "2019"
_4images_image_id: "4923"
_4images_cat_id: "397"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T14:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4923 -->
