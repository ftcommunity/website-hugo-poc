---
layout: "image"
title: "Panzer93.JPG"
date: "2005-10-19T22:07:14"
picture: "Panzer93.jpg"
weight: "8"
konstrukteure: 
- "Markus Mack (MarMac)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5113
- /details3d56.html
imported:
- "2019"
_4images_image_id: "5113"
_4images_cat_id: "404"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:07:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5113 -->
