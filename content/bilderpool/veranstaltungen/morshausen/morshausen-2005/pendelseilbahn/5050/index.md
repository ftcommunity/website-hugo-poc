---
layout: "image"
title: "Seilbahn"
date: "2005-09-30T21:20:27"
picture: "Seilbahn_3.jpg"
weight: "12"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/5050
- /details055f.html
imported:
- "2019"
_4images_image_id: "5050"
_4images_cat_id: "393"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5050 -->
