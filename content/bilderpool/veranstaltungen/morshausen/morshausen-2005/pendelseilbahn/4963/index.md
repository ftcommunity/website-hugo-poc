---
layout: "image"
title: "conv2005 sven028"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven028.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4963
- /detailse209.html
imported:
- "2019"
_4images_image_id: "4963"
_4images_cat_id: "393"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4963 -->
