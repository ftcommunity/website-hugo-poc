---
layout: "image"
title: "conv2005 sven032"
date: "2005-09-25T19:31:08"
picture: "conv2005_sven032.jpg"
weight: "5"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/4967
- /detailsa5bd.html
imported:
- "2019"
_4images_image_id: "4967"
_4images_cat_id: "393"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4967 -->
