---
layout: "image"
title: "ccb069.JPG"
date: "2007-09-24T20:44:20"
picture: "ccb069.JPG"
weight: "18"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11949
- /details1446-2.html
imported:
- "2019"
_4images_image_id: "11949"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T20:44:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11949 -->
Der LKW basiert auf dem Müllabfuhrlaster hier: http://www.ftcommunity.de/categories.php?cat_id=742
hat aber eine Achse mehr bekommen, und die beiden Antriebsmotor treiben jetzt jeder eine davon an. Die hinterste Achse ist nicht angetrieben, wird aber gelenkt.
