---
layout: "image"
title: "Zugmaschine"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk018.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11599
- /detailsad94.html
imported:
- "2019"
_4images_image_id: "11599"
_4images_cat_id: "1056"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11599 -->
Sieht mir schwer nach Faun aus.
