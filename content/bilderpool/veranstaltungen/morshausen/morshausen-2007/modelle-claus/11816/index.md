---
layout: "image"
title: "Traktortransport 1"
date: "2007-09-18T11:14:38"
picture: "PICT5574.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11816
- /details4c28-2.html
imported:
- "2019"
_4images_image_id: "11816"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:14:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11816 -->
Zugmaschine: Kaelble
