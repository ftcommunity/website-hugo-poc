---
layout: "image"
title: "Fahrzeugtür mit Türgriff"
date: "2007-09-18T11:17:02"
picture: "PICT5717.jpg"
weight: "10"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11819
- /detailsff44.html
imported:
- "2019"
_4images_image_id: "11819"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:17:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11819 -->
