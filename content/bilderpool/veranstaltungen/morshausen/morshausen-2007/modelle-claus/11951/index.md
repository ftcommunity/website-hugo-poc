---
layout: "image"
title: "ccb068.JPG"
date: "2007-09-24T20:54:01"
picture: "ccb068.JPG"
weight: "20"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11951
- /details3e29-3.html
imported:
- "2019"
_4images_image_id: "11951"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T20:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11951 -->
Der Schlepper hat zwei Akkublöcke hinten links und rechts. Der Powermot zum Antrieb der Schraube liegt mittschiffs, auf der rechten Seite (Steuerbord) und hier nicht zu sehen liegt daneben ein Minimot mit Hubgetriebe, der das Ruder betätigt. an Backbord gibt es einen zweiten Minimot für das Schleppseil. Die seitliche Abdeckung ist gerade hochgeklappt und steht deshalb als schwarz-rotes Etwas um den rein-roten Kapitäns-Fahrstand herum.
