---
layout: "image"
title: "Seitenschweller"
date: "2007-09-18T11:21:07"
picture: "PICT5719.jpg"
weight: "12"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11821
- /details4728.html
imported:
- "2019"
_4images_image_id: "11821"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:21:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11821 -->
Ich finde den Winkel elegant.
