---
layout: "image"
title: "Traktor"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk017.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11598
- /detailsae28.html
imported:
- "2019"
_4images_image_id: "11598"
_4images_cat_id: "1056"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11598 -->
Im Hintergrund sieht man Thkais' Funkfernsteuerung und (weiter rechts oben) die Baggerschaufel für den Riesenkran.
