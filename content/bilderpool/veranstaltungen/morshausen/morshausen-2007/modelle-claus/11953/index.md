---
layout: "image"
title: "cz143.JPG"
date: "2007-09-24T21:07:14"
picture: "cz143.JPG"
weight: "22"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11953
- /details38d1.html
imported:
- "2019"
_4images_image_id: "11953"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T21:07:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11953 -->
Die Vorderachse der Zugmaschine.
