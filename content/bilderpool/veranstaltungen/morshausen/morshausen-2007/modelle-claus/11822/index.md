---
layout: "image"
title: "Unterseite"
date: "2007-09-18T11:21:42"
picture: "PICT5721.jpg"
weight: "13"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Heiko Engelke"
schlagworte: ["Einzelradaufhängung", "federung", "volksfestbier"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11822
- /details6b36-3.html
imported:
- "2019"
_4images_image_id: "11822"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:21:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11822 -->
