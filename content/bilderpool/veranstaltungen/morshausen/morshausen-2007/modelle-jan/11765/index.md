---
layout: "image"
title: "rrb75.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb75.jpg"
weight: "7"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11765
- /details1634.html
imported:
- "2019"
_4images_image_id: "11765"
_4images_cat_id: "1050"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11765 -->
