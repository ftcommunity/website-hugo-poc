---
layout: "image"
title: "Tic-Tac-Toe-Roboter"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk031.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11612
- /detailsf196.html
imported:
- "2019"
_4images_image_id: "11612"
_4images_cat_id: "1050"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11612 -->
Die Spielsteine sind magnetisch und werden von einem Elektromagneten, der unter dem Spielfeld verfahren wird, automatisch an die richtige Stelle bewegt. Man spielt gegen den Computer durch Eintippen der gewünschten Zielposition seines Steins auf der Zehnertastatur. Alles von Kehrblech gebaut (ich hoffe, ich habe nichts verwechselt). Jedenfalls ist er kein 35jähriger Dipl.-Ing. sondern ein Teen! Hut ab!
