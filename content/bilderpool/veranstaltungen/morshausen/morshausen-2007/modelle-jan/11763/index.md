---
layout: "image"
title: "rrb73.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb73.jpg"
weight: "5"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11763
- /detailse6e6.html
imported:
- "2019"
_4images_image_id: "11763"
_4images_cat_id: "1050"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11763 -->
