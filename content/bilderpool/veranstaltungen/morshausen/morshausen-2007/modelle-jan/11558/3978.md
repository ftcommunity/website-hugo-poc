---
layout: "comment"
hidden: true
title: "3978"
date: "2007-09-16T19:54:56"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
Ein faszinierendes Modell - wie von Geisterhand bewegen sich die Spielsteine auf dem Spielfeld. Eine sehr gute Idee, sehr schön realisiert. Das hat mich sehr beeindruckt.