---
layout: "image"
title: "rrb74.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb74.jpg"
weight: "6"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11764
- /details0f65.html
imported:
- "2019"
_4images_image_id: "11764"
_4images_cat_id: "1050"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11764 -->
