---
layout: "image"
title: "Voith-Schneider-Antrieb"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk026.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11607
- /details8ebf.html
imported:
- "2019"
_4images_image_id: "11607"
_4images_cat_id: "1041"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11607 -->
...in verschiedenen Ausführungen
