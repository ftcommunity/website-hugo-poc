---
layout: "image"
title: "rrb01.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb01.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11691
- /details8faf-3.html
imported:
- "2019"
_4images_image_id: "11691"
_4images_cat_id: "1041"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11691 -->
