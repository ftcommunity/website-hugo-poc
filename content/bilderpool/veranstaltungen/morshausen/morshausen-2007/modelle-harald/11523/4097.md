---
layout: "comment"
hidden: true
title: "4097"
date: "2007-09-23T20:06:30"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Was da gerade überfahren wird, ist übrigens ein Ultra-stark untersetztes Getriebe. Etwas ähnliches habe ich im Magazin des ft-Club Holland gesehen, aber dort kam der Powermot von der anderen Seite und die Anordnung baut ziemlich in die Länge.

Der Getriebewinkel (eigentlich für den M-Motor) hat stirnseitig ein Loch in der Mitte, und da hindurch verläuft die Antriebsachse des Powermot. Innendrin sitzt eine ft-Klemmschnecke, von der aus der Antrieb wie gewohnt weiter geführt wird. Die schwarze Achse ist eine ft-Rastachse 60 mit aufgeklebtem Z44, sodass zu beiden Seiten Antriebskraft abgegeben werden kann. Es ist vielleicht müßig zu sagen, dass diese Anordnung den Grundstock für ein neues Gleichlaufgetriebe darstellt.