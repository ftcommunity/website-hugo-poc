---
layout: "image"
title: "Muldenkipper"
date: "2007-09-16T16:59:44"
picture: "harald3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11523
- /details2908.html
imported:
- "2019"
_4images_image_id: "11523"
_4images_cat_id: "1041"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11523 -->
