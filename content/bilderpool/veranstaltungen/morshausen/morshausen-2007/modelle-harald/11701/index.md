---
layout: "image"
title: "rrb11.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11701
- /detailsfd0d-2.html
imported:
- "2019"
_4images_image_id: "11701"
_4images_cat_id: "1041"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11701 -->
