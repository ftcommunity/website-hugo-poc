---
layout: "image"
title: "Gleislege-Zug"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk090.jpg"
weight: "8"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11671
- /detailsa524.html
imported:
- "2019"
_4images_image_id: "11671"
_4images_cat_id: "1057"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11671 -->
Ein schicker Verteilerkasten.
