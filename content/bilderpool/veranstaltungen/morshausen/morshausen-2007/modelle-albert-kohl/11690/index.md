---
layout: "image"
title: "Gleichleg Zuge mit der Konstrukteur Albert Kohl"
date: "2007-09-17T00:08:00"
picture: "FT-Mrshausen-2007_088.jpg"
weight: "14"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Peter Damen (Alias PeterHolland)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11690
- /detailsa3db-2.html
imported:
- "2019"
_4images_image_id: "11690"
_4images_cat_id: "1057"
_4images_user_id: "22"
_4images_image_date: "2007-09-17T00:08:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11690 -->
