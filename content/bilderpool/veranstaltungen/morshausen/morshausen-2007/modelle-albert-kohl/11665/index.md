---
layout: "image"
title: "Gleislege-Zug - Stromversorgung"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk084.jpg"
weight: "2"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11665
- /detailsb822-2.html
imported:
- "2019"
_4images_image_id: "11665"
_4images_cat_id: "1057"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11665 -->
