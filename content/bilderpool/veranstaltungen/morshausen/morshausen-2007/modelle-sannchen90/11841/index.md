---
layout: "image"
title: "Bagger 3"
date: "2007-09-18T11:41:09"
picture: "PICT5644.jpg"
weight: "8"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11841
- /details2400.html
imported:
- "2019"
_4images_image_id: "11841"
_4images_cat_id: "1049"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:41:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11841 -->
