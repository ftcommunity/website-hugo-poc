---
layout: "image"
title: "Die Schwebebahn ..."
date: "2007-10-15T18:10:20"
picture: "imm000_0A.jpg"
weight: "21"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Schwebebahn"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/12223
- /detailsb5c5.html
imported:
- "2019"
_4images_image_id: "12223"
_4images_cat_id: "1051"
_4images_user_id: "381"
_4images_image_date: "2007-10-15T18:10:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12223 -->
sollte dieses Jahr 3-4 unabhängig Runden fahren, dann für 10s anhalten und wieder 3-4 Runden in der anderen Richtung weiterfahren.
Die Bahn sollte jederzeit per Tastendruck 
1. am Bahnhof anhalten (das ging)
2. wieder weiter fahren (das wird vielleicht nächstes Jahr gehen)
3. irgendwo anhalten (das wird vielleicht nächstes Jahr gehen)
Hürde 1) Mein Mono-Flop ist vor ca. 25 Jahren kaputt gegangen, das hatte ich aber verdrängt und es ist mir erst 1 Woche vor der Con aufgefallen.
Lösung 1) Ein Getriebe mit Eierscheibe ersetzt den Mono-Flop.
Hürde 2) Die Lichtschranke funktionierte bei mir im keller hervorragend, bei dem schönen Wetter in Mörshausen war die Lichtschranke außer Funktion.
Lösung 2)  Ich musste erst die Rolläden herunterlassen und einige Feineinstellungen vornehmen bis es einigermaßen funktionierte.
Hürde 3) Nach einem mehrstündigen Dauerbetrieb ließen dann doch irgendwelche Schleifkontake nach und die Bahn ruckelte in einigen Bahnabschnitten.
Lösung 3) Keine Ahnung, - habe ich noch nicht untersucht.

Fazit: Nächstes Jahr nehme ich garantiert keine normale Lichtschranke um den Grundbaustein anzusteuern und der Mono-Flop ist bis dahin hoffentlich repariert.
