---
layout: "image"
title: "Verseilmaschine"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk079.jpg"
weight: "11"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11660
- /detailsb5b8.html
imported:
- "2019"
_4images_image_id: "11660"
_4images_cat_id: "1051"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11660 -->
Seile werden hier mehrfach (!) verdrillt, um ein richtig dickes Seil zu bilden.
