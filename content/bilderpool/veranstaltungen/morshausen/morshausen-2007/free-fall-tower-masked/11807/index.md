---
layout: "image"
title: "Bremse"
date: "2007-09-18T11:03:40"
picture: "PICT5760.jpg"
weight: "10"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11807
- /details81a6-2.html
imported:
- "2019"
_4images_image_id: "11807"
_4images_cat_id: "1038"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:03:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11807 -->
Die Zahnräder hatten noch nichtmal Verschleiß.
