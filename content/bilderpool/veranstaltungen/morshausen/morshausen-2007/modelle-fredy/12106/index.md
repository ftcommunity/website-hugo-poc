---
layout: "image"
title: "auto040.JPG"
date: "2007-10-03T08:55:43"
picture: "auto040.JPG"
weight: "14"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12106
- /details2d2e.html
imported:
- "2019"
_4images_image_id: "12106"
_4images_cat_id: "1071"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T08:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12106 -->
Klein aber oho!

Da ist alles dran, was drangehört. Die Rast-Kegelzahnräder hinten machen die darunter montierten ft-Taster zu ft-Schaltern.
