---
layout: "image"
title: "auto043.JPG"
date: "2007-10-03T09:03:02"
picture: "auto043.JPG"
weight: "17"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12109
- /details488b.html
imported:
- "2019"
_4images_image_id: "12109"
_4images_cat_id: "1071"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T09:03:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12109 -->
Und hier das Schmankerl: der komplette Antriebsstrang. Dieser verläuft vom PowerMot beginnend erst einmal am Hinterachsdifferenzial vorbei (hier verdeckt) auf das Z10 am linken Bildrand und von da aus über die Kegelräder nach vorn.
