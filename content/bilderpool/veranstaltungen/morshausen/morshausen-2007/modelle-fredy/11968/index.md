---
layout: "image"
title: "Industriemodell"
date: "2007-09-25T09:15:03"
picture: "industriemodell3.jpg"
weight: "9"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11968
- /detailsd54b-2.html
imported:
- "2019"
_4images_image_id: "11968"
_4images_cat_id: "1071"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:15:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11968 -->
