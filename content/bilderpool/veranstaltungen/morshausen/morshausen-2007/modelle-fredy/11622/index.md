---
layout: "image"
title: "Boot, Fahrgestell"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk041.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11622
- /details5dd0-2.html
imported:
- "2019"
_4images_image_id: "11622"
_4images_cat_id: "1071"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11622 -->
