---
layout: "image"
title: "Industriemodell"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk045.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11626
- /details38da.html
imported:
- "2019"
_4images_image_id: "11626"
_4images_cat_id: "1071"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11626 -->
Ganz schön kompliziert, die Abläufe darin...
