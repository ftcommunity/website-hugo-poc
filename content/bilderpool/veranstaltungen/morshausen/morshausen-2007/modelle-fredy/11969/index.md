---
layout: "image"
title: "Industriemodell"
date: "2007-09-25T09:15:03"
picture: "industriemodell4.jpg"
weight: "10"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11969
- /detailsa92a.html
imported:
- "2019"
_4images_image_id: "11969"
_4images_cat_id: "1071"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:15:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11969 -->
