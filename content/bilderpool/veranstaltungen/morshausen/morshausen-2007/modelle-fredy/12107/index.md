---
layout: "image"
title: "auto041.JPG"
date: "2007-10-03T08:56:52"
picture: "auto041.JPG"
weight: "15"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12107
- /details1bb4-2.html
imported:
- "2019"
_4images_image_id: "12107"
_4images_cat_id: "1071"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T08:56:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12107 -->
Kurvenlicht (leuchtet dahin, wohin die Fahrt geht) hat er auch.
