---
layout: "image"
title: "Plotter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk046.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11627
- /detailscfaa.html
imported:
- "2019"
_4images_image_id: "11627"
_4images_cat_id: "1071"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11627 -->
Noch nicht ganz fertig, aber nach HPs "Dicker Berta" konstruiert: Das Papier wird bewegt; Der Stift bewegt sich nur quer dazu.
