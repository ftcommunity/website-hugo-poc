---
layout: "image"
title: "fischertechnik-Luft"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk030.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11611
- /details5d11-3.html
imported:
- "2019"
_4images_image_id: "11611"
_4images_cat_id: "1036"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11611 -->
...im Obergeschoss
