---
layout: "image"
title: "Theke"
date: "2007-09-18T10:58:15"
picture: "PICT5756.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11800
- /details951f.html
imported:
- "2019"
_4images_image_id: "11800"
_4images_cat_id: "1036"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T10:58:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11800 -->
Mein Vater ist seit einigen Jahren jedes Mal mit riesigem Engagement hinter der Theke zu finden. 

Ihm gefällt die Convention jedes Mal riesig, er mag die Atmosphäre ... und das, obwohl er noch nie selber ein Modell aufgebaut hat ...