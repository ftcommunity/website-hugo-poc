---
layout: "image"
title: "Geburtstag"
date: "2007-09-18T10:58:59"
picture: "PICT5768.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11801
- /details460b.html
imported:
- "2019"
_4images_image_id: "11801"
_4images_cat_id: "1036"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T10:58:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11801 -->
Markus beim Auspacken seines Geburtstagsgeschenks