---
layout: "image"
title: "rrb68.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb68.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11758
- /details20ea.html
imported:
- "2019"
_4images_image_id: "11758"
_4images_cat_id: "1036"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11758 -->
