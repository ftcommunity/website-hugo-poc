---
layout: "image"
title: "Waffel018.JPG"
date: "2007-09-23T19:29:26"
picture: "Waffel018.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11934
- /detailsc0d2-2.html
imported:
- "2019"
_4images_image_id: "11934"
_4images_cat_id: "1036"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:29:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11934 -->
Et voila, da hat jemand den Lötkolben gegen den Rührmix getauscht und macht jetzt Waffelteig. Lecker sind sie geworden!
