---
layout: "image"
title: "Fischertechnik Luft in Sauerland"
date: "2007-09-17T19:31:58"
picture: "FT-Mrshausen-2007_217.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11781
- /details4a90.html
imported:
- "2019"
_4images_image_id: "11781"
_4images_cat_id: "1036"
_4images_user_id: "22"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11781 -->
