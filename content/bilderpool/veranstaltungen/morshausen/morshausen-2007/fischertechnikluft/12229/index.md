---
layout: "image"
title: "Wer war denn ..."
date: "2007-10-15T18:10:21"
picture: "imm033_34.jpg"
weight: "33"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/12229
- /details95c9.html
imported:
- "2019"
_4images_image_id: "12229"
_4images_cat_id: "1036"
_4images_user_id: "381"
_4images_image_date: "2007-10-15T18:10:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12229 -->
das eigentlich? Das Bild von vorne ist leider nix geworden, - naja ist halt so eine alte Kamera mit ner Filmrolle, die noch richtig ratscht und klickt. Wenn ich mir aber die unscharfen Fotos von den digitalen Dingern angucke, dann sind die Bilder eigentlich ganz OK.
