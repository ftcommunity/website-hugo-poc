---
layout: "image"
title: "'After Fischertechnik'"
date: "2007-09-16T22:07:25"
picture: "FT-Mrshausen-2007_198.jpg"
weight: "9"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (alias Peter Holland)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11574
- /details16d4.html
imported:
- "2019"
_4images_image_id: "11574"
_4images_cat_id: "1036"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11574 -->
