---
layout: "image"
title: "Fischertechnikluft"
date: "2007-09-18T13:10:31"
picture: "fischertechnikluft2.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11860
- /details75ab.html
imported:
- "2019"
_4images_image_id: "11860"
_4images_cat_id: "1036"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T13:10:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11860 -->
