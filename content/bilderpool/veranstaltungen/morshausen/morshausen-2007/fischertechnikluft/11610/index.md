---
layout: "image"
title: "fischertechnik-Einkäufe"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk029.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11610
- /details5919-2.html
imported:
- "2019"
_4images_image_id: "11610"
_4images_cat_id: "1036"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11610 -->
