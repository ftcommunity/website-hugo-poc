---
layout: "image"
title: "Roboter"
date: "2007-10-03T16:36:25"
picture: "robter1.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12118
- /details8765.html
imported:
- "2019"
_4images_image_id: "12118"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12118 -->
