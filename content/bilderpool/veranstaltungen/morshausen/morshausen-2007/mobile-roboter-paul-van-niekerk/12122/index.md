---
layout: "image"
title: "Codierrad zum Positionieren"
date: "2007-10-03T16:36:25"
picture: "robter5.jpg"
weight: "8"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12122
- /details0d7e.html
imported:
- "2019"
_4images_image_id: "12122"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12122 -->
