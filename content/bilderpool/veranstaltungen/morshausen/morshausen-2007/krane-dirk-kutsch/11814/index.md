---
layout: "image"
title: "Lagerung des Masts"
date: "2007-09-18T11:10:13"
picture: "PICT5596.jpg"
weight: "43"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11814
- /details27fe.html
imported:
- "2019"
_4images_image_id: "11814"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:10:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11814 -->
Beim Aufbau gibt es ein gewaltiges Drehmoment im Lager des Kranmasts. Diese Konstruktion kann die Kräfte aufnehmen, während der Mast schräg steht.
