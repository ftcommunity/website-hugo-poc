---
layout: "image"
title: "Faltkran"
date: "2007-09-18T11:10:34"
picture: "PICT5597.jpg"
weight: "44"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11815
- /details1e9a-2.html
imported:
- "2019"
_4images_image_id: "11815"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:10:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11815 -->
