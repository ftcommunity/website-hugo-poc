---
layout: "image"
title: "Aufbau"
date: "2007-09-18T11:08:05"
picture: "PICT5590.jpg"
weight: "39"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11810
- /detailsce56.html
imported:
- "2019"
_4images_image_id: "11810"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:08:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11810 -->
