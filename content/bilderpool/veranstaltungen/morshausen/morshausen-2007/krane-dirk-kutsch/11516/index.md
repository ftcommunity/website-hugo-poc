---
layout: "image"
title: "Steuerung"
date: "2007-09-16T16:59:44"
picture: "kraene4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11516
- /details996e.html
imported:
- "2019"
_4images_image_id: "11516"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11516 -->
