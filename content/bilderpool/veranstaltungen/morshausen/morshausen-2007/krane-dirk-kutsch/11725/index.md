---
layout: "image"
title: "rrb35.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb35.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11725
- /details2f1d.html
imported:
- "2019"
_4images_image_id: "11725"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11725 -->
