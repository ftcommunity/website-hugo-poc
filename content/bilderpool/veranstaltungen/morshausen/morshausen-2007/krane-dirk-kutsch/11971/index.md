---
layout: "image"
title: "Kran"
date: "2007-09-25T09:26:10"
picture: "kran02.jpg"
weight: "46"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11971
- /details877a.html
imported:
- "2019"
_4images_image_id: "11971"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11971 -->
