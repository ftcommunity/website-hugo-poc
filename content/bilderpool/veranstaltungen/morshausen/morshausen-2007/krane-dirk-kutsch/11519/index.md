---
layout: "image"
title: "Faltkran"
date: "2007-09-16T16:59:44"
picture: "kraene7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11519
- /details007d-2.html
imported:
- "2019"
_4images_image_id: "11519"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11519 -->
