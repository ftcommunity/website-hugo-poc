---
layout: "image"
title: "Führerhaus"
date: "2007-09-25T09:26:10"
picture: "kran04.jpg"
weight: "48"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11973
- /detailsa3ef.html
imported:
- "2019"
_4images_image_id: "11973"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11973 -->
