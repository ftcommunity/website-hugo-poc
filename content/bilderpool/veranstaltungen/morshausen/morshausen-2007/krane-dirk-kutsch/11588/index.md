---
layout: "image"
title: "Faltkran"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk007.jpg"
weight: "12"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11588
- /details288b.html
imported:
- "2019"
_4images_image_id: "11588"
_4images_cat_id: "1040"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11588 -->
...der ging bis zur Decke
