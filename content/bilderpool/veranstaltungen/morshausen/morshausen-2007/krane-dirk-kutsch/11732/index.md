---
layout: "image"
title: "rrb42.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb42.jpg"
weight: "36"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11732
- /details7629.html
imported:
- "2019"
_4images_image_id: "11732"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11732 -->
