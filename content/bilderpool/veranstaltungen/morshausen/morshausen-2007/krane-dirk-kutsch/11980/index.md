---
layout: "image"
title: "Drehkranz"
date: "2007-09-25T09:26:52"
picture: "kran11.jpg"
weight: "55"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11980
- /details3d4b-2.html
imported:
- "2019"
_4images_image_id: "11980"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:52"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11980 -->
