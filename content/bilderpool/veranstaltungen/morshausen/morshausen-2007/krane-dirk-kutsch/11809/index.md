---
layout: "image"
title: "Faltkran"
date: "2007-09-18T11:07:50"
picture: "PICT5555.jpg"
weight: "38"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11809
- /details75bd-2.html
imported:
- "2019"
_4images_image_id: "11809"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:07:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11809 -->
