---
layout: "image"
title: "rrb38.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb38.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11728
- /detailsaf06-3.html
imported:
- "2019"
_4images_image_id: "11728"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11728 -->
