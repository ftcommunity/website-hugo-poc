---
layout: "image"
title: "Aufbau 3"
date: "2007-09-18T11:08:41"
picture: "PICT5592.jpg"
weight: "41"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11812
- /details63ce.html
imported:
- "2019"
_4images_image_id: "11812"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:08:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11812 -->
