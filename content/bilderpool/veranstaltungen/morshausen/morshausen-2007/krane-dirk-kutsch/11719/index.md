---
layout: "image"
title: "rrb29.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb29.jpg"
weight: "23"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11719
- /detailsd4c2.html
imported:
- "2019"
_4images_image_id: "11719"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11719 -->
