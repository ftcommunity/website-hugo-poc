---
layout: "image"
title: "Seilbagger"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk008.jpg"
weight: "13"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11589
- /details2e6f.html
imported:
- "2019"
_4images_image_id: "11589"
_4images_cat_id: "1040"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11589 -->
Wahlweise konnte eine riesige Baggerschaufel oder eine riesige Schleppschaufel angeseilt werden.
