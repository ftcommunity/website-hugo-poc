---
layout: "image"
title: "rrb22.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb22.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11712
- /detailsa60d-3.html
imported:
- "2019"
_4images_image_id: "11712"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11712 -->
