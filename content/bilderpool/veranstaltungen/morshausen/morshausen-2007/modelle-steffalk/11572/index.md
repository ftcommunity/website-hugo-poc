---
layout: "image"
title: "Meister Stefan"
date: "2007-09-16T22:07:25"
picture: "FT-Mrshausen-2007_189.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Peter Damen (alias PeterHolland)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11572
- /details0d71-2.html
imported:
- "2019"
_4images_image_id: "11572"
_4images_cat_id: "1037"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11572 -->
