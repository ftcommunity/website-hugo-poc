---
layout: "image"
title: "7Seg147.JPG"
date: "2007-09-22T13:15:13"
picture: "7Seg147.JPG"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11904
- /details4ad1-2.html
imported:
- "2019"
_4images_image_id: "11904"
_4images_cat_id: "1037"
_4images_user_id: "4"
_4images_image_date: "2007-09-22T13:15:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11904 -->
Der Powermot dreht links und rechts je eine Schnecke m1 und verfährt damit den rollengelagerten Keil, der die Matrix aus 7 E-Magneten hebt und senkt. Die E-Magnete sind für je ein Segment der Anzeige zuständig. Da die Anzeige 4 Stellen hat, wird der ganze Block auf den Schienen (unten im Bild) hin und her verfahren.
