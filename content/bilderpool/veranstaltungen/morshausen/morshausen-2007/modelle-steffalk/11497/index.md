---
layout: "image"
title: "Heck mit Federung"
date: "2007-09-16T16:53:32"
picture: "uhren09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11497
- /details2c9f.html
imported:
- "2019"
_4images_image_id: "11497"
_4images_cat_id: "1037"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11497 -->
