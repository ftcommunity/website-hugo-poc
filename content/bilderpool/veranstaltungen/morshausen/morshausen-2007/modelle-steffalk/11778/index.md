---
layout: "image"
title: "rrb88.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb88.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11778
- /details78e9.html
imported:
- "2019"
_4images_image_id: "11778"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11778 -->
