---
layout: "image"
title: "rrb83.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb83.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11773
- /details3b68.html
imported:
- "2019"
_4images_image_id: "11773"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11773 -->
