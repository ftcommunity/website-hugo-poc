---
layout: "image"
title: "Uhr"
date: "2007-09-16T16:53:32"
picture: "uhren03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11491
- /details5219.html
imported:
- "2019"
_4images_image_id: "11491"
_4images_cat_id: "1037"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11491 -->
