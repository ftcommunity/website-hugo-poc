---
layout: "image"
title: "7Seg146.JPG"
date: "2007-09-22T13:09:20"
picture: "7Seg146.JPG"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11903
- /details3a74-2.html
imported:
- "2019"
_4images_image_id: "11903"
_4images_cat_id: "1037"
_4images_user_id: "4"
_4images_image_date: "2007-09-22T13:09:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11903 -->
Die Silberlinge erzeugen den Systemtakt durch Herunterteilen der 50 Hz aus der Steckdose. Davor im Bild die Taster, mit denen die Uhr gestellt wird.
