---
layout: "comment"
hidden: true
title: "4076"
date: "2007-09-22T15:39:48"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hast die Zugmaschine am rechten Vorderrad Achsenbruch erlitten oder steht das Rad absichtlich schräg?

Gruß,
Stefan