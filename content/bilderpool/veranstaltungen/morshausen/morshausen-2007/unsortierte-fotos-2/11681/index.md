---
layout: "image"
title: "Kleinmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk100.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11681
- /details64ce-2.html
imported:
- "2019"
_4images_image_id: "11681"
_4images_cat_id: "1053"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11681 -->
