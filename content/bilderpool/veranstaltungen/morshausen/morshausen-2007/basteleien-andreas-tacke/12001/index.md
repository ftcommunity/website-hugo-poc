---
layout: "image"
title: "Roboter"
date: "2007-09-25T09:58:38"
picture: "tst3.jpg"
weight: "16"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12001
- /detailsd6d0-2.html
imported:
- "2019"
_4images_image_id: "12001"
_4images_cat_id: "1066"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:58:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12001 -->
