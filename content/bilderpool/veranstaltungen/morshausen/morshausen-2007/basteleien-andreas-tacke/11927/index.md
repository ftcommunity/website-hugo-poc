---
layout: "image"
title: "tst030.JPG"
date: "2007-09-23T19:03:31"
picture: "tst030.JPG"
weight: "8"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11927
- /detailsaa8a.html
imported:
- "2019"
_4images_image_id: "11927"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:03:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11927 -->
