---
layout: "image"
title: "Roboter"
date: "2007-09-25T09:58:38"
picture: "tst4.jpg"
weight: "17"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12002
- /details878a.html
imported:
- "2019"
_4images_image_id: "12002"
_4images_cat_id: "1066"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:58:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12002 -->
