---
layout: "image"
title: "tst035.JPG"
date: "2007-09-23T19:14:54"
picture: "tst035.JPG"
weight: "13"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11932
- /detailsd76b-2.html
imported:
- "2019"
_4images_image_id: "11932"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:14:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11932 -->
Andreas hat seinen eigenen Adapter zur Anpassung des PowerMot.
