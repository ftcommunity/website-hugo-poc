---
layout: "image"
title: "tst032.JPG"
date: "2007-09-23T19:08:47"
picture: "tst032.JPG"
weight: "10"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11929
- /detailsa72b.html
imported:
- "2019"
_4images_image_id: "11929"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:08:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11929 -->
Die Felgen wurden zu ft passend gemacht. In Bayern sagt man: "wer ko, der ko".
