---
layout: "image"
title: "Greifer-Spiel"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk103.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11684
- /details4f5e.html
imported:
- "2019"
_4images_image_id: "11684"
_4images_cat_id: "1066"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11684 -->
