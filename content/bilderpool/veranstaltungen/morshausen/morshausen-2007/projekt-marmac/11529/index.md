---
layout: "image"
title: "Beamer"
date: "2007-09-16T17:09:08"
picture: "beamer2.jpg"
weight: "2"
konstrukteure: 
- "marmac"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11529
- /detailsfb23-2.html
imported:
- "2019"
_4images_image_id: "11529"
_4images_cat_id: "1044"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11529 -->
