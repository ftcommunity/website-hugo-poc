---
layout: "image"
title: "rrb43.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb43.jpg"
weight: "8"
konstrukteure: 
- "Markus Mack (MarMac)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11733
- /detailse39b.html
imported:
- "2019"
_4images_image_id: "11733"
_4images_cat_id: "1044"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11733 -->
