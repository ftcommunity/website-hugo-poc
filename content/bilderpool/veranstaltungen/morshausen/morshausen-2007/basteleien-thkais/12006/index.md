---
layout: "image"
title: "Roboter"
date: "2007-09-25T10:17:07"
picture: "roboter2.jpg"
weight: "10"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/12006
- /detailsa655-2.html
imported:
- "2019"
_4images_image_id: "12006"
_4images_cat_id: "1046"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T10:17:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12006 -->
