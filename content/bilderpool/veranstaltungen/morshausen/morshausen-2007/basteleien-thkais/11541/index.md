---
layout: "image"
title: "Roboter"
date: "2007-09-16T17:09:08"
picture: "elektronik1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11541
- /detailsbcf4-2.html
imported:
- "2019"
_4images_image_id: "11541"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11541 -->
