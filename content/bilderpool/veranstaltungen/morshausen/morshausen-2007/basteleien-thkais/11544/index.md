---
layout: "image"
title: "Kartenleser für das Robo IF"
date: "2007-09-16T17:09:08"
picture: "elektronik4.jpg"
weight: "4"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11544
- /detailsdf78.html
imported:
- "2019"
_4images_image_id: "11544"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11544 -->
