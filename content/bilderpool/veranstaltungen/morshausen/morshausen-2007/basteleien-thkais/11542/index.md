---
layout: "image"
title: "Roboter"
date: "2007-09-16T17:09:08"
picture: "elektronik2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11542
- /details91eb-2.html
imported:
- "2019"
_4images_image_id: "11542"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11542 -->
