---
layout: "image"
title: "Getriebe"
date: "2007-09-16T19:54:57"
picture: "triceratops3.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11570
- /details7582.html
imported:
- "2019"
_4images_image_id: "11570"
_4images_cat_id: "1052"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:54:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11570 -->
