---
layout: "image"
title: "Planetarium"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk039.jpg"
weight: "9"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11620
- /details4c3e-2.html
imported:
- "2019"
_4images_image_id: "11620"
_4images_cat_id: "1052"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11620 -->
Die Sonderzahnräder - einfach genial!
