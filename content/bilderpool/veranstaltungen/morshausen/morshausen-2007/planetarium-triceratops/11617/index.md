---
layout: "image"
title: "Planetarium"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk036.jpg"
weight: "6"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11617
- /detailsb011.html
imported:
- "2019"
_4images_image_id: "11617"
_4images_cat_id: "1052"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11617 -->
Das Zentralgetriebe bei der Sonne.
