---
layout: "image"
title: "Schweißen"
date: "2007-09-16T16:53:32"
picture: "industriemodell5.jpg"
weight: "5"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11511
- /details5a81-2.html
imported:
- "2019"
_4images_image_id: "11511"
_4images_cat_id: "1039"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11511 -->
