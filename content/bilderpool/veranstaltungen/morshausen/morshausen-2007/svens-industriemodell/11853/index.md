---
layout: "image"
title: "Industriemodell 2"
date: "2007-09-18T11:50:17"
picture: "PICT5534.jpg"
weight: "9"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11853
- /detailsf663.html
imported:
- "2019"
_4images_image_id: "11853"
_4images_cat_id: "1039"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:50:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11853 -->
