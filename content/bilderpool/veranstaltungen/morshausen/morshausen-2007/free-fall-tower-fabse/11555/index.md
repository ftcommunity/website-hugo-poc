---
layout: "image"
title: "Bremse"
date: "2007-09-16T19:38:31"
picture: "tower4.jpg"
weight: "4"
konstrukteure: 
- "fabse"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11555
- /detailscba5.html
imported:
- "2019"
_4images_image_id: "11555"
_4images_cat_id: "1048"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11555 -->
