---
layout: "image"
title: "rrb71.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb71.jpg"
weight: "21"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11761
- /details2106.html
imported:
- "2019"
_4images_image_id: "11761"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11761 -->
