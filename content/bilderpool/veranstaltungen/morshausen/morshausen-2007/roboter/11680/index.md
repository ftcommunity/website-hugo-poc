---
layout: "image"
title: "Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk099.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11680
- /detailsd198-2.html
imported:
- "2019"
_4images_image_id: "11680"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11680 -->
... mit vielen Bewegungsfunktionen.
