---
layout: "image"
title: "rrb62.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb62.jpg"
weight: "18"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11752
- /details6523.html
imported:
- "2019"
_4images_image_id: "11752"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11752 -->
