---
layout: "image"
title: "rrb63.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb63.jpg"
weight: "19"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11753
- /details0124-2.html
imported:
- "2019"
_4images_image_id: "11753"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11753 -->
