---
layout: "image"
title: "rrb55.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb55.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11745
- /details21e7-2.html
imported:
- "2019"
_4images_image_id: "11745"
_4images_cat_id: "1035"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11745 -->
