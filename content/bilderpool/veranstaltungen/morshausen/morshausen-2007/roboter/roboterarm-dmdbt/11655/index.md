---
layout: "image"
title: "Roboterarm mit dezentraler Mikrocontroller-Steuerung"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk074.jpg"
weight: "9"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11655
- /details10b6.html
imported:
- "2019"
_4images_image_id: "11655"
_4images_cat_id: "1035"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11655 -->
