---
layout: "image"
title: "Steuerung"
date: "2007-09-16T16:53:32"
picture: "greifarm4.jpg"
weight: "4"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11479
- /details98fd.html
imported:
- "2019"
_4images_image_id: "11479"
_4images_cat_id: "1035"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11479 -->
