---
layout: "image"
title: "rrb64.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb64.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11754
- /details23d9-4.html
imported:
- "2019"
_4images_image_id: "11754"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11754 -->
