---
layout: "image"
title: "Greifarm"
date: "2007-09-16T17:09:08"
picture: "roboterarm04.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11534
- /details3f87.html
imported:
- "2019"
_4images_image_id: "11534"
_4images_cat_id: "1045"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11534 -->
