---
layout: "image"
title: "Roboterarm"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk096.jpg"
weight: "11"
konstrukteure: 
- "Severin"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11677
- /details6bf4-2.html
imported:
- "2019"
_4images_image_id: "11677"
_4images_cat_id: "1045"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11677 -->
... mit LLWIN programmiert, und sein junger Konstrukteur. Ebenfalls Hut ab! Gerade ist nur eine Kette gerissen, die nur schwer wieder im komplizierten Inneren angebracht werden kann.
