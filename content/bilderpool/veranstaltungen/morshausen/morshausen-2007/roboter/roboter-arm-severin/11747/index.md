---
layout: "image"
title: "rrb57.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb57.jpg"
weight: "14"
konstrukteure: 
- "Severin"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11747
- /detailsb796-2.html
imported:
- "2019"
_4images_image_id: "11747"
_4images_cat_id: "1045"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11747 -->
