---
layout: "image"
title: "Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk063.jpg"
weight: "6"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11644
- /detailsa8ce.html
imported:
- "2019"
_4images_image_id: "11644"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11644 -->
Hier steckt die Pfiffigkeit in der Elektronik: Im Vordergrund sichtbar ist ein Kameramodul mit einem Mikrocontroller, der so umprogrammiert wurde, dass er das normalerweise von RoboPro über die V.24-Leitung geschickte Protokoll so erzeugt, dass der Mikrocontroller das Robo-Interface steuert, welches sozusagen denkt, es hinge an einem PC mit RoboPro. Im Hintergrund schräg angebaut ist ein Display mit einer grafischen Benutzeroberfläche, deren Mauszeiger von einem Joystick gesteuert wird, dessen "Stick" die in hinten in der Mitte senkrecht hochragende Metallachse ist. Damit kann man den Roboter mit einem eigenen Befehlssatz programmieren. Sagenhaft gemacht.
