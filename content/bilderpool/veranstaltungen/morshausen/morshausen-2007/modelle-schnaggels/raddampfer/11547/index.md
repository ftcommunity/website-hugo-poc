---
layout: "image"
title: "Schiff"
date: "2007-09-16T19:38:31"
picture: "robomax1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11547
- /details63ab.html
imported:
- "2019"
_4images_image_id: "11547"
_4images_cat_id: "1601"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11547 -->
