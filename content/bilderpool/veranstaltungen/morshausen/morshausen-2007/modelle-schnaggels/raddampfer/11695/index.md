---
layout: "image"
title: "rrb05.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb05.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11695
- /details68ce.html
imported:
- "2019"
_4images_image_id: "11695"
_4images_cat_id: "1601"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11695 -->
