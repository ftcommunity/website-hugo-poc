---
layout: "image"
title: "rrb03.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11693
- /details7fe8-2.html
imported:
- "2019"
_4images_image_id: "11693"
_4images_cat_id: "1601"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11693 -->
