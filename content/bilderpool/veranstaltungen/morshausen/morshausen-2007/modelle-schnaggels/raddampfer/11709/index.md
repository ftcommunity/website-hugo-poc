---
layout: "image"
title: "rrb19.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb19.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11709
- /detailsd986-3.html
imported:
- "2019"
_4images_image_id: "11709"
_4images_cat_id: "1601"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11709 -->
