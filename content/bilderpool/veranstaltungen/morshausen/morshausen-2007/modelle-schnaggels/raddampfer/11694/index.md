---
layout: "image"
title: "rrb04.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb04.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11694
- /details0dd7.html
imported:
- "2019"
_4images_image_id: "11694"
_4images_cat_id: "1601"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11694 -->
