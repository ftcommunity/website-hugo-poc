---
layout: "image"
title: "Robo Max"
date: "2007-09-16T19:38:31"
picture: "robomax3.jpg"
weight: "1"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11549
- /detailse6e6-2.html
imported:
- "2019"
_4images_image_id: "11549"
_4images_cat_id: "1047"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11549 -->
