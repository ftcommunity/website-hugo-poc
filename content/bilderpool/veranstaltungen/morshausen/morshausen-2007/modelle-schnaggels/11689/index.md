---
layout: "image"
title: "Treppe-Robot"
date: "2007-09-16T22:07:27"
picture: "FT-Mrshausen-2007_172.jpg"
weight: "8"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Peter Damen (Alias PeterHolland)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/11689
- /detailse6eb-2.html
imported:
- "2019"
_4images_image_id: "11689"
_4images_cat_id: "1047"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11689 -->
