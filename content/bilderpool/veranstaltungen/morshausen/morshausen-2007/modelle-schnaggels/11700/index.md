---
layout: "image"
title: "rrb10.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb10.jpg"
weight: "9"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11700
- /details2bd3-2.html
imported:
- "2019"
_4images_image_id: "11700"
_4images_cat_id: "1047"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11700 -->
