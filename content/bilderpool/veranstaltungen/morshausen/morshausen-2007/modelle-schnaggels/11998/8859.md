---
layout: "comment"
hidden: true
title: "8859"
date: "2009-03-28T09:41:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Udo, wir waren gar nicht uneinig, dass es mit Standard-ft-Felgen bisher keine uns bekannte Lösung für das Problem gibt (was dennoch nie heißt, dass es keine gibt, nur kennen wir halt keine). Ich meinte ja eben genau, dass man mit selbstgebauten Rädern beliebig in die Größe gehen kann und alles hinbekommt. Das Problem ist eben nur, es möglichst kompakt zu schaffen. Aber es wird ja von vielen Leuten dran gearbeitet und getüftelt. :-)

Gruß,
Stefan