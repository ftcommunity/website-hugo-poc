---
layout: "image"
title: "Mörderkabel"
date: "2007-09-18T10:55:51"
picture: "PICT5649.jpg"
weight: "11"
konstrukteure: 
- "Erik Andresen"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11799
- /details2537.html
imported:
- "2019"
_4images_image_id: "11799"
_4images_cat_id: "1034"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T10:55:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11799 -->
