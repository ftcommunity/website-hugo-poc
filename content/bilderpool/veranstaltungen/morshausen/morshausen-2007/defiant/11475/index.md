---
layout: "image"
title: "Elektronik"
date: "2007-09-16T16:53:32"
picture: "roboter9.jpg"
weight: "9"
konstrukteure: 
- "Defiant"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11475
- /details220b.html
imported:
- "2019"
_4images_image_id: "11475"
_4images_cat_id: "1034"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11475 -->
