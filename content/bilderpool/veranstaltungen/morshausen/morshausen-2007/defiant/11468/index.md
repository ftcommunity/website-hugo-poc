---
layout: "image"
title: "Roboter"
date: "2007-09-16T16:53:32"
picture: "roboter2.jpg"
weight: "2"
konstrukteure: 
- "Defiant"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11468
- /details1afb.html
imported:
- "2019"
_4images_image_id: "11468"
_4images_cat_id: "1034"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11468 -->
Der Roboter fuhr oben rum und ist Hindernissen mit Hilfe von Sensoren ausgewichen.
