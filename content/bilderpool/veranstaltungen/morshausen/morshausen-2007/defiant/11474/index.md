---
layout: "image"
title: "Elektronik"
date: "2007-09-16T16:53:32"
picture: "roboter8.jpg"
weight: "8"
konstrukteure: 
- "Defiant"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11474
- /detailscc74.html
imported:
- "2019"
_4images_image_id: "11474"
_4images_cat_id: "1034"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11474 -->
