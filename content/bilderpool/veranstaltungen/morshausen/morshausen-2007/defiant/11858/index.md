---
layout: "image"
title: "Steckerhäufung"
date: "2007-09-18T12:53:06"
picture: "roboter1_2.jpg"
weight: "12"
konstrukteure: 
- "Defiant"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11858
- /details1018.html
imported:
- "2019"
_4images_image_id: "11858"
_4images_cat_id: "1034"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T12:53:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11858 -->
