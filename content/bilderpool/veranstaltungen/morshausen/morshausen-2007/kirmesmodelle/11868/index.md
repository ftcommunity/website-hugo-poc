---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-18T15:08:28"
picture: "kirmesmodelle7.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11868
- /detailsb0de.html
imported:
- "2019"
_4images_image_id: "11868"
_4images_cat_id: "1058"
_4images_user_id: "453"
_4images_image_date: "2007-09-18T15:08:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11868 -->
