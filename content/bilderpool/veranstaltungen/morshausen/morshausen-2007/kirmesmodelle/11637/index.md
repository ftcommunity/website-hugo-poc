---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk056.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11637
- /detailsfd92-2.html
imported:
- "2019"
_4images_image_id: "11637"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11637 -->
Sehr schön: Auch für Kinder nachbaubare Modelle wurden gezeigt.
