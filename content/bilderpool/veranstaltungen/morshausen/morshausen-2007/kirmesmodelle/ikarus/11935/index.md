---
layout: "image"
title: "kikar114.JPG"
date: "2007-09-23T19:42:32"
picture: "kikar114.JPG"
weight: "5"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/11935
- /details1863.html
imported:
- "2019"
_4images_image_id: "11935"
_4images_cat_id: "1069"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:42:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11935 -->
