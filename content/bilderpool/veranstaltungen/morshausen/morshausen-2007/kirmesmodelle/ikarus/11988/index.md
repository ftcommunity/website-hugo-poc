---
layout: "image"
title: "Ikarus"
date: "2007-09-25T09:37:28"
picture: "ikarus3.jpg"
weight: "11"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11988
- /details84f4-2.html
imported:
- "2019"
_4images_image_id: "11988"
_4images_cat_id: "1069"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:37:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11988 -->
