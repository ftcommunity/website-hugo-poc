---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-18T15:08:28"
picture: "kirmesmodelle2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/11863
- /details189e.html
imported:
- "2019"
_4images_image_id: "11863"
_4images_cat_id: "1069"
_4images_user_id: "453"
_4images_image_date: "2007-09-18T15:08:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11863 -->
