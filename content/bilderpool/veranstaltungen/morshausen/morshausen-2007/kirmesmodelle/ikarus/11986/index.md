---
layout: "image"
title: "Ikarus"
date: "2007-09-25T09:37:28"
picture: "ikarus1.jpg"
weight: "9"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11986
- /detailsa1ab-2.html
imported:
- "2019"
_4images_image_id: "11986"
_4images_cat_id: "1069"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:37:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11986 -->
