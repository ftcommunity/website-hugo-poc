---
layout: "image"
title: "Werbetafel für Profi I'm Walking"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk048.jpg"
weight: "2"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11629
- /details29f2-2.html
imported:
- "2019"
_4images_image_id: "11629"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11629 -->
... von hinten.
