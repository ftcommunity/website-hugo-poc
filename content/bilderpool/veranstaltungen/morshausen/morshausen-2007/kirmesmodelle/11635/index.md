---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk054.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11635
- /detailsc81d.html
imported:
- "2019"
_4images_image_id: "11635"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11635 -->
Zugmaschine mit Kran für den Anhänger.
