---
layout: "image"
title: "Kirmesmodell"
date: "2007-09-18T12:29:03"
picture: "kirmesmodell1.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/11854
- /details25f6-2.html
imported:
- "2019"
_4images_image_id: "11854"
_4images_cat_id: "1058"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T12:29:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11854 -->
