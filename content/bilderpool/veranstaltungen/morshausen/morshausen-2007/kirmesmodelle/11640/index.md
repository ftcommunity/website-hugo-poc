---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk059.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/11640
- /details9738-2.html
imported:
- "2019"
_4images_image_id: "11640"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11640 -->
Das Modell schaltet durch die links vorne sichtbaren kurze Achse nach jeder Umdrehung am Polwendeschalter die Drehrichtung um - es sei denn, der Konstrukteur beaufschlagt den Pneumatikzylinder rechts vorne. Dadurch wird über zwei Rastkurbeln am Winkelgetriebe der ganze Polwendeschalter einfach abgesenkt.
