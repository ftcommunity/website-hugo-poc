---
layout: "image"
title: "Abstützung"
date: "2004-09-23T18:31:16"
picture: "IMG_0908.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "-?-"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/2638
- /details98cb.html
imported:
- "2019"
_4images_image_id: "2638"
_4images_cat_id: "254"
_4images_user_id: "6"
_4images_image_date: "2004-09-23T18:31:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2638 -->
Megageniale Kranstütze die mittels Seil ausgefahren wird. Das Einfahren wird mit gespannten Gummis erledigt.
