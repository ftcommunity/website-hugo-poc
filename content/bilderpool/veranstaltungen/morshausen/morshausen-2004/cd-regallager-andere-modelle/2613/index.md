---
layout: "image"
title: "CD-Regallager"
date: "2004-09-21T13:30:08"
picture: "Alle_CDs_zu_mir.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2613
- /details46a5.html
imported:
- "2019"
_4images_image_id: "2613"
_4images_cat_id: "254"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2613 -->
Wie viel passen da wohl rein