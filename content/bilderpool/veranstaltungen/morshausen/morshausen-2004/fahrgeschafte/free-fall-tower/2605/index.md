---
layout: "image"
title: "Gondel vom Power Tower"
date: "2004-09-20T15:35:22"
picture: "Gondel_Power_Tower_ft01.jpg"
weight: "4"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2605
- /detailsef66-2.html
imported:
- "2019"
_4images_image_id: "2605"
_4images_cat_id: "247"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2605 -->
Schön gebaut, sieht aus wie das Original.
