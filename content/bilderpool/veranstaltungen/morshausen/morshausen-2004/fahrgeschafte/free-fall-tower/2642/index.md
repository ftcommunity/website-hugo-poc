---
layout: "image"
title: "DSC00850"
date: "2004-09-29T16:31:27"
picture: "DSC00850.jpg"
weight: "7"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "-?-"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/2642
- /details7005-2.html
imported:
- "2019"
_4images_image_id: "2642"
_4images_cat_id: "247"
_4images_user_id: "10"
_4images_image_date: "2004-09-29T16:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2642 -->
