---
layout: "image"
title: "Interface Testmodell"
date: "2004-09-21T15:55:05"
picture: "Testmodell_ft01.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2637
- /details4912.html
imported:
- "2019"
_4images_image_id: "2637"
_4images_cat_id: "257"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2637 -->
