---
layout: "comment"
hidden: true
title: "316"
date: "2004-11-15T17:05:08"
uploadBy:
- "Uwe Schmejkal"
license: "unknown"
imported:
- "2019"
---
Modell StoßofenBild zeigt den mit Blöcken voll belegten Blockdrücker, und den Ofen Einstoßbereich mit den Hitzeschutzketten.