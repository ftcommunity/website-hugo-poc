---
layout: "image"
title: "Physik & Mathematik"
date: "2004-09-21T13:30:28"
picture: "Hexapod.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2627
- /details1a4f.html
imported:
- "2019"
_4images_image_id: "2627"
_4images_cat_id: "259"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2627 -->
