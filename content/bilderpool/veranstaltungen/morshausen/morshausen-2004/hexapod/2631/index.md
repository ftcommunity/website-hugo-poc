---
layout: "image"
title: "Das Hexapod bei der Arbeit"
date: "2004-09-21T15:55:05"
picture: "Hexapod_ft02.jpg"
weight: "3"
konstrukteure: 
- "remadus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2631
- /detailsebb0.html
imported:
- "2019"
_4images_image_id: "2631"
_4images_cat_id: "259"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2631 -->
Remadus' Hexapod schreibt gerade ein Wort.