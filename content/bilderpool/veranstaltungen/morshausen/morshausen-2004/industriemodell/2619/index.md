---
layout: "image"
title: "Industriemodell mit 8 Stationen"
date: "2004-09-21T13:30:08"
picture: "Die_Bearbeitungsstrasse.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2619
- /details4f5d.html
imported:
- "2019"
_4images_image_id: "2619"
_4images_cat_id: "265"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2619 -->
