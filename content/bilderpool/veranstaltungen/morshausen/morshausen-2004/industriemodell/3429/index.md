---
layout: "image"
title: "Stanzen"
date: "2004-12-23T11:04:05"
picture: "Stanzen.JPG"
weight: "18"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/3429
- /details1fe1-2.html
imported:
- "2019"
_4images_image_id: "3429"
_4images_cat_id: "265"
_4images_user_id: "1"
_4images_image_date: "2004-12-23T11:04:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3429 -->
