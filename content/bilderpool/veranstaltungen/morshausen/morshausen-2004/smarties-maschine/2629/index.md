---
layout: "image"
title: "Viele, viele bunte Smarties"
date: "2004-09-21T13:30:28"
picture: "Smarties_aus_den_Niederlanden.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- /php/details/2629
- /detailsa95a.html
imported:
- "2019"
_4images_image_id: "2629"
_4images_cat_id: "248"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2629 -->
