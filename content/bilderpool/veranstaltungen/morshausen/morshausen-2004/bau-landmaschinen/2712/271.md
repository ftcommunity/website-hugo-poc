---
layout: "comment"
hidden: true
title: "271"
date: "2004-10-04T22:49:50"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Unglaublich kreativer Einsatz der TeileKompliment! Der Greifer ist toll. Auf die Idee, die Greiferzapfen so an das innere Secheck anzubringen (mit den ganz kurzen Teilen, in denen die kurzen Achsen stecken), wäre ich wahrscheinlich nicht gekommen. Wie ist denn der Baustein in der Mitte befestigt (der die Schließer-Seilrolle trägt)?