---
layout: "image"
title: "Rübenvollernter"
date: "2004-09-29T20:10:33"
picture: "Wwwwm02.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2657
- /details54ce.html
imported:
- "2019"
_4images_image_id: "2657"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:10:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2657 -->
Mehr Details von der wrritze-wrrohren Wrroiwe-Wrropp-Maschin (for non-Germans: die knall-rote Rüben-Rupf-Maschine)

Blick von oben ins Gerät: links der Rübenbunker (mit Kratzboden), rechts oben der Behälter für das Kraut.