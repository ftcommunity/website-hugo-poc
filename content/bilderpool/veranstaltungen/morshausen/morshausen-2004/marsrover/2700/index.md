---
layout: "image"
title: "IMGP4859"
date: "2004-10-01T21:19:05"
picture: "IMGP4859.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/2700
- /details3b2b.html
imported:
- "2019"
_4images_image_id: "2700"
_4images_cat_id: "262"
_4images_user_id: "1"
_4images_image_date: "2004-10-01T21:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2700 -->
