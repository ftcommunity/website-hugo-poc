---
layout: "image"
title: "pvd 136"
date: "2004-11-10T20:48:21"
picture: "pvd_136.jpg"
weight: "16"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3112
- /details8ded-2.html
imported:
- "2019"
_4images_image_id: "3112"
_4images_cat_id: "261"
_4images_user_id: "5"
_4images_image_date: "2004-11-10T20:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3112 -->
