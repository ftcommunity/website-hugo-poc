---
layout: "image"
title: "IMGP4816"
date: "2004-10-01T21:19:05"
picture: "IMGP4816.jpg"
weight: "11"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/2676
- /details2e9e.html
imported:
- "2019"
_4images_image_id: "2676"
_4images_cat_id: "261"
_4images_user_id: "1"
_4images_image_date: "2004-10-01T21:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2676 -->
