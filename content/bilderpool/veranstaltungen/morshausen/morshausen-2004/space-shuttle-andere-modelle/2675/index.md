---
layout: "image"
title: "IMGP4815"
date: "2004-10-01T21:19:04"
picture: "IMGP4815.jpg"
weight: "10"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/2675
- /details4e06-2.html
imported:
- "2019"
_4images_image_id: "2675"
_4images_cat_id: "261"
_4images_user_id: "1"
_4images_image_date: "2004-10-01T21:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2675 -->
