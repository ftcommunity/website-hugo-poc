---
layout: "image"
title: "pvd 052"
date: "2004-11-10T20:48:20"
picture: "pvd_052.jpg"
weight: "15"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/3028
- /detailsbea2-2.html
imported:
- "2019"
_4images_image_id: "3028"
_4images_cat_id: "261"
_4images_user_id: "5"
_4images_image_date: "2004-11-10T20:48:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3028 -->
