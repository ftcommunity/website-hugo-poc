---
layout: "image"
title: "Ultralight-Mobile - Drachenflieger"
date: "2004-09-29T19:20:20"
picture: "Ultra02.jpg"
weight: "6"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
schlagworte: ["Mobile", "Ultralight"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2653
- /detailse729-2.html
imported:
- "2019"
_4images_image_id: "2653"
_4images_cat_id: "261"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2653 -->
Die Idee ist einfach Klasse!

Ein Mobile aus ft-Ultralight-Fliegern. Hier die erste von vier Beteiligten, der Drachenflieger. Nummer 4 war ein Satellit und das Foto ist mir leider verschütt gegangen.

Gehört einfach in jedes Kinderzimmer!
