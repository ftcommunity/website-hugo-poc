---
layout: "image"
title: "Ultralight-Mobile - Motorsegler"
date: "2004-09-29T19:20:20"
picture: "Ultra04.jpg"
weight: "8"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "-?-"
schlagworte: ["Mobile", "Ultralight"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2655
- /detailsde42.html
imported:
- "2019"
_4images_image_id: "2655"
_4images_cat_id: "261"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2655 -->
Ein Mobile aus ft-Ultralight-Fliegern. Hier der dritte von vier Teilnehmern, der Motorsegler.

Wenn das nichts für eine Minikit-Serie ist, dann weiß ich gar nichts mehr.

Gehört einfach in jedes Kinderzimmer!
