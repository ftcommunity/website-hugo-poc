---
layout: "image"
title: "Holgers Startrampe"
date: "2004-09-20T15:35:22"
picture: "Raketenbahnhof_ft01.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2607
- /details82d4-2.html
imported:
- "2019"
_4images_image_id: "2607"
_4images_cat_id: "261"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2607 -->
Sehr schönes Modell, vor allem Masstabsgerecht!
