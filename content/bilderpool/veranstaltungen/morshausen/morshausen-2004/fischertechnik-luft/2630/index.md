---
layout: "image"
title: "Ein schönes altes Thekenschild"
date: "2004-09-21T15:55:05"
picture: "altes_Werbeschild_ft01.jpg"
weight: "16"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/2630
- /details237c-4.html
imported:
- "2019"
_4images_image_id: "2630"
_4images_cat_id: "263"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2630 -->
