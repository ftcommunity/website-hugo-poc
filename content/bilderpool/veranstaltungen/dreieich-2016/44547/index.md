---
layout: "image"
title: "Plakat"
date: "2016-10-03T19:55:15"
picture: "dreieich01.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44547
- /detailsa5fd.html
imported:
- "2019"
_4images_image_id: "44547"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44547 -->
Das war sie also, die Convention 2016. Eigentlich ist es ja das Plakat vom letzten Jahr - dezent angepaßt.