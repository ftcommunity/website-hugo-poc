---
layout: "image"
title: "Das Karussell"
date: "2016-10-01T15:02:07"
picture: "modellevonfamiliebusch2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44481
- /details2738.html
imported:
- "2019"
_4images_image_id: "44481"
_4images_cat_id: "3289"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44481 -->
Trotz strenger Sicherheitsvorkehrungen ist es gelungen das Geheimnis des Antriebs zu lüften: Ein luftgekühlter 2PS-Hafermotor!