---
layout: "image"
title: "Schraubenaufzug"
date: "2016-10-03T19:55:15"
picture: "dreieich07.jpg"
weight: "12"
konstrukteure: 
- "cpuetter"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44553
- /details59b1.html
imported:
- "2019"
_4images_image_id: "44553"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44553 -->
[Kugelbahn mit Schneckenaufzug](http://www.ftcommunity.de/categories.php?cat_id=3104), hier war er mal live zu erleben - elegant!