---
layout: "image"
title: "Präzisions_Stapler"
date: "2016-10-03T10:59:01"
picture: "remadus1.jpg"
weight: "1"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44542
- /details7fd7.html
imported:
- "2019"
_4images_image_id: "44542"
_4images_cat_id: "3312"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44542 -->
Der Roboter mit etwa 4000 (Mikro-) Schritten Auflösung pro Umdrehung einer Achse (insgesamt 5)  stapelt Spielfigürchen.
