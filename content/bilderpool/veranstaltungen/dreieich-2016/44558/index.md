---
layout: "image"
title: "Marshmallows Dungeon"
date: "2016-10-03T19:55:15"
picture: "dreieich12.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44558
- /details4bd5.html
imported:
- "2019"
_4images_image_id: "44558"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44558 -->
Hu, das klingt ja gruselig und läßt schauriges erahnen ...