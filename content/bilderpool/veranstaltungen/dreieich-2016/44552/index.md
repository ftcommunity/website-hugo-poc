---
layout: "image"
title: "Flotter Flitzer"
date: "2016-10-03T19:55:15"
picture: "dreieich06.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44552
- /details72b0.html
imported:
- "2019"
_4images_image_id: "44552"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44552 -->
Mit Power-Motor 8:1 sah das Autochen ungewöhnlich aus, fuhr dafür aber recht agil durch die Halle. "Tank" vorne, Mittelmotor (gerade noch so) und Heckantrieb sorgten für kurventauglichen Fahrspaß.