---
layout: "image"
title: "Unimog"
date: "2016-10-01T15:02:06"
picture: "modellevonpeterholland4.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44478
- /details563a.html
imported:
- "2019"
_4images_image_id: "44478"
_4images_cat_id: "3285"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44478 -->
