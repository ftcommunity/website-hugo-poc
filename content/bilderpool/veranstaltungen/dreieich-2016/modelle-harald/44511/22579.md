---
layout: "comment"
hidden: true
title: "22579"
date: "2016-10-03T13:30:13"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Der war schon mal "fertig" (und letztes Jahr auch dabei). Kabine und Frontlader waren den ganzen Tag über abgebaut und lagen lose auf dem Tisch.

https://ftcommunity.de/categories.php?cat_id=3313&page=3

Die Vorderachse hat, ganz frisch aus dem 3D-Drucker, zwei blaue Teile bekommen, die das Rad und die Spurstange aufnehmen, die Antriebsachse lagern und das Ganze an den Gelenksteinen der Lenkung festmachen. Leider war das aber zu frisch (erst am Tag vor der Convention montiert) und da fehlt immer noch was:
- die Lenkung schielt (verzogenes Lenktrapez, an die Spurstangen hatte ich gar nicht mehr gedacht)
- jetzt bleibt die Achse da, wo sie hingehört (das war der eigentliche Zweck der Übung), aber jetzt verschiebt sich der ganze Block vertikal und dann klemmen die Kegelräder (zu hoch) oder sie knattern (zu tief). Also das Ganze nochmal....

Gruß,
Harald