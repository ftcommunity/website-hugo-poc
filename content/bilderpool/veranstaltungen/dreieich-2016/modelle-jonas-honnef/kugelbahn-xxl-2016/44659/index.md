---
layout: "image"
title: "Kugelbahn XXL 2016"
date: "2016-10-23T20:57:10"
picture: "kugelbahnxxl14.jpg"
weight: "14"
konstrukteure: 
- "Jonas Honnef"
fotografen:
- "Jonas Honnef"
uploadBy: "Jonas Honnef"
license: "unknown"
legacy_id:
- /php/details/44659
- /details3f69-2.html
imported:
- "2019"
_4images_image_id: "44659"
_4images_cat_id: "3323"
_4images_user_id: "2649"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44659 -->
Kugelbahn XXL 2016 "Vakuumgreifer"