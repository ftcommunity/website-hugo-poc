---
layout: "image"
title: "Schwebebahnstrecke 1"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
schlagworte: ["Schwebebahn", "Depot"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44498
- /details488c.html
imported:
- "2019"
_4images_image_id: "44498"
_4images_cat_id: "3299"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44498 -->
An diesem Ende der Strecke gibt es geballte Technik. Das Depot mit Weichen und Abstellgleisen aus grauen Teilen gebaut, und die hiesige Endstation in gelb. Eine Kehrschleife umrundet das Depot.