---
layout: "image"
title: "Brückenfahrt"
date: "2016-10-24T21:20:51"
picture: "brueckenfahrt1.jpg"
weight: "1"
konstrukteure: 
- "LKW: Claus Ludwig"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- /php/details/44667
- /detailsa9db.html
imported:
- "2019"
_4images_image_id: "44667"
_4images_cat_id: "3324"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44667 -->
