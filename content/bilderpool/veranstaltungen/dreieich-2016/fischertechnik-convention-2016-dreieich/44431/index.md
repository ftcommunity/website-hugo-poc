---
layout: "image"
title: "fischertechnikconventiondreieich25.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich25.jpg"
weight: "25"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44431
- /detailsc028.html
imported:
- "2019"
_4images_image_id: "44431"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44431 -->
