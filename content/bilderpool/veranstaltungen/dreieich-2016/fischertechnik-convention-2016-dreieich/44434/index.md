---
layout: "image"
title: "fischertechnikconventiondreieich28.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich28.jpg"
weight: "28"
konstrukteure: 
- "Tino Werner"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44434
- /details6746.html
imported:
- "2019"
_4images_image_id: "44434"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44434 -->
