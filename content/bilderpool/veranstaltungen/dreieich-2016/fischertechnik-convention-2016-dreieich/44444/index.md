---
layout: "image"
title: "fischertechnikconventiondreieich38.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich38.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44444
- /details047d.html
imported:
- "2019"
_4images_image_id: "44444"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44444 -->
