---
layout: "image"
title: "fischertechnikconventiondreieich20.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich20.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44426
- /detailsd66b.html
imported:
- "2019"
_4images_image_id: "44426"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44426 -->
