---
layout: "image"
title: "fischertechnikconventiondreieich21.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich21.jpg"
weight: "21"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44427
- /details8d32.html
imported:
- "2019"
_4images_image_id: "44427"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44427 -->
