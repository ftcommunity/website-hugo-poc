---
layout: "image"
title: "fischertechnikconventiondreieich37.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich37.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44443
- /details1050.html
imported:
- "2019"
_4images_image_id: "44443"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44443 -->
