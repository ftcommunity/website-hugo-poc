---
layout: "image"
title: "fischertechnikconventiondreieich04.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich04.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44410
- /details22dc.html
imported:
- "2019"
_4images_image_id: "44410"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44410 -->
