---
layout: "image"
title: "fischertechnikconventiondreieich57.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich57.jpg"
weight: "57"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44463
- /details0b24.html
imported:
- "2019"
_4images_image_id: "44463"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44463 -->
