---
layout: "image"
title: "fischertechnikconventiondreieich08.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich08.jpg"
weight: "8"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44414
- /detailsb65c.html
imported:
- "2019"
_4images_image_id: "44414"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44414 -->
