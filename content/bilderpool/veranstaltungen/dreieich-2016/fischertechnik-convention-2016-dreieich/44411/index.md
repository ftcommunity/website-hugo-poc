---
layout: "image"
title: "fischertechnikconventiondreieich05.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich05.jpg"
weight: "5"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44411
- /details814e.html
imported:
- "2019"
_4images_image_id: "44411"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44411 -->
