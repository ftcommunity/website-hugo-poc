---
layout: "image"
title: "fischertechnikconventiondreieich49.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich49.jpg"
weight: "49"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44455
- /details139f-2.html
imported:
- "2019"
_4images_image_id: "44455"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44455 -->
