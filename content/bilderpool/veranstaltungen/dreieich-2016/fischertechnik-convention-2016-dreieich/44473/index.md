---
layout: "image"
title: "fischertechnikconventiondreieich67.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich67.jpg"
weight: "67"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44473
- /detailsa208.html
imported:
- "2019"
_4images_image_id: "44473"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44473 -->
