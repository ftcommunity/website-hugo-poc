---
layout: "image"
title: "fischertechnikconventiondreieich10.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich10.jpg"
weight: "10"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44416
- /details0dbe.html
imported:
- "2019"
_4images_image_id: "44416"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44416 -->
