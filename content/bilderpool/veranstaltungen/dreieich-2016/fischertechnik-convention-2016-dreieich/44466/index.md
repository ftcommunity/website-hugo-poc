---
layout: "image"
title: "PDR-Robotiker Entwicklung der I2C-Motortreiber (> 12V, 2A)  für die TXT"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich60.jpg"
weight: "60"
konstrukteure: 
- "Fabian"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44466
- /detailse0d3.html
imported:
- "2019"
_4images_image_id: "44466"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44466 -->
Die PDR-Robotiker Entwicklung der I2C-Motortreiber (> 12V, 2A)  für die TXT war für mich ein Highlight !... 

Website der Robotik-AG des Privatgymnasiums Dr. Richter (kurz PDR) aus Kelkheim :
https://pdr-robotik.de/
