---
layout: "image"
title: "fischertechnikconventiondreieich34.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich34.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44440
- /detailsad8b.html
imported:
- "2019"
_4images_image_id: "44440"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44440 -->
