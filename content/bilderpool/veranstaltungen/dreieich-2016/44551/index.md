---
layout: "image"
title: "Fachbesucher"
date: "2016-10-03T19:55:15"
picture: "dreieich05.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/44551
- /detailsca57.html
imported:
- "2019"
_4images_image_id: "44551"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44551 -->
ft-Freunde in angeregter Diskussion vertieft.