---
layout: "image"
title: "Kugelbahn"
date: "2016-10-03T10:59:01"
picture: "harry1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44536
- /details87c2.html
imported:
- "2019"
_4images_image_id: "44536"
_4images_cat_id: "3309"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44536 -->
