---
layout: "image"
title: "Die_fertige_Brücke"
date: "2016-10-03T10:59:01"
picture: "denkmal3.jpg"
weight: "3"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44529
- /details8c23.html
imported:
- "2019"
_4images_image_id: "44529"
_4images_cat_id: "3307"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44529 -->
