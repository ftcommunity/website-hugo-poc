---
layout: "image"
title: "Kettenspanner"
date: "2016-10-03T10:59:01"
picture: "trapp2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44539
- /details5173.html
imported:
- "2019"
_4images_image_id: "44539"
_4images_cat_id: "3310"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44539 -->
Das Zahnrad in der Mitte sieht nicht nur witzig aus. Es ist auch ein einfacher Kettenspanner.

Video-Link:
https://www.youtube.com/watch?v=0Mj9BG4pfzg
