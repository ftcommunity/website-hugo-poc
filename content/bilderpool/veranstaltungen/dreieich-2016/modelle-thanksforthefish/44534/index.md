---
layout: "image"
title: "FT_Brille"
date: "2016-10-03T10:59:01"
picture: "thanksforthefish3.jpg"
weight: "4"
konstrukteure: 
- "Philip Geerken"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44534
- /detailsa2d2.html
imported:
- "2019"
_4images_image_id: "44534"
_4images_cat_id: "3286"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44534 -->
