---
layout: "image"
title: "Getriebe"
date: "2016-10-03T10:59:01"
picture: "geometer2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44541
- /details502b.html
imported:
- "2019"
_4images_image_id: "44541"
_4images_cat_id: "3311"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44541 -->
Video-Link:
https://www.youtube.com/watch?v=wmY78J86oq8
und
https://www.youtube.com/watch?v=jwmn9pi0rkA

Konstrukteur korrigiert, Website-Team, 24.09.2020
