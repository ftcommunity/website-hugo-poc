---
layout: "image"
title: "Getriebe"
date: "2016-10-03T10:59:01"
picture: "geometer1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44540
- /details5afa.html
imported:
- "2019"
_4images_image_id: "44540"
_4images_cat_id: "3311"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44540 -->
Video-Link:
https://www.youtube.com/watch?v=XQnyOGWpPl8
