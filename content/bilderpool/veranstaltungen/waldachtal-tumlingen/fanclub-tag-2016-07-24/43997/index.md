---
layout: "image"
title: "3D Drucker"
date: "2016-07-25T14:24:24"
picture: "ftfct37.jpg"
weight: "61"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43997
- /detailscc71.html
imported:
- "2019"
_4images_image_id: "43997"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43997 -->
auch wenn es die Farbe vermuten lässt, das Gehäuse der Steuerung stammt nicht aus dem Drucker
