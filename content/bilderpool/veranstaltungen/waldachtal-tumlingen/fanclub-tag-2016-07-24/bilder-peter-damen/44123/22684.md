---
layout: "comment"
hidden: true
title: "22684"
date: "2016-10-27T13:16:30"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ob das neue Design nun gefällt oder nicht - Fischertechnik hat in einem Facebookpost angekündigt, dass es 2017 neue Bauteile geben wird und das dazugehörige Bild gibt Raum für Spekulationen: https://www.facebook.com/fischertechnik/photos/a.438455482889970.100010.429777700424415/1160666724002172/?type=3&theater
Was befindet sich wohl unter der Decke?

Gruß
David