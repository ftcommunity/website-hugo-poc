---
layout: "comment"
hidden: true
title: "22354"
date: "2016-08-02T13:58:13"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wenn ich das richtig verstehe, ist diese Umfrage von fischertechnik selbst? Das kann doch hoffentlich nicht deren Ernst sein!? Will man da jetzt denselben Bockmist bauen wie bei Lego? Mit welchem Argument wollen sie denn dann fischertechnik gegenüber Lego verkaufen? "Wir machen denselben Blödsinn mit bergeweise nichtsnutziger Verkleidungsteile, also kauft doch ft anstatt Lego?" Wenn das wirklich so wäre, würde ich mich Severin entsetzt anschließen.

Gruß,
Stefan