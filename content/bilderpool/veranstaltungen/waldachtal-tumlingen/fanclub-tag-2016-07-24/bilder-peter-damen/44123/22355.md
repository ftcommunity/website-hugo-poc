---
layout: "comment"
hidden: true
title: "22355"
date: "2016-08-02T14:02:34"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ich kann mich Stefan und Severin nur anschließen. Spezialteile nur für das Design sind unnötig. Damit würde Fischertechnik das seit 51 Jahren bewährte Konzept des Baukastensystems aufgeben. Und dass man auch mit einer begrenzten Zahl an Bauteilen sehr schöne Modelle bauen kann, dürfte dieser Bilderpool ja eindrücklich demonstrieren.

Grüße,
David