---
layout: "image"
title: "RoboPro  ftc-Community  Slide"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen26.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44114
- /details4ff1.html
imported:
- "2019"
_4images_image_id: "44114"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44114 -->
