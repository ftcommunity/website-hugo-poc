---
layout: "image"
title: "Vieles Chinesisches Interesse beim Wettkampf"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44102
- /details3216.html
imported:
- "2019"
_4images_image_id: "44102"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44102 -->
