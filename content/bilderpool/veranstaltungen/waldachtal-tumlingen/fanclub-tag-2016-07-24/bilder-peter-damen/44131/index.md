---
layout: "image"
title: "Interessante Antriebe Familie Stoll"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen43.jpg"
weight: "43"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44131
- /details5f66.html
imported:
- "2019"
_4images_image_id: "44131"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44131 -->
