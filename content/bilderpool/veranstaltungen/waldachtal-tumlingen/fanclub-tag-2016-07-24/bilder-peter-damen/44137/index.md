---
layout: "image"
title: "Interessante Antriebe Familie Stoll"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen49.jpg"
weight: "49"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44137
- /details95f5.html
imported:
- "2019"
_4images_image_id: "44137"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44137 -->
