---
layout: "image"
title: "RoboPro  ftc-Community  Slide"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44113
- /detailsac19.html
imported:
- "2019"
_4images_image_id: "44113"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44113 -->
