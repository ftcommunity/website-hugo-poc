---
layout: "image"
title: "Wiener Rad"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44093
- /details0f80-3.html
imported:
- "2019"
_4images_image_id: "44093"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44093 -->
