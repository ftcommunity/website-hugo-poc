---
layout: "image"
title: "Brücke Mittags"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen33.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44121
- /detailsfb32.html
imported:
- "2019"
_4images_image_id: "44121"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44121 -->
