---
layout: "image"
title: "Harald's  Modell"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen56.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44144
- /details4bee-2.html
imported:
- "2019"
_4images_image_id: "44144"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44144 -->
