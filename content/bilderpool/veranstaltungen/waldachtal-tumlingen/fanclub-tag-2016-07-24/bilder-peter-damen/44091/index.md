---
layout: "image"
title: "Vieles Chinesisches Interesse beim Tripod Hand   (Peter Poederoyen NL)"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen03.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44091
- /details02ca.html
imported:
- "2019"
_4images_image_id: "44091"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44091 -->
