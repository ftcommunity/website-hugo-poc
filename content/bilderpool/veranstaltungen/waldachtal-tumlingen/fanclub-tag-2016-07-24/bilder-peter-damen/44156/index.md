---
layout: "image"
title: "Eine der wenige Modellen aus Austria"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen68.jpg"
weight: "68"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44156
- /details6846.html
imported:
- "2019"
_4images_image_id: "44156"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44156 -->
