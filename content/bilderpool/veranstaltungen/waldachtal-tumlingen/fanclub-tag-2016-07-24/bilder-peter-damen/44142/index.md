---
layout: "image"
title: "Harald's  Modell"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen54.jpg"
weight: "54"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44142
- /details1683-2.html
imported:
- "2019"
_4images_image_id: "44142"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44142 -->
