---
layout: "image"
title: "Schönes Modell"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen69.jpg"
weight: "69"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44157
- /detailsa6c0-2.html
imported:
- "2019"
_4images_image_id: "44157"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44157 -->
