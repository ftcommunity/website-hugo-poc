---
layout: "image"
title: "Interessante Antriebe Familie Stoll"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen44.jpg"
weight: "44"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/44132
- /detailsebd6-3.html
imported:
- "2019"
_4images_image_id: "44132"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44132 -->
