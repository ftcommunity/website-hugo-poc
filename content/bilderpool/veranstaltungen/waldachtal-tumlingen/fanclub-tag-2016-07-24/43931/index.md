---
layout: "image"
title: "Überblick (2)"
date: "2016-07-24T20:10:12"
picture: "fct06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43931
- /details84b6.html
imported:
- "2019"
_4images_image_id: "43931"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43931 -->
