---
layout: "image"
title: "Überblick (1)"
date: "2016-07-24T20:10:12"
picture: "fct05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43930
- /detailsed86.html
imported:
- "2019"
_4images_image_id: "43930"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43930 -->
