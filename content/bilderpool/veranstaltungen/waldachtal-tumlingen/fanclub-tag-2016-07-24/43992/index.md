---
layout: "image"
title: "3D Druck"
date: "2016-07-25T14:24:24"
picture: "ftfct32.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43992
- /detailsd73c.html
imported:
- "2019"
_4images_image_id: "43992"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43992 -->
Gedruckt vom Fischertechnik 3D Drucker
