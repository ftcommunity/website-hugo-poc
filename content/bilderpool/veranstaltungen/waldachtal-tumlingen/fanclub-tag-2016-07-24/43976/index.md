---
layout: "image"
title: "Rekordversuch Brücke"
date: "2016-07-25T14:24:24"
picture: "ftfct16.jpg"
weight: "41"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43976
- /details415e-2.html
imported:
- "2019"
_4images_image_id: "43976"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43976 -->
Blick auf die Brücke
