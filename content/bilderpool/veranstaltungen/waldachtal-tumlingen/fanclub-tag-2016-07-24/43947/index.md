---
layout: "image"
title: "noch 'ne Drohne"
date: "2016-07-24T20:10:12"
picture: "fct22.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43947
- /details544d-3.html
imported:
- "2019"
_4images_image_id: "43947"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43947 -->
