---
layout: "image"
title: "Brücke12"
date: "2016-07-27T18:03:54"
picture: "Brcke12.jpg"
weight: "75"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "FT"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/44035
- /details5c46.html
imported:
- "2019"
_4images_image_id: "44035"
_4images_cat_id: "3255"
_4images_user_id: "560"
_4images_image_date: "2016-07-27T18:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44035 -->
