---
layout: "image"
title: "Schloss Zapfenstein"
date: "2016-07-24T20:10:12"
picture: "fct17.jpg"
weight: "16"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43942
- /detailsc1bb-2.html
imported:
- "2019"
_4images_image_id: "43942"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43942 -->
