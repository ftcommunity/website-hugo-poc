---
layout: "image"
title: "3D Druck"
date: "2016-07-25T14:24:24"
picture: "ftfct34.jpg"
weight: "58"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43994
- /details2f4d.html
imported:
- "2019"
_4images_image_id: "43994"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43994 -->
sehr präzise gefertigtes Behältnis
