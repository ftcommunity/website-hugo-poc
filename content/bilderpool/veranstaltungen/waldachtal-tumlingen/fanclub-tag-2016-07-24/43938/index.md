---
layout: "image"
title: "Technik-Wimmelbild"
date: "2016-07-24T20:10:12"
picture: "fct13.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43938
- /details622b.html
imported:
- "2019"
_4images_image_id: "43938"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43938 -->
Die Unterseite vom Gürteltier, ähh, vom Flugzeug-Schlepper. Zwei gelenkte und angetriebene Achsen, dazu ein Zweiganggetriebe, mittels E-Motor geschaltet (unten mitte, die Kurbel zieht/schiebt die Schaltkulisse)
