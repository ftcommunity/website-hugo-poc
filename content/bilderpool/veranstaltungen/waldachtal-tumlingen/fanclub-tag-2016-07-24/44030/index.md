---
layout: "image"
title: "Brücke7"
date: "2016-07-27T18:03:54"
picture: "Brcke07.jpg"
weight: "70"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "FT"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/44030
- /detailsf8fe.html
imported:
- "2019"
_4images_image_id: "44030"
_4images_cat_id: "3255"
_4images_user_id: "560"
_4images_image_date: "2016-07-27T18:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44030 -->
