---
layout: "image"
title: "Logistikzentrum"
date: "2016-07-25T14:24:24"
picture: "ftfct30.jpg"
weight: "54"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43990
- /details045e.html
imported:
- "2019"
_4images_image_id: "43990"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43990 -->
Hochregallager und Logistikbereich
