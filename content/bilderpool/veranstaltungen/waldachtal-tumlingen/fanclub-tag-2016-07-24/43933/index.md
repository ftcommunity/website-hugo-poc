---
layout: "image"
title: "Rolltreppe"
date: "2016-07-24T20:10:12"
picture: "fct08.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43933
- /detailsb5a6.html
imported:
- "2019"
_4images_image_id: "43933"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43933 -->
