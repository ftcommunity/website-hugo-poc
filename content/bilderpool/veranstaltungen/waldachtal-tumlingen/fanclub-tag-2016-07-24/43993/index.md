---
layout: "image"
title: "3D Druck"
date: "2016-07-25T14:24:24"
picture: "ftfct33.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43993
- /detailsc082-2.html
imported:
- "2019"
_4images_image_id: "43993"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43993 -->
Ohne Nachbearbeitung Bausteins lassen sich keine Zapfen in die Nut schieben
