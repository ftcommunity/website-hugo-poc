---
layout: "image"
title: "Burg..."
date: "2016-07-25T14:24:24"
picture: "ftfct01.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43961
- /detailsf857.html
imported:
- "2019"
_4images_image_id: "43961"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43961 -->
... liebevoll aufgebaut aus Fischertechnik
