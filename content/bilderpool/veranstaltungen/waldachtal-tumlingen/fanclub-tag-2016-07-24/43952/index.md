---
layout: "image"
title: "Gelenkbus mit allem"
date: "2016-07-24T20:10:12"
picture: "fct27.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43952
- /details6939.html
imported:
- "2019"
_4images_image_id: "43952"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43952 -->
Der Antrieb sitzt wie in echt, ganz hinten. Alle Türen werden elektrisch betätigt.
