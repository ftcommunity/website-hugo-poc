---
layout: "image"
title: "Rekordversuch: die Brücke"
date: "2016-07-24T20:10:12"
picture: "fct02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43927
- /details3f10.html
imported:
- "2019"
_4images_image_id: "43927"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43927 -->
Bei Tragseilbrücken kann man unten drunter viel Luft verbauen.
