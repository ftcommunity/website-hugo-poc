---
layout: "image"
title: "Schaufellader"
date: "2016-07-25T14:24:24"
picture: "ftfct03.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43963
- /details4227.html
imported:
- "2019"
_4images_image_id: "43963"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43963 -->
