---
layout: "image"
title: "Schweißroboter"
date: "2016-07-25T14:24:24"
picture: "ftfct07.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43967
- /detailsa96d-2.html
imported:
- "2019"
_4images_image_id: "43967"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43967 -->
