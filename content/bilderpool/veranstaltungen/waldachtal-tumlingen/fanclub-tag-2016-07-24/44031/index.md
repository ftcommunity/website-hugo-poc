---
layout: "image"
title: "Brücke8"
date: "2016-07-27T18:03:54"
picture: "Brcke08.jpg"
weight: "71"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "FT"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/44031
- /details8d8a-2.html
imported:
- "2019"
_4images_image_id: "44031"
_4images_cat_id: "3255"
_4images_user_id: "560"
_4images_image_date: "2016-07-27T18:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44031 -->
