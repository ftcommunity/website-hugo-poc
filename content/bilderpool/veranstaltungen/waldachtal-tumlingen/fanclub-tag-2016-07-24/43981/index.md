---
layout: "image"
title: "Portalkran"
date: "2016-07-25T14:24:24"
picture: "ftfct21.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43981
- /details4723.html
imported:
- "2019"
_4images_image_id: "43981"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43981 -->
