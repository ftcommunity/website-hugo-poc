---
layout: "image"
title: "Wenn ich nur wüsst, was drinnen ist"
date: "2016-07-24T20:10:12"
picture: "fct04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43929
- /details8e96.html
imported:
- "2019"
_4images_image_id: "43929"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43929 -->
Da drin soll angeblich Leere herrschen. Aber mindestens in den Drehgestellen müssen interessante Details versteckt sein.
