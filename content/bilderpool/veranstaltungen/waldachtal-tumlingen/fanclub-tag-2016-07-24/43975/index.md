---
layout: "image"
title: "Rekordversuch Brücke"
date: "2016-07-25T14:24:24"
picture: "ftfct15.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "david - patrick"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43975
- /details6343.html
imported:
- "2019"
_4images_image_id: "43975"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43975 -->
