---
layout: "image"
title: "schaut man hinreichend genau in die Details eines Riesenrads,..."
date: "2016-07-24T20:10:12"
picture: "fct25.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/43950
- /details9bc5-2.html
imported:
- "2019"
_4images_image_id: "43950"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43950 -->
... sieht man wieder eins. Das hat was von Rekursion oder den Apfelmännchen.
