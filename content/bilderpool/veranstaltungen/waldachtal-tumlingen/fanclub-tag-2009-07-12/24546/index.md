---
layout: "image"
title: "Industriemodell"
date: "2009-07-12T16:59:54"
picture: "fanclubtag10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24546
- /detailsaf79.html
imported:
- "2019"
_4images_image_id: "24546"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24546 -->
Alles mit viel Liebe zum Detail gebaut.
