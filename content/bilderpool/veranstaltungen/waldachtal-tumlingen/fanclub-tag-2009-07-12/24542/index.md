---
layout: "image"
title: "Turmdrehkran"
date: "2009-07-12T16:59:54"
picture: "fanclubtag06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24542
- /details5908.html
imported:
- "2019"
_4images_image_id: "24542"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24542 -->
Immer wieder schön.
