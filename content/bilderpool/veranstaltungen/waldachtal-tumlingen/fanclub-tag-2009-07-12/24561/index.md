---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag25.jpg"
weight: "25"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24561
- /details6f88.html
imported:
- "2019"
_4images_image_id: "24561"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24561 -->
Hier ohne so viel Gegenlicht aufgenommen.
