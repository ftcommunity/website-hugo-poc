---
layout: "image"
title: "Computing-Einführung"
date: "2009-07-12T17:00:17"
picture: "fanclubtag33.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24569
- /details4a59.html
imported:
- "2019"
_4images_image_id: "24569"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24569 -->
Neben dem normalen Beamer wurden auch von einem Overheadprojektor (so sah es jedenfalls aus)  Live-Bilder des Interface an die Wand geworfen.
