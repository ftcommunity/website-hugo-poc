---
layout: "image"
title: "Selbstbautaster"
date: "2009-07-12T16:59:54"
picture: "fanclubtag08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24544
- /detailsf1ae.html
imported:
- "2019"
_4images_image_id: "24544"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24544 -->
Hier ein Detailblick auf die Elektrik.
