---
layout: "image"
title: "Kirmesmodell"
date: "2009-07-12T16:59:53"
picture: "fanclubtag03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24539
- /details3b46.html
imported:
- "2019"
_4images_image_id: "24539"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24539 -->
Der MiniMot dreht die Sitze, das ganze wird von einem PowerMot mit Schnecke nochmal gedreht, bis den Leuten schlecht wird. ;-) Die Drehung am PowerMot wird mittels Impulstaster abgegriffen. Mit einem schön gemachten Programm wird ein realistischer Ablauf erreicht: Langsam anfahren, schneller werden, dann mehrmals um die zweite Achse drehen und wieder zurück (damit verheddern sich auch ohne Schleifringe die Kabel zum MiniMot nicht) und wieder zurückdrehen, langsamer werden und aus. Schön gebaut, relativ wenig Materialeinsatz - auch prima!
