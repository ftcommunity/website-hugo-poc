---
layout: "image"
title: "Waschanlage"
date: "2009-07-12T17:00:17"
picture: "fanclubtag21.jpg"
weight: "21"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24557
- /details5243.html
imported:
- "2019"
_4images_image_id: "24557"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24557 -->
Ein richtiges Industriemodell hinter Glas.
