---
layout: "image"
title: "Kugelbahn"
date: "2009-07-12T16:59:54"
picture: "fanclubtag05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24541
- /details8706.html
imported:
- "2019"
_4images_image_id: "24541"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24541 -->
Jeder durfte kurbeln, um den Tischtennisball abrollen zu lassen. Da stand fast immer jemand und drehte. :-)
