---
layout: "image"
title: "Taktstraße"
date: "2009-07-12T18:57:04"
picture: "tag2.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24571
- /details73a7-3.html
imported:
- "2019"
_4images_image_id: "24571"
_4images_cat_id: "1688"
_4images_user_id: "430"
_4images_image_date: "2009-07-12T18:57:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24571 -->
