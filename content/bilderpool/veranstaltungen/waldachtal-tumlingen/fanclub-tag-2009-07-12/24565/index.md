---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag29.jpg"
weight: "29"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24565
- /details2dad-2.html
imported:
- "2019"
_4images_image_id: "24565"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24565 -->
Hier der Wagen in Bewegung. Absolut rasant.
