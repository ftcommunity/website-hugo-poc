---
layout: "image"
title: "Dreizylinder-Luftmotor mit Generator"
date: "2009-07-12T17:00:16"
picture: "fanclubtag13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24549
- /detailse026.html
imported:
- "2019"
_4images_image_id: "24549"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24549 -->
Die Drei Zylinder links bilden einen Sternmotor, der die ganze Anordnung sehr flott antreibt. Unter dem Tisch steht ein "richtiger" Kompressor, der von Druckminderen in dem roten Gehäuse rechts von 7 auf 2 bar geregelt wird. Da geht's ab! ;-)
