---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag28.jpg"
weight: "28"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24564
- /detailsaff7.html
imported:
- "2019"
_4images_image_id: "24564"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24564 -->
Hier der Wagen im Stillstand. Er ist mit Stahlseilen und Karabinerhaken aufgehängt. Seht Euch die Stromschienen an!
