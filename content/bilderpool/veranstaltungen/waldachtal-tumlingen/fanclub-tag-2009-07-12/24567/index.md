---
layout: "image"
title: "Schätzfrage"
date: "2009-07-12T17:00:17"
picture: "fanclubtag31.jpg"
weight: "31"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24567
- /detailsa77d-2.html
imported:
- "2019"
_4images_image_id: "24567"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24567 -->
Wie viele Bausteine sind da drin? Erster Preis ist der Fußball rechts mit echten Unterschriften aller VFB-Fußballer (ich hoffe ich liege richtig), zweiter Preis ein neuer Bulldozer-Kasten, dritter Preis ein kleinerer Kasten. Man konnte vor Ort Zettel mit seiner Schätzung und Anschrift ausfüllen und einwerfen.
