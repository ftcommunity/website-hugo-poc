---
layout: "image"
title: "Kran mit Selbstbausteuerung"
date: "2009-07-12T16:59:54"
picture: "fanclubtag07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/24543
- /details2e32.html
imported:
- "2019"
_4images_image_id: "24543"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24543 -->
Dieser Junge hatte den Kran mit selbstgebauten Tastern sehr schön steuerbar gemacht.
