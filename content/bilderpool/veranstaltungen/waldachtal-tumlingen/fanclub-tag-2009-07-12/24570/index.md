---
layout: "image"
title: "Drehmaschine"
date: "2009-07-12T18:57:04"
picture: "tag1.jpg"
weight: "34"
konstrukteure: 
- "David van Krimpen"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/24570
- /details72e6.html
imported:
- "2019"
_4images_image_id: "24570"
_4images_cat_id: "1688"
_4images_user_id: "430"
_4images_image_date: "2009-07-12T18:57:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24570 -->
