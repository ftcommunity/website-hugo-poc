---
layout: "image"
title: "Dirks Kran"
date: "2010-07-04T21:52:43"
picture: "FanClubTag2010_04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27610
- /details68d5.html
imported:
- "2019"
_4images_image_id: "27610"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T21:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27610 -->
Wohl zum letzten Mal aufgebaut, Dirk hat am Ende einfach die Seile durchgeschnitten zum Abbauen...
