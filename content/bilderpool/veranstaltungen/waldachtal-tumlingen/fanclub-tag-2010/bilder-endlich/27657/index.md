---
layout: "image"
title: "FT Fan Tag 2010"
date: "2010-07-05T16:39:56"
picture: "ftfanclubtag39.jpg"
weight: "39"
konstrukteure: 
- "Raphael (fischercake)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27657
- /details6aa0.html
imported:
- "2019"
_4images_image_id: "27657"
_4images_cat_id: "1990"
_4images_user_id: "1162"
_4images_image_date: "2010-07-05T16:39:56"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27657 -->
Kran
