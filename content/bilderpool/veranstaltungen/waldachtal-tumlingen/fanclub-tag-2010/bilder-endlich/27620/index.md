---
layout: "image"
title: "FT Fan Tag 2010"
date: "2010-07-05T16:39:56"
picture: "ftfanclubtag02.jpg"
weight: "2"
konstrukteure: 
- "ft"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/27620
- /detailsd295.html
imported:
- "2019"
_4images_image_id: "27620"
_4images_cat_id: "1990"
_4images_user_id: "1162"
_4images_image_date: "2010-07-05T16:39:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27620 -->
Neuheiten 2010
