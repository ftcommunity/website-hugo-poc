---
layout: "comment"
hidden: true
title: "21015"
date: "2015-08-09T13:39:34"
uploadBy:
- "Svefisch"
license: "unknown"
imported:
- "2019"
---
Das wäre schön, wenn ich ergänzende Bilder hätte. Mir liegt leider nur das Original-Clubheft vor, auf dem u.a. die Gelenksteine erkennbar sind. Und ja, 31008 meine ich. Dieser hieß in älteren Teilelisten einfach nur Gelenkstein, das 45-er sollte das Teil nur präzisieren.
Wie genau die Verbindungen zwischen den Segmenten ausgeführt waren, kann ich leider auch nicht mehr sagen, und auch nicht auf dem Bild erkennen. Ich versuchte nur Hinweise für die präzisere Anpassung des neuen Modells an das Original zu geben, zumal Dirk auch sagte, dass das neue noch nicht fertig wäre.

Gruß 
Holger