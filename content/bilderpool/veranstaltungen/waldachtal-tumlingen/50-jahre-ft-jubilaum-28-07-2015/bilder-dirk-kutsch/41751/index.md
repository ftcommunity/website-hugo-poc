---
layout: "image"
title: "Rückfahrt"
date: "2015-08-06T14:28:36"
picture: "dirk25.jpg"
weight: "25"
konstrukteure: 
- "den Namen hätte ich gerne gewusst."
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41751
- /detailsdfe3.html
imported:
- "2019"
_4images_image_id: "41751"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41751 -->
Man wollte uns nicht wieder nach Hause lassen und im Schwarzwald bedeutet das laaange Umwege.
