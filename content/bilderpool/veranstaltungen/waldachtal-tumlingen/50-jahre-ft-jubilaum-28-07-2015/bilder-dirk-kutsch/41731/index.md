---
layout: "image"
title: "Bauteilübersicht"
date: "2015-08-06T14:28:36"
picture: "dirk05.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41731
- /detailsbaf6.html
imported:
- "2019"
_4images_image_id: "41731"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41731 -->
Das sind wohl die aktuellen Bauteile die derzeit produziert werden?!? Die Tafel hing in Salzstetten an der Wand.
