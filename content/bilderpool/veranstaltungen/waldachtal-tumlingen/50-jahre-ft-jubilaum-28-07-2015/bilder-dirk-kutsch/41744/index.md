---
layout: "image"
title: "Schaufelradbagger 289"
date: "2015-08-06T14:28:36"
picture: "dirk18.jpg"
weight: "18"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/41744
- /details5322.html
imported:
- "2019"
_4images_image_id: "41744"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41744 -->
