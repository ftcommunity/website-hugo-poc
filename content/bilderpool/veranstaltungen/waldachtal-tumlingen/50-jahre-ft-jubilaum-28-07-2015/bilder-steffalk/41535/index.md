---
layout: "image"
title: "Landwirtschaft"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk052.jpg"
weight: "52"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41535
- /details7f90.html
imported:
- "2019"
_4images_image_id: "41535"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41535 -->
