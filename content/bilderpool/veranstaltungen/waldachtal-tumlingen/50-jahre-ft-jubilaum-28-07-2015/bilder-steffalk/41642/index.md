---
layout: "image"
title: "Industriemodell"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk159.jpg"
weight: "159"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41642
- /details5a5a-3.html
imported:
- "2019"
_4images_image_id: "41642"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "159"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41642 -->
Per Elektromagnet können vom Wagen Teile angehoben und aufs Förderband gelegt werden.
