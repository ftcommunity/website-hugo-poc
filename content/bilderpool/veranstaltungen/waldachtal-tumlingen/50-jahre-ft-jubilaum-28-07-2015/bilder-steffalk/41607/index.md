---
layout: "image"
title: "Futuristischer Renner"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk124.jpg"
weight: "124"
konstrukteure: 
- "Patrick Holtz"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41607
- /details7680-2.html
imported:
- "2019"
_4images_image_id: "41607"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41607 -->
... mit Lenkung, Antrieb mittels XM-Motor... und Flügeltüren!
