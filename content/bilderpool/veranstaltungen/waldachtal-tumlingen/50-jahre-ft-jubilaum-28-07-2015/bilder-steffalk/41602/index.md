---
layout: "image"
title: "Roboterfahrzeug"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk119.jpg"
weight: "119"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41602
- /details6bf7.html
imported:
- "2019"
_4images_image_id: "41602"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "119"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41602 -->
Sehr robust konstruiert und mit Webcam ausgestattet.
