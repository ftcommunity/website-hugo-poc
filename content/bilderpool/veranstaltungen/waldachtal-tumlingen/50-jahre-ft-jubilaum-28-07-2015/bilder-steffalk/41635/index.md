---
layout: "image"
title: "Pistenraupen"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk152.jpg"
weight: "152"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41635
- /details300b-2.html
imported:
- "2019"
_4images_image_id: "41635"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41635 -->
Siehe ft:pedia 2015-1 für eine ausführliche Beschreibung. Spitzenmodelle!
