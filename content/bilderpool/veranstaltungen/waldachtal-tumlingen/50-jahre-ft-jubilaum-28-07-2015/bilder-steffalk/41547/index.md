---
layout: "image"
title: "Flipper"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk064.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41547
- /details4314.html
imported:
- "2019"
_4images_image_id: "41547"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41547 -->
Blick auf die Technik auf der Unterseite
