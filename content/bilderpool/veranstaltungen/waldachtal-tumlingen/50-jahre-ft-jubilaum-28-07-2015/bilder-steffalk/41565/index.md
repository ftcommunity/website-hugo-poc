---
layout: "image"
title: "Schulprojekt"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk082.jpg"
weight: "82"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41565
- /details5a9c-2.html
imported:
- "2019"
_4images_image_id: "41565"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41565 -->
Beschreibung
