---
layout: "image"
title: "Landwirtschaftliche Fahrzeuge und Maschinen"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk023.jpg"
weight: "23"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41506
- /details1cc7-2.html
imported:
- "2019"
_4images_image_id: "41506"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41506 -->
