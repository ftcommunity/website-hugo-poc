---
layout: "image"
title: "Haralds Bagger"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk163.jpg"
weight: "163"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41646
- /details64b9.html
imported:
- "2019"
_4images_image_id: "41646"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "163"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41646 -->
