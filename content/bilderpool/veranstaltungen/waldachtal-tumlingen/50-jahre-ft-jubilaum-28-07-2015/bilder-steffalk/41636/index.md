---
layout: "image"
title: "Roboter mit Pixy-Kamera, weitere Roboterfahrzeuge"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk153.jpg"
weight: "153"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41636
- /detailsfa72.html
imported:
- "2019"
_4images_image_id: "41636"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "153"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41636 -->
Für das Modell mit der Pixy siehe ft:pedia 2014-4. Das Modell folgt u.a. einem vor es hingehaltenen farbigen Ball.
