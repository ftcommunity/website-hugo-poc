---
layout: "image"
title: "Bauernhof"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk148.jpg"
weight: "148"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41631
- /details4930.html
imported:
- "2019"
_4images_image_id: "41631"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "148"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41631 -->
Eine wundervolle Idee, das mal mit fischertechnik zu bauen!
