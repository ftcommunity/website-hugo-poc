---
layout: "image"
title: "Ventilatoren"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk138.jpg"
weight: "138"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41621
- /details693a.html
imported:
- "2019"
_4images_image_id: "41621"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "138"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41621 -->
Der handbetriebene im Turm rechts hat eine enorme Übersetzung von der Kurbel unten bis zu den Propellerflügeln oben.

Obendrüber sieht man Ralfs Laufmodelle.
