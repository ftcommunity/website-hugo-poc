---
layout: "image"
title: "Schachspiel"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk137.jpg"
weight: "137"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41620
- /detailsff7e-2.html
imported:
- "2019"
_4images_image_id: "41620"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "137"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41620 -->
Eine herrlich kreative Gemeinschaftsarbeit der Geerkens.
