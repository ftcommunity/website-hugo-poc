---
layout: "image"
title: "Industriemodell"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk155.jpg"
weight: "155"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41638
- /details6530-2.html
imported:
- "2019"
_4images_image_id: "41638"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "155"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41638 -->
Jede Menge echter Industrie-Aktoren.
