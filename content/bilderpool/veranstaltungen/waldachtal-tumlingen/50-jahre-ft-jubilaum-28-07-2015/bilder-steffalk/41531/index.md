---
layout: "image"
title: "Münzgeber"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk048.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41531
- /details8101.html
imported:
- "2019"
_4images_image_id: "41531"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41531 -->
