---
layout: "image"
title: "Plotterkonstruktion"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk111.jpg"
weight: "111"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41594
- /details1945.html
imported:
- "2019"
_4images_image_id: "41594"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "111"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41594 -->
Die Erbauerin und der Erbauer dieses Plotters wollen eine Genauigkeit von 0,025 mm erreichen, wenn sie fertig sind!
