---
layout: "image"
title: "Belastungstest (4)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk035.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41518
- /details5cb7.html
imported:
- "2019"
_4images_image_id: "41518"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41518 -->
... immer mehr von den Tastern drücken. An jedem Taster hängt eine weitere LED:
