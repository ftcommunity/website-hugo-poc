---
layout: "comment"
hidden: true
title: "20948"
date: "2015-07-30T20:13:22"
uploadBy:
- "Bennik"
license: "unknown"
imported:
- "2019"
---
Ich habe die Erbauer mal gefragt:
Der Raspberry Pi ist nur für den Video Uplink. Die App wird direkt über Bluetooth mit dem TXC verbunden und zwischen Raspberry Pi und TX Controller besteht keine Verbindung.

@H.A.R.R.Y. Nein, der TXC kann nicht als Slave arbeiten.