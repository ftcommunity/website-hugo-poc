---
layout: "image"
title: "Kirmesmodell, Roboterarm"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk125.jpg"
weight: "125"
konstrukteure: 
- "Patrick Holtz"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41608
- /details0943.html
imported:
- "2019"
_4images_image_id: "41608"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "125"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41608 -->
