---
layout: "image"
title: "Hebekunst (3)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk040.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41523
- /details80f1-2.html
imported:
- "2019"
_4images_image_id: "41523"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41523 -->
Man sieht den Pneumatikzylinder für die Hebelmechanik.
