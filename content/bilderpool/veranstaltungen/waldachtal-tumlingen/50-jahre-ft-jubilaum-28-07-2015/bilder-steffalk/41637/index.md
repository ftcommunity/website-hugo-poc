---
layout: "image"
title: "Industriemodell"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk154.jpg"
weight: "154"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41637
- /details6b58-2.html
imported:
- "2019"
_4images_image_id: "41637"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "154"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41637 -->
... aber was für eines! Hier wird echte Industriesteuerung und echte Industrie-Pneumatik verwendet. Sehr flott, das alles!
