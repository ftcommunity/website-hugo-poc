---
layout: "image"
title: "Schwerlastkran, Kardanwelle"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk151.jpg"
weight: "151"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41634
- /details4905.html
imported:
- "2019"
_4images_image_id: "41634"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "151"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41634 -->
