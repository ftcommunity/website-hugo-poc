---
layout: "image"
title: "Baufahrzeuge"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk132.jpg"
weight: "132"
konstrukteure: 
- "David Holtz (davidrpf)"
- "Patrick Holtz"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41615
- /details932b.html
imported:
- "2019"
_4images_image_id: "41615"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41615 -->
Der Bagger verfügt über Allradantrieb und pneumatische Knicklenkung
