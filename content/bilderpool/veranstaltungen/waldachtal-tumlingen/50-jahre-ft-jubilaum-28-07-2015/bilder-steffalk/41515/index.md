---
layout: "image"
title: "Belastungstest (1)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk032.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41515
- /details4670-3.html
imported:
- "2019"
_4images_image_id: "41515"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41515 -->
Der graue Motor treibt den schwarzen als Generator an. Die Pfeil-Tasten links oben dienen dazu, je eine LED aus dem LED-Balken in der Mitte mehr oder weniger als Last zuzuschalten. Die Messgeräte geben ein Maß für die Last.
