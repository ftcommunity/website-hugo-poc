---
layout: "image"
title: "Brückenbau"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk108.jpg"
weight: "108"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41591
- /detailsd50a.html
imported:
- "2019"
_4images_image_id: "41591"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "108"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41591 -->
In einigen Details weicht es vom "Foto-Modell" ;-) ab (man betrachte die Verstrebungen mal genau). Ich weiß nicht, ob das Modell wirklich noch existierte oder tatsächlich nochmal gebaut wurde. Vielleicht kann da jemand noch etwas klären. Jedenfalls waren im Antrieb über diesem Kettenzug sowohl ein großer ft-Drehkranz als auch ein schwarzer S-Motor verbaut, die es beide 1970 ja noch nicht gab.
