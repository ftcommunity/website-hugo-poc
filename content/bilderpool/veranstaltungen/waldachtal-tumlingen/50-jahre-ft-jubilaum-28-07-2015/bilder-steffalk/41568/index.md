---
layout: "image"
title: "Severins 3D-Drucker"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk085.jpg"
weight: "85"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41568
- /details8dc5.html
imported:
- "2019"
_4images_image_id: "41568"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41568 -->
Druckt gerade eine Blumenvase!
