---
layout: "image"
title: "Baukran"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41484
- /detailsa3b6.html
imported:
- "2019"
_4images_image_id: "41484"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41484 -->
