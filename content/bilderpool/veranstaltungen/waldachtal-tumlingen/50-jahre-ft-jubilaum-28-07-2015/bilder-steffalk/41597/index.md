---
layout: "image"
title: "Traktor mit Baumstammgreifer und Anhänger"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk114.jpg"
weight: "114"
konstrukteure: 
- "Tobias Endlich"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41597
- /details7e84.html
imported:
- "2019"
_4images_image_id: "41597"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "114"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41597 -->
