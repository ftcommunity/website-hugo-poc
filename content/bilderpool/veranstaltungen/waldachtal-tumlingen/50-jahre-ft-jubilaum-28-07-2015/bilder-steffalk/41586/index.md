---
layout: "image"
title: "Roboter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk103.jpg"
weight: "103"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41586
- /details226b.html
imported:
- "2019"
_4images_image_id: "41586"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41586 -->
Mit einem TXT-Interface im Herzen.
