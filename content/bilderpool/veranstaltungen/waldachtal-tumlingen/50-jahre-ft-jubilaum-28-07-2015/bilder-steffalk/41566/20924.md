---
layout: "comment"
hidden: true
title: "20924"
date: "2015-07-29T12:57:47"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Rob sagt es. Im Laufe des Tages verrutschte die Nabenkonstruktion so stark, dass das Rad oben über 10cm zur Seite kippte. Dadurch war Drehen natürlich nicht mehr möglich.
Der Aufbau der Nabe war einfach Murks, weshalb das Riesenrad jetzt auch den Weg aller ft-Modelle gegangen ist: Zerlegt und Wegsortiert.
Grüße,
Martin