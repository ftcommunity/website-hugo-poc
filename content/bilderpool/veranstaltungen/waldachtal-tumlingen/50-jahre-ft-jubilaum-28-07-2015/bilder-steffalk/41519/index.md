---
layout: "image"
title: "Belastungstest (5)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk036.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41519
- /detailsd37e.html
imported:
- "2019"
_4images_image_id: "41519"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41519 -->
Die Taster werden von den von rechts verschobenen U-Trägern nach und nach zugeschaltet.
