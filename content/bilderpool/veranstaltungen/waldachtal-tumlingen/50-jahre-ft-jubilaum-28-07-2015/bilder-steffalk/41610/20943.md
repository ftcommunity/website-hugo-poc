---
layout: "comment"
hidden: true
title: "20943"
date: "2015-07-30T10:38:58"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Wenn ich mich richtig erinnere war ein einzelner Encoder Motor für die 2 Achse (per Schnecke auf Drehkranz) zu schwach und es wurde ein roter Power-Motor parallel  geschaltet. 

Der Encoder wird mit 173 U/min im Abtrieb etwas gebremst da der rote PM nur 115 U/min hat. Insgesamt aber bessere als nur ein Motor allein :) 

2 Encoder sind einfach zu teuer...