---
layout: "image"
title: "Roboterarm und Bearbeitungsstation"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk127.jpg"
weight: "127"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41610
- /detailsbdac.html
imported:
- "2019"
_4images_image_id: "41610"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "127"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41610 -->
