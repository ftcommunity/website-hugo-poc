---
layout: "image"
title: "Flipper, Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk066.jpg"
weight: "66"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41549
- /detailsd66e.html
imported:
- "2019"
_4images_image_id: "41549"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41549 -->
