---
layout: "image"
title: "Kirmes-Tower"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk144.jpg"
weight: "144"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41627
- /details53ab.html
imported:
- "2019"
_4images_image_id: "41627"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "144"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41627 -->
Vom Sohnemann der Dame rechts selbst erdacht und zum Funktionieren gebracht. Da der Rahmen mit den Leuten drauf in der Mitte einen Steg hat, an dem in der Mitte das Tragseil hängt, ist eine weitere Verstrebung in Querrichtung nicht möglich. Aber viele fragten danach und merkten dann erst, dass das ja gar nicht gehen würde ;-)
