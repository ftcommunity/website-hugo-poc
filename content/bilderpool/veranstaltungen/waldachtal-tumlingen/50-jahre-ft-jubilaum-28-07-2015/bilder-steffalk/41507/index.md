---
layout: "image"
title: "Landwirtschaftliche Fahrzeuge und Maschinen"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk024.jpg"
weight: "24"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41507
- /detailsc37a.html
imported:
- "2019"
_4images_image_id: "41507"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41507 -->
