---
layout: "image"
title: "Autoscooter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk141.jpg"
weight: "141"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41624
- /detailse4a4-4.html
imported:
- "2019"
_4images_image_id: "41624"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41624 -->
Ein sehr gut funktionierendes Modell.
