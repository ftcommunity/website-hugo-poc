---
layout: "image"
title: "Severins 3D-Drucker"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk087.jpg"
weight: "87"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41570
- /detailsbdf5-2.html
imported:
- "2019"
_4images_image_id: "41570"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41570 -->
Seitlicher Einblick während des Drucks einer Blumenvase
