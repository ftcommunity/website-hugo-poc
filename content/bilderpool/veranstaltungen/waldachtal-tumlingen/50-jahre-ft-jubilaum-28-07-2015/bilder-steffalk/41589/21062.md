---
layout: "comment"
hidden: true
title: "21062"
date: "2015-09-11T17:56:26"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo,

Eine Tibo Bausatz + Fischertechnik ist sehr gut kombinierbar.
Am FT-Fanclubtag-Juli 2015 gab es schon einige Beispielen.

Mehr experimentier-möglichkeiten gibt es zb für :

- Mini Bots
- TXT Discovery Set
- Solar Power-Baukasten
- Roboterspinne

Viel mehr Info + mögklichkeiten gibt es bei :

[1] http://variobot.com/blog
[2] http://www.variobot.com/blog
[3] http://eepurl.com/biscM5
[4] http://www.facebook.com/variobot
[5] http://variobot.com
[6] http://twitter.com/variobot
[7] http://facebook.com/variobot
[8] http://youtube.com/c/variobot
[9] http://linkedin.com/in/tinowerner
[10] https://plus.google.com/+variobot

VARIOBOT ®
Mannheimer Straße 6
76344 Leopoldshafen, Germany

+49 151 50 60 21 68
tino.werner@variobot.com

Gruss,

Peter
Poederoyen NL