---
layout: "image"
title: "Hebekunst (2)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk039.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41522
- /detailsb956.html
imported:
- "2019"
_4images_image_id: "41522"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41522 -->
Hier der Antrieb.
