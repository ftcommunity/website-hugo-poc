---
layout: "image"
title: "Hohe Kugelbahn"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk160.jpg"
weight: "160"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41643
- /details5e0f.html
imported:
- "2019"
_4images_image_id: "41643"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "160"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41643 -->
