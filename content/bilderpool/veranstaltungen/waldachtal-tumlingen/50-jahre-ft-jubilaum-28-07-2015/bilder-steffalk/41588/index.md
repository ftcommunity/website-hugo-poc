---
layout: "image"
title: "Spinne"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk105.jpg"
weight: "105"
konstrukteure: 
- "Tino Werner"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41588
- /details09a2-2.html
imported:
- "2019"
_4images_image_id: "41588"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41588 -->
Das Modell ist Infrarot-ferngesteuert und bewegte sich perfekt in die jeweils gewünschte Richtung.
