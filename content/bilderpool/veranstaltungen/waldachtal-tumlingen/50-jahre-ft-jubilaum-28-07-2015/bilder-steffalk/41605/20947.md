---
layout: "comment"
hidden: true
title: "20947"
date: "2015-07-30T20:09:18"
uploadBy:
- "Bennik"
license: "unknown"
imported:
- "2019"
---
Naja, die Farben wurden nicht automatisch erkannt. Da hat es uns leider an Sensorik gefehlt um die verschiedenen Eigenschaften (Farbe, Oberfläche) zu erkennen. Man musste dem Programm sagen, welche Platte jetzt dran kommt. Aber das ist beim original genauso ;)

LG
Bennik