---
layout: "image"
title: "Holz-Umschlagsplatz"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk043.jpg"
weight: "43"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41526
- /detailsa39f.html
imported:
- "2019"
_4images_image_id: "41526"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41526 -->
Ein Portalkran, der tatsächlich Holz trug.
