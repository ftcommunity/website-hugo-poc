---
layout: "image"
title: "Severins 3D-Drucker"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk086.jpg"
weight: "86"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41569
- /details6567-2.html
imported:
- "2019"
_4images_image_id: "41569"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41569 -->
fischertechnik und jede Menge Fremdkomponenten erfolgreich kombiniert. Die Seilzüge wurden pfiffig dadurch gespannt, dass sie am Ende zu einer Schlaufe verknotet wurden, in die Kabelbinder (Bildmitte, auf einer Metallachse) eingeflochten sind. Das Zuziehen der Kabelbinder spannt das Seil hinreichend straff für eine genaue Arbeitsweise.
