---
layout: "image"
title: "Seilzug-Plotter"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk164.jpg"
weight: "164"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41647
- /details3098.html
imported:
- "2019"
_4images_image_id: "41647"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "164"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41647 -->
Die Maschine berechnet die benötigten Seil-Längen, um mit dem Stift eine gewünschte Position auf dem senkrecht hängenden Papier zu erreichen.
