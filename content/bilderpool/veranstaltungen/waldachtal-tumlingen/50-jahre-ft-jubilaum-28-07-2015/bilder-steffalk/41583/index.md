---
layout: "image"
title: "Clubhefte des Niederländischen fischertechnik-Clubs"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk100.jpg"
weight: "100"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41583
- /details7514.html
imported:
- "2019"
_4images_image_id: "41583"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41583 -->
Das rechts oben sind übrigens blaue U-Träger 150.
