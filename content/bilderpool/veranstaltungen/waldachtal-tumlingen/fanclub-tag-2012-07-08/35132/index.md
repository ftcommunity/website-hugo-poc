---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35132
- /detailsed09.html
imported:
- "2019"
_4images_image_id: "35132"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35132 -->
Vorstellung der neuen Kästen
