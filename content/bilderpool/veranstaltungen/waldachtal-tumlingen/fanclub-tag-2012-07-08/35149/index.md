---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix42.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35149
- /detailsd38c.html
imported:
- "2019"
_4images_image_id: "35149"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35149 -->
Bankautomat
