---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35130
- /detailsed32.html
imported:
- "2019"
_4images_image_id: "35130"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35130 -->
Vorstellung der neuen Kästen
