---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix38.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35145
- /details6198.html
imported:
- "2019"
_4images_image_id: "35145"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35145 -->
Schokoladenautomat
