---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35125
- /detailsdc3b.html
imported:
- "2019"
_4images_image_id: "35125"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35125 -->
Vorstellung der neuen Kästen
