---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix10.jpg"
weight: "10"
konstrukteure: 
- "Elias Martin"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35117
- /detailsc73f.html
imported:
- "2019"
_4images_image_id: "35117"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35117 -->
CNC-Fräse
