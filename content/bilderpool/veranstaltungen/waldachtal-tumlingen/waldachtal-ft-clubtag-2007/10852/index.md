---
layout: "image"
title: "fischer-Tip"
date: "2007-06-10T21:12:02"
picture: "ft-Clubtag_-_46.jpg"
weight: "11"
konstrukteure: 
- "Artur Fischer"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10852
- /details607b.html
imported:
- "2019"
_4images_image_id: "10852"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10852 -->
