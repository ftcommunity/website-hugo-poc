---
layout: "image"
title: "Clubtag"
date: "2007-06-10T21:01:38"
picture: "ft-Clubtag_-_09.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Markus Mack (MarMac)"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10816
- /detailsb119.html
imported:
- "2019"
_4images_image_id: "10816"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:01:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10816 -->
Durchs Bild rennend: heiko
