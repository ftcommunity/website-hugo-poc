---
layout: "image"
title: "Spielecke"
date: "2007-06-10T21:02:55"
picture: "ft-Clubtag_-_11.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10818
- /details2919.html
imported:
- "2019"
_4images_image_id: "10818"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10818 -->
