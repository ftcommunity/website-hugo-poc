---
layout: "image"
title: "Brücke Totale"
date: "2007-06-10T21:07:13"
picture: "ft-Clubtag_-_30.jpg"
weight: "4"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10836
- /details1e95.html
imported:
- "2019"
_4images_image_id: "10836"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:07:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10836 -->
