---
layout: "image"
title: "Tiefbohrgerät"
date: "2007-06-10T21:08:55"
picture: "ft-Clubtag_-_36.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10842
- /details6f8d-2.html
imported:
- "2019"
_4images_image_id: "10842"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:08:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10842 -->
