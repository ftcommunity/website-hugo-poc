---
layout: "image"
title: "Tiefbohrgerät"
date: "2007-06-10T21:08:41"
picture: "ft-Clubtag_-_35.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10841
- /detailsaef4.html
imported:
- "2019"
_4images_image_id: "10841"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:08:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10841 -->
