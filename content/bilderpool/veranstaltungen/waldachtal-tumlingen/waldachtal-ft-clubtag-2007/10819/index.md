---
layout: "image"
title: "Spielecke"
date: "2007-06-10T21:03:11"
picture: "ft-Clubtag_-_12.jpg"
weight: "8"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10819
- /details0937.html
imported:
- "2019"
_4images_image_id: "10819"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:03:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10819 -->
