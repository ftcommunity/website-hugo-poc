---
layout: "image"
title: "fischer-Tip (Speisestärke)"
date: "2007-06-10T21:12:17"
picture: "ft-Clubtag_-_47.jpg"
weight: "12"
konstrukteure: 
- "Artur Fischer"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10853
- /detailsc51f.html
imported:
- "2019"
_4images_image_id: "10853"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:12:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10853 -->
