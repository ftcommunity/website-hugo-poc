---
layout: "image"
title: "Werkstatt"
date: "2007-06-10T21:03:22"
picture: "ft-Clubtag_-_13.jpg"
weight: "1"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10820
- /detailse873.html
imported:
- "2019"
_4images_image_id: "10820"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:03:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10820 -->
