---
layout: "image"
title: "Robo Explorer"
date: "2007-06-10T21:05:32"
picture: "ft-Clubtag_-_22.jpg"
weight: "10"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/10829
- /details3ea8.html
imported:
- "2019"
_4images_image_id: "10829"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:05:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10829 -->
