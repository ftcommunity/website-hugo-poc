---
layout: "image"
title: "Kugelbeschleuniger (?)"
date: "2014-07-30T10:52:23"
picture: "DSC00530_bearb.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39105
- /details623c.html
imported:
- "2019"
_4images_image_id: "39105"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-30T10:52:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39105 -->
