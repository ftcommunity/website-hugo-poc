---
layout: "image"
title: "Kugelbahn"
date: "2014-07-30T10:52:23"
picture: "DSC00521_bearb.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/39102
- /details5b4c.html
imported:
- "2019"
_4images_image_id: "39102"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-30T10:52:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39102 -->
