---
layout: "image"
title: "Vor dem Zulieferer"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung045.jpg"
weight: "22"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6504
- /details21e8.html
imported:
- "2019"
_4images_image_id: "6504"
_4images_cat_id: "355"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6504 -->
Wunderschöne Landschaft da oben, und obendrein hatten wir Prachtwetter.
