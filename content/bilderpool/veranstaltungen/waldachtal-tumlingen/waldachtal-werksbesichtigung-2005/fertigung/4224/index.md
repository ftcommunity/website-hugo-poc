---
layout: "image"
title: "Industrieroboter in der Spritz-Gießerei"
date: "2005-05-28T12:35:58"
picture: "SANY1390.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4224
- /details0650.html
imported:
- "2019"
_4images_image_id: "4224"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T12:35:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4224 -->
