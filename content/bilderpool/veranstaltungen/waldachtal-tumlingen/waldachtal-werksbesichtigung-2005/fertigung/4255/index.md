---
layout: "image"
title: "Maschinen in der Spritz-Gießerei"
date: "2005-05-28T22:47:19"
picture: "SANY001032.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4255
- /details1477-2.html
imported:
- "2019"
_4images_image_id: "4255"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:47:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4255 -->
