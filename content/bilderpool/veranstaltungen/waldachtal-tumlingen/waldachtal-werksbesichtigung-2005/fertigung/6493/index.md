---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung034.jpg"
weight: "46"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6493
- /details2acb.html
imported:
- "2019"
_4images_image_id: "6493"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6493 -->
