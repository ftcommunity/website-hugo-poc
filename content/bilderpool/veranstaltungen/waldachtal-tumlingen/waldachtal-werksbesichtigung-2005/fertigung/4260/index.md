---
layout: "image"
title: "Maschinen in der Spritz-Gießerei"
date: "2005-05-28T22:53:18"
picture: "SANY001037.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4260
- /details5f09-2.html
imported:
- "2019"
_4images_image_id: "4260"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:53:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4260 -->
