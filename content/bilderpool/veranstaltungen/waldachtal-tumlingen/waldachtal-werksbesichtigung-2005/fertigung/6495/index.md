---
layout: "image"
title: "Fertigungsroboter"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung036.jpg"
weight: "48"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6495
- /details659f.html
imported:
- "2019"
_4images_image_id: "6495"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6495 -->
Dieser Roboter arbeitete hinter einem Sicherheitskäfig. Es war beeindruckend, wie schnell und gleichzeitig präzise die Maschine ihren Greifer positionieren konnte (auch wenn dieser Roboter nicht mit der Produktion von fischertechnik beschäftigt war).
