---
layout: "image"
title: "Industrieroboter in der Spritz-Gießerei"
date: "2005-05-28T12:30:10"
picture: "SANY01310.jpg"
weight: "2"
konstrukteure: 
- "---"
fotografen:
- "---"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4220
- /details056f.html
imported:
- "2019"
_4images_image_id: "4220"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T12:30:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4220 -->
