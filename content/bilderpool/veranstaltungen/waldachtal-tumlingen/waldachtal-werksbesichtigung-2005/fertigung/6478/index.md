---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung019.jpg"
weight: "31"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6478
- /details1f0b.html
imported:
- "2019"
_4images_image_id: "6478"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6478 -->
Spritzgussmaschinen...
