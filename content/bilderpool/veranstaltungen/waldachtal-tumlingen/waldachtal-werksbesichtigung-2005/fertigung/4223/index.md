---
layout: "image"
title: "Industrieroboter in der Spritz-Gießerei"
date: "2005-05-28T12:35:58"
picture: "SANY01370.jpg"
weight: "5"
konstrukteure: 
- "---"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4223
- /details9720.html
imported:
- "2019"
_4images_image_id: "4223"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T12:35:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4223 -->
