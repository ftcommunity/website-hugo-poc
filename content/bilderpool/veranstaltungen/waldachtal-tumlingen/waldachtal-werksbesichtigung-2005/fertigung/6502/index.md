---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung043.jpg"
weight: "55"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6502
- /details2a41-2.html
imported:
- "2019"
_4images_image_id: "6502"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6502 -->
Der Herr im brauen Hemd im Vordergrund ist Herr Wolfarth von fischertechnik.
