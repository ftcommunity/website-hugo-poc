---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung031.jpg"
weight: "43"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6490
- /details0e63.html
imported:
- "2019"
_4images_image_id: "6490"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6490 -->
Strebenfertigung
