---
layout: "image"
title: "Maschinen in der Spritz-Gießerei"
date: "2005-05-28T22:45:15"
picture: "SANY001029.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4253
- /details332e.html
imported:
- "2019"
_4images_image_id: "4253"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:45:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4253 -->
