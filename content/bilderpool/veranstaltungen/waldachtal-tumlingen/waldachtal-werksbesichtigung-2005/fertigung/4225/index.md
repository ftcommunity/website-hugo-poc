---
layout: "image"
title: "Industrieroboter in der Spritz-Gießerei"
date: "2005-05-28T12:35:58"
picture: "SANY01400.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4225
- /detailsc70e-2.html
imported:
- "2019"
_4images_image_id: "4225"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T12:35:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4225 -->
