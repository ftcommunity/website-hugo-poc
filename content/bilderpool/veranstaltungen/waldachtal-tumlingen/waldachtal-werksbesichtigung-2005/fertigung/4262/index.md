---
layout: "image"
title: "Gesammtansicht Spritz-Gießerei"
date: "2005-05-28T22:55:39"
picture: "SANY001041.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4262
- /detailsa489.html
imported:
- "2019"
_4images_image_id: "4262"
_4images_cat_id: "594"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:55:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4262 -->
