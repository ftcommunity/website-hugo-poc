---
layout: "image"
title: "Lehrsaal Wandkästen"
date: "2005-05-28T13:34:33"
picture: "SANY001006.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4233
- /detailsba7a-2.html
imported:
- "2019"
_4images_image_id: "4233"
_4images_cat_id: "355"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T13:34:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4233 -->
