---
layout: "image"
title: "Lehrsaal Wandkästen"
date: "2005-05-28T13:32:43"
picture: "SANY0001000.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4226
- /detailsa40f.html
imported:
- "2019"
_4images_image_id: "4226"
_4images_cat_id: "355"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T13:32:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4226 -->
