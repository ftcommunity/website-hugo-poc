---
layout: "image"
title: "Im Artur Fischer-Museum"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung091.jpg"
weight: "61"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6550
- /detailsce27.html
imported:
- "2019"
_4images_image_id: "6550"
_4images_cat_id: "595"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6550 -->
