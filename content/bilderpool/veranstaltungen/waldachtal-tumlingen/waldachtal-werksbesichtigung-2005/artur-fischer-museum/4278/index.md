---
layout: "image"
title: "Besuch im Artur Fischer Museum"
date: "2005-05-29T11:03:09"
picture: "SANY001058.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4278
- /details14a1-2.html
imported:
- "2019"
_4images_image_id: "4278"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-29T11:03:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4278 -->
