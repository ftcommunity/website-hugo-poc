---
layout: "image"
title: "Besuch im Artur Fischer Museum"
date: "2005-05-29T10:34:59"
picture: "SANY001044.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4265
- /detailsf643.html
imported:
- "2019"
_4images_image_id: "4265"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-29T10:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4265 -->
