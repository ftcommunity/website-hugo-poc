---
layout: "image"
title: "Besuch im Artur Fischer Museum"
date: "2005-05-29T10:34:59"
picture: "SANY001053.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "mari"
license: "unknown"
legacy_id:
- /php/details/4273
- /details1e48.html
imported:
- "2019"
_4images_image_id: "4273"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-29T10:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4273 -->
