---
layout: "image"
title: "Schaukasten in der Nähe des Museums"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung104.jpg"
weight: "21"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6563
- /detailsf557-2.html
imported:
- "2019"
_4images_image_id: "6563"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "104"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6563 -->
