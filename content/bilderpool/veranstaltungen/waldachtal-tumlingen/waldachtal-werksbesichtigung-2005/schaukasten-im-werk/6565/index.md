---
layout: "image"
title: "Schaukasten in der Nähe des Museums"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung106.jpg"
weight: "23"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6565
- /details3829.html
imported:
- "2019"
_4images_image_id: "6565"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "106"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6565 -->
