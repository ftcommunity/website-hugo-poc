---
layout: "image"
title: "Schautafeln im Empfangsraum"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung009.jpg"
weight: "9"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/6468
- /details6398.html
imported:
- "2019"
_4images_image_id: "6468"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6468 -->
