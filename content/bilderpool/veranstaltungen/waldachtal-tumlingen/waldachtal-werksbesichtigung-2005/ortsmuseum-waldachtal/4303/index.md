---
layout: "image"
title: "Fischer C-Box im Ortsmuseum Waldachtal"
date: "2005-05-30T20:17:49"
picture: "Fischer_C-Box.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- /php/details/4303
- /details9cce.html
imported:
- "2019"
_4images_image_id: "4303"
_4images_cat_id: "596"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4303 -->
