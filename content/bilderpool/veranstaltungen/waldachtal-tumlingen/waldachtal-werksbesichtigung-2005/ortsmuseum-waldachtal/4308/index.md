---
layout: "image"
title: "fischertechnik im Ortsmuseum Waldachtal"
date: "2005-05-30T20:17:49"
picture: "fischertechnik_Schulbaukasten_e4.jpg"
weight: "7"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "-?-"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- /php/details/4308
- /details7ed4.html
imported:
- "2019"
_4images_image_id: "4308"
_4images_cat_id: "596"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4308 -->
