---
layout: "comment"
hidden: true
title: "580"
date: "2005-06-12T17:43:34"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Patantierte MustersteineDies hier sind Muster von patentierten Steinen 30 für fischertechnik die jedoch nie in den Verkauf kamen. Zum einen rechts vorne (und der rote hinten) ein alternativer Zapfen, der nicht mehr eingeprest wird und eine längliche Form hat und sofort mit gespritzt wird und zum anderen links vorne ein Baustein mit einer Kerbe. Diesen Baustein kann man auch von außen einklicken. 
Die hintere Nut ist bei diesen Bausteinen eingebohrt.
Die Patentschriften zu diesen Bausteinen mit Zeichnungen kann man sich auf dem Patentserver herunterlanden.
Holger