---
layout: "image"
title: "PICT0811"
date: "2005-05-29T19:04:58"
picture: "PICT0811.jpg"
weight: "17"
konstrukteure: 
- "Uwe Timm (Chemikus)"
fotografen:
- "Uwe Timm (Chemikus)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/4288
- /details3cf0-2.html
imported:
- "2019"
_4images_image_id: "4288"
_4images_cat_id: "355"
_4images_user_id: "5"
_4images_image_date: "2005-05-29T19:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4288 -->
