---
layout: "image"
title: "Waldachtal Fanclubtreffen 1.06.2008"
date: "2008-06-07T19:47:15"
picture: "waldachtalfanclubtreffen37.jpg"
weight: "37"
konstrukteure: 
- "ft"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/14665
- /detailsd3e3.html
imported:
- "2019"
_4images_image_id: "14665"
_4images_cat_id: "1345"
_4images_user_id: "409"
_4images_image_date: "2008-06-07T19:47:15"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14665 -->
Modell von den Neuheiten Kehrmaschine und Da Vinci Machines
