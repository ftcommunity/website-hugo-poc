---
layout: "image"
title: "Techniksiskussion Radlader"
date: "2008-06-20T16:07:14"
picture: "Technikdiskussion_Radlader.jpg"
weight: "41"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Hr. Peterra"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/14716
- /details8c7d.html
imported:
- "2019"
_4images_image_id: "14716"
_4images_cat_id: "1345"
_4images_user_id: "107"
_4images_image_date: "2008-06-20T16:07:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14716 -->
