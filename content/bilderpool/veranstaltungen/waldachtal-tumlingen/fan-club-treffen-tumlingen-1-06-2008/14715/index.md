---
layout: "image"
title: "Radlader"
date: "2008-06-20T16:07:13"
picture: "Radlader_1.jpg"
weight: "40"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Hr. Peterra"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/14715
- /details25f7.html
imported:
- "2019"
_4images_image_id: "14715"
_4images_cat_id: "1345"
_4images_user_id: "107"
_4images_image_date: "2008-06-20T16:07:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14715 -->
Das Modell ist auch unter Scale-Modelle/Baumaschinen/Radlader (jw) zu finden
