---
layout: "image"
title: "Waldachtal Fanclubtreffen 1.06.2008"
date: "2008-06-07T19:47:14"
picture: "waldachtalfanclubtreffen07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/14635
- /detailsad40.html
imported:
- "2019"
_4images_image_id: "14635"
_4images_cat_id: "1345"
_4images_user_id: "409"
_4images_image_date: "2008-06-07T19:47:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14635 -->
Arbeitsplatz beim Computing Workshop
