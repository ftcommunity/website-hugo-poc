---
layout: "image"
title: "Waldachtal Fanclubtreffen 1.06.2008"
date: "2008-06-07T19:47:15"
picture: "waldachtalfanclubtreffen30.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/14658
- /details990d.html
imported:
- "2019"
_4images_image_id: "14658"
_4images_cat_id: "1345"
_4images_user_id: "409"
_4images_image_date: "2008-06-07T19:47:15"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14658 -->
Die Aussteller wurden "alle" vom SWR Fernsehen interviewt
