---
layout: "image"
title: "Teacher"
date: "2009-06-27T19:56:57"
picture: "ft_teacher_a.jpg"
weight: "7"
konstrukteure: 
- "Teacher"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Arkansas", "PCS", "AOR"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24464
- /details7308.html
imported:
- "2019"
_4images_image_id: "24464"
_4images_cat_id: "1678"
_4images_user_id: "585"
_4images_image_date: "2009-06-27T19:56:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24464 -->
This is one of the teachers attending the training session.