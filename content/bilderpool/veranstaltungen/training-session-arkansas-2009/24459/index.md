---
layout: "image"
title: "Interesting Model"
date: "2009-06-27T19:56:56"
picture: "ft_interesting_a.jpg"
weight: "2"
konstrukteure: 
- "Student"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Arkansas", "PCS"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24459
- /details3142.html
imported:
- "2019"
_4images_image_id: "24459"
_4images_cat_id: "1678"
_4images_user_id: "585"
_4images_image_date: "2009-06-27T19:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24459 -->
After being introduced to ft nomenclature, students are asked to construct an interesting model from random elements.