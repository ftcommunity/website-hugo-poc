---
layout: "image"
title: "Containerbrücke"
date: "2006-04-29T11:14:33"
picture: "Bemmel_06_6.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Jürgen Warwel (jw)"
uploadBy: "jw"
license: "unknown"
legacy_id:
- /php/details/6171
- /details14a0.html
imported:
- "2019"
_4images_image_id: "6171"
_4images_cat_id: "580"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6171 -->
Fast geschafft - in einer Stunde ist die Ausstellung vorbei.
Die Luft in der Halle wird immer schlechter, mein Akku ist leer. 

Wir Modellbauer sind verrückt - aber ohne uns währe die Welt um die schönste Nebensache ärmer.
