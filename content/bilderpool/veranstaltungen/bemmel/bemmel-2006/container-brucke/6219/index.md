---
layout: "image"
title: "Container Terminal 2"
date: "2006-05-07T15:16:33"
picture: "IMG_2525_1.jpg"
weight: "4"
konstrukteure: 
- "Jürgen Warwel (jw)"
fotografen:
- "Rob  van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/6219
- /details2ba7.html
imported:
- "2019"
_4images_image_id: "6219"
_4images_cat_id: "580"
_4images_user_id: "379"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6219 -->
