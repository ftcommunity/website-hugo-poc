---
layout: "image"
title: "Kran 2"
date: "2006-05-07T15:16:33"
picture: "IMG_2519_1.jpg"
weight: "4"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Rob  van Baal"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- /php/details/6233
- /detailsa81a.html
imported:
- "2019"
_4images_image_id: "6233"
_4images_cat_id: "581"
_4images_user_id: "379"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6233 -->
