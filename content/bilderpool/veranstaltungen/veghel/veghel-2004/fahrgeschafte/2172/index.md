---
layout: "image"
title: "RR04.JPG"
date: "2004-02-20T12:22:46"
picture: "RR04_2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2172
- /details240c.html
imported:
- "2019"
_4images_image_id: "2172"
_4images_cat_id: "426"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2172 -->
Es gab zwei Riesenräder zu sehen. Hier das 'eine' von [...] (sorry, hab den Namen nicht notiert), das andere von den Brickweddes.
Detailaufnahmen in der Kategorie 'Kirmesmodelle', http://www.ftcommunity.de/categories.php?cat_id=124