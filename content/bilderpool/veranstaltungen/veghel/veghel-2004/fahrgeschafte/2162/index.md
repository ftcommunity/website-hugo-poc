---
layout: "image"
title: "Pianoroboter 9"
date: "2004-02-20T12:22:45"
picture: "Riesenrad_9_2.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2162
- /details0bb1.html
imported:
- "2019"
_4images_image_id: "2162"
_4images_cat_id: "426"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2162 -->
Der Meister lebt auf großem Fuße...