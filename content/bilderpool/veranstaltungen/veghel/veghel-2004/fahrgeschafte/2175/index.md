---
layout: "image"
title: "Riesenrad 1"
date: "2004-02-20T12:22:55"
picture: "Riesenrad_1.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Kirmes", "Riesenrad"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2175
- /details4cdd-4.html
imported:
- "2019"
_4images_image_id: "2175"
_4images_cat_id: "426"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2175 -->
