---
layout: "image"
title: "Freefall01.JPG"
date: "2004-02-20T12:21:29"
picture: "Freefall01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2136
- /details6a85-2.html
imported:
- "2019"
_4images_image_id: "2136"
_4images_cat_id: "426"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2136 -->
Dieses Modell ist in Schoonhoven 2003 als 'Starlifter' dabei gewesen: 
http://www.ftcommunity.de/categories.php?cat_id=202