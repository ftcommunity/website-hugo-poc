---
layout: "image"
title: "Kitzelroboter 1"
date: "2004-02-20T12:22:45"
picture: "Kitzelroboter_1.jpg"
weight: "4"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
schlagworte: ["Roboter", "Knickarm"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2143
- /details85d7-2.html
imported:
- "2019"
_4images_image_id: "2143"
_4images_cat_id: "427"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2143 -->
Es handelt sich bei diesem Modell um einen Knickarm-Roboter mit sechs
Bewegungsachsen plus Greifer. Der Roboterarm wird von insgesamt neun
Motoren angetrieben (3 x Power Motor 8:1, 2 x Power Motor 50:1 und
4 x S-Motor). Die Positionsrückmeldung erfolgt über sieben Mini-Taster.
Die maximale Reichweite vom zentralen Drehpunkt bis zur Greiferspitze
gemessen beträgt 59 cm. Die Achsen haben folgende Arbeitsbereiche:

Achse 1:   360 Grad
Achse 2:   243 Grad
Achse 3:   236 Grad
Achse 4:   360 Grad
Achse 5:   190 Grad
Achse 6:   360 Grad

Das Modell wiegt komplett mit Gegengewichten, Elektronik und Unterbau,
jedoch ohne Stromversorgung, ca. 8,5 Kilogramm. Die Steuerelektronik ist
eine Eigenentwicklung auf Basis eines ATMEL ATmega32 Microcontrollers;
das Programm wurde in Assembler erstellt. Die Bauzeit für die Mechanik
und die Verkabelung betrug ca. 70 Stunden; die Elektronik war aus einem
früheren Projekt vorhanden.