---
layout: "image"
title: "Pianoroboter 9"
date: "2004-02-20T12:22:46"
picture: "Pianoroboter_9.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2163
- /details2ce8.html
imported:
- "2019"
_4images_image_id: "2163"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2163 -->
Der Meister lebt auf großem Fuße...