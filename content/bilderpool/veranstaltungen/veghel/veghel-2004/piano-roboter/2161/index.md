---
layout: "image"
title: "Pianoroboter 8"
date: "2004-02-20T12:22:45"
picture: "Pianoroboter_8.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- /php/details/2161
- /detailsfe60-2.html
imported:
- "2019"
_4images_image_id: "2161"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2161 -->
Detail Schulter (Ventile für die Fingersteuerung)