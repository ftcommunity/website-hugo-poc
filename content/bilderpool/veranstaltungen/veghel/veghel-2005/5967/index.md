---
layout: "image"
title: "Veghel_041.jpg"
date: "2006-03-26T15:44:21"
picture: "Veghel_041.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Piano", "Roboter"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5967
- /details5b2d.html
imported:
- "2019"
_4images_image_id: "5967"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:44:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5967 -->
