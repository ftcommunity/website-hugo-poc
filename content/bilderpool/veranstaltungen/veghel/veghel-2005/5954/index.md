---
layout: "image"
title: "Veghel_021.jpg"
date: "2006-03-26T15:30:24"
picture: "Veghel_021.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Portalkran"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5954
- /details2289.html
imported:
- "2019"
_4images_image_id: "5954"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:30:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5954 -->
