---
layout: "image"
title: "Veghel_019.jpg"
date: "2006-03-26T15:27:30"
picture: "Veghel_019.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Portalkran"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5953
- /detailsd178.html
imported:
- "2019"
_4images_image_id: "5953"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:27:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5953 -->
