---
layout: "image"
title: "Veghel_022.jpg"
date: "2006-03-26T15:32:13"
picture: "Veghel_022.jpg"
weight: "1"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5955
- /details0a4c-2.html
imported:
- "2019"
_4images_image_id: "5955"
_4images_cat_id: "517"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:32:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5955 -->
