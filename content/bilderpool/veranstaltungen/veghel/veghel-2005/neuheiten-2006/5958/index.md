---
layout: "image"
title: "Veghel_029.jpg"
date: "2006-03-26T15:33:40"
picture: "Veghel_029.jpg"
weight: "4"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5958
- /details7a75.html
imported:
- "2019"
_4images_image_id: "5958"
_4images_cat_id: "517"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:33:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5958 -->
