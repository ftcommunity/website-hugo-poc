---
layout: "image"
title: "Veghel_018.jpg"
date: "2006-03-26T15:27:06"
picture: "Veghel_018.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Portalkran"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5952
- /details28ae.html
imported:
- "2019"
_4images_image_id: "5952"
_4images_cat_id: "515"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:27:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5952 -->
