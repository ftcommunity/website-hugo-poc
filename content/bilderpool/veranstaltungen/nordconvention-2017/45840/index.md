---
layout: "image"
title: "3-D Druck 2 farbig Ultimaker"
date: "2017-05-15T12:07:39"
picture: "nordconvention30.jpg"
weight: "30"
konstrukteure: 
- "Robin"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45840
- /details1ede-3.html
imported:
- "2019"
_4images_image_id: "45840"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45840 -->
