---
layout: "image"
title: "Schwungrad vom Sägewerk"
date: "2017-05-17T16:39:47"
picture: "nordc13.jpg"
weight: "87"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45900
- /details6ef9-2.html
imported:
- "2019"
_4images_image_id: "45900"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45900 -->
