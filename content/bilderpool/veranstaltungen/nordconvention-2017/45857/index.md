---
layout: "image"
title: "Müllwagen hinten"
date: "2017-05-15T12:07:43"
picture: "nordconvention47.jpg"
weight: "47"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45857
- /details1950-2.html
imported:
- "2019"
_4images_image_id: "45857"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:43"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45857 -->
