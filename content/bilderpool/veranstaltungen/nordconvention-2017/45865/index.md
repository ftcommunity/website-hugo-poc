---
layout: "image"
title: "Brickly"
date: "2017-05-15T12:07:47"
picture: "nordconvention55.jpg"
weight: "55"
konstrukteure: 
- "Grau"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45865
- /detailsbb35.html
imported:
- "2019"
_4images_image_id: "45865"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45865 -->
