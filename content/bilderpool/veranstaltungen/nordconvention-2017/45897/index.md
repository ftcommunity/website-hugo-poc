---
layout: "image"
title: "Sägewerk von Ingwer"
date: "2017-05-17T15:53:45"
picture: "nordc10.jpg"
weight: "84"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45897
- /details21af-2.html
imported:
- "2019"
_4images_image_id: "45897"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45897 -->
