---
layout: "image"
title: "Karussell"
date: "2017-05-15T12:07:47"
picture: "nordconvention53.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45863
- /details8677.html
imported:
- "2019"
_4images_image_id: "45863"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45863 -->
