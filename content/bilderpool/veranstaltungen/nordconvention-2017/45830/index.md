---
layout: "image"
title: "Distanzring"
date: "2017-05-15T12:07:35"
picture: "nordconvention20.jpg"
weight: "20"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45830
- /details580d.html
imported:
- "2019"
_4images_image_id: "45830"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:35"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45830 -->
