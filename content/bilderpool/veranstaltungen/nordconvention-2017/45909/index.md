---
layout: "image"
title: "Lokomobil von Stephan"
date: "2017-05-17T16:39:47"
picture: "nordc22.jpg"
weight: "96"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45909
- /detailsb03a.html
imported:
- "2019"
_4images_image_id: "45909"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45909 -->
