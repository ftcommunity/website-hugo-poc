---
layout: "image"
title: "Kugelbahn"
date: "2017-05-15T12:07:43"
picture: "nordconvention49.jpg"
weight: "49"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45859
- /details65ef.html
imported:
- "2019"
_4images_image_id: "45859"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:43"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45859 -->
