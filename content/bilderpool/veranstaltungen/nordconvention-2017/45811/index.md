---
layout: "image"
title: "Aufbau"
date: "2017-05-15T12:07:26"
picture: "nordconvention01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45811
- /details4073.html
imported:
- "2019"
_4images_image_id: "45811"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45811 -->
