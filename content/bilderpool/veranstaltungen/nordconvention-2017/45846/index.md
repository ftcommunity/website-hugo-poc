---
layout: "image"
title: "Sitzgruppe ft"
date: "2017-05-15T12:07:39"
picture: "nordconvention36.jpg"
weight: "36"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45846
- /details6c29.html
imported:
- "2019"
_4images_image_id: "45846"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45846 -->
