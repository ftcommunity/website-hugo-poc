---
layout: "image"
title: "Der alte Jet"
date: "2017-05-17T16:39:47"
picture: "nordc23.jpg"
weight: "97"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45910
- /details38b6.html
imported:
- "2019"
_4images_image_id: "45910"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45910 -->
Die Aufkleber für die Fenster waren schon so schief augeklebt als ich den gekauft habe. Wenn ich die abmachen würde gehen sie vermutlich kaputt.