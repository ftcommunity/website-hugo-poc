---
layout: "image"
title: "Müllwagen Detail"
date: "2017-05-15T12:07:43"
picture: "nordconvention46.jpg"
weight: "46"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45856
- /details8481.html
imported:
- "2019"
_4images_image_id: "45856"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:43"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45856 -->
