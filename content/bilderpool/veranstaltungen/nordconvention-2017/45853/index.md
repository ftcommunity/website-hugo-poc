---
layout: "image"
title: "LKW"
date: "2017-05-15T12:07:43"
picture: "nordconvention43.jpg"
weight: "43"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45853
- /detailse86c.html
imported:
- "2019"
_4images_image_id: "45853"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:43"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45853 -->
