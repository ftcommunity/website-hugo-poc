---
layout: "image"
title: "Aufbau Kopf"
date: "2017-05-15T12:07:35"
picture: "nordconvention23.jpg"
weight: "23"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45833
- /detailsea56.html
imported:
- "2019"
_4images_image_id: "45833"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:35"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45833 -->
Oben vier Segmentanzeigen für bis zu 4 Spieler gleichzeitig.
Unten vier 8x8 Dot Matrix Adafruit.
