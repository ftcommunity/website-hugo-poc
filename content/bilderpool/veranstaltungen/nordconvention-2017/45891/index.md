---
layout: "image"
title: "Eine Seitenkugel"
date: "2017-05-17T15:53:45"
picture: "nordc04.jpg"
weight: "78"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45891
- /detailsb80e-2.html
imported:
- "2019"
_4images_image_id: "45891"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45891 -->
