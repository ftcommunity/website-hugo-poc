---
layout: "image"
title: "Sägewerk Welle"
date: "2017-05-15T12:07:32"
picture: "nordconvention13.jpg"
weight: "13"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45823
- /detailsd161.html
imported:
- "2019"
_4images_image_id: "45823"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45823 -->
