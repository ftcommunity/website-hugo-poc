---
layout: "image"
title: "Gesamtansicht"
date: "2017-05-17T15:53:45"
picture: "nordc08.jpg"
weight: "82"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45895
- /detailsf04a-3.html
imported:
- "2019"
_4images_image_id: "45895"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45895 -->
