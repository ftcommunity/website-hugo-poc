---
layout: "image"
title: "Münzenautomat"
date: "2017-05-15T12:07:51"
picture: "nordconvention67.jpg"
weight: "67"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45877
- /detailsc1b2.html
imported:
- "2019"
_4images_image_id: "45877"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:51"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45877 -->
