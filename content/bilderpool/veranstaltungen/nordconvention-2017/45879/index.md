---
layout: "image"
title: "Leuchtturm"
date: "2017-05-15T12:07:51"
picture: "nordconvention69.jpg"
weight: "69"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45879
- /details2d11.html
imported:
- "2019"
_4images_image_id: "45879"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:51"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45879 -->
