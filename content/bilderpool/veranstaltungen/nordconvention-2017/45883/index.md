---
layout: "image"
title: "fischertechnik Keller"
date: "2017-05-15T12:07:55"
picture: "nordconvention73.jpg"
weight: "73"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Silke"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45883
- /details6165.html
imported:
- "2019"
_4images_image_id: "45883"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:55"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45883 -->
