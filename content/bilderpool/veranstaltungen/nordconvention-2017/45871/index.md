---
layout: "image"
title: "Ballweitergabemaschine"
date: "2017-05-15T12:07:51"
picture: "nordconvention61.jpg"
weight: "61"
konstrukteure: 
- "ftcommunity"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45871
- /details9719-4.html
imported:
- "2019"
_4images_image_id: "45871"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:51"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45871 -->
