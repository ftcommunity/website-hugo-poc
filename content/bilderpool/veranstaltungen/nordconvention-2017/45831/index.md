---
layout: "image"
title: "Lager"
date: "2017-05-15T12:07:35"
picture: "nordconvention21.jpg"
weight: "21"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45831
- /detailsffd2.html
imported:
- "2019"
_4images_image_id: "45831"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:35"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45831 -->
