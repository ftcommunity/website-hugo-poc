---
layout: "image"
title: "Radarturm"
date: "2017-05-15T12:07:47"
picture: "nordconvention51.jpg"
weight: "51"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45861
- /details8887.html
imported:
- "2019"
_4images_image_id: "45861"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45861 -->
