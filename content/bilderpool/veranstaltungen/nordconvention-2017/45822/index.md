---
layout: "image"
title: "Fachwerk Detail"
date: "2017-05-15T12:07:32"
picture: "nordconvention12.jpg"
weight: "12"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45822
- /details3c97.html
imported:
- "2019"
_4images_image_id: "45822"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:32"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45822 -->
