---
layout: "image"
title: "Sattelauflieger"
date: "2017-05-15T12:07:39"
picture: "nordconvention34.jpg"
weight: "34"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45844
- /details6880.html
imported:
- "2019"
_4images_image_id: "45844"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45844 -->
