---
layout: "image"
title: "Sägewerk von Ingwer"
date: "2017-05-17T16:39:47"
picture: "nordc11.jpg"
weight: "85"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45898
- /details384a-2.html
imported:
- "2019"
_4images_image_id: "45898"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45898 -->
