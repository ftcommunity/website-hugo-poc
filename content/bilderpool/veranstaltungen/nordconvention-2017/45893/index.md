---
layout: "image"
title: "Die mittlere Kugel"
date: "2017-05-17T15:53:45"
picture: "nordc06.jpg"
weight: "80"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/45893
- /detailsa67a.html
imported:
- "2019"
_4images_image_id: "45893"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45893 -->
