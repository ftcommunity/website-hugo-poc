---
layout: "image"
title: "Trike"
date: "2017-05-15T12:07:39"
picture: "nordconvention33.jpg"
weight: "33"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/45843
- /detailsdb7c.html
imported:
- "2019"
_4images_image_id: "45843"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45843 -->
