---
layout: "overview"
title: "LiDAR Scanner
"
date: 2020-03-06T21:04:40+01:00
---

Mit den Kindern zusammen auf Basis der Master Plus Discovery / Galaxy Modelle entworfen.

Video [hier](https://youtu.be/xhSv_N6YcIY) auf YouTube.
