---
layout: "image"
title: "Die LiDAR Station"
date: 2020-03-06T21:04:50+01:00
picture: "DSC06918.JPG"
weight: "1"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "Website-Team"
license: "unknown"
---

Ankunft am Einsatzort
