---
layout: "image"
title: "Der Radar-Screen"
date: 2020-03-06T21:04:40+01:00
picture: "screenshot.JPG"
weight: "8"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Peter Habermehl (PHabermehl)"
uploadBy: "Website-Team"
license: "unknown"
---

Feindliche Objekte im Anflug
