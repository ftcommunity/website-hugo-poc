---
layout: "image"
title: "Schleifring"
date: 2020-03-08T16:58:37+01:00
picture: "P1000649.JPG"
weight: "7"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Blick auf den Taster von der Seite. Die Position der Unterbrecherstücke ist auf beiden Schleifringen **identisch**.