---
layout: "image"
title: "Vogelperspektive"
date: 2020-03-08T16:58:38+01:00
picture: "P1000651.JPG"
weight: "8"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell von oben gesehen.