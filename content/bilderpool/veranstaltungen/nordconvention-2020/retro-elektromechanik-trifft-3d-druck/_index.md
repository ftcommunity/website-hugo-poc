---
layout: "overview"
title: "Retro-Elektromechanik trifft 3D-Druck 
"
date: 2020-03-08T16:58:28+01:00
---

Dieses Modell ist ein typisches Beispiel, wie man mit modernen Mitteln Probleme löst, die man ohne diese Mittel gar nicht hätte.
In diesem Fall ging es darum, die Frage zu beantworten, was man mit [nicht-runden Zahnrädern](https://www.thingiverse.com/thing:3302692) aus dem 3D-Drucker anfangen kann.
Da ich kürzlich ein Fan der alten [elektromechanischen Kästen](https://ft-datenbank.de/ft-article/1235) geworden bin, habe ich beide Anwendungsgebiete zusammengefasst.
Außerdem sollte das Modell klein genug sein, um bequem mit der Bahn transportiert werden zu können.
