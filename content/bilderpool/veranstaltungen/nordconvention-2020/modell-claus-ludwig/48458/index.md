---
layout: "image"
title: "Flipper bei Nacht"
date: 2020-03-09T21:13:21+01:00
picture: "Flipper bei Nacht.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Claus Ludwig (Claus)"
uploadBy: "Website-Team"
license: "unknown"
---

Ansicht in der dunklen Eckkneipe von nebenan
