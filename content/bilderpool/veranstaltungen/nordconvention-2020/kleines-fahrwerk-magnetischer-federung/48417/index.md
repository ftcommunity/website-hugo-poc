---
layout: "image"
title: "Ansicht von unten"
date: 2020-03-06T19:44:55+01:00
picture: "2020-01-06 Kleines Fahrwerk mit magnetischer Federung3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Ziel war, das Fahrwerk möglichst kompakt zu bauen.
