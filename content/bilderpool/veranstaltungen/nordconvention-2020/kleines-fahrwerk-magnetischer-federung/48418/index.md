---
layout: "image"
title: "Gesamtansicht von hinten"
date: 2020-03-06T19:44:57+01:00
picture: "2020-01-06 Kleines Fahrwerk mit magnetischer Federung2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Hinterachse besteht aus Längslenkern mit einem etwas unrealistischen Gummiantrieb. Die Federung wirkt durch je zwei am Fahrzeugrahmen und zwei auf dem Längslenker befindlichen Magnete. Der XS-Motor, der das Differenzial antreibt, sorgt nur für langsamen Vortrieb.

Die Stromversorgung kommt von oben - per Kabel. Versuche mit einer 9V-Blockbatterie verliefen nicht gut, und der Akku war mir zu schwer und groß.
