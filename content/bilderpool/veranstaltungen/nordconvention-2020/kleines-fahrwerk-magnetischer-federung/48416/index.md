---
layout: "image"
title: "Vorderachse (1)"
date: 2020-03-06T19:44:54+01:00
picture: "2020-01-06 Kleines Fahrwerk mit magnetischer Federung4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

In der Mitte sitzen zwei Gelenksteine auf zwei K-Achsen. Die S-Kupplung 30 ganz vorne und die beiden BS15 mit Bohrung auf der hinteren Seite werden nur von zwei obenauf senkrecht stehenden BS7,5 mit zwei Verbindern 30 zusammengehalten, die wiederum den waagerechten BS7,5 mit den Federmagneten tragen.
