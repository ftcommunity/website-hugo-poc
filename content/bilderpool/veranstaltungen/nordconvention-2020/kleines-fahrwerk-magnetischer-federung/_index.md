---
layout: "overview"
title: "Kleines Fahrwerk mit magnetischer Federung"
date: 2020-03-06T19:44:51+01:00
---

Ein kompaktes Fahrwerk mit Heckantrieb, Lenkung und magnetisch gefederter Einzelradaufhängung.
