---
layout: "image"
title: "Gesamtansicht von vorne"
date: 2020-03-06T19:44:58+01:00
picture: "2020-01-06 Kleines Fahrwerk mit magnetischer Federung1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Website-Team"
license: "unknown"
---

Die Vorderachse verfügt über einzeln aufgehängte Querlenker und wird durch 6 Neodym-Magnete gefedert, die so angeordnet sind, dass die senkrecht stehenden am Fahrzeug angebrachten die waagerecht auf den Querlenkern befestigten abstoßen. Die Vorderräder sitzen auf Freilaufnaben.

Leider stellt sich aber heraus, dass sich abstoßende Magnete nur bedingt gut für eine Fahrzeugfederung sind. Die Kraft, mit der sie sich abstoßen, nimmt quadratisch mit dem Abstand ab, also auch umkehrt quadratisch mit sinkendem Abstand zu. Das bewirkt, dass die Räder viel lieber aus- als einfedern: Wenn ein Rad über etwas drüberfährt, wird eher das ganze Fahrzeug angehoben und die anderen Räder federn aus, anstatt dass das eine Rad einfedert. Genau anders als man es gerne hätte.
