---
layout: "image"
title: "Schaltplan"
date: 2020-03-21T16:04:44+01:00
picture: "schaltplan-em-pause.gif"
weight: "2"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Grafik verdeutlicht den Stromlauf. Besonderes Augenmerk gilt dem Umstand, daß hierfür nicht mal ein Relais notwendig ist. Das Schaltwerk ist so konzipiert, daß dieses erst aktiviert wird, wenn der Haupt- motor den Aufzug bis zum jeweiligen Endtaster geführt hat. Der Hauptmotor stoppt, und das Schaltwerk setzt sich in Bewegung. Die Polarität für den Hauptmotor wechselt, und selbiger startet in die Gegenrich- tung; das Schaltwerk stoppt.  Der Startimpuls für den Hauptmotor kann übrigens für beide Richtungen individuell festgelegt werden, indem die Schaltnocken für B3/B4 entsprechend ausgerichtet werden. Der Startimpuls muß allerdings zwingend innerhalb der Zeit erfolgen, solange C1 aktiv ist. Und der Startimpuls auf B3/B4 muß minde- stens so lange dauern, bis der Hauptmotor den gerade aktiven Endtaster wieder freigegeben hat.  Übrigens: die Polarität als Ganzes ist interessanterweise vollkommen wurscht. Die zwei Endtaster des Hauptmotors arbeiten für beide Richtungen gleichermaßen. Lediglich der Motor für die Nebenfunktion - im dargestellten Beispiel die Vereinzelung der Kugeln oben - hat eine definierte Drehrichtung. Für den Motor des Schaltwerks sollte die Drehrichtung insofern definiert sein, als daß der Schaltnocken für C1 genau dann deaktiviert wird, wenn die Schaltnocken für A1/A2 (Polarität Hauptmotor) den günstigsten Drehwinkel (empfohlen) erreicht haben.  Eine elektromechanische Steuerung wie diese zeigt auch letztlich die Diskrepanz, warum die einzelnen Taster immer mit einer gewissen Reserve ausgerichtet sein müssen. Denn im Unterschied zu rein digita- len Steuerungen können eben nur dort Impulse auch wirklich synchron definiert erfolgen.
