---
layout: "image"
title: "Ansicht von oben"
date: 2020-03-21T16:04:42+01:00
picture: "ansichtvonoben.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Website-Team"
license: "unknown"
---

