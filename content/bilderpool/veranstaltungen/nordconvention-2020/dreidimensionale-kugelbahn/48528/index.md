---
layout: "image"
title: "Gesamtansicht von nahem"
date: 2020-03-29T17:11:38+02:00
picture: "gesamtnah.jpg"
weight: "3"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Website-Team"
license: "unknown"
---

