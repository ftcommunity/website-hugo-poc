---
layout: "image"
title: "3D-Kugelbahn: Gesamtansicht"
date: 2020-03-29T17:11:40+02:00
picture: "gesamt.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Website-Team"
license: "unknown"
---

Zum Modell: Es handelt sich um eine dreidimensionale Kugelbahn - drei Dimensionen, weil es sich um drei ineinander verschlungene Winkelebenen handelt. Sind alle Kugeln am Ende einer Bahn angelangt, so dreht sich das Modell um exakt 120° weiter; die Kugeln kippen in die nächste Winkelebene und laufen dann in einen Mechanismus zum Vereinzeln der Kugeln. Dieser Mechanismus ist für alle drei Bahnen in unterschiedlicher Art gestaltet, und jede der drei Bahnen ist für sich eigenständig. Eine Bahn ist sogar mit einem Looping ausgestattet, in einer anderen Bahn gibt's ein Klangrohr.

Die Steuerung ist, wie bei mir üblich, klassisch aus Silberlingen und Einzel-ICs aufgebaut. Im grunde handelt es sich um eine simple Zeitsteuerung, die den Lauf der Kugeln in den Bahnen "großzügig" berücksichtigt, die Konstruktion dann um 120° weiterdreht und über den separaten Vereinzeler die Kugeln nach und nach in die jeweils aktive Bahn führt.

Der präzise Stop nach genau 120° erfolgt einerseits mittels schweren Verschlußriegels im Standfuß sowie andererseits eines Tasters als Selbstauslöser, um den Hauptmotor abzuschalten. Der Hauptmotor treibt das Modell über einen eigens dafür konstruierten Ring aus 48 Bausteinen 15 & 48 Winkelsteinen 7,5° und darauf montierten 213 Kettengliedern an.

Der sich drehende Teil wiegt insgesamt ca. 7,5 kg (!) und ist massemäßig hochfein austariert. Die notwendige Antriebskraft ist so minimal, daß hier ein kleiner Fingerstoß ausreicht. Ein weiterer Faktor für die Leichtläufigkeit bildet zweifelsohne das in der Mitte verbaute und in den Stützen auf Rollen gelagerte Metallrohr. Die einzige Unwucht bilden tatsächlich die acht in der Bahn liegenden Kugeln selbst.

Was die Teile insgesamt betrifft, so habe ich bei 120 Stk. verbauten Winkelträgern 120 aufgehört zu zählen; auch deshalb, weil in den verschiedenen Bauphasen auch immer wieder Umbauten erforderlich wurden. Eine Konstruktion wie diese erfordert immerhin, daß sich die gesamte Masse gleichmäßig auf die Mitte konzentriert, völlig egal, in welcher Position der sich drehende Teil gerade befindet. Es gibt hier kein Oben und kein Unten. Wieviel Winkelsteine ich verbaut habe, läßt sich aktuell ebenfalls nicht beziffern. Sehr viel verbautes Material fungiert ausschließlich massiver Stabilisierungen und wäre jetzt von außen nicht zugänglich. Sicher verbaut sind jedoch fünf Motoren - 4 Minimotoren und 1 großer Motor XM.

Übrigens: im Standfuß befindet sich die Stromzufuhr für den beweglichen Teil. Diese ist realisiert mit normalen Schrauben M4 und passenden Hutmuttern - natürlich ebenfalls präzise ausgerichtet. Dadurch wird einerseits immer der richtige (also jeweils dann "oben" befindliche) Motor für die Vereinzelung der Kugeln aktiviert und andererseits eine Ringschaltung aus sechs Rainbow-LEDs versorgt.

Mit freundlichen Grüßen, Thomas Habig

alias Triceratops
