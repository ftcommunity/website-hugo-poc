---
layout: "image"
title: "Nordconvention 2020"
date: 2020-03-06T18:44:58+01:00
picture: "a6_Postkarte2020-01 -Seite001.jpg"
weight: "1"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Holger Bernhardt (Svefisch)"
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Website-Team"
license: "unknown"
---
