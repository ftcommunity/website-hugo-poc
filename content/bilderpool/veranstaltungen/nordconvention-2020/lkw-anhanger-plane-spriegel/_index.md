---
layout: "overview"
title: "LKW-Anhänger mit Plane und Spriegel
"
date: 2020-03-06T20:48:00+01:00
---

Für meinen [kleinen Kipper-LKW](http://ftcommunity.de/bilderpool/modelle/radfahrzeuge/lkw/kleiner-kipper-doppelachsanhanger-ir/gallery-index/) wollte ich schon länger einen Anhänger bauen.
Auf [Svens](https://forum.ftcommunity.de/memberlist.php?mode=viewprofile&u=2) Anregung hin entstand dieses Modell basierend auf den [Kippermulden 31844](https://ft-datenbank.de/ft-article/2421).
Der Aufbau lehnt sich stark an den [Hydraulik-Anhänger](https://ft-datenbank.de/ft-article/3005) an, allerdings eben ohne Hydraulik und Kippfunktion.
Die sehr raren [Spriegel 31265](https://ft-datenbank.de/ft-article/3984) wurden durch von Sven konstruierte, selbstgefertigte 3D-Drucke ersetzt.
Die "Plane" besteht aus Fotokarton und Papier.
