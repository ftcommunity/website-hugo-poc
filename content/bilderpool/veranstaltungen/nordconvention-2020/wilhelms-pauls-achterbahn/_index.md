---
layout: "overview"
title: "Wilhelms und Pauls Achterbahn
"
date: 2020-03-12T18:17:13+01:00
---

Als Inspiration diente die Achterbahn von Dr. Dabert konstruktorius. Wir haben bei unserer Ausführung die beiden Fahrrampen als Modul gestaltet und unter 90 Grad angeordnet. Die dadurch entstandene Höhe ermöglicht eine Fahrbahnlänge von ca. 7m. Bei der Ausgestaltung der Wagen war unser Ansatz ausschließlich Fischertechnikbauelemente zu verwenden. Es wurden ca. 20 verschiedene Ausführungen getestet bis wir zu der dargestellten Lösung kamen.
