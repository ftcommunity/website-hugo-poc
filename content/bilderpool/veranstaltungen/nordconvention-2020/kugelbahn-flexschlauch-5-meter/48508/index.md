---
layout: "image"
title: "Kugelbahn mit Flexschlauch"
date: 2020-03-21T16:29:15+01:00
picture: "gesamt.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Website-Team"
license: "unknown"
---
Das Grundmodell stammt aus Dynamik XXL (Parcours 2, Seite 54 - 79) mit folgenden Modifikationen:

* graue Statik statt schwarze und spiegelverkehrt aufgebaut
* keine Verwendung des Riegelsteins - Tausch durch diverse alternative Elemente
* Kette verlängert auf 360 Glieder statt 351 Stk. (der oberste Baustein 15 m. Bohrung ist getauscht durch einen Baustein 5 & Baustein 15 - Achse durch obenliegende Nute)
* mit Kette synchronisiertes Getriebe und mechanische Zählung der Kugeln; der Antrieb stoppt nach exakt 12 Kugeln - Start manuell via Tastendruck

Erweiterungen:
* außenliegende Halterungen an allen vier Seiten
* durchgehender Flexschlauch 5 Meter
* zusätzliche Wechselweiche im oberen Bereich mit angepaßten Bahnverläufen

Der Flexschlauch stammt aus dem Baumarkt und wird unter der Bezeichnung "Teichtechnikschlauch"
vertrieben. Es gibt ihn verschiedenen Härtegraden und auch verschiedenen Durchmessern (benötigt
werden 19 mm bzw. 3/4'').
