---
layout: "image"
title: "Sägewerk"
date: "2007-11-10T15:28:05"
picture: "wilhelmbrickwedde02.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12579
- /details92ef.html
imported:
- "2019"
_4images_image_id: "12579"
_4images_cat_id: "1134"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12579 -->
