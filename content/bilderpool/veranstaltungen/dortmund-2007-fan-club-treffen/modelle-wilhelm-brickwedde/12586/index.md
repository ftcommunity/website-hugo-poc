---
layout: "image"
title: "Antriebe"
date: "2007-11-10T15:28:05"
picture: "wilhelmbrickwedde09.jpg"
weight: "9"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12586
- /detailsd2db.html
imported:
- "2019"
_4images_image_id: "12586"
_4images_cat_id: "1134"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12586 -->
