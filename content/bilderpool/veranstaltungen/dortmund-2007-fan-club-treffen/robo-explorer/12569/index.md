---
layout: "image"
title: "Robo Explorer"
date: "2007-11-10T15:10:47"
picture: "roboexplorer5.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12569
- /detailsd25e-2.html
imported:
- "2019"
_4images_image_id: "12569"
_4images_cat_id: "1132"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:10:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12569 -->
