---
layout: "image"
title: "LKW"
date: "2007-11-10T15:28:05"
picture: "raupe2.jpg"
weight: "2"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12589
- /details5116.html
imported:
- "2019"
_4images_image_id: "12589"
_4images_cat_id: "1135"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12589 -->
