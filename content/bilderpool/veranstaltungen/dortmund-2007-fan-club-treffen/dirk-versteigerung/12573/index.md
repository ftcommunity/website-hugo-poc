---
layout: "image"
title: "Dirk und die Versteigerung"
date: "2007-11-10T15:10:47"
picture: "dirkunddieversteigerung2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12573
- /details1a41.html
imported:
- "2019"
_4images_image_id: "12573"
_4images_cat_id: "1133"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:10:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12573 -->
