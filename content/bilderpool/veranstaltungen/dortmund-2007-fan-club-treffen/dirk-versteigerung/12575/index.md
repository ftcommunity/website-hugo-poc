---
layout: "image"
title: "Dirk und die Versteigerung"
date: "2007-11-10T15:10:47"
picture: "dirkunddieversteigerung4.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12575
- /details5cae.html
imported:
- "2019"
_4images_image_id: "12575"
_4images_cat_id: "1133"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:10:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12575 -->
