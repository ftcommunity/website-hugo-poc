---
layout: "image"
title: "'Rennstrecke'"
date: "2007-11-10T15:28:06"
picture: "luft04.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12593
- /details6385.html
imported:
- "2019"
_4images_image_id: "12593"
_4images_cat_id: "1136"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12593 -->
