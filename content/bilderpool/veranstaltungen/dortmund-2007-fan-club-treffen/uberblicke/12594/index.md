---
layout: "image"
title: "Schiffe versenken"
date: "2007-11-10T15:28:06"
picture: "luft05.jpg"
weight: "5"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12594
- /details3d57.html
imported:
- "2019"
_4images_image_id: "12594"
_4images_cat_id: "1136"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12594 -->
