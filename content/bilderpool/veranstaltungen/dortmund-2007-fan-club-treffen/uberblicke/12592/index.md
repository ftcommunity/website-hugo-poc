---
layout: "image"
title: "Luft"
date: "2007-11-10T15:28:05"
picture: "luft03.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12592
- /details1212.html
imported:
- "2019"
_4images_image_id: "12592"
_4images_cat_id: "1136"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12592 -->
