---
layout: "overview"
title: "modell-hobby-spiel 2022"
date: 2022-10-05T18:23:02+02:00
---

Die "modell-hobby-spiel" fand vom 30.09. bis 2.10.2022 in Leipzig statt.

Die ft-Community war mit einem kleinen Team vertreten. Von 67 000 Besuchern waren vielleicht ca. 2 500 an unserem Stand, zusätzlich ein paar Aussteller von den Nachbarständen.

Wir haben ca. 600 Give-Aways verteilt. Zusätzlich haben die großen und kleinen Besucher 7 kg Naschi-Tüten aus dem Kirmes-Greifer geangelt.
