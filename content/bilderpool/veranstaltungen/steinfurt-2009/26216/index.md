---
layout: "image"
title: "Wilhelm"
date: "2010-02-07T13:48:08"
picture: "DSCN3149.jpg"
weight: "4"
konstrukteure: 
- "Wilhelm"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26216
- /detailsd559.html
imported:
- "2019"
_4images_image_id: "26216"
_4images_cat_id: "1820"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T13:48:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26216 -->
