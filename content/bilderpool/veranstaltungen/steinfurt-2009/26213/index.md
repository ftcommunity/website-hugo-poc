---
layout: "image"
title: "Dirk und Fredy beim Aufbau"
date: "2010-02-07T13:44:21"
picture: "DSCN3142.jpg"
weight: "1"
konstrukteure: 
- "Dirk und Fredy"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26213
- /details70be.html
imported:
- "2019"
_4images_image_id: "26213"
_4images_cat_id: "1820"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T13:44:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26213 -->
