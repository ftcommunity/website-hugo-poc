---
layout: "image"
title: "unbekannter Aussteller"
date: "2010-02-07T13:49:08"
picture: "DSCN3155.jpg"
weight: "5"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26217
- /details2fb5.html
imported:
- "2019"
_4images_image_id: "26217"
_4images_cat_id: "1820"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T13:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26217 -->
