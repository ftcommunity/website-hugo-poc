---
layout: "image"
title: "Hier darf gespielt werden"
date: "2010-02-07T14:25:18"
picture: "DSCN3162.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26234
- /details8975.html
imported:
- "2019"
_4images_image_id: "26234"
_4images_cat_id: "1867"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26234 -->
