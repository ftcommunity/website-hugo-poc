---
layout: "image"
title: "Holgers Mühle"
date: "2010-02-07T13:45:18"
picture: "DSCN3147.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26214
- /details502a.html
imported:
- "2019"
_4images_image_id: "26214"
_4images_cat_id: "1820"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T13:45:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26214 -->
