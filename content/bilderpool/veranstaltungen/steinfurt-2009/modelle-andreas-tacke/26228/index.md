---
layout: "image"
title: "Modelle"
date: "2010-02-07T14:25:17"
picture: "DSCN3157.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26228
- /details2fa6.html
imported:
- "2019"
_4images_image_id: "26228"
_4images_cat_id: "1866"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26228 -->
