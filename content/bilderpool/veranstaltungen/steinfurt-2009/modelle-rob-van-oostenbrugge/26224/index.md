---
layout: "image"
title: "Tänzer"
date: "2010-02-07T14:25:17"
picture: "DSCN3136.jpg"
weight: "4"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26224
- /details7aa8.html
imported:
- "2019"
_4images_image_id: "26224"
_4images_cat_id: "1864"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26224 -->
