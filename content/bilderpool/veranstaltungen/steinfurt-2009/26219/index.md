---
layout: "image"
title: "Zuschauer"
date: "2010-02-07T13:57:22"
picture: "DSCN3163.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/26219
- /detailse670.html
imported:
- "2019"
_4images_image_id: "26219"
_4images_cat_id: "1820"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T13:57:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26219 -->
