---
layout: "image"
title: "Förderband Antrieb"
date: "2010-03-15T19:09:51"
picture: "folderband4.jpg"
weight: "5"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26733
- /details59d5.html
imported:
- "2019"
_4images_image_id: "26733"
_4images_cat_id: "1908"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26733 -->
