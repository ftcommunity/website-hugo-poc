---
layout: "image"
title: "F I G H T"
date: "2010-03-15T19:09:51"
picture: "sammies.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26735
- /detailsaea7.html
imported:
- "2019"
_4images_image_id: "26735"
_4images_cat_id: "1909"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26735 -->
