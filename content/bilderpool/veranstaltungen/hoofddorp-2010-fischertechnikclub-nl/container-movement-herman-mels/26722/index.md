---
layout: "image"
title: "Container movement system"
date: "2010-03-15T19:09:50"
picture: "container1.jpg"
weight: "2"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26722
- /detailsdde3.html
imported:
- "2019"
_4images_image_id: "26722"
_4images_cat_id: "1906"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26722 -->
