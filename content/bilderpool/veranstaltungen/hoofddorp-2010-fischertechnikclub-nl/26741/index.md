---
layout: "image"
title: "Star Heavy Lifter 1:40"
date: "2010-03-15T19:09:51"
picture: "kraan.jpg"
weight: "2"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26741
- /details7257.html
imported:
- "2019"
_4images_image_id: "26741"
_4images_cat_id: "1902"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26741 -->
