---
layout: "image"
title: "Detail of the frame following a cycle of all extensions (again req. ext4)"
date: "2010-03-21T18:38:02"
picture: "ad3.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26788
- /details1b0e.html
imported:
- "2019"
_4images_image_id: "26788"
_4images_cat_id: "1903"
_4images_user_id: "716"
_4images_image_date: "2010-03-21T18:38:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26788 -->
This frane is almost identical to the one at the A-bar, the only difference is the TransactionID which has now increased by 5.