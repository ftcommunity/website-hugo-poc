---
layout: "image"
title: "Ad in action preparing the TX test with 6 TX's"
date: "2010-03-15T19:09:50"
picture: "ad.jpg"
weight: "1"
konstrukteure: 
- "Ad, Richard and Paul"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26707
- /details4310.html
imported:
- "2019"
_4images_image_id: "26707"
_4images_cat_id: "1903"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26707 -->
