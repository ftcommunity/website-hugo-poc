---
layout: "image"
title: "Protocol analysis of the same trace"
date: "2010-03-21T18:38:02"
picture: "ad4.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26789
- /details719a.html
imported:
- "2019"
_4images_image_id: "26789"
_4images_cat_id: "1903"
_4images_user_id: "716"
_4images_image_date: "2010-03-21T18:38:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26789 -->
Visible are the start and stop times of the frames, commands/replies and the TID/SID combinations.
Clearly the Master interrogated the slaves in the sequence 4, 5, 1, 2, 3 and 4 again.