---
layout: "image"
title: "mechanische Noten mit Ketten"
date: "2010-03-15T19:09:50"
picture: "tingel1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/26714
- /details3ae4.html
imported:
- "2019"
_4images_image_id: "26714"
_4images_cat_id: "1904"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26714 -->
