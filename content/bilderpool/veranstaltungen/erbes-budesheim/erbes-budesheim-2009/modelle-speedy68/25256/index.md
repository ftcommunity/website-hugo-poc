---
layout: "image"
title: "speedy68s Achterbahn"
date: "2009-09-23T20:48:31"
picture: "convention041.jpg"
weight: "20"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25256
- /details097c-2.html
imported:
- "2019"
_4images_image_id: "25256"
_4images_cat_id: "1724"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25256 -->
