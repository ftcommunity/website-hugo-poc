---
layout: "image"
title: "Freefall-Achterbahn"
date: "2009-09-19T22:23:07"
picture: "DSC_0013.jpg"
weight: "1"
konstrukteure: 
- "Speedy68"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24979
- /details1514.html
imported:
- "2019"
_4images_image_id: "24979"
_4images_cat_id: "1724"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24979 -->
