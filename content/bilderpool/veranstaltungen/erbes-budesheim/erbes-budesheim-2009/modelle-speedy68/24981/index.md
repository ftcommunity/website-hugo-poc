---
layout: "image"
title: "Looping close up..."
date: "2009-09-19T22:23:07"
picture: "DSC_0025.jpg"
weight: "3"
konstrukteure: 
- "Speedy68"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24981
- /details7ed3-2.html
imported:
- "2019"
_4images_image_id: "24981"
_4images_cat_id: "1724"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24981 -->
