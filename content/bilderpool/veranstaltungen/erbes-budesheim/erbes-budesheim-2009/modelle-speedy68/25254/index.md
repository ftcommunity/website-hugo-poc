---
layout: "image"
title: "speedy68s Achterbahn"
date: "2009-09-23T20:48:31"
picture: "convention039.jpg"
weight: "18"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25254
- /details1d7f-3.html
imported:
- "2019"
_4images_image_id: "25254"
_4images_cat_id: "1724"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25254 -->
Hier starten die Wagen. Das ist tatsächlich alles vollautomatisiert!
