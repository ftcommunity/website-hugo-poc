---
layout: "image"
title: "Propellerflugzeug im Flug"
date: "2009-09-23T20:48:31"
picture: "convention047.jpg"
weight: "8"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25262
- /details70c6.html
imported:
- "2019"
_4images_image_id: "25262"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25262 -->
