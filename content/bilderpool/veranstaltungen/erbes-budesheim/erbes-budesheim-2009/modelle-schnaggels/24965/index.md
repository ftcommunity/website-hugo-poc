---
layout: "image"
title: "RoboMax"
date: "2009-09-19T22:07:23"
picture: "conv1.jpg"
weight: "2"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24965
- /detailscfed-3.html
imported:
- "2019"
_4images_image_id: "24965"
_4images_cat_id: "1737"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:07:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24965 -->
