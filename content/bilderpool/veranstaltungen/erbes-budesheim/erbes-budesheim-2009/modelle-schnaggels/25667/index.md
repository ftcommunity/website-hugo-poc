---
layout: "image"
title: "schnaggels"
date: "2009-11-02T21:41:44"
picture: "verschiedene19.jpg"
weight: "12"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25667
- /detailsa53e-2.html
imported:
- "2019"
_4images_image_id: "25667"
_4images_cat_id: "1737"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25667 -->
Ein paar E-Tech Module wurden hier schon verbaut... Funktion?
