---
layout: "image"
title: "Karussell"
date: "2009-09-23T20:48:30"
picture: "convention010.jpg"
weight: "2"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25225
- /details66a4.html
imported:
- "2019"
_4images_image_id: "25225"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25225 -->
