---
layout: "image"
title: "Dameroboter"
date: "2009-09-19T22:01:56"
picture: "conv2.jpg"
weight: "2"
konstrukteure: 
- "Jan (kehrblech)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24954
- /detailsa34a-2.html
imported:
- "2019"
_4images_image_id: "24954"
_4images_cat_id: "1733"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:01:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24954 -->
