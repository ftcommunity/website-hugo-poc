---
layout: "image"
title: "Kugelroboter"
date: "2009-09-23T20:48:33"
picture: "convention127.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25342
- /details37ad.html
imported:
- "2019"
_4images_image_id: "25342"
_4images_cat_id: "1774"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "127"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25342 -->
