---
layout: "image"
title: "Almost empty hall"
date: "2009-09-22T21:44:13"
picture: "ftconvention01.jpg"
weight: "22"
konstrukteure: 
- "sda"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/25068
- /details781e.html
imported:
- "2019"
_4images_image_id: "25068"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25068 -->
around 9 o'clock.