---
layout: "image"
title: "fischer Tip Bastelecke"
date: "2009-09-23T20:48:29"
picture: "convention004.jpg"
weight: "48"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25219
- /detailse363-4.html
imported:
- "2019"
_4images_image_id: "25219"
_4images_cat_id: "1775"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25219 -->
