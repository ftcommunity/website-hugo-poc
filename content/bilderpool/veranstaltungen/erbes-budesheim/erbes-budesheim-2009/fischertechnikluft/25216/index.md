---
layout: "image"
title: "Verpflegungsbereich"
date: "2009-09-23T20:48:29"
picture: "convention001.jpg"
weight: "45"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25216
- /details3cbd-2.html
imported:
- "2019"
_4images_image_id: "25216"
_4images_cat_id: "1775"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25216 -->
