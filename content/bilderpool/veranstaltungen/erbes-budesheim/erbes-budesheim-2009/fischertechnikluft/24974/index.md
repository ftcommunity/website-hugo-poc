---
layout: "image"
title: "Danke Herr Ralf! und team Knobloch...."
date: "2009-09-19T22:18:54"
picture: "DSC_0036.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24974
- /detailsf081.html
imported:
- "2019"
_4images_image_id: "24974"
_4images_cat_id: "1775"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:18:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24974 -->
