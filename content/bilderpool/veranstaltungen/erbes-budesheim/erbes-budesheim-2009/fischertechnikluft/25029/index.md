---
layout: "image"
title: "Michael Sögtrop"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim27.jpg"
weight: "13"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25029
- /details7bcf-3.html
imported:
- "2019"
_4images_image_id: "25029"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25029 -->
Erbes-Büdesheim-2009
