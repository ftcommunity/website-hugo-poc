---
layout: "image"
title: "Dirk Kutsch"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim32.jpg"
weight: "16"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25034
- /details41a2.html
imported:
- "2019"
_4images_image_id: "25034"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25034 -->
Fischertechnik Transport-2009
