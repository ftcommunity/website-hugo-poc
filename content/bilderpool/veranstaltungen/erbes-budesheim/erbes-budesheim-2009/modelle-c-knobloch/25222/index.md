---
layout: "image"
title: "Mobiler Roboter"
date: "2009-09-23T20:48:30"
picture: "convention007.jpg"
weight: "2"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25222
- /details28e7.html
imported:
- "2019"
_4images_image_id: "25222"
_4images_cat_id: "1786"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25222 -->
