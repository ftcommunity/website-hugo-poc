---
layout: "image"
title: "Modell von Knobloch jr."
date: "2009-09-23T20:48:30"
picture: "convention006.jpg"
weight: "1"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25221
- /details3e75.html
imported:
- "2019"
_4images_image_id: "25221"
_4images_cat_id: "1786"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25221 -->
