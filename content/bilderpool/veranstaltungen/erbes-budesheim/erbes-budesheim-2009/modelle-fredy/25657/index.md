---
layout: "image"
title: "Fredy - Kettenfahrzeug"
date: "2009-11-02T21:41:44"
picture: "verschiedene09.jpg"
weight: "12"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25657
- /details0ec1-2.html
imported:
- "2019"
_4images_image_id: "25657"
_4images_cat_id: "1739"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25657 -->
