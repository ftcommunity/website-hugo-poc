---
layout: "image"
title: "Mobiler Roboter"
date: "2009-09-19T22:09:54"
picture: "conv1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24968
- /details9527.html
imported:
- "2019"
_4images_image_id: "24968"
_4images_cat_id: "1739"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:09:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24968 -->
