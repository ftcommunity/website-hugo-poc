---
layout: "image"
title: "Fredys Prototyp"
date: "2009-09-23T20:48:31"
picture: "convention099.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25314
- /details20e7-2.html
imported:
- "2019"
_4images_image_id: "25314"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25314 -->
