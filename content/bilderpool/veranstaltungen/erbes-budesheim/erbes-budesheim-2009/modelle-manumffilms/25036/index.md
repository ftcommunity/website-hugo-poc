---
layout: "image"
title: "Manu"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim34.jpg"
weight: "6"
konstrukteure: 
- "manumffilms"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25036
- /details264e.html
imported:
- "2019"
_4images_image_id: "25036"
_4images_cat_id: "1731"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25036 -->
Erbes-Büdesheim-2009
