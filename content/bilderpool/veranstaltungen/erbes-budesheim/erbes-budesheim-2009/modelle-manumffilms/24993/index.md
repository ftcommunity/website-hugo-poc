---
layout: "image"
title: "Greifer"
date: "2009-09-19T23:09:05"
picture: "DSC_0030.jpg"
weight: "1"
konstrukteure: 
- "manumffilms"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24993
- /details6194.html
imported:
- "2019"
_4images_image_id: "24993"
_4images_cat_id: "1731"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24993 -->
