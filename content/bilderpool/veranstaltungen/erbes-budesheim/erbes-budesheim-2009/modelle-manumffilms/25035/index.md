---
layout: "image"
title: "Manu"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim33.jpg"
weight: "5"
konstrukteure: 
- "manumffilms"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25035
- /details83d0.html
imported:
- "2019"
_4images_image_id: "25035"
_4images_cat_id: "1731"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25035 -->
Erbes-Büdesheim-2009
