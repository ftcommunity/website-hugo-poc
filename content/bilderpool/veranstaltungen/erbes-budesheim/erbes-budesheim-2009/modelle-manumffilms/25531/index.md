---
layout: "image"
title: "manuMFfilms - 6-Achs Roboter auf Schienen"
date: "2009-10-08T17:22:55"
picture: "verschiedene34.jpg"
weight: "17"
konstrukteure: 
- "manumffilms"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25531
- /details3550-2.html
imported:
- "2019"
_4images_image_id: "25531"
_4images_cat_id: "1731"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25531 -->
Funktioniert!
