---
layout: "image"
title: "manus Roboterarm"
date: "2009-09-23T20:48:31"
picture: "convention029.jpg"
weight: "16"
konstrukteure: 
- "manumffilms"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25244
- /detailsde63.html
imported:
- "2019"
_4images_image_id: "25244"
_4images_cat_id: "1731"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25244 -->
