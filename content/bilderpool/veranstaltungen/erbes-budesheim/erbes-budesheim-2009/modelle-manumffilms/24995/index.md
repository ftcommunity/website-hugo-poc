---
layout: "image"
title: "6-Acher auf Schienen, close up"
date: "2009-09-19T23:09:05"
picture: "DSC_0032.jpg"
weight: "3"
konstrukteure: 
- "manumffilms"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24995
- /details05d1.html
imported:
- "2019"
_4images_image_id: "24995"
_4images_cat_id: "1731"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24995 -->
