---
layout: "image"
title: "ThanksForTheFishs Taschenbedruckmaschine"
date: "2009-09-23T20:48:30"
picture: "convention024.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25239
- /details5316.html
imported:
- "2019"
_4images_image_id: "25239"
_4images_cat_id: "1773"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25239 -->
