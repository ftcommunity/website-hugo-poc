---
layout: "image"
title: "Knobloch GmbH - Radarstation"
date: "2009-10-08T17:22:55"
picture: "verschiedene29.jpg"
weight: "8"
konstrukteure: 
- "Ralf Knobloch"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25526
- /detailsc9cc.html
imported:
- "2019"
_4images_image_id: "25526"
_4images_cat_id: "1787"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25526 -->
Radarstation - Oberteil
