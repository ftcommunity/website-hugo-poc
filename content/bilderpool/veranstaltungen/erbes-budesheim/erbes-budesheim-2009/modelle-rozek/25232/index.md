---
layout: "image"
title: "3D-Drucker"
date: "2009-09-23T20:48:30"
picture: "convention017.jpg"
weight: "10"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25232
- /details4f48-3.html
imported:
- "2019"
_4images_image_id: "25232"
_4images_cat_id: "1751"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25232 -->
