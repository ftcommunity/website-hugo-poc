---
layout: "image"
title: "3D-Drucker"
date: "2009-09-23T20:48:30"
picture: "convention014.jpg"
weight: "7"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25229
- /detailsb78d-2.html
imported:
- "2019"
_4images_image_id: "25229"
_4images_cat_id: "1751"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25229 -->
