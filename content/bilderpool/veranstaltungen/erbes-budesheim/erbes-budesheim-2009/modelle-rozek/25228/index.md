---
layout: "image"
title: "3D-Drucker"
date: "2009-09-23T20:48:30"
picture: "convention013.jpg"
weight: "6"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25228
- /details2c2f.html
imported:
- "2019"
_4images_image_id: "25228"
_4images_cat_id: "1751"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25228 -->
Aus einem im Rechner gespeicherten 3D-Modell baut diese Maschine aus Heißkleber die Form nach. Es gab zwei von solchen 3D-Maschinen auf der Convention.
