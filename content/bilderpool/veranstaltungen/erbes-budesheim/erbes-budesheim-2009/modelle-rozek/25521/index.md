---
layout: "image"
title: "rozek - FTIStrap"
date: "2009-10-08T17:22:55"
picture: "verschiedene24.jpg"
weight: "11"
konstrukteure: 
- "Andreas Rozek"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25521
- /details0c15-2.html
imported:
- "2019"
_4images_image_id: "25521"
_4images_cat_id: "1751"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25521 -->
FTIStrap (voll funktionsfähiger 3D-Drucker) - das Original
