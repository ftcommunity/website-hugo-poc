---
layout: "comment"
hidden: true
title: "10130"
date: "2009-10-25T14:06:36"
uploadBy:
- "Limit"
license: "unknown"
imported:
- "2019"
---
Danke :)
Das Problem bei der Alufolie von der Rolle (auch wenn sie extra dick ist), ist das sie einreißt. Des Wegen hab ich noch eine Frischhaltefolie auf die Alufolie geklebt. Dann hab ich auf die Frischhaltefolie Drähte verlegt. Als nächstes kommt wieder die kombinierte Folie aus Frischhalte- und Alufolie. Kleine Löcher habe ich dann mit Paketklebeband abgeklebt und sicherheitshalber den kompletten Kanal mit transparenten Bootslack überstrichen.
Sollte nun die obere Folienschicht (aus Bootslack, Alu- und Frischhaltefolie) undicht werden, merken das die Drähte, die auf der unteren Schicht liegen und sagen das der Steuerung. Diese kann dann entsprechen reagieren (z.b. bei Abwesenheit Funkklingel über Relais auslösen und/oder Pumpe abschalten).
Gruß, 
Marius