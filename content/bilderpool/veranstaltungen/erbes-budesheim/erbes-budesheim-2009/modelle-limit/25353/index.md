---
layout: "image"
title: "Limits Wildwasserbahn"
date: "2009-09-23T20:48:33"
picture: "convention138.jpg"
weight: "14"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25353
- /detailsbdce.html
imported:
- "2019"
_4images_image_id: "25353"
_4images_cat_id: "1743"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "138"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25353 -->
Die Lampen wirken gleichzeitig als Taster, sehr schöner Effekt.
