---
layout: "image"
title: "Windmühle"
date: "2009-09-23T20:48:31"
picture: "convention076.jpg"
weight: "24"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25291
- /detailsa029.html
imported:
- "2019"
_4images_image_id: "25291"
_4images_cat_id: "1723"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25291 -->
