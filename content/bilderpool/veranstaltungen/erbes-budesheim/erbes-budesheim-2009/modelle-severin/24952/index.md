---
layout: "image"
title: "Windrad"
date: "2009-09-19T21:59:15"
picture: "conv6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24952
- /details3700.html
imported:
- "2019"
_4images_image_id: "24952"
_4images_cat_id: "1723"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:59:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24952 -->
