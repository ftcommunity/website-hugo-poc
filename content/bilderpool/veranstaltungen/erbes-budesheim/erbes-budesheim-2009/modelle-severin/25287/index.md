---
layout: "image"
title: "Roboterarm"
date: "2009-09-23T20:48:31"
picture: "convention072.jpg"
weight: "20"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25287
- /detailsc2af.html
imported:
- "2019"
_4images_image_id: "25287"
_4images_cat_id: "1723"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25287 -->
