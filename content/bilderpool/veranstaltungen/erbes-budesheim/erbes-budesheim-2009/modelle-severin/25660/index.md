---
layout: "image"
title: "Severin - Sechsachsroboter"
date: "2009-11-02T21:41:44"
picture: "verschiedene12.jpg"
weight: "26"
konstrukteure: 
- "Severin"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25660
- /detailsfb6c-2.html
imported:
- "2019"
_4images_image_id: "25660"
_4images_cat_id: "1723"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25660 -->
