---
layout: "image"
title: "Roboterarm"
date: "2009-09-19T21:59:15"
picture: "conv5.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24951
- /details6443.html
imported:
- "2019"
_4images_image_id: "24951"
_4images_cat_id: "1723"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:59:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24951 -->
