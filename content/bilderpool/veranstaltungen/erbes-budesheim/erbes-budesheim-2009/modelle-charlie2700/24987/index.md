---
layout: "image"
title: "andere Variante mit Karl!"
date: "2009-09-19T23:09:05"
picture: "DSC_0033.jpg"
weight: "3"
konstrukteure: 
- "Karl Tillmetz (charlie2700)"
fotografen:
- "Richard R. Budding"
schlagworte: ["m05"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24987
- /detailse286.html
imported:
- "2019"
_4images_image_id: "24987"
_4images_cat_id: "1734"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24987 -->
