---
layout: "image"
title: "Masked - Feefall-Tower"
date: "2009-10-08T17:22:55"
picture: "verschiedene23.jpg"
weight: "8"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25520
- /details046d.html
imported:
- "2019"
_4images_image_id: "25520"
_4images_cat_id: "1736"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25520 -->
Anfangs haben wohl nicht alle ft-Männchen überlebt, aber als es dann mal eingestellt war schon.
