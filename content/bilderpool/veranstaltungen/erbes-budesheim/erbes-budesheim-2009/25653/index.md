---
layout: "image"
title: "niekerk -  FTIStrap 3D-Drucker"
date: "2009-11-02T21:41:43"
picture: "verschiedene05.jpg"
weight: "30"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25653
- /details05d9.html
imported:
- "2019"
_4images_image_id: "25653"
_4images_cat_id: "1721"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25653 -->
