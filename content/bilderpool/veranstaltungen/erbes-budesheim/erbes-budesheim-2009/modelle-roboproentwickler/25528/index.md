---
layout: "image"
title: "ROBOProEntwickler -"
date: "2009-10-08T17:22:55"
picture: "verschiedene31.jpg"
weight: "5"
konstrukteure: 
- "Michael Sögtrop"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25528
- /details92f0.html
imported:
- "2019"
_4images_image_id: "25528"
_4images_cat_id: "1755"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25528 -->
Ausstellungstisch von ROBOProEntwickler
