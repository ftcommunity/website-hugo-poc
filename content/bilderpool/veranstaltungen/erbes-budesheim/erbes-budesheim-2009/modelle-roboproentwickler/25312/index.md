---
layout: "image"
title: "Raumschiff und mobile Roboter"
date: "2009-09-23T20:48:31"
picture: "convention097.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25312
- /details6805-2.html
imported:
- "2019"
_4images_image_id: "25312"
_4images_cat_id: "1755"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25312 -->
