---
layout: "image"
title: "Tresor mit Codeschloss"
date: "2009-09-23T20:48:31"
picture: "convention095.jpg"
weight: "2"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25310
- /details8162.html
imported:
- "2019"
_4images_image_id: "25310"
_4images_cat_id: "1755"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25310 -->
