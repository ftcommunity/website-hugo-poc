---
layout: "image"
title: "Roboter"
date: "2009-09-23T20:48:33"
picture: "convention131.jpg"
weight: "25"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25346
- /details2f76-2.html
imported:
- "2019"
_4images_image_id: "25346"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25346 -->
