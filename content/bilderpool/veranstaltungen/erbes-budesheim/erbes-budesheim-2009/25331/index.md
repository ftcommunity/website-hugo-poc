---
layout: "image"
title: "Carels Robotics Studio-Demo"
date: "2009-09-23T20:48:32"
picture: "convention116.jpg"
weight: "20"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25331
- /detailsc113.html
imported:
- "2019"
_4images_image_id: "25331"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "116"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25331 -->
Das kann ich mal ein bisschen übersetzen:

- VPL - Visual Programming Language: Sieht fast aus wie RoboPro, funktioniert aber ganz anders.

- asynchronous message flow - Anders als bei RoboPro werden zwischen den beteiligten Softwarekomponenten Nachrichten verschickt und nicht Befehle aufgerufen

- DSS - Decentralized Software Service - Damit verteilt man große Robotics-Anwendungen auf verschiedenen Rechner

- CCR - Concurrency and Coordination Runtime - Das ist eine leicht zu benutzende Bibliothek, um parallele Vorgänge automatisch auf mehrere Prozessoren zu verteilen, ohne sich mit den damit verbundenen Details und Fallstricken herumplagen zu müssen. Sie kümmert sich auch um Dinge wie partial failure, also z. B. das Fehlschlagen eines, aber nicht aller parallelen Prozesse, und der Behandlung dieser Situation.

- Visual Simulation Environment: Da steckt eine Physik-Engine dahinter, mit der man Robotics-Anwendungen recht realistisch vorab testen kann, ohne gleich echte Hardware zu brauchen oder kaputt zu machen.

- Unten ist noch ein Diagramm darüber, wie Activities, Services, Messages und Notifications (sowas wie Broadcast-Messages) zusammen wirken.
