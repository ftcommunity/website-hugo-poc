---
layout: "image"
title: "Thkais Servoansteuerung"
date: "2009-09-23T20:48:32"
picture: "convention108.jpg"
weight: "19"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25323
- /detailsfa0f.html
imported:
- "2019"
_4images_image_id: "25323"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "108"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25323 -->
