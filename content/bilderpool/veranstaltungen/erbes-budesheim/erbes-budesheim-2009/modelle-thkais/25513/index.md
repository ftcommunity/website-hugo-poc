---
layout: "image"
title: "thkais - Kugelbahn"
date: "2009-10-08T17:22:54"
picture: "verschiedene16.jpg"
weight: "26"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25513
- /details2f0d.html
imported:
- "2019"
_4images_image_id: "25513"
_4images_cat_id: "1741"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25513 -->
Seitenansicht
