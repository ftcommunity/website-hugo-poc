---
layout: "image"
title: "Servosteuerung"
date: "2009-09-19T22:18:55"
picture: "conv6.jpg"
weight: "6"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24978
- /details6ed5.html
imported:
- "2019"
_4images_image_id: "24978"
_4images_cat_id: "1741"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:18:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24978 -->
