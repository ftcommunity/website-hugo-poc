---
layout: "image"
title: "Thkais Kugeluhr"
date: "2009-09-23T20:48:32"
picture: "convention111.jpg"
weight: "22"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25326
- /details77e8-2.html
imported:
- "2019"
_4images_image_id: "25326"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "111"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25326 -->
Oben die Einerstelle der Minuten, darunter eine gerade leere Bahn für einen Ball alle 5 min, dann drei Bälle in der Bahn für 0 - 11 Uhr.
