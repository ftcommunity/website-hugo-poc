---
layout: "image"
title: "Thkais Kugeluhr"
date: "2009-09-23T20:48:32"
picture: "convention113.jpg"
weight: "24"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25328
- /detailseabd-2.html
imported:
- "2019"
_4images_image_id: "25328"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "113"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25328 -->
Einfach schön, so eine Kugelbahn in grauem ft!
