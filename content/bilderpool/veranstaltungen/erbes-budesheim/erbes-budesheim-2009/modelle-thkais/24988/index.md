---
layout: "image"
title: "Kugelbahn: Clock??"
date: "2009-09-19T23:09:05"
picture: "DSC_0024.jpg"
weight: "7"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24988
- /detailsa894.html
imported:
- "2019"
_4images_image_id: "24988"
_4images_cat_id: "1741"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24988 -->
