---
layout: "image"
title: "thkais - Ufo"
date: "2009-10-08T17:22:54"
picture: "verschiedene17.jpg"
weight: "27"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25514
- /detailsab3a.html
imported:
- "2019"
_4images_image_id: "25514"
_4images_cat_id: "1741"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25514 -->
Sah sehr gut steuerbar aus - Punktlandung...
