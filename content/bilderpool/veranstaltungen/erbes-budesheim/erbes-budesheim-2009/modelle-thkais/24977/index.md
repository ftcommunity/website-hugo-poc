---
layout: "image"
title: "Intelligent Interface"
date: "2009-09-19T22:18:55"
picture: "conv5.jpg"
weight: "5"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24977
- /detailsda8b.html
imported:
- "2019"
_4images_image_id: "24977"
_4images_cat_id: "1741"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:18:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24977 -->
