---
layout: "image"
title: "Mobiler Roboter"
date: "2009-09-23T20:48:31"
picture: "convention091.jpg"
weight: "14"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25306
- /details83f5.html
imported:
- "2019"
_4images_image_id: "25306"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25306 -->
