---
layout: "image"
title: "Screen dump of the MS-VPL application"
date: "2009-09-26T17:57:07"
picture: "ftconvention1.jpg"
weight: "1"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/25362
- /details4e40.html
imported:
- "2019"
_4images_image_id: "25362"
_4images_cat_id: "1752"
_4images_user_id: "136"
_4images_image_date: "2009-09-26T17:57:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25362 -->
The screen dump shows the application that is used during the FT-convention 2009.
