---
layout: "image"
title: "close up Gabellichtschranke"
date: "2009-09-19T22:03:48"
picture: "DSC_0019-1.jpg"
weight: "5"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24958
- /detailsa904.html
imported:
- "2019"
_4images_image_id: "24958"
_4images_cat_id: "1748"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:03:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24958 -->
