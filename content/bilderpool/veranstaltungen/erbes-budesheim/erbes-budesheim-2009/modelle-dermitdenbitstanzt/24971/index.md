---
layout: "image"
title: "close up greifer"
date: "2009-09-19T22:18:54"
picture: "DSC_0020.jpg"
weight: "6"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24971
- /detailsafc6.html
imported:
- "2019"
_4images_image_id: "24971"
_4images_cat_id: "1748"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:18:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24971 -->
