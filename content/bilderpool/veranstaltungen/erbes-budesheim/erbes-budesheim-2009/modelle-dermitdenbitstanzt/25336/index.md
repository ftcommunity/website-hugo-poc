---
layout: "image"
title: "dmdbts Roboterarm"
date: "2009-09-23T20:48:33"
picture: "convention121.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25336
- /detailsa548.html
imported:
- "2019"
_4images_image_id: "25336"
_4images_cat_id: "1748"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "121"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25336 -->
