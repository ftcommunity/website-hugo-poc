---
layout: "image"
title: "BRAIN versus TX"
date: "2009-09-19T23:09:05"
picture: "DSC_0021.jpg"
weight: "1"
konstrukteure: 
- "Henning Leuz"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24991
- /detailsa250.html
imported:
- "2019"
_4images_image_id: "24991"
_4images_cat_id: "1788"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24991 -->
