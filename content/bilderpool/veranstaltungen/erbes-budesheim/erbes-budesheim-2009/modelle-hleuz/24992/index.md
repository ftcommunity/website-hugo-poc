---
layout: "image"
title: "TX versus BRAIN"
date: "2009-09-19T23:09:05"
picture: "DSC_0022.jpg"
weight: "2"
konstrukteure: 
- "Henning Leuz"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24992
- /details9885.html
imported:
- "2019"
_4images_image_id: "24992"
_4images_cat_id: "1788"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24992 -->
