---
layout: "image"
title: "Eingepackt"
date: "2009-09-23T20:48:31"
picture: "convention060.jpg"
weight: "10"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25275
- /details7a0c.html
imported:
- "2019"
_4images_image_id: "25275"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25275 -->
Mitsamt Einkauf, wie man hinten sieht.
