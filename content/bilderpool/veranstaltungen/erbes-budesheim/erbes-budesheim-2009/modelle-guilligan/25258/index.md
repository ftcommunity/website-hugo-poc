---
layout: "image"
title: "Dirk Kutschs Riesenkran"
date: "2009-09-23T20:48:31"
picture: "convention043.jpg"
weight: "12"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25258
- /details4dc5.html
imported:
- "2019"
_4images_image_id: "25258"
_4images_cat_id: "1727"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25258 -->
