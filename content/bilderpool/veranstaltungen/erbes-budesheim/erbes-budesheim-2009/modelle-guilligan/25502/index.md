---
layout: "image"
title: "Guilligan"
date: "2009-10-08T17:22:54"
picture: "verschiedene05.jpg"
weight: "18"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25502
- /details76ba.html
imported:
- "2019"
_4images_image_id: "25502"
_4images_cat_id: "1727"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25502 -->
Samstag nach dem Abendessen. Geschafft! Ist auch noch alles da?
