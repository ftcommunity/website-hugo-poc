---
layout: "image"
title: "Dirk Kutschs Riesenkran"
date: "2009-09-23T20:48:31"
picture: "convention044.jpg"
weight: "13"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25259
- /detailsdc11-2.html
imported:
- "2019"
_4images_image_id: "25259"
_4images_cat_id: "1727"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25259 -->
