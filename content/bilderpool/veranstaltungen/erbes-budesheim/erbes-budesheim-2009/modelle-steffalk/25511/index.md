---
layout: "image"
title: "steffalk - Drucker"
date: "2009-10-08T17:22:54"
picture: "verschiedene14.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25511
- /details532b.html
imported:
- "2019"
_4images_image_id: "25511"
_4images_cat_id: "1772"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25511 -->
Verzweiflung, oder gibt es die Tastatur so gebogen zu kaufen?
