---
layout: "image"
title: "Was da wohl drin war"
date: "2009-09-23T20:48:31"
picture: "convention085.jpg"
weight: "13"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25300
- /details2aa9.html
imported:
- "2019"
_4images_image_id: "25300"
_4images_cat_id: "1735"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25300 -->
