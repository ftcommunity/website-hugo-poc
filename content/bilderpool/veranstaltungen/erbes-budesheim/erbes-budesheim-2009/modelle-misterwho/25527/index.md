---
layout: "image"
title: "MisterWho - Rädchen Sammler, LKW"
date: "2009-10-08T17:22:55"
picture: "verschiedene30.jpg"
weight: "16"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/25527
- /detailsbcd7-3.html
imported:
- "2019"
_4images_image_id: "25527"
_4images_cat_id: "1735"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25527 -->
Rädchen Sammel-Roboter, LKW
