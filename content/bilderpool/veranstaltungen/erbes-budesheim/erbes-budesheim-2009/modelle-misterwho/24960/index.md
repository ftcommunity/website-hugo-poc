---
layout: "image"
title: "Suchroboter"
date: "2009-09-19T22:06:09"
picture: "conv2.jpg"
weight: "5"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/24960
- /details6b7e.html
imported:
- "2019"
_4images_image_id: "24960"
_4images_cat_id: "1735"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:06:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24960 -->
