---
layout: "image"
title: "cleaning bot"
date: "2009-09-19T21:55:25"
picture: "DSC_0006-2.jpg"
weight: "1"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/24941
- /detailsf271-2.html
imported:
- "2019"
_4images_image_id: "24941"
_4images_cat_id: "1735"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T21:55:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24941 -->
