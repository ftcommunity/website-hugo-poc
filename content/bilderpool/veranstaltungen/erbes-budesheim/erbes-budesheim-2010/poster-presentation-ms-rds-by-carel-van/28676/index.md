---
layout: "image"
title: "The French corner (2)"
date: "2010-09-27T23:33:15"
picture: "msrds20.jpg"
weight: "20"
konstrukteure: 
- "Rei_vilo"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28676
- /detailsa89e.html
imported:
- "2019"
_4images_image_id: "28676"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28676 -->
The model of Rei_vilo.
http://sites.google.com/site/vilorei/fischertechnik/magasin-9