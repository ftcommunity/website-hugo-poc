---
layout: "image"
title: "Lovely Koblenz"
date: "2010-09-27T23:33:15"
picture: "msrds27.jpg"
weight: "27"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28683
- /detailsaf5c.html
imported:
- "2019"
_4images_image_id: "28683"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28683 -->
During our trip back to Enschede we pay a small visit to Koblnenz. Koblenz has a very beautiful old city.
