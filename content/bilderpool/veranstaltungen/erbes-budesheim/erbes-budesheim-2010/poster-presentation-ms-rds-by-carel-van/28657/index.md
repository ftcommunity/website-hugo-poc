---
layout: "image"
title: "Residents Villa Frank (1)"
date: "2010-09-27T23:33:14"
picture: "msrds01.jpg"
weight: "1"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28657
- /details4068.html
imported:
- "2019"
_4images_image_id: "28657"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28657 -->
Our residents in Albig during the ft-convention 2010