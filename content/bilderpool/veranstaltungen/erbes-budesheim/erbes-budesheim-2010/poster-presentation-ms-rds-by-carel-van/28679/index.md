---
layout: "image"
title: "The camera technicians"
date: "2010-09-27T23:33:15"
picture: "msrds23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28679
- /details55b7-2.html
imported:
- "2019"
_4images_image_id: "28679"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28679 -->
