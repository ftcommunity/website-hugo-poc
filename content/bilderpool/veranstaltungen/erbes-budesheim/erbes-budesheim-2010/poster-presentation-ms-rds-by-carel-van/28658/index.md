---
layout: "image"
title: "Residents Villa Frank, my workplace (2)"
date: "2010-09-27T23:33:15"
picture: "msrds02.jpg"
weight: "2"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/28658
- /detailsb5d7.html
imported:
- "2019"
_4images_image_id: "28658"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28658 -->
My workplace. Here I did the last preparations.