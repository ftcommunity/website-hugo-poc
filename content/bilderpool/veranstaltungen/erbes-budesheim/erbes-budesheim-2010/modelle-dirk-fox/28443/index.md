---
layout: "image"
title: "Aufzug"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim020.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28443
- /detailsb2f1-2.html
imported:
- "2019"
_4images_image_id: "28443"
_4images_cat_id: "2089"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28443 -->
