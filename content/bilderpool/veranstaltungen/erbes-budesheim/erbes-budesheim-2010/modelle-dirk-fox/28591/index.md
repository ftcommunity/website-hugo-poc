---
layout: "image"
title: "3-Etagenaufzug"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim168.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28591
- /detailsb159.html
imported:
- "2019"
_4images_image_id: "28591"
_4images_cat_id: "2089"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "168"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28591 -->
Tür einer Etage geöffnet
