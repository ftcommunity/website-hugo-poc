---
layout: "image"
title: "Container-Kranwagen"
date: "2010-09-26T19:18:18"
picture: "Container-Kranwagen_-_Arjen_Neijsen_JMN.jpg"
weight: "7"
konstrukteure: 
- "Arjen Neijsen (JMN)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28358
- /details9f99.html
imported:
- "2019"
_4images_image_id: "28358"
_4images_cat_id: "2051"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28358 -->
