---
layout: "image"
title: "Roboter"
date: "2010-09-26T12:18:53"
picture: "roboter4.jpg"
weight: "4"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28266
- /details590a.html
imported:
- "2019"
_4images_image_id: "28266"
_4images_cat_id: "2052"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:18:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28266 -->
