---
layout: "image"
title: "Roboterarme"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim005.jpg"
weight: "10"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28428
- /detailsa35c-2.html
imported:
- "2019"
_4images_image_id: "28428"
_4images_cat_id: "2052"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28428 -->
Verschiedene Arten von Roboterarmen
