---
layout: "image"
title: "Claus Barchfeld"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim34.jpg"
weight: "19"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28861
- /details7456-2.html
imported:
- "2019"
_4images_image_id: "28861"
_4images_cat_id: "2052"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28861 -->
Robotermodelle
