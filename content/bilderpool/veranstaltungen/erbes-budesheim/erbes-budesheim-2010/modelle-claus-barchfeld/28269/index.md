---
layout: "image"
title: "Roboter"
date: "2010-09-26T12:18:53"
picture: "roboter7.jpg"
weight: "7"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28269
- /details0368.html
imported:
- "2019"
_4images_image_id: "28269"
_4images_cat_id: "2052"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:18:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28269 -->
