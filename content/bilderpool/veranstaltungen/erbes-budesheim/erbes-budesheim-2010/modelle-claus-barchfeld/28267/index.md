---
layout: "image"
title: "Roboter"
date: "2010-09-26T12:18:53"
picture: "roboter5.jpg"
weight: "5"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28267
- /details2e94.html
imported:
- "2019"
_4images_image_id: "28267"
_4images_cat_id: "2052"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:18:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28267 -->
