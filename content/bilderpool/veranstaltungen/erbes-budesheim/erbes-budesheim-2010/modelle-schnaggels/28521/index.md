---
layout: "image"
title: "Steuerung vom Hexapod"
date: "2010-09-27T19:56:23"
picture: "fischertechnikconventioninerbesbuedesheim098.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28521
- /details50bd.html
imported:
- "2019"
_4images_image_id: "28521"
_4images_cat_id: "2083"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:23"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28521 -->
Viel Roboter- und Pneumatiktechnik ist notwendig um das Hexapod zu steuern
