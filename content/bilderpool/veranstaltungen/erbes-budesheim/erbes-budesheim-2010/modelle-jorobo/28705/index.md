---
layout: "image"
title: "Modell von jorobo"
date: "2010-09-28T17:22:38"
picture: "dfg3.jpg"
weight: "5"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28705
- /details3cfb.html
imported:
- "2019"
_4images_image_id: "28705"
_4images_cat_id: "2092"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T17:22:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28705 -->
