---
layout: "image"
title: "Coaster"
date: "2010-09-26T16:07:13"
picture: "eb21.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28303
- /details1544-3.html
imported:
- "2019"
_4images_image_id: "28303"
_4images_cat_id: "2092"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28303 -->
