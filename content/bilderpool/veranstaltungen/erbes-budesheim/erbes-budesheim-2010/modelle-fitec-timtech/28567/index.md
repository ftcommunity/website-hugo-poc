---
layout: "image"
title: "Hochregal Einsortierer"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim144.jpg"
weight: "9"
konstrukteure: 
- "Tim Ronellenfitsch (timtech)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28567
- /detailseadd.html
imported:
- "2019"
_4images_image_id: "28567"
_4images_cat_id: "2075"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "144"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28567 -->
