---
layout: "image"
title: "Hochregallager"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim037.jpg"
weight: "4"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28460
- /details73a7.html
imported:
- "2019"
_4images_image_id: "28460"
_4images_cat_id: "2075"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28460 -->
