---
layout: "image"
title: "fischertechnikconventioninerbesbuedesheim039.jpg"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim039.jpg"
weight: "5"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28462
- /detailsba00.html
imported:
- "2019"
_4images_image_id: "28462"
_4images_cat_id: "2075"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28462 -->
