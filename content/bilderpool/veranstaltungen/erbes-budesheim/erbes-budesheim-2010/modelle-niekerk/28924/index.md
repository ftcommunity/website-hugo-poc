---
layout: "image"
title: "..."
date: "2010-10-03T15:00:57"
picture: "APP-2010-025038.jpg"
weight: "9"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28924
- /detailsfb53.html
imported:
- "2019"
_4images_image_id: "28924"
_4images_cat_id: "2102"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28924 -->
