---
layout: "image"
title: "3D-Drucker Gesamte Anlage"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim031.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28454
- /detailsb757.html
imported:
- "2019"
_4images_image_id: "28454"
_4images_cat_id: "2102"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28454 -->
