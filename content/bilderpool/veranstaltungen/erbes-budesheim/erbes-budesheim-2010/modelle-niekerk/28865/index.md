---
layout: "image"
title: "Paul van Niekerk"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim38.jpg"
weight: "7"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28865
- /details1954.html
imported:
- "2019"
_4images_image_id: "28865"
_4images_cat_id: "2102"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28865 -->
3D-Drucker
