---
layout: "image"
title: "Traktor"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim125.jpg"
weight: "7"
konstrukteure: 
- "Dieter Meckel (Dinomania01)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28548
- /detailsec2c.html
imported:
- "2019"
_4images_image_id: "28548"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "125"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28548 -->
Ich habe diesen Bausatz auf der Convention bei Knobloch gekauft und dann gleich an Ort und Stelle zusammengebaut. Anschließend dann zu den anderen Modellen vor mir dazugestellt.
