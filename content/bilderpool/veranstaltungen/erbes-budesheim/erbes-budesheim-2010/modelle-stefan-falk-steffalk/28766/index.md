---
layout: "image"
title: "Obertodesteufelsdriver"
date: "2010-09-29T20:02:42"
picture: "steffalk1.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28766
- /detailsf039-2.html
imported:
- "2019"
_4images_image_id: "28766"
_4images_cat_id: "2062"
_4images_user_id: "997"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28766 -->
tolles Modell, vorallem brauchts für den fischertechnik Looping nicht mal eine so hohe Anfangshöhe...
