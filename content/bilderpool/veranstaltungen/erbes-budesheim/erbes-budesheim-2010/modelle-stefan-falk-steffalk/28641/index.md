---
layout: "image"
title: "Papierspender"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim218.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28641
- /details43b4.html
imported:
- "2019"
_4images_image_id: "28641"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "218"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28641 -->
