---
layout: "image"
title: "Papierwendeanlage (2)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim157.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28580
- /detailsd106.html
imported:
- "2019"
_4images_image_id: "28580"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "157"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28580 -->
