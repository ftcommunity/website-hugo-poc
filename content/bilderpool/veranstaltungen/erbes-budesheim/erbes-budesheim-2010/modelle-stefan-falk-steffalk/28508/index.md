---
layout: "image"
title: "Elektronische Henne"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim085.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28508
- /details1856-2.html
imported:
- "2019"
_4images_image_id: "28508"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28508 -->
Gack, gack, gaack
