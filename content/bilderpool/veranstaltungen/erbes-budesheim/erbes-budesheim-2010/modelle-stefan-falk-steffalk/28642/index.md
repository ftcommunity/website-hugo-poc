---
layout: "image"
title: "Energieversorgungsturm"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim219.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28642
- /details225f-3.html
imported:
- "2019"
_4images_image_id: "28642"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "219"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28642 -->
