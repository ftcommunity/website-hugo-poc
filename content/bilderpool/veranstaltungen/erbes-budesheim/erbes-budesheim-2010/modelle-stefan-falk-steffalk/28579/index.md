---
layout: "image"
title: "Papierwendeanlage"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim156.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28579
- /details05ba.html
imported:
- "2019"
_4images_image_id: "28579"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "156"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28579 -->
