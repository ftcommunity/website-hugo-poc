---
layout: "image"
title: "Achterbahn"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim080.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28503
- /details6bf4.html
imported:
- "2019"
_4images_image_id: "28503"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28503 -->
