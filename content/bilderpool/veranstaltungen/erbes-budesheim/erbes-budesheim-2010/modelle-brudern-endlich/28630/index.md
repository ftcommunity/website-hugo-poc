---
layout: "image"
title: "Getränkeautomat"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim207.jpg"
weight: "19"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28630
- /details0a38.html
imported:
- "2019"
_4images_image_id: "28630"
_4images_cat_id: "2058"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "207"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28630 -->
