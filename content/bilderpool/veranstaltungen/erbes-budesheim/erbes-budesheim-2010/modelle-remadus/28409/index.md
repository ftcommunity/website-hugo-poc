---
layout: "image"
title: "Zwiesprache"
date: "2010-09-27T17:30:31"
picture: "IMG_3849_Remadus.JPG"
weight: "17"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/28409
- /detailsba67.html
imported:
- "2019"
_4images_image_id: "28409"
_4images_cat_id: "2050"
_4images_user_id: "4"
_4images_image_date: "2010-09-27T17:30:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28409 -->
Ich glaube nicht, dass Remadus sein Werk anbetet (na ja, wer weiß?). Es sieht eher danach aus, dass er der Uhr gerade Leben einhaucht.
