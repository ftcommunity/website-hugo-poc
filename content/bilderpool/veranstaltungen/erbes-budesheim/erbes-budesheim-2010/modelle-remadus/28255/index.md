---
layout: "image"
title: "Uhr"
date: "2010-09-26T12:10:02"
picture: "uhr5.jpg"
weight: "5"
konstrukteure: 
- "remadus"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28255
- /details59c3.html
imported:
- "2019"
_4images_image_id: "28255"
_4images_cat_id: "2050"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:10:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28255 -->
