---
layout: "image"
title: "Standuhr"
date: "2010-09-27T18:07:26"
picture: "ser4.jpg"
weight: "21"
konstrukteure: 
- "remadus"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28422
- /detailsf428-2.html
imported:
- "2019"
_4images_image_id: "28422"
_4images_cat_id: "2050"
_4images_user_id: "1162"
_4images_image_date: "2010-09-27T18:07:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28422 -->
