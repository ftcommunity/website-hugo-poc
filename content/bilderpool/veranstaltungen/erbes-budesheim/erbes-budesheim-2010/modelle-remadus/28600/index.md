---
layout: "image"
title: "Uhr"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim177.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28600
- /details8665.html
imported:
- "2019"
_4images_image_id: "28600"
_4images_cat_id: "2050"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "177"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28600 -->
Die Mechanik der Uhr
