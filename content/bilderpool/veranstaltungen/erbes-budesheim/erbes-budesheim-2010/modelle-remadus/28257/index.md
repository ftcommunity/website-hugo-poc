---
layout: "image"
title: "Uhr"
date: "2010-09-26T12:10:02"
picture: "uhr7.jpg"
weight: "7"
konstrukteure: 
- "remadus"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28257
- /detailscf42.html
imported:
- "2019"
_4images_image_id: "28257"
_4images_cat_id: "2050"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:10:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28257 -->
