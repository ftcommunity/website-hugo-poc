---
layout: "image"
title: "Martin Romann"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim04.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28831
- /details5277.html
imported:
- "2019"
_4images_image_id: "28831"
_4images_cat_id: "2050"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28831 -->
Standuhr
