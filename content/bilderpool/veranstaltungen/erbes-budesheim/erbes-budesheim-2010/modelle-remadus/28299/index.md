---
layout: "image"
title: "Pendeluhr"
date: "2010-09-26T16:06:54"
picture: "eb17.jpg"
weight: "10"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28299
- /details0213-3.html
imported:
- "2019"
_4images_image_id: "28299"
_4images_cat_id: "2050"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:54"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28299 -->
