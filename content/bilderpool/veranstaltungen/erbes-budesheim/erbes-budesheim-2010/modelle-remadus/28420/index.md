---
layout: "image"
title: "Standuhr"
date: "2010-09-27T18:07:26"
picture: "ser2.jpg"
weight: "19"
konstrukteure: 
- "remadus"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28420
- /detailsa938-2.html
imported:
- "2019"
_4images_image_id: "28420"
_4images_cat_id: "2050"
_4images_user_id: "1162"
_4images_image_date: "2010-09-27T18:07:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28420 -->
