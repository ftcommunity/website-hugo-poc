---
layout: "image"
title: "Pendeluhr"
date: "2010-09-26T16:07:13"
picture: "eb26.jpg"
weight: "12"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28308
- /details0a74.html
imported:
- "2019"
_4images_image_id: "28308"
_4images_cat_id: "2050"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28308 -->
