---
layout: "image"
title: "Uhr"
date: "2010-09-26T12:10:02"
picture: "uhr4.jpg"
weight: "4"
konstrukteure: 
- "remadus"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28254
- /details0878-3.html
imported:
- "2019"
_4images_image_id: "28254"
_4images_cat_id: "2050"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:10:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28254 -->
