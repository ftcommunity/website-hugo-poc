---
layout: "image"
title: "First Drop"
date: "2010-09-26T11:59:16"
picture: "FTC2010_19.jpg"
weight: "12"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Sebastian Schräder"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- /php/details/28245
- /details1172.html
imported:
- "2019"
_4images_image_id: "28245"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28245 -->
