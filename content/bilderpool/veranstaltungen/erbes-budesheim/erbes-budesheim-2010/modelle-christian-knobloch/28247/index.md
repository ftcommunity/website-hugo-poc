---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T12:03:26"
picture: "achterbahn1.jpg"
weight: "14"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28247
- /details3b4a-3.html
imported:
- "2019"
_4images_image_id: "28247"
_4images_cat_id: "2049"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:03:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28247 -->
