---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T11:55:16"
picture: "FTC2010_14.jpg"
weight: "10"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Sebastian Schräder"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- /php/details/28243
- /detailsb0b6.html
imported:
- "2019"
_4images_image_id: "28243"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:55:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28243 -->
