---
layout: "image"
title: "."
date: "2010-10-03T15:00:55"
picture: "APP-2010-025001.jpg"
weight: "78"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28890
- /details3c45.html
imported:
- "2019"
_4images_image_id: "28890"
_4images_cat_id: "2049"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28890 -->
