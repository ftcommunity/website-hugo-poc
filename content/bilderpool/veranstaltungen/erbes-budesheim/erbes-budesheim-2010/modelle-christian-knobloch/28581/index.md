---
layout: "image"
title: "Firestorm-Megacoaster"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim158.jpg"
weight: "52"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28581
- /details2f17.html
imported:
- "2019"
_4images_image_id: "28581"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "158"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28581 -->
