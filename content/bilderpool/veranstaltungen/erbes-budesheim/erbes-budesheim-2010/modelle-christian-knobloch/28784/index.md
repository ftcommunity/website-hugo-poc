---
layout: "image"
title: "Fahrwerk mit Kugellager"
date: "2010-09-30T19:17:28"
picture: "firestorm_upload01.jpg"
weight: "61"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28784
- /details0cbe-3.html
imported:
- "2019"
_4images_image_id: "28784"
_4images_cat_id: "2049"
_4images_user_id: "997"
_4images_image_date: "2010-09-30T19:17:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28784 -->
selbst hergestellt, aus Aluminium
