---
layout: "comment"
hidden: true
title: "12312"
date: "2010-09-27T16:49:04"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Na klaro war der Tarnkappenbomber wieder mit dabei!

In der Anmeldeliste der Convention stands auch geschrieben, unter "Harald": Zwei Großflugzeuge, ein Panzer, etwas Kleinzeug. In Worten: z-w-e-i Großflugzeuge. Eins hing am Seil über meinem Stand. Das andere hing auch am Seil, aber dem anderen, das hier im Bild zu sehen ist :-)

Gruß,
Harald