---
layout: "image"
title: "Full action!"
date: "2010-10-03T15:00:55"
picture: "APP-2010-025006.jpg"
weight: "83"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28895
- /detailsee37-2.html
imported:
- "2019"
_4images_image_id: "28895"
_4images_cat_id: "2049"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28895 -->
