---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T16:06:54"
picture: "eb12.jpg"
weight: "25"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28294
- /details8b3d.html
imported:
- "2019"
_4images_image_id: "28294"
_4images_cat_id: "2049"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28294 -->
