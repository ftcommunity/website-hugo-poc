---
layout: "image"
title: "Firestorm-Megacoaster"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim195.jpg"
weight: "53"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28618
- /details920b.html
imported:
- "2019"
_4images_image_id: "28618"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "195"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28618 -->
Die Wagen vor der Einfahrt in die Station in Warteposition
