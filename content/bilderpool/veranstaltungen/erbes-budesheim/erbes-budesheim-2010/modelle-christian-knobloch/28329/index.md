---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:41"
picture: "achterbahn15.jpg"
weight: "44"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28329
- /details4dcf-2.html
imported:
- "2019"
_4images_image_id: "28329"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:41"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28329 -->
