---
layout: "image"
title: "Cristian Knobloch"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim16.jpg"
weight: "74"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28843
- /details8815.html
imported:
- "2019"
_4images_image_id: "28843"
_4images_cat_id: "2049"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28843 -->
Firestorm Megacoaster
