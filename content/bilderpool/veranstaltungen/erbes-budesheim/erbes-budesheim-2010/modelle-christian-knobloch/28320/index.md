---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:35"
picture: "achterbahn06.jpg"
weight: "35"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28320
- /detailsef53.html
imported:
- "2019"
_4images_image_id: "28320"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28320 -->
