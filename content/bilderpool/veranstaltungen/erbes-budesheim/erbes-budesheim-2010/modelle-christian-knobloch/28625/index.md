---
layout: "image"
title: "Die Station"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim202.jpg"
weight: "60"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28625
- /detailsd8fb.html
imported:
- "2019"
_4images_image_id: "28625"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "202"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28625 -->
