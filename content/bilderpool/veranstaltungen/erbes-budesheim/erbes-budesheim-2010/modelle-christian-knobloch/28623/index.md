---
layout: "image"
title: "Energieversorgung"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim200.jpg"
weight: "58"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28623
- /detailsf2bb-2.html
imported:
- "2019"
_4images_image_id: "28623"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "200"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28623 -->
