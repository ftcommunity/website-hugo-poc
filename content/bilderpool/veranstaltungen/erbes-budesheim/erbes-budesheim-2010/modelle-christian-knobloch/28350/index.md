---
layout: "image"
title: "Firestorm (Seitenansicht)"
date: "2010-09-26T18:13:32"
picture: "Firestorm_03_-_Christian_Knobloch.jpg"
weight: "46"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28350
- /detailsa153.html
imported:
- "2019"
_4images_image_id: "28350"
_4images_cat_id: "2049"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28350 -->
