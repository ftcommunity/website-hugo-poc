---
layout: "image"
title: "Lifthill"
date: "2010-09-26T11:53:56"
picture: "FTC2010_01.jpg"
weight: "1"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Sebastian Schräder"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- /php/details/28234
- /details4730.html
imported:
- "2019"
_4images_image_id: "28234"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:53:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28234 -->
