---
layout: "image"
title: "Bahnen des Firestorm-Megacoasters"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim139.jpg"
weight: "50"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28562
- /detailsf74f.html
imported:
- "2019"
_4images_image_id: "28562"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "139"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28562 -->
