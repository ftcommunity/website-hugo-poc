---
layout: "image"
title: "ready to launch..."
date: "2010-10-03T15:00:56"
picture: "APP-2010-025007.jpg"
weight: "84"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28896
- /detailsc4d3.html
imported:
- "2019"
_4images_image_id: "28896"
_4images_cat_id: "2049"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28896 -->
