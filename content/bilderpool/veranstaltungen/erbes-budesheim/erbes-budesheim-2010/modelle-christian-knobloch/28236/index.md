---
layout: "image"
title: "Wirbelstrombremse"
date: "2010-09-26T11:53:57"
picture: "FTC2010_03.jpg"
weight: "3"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Sebastian Schräder"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- /php/details/28236
- /details3140.html
imported:
- "2019"
_4images_image_id: "28236"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:53:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28236 -->
