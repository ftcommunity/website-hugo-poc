---
layout: "image"
title: "First Drop"
date: "2010-09-26T11:59:16"
picture: "FTC2010_20.jpg"
weight: "13"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Sebastian Schräder"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- /php/details/28246
- /details0535-2.html
imported:
- "2019"
_4images_image_id: "28246"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28246 -->
