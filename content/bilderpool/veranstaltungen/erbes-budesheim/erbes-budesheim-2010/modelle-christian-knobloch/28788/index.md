---
layout: "image"
title: "Wirbelstrombremse"
date: "2010-09-30T19:17:29"
picture: "firestorm_upload05.jpg"
weight: "65"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28788
- /detailsc036.html
imported:
- "2019"
_4images_image_id: "28788"
_4images_cat_id: "2049"
_4images_user_id: "997"
_4images_image_date: "2010-09-30T19:17:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28788 -->
