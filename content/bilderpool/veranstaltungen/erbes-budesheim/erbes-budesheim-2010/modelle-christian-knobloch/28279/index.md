---
layout: "image"
title: "Firestorm-Megacoaster"
date: "2010-09-26T14:33:44"
picture: "firestorm1.jpg"
weight: "18"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/28279
- /details8537.html
imported:
- "2019"
_4images_image_id: "28279"
_4images_cat_id: "2049"
_4images_user_id: "1007"
_4images_image_date: "2010-09-26T14:33:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28279 -->
Das ist die ganze Achterbahn von Christian Knobloch
