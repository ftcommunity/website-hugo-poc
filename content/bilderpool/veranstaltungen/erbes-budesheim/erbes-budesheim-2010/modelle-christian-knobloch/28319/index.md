---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:35"
picture: "achterbahn05.jpg"
weight: "34"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28319
- /detailsf7eb.html
imported:
- "2019"
_4images_image_id: "28319"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28319 -->
