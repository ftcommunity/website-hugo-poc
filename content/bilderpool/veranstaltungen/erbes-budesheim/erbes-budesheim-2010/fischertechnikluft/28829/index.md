---
layout: "image"
title: "FT-Luft in der nähe Erbes-Budesheim entlang der Rhein"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim02.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28829
- /detailse0cc.html
imported:
- "2019"
_4images_image_id: "28829"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28829 -->
FT-Luft in der nähe Erbes-Budesheim entlang der Rhein
