---
layout: "image"
title: "Fischer-TIP"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim35.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28862
- /detailsb8d4.html
imported:
- "2019"
_4images_image_id: "28862"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28862 -->
Fischer-TIP
