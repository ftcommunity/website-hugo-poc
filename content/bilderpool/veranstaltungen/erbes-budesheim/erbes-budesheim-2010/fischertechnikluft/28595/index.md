---
layout: "image"
title: "Harald Steinhaus und Thomas Brestrich"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim172.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28595
- /details1a83.html
imported:
- "2019"
_4images_image_id: "28595"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "172"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28595 -->
