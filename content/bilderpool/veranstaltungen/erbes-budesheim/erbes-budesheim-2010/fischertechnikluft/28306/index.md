---
layout: "image"
title: "Bastelecke"
date: "2010-09-26T16:07:13"
picture: "eb24.jpg"
weight: "4"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28306
- /details26d3.html
imported:
- "2019"
_4images_image_id: "28306"
_4images_cat_id: "2063"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28306 -->
