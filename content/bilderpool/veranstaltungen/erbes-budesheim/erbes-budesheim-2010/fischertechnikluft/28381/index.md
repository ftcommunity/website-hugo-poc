---
layout: "image"
title: "Security"
date: "2010-09-26T19:45:22"
picture: "fischertechnikluft16.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28381
- /detailsf0fd.html
imported:
- "2019"
_4images_image_id: "28381"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28381 -->
Dieser Mann hat dafür gesorgt, dass die Aussteller ein open-end hatten und dass den Modellen nachts nichts passiert. Danke dafür ;)
