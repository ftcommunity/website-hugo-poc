---
layout: "image"
title: "Holländische Versteigerung"
date: "2010-09-26T16:06:48"
picture: "eb06.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28288
- /detailsff36-2.html
imported:
- "2019"
_4images_image_id: "28288"
_4images_cat_id: "2063"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28288 -->
