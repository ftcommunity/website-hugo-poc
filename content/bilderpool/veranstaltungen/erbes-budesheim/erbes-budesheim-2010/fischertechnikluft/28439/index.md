---
layout: "image"
title: "Zwei Aussteller"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim016.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28439
- /detailsb36f.html
imported:
- "2019"
_4images_image_id: "28439"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28439 -->
