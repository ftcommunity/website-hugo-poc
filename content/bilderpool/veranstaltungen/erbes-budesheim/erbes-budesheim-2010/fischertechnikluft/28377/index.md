---
layout: "image"
title: "Aussteller-Abendessen am Samstag"
date: "2010-09-26T19:45:22"
picture: "fischertechnikluft12.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28377
- /details871e-3.html
imported:
- "2019"
_4images_image_id: "28377"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:22"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28377 -->
