---
layout: "image"
title: "Harald beim Kräftemessen"
date: "2010-09-26T19:45:22"
picture: "fischertechnikluft15.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "C-Knobloch"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- /php/details/28380
- /detailsaf07-2.html
imported:
- "2019"
_4images_image_id: "28380"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28380 -->
mit Zahnspangengummis (werden als Federung bei einem Kettenfahrzeug verwendet)
