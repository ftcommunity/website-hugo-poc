---
layout: "image"
title: "[5/5] Leitspindel-Drehmaschine DL1 mit Power Supply"
date: "2010-09-30T14:02:18"
picture: "convention5.jpg"
weight: "12"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28783
- /details6396.html
imported:
- "2019"
_4images_image_id: "28783"
_4images_cat_id: "2064"
_4images_user_id: "723"
_4images_image_date: "2010-09-30T14:02:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28783 -->
Ausführliche Beschreibung unter http://www.ftcommunity.de/details.php?image_id=28101
