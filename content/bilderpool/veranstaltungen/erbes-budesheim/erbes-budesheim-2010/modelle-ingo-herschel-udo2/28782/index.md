---
layout: "image"
title: "[4/5] Portalroboter 3D-XYZ-GES, Steuerung"
date: "2010-09-30T14:02:18"
picture: "convention4.jpg"
weight: "11"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/28782
- /details5671.html
imported:
- "2019"
_4images_image_id: "28782"
_4images_cat_id: "2064"
_4images_user_id: "723"
_4images_image_date: "2010-09-30T14:02:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28782 -->
Link zur Beschreibung unter Bild [3/5]
