---
layout: "image"
title: "Antrieb der Drehmaschine"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim163.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28586
- /detailse921.html
imported:
- "2019"
_4images_image_id: "28586"
_4images_cat_id: "2064"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "163"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28586 -->
