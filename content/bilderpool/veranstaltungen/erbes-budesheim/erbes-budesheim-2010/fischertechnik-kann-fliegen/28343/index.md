---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:41"
picture: "ssfs14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28343
- /detailse487.html
imported:
- "2019"
_4images_image_id: "28343"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:41"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28343 -->
