---
layout: "image"
title: "Eisenbahnkupplung"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim059.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28482
- /details6e0d.html
imported:
- "2019"
_4images_image_id: "28482"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28482 -->
