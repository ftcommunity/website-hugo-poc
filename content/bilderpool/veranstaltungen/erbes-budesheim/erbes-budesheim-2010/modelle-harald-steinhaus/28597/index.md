---
layout: "image"
title: "Flieger"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim174.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28597
- /details2b20.html
imported:
- "2019"
_4images_image_id: "28597"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "174"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28597 -->
