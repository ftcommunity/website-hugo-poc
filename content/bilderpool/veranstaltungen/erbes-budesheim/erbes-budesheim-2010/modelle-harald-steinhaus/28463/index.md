---
layout: "image"
title: "Geländwagen"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim040.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28463
- /details218f.html
imported:
- "2019"
_4images_image_id: "28463"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28463 -->
