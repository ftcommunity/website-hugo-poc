---
layout: "image"
title: "Harald Steinhaus"
date: "2010-10-02T23:55:10"
picture: "2010-Erbes-Budesheim_087.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28812
- /detailsdde0.html
imported:
- "2019"
_4images_image_id: "28812"
_4images_cat_id: "2060"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28812 -->
