---
layout: "image"
title: "Details des Fliegers"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim068.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28491
- /detailsa01f.html
imported:
- "2019"
_4images_image_id: "28491"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28491 -->
