---
layout: "image"
title: "Tarnkappenbomber"
date: "2010-09-27T17:39:09"
picture: "IMG_4000_Tarnkappenbomber.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/28410
- /details1f75.html
imported:
- "2019"
_4images_image_id: "28410"
_4images_cat_id: "2060"
_4images_user_id: "4"
_4images_image_date: "2010-09-27T17:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28410 -->
Ja, er war auch dort!
Am langen Seil, und immer schön der Seilkamera hinterher. Ein paarmal sind sie aber doch zusammengerasselt; das erklärt die Hopser im Video.
