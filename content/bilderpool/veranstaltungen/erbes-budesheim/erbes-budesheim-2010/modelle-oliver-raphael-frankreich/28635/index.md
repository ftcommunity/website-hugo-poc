---
layout: "image"
title: "Sortieranlage (?) (3)"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim212.jpg"
weight: "5"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28635
- /details3992-2.html
imported:
- "2019"
_4images_image_id: "28635"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "212"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28635 -->
