---
layout: "image"
title: "Raphael / Oliver"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim19.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28846
- /details4709.html
imported:
- "2019"
_4images_image_id: "28846"
_4images_cat_id: "2076"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28846 -->
Neue Entwicklungen
