---
layout: "image"
title: "Sortieranlage für Räder"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim049.jpg"
weight: "1"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28472
- /detailsc650.html
imported:
- "2019"
_4images_image_id: "28472"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28472 -->
