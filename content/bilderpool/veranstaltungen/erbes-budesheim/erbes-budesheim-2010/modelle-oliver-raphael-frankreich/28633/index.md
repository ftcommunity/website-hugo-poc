---
layout: "image"
title: "Sortieranlage (?)"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim210.jpg"
weight: "3"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28633
- /details7ad7.html
imported:
- "2019"
_4images_image_id: "28633"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "210"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28633 -->
