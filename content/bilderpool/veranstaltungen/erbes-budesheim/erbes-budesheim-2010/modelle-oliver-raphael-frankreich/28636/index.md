---
layout: "image"
title: "Sortieranlage (?) (4)"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim213.jpg"
weight: "6"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28636
- /detailsc284.html
imported:
- "2019"
_4images_image_id: "28636"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "213"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28636 -->
