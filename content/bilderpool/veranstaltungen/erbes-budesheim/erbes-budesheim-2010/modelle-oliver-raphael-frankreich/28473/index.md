---
layout: "image"
title: "Sortieranlage für Räder"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim050.jpg"
weight: "2"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28473
- /details95ea-2.html
imported:
- "2019"
_4images_image_id: "28473"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28473 -->
