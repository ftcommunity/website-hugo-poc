---
layout: "image"
title: ":-)"
date: "2010-10-03T15:00:56"
picture: "APP-2010-025029.jpg"
weight: "6"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28914
- /details4240.html
imported:
- "2019"
_4images_image_id: "28914"
_4images_cat_id: "2080"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28914 -->
