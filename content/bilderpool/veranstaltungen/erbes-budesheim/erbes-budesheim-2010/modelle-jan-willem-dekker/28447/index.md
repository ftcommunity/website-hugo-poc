---
layout: "image"
title: "Dampfmaschine"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim024.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28447
- /details2131.html
imported:
- "2019"
_4images_image_id: "28447"
_4images_cat_id: "2080"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28447 -->
