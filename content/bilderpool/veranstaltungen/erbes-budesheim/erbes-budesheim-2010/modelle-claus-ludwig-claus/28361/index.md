---
layout: "image"
title: "LKW"
date: "2010-09-26T19:18:18"
picture: "LKW_-_Claus_Ludwig_claus.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28361
- /detailse7bb.html
imported:
- "2019"
_4images_image_id: "28361"
_4images_cat_id: "2061"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28361 -->
Detaildarstellung unter
http://www.ftcommunity.de/categories.php?cat_id=1717
