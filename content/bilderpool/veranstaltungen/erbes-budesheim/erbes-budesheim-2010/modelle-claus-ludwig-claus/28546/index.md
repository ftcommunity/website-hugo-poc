---
layout: "image"
title: "Mülltransportauflieger"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim123.jpg"
weight: "17"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28546
- /detailsee10-2.html
imported:
- "2019"
_4images_image_id: "28546"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "123"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28546 -->
