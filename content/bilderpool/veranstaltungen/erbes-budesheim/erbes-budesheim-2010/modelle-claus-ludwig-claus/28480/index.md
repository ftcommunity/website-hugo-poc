---
layout: "image"
title: "Drehmaschine"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim057.jpg"
weight: "15"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28480
- /detailsae69-2.html
imported:
- "2019"
_4images_image_id: "28480"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28480 -->
Spanneinheit mit Drehteil und Werkzeugschlittten
