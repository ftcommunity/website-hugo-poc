---
layout: "image"
title: "Details des Müllwagens"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim204.jpg"
weight: "20"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28627
- /details929b.html
imported:
- "2019"
_4images_image_id: "28627"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "204"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28627 -->
