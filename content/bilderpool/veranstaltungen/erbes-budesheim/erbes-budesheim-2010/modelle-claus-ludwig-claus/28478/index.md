---
layout: "image"
title: "Mähdrescher"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim055.jpg"
weight: "13"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28478
- /detailse674.html
imported:
- "2019"
_4images_image_id: "28478"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28478 -->
