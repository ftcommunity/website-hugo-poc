---
layout: "image"
title: "Claus Ludwig"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim24.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28851
- /detailsdd3c-2.html
imported:
- "2019"
_4images_image_id: "28851"
_4images_cat_id: "2061"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28851 -->
Mahdrescher, Traktoren und andere Modellen
