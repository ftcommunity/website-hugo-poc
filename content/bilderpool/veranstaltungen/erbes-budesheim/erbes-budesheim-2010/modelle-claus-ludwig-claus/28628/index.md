---
layout: "image"
title: "Pneumatik-  undc Elektronikeinheit"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim205.jpg"
weight: "21"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28628
- /details83dc.html
imported:
- "2019"
_4images_image_id: "28628"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "205"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28628 -->
