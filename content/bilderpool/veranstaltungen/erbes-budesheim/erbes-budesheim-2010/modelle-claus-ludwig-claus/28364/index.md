---
layout: "image"
title: "Müllwagen"
date: "2010-09-26T19:30:27"
picture: "Mllwagen_-_Claus_Ludwig_claus.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28364
- /details06fb.html
imported:
- "2019"
_4images_image_id: "28364"
_4images_cat_id: "2061"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:30:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28364 -->
Detaildarstellung unter
http://www.ftcommunity.de/categories.php?cat_id=742&page=1
