---
layout: "image"
title: "Zugmaschine"
date: "2010-09-26T19:18:18"
picture: "Zugmaschine_-_Claus_Ludwig_claus.jpg"
weight: "6"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28363
- /details1d81.html
imported:
- "2019"
_4images_image_id: "28363"
_4images_cat_id: "2061"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T19:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28363 -->
