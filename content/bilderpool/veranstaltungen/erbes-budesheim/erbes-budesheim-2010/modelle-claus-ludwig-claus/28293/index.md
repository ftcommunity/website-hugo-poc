---
layout: "image"
title: "Mähdrescher"
date: "2010-09-26T16:06:54"
picture: "eb11.jpg"
weight: "2"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28293
- /details8394.html
imported:
- "2019"
_4images_image_id: "28293"
_4images_cat_id: "2061"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:54"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28293 -->
