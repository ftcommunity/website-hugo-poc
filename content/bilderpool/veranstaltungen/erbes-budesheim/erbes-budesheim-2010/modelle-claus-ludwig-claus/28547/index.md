---
layout: "image"
title: "Detail vom Mülltransportauflieger"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim124.jpg"
weight: "18"
konstrukteure: 
- "Claus Ludwig (Claus)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28547
- /details4f02.html
imported:
- "2019"
_4images_image_id: "28547"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28547 -->
