---
layout: "image"
title: "Volker-James"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim29.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28856
- /details7ec5.html
imported:
- "2019"
_4images_image_id: "28856"
_4images_cat_id: "2066"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28856 -->
CubeSolver
