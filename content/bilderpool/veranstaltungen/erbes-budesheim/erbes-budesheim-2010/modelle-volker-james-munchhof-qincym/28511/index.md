---
layout: "image"
title: "Rubik's Cube"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim088.jpg"
weight: "3"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28511
- /details86b1.html
imported:
- "2019"
_4images_image_id: "28511"
_4images_cat_id: "2066"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28511 -->
Die Anlage nochmal im Detail
