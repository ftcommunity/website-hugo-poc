---
layout: "image"
title: "..."
date: "2010-10-03T15:00:55"
picture: "FTconvention2010-20100925-13-59-00-DSC_3131.jpg"
weight: "13"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28886
- /detailse23b.html
imported:
- "2019"
_4images_image_id: "28886"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28886 -->
