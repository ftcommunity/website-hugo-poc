---
layout: "image"
title: "bus technique"
date: "2010-10-03T15:00:55"
picture: "1.jpg"
weight: "4"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28877
- /details44ef.html
imported:
- "2019"
_4images_image_id: "28877"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28877 -->
