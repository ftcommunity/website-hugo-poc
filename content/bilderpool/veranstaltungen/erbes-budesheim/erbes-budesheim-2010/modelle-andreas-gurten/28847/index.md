---
layout: "image"
title: "Andreas Gürten"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim20.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28847
- /details858d-2.html
imported:
- "2019"
_4images_image_id: "28847"
_4images_cat_id: "2088"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28847 -->
Cruqius Pumpwerke
