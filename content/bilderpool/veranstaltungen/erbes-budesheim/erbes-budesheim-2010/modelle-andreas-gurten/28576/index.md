---
layout: "image"
title: "Pneumatisches Modell (3)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim153.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (laserman)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28576
- /details572c.html
imported:
- "2019"
_4images_image_id: "28576"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "153"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28576 -->
