---
layout: "image"
title: "Schwerlastkran"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim184.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28607
- /detailsc017-2.html
imported:
- "2019"
_4images_image_id: "28607"
_4images_cat_id: "2069"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "184"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28607 -->
