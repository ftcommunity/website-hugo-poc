---
layout: "image"
title: "Containerterminal"
date: "2010-09-26T22:41:24"
picture: "Containerterminal_-_Karl_Tillmetz_charlie2700.jpg"
weight: "1"
konstrukteure: 
- "Karl Tillmetz (charlie2700)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28389
- /details1f86-2.html
imported:
- "2019"
_4images_image_id: "28389"
_4images_cat_id: "2069"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T22:41:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28389 -->
