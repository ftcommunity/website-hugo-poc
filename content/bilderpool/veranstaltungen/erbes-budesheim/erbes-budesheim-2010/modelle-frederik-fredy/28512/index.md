---
layout: "image"
title: "Bausteinschieber"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim089.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28512
- /detailsd808-3.html
imported:
- "2019"
_4images_image_id: "28512"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "89"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28512 -->
