---
layout: "image"
title: "Boxen-Wechsler"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim132.jpg"
weight: "13"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28555
- /detailsc020-2.html
imported:
- "2019"
_4images_image_id: "28555"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28555 -->
