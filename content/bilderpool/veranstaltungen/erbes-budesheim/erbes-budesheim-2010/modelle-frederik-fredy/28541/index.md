---
layout: "image"
title: "Magnetimpulsanlage mit Zählwerk"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim118.jpg"
weight: "12"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28541
- /details14ac-2.html
imported:
- "2019"
_4images_image_id: "28541"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "118"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28541 -->
