---
layout: "image"
title: "LKW mit Anhänger beladen mit Kies"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim146.jpg"
weight: "17"
konstrukteure: 
- "Fabian & Jürgen Becker"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28569
- /details348b-2.html
imported:
- "2019"
_4images_image_id: "28569"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "146"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28569 -->
