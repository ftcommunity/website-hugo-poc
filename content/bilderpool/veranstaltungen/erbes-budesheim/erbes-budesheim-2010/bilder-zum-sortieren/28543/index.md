---
layout: "image"
title: "Zugmaschine des Tiefladers"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim120.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28543
- /details32ad-2.html
imported:
- "2019"
_4images_image_id: "28543"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "120"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28543 -->
