---
layout: "image"
title: "Traktor mit Anhänger"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim147.jpg"
weight: "18"
konstrukteure: 
- "Fabian & Jürgen Becker"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28570
- /details4998.html
imported:
- "2019"
_4images_image_id: "28570"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "147"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28570 -->
