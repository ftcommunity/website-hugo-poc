---
layout: "image"
title: "Auslegerkran"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim008.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28431
- /detailsc426.html
imported:
- "2019"
_4images_image_id: "28431"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28431 -->
