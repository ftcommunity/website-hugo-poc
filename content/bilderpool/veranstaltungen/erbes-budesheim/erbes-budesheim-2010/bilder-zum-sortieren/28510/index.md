---
layout: "image"
title: "Rubik's Cube"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim087.jpg"
weight: "11"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28510
- /details0d51.html
imported:
- "2019"
_4images_image_id: "28510"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28510 -->
Elektronische Anlage, um den Zauberwürfel wierder in die richtige Lage zu bewegen
