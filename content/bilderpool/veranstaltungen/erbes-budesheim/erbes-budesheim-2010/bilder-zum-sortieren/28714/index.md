---
layout: "image"
title: "Modelle auf der FT-Convention"
date: "2010-09-28T17:22:39"
picture: "ddgh06.jpg"
weight: "24"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28714
- /details3e41.html
imported:
- "2019"
_4images_image_id: "28714"
_4images_cat_id: "2055"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T17:22:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28714 -->
