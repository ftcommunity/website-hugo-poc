---
layout: "image"
title: "Fahr-Roboter"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim021.jpg"
weight: "4"
konstrukteure: 
- "Frank Hermann"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28444
- /details844d.html
imported:
- "2019"
_4images_image_id: "28444"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28444 -->
