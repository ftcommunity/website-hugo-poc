---
layout: "image"
title: "Ballgreifer (2)"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim075.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28498
- /detailsc553.html
imported:
- "2019"
_4images_image_id: "28498"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28498 -->
