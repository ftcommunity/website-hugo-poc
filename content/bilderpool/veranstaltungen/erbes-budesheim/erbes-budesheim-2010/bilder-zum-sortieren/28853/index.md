---
layout: "image"
title: "Fritz Roller"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim26.jpg"
weight: "33"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/28853
- /detailsed21.html
imported:
- "2019"
_4images_image_id: "28853"
_4images_cat_id: "2055"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28853 -->
Fritz Roller erklärt die Inundatie-Waaierschleuse......
