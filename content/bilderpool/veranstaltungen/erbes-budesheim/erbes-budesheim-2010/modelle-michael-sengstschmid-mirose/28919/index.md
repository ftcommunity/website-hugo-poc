---
layout: "image"
title: "Michael Sengstschmid (mirose)"
date: "2010-10-03T15:00:57"
picture: "APP-2010-025030.jpg"
weight: "11"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28919
- /details1711-2.html
imported:
- "2019"
_4images_image_id: "28919"
_4images_cat_id: "2068"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28919 -->
