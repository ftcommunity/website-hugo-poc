---
layout: "image"
title: "greed is good"
date: "2010-10-03T15:00:57"
picture: "APP-2010-025031.jpg"
weight: "12"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28920
- /detailse3e4.html
imported:
- "2019"
_4images_image_id: "28920"
_4images_cat_id: "2068"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28920 -->
