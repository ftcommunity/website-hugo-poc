---
layout: "image"
title: "Detail des Münzensortierers"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim095.jpg"
weight: "7"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28518
- /detailsfdd9-2.html
imported:
- "2019"
_4images_image_id: "28518"
_4images_cat_id: "2068"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28518 -->
