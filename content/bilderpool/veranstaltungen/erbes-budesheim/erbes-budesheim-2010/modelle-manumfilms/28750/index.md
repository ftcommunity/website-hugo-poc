---
layout: "image"
title: "rechte Seite"
date: "2010-09-29T15:01:11"
picture: "modellevonmanuelankmanumffilms1.jpg"
weight: "6"
konstrukteure: 
- "manumffilms"
fotografen:
- "-?-"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28750
- /detailsba9e.html
imported:
- "2019"
_4images_image_id: "28750"
_4images_cat_id: "2087"
_4images_user_id: "934"
_4images_image_date: "2010-09-29T15:01:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28750 -->
7-Achs-Roboterarm + Greifer, gesteuert via Webcam - RIGHT