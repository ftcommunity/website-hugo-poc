---
layout: "image"
title: "Gesamt"
date: "2010-09-29T15:01:11"
picture: "modellevonmanuelankmanumffilms4.jpg"
weight: "9"
konstrukteure: 
- "manumffilms"
fotografen:
- "--"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- /php/details/28753
- /details94f8.html
imported:
- "2019"
_4images_image_id: "28753"
_4images_cat_id: "2087"
_4images_user_id: "934"
_4images_image_date: "2010-09-29T15:01:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28753 -->
7-Achs-Roboterarm + Greifer, gesteuert via Webcam - FULL
