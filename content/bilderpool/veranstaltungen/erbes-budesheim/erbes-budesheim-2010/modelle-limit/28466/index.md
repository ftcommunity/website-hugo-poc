---
layout: "image"
title: "Marius Seider (Limit) hinter seiner Tip-Company"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim043.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28466
- /details6bfa.html
imported:
- "2019"
_4images_image_id: "28466"
_4images_cat_id: "2074"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28466 -->
