---
layout: "comment"
hidden: true
title: "12422"
date: "2010-10-03T11:17:35"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Link um Carillon:
http://www.ftcommunity.de/categories.php?cat_id=1934

Carillon -Fischertechnik 

Het Robo-Pro-hoofdprogramma kan achter elkaar de onderstaande melodieën spelen met het Fischertechnik Carillon met 8 stalen klokken C3-C4. Voor elk melodie en toon heb ik een apart subprogramma. gemaakt 

- London Bridge 
- When the Saints go marching on 
- Ozewiezewoze 
- Boeren-oogst-lied 
- Zag beren broodjes smeren 
- Den Haag 
- Daar was laatst een meisje loos 
- Alle Menschen werden Bruder 
- Vader Jacob 
- Jingle-Bells 
- e.a. 

Ik heb de klokken direct middels elektro-magneten en 4mm verchroomde stalen stelen met een messing hamerstuk werkend gekregen. Voor elke toon wordt een electromagneet 0,08 sec bekrachtigd gevolgd door een standaard wachttijd van 0,4 sec. Per melodie voeg ik aanvullende wachttijden toe waar dit nodig is. 
De Link naar m’n werkende Fischertechnik Carilllon op You-Tube is : 
http://www.youtube.com/watch?v=-rUGIXUCbGs en http://www.youtube.com/watch?v=kZe7aG0HQpY 

Leverancier in Nederland van handbellen-set G33850 met (slechts) 8 tonen : (ca. 80 Euro) http://www.demuziekboom.nl De Muziekboom, Waterlelie 143, 9207-AZ Drachten NL 


Betaalbare sets met meer tonen zijn in Nederland en elders binnen Europa volgens mij moeilijk te verkrijgen. Na een interessante en luisterlijk bezoek aan het Carillon in de Gasthuistoren te Zaltbommel, hoorde ik van beroeps-beiaardier Wout van der Linden dat het bespelen van handklokken in Amerika meer leeft dan hier in Europa. 

Een interessante link naar Handbellen-sets met meer dan 8 tonen van Kids Play Music USA: 
http://www.kidsplaymusic.org/id13.html 
De shippingkosten vanuit Amerika zijn helaas onaantrekkelijk: $ 50,-- naar Europa !!!......