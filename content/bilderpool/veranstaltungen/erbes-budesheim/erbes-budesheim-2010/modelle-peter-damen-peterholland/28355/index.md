---
layout: "image"
title: "Unimog und Traktor"
date: "2010-09-26T18:27:46"
picture: "Unimog_-_Peter_Damen_peterholland.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28355
- /details5b15.html
imported:
- "2019"
_4images_image_id: "28355"
_4images_cat_id: "2059"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:27:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28355 -->
