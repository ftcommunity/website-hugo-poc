---
layout: "comment"
hidden: true
title: "12421"
date: "2010-10-03T11:09:52"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Eine Waaierschleuse ist eine spezielle Schleuse, die gegen den Wasserdruck geöffnet und geschlossen werden kann. Dieser Schleusentyp wurde von Jan Blanken (1755-1838) erfunden. 

Noch mehr historische hintergrunden der 
Inundatie-waaiersluizen Nieuwe Hollandse Waterlinie gibt es unter: 

http://www.ftcommunity.de/data/downloads/beschreibungen/ftinundatiewaaiersluis.doc 

Bau-details gibt es unter: http://www.ftcommunity.de/categories.php?cat_id=1692