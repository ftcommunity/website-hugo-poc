---
layout: "image"
title: "Ferngesteuerter Katamaran (2)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim136.jpg"
weight: "2"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28559
- /detailsd33a.html
imported:
- "2019"
_4images_image_id: "28559"
_4images_cat_id: "2086"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "136"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28559 -->
