---
layout: "image"
title: "Unterteil"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim191.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28614
- /details447d-2.html
imported:
- "2019"
_4images_image_id: "28614"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "191"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28614 -->
