---
layout: "image"
title: "close up"
date: "2010-10-03T15:00:56"
picture: "APP-2010-025024.jpg"
weight: "13"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/28911
- /details8f0b.html
imported:
- "2019"
_4images_image_id: "28911"
_4images_cat_id: "2077"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28911 -->
