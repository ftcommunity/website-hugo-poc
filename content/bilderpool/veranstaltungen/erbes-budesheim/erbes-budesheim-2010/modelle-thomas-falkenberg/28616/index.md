---
layout: "image"
title: "Bauteile einer Abraumanlage"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim193.jpg"
weight: "9"
konstrukteure: 
- "Thomas Falkenberg (speedy68)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28616
- /details5038.html
imported:
- "2019"
_4images_image_id: "28616"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "193"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28616 -->
