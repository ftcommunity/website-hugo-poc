---
layout: "image"
title: "Schnittmodell eines Radladers"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim034.jpg"
weight: "4"
konstrukteure: 
- "Fabian & Jürgen Becker"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28457
- /details03a4.html
imported:
- "2019"
_4images_image_id: "28457"
_4images_cat_id: "2091"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28457 -->
