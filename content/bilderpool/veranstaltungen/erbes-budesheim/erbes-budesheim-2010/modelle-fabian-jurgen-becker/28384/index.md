---
layout: "image"
title: "Bagger und Kipper mit Anhänger"
date: "2010-09-26T21:23:28"
picture: "Bagger__Kipper_mit_Anhnger_-_Fabian__Jrgen_Becker.jpg"
weight: "1"
konstrukteure: 
- "Fabian & Jürgen Becker"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/28384
- /details0be5.html
imported:
- "2019"
_4images_image_id: "28384"
_4images_cat_id: "2091"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T21:23:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28384 -->
