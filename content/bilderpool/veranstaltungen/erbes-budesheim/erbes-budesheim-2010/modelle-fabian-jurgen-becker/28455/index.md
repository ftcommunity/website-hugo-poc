---
layout: "image"
title: "Schnittmodell eines Radladers"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim032.jpg"
weight: "2"
konstrukteure: 
- "Fabian & Jürgen Becker"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28455
- /details42d2.html
imported:
- "2019"
_4images_image_id: "28455"
_4images_cat_id: "2091"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28455 -->
