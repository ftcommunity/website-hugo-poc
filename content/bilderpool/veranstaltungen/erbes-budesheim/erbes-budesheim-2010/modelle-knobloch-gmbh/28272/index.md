---
layout: "image"
title: "Spiegelteleskop"
date: "2010-09-26T12:23:16"
picture: "spiegelteleskop2.jpg"
weight: "2"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28272
- /details29f0.html
imported:
- "2019"
_4images_image_id: "28272"
_4images_cat_id: "2053"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:23:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28272 -->
