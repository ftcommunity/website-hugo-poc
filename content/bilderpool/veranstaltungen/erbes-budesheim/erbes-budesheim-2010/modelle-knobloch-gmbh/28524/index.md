---
layout: "image"
title: "Die Steuerung des Spiegelteleskops"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim101.jpg"
weight: "6"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28524
- /details4138.html
imported:
- "2019"
_4images_image_id: "28524"
_4images_cat_id: "2053"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "101"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28524 -->
