---
layout: "image"
title: "Der Aufgang zum Teleskop"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim103.jpg"
weight: "8"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28526
- /details3595.html
imported:
- "2019"
_4images_image_id: "28526"
_4images_cat_id: "2053"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28526 -->
