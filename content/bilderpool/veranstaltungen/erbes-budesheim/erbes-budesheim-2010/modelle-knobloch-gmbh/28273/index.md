---
layout: "image"
title: "Spiegelteleskop"
date: "2010-09-26T12:23:16"
picture: "spiegelteleskop3.jpg"
weight: "3"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Heiko"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/28273
- /details0e30.html
imported:
- "2019"
_4images_image_id: "28273"
_4images_cat_id: "2053"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:23:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28273 -->
