---
layout: "image"
title: "Fahrwerk des Krans"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim130.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/28553
- /details8f8a-2.html
imported:
- "2019"
_4images_image_id: "28553"
_4images_cat_id: "2078"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28553 -->
