---
layout: "image"
title: "Kran"
date: "2010-09-26T16:07:13"
picture: "eb28.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/28310
- /detailsafb4.html
imported:
- "2019"
_4images_image_id: "28310"
_4images_cat_id: "2078"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28310 -->
