---
layout: "image"
title: "Modelle auf der FT-Convention"
date: "2010-09-28T17:22:39"
picture: "ddgh11.jpg"
weight: "5"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/28719
- /detailscb2c.html
imported:
- "2019"
_4images_image_id: "28719"
_4images_cat_id: "2078"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T17:22:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28719 -->
