---
layout: "image"
title: "3D-Drucker"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim113.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32640
- /details4c6e.html
imported:
- "2019"
_4images_image_id: "32640"
_4images_cat_id: "2441"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "113"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32640 -->
Der "druckt" aus Heißkleber bestehende dreidimensionale Gegenstände. High Tech. Es gab mindestens zwei solcher Geräte auf der Convention.
