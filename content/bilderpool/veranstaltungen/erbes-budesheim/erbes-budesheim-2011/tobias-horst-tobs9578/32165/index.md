---
layout: "image"
title: "ft Convention 2011"
date: "2011-09-25T20:06:24"
picture: "ftconvention2.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/32165
- /details61ca.html
imported:
- "2019"
_4images_image_id: "32165"
_4images_cat_id: "2384"
_4images_user_id: "130"
_4images_image_date: "2011-09-25T20:06:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32165 -->
Schöner Top Spin, nur die Bewegungsabläufe waren ziemlich langatmig.