---
layout: "image"
title: "Martin Westphal (Masked)"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim036.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32563
- /details9b65.html
imported:
- "2019"
_4images_image_id: "32563"
_4images_cat_id: "2392"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32563 -->
Er hatte den Druck des ft:c-Posters organisiert.
