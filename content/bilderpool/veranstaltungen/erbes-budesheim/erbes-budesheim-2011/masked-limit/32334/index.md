---
layout: "image"
title: "DSC06148"
date: "2011-09-25T20:36:34"
picture: "modelle160.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32334
- /details4ffa.html
imported:
- "2019"
_4images_image_id: "32334"
_4images_cat_id: "2392"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "160"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32334 -->
