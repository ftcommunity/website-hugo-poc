---
layout: "image"
title: "DSC05885"
date: "2011-09-25T20:36:33"
picture: "modelle029.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32203
- /details5fd8-2.html
imported:
- "2019"
_4images_image_id: "32203"
_4images_cat_id: "2392"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32203 -->
