---
layout: "image"
title: "Anfänge eines neuen Fahrzeugs"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim089.jpg"
weight: "13"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32616
- /detailsdfec.html
imported:
- "2019"
_4images_image_id: "32616"
_4images_cat_id: "2423"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "89"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32616 -->
