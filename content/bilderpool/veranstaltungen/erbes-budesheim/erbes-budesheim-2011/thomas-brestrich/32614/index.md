---
layout: "image"
title: "Pneumatisches Hexapod"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim087.jpg"
weight: "11"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32614
- /details8ded.html
imported:
- "2019"
_4images_image_id: "32614"
_4images_cat_id: "2423"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32614 -->
