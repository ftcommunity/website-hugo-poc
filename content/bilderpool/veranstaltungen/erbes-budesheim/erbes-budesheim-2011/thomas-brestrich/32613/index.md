---
layout: "image"
title: "Pneumatisches Hexapod"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim086.jpg"
weight: "10"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32613
- /details34b8.html
imported:
- "2019"
_4images_image_id: "32613"
_4images_cat_id: "2423"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32613 -->
