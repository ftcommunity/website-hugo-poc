---
layout: "image"
title: "DSC06080"
date: "2011-09-25T20:36:34"
picture: "modelle150.jpg"
weight: "4"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32324
- /details6030.html
imported:
- "2019"
_4images_image_id: "32324"
_4images_cat_id: "2423"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "150"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32324 -->
