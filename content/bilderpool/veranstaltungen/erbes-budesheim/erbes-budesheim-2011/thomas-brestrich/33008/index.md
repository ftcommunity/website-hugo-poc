---
layout: "image"
title: "Löschboot 2"
date: "2011-09-30T18:09:49"
picture: "Lschboot_2.jpg"
weight: "20"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/33008
- /detailsdfda.html
imported:
- "2019"
_4images_image_id: "33008"
_4images_cat_id: "2423"
_4images_user_id: "1126"
_4images_image_date: "2011-09-30T18:09:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33008 -->
