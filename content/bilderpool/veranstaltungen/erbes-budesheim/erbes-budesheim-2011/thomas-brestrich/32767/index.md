---
layout: "image"
title: "Pneumatisches Hexapod von Thomas Bestrich (Schnaggels)"
date: "2011-09-26T17:47:41"
picture: "dm074.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32767
- /details1887-2.html
imported:
- "2019"
_4images_image_id: "32767"
_4images_cat_id: "2423"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32767 -->
