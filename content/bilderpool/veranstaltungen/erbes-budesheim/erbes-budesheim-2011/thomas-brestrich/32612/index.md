---
layout: "image"
title: "Boot und Bootsantrieb"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim085.jpg"
weight: "9"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32612
- /details4903.html
imported:
- "2019"
_4images_image_id: "32612"
_4images_cat_id: "2423"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32612 -->
