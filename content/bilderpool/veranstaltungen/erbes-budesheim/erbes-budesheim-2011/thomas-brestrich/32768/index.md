---
layout: "image"
title: "Pneumatisches Hexapod"
date: "2011-09-26T17:47:41"
picture: "dm075.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32768
- /details0652.html
imported:
- "2019"
_4images_image_id: "32768"
_4images_cat_id: "2423"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32768 -->
