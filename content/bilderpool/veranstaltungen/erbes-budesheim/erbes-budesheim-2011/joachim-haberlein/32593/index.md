---
layout: "image"
title: "Industriemodell (Auftragsarbeit der Knobloch GmbH)"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim066.jpg"
weight: "25"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32593
- /details3841.html
imported:
- "2019"
_4images_image_id: "32593"
_4images_cat_id: "2398"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32593 -->
Thomas Kaiser (thkais), dieser Glückspilz, durfte diese Modell im Kundenauftrag bauen...
