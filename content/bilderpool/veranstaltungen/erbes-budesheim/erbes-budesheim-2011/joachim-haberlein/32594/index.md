---
layout: "image"
title: "Industriemodell (Auftragsarbeit der Knobloch GmbH)"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim067.jpg"
weight: "26"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32594
- /detailsf8ec-2.html
imported:
- "2019"
_4images_image_id: "32594"
_4images_cat_id: "2398"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32594 -->
