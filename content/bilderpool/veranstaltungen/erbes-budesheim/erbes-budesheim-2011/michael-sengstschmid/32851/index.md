---
layout: "image"
title: "Sessellift Michael Sengstschmid"
date: "2011-09-27T20:47:57"
picture: "sesselliftmichaelsengstschmid24.jpg"
weight: "49"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32851
- /details39c2-2.html
imported:
- "2019"
_4images_image_id: "32851"
_4images_cat_id: "2407"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32851 -->
