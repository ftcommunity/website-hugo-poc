---
layout: "image"
title: "Sessellift Michael Sengstschmid"
date: "2011-09-27T20:47:57"
picture: "sesselliftmichaelsengstschmid23.jpg"
weight: "48"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32850
- /details3d5d.html
imported:
- "2019"
_4images_image_id: "32850"
_4images_cat_id: "2407"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32850 -->
