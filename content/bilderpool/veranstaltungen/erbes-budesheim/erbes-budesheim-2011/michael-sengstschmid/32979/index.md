---
layout: "image"
title: "sesselbahn1.jpg"
date: "2011-09-28T10:45:36"
picture: "sesselbahn1.jpg"
weight: "52"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32979
- /details73fd-2.html
imported:
- "2019"
_4images_image_id: "32979"
_4images_cat_id: "2407"
_4images_user_id: "1007"
_4images_image_date: "2011-09-28T10:45:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32979 -->
