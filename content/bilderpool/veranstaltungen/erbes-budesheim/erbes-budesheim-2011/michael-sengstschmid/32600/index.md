---
layout: "image"
title: "Riesen-Seilbahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim073.jpg"
weight: "19"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32600
- /details622f-2.html
imported:
- "2019"
_4images_image_id: "32600"
_4images_cat_id: "2407"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32600 -->
Blick von unten auf den Antrieb. Man erkennt einige der Powermotoren.
