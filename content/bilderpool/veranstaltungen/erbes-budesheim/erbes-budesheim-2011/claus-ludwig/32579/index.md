---
layout: "image"
title: "Modelle mit plastisch verformtem fischertechnik"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim052.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
schlagworte: ["modding"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32579
- /detailsa6b7.html
imported:
- "2019"
_4images_image_id: "32579"
_4images_cat_id: "2416"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32579 -->
fischertechnik-Teile, einige Zeit in kochendes Wasser gelegt, werden offenbar plastisch verformt. So sind bleibende Ringe und gebogene Teile wie bei den Schutzblechen des Oldtimers in der Mitte möglich.
