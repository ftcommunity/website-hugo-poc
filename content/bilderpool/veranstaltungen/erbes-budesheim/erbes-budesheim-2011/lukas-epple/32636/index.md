---
layout: "image"
title: "conventionerbesbuedesheim109.jpg"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim109.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32636
- /details9831-2.html
imported:
- "2019"
_4images_image_id: "32636"
_4images_cat_id: "2428"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "109"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32636 -->
