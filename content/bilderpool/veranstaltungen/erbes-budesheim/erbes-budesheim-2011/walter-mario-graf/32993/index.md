---
layout: "image"
title: "Eisenbahn_6225"
date: "2011-09-30T16:35:22"
picture: "IMG_6225.JPG"
weight: "16"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/32993
- /details4d2f.html
imported:
- "2019"
_4images_image_id: "32993"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32993 -->
