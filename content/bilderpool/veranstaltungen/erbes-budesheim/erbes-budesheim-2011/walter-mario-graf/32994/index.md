---
layout: "image"
title: "Eisenbahn_6226"
date: "2011-09-30T16:35:51"
picture: "IMG_6226.JPG"
weight: "17"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/32994
- /details4b07-2.html
imported:
- "2019"
_4images_image_id: "32994"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32994 -->
