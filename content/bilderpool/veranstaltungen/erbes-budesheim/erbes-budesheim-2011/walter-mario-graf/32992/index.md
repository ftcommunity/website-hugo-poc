---
layout: "image"
title: "Eisenbahn_6224.JPG"
date: "2011-09-30T16:34:59"
picture: "IMG_6224.JPG"
weight: "15"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/32992
- /details8532.html
imported:
- "2019"
_4images_image_id: "32992"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32992 -->
