---
layout: "image"
title: "DSC05967"
date: "2011-09-25T20:36:33"
picture: "modelle080.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32254
- /detailsc0e3-2.html
imported:
- "2019"
_4images_image_id: "32254"
_4images_cat_id: "2405"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32254 -->
