---
layout: "image"
title: "Getriebe mit zyklisch variables Übersetzungsverhältnis"
date: "2011-09-27T21:57:07"
picture: "zyklischvariablesgetriebe2.jpg"
weight: "5"
konstrukteure: 
- "Wilhelm Klopmeier"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32917
- /detailse703.html
imported:
- "2019"
_4images_image_id: "32917"
_4images_cat_id: "2418"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:57:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32917 -->
.	konventionelle Exzenterpresse (zum Vergleich)
.	zum Schmieden optimierte Exzenterpresse (Stillstand im OT, kurze Druckberührzeit, schneller Rückhub)
.	zum Tiefziehen optimierte Exzenterpresse (Stillstand im OT, verminderte Geschwindigkeit beim Auftreffen des Werkzeuges, schneller Rückhub (die Höhe des Auftreffpunktes kann eingestellt werden, im Modell wäre dazu ein  Rädertausch erforderlich))

Die beiliegende Bilder zeigen die entsprechenden Hubdiagrammen, mit denen die technologischen Anforderungen erfüllt werden.
