---
layout: "image"
title: "Roboter als Tonnengreifer"
date: "2011-09-26T17:47:41"
picture: "dm063.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32756
- /details7024.html
imported:
- "2019"
_4images_image_id: "32756"
_4images_cat_id: "2381"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32756 -->
