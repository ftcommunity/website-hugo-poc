---
layout: "image"
title: "Ur-ft-Plotter, Schweißroboter, ft-Flieger, Waage, ..."
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim045.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32572
- /detailse703-2.html
imported:
- "2019"
_4images_image_id: "32572"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32572 -->
