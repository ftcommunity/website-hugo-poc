---
layout: "image"
title: "DSC06056"
date: "2011-09-25T20:36:34"
picture: "modelle134.jpg"
weight: "9"
konstrukteure: 
- "Felix Geerken"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32308
- /details43a5.html
imported:
- "2019"
_4images_image_id: "32308"
_4images_cat_id: "2381"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "134"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32308 -->
