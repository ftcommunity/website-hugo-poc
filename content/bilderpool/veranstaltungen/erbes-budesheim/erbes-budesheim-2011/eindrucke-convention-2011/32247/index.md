---
layout: "image"
title: "DSC05957"
date: "2011-09-25T20:36:33"
picture: "modelle073.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32247
- /detailscb03.html
imported:
- "2019"
_4images_image_id: "32247"
_4images_cat_id: "2381"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32247 -->
