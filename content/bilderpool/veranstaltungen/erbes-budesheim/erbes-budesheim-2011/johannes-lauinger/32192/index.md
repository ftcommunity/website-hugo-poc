---
layout: "image"
title: "DSC05873"
date: "2011-09-25T20:36:33"
picture: "modelle018.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32192
- /detailsa53c.html
imported:
- "2019"
_4images_image_id: "32192"
_4images_cat_id: "2430"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32192 -->
