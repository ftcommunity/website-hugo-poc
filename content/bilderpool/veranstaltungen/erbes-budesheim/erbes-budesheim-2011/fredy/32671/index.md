---
layout: "image"
title: "Dragster und Oldtimer"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim144.jpg"
weight: "21"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32671
- /details825c.html
imported:
- "2019"
_4images_image_id: "32671"
_4images_cat_id: "2388"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "144"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32671 -->
Der rechte ist ein klassischer Retro-Nachbau.
