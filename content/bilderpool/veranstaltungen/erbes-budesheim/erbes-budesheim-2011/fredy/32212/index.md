---
layout: "image"
title: "DSC05912"
date: "2011-09-25T20:36:33"
picture: "modelle038.jpg"
weight: "16"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32212
- /detailsf8d1.html
imported:
- "2019"
_4images_image_id: "32212"
_4images_cat_id: "2388"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32212 -->
