---
layout: "image"
title: "Dragster"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim141.jpg"
weight: "4"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32668
- /details334e-2.html
imported:
- "2019"
_4images_image_id: "32668"
_4images_cat_id: "2389"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32668 -->
