---
layout: "image"
title: "Handhabungs-Roboter"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim142.jpg"
weight: "5"
konstrukteure: 
- "Joachim Jacobi (MisterWho)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32669
- /details5864.html
imported:
- "2019"
_4images_image_id: "32669"
_4images_cat_id: "2389"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "142"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32669 -->
