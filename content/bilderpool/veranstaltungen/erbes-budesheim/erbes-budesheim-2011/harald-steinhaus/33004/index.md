---
layout: "image"
title: "Feuerlöschboot"
date: "2011-09-30T17:19:04"
picture: "IMG_6343.JPG"
weight: "43"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/33004
- /details26cd-2.html
imported:
- "2019"
_4images_image_id: "33004"
_4images_cat_id: "2393"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T17:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33004 -->
Die Rohrhülsen sollen nur das Loch füllen, das der Akku hinterlassen hat. Der musste nach hinten wandern, weil das Boot zu buglastig wurde.
