---
layout: "image"
title: "Kirmesmodelle"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim120.jpg"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32647
- /detailsffc6-3.html
imported:
- "2019"
_4images_image_id: "32647"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "120"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32647 -->
Ur-ft-Schleifringe, aber als Kontakt Ur-ft-Federstäbe drangepresst.
