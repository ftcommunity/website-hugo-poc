---
layout: "image"
title: "Kirmesmodelle"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim119.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32646
- /detailsab67-2.html
imported:
- "2019"
_4images_image_id: "32646"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "119"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32646 -->
Man sieht es hier wegen der Drehbewegung nicht, aber die Außenseiten der Sitzflächen waren tatsächlich mit kirmesmäßig glitzernder Folie beklebt. Und, ganz Harald, konnte man dieses Fahrgeschäft natürlich zusammenklappen und auf das hier sichtbare Fahrzeug zum Transport packen.
