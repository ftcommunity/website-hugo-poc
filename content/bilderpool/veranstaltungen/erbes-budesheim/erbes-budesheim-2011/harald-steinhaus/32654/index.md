---
layout: "image"
title: "Katamaran"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim127.jpg"
weight: "31"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32654
- /details8424.html
imported:
- "2019"
_4images_image_id: "32654"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "127"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32654 -->
Der luxuriöse Arbeitsplatz des Bootsführers. Führerhaus-Fertigteile normal in einem primitiv-Laster zu verwenden gelingt Harald ja nicht :-)
