---
layout: "image"
title: "Haralds Flotte II"
date: "2011-09-27T20:47:57"
picture: "Haralds_Flotte_2.jpg"
weight: "39"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/32821
- /details863a-2.html
imported:
- "2019"
_4images_image_id: "32821"
_4images_cat_id: "2393"
_4images_user_id: "1126"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32821 -->
Flotte im Teicheinsatz
