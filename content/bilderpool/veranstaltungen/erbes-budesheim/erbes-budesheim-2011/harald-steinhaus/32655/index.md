---
layout: "image"
title: "Bootsantrieb"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim128.jpg"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32655
- /detailse11c.html
imported:
- "2019"
_4images_image_id: "32655"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "128"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32655 -->
Mit richtigen Schiffsschrauben
