---
layout: "image"
title: "Fahrgeschäft I"
date: "2011-09-27T20:47:57"
picture: "Fahrgeschft_1.jpg"
weight: "41"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/32823
- /details6055-2.html
imported:
- "2019"
_4images_image_id: "32823"
_4images_cat_id: "2393"
_4images_user_id: "1126"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32823 -->
Fahrgeschäft
