---
layout: "image"
title: "Boot"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim129.jpg"
weight: "33"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32656
- /details808a.html
imported:
- "2019"
_4images_image_id: "32656"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "129"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32656 -->
Mit dem Bootsantrieb, den Harald in der ft:pedia 1/2011 schon vorstellte.
