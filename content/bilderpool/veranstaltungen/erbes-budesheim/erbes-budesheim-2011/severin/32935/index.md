---
layout: "image"
title: "6-Achsroboter   Severin"
date: "2011-09-27T23:24:31"
picture: "achsroboterseverinboth1.jpg"
weight: "4"
konstrukteure: 
- "Severin"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32935
- /detailsa58f.html
imported:
- "2019"
_4images_image_id: "32935"
_4images_cat_id: "2419"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32935 -->
