---
layout: "image"
title: "Geschicklichkeitsspiel"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim152.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32679
- /details2172.html
imported:
- "2019"
_4images_image_id: "32679"
_4images_cat_id: "2409"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32679 -->
Man musste durch Handsteuerung der drei Ventile den Ball umlagern.
