---
layout: "image"
title: "Münzsortierer von Andreas Gürten (laserman)"
date: "2011-09-26T17:47:41"
picture: "dm087.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32780
- /details80a5-2.html
imported:
- "2019"
_4images_image_id: "32780"
_4images_cat_id: "2409"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32780 -->
