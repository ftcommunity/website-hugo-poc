---
layout: "image"
title: "Die Münze rollt......."
date: "2011-09-27T21:23:18"
picture: "Muenzsortierer2.jpg"
weight: "19"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Jutta Püttmann"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/32874
- /detailsfd6c.html
imported:
- "2019"
_4images_image_id: "32874"
_4images_cat_id: "2409"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T21:23:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32874 -->
