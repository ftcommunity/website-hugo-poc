---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:51:59"
picture: "hubschrauber01.jpg"
weight: "27"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32905
- /details8b18.html
imported:
- "2019"
_4images_image_id: "32905"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32905 -->
