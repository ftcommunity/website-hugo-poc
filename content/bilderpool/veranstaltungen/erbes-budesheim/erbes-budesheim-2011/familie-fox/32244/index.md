---
layout: "image"
title: "DSC05953"
date: "2011-09-25T20:36:33"
picture: "modelle070.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
- "Magnus Fox"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32244
- /details5053-2.html
imported:
- "2019"
_4images_image_id: "32244"
_4images_cat_id: "2386"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32244 -->
