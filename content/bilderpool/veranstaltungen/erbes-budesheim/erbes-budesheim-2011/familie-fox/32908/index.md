---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:52:07"
picture: "hubschrauber04.jpg"
weight: "30"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32908
- /details2577-2.html
imported:
- "2019"
_4images_image_id: "32908"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:52:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32908 -->
