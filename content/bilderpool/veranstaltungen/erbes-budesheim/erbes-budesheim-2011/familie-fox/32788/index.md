---
layout: "image"
title: "Hubschrauber"
date: "2011-09-26T17:47:41"
picture: "dm095.jpg"
weight: "21"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32788
- /detailscebb.html
imported:
- "2019"
_4images_image_id: "32788"
_4images_cat_id: "2386"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32788 -->
