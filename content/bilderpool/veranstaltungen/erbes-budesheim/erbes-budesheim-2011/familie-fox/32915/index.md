---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:52:07"
picture: "hubschrauber11.jpg"
weight: "37"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32915
- /details4a2f.html
imported:
- "2019"
_4images_image_id: "32915"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:52:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32915 -->
