---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:51:59"
picture: "hubschrauber03.jpg"
weight: "29"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32907
- /details4e30.html
imported:
- "2019"
_4images_image_id: "32907"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32907 -->
