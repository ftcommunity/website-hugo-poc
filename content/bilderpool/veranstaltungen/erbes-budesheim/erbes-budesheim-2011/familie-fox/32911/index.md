---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:52:07"
picture: "hubschrauber07.jpg"
weight: "33"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32911
- /detailsab7a.html
imported:
- "2019"
_4images_image_id: "32911"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:52:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32911 -->
