---
layout: "image"
title: "Miniplotter"
date: "2011-09-26T17:47:41"
picture: "dm064.jpg"
weight: "17"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32757
- /details832d-2.html
imported:
- "2019"
_4images_image_id: "32757"
_4images_cat_id: "2386"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32757 -->
