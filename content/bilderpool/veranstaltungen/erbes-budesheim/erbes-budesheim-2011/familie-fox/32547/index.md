---
layout: "image"
title: "conventionerbesbuedesheim020.jpg"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim020.jpg"
weight: "15"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32547
- /detailsac35.html
imported:
- "2019"
_4images_image_id: "32547"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32547 -->
