---
layout: "image"
title: "DSC05951"
date: "2011-09-25T20:36:33"
picture: "modelle068.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32242
- /detailsc466.html
imported:
- "2019"
_4images_image_id: "32242"
_4images_cat_id: "2386"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32242 -->
