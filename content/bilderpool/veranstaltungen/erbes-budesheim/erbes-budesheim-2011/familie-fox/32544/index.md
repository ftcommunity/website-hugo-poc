---
layout: "image"
title: "Hubschrauber"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim017.jpg"
weight: "12"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32544
- /details2ee3.html
imported:
- "2019"
_4images_image_id: "32544"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32544 -->
Die Steuerung der Taumelscheibe erfolgt ohne Motoren - nur über Seilzüge. Ganz wie im Original!
