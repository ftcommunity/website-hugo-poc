---
layout: "image"
title: "Standuhr"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim065.jpg"
weight: "6"
konstrukteure: 
- "Martin Romann (remadus)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32592
- /detailsd018.html
imported:
- "2019"
_4images_image_id: "32592"
_4images_cat_id: "2403"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32592 -->
Hemmung und Antrieb des automatischen Gewichtsaufzugs
