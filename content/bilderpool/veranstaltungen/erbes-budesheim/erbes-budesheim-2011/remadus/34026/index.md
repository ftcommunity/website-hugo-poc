---
layout: "image"
title: "ft Convention EB 2011"
date: "2012-01-24T19:04:09"
picture: "conventon18.jpg"
weight: "16"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/34026
- /details52ec-2.html
imported:
- "2019"
_4images_image_id: "34026"
_4images_cat_id: "2403"
_4images_user_id: "130"
_4images_image_date: "2012-01-24T19:04:09"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34026 -->
Sehr schönes Modell von Remadus.
