---
layout: "image"
title: "Karussell"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim044.jpg"
weight: "5"
konstrukteure: 
- "Raphael (fischercake)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32571
- /detailse88c.html
imported:
- "2019"
_4images_image_id: "32571"
_4images_cat_id: "2446"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32571 -->
Der Ring wird fest gehalten, und auf ihm laufen die drei Gondeln. Normalerweise hängt diese Modell an der Decke, nur für die Convention bekam es Stützen.
