---
layout: "image"
title: "conventionerbesbuedesheim033.jpg"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim033.jpg"
weight: "26"
konstrukteure: 
- "Felix Geerken"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32560
- /detailsc241.html
imported:
- "2019"
_4images_image_id: "32560"
_4images_cat_id: "2399"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32560 -->
