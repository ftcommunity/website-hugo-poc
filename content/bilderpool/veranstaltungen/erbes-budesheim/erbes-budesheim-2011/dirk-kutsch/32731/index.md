---
layout: "image"
title: "Verladekran mit Containern von Dirk Kutsch (Guilligan)"
date: "2011-09-26T17:47:41"
picture: "dm038.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32731
- /details0635.html
imported:
- "2019"
_4images_image_id: "32731"
_4images_cat_id: "2417"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32731 -->
