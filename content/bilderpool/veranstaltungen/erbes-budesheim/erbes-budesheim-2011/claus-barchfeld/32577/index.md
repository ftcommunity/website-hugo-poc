---
layout: "image"
title: "Roboterhand mit 45° gedrehtem Zahnrad (1)"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim050.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32577
- /details53b7.html
imported:
- "2019"
_4images_image_id: "32577"
_4images_cat_id: "2406"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32577 -->
In dieser Stellung zeigt der Arm nach rechts und die Hand senkrecht dazu...
