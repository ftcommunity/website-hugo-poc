---
layout: "image"
title: "Robotermodelle + PVC     Claus Barchfeld"
date: "2011-09-27T23:24:31"
picture: "robotermodellepvc5.jpg"
weight: "17"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32928
- /details1b3c-3.html
imported:
- "2019"
_4images_image_id: "32928"
_4images_cat_id: "2406"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32928 -->
