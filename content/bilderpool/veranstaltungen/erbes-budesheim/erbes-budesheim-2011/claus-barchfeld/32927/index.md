---
layout: "image"
title: "Robotermodelle + PVC     Claus Barchfeld"
date: "2011-09-27T23:24:31"
picture: "robotermodellepvc4.jpg"
weight: "16"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32927
- /detailsf08b.html
imported:
- "2019"
_4images_image_id: "32927"
_4images_cat_id: "2406"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32927 -->
