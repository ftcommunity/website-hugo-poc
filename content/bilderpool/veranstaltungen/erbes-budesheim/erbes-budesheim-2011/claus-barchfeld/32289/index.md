---
layout: "image"
title: "DSC06035"
date: "2011-09-25T20:36:33"
picture: "modelle115.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32289
- /detailsb735.html
imported:
- "2019"
_4images_image_id: "32289"
_4images_cat_id: "2406"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "115"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32289 -->
