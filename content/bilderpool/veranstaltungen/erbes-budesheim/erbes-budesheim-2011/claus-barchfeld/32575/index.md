---
layout: "image"
title: "Roboter mit Rohren als Bauelement"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim048.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32575
- /detailsbff9-2.html
imported:
- "2019"
_4images_image_id: "32575"
_4images_cat_id: "2406"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32575 -->
