---
layout: "image"
title: "Seilbahn Talstation"
date: "2015-10-01T13:37:10"
picture: "Seilbahn_Talstation.jpg"
weight: "3"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42018
- /details0cd3.html
imported:
- "2019"
_4images_image_id: "42018"
_4images_cat_id: "3122"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42018 -->
