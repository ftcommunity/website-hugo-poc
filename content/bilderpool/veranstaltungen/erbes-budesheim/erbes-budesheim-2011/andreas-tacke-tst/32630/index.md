---
layout: "image"
title: "Kugeluhr"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim103.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32630
- /details0b9f-2.html
imported:
- "2019"
_4images_image_id: "32630"
_4images_cat_id: "2395"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32630 -->
Sehr schön kompakt durch die neuen ft-Flexschienen (deren Wände hier an einigen Stellen entfernt wurden, damit die Kugeln die Spur wechseln können).
