---
layout: "image"
title: "3fach-Werkzeugwechsler von Andreas Tacke"
date: "2011-09-26T17:47:41"
picture: "dm031.jpg"
weight: "25"
konstrukteure: 
- "Andreas Tacke TST"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32724
- /detailsc9cd.html
imported:
- "2019"
_4images_image_id: "32724"
_4images_cat_id: "2395"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32724 -->
