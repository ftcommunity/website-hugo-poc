---
layout: "image"
title: "P-Hubgetriebe1"
date: "2011-09-30T16:44:30"
picture: "IMG_6262.JPG"
weight: "31"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/32999
- /details41be.html
imported:
- "2019"
_4images_image_id: "32999"
_4images_cat_id: "2395"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:44:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32999 -->
Da hat der Andreas noch ein Eisen im Feuer: ein Hubgetriebe mit dem Power-Motor, hier als Prototyp im Kran von Dirk Kutsch.
