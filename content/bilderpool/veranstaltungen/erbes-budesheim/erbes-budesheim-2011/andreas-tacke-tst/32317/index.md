---
layout: "image"
title: "DSC06069"
date: "2011-09-25T20:36:34"
picture: "modelle143.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32317
- /details6275.html
imported:
- "2019"
_4images_image_id: "32317"
_4images_cat_id: "2395"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "143"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32317 -->
