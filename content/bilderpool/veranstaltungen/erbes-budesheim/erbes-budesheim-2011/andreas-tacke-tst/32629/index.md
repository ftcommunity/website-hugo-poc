---
layout: "image"
title: "Gabellichtschranken"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim102.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32629
- /detailsae2b-2.html
imported:
- "2019"
_4images_image_id: "32629"
_4images_cat_id: "2395"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32629 -->
Zur Längspositionierung an einer Kette, und zur Winkelpositionierung an Scheiben.
