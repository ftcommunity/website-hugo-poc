---
layout: "image"
title: "Ein Spiel"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim079.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32606
- /detailsb9d9.html
imported:
- "2019"
_4images_image_id: "32606"
_4images_cat_id: "2443"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32606 -->
