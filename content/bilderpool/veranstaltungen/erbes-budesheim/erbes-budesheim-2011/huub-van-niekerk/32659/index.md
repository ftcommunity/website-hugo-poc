---
layout: "image"
title: "Anfänge einer großen Laufmaschine"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim132.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32659
- /detailsff2d.html
imported:
- "2019"
_4images_image_id: "32659"
_4images_cat_id: "2445"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32659 -->
