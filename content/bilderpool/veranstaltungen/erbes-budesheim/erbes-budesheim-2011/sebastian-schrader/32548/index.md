---
layout: "image"
title: "Containerkran"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim021.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32548
- /details5737.html
imported:
- "2019"
_4images_image_id: "32548"
_4images_cat_id: "2426"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32548 -->
Oben in der Mitte der Container steckt je eine Metallachse. Elektromagnete am "Kranhaken" setzten da an und zogen die Container hoch.
