---
layout: "image"
title: "DSC05878"
date: "2011-09-25T20:36:33"
picture: "modelle023.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32197
- /detailsd0fa-2.html
imported:
- "2019"
_4images_image_id: "32197"
_4images_cat_id: "2426"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32197 -->
