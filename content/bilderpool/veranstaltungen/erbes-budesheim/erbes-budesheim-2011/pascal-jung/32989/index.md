---
layout: "image"
title: "Cockpit"
date: "2011-09-30T16:20:54"
picture: "IMG_6186.JPG"
weight: "10"
konstrukteure: 
- "Pascal Jung"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/32989
- /detailsf9aa-2.html
imported:
- "2019"
_4images_image_id: "32989"
_4images_cat_id: "2404"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32989 -->
Für meine Begriffe sind da etwas zu viele BS30 verbaut, aber gelungen ist es allemal.
