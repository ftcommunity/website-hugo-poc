---
layout: "image"
title: "Düsenjet"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim055.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32582
- /details81d6.html
imported:
- "2019"
_4images_image_id: "32582"
_4images_cat_id: "2404"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32582 -->
Mit Ausfahrfahrwerk und jede Menge Details im hinteren Rumpfteil.
