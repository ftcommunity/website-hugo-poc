---
layout: "image"
title: "Kirmesmodell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim095.jpg"
weight: "11"
konstrukteure: 
- "Karl Tillmetz (charlie2700)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32622
- /detailse9c2.html
imported:
- "2019"
_4images_image_id: "32622"
_4images_cat_id: "2411"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32622 -->
