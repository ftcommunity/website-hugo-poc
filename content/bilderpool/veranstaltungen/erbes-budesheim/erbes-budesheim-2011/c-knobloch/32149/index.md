---
layout: "image"
title: "Firestorm Megacoaster 2 (26)"
date: "2011-09-25T17:42:28"
picture: "modelle25.jpg"
weight: "76"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32149
- /detailse6b9-2.html
imported:
- "2019"
_4images_image_id: "32149"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32149 -->
