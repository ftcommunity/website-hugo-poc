---
layout: "image"
title: "Firestorm-Megacoaster von Christian Knobloch"
date: "2011-09-26T17:47:41"
picture: "dm053.jpg"
weight: "101"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32746
- /details0668-2.html
imported:
- "2019"
_4images_image_id: "32746"
_4images_cat_id: "2382"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32746 -->
Abstellgleis für einen Zug