---
layout: "image"
title: "Megacoaster"
date: "2011-09-27T20:47:57"
picture: "Megacoaster.jpg"
weight: "108"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/32826
- /detailsb138.html
imported:
- "2019"
_4images_image_id: "32826"
_4images_cat_id: "2382"
_4images_user_id: "1126"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32826 -->
