---
layout: "image"
title: "cknoblochfirestormmegacoaster30.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster30.jpg"
weight: "51"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32117
- /details4ccf-3.html
imported:
- "2019"
_4images_image_id: "32117"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32117 -->
