---
layout: "image"
title: "cknoblochfirestormmegacoaster20.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster20.jpg"
weight: "41"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32107
- /details394d.html
imported:
- "2019"
_4images_image_id: "32107"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32107 -->
