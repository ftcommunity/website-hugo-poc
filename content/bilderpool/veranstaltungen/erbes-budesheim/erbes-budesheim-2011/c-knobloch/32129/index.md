---
layout: "image"
title: "Firestorm Megacoaster 2 (6)"
date: "2011-09-25T17:42:28"
picture: "modelle05.jpg"
weight: "56"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32129
- /details35a9.html
imported:
- "2019"
_4images_image_id: "32129"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32129 -->
