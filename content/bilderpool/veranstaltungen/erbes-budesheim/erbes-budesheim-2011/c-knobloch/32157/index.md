---
layout: "image"
title: "Firestorm Megacoaster 2 (34)"
date: "2011-09-25T17:42:28"
picture: "modelle33.jpg"
weight: "84"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32157
- /details10f7.html
imported:
- "2019"
_4images_image_id: "32157"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32157 -->
