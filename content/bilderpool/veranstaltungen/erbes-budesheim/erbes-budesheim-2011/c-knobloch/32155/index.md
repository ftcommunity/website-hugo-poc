---
layout: "image"
title: "Firestorm Megacoaster 2 (32)"
date: "2011-09-25T17:42:28"
picture: "modelle31.jpg"
weight: "82"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32155
- /details891e.html
imported:
- "2019"
_4images_image_id: "32155"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32155 -->
