---
layout: "image"
title: "cknoblochfirestormmegacoaster17.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster17.jpg"
weight: "38"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32104
- /details510c.html
imported:
- "2019"
_4images_image_id: "32104"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32104 -->
