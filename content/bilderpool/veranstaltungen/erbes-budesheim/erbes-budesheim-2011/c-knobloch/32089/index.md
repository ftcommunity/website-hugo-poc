---
layout: "image"
title: "cknoblochfirestormmegacoaster02.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster02.jpg"
weight: "23"
konstrukteure: 
- "C-Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32089
- /details52ca.html
imported:
- "2019"
_4images_image_id: "32089"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32089 -->
