---
layout: "image"
title: "ft Convention 2011"
date: "2011-09-25T23:58:50"
picture: "ftconvention1_2.jpg"
weight: "8"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/32523
- /detailsd630.html
imported:
- "2019"
_4images_image_id: "32523"
_4images_cat_id: "2397"
_4images_user_id: "130"
_4images_image_date: "2011-09-25T23:58:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32523 -->
Ein tolles Großmodell. Sehr imposant und doch grazil. Die ganzen elekrtrisch verstellbaren Teile sind nur Klasse.
