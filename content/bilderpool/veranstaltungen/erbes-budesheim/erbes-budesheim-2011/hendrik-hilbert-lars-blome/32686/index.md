---
layout: "image"
title: "Industriemodell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim159.jpg"
weight: "2"
konstrukteure: 
- "Lars Blome"
- "Hendrik H."
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32686
- /details4330.html
imported:
- "2019"
_4images_image_id: "32686"
_4images_cat_id: "2433"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "159"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32686 -->
