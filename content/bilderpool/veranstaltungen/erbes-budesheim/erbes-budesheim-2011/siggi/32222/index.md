---
layout: "image"
title: "DSC05928"
date: "2011-09-25T20:36:33"
picture: "modelle048.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32222
- /detailsc68d.html
imported:
- "2019"
_4images_image_id: "32222"
_4images_cat_id: "2394"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32222 -->
