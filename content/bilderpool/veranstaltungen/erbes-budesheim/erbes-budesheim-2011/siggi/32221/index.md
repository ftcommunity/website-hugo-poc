---
layout: "image"
title: "DSC05927"
date: "2011-09-25T20:36:33"
picture: "modelle047.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32221
- /detailsb70c-2.html
imported:
- "2019"
_4images_image_id: "32221"
_4images_cat_id: "2394"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32221 -->
