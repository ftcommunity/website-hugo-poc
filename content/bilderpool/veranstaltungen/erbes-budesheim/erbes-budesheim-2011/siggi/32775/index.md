---
layout: "image"
title: "Modelle auf der FT-Convention 2011"
date: "2011-09-26T17:47:41"
picture: "dm082.jpg"
weight: "10"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "-?-"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32775
- /detailsc500-2.html
imported:
- "2019"
_4images_image_id: "32775"
_4images_cat_id: "2394"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32775 -->
