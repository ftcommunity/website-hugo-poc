---
layout: "image"
title: "DSC05961"
date: "2011-09-25T20:36:33"
picture: "modelle075.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32249
- /details162a.html
imported:
- "2019"
_4images_image_id: "32249"
_4images_cat_id: "2425"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32249 -->
