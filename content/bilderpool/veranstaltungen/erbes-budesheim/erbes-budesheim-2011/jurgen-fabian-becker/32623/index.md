---
layout: "image"
title: "Durch die Halle fahrender Bagger"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim096.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32623
- /details4e9d.html
imported:
- "2019"
_4images_image_id: "32623"
_4images_cat_id: "2425"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32623 -->
Natürlich ferngesteuert.
