---
layout: "image"
title: "ft2.jpg"
date: "2011-09-27T16:27:49"
picture: "ft2.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32813
- /details8d35-3.html
imported:
- "2019"
_4images_image_id: "32813"
_4images_cat_id: "2383"
_4images_user_id: "1007"
_4images_image_date: "2011-09-27T16:27:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32813 -->
