---
layout: "image"
title: "ft1.jpg"
date: "2011-09-27T16:27:49"
picture: "ft1.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Tobias Horst (tobs9578)"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- /php/details/32812
- /detailsfb6d.html
imported:
- "2019"
_4images_image_id: "32812"
_4images_cat_id: "2383"
_4images_user_id: "1007"
_4images_image_date: "2011-09-27T16:27:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32812 -->
