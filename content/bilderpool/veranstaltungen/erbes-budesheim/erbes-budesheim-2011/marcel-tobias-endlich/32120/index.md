---
layout: "image"
title: "Tisch von Tobias"
date: "2011-09-25T15:25:21"
picture: "modelle3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32120
- /details769e.html
imported:
- "2019"
_4images_image_id: "32120"
_4images_cat_id: "2383"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T15:25:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32120 -->
Maom-Spender, leider außer Betrieb, Traktor, Truck mit Tieflader und Schmalspurschlepper
