---
layout: "image"
title: "Sylvia Falk erklärt den Kranwagen"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim027.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32554
- /details0596.html
imported:
- "2019"
_4images_image_id: "32554"
_4images_cat_id: "2390"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32554 -->
