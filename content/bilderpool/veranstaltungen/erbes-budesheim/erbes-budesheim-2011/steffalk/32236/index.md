---
layout: "image"
title: "DSC05945"
date: "2011-09-25T20:36:33"
picture: "modelle062.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32236
- /details4246.html
imported:
- "2019"
_4images_image_id: "32236"
_4images_cat_id: "2390"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32236 -->
