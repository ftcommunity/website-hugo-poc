---
layout: "image"
title: "Didaktik"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim057.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32584
- /details304f.html
imported:
- "2019"
_4images_image_id: "32584"
_4images_cat_id: "2444"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32584 -->
