---
layout: "image"
title: "DSC06159"
date: "2011-09-25T20:36:34"
picture: "modelle167.jpg"
weight: "17"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32341
- /details5ad9-2.html
imported:
- "2019"
_4images_image_id: "32341"
_4images_cat_id: "2400"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "167"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32341 -->
