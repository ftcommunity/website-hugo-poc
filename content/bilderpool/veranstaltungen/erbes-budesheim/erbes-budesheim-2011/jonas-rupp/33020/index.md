---
layout: "image"
title: "Paketzentrum mit Rundregallager"
date: "2011-09-30T19:49:20"
picture: "paketzentrummitrundregallager12.jpg"
weight: "38"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan-Jonas Rupp"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/33020
- /detailsd80b-2.html
imported:
- "2019"
_4images_image_id: "33020"
_4images_cat_id: "2400"
_4images_user_id: "1030"
_4images_image_date: "2011-09-30T19:49:20"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33020 -->
Das Modell ist fertig aufgebaut. Leider ist die Programmierung noch nicht fertig :-(
Kurze Funktionsbeschreibung:
 - vorne links wird eins der auf dem Bild zusehenden Pakete in die Anlage geschoben werden; jedes Paket verfügt über ein Etikett, auf der der Addressat vermerkt ist (Name, Straße, PLZ, Land) ; am unteren Rand jedes Pakets befindet sich ein Strichcode, indem das Land und die PLZ im Binärsystem codiert sind (z.B.: 01001010000011000000000001111000 steht für eine Person, die aus Halle - 06120 - in Deutschland kommt)
 - nun fährt das Paket per Fließband zum Einleser
 - der Einleser, bestehend aus Schrittmotor und einem CNY70-Eigenbau, liest den Strichcode des Pakets ein
 - das Paket wird nun über verschiedene Fließbänder zum Einlagerer des Rundregallagers transportiert
 - wenn das Paket richtig liegt schiebt der Einschieber des höhenverstellbaren Einlagerers das Paket an seine Platz im Regalsystem
 - das Rundregallager besteht aus dem Aufzug (dieser wählt die Höhenposition des Pakets) und den hängenden Regaleinheiten (diese können sich auf ft-Laufschienen 240 bewegen; sie bestimmen in welche Spalte das Paket geschoben wird)
 - nun wird gewartet bis sich mindestens 6 Pakete in die gleiche Region (erste Ziffer der PLZ) im Lagersystem befinden und diese 6 Pakete werden dann durch ft-Pneumatik-Zylinder aus dem Regal herraus gestoßen

Ich hoffe meine Erklährung ist einigermaßen Verständlich! Wenn ihr weitere Fragen habt -> einfach einen Kommentar schreiben ;-)
