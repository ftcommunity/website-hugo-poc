---
layout: "image"
title: "Umlaufendes Hochregallager"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim138.jpg"
weight: "20"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32665
- /detailse54a-2.html
imported:
- "2019"
_4images_image_id: "32665"
_4images_cat_id: "2400"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "138"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32665 -->
Detailblick auf die Zufuhranlage.
