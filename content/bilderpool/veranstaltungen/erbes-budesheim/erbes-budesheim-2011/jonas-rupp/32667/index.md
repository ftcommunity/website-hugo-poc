---
layout: "image"
title: "Umlaufendes Hochregallager"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim140.jpg"
weight: "22"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32667
- /details794a.html
imported:
- "2019"
_4images_image_id: "32667"
_4images_cat_id: "2400"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "140"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32667 -->
Blick ins praktisch leere Innere. Alles geschieht von außen.
