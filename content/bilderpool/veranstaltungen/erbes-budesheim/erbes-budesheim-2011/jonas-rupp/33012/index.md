---
layout: "image"
title: "Paketzentrum mit Rundregallager"
date: "2011-09-30T19:49:20"
picture: "paketzentrummitrundregallager04.jpg"
weight: "30"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan-Jonas Rupp"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- /php/details/33012
- /details87be-2.html
imported:
- "2019"
_4images_image_id: "33012"
_4images_cat_id: "2400"
_4images_user_id: "1030"
_4images_image_date: "2011-09-30T19:49:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33012 -->
Modell beim Aufbau
