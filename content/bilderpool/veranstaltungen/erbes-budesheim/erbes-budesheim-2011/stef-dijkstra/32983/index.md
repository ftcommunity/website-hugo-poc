---
layout: "image"
title: "Hochregelar Stef Dijkstra"
date: "2011-09-28T17:25:57"
picture: "hochregelarstefdijkstra1.jpg"
weight: "11"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32983
- /details4163.html
imported:
- "2019"
_4images_image_id: "32983"
_4images_cat_id: "2422"
_4images_user_id: "22"
_4images_image_date: "2011-09-28T17:25:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32983 -->
