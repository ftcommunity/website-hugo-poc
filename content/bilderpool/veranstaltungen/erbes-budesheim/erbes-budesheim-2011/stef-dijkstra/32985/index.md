---
layout: "image"
title: "Hochregelar Stef Dijkstra"
date: "2011-09-28T17:25:57"
picture: "hochregelarstefdijkstra3.jpg"
weight: "13"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32985
- /details47c7-2.html
imported:
- "2019"
_4images_image_id: "32985"
_4images_cat_id: "2422"
_4images_user_id: "22"
_4images_image_date: "2011-09-28T17:25:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32985 -->
