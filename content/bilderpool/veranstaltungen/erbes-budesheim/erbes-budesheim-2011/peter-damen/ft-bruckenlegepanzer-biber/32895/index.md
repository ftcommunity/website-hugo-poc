---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:30:11"
picture: "brueckenlegepanzerbiber21.jpg"
weight: "21"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32895
- /details2d68-2.html
imported:
- "2019"
_4images_image_id: "32895"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:30:11"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32895 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Aluminium Panzerschnellbrücke:
2 x 2,6 kg  = 5,2 kg
2 x 1 m =  2,0 m  (statt 2x 1,1 = 2,2m Massstab 1:10).  


Der Brückenlegepanzer mit der namensgebenden Panzerschnellbrücke Biber ist dazu vorgesehen, im Gefecht je nach Geländebeschaffenheit Geländeeinschnitte wie Gewässer und Schluchten bis 20 Meter Breite zu überwinden. Die Panzerschnellbrücke ist 22 Meter lang, 4 Meter breit und kann innerhalb von 2 bis 8 Minuten unter Panzerschutz ausgelegt werden. Ihr Gewicht beträgt etwa 9,94 Tonnen. 

Im Gegensatz zu anderen Brückenlegern, die ihre Brücke im Scherenverfahren verlegen, erfolgt der Verlegevorgang beim Biber horizontal im freien Vorbau. Der Vorteil dieser Verlegeart ist die deutliche niedrigere Silhouette, was jedoch wiederum mit einem erheblich höheren technischen Aufwand erkauft werden muss.
In Transportstellung liegen die jeweils 11 Meter langen symmetrischen Brückenhälften horizontal übereinander auf dem Haupt- und Heckausleger des Fahrzeugs. Zum Verlegen werden die beiden Elemente angehoben und die untere Brückenhälfte nach vorne verschoben. Nach dem Verbinden wird die gesamte Festbrücke mittels Hauptauslegers über dem Hindernis abgelegt. Die Aufnahme der Brücke erfolgt in umgekehrter Reihenfolge. Während des Verlegevorgangs ruht die gesamte Last auf dem am Bug befindlichen Stützschild, das die Standfestigkeit erhöht.

