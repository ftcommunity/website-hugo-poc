---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:30:04"
picture: "brueckenlegepanzerbiber15.jpg"
weight: "15"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32889
- /details1a6b.html
imported:
- "2019"
_4images_image_id: "32889"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:30:04"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32889 -->
