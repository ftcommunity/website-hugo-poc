---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:30:04"
picture: "brueckenlegepanzerbiber16.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32890
- /detailsa51f.html
imported:
- "2019"
_4images_image_id: "32890"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:30:04"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32890 -->
