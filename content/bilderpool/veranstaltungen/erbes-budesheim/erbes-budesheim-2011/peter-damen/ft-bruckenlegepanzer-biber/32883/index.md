---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:29:54"
picture: "brueckenlegepanzerbiber09.jpg"
weight: "9"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32883
- /details23c8.html
imported:
- "2019"
_4images_image_id: "32883"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:29:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32883 -->
