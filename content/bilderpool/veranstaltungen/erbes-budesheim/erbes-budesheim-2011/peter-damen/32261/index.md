---
layout: "image"
title: "DSC05984"
date: "2011-09-25T20:36:33"
picture: "modelle087.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32261
- /details9381.html
imported:
- "2019"
_4images_image_id: "32261"
_4images_cat_id: "2396"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32261 -->
