---
layout: "image"
title: "Uhr"
date: "2011-09-26T17:47:41"
picture: "dm072.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32765
- /details9a3b.html
imported:
- "2019"
_4images_image_id: "32765"
_4images_cat_id: "2410"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32765 -->
