---
layout: "image"
title: "Planetarium"
date: "2011-09-26T17:47:41"
picture: "dm069.jpg"
weight: "8"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dieter Meckel (Dinomania01)"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- /php/details/32762
- /detailsb628-2.html
imported:
- "2019"
_4images_image_id: "32762"
_4images_cat_id: "2410"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32762 -->
