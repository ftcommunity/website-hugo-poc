---
layout: "image"
title: "Geometers Planetarium"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim041.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32568
- /details61c7.html
imported:
- "2019"
_4images_image_id: "32568"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32568 -->
