---
layout: "comment"
hidden: true
title: "15934"
date: "2011-12-27T22:01:53"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

Es gibt ja sehr viel Know-how bei deinem Planetarium !......Kompliment.
Es erklärt deutlich den Lauf von Merkur, Venus und Erde um die Sonne. 
Wie die Getriebe mit Hilfe von Differentialen konzipiert wurden, ist in ft-pedia-Ausgabe 3/2011 sehr deutlicht beschrieben. Das erspart sehr viele Zahnräder..........
Ich habe deines Planetarium-Modell schon nachgebaut mit kleine änderungen.
Die Bilder des FT-Treffen in Erbes-Budemheim + ftpedia-ausgabe-4 reichen.

Auch der Sternbilder sind sehr gut erklärt und interessieren mir und meine Kinder (Antonie 12 Jahre + Annemieke 9 Jahre) sehr.
Mit Hilfe des Planetariumprogramms Stellarium hast du und deine Tochter Jutta eine sehre schöne (FT-passendes)  Merkatorprojektion des Sternenhimmels um die Ekliptik herum erstellt. Die Helligkeit der Sterne wird durch die Dicke der aufgemalten weißen Punkte sehr realistisch wiedergegeben. In Erbes-Budesheim war das im Dunkel sehr interessant.

Wäre es bitte möglich ein digitaler Kopie zu machen deiner Merkatorprojektion des Sternenhimmels ?  

Das Interesse für Astronomie hat mir in Erbes-Budesheim getriggert.  Meine Kinder waren leider nicht dabei.

Grüss,

Peter
Poederoyen NL