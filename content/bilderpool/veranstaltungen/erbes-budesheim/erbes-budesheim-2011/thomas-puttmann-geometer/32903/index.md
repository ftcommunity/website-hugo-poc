---
layout: "image"
title: "Planetarium + 50Hz-Uhr  Thomas Püttmann"
date: "2011-09-27T21:51:59"
picture: "planetariumhzuhr6.jpg"
weight: "17"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32903
- /details569b-2.html
imported:
- "2019"
_4images_image_id: "32903"
_4images_cat_id: "2410"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32903 -->
