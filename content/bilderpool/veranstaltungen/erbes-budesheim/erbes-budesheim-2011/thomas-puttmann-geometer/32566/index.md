---
layout: "image"
title: "Geometers Planetarium"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim039.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32566
- /detailse315-2.html
imported:
- "2019"
_4images_image_id: "32566"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32566 -->
