---
layout: "image"
title: "Kniegelenk-Presse (hobby-2), Ölförderpumpe und Turm"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim155.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/32682
- /details3031.html
imported:
- "2019"
_4images_image_id: "32682"
_4images_cat_id: "2387"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "155"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32682 -->
