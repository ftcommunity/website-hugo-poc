---
layout: "image"
title: "DSC06156"
date: "2011-09-25T20:36:34"
picture: "modelle165.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/32339
- /detailsecbd-2.html
imported:
- "2019"
_4images_image_id: "32339"
_4images_cat_id: "2401"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "165"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32339 -->
