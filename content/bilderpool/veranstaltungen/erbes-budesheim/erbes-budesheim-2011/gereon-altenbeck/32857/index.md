---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:00"
picture: "schwebebahngereonaltenbeck02.jpg"
weight: "20"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32857
- /detailseb47.html
imported:
- "2019"
_4images_image_id: "32857"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32857 -->
