---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:10"
picture: "schwebebahngereonaltenbeck12.jpg"
weight: "30"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32867
- /details62cf.html
imported:
- "2019"
_4images_image_id: "32867"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:10"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32867 -->
