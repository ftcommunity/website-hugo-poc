---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:00"
picture: "schwebebahngereonaltenbeck06.jpg"
weight: "24"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32861
- /detailsa851.html
imported:
- "2019"
_4images_image_id: "32861"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32861 -->
