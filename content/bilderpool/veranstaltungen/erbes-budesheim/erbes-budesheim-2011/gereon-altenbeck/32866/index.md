---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:10"
picture: "schwebebahngereonaltenbeck11.jpg"
weight: "29"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32866
- /details82c8-2.html
imported:
- "2019"
_4images_image_id: "32866"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32866 -->
