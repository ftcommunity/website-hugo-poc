---
layout: "image"
title: "Fischer-Tip-Fans"
date: "2011-09-27T23:24:31"
picture: "fischertip2.jpg"
weight: "2"
konstrukteure: 
- "Fischer-Tip-Fans"
fotografen:
- "Peter  Poederoyen NL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/32940
- /details1b8b-2.html
imported:
- "2019"
_4images_image_id: "32940"
_4images_cat_id: "2420"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32940 -->
