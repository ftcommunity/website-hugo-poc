---
layout: "image"
title: "Rasenmäher (von H.A.R.R.Y.)"
date: "2013-09-29T21:54:09"
picture: "convention04.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Lars"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37447
- /details0ba4.html
imported:
- "2019"
_4images_image_id: "37447"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37447 -->
Bilder von der Convention 2013
Detailaufnahme
Bild 1 von 2
.
Modell:            Rasenmäher
Konstrukteur:  H.A.R.R.Y.
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.