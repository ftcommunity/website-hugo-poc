---
layout: "image"
title: "Drone (von Hendrik, Lars)"
date: "2013-09-29T21:54:21"
picture: "convention15.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37458
- /details99a1.html
imported:
- "2019"
_4images_image_id: "37458"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37458 -->
Bilder von der Convention 2013
Gesamtansicht
Bild 1 von 1
.
Modell:            Drone
Konstrukteur:  Hendrik, Lars
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.