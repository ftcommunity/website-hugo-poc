---
layout: "image"
title: "Industrieanlage (von Endlich)"
date: "2013-09-29T21:54:21"
picture: "convention13.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37456
- /detailsfc4f.html
imported:
- "2019"
_4images_image_id: "37456"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37456 -->
Bilder von der Convention 2013
Detailaufnahme
Bild 2 von 2
.
Modell:            Industrieanlage
Konstrukteur:  Marcel Endlich
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.