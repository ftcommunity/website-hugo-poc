---
layout: "image"
title: "Riesenkran"
date: "2015-10-01T13:37:10"
picture: "Riesenkran_2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41976
- /details3dfd-2.html
imported:
- "2019"
_4images_image_id: "41976"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41976 -->
