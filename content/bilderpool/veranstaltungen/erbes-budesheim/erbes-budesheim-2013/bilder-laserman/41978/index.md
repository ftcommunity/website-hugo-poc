---
layout: "image"
title: "Kugelbahn 1"
date: "2015-10-01T13:37:10"
picture: "Kugelbahn_1.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41978
- /detailsa684.html
imported:
- "2019"
_4images_image_id: "41978"
_4images_cat_id: "3120"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41978 -->
