---
layout: "image"
title: "Kirmesmodelle (von Jan Wilhelm Dekker)"
date: "2013-09-29T21:54:09"
picture: "convention02.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "?"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37445
- /details47ff-2.html
imported:
- "2019"
_4images_image_id: "37445"
_4images_cat_id: "2788"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37445 -->
Bilder von der Convention 2013
Langzeitbelichtung
Bild 1 von 1
.
Modell:            Kirmes(modelle)
Konstrukteur:  Jan Wilhelm Dekker
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.