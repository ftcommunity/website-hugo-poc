---
layout: "image"
title: "Fräsmaschine mit Werkzeugwechsler"
date: "2013-09-30T23:47:38"
picture: "DSC00263_800x800.jpg"
weight: "13"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37472
- /detailsbdfe.html
imported:
- "2019"
_4images_image_id: "37472"
_4images_cat_id: "2784"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37472 -->
