---
layout: "image"
title: "Firestorm (von Christian)"
date: "2013-09-29T21:54:09"
picture: "convention07.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Lars"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/37450
- /detailsc24e.html
imported:
- "2019"
_4images_image_id: "37450"
_4images_cat_id: "2795"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37450 -->
Bilder von der Convention 2013
Actionaufnahme
Bild 1 von 2
.
Modell:            Firestorm
Konstrukteur:  Christian Knobloch
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.