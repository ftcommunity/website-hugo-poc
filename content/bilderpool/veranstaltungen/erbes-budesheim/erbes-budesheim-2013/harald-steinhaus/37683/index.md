---
layout: "image"
title: "IMG_9567.JPG"
date: "2013-10-06T15:44:49"
picture: "IMG_9567.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37683
- /detailsc604.html
imported:
- "2019"
_4images_image_id: "37683"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T15:44:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37683 -->
Noch'n Allrad. 
Hier aber mit Knicklenkung und voll geländegängig. Die Räder sind immer zu zweit an einer Schwinge montiert und über dasselbe "Differential-Antriebsrad" (31414) in der Wippachse angetrieben. Der Vorderwagen lenkt um die beiden roten Gelenkwürfel in der Hochachse, und rollt (in der Längsachse) um die beiden Gelenksteine (31008, aber schwarz) unter den beiden Winkeln 60°, durch die die Antriebsachse hindurch führt.
