---
layout: "image"
title: "IMG_9683.JPG"
date: "2013-10-06T15:55:40"
picture: "IMG_9683.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37684
- /detailsc7cf.html
imported:
- "2019"
_4images_image_id: "37684"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T15:55:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37684 -->
Der Stand mitten im Aufbau. Der Jumping-Mittelbau steht, und ein paar Gondeln sind auch schon dran. Der Rest befindet sich noch auf Wagen 2 (vorn, hinter der Zugmaschine, mit den blauen und grünen Handläufen oben drauf) und Wagen 3 (an der Tischkante oben rechts). Wagen 2 wird gleich zum Bestandteil des Podiums. Details hier: http://www.ftcommunity.de/categories.php?cat_id=2752
