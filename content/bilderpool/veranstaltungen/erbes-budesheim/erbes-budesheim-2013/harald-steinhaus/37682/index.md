---
layout: "image"
title: "IMG_9566.JPG"
date: "2013-10-06T15:27:06"
picture: "IMG_9566.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37682
- /detailsa2a2.html
imported:
- "2019"
_4images_image_id: "37682"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T15:27:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37682 -->
Technische Spielereien:
oben: Ergebnisse aus dem 3D-Drucker. Der Drehkranz Z30 ist von MisterWho. Der Rest umfasst drei Anläufe, um einen Schleifring mit 6 Bahnen und einen Universal-LED-Baustein herzustellen -- alles noch nicht das Wahre.
rechts: ein Harmonic-Drive-Getriebe. Mittendrin ein gemoddeter ft-Zylinder, quer halbiert und bis auf zwei vorstehende Nasen weiter zurückgeschnitten. Die Nasen greifen um das Z15 und heben dort die umlaufende Kette davon ab. Bei jeder Umdrehung landet ein derart abgehobenes Kettenteil um ein paar Zähne weiter zurück auf dem Zahnrad. Das ergibt ein kompaktes Untersetzungsgetriebe.
unten: Der fest eingebaute Ablauf lässt einen Motor immer vor und zurück fahren, während der andere zeitversetzt folgt und beide sich kurzzeitig an den Enden befinden. Die Steuerung ist auf dem Bild erkennbar -- man muss nur sehen können...
