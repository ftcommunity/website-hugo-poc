---
layout: "comment"
hidden: true
title: "18383"
date: "2013-10-07T20:29:30"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Man wundert sich, aber es ist gerade umgekehrt: im Tarnmodus kommt er viiiiel weiter als im sichtbaren Modus! So richtig erklären kann ich mir das selber nicht, auch wenn ich schon eine grobe Vermutung habe. Die absoluten Werte sind natürlich geheim.


Gruß,
Harald