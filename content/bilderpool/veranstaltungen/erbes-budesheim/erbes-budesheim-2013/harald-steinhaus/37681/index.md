---
layout: "image"
title: "IMG_9560.JPG"
date: "2013-10-06T15:16:40"
picture: "IMG_9560.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37681
- /details8ad0.html
imported:
- "2019"
_4images_image_id: "37681"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T15:16:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37681 -->
Links: Ein Land Rover mit Allradantrieb und Federung rund herum - an der Technik ist nichts gemoddet. Einzelheiten: http://www.ftcommunity.de/categories.php?cat_id=2801

Rechts: ein Pickup mit Frontantrieb. ( http://www.ftcommunity.de/categories.php?cat_id=2761 ) In der Vorderachse sind zwei gemoddete BS15-Loch verbaut.
