---
layout: "image"
title: "eb001.jpg"
date: "2013-10-03T09:29:05"
picture: "eb001.jpg"
weight: "2"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Daniel A."
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/37506
- /details4a1b-2.html
imported:
- "2019"
_4images_image_id: "37506"
_4images_cat_id: "2796"
_4images_user_id: "1"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37506 -->
