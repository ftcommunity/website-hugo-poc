---
layout: "image"
title: "Kran"
date: "2013-09-30T23:47:38"
picture: "DSC00271_800x800.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- /php/details/37475
- /details9c05-2.html
imported:
- "2019"
_4images_image_id: "37475"
_4images_cat_id: "2792"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37475 -->
