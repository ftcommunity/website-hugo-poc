---
layout: "image"
title: "Ferngesteuerter Schopf-Bergbau-Radlader"
date: "2013-10-01T23:57:42"
picture: "schopfbergbauradlader3.jpg"
weight: "3"
konstrukteure: 
- "Jörg Busch"
- "Erik Busch"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37485
- /detailsa804.html
imported:
- "2019"
_4images_image_id: "37485"
_4images_cat_id: "2785"
_4images_user_id: "22"
_4images_image_date: "2013-10-01T23:57:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37485 -->
mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung
