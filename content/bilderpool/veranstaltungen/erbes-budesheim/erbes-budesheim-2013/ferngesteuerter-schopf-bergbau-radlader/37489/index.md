---
layout: "image"
title: "Ferngesteuerter Schopf-Bergbau-Radlader"
date: "2013-10-01T23:57:42"
picture: "schopfbergbauradlader7.jpg"
weight: "7"
konstrukteure: 
- "Jörg Busch"
- "Erik Busch"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37489
- /detailsf7df-2.html
imported:
- "2019"
_4images_image_id: "37489"
_4images_cat_id: "2785"
_4images_user_id: "22"
_4images_image_date: "2013-10-01T23:57:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37489 -->
mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung
