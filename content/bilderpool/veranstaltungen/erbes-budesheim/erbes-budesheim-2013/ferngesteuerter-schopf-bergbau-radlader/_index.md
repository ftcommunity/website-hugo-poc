---
layout: "overview"
title: "Ferngesteuerter Schopf-Bergbau-Radlader"
date: 2020-02-22T09:06:44+01:00
legacy_id:
- /php/categories/2785
- /categories02f0.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2785 --> 
Ferngesteuerter Schopf-Bergbau-Radlader mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung
