---
layout: "image"
title: "Rollwagen vom Mega Kran"
date: "2012-10-01T20:50:59"
picture: "ftconvention24.jpg"
weight: "9"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35647
- /details1fb1-2.html
imported:
- "2019"
_4images_image_id: "35647"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35647 -->
