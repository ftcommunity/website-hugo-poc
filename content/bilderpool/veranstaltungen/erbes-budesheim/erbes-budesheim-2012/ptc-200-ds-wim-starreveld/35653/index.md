---
layout: "image"
title: "Schöne Querverstrebung"
date: "2012-10-01T20:50:59"
picture: "ftconvention30.jpg"
weight: "15"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35653
- /details2379.html
imported:
- "2019"
_4images_image_id: "35653"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35653 -->
Ob da ein Winkelträger gemoddet wurde?
