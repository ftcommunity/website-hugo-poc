---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-01T20:50:59"
picture: "ftconvention29.jpg"
weight: "14"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35652
- /details6b7f-2.html
imported:
- "2019"
_4images_image_id: "35652"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35652 -->
Hier die Gesammtansicht. Garnicht so einfach das Mordsteil komplett aufs Bild zu bekommen.
