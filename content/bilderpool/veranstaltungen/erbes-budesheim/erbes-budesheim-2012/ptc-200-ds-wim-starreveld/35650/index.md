---
layout: "image"
title: "Die Gesamtansicht ..."
date: "2012-10-01T20:50:59"
picture: "ftconvention27.jpg"
weight: "12"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35650
- /details3d32.html
imported:
- "2019"
_4images_image_id: "35650"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35650 -->
... vom Unterteil des Krans
