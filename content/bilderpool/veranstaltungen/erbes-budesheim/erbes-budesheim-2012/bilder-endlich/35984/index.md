---
layout: "image"
title: "DSC09193"
date: "2012-10-20T23:33:50"
picture: "conv098.jpg"
weight: "79"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/35984
- /detailse336-2.html
imported:
- "2019"
_4images_image_id: "35984"
_4images_cat_id: "2680"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:50"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35984 -->
