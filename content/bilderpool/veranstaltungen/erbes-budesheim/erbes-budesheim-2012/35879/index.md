---
layout: "image"
title: "Fahrsimulator"
date: "2012-10-20T19:40:41"
picture: "IMG_8067.JPG"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35879
- /detailsd582.html
imported:
- "2019"
_4images_image_id: "35879"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T19:40:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35879 -->
Einfach klasse gemacht: die Bewegungen der Plattform sind mit dem Bahnverlauf im Video sauber koordiniert. Hier geht's gerade bergauf, und entsprechend kippt die Plattform nach hinten.
