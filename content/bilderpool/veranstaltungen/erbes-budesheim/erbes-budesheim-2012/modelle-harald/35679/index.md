---
layout: "image"
title: "Haralds Bombermodell mit Ralfs Trippel-Trappel Robbi"
date: "2012-10-01T20:51:00"
picture: "ftconvention56.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35679
- /details82d5.html
imported:
- "2019"
_4images_image_id: "35679"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35679 -->
Da haben Ralf und ich uns einen Spass draus gemacht Hugo auf einigen Modellen zu fotografieren.
