---
layout: "image"
title: "Rückbank und Fderungszylinder"
date: "2012-10-01T20:51:00"
picture: "ftconvention47.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35670
- /detailsa55d.html
imported:
- "2019"
_4images_image_id: "35670"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35670 -->
Hier hab ich einen Blick auf die Rückbank und einen Zylinder der Hydro-Pneumatik, in diesem Fall wohl eher nur Pneumatik.
