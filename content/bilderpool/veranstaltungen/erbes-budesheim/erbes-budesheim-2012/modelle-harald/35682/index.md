---
layout: "image"
title: "Kardangelenk"
date: "2012-10-01T20:51:00"
picture: "ftconvention59.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35682
- /detailsf815.html
imported:
- "2019"
_4images_image_id: "35682"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35682 -->
Ein Funktionsmodell eines Kardangelenkes
