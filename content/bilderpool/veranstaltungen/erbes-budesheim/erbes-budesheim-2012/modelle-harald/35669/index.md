---
layout: "image"
title: "Inneneinrichtung der 'Göttin'"
date: "2012-10-01T20:51:00"
picture: "ftconvention46.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35669
- /detailsbb72.html
imported:
- "2019"
_4images_image_id: "35669"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35669 -->
Hier die Vordersitze.