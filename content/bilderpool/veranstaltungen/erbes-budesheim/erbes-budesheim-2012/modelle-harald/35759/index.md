---
layout: "image"
title: "Citroen von Harald Steinhaus"
date: "2012-10-03T10:59:00"
picture: "convention40.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35759
- /detailsdeff.html
imported:
- "2019"
_4images_image_id: "35759"
_4images_cat_id: "2652"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35759 -->
