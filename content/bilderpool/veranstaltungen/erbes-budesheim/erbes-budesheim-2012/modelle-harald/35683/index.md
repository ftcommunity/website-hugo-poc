---
layout: "image"
title: "Panzer"
date: "2012-10-01T20:51:00"
picture: "ftconvention60.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35683
- /details58db.html
imported:
- "2019"
_4images_image_id: "35683"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35683 -->
