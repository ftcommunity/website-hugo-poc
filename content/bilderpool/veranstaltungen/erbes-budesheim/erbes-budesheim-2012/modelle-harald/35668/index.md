---
layout: "image"
title: "Citroen DS (Deesse, die Göttin)"
date: "2012-10-01T20:51:00"
picture: "ftconvention45.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35668
- /detailsaaa5.html
imported:
- "2019"
_4images_image_id: "35668"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35668 -->
Braucht man nicht weiter zu beschreiben ... ;-)
