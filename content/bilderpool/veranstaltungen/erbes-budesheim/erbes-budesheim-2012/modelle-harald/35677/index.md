---
layout: "image"
title: "Der Tarnkappenbomber war auch wieder dabei .."
date: "2012-10-01T20:51:00"
picture: "ftconvention54.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35677
- /details9224-2.html
imported:
- "2019"
_4images_image_id: "35677"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35677 -->
Ich hatte ihn noch kurz gesehen bevor er sich plötzlich getarnt hatte. Tja, da war ich etwas zu langsam.
