---
layout: "comment"
hidden: true
title: "18468"
date: "2013-11-06T00:07:28"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Ingo,
das sieht nur so aus - die "Schieflage" der Stiftführung ist nicht der Konstruktion, sondern acht Stunden Convention geschuldet - an dem Plotterkopf wurde so oft 'herumjustiert', dass er am Ende des Samstags "in den Seilen hing". Die Konstruktion ist so stabil, dass sich da (zumindest beim Plotten) nichts verschiebt.
Gruß, Dirk