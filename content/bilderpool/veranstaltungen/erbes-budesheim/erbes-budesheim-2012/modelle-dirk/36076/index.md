---
layout: "image"
title: "Plotter und Funkuhr - Dirk Fox"
date: "2012-10-29T20:05:15"
picture: "Dirk_Fox_-_Plotter_und_Funkuhr.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/36076
- /details1025.html
imported:
- "2019"
_4images_image_id: "36076"
_4images_cat_id: "2655"
_4images_user_id: "1126"
_4images_image_date: "2012-10-29T20:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36076 -->
Minimalistischer Präzisionsplotter (siehe ft:pedia 4/2011, 1/2012 und 2/2012) und TX als Funkuhr (siehe ft:pedia 3/2012).