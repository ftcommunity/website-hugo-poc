---
layout: "image"
title: "Ralfs Trippel Trappel Robbi auf Haralds Bomber"
date: "2012-10-01T20:51:00"
picture: "ftconvention73.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35696
- /details8920.html
imported:
- "2019"
_4images_image_id: "35696"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35696 -->
Da haben Ralf und ich uns einen Spass draus gemacht den Trippel Trappel Robbi auf einigen Modellen zu fotografieren. Hier mal von vorne auf dem Bomber von Harald zu sehen.
