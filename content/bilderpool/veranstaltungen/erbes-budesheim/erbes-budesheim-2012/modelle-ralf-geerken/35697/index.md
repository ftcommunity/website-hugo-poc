---
layout: "image"
title: "Alberreien"
date: "2012-10-01T20:51:00"
picture: "ftconvention74.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35697
- /details208f.html
imported:
- "2019"
_4images_image_id: "35697"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35697 -->
Da haben Ralf und ich uns einen Spass draus gemacht den Trippel Trappel Robbi auf einigen Modellen zu fotografieren. Hier auf dem Unimog vom Claus W. Ludwig