---
layout: "overview"
title: "Rennauto von Andreas Tacke"
date: 2020-02-22T09:04:46+01:00
legacy_id:
- /php/categories/2644
- /categoriesc4b3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2644 --> 
Außerhalb der Kernzeiten zeigte Andreas Tacke ein rückstoßgetriebenes Rennauto, dass an einer Schnur entlang fuhr und vergleichsweise hohe Geschwindigkeiten erzielte. 
Für mich zugleich die Herausforderung, während der Fahrt (!) einige Bilder zu ergattern.