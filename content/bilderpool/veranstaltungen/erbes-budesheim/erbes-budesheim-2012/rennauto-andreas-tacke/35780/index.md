---
layout: "image"
title: "TSTs Rennauto 8"
date: "2012-10-03T21:24:35"
picture: "P1050111_verkleinert.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Bernhard Lehner (bflehner)"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/35780
- /details6e84.html
imported:
- "2019"
_4images_image_id: "35780"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T21:24:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35780 -->
Folgebild. Hier bestätigten sich die Rechnungen, die besagten, dass das Auto im kurzen Zeitraum der Aufnahme eines Einzelbildes 1-2 cm zurücklegt.
