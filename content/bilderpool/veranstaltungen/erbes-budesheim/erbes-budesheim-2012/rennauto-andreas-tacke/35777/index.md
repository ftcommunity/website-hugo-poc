---
layout: "image"
title: "TSTs Rennauto 5"
date: "2012-10-03T20:04:41"
picture: "P1050050_verkleinert.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Bernhard Lehner (bflehner)"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- /php/details/35777
- /details091b.html
imported:
- "2019"
_4images_image_id: "35777"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T20:04:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35777 -->
Gleiches Phänomen, andere Bilderserie.
