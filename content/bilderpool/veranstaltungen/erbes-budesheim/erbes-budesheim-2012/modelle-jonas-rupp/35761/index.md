---
layout: "image"
title: "Rundregallager von Jonas Rupp"
date: "2012-10-03T10:59:00"
picture: "convention42.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35761
- /details6e9f.html
imported:
- "2019"
_4images_image_id: "35761"
_4images_cat_id: "2672"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35761 -->
