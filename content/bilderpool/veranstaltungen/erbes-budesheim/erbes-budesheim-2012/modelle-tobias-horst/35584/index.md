---
layout: "image"
title: "Kirmesmodell 'Frisbee'"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim22.jpg"
weight: "4"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35584
- /details34c5.html
imported:
- "2019"
_4images_image_id: "35584"
_4images_cat_id: "2669"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35584 -->
Die Sitze mit Sicherheitsbügel.
