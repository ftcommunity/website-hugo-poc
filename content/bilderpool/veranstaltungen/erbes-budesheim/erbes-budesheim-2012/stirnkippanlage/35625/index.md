---
layout: "image"
title: "Stirnkippanlage"
date: "2012-10-01T20:50:59"
picture: "ftconvention02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35625
- /details62d4.html
imported:
- "2019"
_4images_image_id: "35625"
_4images_cat_id: "2658"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35625 -->
Stirnkippanlage mit Zahnelement nach einem Modell aus der "Hobby" Buchreihe der 1970 er Jahre.
Hobby 2, Band 6