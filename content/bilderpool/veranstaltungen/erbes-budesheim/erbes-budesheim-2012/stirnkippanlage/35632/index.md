---
layout: "image"
title: "Stirnkippanlage"
date: "2012-10-01T20:50:59"
picture: "ftconvention09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35632
- /detailsea55-4.html
imported:
- "2019"
_4images_image_id: "35632"
_4images_cat_id: "2658"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35632 -->
Hier sieht man das Zahnelement