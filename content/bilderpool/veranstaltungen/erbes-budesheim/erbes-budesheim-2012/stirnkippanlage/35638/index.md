---
layout: "image"
title: "Klappbrücke System 'Strobel'"
date: "2012-10-01T20:50:59"
picture: "ftconvention15.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35638
- /details9599.html
imported:
- "2019"
_4images_image_id: "35638"
_4images_cat_id: "2658"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35638 -->
