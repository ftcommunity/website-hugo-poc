---
layout: "image"
title: "Riesenrad"
date: "2015-10-01T13:37:10"
picture: "Riesenrad.jpg"
weight: "14"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42011
- /details1f85-2.html
imported:
- "2019"
_4images_image_id: "42011"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42011 -->
