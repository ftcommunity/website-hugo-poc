---
layout: "image"
title: "Münzsortierer"
date: "2015-10-01T13:37:10"
picture: "Mnzsortierer_3.jpg"
weight: "8"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42005
- /details21a4.html
imported:
- "2019"
_4images_image_id: "42005"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42005 -->
