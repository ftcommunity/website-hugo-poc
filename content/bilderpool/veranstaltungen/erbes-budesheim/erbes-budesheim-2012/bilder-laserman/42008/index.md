---
layout: "image"
title: "Riesenbagger von hinten"
date: "2015-10-01T13:37:10"
picture: "Riesenbagger_von_hinten.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42008
- /detailsb3c5.html
imported:
- "2019"
_4images_image_id: "42008"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42008 -->
