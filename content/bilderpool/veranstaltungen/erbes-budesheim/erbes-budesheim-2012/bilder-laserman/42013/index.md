---
layout: "image"
title: "XY-Tisch"
date: "2015-10-01T13:37:10"
picture: "XY-Tisch.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/42013
- /details6ede.html
imported:
- "2019"
_4images_image_id: "42013"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42013 -->
