---
layout: "image"
title: "Getränkeautomat von Lukas Kamm"
date: "2012-10-03T10:59:00"
picture: "convention39.jpg"
weight: "8"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35758
- /details7fea.html
imported:
- "2019"
_4images_image_id: "35758"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35758 -->
