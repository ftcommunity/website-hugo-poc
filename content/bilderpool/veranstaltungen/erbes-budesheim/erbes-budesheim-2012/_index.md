---
layout: "overview"
title: "Erbes-Büdesheim 2012"
date: 2020-02-22T09:04:46+01:00
legacy_id:
- /php/categories/2639
- /categories5e4d.html
- /categories60e7-2.html
- /categories78ee.html
- /categories8597.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2639 --> 
Die Fischertechnik Convention 2012 fand am 29.09 von 10 bis  16 Uhr in Erbes Büdesheim statt und wurde von Knobloch organisiert.