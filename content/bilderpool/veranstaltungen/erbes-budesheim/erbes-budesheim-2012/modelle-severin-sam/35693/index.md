---
layout: "image"
title: "Roboter Greifarm"
date: "2012-10-01T20:51:00"
picture: "ftconvention70.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35693
- /details47a4.html
imported:
- "2019"
_4images_image_id: "35693"
_4images_cat_id: "2647"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35693 -->
