---
layout: "image"
title: "6-Achs-Roboter von Severin"
date: "2012-10-03T10:59:00"
picture: "convention13_2.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35732
- /detailsd7dc-2.html
imported:
- "2019"
_4images_image_id: "35732"
_4images_cat_id: "2647"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35732 -->
