---
layout: "image"
title: "ft Truck"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim23.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35585
- /details2282.html
imported:
- "2019"
_4images_image_id: "35585"
_4images_cat_id: "2670"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35585 -->
Sehr schöner und funktioneller Truck