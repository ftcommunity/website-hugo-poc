---
layout: "image"
title: "Radlader von Strobel"
date: "2012-10-03T10:59:00"
picture: "convention34.jpg"
weight: "17"
konstrukteure: 
- "Strobel"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35753
- /detailscff4.html
imported:
- "2019"
_4images_image_id: "35753"
_4images_cat_id: "2670"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35753 -->
