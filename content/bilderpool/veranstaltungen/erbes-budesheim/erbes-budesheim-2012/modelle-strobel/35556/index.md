---
layout: "image"
title: "KlappbrückeSystem Strobel 3"
date: "2012-09-29T21:24:59"
picture: "convention15.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "fish"
uploadBy: "fish"
license: "unknown"
legacy_id:
- /php/details/35556
- /detailsd9e1.html
imported:
- "2019"
_4images_image_id: "35556"
_4images_cat_id: "2670"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35556 -->
Siehe KlappbrückeSystem Strobel1