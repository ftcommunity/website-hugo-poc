---
layout: "image"
title: "ETF Mining Truck - Prototyp - von Gereon Altenbeck"
date: "2012-10-03T10:59:00"
picture: "convention31.jpg"
weight: "5"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35750
- /detailsb08f.html
imported:
- "2019"
_4images_image_id: "35750"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35750 -->
