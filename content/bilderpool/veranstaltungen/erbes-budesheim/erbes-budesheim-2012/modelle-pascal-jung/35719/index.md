---
layout: "image"
title: "Technisches Datenblatt"
date: "2012-10-01T20:51:01"
picture: "ftconvention96.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35719
- /detailsa1ca.html
imported:
- "2019"
_4images_image_id: "35719"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:01"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35719 -->
