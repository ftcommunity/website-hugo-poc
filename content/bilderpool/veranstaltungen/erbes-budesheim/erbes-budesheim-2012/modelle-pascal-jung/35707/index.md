---
layout: "image"
title: "Prospektbild vom neuen Minenbagger"
date: "2012-10-01T20:51:00"
picture: "ftconvention84.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35707
- /details8e40.html
imported:
- "2019"
_4images_image_id: "35707"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35707 -->
Hier das Bild von der Tieflöffelausführung.