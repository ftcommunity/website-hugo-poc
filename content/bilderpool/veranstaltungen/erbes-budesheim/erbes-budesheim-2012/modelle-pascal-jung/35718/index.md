---
layout: "image"
title: "Beschreibung vom Bagger"
date: "2012-10-01T20:51:01"
picture: "ftconvention95.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35718
- /detailsb241-2.html
imported:
- "2019"
_4images_image_id: "35718"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:01"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35718 -->
