---
layout: "image"
title: "Die stilisierte Schaufel"
date: "2012-10-01T20:51:00"
picture: "ftconvention91.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35714
- /detailsebde-2.html
imported:
- "2019"
_4images_image_id: "35714"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35714 -->
Einfach aber doch sofort erkennbar.