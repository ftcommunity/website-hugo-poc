---
layout: "image"
title: "Auslegerarm vom riesigen Minenbagger"
date: "2012-10-01T20:51:01"
picture: "ftconvention93.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35716
- /detailsefa4.html
imported:
- "2019"
_4images_image_id: "35716"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:01"
_4images_image_order: "93"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35716 -->
