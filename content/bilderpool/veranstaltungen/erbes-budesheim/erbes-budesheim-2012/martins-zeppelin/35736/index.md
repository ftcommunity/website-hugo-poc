---
layout: "image"
title: "Wall-E von Martin Westphal"
date: "2012-10-03T10:59:00"
picture: "convention17.jpg"
weight: "7"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35736
- /details8983.html
imported:
- "2019"
_4images_image_id: "35736"
_4images_cat_id: "2648"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35736 -->
