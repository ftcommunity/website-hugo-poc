---
layout: "comment"
hidden: true
title: "17330"
date: "2012-10-01T16:11:17"
uploadBy:
- "DenkMal"
license: "unknown"
imported:
- "2019"
---
Die Kombination BS5, BS7,5°, BS5 hatte ich nicht nur wegen der Maße gewählt, sondern vor allem, um mir ein Nachrüsten mit 30er-Streben zu ermöglichen (Verbinden der WTs über die drei roten BS hinweg). Bei Auf- und Abbau, sowie Transport zeigte sich bisher mehrfach, dass hier tatsächlich eine Schwachstelle ist.