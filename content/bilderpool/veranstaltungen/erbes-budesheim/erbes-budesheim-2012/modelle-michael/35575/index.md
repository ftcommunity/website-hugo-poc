---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim13.jpg"
weight: "13"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35575
- /details3fa6.html
imported:
- "2019"
_4images_image_id: "35575"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35575 -->
Die Welle
