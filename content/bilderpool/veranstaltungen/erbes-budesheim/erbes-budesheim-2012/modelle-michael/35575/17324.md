---
layout: "comment"
hidden: true
title: "17324"
date: "2012-10-01T15:41:38"
uploadBy:
- "DenkMal"
license: "unknown"
imported:
- "2019"
---
HIer kann man die Geheimnisse der Nabe am besten sehen: Statt normalen Radnaben habe ich schwarze Freilaufnaben benutzt, dies lassen sich fester anziehen (löst sich nach mehreren Stunden Betrieb dennoch etwas) und sind auch stabiler, um die 15kg von den vier Drehscheiben auf die Achse (Lange Plotterachse, Nachbau, einziges Nicht-100%-Originalteil) zu übertragen.

Die normalen Radnaben lösten sich teils schon nach 1h, die Drehscheibe drückte dann die Verschraubung auseinander und hing fast direkt auf der Achse.