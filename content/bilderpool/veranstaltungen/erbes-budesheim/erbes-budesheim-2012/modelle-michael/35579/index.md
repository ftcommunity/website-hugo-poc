---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim17.jpg"
weight: "17"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "stephan"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/35579
- /detailsf259-2.html
imported:
- "2019"
_4images_image_id: "35579"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35579 -->
Die Verbindung der Stützen
