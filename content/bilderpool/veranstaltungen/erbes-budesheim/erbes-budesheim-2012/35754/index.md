---
layout: "image"
title: "Rennwagen von Thomas Brestrich"
date: "2012-10-03T10:59:00"
picture: "convention35.jpg"
weight: "7"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35754
- /details9fcb.html
imported:
- "2019"
_4images_image_id: "35754"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35754 -->
