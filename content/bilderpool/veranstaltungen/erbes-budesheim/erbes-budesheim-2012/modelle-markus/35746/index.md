---
layout: "image"
title: "Prater von Markus Wolf - Kabinen"
date: "2012-10-03T10:59:00"
picture: "convention27.jpg"
weight: "9"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35746
- /detailse6f2.html
imported:
- "2019"
_4images_image_id: "35746"
_4images_cat_id: "2654"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35746 -->
