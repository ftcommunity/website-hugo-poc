---
layout: "image"
title: "Münzsortierer von Michael Sengstschmid"
date: "2012-10-03T10:59:01"
picture: "convention51.jpg"
weight: "4"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35770
- /details209b.html
imported:
- "2019"
_4images_image_id: "35770"
_4images_cat_id: "2651"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:01"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35770 -->
