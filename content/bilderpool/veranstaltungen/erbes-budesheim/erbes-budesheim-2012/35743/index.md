---
layout: "image"
title: "Fischertechnik-Uhr (Clubmodell) von Volker-James Münchhof"
date: "2012-10-03T10:59:00"
picture: "convention24.jpg"
weight: "4"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/35743
- /details4d57.html
imported:
- "2019"
_4images_image_id: "35743"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35743 -->
