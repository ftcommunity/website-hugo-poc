---
layout: "image"
title: "Quadrokopter"
date: "2014-09-28T13:06:32"
picture: "ebbilderseverin52.jpg"
weight: "43"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39429
- /detailsf2bc.html
imported:
- "2019"
_4images_image_id: "39429"
_4images_cat_id: "2951"
_4images_user_id: "558"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39429 -->
