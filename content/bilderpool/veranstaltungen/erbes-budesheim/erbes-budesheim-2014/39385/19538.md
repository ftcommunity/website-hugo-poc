---
layout: "comment"
hidden: true
title: "19538"
date: "2014-09-28T15:03:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Laufbahn für die Kugeln besteht aus biegbarem 4-mm-Alu-Strang. Die Kugeln flutschten da nur so drüber, das sah total elegant aus. Das das so leichtgängig ist, genügt ganz wenig Höhenunterschied, und die Kugeln rasen trotzdem nur so daher.