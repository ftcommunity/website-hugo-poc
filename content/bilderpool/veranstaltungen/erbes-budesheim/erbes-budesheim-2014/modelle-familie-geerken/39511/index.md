---
layout: "image"
title: "Karlsruher Turmbergbahn und Laufroboter auf Laufband"
date: "2014-10-03T16:42:19"
picture: "modellevonfamiliegeerken2.jpg"
weight: "2"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39511
- /details13bd.html
imported:
- "2019"
_4images_image_id: "39511"
_4images_cat_id: "2960"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39511 -->
