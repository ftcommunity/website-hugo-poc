---
layout: "image"
title: "Weiche (Detailansicht)"
date: "2014-10-03T16:42:19"
picture: "modellevonfamiliegeerken6.jpg"
weight: "6"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39515
- /details70b4-2.html
imported:
- "2019"
_4images_image_id: "39515"
_4images_cat_id: "2960"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39515 -->
