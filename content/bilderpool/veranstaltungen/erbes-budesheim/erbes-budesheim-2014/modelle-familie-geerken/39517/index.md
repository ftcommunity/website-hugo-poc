---
layout: "image"
title: "Waggon vor Weiche (Seilführung)"
date: "2014-10-03T16:42:19"
picture: "modellevonfamiliegeerken8.jpg"
weight: "8"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39517
- /detailsa97d.html
imported:
- "2019"
_4images_image_id: "39517"
_4images_cat_id: "2960"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39517 -->
