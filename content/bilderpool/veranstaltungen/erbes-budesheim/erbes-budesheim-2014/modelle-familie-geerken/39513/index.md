---
layout: "image"
title: "Weiche mit Standseilbahnwagen"
date: "2014-10-03T16:42:19"
picture: "modellevonfamiliegeerken4.jpg"
weight: "4"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39513
- /details936a.html
imported:
- "2019"
_4images_image_id: "39513"
_4images_cat_id: "2960"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39513 -->
