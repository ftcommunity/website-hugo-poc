---
layout: "image"
title: "Portalhubwagen II"
date: "2014-10-03T16:42:19"
picture: "modellevonharaldsteinhaus3.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39520
- /details239c.html
imported:
- "2019"
_4images_image_id: "39520"
_4images_cat_id: "2961"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39520 -->
