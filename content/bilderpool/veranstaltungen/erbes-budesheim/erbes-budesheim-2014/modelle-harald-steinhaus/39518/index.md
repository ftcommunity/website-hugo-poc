---
layout: "image"
title: "Sumpfboot"
date: "2014-10-03T16:42:19"
picture: "modellevonharaldsteinhaus1.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39518
- /detailsce67.html
imported:
- "2019"
_4images_image_id: "39518"
_4images_cat_id: "2961"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39518 -->
