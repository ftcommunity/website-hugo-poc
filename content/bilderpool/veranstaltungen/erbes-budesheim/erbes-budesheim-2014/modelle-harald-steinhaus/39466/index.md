---
layout: "image"
title: "Sumpfboot2.jpg"
date: "2014-09-29T20:08:33"
picture: "Sumpfboot2.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39466
- /detailsc6cf.html
imported:
- "2019"
_4images_image_id: "39466"
_4images_cat_id: "2961"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T20:08:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39466 -->
Viel ist da wirklich nicht dran: der Propeller erzeugt einen Luftstrom, der das Boot vorantreibt. Im Strom stehen zwei Leitflächen, die durch den Servo geschwenkt werden. Wo das Speichenrad ist, ist die Führungsplatte 32455 nicht weit entfernt.

Den Kindern hat es sichtlich Spaß gemacht.
