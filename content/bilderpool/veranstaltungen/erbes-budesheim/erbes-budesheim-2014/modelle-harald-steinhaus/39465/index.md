---
layout: "image"
title: "Sumpfboot1.jpg"
date: "2014-09-29T20:06:02"
picture: "Sumpfboot1.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/39465
- /details7771.html
imported:
- "2019"
_4images_image_id: "39465"
_4images_cat_id: "2961"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T20:06:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39465 -->
Der Rumpf ist derjenige vom Rennboot: http://ftcommunity.de/categories.php?cat_id=2587
Gegen stauende Nässe ist der Empfänger in einen Plastikbeutel verpackt worden. Das Vollbad im letzten Jahr hat ihm nicht geschadet, aber man muss das Schicksal ja nicht heraus fordern.
