---
layout: "image"
title: "Multiplizierer + Addierer + Sinus  ( Micheal W )"
date: "2014-10-05T11:12:27"
picture: "multipliziereraddierersinusmichealw1.jpg"
weight: "1"
konstrukteure: 
- "Michael W"
fotografen:
- "Peter Poedreoyen NL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39660
- /detailsd6d6.html
imported:
- "2019"
_4images_image_id: "39660"
_4images_cat_id: "2971"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T11:12:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39660 -->
Hier mal ein paar Detailfotos der gezeigten Modelle aus dem Pool: 

Multiplizierer: http://www.ftcommunity.de/details.php?image_id=39361 

Addierer mit Raederkurbelgetriebe: http://www.ftcommunity.de/details.php?image_id=38402 

Sinus: http://www.ftcommunity.de/details.php?image_id=39357 

Addierer mit Differential: http://www.ftcommunity.de/details.php?image_id=38424 
