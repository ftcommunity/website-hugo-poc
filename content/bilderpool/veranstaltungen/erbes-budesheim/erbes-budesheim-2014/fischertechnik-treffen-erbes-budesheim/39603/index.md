---
layout: "image"
title: "immer interessante + schöne Modelle   (Michael Sengstschmied)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim35.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39603
- /detailsa635.html
imported:
- "2019"
_4images_image_id: "39603"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39603 -->
