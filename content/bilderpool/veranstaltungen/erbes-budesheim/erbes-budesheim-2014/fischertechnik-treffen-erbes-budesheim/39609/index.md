---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim41.jpg"
weight: "41"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39609
- /details1456.html
imported:
- "2019"
_4images_image_id: "39609"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39609 -->
