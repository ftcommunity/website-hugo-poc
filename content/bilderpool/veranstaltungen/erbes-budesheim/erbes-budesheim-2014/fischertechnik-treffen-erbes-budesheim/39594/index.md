---
layout: "image"
title: "immer interessante + schöne Modellen (Thomas Püttmann)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim26.jpg"
weight: "26"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39594
- /detailsdc97.html
imported:
- "2019"
_4images_image_id: "39594"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39594 -->
