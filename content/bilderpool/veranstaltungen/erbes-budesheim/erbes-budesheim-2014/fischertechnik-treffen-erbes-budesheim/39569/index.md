---
layout: "image"
title: "Paul Niekerk"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39569
- /details6f4d.html
imported:
- "2019"
_4images_image_id: "39569"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39569 -->
