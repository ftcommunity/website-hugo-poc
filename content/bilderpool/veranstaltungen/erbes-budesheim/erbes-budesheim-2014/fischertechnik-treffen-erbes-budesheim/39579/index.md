---
layout: "image"
title: "After-Fischertechnik-Treffen"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim11.jpg"
weight: "11"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39579
- /details1b37.html
imported:
- "2019"
_4images_image_id: "39579"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39579 -->
