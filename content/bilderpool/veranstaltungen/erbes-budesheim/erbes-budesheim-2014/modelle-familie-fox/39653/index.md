---
layout: "image"
title: "fischertechnik-Bahn auf G-Schienen"
date: "2014-10-04T23:18:43"
picture: "modellevonfamiliefox3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39653
- /details58af.html
imported:
- "2019"
_4images_image_id: "39653"
_4images_cat_id: "2969"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39653 -->
