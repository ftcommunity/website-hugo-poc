---
layout: "image"
title: "Knickarm-Roboter mit Nunchuk-Steuerung"
date: "2014-10-04T23:18:43"
picture: "modellevonfamiliefox1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39651
- /details3e71.html
imported:
- "2019"
_4images_image_id: "39651"
_4images_cat_id: "2969"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39651 -->
Detailfotos siehe [http://www.ftcommunity.de/categories.php?cat_id=2934]("http://www.ftcommunity.de/categories.php?cat_id=2934")
