---
layout: "image"
title: "Funktionsmodelle"
date: "2014-10-04T23:18:43"
picture: "modellevonfamiliefox2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
- "Magnus Fox"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39652
- /detailsa5d6-3.html
imported:
- "2019"
_4images_image_id: "39652"
_4images_cat_id: "2969"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39652 -->
* [Dampfmaschine](http://www.ftcommunity.de/categories.php?cat_id=2676) [ft:pedia 4/2012](https://ftcommunity.de/ftpedia/2012/2012-4)

* [Selbstbau-Differential](http://www.ftcommunity.de/categories.php?cat_id=2873) mit Dreigangschaltung,

* Ewigkeitsmaschine 

* [DCF77-Funkuhr](http://www.ftcommunity.de/categories.php?cat_id=2613) [ftpedia 3/2012](https://ftcommunity.de/ftpedia/2012/2012-3/)
