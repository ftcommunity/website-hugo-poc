---
layout: "image"
title: "Aufbau"
date: "2014-10-04T15:59:57"
picture: "turmaufbau1.jpg"
weight: "18"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/39630
- /details9069.html
imported:
- "2019"
_4images_image_id: "39630"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39630 -->
Hier wird gerade darüber diskutiert, wie der Turm am Besten an den Zinken des Teleskopladers festgemacht werden kann. Ziel war es, dass er in der Horizontalen zwar fest ist, sich aber in der Vertikalen noch bewegen kann.
