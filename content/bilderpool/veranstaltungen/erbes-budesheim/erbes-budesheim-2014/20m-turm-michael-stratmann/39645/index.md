---
layout: "image"
title: "Turm mit Teleskopkran"
date: "2014-10-04T23:18:43"
picture: "turmvonmichaelstratmann1.jpg"
weight: "25"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39645
- /detailsca23-2.html
imported:
- "2019"
_4images_image_id: "39645"
_4images_cat_id: "2966"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39645 -->
