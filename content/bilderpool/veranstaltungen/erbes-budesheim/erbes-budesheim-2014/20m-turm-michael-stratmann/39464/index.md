---
layout: "image"
title: "20m Turm im Auto"
date: "2014-09-29T17:27:54"
picture: "20140927_233033.jpg"
weight: "17"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Michael Stratmann (Denkmal)"
uploadBy: "DenkMal"
license: "unknown"
legacy_id:
- /php/details/39464
- /details69dc.html
imported:
- "2019"
_4images_image_id: "39464"
_4images_cat_id: "2966"
_4images_user_id: "1470"
_4images_image_date: "2014-09-29T17:27:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39464 -->
Dank der vielen helfenden Hände wurde der Turm für den Heimweg zu meinem bisher kleinsten Modell!
