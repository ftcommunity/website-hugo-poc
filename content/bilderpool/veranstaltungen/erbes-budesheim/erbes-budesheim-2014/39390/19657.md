---
layout: "comment"
hidden: true
title: "19657"
date: "2014-10-13T00:27:06"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Ohhh ja Danke. Inzwischen versuche ich die Steuerung von dem "angeblich überdimensionierten" TX-Controller auf eine klassische Art mit "herkömmlichen" Mitteln mal zu lösen. Irgendjemand sagte zu mir auf der Convention "Sag mal ist der TX nicht ein wenig überdimensioniert für die Steuerung?". Ich antwortete "Klar, aber das ging immer noch am schnellsten!". Und ich behielt bis jetzt recht. Zwei Versuche und viele Stunden Tüftelei habe ich schon hinter mir. Aber erstmal das Problem: Die Bahn wird über einen einzigen Taster gesteuert. Wenn er nicht gedrückt ist fährt die Bahn. Ist er gedrückt, fährt die Bahn nach ca. 10s in entgegengesetzter Richtung. Sieht erstmal nicht so schwer aus - aaaber! Wenn die Steuerung eingeschaltet wird und irgendein Wagen oben steht fängt das Problem an - die Steuerung weiß ja nicht welcher Wagen oben steht. Mit dem TX war das ganz einfach zu lösen, wenn nach 0,3s der Taster nicht öffnet, dann Motorumkehr. Beim nächsten tastendruck 10s Warten. usw. usw. - Fertig. Herkömmlich siht das ganz anders aus. 1. Lösung rein mechanisch: Wenn Taster gedrückt, läuft ein MiniMot mit mehreren Getrieben los und schaltet die Drehrichtung des Turmbergbahn-Antriebs nach max. 10s um. Kurz vor dem Umschalten wird der Turmbergbahn-Motor schon über Segemntscheiben eingeschaltet (d.h. der geöffnete Taster wir überbrückt), läuft dann in den meisten Fällen kurz verkehrt herum (was aber nicht so schlimm ist), wird kurz darauf umgeschaltet, öffnet dann den Turmberbahntaster und kurz darauf den Segmentscheibentaster. Die Bahn fäht und der ganze Rhythmus geht von vorne los. Mit den Silberlingen gestaltet sich das aber schon schwieriger - vor allen Dingen weil mein Monoflop seit ca 35J den Geist aufgegeben hat. Deswegen bin ich auf ein ETec-Modul zurück- (das muss man sich mal vorstellen) zurückgegangen. Die Verbindung klappt zwar irgendwie - ist aber auch nicht trivial. Vielleicht hat ja irgendwer von euch eine Idee, das Problem "herkömmlich" zu lösen. Bin gespannt auf eure Ideen. Gruß Ralf Geerken