---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/39483
- /details8ae6-2.html
imported:
- "2019"
_4images_image_id: "39483"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39483 -->
Hier eine Gesamtansicht. Das Rad ohne Kabinen besteht aus fast 8000 Teilen. Wie im Original hat es 15 Kabinen. Die leeren Kabinenhalter gibt es erst seit der Nachkriegszeit.
Ursprünglich hatte das Rad 30 davon. Die brannten nach einem Bombenangriff ab. Danach hat man nur noch 15 Kabinen installiert.
