---
layout: "image"
title: "Detailansicht"
date: "2014-10-03T22:04:09"
picture: "modellevonmarkuswolf09.jpg"
weight: "18"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39543
- /details27c7.html
imported:
- "2019"
_4images_image_id: "39543"
_4images_cat_id: "2956"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39543 -->
