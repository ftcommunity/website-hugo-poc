---
layout: "image"
title: "Antrieb I"
date: "2014-10-03T22:04:09"
picture: "modellevonmarkuswolf04.jpg"
weight: "13"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Fox, Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39538
- /detailsfa18.html
imported:
- "2019"
_4images_image_id: "39538"
_4images_cat_id: "2956"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39538 -->
