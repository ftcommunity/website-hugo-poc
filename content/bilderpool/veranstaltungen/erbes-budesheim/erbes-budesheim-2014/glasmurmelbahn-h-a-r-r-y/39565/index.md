---
layout: "image"
title: "Der Schlepplift - Unterwegs"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn14.jpg"
weight: "25"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39565
- /details7975-2.html
imported:
- "2019"
_4images_image_id: "39565"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39565 -->
Die Schleppschlitten laufen, gezogen von der Kette, auf den Aluminiumschienen nach oben und nehmen die Glasmurmeln mit.

---

The buckets of the drag lift are attached to a chain, get dragged upward sliding on the aluminium rods and so give the marbles a ride to the top.