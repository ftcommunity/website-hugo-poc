---
layout: "image"
title: "Oktogon"
date: "2014-10-03T16:42:19"
picture: "kubelbahnvonrenetrapp11.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Dirk Fox, Gudula Kiehle"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39532
- /details2d32.html
imported:
- "2019"
_4images_image_id: "39532"
_4images_cat_id: "2962"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39532 -->
