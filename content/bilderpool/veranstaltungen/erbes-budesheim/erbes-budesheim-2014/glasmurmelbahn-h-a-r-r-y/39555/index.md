---
layout: "image"
title: "Die Westseite"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn04.jpg"
weight: "15"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39555
- /details453d.html
imported:
- "2019"
_4images_image_id: "39555"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39555 -->
Im Hebewerk sind die Maschinen untergebracht: Rechts im Bild das Heberad, links auf dem Turm die Bergstation des Schlepplifts. Unter dem Turm der Bergstation ist die Schaltwarte untergebracht.

---

At this 'building' all the machines are located: on the right hand side the lifting wheel (Heberad), the drag lift top station resides inside the tower on the left. Below the tower the power station is located.