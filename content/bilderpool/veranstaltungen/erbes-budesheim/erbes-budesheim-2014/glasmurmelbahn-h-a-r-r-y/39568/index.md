---
layout: "image"
title: "Der Schlepplift - Bergstation"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn17.jpg"
weight: "28"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39568
- /details05f6.html
imported:
- "2019"
_4images_image_id: "39568"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39568 -->
Gerade steigt ein "Fahrgast" aus.

Und der kleine 'Rundgang' endet hier.

---

A marble is just about to leave the drag lift.

Our small 'walkabout' just ends here.