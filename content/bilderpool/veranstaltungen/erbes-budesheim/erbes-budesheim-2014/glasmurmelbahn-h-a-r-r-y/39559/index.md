---
layout: "image"
title: "Heberad - Detail"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn08.jpg"
weight: "19"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "Hohlrad", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39559
- /detailse607.html
imported:
- "2019"
_4images_image_id: "39559"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39559 -->
So schmal ist das Heberad: Gerade mal 15mm breit und die Murmeln stehen leicht heraus.

---

The lifting wheel is a slender construction. It thickness is 15mm (roughly 0.6 inches). The 5/16 marbles stick out slightly.