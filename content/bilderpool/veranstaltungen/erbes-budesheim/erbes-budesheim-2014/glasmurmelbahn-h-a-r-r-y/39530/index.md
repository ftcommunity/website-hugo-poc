---
layout: "image"
title: "Mühlenhäuschen und Schlepplift"
date: "2014-10-03T16:42:19"
picture: "kubelbahnvonrenetrapp09.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Dirk Fox, Gudula Kiehle"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39530
- /details94cd.html
imported:
- "2019"
_4images_image_id: "39530"
_4images_cat_id: "2962"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39530 -->
