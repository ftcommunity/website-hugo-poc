---
layout: "image"
title: "Kein Wasser, pardon, keine Murmeln auf der Mühle"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn11.jpg"
weight: "22"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39562
- /details154e.html
imported:
- "2019"
_4images_image_id: "39562"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39562 -->
Dafür sieht man jetzt mal die Schaufeln des Mühlrades.

- Um das Mühlrad so zeigen zu können, ist natürlich das Dach des Mühlenhäuschens abgenommen -

Im Hintergrund ist dann noch die Einlaßschleuse des Schleppliftes zu sehen.

---

Without marbles arriving the wheel stops. Now the buckets get visible.

- The roof has been removed to take the pictures of the internals. -

In the background we have the inlet lock of the drag lift (the second marble lifter).