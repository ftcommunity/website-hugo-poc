---
layout: "image"
title: "... klipp, klapp, klickediklack"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn10.jpg"
weight: "21"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "Mühlrad", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39561
- /details9197.html
imported:
- "2019"
_4images_image_id: "39561"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39561 -->
Ui, da ist ja richtig Tempo drauf.

---

The wheel really gets speed.