---
layout: "image"
title: "Die Nordseite"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn03.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39554
- /details04b0.html
imported:
- "2019"
_4images_image_id: "39554"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39554 -->
Diese Seite war auf der Convention (leider) nicht zugänglich.

---

Visitors could not see this side, unfortunately.