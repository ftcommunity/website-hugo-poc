---
layout: "image"
title: "Tresor"
date: "2014-09-28T13:06:32"
picture: "ebbilderseverin57.jpg"
weight: "47"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/39434
- /details9088.html
imported:
- "2019"
_4images_image_id: "39434"
_4images_cat_id: "2951"
_4images_user_id: "558"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39434 -->
