---
layout: "image"
title: "FT-Convention 2014: Kirmesmodell"
date: "2014-09-28T13:06:32"
picture: "margau2.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "margau"
uploadBy: "margau"
license: "unknown"
legacy_id:
- /php/details/39374
- /detailsd245.html
imported:
- "2019"
_4images_image_id: "39374"
_4images_cat_id: "2951"
_4images_user_id: "1805"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39374 -->
Ein Karusell
