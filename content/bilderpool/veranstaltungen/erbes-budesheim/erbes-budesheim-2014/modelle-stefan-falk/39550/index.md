---
layout: "image"
title: "Rein pneumatische Abfüllanlage"
date: "2014-10-03T22:04:09"
picture: "modellestefanfalk5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Gudula Kiehle"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39550
- /details544c.html
imported:
- "2019"
_4images_image_id: "39550"
_4images_cat_id: "2964"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39550 -->
