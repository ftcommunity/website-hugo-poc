---
layout: "image"
title: "Münzsortierer"
date: "2015-10-01T13:37:10"
picture: "Mnzsortierer.jpg"
weight: "18"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41953
- /details5931.html
imported:
- "2019"
_4images_image_id: "41953"
_4images_cat_id: "3119"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41953 -->
