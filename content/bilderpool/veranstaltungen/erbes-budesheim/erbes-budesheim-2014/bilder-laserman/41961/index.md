---
layout: "image"
title: "Stahlwalzstraße Antrieb"
date: "2015-10-01T13:37:10"
picture: "Stahlwalzstrae_Antrieb.jpg"
weight: "26"
konstrukteure: 
- "Die Mädels"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41961
- /details6ec3.html
imported:
- "2019"
_4images_image_id: "41961"
_4images_cat_id: "3119"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41961 -->
