---
layout: "image"
title: "Noch ein Pistenbully"
date: "2015-10-01T13:37:10"
picture: "Noch_ein_Pistenbully.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41955
- /details8cee-2.html
imported:
- "2019"
_4images_image_id: "41955"
_4images_cat_id: "3119"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41955 -->
