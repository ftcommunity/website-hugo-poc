---
layout: "image"
title: "Details"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog5.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Bert Rook + Rob van Baal"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39668
- /detailsa7b9.html
imported:
- "2019"
_4images_image_id: "39668"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39668 -->
De vleugelhoek-verstelling per vleugel vindt plaats door XS-motoren via een RoboPro-programma afhankelijk of de Libelle naar boven of beneden vliegt. 
De Pot-meter rechts dient hiervoor als ingang.
