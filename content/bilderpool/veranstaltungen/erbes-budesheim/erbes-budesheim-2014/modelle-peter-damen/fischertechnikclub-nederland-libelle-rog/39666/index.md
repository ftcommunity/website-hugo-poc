---
layout: "image"
title: "Libelle"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog3.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Bert Rook + Rob van Baal"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/39666
- /detailsdade.html
imported:
- "2019"
_4images_image_id: "39666"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39666 -->
Details gibt es unter : 
http://www.ftcommunity.de/details.php?image_id=39087 und 

http://www.ftcommunity.de/categories.php?cat_id=2921 

