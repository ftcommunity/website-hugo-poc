---
layout: "image"
title: "Pistenbully 2"
date: "2014-10-05T13:51:13"
picture: "modellevonfamiliebusch3.jpg"
weight: "3"
konstrukteure: 
- "Jörg Busch"
- "Erik Busch"
fotografen:
- "Johann Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/39663
- /details0532.html
imported:
- "2019"
_4images_image_id: "39663"
_4images_cat_id: "2972"
_4images_user_id: "1126"
_4images_image_date: "2014-10-05T13:51:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39663 -->
