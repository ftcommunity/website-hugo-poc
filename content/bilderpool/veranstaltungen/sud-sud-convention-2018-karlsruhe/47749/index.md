---
layout: "image"
title: "Süd - Süd - Convention 2018 Karlsruhe"
date: "2018-07-21T19:32:07"
picture: "ausstellung1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chrischan1974"
license: "unknown"
legacy_id:
- /php/details/47749
- /detailsf505.html
imported:
- "2019"
_4images_image_id: "47749"
_4images_cat_id: "3525"
_4images_user_id: "2874"
_4images_image_date: "2018-07-21T19:32:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47749 -->
Ein Blick in die Ausstellungshalle