---
layout: "image"
title: "EDE 08"
date: "2011-04-02T23:50:37"
picture: "ede08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30397
- /details4247.html
imported:
- "2019"
_4images_image_id: "30397"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30397 -->
Demag CC4800: 34 Kilo schwer.
Unterwagen: 13,7kg
Oberwagen: 10kg
Gegengewicht: 3,5kg
Derrickausleger inclusive Balastplatte: 3,1kg
Ausleger:3,7kg
Exclusive RC anlage und Accu
Deswegen der stutze unter der Oberwagen