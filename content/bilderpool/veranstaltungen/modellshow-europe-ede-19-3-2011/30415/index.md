---
layout: "image"
title: "EDE 26"
date: "2011-04-02T23:50:38"
picture: "ede26.jpg"
weight: "26"
konstrukteure: 
- "Dennis Bosman"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30415
- /details8b5c.html
imported:
- "2019"
_4images_image_id: "30415"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30415 -->
Lego: Mercedes Actros mit Nooteboom Auflieger