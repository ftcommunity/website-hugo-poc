---
layout: "image"
title: "EDE 34"
date: "2011-04-02T23:50:38"
picture: "ede34.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30423
- /details5d57.html
imported:
- "2019"
_4images_image_id: "30423"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30423 -->
Meccano: Liebherr LR 1750