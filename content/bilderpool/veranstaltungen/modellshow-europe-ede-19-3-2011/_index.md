---
layout: "overview"
title: "Modellshow Europe  EDE 19-3-2011"
date: 2020-02-22T09:08:43+01:00
legacy_id:
- /php/categories/2260
- /categories6ba8.html
- /categories62c4.html
- /categories4535.html
- /categoriesbefa.html
- /categoriesb2df.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2260 --> 
Zum 19ten Mal wurde diesen Show organisiert.
Zum Xten Mal waren dieses Jahr sogar 6 Mitglieder des fischertechnikclubs-NL dabei. Auf gut 26m Tischlänge (80cm Breite) wurden 3 Große Kräne, 2 Brückelegepanzer und eine Kirmes aufgebaut.