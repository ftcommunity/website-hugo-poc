---
layout: "image"
title: "EDE 20"
date: "2011-04-02T23:50:38"
picture: "ede20.jpg"
weight: "20"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30409
- /detailsaef3-2.html
imported:
- "2019"
_4images_image_id: "30409"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30409 -->
Details der Manitowoc: der Kabine finde ich sehr gut gelungen
