---
layout: "image"
title: "EDE 07"
date: "2011-04-02T23:50:37"
picture: "ede07.jpg"
weight: "7"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30396
- /details0d00.html
imported:
- "2019"
_4images_image_id: "30396"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30396 -->
Jan-Willen Dekker beim aufbau der Kirmis
