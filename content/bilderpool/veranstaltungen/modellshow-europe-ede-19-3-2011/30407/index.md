---
layout: "image"
title: "EDE 18"
date: "2011-04-02T23:50:38"
picture: "ede18.jpg"
weight: "18"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30407
- /details7073.html
imported:
- "2019"
_4images_image_id: "30407"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30407 -->
Details der Manitowoc: einfacher gehts nicht. Verstrebung bein dreieckigen Derrickausleger
