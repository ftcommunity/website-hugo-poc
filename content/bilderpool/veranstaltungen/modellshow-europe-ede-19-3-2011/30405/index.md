---
layout: "image"
title: "EDE 16"
date: "2011-04-02T23:50:38"
picture: "ede16.jpg"
weight: "16"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30405
- /detailsd63c.html
imported:
- "2019"
_4images_image_id: "30405"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30405 -->
Details der Manitowoc: Stutsen und Gelender
