---
layout: "comment"
hidden: true
title: "16642"
date: "2012-03-19T15:17:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Außer dem Tank ist aber an dem Auto doch kaum was schweres dran, un den Tank mussten doch alle Autos tragen, oder? Bist Du sicher, dass es am Gewicht lag? Vielleicht haben diese Reifen ja auch eine etwas hohe Reibung, oder es sind Kunststoffachsen, die sich, wenn auch nur ganz wenig, verbiegen und so die Reifen zum Reiben zusätzlich zum Rollen bringen. Das kostet ganz erheblich Energie und könnte wohl was ausmachen.

Gruß,
Stefan