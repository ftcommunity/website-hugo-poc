---
layout: "image"
title: "EDE 04"
date: "2011-04-02T23:50:37"
picture: "ede04.jpg"
weight: "4"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30393
- /details044e.html
imported:
- "2019"
_4images_image_id: "30393"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30393 -->
Hier überdenkt Wim Starreveld (mit der rucken zum Kamera) wie er sein Kran aufbauen wil
