---
layout: "image"
title: "EDE 11"
date: "2011-04-02T23:50:38"
picture: "ede11.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/30400
- /details1caa.html
imported:
- "2019"
_4images_image_id: "30400"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30400 -->
Brückenlegerpantser Bieber. Der version von Peter Damen
Rechts ein aus Alu platte aufgebauten teil der brücke.
