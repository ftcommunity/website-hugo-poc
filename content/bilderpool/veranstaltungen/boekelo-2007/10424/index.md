---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:01"
picture: "boekelo15.jpg"
weight: "18"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10424
- /detailsae9a.html
imported:
- "2019"
_4images_image_id: "10424"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10424 -->
fichertechnikclub event 2007 Boekelo
