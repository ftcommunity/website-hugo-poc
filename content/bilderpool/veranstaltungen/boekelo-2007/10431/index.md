---
layout: "image"
title: "Industy robot"
date: "2007-05-15T14:50:12"
picture: "boekelo22.jpg"
weight: "25"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10431
- /detailsac4b.html
imported:
- "2019"
_4images_image_id: "10431"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10431 -->
fischertechnik Nederland (Freetime)
