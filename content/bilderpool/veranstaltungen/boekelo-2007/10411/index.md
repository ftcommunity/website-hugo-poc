---
layout: "image"
title: "Inside the front hall at 8 o'clock AM"
date: "2007-05-15T14:49:48"
picture: "boekelo02.jpg"
weight: "5"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10411
- /detailsd29d.html
imported:
- "2019"
_4images_image_id: "10411"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10411 -->
fichertechnikclub event 2007 Boekelo
