---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:12"
picture: "boekelo27.jpg"
weight: "30"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10436
- /details3207.html
imported:
- "2019"
_4images_image_id: "10436"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10436 -->
fichertechnikclub event 2007 Boekelo
