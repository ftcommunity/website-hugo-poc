---
layout: "image"
title: "fam Tielemans"
date: "2007-05-15T14:49:48"
picture: "boekelo05.jpg"
weight: "8"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10414
- /details4345.html
imported:
- "2019"
_4images_image_id: "10414"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10414 -->
The interior of the main hall
