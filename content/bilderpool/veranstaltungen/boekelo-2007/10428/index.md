---
layout: "image"
title: "Presentation of Microsoft-Visual Robotics Studio"
date: "2007-05-15T14:50:01"
picture: "boekelo19.jpg"
weight: "22"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10428
- /details46d0.html
imported:
- "2019"
_4images_image_id: "10428"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10428 -->
See also
http://msdn2.microsoft.com/en-us/robotics/aa731523.aspx
On-demand videos
http://msdn2.microsoft.com/en-us/robotics/bb383569.aspx

English: http://www.fischertechnik.de/en/msrspp/index.html
German: http://www.fischertechnik.de/de/msrspp/index.html
