---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:49:48"
picture: "boekelo08.jpg"
weight: "11"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10417
- /details7964.html
imported:
- "2019"
_4images_image_id: "10417"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10417 -->
fichertechnikclub event 2007 Boekelo
