---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:12"
picture: "boekelo30.jpg"
weight: "33"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10439
- /details4ae8.html
imported:
- "2019"
_4images_image_id: "10439"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10439 -->
fichertechnikclub event 2007 Boekelo
