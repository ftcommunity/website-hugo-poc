---
layout: "image"
title: "The end of this event"
date: "2007-05-15T14:50:18"
picture: "boekelo33.jpg"
weight: "36"
konstrukteure: 
- "fischertechnikclub NL"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/10442
- /detailsb34d.html
imported:
- "2019"
_4images_image_id: "10442"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10442 -->
After some houres the fichertechnikclub event 2007 Boekelo ended with an empty hall.
See you next year in Twente.
