---
layout: "image"
title: "Die 'Ladestation' der Schwebebahn"
date: "2015-10-06T18:38:55"
picture: "olagino21.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42082
- /details77a3.html
imported:
- "2019"
_4images_image_id: "42082"
_4images_cat_id: "3129"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42082 -->
Für den Dauerbetrieb der Schwebebahnzüge waren genügend Akkus vorhanden, da durch die Zeitverzögerung beim Depotbau nicht alle Züge in Betrieb gehen konnten.