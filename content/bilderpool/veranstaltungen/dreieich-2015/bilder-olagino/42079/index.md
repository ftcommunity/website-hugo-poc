---
layout: "image"
title: "Delta-Plotter (Detailansicht)"
date: "2015-10-06T18:38:55"
picture: "olagino18.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42079
- /detailsde67.html
imported:
- "2019"
_4images_image_id: "42079"
_4images_cat_id: "3129"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42079 -->
