---
layout: "image"
title: "Film-Abspielgerät Praxinoskop"
date: "2015-10-01T13:37:10"
picture: "Film-Abspielgert.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41920
- /detailsa5ca.html
imported:
- "2019"
_4images_image_id: "41920"
_4images_cat_id: "3118"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41920 -->
