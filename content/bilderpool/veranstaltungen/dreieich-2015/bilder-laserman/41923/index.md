---
layout: "image"
title: "Kabelabdeckung"
date: "2015-10-01T13:37:10"
picture: "Kabelabdeckung.jpg"
weight: "9"
konstrukteure: 
- "Tobias Horst (tobs9578)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/41923
- /detailsb3f1.html
imported:
- "2019"
_4images_image_id: "41923"
_4images_cat_id: "3118"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41923 -->
