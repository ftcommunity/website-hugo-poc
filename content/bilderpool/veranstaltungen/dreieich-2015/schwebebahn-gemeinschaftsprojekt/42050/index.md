---
layout: "image"
title: "IMG_2152.JPG"
date: "2015-10-05T21:21:06"
picture: "IMG_2152mit.JPG"
weight: "7"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42050
- /details74ce.html
imported:
- "2019"
_4images_image_id: "42050"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:21:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42050 -->
Die Züge sind alle gleich konstruiert. Ähm... naja, im Prinzip. So ein bisschen kommt bei jedem Zug auch die Handschrift des Erbauers durch. Dieser hier ist meiner, in den Farben Gold, Schwarz/Rot und Rot gehalten, mit Schiebetüren im etwas verlängerten Mittelwagen..
