---
layout: "image"
title: "Wendeanlage von sjost (Drehscheibe)"
date: "2015-10-06T18:38:55"
picture: "olagino05.jpg"
weight: "15"
konstrukteure: 
- "sjost"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42066
- /details07dd-2.html
imported:
- "2019"
_4images_image_id: "42066"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42066 -->
Die Komplexe Drehscheibe im Detail