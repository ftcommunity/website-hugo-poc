---
layout: "image"
title: "IMG_1982.JPG"
date: "2015-10-05T21:34:37"
picture: "IMG_1982.JPG"
weight: "10"
konstrukteure: 
- "Getriebesand"
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/42053
- /detailsa147-2.html
imported:
- "2019"
_4images_image_id: "42053"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:34:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42053 -->
Die Schiebetür im Haraldschen Zug. Die Kette hat zwei Mitnehmer, einen oben und einen unten (unter dem schwarzen Kettenglied - das ist ein ganz altes, mit durchgehend freier Nut; ein Flachstein 15*30 täte es auch). Die BSB-Grundplatte auf dem Motor sorgt für einen gewissen Andruck zwischen Kette und Z10. Dadurch rattert der Antrieb nach Erreichen der Endstellung (Tür auf oder zu) einfach solange weiter, bis der Strom abgeschaltet wird. Im Gegensatz zu Endschaltern setzt diese Art Rutschkupplung auch dann den Antrieb außer Gefecht, wenn sich noch Körperteile oder Gepäckstücke im Türspalt befinden.
