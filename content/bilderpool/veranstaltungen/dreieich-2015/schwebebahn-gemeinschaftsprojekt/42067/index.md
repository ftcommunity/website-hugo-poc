---
layout: "image"
title: "Wendeanlage von sjost (Drehscheibe mit Zug)"
date: "2015-10-06T18:38:55"
picture: "olagino06.jpg"
weight: "16"
konstrukteure: 
- "sjost"
fotografen:
- "olagino"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- /php/details/42067
- /details9855.html
imported:
- "2019"
_4images_image_id: "42067"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42067 -->
Auf diesem Bild ist die Drehscheibe mit dem Ferngesteuerten Zug zu sehen