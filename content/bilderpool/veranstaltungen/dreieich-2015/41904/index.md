---
layout: "image"
title: "DCF-77 TX Funkuhr"
date: "2015-09-30T10:44:27"
picture: "coneich1.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Marius Seider (Limit)"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- /php/details/41904
- /details8c71.html
imported:
- "2019"
_4images_image_id: "41904"
_4images_cat_id: "3097"
_4images_user_id: "430"
_4images_image_date: "2015-09-30T10:44:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41904 -->
Die unglaublich schicke TX-Funkuhr, die via I2C arbeitet (steht in der ft:pedia wie genau das funktioniert)
