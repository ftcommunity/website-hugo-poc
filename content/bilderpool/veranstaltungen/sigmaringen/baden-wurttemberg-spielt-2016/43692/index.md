---
layout: "image"
title: "Dekoration 'fischertechnik'"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt14.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43692
- /detailsadf2.html
imported:
- "2019"
_4images_image_id: "43692"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43692 -->
Ach ja, da war ja noch die eigens angereiste Dekoration "Zwanzig Sechzehn" ...
https://www.ftcommunity.de/categories.php?cat_id=3146

Nach der Maker Faire in Hannover wieder gangbar gemacht so gut es ging, hat die Hebekunst den ganzen Tag irgendwie durchgehalten, nicht zuletzt auch durch den unermüdlichen Einsatz des mitgereisten Mechanikers. Der Schlepplift hat um kurz nach 18:00 dann den Feierabend eingeläutet -  eine Murmel klemmte ...

Bilanz:
Viele begeisterte Besucher, viele ft-Flyer (aktuelles Lieferprogramm) unter das Volk gebracht und etliche Kommentare im Sinne "ach wie praktisch, das probiere ich auch gleich mal selbst" - die ft-Tische waren ja direkt nebenan.