---
layout: "image"
title: "Spielstation 'fischertechnik Dynamic'"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt12.jpg"
weight: "12"
konstrukteure: 
- "101Entertainment"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43690
- /details7f75.html
imported:
- "2019"
_4images_image_id: "43690"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43690 -->
Dynamic L (hier noch die ältere Kartonversion), und davon reichlich, laden zum Selberbauen ein. Gerade für die jüngeren Kids und noch unerfahreneren fischertechniker ein schnelles Erfolgserlebnis: Grundplatte, Flexschiene und ein Baustein für die Höhendifferenz - fertig ist die "Einstiegsdroge". Der Weiterbau ging dann plötzlich von ganz alleine.

(Entschuldigt die Bildqualität, die Lichtverhältnisse waren etwas schwierig am Sonntag früh)