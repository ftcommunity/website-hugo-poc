---
layout: "image"
title: "Spielstation 'Bruder' (eine Hälfte)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt09.jpg"
weight: "9"
konstrukteure: 
- "101Entertainment"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43687
- /detailsea34.html
imported:
- "2019"
_4images_image_id: "43687"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43687 -->
Liebevoll arrangiert für die jüngeren Besucher. Landwirtschaftsgeräte und Baumaschinen kommen bei dem Zielpublikum immer gut an.