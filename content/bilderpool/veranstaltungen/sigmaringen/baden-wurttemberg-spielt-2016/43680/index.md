---
layout: "image"
title: "Schloß Sigmaringen (II)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt02.jpg"
weight: "2"
konstrukteure: 
- "Verschiedene Architekten"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/43680
- /details11d0.html
imported:
- "2019"
_4images_image_id: "43680"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43680 -->
Anblick vom Donauufer aus.