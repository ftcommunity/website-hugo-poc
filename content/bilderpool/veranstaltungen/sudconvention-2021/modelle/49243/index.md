---
layout: "image"
title: "Trebuchet"
date: 2021-10-31T14:18:09+01:00
picture: "P1012033.JPG"
weight: "12"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Ein Trebuchet. Auf Wurfvorführungen während der Veranstaltung wurde verzichtet. 