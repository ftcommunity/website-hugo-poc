---
layout: "image"
title: "LED mit Wechselstrom"
date: 2021-10-31T14:17:38+01:00
picture: "P1012015.JPG"
weight: "20"
konstrukteure: 
- "Felix Geerken"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Die LEDs noch mal etwas langsamer zum Mitdenken