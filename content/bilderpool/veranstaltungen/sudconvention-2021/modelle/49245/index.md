---
layout: "image"
title: "Riesenräder"
date: 2021-10-31T14:18:15+01:00
picture: "P1012031.JPG"
weight: "10"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Alle Riesenräder, die jemals von fischertechnik auf den Markt kamen, live und in Farbe! Die etwas neueren Modelle.