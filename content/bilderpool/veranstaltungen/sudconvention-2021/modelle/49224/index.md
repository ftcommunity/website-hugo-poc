---
layout: "image"
title: "Industriemodell – Detail"
date: 2021-10-31T14:17:06+01:00
picture: "P1012026.JPG"
weight: "6"
konstrukteure: 
- "PSG-Robotik-AG"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Hier werden die Deckel fest auf die Dose gedrückt.
