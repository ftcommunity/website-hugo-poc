---
layout: "image"
title: "Hermann"
date: 2021-10-31T14:17:31+01:00
picture: "P1012020.JPG"
weight: "22"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Hier noch das komplette Modell mit Laufband und dem Kollegen Hugo. Außerdem: Ralf (mit Maske).