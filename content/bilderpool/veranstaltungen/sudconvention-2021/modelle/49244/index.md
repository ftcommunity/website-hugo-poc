---
layout: "image"
title: "Riesenräder"
date: 2021-10-31T14:18:12+01:00
picture: "P1012032.JPG"
weight: "11"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Die etwas älteren Riesenräder. Alle Informationen zu dem Dokument: https://ftcommuniinty.de/knowhow/bauanleitungen/riesenraeder/
