---
layout: "image"
title: "Hermann"
date: 2021-10-31T14:17:35+01:00
picture: "P1012017.JPG"
weight: "21"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Hermann, der Laufroboter (https://ftcommunity.de/bilderpool/modelle/roboter-industrieanlagen-computing/lauf-fahr-kletterroboter/laufmaschinen-ralf-geerken/36253/) hat jetzt ein Laufband spendiert bekommen.,