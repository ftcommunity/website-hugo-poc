---
layout: "image"
title: "Boote"
date: 2021-10-31T14:18:02+01:00
picture: "P1012006.JPG"
weight: "14"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Die Herausforderung, so erklärte der Konstrukteur, ist es, das Gewicht so auszubalancieren, dass die Boote keine Schlagseite bekommen. Beide Modelle sind schwimmfähig.