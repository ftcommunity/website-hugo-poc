---
layout: "image"
title: "Paternoster"
date: 2021-10-31T14:18:42+01:00
picture: "P1012023.JPG"
weight: "2"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Informationen zu dem Modell und dem Thema gibt es in der ft:pedia (https://ftcommunity.de/ftpedia/2021/2021-1/ftpedia-2021-1.pdf#page=29)
