---
layout: "image"
title: "Kugelbahn – Fördermechanismus"
date: 2021-10-31T14:16:59+01:00
picture: "P1012029.JPG"
weight: "8"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Das besondere an dem Modell ist der Fördermechanismus mit zwei gegenläufigen Rädern.
