---
layout: "image"
title: "Industriemodell"
date: 2021-10-31T14:17:13+01:00
picture: "P1012025.JPG"
weight: "4"
konstrukteure: 
- "PSG-Robotik-AG"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Mit diesem Modell einer Verpackungsanlage belegten die Schülerinnen und Schüler der PDR-Robotik-AG den 1. Platz des Förderpreises des Museums, der bei der Convention vergeben wurde. Die Professionalität des Teams wurde allgemein bewundert.