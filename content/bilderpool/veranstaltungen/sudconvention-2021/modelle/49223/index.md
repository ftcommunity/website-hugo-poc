---
layout: "image"
title: "Kugelbahn"
date: 2021-10-31T14:17:03+01:00
picture: "P1012028.JPG"
weight: "7"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Ein neues Modell von Torsten, ohne Python