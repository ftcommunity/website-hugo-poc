---
layout: "image"
title: "Polyeder"
date: 2021-10-31T14:17:48+01:00
picture: "P1012013.JPG"
weight: "18"
konstrukteure: 
- "Familie Geerken"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Ein 12-Flächner (Dodekaeder) und ein 20-Flächner (Ikosaeder)