---
layout: "image"
title: "Trebuchet"
date: 2021-10-31T14:18:05+01:00
picture: "P1012034.JPG"
weight: "13"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Der obere Teil des Trebuchets