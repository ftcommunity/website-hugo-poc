---
layout: "image"
title: "LED mit Wechselstrom"
date: 2021-10-31T14:17:45+01:00
picture: "P1012014.JPG"
weight: "19"
konstrukteure: 
- "Felix Geerken"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Durch die Versorgung der LEDs mit Wechselstrom aus dem alten Trafo ergeben sich schöne Lichteffekte.
