---
layout: "image"
title: "Endloskugelbahn"
date: 2021-10-31T14:17:26+01:00
picture: "P1012004.JPG"
weight: "24"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Basierend auf einem Modell, das vor vielen Jahren in Erbes-Büdesheim zu sehen war (https://ftcommunity.de/bilderpool/veranstaltungen/erbes-budesheim/erbes-budesheim-2012/modelle-fredy/35546/) hier eine nette einfache Endloskugelbahn
