---
layout: "image"
title: "Industriemodell – Zuführung"
date: 2021-10-31T14:17:09+01:00
picture: "P1012027.JPG"
weight: "5"
konstrukteure: 
- "PSG-Robotik-AG"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Die bunten Steine aus dem 3D-Drucker werden in die gelben Dosen gepackt.
