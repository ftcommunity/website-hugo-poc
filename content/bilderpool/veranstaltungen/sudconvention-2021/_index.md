---
layout: "overview"
title: "Südconvention 2021"
date: 2021-10-31T14:16:56+01:00
---

Am 30.10.2021 fand die [Südconvention](https://ftcommunity.de/fans/veranstaltungen/suedconvention2021/) wieder im [Erlebnismuseum Fördertechnik](https://www.erlebnismuseum-fördertechnik.de/) in Sinsheim statt.  
