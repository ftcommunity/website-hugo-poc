---
layout: "image"
title: "Südconvention 2021"
date: 2021-10-31T14:18:19+01:00
picture: "P1012022.JPG"
weight: "1"
konstrukteure: 
- "Veranstalter und Aussteller"
fotografen:
- "EstherM"
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell rechts oberhalb der Bidlmitte gehört nicht zur Convention.