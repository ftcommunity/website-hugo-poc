---
layout: "image"
title: "Der Aufbau"
date: "2016-03-10T20:29:35"
picture: "neumuenster15.jpg"
weight: "15"
konstrukteure: 
- "Frank Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43047
- /detailsc738.html
imported:
- "2019"
_4images_image_id: "43047"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43047 -->
Frank vor seinem Schaufelradbagger.
