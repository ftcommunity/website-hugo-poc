---
layout: "image"
title: "neumuenster04.jpg"
date: "2016-03-10T20:29:35"
picture: "neumuenster04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43036
- /details32ad.html
imported:
- "2019"
_4images_image_id: "43036"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43036 -->
