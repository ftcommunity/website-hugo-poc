---
layout: "image"
title: "Turmbergbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster06.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43038
- /details9dd6.html
imported:
- "2019"
_4images_image_id: "43038"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43038 -->
