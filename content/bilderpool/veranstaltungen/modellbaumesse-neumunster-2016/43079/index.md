---
layout: "image"
title: "Traktor mit Nähwerk"
date: "2016-03-10T20:29:35"
picture: "neumuenster47.jpg"
weight: "47"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43079
- /details2edd.html
imported:
- "2019"
_4images_image_id: "43079"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43079 -->
