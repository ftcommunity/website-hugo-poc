---
layout: "overview"
title: "Modellbaumesse Neumünster 2016"
date: 2020-02-22T09:09:38+01:00
legacy_id:
- /php/categories/3200
- /categories0381.html
- /categories38f8.html
- /categories13f0-2.html
- /categories4e1b.html
- /categories8fc7.html
- /categories3189.html
- /categories5eb6.html
- /categoriesb6bd.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3200 --> 
Hallo fischertechnik-Freunde,

hier die Bilder zur ersten fischertechnik-Ausstellung vom 05.und 06. März 2016  im hohen Norden
in Schleswig-Holstein. Wir haben guten Publikumsververkehr bekommen. 

Wir hatten regen Zuspruch und haben den einen oder anderen Besucher begeistern können. 

Unsrere Mitmachaktionen kamen gut beim Publikum an. Wie z.B. Freundschaftsbänder stricken,
Kegeln, Roboter und Kran steuern, etc.

