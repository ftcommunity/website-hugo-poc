---
layout: "image"
title: "Der Aufbau"
date: "2016-03-10T20:29:35"
picture: "neumuenster08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43040
- /details2d9a.html
imported:
- "2019"
_4images_image_id: "43040"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43040 -->
Innenansicht Stand
