---
layout: "comment"
hidden: true
title: "21782"
date: "2016-03-10T22:44:39"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was genau machen denn die Pneumatikzylinder da? Federn? Sind die dafür kräftig genug?

Gruß,
Stefan