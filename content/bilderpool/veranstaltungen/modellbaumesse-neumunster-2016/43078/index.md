---
layout: "image"
title: "Bergungsraupe"
date: "2016-03-10T20:29:35"
picture: "neumuenster46.jpg"
weight: "46"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43078
- /details1e7a.html
imported:
- "2019"
_4images_image_id: "43078"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43078 -->
