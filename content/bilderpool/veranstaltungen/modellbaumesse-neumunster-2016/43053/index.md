---
layout: "image"
title: "Laser-Plotter"
date: "2016-03-10T20:29:35"
picture: "neumuenster21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43053
- /details8f53.html
imported:
- "2019"
_4images_image_id: "43053"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43053 -->
Zwei gelaserte Beispiele.
