---
layout: "image"
title: "Buggy + fischertechnik Buch"
date: "2016-03-10T20:29:35"
picture: "neumuenster48.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43080
- /details38ed.html
imported:
- "2019"
_4images_image_id: "43080"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43080 -->
