---
layout: "image"
title: "Modellbaumesse Neumünster 2016 (SH)"
date: "2016-03-10T20:29:35"
picture: "neumuenster01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43033
- /details99c4.html
imported:
- "2019"
_4images_image_id: "43033"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43033 -->
Das Team:

Holger (Svefisch)
Ben Jonas Alpen
Ralf (ThanksForTheFish)
Dirk (DirkW)
Finn Lukas Alpen
Frank (Frank Alpen)
Christine (am Fotoapperat)
