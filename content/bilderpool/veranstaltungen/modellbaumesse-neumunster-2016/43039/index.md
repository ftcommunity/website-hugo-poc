---
layout: "image"
title: "Der Aufbau"
date: "2016-03-10T20:29:35"
picture: "neumuenster07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43039
- /details22d7.html
imported:
- "2019"
_4images_image_id: "43039"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43039 -->
