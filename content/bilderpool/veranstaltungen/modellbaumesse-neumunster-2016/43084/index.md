---
layout: "image"
title: "Messe"
date: "2016-03-10T20:29:35"
picture: "neumuenster52.jpg"
weight: "52"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43084
- /details23ce.html
imported:
- "2019"
_4images_image_id: "43084"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43084 -->
Ralf erklärt seine Strickmaschine.
