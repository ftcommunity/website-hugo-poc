---
layout: "image"
title: "Messe"
date: "2016-03-10T20:29:35"
picture: "neumuenster54.jpg"
weight: "54"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/43086
- /details2872.html
imported:
- "2019"
_4images_image_id: "43086"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43086 -->
Christine befüllt den Schaufelradbagger mit Bällen.
