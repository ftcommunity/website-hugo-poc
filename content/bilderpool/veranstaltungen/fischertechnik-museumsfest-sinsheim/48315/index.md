---
layout: "image"
title: "Ausstellungsfläche"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos18.jpg"
weight: "67"
konstrukteure: 
- "-?-"
fotografen:
- "Erlebnismuseum Fördertechnik"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48315
- /details9c37-3.html
imported:
- "2019"
_4images_image_id: "48315"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48315 -->
