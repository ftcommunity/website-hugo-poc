---
layout: "image"
title: "Achterbahn - Prototyp von Wagen und Bahn (1)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim05.jpg"
weight: "5"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48253
- /details220c.html
imported:
- "2019"
_4images_image_id: "48253"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48253 -->
