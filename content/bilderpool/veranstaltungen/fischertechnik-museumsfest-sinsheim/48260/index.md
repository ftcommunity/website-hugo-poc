---
layout: "image"
title: "Energieerhaltung - Erläuterung"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim12.jpg"
weight: "12"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48260
- /details0922.html
imported:
- "2019"
_4images_image_id: "48260"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48260 -->
