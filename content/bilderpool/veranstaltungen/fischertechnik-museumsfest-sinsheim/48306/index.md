---
layout: "image"
title: "Polarkoordinaten Plotter"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos04.jpg"
weight: "58"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Erlebnismuseum Fördertechnik"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48306
- /details56a3-3.html
imported:
- "2019"
_4images_image_id: "48306"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48306 -->
