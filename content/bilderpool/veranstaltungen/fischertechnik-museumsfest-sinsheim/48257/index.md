---
layout: "image"
title: "Zentrifugalkraft"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim09.jpg"
weight: "9"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48257
- /details73d8.html
imported:
- "2019"
_4images_image_id: "48257"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48257 -->
Durch Kurbeln dreht sich der Stern und die Kugeln bewegen sich nach außen.
