---
layout: "image"
title: "Schrägseilbrücke - Steuerungszentrum"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim42.jpg"
weight: "42"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48290
- /detailsa3a0.html
imported:
- "2019"
_4images_image_id: "48290"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48290 -->
Das Teil mit den 36 Steckern links sind zwei 6-fach-Umschalt-Relais.
