---
layout: "image"
title: "tiefgründige Gespräche"
date: "2018-10-25T19:35:15"
picture: "FTC_Fotos25.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Erlebnismuseum Fördertechnik"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48320
- /detailsdf22.html
imported:
- "2019"
_4images_image_id: "48320"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48320 -->
