---
layout: "image"
title: "Achterbahn"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim02.jpg"
weight: "2"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48250
- /detailsfd5c.html
imported:
- "2019"
_4images_image_id: "48250"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48250 -->
Von einem Mitarbeiter des Museums erbaut.
