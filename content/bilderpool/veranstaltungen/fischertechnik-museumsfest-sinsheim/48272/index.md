---
layout: "image"
title: "Fußballspiel - Erläuterung"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim24.jpg"
weight: "24"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48272
- /details08f2.html
imported:
- "2019"
_4images_image_id: "48272"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48272 -->
