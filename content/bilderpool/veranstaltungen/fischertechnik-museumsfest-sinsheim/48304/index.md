---
layout: "image"
title: "Standseilbahn - Wagen von der Rückseite"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim56.jpg"
weight: "56"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48304
- /details50a3.html
imported:
- "2019"
_4images_image_id: "48304"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48304 -->
Von dieser Perspektive gab es noch gar kein Foto davon.
