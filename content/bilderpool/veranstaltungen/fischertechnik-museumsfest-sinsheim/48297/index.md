---
layout: "image"
title: "Hochregallager (2)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim49.jpg"
weight: "49"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48297
- /detailseadb.html
imported:
- "2019"
_4images_image_id: "48297"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48297 -->
