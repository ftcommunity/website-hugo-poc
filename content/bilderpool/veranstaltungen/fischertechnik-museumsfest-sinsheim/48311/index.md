---
layout: "image"
title: "DIE Kugelbahn"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos11.jpg"
weight: "63"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Erlebnismuseum Fördertechnik"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48311
- /details5914-3.html
imported:
- "2019"
_4images_image_id: "48311"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48311 -->
