---
layout: "image"
title: "Kreiselspiel"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim53.jpg"
weight: "53"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48301
- /details5c6e-2.html
imported:
- "2019"
_4images_image_id: "48301"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48301 -->
Wie auf der Süd-Convention 2018 und in der ft:pedia 2018-3.
