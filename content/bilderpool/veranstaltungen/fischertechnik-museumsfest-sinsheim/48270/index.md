---
layout: "image"
title: "Fußballspiel (1)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim22.jpg"
weight: "22"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48270
- /detailsac3f.html
imported:
- "2019"
_4images_image_id: "48270"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48270 -->
