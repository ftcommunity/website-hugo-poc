---
layout: "image"
title: "Schrägseilbrücke - Steuerkonsole"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim45.jpg"
weight: "45"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48293
- /detailsa87f.html
imported:
- "2019"
_4images_image_id: "48293"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48293 -->
