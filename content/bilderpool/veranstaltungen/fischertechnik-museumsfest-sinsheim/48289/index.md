---
layout: "image"
title: "Schrägseilbrücke"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim41.jpg"
weight: "41"
konstrukteure: 
- "ClassicMan"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48289
- /details08ed.html
imported:
- "2019"
_4images_image_id: "48289"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48289 -->
In diesem Modell stecken so viele Details, das sieht man auf den ersten und auch auf den zweiten Blick gar nicht.
