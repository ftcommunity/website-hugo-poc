---
layout: "image"
title: "Achterbahn - Prototyp von Wagen und Bahn (2)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim06.jpg"
weight: "6"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48254
- /details2d7a.html
imported:
- "2019"
_4images_image_id: "48254"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48254 -->
