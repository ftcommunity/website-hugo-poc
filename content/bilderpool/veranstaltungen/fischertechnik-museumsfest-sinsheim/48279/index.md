---
layout: "image"
title: "Autofocus-Kamera - Erläuterung"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim31.jpg"
weight: "31"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48279
- /details9b1d.html
imported:
- "2019"
_4images_image_id: "48279"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48279 -->
