---
layout: "image"
title: "3D-Scanner (1)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim26.jpg"
weight: "26"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48274
- /details4622.html
imported:
- "2019"
_4images_image_id: "48274"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48274 -->
