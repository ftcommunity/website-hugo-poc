---
layout: "image"
title: "Putzroboter - von oben"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim15.jpg"
weight: "15"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48263
- /detailsacd9.html
imported:
- "2019"
_4images_image_id: "48263"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48263 -->
