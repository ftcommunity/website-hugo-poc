---
layout: "image"
title: "Turmbergbahn"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos06.jpg"
weight: "60"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Erlebnismuseum Fördertechnik"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- /php/details/48308
- /details7ecf.html
imported:
- "2019"
_4images_image_id: "48308"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48308 -->
(Konstrukteur und Titel korrigiert, Website-Team, 18.09.2020)
