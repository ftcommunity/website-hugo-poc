---
layout: "image"
title: "Modelbaumesse Wien_Saturnrakete und Mondlandefähre"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien02.jpg"
weight: "4"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42160
- /details623f.html
imported:
- "2019"
_4images_image_id: "42160"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42160 -->
FISCHERTECHNIK TEAM AUSTRIA
