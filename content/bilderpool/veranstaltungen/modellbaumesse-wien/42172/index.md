---
layout: "image"
title: "Modelbaumesse Wien_Das Wiener Riesenrad"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien14.jpg"
weight: "16"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/42172
- /details6748.html
imported:
- "2019"
_4images_image_id: "42172"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42172 -->
Markus in Action!
