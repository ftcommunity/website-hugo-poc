---
layout: "image"
title: "Riesenrad in Wien 2"
date: "2015-10-28T08:58:30"
picture: "IMG_0119.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Dirk Haizmann (ft)"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/42158
- /details60d8.html
imported:
- "2019"
_4images_image_id: "42158"
_4images_cat_id: "3142"
_4images_user_id: "560"
_4images_image_date: "2015-10-28T08:58:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42158 -->
