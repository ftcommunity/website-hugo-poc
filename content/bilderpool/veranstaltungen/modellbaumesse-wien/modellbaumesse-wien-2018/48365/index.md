---
layout: "image"
title: "Modellbaumesse Wien 2018"
date: "2018-11-02T16:38:59"
picture: "modellbaumessewien2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48365
- /details053d.html
imported:
- "2019"
_4images_image_id: "48365"
_4images_cat_id: "3543"
_4images_user_id: "968"
_4images_image_date: "2018-11-02T16:38:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48365 -->
Immer viel Andrang und Besucherverkehr. Diese Messe wird von ca. 60.000 Menschen besucht.
