---
layout: "image"
title: "Modellbaumesse Wien 2018"
date: "2018-11-02T16:38:59"
picture: "modellbaumessewien6.jpg"
weight: "6"
konstrukteure: 
- "Modellbau Kirchert"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/48369
- /detailsc3a0.html
imported:
- "2019"
_4images_image_id: "48369"
_4images_cat_id: "3543"
_4images_user_id: "968"
_4images_image_date: "2018-11-02T16:38:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48369 -->
Immer umlagert....diverse Kugelbahnen.
