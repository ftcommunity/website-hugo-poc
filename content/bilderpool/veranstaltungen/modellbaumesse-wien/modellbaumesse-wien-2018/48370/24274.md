---
layout: "comment"
hidden: true
title: "24274"
date: "2018-11-05T21:48:16"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Wie transportiert man sowas denn? Doch wohl kaum in einem Stück - und dann müssen ja jedesmal die Seile neu gezogen werden - meine Anerkennung!