---
layout: "image"
title: "Amelia Builds the Trike"
date: "2009-08-25T00:48:17"
picture: "sm_ft_amelia_b.jpg"
weight: "107"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Amelia"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24827
- /details9a67.html
imported:
- "2019"
_4images_image_id: "24827"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-08-25T00:48:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24827 -->
Amelia builds the trike from the UII kit.