---
layout: "image"
title: "PCS Brain"
date: "2008-12-09T07:39:20"
picture: "PCS_Brain_sm_iso.jpg"
weight: "67"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16572
- /details9971.html
imported:
- "2019"
_4images_image_id: "16572"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-12-09T07:39:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16572 -->
Here is a rendering of the PCS Brain, indicating the ft slots. Thought to share.