---
layout: "image"
title: "Battery Test"
date: "2009-05-30T09:12:55"
picture: "motor_test_2x2b.jpg"
weight: "101"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24128
- /details2f75.html
imported:
- "2019"
_4images_image_id: "24128"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-30T09:12:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24128 -->
Testing the PCS BRAIN with a single rechargeable 9v battery.