---
layout: "image"
title: "Dragster with the BRAIN"
date: "2009-05-29T15:30:25"
picture: "sm_ft_car_2.jpg"
weight: "96"
konstrukteure: 
- "Laura and Sandon"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["dragster"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24123
- /details26d3-2.html
imported:
- "2019"
_4images_image_id: "24123"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-29T15:30:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24123 -->
Laura and her son constructed this dragster with a BRAIN!