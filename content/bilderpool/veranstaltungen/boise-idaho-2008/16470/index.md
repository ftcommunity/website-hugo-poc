---
layout: "image"
title: "ft Walker"
date: "2008-11-21T19:43:23"
picture: "ft_walker_4.jpg"
weight: "65"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["walking", "robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16470
- /details230f.html
imported:
- "2019"
_4images_image_id: "16470"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-11-21T19:43:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16470 -->
These are various walkers I built over the year. Thought to share.