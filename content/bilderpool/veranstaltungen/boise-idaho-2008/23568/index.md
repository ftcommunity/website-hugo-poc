---
layout: "image"
title: "Updated Lock Picker"
date: "2009-04-02T23:01:13"
picture: "sm_lc_right.jpg"
weight: "82"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["lock", "picker"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/23568
- /details235c.html
imported:
- "2019"
_4images_image_id: "23568"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-04-02T23:01:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23568 -->
This is an updated version of the Lock Picker. It uses two power motors and the PCS BRAIN. The Lock Picker generates and tests all the different possible 3 digit 
combinations  for a Master Combination Lock, eventually opening the lock! 

Harrison Grover built and programmed this model! 

I have an instructable at: 
http://www.instructables.com/id/fischertechnik-Robot-Lock-Picker/