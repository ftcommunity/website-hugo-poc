---
layout: "image"
title: "Nomenclature Baggie"
date: "2008-08-12T19:02:34"
picture: "pcs_nomen_bag_a.jpg"
weight: "52"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["nomenclature", "baggie", "teacher"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15041
- /detailsf209.html
imported:
- "2019"
_4images_image_id: "15041"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-08-12T19:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15041 -->
This is the insert card for a small bag of ft elements. The baggie is meant as an introduction to ft at a training seminar of elementary school teachers in Boise. (The seminar is today! 8-12-08)