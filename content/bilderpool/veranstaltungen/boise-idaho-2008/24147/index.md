---
layout: "image"
title: "Reaction Time Tester"
date: "2009-05-30T09:12:56"
picture: "rtt_3.jpg"
weight: "102"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24147
- /details2301.html
imported:
- "2019"
_4images_image_id: "24147"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-30T09:12:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24147 -->
This is my next entry into the www.instructables.com LED contest.