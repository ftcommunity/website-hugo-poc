---
layout: "image"
title: "Nomenclature Baggie"
date: "2008-08-13T18:52:43"
picture: "pcs_nomen_bag_c.jpg"
weight: "54"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["nomenclature", "baggie", "teacher"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/15044
- /detailsf1dc.html
imported:
- "2019"
_4images_image_id: "15044"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-08-13T18:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15044 -->
The box of nomenclature baggies to be delivered to the teacher seminar.