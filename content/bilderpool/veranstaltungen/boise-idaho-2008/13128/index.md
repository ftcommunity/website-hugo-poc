---
layout: "image"
title: "BSU_Team_7"
date: "2007-12-21T16:24:42"
picture: "bsu_g7b.jpg"
weight: "7"
konstrukteure: 
- "BSU students"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Boise State University", "2007"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13128
- /details59ab.html
imported:
- "2019"
_4images_image_id: "13128"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2007-12-21T16:24:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13128 -->
These are robots designed, created and programmed by Boise State University students in Boise Idaho, Dec 2007.
(google translation -Dabei handelt es sich um Roboter konzipiert, entwickelt und programmiert von Boise State University Studenten in Boise Idaho, Dezember 2007.!)