---
layout: "image"
title: "Line Follower"
date: "2008-12-10T14:11:52"
picture: "Simple_Line_Follower.jpg"
weight: "68"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Line", "Following", "Robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16573
- /detailsa5b7.html
imported:
- "2019"
_4images_image_id: "16573"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-12-10T14:11:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16573 -->
Here is a simple line following robot that I put together. Thought to share.