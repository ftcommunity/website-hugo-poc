---
layout: "image"
title: "ft Robot"
date: "2008-12-15T20:21:19"
picture: "sm_ft_robot_2.jpg"
weight: "71"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16624
- /details50e2.html
imported:
- "2019"
_4images_image_id: "16624"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-12-15T20:21:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16624 -->
Robot using PCS BRAIN.