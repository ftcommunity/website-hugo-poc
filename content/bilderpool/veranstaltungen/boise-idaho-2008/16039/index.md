---
layout: "image"
title: "August at Play 2"
date: "2008-10-25T09:37:17"
picture: "aug_2.jpg"
weight: "57"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["August"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/16039
- /details5507.html
imported:
- "2019"
_4images_image_id: "16039"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-10-25T09:37:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16039 -->
This is my son playing with models in my office.