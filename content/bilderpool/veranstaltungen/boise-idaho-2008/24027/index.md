---
layout: "image"
title: "ft Ferris Wheel and the Brain"
date: "2009-05-14T23:07:49"
picture: "sm_fw_3.jpg"
weight: "87"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/24027
- /details1c42.html
imported:
- "2019"
_4images_image_id: "24027"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-14T23:07:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24027 -->
This is a closeup of the BRAIN integrated with a Ferris Wheel.