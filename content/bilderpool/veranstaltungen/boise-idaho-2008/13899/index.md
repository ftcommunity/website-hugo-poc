---
layout: "image"
title: "ft brochure"
date: "2008-03-11T19:22:30"
picture: "ft_brochure.jpg"
weight: "45"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["ft", "brochure", "ITEA", "ft", "sightings"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/13899
- /details0ab7.html
imported:
- "2019"
_4images_image_id: "13899"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-03-11T19:22:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13899 -->
Here is a close-up of the brochure. I still have a request in.