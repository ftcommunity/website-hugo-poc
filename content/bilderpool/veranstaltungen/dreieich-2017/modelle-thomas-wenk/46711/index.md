---
layout: "image"
title: "Wellentransporter"
date: "2017-10-02T17:32:52"
picture: "modellethomaswenk5.jpg"
weight: "5"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46711
- /detailsa7f8-2.html
imported:
- "2019"
_4images_image_id: "46711"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46711 -->
