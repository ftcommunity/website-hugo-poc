---
layout: "image"
title: "Binärzählwerk"
date: "2017-10-02T17:32:52"
picture: "modellethomaswenk4.jpg"
weight: "4"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46710
- /detailsd1b6-2.html
imported:
- "2019"
_4images_image_id: "46710"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46710 -->
