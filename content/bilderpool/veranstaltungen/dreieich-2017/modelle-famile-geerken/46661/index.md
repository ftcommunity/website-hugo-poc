---
layout: "image"
title: "Würfel"
date: "2017-10-02T17:32:28"
picture: "modellederfamilegeerken3.jpg"
weight: "3"
konstrukteure: 
- "Familie Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46661
- /details2bc2.html
imported:
- "2019"
_4images_image_id: "46661"
_4images_cat_id: "3446"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46661 -->
