---
layout: "image"
title: "Leuchtturm"
date: "2017-10-02T17:32:28"
picture: "modellederfamilegeerken1.jpg"
weight: "1"
konstrukteure: 
- "Familie Geerken"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46659
- /details8dbc-2.html
imported:
- "2019"
_4images_image_id: "46659"
_4images_cat_id: "3446"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46659 -->
Wahrzeichen der Nordconvention...