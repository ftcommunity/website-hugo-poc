---
layout: "image"
title: "Eingang Phillipp-Köppen-Halle"
date: "2017-09-27T18:25:04"
picture: "dreieich83.jpg"
weight: "83"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46507
- /details93e0-2.html
imported:
- "2019"
_4images_image_id: "46507"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:25:04"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46507 -->
