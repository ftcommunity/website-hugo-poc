---
layout: "image"
title: "Panzer"
date: "2017-09-27T18:24:18"
picture: "dreieich12.jpg"
weight: "12"
konstrukteure: 
- "Severin"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46436
- /detailsd5cb.html
imported:
- "2019"
_4images_image_id: "46436"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:18"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46436 -->
