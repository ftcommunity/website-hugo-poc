---
layout: "image"
title: "Kugelspirale"
date: "2017-09-30T11:52:18"
picture: "aIMG_2874.jpg"
weight: "85"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/46522
- /detailsa123-2.html
imported:
- "2019"
_4images_image_id: "46522"
_4images_cat_id: "3437"
_4images_user_id: "2638"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46522 -->
