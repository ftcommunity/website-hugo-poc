---
layout: "image"
title: "Pistenbully"
date: "2017-09-27T18:24:30"
picture: "dreieich25.jpg"
weight: "25"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46449
- /details2b4e.html
imported:
- "2019"
_4images_image_id: "46449"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46449 -->
