---
layout: "image"
title: "Kugelbahn"
date: "2017-09-27T18:24:42"
picture: "dreieich48.jpg"
weight: "48"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46472
- /detailsf681.html
imported:
- "2019"
_4images_image_id: "46472"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:42"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46472 -->
