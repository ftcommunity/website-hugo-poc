---
layout: "image"
title: "Schienenfahrzeuge"
date: "2017-09-27T18:24:36"
picture: "dreieich42.jpg"
weight: "42"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46466
- /details2f1c-2.html
imported:
- "2019"
_4images_image_id: "46466"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:36"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46466 -->
