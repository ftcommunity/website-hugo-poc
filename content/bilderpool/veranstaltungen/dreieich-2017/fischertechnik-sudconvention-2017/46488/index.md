---
layout: "image"
title: "Maoam-Automat"
date: "2017-09-27T18:24:51"
picture: "dreieich64.jpg"
weight: "64"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46488
- /detailsd09b.html
imported:
- "2019"
_4images_image_id: "46488"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:51"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46488 -->
