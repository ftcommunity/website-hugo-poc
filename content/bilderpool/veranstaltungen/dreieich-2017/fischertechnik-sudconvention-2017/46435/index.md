---
layout: "image"
title: "Wall-E"
date: "2017-09-27T18:24:18"
picture: "dreieich11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46435
- /details63c6.html
imported:
- "2019"
_4images_image_id: "46435"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46435 -->
