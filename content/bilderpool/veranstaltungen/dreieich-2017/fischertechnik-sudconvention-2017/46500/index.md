---
layout: "image"
title: "Transformer"
date: "2017-09-27T18:24:57"
picture: "dreieich76.jpg"
weight: "76"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46500
- /details05f4.html
imported:
- "2019"
_4images_image_id: "46500"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:57"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46500 -->
