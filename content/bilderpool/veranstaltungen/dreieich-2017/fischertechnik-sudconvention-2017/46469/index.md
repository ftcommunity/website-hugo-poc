---
layout: "image"
title: "Panzer"
date: "2017-09-27T18:24:42"
picture: "dreieich45.jpg"
weight: "45"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46469
- /detailsaa32.html
imported:
- "2019"
_4images_image_id: "46469"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:42"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46469 -->
