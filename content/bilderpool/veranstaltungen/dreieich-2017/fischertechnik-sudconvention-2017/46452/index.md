---
layout: "image"
title: "Fahrantrieb unten"
date: "2017-09-27T18:24:30"
picture: "dreieich28.jpg"
weight: "28"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46452
- /details2431-2.html
imported:
- "2019"
_4images_image_id: "46452"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46452 -->
