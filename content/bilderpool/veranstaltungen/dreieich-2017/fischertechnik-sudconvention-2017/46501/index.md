---
layout: "image"
title: "Transformer"
date: "2017-09-27T18:24:57"
picture: "dreieich77.jpg"
weight: "77"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46501
- /details1529.html
imported:
- "2019"
_4images_image_id: "46501"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:57"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46501 -->
