---
layout: "image"
title: "Saterday afternoon, back home"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch21.jpg"
weight: "21"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46411
- /details66fa.html
imported:
- "2019"
_4images_image_id: "46411"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46411 -->
