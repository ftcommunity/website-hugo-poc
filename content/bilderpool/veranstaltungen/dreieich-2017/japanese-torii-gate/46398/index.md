---
layout: "image"
title: "Fryday evening"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch08.jpg"
weight: "8"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46398
- /details986e.html
imported:
- "2019"
_4images_image_id: "46398"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46398 -->
