---
layout: "image"
title: "Saterday afternoon, back home"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch24.jpg"
weight: "24"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46414
- /details6153.html
imported:
- "2019"
_4images_image_id: "46414"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46414 -->
