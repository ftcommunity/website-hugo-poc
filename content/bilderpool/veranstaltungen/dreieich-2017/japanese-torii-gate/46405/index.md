---
layout: "image"
title: "ftconventionchinesestriumphalarch15.jpg"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch15.jpg"
weight: "15"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46405
- /details41e0.html
imported:
- "2019"
_4images_image_id: "46405"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46405 -->
