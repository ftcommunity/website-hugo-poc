---
layout: "image"
title: "Saturday morning 7 o' clock"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch10.jpg"
weight: "10"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46400
- /detailsd5b9.html
imported:
- "2019"
_4images_image_id: "46400"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46400 -->
