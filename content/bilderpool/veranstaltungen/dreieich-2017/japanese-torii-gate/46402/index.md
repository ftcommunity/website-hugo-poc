---
layout: "image"
title: "Saturday morning"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch12.jpg"
weight: "12"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46402
- /details0b9d.html
imported:
- "2019"
_4images_image_id: "46402"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46402 -->
