---
layout: "image"
title: "Saturday morning just before the start of the convention"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch14.jpg"
weight: "14"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46404
- /details6718.html
imported:
- "2019"
_4images_image_id: "46404"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46404 -->
