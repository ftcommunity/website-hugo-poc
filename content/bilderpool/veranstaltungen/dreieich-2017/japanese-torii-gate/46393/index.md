---
layout: "image"
title: "ftconventionchinesestriumphalarch03.jpg"
date: "2017-09-25T13:48:07"
picture: "ftconventionchinesestriumphalarch03.jpg"
weight: "3"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46393
- /details6ce6.html
imported:
- "2019"
_4images_image_id: "46393"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46393 -->
