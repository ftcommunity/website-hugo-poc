---
layout: "image"
title: "Saturday morning"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch11.jpg"
weight: "11"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46401
- /details8d09.html
imported:
- "2019"
_4images_image_id: "46401"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46401 -->
