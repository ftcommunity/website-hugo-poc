---
layout: "image"
title: "ftconventionchinesestriumphalarch20.jpg"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch20.jpg"
weight: "20"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46410
- /details27ee.html
imported:
- "2019"
_4images_image_id: "46410"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46410 -->
