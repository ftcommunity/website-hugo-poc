---
layout: "image"
title: "ftconventionchinesestriumphalarch02.jpg"
date: "2017-09-25T13:48:07"
picture: "ftconventionchinesestriumphalarch02.jpg"
weight: "2"
konstrukteure: 
- "Michael Stratmann (Denkmal)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46392
- /detailsd320.html
imported:
- "2019"
_4images_image_id: "46392"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46392 -->
