---
layout: "image"
title: "Unimog"
date: "2017-10-02T17:32:52"
picture: "modellepeterdamen2.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46676
- /details94ac.html
imported:
- "2019"
_4images_image_id: "46676"
_4images_cat_id: "3450"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46676 -->
... mit wasserfester Plane
