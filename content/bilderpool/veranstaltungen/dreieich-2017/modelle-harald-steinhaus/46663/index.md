---
layout: "image"
title: "Achtung, die Legos kommen..."
date: "2017-10-02T17:32:28"
picture: "modelleharaldsteinhaus1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46663
- /details23b0.html
imported:
- "2019"
_4images_image_id: "46663"
_4images_cat_id: "3448"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46663 -->
Hübsches Verteidigungsgerät, voll mit Technik
