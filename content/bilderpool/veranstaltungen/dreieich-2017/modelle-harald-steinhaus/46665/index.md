---
layout: "image"
title: "Panzer - Federung"
date: "2017-10-02T17:32:28"
picture: "modelleharaldsteinhaus3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46665
- /detailsb5cb.html
imported:
- "2019"
_4images_image_id: "46665"
_4images_cat_id: "3448"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46665 -->
Federung der Gleisketten-Achsen
