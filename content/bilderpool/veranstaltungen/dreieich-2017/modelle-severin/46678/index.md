---
layout: "image"
title: "Panzer"
date: "2017-10-02T17:32:52"
picture: "modellevonseverinboth1.jpg"
weight: "1"
konstrukteure: 
- "Severin Both"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46678
- /details4fbc.html
imported:
- "2019"
_4images_image_id: "46678"
_4images_cat_id: "3451"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46678 -->
