---
layout: "image"
title: "ftconventiondreiech078.jpg"
date: "2017-09-25T13:47:52"
picture: "ftconventiondreiech078.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46362
- /detailsa46a.html
imported:
- "2019"
_4images_image_id: "46362"
_4images_cat_id: "3439"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:52"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46362 -->
