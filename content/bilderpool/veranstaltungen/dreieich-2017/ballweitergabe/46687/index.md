---
layout: "image"
title: "Gesamtansicht der Ballweitergabemaschine"
date: "2017-10-02T17:32:52"
picture: "ballweitergabemaschine1.jpg"
weight: "16"
konstrukteure: 
- "Diverse"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46687
- /details20bd.html
imported:
- "2019"
_4images_image_id: "46687"
_4images_cat_id: "3439"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46687 -->
Gemeinschaftsprojekt