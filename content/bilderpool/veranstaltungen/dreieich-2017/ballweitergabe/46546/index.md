---
layout: "image"
title: "Ballweitergabe"
date: "2017-09-30T13:03:40"
picture: "ftconvs04.jpg"
weight: "14"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46546
- /details616b.html
imported:
- "2019"
_4images_image_id: "46546"
_4images_cat_id: "3439"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46546 -->
In der Zulauf-Wanne befindet sich unten eine Verkleidungsplatte, die per Gummizug angeschnippst wird und dann den Ball wie beim Basketball in den großen Korb (mit zwei grünen Zielringen) hinein wirft. Am Ausgang wird der Strom der ablaufenden Bälle gebremst und getaktet.
