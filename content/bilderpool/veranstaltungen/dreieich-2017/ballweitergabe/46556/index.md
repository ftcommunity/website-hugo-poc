---
layout: "image"
title: "Ball02"
date: "2017-09-30T18:52:16"
picture: "2017-09_Dreieich2.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46556
- /detailsab41.html
imported:
- "2019"
_4images_image_id: "46556"
_4images_cat_id: "3439"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T18:52:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46556 -->
Die Wippe am Wendepunkt. Links wird angesaugt, rechts wurde der Gebläsemotor gerade abgeschaltet und der Ball ist über die grünen Bahnteile nach hinten entfleucht.
