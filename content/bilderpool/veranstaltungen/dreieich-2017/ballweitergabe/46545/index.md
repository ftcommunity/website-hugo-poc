---
layout: "image"
title: "Ballweitergabe"
date: "2017-09-30T13:03:40"
picture: "ftconvs03.jpg"
weight: "13"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46545
- /detailsae1b.html
imported:
- "2019"
_4images_image_id: "46545"
_4images_cat_id: "3439"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46545 -->
Drei P-Zylinder mit Hebelmechanik schubsen den Ball um drei Ecken bis zum Ausgang.
