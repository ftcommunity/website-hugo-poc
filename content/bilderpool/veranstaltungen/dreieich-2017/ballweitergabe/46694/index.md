---
layout: "image"
title: "Kurbeltransporter"
date: "2017-10-02T17:32:52"
picture: "ballweitergabemaschine8.jpg"
weight: "23"
konstrukteure: 
- "Diverse"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46694
- /details6fef.html
imported:
- "2019"
_4images_image_id: "46694"
_4images_cat_id: "3439"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46694 -->
