---
layout: "image"
title: "Ballschubser"
date: "2017-10-02T17:32:52"
picture: "ballweitergabemaschine6.jpg"
weight: "21"
konstrukteure: 
- "Diverse"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46692
- /details3e2d-2.html
imported:
- "2019"
_4images_image_id: "46692"
_4images_cat_id: "3439"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46692 -->
