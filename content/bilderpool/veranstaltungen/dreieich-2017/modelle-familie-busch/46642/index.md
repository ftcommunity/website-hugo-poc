---
layout: "image"
title: "Pistenbully (Erik)"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch7.jpg"
weight: "7"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46642
- /detailseba6.html
imported:
- "2019"
_4images_image_id: "46642"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46642 -->
Ausführliche Beschreibung in ft:pedia 1/2015
