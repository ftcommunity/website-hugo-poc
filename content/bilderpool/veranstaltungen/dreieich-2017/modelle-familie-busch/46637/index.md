---
layout: "image"
title: "Flugzeugschlepper (Jörg)"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch2.jpg"
weight: "2"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46637
- /details65b1.html
imported:
- "2019"
_4images_image_id: "46637"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46637 -->
Blick unter die Haube
