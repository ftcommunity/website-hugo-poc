---
layout: "image"
title: "Pistenbully (Erik) in Action"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch8.jpg"
weight: "8"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46643
- /details8683.html
imported:
- "2019"
_4images_image_id: "46643"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46643 -->
Ausführliche Beschreibung in ft:pedia 1/2015
