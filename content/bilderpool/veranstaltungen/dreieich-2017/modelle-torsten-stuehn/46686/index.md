---
layout: "image"
title: "3D-Scanner"
date: "2017-10-02T17:32:52"
picture: "modellevontorstenstuehn1.jpg"
weight: "1"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46686
- /details3401.html
imported:
- "2019"
_4images_image_id: "46686"
_4images_cat_id: "3453"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46686 -->
Scan mit fischertechnik-Kamera und Laser oder fokussiertem fischertechnik-Lämpchen;
rechts die (mit dem fischertechnik-3D-Drucker) verkleinert ausgedruckte Ente (rot).
