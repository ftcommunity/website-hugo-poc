---
layout: "image"
title: "Foucault-Pendel"
date: "2017-09-30T13:03:40"
picture: "ftconvs07.jpg"
weight: "6"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/46549
- /detailsfb34.html
imported:
- "2019"
_4images_image_id: "46549"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46549 -->
Unten im Arm wird ein Stift eingespannt, der die Bewegungsbahn auf ein Stück Papier malt.
