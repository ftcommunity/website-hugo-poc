---
layout: "image"
title: "Transformer - Detail"
date: "2017-10-02T17:32:39"
picture: "modellevonjenslemkamp4.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46674
- /details4877.html
imported:
- "2019"
_4images_image_id: "46674"
_4images_cat_id: "3449"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46674 -->
Hohe Belastung für Lager und Motoren
