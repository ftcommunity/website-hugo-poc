---
layout: "image"
title: "Ballgreifer"
date: "2017-10-02T17:32:53"
picture: "modellepeterhabermehl1.jpg"
weight: "1"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46726
- /detailsd9c3.html
imported:
- "2019"
_4images_image_id: "46726"
_4images_cat_id: "3460"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46726 -->
