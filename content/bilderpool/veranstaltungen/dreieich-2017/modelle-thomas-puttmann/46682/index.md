---
layout: "image"
title: "Binäraddierer"
date: "2017-10-02T17:32:52"
picture: "modellevonthomaspuettmann1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46682
- /details03da.html
imported:
- "2019"
_4images_image_id: "46682"
_4images_cat_id: "3452"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46682 -->
Ausführlich beschrieben in ft:pedia 3/2014.
