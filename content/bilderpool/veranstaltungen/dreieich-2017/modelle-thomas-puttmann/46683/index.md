---
layout: "image"
title: "Delta-Roboter"
date: "2017-10-02T17:32:52"
picture: "modellevonthomaspuettmann2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46683
- /details59fb.html
imported:
- "2019"
_4images_image_id: "46683"
_4images_cat_id: "3452"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46683 -->
Spielt Tic-Tac-Toe gegen menschlichen Mitspieler.
