---
layout: "image"
title: "Fahrtrainer (digital)"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46716
- /detailsa017.html
imported:
- "2019"
_4images_image_id: "46716"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46716 -->
Work in progress
