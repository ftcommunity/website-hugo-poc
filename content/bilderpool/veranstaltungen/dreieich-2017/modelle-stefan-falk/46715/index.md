---
layout: "image"
title: "Fahrtrainer"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46715
- /details939e.html
imported:
- "2019"
_4images_image_id: "46715"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46715 -->
... mit hangemalter Fahrstrecke (aus den 70er Jahren)
