---
layout: "image"
title: "Stepper"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46721
- /detailsd908.html
imported:
- "2019"
_4images_image_id: "46721"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46721 -->
