---
layout: "image"
title: "Zyklisch variable Getriebe"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46722
- /detailsc2da.html
imported:
- "2019"
_4images_image_id: "46722"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46722 -->
Angelehnt an die Getriebe von Wilhelm Klopmeier (innklusive Umkehrgetriebe, ganz rechts...)
