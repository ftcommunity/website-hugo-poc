---
layout: "image"
title: "Teleskop-Gabelstapler"
date: "2017-10-02T17:32:53"
picture: "modellefamilieholtz3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46725
- /details032f.html
imported:
- "2019"
_4images_image_id: "46725"
_4images_cat_id: "3459"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46725 -->
Ferngesteuerter Gabelstapler mit pneumatischem Teleskoparm
