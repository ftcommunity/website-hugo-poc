---
layout: "image"
title: "Fertigungsstraße"
date: "2017-10-02T17:32:53"
picture: "modellefamilieholtz2.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46724
- /details1a16.html
imported:
- "2019"
_4images_image_id: "46724"
_4images_cat_id: "3459"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46724 -->
Schweißen und Zusammenbau einer Porsche-Karosserie
