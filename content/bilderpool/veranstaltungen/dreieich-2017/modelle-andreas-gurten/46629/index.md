---
layout: "image"
title: "Paternoster"
date: "2017-10-02T17:32:28"
picture: "modelleandreasguerten2.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46629
- /details2e49-2.html
imported:
- "2019"
_4images_image_id: "46629"
_4images_cat_id: "3441"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46629 -->
Funktionsmodell eines Umlauf-Aufzugs
