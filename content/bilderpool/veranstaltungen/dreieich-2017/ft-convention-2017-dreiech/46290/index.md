---
layout: "image"
title: "ftconventiondreiech006.jpg"
date: "2017-09-25T13:46:56"
picture: "ftconventiondreiech006.jpg"
weight: "6"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46290
- /details98a6.html
imported:
- "2019"
_4images_image_id: "46290"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:46:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46290 -->
