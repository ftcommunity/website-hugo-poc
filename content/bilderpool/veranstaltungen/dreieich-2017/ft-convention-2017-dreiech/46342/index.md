---
layout: "image"
title: "ftconventiondreiech058.jpg"
date: "2017-09-25T13:47:43"
picture: "ftconventiondreiech058.jpg"
weight: "58"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46342
- /details8d7b.html
imported:
- "2019"
_4images_image_id: "46342"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:43"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46342 -->
