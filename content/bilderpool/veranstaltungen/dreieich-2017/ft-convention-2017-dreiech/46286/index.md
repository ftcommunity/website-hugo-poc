---
layout: "image"
title: "ftconventiondreiech002.jpg"
date: "2017-09-25T13:46:56"
picture: "ftconventiondreiech002.jpg"
weight: "2"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46286
- /details952b.html
imported:
- "2019"
_4images_image_id: "46286"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:46:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46286 -->
