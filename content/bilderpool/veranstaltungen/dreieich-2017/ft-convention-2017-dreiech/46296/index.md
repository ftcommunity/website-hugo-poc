---
layout: "image"
title: "ftconventiondreiech012.jpg"
date: "2017-09-25T13:47:20"
picture: "ftconventiondreiech012.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46296
- /detailsadc0.html
imported:
- "2019"
_4images_image_id: "46296"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:20"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46296 -->
