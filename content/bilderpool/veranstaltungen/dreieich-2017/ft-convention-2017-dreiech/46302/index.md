---
layout: "image"
title: "ftconventiondreiech018.jpg"
date: "2017-09-25T13:47:20"
picture: "ftconventiondreiech018.jpg"
weight: "18"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46302
- /details5d02.html
imported:
- "2019"
_4images_image_id: "46302"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:20"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46302 -->
