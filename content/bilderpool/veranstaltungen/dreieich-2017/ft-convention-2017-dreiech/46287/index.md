---
layout: "image"
title: "ftconventiondreiech003.jpg"
date: "2017-09-25T13:46:56"
picture: "ftconventiondreiech003.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/46287
- /detailsa161-2.html
imported:
- "2019"
_4images_image_id: "46287"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:46:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46287 -->
