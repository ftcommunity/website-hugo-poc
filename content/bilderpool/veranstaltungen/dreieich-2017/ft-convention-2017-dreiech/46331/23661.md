---
layout: "comment"
hidden: true
title: "23661"
date: "2017-09-29T16:34:29"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Turm 2 / Bergstation der Schrägseilbrücke.
Die Statik hat Jan (8) entworfen und alleine zusammengeschraubt.