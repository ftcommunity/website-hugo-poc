---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen6.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38145
- /detailsa517.html
imported:
- "2019"
_4images_image_id: "38145"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38145 -->
Der Cymbalstern und zwei von drei Trucks. Der Stern mit dem Glockenspiel zum selber schalten lief den ganzen Tag.
