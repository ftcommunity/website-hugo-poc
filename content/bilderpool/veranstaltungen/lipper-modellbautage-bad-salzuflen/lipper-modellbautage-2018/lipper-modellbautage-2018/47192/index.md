---
layout: "image"
title: "Lipper Modedllbautage 2018"
date: "2018-01-25T16:43:57"
picture: "lippermodellbautage3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/47192
- /detailse70e.html
imported:
- "2019"
_4images_image_id: "47192"
_4images_cat_id: "3491"
_4images_user_id: "968"
_4images_image_date: "2018-01-25T16:43:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47192 -->
Das Riesenrad ist ,bis auf die Kabinen ,ein kompletter Neubau.283 cm hoch.
Der Aufbau dauert c. 6Std.
