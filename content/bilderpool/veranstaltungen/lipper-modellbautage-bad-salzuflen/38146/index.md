---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen7.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38146
- /details0671.html
imported:
- "2019"
_4images_image_id: "38146"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38146 -->
V.l.n.r. Das Teil mit den drei Männle,ein Planetengtriebe,dann eine" 90 Grad Welle", ein Laufrad sowie ein Differential.
Alle vier Teile zum selbstbetätigen.
