---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38141
- /detailsc8e3.html
imported:
- "2019"
_4images_image_id: "38141"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38141 -->
Gesamtansicht 2
