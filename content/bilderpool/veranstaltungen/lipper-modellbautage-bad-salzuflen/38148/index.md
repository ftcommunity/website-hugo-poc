---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen9.jpg"
weight: "9"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38148
- /detailsb31e.html
imported:
- "2019"
_4images_image_id: "38148"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38148 -->
Mit so viel Interesse habe ich nicht gerechnet.In den 3 Tagen waren ca. 17.000 Besucher in der Halle.
Die Zahl derer die bei mir geschaut haben dürfte sich im Bereich von mehreren tsd. bewegt haben.
