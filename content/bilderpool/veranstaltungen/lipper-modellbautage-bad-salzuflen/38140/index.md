---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38140
- /details07a0.html
imported:
- "2019"
_4images_image_id: "38140"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38140 -->
Gesamtansicht 1 mit Johanna vor dem Ansturm.
