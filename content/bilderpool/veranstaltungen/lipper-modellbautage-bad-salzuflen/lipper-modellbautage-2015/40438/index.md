---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage01.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wol"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/40438
- /detailsb4c1.html
imported:
- "2019"
_4images_image_id: "40438"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40438 -->
Am ersten Tag noch solo.
