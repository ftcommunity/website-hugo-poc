---
layout: "overview"
title: "Lipper Modellbautage 2015"
date: 2020-02-22T09:08:48+01:00
legacy_id:
- /php/categories/3033
- /categorieseaa5.html
- /categories864e.html
- /categories36c3.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3033 --> 
Trotz einiger Vorbehalte habe ich dieses Jahr wieder an dieser regionalen Modellausstellung teilgenommen.
Wie letztes Jahr schon fand das ganze auf ca 12.000 qm im Messezentrum Bad Salzuflen.

Schwerpunktmäßig wurden Eisenbahnen in allen Größen gezeigt. Dazu Schiffsmodelle, 
Trucks, Agrarmodellbau mit entsprechenden Vorführparcours und natürlich alle erdenklichen Heli und Flugzeugausteller sowie Händler zu allen Themenbereichen. 

Ich hatte dieses Jahr meine Kugelbahn dabei,dasWiener Riesenrad sowie diverse kleinere Modelle und die Basteleien meiner Kinder,
also praktisch das gleiche Besteck wie in EB 2014. 

Ich werde mal versuchen, meinen subjektiven Eindruckzum Thema FT  mittels aufgeschnappter Aussagen um mich herum zu vermitteln. 

1) " Das habe ich auch noch irgenwo."
2) "Was ist das denn ? " (viele Kinder) 
3) "Ich wußte gar nicht das es das noch gibt " 
 4) "Das hatte ich als Kind auch mal" 

Zentrale Erkenntniss diese Jahr war für mich: Es müßen, trotz ebay u.s.w. noch Millionen von FT Teilen auf deutschen Dächern und in Kellern liegen.

Für mich hat sich das ganze in der Weise gelohnt, als das ich auf einem nicht FT Treffen ganz viele begeisterte Gesichter von Jung und Alt gesehen habe. 
Werde wahrscheinlich nächsten Januar wieder dabei sein. 

Es wäre übrigens gut möglich, FT da etwas größer zu präsentieren. Wer also Zeit und Lust und etwas zum ausstellen hat, könnte sich ggf. bei mir melden.
Ich würde mich dann um die Ausstellungsfläche etc. kümmern. Aber Achtung !...... das ganze ist natürlich eine  ideelle Aktion. Die Kosten für Anfahrt,Unterkuntf und Verpflegung
trägt man selber. Wer trotzdem Bock hat ,kann sich gerne bei mir melden.

Grüß aus OWL



