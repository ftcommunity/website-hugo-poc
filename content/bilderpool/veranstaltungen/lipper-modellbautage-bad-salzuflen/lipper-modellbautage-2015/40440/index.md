---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage03.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wol"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/40440
- /details2205.html
imported:
- "2019"
_4images_image_id: "40440"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40440 -->
Die gesammelten Werke meiner Kinder.
