---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage02.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wol"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/40439
- /detailseedf.html
imported:
- "2019"
_4images_image_id: "40439"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40439 -->
Sa. und So. dann mit den Kindern. Die waren allerdings mehr unterwegs als am Stand.
