---
layout: "image"
title: "ftc-TTB-Modulring"
date: "2018-06-19T08:26:44"
picture: "mintka14.jpg"
weight: "15"
konstrukteure: 
- "Sisyphos"
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47711
- /details4d6f.html
imported:
- "2019"
_4images_image_id: "47711"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47711 -->
Minimalistisch. Was aus Dreieich-Offenthal noch übrig war.

Links im Bild schaut noch ein Stück vom "Fernrohr" herein. Alte Linsen gegen neue Linsen im direkten Vergleich.
