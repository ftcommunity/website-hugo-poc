---
layout: "image"
title: "... wieder hoch ..."
date: "2018-06-19T08:26:44"
picture: "mintka03.jpg"
weight: "4"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47700
- /detailsf9bf.html
imported:
- "2019"
_4images_image_id: "47700"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47700 -->
Doppelseitiges Heben in der Halbzollklasse. Sportlich!
