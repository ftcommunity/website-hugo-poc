---
layout: "image"
title: "Der Meister nach der Arbeit"
date: "2018-06-19T08:26:44"
picture: "mintka09.jpg"
weight: "10"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47706
- /details4c06.html
imported:
- "2019"
_4images_image_id: "47706"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47706 -->
Es war ziemlich schwül in der Halle. Nur damit jetzt keine Gerüchte aufkommen.
