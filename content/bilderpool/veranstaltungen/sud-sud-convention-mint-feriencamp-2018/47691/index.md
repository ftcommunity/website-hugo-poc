---
layout: "image"
title: "Uhrengetriebe"
date: "2018-06-18T12:39:28"
picture: "suedsuedconvention1.jpg"
weight: "1"
konstrukteure: 
- "Aussteller"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/47691
- /detailsd9d7.html
imported:
- "2019"
_4images_image_id: "47691"
_4images_cat_id: "3519"
_4images_user_id: "104"
_4images_image_date: "2018-06-18T12:39:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47691 -->
Die Uhr zieht sich bei Bedarf selbst auf, ohne ihren Lauf oder das Schlagwerk zu unterbrechen
