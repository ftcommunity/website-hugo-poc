---
layout: "image"
title: "Geometers Sextant"
date: "2018-06-19T08:26:44"
picture: "mintka10.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47707
- /details1f73.html
imported:
- "2019"
_4images_image_id: "47707"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47707 -->
Eine anschauliche Erklärung,
