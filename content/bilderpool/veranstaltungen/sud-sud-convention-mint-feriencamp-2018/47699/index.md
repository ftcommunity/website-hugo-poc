---
layout: "image"
title: "... über den Holzweg ..."
date: "2018-06-19T08:26:44"
picture: "mintka02.jpg"
weight: "3"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47699
- /detailse623.html
imported:
- "2019"
_4images_image_id: "47699"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47699 -->
Wie schön sich doch fischertechnik und Cuboro vertragen.
