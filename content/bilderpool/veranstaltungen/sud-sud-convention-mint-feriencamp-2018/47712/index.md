---
layout: "image"
title: "Zickige Ameise"
date: "2018-06-19T08:26:44"
picture: "mintka15.jpg"
weight: "16"
konstrukteure: 
- "Sisyphos"
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47712
- /detailsbec6-2.html
imported:
- "2019"
_4images_image_id: "47712"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47712 -->
Von wegen fleissig. Die hatte heuer deutlich keine Lust. War wohl das schwarze Schaf - pardon: Ameise - im Staat.
(Sorry Sisyphos, I felt the need to rename it)