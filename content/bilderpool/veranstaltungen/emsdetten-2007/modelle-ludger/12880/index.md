---
layout: "image"
title: "Förderband"
date: "2007-11-29T17:35:20"
picture: "olli07.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12880
- /detailsf56e.html
imported:
- "2019"
_4images_image_id: "12880"
_4images_cat_id: "1159"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12880 -->
Von Ludger