---
layout: "image"
title: "Raupe"
date: "2007-11-26T16:28:10"
picture: "modellevonludger4.jpg"
weight: "4"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12836
- /details0b77.html
imported:
- "2019"
_4images_image_id: "12836"
_4images_cat_id: "1159"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12836 -->
Die Erbsen laufen über das Förderband zu einer Stadion mit LKWs.
