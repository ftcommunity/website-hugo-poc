---
layout: "image"
title: "Raupe"
date: "2007-11-26T16:28:10"
picture: "modellevonludger1.jpg"
weight: "1"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12833
- /details067d.html
imported:
- "2019"
_4images_image_id: "12833"
_4images_cat_id: "1159"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12833 -->
Hier läd die Raupe ihre Schaufel voll mit Erbsen.
