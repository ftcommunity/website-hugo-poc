---
layout: "image"
title: "Dampfwalze"
date: "2007-11-28T18:08:07"
picture: "walze1.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12860
- /details9ba1.html
imported:
- "2019"
_4images_image_id: "12860"
_4images_cat_id: "1161"
_4images_user_id: "453"
_4images_image_date: "2007-11-28T18:08:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12860 -->
