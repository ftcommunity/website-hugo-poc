---
layout: "image"
title: "LKW"
date: "2007-11-29T17:35:20"
picture: "olli22.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12895
- /details4e70.html
imported:
- "2019"
_4images_image_id: "12895"
_4images_cat_id: "1158"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12895 -->
LKW
