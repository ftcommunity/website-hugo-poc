---
layout: "image"
title: "Funktionsmodell"
date: "2007-11-29T17:35:21"
picture: "olli35.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12908
- /details4d82.html
imported:
- "2019"
_4images_image_id: "12908"
_4images_cat_id: "1158"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:21"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12908 -->
