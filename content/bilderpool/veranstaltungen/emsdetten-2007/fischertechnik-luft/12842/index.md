---
layout: "image"
title: "Überblick"
date: "2007-11-26T16:28:11"
picture: "fischertechnikluft3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12842
- /details82cf.html
imported:
- "2019"
_4images_image_id: "12842"
_4images_cat_id: "1160"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12842 -->
