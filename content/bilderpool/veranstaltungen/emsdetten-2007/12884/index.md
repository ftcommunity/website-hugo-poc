---
layout: "image"
title: "ft 020"
date: "2007-11-29T17:35:20"
picture: "olli11.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12884
- /detailsdc40.html
imported:
- "2019"
_4images_image_id: "12884"
_4images_cat_id: "1158"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12884 -->
Kratübertragung um 90°.