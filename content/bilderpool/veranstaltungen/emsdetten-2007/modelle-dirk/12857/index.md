---
layout: "image"
title: "Radlader"
date: "2007-11-28T18:08:07"
picture: "modellevondirk2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12857
- /details539d.html
imported:
- "2019"
_4images_image_id: "12857"
_4images_cat_id: "1163"
_4images_user_id: "453"
_4images_image_date: "2007-11-28T18:08:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12857 -->
