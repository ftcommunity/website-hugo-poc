---
layout: "image"
title: "Modell von Joachim und Frederik"
date: "2007-11-29T19:57:30"
picture: "industriemodell07.jpg"
weight: "9"
konstrukteure: 
- "Joachim und Frederik"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12926
- /detailsf3c2.html
imported:
- "2019"
_4images_image_id: "12926"
_4images_cat_id: "1169"
_4images_user_id: "453"
_4images_image_date: "2007-11-29T19:57:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12926 -->
