---
layout: "image"
title: "Industriemaschine"
date: "2007-11-29T17:35:20"
picture: "olli31.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12904
- /detailsdbf0.html
imported:
- "2019"
_4images_image_id: "12904"
_4images_cat_id: "1169"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12904 -->
Von Fredy und Joachim