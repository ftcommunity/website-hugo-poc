---
layout: "image"
title: "Modell von Joachim und Frederik"
date: "2007-11-29T19:57:49"
picture: "industriemodell13.jpg"
weight: "15"
konstrukteure: 
- "Joachim und Frederik"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12932
- /detailsad86.html
imported:
- "2019"
_4images_image_id: "12932"
_4images_cat_id: "1169"
_4images_user_id: "453"
_4images_image_date: "2007-11-29T19:57:49"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12932 -->
