---
layout: "image"
title: "Detailansicht Mast"
date: "2007-11-29T19:56:16"
picture: "puetter4.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/12918
- /details6ffc.html
imported:
- "2019"
_4images_image_id: "12918"
_4images_cat_id: "1166"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:56:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12918 -->
In der Mitte vom Mast sind die Leitungen zu sehen welche zu den sensoren gehen, welche für die positionierung des Gegengewichts zuständig sind