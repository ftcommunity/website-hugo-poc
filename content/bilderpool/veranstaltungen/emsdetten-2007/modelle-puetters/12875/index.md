---
layout: "image"
title: "Kran"
date: "2007-11-29T17:35:20"
picture: "olli02.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12875
- /details4f2c.html
imported:
- "2019"
_4images_image_id: "12875"
_4images_cat_id: "1166"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12875 -->
Der Kran den man selber steuern konnte