---
layout: "image"
title: "Gesamtansicht"
date: "2007-11-29T19:56:16"
picture: "puetter1.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/12915
- /details3ef8.html
imported:
- "2019"
_4images_image_id: "12915"
_4images_cat_id: "1166"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:56:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12915 -->
Der kran in seiner Vollen Pracht