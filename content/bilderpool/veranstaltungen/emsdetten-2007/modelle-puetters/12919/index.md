---
layout: "image"
title: "Detailansicht Magnet"
date: "2007-11-29T19:56:16"
picture: "puetter5.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/12919
- /detailsd9be.html
imported:
- "2019"
_4images_image_id: "12919"
_4images_cat_id: "1166"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:56:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12919 -->
Magnet mit Last