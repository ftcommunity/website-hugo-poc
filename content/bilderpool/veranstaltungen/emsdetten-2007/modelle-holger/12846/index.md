---
layout: "image"
title: "Flugzeug Modell"
date: "2007-11-26T16:28:11"
picture: "modellevonholger4.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12846
- /details21d3.html
imported:
- "2019"
_4images_image_id: "12846"
_4images_cat_id: "1162"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12846 -->
