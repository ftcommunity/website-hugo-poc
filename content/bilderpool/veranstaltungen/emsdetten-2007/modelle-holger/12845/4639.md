---
layout: "comment"
hidden: true
title: "4639"
date: "2007-11-27T21:42:05"
uploadBy:
- "fishfriend"
license: "unknown"
imported:
- "2019"
---
Nein, das geht nicht. Die ist viel zu schwer und müßte durch einen Selbstbau aus Pappe oder leichtem Platik ersetzt werden.
Ebenso die Lenkung. Was aber hier auf dem Foto nicht zu sehen ist, das die Lenkausschläge und die Höhe die die Schaufel erreichen fast dem Original entsprechen.
Gruß
Holger Howey