---
layout: "image"
title: "Flugzeug"
date: "2007-11-29T17:35:21"
picture: "olli33.jpg"
weight: "6"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12906
- /details6219.html
imported:
- "2019"
_4images_image_id: "12906"
_4images_cat_id: "1162"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:21"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12906 -->
In 16 Bidlern
