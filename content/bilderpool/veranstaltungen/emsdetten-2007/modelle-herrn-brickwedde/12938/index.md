---
layout: "image"
title: "Riesenrad"
date: "2007-11-30T12:30:19"
picture: "brickwedde3.jpg"
weight: "14"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12938
- /details32e7.html
imported:
- "2019"
_4images_image_id: "12938"
_4images_cat_id: "1167"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:30:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12938 -->
