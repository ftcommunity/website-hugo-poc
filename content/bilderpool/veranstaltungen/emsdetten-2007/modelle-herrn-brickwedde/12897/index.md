---
layout: "image"
title: "Detail (Riesenrad)"
date: "2007-11-29T17:35:20"
picture: "olli24.jpg"
weight: "9"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12897
- /detailsb1ed.html
imported:
- "2019"
_4images_image_id: "12897"
_4images_cat_id: "1167"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12897 -->
Aufhängung der Gondeln(seeeeehr clever gemacht!!!).
