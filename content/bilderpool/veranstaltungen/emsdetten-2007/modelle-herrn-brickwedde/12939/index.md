---
layout: "image"
title: "Trainings-Roboter"
date: "2007-11-30T12:30:19"
picture: "brickwedde4.jpg"
weight: "15"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12939
- /details28d8.html
imported:
- "2019"
_4images_image_id: "12939"
_4images_cat_id: "1167"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:30:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12939 -->
