---
layout: "image"
title: "Detail"
date: "2007-11-29T17:35:20"
picture: "olli20.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12893
- /detailsb4bf.html
imported:
- "2019"
_4images_image_id: "12893"
_4images_cat_id: "1167"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12893 -->
sehr viel Eigenbau.