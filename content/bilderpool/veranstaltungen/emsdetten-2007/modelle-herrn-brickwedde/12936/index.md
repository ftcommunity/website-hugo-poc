---
layout: "image"
title: "Spielbrett"
date: "2007-11-30T12:30:19"
picture: "brickwedde1.jpg"
weight: "12"
konstrukteure: 
- "Familie Brickwedde"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12936
- /detailsa045.html
imported:
- "2019"
_4images_image_id: "12936"
_4images_cat_id: "1167"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:30:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12936 -->
Auf diesem Modell konnten die Kinder mit Kränen und Autos spielen, die Modelle waren Motorisiert.
