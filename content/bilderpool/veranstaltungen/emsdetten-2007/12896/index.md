---
layout: "image"
title: "kleine Raupe"
date: "2007-11-29T17:35:20"
picture: "olli23.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Oliver Kötter"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- /php/details/12896
- /detailsa929.html
imported:
- "2019"
_4images_image_id: "12896"
_4images_cat_id: "1158"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12896 -->
Auf einem Tieflader
