---
layout: "image"
title: "Kirmesmodell"
date: "2007-11-30T12:25:13"
picture: "kirmesmodelle1.jpg"
weight: "6"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/12934
- /details9e9d.html
imported:
- "2019"
_4images_image_id: "12934"
_4images_cat_id: "1170"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:25:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12934 -->
