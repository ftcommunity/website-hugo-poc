---
layout: "image"
title: "Kirmesmodell 3"
date: "2007-11-29T19:38:10"
picture: "puetter3.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- /php/details/12912
- /details3e66.html
imported:
- "2019"
_4images_image_id: "12912"
_4images_cat_id: "1170"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:38:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12912 -->
