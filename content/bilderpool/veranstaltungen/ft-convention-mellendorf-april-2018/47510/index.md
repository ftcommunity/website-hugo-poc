---
layout: "image"
title: "ftconventionapril039.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril039.jpg"
weight: "39"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47510
- /details0a4d-2.html
imported:
- "2019"
_4images_image_id: "47510"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47510 -->
