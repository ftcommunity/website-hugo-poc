---
layout: "image"
title: "ftconventionapril021.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril021.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47492
- /detailscd92.html
imported:
- "2019"
_4images_image_id: "47492"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47492 -->
