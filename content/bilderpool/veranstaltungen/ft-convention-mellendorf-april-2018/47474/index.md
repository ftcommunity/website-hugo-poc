---
layout: "image"
title: "ftconventionapril003.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril003.jpg"
weight: "3"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47474
- /details8e3a.html
imported:
- "2019"
_4images_image_id: "47474"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47474 -->
