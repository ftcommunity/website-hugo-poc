---
layout: "image"
title: "ftconventionapril027.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril027.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47498
- /details04c1.html
imported:
- "2019"
_4images_image_id: "47498"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47498 -->
