---
layout: "image"
title: "ftconventionapril057.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril057.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47528
- /detailsd89e.html
imported:
- "2019"
_4images_image_id: "47528"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47528 -->
