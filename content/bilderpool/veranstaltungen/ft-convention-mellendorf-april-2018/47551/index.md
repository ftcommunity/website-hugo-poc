---
layout: "image"
title: "Atarie with the old FT-logic"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril080.jpg"
weight: "80"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47551
- /details3fcd-2.html
imported:
- "2019"
_4images_image_id: "47551"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47551 -->
