---
layout: "image"
title: "ftconventionapril078.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril078.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47549
- /details65ea-3.html
imported:
- "2019"
_4images_image_id: "47549"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47549 -->
