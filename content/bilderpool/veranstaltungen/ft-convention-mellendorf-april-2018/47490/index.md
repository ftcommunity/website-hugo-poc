---
layout: "image"
title: "ftconventionapril019.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril019.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47490
- /details1f2b.html
imported:
- "2019"
_4images_image_id: "47490"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47490 -->
