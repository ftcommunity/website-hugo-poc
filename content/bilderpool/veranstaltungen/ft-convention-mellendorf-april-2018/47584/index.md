---
layout: "image"
title: "Harzer Bergbahn"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion04.jpg"
weight: "106"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Karina Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/47584
- /details5cc7.html
imported:
- "2019"
_4images_image_id: "47584"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47584 -->
Klasse Modell von Raif Geerken. Genau wie das Original.
