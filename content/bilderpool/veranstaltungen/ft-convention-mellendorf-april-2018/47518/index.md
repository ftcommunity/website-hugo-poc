---
layout: "image"
title: "Almost empty hall."
date: "2018-05-01T11:06:36"
picture: "ftconventionapril047.jpg"
weight: "47"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47518
- /details4958-3.html
imported:
- "2019"
_4images_image_id: "47518"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47518 -->
Friday evening