---
layout: "image"
title: "ftconventionapril040.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril040.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- /php/details/47511
- /details46d6.html
imported:
- "2019"
_4images_image_id: "47511"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47511 -->
Friday evening, the base has arrived.