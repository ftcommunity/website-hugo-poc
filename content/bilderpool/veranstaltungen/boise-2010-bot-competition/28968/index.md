---
layout: "image"
title: "Boise Bot Competion 2010"
date: "2010-10-10T12:29:55"
picture: "sm_bot4.jpg"
weight: "4"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
schlagworte: ["Boise", "Bot", "Competion", "2010", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- /php/details/28968
- /details6ed8.html
imported:
- "2019"
_4images_image_id: "28968"
_4images_cat_id: "2105"
_4images_user_id: "585"
_4images_image_date: "2010-10-10T12:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28968 -->
These are images fromthe Boise Bot Competion 2010.