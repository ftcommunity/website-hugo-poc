---
layout: "comment"
hidden: true
title: "12490"
date: "2010-10-11T06:45:50"
uploadBy:
- "ft_idaho"
license: "unknown"
imported:
- "2019"
---
Rob=> None of the LEGO bots defeated the ft bot! We lost only to the homemade bot using a Parallax board. (We won the first heat, stalemated the second, and then lost the next two). 
Paul =>Thanks for sharing! Yes, the Discovery Center used relaxed rules. We could have built to a smaller footprint, but 15 cm was fun!