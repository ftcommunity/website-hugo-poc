---
layout: "image"
title: "Apeldoorn 04"
date: "2006-05-31T20:08:25"
picture: "Apeldoorn_004.jpg"
weight: "1"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/6296
- /detailsb0eb.html
imported:
- "2019"
_4images_image_id: "6296"
_4images_cat_id: "552"
_4images_user_id: "10"
_4images_image_date: "2006-05-31T20:08:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6296 -->
