---
layout: "image"
title: "Orgelspelende Robot4"
date: "2006-06-01T21:54:51"
picture: "orgel3.jpg"
weight: "3"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Rob van Baal"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6360
- /details16ca.html
imported:
- "2019"
_4images_image_id: "6360"
_4images_cat_id: "555"
_4images_user_id: "5"
_4images_image_date: "2006-06-01T21:54:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6360 -->
Esther Bakker en Marcel Bosch met de Orgelspelende robot