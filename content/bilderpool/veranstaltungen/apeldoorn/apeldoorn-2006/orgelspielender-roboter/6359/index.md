---
layout: "image"
title: "DSC00859"
date: "2006-06-01T21:54:51"
picture: "orgel2.jpg"
weight: "2"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Rob van Baal"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/6359
- /detailsb520.html
imported:
- "2019"
_4images_image_id: "6359"
_4images_cat_id: "555"
_4images_user_id: "5"
_4images_image_date: "2006-06-01T21:54:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6359 -->
Marcel Bosch, Arthur Fischer en Clemens Jansen met de Orgelspelende robot