---
layout: "image"
title: "Richard Budding & Zoon"
date: "2009-02-28T19:51:04"
picture: "2009-Febr-FT-Apeldoorn_013.jpg"
weight: "13"
konstrukteure: 
- "Richard Budding & Zoon"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23255
- /detailsac1b.html
imported:
- "2019"
_4images_image_id: "23255"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23255 -->
