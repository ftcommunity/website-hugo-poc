---
layout: "image"
title: "Fam. Dijkstra - Fokker 50 vliegtuigmodel"
date: "2009-02-28T19:51:03"
picture: "2009-Febr-FT-Apeldoorn_011.jpg"
weight: "11"
konstrukteure: 
- "Fam. Dijkstra - Fokker 50 vliegtuigmodel"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23253
- /details0bed.html
imported:
- "2019"
_4images_image_id: "23253"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23253 -->
