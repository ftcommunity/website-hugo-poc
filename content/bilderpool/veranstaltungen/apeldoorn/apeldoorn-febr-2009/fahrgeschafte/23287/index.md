---
layout: "image"
title: "Clemens Jansen, “Around the World”"
date: "2009-02-28T22:34:42"
picture: "FT-Schoonhoven-2008_028.jpg"
weight: "9"
konstrukteure: 
- "Clemens Jansen, “Around the World”"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23287
- /details1326.html
imported:
- "2019"
_4images_image_id: "23287"
_4images_cat_id: "2012"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T22:34:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23287 -->
