---
layout: "image"
title: "Heftruck"
date: "2009-02-28T22:34:42"
picture: "2009-Febr-FT-Apeldoorn_053.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23283
- /details2f7b.html
imported:
- "2019"
_4images_image_id: "23283"
_4images_cat_id: "2013"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T22:34:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23283 -->
