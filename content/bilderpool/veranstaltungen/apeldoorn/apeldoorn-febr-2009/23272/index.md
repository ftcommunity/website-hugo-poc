---
layout: "image"
title: "Erfinder Andreas Tacke & Sohn"
date: "2009-02-28T19:51:04"
picture: "2009-Febr-FT-Apeldoorn_038.jpg"
weight: "22"
konstrukteure: 
- "Erfinder Andreas Tacke & Sohn"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/23272
- /details71b9.html
imported:
- "2019"
_4images_image_id: "23272"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T19:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23272 -->
