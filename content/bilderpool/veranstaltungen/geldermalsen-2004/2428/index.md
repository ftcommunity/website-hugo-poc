---
layout: "image"
title: "gottwald"
date: "2004-05-14T13:42:05"
picture: "geldermalsengotwald.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2428
- /detailsffdc.html
imported:
- "2019"
_4images_image_id: "2428"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-05-14T13:42:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2428 -->
Nochmals der Gottwald mit eine von Andrie Tieleman hergestelte ft-fahne.