---
layout: "image"
title: "liebherr lr11200_2"
date: "2004-04-29T21:13:52"
picture: "liebherr_lr11200_2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2415
- /detailsb445.html
imported:
- "2019"
_4images_image_id: "2415"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2415 -->
Liebherr  LR11200: ausleger 4,2 meter.