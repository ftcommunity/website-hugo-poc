---
layout: "image"
title: "gotwald ak912_1"
date: "2004-04-29T21:13:52"
picture: "gotwald_ak912_1.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2413
- /detailsf0b4.html
imported:
- "2019"
_4images_image_id: "2413"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2413 -->
Anton Jansen hat der von Kibri gebotenen baukasten der Gottwald AK 912 gesehen, fand der maßsatb 1:87 zu klein und hat deshalb diesen kran mit ft aufgebaut: ausleger länge über 3 meter.