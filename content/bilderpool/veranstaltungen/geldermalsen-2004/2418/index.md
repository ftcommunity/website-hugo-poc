---
layout: "image"
title: "manitowoc m21000_3"
date: "2004-04-29T21:13:52"
picture: "manitowoc_m21000_3.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/2418
- /detailsb352.html
imported:
- "2019"
_4images_image_id: "2418"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2418 -->
Der Manitowoc nochmals, aber jetst von der seite.
Hier sind der obenwagen und unterwagen besser zu sehen.