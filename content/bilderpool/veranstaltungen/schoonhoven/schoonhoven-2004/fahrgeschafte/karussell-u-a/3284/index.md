---
layout: "image"
title: "frank - 63"
date: "2004-11-18T17:20:27"
picture: "frank - 63.jpg"
weight: "8"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "-?-"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/3284
- /detailsd215.html
imported:
- "2019"
_4images_image_id: "3284"
_4images_cat_id: "306"
_4images_user_id: "9"
_4images_image_date: "2004-11-18T17:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3284 -->
