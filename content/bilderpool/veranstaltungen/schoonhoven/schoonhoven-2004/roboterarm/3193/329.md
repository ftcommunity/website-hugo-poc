---
layout: "comment"
hidden: true
title: "329"
date: "2004-11-16T15:16:15"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Das FahrgestellKeine interessanten Änderungen seit über einem Jahr - der Roboterarm hat alle Entwicklungskapazitäten in Anspruch genommen. Es wurden lediglich die dauernd abscherenden Kunststoffrastachsen gegen Eigenkonstruktionen aus Vollmessing ausgetauscht. Bei Fahrversuchen stellten sich noch zwei Schwachpunkte heraus:

1. Die IR-Entfernungssensoren sind zu langsam, um bei Höchstgeschwindigkeit des Fahrwerks noch sicher alle Hindernisse zu erkennen. Eventuell muss die Sensoranordnung noch einmal geändert oder die Fahrgeschwindigkeit reduziert oder zusätzliche Sensoren angebracht werden - oder alles zusammen.

2. Bei Kurvenfahrten neigen die Ketten zu Auflösungserscheinungen, besonders wenn der schwere Roboterarm chauffiert wird. Die alten Rastraupenbeläge halten leider auch nicht viel besser. Zur Zeit fehlt mir noch eine Idee, wie das Problem in den Griff zu bekommen ist.