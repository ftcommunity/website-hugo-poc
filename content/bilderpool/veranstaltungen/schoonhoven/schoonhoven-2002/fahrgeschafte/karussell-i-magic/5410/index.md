---
layout: "image"
title: "Magic602.JPG"
date: "2005-11-28T17:15:40"
picture: "Magic602.JPG"
weight: "23"
konstrukteure: 
- "Fa. Huss"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5410
- /details1ef7.html
imported:
- "2019"
_4images_image_id: "5410"
_4images_cat_id: "79"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T17:15:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5410 -->
Ein 'Magic' der Fa. Huss dürfte hier Vorbild gestanden haben. 

(München, Oktoberfest 2004).
