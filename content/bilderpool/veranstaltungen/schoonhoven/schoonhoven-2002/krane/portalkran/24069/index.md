---
layout: "image"
title: "Portalkran"
date: "2009-05-21T22:29:48"
picture: "Krane_1_007.jpg"
weight: "19"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- /php/details/24069
- /detailsea5c.html
imported:
- "2019"
_4images_image_id: "24069"
_4images_cat_id: "84"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24069 -->
