---
layout: "image"
title: "DCP 2479"
date: "2003-04-27T13:07:39"
picture: "DCP_2479.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/790
- /detailsef64.html
imported:
- "2019"
_4images_image_id: "790"
_4images_cat_id: "85"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T13:07:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=790 -->
