---
layout: "image"
title: "Herman Mels"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven05.jpg"
weight: "5"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25696
- /details65cd.html
imported:
- "2019"
_4images_image_id: "25696"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25696 -->
Bearbeitungsstation
