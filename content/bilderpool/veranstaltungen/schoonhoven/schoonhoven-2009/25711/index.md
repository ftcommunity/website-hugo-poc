---
layout: "image"
title: "Rob van Oostenbrugge"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25711
- /details9cb3.html
imported:
- "2019"
_4images_image_id: "25711"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25711 -->
FT-Heftruck   (Forklift)  -Detail