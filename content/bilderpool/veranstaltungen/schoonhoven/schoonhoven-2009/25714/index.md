---
layout: "image"
title: "Rob van Oostenbrugge"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25714
- /details92f9-3.html
imported:
- "2019"
_4images_image_id: "25714"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25714 -->
FT-Heftruck   (Forklift)