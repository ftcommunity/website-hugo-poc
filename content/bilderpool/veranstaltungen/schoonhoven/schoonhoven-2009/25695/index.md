---
layout: "image"
title: "Herman Mels"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25695
- /detailsd395.html
imported:
- "2019"
_4images_image_id: "25695"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25695 -->
Bearbeitungsstation