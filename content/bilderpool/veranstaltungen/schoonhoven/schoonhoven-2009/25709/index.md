---
layout: "image"
title: "Rob van Oostenbrugge"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25709
- /detailsb8a5.html
imported:
- "2019"
_4images_image_id: "25709"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25709 -->
FT-Modellen