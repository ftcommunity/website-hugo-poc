---
layout: "image"
title: "Andreas Tacke"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven17.jpg"
weight: "17"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25708
- /detailsba65.html
imported:
- "2019"
_4images_image_id: "25708"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25708 -->
Neue Entwicklungen
