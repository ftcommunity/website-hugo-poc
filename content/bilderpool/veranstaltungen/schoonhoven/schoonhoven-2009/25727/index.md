---
layout: "image"
title: "Freude für Kinder + Eltern"
date: "2009-11-08T13:54:27"
picture: "schoonhoven_2009_083.jpg"
weight: "32"
konstrukteure: 
- "FT-Freunden"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25727
- /detailsb1bf.html
imported:
- "2019"
_4images_image_id: "25727"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-08T13:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25727 -->
