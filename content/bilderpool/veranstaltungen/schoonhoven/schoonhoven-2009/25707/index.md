---
layout: "image"
title: "David van Krimpen"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25707
- /detailsed81.html
imported:
- "2019"
_4images_image_id: "25707"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25707 -->
FT-Modellen