---
layout: "image"
title: "Carel van Leeuwen"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven28.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25719
- /detailsdab8.html
imported:
- "2019"
_4images_image_id: "25719"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25719 -->
Entwicklung Robo-Interface-Kommunikation