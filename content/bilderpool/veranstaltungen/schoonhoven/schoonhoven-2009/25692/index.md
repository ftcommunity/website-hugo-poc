---
layout: "image"
title: "Herman Mels"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25692
- /detailsf2f1.html
imported:
- "2019"
_4images_image_id: "25692"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25692 -->
Bearbeitungsstation