---
layout: "image"
title: "Jan Willem Dekker"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25700
- /details4951.html
imported:
- "2019"
_4images_image_id: "25700"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25700 -->
Kermis Attraktion