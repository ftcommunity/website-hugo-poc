---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:23"
picture: "fischertechnikbijeenkomstschoonhovennov84.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29190
- /details8f35-2.html
imported:
- "2019"
_4images_image_id: "29190"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:23"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29190 -->
FT-Treffen-Schoonhoven-Thema