---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov73.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29179
- /detailsd100.html
imported:
- "2019"
_4images_image_id: "29179"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29179 -->
FT-Treffen-Schoonhoven-Thema