---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29110
- /details950f.html
imported:
- "2019"
_4images_image_id: "29110"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29110 -->
Peter Krijnen + Anton Janssen