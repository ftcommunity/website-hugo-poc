---
layout: "image"
title: "Herman Mels"
date: "2010-11-06T23:39:09"
picture: "fischertechnikbijeenkomstschoonhovennov19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29125
- /details006e.html
imported:
- "2019"
_4images_image_id: "29125"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:09"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29125 -->
Herman Mels