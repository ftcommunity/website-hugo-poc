---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:39:09"
picture: "fischertechnikbijeenkomstschoonhovennov12.jpg"
weight: "12"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29118
- /details4c58.html
imported:
- "2019"
_4images_image_id: "29118"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29118 -->
Peter Krijnen + Anton Janssen
