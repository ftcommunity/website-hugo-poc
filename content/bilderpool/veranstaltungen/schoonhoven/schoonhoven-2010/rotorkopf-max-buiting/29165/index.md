---
layout: "image"
title: "Max Buiting"
date: "2010-11-06T23:39:56"
picture: "fischertechnikbijeenkomstschoonhovennov59.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29165
- /detailsfda6-3.html
imported:
- "2019"
_4images_image_id: "29165"
_4images_cat_id: "2344"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:56"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29165 -->
Max Buiting