---
layout: "image"
title: "Herman Mels"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov27.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29133
- /details8d34.html
imported:
- "2019"
_4images_image_id: "29133"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29133 -->
Herman Mels