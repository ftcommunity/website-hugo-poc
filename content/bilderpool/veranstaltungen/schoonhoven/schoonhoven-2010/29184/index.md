---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov78.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29184
- /details7954.html
imported:
- "2019"
_4images_image_id: "29184"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29184 -->
FT-Treffen-Schoonhoven-Thema