---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:27"
picture: "fischertechnikbijeenkomstschoonhovennov33.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29139
- /details337f.html
imported:
- "2019"
_4images_image_id: "29139"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:27"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29139 -->
Kubus Rob Tovenaar