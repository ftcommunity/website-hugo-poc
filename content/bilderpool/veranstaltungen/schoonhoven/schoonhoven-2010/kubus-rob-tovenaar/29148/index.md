---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:45"
picture: "fischertechnikbijeenkomstschoonhovennov42.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29148
- /details0553.html
imported:
- "2019"
_4images_image_id: "29148"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:45"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29148 -->
Kubus Rob Tovenaar