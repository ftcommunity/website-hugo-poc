---
layout: "image"
title: "3D-Drucker Paul Niekerk"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov01.jpg"
weight: "1"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "-?-"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29107
- /details3e29.html
imported:
- "2019"
_4images_image_id: "29107"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29107 -->
3D-Drucker Paul Niekerk
