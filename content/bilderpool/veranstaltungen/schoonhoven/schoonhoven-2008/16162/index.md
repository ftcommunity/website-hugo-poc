---
layout: "image"
title: "Marcel Bosch   Treinen"
date: "2008-11-01T22:33:45"
picture: "FT-Schoonhoven-2008_084.jpg"
weight: "66"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/16162
- /details9e7b.html
imported:
- "2019"
_4images_image_id: "16162"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16162 -->
