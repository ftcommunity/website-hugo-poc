---
layout: "image"
title: "T-rex the Roborama robot"
date: "2008-12-12T22:54:12"
picture: "IMG_1098.jpg"
weight: "72"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
schlagworte: ["Roborama", "robot", "competition", "sharp"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/16587
- /details63b9.html
imported:
- "2019"
_4images_image_id: "16587"
_4images_cat_id: "1460"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16587 -->
T-rex was designed to compete in the Roborama competition of the Dutch HCC Robotica users group and the Belgian Robot mc group. It has two Ultrasound distance sensors, three infrared Sharp distance sensors, and two line sensors. The line sensors are folded under the robot when not used for line following. Here it is shown without the gripper and the bag to hold the collected cans. One week later (november 8) this robot won the 1st place in the overall category.
