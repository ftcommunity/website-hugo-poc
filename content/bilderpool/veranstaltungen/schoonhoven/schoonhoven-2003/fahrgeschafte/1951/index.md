---
layout: "image"
title: "Teppich01.JPG"
date: "2003-11-11T13:32:44"
picture: "Teppich01.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1951
- /detailsf5d5.html
imported:
- "2019"
_4images_image_id: "1951"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T13:32:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1951 -->
"Der fliegende Teppich", hier von hinten.