---
layout: "comment"
hidden: true
title: "9457"
date: "2009-06-19T14:13:25"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hey, da sind ja sogar noch die Ur-Versionen des h4G Grundbausteins (noch mit dem großen Drehknopf) und des h4ML (mit dem Loch anstatt Sieb über dem Piezoelement) verbaut!

Gruß,
Stefan