---
layout: "image"
title: "Panzer03.JPG"
date: "2003-11-10T21:00:25"
picture: "Panzer03.jpg"
weight: "3"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1937
- /detailsb29c.html
imported:
- "2019"
_4images_image_id: "1937"
_4images_cat_id: "415"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1937 -->
