---
layout: "image"
title: "S-Bagg01.JPG"
date: "2003-11-10T21:00:41"
picture: "S-Bagg01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1943
- /details37b7.html
imported:
- "2019"
_4images_image_id: "1943"
_4images_cat_id: "413"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1943 -->
Die Fahrgestelle sind alle lenkbar!