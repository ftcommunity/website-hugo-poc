---
layout: "comment"
hidden: true
title: "11092"
date: "2010-03-01T17:27:49"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich habe heute mal die Augen aufgesperrt und voilà - das Gummiband gleich im ersten Anlauf in einem Gardinen- und Stoffe-Laden gefunden. Meterware, in verschiedenen Breiten, darunter 30 und 40 mm, in schwarz oder weiß; das 30 mm breite kostet 2,40 Euro per Meter.

Gruß,
Harald