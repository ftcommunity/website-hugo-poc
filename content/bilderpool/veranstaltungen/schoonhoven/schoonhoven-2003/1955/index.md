---
layout: "image"
title: "Uhr02.JPG"
date: "2003-11-11T13:32:44"
picture: "Uhr02.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1955
- /detailsf78a.html
imported:
- "2019"
_4images_image_id: "1955"
_4images_cat_id: "202"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T13:32:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1955 -->
Der Antrieb (mit Wechselspannung, 50 Hz)