---
layout: "image"
title: "4"
date: "2007-11-25T15:18:05"
picture: "autofabrikvonralph4.jpg"
weight: "4"
konstrukteure: 
- "Ralph Roetman"
fotografen:
- "Ralph Roetman"
uploadBy: "ralph_ft"
license: "unknown"
legacy_id:
- /php/details/12832
- /details1573.html
imported:
- "2019"
_4images_image_id: "12832"
_4images_cat_id: "1157"
_4images_user_id: "575"
_4images_image_date: "2007-11-25T15:18:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12832 -->
