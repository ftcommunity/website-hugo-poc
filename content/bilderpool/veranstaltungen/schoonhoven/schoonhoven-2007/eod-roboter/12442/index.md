---
layout: "image"
title: "Screenshot Bedienfeld"
date: "2007-11-04T20:22:10"
picture: "minensuchroboter1.jpg"
weight: "1"
konstrukteure: 
- "Andries Tieleman"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12442
- /details91b2.html
imported:
- "2019"
_4images_image_id: "12442"
_4images_cat_id: "1114"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12442 -->
