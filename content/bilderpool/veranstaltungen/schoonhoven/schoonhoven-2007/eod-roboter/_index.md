---
layout: "overview"
title: "EOD-Roboter"
date: 2020-02-22T08:58:06+01:00
legacy_id:
- /php/categories/1114
- /categories303b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1114 --> 
EOD steht für "Explosive Ordnance Disposal" -- sprich, Entschärfung von Sprengkörpern. Das kann mit verschiedenen Methoden geschehen: Wegbringen, Aufbohren, Zersägen, Aufsprengen (mit Wasser), zur Not auch Beschießen.