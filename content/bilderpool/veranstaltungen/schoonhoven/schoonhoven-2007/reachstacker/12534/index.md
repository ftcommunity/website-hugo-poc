---
layout: "image"
title: "Reachstacker13.JPG"
date: "2007-11-08T18:52:50"
picture: "Reachstacker13.JPG"
weight: "6"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12534
- /detailscbbb.html
imported:
- "2019"
_4images_image_id: "12534"
_4images_cat_id: "1115"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T18:52:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12534 -->
Details der Lenkung. Rechts außerhalb befindet sich der Powermot zum Antrieb. Das Lenkgestänge gibt es zweimal: oberhalb und unterhalb vom zentralen Träger aus 4 Alus. Da wurde durchaus mit Blick auf größere Kräfte gebaut.
