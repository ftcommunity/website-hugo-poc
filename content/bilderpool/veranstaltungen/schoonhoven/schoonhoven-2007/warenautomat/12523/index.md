---
layout: "image"
title: "Warenautomat05.JPG"
date: "2007-11-05T21:52:34"
picture: "Warenautomat05.JPG"
weight: "5"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12523
- /detailsa71e.html
imported:
- "2019"
_4images_image_id: "12523"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12523 -->
Die Magazine können einzeln herausgenommen werden.
