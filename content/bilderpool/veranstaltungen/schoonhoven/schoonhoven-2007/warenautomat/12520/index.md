---
layout: "image"
title: "Warenautomat02.JPG"
date: "2007-11-05T21:47:33"
picture: "Warenautomat02.JPG"
weight: "2"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12520
- /detailsfcc1.html
imported:
- "2019"
_4images_image_id: "12520"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12520 -->
Die Rückseite ist eher unspektakulär. Aber nur äußerlich!
