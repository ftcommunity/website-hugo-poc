---
layout: "image"
title: "Warenautomat08.JPG"
date: "2007-11-05T22:00:52"
picture: "Warenautomat08.JPG"
weight: "8"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12526
- /details00a5-3.html
imported:
- "2019"
_4images_image_id: "12526"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T22:00:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12526 -->
Die Magazinschächte sind einzeln herausnehmbar. Sie sind auf zwei ft-Achsen "aufgespießt" und mit Schnecken m1 in ihren Positionen verriegelt.
