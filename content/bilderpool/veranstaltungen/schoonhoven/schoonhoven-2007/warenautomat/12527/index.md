---
layout: "image"
title: "Warenautomat09.JPG"
date: "2007-11-05T22:02:38"
picture: "Warenautomat09.JPG"
weight: "9"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12527
- /details3b7a.html
imported:
- "2019"
_4images_image_id: "12527"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T22:02:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12527 -->
Hier ist das mittlere Magazin herausgenommen worden. Dahinter sieht man die Kontaktstifte für die Elektrik und noch weiter hinten den Auswurfschacht.
