---
layout: "image"
title: "Warenautomat04.JPG"
date: "2007-11-05T21:51:43"
picture: "Warenautomat04.JPG"
weight: "4"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12522
- /details48ab-2.html
imported:
- "2019"
_4images_image_id: "12522"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12522 -->
Blick auf den (von hinten) linken Warenschacht. Das Magazin ist eine Art Paternoster, in dem die Schokoriegel von hinten unten nach vorne unten umlaufen und bei Erreichen der untersten Position freigegeben werden und in den Auswurfschacht fallen.
