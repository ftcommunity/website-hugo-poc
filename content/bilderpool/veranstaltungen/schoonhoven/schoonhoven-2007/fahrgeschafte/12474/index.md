---
layout: "image"
title: "Kirmesmodell"
date: "2007-11-04T20:23:27"
picture: "verschiedenes13.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12474
- /details85f5.html
imported:
- "2019"
_4images_image_id: "12474"
_4images_cat_id: "1121"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12474 -->
