---
layout: "image"
title: "verschiedenes03.jpg"
date: "2007-11-04T20:23:27"
picture: "verschiedenes03.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12464
- /detailsbcd6-3.html
imported:
- "2019"
_4images_image_id: "12464"
_4images_cat_id: "1121"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12464 -->
