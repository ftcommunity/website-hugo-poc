---
layout: "image"
title: "Faltkran 2, Abstützung"
date: "2007-11-04T20:05:56"
picture: "faltkran2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12441
- /details30c6-2.html
imported:
- "2019"
_4images_image_id: "12441"
_4images_cat_id: "1111"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:05:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12441 -->
