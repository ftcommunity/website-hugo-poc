---
layout: "image"
title: "Gesamtansicht"
date: "2007-11-04T20:02:18"
picture: "bulldozer1.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12437
- /details392e.html
imported:
- "2019"
_4images_image_id: "12437"
_4images_cat_id: "1113"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12437 -->
