---
layout: "image"
title: "Gewusel"
date: "2007-11-04T20:23:27"
picture: "verschiedenes07.jpg"
weight: "8"
konstrukteure: 
- "unbekannt"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12468
- /detailsd75f.html
imported:
- "2019"
_4images_image_id: "12468"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12468 -->
Was sind das für Teile? Die lagen hinter den Schaumodellen von Herrn <s>Busch</s> Bosch.