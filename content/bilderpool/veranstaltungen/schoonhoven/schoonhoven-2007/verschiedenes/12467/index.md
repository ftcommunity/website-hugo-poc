---
layout: "image"
title: "Seerose 3"
date: "2007-11-04T20:23:27"
picture: "verschiedenes06.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12467
- /details45bd.html
imported:
- "2019"
_4images_image_id: "12467"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12467 -->
