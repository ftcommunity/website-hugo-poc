---
layout: "image"
title: "Hallsensor059.JPG"
date: "2007-11-08T19:28:17"
picture: "Hallsensor059.JPG"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12540
- /detailsddd4.html
imported:
- "2019"
_4images_image_id: "12540"
_4images_cat_id: "1117"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T19:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12540 -->
Vier Hall-Sensoren in eingebautem Zustand, davor einer mit offenem Deckel und darüber ein BS15 mit Magnet, auf den die Hallsensoren ansprechen.

Zwischen den Buchsen der Leuchtbausteine ist bei allen noch ein Loch gebohrt und da hindurch der dritte Kontakt (das Signal, neben Plus und Minus) herausgeführt und mit einer schwarzen Buchse versehen worden.
