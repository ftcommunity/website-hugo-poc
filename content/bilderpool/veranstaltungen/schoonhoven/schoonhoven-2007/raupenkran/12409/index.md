---
layout: "image"
title: "Gitterspitze"
date: "2007-11-04T19:45:04"
picture: "raupenkran17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12409
- /detailse428.html
imported:
- "2019"
_4images_image_id: "12409"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12409 -->
