---
layout: "image"
title: "Raupenfahrwerk, Treppe und Befestigung"
date: "2007-11-04T19:45:04"
picture: "raupenkran14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12406
- /details206e-3.html
imported:
- "2019"
_4images_image_id: "12406"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12406 -->
Die diagonale Verstrebung des Fahrwerks mit einem Aluprofil finde ich sehr interessant.