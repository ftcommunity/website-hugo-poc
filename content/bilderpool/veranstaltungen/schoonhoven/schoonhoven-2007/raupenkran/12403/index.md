---
layout: "image"
title: "Sockel und Drehkranz"
date: "2007-11-04T19:45:04"
picture: "raupenkran11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12403
- /details112c.html
imported:
- "2019"
_4images_image_id: "12403"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12403 -->
