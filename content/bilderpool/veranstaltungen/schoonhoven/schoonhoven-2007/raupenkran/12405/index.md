---
layout: "image"
title: "Konstrukteur und Steuerung"
date: "2007-11-04T19:45:04"
picture: "raupenkran13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12405
- /detailsd038.html
imported:
- "2019"
_4images_image_id: "12405"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12405 -->
