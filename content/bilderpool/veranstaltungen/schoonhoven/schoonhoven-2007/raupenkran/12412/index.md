---
layout: "image"
title: "Teleskopausleger"
date: "2007-11-04T19:45:05"
picture: "raupenkran20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12412
- /detailsae8b-2.html
imported:
- "2019"
_4images_image_id: "12412"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:05"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12412 -->
