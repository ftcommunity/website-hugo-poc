---
layout: "image"
title: "Gegengewicht"
date: "2007-11-04T19:45:04"
picture: "raupenkran07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12399
- /details5a69.html
imported:
- "2019"
_4images_image_id: "12399"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12399 -->
Superlift?