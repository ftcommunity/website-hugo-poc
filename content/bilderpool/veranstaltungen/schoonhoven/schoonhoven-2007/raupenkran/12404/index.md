---
layout: "image"
title: "Totalansicht"
date: "2007-11-04T19:45:04"
picture: "raupenkran12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12404
- /details3694-2.html
imported:
- "2019"
_4images_image_id: "12404"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12404 -->
