---
layout: "image"
title: "Schieflage"
date: "2007-11-04T19:45:04"
picture: "raupenkran16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12408
- /detailsf213.html
imported:
- "2019"
_4images_image_id: "12408"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12408 -->
