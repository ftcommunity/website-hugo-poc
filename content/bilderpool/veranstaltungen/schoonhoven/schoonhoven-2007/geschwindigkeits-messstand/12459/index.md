---
layout: "image"
title: "Messung 3"
date: "2007-11-04T20:23:27"
picture: "geschwindigkeitsmessstand4.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12459
- /details26ed.html
imported:
- "2019"
_4images_image_id: "12459"
_4images_cat_id: "1116"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12459 -->
