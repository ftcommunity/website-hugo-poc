---
layout: "image"
title: "Original"
date: "2007-11-04T20:02:18"
picture: "aleliftingdevice19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12433
- /detailsb031-2.html
imported:
- "2019"
_4images_image_id: "12433"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12433 -->
(ist das auf 800px noch lesbar?)