---
layout: "image"
title: "Sockel Detail 4, Drehmechanismus"
date: "2007-11-04T20:02:17"
picture: "aleliftingdevice12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/12426
- /detailsfd65.html
imported:
- "2019"
_4images_image_id: "12426"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:17"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12426 -->
Auf jeder Seite befindet sich eine Winde, die in nicht sichtbarem Tempo den Kran beiseite zieht.