---
layout: "image"
title: "Waage124.JPG"
date: "2005-11-28T19:14:16"
picture: "Waage124.JPG"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5423
- /details6cc8.html
imported:
- "2019"
_4images_image_id: "5423"
_4images_cat_id: "436"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T19:14:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5423 -->
