---
layout: "image"
title: "Schoonhoven 05"
date: "2005-11-06T19:14:54"
picture: "Schoon_05.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5230
- /details9016.html
imported:
- "2019"
_4images_image_id: "5230"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T19:14:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5230 -->
