---
layout: "image"
title: "Capriolo148.JPG"
date: "2005-11-06T21:18:57"
picture: "Capriolo148.JPG"
weight: "5"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5250
- /detailsa118.html
imported:
- "2019"
_4images_image_id: "5250"
_4images_cat_id: "439"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:18:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5250 -->
Die Gondel mit den Sitzen kann frei rotieren, solange nicht die Magnetbremse angezogen ist. Die Schulterbügel sind gerade heruntergeklappt.
