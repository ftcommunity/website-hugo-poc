---
layout: "image"
title: "Autofabrik05.JPG"
date: "2005-11-06T20:58:23"
picture: "Autofabrik05.JPG"
weight: "5"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5242
- /details75df-2.html
imported:
- "2019"
_4images_image_id: "5242"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T20:58:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5242 -->
Die vorderen Radkästen sind ausgeschnitten. Die Schneidedrähte werden pneumatisch verfahren.

Auch hier wacht eine Laser-Lichtschranke über die richtige Position.
