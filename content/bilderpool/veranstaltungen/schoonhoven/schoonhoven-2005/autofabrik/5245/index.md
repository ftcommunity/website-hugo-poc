---
layout: "image"
title: "Autofabrik08.JPG"
date: "2005-11-06T21:02:52"
picture: "Autofabrik08.JPG"
weight: "8"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5245
- /details7ba5.html
imported:
- "2019"
_4images_image_id: "5245"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:02:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5245 -->
Das Abfallteil wurde entfernt, jetzt hebt derselbe Greifer die Karosserie von der Palette, um sie dann aufs Fahrgestell aufzusetzen.
