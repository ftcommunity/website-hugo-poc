---
layout: "image"
title: "Autofabrik04.JPG"
date: "2005-11-06T20:56:14"
picture: "Autofabrik04.JPG"
weight: "4"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5241
- /detailsad53.html
imported:
- "2019"
_4images_image_id: "5241"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T20:56:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5241 -->
Die Schneidestation. Quer durchs Bild verläuft der Schneidedraht für die Karosserieform. Dahinter (an den grünen Steckern erkennbar) befinden sich die gebogenen Schneidedrähte für die Radkästen.
