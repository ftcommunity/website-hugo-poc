---
layout: "image"
title: "HopperDredge02.JPG"
date: "2005-11-10T22:01:23"
picture: "HopperDredge02.JPG"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5280
- /details22fb-2.html
imported:
- "2019"
_4images_image_id: "5280"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:01:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5280 -->
Ein Blick ins Innere. Die Zylinder betätigen die Luken im Schiffsboden.
