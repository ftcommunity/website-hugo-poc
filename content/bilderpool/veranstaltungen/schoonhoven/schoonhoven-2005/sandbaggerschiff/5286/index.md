---
layout: "image"
title: "HopperDredge12.JPG"
date: "2005-11-10T22:14:05"
picture: "HopperDredge12.JPG"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5286
- /details68ae.html
imported:
- "2019"
_4images_image_id: "5286"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:14:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5286 -->
Die Bodenluken sind alle geöffnet, aber wegen der ft-blauen Tischdecke ist das nur in Bildmitte erkennbar, wo ein Blatt Papier untergelegt wurde.
