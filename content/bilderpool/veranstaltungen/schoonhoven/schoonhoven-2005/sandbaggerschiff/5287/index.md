---
layout: "image"
title: "HopperDredge13.JPG"
date: "2005-11-10T22:15:32"
picture: "HopperDredge13.JPG"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5287
- /details6d4c.html
imported:
- "2019"
_4images_image_id: "5287"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:15:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5287 -->
Der Kran hat einen Überlastsensor: bei zu hoher Spannung im Seil wird der schwarze Minitaster betätigt.
