---
layout: "image"
title: "HopperDredge04.JPG"
date: "2005-11-10T22:06:19"
picture: "HopperDredge04.JPG"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5282
- /detailse5b6.html
imported:
- "2019"
_4images_image_id: "5282"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:06:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5282 -->
