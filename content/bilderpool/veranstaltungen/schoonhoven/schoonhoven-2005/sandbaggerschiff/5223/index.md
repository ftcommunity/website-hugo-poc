---
layout: "image"
title: "Hopper-kran"
date: "2005-11-06T11:02:55"
picture: "Schoonhoven-2005_052.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Clemens Jansen"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5223
- /details5b55.html
imported:
- "2019"
_4images_image_id: "5223"
_4images_cat_id: "453"
_4images_user_id: "22"
_4images_image_date: "2005-11-06T11:02:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5223 -->
