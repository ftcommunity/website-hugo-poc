---
layout: "image"
title: "HopperDredge06.JPG"
date: "2005-11-10T22:09:34"
picture: "HopperDredge06.JPG"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5284
- /detailsce93.html
imported:
- "2019"
_4images_image_id: "5284"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:09:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5284 -->
Davits sind ausgeschwenkt und das Knie am vorderen Ende ist hinabgelassen. Jetzt kann der Arm abgesenkt werden.
