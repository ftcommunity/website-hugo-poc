---
layout: "image"
title: "HopperDredge03.JPG"
date: "2005-11-10T22:05:21"
picture: "HopperDredge03.JPG"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
schlagworte: ["32455"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5281
- /details8a1c.html
imported:
- "2019"
_4images_image_id: "5281"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:05:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5281 -->
Der Saugarm wird per Seilwinde abgelassen, nachdem die Davits ausgeschwenkt sind.

Die Teile, die den Kran auf den Zahnstangen in der Spur halten (Bauplatte 15x15 mit Verlängerung und 90° gebogenem 'Kragen'), stammen von den "alten" elektromagnetischen P-Ventilen.
