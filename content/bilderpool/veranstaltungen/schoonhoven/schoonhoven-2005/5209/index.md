---
layout: "image"
title: "Autokran"
date: "2005-11-05T23:30:53"
picture: "PICT5082_klein.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Heiko aka wahsager"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- /php/details/5209
- /details6643.html
imported:
- "2019"
_4images_image_id: "5209"
_4images_cat_id: "436"
_4images_user_id: "127"
_4images_image_date: "2005-11-05T23:30:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5209 -->
