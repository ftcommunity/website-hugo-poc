---
layout: "image"
title: "Urkunde"
date: "2005-11-06T01:53:02"
picture: "Schoonhoven-2005_033.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/5210
- /detailsb65b.html
imported:
- "2019"
_4images_image_id: "5210"
_4images_cat_id: "436"
_4images_user_id: "22"
_4images_image_date: "2005-11-06T01:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5210 -->
