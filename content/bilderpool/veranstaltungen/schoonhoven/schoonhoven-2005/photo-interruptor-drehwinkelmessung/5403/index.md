---
layout: "image"
title: "Detail of photo interruptor"
date: "2005-11-25T11:59:22"
picture: "DSCF0037a.jpg"
weight: "3"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5403
- /details55b8.html
imported:
- "2019"
_4images_image_id: "5403"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T11:59:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5403 -->
The photo interruptor in more detail. It is mounted directly on the 38216 Plug in light holder. Since all four connections are needed, some sawing is needed.
