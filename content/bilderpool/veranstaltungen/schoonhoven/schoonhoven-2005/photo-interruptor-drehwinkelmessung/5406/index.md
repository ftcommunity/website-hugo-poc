---
layout: "image"
title: "Photo interruptor soldering detail"
date: "2005-11-25T12:08:11"
picture: "DSCF0046a.jpg"
weight: "6"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5406
- /details5643.html
imported:
- "2019"
_4images_image_id: "5406"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T12:08:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5406 -->
Here's another view on how the sensor is soldered.
