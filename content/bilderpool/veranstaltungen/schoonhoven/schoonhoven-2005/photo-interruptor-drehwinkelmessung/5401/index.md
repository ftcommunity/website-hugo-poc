---
layout: "image"
title: "Photo interruptor, no cables"
date: "2005-11-25T11:51:05"
picture: "DSCF0029a.jpg"
weight: "1"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
schlagworte: ["sensor", "photo", "interruptor", "robot"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5401
- /details312a.html
imported:
- "2019"
_4images_image_id: "5401"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T11:51:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5401 -->
Photo interruptor, as used on the TNO Robot 2005. The position is rather critical, but it is easy with ft to get the correct positioning. I did this in a hurry, and did not try to optimize, nor find a more beautiful solution.
