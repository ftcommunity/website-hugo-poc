---
layout: "image"
title: "Schoonhoven 04"
date: "2005-11-06T17:44:42"
picture: "Schoon_04.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5229
- /details5c43.html
imported:
- "2019"
_4images_image_id: "5229"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T17:44:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5229 -->
