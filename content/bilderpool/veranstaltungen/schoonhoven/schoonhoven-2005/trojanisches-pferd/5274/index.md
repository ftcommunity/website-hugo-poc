---
layout: "image"
title: "Troja02.JPG"
date: "2005-11-07T20:17:08"
picture: "Troja02.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5274
- /details5718.html
imported:
- "2019"
_4images_image_id: "5274"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:17:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5274 -->
