---
layout: "image"
title: "schoonhoven 08"
date: "2005-11-06T19:14:54"
picture: "Schoon_08.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/5233
- /detailsba6e.html
imported:
- "2019"
_4images_image_id: "5233"
_4images_cat_id: "436"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T19:14:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5233 -->
