---
layout: "image"
title: "Arc-de-Triomphe73.JPG"
date: "2005-11-28T19:09:19"
picture: "Arc-de-Triomphe73.JPG"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5419
- /details24fe.html
imported:
- "2019"
_4images_image_id: "5419"
_4images_cat_id: "436"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T19:09:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5419 -->
