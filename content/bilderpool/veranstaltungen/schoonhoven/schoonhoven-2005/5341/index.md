---
layout: "image"
title: "Omniwheels"
date: "2005-11-15T19:15:15"
picture: "Omniwheels.jpg"
weight: "12"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/5341
- /details890a-2.html
imported:
- "2019"
_4images_image_id: "5341"
_4images_cat_id: "436"
_4images_user_id: "385"
_4images_image_date: "2005-11-15T19:15:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5341 -->
With the joystick on top of this robot, you can move it in any direction! These fischertechnik omniwheels are not perfectly round, but they perform quite well.
