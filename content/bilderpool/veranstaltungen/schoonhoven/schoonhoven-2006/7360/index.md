---
layout: "image"
title: "fischertechnikschoonh44.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh44.jpg"
weight: "22"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7360
- /detailsbf60.html
imported:
- "2019"
_4images_image_id: "7360"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7360 -->
