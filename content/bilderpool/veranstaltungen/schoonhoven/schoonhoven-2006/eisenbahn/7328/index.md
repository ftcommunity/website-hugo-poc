---
layout: "image"
title: "fischertechnikschoonh12.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh12.jpg"
weight: "2"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7328
- /details7027.html
imported:
- "2019"
_4images_image_id: "7328"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7328 -->
