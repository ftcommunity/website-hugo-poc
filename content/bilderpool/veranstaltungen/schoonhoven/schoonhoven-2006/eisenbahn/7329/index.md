---
layout: "image"
title: "fischertechnikschoonh13.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh13.jpg"
weight: "3"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7329
- /details13ac.html
imported:
- "2019"
_4images_image_id: "7329"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7329 -->
