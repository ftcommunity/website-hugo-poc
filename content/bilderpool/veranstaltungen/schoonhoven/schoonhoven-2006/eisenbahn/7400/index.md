---
layout: "image"
title: "fischertechnikschoonh84.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh84.jpg"
weight: "5"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7400
- /details2701.html
imported:
- "2019"
_4images_image_id: "7400"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7400 -->
