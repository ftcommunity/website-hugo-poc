---
layout: "image"
title: "fischertechnikschoonh79.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh79.jpg"
weight: "45"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7395
- /details868f.html
imported:
- "2019"
_4images_image_id: "7395"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7395 -->
