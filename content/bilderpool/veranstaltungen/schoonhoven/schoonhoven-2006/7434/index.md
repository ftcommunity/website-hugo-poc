---
layout: "image"
title: "Weltkugel"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw24.jpg"
weight: "55"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7434
- /detailsaa34-2.html
imported:
- "2019"
_4images_image_id: "7434"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7434 -->
