---
layout: "image"
title: "fischertechnikschoonh68.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh68.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7384
- /details3b21.html
imported:
- "2019"
_4images_image_id: "7384"
_4images_cat_id: "1128"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7384 -->
