---
layout: "image"
title: "fischertechnikschoonh67.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh67.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7383
- /details0d5a.html
imported:
- "2019"
_4images_image_id: "7383"
_4images_cat_id: "1128"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7383 -->
