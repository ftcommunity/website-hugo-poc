---
layout: "image"
title: "fischertechnikschoonh46.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh46.jpg"
weight: "24"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7362
- /details21d9.html
imported:
- "2019"
_4images_image_id: "7362"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7362 -->
