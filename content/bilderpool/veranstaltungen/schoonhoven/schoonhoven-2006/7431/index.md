---
layout: "image"
title: "Diverse Modelle"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw21.jpg"
weight: "52"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7431
- /detailsad14.html
imported:
- "2019"
_4images_image_id: "7431"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7431 -->
