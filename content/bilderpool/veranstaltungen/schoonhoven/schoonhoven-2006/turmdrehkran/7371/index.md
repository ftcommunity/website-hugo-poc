---
layout: "image"
title: "fischertechnikschoonh55.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh55.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7371
- /details2bec-2.html
imported:
- "2019"
_4images_image_id: "7371"
_4images_cat_id: "1124"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7371 -->
