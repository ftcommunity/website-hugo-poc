---
layout: "image"
title: "Turmdrehkran (Aufbau)"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw05.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7415
- /details0bea.html
imported:
- "2019"
_4images_image_id: "7415"
_4images_cat_id: "1124"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7415 -->
