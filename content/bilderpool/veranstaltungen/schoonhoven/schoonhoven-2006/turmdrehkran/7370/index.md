---
layout: "image"
title: "fischertechnikschoonh54.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh54.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch (Guilligan)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7370
- /detailsb82c.html
imported:
- "2019"
_4images_image_id: "7370"
_4images_cat_id: "1124"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7370 -->
