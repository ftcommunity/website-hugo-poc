---
layout: "image"
title: "fischertechnikschoonh63.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh63.jpg"
weight: "36"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7379
- /details9f16.html
imported:
- "2019"
_4images_image_id: "7379"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7379 -->
