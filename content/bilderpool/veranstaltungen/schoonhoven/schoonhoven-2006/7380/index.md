---
layout: "image"
title: "fischertechnikschoonh64.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh64.jpg"
weight: "37"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7380
- /details9f23.html
imported:
- "2019"
_4images_image_id: "7380"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7380 -->
