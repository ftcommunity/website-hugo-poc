---
layout: "image"
title: "fischertechnikschoonh77.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh77.jpg"
weight: "43"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7393
- /detailsb0b3.html
imported:
- "2019"
_4images_image_id: "7393"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7393 -->
