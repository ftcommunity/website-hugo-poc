---
layout: "image"
title: "Festo Alternativ     Fritz Roller"
date: "2006-11-06T17:16:53"
picture: "Festo-alternatief_001.jpg"
weight: "58"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/7437
- /detailsaa71.html
imported:
- "2019"
_4images_image_id: "7437"
_4images_cat_id: "701"
_4images_user_id: "22"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7437 -->
Festo hat auch ein "standart"-Alternativ:

CN-M5-PK2  FES 19.521   (1 á 2 Euro/St)

LCN-M5-PK2   FES 19.523   (2 á 3 Euro/St)

FT-Baustein 15 mit Bohrung  32.064

Gruss,

Peter Damen
Poederoyen, Holland
