---
layout: "image"
title: "fischertechnikschoonh76.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh76.jpg"
weight: "42"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7392
- /detailsed2f.html
imported:
- "2019"
_4images_image_id: "7392"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7392 -->
