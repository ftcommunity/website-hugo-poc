---
layout: "image"
title: "fischertechnikschoonh31.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh31.jpg"
weight: "12"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7347
- /details44f3.html
imported:
- "2019"
_4images_image_id: "7347"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7347 -->
