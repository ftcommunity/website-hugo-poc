---
layout: "image"
title: "fischertechnikschoonh59.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh59.jpg"
weight: "32"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7375
- /details376f.html
imported:
- "2019"
_4images_image_id: "7375"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7375 -->
