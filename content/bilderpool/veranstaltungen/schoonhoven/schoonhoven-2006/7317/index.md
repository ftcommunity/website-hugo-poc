---
layout: "image"
title: "fischertechnikschoonh01.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh01.jpg"
weight: "1"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7317
- /details1c68.html
imported:
- "2019"
_4images_image_id: "7317"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7317 -->
