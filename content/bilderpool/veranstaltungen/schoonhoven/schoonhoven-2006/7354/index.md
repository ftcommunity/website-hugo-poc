---
layout: "image"
title: "fischertechnikschoonh38.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh38.jpg"
weight: "18"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7354
- /detailsfca3.html
imported:
- "2019"
_4images_image_id: "7354"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7354 -->
