---
layout: "image"
title: "fischertechnikschoonh04.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh04.jpg"
weight: "4"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
schlagworte: ["Speichenrad"]
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7320
- /detailscb20.html
imported:
- "2019"
_4images_image_id: "7320"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7320 -->
