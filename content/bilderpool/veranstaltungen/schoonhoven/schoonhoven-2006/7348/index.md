---
layout: "image"
title: "fischertechnikschoonh32.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh32.jpg"
weight: "13"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7348
- /details9b79-2.html
imported:
- "2019"
_4images_image_id: "7348"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7348 -->
