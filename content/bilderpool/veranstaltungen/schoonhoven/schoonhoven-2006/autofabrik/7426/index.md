---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw16.jpg"
weight: "8"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7426
- /details236d.html
imported:
- "2019"
_4images_image_id: "7426"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7426 -->
7.Station: Befestigung der Reifen