---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw15.jpg"
weight: "7"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7425
- /detailsda89-2.html
imported:
- "2019"
_4images_image_id: "7425"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7425 -->
6.Station: Reifenmontag (Ich glaube das Führerhaus fehlt hier, weil die Refeinmontage für das Fernsehfilmteam ohne Führerhaus ablief)