---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw14.jpg"
weight: "6"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7424
- /details80f0-2.html
imported:
- "2019"
_4images_image_id: "7424"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7424 -->
5.Station: Führerhaus verschweißen