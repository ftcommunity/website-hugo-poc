---
layout: "image"
title: "fischertechnikschoonh43.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh43.jpg"
weight: "21"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7359
- /detailsdf86.html
imported:
- "2019"
_4images_image_id: "7359"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7359 -->
