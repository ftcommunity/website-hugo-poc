---
layout: "image"
title: "Zählwerk"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw19.jpg"
weight: "50"
konstrukteure: 
- "Max Buiting"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7429
- /detailsf538.html
imported:
- "2019"
_4images_image_id: "7429"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7429 -->
