---
layout: "image"
title: "fischertechnikschoonh47.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh47.jpg"
weight: "25"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7363
- /details4185.html
imported:
- "2019"
_4images_image_id: "7363"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7363 -->
