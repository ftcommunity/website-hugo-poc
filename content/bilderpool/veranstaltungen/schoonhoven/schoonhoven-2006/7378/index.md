---
layout: "image"
title: "fischertechnikschoonh62.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh62.jpg"
weight: "35"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7378
- /details3762-2.html
imported:
- "2019"
_4images_image_id: "7378"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7378 -->
