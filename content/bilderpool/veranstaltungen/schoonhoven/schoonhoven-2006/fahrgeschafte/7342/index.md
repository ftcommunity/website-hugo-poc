---
layout: "image"
title: "fischertechnikschoonh26.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh26.jpg"
weight: "9"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7342
- /detailsc430.html
imported:
- "2019"
_4images_image_id: "7342"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7342 -->
