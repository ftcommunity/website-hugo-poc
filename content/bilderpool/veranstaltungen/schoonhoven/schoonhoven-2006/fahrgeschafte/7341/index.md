---
layout: "image"
title: "fischertechnikschoonh25.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh25.jpg"
weight: "8"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7341
- /details42cc-2.html
imported:
- "2019"
_4images_image_id: "7341"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7341 -->
