---
layout: "image"
title: "fischertechnikschoonh19.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh19.jpg"
weight: "6"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7335
- /details9d55-3.html
imported:
- "2019"
_4images_image_id: "7335"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7335 -->
