---
layout: "image"
title: "fischertechnikschoonh20.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh20.jpg"
weight: "7"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7336
- /detailsabdf.html
imported:
- "2019"
_4images_image_id: "7336"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7336 -->
