---
layout: "image"
title: "fischertechnikschoonh27.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh27.jpg"
weight: "10"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7343
- /detailsd10f.html
imported:
- "2019"
_4images_image_id: "7343"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7343 -->
