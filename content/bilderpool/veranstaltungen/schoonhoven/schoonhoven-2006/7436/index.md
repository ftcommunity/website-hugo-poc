---
layout: "image"
title: "Atoscooter"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw26.jpg"
weight: "57"
konstrukteure: 
- "Eric Bernhard"
fotografen:
- "MisterWho (Joachim Jacobi)"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- /php/details/7436
- /detailse505-2.html
imported:
- "2019"
_4images_image_id: "7436"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7436 -->
