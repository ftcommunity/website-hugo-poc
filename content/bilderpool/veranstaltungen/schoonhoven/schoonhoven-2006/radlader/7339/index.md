---
layout: "image"
title: "fischertechnikschoonh23.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh23.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7339
- /detailsaca9.html
imported:
- "2019"
_4images_image_id: "7339"
_4images_cat_id: "1126"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7339 -->
