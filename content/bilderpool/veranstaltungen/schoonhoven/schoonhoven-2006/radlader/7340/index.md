---
layout: "image"
title: "fischertechnikschoonh24.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh24.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- /php/details/7340
- /details5b57-2.html
imported:
- "2019"
_4images_image_id: "7340"
_4images_cat_id: "1126"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7340 -->
