---
layout: "image"
title: "makerfaire15.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41112
- /details2935-2.html
imported:
- "2019"
_4images_image_id: "41112"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41112 -->
