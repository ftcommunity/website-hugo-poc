---
layout: "image"
title: "makerfaire25.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41122
- /detailsb472.html
imported:
- "2019"
_4images_image_id: "41122"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41122 -->
