---
layout: "image"
title: "makerfaire42.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire42.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41139
- /details1e8a.html
imported:
- "2019"
_4images_image_id: "41139"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41139 -->
