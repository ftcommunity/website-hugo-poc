---
layout: "image"
title: "makerfaire006"
date: "2015-06-08T21:30:57"
picture: "makerfaire06_2.jpg"
weight: "59"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/41179
- /details8261.html
imported:
- "2019"
_4images_image_id: "41179"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41179 -->
Flipper und Bahnhofsuhr (Dirk Fox)
