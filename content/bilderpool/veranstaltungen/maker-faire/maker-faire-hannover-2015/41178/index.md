---
layout: "image"
title: "makerfaire005"
date: "2015-06-08T21:30:57"
picture: "makerfaire05_2.jpg"
weight: "58"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/41178
- /detailsa3ab.html
imported:
- "2019"
_4images_image_id: "41178"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41178 -->
