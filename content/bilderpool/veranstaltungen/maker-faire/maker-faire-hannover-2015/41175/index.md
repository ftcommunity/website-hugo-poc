---
layout: "image"
title: "makerfaire002"
date: "2015-06-08T21:30:57"
picture: "makerfaire02_2.jpg"
weight: "55"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/41175
- /details8ae2.html
imported:
- "2019"
_4images_image_id: "41175"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41175 -->
