---
layout: "image"
title: "makerfaire04.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/41101
- /details17cd.html
imported:
- "2019"
_4images_image_id: "41101"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41101 -->
