---
layout: "image"
title: "Drohne und ferngesteuerter Arduino-SUV"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse05.jpg"
weight: "5"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46276
- /detailseba3.html
imported:
- "2019"
_4images_image_id: "46276"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46276 -->
Der SUV wurde von einem Arduino Uno und einer Graupner-Fernsteuerung gesteuert.
Und die Drohne bekam die eine oder andere Flugeinheit spendiert.