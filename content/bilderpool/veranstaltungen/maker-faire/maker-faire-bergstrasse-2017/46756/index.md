---
layout: "image"
title: "Und noch mehr Fachgespräche"
date: "2017-10-02T21:00:58"
picture: "klein-0010.jpg"
weight: "29"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46756
- /details0a62.html
imported:
- "2019"
_4images_image_id: "46756"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T21:00:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46756 -->
Praktischerweise war der dpunkt-Verlag fast direkt nebenan. Der Lieferservice für Kochboxen, der eigentlich noch dazwischen gequetscht war, ist freundlicherweise schnell an einen besser sichtbaren Platz umgezogen.