---
layout: "overview"
title: "Maker Faire Bergstraße 2017"
date: 2020-02-22T09:09:19+01:00
legacy_id:
- /php/categories/3433
- /categories73f1.html
- /categories99e8.html
- /categoriesc4f7.html
- /categoriesabf1.html
- /categories9150.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3433 --> 
Vierzehn kleine (und etwas größere) fischertechnikerlein hatten sich unter der Federführung und organisierenden Hand von Esther bei der Maker Faire Bergstraße in Bensheim eingefunden - und brachten vom 16.-17.09.2017 hunderte von Kinder- (und ehemaligen Kinder-) augen zum Leuchten.