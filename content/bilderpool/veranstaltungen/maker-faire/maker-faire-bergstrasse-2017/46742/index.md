---
layout: "image"
title: "Hochregallager"
date: "2017-10-02T20:30:44"
picture: "klein-006.jpg"
weight: "15"
konstrukteure: 
- "Raphael Jacob"
fotografen:
- "Esther"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46742
- /detailsf72d.html
imported:
- "2019"
_4images_image_id: "46742"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46742 -->
Nicht das Modell eines Hochregallagers ist das Besondere, sondern die eigene Firmware-Bibliothek!
