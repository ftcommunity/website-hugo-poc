---
layout: "image"
title: "Unser Stand - kurz vor der Eröffnung..."
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse01.jpg"
weight: "1"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Dirk Fox"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- /php/details/46272
- /detailsffc6.html
imported:
- "2019"
_4images_image_id: "46272"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46272 -->
Die Ruhe vor dem Sturm... alle Modelle sind aufgebaut, getestet und warten auf die Besucher.