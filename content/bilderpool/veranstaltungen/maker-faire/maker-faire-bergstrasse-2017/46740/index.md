---
layout: "image"
title: "Flugobjekt"
date: "2017-10-02T20:30:44"
picture: "klein-008.jpg"
weight: "13"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "EstherM"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46740
- /detailsd81e.html
imported:
- "2019"
_4images_image_id: "46740"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46740 -->
