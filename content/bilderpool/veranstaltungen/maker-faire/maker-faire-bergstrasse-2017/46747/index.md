---
layout: "image"
title: "Es tobt der Mob!"
date: "2017-10-02T20:30:44"
picture: "klein-001.jpg"
weight: "20"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46747
- /detailsdf7b.html
imported:
- "2019"
_4images_image_id: "46747"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46747 -->
