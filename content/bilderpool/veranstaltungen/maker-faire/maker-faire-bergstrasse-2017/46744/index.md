---
layout: "image"
title: "Wieder das Team am Stand"
date: "2017-10-02T20:30:44"
picture: "klein-004.jpg"
weight: "17"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- /php/details/46744
- /details499a.html
imported:
- "2019"
_4images_image_id: "46744"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46744 -->
