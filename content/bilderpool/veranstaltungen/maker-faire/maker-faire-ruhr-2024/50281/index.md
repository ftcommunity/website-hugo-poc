---
layout: "image"
title: "Ankündigung der IDEA 2024"
date: 2024-03-22T16:39:59+01:00
picture: "IMG-20240317-WA0013.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---
Hier wird die IDEA 2024 ("Nordconvention") am 20.4.2024 im Schulzentrum Mellendorf angekündigt.
