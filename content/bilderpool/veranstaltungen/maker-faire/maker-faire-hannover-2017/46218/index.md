---
layout: "image"
title: "makerfairehannover7.jpg"
date: "2017-08-28T14:50:33"
picture: "makerfairehannover7.jpg"
weight: "7"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46218
- /details75af.html
imported:
- "2019"
_4images_image_id: "46218"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-28T14:50:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46218 -->
