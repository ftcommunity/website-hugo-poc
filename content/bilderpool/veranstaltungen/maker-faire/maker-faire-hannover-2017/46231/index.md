---
layout: "image"
title: "Bau Hexapod"
date: "2017-09-01T18:06:06"
picture: "makerffaire4.jpg"
weight: "19"
konstrukteure: 
- "Thingiverse"
fotografen:
- "Dirk Wölffel (DirkW)"
schlagworte: ["Hexapod"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46231
- /detailsc63e-3.html
imported:
- "2019"
_4images_image_id: "46231"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:06:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46231 -->
Zusammenbau Hexapod
