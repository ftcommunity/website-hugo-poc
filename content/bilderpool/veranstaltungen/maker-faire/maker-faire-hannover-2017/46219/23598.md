---
layout: "comment"
hidden: true
title: "23598"
date: "2017-08-29T07:17:05"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hmm, mit nur 6 Beinen ist das wohl eher eine Krabbe. Wie auch immer, vielleicht kommt sie ja auch mal in Dreieich vorbeigekrabbelt.

Dieses Jahr hattet ihr jedenfalls einen viel besser einsehbaren Platz als letzes Mal.

Gruß
H.A.R.R.Y.