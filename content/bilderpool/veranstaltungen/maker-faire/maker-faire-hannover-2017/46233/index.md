---
layout: "image"
title: "Bein Hexapod"
date: "2017-09-01T18:06:06"
picture: "makerffaire6.jpg"
weight: "21"
konstrukteure: 
- "Thingiverse"
fotografen:
- "Dirk Wölffel (DirkW)"
schlagworte: ["Hexapod"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46233
- /details82ca.html
imported:
- "2019"
_4images_image_id: "46233"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:06:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46233 -->
