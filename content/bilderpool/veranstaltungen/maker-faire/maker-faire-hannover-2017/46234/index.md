---
layout: "image"
title: "Servo Halter Hexapod"
date: "2017-09-01T18:06:06"
picture: "makerffaire7.jpg"
weight: "22"
konstrukteure: 
- "Thingiverse"
fotografen:
- "Dirk Wölffel (DirkW)"
schlagworte: ["Hexapod"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46234
- /detailsfbf8.html
imported:
- "2019"
_4images_image_id: "46234"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:06:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46234 -->
