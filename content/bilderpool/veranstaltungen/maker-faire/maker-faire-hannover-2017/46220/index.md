---
layout: "image"
title: "makerfaireb1.jpg"
date: "2017-08-29T19:05:31"
picture: "makerfaireb1.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/46220
- /details022b.html
imported:
- "2019"
_4images_image_id: "46220"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-29T19:05:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46220 -->
