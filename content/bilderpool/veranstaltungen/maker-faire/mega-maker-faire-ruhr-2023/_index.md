---
layout: "overview"
title: "Mega-Maker-Faire Ruhr 2023"
date: 2023-03-29T15:29:43+02:00
---

Die Maker-Faire-Ruhr fand am 25./26.03.23 in der [DASA Arbeitswelt Ausstellung](https://www.dasa-dortmund.de/) in Dormund statt.

Die ftc war mit einem Stand vertreten, von dem hier einige Eindrücke zu sehen sind.
