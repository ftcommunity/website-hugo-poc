---
layout: "image"
title: "makerfaire084.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire084.jpg"
weight: "81"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43538
- /details93b5-2.html
imported:
- "2019"
_4images_image_id: "43538"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43538 -->
