---
layout: "image"
title: "makerfaire174.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire174.jpg"
weight: "171"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43628
- /detailsb57c.html
imported:
- "2019"
_4images_image_id: "43628"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "174"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43628 -->
