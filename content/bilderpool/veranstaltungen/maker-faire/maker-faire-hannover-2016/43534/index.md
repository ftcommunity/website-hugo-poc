---
layout: "image"
title: "makerfaire080.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire080.jpg"
weight: "77"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43534
- /details58b4-3.html
imported:
- "2019"
_4images_image_id: "43534"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43534 -->
