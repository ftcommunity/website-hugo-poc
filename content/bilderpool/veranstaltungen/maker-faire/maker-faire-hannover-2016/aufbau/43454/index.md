---
layout: "image"
title: "makerfaire6.jpg"
date: "2016-05-30T19:31:22"
picture: "makerfaire6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43454
- /details070e-2.html
imported:
- "2019"
_4images_image_id: "43454"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:31:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43454 -->
