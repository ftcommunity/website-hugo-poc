---
layout: "image"
title: "makerfaire102.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire102.jpg"
weight: "99"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43556
- /details3d3f.html
imported:
- "2019"
_4images_image_id: "43556"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43556 -->
