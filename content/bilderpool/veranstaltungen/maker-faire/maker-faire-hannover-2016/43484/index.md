---
layout: "image"
title: "makerfaire030.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire030.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43484
- /details21d8-2.html
imported:
- "2019"
_4images_image_id: "43484"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43484 -->
