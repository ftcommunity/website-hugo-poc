---
layout: "image"
title: "makerfaire056.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire056.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43510
- /details1318.html
imported:
- "2019"
_4images_image_id: "43510"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43510 -->
