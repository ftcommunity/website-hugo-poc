---
layout: "image"
title: "makerfaire026.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire026.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43480
- /details5b6b.html
imported:
- "2019"
_4images_image_id: "43480"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43480 -->
