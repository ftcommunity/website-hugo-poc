---
layout: "image"
title: "makerfaire176.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire176.jpg"
weight: "173"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43630
- /detailsa8b5.html
imported:
- "2019"
_4images_image_id: "43630"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "176"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43630 -->
