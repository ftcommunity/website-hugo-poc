---
layout: "image"
title: "makerfaire197.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire197.jpg"
weight: "194"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43651
- /details42e1.html
imported:
- "2019"
_4images_image_id: "43651"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "197"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43651 -->
