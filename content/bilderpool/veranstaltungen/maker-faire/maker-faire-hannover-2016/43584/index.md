---
layout: "image"
title: "makerfaire130.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire130.jpg"
weight: "127"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43584
- /details8245-2.html
imported:
- "2019"
_4images_image_id: "43584"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43584 -->
