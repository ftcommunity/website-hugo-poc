---
layout: "image"
title: "makerfaire054.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire054.jpg"
weight: "51"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43508
- /detailsded9-2.html
imported:
- "2019"
_4images_image_id: "43508"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43508 -->
