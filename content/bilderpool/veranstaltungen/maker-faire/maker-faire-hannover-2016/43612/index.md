---
layout: "image"
title: "makerfaire158.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire158.jpg"
weight: "155"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43612
- /details5b47-3.html
imported:
- "2019"
_4images_image_id: "43612"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "158"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43612 -->
