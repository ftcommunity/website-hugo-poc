---
layout: "image"
title: "grillen9.jpg"
date: "2016-05-30T19:40:46"
picture: "grillen9.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43662
- /detailsdc7a.html
imported:
- "2019"
_4images_image_id: "43662"
_4images_cat_id: "3229"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:40:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43662 -->
