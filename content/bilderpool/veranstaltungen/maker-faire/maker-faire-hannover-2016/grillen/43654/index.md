---
layout: "image"
title: "grillen1.jpg"
date: "2016-05-30T19:40:46"
picture: "grillen1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43654
- /detailsd26c.html
imported:
- "2019"
_4images_image_id: "43654"
_4images_cat_id: "3229"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:40:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43654 -->
