---
layout: "image"
title: "makerfaire182.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire182.jpg"
weight: "179"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43636
- /details135b.html
imported:
- "2019"
_4images_image_id: "43636"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "182"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43636 -->
