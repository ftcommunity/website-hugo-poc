---
layout: "image"
title: "makerfaire178.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire178.jpg"
weight: "175"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43632
- /detailsc23c-2.html
imported:
- "2019"
_4images_image_id: "43632"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "178"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43632 -->
