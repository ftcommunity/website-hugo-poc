---
layout: "image"
title: "makerfaire159.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire159.jpg"
weight: "156"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43613
- /details1868.html
imported:
- "2019"
_4images_image_id: "43613"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "159"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43613 -->
