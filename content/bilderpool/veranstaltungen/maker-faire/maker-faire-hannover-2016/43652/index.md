---
layout: "image"
title: "makerfaire198.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire198.jpg"
weight: "195"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43652
- /details22a5.html
imported:
- "2019"
_4images_image_id: "43652"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "198"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43652 -->
