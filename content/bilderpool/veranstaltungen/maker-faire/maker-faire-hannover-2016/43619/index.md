---
layout: "image"
title: "makerfaire165.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire165.jpg"
weight: "162"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/43619
- /details335d-3.html
imported:
- "2019"
_4images_image_id: "43619"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "165"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43619 -->
