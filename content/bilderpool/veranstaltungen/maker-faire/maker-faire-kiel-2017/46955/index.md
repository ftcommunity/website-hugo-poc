---
layout: "image"
title: "Programmierung"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel28.jpg"
weight: "28"
konstrukteure: 
- "Grau"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46955
- /details4f27.html
imported:
- "2019"
_4images_image_id: "46955"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46955 -->
