---
layout: "image"
title: "Modell aus Technikgeschichte mit fischertechnik"
date: "2017-11-20T19:26:36"
picture: "makerfairkiel32.jpg"
weight: "32"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46959
- /details467c.html
imported:
- "2019"
_4images_image_id: "46959"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:36"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46959 -->
