---
layout: "image"
title: "Discovery Robot"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel30.jpg"
weight: "30"
konstrukteure: 
- "Grau"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46957
- /details7b57.html
imported:
- "2019"
_4images_image_id: "46957"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46957 -->
