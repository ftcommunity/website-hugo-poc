---
layout: "image"
title: "Alienangriff"
date: "2017-11-20T19:26:24"
picture: "makerfairkiel14.jpg"
weight: "14"
konstrukteure: 
- "Christian Wiechmann"
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46941
- /details952e.html
imported:
- "2019"
_4images_image_id: "46941"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46941 -->
