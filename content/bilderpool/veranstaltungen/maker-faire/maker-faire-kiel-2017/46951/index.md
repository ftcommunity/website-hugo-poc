---
layout: "image"
title: "Kugelbahn"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel24.jpg"
weight: "24"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46951
- /detailsabef.html
imported:
- "2019"
_4images_image_id: "46951"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46951 -->
