---
layout: "image"
title: "Schachspiel"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel25.jpg"
weight: "25"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46952
- /detailscabb.html
imported:
- "2019"
_4images_image_id: "46952"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46952 -->
