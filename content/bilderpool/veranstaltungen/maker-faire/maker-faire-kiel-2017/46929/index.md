---
layout: "image"
title: "Stand"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel02.jpg"
weight: "2"
konstrukteure: 
- "ftcommunity"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46929
- /details4a16.html
imported:
- "2019"
_4images_image_id: "46929"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46929 -->
