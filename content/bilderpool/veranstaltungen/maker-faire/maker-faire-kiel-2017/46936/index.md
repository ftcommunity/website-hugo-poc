---
layout: "image"
title: "Drehbank"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel09.jpg"
weight: "9"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46936
- /details457d.html
imported:
- "2019"
_4images_image_id: "46936"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46936 -->
Das Dreibackenfutter ist mit dem 3D Drucker gedruckt
