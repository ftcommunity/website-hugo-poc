---
layout: "image"
title: "Astronaut"
date: "2017-11-20T19:26:24"
picture: "makerfairkiel16.jpg"
weight: "16"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/46943
- /details8f80.html
imported:
- "2019"
_4images_image_id: "46943"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:24"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46943 -->
