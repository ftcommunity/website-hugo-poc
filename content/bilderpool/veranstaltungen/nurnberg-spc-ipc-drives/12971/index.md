---
layout: "image"
title: "22"
date: "2007-12-01T09:11:01"
picture: "Neuer_Ordner_022.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12971
- /details58b9.html
imported:
- "2019"
_4images_image_id: "12971"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:11:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12971 -->
SPS/IPC Drives Nürnberg 2007