---
layout: "image"
title: "sps ipc drives 2005 022"
date: "2005-11-24T14:29:45"
picture: "sps_ipc_drives_2005_022.jpg"
weight: "10"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "Sven Engelke (sven)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/5379
- /details375b-2.html
imported:
- "2019"
_4images_image_id: "5379"
_4images_cat_id: "591"
_4images_user_id: "1"
_4images_image_date: "2005-11-24T14:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5379 -->
