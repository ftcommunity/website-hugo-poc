---
layout: "overview"
title: "Nürnberg, SPC/IPC Drives"
date: 2020-02-22T08:58:30+01:00
legacy_id:
- /php/categories/7
- /categoriesc39c.html
- /categories7802.html
- /categories30d1.html
- /categories5cfa.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=7 --> 
Alljährlich findet in Nürnberg eine Fachmesse über Speicherprogrammierbare Steuerungen statt, wo fischertechnik-Modelle zur Demonstration gezeigt werden. Lothar Vogt, der selbst Kuka-Roboter programmiert, hat uns seine Fotos der Messe zur Verfügung gestellt.