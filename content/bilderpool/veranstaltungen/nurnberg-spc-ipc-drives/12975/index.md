---
layout: "image"
title: "26"
date: "2007-12-01T09:11:01"
picture: "Neuer_Ordner_026.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12975
- /detailsc605.html
imported:
- "2019"
_4images_image_id: "12975"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:11:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12975 -->
SPS/IPC Drives Nürnberg 2007