---
layout: "image"
title: "Panzer"
date: "2003-07-08T17:12:53"
picture: "Panzer_von_vorne.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1238
- /details12bb.html
imported:
- "2019"
_4images_image_id: "1238"
_4images_cat_id: "446"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1238 -->
