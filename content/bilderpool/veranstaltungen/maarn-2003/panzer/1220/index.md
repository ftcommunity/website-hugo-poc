---
layout: "image"
title: "Panzer von vorne"
date: "2003-07-08T16:05:25"
picture: "Panzer.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1220
- /details483b-2.html
imported:
- "2019"
_4images_image_id: "1220"
_4images_cat_id: "446"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1220 -->
