---
layout: "image"
title: "Panzer1"
date: "2003-07-31T18:41:45"
picture: "panzer1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Panzer", "Raupen"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1273
- /detailsea37.html
imported:
- "2019"
_4images_image_id: "1273"
_4images_cat_id: "446"
_4images_user_id: "34"
_4images_image_date: "2003-07-31T18:41:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1273 -->
Hier sieht man den Panzer mit abgenommener Blende, so kann man den einen Power-Mot sehen.