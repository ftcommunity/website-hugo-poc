---
layout: "image"
title: "Mammoetkran 4"
date: "2003-07-31T18:41:47"
picture: "hebekran4.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1282
- /details865c.html
imported:
- "2019"
_4images_image_id: "1282"
_4images_cat_id: "447"
_4images_user_id: "34"
_4images_image_date: "2003-07-31T18:41:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1282 -->
Schon unglaublich die Bauteileanzahl