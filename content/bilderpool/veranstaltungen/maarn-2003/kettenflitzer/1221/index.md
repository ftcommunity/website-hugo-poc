---
layout: "image"
title: "Schwertransporter mit Frickelsiggis Kettenflitzer"
date: "2003-07-08T16:05:25"
picture: "Scwertransporter_mit_Kettenflitzer.jpg"
weight: "4"
konstrukteure: 
- "Siegfried Kloster (frickelsiggi)"
fotografen:
- "Stephan Wenkers"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1221
- /details04f0.html
imported:
- "2019"
_4images_image_id: "1221"
_4images_cat_id: "444"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1221 -->
