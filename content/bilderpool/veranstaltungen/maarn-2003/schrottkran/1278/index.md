---
layout: "image"
title: "Schrottkran"
date: "2003-07-31T18:41:47"
picture: "schrottkran.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1278
- /details6ae1.html
imported:
- "2019"
_4images_image_id: "1278"
_4images_cat_id: "448"
_4images_user_id: "34"
_4images_image_date: "2003-07-31T18:41:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1278 -->
Schrottkran mit sehr interessantem Greifarm, der über ein Seil gesteuert wird und von einem zweiten gehalten wird.