---
layout: "image"
title: "Flaschen Abfüllanlage17"
date: "2003-08-04T09:17:36"
picture: "abfuellanlage17.jpg"
weight: "14"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1303
- /detailsa624.html
imported:
- "2019"
_4images_image_id: "1303"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1303 -->
Da setzt der Greifer die Flaschen ab.
Eine ausführliche Beschreibung der Anlage gibt es beim NL-ft-Fanclub.