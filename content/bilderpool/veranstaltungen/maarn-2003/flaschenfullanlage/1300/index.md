---
layout: "image"
title: "Flaschen Abfüllanlage14"
date: "2003-08-04T09:17:35"
picture: "abfuellanlage14.jpg"
weight: "11"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1300
- /details5596.html
imported:
- "2019"
_4images_image_id: "1300"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1300 -->
Rechts kommen die Flaschen an und reihen sich auf (an den Tastern). Etwas links davon ist ein drehbarer pneumatischer Greifer der die Flaschen greift und auf der im Bild sichtbaren Position absetzt. Natürlich in Kästen! Das Magazin ist die Unterseite der Silberlinge. Oben sieht man einen der Kästen rausragen. Im vorhergehen Bild 13 kann man sehen wo die befüllten Kästen rauskommen und wo sie dann vom Greifer genommen werden.
Na...Wird die Anlage noch weitergebaut?