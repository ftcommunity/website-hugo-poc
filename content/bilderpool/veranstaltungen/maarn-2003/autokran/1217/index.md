---
layout: "image"
title: "Lenkungsaufbau"
date: "2003-07-08T16:05:24"
picture: "Lenkungsaufbau.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- /php/details/1217
- /details889e.html
imported:
- "2019"
_4images_image_id: "1217"
_4images_cat_id: "445"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1217 -->
Durch das lange Aluprofil in der Mitte werden die 4 Achsen gleichzeitig gelenkt. Da das Profil ja unterschiedlich schwenkt,bedingt durch die einseitige Befestigung, schwenken die Achsen auch dementsprechend unterschiedlich. Ich finde das eine geniale Lösung.