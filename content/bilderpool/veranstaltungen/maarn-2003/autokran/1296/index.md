---
layout: "image"
title: "Autokran4"
date: "2003-08-04T09:17:35"
picture: "kran4.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1296
- /detailsa4e7-2.html
imported:
- "2019"
_4images_image_id: "1296"
_4images_cat_id: "445"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1296 -->
Und hier ist er nochmal der Autokran in seiner vollen Pracht zu sehen.