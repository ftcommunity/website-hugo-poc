---
layout: "image"
title: "Freefalltower"
date: "2003-07-10T14:03:57"
picture: "freefalltower.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/1244
- /detailsc357.html
imported:
- "2019"
_4images_image_id: "1244"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1244 -->
Der Freefalltower hat einen sehr leichten Korb. So braucht er zum bremsen nur eine sehr kleine Bremse mit einer Feder auf einem Zylinder (links oben).