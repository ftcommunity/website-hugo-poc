---
layout: "image"
title: "Hebevorrichtung"
date: 2025-03-07T11:29:04+01:00
picture: "neumuenster_2025_23.jpg"
weight: "18"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der eigentliche Greifer hängt an Ketten,  die aufgewickelt werden.