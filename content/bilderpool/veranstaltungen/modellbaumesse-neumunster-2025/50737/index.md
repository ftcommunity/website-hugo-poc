---
layout: "image"
title: "Falkirk Wheel"
date: 2025-03-07T11:28:36+01:00
picture: "neumuenster_2025_43.jpg"
weight: "45"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Zur Demonstration standen auch ständig Videos aus dem Internet zum Original in Schottland zur Verfügung.