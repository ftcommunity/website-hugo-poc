---
layout: "image"
title: "Heugebläse"
date: 2025-03-07T11:28:49+01:00
picture: "neumuenster_2025_06.jpg"
weight: "31"
konstrukteure: 
- "Frank Alpen"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Vorlage von Franks Heugebläse diente dazu, Heu auf einen Dachboden zu blasen. Hier wird das Ganze durch den Transport von fischer-Tipps simuliert und konnte gerade von den Kleinsten über einen Tastendruck in Betrieb genommen werden.