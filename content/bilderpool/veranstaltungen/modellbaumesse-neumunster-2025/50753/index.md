---
layout: "image"
title: "Fahrerkabine Liebherr"
date: 2025-03-07T11:28:56+01:00
picture: "neumuenster_2025_36.jpg"
weight: "25"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Fahrersitz ist mit einer Neigevorrichtung versehen, damit der Kranführer immer einen sicheren und bequemen Blick auf den Ausleger haben kann.