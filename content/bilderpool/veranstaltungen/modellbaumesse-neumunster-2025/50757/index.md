---
layout: "image"
title: "Liebherr HS 8040.4"
date: 2025-03-07T11:29:01+01:00
picture: "neumuenster_2025_11.jpg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Das etwa 30 kg schwere Fahrzeug stand sicher auf dem Tisch und war von drei Seiten aus zugänglich.