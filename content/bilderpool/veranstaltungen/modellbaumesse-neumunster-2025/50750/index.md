---
layout: "image"
title: "Untere Flaschenzugrollen"
date: 2025-03-07T11:28:52+01:00
picture: "neumuenster_2025_40.jpg"
weight: "29"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Die Seilwinde zum Anheben und Absenken des Kranauslegers ist etwas versteckt, dafür ist der erforderliche Flaschenzug umso deutlicher zu sehen.