---
layout: "image"
title: "Heugebläse"
date: 2025-03-07T11:28:47+01:00
picture: "neumuenster_2025_28.jpg"
weight: "32"
konstrukteure: 
- "Frank Alpen"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Das "Heu" wird dazu aus dem Anhänger vom Feld über Förderbänder in das Gebläse geführt und von diesem durch eine Rohrkonstruktion an seinen Bestimmungsort gebracht.