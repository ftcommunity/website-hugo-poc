---
layout: "image"
title: "Falkirk Wheel"
date: 2025-03-07T11:28:39+01:00
picture: "neumuenster_2025_16.jpg"
weight: "42"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Auch der Nachbau des schottischen Schiffshebewerks Falkirk Wheel hat immer wieder Besucher angezogen.