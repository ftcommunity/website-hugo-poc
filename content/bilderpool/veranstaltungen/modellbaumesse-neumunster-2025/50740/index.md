---
layout: "image"
title: "Draisine"
date: 2025-03-07T11:28:40+01:00
picture: "neumuenster_2025_50.jpg"
weight: "40"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Eigentlich sieht es so aus, dass die beiden Draisinenfahrer die Arbeit recht locker nehmen, da sie jeweils nur auf einem Bein stehen. Aufgrund ihrer Rückenhaltung ist es aber nicht unwahrscheinlich, dass sie ziemlich schnell Bandscheibenvorfälle erleiden.