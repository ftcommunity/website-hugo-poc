---
layout: "image"
title: "Oberwasser Fallkirk Wheel"
date: 2025-03-07T11:28:34+01:00
picture: "neumuenster_2025_47.jpg"
weight: "49"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Selbst die Kanalzuführung des Oberwassers wurde im Modell filigran ausgeführt.