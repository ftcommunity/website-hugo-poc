---
layout: "image"
title: "Antrieb Falkirk Wheel"
date: 2025-03-07T11:29:15+01:00
picture: "neumuenster_2025_45.jpg"
weight: ""
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Antrieb für die großen Flügel wurde über Ketten von einer Welle aus dem dahinter liegenden Berg geführt.