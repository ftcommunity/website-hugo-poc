---
layout: "image"
title: "Liebherr HS 8040.4"
date: 2025-03-07T11:29:03+01:00
picture: "neumuenster_2025_10.jpg"
weight: "19"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Detlefs Liebherr-Kran HS 8040.4 war auch schon vom Weiten aus zu sehen. Mit seine 4,8 Meter langen Ausleger war es das einzige Objekt in der Halle, das bis zur Decke reichte.