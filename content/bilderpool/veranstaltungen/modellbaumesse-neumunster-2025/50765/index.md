---
layout: "image"
title: "Standbesucher"
date: 2025-03-07T11:29:10+01:00
picture: "neumuenster_2025_19.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Irgendwie kam kaum einer an unserem Stand vorbei, ohne hinzusehen. Häufig war zu hören: Das ist fischertechnik. Oder aber auch von Halbwüchsigen: Der Kran ist aus LEGO gebaut. Naja …