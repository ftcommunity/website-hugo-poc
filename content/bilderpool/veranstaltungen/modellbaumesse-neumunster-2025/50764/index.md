---
layout: "image"
title: "Naschi-Greifer"
date: 2025-03-07T11:29:09+01:00
picture: "neumuenster_2025_03.jpg"
weight: "14"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Auch wenn es hier nicht so aussieht: Christians Naschi-Greifer wurde immer wieder gerne von den Kindern ausprobiert