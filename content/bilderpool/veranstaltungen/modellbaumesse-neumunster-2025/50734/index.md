---
layout: "image"
title: "Fördertrog"
date: 2025-03-07T11:28:33+01:00
picture: "neumuenster_2025_48.jpg"
weight: "50"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Im Modell rollten die beiden Fördertröge, die sich in den Augen der großen Hanteln befinden, einfach auf Rollen ab, wobei die Schwerkraft die Lage bestimmte. Im Original gibt es aber eine präzise Absicherung und Steuerung der Troglage über Zahnräder, nicht das da einfach etwas vereist und die Tröge in Schieflage gelangen.