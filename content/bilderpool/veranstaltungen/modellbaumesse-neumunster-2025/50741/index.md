---
layout: "image"
title: "Schaufellader"
date: 2025-03-07T11:28:41+01:00
picture: "neumuenster_2025_49.jpg"
weight: "39"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der kleine Schaufellader mit Knicklenkung kann immerhin das Führerhaus mit der Schaufel drehen, wobei sich diese wohl eher auf der anderen Seite befinden sollte: Über die kleineren Räder passt das Gestänge nämlich knapp rüber.