---
layout: "image"
title: "Falkirk Wheel"
date: 2025-03-07T11:28:38+01:00
picture: "neumuenster_2025_17.jpg"
weight: "43"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Pelé hat die Trogflügelverkleidungen extra mit der Laubsäge aus Sperrholz ausgesägt und mehrfach lackiert.