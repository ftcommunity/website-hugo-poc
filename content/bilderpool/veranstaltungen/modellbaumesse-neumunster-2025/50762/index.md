---
layout: "image"
title: "Portalkran im Greifer"
date: 2025-03-07T11:29:07+01:00
picture: "neumuenster_2025_21.jpg"
weight: "16"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Die eigentliche Hebevorrichtung wird in diesem Bild erkennbar.