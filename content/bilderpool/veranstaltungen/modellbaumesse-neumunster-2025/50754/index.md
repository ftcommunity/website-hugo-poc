---
layout: "image"
title: "Kranaufbau"
date: 2025-03-07T11:28:57+01:00
picture: "neumuenster_2025_14.jpg"
weight: "23"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der gesamte Kranaufbau strotzt nur so vor Detailverliebtheit.