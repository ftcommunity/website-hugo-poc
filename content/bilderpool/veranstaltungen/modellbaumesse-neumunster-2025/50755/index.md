---
layout: "image"
title: "Steuerschrank"
date: 2025-03-07T11:28:58+01:00
picture: "neumuenster_2025_13.jpg"
weight: "22"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Selbst die eigentlich hinter Türen versteckten Steuereinrichtungen und Energieverteiler sind modellmäßig ein Augenschmaus.