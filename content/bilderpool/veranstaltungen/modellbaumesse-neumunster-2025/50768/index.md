---
layout: "image"
title: "Gesamtansicht des Stands"
date: 2025-03-07T11:29:14+01:00
picture: "neumuenster_2025_34.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der Stand war dieses Mal etwas kleiner als sonst, die Flächen aber gut mit Modellen gefüllt.