---
layout: "image"
title: "Greifer"
date: 2025-03-07T11:29:08+01:00
picture: "neumuenster_2025_20.jpg"
weight: "15"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Um nicht nur das Gesamtwerk zu sehen: Hier der eigentliche Greifer.