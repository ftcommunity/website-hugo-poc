---
layout: "image"
title: "Kranausleger"
date: 2025-03-07T11:28:59+01:00
picture: "neumuenster_2025_12.jpg"
weight: "21"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Der gigantische Kranausleger machte auch aus der Nähe einen stabilen Eindruck.