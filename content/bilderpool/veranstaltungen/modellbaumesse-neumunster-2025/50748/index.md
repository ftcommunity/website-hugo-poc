---
layout: "image"
title: "Gabelstabler"
date: 2025-03-07T11:28:50+01:00
picture: "neumuenster_2025_27.jpg"
weight: "30"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Dirks ferngesteuerter Gabelstapler war immer wieder ein beliebter Anziehungspunkt. Hier konnte die Jugend sich darin üben,  eine Palette aus dem Regal zu holen, um an etwas zum Naschen und ein ft-Give-Away-Auto zu gelangen.