---
layout: "overview"
title: "Modellbaumesse Neumünster 2025"
date: 2025-03-07T11:28:30+01:00
---

Traditionell waren wir auch 2025 auf der Modellbaumesse im schleswig-holsteinischen Neumünster vertreten. Der Andrang war immer wieder groß, viele Besucher waren von den ausgestellten Modellen fasziniert und die Kinder (du auch ein paar Erwachsene) hatten Gelegenheit, selbst aktiv zu spielen.