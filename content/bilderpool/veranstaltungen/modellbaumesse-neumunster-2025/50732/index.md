---
layout: "image"
title: "Spendenbox"
date: 2025-03-07T11:28:30+01:00
picture: "neumuenster_2025_30.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

In die Spendenbox für den ftc Modellbau e.V. wurden immer wieder Münzen oder Scheine eingesteckt. 