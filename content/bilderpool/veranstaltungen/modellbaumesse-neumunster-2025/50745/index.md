---
layout: "image"
title: "Schnipsel"
date: 2025-03-07T11:28:46+01:00
picture: "neumuenster_2025_01.jpg"
weight: "34"
konstrukteure: 
- "Frank Alpen"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Eigentlich sieht es hier noch recht ordentlich aus. Doch bei der Rückführung des "Heus" in den Anhänger ging immer wieder einiges daneben, sodass der umliegende Fußboden gut mit fischer-Tips belegt war.