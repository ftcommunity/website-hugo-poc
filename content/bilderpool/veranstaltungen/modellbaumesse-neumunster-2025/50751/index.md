---
layout: "image"
title: "Seilwinden"
date: 2025-03-07T11:28:53+01:00
picture: "neumuenster_2025_39.jpg"
weight: "28"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Im Mittelbau sind die kraftvollen Seilwinden für den Kranhaken zu sehen. Der riesige Flaschenzug am hinteren Ende dient zum Anheben und Absenken des Auslegers über den zwischenliegenden Hilfsausleger. Und ganz hinten ist die Vorrichtung zum Aufnehmen und Ablegen von Gegengewichten zu sehen.