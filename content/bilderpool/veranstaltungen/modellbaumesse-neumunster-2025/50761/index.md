---
layout: "image"
title: "Horizontalantrieb"
date: 2025-03-07T11:29:06+01:00
picture: "neumuenster_2025_22.jpg"
weight: "17"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Über die Fläche wird das Ganze über Schneckenantriebe bewegt.