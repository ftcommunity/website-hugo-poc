---
layout: "image"
title: "Jetzt wird es gruselig"
date: 2025-03-07T11:28:35+01:00
picture: "neumuenster_2025_46.jpg"
weight: "48"
konstrukteure: 
- "Pelé Haupt"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Die dahinter liegende Landschaft machte das Modell extra spannend.