---
layout: "image"
title: "Neuheiten + Kugelbahn"
date: 2025-03-07T11:29:12+01:00
picture: "neumuenster_2025_26.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Die neben den Neuheiten platzierte Riesen-Kugelbahn wurde immer wieder von den Jüngsten mit Kugeln bestückt.