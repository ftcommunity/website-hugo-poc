---
layout: "image"
title: "Blick auf den Stand"
date: 2025-03-07T11:29:02+01:00
picture: "neumuenster_2025_24.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Ganz rechts ging es mit Christians Naschi-Greifer los, links daneben waren neben ein paar niederländischen Spezialfiguren die aktuell herauskommenden Baustellen-Neuheiten in Schwarz-Weiß-Orange zu sehen.