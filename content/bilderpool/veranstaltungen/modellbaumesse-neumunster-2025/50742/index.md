---
layout: "image"
title: "Modellsammlung"
date: 2025-03-07T11:28:42+01:00
picture: "neumuenster_2025_32.jpg"
weight: "38"
konstrukteure: 
- "Holger Bernhardt (Svefisch)"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Neben "Pneumatikgeschiebe" gab es von mir noch ein paar weitere kleine Modelle