---
layout: "image"
title: "Blick auf den Stand"
date: 2025-03-07T11:28:32+01:00
picture: "neumuenster_2025_33.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Es folgten Franks Heugebläse, ein Flipper, ein paar Kleinmodelle und dann der große Liebherr-Kran von Detlef. Den Abschluss ganz links machte dann Pelés Schiffshebewerk.