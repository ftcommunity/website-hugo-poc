---
layout: "image"
title: "Flipper"
date: 2025-03-07T11:28:45+01:00
picture: "neumuenster_2025_29.jpg"
weight: "36"
konstrukteure: 
- "Frank Alpen"
fotografen:
- "Holger Bernhardt (Svefisch)"
uploadBy: "Website-Team"
license: "unknown"
---

Ein weiteres gern ausprobiertes Teil war der obligatorische fischertechnik-Flipper. Erfahrene und weniger erfahrene Besucher hatten hier immer wieder ihren Spaß.