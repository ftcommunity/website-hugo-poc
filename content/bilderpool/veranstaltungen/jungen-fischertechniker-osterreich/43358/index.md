---
layout: "image"
title: "Die jungen Fischertechniker aus Österreich..."
date: "2016-05-15T10:46:08"
picture: "diejungenfischertechnikerausoesterreich13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43358
- /details9336.html
imported:
- "2019"
_4images_image_id: "43358"
_4images_cat_id: "3220"
_4images_user_id: "1688"
_4images_image_date: "2016-05-15T10:46:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43358 -->
