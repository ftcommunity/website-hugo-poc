---
layout: "image"
title: "Spiel Memory"
date: "2007-11-08T23:06:53"
picture: "seevetalhittfeld01.jpg"
weight: "1"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12541
- /details6b9b.html
imported:
- "2019"
_4images_image_id: "12541"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12541 -->
Die Kinder mussten immer 2 gleichartige Bilder finden