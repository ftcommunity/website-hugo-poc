---
layout: "image"
title: "Fischertechnik wiegen"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld09.jpg"
weight: "9"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/12549
- /details75ae.html
imported:
- "2019"
_4images_image_id: "12549"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12549 -->
Hier konnte man sich ft-Teile in einen Beutel packen, wiegen und dann nach Gewicht bezahlen.