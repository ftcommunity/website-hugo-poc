---
layout: "image"
title: "Explorer von unten mit Farbsensor"
date: "2007-03-23T22:21:21"
picture: "162_6258.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9674
- /detailsca49.html
imported:
- "2019"
_4images_image_id: "9674"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9674 -->
Hier ist der Prototyp vom Farbsensor zu sehen. 
Ich würde eine RGB LED nehmen und einen Fototransistor. Durch versch. farbiges anleuchten der Fläche kann man über die ermittelten Grauwerte die Farbe ermitteln.
