---
layout: "image"
title: "UT Kasten Deckel 3"
date: "2007-03-23T22:21:21"
picture: "162_6230.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9699
- /details04ae-2.html
imported:
- "2019"
_4images_image_id: "9699"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9699 -->
