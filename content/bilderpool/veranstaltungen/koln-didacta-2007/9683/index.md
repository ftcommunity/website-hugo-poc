---
layout: "image"
title: "Karussell"
date: "2007-03-23T22:21:21"
picture: "162_6246.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9683
- /details681d.html
imported:
- "2019"
_4images_image_id: "9683"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9683 -->
