---
layout: "image"
title: "Explorer Steuerung"
date: "2007-03-23T22:21:21"
picture: "162_6262.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9671
- /detailsd9b0.html
imported:
- "2019"
_4images_image_id: "9671"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9671 -->
Der Roboter wird durch die Taste "gerufen".
Die Interfaces haben eine Funkverbindung.
