---
layout: "image"
title: "ft:duino-Modell"
date: 2024-09-28T17:56:56+02:00
picture: "P1035896.JPG"
weight: "33"
konstrukteure: 
- "Peter Habermehl (Phabermehl)"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Das Modell baut die Türme von Hanoi.