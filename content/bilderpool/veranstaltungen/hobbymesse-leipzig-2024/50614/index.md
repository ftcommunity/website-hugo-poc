---
layout: "image"
title: "Kran"
date: 2024-09-28T17:58:42+02:00
picture: "P1035915.JPG"
weight: "75"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Hier ist das Kran-Zubehör in seiner Verpackung abmarschbereit zur Heimfahrt.