---
layout: "image"
title: "Kran"
date: 2024-09-28T17:58:18+02:00
picture: "P1035880.JPG"
weight: "63"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Auf Bitten unseres Nachbarstandes darf das Flugzeug ein paar Runden fliegen.