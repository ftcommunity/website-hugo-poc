---
layout: "image"
title: "Leuchtturm"
date: 2024-09-28T17:58:54+02:00
picture: "1727439389459.jpg"
weight: "80"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Damit verabschiedet sich der Leuchtturm von der Messe.