---
layout: "image"
title: "Kirmes-Greifer"
date: 2024-09-28T17:56:15+02:00
picture: "P1035890.JPG"
weight: "18"
konstrukteure: 
- "Christian Wiechmann"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Wie immer ein Highlight und nicht nur bei den Kindern beliebt: der Kirmesgreifer.