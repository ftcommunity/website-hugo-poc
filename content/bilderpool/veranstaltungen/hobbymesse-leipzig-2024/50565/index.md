---
layout: "image"
title: "Kleinmodelle"
date: 2024-09-28T17:56:38+02:00
picture: "P1035894.JPG"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Website-Team"
license: "unknown"
---

Allerhand zum Thema Getriebe: ein Klopmeier-Getriebe, die eiereckigen Zahnräder und die Mini-Murmelbahn.