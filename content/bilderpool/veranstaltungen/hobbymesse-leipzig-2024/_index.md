---
layout: "overview"
title: "Hobbymesse Leipzig 2024"
date: 2024-09-28T17:55:55+02:00
---

Die Hobbymesse Leipzig (ehemals modell-hobby-spiel) fand vom 20.09. bis 22.09.2024 in Leipzig statt.

Wie schon 2022 war die ft-Community mit einem Stand vertreten. Die großen und kleinen Besucherinnen und Besucher konnten sich über Modelle zum Mitmachen und zum Angucken freuen. Auch die Nachbarstände zeigten Interesse.
