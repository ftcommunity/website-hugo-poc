---
layout: "image"
title: "Industrieroboter 7"
date: "2003-04-22T16:21:52"
picture: "Industrieroboter 7.jpg"
weight: "7"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/293
- /details5be6.html
imported:
- "2019"
_4images_image_id: "293"
_4images_cat_id: "39"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:21:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=293 -->
