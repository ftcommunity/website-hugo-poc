---
layout: "image"
title: "Rundblick 4"
date: "2003-04-22T16:40:07"
picture: "Rundblick 4.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/355
- /details5cb3.html
imported:
- "2019"
_4images_image_id: "355"
_4images_cat_id: "45"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=355 -->
