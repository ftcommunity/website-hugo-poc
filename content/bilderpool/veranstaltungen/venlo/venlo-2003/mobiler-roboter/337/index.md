---
layout: "image"
title: "Mobiler Roboter 1"
date: "2003-04-22T16:40:07"
picture: "Mobiler Roboter 1.jpg"
weight: "1"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/337
- /details4b86-2.html
imported:
- "2019"
_4images_image_id: "337"
_4images_cat_id: "43"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=337 -->
