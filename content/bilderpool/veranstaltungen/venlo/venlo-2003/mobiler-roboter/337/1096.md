---
layout: "comment"
hidden: true
title: "1096"
date: "2006-05-17T04:30:39"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Nach verschiedenen Studien, denen nur ein kurzes Leben auf dem Basteltisch beschieden war, ist dies die erste voll funktionsfähige Version des Roboters. Differentialantrieb (Raupenantrieb) mit optischen Impulsgebern, vorne und hinten Tastfühler, schwenkbarer Infrarot-Entfernungssensor, Putzwalze.