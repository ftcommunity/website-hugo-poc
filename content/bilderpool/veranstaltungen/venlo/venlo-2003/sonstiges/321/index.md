---
layout: "image"
title: "Classic 2"
date: "2003-04-22T16:40:07"
picture: "Classic 2.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "NN"
schlagworte: ["Speichenrad"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/321
- /details9f04.html
imported:
- "2019"
_4images_image_id: "321"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=321 -->
