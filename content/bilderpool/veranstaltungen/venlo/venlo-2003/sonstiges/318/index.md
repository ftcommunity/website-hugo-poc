---
layout: "image"
title: "Bagger 2"
date: "2003-04-22T16:40:07"
picture: "Bagger 2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/318
- /detailse7b1-2.html
imported:
- "2019"
_4images_image_id: "318"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=318 -->
