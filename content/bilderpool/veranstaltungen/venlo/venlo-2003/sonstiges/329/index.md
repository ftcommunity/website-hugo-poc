---
layout: "image"
title: "Helikopter 1"
date: "2003-04-22T16:40:07"
picture: "Helikopter 1.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/329
- /detailsd87c-2.html
imported:
- "2019"
_4images_image_id: "329"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=329 -->
