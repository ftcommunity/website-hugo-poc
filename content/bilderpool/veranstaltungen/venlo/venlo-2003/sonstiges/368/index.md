---
layout: "image"
title: "Video"
date: "2003-04-22T16:40:07"
picture: "Video.jpg"
weight: "21"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "-?-"
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/368
- /detailsc04d.html
imported:
- "2019"
_4images_image_id: "368"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=368 -->
