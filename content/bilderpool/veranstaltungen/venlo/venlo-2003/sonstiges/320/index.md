---
layout: "image"
title: "Classic 1"
date: "2003-04-22T16:40:07"
picture: "Classic 1.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "NN"
schlagworte: ["Speichenrad"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/320
- /details14e4.html
imported:
- "2019"
_4images_image_id: "320"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=320 -->
