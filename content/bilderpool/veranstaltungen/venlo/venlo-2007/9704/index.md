---
layout: "image"
title: "Kamera 2"
date: "2007-03-23T23:43:25"
picture: "162_6274.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9704
- /detailsb2e6.html
imported:
- "2019"
_4images_image_id: "9704"
_4images_cat_id: "855"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9704 -->
