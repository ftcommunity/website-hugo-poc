---
layout: "image"
title: "Kamera"
date: "2007-03-23T23:43:25"
picture: "162_6275.jpg"
weight: "44"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/9703
- /details2b51-2.html
imported:
- "2019"
_4images_image_id: "9703"
_4images_cat_id: "855"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9703 -->
Ein sehr schönes Modell einer Kamera
