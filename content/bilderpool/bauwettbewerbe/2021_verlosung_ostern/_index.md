---
layout: "overview"
title: Verlosung zu Ostern 2021
date: 2021-04-07T13:17:10+02:00
---
Auch in diesem Jahr gibt es eine Verlosung zu Ostern. Unter allen Teilnehmenden, die bis zum 14.4. (das ist der letzte Tag der Osterferien in
Hessen) Bilder eines Modelles für den Bilderpool bereitstellen ([Anleitung](https://ftcommunity.de/fans/bilderpool)) oder selbst hochladen, verlosen wir ein Set eiförmige Zahnräder aus dem 3D-Drucker. Es gibt drei Voraussetzungen:

1. Das Modell muss einen deutllichen Bezug zu Ostern aufweisen (Eiffeltürme, weil der Konstrukteur mal zu Ostern in Paris war, gelten nicht).
2. Die Bilder dürfen noch nicht im Bilderpool oder im Forum existieren.
2. Zum Erhalt des Gewinns ist eine Postadresse in Deutschland erforderlich.

Die Verlosung wird von mir (EstherM) und Peter Habermehl durchgeführt. Daher können auch Mitlgieder des Vorstandes oder sonst wie an der Organisation von Verein, Webseite und Forum Beteiligte sowie deren Familienangehörige gerne teilnehmen.



