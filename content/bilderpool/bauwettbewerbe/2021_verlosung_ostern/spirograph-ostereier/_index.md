---
layout: "overview"
title: "Spirograph für Ostereier"
date: 2021-04-07T13:31:49+02:00
---

Inspiriert von einigen Fischertechnik-Spirographen, die ich auf YouTube gefunden habe, habe ich nun - passend zu Ostern - einen Spirographen für Eier gebaut. Vielleicht inspiriert er ja zu ähnlichen Projekten:
https://youtu.be/8OllJ-qGc30
