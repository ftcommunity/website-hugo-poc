---
layout: "image"
title: "Schreibschlitten mit einstellbarer Elliptik"
date: 2021-04-07T13:31:49+02:00
picture: "Eierspirograph_4.jpeg"
weight: "4"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Spirograph", "Ei", "Ostern", "Osterei"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Mechanik der Stiftführung ist vom "Ellipsograph des Archimedes" inspierert. Im Nachhinein bin ich mir aber nicht mehr sicher, ob dieser Aufwand nötig war.
In jedem Fall ist es wichtig, in der ganzen Einheit möglichst wenig Spiel und Verwindung zuzulassen. Ich habe dafür die Gelenkbausteine noch mit kleinen Hülsen und durchgesteckten Achsen verstärkt.
