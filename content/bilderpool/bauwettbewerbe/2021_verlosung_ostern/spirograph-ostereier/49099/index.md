---
layout: "image"
title: "Eierspirograph - Draufsicht"
date: 2021-04-07T13:31:53+02:00
picture: "Eierspirograph_1.jpeg"
weight: "1"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Spirograph", "Ei", "Ostern", "Osterei"]
uploadBy: "Website-Team"
license: "unknown"
---

