---
layout: "image"
title: "Antriebseinheit und Spirographenmechanik"
date: 2021-04-07T13:31:52+02:00
picture: "Eierspirograph_2.jpeg"
weight: "2"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Spirograph", "Ei", "Ostern", "Osterei"]
uploadBy: "Website-Team"
license: "unknown"
---

zu 3.: Für die Spirographeneinheit sind diverse Mechaniken denkbar. Man sollte sie allerdings zuerst mit einem abwaschbaren Stift ausprobieren (Bleistift), um zu prüfen, ob das Ergebnis auf dem Ei auch schön aussieht.
Wichtig ist, dass es über den Eiumfang mindestens 8 Wiederholungen des Musters gibt, da sonst das Auge nur "Krickel-Krackel" sieht.
zu 1.: Das improvisierte Z58 führt mit Sicherheit zu ausreichender Wiederholungszahl. Das Muster ist allerdings exterm dicht. Es geht sicher auch anders.
zu 2.: Mit der "Differentialsäge" wird auf die sehr langsame Drehbewegung des Eis eine Hin- und Herbewegung aufgeprägt. Erst dadurch sind interessante Muster möglich.
