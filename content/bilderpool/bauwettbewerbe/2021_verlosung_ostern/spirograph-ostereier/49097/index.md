---
layout: "image"
title: "Halterung für das Ei und Axiale Justage"
date: 2021-04-07T13:31:50+02:00
picture: "Eierspirograph_3.jpeg"
weight: "3"
konstrukteure: 
- "FiTe Klapauzius"
fotografen:
- "FiTe Klapauzius"
schlagworte: ["Spirograph", "Ei", "Ostern", "Osterei"]
uploadBy: "Website-Team"
license: "unknown"
---

Um das Ei möglichst vollständig zu bemalen, ist eine kleine Halterung notwendig. Mir ist es nicht gelungen diese schlupffrei zu gestalten, daher das doppelseitige Klebeband. Es sollte alle 2-3 Eier gewechselt (überklebt) werden.
