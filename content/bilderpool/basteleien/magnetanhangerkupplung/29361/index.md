---
layout: "image"
title: "Kupplungsidee 2 hält"
date: "2010-11-24T22:26:24"
picture: "SDC11025.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29361
- /details45c9.html
imported:
- "2019"
_4images_image_id: "29361"
_4images_cat_id: "2129"
_4images_user_id: "381"
_4images_image_date: "2010-11-24T22:26:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29361 -->
