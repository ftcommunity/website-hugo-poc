---
layout: "image"
title: "Kupplungsidee1"
date: "2010-11-24T22:26:24"
picture: "SDC11034.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Magnete", "Magnet", "Kupplung"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/29356
- /details628e.html
imported:
- "2019"
_4images_image_id: "29356"
_4images_cat_id: "2129"
_4images_user_id: "381"
_4images_image_date: "2010-11-24T22:26:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29356 -->
Universalkupplung
