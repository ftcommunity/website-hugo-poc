---
layout: "image"
title: "fischertechnik Computing"
date: "2010-09-05T09:28:55"
picture: "fischertechnikcomputing1.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Sascha Lipka"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- /php/details/28029
- /detailsb809.html
imported:
- "2019"
_4images_image_id: "28029"
_4images_cat_id: "463"
_4images_user_id: "635"
_4images_image_date: "2010-09-05T09:28:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28029 -->
Habe dieses Set Gestern bei der Uni in Gaborone, Botswana, gefunden. Wie neu, noch in original Verpackung. Selbst die kleinen Beutel mit den Teilen drin sind noch zu. Habe das Set mit der Uni getauscht fuer einen neues ROBO Industry II Set mit Controller.