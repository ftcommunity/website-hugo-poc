---
layout: "image"
title: "ein defekter Zylinder  (oberer Anschluss abgerissen)"
date: "2016-05-16T16:58:44"
picture: "pneumatikbetaetiger5.jpg"
weight: "5"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43383
- /details8016.html
imported:
- "2019"
_4images_image_id: "43383"
_4images_cat_id: "3222"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43383 -->
bei diesem Zylinder ist der obere Kopf (Schlauchanschluss) vom transparenten Rohr getrennt - abgebrochen
