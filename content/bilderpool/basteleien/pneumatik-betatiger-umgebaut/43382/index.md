---
layout: "image"
title: "Unterteil entfernt .."
date: "2016-05-16T16:58:44"
picture: "pneumatikbetaetiger4.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43382
- /detailsc903.html
imported:
- "2019"
_4images_image_id: "43382"
_4images_cat_id: "3222"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43382 -->
