---
layout: "image"
title: "Betätiger mit Dichtung aus dem defekten Zylinder"
date: "2016-05-16T16:58:44"
picture: "pneumatikbetaetiger1.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43379
- /detailsa1b8.html
imported:
- "2019"
_4images_image_id: "43379"
_4images_cat_id: "3222"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43379 -->
leichtes einpressen - und schwupss, die Dichtung aus dem Zylinder passt in das Betätiger-Gehäuse. Somit haben wir eine Art Düse - könnte zum berührungslosen Abtasten von eben Flächen dienen - evtl auch als Vakuum-Sauger - wobei Tests ergeben haben, dass die Haltekraft nicht allzu hoch ist. nach ft-pedia-Harald-Definiton also eine Kaulquappe oder sehr kleiner Frosch ohne Krone .. ;-)
