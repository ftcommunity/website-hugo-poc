---
layout: "image"
title: "Sauger mit eingeschobenen Schlauch"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette6.jpg"
weight: "6"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43389
- /details4a3e.html
imported:
- "2019"
_4images_image_id: "43389"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43389 -->
hier sieht man den durchgeschobenen Schlauch und das abgeschnittene Endstück des Adapters
