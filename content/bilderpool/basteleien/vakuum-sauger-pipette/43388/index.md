---
layout: "image"
title: "Untertasse und Zylinder"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette5.jpg"
weight: "5"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43388
- /details9148.html
imported:
- "2019"
_4images_image_id: "43388"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43388 -->
