---
layout: "image"
title: "Anschlussstück des Saugers - nicht FT-kompatibel"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette9.jpg"
weight: "9"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43392
- /details2754.html
imported:
- "2019"
_4images_image_id: "43392"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43392 -->
Hier der komplette Vakuum-Sauger
