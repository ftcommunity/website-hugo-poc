---
layout: "overview"
title: "Basteleien"
date: 2020-02-22T07:41:34+01:00
legacy_id:
- /php/categories/463
- /categories053c.html
- /categoriesbde1.html
- /categories425e.html
- /categories8e95.html
- /categories4584.html
- /categories05ed.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=463 --> 
Immer wieder bauen Fans bestehende ft-Teile um oder sie basteln selber neue Teile. 
Es wird also gebohrt, gesägt, gelötet, 3D-gedruckt, geklebt und gefeilt. 
Hier gibts die Fotos dazu!

Ganze Modelle -- auch wenn sie modifizierte Teile verwenden -- 
findet ihr in der Kategorie [Modelle](https://ftcommunity.de/bilderpool/modelle/). 
Hier findet ihr die Teile selbst.
