---
layout: "image"
title: "30493_I'm walking_Bauanleitung_Seite41_Ausschnitt"
date: "2009-05-30T09:12:55"
picture: "ergaenzunsseitenzupdfbauanleitungen8.jpg"
weight: "8"
konstrukteure: 
- "ft"
fotografen:
- "Ed"
uploadBy: "Ed"
license: "unknown"
legacy_id:
- /php/details/24136
- /detailsdf75.html
imported:
- "2019"
_4images_image_id: "24136"
_4images_cat_id: "1656"
_4images_user_id: "43"
_4images_image_date: "2009-05-30T09:12:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24136 -->
Erläuterung zur Fortbewegung auf sechs Beinen