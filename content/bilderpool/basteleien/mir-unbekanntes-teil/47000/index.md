---
layout: "image"
title: "Antriebsfeder 380 ?"
date: "2017-12-18T20:26:16"
picture: "Antriebsfeder.png"
weight: "13"
konstrukteure: 
- "Peter Wyrsch"
fotografen:
- "Peter Wyrsch"
uploadBy: "pwy"
license: "unknown"
legacy_id:
- /php/details/47000
- /detailse2e1.html
imported:
- "2019"
_4images_image_id: "47000"
_4images_cat_id: "2865"
_4images_user_id: "2715"
_4images_image_date: "2017-12-18T20:26:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47000 -->
In meiner Samlung fand ich eine Antriebsfeder 380 ??
Weder in meinen Handbücher zu den Hobbykästen noch in der Datenbank ist diese erwähnt.