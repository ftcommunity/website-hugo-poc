---
layout: "comment"
hidden: true
title: "23871"
date: "2017-12-19T15:04:01"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ob ich eine 380er habe, weiß ich nicht, aber ich habe auch eine ganze Reihe verschiedener Längen, insbesondere recht kurze - und ich habe keinen Schimmer, woher ich die alle haben sollte. Ich kenne auswendig nur die Länge, die in den Ur-ft-Kästen um die Viererpacks von Achsen mit den zwei roten Haltern lagen, und ich hätte auch angenommen, die im hobby-1 um die beiden Seilhaken geschlungene wäre dieselbe Länge. Offenbar gab es aber doch recht viele Längen, und ich meine, im Forum stand auch mal ein Hersteller, der genau solche Federn in allen möglichen Längen anbot.

Gruß,
Stefan