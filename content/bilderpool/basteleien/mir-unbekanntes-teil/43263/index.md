---
layout: "image"
title: "Baustein 32227 _grau Bosch Renner"
date: "2016-04-08T21:38:56"
picture: "Bosch_Renner_IMG_5394.jpg"
weight: "9"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/43263
- /detailsec16.html
imported:
- "2019"
_4images_image_id: "43263"
_4images_cat_id: "2865"
_4images_user_id: "1688"
_4images_image_date: "2016-04-08T21:38:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43263 -->
Baustein 32227 _grau Bosch Renner
