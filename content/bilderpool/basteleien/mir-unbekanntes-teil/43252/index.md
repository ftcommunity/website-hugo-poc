---
layout: "image"
title: "unbekannt_1"
date: "2016-04-04T21:54:43"
picture: "unbek_tei_kleinl.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/43252
- /details7448.html
imported:
- "2019"
_4images_image_id: "43252"
_4images_cat_id: "2865"
_4images_user_id: "10"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43252 -->
Hallo zusammen. Dieses Teil ist mir jetzt in die Finger geraten und ich habe keine Idee in welchem Baukasten dieses Teil enthalten war und welchem Zweck es diente. 
Lothar
