---
layout: "comment"
hidden: true
title: "19755"
date: "2014-12-02T09:43:09"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
na, darauf wär ich nie gekommen! Danke für den Hinweis!

Ich hab schon versucht, ob das Raster der Lochung irgendwie zu Zahnrädern passt oder sich die Bauplatten draufstecken lassen und lauter so ein Kram...funktioniert alles nicht. Auch sonst ist mir bisher keinerlei Verwendung eingefallen. 

wie ist das dann mit dem Reinheitsgebot der ft-Puristen? dürfen nur Original fischertechnik Styroporplatten verwendet werden? *lach* gibt es die in der Ersatzteilliste???
naja, so abwegig ist die Frage nicht; gibt sogar einen Original fischertechnik Luftballon:
https://knobloch-gmbh.de/de/fischertechnik/einzelteile/neuteile/neuteile-2013/luftballon-blau

Gruß
Richard