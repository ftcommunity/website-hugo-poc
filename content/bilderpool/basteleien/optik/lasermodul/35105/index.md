---
layout: "image"
title: "Lasermodul in Gehäuse von Lichtempfindlichem Widerstand"
date: "2012-07-07T09:52:30"
picture: "ftLASER1.jpg"
weight: "3"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- /php/details/35105
- /details8256.html
imported:
- "2019"
_4images_image_id: "35105"
_4images_cat_id: "545"
_4images_user_id: "1524"
_4images_image_date: "2012-07-07T09:52:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35105 -->
Wie sich die Ideen doch gleichen können. Und doch sind Unterschiede zu erkennen. Die Gefahr bleibt bei Laserarbeiten jedoch immer wieder bestehen...   Nicht in den Strahl blicken!!!