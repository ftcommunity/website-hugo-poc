---
layout: "image"
title: "How to assemble?"
date: "2006-10-28T08:17:34"
picture: "schanke_002.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/7234
- /details1be1.html
imported:
- "2019"
_4images_image_id: "7234"
_4images_cat_id: "694"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7234 -->
Make it as small as possible to fit inside a cable....
