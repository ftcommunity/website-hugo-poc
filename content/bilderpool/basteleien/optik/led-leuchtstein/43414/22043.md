---
layout: "comment"
hidden: true
title: "22043"
date: "2016-05-28T19:49:53"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Hallo Roland, 

ok, dann ist wohl weiß die beste Wahl. Vielleicht kann man es ja sehr dünn machen, damit noch genügend Licht durchkommt. Die Platinchen mit den LEDs haben die 

Maße 14 x 30 mm. 

Die Platinen sind 1,5 mm dick und der Aufbau nochmal 1,6 mm, in Summe bauen sie also 3,1 mm in die Höhe. 

Bitte auf der Anschlussseite eine Aussparung für die vier Anschlussdrähte lassen.

Danke und Gruß, Dirk