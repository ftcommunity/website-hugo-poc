---
layout: "image"
title: "LED im FT Leuchtstein"
date: "2017-02-05T15:27:45"
picture: "ledimftleuchtstein4.jpg"
weight: "4"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45124
- /details67a6.html
imported:
- "2019"
_4images_image_id: "45124"
_4images_cat_id: "3362"
_4images_user_id: "2240"
_4images_image_date: "2017-02-05T15:27:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45124 -->
