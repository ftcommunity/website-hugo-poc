---
layout: "image"
title: "LED"
date: "2008-01-22T18:10:20"
picture: "LED1.jpg"
weight: "15"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13369
- /details4b6e-2.html
imported:
- "2019"
_4images_image_id: "13369"
_4images_cat_id: "1073"
_4images_user_id: "456"
_4images_image_date: "2008-01-22T18:10:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13369 -->
Das ist eine LED superhell rot von reichelt. Sie ist in der einen Hälfte des Gelenksteines eingebaut. Vorwiderstand ist auch dran und 2 Buchsen zum Anschließen von ft-Kabeln.
