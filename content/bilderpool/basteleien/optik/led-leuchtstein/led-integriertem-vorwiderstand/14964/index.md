---
layout: "image"
title: "Led in Halterung"
date: "2008-07-26T16:23:19"
picture: "Bild_82.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
schlagworte: ["LED"]
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/14964
- /detailsd942-2.html
imported:
- "2019"
_4images_image_id: "14964"
_4images_cat_id: "1190"
_4images_user_id: "791"
_4images_image_date: "2008-07-26T16:23:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14964 -->
12V LED in der Halterung.