---
layout: "image"
title: "LED mit integriertem Vorwiderstand"
date: "2007-12-30T20:28:31"
picture: "ledmitintegriertemvorwiderstand2.jpg"
weight: "2"
konstrukteure: 
- "WERWEX"
fotografen:
- "WERWEX"
uploadBy: "werwex"
license: "unknown"
legacy_id:
- /php/details/13176
- /detailsaff4.html
imported:
- "2019"
_4images_image_id: "13176"
_4images_cat_id: "1190"
_4images_user_id: "689"
_4images_image_date: "2007-12-30T20:28:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13176 -->
Das Beispiel zeigt eine LED mit integriertem Vorwiderstand ausgelegt für 12 Volt.
Angebaut z.B. als Kontrol LED an einem Mini-Motor