---
layout: "image"
title: "LED mit Platine"
date: "2012-07-07T09:52:30"
picture: "ftLED3.jpg"
weight: "12"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- /php/details/35103
- /detailsc5cb.html
imported:
- "2019"
_4images_image_id: "35103"
_4images_cat_id: "1190"
_4images_user_id: "1524"
_4images_image_date: "2012-07-07T09:52:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35103 -->
LED in der Vergrößerung