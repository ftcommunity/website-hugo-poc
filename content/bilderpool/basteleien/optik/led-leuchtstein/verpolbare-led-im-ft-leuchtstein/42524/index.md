---
layout: "image"
title: "LEDs"
date: "2015-12-19T12:41:27"
picture: "vlifl3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/42524
- /details5b69.html
imported:
- "2019"
_4images_image_id: "42524"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42524 -->
LED Platinen passen dank ihrer Größe (12mm x12mm) in die ft Leuchtsteine
