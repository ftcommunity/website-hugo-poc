---
layout: "image"
title: "LED-Leuchtstein01.JPG"
date: "2007-12-01T16:36:30"
picture: "LED-Leuchtstein01.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12976
- /details772f.html
imported:
- "2019"
_4images_image_id: "12976"
_4images_cat_id: "1073"
_4images_user_id: "4"
_4images_image_date: "2007-12-01T16:36:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12976 -->
Meine Version, aufbauend auf der Idee von Franky: die LED soll so tief im Stein sitzen, dass die alten Leuchtkappen problemlos drauf passen. Deswegen nehme ich auch die alte Sorte Leuchtsteine, die noch keine seitlichen Löcher für die Clipse der neuen Leuchtkappen haben.

1. Eine 2 mm-Bohrung quer durch den unteren Bereich des Leuchtsteins anbringen.

2. Von dort nach oben hin die Gehäusewand aufschlitzen (sieht man bei den Steinen rechts oben).

3. Die Drähte der LED abwinkeln und von innen nach außen durch die Bohrung hindurch-friemeln. Wenn der Draht ein bisschen außen heraussteht, kann man ihn mit einer Zange weiter herausziehen.

4. LED-Drähte straff nach oben/innen ziehen, damit sie durch den Schlitz in der Außenwand in die dahinter liegende Kammer wandern (rechts oben ist das gerade geschehen).

5. Einen LED-Draht ablängen und zwischen Gehäusewand und Buchsenröhrchen einklemmen.

6. Widerstand einseitig ablängen und in gleicher Weise an der anderen Gehäuseseite einklemmen (mitte rechts).

7. Widerstand und LED-Draht verlöten und kappen.
