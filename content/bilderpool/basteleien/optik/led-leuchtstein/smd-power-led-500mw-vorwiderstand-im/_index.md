---
layout: "overview"
title: "SMD Power LED 500mW mit Vorwiderstand im Leuchtstein"
date: 2020-02-22T07:45:56+01:00
legacy_id:
- /php/categories/3188
- /categories217c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3188 --> 
Hier mal eine neue Variante der LEDs im Leuchstein:

Diese SMD LED habe ich bei Ali-Express bestellt, 100 Stück für 1,60 Euro inkl. Porto. Sie hat die Abmessungen 7 x 2 mm und verträgt bis zu 150 mA bei 3,5V Flussspannung, i.e. ca. 500 mW maximale Leistung. Sie entfaltet aber bereits bei 50 mA eine grelle, blendende Helligkeit. Mit dem Vorwiderstand von 100 Ohm ergeben sich folgende Arbeitspunkte:

8,4 V Akku: ca. 50mA (reicht schon gut, ist so hell wie eine Linsenlampe, hat aber einen größeren Beleuchtungswinkel

12V Trafo: ca. 85 mA (ist nicht so viel heller als bei 50 mA, vermutlich wegen Erwärmung, läuft aber stabil im Dauerbetrieb. 

Zum Einbau habe ich erst die LED mit dem Widerstand verlötet (auf einer ebenen Fläche als Unterlage) und dann beide zusammen in den Leuchtstein verlötet. Am besten schleift man die Kontaktbuchsen des Leuchtsteins ein wenig mit Sandpapier an, um die Oberfläche von Oxid zu befreien und lötet zunächst auf beide Kontaktbuchsen jeweils einen Fleck mit Lötzinn auf. Dann schmiert man SMD Lötpaste auf beide Kontaktbuchsen, platziert die bereits zusammen gelöteten Bauteile (LED + Widerstand) darauf und verlötet sie nun mit den Kontaktbuchsen. Das ist alles etwas tricky, bis man den optimalen Prozess herausgefunden hat.