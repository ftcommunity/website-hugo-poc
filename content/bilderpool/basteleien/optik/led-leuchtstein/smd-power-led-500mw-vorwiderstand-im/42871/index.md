---
layout: "image"
title: "Leuchtend von oben"
date: "2016-02-13T21:30:12"
picture: "LED_burn.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/42871
- /details2684.html
imported:
- "2019"
_4images_image_id: "42871"
_4images_cat_id: "3188"
_4images_user_id: "579"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42871 -->
Wenn man direkt in die LED hineinblickt, hat sie eine starke Blendwirkung. Hier wird sie an einem LiPo 2s Akku mit 7,4 - 8 V betrieben, also bei ca einem Drittel ihrer maximalen Leistung.