---
layout: "image"
title: "LEDs"
date: "2007-12-16T00:19:57"
picture: "leds1.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/13083
- /detailsa5ba.html
imported:
- "2019"
_4images_image_id: "13083"
_4images_cat_id: "699"
_4images_user_id: "453"
_4images_image_date: "2007-12-16T00:19:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13083 -->
Hier mal ein Beitrag von mir zu dem Thema.
