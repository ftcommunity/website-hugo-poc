---
layout: "comment"
hidden: true
title: "4878"
date: "2008-01-01T13:10:35"
uploadBy:
- "FtfanClub"
license: "unknown"
imported:
- "2019"
---
es wirkt etwas grell auf dem Bild, in der realität kommt es gut rüber. Die Lampe ist ausgelegt für 12 V und schon bei geringer Voltzahl spricht sie gut an.

Ich mache demnöchst noch ein paar Empfindlichkeitsversuche, aber zB an einem FlipFlop oder mehreren flipFlops kommt es sehr gut. Da ist plötzlich bewegung im Spiel. da man aber die schwachen Brinchen gewohnt ist, wirkt es an einem Silberling nicht so echt, nicht nostalgisch, aber praktisch :-)