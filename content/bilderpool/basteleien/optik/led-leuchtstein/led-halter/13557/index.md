---
layout: "image"
title: "Einfacher 3mm LED Halter"
date: "2008-02-05T17:14:32"
picture: "8.jpg"
weight: "15"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["3mm", "LED", "Halter"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13557
- /detailsaf4d-2.html
imported:
- "2019"
_4images_image_id: "13557"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13557 -->
Einfacher geht es kaum noch.
