---
layout: "image"
title: "Einfacher LED Halter"
date: "2008-02-05T17:14:32"
picture: "7.jpg"
weight: "17"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13559
- /details5f3a.html
imported:
- "2019"
_4images_image_id: "13559"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13559 -->
Hier mal auf einem Baustein 5
