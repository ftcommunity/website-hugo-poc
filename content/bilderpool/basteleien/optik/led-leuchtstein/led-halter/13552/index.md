---
layout: "image"
title: "3mm LED Halter"
date: "2008-02-05T17:14:32"
picture: "9.jpg"
weight: "10"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["LED", "Halter", "3mm"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13552
- /detailsb2ec-2.html
imported:
- "2019"
_4images_image_id: "13552"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13552 -->
Teile für einen LED Halter
ohne ft-Teile zu bearbeiten.
