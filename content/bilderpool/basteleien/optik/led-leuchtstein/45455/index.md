---
layout: "image"
title: "Material für LED Leuchtsteine"
date: "2017-03-06T21:07:12"
picture: "IMG_20161222_142416.jpg"
weight: "27"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["LED", "Beleuchtung", "eigenbau", "Leuchsteine"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/45455
- /detailsf106.html
imported:
- "2019"
_4images_image_id: "45455"
_4images_cat_id: "1073"
_4images_user_id: "2638"
_4images_image_date: "2017-03-06T21:07:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45455 -->
Die Materialsammlung
(Leider habe ich einen Widerstand mit zu großer Leistung genommen, daher wird es knapp mit dem Platz.)