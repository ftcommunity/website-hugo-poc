---
layout: "image"
title: "LED imLeuchtstein"
date: "2007-11-03T12:52:36"
picture: "helleledsimleuchtbaustein1.jpg"
weight: "5"
konstrukteure: 
- "Frank Hanke (franky)"
fotografen:
- "Frank Hanke (franky)"
uploadBy: "franky"
license: "unknown"
legacy_id:
- /php/details/12382
- /details2ef8.html
imported:
- "2019"
_4images_image_id: "12382"
_4images_cat_id: "1073"
_4images_user_id: "666"
_4images_image_date: "2007-11-03T12:52:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12382 -->
Helle LED mit einem Vorwiderstand (220 Ohm) in einem Leuchtbaustein für den Betrieb für etwa 9V.
Daten
Strom etwa 25mA
Vorwärtsspannung etwa 3,5V
=> R=220 Ohm

Im Gegensatz zu der LED liesssen bei einer  Lampe etwa 100mA. 

Wird der Baustein so an einem Lampenport des Interfaces verwended fliessen etwa 23mA. Die Dinger Leuchten ultrahell.
Allerdings passen, bauartbedingt, die alten, geschlossenen Lampenkappen nicht mehr. Die mit den Löscher gehen noch. Die neuen Käppies sind kein Problem, da diese sowieso etwas höher sind.

Schliesst man die LED direkt an ein vollgeladenes Akkupack an, so fliesst ein Striom von etwa 28mA. Sicherlich kann man auch den nächst grösseren Widerstand (270 Ohm) benutzen.

Nachteil: Ich muss die LED immer richtig gepolt anschliessen. Selbst wenn die Verpolung  kurz vertauscht wird, ist das auch kein Beinbruch, da die Sperrspannung, wenn ich das Datenblatt richtig interpretiert habe, um die 30V liegt.

Falls es nicht so deutlich sichtbar ist, der Widerstand und LED sind in der Mitte von dem Steckteil zusammengelöted. Die anderen Beinchen werde aussen durch die Löcher geführt. So spart man sich diverse isolierungen und es ist sichergestllt das sich keiner Drähte berühren.
