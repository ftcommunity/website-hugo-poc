---
layout: "image"
title: "Led-Leuchtstein Hauptansicht (3)"
date: "2007-11-05T18:58:22"
picture: "Led_wei.jpg"
weight: "8"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
schlagworte: ["Led", "Leuchtstein", "Selbstbau", "Eigenbau", "Licht", "Beleuchtung", "Löten", "Leuchtkappe"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/12512
- /details7e0b.html
imported:
- "2019"
_4images_image_id: "12512"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-11-05T18:58:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12512 -->
Links: eingeschaltete Led ohne Kappe
Mitte: ausgeschaltete Led mit klarer Kappe
Rechts: eingeschaltete Led mit klarer Kappe