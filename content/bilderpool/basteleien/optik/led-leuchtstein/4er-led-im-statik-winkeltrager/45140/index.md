---
layout: "image"
title: "LED Streifen"
date: "2017-02-11T19:43:45"
picture: "erledimstatikwinkeltraeger6.jpg"
weight: "6"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- /php/details/45140
- /detailseba3-2.html
imported:
- "2019"
_4images_image_id: "45140"
_4images_cat_id: "3364"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T19:43:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45140 -->
Jeweils zwei der LEDs können getrennt angesteuert werden.