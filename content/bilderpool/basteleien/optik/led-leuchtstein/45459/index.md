---
layout: "image"
title: "LED Leuchtsteine: Spot und Fluter"
date: "2017-03-06T21:07:12"
picture: "led01.jpg"
weight: "31"
konstrukteure: 
- "ClassicMan"
fotografen:
- "ClassicMan"
schlagworte: ["LED", "Leuchtbausteine"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- /php/details/45459
- /detailsc6f9.html
imported:
- "2019"
_4images_image_id: "45459"
_4images_cat_id: "1073"
_4images_user_id: "2638"
_4images_image_date: "2017-03-06T21:07:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45459 -->
Hier die fertigen LED Leuchtsteine.

Links: der "Spot" mit geringerem Abstrahlwinkel. WIr nutzen ihn z.B. an unserer Schrägseilbrücke die Fahrbahnbeleuchtung, die von oben herabhängt und nur die Schmale Fahrbahn ausleuchten soll, sowie für die "Scheinwerfer", die später die Türme der Brücke anstrahlen.

Rechts: die "Fluter" mit einem gleichmäßigen und weiten Abstrahlwinkel. Für Warnleuchten und später an Fahrzeugen Bllinker etc.