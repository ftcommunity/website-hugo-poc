---
layout: "image"
title: "Led-Leuchtstein Hauptansicht (2)"
date: "2007-09-27T20:22:13"
picture: "DSCF1720_neu.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
schlagworte: ["Led", "Leuchtstein", "Widerstand", "löten", "Eigenbau", "Elektronik"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/12031
- /details960a.html
imported:
- "2019"
_4images_image_id: "12031"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-09-27T20:22:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12031 -->
Hier sieht man einen Teil des eingelöteten Widerstandes. Diese neue Ära des Lichtes bei Fischertechnik hat klare 
Vorteile:
-sehr geringe Stromaufnahme 20mA,
 (im Vergleich: Die Linsenstecklampe von                    
  ft verbraucht 150mA)
 dadurch längere Spielzeit bei Akkubetrieb
-deutlich längere Lebensdauer
-bei Eigenbau sehr geringe Anschaffungs-
 kosten (0,70 Euro für Led & Widerstand)

Nachteile:
-Led bleibt dauerhaft im Leuchtstein
-man darf nicht direkt in den Strahl der    
 Led blicken, da die Gefahr von Netzhaut-
 verbrennung mit steigender mcd Zahl 
 zunimmt
(Die Anleitung zum Nachbauen folgt auf den weiteren Fotos.)