---
layout: "image"
title: "Led-Leuchtstein mit blauer Kappe"
date: "2007-11-05T19:42:44"
picture: "Led_blau.jpg"
weight: "11"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
schlagworte: ["Led", "Leuchtstein", "Selbstbau", "Eigenbau", "Licht", "Beleuchtung", "Löten", "Leuchtkappe"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- /php/details/12515
- /details5369.html
imported:
- "2019"
_4images_image_id: "12515"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-11-05T19:42:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12515 -->
Links: eingeschaltete Led
Rechts: ausgeschaltete Led