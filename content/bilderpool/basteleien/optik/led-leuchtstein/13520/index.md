---
layout: "image"
title: "LED-Strahler"
date: "2008-02-04T13:59:56"
picture: "LED5.jpg"
weight: "19"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/13520
- /details70f3.html
imported:
- "2019"
_4images_image_id: "13520"
_4images_cat_id: "1073"
_4images_user_id: "456"
_4images_image_date: "2008-02-04T13:59:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13520 -->
