---
layout: "image"
title: "Lampe mit Reflektor"
date: "2008-03-15T09:11:09"
picture: "lampe2.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/13903
- /details61a5.html
imported:
- "2019"
_4images_image_id: "13903"
_4images_cat_id: "1276"
_4images_user_id: "453"
_4images_image_date: "2008-03-15T09:11:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13903 -->
Hier ist die Lampe in Betrieb.
