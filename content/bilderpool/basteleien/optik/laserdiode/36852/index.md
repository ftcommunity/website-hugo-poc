---
layout: "image"
title: "Laserdiode 3V"
date: "2013-04-19T17:54:03"
picture: "IMG_9841.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["3V", "Laserdiode"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36852
- /detailsd8c6.html
imported:
- "2019"
_4images_image_id: "36852"
_4images_cat_id: "2737"
_4images_user_id: "1359"
_4images_image_date: "2013-04-19T17:54:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36852 -->
3V Laserdiode in einem 30x15x15mm Gehäuse
