---
layout: "image"
title: "Laserdiode mit Ablenkspiegeln"
date: "2013-04-21T08:03:03"
picture: "IMG_9841_2.jpg"
weight: "4"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Laser", "Ablenkeinheit", "Laserdiode"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36855
- /details6bd7.html
imported:
- "2019"
_4images_image_id: "36855"
_4images_cat_id: "2737"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36855 -->
Laserdiode mit Ablenkprototyp (2x Oberflächenbedampfte Laserspiegel)
die beiden Achsen werden später mit Motoren bzw. Galvano-Einheiten um ca 45-90 Grad drehen können und somit den Strahl in X und Y - Richtung getrennt voneinander abgelenken.
