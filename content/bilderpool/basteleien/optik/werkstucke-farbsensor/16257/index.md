---
layout: "image"
title: "Werkstücke bunt"
date: "2008-11-11T21:54:21"
picture: "werkstuecke2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/16257
- /detailse943-2.html
imported:
- "2019"
_4images_image_id: "16257"
_4images_cat_id: "1465"
_4images_user_id: "373"
_4images_image_date: "2008-11-11T21:54:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16257 -->
Natürlich lassen die sich wunderbar mit dem ft-Fototransistor (oder dem Farbsensor, wenn man einen hat) am Robo-Interface unterscheiden.
