---
layout: "overview"
title: "Selbstbau Laserstrahler"
date: 2020-02-22T07:46:04+01:00
legacy_id:
- /php/categories/1523
- /categoriesf31b.html
- /categories1386.html
- /categoriese50c.html
- /categoriese381.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1523 --> 
Selbstbau eines Punkt-Laserstrahlers für den Einbau und Betrieb in fischertechnik Modellen für Lichtschranken und Auslösung von Schaltvorgängen.