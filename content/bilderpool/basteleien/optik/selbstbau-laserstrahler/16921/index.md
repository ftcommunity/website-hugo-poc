---
layout: "image"
title: "Laserstrahler 04"
date: "2009-01-07T15:12:38"
picture: "laserstrahler05.jpg"
weight: "5"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16921
- /details4cbe.html
imported:
- "2019"
_4images_image_id: "16921"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16921 -->
Der in den Störlichttubus eingepresste Punkt-Laserstrahler wird durch das aufgedrückte Stück Schrumpfschlauch zentriert und in der Achse des Störlichttubuses ausgerichtet.
