---
layout: "comment"
hidden: true
title: "8195"
date: "2009-01-08T07:46:10"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ich brauche zwar selbst keinen Laserstrahl in FT, aber diese erstklassig recherchierte, bebilderte und beschriebene Bauanleitung verdient wirklich mal ein Extra-Lob!!!

Ganz großartig, vor allem auch die grandiose Klebeschablone und die Warnhinweise am Ende! Da hat sich qincym wirklich Gedanken gemacht. Ganz großes Kino!

Gruß, Thomas