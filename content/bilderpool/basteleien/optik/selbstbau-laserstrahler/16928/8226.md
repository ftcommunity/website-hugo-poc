---
layout: "comment"
hidden: true
title: "8226"
date: "2009-01-10T07:33:52"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
das kann durchaus passieren. Ein Fototransistor ist nicht so einfach anzuschließen, wie ein Fotowiderstand, da er eine Polung und einen gewissen Arbeitspunkt hat.
Am besten geht es in einer Spannungsteilerschaltung. Daß der Fototransistor gut arbeitet erkennt man dann an der Geschwindigkeit. 1 MHz ist durchaus drin.

Als dann
Remadus