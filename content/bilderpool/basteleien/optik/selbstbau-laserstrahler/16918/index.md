---
layout: "image"
title: "Laserstrahler 01a"
date: "2009-01-07T15:12:37"
picture: "laserstrahler02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Volker-James Münchhof (qincym)"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16918
- /details5c40.html
imported:
- "2019"
_4images_image_id: "16918"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16918 -->
Zur Auswahl kam der Punkt-Laserstrahler OLP503P158550 (z.B. bei Conrad unter der Bestellnummer 158550 erhältlich), der wie ein Laserpointer zur Laserklasse 2 gehört. Auf dem gezeigten Datenblatt fehlt die Angabe des Stromes von 30 mA bei einer angelegten Spannung von 3 Volt. Soll der Punkt-Laserstrahler mit der bei fischertechnik üblichen Spannung von 9 Volt betrieben werden, ist ein Vorwiderstand von 200 Ohm notwendig, der bei einem Strom von 30 mA einen Spannungsabfall von 6 Volt erzeugt.  Die Abmessungen des Punkt-Laserstrahlers zeigen, dass der Punkt-Laserstrahler genau in das hintere Ende des Störlichttubuses, ft# 31363, eingepasst werden kann.

Das gezeigte Datenblatt kann unter:

http://www2.produktinfo.conrad.com/datenblaetter/150000-174999/158550-da-01-en-LASERMODUL_3V_1MW_650NM.pdf

eingesehen werden.
