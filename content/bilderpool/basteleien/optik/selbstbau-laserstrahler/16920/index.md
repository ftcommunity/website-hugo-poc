---
layout: "image"
title: "Laserstrahler 03"
date: "2009-01-07T15:12:38"
picture: "laserstrahler04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16920
- /details11b2.html
imported:
- "2019"
_4images_image_id: "16920"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16920 -->
Jetzt ist das Stück Schrumpfschlauch auf den Punkt-Laserstrahler aufgeschoben. Diese Einheit kann nun in den Störlichttubus von hinten eingepresst werden.
