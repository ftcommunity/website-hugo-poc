---
layout: "image"
title: "Laserstrahler 08"
date: "2009-01-07T15:12:38"
picture: "laserstrahler09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- /php/details/16925
- /detailsaa76.html
imported:
- "2019"
_4images_image_id: "16925"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16925 -->
Der Störlichttubus mit dem Punkt-Laserstrahler wird vorn mit einer Störlichtkappe, z.B. ft# 31362, versehen und wie das Bild zeigt mit etwas Sekundenkleber auf den Baustein 5 mm aufgeklebt.