---
layout: "image"
title: "Adapter"
date: "2006-09-01T21:39:36"
picture: "Adapter.jpg"
weight: "2"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
schlagworte: ["Sharp", "Entfernungssensor", "Spannungsregler", "Interface"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/6766
- /detailsaf7a-2.html
imported:
- "2019"
_4images_image_id: "6766"
_4images_cat_id: "650"
_4images_user_id: "46"
_4images_image_date: "2006-09-01T21:39:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6766 -->
Der kompakte Aufbau etwas näher. Der Spannungsregler versteckt sich ein wenig im Hintergrund. Mit dieser Schaltung paßt der Sharp-Infrarot-Entfernungssensor an das Interface und liefert Analogwerte zwischen 40 und 240, wenn kein Verstärker dazukommt.
