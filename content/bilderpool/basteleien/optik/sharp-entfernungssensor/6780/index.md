---
layout: "image"
title: "Robo Interface D1 input"
date: "2006-09-04T16:33:54"
picture: "DSCF0180a.jpg"
weight: "6"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
schlagworte: ["sensor", "analogue", "distance"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/6780
- /details01f8.html
imported:
- "2019"
_4images_image_id: "6780"
_4images_cat_id: "650"
_4images_user_id: "385"
_4images_image_date: "2006-09-04T16:33:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6780 -->
This picture shows the signal on the D1 input when a signal is applied. Every 10 ms, the 5V supply is briefly interrupted in order to read the analogue input value, in this case the value of a potmeter.

This behaviour is only visible in the "Distance sensor mode", not in the "Analogue input mode". One way to get your intetrface into this mode is by running if3_Diag_V74_Betatest.exe, available in FtCComp V1.66a. Note that stoping this program does not stop this behaviour. Also, the RoboPro Interface Test does not reset this behaviour. Power cycling your interface does the trick.

I happened to have two Sharp distance sensors connected to the D inputs, with no resistors in between. They seem to have survived this experiment, luckily.
