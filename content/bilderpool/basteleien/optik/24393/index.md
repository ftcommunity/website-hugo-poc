---
layout: "image"
title: "Ein Selbstbauspiegel"
date: "2009-06-15T22:51:46"
picture: "selbstbauspiegel2.jpg"
weight: "2"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- /php/details/24393
- /details0fea.html
imported:
- "2019"
_4images_image_id: "24393"
_4images_cat_id: "1672"
_4images_user_id: "969"
_4images_image_date: "2009-06-15T22:51:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24393 -->
Und das ganze von der Seite.