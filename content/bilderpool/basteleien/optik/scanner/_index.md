---
layout: "overview"
title: "Scanner"
date: 2020-02-22T07:46:09+01:00
legacy_id:
- /php/categories/2196
- /categories7b91.html
- /categoriesa018.html
- /categories1f4b.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2196 --> 
Ein Scanner aus Fischertechnik der ein Din A6 Blatt (So klein das ein scann-Vorgang nicht so lange dauert)
mit einem Farbsensor abfährt und dann das gescannte Bild als Bitmap speichert.