---
layout: "image"
title: "Elektronik Baukasten"
date: "2016-06-26T11:11:25"
picture: "20151130_0001231.jpg"
weight: "8"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43785
- /detailsc3ef.html
imported:
- "2019"
_4images_image_id: "43785"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43785 -->
Ich habe den Elektronik Baukasten in die alten UT bzw Hobby 
Kästen sortiert.