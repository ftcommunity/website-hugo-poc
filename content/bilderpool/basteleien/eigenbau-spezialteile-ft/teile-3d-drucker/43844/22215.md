---
layout: "comment"
hidden: true
title: "22215"
date: "2016-07-07T07:11:48"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo H.A.R.R.Y.,

auch der "BS 15 nur Nut" wäre schon klasse gewesen.
Leider wurde auch dieser nie umgesetzt.
Ich würde es ausdrücklich begrüßen, wenn mal ein entsprechendes Bauteil aufgelegt werden würde. Oftmals ist der eine Zapfen am Ende eines Bauteiles (Grundbausteins,  Statikteils oder Winkelsteins) eben störend.

Also: Her damit :)

Gruß

Lurchi