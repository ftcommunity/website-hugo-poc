---
layout: "image"
title: "3D-Gedruckter Zapfen"
date: "2016-09-10T14:26:54"
picture: "bild1.jpg"
weight: "5"
konstrukteure: 
- "3D-Drucker"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/44344
- /detailsbda4.html
imported:
- "2019"
_4images_image_id: "44344"
_4images_cat_id: "3272"
_4images_user_id: "1624"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44344 -->
Vor kurzem bekam ich meinen 3D-Drucker - da war ich auch ganz angefixt, ft Zubehör zu drucken. Das hier ist mein erster Versuch (eines ft Zubehörteils, nicht mein erster Druck überhaupt), es soll ein Teil sein, welches in meinem Flipper die aus Draht bestehenden Rampen halten sollte. Dieses Versuchsteil dient aber erstmal dazu, zu schauen wie sich der Zapfen im ft-System macht
Der Zapfen ist ein "Längs-Zapfen", also lässt sich das Teil nur in zwei Richtungen auf eine Nut schieben, statt der üblichen vier. Dafür brauchts hierfür kein Stützmaterial und der Zapfen ist stabiler.