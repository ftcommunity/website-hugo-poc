---
layout: "comment"
hidden: true
title: "21634"
date: "2016-01-31T22:17:36"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Aber ... isch 'abe doch gar keine Arduino!

Mit einem Muster oder einer Schablone ließe sich da schon was machen. Die Dateien (*.pk2 von PTC Creo) oder *.STL (aber... passend für den Velleman 8400 mit 0,35 mm Düse -- das macht schon etwas aus) gebe ich gerne her.

Gruß,
Harald