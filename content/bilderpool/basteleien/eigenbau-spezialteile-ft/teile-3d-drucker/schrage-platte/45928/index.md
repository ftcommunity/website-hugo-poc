---
layout: "image"
title: "Scheune 01"
date: "2017-06-04T12:09:54"
picture: "schraegeplatte3.jpg"
weight: "3"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45928
- /details2627.html
imported:
- "2019"
_4images_image_id: "45928"
_4images_cat_id: "3410"
_4images_user_id: "1355"
_4images_image_date: "2017-06-04T12:09:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45928 -->
Da wir den Teil öfters ausgedruckt haben, konnten wir auch damit eine Scheune bauen.