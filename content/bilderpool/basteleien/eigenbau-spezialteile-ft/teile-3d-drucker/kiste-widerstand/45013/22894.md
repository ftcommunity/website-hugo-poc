---
layout: "comment"
hidden: true
title: "22894"
date: "2017-01-10T14:24:37"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

könnt ihr nicht einfach die Boxen vom 2-6 Programm (Vorstufe) nehmen?
Art nr. (Beispiel)  38289, 38287, 35299, 38442, 38440 ....
Die bieten sich ja hier gut an. Ich habe die schon mehrfach verwendet.