---
layout: "image"
title: "Kiste mit Widerstand 04"
date: "2017-01-07T20:22:20"
picture: "kistemitwiderstand4.jpg"
weight: "4"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/45013
- /details7994.html
imported:
- "2019"
_4images_image_id: "45013"
_4images_cat_id: "3348"
_4images_user_id: "1355"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45013 -->
Zusammengesetzter Bauteil. Damit der Deckel nicht so leicht runtergeht, habe ich ihn noch mit einem Modellbaukleber an die Kiste angeklebt.