---
layout: "image"
title: "Dreiecksplatte Computing und Schieber"
date: "2016-09-09T19:32:07"
picture: "dgedruckteteile2.jpg"
weight: "2"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer (allsystemgmbh)"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- /php/details/44343
- /detailsbf15.html
imported:
- "2019"
_4images_image_id: "44343"
_4images_cat_id: "3274"
_4images_user_id: "1688"
_4images_image_date: "2016-09-09T19:32:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44343 -->
Dreiecksplatte Computing Trainings-Roboter 32317
https://ft-datenbank.de/details.php?ArticleVariantId=fb90e6e9-eabc-463e-a671-6f762dd09b31

Schieber 35970
https://ft-datenbank.de/details.php?ArticleVariantId=af6fa319-6ccb-49d2-9488-825cfdd5584b

...die stl. Files sind im FT-Designer enthalten
