---
layout: "comment"
hidden: true
title: "22866"
date: "2017-01-03T21:59:39"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Oh, und du meinst, dass ich die in gewohntem Maße überwältigende, stürmische (und bisweilen erdrückend überschwängliche) Resonanz aus Tumlingen so einfach aushalten kann? Bei sowas kann leicht das Ego überlaufen!

Gruß,
Harald
(der erst einmal die Antwort auf eine lange eMail mit Teile-Vorschlägen aus 2003 abwartet!)