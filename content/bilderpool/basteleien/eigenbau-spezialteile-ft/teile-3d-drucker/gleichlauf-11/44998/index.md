---
layout: "image"
title: "Sonnenrad Z10"
date: "2017-01-01T21:43:29"
picture: "gl3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/44998
- /details01e1.html
imported:
- "2019"
_4images_image_id: "44998"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44998 -->
Das Sonnenrad Z10 wird aus der ft-Rastkurbel 35071 durch spanabhebende Verfahren gewonnen. Dem PLA aus dem 3D-Drucker traue ich nicht solche Kräfte zu, wie sie das ft-Teil ertragen kann. Ich habe einen Schaft dran gelassen, der jetzt in einem Stück Messingrohr versteckt ist. Dieses Rohrstück ist nötig, um vom 7 mm-Schaft des Zahnrads auf die 8 mm Innendurchmesser des Kugellagers 8x12x4 zu kommen.

Das Zahnrad ist innen auf 4 mm runden Querschnitt aufgebohrt und hat eine Querbohrung mit Gewinde m2,5 und einer Madenschraube darin. Damit wird es auf der Antriebswelle fest gezogen.
