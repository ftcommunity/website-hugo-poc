---
layout: "image"
title: "Teile sind fertig"
date: "2017-01-01T21:43:29"
picture: "gl5.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45000
- /details0a73.html
imported:
- "2019"
_4images_image_id: "45000"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45000 -->
Ich habe lange genug versucht, die Evolventen-Zahnform aus dem Drucker zu holen -- es ist uferlos. Klar, die Zahnform kann man zeichnen oder von Turbocad übernehmen. Aber der Drucker / Slicer macht sich darüber eigene Vorstellungen, und am Ende ist dann nur die Düse verstopft, weil das Filament beim Drucken dauernd mit minimalem Vorschub vor und zurück bewegt wird. Mittlerweile bin ich froh, wenn die Zähne ohne Nachbearbeitung "flutschen".
