---
layout: "image"
title: "Gleichlauf_11b1.jpg"
date: "2017-01-15T19:39:37"
picture: "Gleichlauf_11b1.jpg"
weight: "8"
konstrukteure: 
- "China?"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Eitech", "modul1"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45040
- /detailsd3b5.html
imported:
- "2019"
_4images_image_id: "45040"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-15T19:39:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45040 -->
Ludger hat mir da einen heißen Tip gegeben: Es gibt die Z10 modul 1 mit Klemmring auch in "fertig" zu kaufen:

http://www.eiwa-es.de/de_robot2012/artikeldetails.php?aid=105

... und sie sind Teil eines Systems, bei dem man mehrere Zahnräder auf achteckige Träger stecken kann, die entweder lose auf der Welle sitzen (blau, links oben schon montiert) oder geklemmt sind (schwarz, rechts oben).

Mich hat dieser Schritt nach vorn allerdings wieder zurück geworfen. Die Zahnräder sind dicker als mein "Standard", der Bund an der Madenschraube muss abgedreht werden, um in ein Lager zu passen, und die Zähne sind bei mir auch so 1 mm kürzer, d.h. das Z10 passt so nicht in den Planetensatz hinein. Mache ich die gedruckten Zahnräder größer, passt aber das innenverzahnte Z38 nicht mehr in den 45er Durchmesser...
