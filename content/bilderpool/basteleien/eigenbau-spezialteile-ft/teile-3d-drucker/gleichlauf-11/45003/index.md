---
layout: "image"
title: "Gleichlauf_117.jpg"
date: "2017-01-01T22:25:36"
picture: "Gleichlauf_117.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/45003
- /detailse9a0.html
imported:
- "2019"
_4images_image_id: "45003"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T22:25:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45003 -->
Das passiert, wenn der Brim (Haftrand) zu klein ist oder gar nicht klebt (z.B. wenn das Krepppapier nicht rechtzeitig getauscht wurde). Es hätte auch noch schlimmer kommen können: dann löst sich das ganze Werk von der Platte, rutscht an den Rand oder fällt herunter, und die Düse druckt aus dem restlichen Inhalt der STL-Datei ein "Vogelnest" aus frei schwebenden Fäden.
