---
layout: "image"
title: "Stromverteiler"
date: "2018-06-13T16:43:05"
picture: "stromverteiler1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
fotografen:
- "Dirk Wölffel (DirkW)"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- /php/details/47688
- /detailsfd08.html
imported:
- "2019"
_4images_image_id: "47688"
_4images_cat_id: "3518"
_4images_user_id: "2303"
_4images_image_date: "2018-06-13T16:43:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47688 -->
Hier ein Stromverteiler für 9 Volt. Vorne die 3,5 mm Klinke für das Netzteil
Dahinter jeweils 2 Buchsen zum anschließen. Wenn ihr zwei TX(T) Controller 
über ein Netztteil ansteuern möchtet.
