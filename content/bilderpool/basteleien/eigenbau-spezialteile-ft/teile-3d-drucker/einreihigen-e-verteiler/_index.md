---
layout: "overview"
title: "Einreihigen E-Verteiler"
date: 2020-02-22T07:46:36+01:00
legacy_id:
- /php/categories/3353
- /categoriesce70.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3353 --> 
Nun habe ich mit dem 3D-Drucker einen einreihigen E-Verteiler (45x15x15) konstruiert, ausgedruckt und zusammengebaut.