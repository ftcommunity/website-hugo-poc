---
layout: "image"
title: "Zusammengebaut von Vorne"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok2.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/47575
- /detailscbaa.html
imported:
- "2019"
_4images_image_id: "47575"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47575 -->
Die Lok von Vorne