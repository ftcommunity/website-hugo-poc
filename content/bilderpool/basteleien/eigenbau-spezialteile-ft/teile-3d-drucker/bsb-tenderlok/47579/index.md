---
layout: "image"
title: "Seitenteil von außen"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok6.jpg"
weight: "6"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- /php/details/47579
- /details2546.html
imported:
- "2019"
_4images_image_id: "47579"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47579 -->
... und hier von außen