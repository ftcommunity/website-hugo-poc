---
layout: "overview"
title: "BSB Tenderlok"
date: 2020-02-22T07:46:38+01:00
legacy_id:
- /php/categories/3509
- /categories6be2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3509 --> 
Mein Sohn und ich haben versucht mit dem 3D-Drucker die BSB-Tenderlok aus dem Jahr 1980 zu bauen.

Unter https://www.youtube.com/watch?v=5jByyONMSr4 seht ihr die BSB-Dampflok und die von uns gebaute BSB-Tenderlok.
