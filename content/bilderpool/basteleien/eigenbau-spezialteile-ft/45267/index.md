---
layout: "image"
title: "Werbedisplay"
date: "2017-02-20T17:32:24"
picture: "2017-02-08_19.18.57.jpg"
weight: "32"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45267
- /detailsc85e.html
imported:
- "2019"
_4images_image_id: "45267"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-02-20T17:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45267 -->
Werbedisplays mit Windmühle mit Solar Antrieb, Riesenrad mit Solarantrieb und Master Galaxy mit Beleuchtung.