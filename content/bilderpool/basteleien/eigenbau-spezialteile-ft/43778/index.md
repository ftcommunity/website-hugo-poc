---
layout: "image"
title: "Rundumleuchten mit Leuchtstein"
date: "2016-06-26T11:11:24"
picture: "20160514_1736361.jpg"
weight: "1"
konstrukteure: 
- "Die Idee ist nicht von mir"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43778
- /details3439.html
imported:
- "2019"
_4images_image_id: "43778"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43778 -->
Ich habe auf einen Leuchtstein ein Rundumleuchte befestigt die 
eigentlich für den Modellbau gemacht ist.Es ist ein Gleichrichter verbaut daher ist es egal wie die Polarität ist.