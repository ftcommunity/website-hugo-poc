---
layout: "image"
title: "Servo Fahrtenregler im Batteriegehäuse"
date: "2017-04-11T20:34:31"
picture: "2017-04-10_16.47.48.jpg"
weight: "39"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45731
- /details7f57.html
imported:
- "2019"
_4images_image_id: "45731"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-04-11T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45731 -->
Damit kann der Servoausgang der Fernsteuerung als Motorausgang genutzt werden.Zusätzlich gibt es noch Bremslicht wenn der Hebel schnell los gelassen wird und Rückfahrlicht.