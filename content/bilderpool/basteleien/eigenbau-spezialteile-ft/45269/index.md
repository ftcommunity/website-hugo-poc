---
layout: "image"
title: "Prog. Schrittmotorensteuerung"
date: "2017-02-20T17:32:24"
picture: "2017-02-08_09.03.18.jpg"
weight: "34"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45269
- /detailsbb69-2.html
imported:
- "2019"
_4images_image_id: "45269"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-02-20T17:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45269 -->
Von nahen