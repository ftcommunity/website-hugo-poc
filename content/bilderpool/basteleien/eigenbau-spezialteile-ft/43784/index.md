---
layout: "image"
title: "Magnetischer summer im Leuchtstein"
date: "2016-06-26T11:11:25"
picture: "20160514_1814391.jpg"
weight: "7"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43784
- /details7470.html
imported:
- "2019"
_4images_image_id: "43784"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43784 -->
