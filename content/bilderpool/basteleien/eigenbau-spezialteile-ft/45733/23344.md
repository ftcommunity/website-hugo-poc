---
layout: "comment"
hidden: true
title: "23344"
date: "2017-04-12T14:29:40"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Wo kann man den Kompressor bestellen? Wie groß ist die Vakuumleistung? Ist sie besser als die der "Vakuumpumpe" (die zwei-Zylinder Konstruktion, bei der der eine Zylinder den anderen betätigt) aus den Pneuvac / Electro Pneumatic Baukästen?

Gruß,
David