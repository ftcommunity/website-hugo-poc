---
layout: "image"
title: "Laser im Motorgehäuse"
date: "2017-09-27T18:24:07"
picture: "20170926_221202.jpg"
weight: "52"
konstrukteure: 
- "Bernd L"
fotografen:
- "Bernd L"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/46421
- /detailsddb3.html
imported:
- "2019"
_4images_image_id: "46421"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-09-27T18:24:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46421 -->
