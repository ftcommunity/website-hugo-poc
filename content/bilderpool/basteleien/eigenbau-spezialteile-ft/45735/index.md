---
layout: "image"
title: "Innenleben vom Servo Fahrtenregler"
date: "2017-04-12T16:14:03"
picture: "2017-04-07_12.08.55.jpg"
weight: "43"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45735
- /details3466.html
imported:
- "2019"
_4images_image_id: "45735"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-04-12T16:14:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45735 -->
Kern ist ein Fahrtenregler von sol expert mit einer Betriebsspannung von 6-12 volt.An den Lichtausgängen sind Reedrelais damit Lampen betrieben werden können.