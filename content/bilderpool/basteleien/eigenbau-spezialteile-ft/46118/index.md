---
layout: "image"
title: "Pneumatik Druckschalter mit Druckanzeige"
date: "2017-08-12T10:38:20"
picture: "2017-07-31_22.03.161.jpg"
weight: "45"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/46118
- /details37d5.html
imported:
- "2019"
_4images_image_id: "46118"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-08-12T10:38:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46118 -->
Druckschalter für den Pneumatik Kompressor mit beleuchteter
Anzeige.Der Kompressor stellt sich bei 0,35 bar ab und springt bei 0,25 wieder an.Das wird durch blaue Kontroll Leds angezeigt.
Der Schalter ist einstellbar