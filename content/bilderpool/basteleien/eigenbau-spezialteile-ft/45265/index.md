---
layout: "image"
title: "Prog. Schrittmotor Steuerung"
date: "2017-02-20T17:32:24"
picture: "2017-02-20_08.28.15.jpg"
weight: "30"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45265
- /detailsaf01.html
imported:
- "2019"
_4images_image_id: "45265"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-02-20T17:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45265 -->
Es ist eine Dual Schrittmotorensteuerung von Millumi verbaut.2 Motoren können gleichzeitig betrieben werden.200 Schritte können programmiert werden und dann einmal oder endlos abgespielt werden.Alle Bedienelemente sind extern mit Tastern ansteuerbar.Natürlich kann man auch manuell die Motoren fahren.Die Max Geschwindigkeit wie auch Brems-bzw Anfahrzeit ist für jeden Motor  einstellbar.Es können auch Endkontakte angeschlossen werden bis wie weit der Antrieb fahren soll.Bestens geeignet für Kirmesmodelle,wie den Ranger oder das Kettenkarussell.
Ich habe ein Video gedreht ,da kann man sehen wie es funktioniert.
Hier der Link:https://www.youtube.com/watch?v=qFP6enwYHHI