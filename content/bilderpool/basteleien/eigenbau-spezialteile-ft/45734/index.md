---
layout: "image"
title: "Kompressor mit Druck und Vacuumanschluss 2"
date: "2017-04-11T20:34:31"
picture: "2017-04-10_16.52.27.jpg"
weight: "42"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45734
- /details88fc.html
imported:
- "2019"
_4images_image_id: "45734"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-04-11T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45734 -->
