---
layout: "comment"
hidden: true
title: "23083"
date: "2017-02-20T20:37:39"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
WOW!! Das ist mal eine Ansage an die Fischerwerke! Die Boards und die Sensormodule können mit denen aus dem Schwarzwald optisch und in der Handhabung mithalten und übertreffen deren Funktionen um Welten.

Gruß, David