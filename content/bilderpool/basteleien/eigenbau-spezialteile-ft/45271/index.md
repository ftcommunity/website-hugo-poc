---
layout: "image"
title: "Arduino und C - Control Steuerung"
date: "2017-02-20T17:32:24"
picture: "20160925_112418.jpg"
weight: "36"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/45271
- /details29fe.html
imported:
- "2019"
_4images_image_id: "45271"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-02-20T17:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45271 -->
Arduino und C Controlmodule, Motormodul, Relaismodul, Displaymodul, Tastaturmodul, Sensoren, Led, Touchsensoren