---
layout: "image"
title: "Relais Board im Powerblock"
date: "2016-07-25T14:24:24"
picture: "2016-07-24_19.53.391.jpg"
weight: "23"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/43960
- /details9b0f.html
imported:
- "2019"
_4images_image_id: "43960"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43960 -->
Relais Shield eingebaut.Es stehen 4 wechler Relais zu verfügung. 
Es ist möglich die Relais manuell zu betätigen.