---
layout: "image"
title: "Forum Ausgabe '0'"
date: "2016-08-09T09:27:04"
picture: "Forum_0_1_klein.jpg"
weight: "38"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/44174
- /details6546.html
imported:
- "2019"
_4images_image_id: "44174"
_4images_cat_id: "463"
_4images_user_id: "10"
_4images_image_date: "2016-08-09T09:27:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44174 -->
Die "blaue Mauritius" unter den Forum-Heften.
Ein Fehldruck anscheinend.
