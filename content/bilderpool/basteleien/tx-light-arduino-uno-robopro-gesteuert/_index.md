---
layout: "overview"
title: "TX Light Arduino Uno der von RoboPro gesteuert wird"
date: 2020-02-22T07:46:59+01:00
legacy_id:
- /php/categories/3371
- /categories15ff.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3371 --> 
TX Light
Arduino Uno der von RoboPro gesteuert werden kann.
Lasten wie Motoren oder Lampen können mit dem Motorshield 2.3 von Adafruit betrieben werden.
Wenn nur LEDs oder etwas Elektronk betrieben werden soll geht es auch ohne Motorshield.
Durch Auflöten eines 20 pol Steckers, 10 Widerstände und ein paar Buchsen hat man ein Interface fast wie das alte parallele oder serielle fischertechnik Interface.

Man hat 
8 Mausgänge
 davon sind 4 M-Ausgänge für Lampen oder Motoren am Motorshield oder 8 O-Ausgänge am Arduino Uno Achtung diese aber nur bis 20mA !!
8 Eingänge
davon 4 digitale EIngänge und 4 digitale/analoge Eingänge



