---
layout: "image"
title: "TX Light"
date: "2017-02-18T22:45:28"
picture: "txlightarduinounodervonroboprogesteuertwird1.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/45260
- /details71eb.html
imported:
- "2019"
_4images_image_id: "45260"
_4images_cat_id: "3371"
_4images_user_id: "34"
_4images_image_date: "2017-02-18T22:45:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45260 -->
Tx Light Arduino Uno mit Motorshield 2.3 von Adafruit und
fischertechnik-Bausatz von CVK "Schildkröte" 67068 von 1991
mit 20pol Kabel

H. Howey
fishfriend
