---
layout: "image"
title: "CNC-Fräse"
date: "2014-03-17T21:12:53"
picture: "fraese1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "Thomas Kaltenbrunner (Kalti)"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/38475
- /details47df.html
imported:
- "2019"
_4images_image_id: "38475"
_4images_cat_id: "2870"
_4images_user_id: "1342"
_4images_image_date: "2014-03-17T21:12:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38475 -->
Bild meiner CNC-Fräse im aufgeklapptem Zustand
