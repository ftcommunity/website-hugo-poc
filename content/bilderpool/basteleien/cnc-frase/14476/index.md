---
layout: "image"
title: "Bild 04 - Detail Antrieb Y-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_04_-_Detail_Antrieb_Y-Achse.jpg"
weight: "7"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14476
- /details08bb.html
imported:
- "2019"
_4images_image_id: "14476"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14476 -->
Detail Antrieb Y-Achse
