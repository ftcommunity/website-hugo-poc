---
layout: "image"
title: "Bild 08 - Detail Fraeser-Befestigung an X-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_08_-_Detail_Fraeser-Befestigung_an_X-Achse.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14472
- /details858d.html
imported:
- "2019"
_4images_image_id: "14472"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14472 -->
Detail Fräser-Befestigung an X-Achse
