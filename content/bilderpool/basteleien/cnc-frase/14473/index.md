---
layout: "image"
title: "Bild 07 - Detail Rollenfuehrung X-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_07_-_Detail_Rollenfuehrung_X-Achse.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14473
- /details3d25.html
imported:
- "2019"
_4images_image_id: "14473"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14473 -->
Detail Rollenfuehrung X-Achse

Die Rollen tragen das Gewicht der Holzkonstruktion.

Beim Holz sollte man dicke Sperrholzplaten nehmen (1 cm dick). Sonst bekommt man Probleme mit den Schrauben. Sperrholz bricht dann nämlich.
