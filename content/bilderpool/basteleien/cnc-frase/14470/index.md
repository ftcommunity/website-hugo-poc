---
layout: "image"
title: "Bild 10 - Gefrästes Logo in Nixie-Uhr"
date: "2008-05-05T16:03:45"
picture: "Bild_10_-_Gefrstes_Logo_in_Nixie-Uhr.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14470
- /detailsb113.html
imported:
- "2019"
_4images_image_id: "14470"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14470 -->
CNC-Fräse aus umgebautem Plotter 85
