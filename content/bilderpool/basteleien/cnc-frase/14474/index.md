---
layout: "image"
title: "Bild 06 - Detail Eindrehmutter für Z-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_06_-_Detail_Eindrehmutter_fr_Z-Achse.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/14474
- /detailsaf54.html
imported:
- "2019"
_4images_image_id: "14474"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14474 -->
Detail Eindrehmutter für Z-Achse

Diese kann man ins Holz einschrauben. Auf der einen Seite hat diese Mutter ein M4er Gewinde. Dadurch kann der Gewindestab die Z-Achse heben und senken.
