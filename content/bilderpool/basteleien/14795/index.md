---
layout: "image"
title: "Schlüssel2"
date: "2008-07-01T20:39:54"
picture: "sleutel2.jpg"
weight: "8"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/14795
- /details9702.html
imported:
- "2019"
_4images_image_id: "14795"
_4images_cat_id: "463"
_4images_user_id: "764"
_4images_image_date: "2008-07-01T20:39:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14795 -->
