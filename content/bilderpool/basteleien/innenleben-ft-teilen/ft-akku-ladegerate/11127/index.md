---
layout: "image"
title: "ft-Akku-Lader aktuelle Version - Bild 3"
date: "2007-07-18T18:34:53"
picture: "ftladegeraete3_2.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11127
- /details8e64.html
imported:
- "2019"
_4images_image_id: "11127"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-18T18:34:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11127 -->
dicht gepackte diskreter Aufbau
