---
layout: "image"
title: "ft-Akku-Lader aktuelle Version - Bild 2"
date: "2007-07-18T18:34:53"
picture: "ftladegeraete2_2.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11126
- /detailsb8e3.html
imported:
- "2019"
_4images_image_id: "11126"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-18T18:34:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11126 -->
nach Entfernung der Spezialschrauben
