---
layout: "image"
title: "ft-Akku-Lader ältere Version - Bild 2"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete4.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11059
- /details8068.html
imported:
- "2019"
_4images_image_id: "11059"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11059 -->
IC oben im Adapter (vermutlich) Festspannungsregler Aufschrift "LM317T WKOGP9819 CHINA"
