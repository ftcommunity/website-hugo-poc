---
layout: "image"
title: "ft-Akku-Lader aktuelle Version - Bild 1"
date: "2007-07-18T18:34:53"
picture: "ftladegeraete1_2.jpg"
weight: "7"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/11125
- /details97f6.html
imported:
- "2019"
_4images_image_id: "11125"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-18T18:34:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11125 -->
Lade Elektronik nur im Steckernetzteil Gehäuse, diese Version war im zuletzt gekauften Akku-Set und daher nehme ich mal an das dies die aktuelle ist :)
