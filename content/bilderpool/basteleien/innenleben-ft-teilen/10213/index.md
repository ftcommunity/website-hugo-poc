---
layout: "image"
title: "Power Motor"
date: "2007-04-29T19:59:11"
picture: "platinen15.jpg"
weight: "16"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10213
- /details5134.html
imported:
- "2019"
_4images_image_id: "10213"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10213 -->
Es hat keine grosse Platine, aber es hat eine.