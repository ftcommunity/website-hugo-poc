---
layout: "image"
title: "Vorderseite Version 1"
date: "2007-02-26T09:03:37"
picture: "image2.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/9155
- /details6890.html
imported:
- "2019"
_4images_image_id: "9155"
_4images_cat_id: "845"
_4images_user_id: "120"
_4images_image_date: "2007-02-26T09:03:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9155 -->
vom Parallel-Interface für den C64
Bitte nicht an der fehlenden Diode stören :) Der Schaden stammt noch vom Vorbesitzer und wird noch behoben...
