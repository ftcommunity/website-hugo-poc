---
layout: "image"
title: "Blick ins Innere (1)"
date: "2018-10-17T21:54:48"
picture: "innereiendesfischertechnikpowercontrollers2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48244
- /details2f42.html
imported:
- "2019"
_4images_image_id: "48244"
_4images_cat_id: "3540"
_4images_user_id: "104"
_4images_image_date: "2018-10-17T21:54:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48244 -->
Auffällig ist der große Kühlkörper darin.
