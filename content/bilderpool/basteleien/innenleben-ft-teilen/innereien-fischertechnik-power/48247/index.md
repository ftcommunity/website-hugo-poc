---
layout: "image"
title: "Blick auf den Rest"
date: "2018-10-17T21:54:48"
picture: "innereiendesfischertechnikpowercontrollers5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/48247
- /detailsd61a.html
imported:
- "2019"
_4images_image_id: "48247"
_4images_cat_id: "3540"
_4images_user_id: "104"
_4images_image_date: "2018-10-17T21:54:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48247 -->
Vielleicht erkennt ja noch jemand was Interessantes.
