---
layout: "image"
title: "Differenzial 31043.jpg"
date: "2014-05-24T14:17:31"
picture: "IMG_0631crop.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/38836
- /details8cbd.html
imported:
- "2019"
_4images_image_id: "38836"
_4images_cat_id: "2007"
_4images_user_id: "4"
_4images_image_date: "2014-05-24T14:17:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38836 -->
Irgendwo im Fahrzeug war ein ungewollter beidseitiger Freilauf drin. Als auch das Nachziehen aller Klemmteile nichts fruchtete, fiel der Blick aufs Differenzial: da hatte sich das eine Kegelzahnrad von seinem Rändelsitz gelöst.
