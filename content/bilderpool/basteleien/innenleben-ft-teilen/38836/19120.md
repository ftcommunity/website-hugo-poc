---
layout: "comment"
hidden: true
title: "19120"
date: "2014-06-05T16:29:53"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hi,
das lässt sich sogar ohne Werkzeug wieder "ganz" machen: 
- mit sanfter Gewalt das Zahnrad wieder auf seinen Rändelsitz befördern,
- Achse einstecken
- Deckel wieder hinein klipsen.

Der schwierigste Teil ist tatsächlich, den Deckel heraus zu bekommen. Ich bin mit dem Taschenmesser drauf los gegangen, und der Deckel hat jetzt eine Scharte an der umlaufenden Krempe. Ich hab aber aufgepasst, dass die Scharte auf der Rückseite liegt und nicht ins Bild kommt.

Gruß,
Harald