---
layout: "image"
title: "Tasten für IR Handsender von Ruwido"
date: "2007-04-29T19:59:11"
picture: "platinen13.jpg"
weight: "14"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10211
- /detailse5c4.html
imported:
- "2019"
_4images_image_id: "10211"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10211 -->
Ich erschrak richtig, als ich den Sender öffnete und die Tasten rausnahm, die sind sehr lang.