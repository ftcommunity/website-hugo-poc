---
layout: "image"
title: "Solarschalter"
date: "2007-04-29T19:59:11"
picture: "platinen04.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10202
- /detailsfcc6.html
imported:
- "2019"
_4images_image_id: "10202"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10202 -->
Nicht von der Knobloch GmbH