---
layout: "image"
title: "Motor mit Getriebebox"
date: "2016-10-27T15:50:04"
picture: "traktormotor1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/44689
- /details73b3.html
imported:
- "2019"
_4images_image_id: "44689"
_4images_cat_id: "3328"
_4images_user_id: "558"
_4images_image_date: "2016-10-27T15:50:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44689 -->
Bei 12V: 1,1A blockiert,  150mA Leerlauf
Die 25,4(:1) dürften der Untersetzung entsprechen.