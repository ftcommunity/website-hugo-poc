---
layout: "comment"
hidden: true
title: "7590"
date: "2008-10-16T17:31:28"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Die Unschärfte sieht man auch immer erst am PC.
Das LCD der Kamera reicht dazu kaum aus.
Besser immer ein kleines Stativ und Selbstauslöser verwenden.
Sowas kostet nicht viel, und Du erkennst Deine Bilder nicht wieder.