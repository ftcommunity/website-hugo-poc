---
layout: "image"
title: "IR von hinten"
date: "2007-04-29T19:59:11"
picture: "platinen06.jpg"
weight: "7"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10204
- /details8525.html
imported:
- "2019"
_4images_image_id: "10204"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10204 -->
Leider nicht ganz scharf.