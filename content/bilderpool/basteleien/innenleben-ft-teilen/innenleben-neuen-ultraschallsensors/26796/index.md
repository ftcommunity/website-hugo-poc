---
layout: "image"
title: "electronics2"
date: "2010-03-23T19:36:37"
picture: "innenlebendesneueultraschallsensor4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Ad"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26796
- /details723b.html
imported:
- "2019"
_4images_image_id: "26796"
_4images_cat_id: "1914"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26796 -->
Visible are a Maxim driver (with chargepump) to achieve enough voltage for the sender, a microprocessor and a dual opamp.