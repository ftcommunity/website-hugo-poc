---
layout: "comment"
hidden: true
title: "7591"
date: "2008-10-16T17:35:17"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Die bestückten Platinen so auf den Tepich zu legen sollte man lieber lassen.
Durch die statische Aufladung können die Bauteile nämlich beschädigt werden.