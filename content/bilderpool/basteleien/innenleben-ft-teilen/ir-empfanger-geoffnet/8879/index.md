---
layout: "image"
title: "IR Empfänger"
date: "2007-02-04T12:35:37"
picture: "irempfaenger3.jpg"
weight: "3"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8879
imported:
- "2019"
_4images_image_id: "8879"
_4images_cat_id: "806"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8879 -->
von unten. Die Motortreiber sind wie beim RoboInt TLE 4205G