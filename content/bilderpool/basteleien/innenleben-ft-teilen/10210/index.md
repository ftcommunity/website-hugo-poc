---
layout: "image"
title: "IR Handsender von Ruwido von unten"
date: "2007-04-29T19:59:11"
picture: "platinen12.jpg"
weight: "13"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10210
- /detailsb29d.html
imported:
- "2019"
_4images_image_id: "10210"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10210 -->
Es gibt hier eigentlich nichts zu sehen neben den Batteriefedern.