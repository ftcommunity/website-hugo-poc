---
layout: "image"
title: "10_20_1 + zahnräder + ausgangsachse_2"
date: "2008-01-26T13:12:32"
picture: "10_20_1__zahnrder__ausgangsachse_2.jpg"
weight: "23"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13408
- /details80a1.html
imported:
- "2019"
_4images_image_id: "13408"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13408 -->
