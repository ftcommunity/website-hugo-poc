---
layout: "image"
title: "11_8_1 motor + z8 + getriebebasis"
date: "2008-01-26T13:12:32"
picture: "11_8_1_motor__z8__getriebebasis.jpg"
weight: "24"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13409
- /detailse5f0.html
imported:
- "2019"
_4images_image_id: "13409"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13409 -->
