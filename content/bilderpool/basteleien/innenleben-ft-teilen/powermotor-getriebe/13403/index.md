---
layout: "image"
title: "5_20_1 motor + z8 + getriebebasis_1"
date: "2008-01-26T13:12:32"
picture: "5_20_1_motor__z8__getriebebasis_1.jpg"
weight: "18"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13403
- /detailscf3e.html
imported:
- "2019"
_4images_image_id: "13403"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13403 -->
