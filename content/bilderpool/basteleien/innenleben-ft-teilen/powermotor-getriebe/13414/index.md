---
layout: "image"
title: "16_50_1 + zahnräder_2."
date: "2008-01-26T13:12:32"
picture: "16_50_1__zahnrder_2.jpg"
weight: "29"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13414
- /detailsdff4.html
imported:
- "2019"
_4images_image_id: "13414"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13414 -->
