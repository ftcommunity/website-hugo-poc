---
layout: "image"
title: "21_125_1 + zahnräder_2."
date: "2008-01-26T13:12:32"
picture: "21_125_1__zahnrder_2.jpg"
weight: "34"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13419
- /details602b.html
imported:
- "2019"
_4images_image_id: "13419"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13419 -->
