---
layout: "image"
title: "34_messgeräten"
date: "2008-01-26T13:26:48"
picture: "34_messgerten.jpg"
weight: "47"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13432
- /details4b10.html
imported:
- "2019"
_4images_image_id: "13432"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:26:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13432 -->
Spanungversorgung: ELV SPS7330
Multimesgerät: ELV DMM7000
Frequentzähler: ELV Universelles Frequentzzähler-Modul, eingebaut in ein ELV 7000 gehäuse