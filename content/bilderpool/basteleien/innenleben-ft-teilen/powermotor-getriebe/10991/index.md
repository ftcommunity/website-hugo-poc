---
layout: "image"
title: "Der geöffnete Motor"
date: "2007-06-30T15:51:01"
picture: "pmgetriebe11.jpg"
weight: "11"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- /php/details/10991
- /details2ebc-2.html
imported:
- "2019"
_4images_image_id: "10991"
_4images_cat_id: "993"
_4images_user_id: "558"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10991 -->
