---
layout: "image"
title: "28_312_1 getriebe + z30-12_2"
date: "2008-01-26T13:12:33"
picture: "28_312_1_getriebe__z30-12_2.jpg"
weight: "41"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13426
- /details06b7.html
imported:
- "2019"
_4images_image_id: "13426"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13426 -->
