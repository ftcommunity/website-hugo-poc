---
layout: "image"
title: "20_125_1 + zahnräder_1"
date: "2008-01-26T13:12:32"
picture: "20_125_1__zahnrder_1.jpg"
weight: "33"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13418
- /detailsb664.html
imported:
- "2019"
_4images_image_id: "13418"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13418 -->
