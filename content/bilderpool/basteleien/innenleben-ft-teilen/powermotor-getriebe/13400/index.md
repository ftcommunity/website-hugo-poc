---
layout: "image"
title: "2_getriebedeckel für 8_1 + 20_1 + 50_1"
date: "2008-01-26T13:12:32"
picture: "2_getriebedeckel_fr_8_1__20_1__50_1.jpg"
weight: "15"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13400
- /details4d0f.html
imported:
- "2019"
_4images_image_id: "13400"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13400 -->
