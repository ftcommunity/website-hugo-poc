---
layout: "image"
title: "6_20_1 motor + z8 + getriebebasis_2"
date: "2008-01-26T13:12:32"
picture: "6_20_1_motor__z8__getriebebasis_2.jpg"
weight: "19"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
uploadBy: "pk"
license: "unknown"
legacy_id:
- /php/details/13404
- /detailse439.html
imported:
- "2019"
_4images_image_id: "13404"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13404 -->
