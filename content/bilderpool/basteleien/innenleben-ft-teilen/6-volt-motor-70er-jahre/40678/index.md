---
layout: "image"
title: "6 volt Motor ohne Rückwand"
date: "2015-03-22T10:53:43"
picture: "motor01.jpg"
weight: "1"
konstrukteure: 
- "Pim Sturm"
fotografen:
- "Pim Sturm"
schlagworte: ["Motor"]
uploadBy: "pim_s"
license: "unknown"
legacy_id:
- /php/details/40678
- /detailsff06.html
imported:
- "2019"
_4images_image_id: "40678"
_4images_cat_id: "3055"
_4images_user_id: "2398"
_4images_image_date: "2015-03-22T10:53:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40678 -->
