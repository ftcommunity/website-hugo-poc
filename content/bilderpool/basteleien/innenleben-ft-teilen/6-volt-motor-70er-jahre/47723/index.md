---
layout: "image"
title: "Motoren zerlegt"
date: "2018-07-13T18:04:58"
picture: "M7.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47723
- /details7f71.html
imported:
- "2019"
_4images_image_id: "47723"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47723 -->
Beim unteren Motor (hohe Stromaufnahme von 4 Ampere im Leerlauf, war der 1. von links im ersten Bild) hatte sich Graphit in den Ritzen zwischen den Kommutatorsegmenten auf der Welle festgesetzt und einen Nebenschluß zu den Wicklungen verursacht. Ich habe einfach nur die Ritzen freigeputzt und jetzt läuft er wieder top mit einer Stromaufnahme von 150 mA im Leerlauf.

Beim oberen Motor (lief gar nicht mehr, war der zweite von links im ersten Bild) hatten sich Wicklungsdrähte in den Schleifkontakte verfangen und den Graphitblock abgerissen (mir ist unklar, wie es zu so einem Fehler kommen kann). Ich habe die Wicklungsdrähte (nach Zuordnung über Widerstandsmessung) wieder angelötet, entsprechend gekürzt und die Rückplatte des anderen defekten Motors mit intakten Schleifkontakten eingebaut. Motor läuft wieder top mit Stromaufnahme von 150mA im Leerlauf.