---
layout: "image"
title: "Motoren ausgebaut"
date: "2018-07-13T18:04:58"
picture: "M6.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/47722
- /details6c97.html
imported:
- "2019"
_4images_image_id: "47722"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47722 -->
Diese beiden konnten gerettet werden.