---
layout: "comment"
hidden: true
title: "11858"
date: "2010-07-17T03:21:57"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Es dürfte sich bei den LEDs um sogenannte Blink-LEDs handeln. Die haben eine eingebaute Steuerung und blinken selsttätig. Die beiden Transistoren dienen hier lediglich als Ausgangsverstärker für die angeschlossenen Lampen. Insgesamt ist die Schaltung relativ simpel (um nicht zu sagen billig), dafür aber effektiv und platzsparend (keine Kondensatoren etc. notwendig).

Gruß, Thomas