---
layout: "image"
title: "Deckel offen"
date: "2010-07-16T23:32:20"
picture: "blinkelektronik2.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/27766
- /detailsd8f5.html
imported:
- "2019"
_4images_image_id: "27766"
_4images_cat_id: "2000"
_4images_user_id: "373"
_4images_image_date: "2010-07-16T23:32:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27766 -->
Man sieht die Buchen, die gleichzeitig als Klemmkontakte für eingesetzte Stecklampen dienen. Eine wie ich finde sehr schöne Lösung.
