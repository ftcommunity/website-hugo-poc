---
layout: "image"
title: "Verlängerung für Achsverschraubung"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen6.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40433
- /detailsee1d.html
imported:
- "2019"
_4images_image_id: "40433"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40433 -->
Normalerweise reicht der Überstand der Rastachsen nicht, um eine Achsverschraubung zu befestigen. Mit der Verlängerung reicht es.