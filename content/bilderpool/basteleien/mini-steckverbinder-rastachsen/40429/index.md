---
layout: "image"
title: "Detailansicht"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40429
- /detailse1c4.html
imported:
- "2019"
_4images_image_id: "40429"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40429 -->
Das Profil ist aus einem 4x3mm Messing-I-Profil hergestellt, welches ich durch immer kleinere Löcher gezogen habe, am Ende durch ein 3,9mm-Loch. Dadurch ist es außen rund geworden und passt leichtgängig in ft-Löcher. Es steckt in den Klauen der Rastachsen, zusätzlichen Halt geben die Überstände oben und unten. Die kleinen Kerben links und rechts dienen dazu, dass man die Mini-Steckverbinder verschränkt aneinander stecken kann.