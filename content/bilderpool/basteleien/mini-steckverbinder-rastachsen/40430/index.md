---
layout: "image"
title: "beengte Platzverhältnisse"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen3.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40430
- /details5eba.html
imported:
- "2019"
_4images_image_id: "40430"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40430 -->
Man kann Rastachsen unter beengten Platzverhältnissen verbinden. Mam braucht nur ein wenig Platz für die Verdickung an den Achsenden.