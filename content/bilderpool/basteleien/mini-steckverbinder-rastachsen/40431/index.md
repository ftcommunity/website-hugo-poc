---
layout: "image"
title: "Direkverbindung und Nutzung als Zapfen"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen4.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40431
- /details8e07.html
imported:
- "2019"
_4images_image_id: "40431"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40431 -->
Man kann Rastzahnräder direkt miteinander verbinden oder den Mini-Steckverbinder als Drehzapfen verwenden.