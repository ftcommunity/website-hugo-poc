---
layout: "comment"
hidden: true
title: "11793"
date: "2010-07-03T19:11:11"
uploadBy:
- "fischli"
license: "unknown"
imported:
- "2019"
---
Die Spannung beträgt ungefähr acht volt, ich habe den alten Fischertechniktrafo angeschlossen. Woraus die Elektroden sind weiß ich nicht genau. Ich habe mit der Blechschere eine alte verbogene Weihnachtsplätzchenform zurecht geschnitten. Das ganze war irgendwie beschichtet, ich vermute mal, es war Weißblech. Man kann aber auch andere Metalle nehmen, zb. einen Draht aus Kupfer. Mit dem Salz muss man eben rumprobieren. Habe auch verschiedenes gemacht. Mit zu wenig geht es schlecht, aber wenn man zuviel reintut klappts auch nicht mehr so gut. 
Vielleicht helfen dir die Informationen ja weiter. 
Gruß Fischli

PS. Zurzeit bin ich dabei eine richtige Brennstoffzelle zu bauen. Wenn die fertig ist stelle ich davon auch noch bilder rein.