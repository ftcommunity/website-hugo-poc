---
layout: "image"
title: "13"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen13.jpg"
weight: "13"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27584
- /details3d2b.html
imported:
- "2019"
_4images_image_id: "27584"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27584 -->
Zur Herstellung von mehr Wasserstoff habe ich Metallplättchen aus Weißblech genommen, diese haben eine größere Oberfläche als ein Stück Draht, daherentsteht auch mehr Wasserstoff. Ich habe auch festgestellt, dass die Wasserstoffherstellung sehr viel Strom verbraucht. Ein Akku wäre ungegeignet, deshalb nehme ich auch den Trafo