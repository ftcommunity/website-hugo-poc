---
layout: "image"
title: "16"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen16.jpg"
weight: "16"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27587
- /details157c.html
imported:
- "2019"
_4images_image_id: "27587"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27587 -->
So ist aller Wasserstoff entwichen.