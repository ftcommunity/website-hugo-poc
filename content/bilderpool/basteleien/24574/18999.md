---
layout: "comment"
hidden: true
title: "18999"
date: "2014-04-30T19:12:22"
uploadBy:
- "lemkajen"
license: "unknown"
imported:
- "2019"
---
Ich persönlich finde übrigens den Geruch der "neuen" (80er) elektronik ebenso beeindruckend, das liegt daran, daß mir mein Vater im zarten Alter von etwa 6 (mein Alter damals, nicht das meines Vaters..) eröffnete "Fischertechnik hört auf" . - eine Welt brach zusammen, denn ich hatte just ec1,ec2 und ec3 "zusammen" und wollte jetzt mal so richtig mit flipflops und monoflops anfangen....

Gemeint war aber, daß die Silberlinge auslaufen und es was Neues gibt, was wir uns aber (vermeintlich niemals) leisten können würden...
zu Weihnachten gabs dann aber wundsamerweise den Neuen Elektronikkasten  - und der roch soooo gut (und die riechen immer noch spitzenmäßig) :-))