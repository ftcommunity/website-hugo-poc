---
layout: "image"
title: "Platine von oben"
date: "2016-07-28T16:54:28"
picture: "amafc4.jpg"
weight: "4"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44041
- /details8069.html
imported:
- "2019"
_4images_image_id: "44041"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44041 -->
Herstellung der Platine: https://ftcommunity.de/details.php?image_id=43341#col3
