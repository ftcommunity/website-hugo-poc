---
layout: "image"
title: "Arduino Micro - Tiny TX"
date: "2017-05-28T16:45:03"
picture: "tinytx1.jpg"
weight: "18"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45923
- /details9c56.html
imported:
- "2019"
_4images_image_id: "45923"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-05-28T16:45:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45923 -->
Dieses Board basiert auf dem Arduino Micro.
Features:
3 Motorausgänge
2 Servo
2 (4) schnelle Zähleingänge
6 Universaleingänge

Verwendung: mobile Systeme, Zwischen MiniBots und TX Controller
