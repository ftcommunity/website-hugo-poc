---
layout: "image"
title: "Controller Lineup"
date: "2017-05-28T16:45:03"
picture: "tinytx3.jpg"
weight: "20"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45925
- /details0c94-2.html
imported:
- "2019"
_4images_image_id: "45925"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-05-28T16:45:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45925 -->
silber: Arduino Mega als ft Controller
schwarz: IR Empfänger (alte Version)
rot (quadratisch): "Tiny TX": kompakter Controller
rot (rechteckig): Wifi Controller für Fischertechnik (erlaubt IoT und Smartphonesteuerung), zusätzlich: Einsatz als IR Empfänger
