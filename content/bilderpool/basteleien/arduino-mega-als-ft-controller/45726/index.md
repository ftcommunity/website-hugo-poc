---
layout: "image"
title: "Übersicht"
date: "2017-04-10T16:31:32"
picture: "amecg1.jpg"
weight: "15"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45726
- /detailsdf39.html
imported:
- "2019"
_4images_image_id: "45726"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-04-10T16:31:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45726 -->
Verbindung des Extensionmoduls mit dem Arduino Mega, 8 zusätzliche Ausgänge für je bis zu 450mA
