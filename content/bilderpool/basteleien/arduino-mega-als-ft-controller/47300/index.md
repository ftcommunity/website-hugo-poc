---
layout: "image"
title: "Nema 14 Schrittmotor"
date: "2018-02-17T19:05:25"
picture: "step2.jpg"
weight: "22"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/47300
- /detailsdb8c.html
imported:
- "2019"
_4images_image_id: "47300"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2018-02-17T19:05:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47300 -->
Nema 14 Schrittmotor in 3D gedruckter Halterung mit ft Nuten, passend dazu das Z10 oder ein Adapter auf 4mm Rastachsen

Video: https://youtu.be/kjhINmJub4k
