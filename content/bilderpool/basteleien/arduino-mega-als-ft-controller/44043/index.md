---
layout: "image"
title: "Pins"
date: "2016-07-28T16:54:28"
picture: "amafc6.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/44043
- /detailse3ea.html
imported:
- "2019"
_4images_image_id: "44043"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44043 -->
Für den Anschluss der Fischertechnik Stecker verwende ich im Gegensatz zu https://ftcommunity.de/details.php?image_id=43336#col3 die zweite Generation "kombinierte Buchse". Der Kontakt erfolgt nach wie vor über die einzelnen Pins. Allerdings wurden die Messinghülsen (bis jetzt noch nicht auf dem Foto) innen verzinnt, sodass die ft Stecker noch besser halten.
