---
layout: "comment"
hidden: true
title: "22324"
date: "2016-07-28T20:47:32"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Die Bordspannung (3.3 Volt) ist das Problem: Die Motor ICs (L293x) funktionieren schon ab 2.7 Volt, also kein Problem. Allerdings funktionieren einige ft Sensoren nicht mehr mit 3.3 Volt.
Folgendes klappt:
- Taster
- Encoder
Folgendes klappt nicht:
- Farbsensor
- Spursensor
- i2c mit 5 Volt Hardware (wie beim TX)

Grüße
David