---
layout: "image"
title: "Nut"
date: "2017-03-23T11:41:09"
picture: "amafc4_2.jpg"
weight: "11"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45633
- /details3eae.html
imported:
- "2019"
_4images_image_id: "45633"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45633 -->
Die Passform von Nut zu Zapfen entspricht der Qualtiät von Spritzgussbauteilen
