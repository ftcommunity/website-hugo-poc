---
layout: "image"
title: "Fußgängerampel"
date: "2017-04-10T09:45:50"
picture: "amafce1.jpg"
weight: "14"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45725
- /details829e.html
imported:
- "2019"
_4images_image_id: "45725"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-04-10T09:45:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45725 -->
Das Erweiterungsmodul lässt sich gut für Lauflichter nutzen. Hier wird eine Ampelanlage damit gesteuert: https://youtu.be/CtFk9EP0fCg
