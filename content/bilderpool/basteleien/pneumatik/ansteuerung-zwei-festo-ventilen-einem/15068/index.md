---
layout: "image"
title: "Betätigung nur eines Ventils"
date: "2008-08-22T19:50:14"
picture: "magnetventile5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/15068
- /details0664.html
imported:
- "2019"
_4images_image_id: "15068"
_4images_cat_id: "1370"
_4images_user_id: "104"
_4images_image_date: "2008-08-22T19:50:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15068 -->
Diese Variante wird nun hoffentlich zum gewünschten Erfolg führen. Sie ist fast identisch mit der Originalanleitung aufgebaut, nur dass als betätigendes Element kein BS7,5 mit aufgeklebten Etiketten, sondern einfach ein Winkelstein gleichseitig verwendet wird. Durch Verschieben des Winkelsteins kann die Anordnung gut justiert werden. Durch mehr oder weniger weites Zusammenschieben der BS 5 unterhalb der Winkelelemente kann das Bindeglied, hier wieder eine Platte 15x45 mit zwei Zapfen, gegen Verdrehen geschützt und gleichzeitig leichtläufig gehalten werden. Tests ergaben, dass der E-Magnet das Ventil bei guter Justage sehr sauber durchschaltet. Allerdings muss er mit geglätteter Gleichspannung betrieben werden. Ungeglättete führt zu Brummen, nicht sauber durchgeschaltetem Ventil und somit starkem Druckverlust.

Dieses Ventil muss dann einen Pneumatik-Doppelbetätiger ansteuern, der dann erst die beiden Ventile betätigt, die einen Zylinder beidseitig steuern. Fotos davon werden veröffentlicht, wenn die Uhr fertig ist.
