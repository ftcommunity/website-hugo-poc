---
layout: "comment"
hidden: true
title: "14550"
date: "2011-07-04T19:36:57"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo Martin,

ein Linearregler wie der 7805 ist für deinen Zweck nicht gut geeignet - der heizt viel zu viel. Am besten wäre es natürlich, die Pumpe mit vier AA-Zellen von 1,2 V zu betreiben. Wenn du einen Regler einsetzen willst, ist ein Step-Down DC-DC-Konverter wie der LM2576 bestimmt viel besser geeignet als der 7805. Ob das die optimale Lösung ist, kann ich aber nicht sagen.

Gruß, Thomas