---
layout: "comment"
hidden: true
title: "14548"
date: "2011-07-04T15:51:00"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo,

die glänzende Flasche sieht sehr professionell aus. Paßt der Kleinkompressor eigentlich in ein 9V-Batteriefach? Wozu wird der ft-Lufttank noch benötigt?

Gruß, Thomas