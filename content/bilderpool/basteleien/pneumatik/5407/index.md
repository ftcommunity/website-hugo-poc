---
layout: "image"
title: "Kolbenbearbeitung"
date: "2005-11-27T18:02:31"
picture: "IMG_1714.jpg"
weight: "2"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5407
- /detailsacbd.html
imported:
- "2019"
_4images_image_id: "5407"
_4images_cat_id: "464"
_4images_user_id: "6"
_4images_image_date: "2005-11-27T18:02:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5407 -->
Zuerst wird der Kolben gerade im Drehfutter eingespannt. Ich bediene mich hier eines Bohrfutters im Reitstock, in dem ich die noch vorhandene Kolbenstange einspanne und das Ganze dann in das Drehfutter schiebe. Dann wird die Kolbenstange mit einem dünnen Abstechstahl am Kolben abgestochen.
