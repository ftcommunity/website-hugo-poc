---
layout: "image"
title: "Vakuumpumpe"
date: "2004-06-13T17:20:44"
picture: "Vakuum_1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Vakuum"]
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2527
- /details6c04.html
imported:
- "2019"
_4images_image_id: "2527"
_4images_cat_id: "280"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2527 -->
Vakuumpumpe mit Behälter aus einer
Abwasserrohrmuffe und 2 passenden
Deckeln.Dieser Behälter eignet sich nur
für Vakuum, da bei Überdruck die Deckel
wie überdimensionale Kolben rausgeschoben werden.