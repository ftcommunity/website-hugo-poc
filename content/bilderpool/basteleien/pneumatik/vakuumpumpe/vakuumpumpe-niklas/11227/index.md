---
layout: "image"
title: "Vakuumpumpe"
date: "2007-07-31T14:04:14"
picture: "vakuum1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11227
- /details0a55.html
imported:
- "2019"
_4images_image_id: "11227"
_4images_cat_id: "1015"
_4images_user_id: "557"
_4images_image_date: "2007-07-31T14:04:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11227 -->
Hier der saugnapf, ich hatte grad nix besseres zur Hand, hier besteht aber noch verbesserunsbedarf durch einen Richtigen saugnapf, das weiße Teil ist einer der adapter eines magnetventiles