---
layout: "image"
title: "Vakuumpumpe"
date: "2007-07-31T14:04:14"
picture: "vakuum2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- /php/details/11228
- /details5591.html
imported:
- "2019"
_4images_image_id: "11228"
_4images_cat_id: "1015"
_4images_user_id: "557"
_4images_image_date: "2007-07-31T14:04:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11228 -->
Hier das Rückschlagventil