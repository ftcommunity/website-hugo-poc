---
layout: "image"
title: "Vakuumpumpe"
date: "2004-06-13T17:20:44"
picture: "Vakuum_2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/2528
- /details6aa3.html
imported:
- "2019"
_4images_image_id: "2528"
_4images_cat_id: "280"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2528 -->
