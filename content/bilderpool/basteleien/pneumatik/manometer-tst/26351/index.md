---
layout: "image"
title: "Manometer"
date: "2010-02-13T15:24:20"
picture: "manometer1_2.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26351
- /details3b95.html
imported:
- "2019"
_4images_image_id: "26351"
_4images_cat_id: "1834"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26351 -->
Hier ein Standartmanometer integriert in einen von mir angefertigten Baustein.
So läßt sich das Manometer super befestigen.
Der Baustein hat die Maße 45x45x15mm.
