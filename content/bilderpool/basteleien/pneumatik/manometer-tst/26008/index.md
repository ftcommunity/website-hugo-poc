---
layout: "image"
title: "Manometer Einbau"
date: "2010-01-01T14:38:47"
picture: "manometer1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26008
- /details3779.html
imported:
- "2019"
_4images_image_id: "26008"
_4images_cat_id: "1834"
_4images_user_id: "182"
_4images_image_date: "2010-01-01T14:38:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26008 -->
Ich habe hier ein Gehäuse gefertigt um ein Standart Manometer ordentlich zu verpacken.
