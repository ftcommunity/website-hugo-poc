---
layout: "comment"
hidden: true
title: "17197"
date: "2012-09-07T21:47:11"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Klasse! Damit könnte man auch Bälle von einer Station zur nächsten übergeben, wie das gerade für die ftconvention diskutiert wird.

Gruß,
Harald