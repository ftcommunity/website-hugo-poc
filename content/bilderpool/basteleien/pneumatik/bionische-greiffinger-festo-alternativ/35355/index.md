---
layout: "image"
title: "Bionische Greiffinger (Festo) + Fischertechnik"
date: "2012-08-24T20:58:37"
picture: "bionischegreiffingerfestodrosselventilalternativ5.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/35355
- /details2805.html
imported:
- "2019"
_4images_image_id: "35355"
_4images_cat_id: "2620"
_4images_user_id: "22"
_4images_image_date: "2012-08-24T20:58:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35355 -->
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen.
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden.
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Der bionische Greifer (Festo) wirdt im 3D Selective Laser Sintering-Verfahren aus Polyamidpulver hergestellt.
