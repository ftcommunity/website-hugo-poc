---
layout: "image"
title: "Alternatives Drosselventil"
date: "2012-10-23T20:12:43"
picture: "ventil.jpg"
weight: "7"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/36073
- /detailsd2ab.html
imported:
- "2019"
_4images_image_id: "36073"
_4images_cat_id: "2620"
_4images_user_id: "182"
_4images_image_date: "2012-10-23T20:12:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36073 -->
Mir ist da auch noch eine Idee zu einem Drosselventil gekommen.
Man nehme einen Baustein 15 mit Bohrung versehe Ihn mit einem zusätzlichen Querloch, baue dann einen von meinen Kulissensteinen mit einer Rändelschraube ein und schon hat man eine Drossel die den Original sehr nahe kommt.
