---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer3.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37289
- /details25d7.html
imported:
- "2019"
_4images_image_id: "37289"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37289 -->
Mit ein pneumatik Zylinder ist eine automatische Schließvorgang einfach möglich.

Statt ein pneumatiek Greifer-Antrieb wäre ein Antrieb mit einer Spindel auch möglich. Beim Schließvorgang muss man in RoboPro dann die Motorspannung überwachen. Schließt sich die Zange, steigt der Motorstrom stark an und die Spannung bricht ein. Damit lässt sich die Zange schnell und an unterschiedlichen Positionen abschalten. 
So lassen sich unterschiedliche geformte Gegenstände sicher greifen. Auch die Eischale geht nicht zu Bruch.
Notwendig ist aber eine stabilisierte Versorgungsspannung damit die Spannung reproduzierbar ist.
