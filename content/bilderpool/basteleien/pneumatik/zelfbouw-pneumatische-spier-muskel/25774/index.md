---
layout: "image"
title: "Pneumatik-3D-Slurf    (Rüssel)"
date: "2009-11-13T21:18:50"
picture: "Pneumatik-3D-Slurf_007.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25774
- /details7743.html
imported:
- "2019"
_4images_image_id: "25774"
_4images_cat_id: "1695"
_4images_user_id: "22"
_4images_image_date: "2009-11-13T21:18:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25774 -->
