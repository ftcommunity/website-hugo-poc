---
layout: "image"
title: "Druckdensoren + IC-Digital + 5V-Module"
date: "2009-11-20T17:55:43"
picture: "DrucksensorenIC-Digital_008.jpg"
weight: "10"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25812
- /details12df.html
imported:
- "2019"
_4images_image_id: "25812"
_4images_cat_id: "1695"
_4images_user_id: "22"
_4images_image_date: "2009-11-20T17:55:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25812 -->
RBA-DRuck-Sensoren von Sensortechnics (0,5 - 4,5 V ratiometric output, ca. 20Euro) + 
IC-Digital + 
5V-Module TA78L05 / 5v / SOT-89 , Conradnr.: 140805-89
