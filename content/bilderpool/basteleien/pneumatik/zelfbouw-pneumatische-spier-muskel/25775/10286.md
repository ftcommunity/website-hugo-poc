---
layout: "comment"
hidden: true
title: "10286"
date: "2009-11-22T18:24:16"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Freunden,

Ich versuche für meine Pneumatik Muskel eine stabile Druckregulierung zu machen.
Bilder gibt es beim FT-Community unter:

http://www.ftcommunity.de/categories.php?cat_id=1695 und

http://www.ftcommunity.de/details.php?image_id=25812

Ich versuche mit Robo-TX eine stabiele Druckregulierung für eine Pneumatik Muskel zu programieren.
Ziel ist mit ein Pot-Meter eine stabile Luftdruck zu realisieren, damit die Pneumatik Muskel eine feste einstellbare Verkurzung hat. 
Es gibt eine feste Zusammenhang schwischen den Druck und die Länge einer Pneumatik Muskel.

Problem is aber :
Fast immer gibt es eine Abweichung zwischen der Potmeter-einstellung und der Drucksensor.
Das Programm wird schell instabil. In die Niederlände sagt man: das Programm "Jo-Jo-t".

Beim Pneumatik Muskel gibt es dann immer Pressluft-Zufür via O1 oder Pressluf-Abfür via O2.
Eine Druck-range (etwa 200 mBar = 0,2 Bar) wobei gar nichts passiert wäre o.k. 
Wenn das Unterschied (Differenz) zwischen die Drucksensor-druck und Pot-meter grösser ist als 200 mBar, nur dann muss Pressluft zu- oder abgeführt werden.

Wie ist solch eine stabiele Regulierung realisierbar ? ........es gibt in Robo-Pro kein standard -Vergleichung für dieses Problem.


Im Robopro-Progamm Pneumatik-Muskel rechne ich die Pot-meter Wiederstand um nach mBar.
Die Druksensor-Spannung ( 0,5 -4,5V ) rechne ich auch um nach mBar zum Vergleich.
In mBar gibt es nur Integer. 

Das Robopro-Progamm Pneumatik-Muskel habe ich im FT-Community geupload, aber es ist noch nicht freigeschaltet.
Das Versuch-Prinzip ist aber gleich wie dass Robo-Pro-Programm bei meinem Garten-Wehren :

http://www.ftcommunity.de/details.php?image_id=17256

Da gab es auch solch eine instabile Situation :

http://www.ftcommunity.de/details.php?image_id=23614 und

Hier war eine geringere Mess-frequenz eine gute Löschung. In wirklichkeit macht man dass bei Wehren unserem Wasserverband Rivierenland auch.

Beim Pneumatik Muskel wäre vielleicht eine elegantere Lösung möglich ?
Auch bei andere Fischertechnik-Modellen wäre eine stabiele Regulierung schön sein. 

Mit freundliche grüss,

Peter Damen
Poederoyen NL