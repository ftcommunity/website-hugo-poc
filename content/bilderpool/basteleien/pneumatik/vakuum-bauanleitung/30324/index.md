---
layout: "image"
title: "Vakuumgreifer"
date: "2011-03-26T21:28:35"
picture: "vakuum03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30324
- /details64ce.html
imported:
- "2019"
_4images_image_id: "30324"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30324 -->
Tipp:
Wie auf den Bildern zu sehen ist, kann man das ganze mit Federn befestigen:
Wenn der Greifer das Objekt geriffen hat wird automatisch der Minitaster ausgelöst, was aufwändige Positionierung in dieser Richtung erspart.