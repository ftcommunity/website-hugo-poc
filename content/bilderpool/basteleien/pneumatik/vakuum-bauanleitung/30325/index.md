---
layout: "image"
title: "Vakuumspeicher"
date: "2011-03-26T21:28:35"
picture: "vakuum04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30325
- /details8dc3.html
imported:
- "2019"
_4images_image_id: "30325"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30325 -->
Vakuum durch Luftabschluss:

Der Schlauch, der zum Saugnapf führt, wird an ein Pneumatikventil angeschlossen.
Der Saugnapf wird dann während das Ventil offen ist auf den Gegenstand gedrückt, dann wird das Ventil geschlossen um den Gegenstand festzuhalten.
Diese Methode ist die einfachste, allerdings können damit Gegenstände nur für kurze Zeit gehoben werden.
Hilfreich ist ein Anfeuchten des Saugnapfs. 

 

Vakuum mit Pumpe und ft-Rückschlagventil:

Um genügend Unterdruck aufzubauen, braucht man einen "Vakuumspeicher":
Dazu kann man z.B. ein leeres Nutellaglas verwenden: