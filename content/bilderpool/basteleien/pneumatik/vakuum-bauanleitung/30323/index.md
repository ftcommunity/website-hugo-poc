---
layout: "image"
title: "Vakuumgreifer"
date: "2011-03-26T21:28:35"
picture: "vakuum02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30323
- /details0b28.html
imported:
- "2019"
_4images_image_id: "30323"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30323 -->
Man durchsticht den Saugnapf in der Mitte und befestigt diesen mithilfe einer Staudüse.
("Düse blau" Art.Nr. 31632 bei Knobloch)
Die Düse kommt in einen Baustein 15 mit Loch und ist dort frei beweglich. 

Wer keine solche Staudüse zur Verfügung hat kann den Saugnapf auf direkt über einen Schlauch ziehen, das ist allerdings wackeliger. 

Unbedingt auf Dichtheit achten!