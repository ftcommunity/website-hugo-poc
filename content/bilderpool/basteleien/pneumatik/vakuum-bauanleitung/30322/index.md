---
layout: "image"
title: "Saugnapf"
date: "2011-03-26T21:28:35"
picture: "vakuum01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30322
- /details5c7f.html
imported:
- "2019"
_4images_image_id: "30322"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30322 -->
Vakuumgreifer mit fischertechnik

Mit den fischertechnik Pneumatik-Teilen und einem Saugnapf lassen sich Vakuumgreifer bauen, mit denen Dinge mit glatter Oberfläche gegriffen werden können.
Genauer genommen gibt es dazu 3 Möglichkeiten:

Vakuum durch Luftabschluss nach dem Zusammendrücken des Saugnapfes

Vakuum mit Pumpe und (original-) fischertechnik - Rückschlagventil

Vakuum mit Pumpe und Selbstbau-Rückschlagventil

Für alle 3 Möglichkeiten braucht man einen Saugnapf, mit dem die Gegenstände gegriffen werden.

Präparieren des Saugnapfs:

Es sind praktisch alle Arten von Saugnäpfen geeignet, hier ein Beispiel der Marke Wenco:
(Einfache Haken mit Saugnapf zum an die Wand "kleben", der Haken wird natürlich nicht benötigt...)
Es sind z.B. auch die Säugnäpfe von "Dart" - Pfeilen o.ä. verwendbar.