---
layout: "image"
title: "Fischertechnik-Qualle  + pneumatik Muskel  an der Decke"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel20.jpg"
weight: "20"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37270
- /details7962.html
imported:
- "2019"
_4images_image_id: "37270"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37270 -->
Der Fluidic Muscle oben der  Fischertechnik Qualle an der Decke hebt die ca. 6 kg Qualle beim sliessen der 8 Tentakel. Auch fùr dieser nutze ich 3,8mm Reedsensor. Die Monoflop-Funktion der E-Tech-Module sorgt für genau 2 sekunden 2 Bar in der Fluidic Muscle.
