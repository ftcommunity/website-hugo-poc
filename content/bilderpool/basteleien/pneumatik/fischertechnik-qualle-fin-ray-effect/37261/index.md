---
layout: "image"
title: "Fischertechnik-Qualle  mit  Fin-Ray-Effect + pneumatik Muskel"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel11.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37261
- /details300c.html
imported:
- "2019"
_4images_image_id: "37261"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37261 -->
Ein RoboPro-Programm versorgt die Synchronisation der 8 Getriebe-motoren  (Conrad-224040  312:1).
Zum abfragen nutze ich 3,8mm Reedsensoren (Conrad-503548) für jede Tentakel-Antrieb. Die 3,8mm Durchmesser ermöglicht eine kompaktere einbau im Fischertechnik-Bausteinen.
Mit ein Pot-meter gibt es die möglichkeit die Wartezeit der Tentakel-Antrieb-Frequenz zu regulieren.
Auch eine "Intro-Wave" der 8 Tentakel habe ich in RoboPro programiert
