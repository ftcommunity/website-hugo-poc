---
layout: "image"
title: "Fischertechnik-Qualle  mit  Fin-Ray-Effect + pneumatik Muskel    (Decke)"
date: "2013-08-24T22:22:05"
picture: "quallefinrayeffectpneumatikmuskel01.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37251
- /details20af.html
imported:
- "2019"
_4images_image_id: "37251"
_4images_cat_id: "2772"
_4images_user_id: "22"
_4images_image_date: "2013-08-24T22:22:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37251 -->
Quallen sind faszinierende Lebewesen, die selbst zu etwa 99% aus Wasser bestehen. Sie haben sich den verschiedensten Umgebungen sowohl im Salz- als auch im Süßwasser über Jahrmillionen effizient angepasst. Der Bau einer künstlichen Fischertechnik-Qualle kann immer nur als ein Versuch gesehen werden, sich diesen faszinierenden Tieren zu nähern und von ihnen zu lernen.

Jedes Tentakel ist als Struktur mit Fin Ray Effect (=Festo-Entwicklung) ausgebildet - der Fin Ray Effect ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Die Struktur selbst besteht aus einer alternierenden Zug- und Druckflanke, die mit Spanten verbunden ist. 
Wenn eine Flanke mit Druck beaufschlagt wird, wölbt sich die geometrische Struktur von selbst entgegen der einwirkenden Kraftrichtung. Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Tentakel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.

Durch die gleichzeitige Wellenbewegung in den acht Tentakel wird den Vortrieb erzeugen. Zusammen sorgen die Tentakel für einen peristaltischen Vortrieb, ähnlich  dem des biologischen Vorbildes.
Der Fluidic Muscle oben der  Fischertechnik Qualle, kombiniert mit dem Fin Ray Effect bei jedem Tentakel bildet die zentrale Vortrieb. 

Ein RoboPro-Programm versorgt die Synchronisation der 8 Getriebe-motoren  (Conrad-224040  312:1).
Zum abfragen nutze ich 3,8mm Reedsensoren (Conrad-503548) für jede Tentakel-Antrieb. Die 3,8mm Durchmesser ermöglicht eine kompaktere einbau im Fischertechnik-Bausteinen.
Mit ein Pot-meter gibt es die möglichkeit die Wartezeit der Tentakel-Antrieb-Frequenz zu regulieren.
Auch eine "Intro-Wave" der 8 Tentakel habe ich in RoboPro programiert

Der Fluidic Muscle oben der  Fischertechnik Qualle an der Decke hebt die ca. 6 kg Qualle beim sliessen der 8 Tentakel. Auch fùr dieser nutze ich 3,8mm Reedsensor. Die Monoflop-Funktion der E-Tech-Module sorgt für genau 2 sekunden 2 Bar in der Fluidic Muscle. 

Die blaue Soffy Beachball ersetze ich nächste Woche in einer 40cm transparante Bal (2-delig Ballkit-code MT400TNL). Mit 8 hell-blaue Leds aus einer alte Taschenlampe gibt es dann noch mehr Effekt !
