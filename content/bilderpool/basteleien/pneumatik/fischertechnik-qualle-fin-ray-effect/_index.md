---
layout: "overview"
title: "Fischertechnik-Qualle mit Fin Ray Effect + pneumatik Muskel"
date: 2020-02-22T07:42:08+01:00
legacy_id:
- /php/categories/2772
- /categories9524.html
- /categoriesa427.html
- /categories1aaf.html
- /categories7f15.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2772 --> 
Durch die gleichzeitige Wellenbewegung in den acht Tentakel wird den Vortrieb erzeugen. Zusammen sorgen die Tentakel für einen peristaltischen Vortrieb, ähnlich  dem des biologischen Vorbildes.
Der Fluidic Muscle oben der  Fischertechnik Qualle, kombiniert mit dem Fin Ray Effect bei jedem Tentakel bildet die zentrale Vortrieb. 
