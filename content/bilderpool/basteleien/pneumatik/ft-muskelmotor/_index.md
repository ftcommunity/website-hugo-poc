---
layout: "overview"
title: "FT-Muskelmotor"
date: 2020-02-22T07:41:55+01:00
legacy_id:
- /php/categories/1709
- /categories378a.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1709 --> 
Ein Nocken und ein gesperrtes Freilauflager übertragen die lineare Bewegung - erzeugt wie bei einer Muskelkontraktion - auf eine Welle und setzen diese in eine Drehbewegung um. 



Bei Druckentlastung fährt der Pneumatischer Muskel wieder in seine Ursprungslänge zurück. Dank des entriegelten Freilauflagers wird diese Bewegung nicht auf die Welle übertragen. 

Bei ehrere pneumatische Muskeln lässt sich eine gleichmäßige Drehbewegung erzeugen. 