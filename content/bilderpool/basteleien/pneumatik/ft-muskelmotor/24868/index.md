---
layout: "image"
title: "Muskel-3"
date: "2009-08-30T09:28:51"
picture: "ftmuskelmotor5.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/24868
- /details9e7c.html
imported:
- "2019"
_4images_image_id: "24868"
_4images_cat_id: "1709"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24868 -->
Ein Nocken und ein gesperrtes Freilauflager übertragen die lineare Bewegung - erzeugt wie bei einer Muskelkontraktion - auf eine Welle und setzen diese in eine Drehbewegung um. 

Bei Druckentlastung fährt der Pneumatischer Muskel wieder in seine Ursprungslänge zurück. Dank des entriegelten Freilauflagers wird diese Bewegung nicht auf die Welle übertragen. 
Bei ehrere pneumatische Muskeln lässt sich eine gleichmäßige Drehbewegung erzeugen.
