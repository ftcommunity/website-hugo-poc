---
layout: "image"
title: "noch'n Kompressor"
date: "2003-10-08T15:42:09"
picture: "PA030073.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Kompressor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/1798
- /details6e10.html
imported:
- "2019"
_4images_image_id: "1798"
_4images_cat_id: "18"
_4images_user_id: "4"
_4images_image_date: "2003-10-08T15:42:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1798 -->
