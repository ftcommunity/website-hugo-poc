---
layout: "image"
title: "Kompressor"
date: "2007-01-19T10:50:50"
picture: "kompressor4.jpg"
weight: "23"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8513
- /detailsc069.html
imported:
- "2019"
_4images_image_id: "8513"
_4images_cat_id: "18"
_4images_user_id: "453"
_4images_image_date: "2007-01-19T10:50:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8513 -->
