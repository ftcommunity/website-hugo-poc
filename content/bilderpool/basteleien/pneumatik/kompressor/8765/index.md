---
layout: "image"
title: "Kompresssor rad"
date: "2007-01-30T19:24:52"
picture: "Neuer_Ordner_001.jpg"
weight: "25"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/8765
- /detailsd2a4-2.html
imported:
- "2019"
_4images_image_id: "8765"
_4images_cat_id: "18"
_4images_user_id: "453"
_4images_image_date: "2007-01-30T19:24:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8765 -->
