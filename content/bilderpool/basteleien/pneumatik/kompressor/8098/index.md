---
layout: "image"
title: "Kompressor3"
date: "2006-12-21T15:29:24"
picture: "Kompressor4.jpg"
weight: "19"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/8098
- /details2cb3.html
imported:
- "2019"
_4images_image_id: "8098"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2006-12-21T15:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8098 -->
Hier ist er komplett mit Tank zusammengebaut.
Ein kompakter leistungstarker Kompressor der auch an die Standartmodelle von fischertechnik paßt.
