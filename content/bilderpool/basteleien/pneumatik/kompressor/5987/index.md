---
layout: "image"
title: "lemo2.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00612_resize.jpg"
weight: "11"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["lemo", "manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5987
- /detailsaff8-2.html
imported:
- "2019"
_4images_image_id: "5987"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5987 -->
Direkt an die Kontakte wurden Buchsen gelötet, die Bauplatte wurde mit Heißkleber fixiert. Ebenso das Manometer in einer Gelenksteinhälfte.
