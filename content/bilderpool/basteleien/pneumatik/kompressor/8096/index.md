---
layout: "image"
title: "Kompressor 1"
date: "2006-12-21T15:29:24"
picture: "Kompressor1.jpg"
weight: "17"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/8096
- /details10d2.html
imported:
- "2019"
_4images_image_id: "8096"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2006-12-21T15:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8096 -->
Nach meinem ersten Prototyp habe ich jetzt ein Chassis entwickelt um den Powermotor und den Zylinder stabil miteinander zu verbinden. Der Kompressor liefert reichlich Luft da der Hub des Zylinders voll ausgenutzt wird. Der Maximaldruck den der Motor schafft bis er stehenbleibt liegt bei 1,5 Bar!!!!.
0,7 Bar schafft er spielend und auch zuverlässig.
