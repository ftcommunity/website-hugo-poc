---
layout: "image"
title: "lemo3.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00613_resize.jpg"
weight: "12"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["lemo", "druckschalter", "pumpe", "kompressor", "pneumatik", "flip-flop", "flipflop", "überdruck", "druckabschaltung", "druckregelung"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5988
- /details6e47-2.html
imported:
- "2019"
_4images_image_id: "5988"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5988 -->
Zur eleganten Abschaltung dient ein einstellbarer Druckschalter, das Flip-Flop dient eigentlich nur zum Invertieren des Signals. Auf dem Bild ist die Pumpe gerade nicht in Betrieb, weil die eingestellte Schwelle von 0,6 bar erreicht ist und der Sensor-Kontakt jetzt geschlossen ist.
