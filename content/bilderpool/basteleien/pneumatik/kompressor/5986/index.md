---
layout: "image"
title: "dsc00611_resize.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00611_resize.jpg"
weight: "10"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["lemo", "manometer", "pumpe", "kompressor", "pneumatik", "druckabschaltung"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/5986
- /detailsfb47-3.html
imported:
- "2019"
_4images_image_id: "5986"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5986 -->
Die günstigere Lemo-Pumpe ist schon am FT-Akku recht stark und wurde hier bei 1 bar gestoppt.
