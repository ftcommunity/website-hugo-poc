---
layout: "image"
title: "Kompressor mit XM Motor"
date: "2009-10-11T11:09:15"
picture: "KompresorXM1.jpg"
weight: "32"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25534
- /detailscf3a.html
imported:
- "2019"
_4images_image_id: "25534"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2009-10-11T11:09:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25534 -->
Da der Powermotor ja ausläuft habe ich mir eine Variante mit dem XM Motor überlegt.
Bis auf den Excenter ist alles Original ft.
Wäre eigentlich eine schöne Ergänzung für´s bestehende ft Programm, der aktuelle Kompressor ist ja nicht sehr Leistungsstark.
