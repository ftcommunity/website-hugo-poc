---
layout: "image"
title: "Mini-Drucklufttank - Einseitig Komplett"
date: "2015-04-18T21:33:57"
picture: "miniluftdrucktank1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40802
- /detailse69b.html
imported:
- "2019"
_4images_image_id: "40802"
_4images_cat_id: "3066"
_4images_user_id: "1677"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40802 -->
Fehlt nur noch ein Rückschlagventil und ein Kompressorkolben bzw. eine ePumpe.
Beim Kompressorkolben kann man einen höheren Druck erzeugen, so dass einer der Deckel mit einem PLOP abspringt. Bei mir war es bis jetzt immer der ohne Loch.
Mein FT eKompressor hat es bisher nicht geschaft.