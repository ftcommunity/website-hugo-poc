---
layout: "image"
title: "Mini-Drucklufttankdeckel - 1"
date: "2015-04-18T21:33:57"
picture: "miniluftdrucktank2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/40803
- /detailsef5f-2.html
imported:
- "2019"
_4images_image_id: "40803"
_4images_cat_id: "3066"
_4images_user_id: "1677"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40803 -->
Der isolierte Schlauch mit Schlauchanschluss wird von Vorne durch den Deckel geschoben,die Deckelöffnung wird auseinandner gedrückt.