---
layout: "image"
title: "Die Idee"
date: "2012-01-25T20:25:44"
picture: "ueberundunterdruckbehaelter2.jpg"
weight: "2"
konstrukteure: 
- "Michael Stumberger"
fotografen:
- "Michael Stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34061
- /details3e07.html
imported:
- "2019"
_4images_image_id: "34061"
_4images_cat_id: "2519"
_4images_user_id: "859"
_4images_image_date: "2012-01-25T20:25:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34061 -->
Da mich jede gesehene Lösung nicht befriedigte, besorgte ich mir von Conrad einen Rohling mit 4 cm Durchmesser aus Aluminium. Kurz abgedreht, ein Gewinde und zwei Löcher bzw. 3, um den Zapfen zu versenken. Und siehe da.....