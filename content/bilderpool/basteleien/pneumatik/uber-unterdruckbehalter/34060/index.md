---
layout: "image"
title: "Die Voraussetzung"
date: "2012-01-25T20:25:44"
picture: "ueberundunterdruckbehaelter1.jpg"
weight: "1"
konstrukteure: 
- "Michael Stumberger"
fotografen:
- "Michael Stumberger"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- /php/details/34060
- /details32ec-2.html
imported:
- "2019"
_4images_image_id: "34060"
_4images_cat_id: "2519"
_4images_user_id: "859"
_4images_image_date: "2012-01-25T20:25:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34060 -->
Ich habe bei ebay je 2 Behälter 2x erstanden. Die linken Behälter fassen 0,6 ltr und die rechten 0,75 ltr. Zu meinem Glück stellte ich fest, daß beide Typen das gleiche Gewinde für den Verschlußdeckel verwendeten. Das Gewinde beträgt M26x1,5.