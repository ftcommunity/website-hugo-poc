---
layout: "image"
title: "Zylinder als Längsaktuator"
date: "2007-02-23T10:51:38"
picture: "04-Hexapod.jpg"
weight: "4"
konstrukteure: 
- "Martin Romann (Remadus)"
fotografen:
- "Martin Romann (Remadus)"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- /php/details/9138
- /details2273.html
imported:
- "2019"
_4images_image_id: "9138"
_4images_cat_id: "830"
_4images_user_id: "46"
_4images_image_date: "2007-02-23T10:51:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9138 -->
Kreuzgelenk oben, Kreuzgelenk unten - Fertig.

Zwei Jahre braucht man, um soviele Bausteine wegzulassen.
