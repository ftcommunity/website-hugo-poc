---
layout: "image"
title: "Deckel eines (einfachen) Batteriefachs"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach08.jpg"
weight: "8"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37193
- /details7e31.html
imported:
- "2019"
_4images_image_id: "37193"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37193 -->
Deckel ebenfalls von einem Stück Rand befreit.
