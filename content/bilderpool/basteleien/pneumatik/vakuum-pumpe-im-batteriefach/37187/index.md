---
layout: "image"
title: "Vacuumpumpe 12V ca.0.9-1.1 bar"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach02.jpg"
weight: "2"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37187
- /detailse04f.html
imported:
- "2019"
_4images_image_id: "37187"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37187 -->
Für 16? habe ich diese Pumpe aus der eBucht geangelt.
Beschreibung siehe Bild.
Ich finde ihn leiser als einen Blauen von FT.
Er arbeitene mit einem Kolben.
Und er saugt und pumpt.
