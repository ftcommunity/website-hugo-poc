---
layout: "image"
title: "Pumpe - Batteriefach - Oesen mit Pumpe verlötet"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach10.jpg"
weight: "10"
konstrukteure: 
- "Kai Baumgart (DasKasperle)"
fotografen:
- "Kai Baumgart (DasKasperle)"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- /php/details/37195
- /details566c-2.html
imported:
- "2019"
_4images_image_id: "37195"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37195 -->
Isolationsschicht einelegen.
