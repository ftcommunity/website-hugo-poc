---
layout: "image"
title: "Ventilinsel"
date: "2006-01-07T19:17:19"
picture: "IMG_0151.jpg"
weight: "1"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5566
- /details1bc0-2.html
imported:
- "2019"
_4images_image_id: "5566"
_4images_cat_id: "483"
_4images_user_id: "6"
_4images_image_date: "2006-01-07T19:17:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5566 -->
Zu sehen sind die IC Fassungen, in denen die Relais sitzen. Immer zwei Relais für zwei Ventile um einen Zylinder ein und ausfahren zu lassen.

Die Rlais kann man über das Interface - Rpbo oder Int. - ansteuern. Die Rlais benutzen die gemeinsame Masse am Interface.

Die Ventile sind von Kuhnke "Micro Magnetventil MPP1". Die benutzen alle gemeinsam die Masse des Spannungswandler.

Die Platine habe ich mir von einem E-Bay Anbieter herstellen lassen.

Funktionieren tut es, allerdings sind die Luftkanäle in den Ventilen anscheinend zu klein und die Zylinderbewegung geht recht langsam von statten, leider...
