---
layout: "image"
title: "Ventilinsel"
date: "2006-01-07T19:17:20"
picture: "IMG_0150.jpg"
weight: "2"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5567
- /detailsfe90.html
imported:
- "2019"
_4images_image_id: "5567"
_4images_cat_id: "483"
_4images_user_id: "6"
_4images_image_date: "2006-01-07T19:17:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5567 -->
Ventilplatine, Spannungswandler und angeschlossener Zylinder. Das gelbe Kabel ist die gemeinsame Masse der Relais, das kabel ist mit dem Interface verbunden.
