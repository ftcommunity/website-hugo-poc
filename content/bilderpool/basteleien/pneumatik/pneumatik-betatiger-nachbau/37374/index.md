---
layout: "image"
title: "Betätiger vor der Endmontage"
date: "2013-09-11T12:40:09"
picture: "Rollmembran_und_Kolben.jpg"
weight: "23"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
schlagworte: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/37374
- /details2f27.html
imported:
- "2019"
_4images_image_id: "37374"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37374 -->
Gut zu erkennen ist die zusammengebaute Kolbenstange mit Führung im Vordergrund.

Hinten schaut die Rollmembran noch heraus - wie der Finger eines Gummihandschuhs. Ist aber nur ein abgeschnittener Luftballon. Rollmembran nehme ich, weil ich keine Idee und Möglichkeit habe eine gleitende Dichtung herzustellen.

Rollmembran und Rad entsprechen von der Idee her noch am ehesten dem ursprünglichen pneumatischen Betätiger von ft. Die Kolbenstange ist eigentlich nur die Führung und sorgt derzeit dafür, daß das Rad nicht aus der Konstruktion fällt. Das könnte man allerdings auch anklebenund käme dem Vorbild näher.