---
layout: "image"
title: "Pneumatik Betätiger"
date: "2013-06-06T19:14:34"
picture: "betaetiger1.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/37027
- /details201c.html
imported:
- "2019"
_4images_image_id: "37027"
_4images_cat_id: "311"
_4images_user_id: "453"
_4images_image_date: "2013-06-06T19:14:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37027 -->
Rückansicht aller Einzelteile des ft Betätiger, vielleicht hilft es euch beim nachbauen.
