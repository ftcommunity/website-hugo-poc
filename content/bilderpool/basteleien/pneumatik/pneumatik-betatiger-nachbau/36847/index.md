---
layout: "image"
title: "P-Betätiger 3"
date: "2013-04-18T20:27:44"
picture: "bild3.jpg"
weight: "7"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/36847
- /details9a2b.html
imported:
- "2019"
_4images_image_id: "36847"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36847 -->
Belastungstests. Ich glaube, da passt noch mehr Luft rein.