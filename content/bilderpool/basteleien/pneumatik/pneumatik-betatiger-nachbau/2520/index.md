---
layout: "image"
title: "PBet166-1.jpg"
date: "2004-06-06T19:16:08"
picture: "PBet166-1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Pneumatik", "Betätiger", "Eigenbau"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2520
- /details2c17.html
imported:
- "2019"
_4images_image_id: "2520"
_4images_cat_id: "311"
_4images_user_id: "4"
_4images_image_date: "2004-06-06T19:16:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2520 -->
