---
layout: "image"
title: "P-Betätiger klein V2 3"
date: "2013-06-08T20:40:06"
picture: "bild3_2.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- /php/details/37030
- /details45d3-2.html
imported:
- "2019"
_4images_image_id: "37030"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-06-08T20:40:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37030 -->
Die Membran wurde auf das Wattestäbchen gedreht, und dann mit Tesa befestigt.