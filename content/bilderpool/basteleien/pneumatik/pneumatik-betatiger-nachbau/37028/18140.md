---
layout: "comment"
hidden: true
title: "18140"
date: "2013-07-03T20:47:37"
uploadBy:
- "Phil"
license: "unknown"
imported:
- "2019"
---
Das ist doch ungefähr das, was ich schon vorher versucht hatte - nur nicht mit einer Hülse+Scheibe, sondern mit einem Rad 23. Das Problem war, das die Federkraft des Tasters so groß war, dass sich die Membran um den Taster herum ausdehnt, aber nicht den Taster drückt. Um dies doch zu erreichen, müsste man allerdings einen sehr großen Druck in die Membran schicken.

Gruß,
Phil