---
layout: "image"
title: "Fischertechnik pneumatischer Yo-Yo"
date: "2009-12-20T18:32:30"
picture: "fischertechnikpneumatischeryoyo1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/25967
- /details82a1-2.html
imported:
- "2019"
_4images_image_id: "25967"
_4images_cat_id: "1826"
_4images_user_id: "22"
_4images_image_date: "2009-12-20T18:32:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25967 -->
Festo -Bionic hat einige Jahren her eine pneumatischer Yo-Yo entwickelt.
Ein pneumatischer Muskel is schnell und hat viel Kraft.

Der Trick einer pneumatischer Muskel ist eine genaue Abstand-, Druck- und Zeit- Regulierung.

Ich habe versucht mit Fischertechnik ein pneumatischer Yo-Yo zu machen, und es funktioniert !......endlich...
Mit der FT-US-Abstandsensor und der Robo-Interface bleibt das Schwungrad / Maxwell-Rad yo-yo-en.
