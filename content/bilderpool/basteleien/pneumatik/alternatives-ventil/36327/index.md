---
layout: "image"
title: "03 Alternatives Ventil"
date: "2012-12-18T21:38:26"
picture: "ventil3.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- /php/details/36327
- /details72f4.html
imported:
- "2019"
_4images_image_id: "36327"
_4images_cat_id: "2695"
_4images_user_id: "860"
_4images_image_date: "2012-12-18T21:38:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36327 -->
Zwei Arten zum Befestigen des ft-Schlauchs sind hier zu sehen: 
Man kann zum einen den Anschluss ein wenig dünner feilen und ein Teil Nr. 35328 drankleben oder man steckt ein blaues T-Stück rein. Beides funktioniert wunderbar.

Kurzes Video: http://www.youtube.com/watch?v=NPYvVA3zlxU