---
layout: "image"
title: "Druckminderer"
date: "2006-10-02T19:53:38"
picture: "ft_Teile_001.jpg"
weight: "8"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7105
- /details88a7.html
imported:
- "2019"
_4images_image_id: "7105"
_4images_cat_id: "487"
_4images_user_id: "473"
_4images_image_date: "2006-10-02T19:53:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7105 -->
ich kann jeden Druck runterbrechen auf das gesunde Ft-Maaß. Ich benutze dazu meine alle Tauchflasche mit 220bar. Luft ohne Ende :-)