---
layout: "image"
title: "Pneumatik-Drehdurchführung"
date: "2015-03-14T16:23:37"
picture: "pdurch1.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40666
- /detailseab2.html
imported:
- "2019"
_4images_image_id: "40666"
_4images_cat_id: "464"
_4images_user_id: "4"
_4images_image_date: "2015-03-14T16:23:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40666 -->
Zu Pneumatik-Drehdurchführungen aus Zylinderteilen siehe auch den Artikel von René hier: http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2014-3.pdf  S. 17. 

Ich habe ein CAD-Programm und einen 3D-Drucker bemüht: Das linke ist ein Schnittbild. Das gelbe sind zwei O-Ringe aus dem Modellbauhandel. Das hellblaue Rohr ist das innere von zwei Messingrohren, das äußere ist nicht gezeigt. 
Die Löcher oben müssen mit einem Stück Plastik zugeklebt werden. Wenn man diese "Deckel" gleich mit drucken lässt, wird innen drin alles mit Stützmaterial zu gekleistert, und dann hilft sowieso nur das Aufbohren von der Oberseite, damit die Dichtungssitze heil bleiben.
