---
layout: "image"
title: "Trichter"
date: "2008-09-12T22:45:53"
picture: "anleitungzumoelenvonzylindern5.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/15228
- /details2e8d.html
imported:
- "2019"
_4images_image_id: "15228"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15228 -->
Zuerst muss man den Schlauch auf den Zylinder stecken und mit einer Pipette ein bisschen Paraffinum Perliquidum hereinfüllen.