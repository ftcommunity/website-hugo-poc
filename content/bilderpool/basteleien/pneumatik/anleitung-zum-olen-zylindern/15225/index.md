---
layout: "image"
title: "Aufbau"
date: "2008-09-12T22:45:52"
picture: "anleitungzumoelenvonzylindern2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- /php/details/15225
- /detailsf69d.html
imported:
- "2019"
_4images_image_id: "15225"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15225 -->
Bevor man überhaupt anfangen kann, muss man dieses Gestell aufbauen.