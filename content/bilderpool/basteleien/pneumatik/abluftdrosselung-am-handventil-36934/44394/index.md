---
layout: "image"
title: "Abluftdrosselung"
date: "2016-09-25T11:58:52"
picture: "Abluftdrosselung_Handventil.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten (Laserman)"
fotografen:
- "Andreas Gürten (Laserman)"
schlagworte: ["Abluftdrosselung", "Handventil", "langsam", "Bewegung", "Pneumatik", "Luftdruck", "Luftzylinder", "air", "cylinder", "valve", "hand", "pneumatic", "slow", "movement"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- /php/details/44394
- /detailsaf70.html
imported:
- "2019"
_4images_image_id: "44394"
_4images_cat_id: "3282"
_4images_user_id: "724"
_4images_image_date: "2016-09-25T11:58:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44394 -->
Nun kann man endlich die Abluft am Handventil drosseln, für langsame Bewegung des Luftzylinders in beide Richtungen.

Einfach einen Schlauchanschluß # 35328 auf den dicken Anschluß des Handventils schieben.
Dann kann man einen kurzen Schlauch daran anschließen (ca. 2cm) und kann durch gefühlvolles abquetschen Desselben (z.B. mit Kabelbinder) die Abluft drosseln.
