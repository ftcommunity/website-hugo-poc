---
layout: "image"
title: "Vergleich"
date: "2005-11-20T07:22:39"
picture: "IMG_1722.jpg"
weight: "1"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
schlagworte: ["Eigenbau", "Pneumatik"]
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/5348
- /details6a11.html
imported:
- "2019"
_4images_image_id: "5348"
_4images_cat_id: "464"
_4images_user_id: "6"
_4images_image_date: "2005-11-20T07:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5348 -->
Ein umgebauter Pneumatikzylinder im Vergleich zum Original. Der Umbau gestalte sich doch umfangreicher als gedacht.
