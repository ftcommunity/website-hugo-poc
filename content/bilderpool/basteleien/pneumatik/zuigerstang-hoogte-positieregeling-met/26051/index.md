---
layout: "image"
title: "Zuigerstang Hoogte-Positieregeling met US-Sensor"
date: "2010-01-09T18:42:04"
picture: "zuigerstanghoogtepositieregelingmetussensor1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26051
- /details4d04.html
imported:
- "2019"
_4images_image_id: "26051"
_4images_cat_id: "1839"
_4images_user_id: "22"
_4images_image_date: "2010-01-09T18:42:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26051 -->
Zuigerstang Hoogte-Positieregeling met US-Sensor
