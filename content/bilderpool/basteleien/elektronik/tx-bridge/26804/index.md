---
layout: "image"
title: "TX-Bridge5"
date: "2010-03-23T21:06:11"
picture: "tx08.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26804
- /details9eb3.html
imported:
- "2019"
_4images_image_id: "26804"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T21:06:11"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26804 -->
Equivalent of Ext1 and Ext2 connectors