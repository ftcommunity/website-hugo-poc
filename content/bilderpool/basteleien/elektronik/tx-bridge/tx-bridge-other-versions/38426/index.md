---
layout: "image"
title: "5V Version"
date: "2014-03-03T11:03:35"
picture: "txbridgeotherversions2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38426
- /detailse207.html
imported:
- "2019"
_4images_image_id: "38426"
_4images_cat_id: "2861"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38426 -->
As you can see, the IR receiver looks in the wrong direction and the X-tal is a type with the wrong case.