---
layout: "image"
title: "Stepper motor controller bottom view"
date: "2014-03-03T11:03:35"
picture: "txbridgeotherversions5.jpg"
weight: "5"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38429
- /details888e.html
imported:
- "2019"
_4images_image_id: "38429"
_4images_cat_id: "2861"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38429 -->
Very few components are required. 32 units can be controlled over a single I2C bus. my TX-bridge however only supports 4. It does however support advanced motor control in the sense that the motor speeds can be linked to each other. When setting different number of steps for two motors and designating one as master for the other, the motors will reach their destinations simultaneously. This is accomplished with the Bresenham algorithm which makes this ideal for plotters.