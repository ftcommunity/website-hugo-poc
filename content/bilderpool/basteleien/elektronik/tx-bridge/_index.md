---
layout: "overview"
title: "tx bridge"
date: 2020-02-22T07:43:58+01:00
legacy_id:
- /php/categories/1913
- /categories0314.html
- /categories9e29.html
- /categories195c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1913 --> 
The circuit diagram of my bridge between the TX-controller and the old I/O-Extensions. The bridge also features an IR interface for the new ControlSet and connections for 4 servos or Sharp distance sensors.