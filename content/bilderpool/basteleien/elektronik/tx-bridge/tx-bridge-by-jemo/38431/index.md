---
layout: "image"
title: "Closed case"
date: "2014-03-03T11:03:35"
picture: "txbridgebyjemo2.jpg"
weight: "2"
konstrukteure: 
- "JEMO"
fotografen:
- "JEMO"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38431
- /details0152-2.html
imported:
- "2019"
_4images_image_id: "38431"
_4images_cat_id: "2862"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38431 -->
