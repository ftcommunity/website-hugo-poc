---
layout: "image"
title: "Open case"
date: "2014-03-03T11:03:35"
picture: "txbridgebyjemo1.jpg"
weight: "1"
konstrukteure: 
- "JEMO"
fotografen:
- "JEMO"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38430
- /details92ad.html
imported:
- "2019"
_4images_image_id: "38430"
_4images_cat_id: "2862"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38430 -->
On prototype board with single 6-pin connector and an LED