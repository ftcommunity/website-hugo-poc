---
layout: "image"
title: "In the lid"
date: "2014-03-03T11:03:35"
picture: "txbridge6.jpg"
weight: "6"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38423
- /details5227.html
imported:
- "2019"
_4images_image_id: "38423"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38423 -->
Without the mounting posts it fits nicely