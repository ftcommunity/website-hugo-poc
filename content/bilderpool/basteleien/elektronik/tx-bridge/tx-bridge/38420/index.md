---
layout: "image"
title: "PCB with SMDs"
date: "2014-03-03T11:03:35"
picture: "txbridge3.jpg"
weight: "3"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38420
- /detailsf355.html
imported:
- "2019"
_4images_image_id: "38420"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38420 -->
I used hot air soldering which made a bit of a mess because it is difficult to apply the correct amount of paste. Also the Voltage regulator is missing because I didn't have any left.