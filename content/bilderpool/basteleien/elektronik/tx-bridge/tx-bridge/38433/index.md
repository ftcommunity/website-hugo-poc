---
layout: "image"
title: "tx-bridge in case with mounting posts"
date: "2014-03-07T10:45:14"
picture: "foto_1.jpg"
weight: "7"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/38433
- /details917c.html
imported:
- "2019"
_4images_image_id: "38433"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38433 -->
Since the case is 0.5mm bigger than the lid, it is probably better to mount the PCB from underneath to the case rather than from above to the lid.