---
layout: "image"
title: "Master"
date: "2010-03-23T19:36:37"
picture: "tx03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/26799
- /details4db2.html
imported:
- "2019"
_4images_image_id: "26799"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26799 -->
The master shows that 3 extensions are connected, the bridge itself (8) and two IO-Extensions (1,2)