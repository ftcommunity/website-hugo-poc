---
layout: "image"
title: "cmucam_explorer_2"
date: "2008-01-16T16:52:10"
picture: "cmucam-12.jpg"
weight: "7"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/13340
- /detailsf422.html
imported:
- "2019"
_4images_image_id: "13340"
_4images_cat_id: "1212"
_4images_user_id: "716"
_4images_image_date: "2008-01-16T16:52:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13340 -->
The explorer with a CmuCam3 and the US distance sensor