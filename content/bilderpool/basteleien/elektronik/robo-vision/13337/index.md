---
layout: "image"
title: "cmucam_4"
date: "2008-01-16T16:52:09"
picture: "cmucam-08.jpg"
weight: "4"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/13337
- /detailsea3e.html
imported:
- "2019"
_4images_image_id: "13337"
_4images_cat_id: "1212"
_4images_user_id: "716"
_4images_image_date: "2008-01-16T16:52:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13337 -->
back side