---
layout: "overview"
title: "Spursucher as quadrature encoder with fast counter (ZE)"
date: 2020-02-22T07:43:52+01:00
legacy_id:
- /php/categories/1666
- /categories86ed-2.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1666 --> 
As part of my somewhat longer project of adding an LCD to the Robo Interface and combining C and RoboPro, I found a way of using the fast counter in the Robo Interface escpecially for use with encoder motors like the Maxon. I don\'t have such a motor so I used a Spursucher to achieve a similar result.