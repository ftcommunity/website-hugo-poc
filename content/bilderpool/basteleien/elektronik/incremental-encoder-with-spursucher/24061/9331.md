---
layout: "comment"
hidden: true
title: "9331"
date: "2009-05-21T20:22:27"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
The hardware counter could be used if we do the conversion from quadrature to up/down count externally. I don't think there are any more timers available but with extension modules anything is possible :)

@schnaggels: Thats good because, as a colour sensor it sucks.

It probably isn't clear from the picture but with proper alignment you can determine the count direction and increase the resolution (see my robopro prog for an example on how to achieve this).
For the segmentscheibe I used the excel sheet from Andreas Guerten, works a charm.