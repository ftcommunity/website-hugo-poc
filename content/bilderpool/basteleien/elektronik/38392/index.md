---
layout: "image"
title: "Drehencoder vorne"
date: "2014-02-26T08:17:56"
picture: "IMG_0003.jpg"
weight: "18"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38392
- /details93cd.html
imported:
- "2019"
_4images_image_id: "38392"
_4images_cat_id: "466"
_4images_user_id: "1359"
_4images_image_date: "2014-02-26T08:17:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38392 -->
Nutzbarmachung herkömmlicher Drehencoder (hier einer mit 3mm-Achse) auf Rast-Achsen-Verbinder adaptiert (Feile, Superkleber, ein Rest Schaschlik-Spieß)
Lieferant: RS-Components
