---
layout: "image"
title: "All items of the project!"
date: "2006-09-04T21:30:07"
picture: "rrb_006.jpg"
weight: "17"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6782
- /details3d88.html
imported:
- "2019"
_4images_image_id: "6782"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-09-04T21:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6782 -->
Waiting for the PCB's, later this week all boxes will be assembled.
