---
layout: "image"
title: "Close up"
date: "2009-09-21T22:36:15"
picture: "DSC_0007.jpg"
weight: "31"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/25051
- /detailsd987.html
imported:
- "2019"
_4images_image_id: "25051"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2009-09-21T22:36:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25051 -->
