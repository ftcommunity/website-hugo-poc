---
layout: "image"
title: "Proto Type of the Print and Sonar"
date: "2006-06-27T16:42:52"
picture: "Desktop_002.jpg"
weight: "11"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6586
- /detailse81f-2.html
imported:
- "2019"
_4images_image_id: "6586"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-27T16:42:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6586 -->
Hi all,

The sensor is finshed. The prototype fits the 9V FT box, with a little trouble.

The pcb will we created after extensive testing. It looks great!
