---
layout: "image"
title: "SRF05 sensor connected the the standard PCB"
date: "2006-10-28T08:17:34"
picture: "thebad.jpg"
weight: "24"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/7230
- /detailsf341.html
imported:
- "2019"
_4images_image_id: "7230"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7230 -->
Special for Thomas
