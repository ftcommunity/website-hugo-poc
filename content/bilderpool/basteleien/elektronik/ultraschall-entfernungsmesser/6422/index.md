---
layout: "image"
title: "DistanceSensor work in progress"
date: "2006-06-13T22:47:52"
picture: "work_in_progress.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
schlagworte: ["distance", "sensor", "Robo", "pro", "D", "eingang"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6422
- /details7256-2.html
imported:
- "2019"
_4images_image_id: "6422"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-13T22:47:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6422 -->
Finally we have received the sensors. The timing diagram is readable from the scoop.

Will post the result!

We Measure from 2 till 300 cm in range of 6V. So >3 meter ==> 6V output from the sensor.
Below 2cm the sensor will not reply any data.
The scale is linear! so 1.5meter means 3V.

Looks VERY promising..
