---
layout: "image"
title: "Robo interface in action"
date: "2006-06-24T19:38:11"
picture: "Desktop_006.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6569
- /details7d6e.html
imported:
- "2019"
_4images_image_id: "6569"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6569 -->
Value of Sensor directly input as A1.
The Value should be 3.3V, Robo interface stated 3.4V.

NICE...
