---
layout: "image"
title: "Ultra Sonic sensor at work..."
date: "2006-06-24T19:38:11"
picture: "sensor_005.jpg"
weight: "10"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6573
- /detailsc3e0-2.html
imported:
- "2019"
_4images_image_id: "6573"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6573 -->
3.3 Volt means 94 cm, not calibrated yet, design is 2V per meter. Max range apprx. 3 meter.
