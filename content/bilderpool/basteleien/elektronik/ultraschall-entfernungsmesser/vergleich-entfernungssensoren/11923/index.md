---
layout: "image"
title: "Close up FT sensor and the Reed contact"
date: "2007-09-23T18:24:07"
picture: "comparedistancesensorfischertechnik14.jpg"
weight: "14"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11923
- /details286d.html
imported:
- "2019"
_4images_image_id: "11923"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T18:24:07"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11923 -->
