---
layout: "image"
title: "full motion: Sensors in action"
date: "2007-09-23T18:24:07"
picture: "comparedistancesensorfischertechnik15.jpg"
weight: "15"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11924
- /detailsf5b2-2.html
imported:
- "2019"
_4images_image_id: "11924"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T18:24:07"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11924 -->
