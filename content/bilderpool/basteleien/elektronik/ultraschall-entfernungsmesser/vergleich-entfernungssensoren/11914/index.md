---
layout: "image"
title: "Average function"
date: "2007-09-23T17:39:05"
picture: "comparedistancesensorfischertechnik05.jpg"
weight: "5"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11914
- /details8d7c.html
imported:
- "2019"
_4images_image_id: "11914"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11914 -->
Both sensors will produce 6 measurements before they will be utilized.
