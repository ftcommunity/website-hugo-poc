---
layout: "image"
title: "Output again, standardized to the same Y axe scale"
date: "2007-09-23T17:39:05"
picture: "comparedistancesensorfischertechnik08.jpg"
weight: "8"
konstrukteure: 
- "richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11917
- /details7274.html
imported:
- "2019"
_4images_image_id: "11917"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11917 -->
Use the same scale for both samples, 
The actual distance is between 3 cm and 119 cm.

Have to figure out how to get ride of the spikes.
