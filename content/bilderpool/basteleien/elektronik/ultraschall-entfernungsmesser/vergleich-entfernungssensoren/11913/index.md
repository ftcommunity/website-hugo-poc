---
layout: "image"
title: "Robo Pro program."
date: "2007-09-23T17:39:04"
picture: "comparedistancesensorfischertechnik04.jpg"
weight: "4"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/11913
- /details33bc.html
imported:
- "2019"
_4images_image_id: "11913"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11913 -->
The left Gauge is the FT sensor, I connected my sensor to A1, the sensor of FT to the D1 eingang.

The train is sampling during 7 passes. The speed of the motor is set to 4. 

The FT sensor is calibrated directly to full cm, e.g. 13cm, My own sensor is using .5cm as output, so divide by 2.
