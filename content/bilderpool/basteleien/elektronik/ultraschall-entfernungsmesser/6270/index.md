---
layout: "image"
title: "DistanceSensorBlockDiagram"
date: "2006-05-17T16:38:32"
picture: "distanceSensorBlockdiagram.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
schlagworte: ["Robo", "Pro", "distance", "sensor"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- /php/details/6270
- /details258f.html
imported:
- "2019"
_4images_image_id: "6270"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-05-17T16:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6270 -->
Ultrasonic Sensor
Electronic schema for the distance sensor.

    * Range - 2cm to 3m (~.75" to 10')
    * Supply Voltage: 5V +/-10% (Absolute: Minimum 4.5V, Maximum 6V)
    * Supply Current: 30 mA typ; 35 mA max
    * 3-pin interface (power, ground, signal)
    * 20 mA power consumption
    * Narrow acceptance angle
    * Simple pulse in / pulse out communication
    * Indicator LED shows measurement in progress
    * Input Trigger - positive TTL pulse, 2 uS min, 5 uS typ.
    * Echo Pulse - positive TTL pulse, 115 uS to 18.5 mS
    * Echo Hold-off - 350 uS from fall of Trigger pulse
    * Burst Frequency - 40 kHz for 200 uS
    * Size - 22 mm H x 46 mm W x 16 mm D (0.85 in x 1.8 in x 0.6 in)
