---
layout: "image"
title: "Platine"
date: "2017-05-08T18:14:36"
picture: "wfpp3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45801
- /detailsdb32.html
imported:
- "2019"
_4images_image_id: "45801"
_4images_cat_id: "3404"
_4images_user_id: "2228"
_4images_image_date: "2017-05-08T18:14:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45801 -->
doppelschichtige Platine für Spannungsregler, Motortreiber, Schalter, Stecker und natürlich den Mikrocontroller
