---
layout: "image"
title: "Funktion als IR Empfänger"
date: "2017-05-14T15:37:35"
picture: "IMG_1895.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
schlagworte: ["IR", "Wifi", "iOS", "Fernsteuerung"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/45808
- /detailsff2d-2.html
imported:
- "2019"
_4images_image_id: "45808"
_4images_cat_id: "3404"
_4images_user_id: "2228"
_4images_image_date: "2017-05-14T15:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45808 -->
Der Wifi Controller nutzt nun auch seine Infrarotdiode und ist damit 100 % kompatibel zum Handsender des Fischertechnik Control Sets. 

Verglichen mit der ersten Generation IR Empfänger, hinten im Bild (https://ftcommunity.de/categories.php?cat_id=3219, oder der ft:pedia Artikel dazu: https://ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-2.pdf) ist die zweite Generation über 70 % kleiner, leistungsfähiger und hat mehr Outputs.
