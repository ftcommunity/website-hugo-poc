---
layout: "comment"
hidden: true
title: "23439"
date: "2017-06-05T10:46:28"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Danke für das Lob :-)

Ich habe bis jetzt keine Anleitung erstellt, hauptsächlich aufgrund mangelhafter Kompatibilität des CAD Programms: (vgl. https://ftcommunity.de/details.php?image_id=45923)

Zur Kontaktaufnahme schicke mir doch bitte eine PN im Forum an "davidrpf".

Gruß
David