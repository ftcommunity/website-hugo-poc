---
layout: "image"
title: "Signale am FT-Interface noch als jpg für die Vorschau"
date: "2014-04-09T10:06:21"
picture: "E1_E4_M1_M2_8bit.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38539
- /detailsee4b.html
imported:
- "2019"
_4images_image_id: "38539"
_4images_cat_id: "2877"
_4images_user_id: "579"
_4images_image_date: "2014-04-09T10:06:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38539 -->
