---
layout: "image"
title: "FT Universal-Interface geöffnet mit eingebautem AVR Mikrocontroller"
date: "2014-04-08T13:44:57"
picture: "FTUI_mod_offen.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["AVR", "Parallel", "Interface", "Universal"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/38535
- /detailsd9a0.html
imported:
- "2019"
_4images_image_id: "38535"
_4images_cat_id: "2877"
_4images_user_id: "579"
_4images_image_date: "2014-04-08T13:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38535 -->
Das kleine Board fügt sich gut in den vorhandenen Platz ein. Die Unterseite ist mit Pappe isoliert, die mit Tesafilm am Rand des Boards festgeklebt wurde. Hier nutze ich auch die analogen Eingänge Ex und Ey an Pin 13 & 14 des FT-Interfaces, um sie direkt den analogen Eingängen PA1 & 2 des ATTiny26 zuzuführen.
