---
layout: "comment"
hidden: true
title: "18901"
date: "2014-04-09T09:19:37"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Laut Datenblatt lässt sich das Eingangsschieberegister HCF4014 sogar bis 6 MHz takten, das Ausgangsschieberegister HCF4094 dagegen nur bis 1,5 MHz.

Wenn Du an meinem Code dafür interessiert bist kannst Du ihn hier herunterladen:

http://home.arcor.de/uffmann/Electronics-Dateien/FTUI.zip

Hier habe ich das Ganze auch noch genauer beschrieben:

http://home.arcor.de/uffmann/Mikrocontroller.html

http://home.arcor.de/uffmann/Mikrocontroller-Dateien/ft2.pdf