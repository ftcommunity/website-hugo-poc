---
layout: "overview"
title: "Altes FT Universal-Interface"
date: 2020-02-22T07:44:15+01:00
legacy_id:
- /php/categories/2877
- /categoriesd651.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2877 --> 
Altes FT Universal-Interface (Parallelschnittstelle) gesteuert durch AVR Mikrocontroller

Einbau eines kleinen Mikrocontroller-Boards mit Infrarot-Empfänger in das Gehäuse des FT-Interfaces, um eine offline-fähige Steuereinheit daraus zu machen oder um ein IR-Control-Set für 4 Motoren daraus zu machen. So entfällt das externe Flach-bandkabel zum Steuerrechner. Der Einbau ist von außen kaum zu erkennen, so dass euch ein erstes Erstaunen von FT-Fans sicher sein dürfte, wie ihr es geschafft habt, das FT-Interface off-line-fähig zu machen.
