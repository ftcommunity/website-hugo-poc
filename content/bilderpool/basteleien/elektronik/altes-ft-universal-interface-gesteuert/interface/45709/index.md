---
layout: "image"
title: "fischertechnik Interface IBM"
date: "2017-04-01T12:40:41"
picture: "ch5.jpg"
weight: "5"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/45709
- /details46e6.html
imported:
- "2019"
_4images_image_id: "45709"
_4images_cat_id: "3392"
_4images_user_id: "2374"
_4images_image_date: "2017-04-01T12:40:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45709 -->
