---
layout: "image"
title: "h4-GB DIL"
date: "2010-06-04T10:54:29"
picture: "h4-GB-Mikro2.jpg"
weight: "15"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/27355
- /details3a2b.html
imported:
- "2019"
_4images_image_id: "27355"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-06-04T10:54:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27355 -->
Die wohl denkbar kleinste Variante des h4-GB; die paßt in eine
16polige IC-Fassung und ist voll funktionsfähig. Nur der Elko, die
Diodenkaskade (Buchsen 1 & 2), die zwei Ausgangs- sowie ein
Rückkopplungswiderstand sind hier weggelassen, können aber
bei Bedarf zusätzlich extern gesetzt werden. Als dritte Version ist
dieser h4-GB in meiner Nachbau-Anleitung enthalten.
