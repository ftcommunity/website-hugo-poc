---
layout: "image"
title: "Zähler"
date: 2023-03-28T11:59:54+02:00
picture: "EModule_6.jpeg"
weight: "6"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["elektronik", " schaltung", " e-m", " Elektromodule"]
uploadBy: "Website-Team"
license: "unknown"
---

größeres Modell mit Auf/Ab