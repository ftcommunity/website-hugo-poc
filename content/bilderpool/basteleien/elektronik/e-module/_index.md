---
layout: "overview"
title: "E-Module"
date: 2023-03-28T11:59:49+02:00
---

Ich wollte eine Reihe von Elektromodulen herstellen, indem ich 60 x 60 mm Kassetten von Fischertechnik wiederverwendete und verschiedene (billige) elektronische Komponenten hinzufügte, um Spannung, Strom, Temperatur, Zeit sowie Timer, Thermostate und Ladegeräte zu messen. Es brauchte einen kundenspezifischen Deckel für die Kassette.

Dies wurde durch einen Remix des exzellenten Designs von Dan Malec unter Verwendung seines fischertechnik 6060 Kassettendeckeldesigns (https://www.thingiverse.com/thing:4544287) als Basis erreicht.

Die verschiedenen Deckel können bei Thingiverse heruntergeladen werden.

(https://www.thingiverse.com/thing:5188458)