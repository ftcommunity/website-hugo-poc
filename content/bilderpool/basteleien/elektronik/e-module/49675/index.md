---
layout: "image"
title: "Solar"
date: 2023-03-28T11:59:52+02:00
picture: "EModule_7.jpeg"
weight: "7"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["elektronik", " schaltung", " e-m", " Elektromodule"]
uploadBy: "Website-Team"
license: "unknown"
---

verstärkt 2V auf 5/9V, niedriger Strom