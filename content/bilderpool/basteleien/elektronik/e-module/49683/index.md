---
layout: "image"
title: "Boost/Buck"
date: 2023-03-28T12:00:03+02:00
picture: "EModule_15.jpeg"
weight: "15"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["elektronik", " schaltung", " e-m", " Elektromodule"]
uploadBy: "Website-Team"
license: "unknown"
---

variabler Aufwärts-Abwärtswandler