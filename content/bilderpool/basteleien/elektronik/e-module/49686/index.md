---
layout: "image"
title: "Relais"
date: 2023-03-28T12:00:07+02:00
picture: "EModule_12.jpeg"
weight: "12"
konstrukteure: 
- "Jeroen Regtien"
fotografen:
- "Jeroen Regtien"
schlagworte: ["elektronik", " schaltung", " e-m", " Elektromodule"]
uploadBy: "Website-Team"
license: "unknown"
---

rechts eine bistabile Version