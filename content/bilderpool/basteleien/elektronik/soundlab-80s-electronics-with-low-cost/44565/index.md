---
layout: "image"
title: "Gesamtaufbau"
date: "2016-10-11T17:32:13"
picture: "soundlabselectronicswithlowcostoscilloscope1.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/44565
- /detailsc7ab-2.html
imported:
- "2019"
_4images_image_id: "44565"
_4images_cat_id: "3315"
_4images_user_id: "1359"
_4images_image_date: "2016-10-11T17:32:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44565 -->
