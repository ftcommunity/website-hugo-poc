---
layout: "image"
title: "03 28pol. Buchsenplatte, Belegung"
date: "2010-02-26T21:03:45"
picture: "robotxcontrollerstiftleistenanschluss3.jpg"
weight: "3"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26554
- /details66d7.html
imported:
- "2019"
_4images_image_id: "26554"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26554 -->
Die von mir gewählte Variante für die Anordnung der max. 36 Modellkabel-Flachstecker auf der 28pol. Buchsenplatte. Die roten und grünen Buchsen sind bei Auslastung mit Flachsteckern doppelt zu belegen. Zur Verbindung mit dem ROBO TX Controller werden dann 23 Pole inkl. nur einer Masseleitung benötigt.

Diese Belegung berücksichtigt noch nicht! die Kompatibilität zum Eingang der modifizierten Adapterplatine 75151 an der Stiftleiste des ROBO TX Controllers, Link dazu unter Bild 07  

Edit 27.02.2010:
Da ich das messtechnisch noch nicht prüfen konnte, sollten vorsorglich über das Flachkabel für +9V eine zweite Leitung rot an 2 oder 22 sowie für die Masse grün zwei weitere an 15 und 25 genutzt werden. Damit wäre das 26pol. Flachbandkabel 25pol. belegt.
