---
layout: "image"
title: "04 28pol. Buchsenplatte, Unterseite"
date: "2010-02-26T21:03:45"
picture: "robotxcontrollerstiftleistenanschluss4.jpg"
weight: "4"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26555
- /detailsc510.html
imported:
- "2019"
_4images_image_id: "26555"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26555 -->
Die Unterseite der 28pol. Buchsenplatte mit der für den ROBO TX Controller modifizierten Verkabelung. Die 26pol. Stiftleiste liegt zwischen die S-Riegel passend auf ihnen verklebt auf.

Edit 28.02.2010: Bitte Hinweise zur Polanzahl für +9V und Masse unter Bild 03 beachten!
