---
layout: "image"
title: "09 Flachsteckerbelegung kpl. 1"
date: "2010-03-04T21:23:27"
picture: "robotxstiftleistenanschluss1.jpg"
weight: "8"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26591
- /details582b.html
imported:
- "2019"
_4images_image_id: "26591"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-03-04T21:23:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26591 -->
Anordnung der max. Flachsteckerbelegung zum besseren Überblick hier zunächst mal ohne Verkabelung

Die 25 Flachstecker rot und grün gehören zur externenen Verkabelung der Stiftleiste.
Die 34 andersfarbigen Flachstecker gehören zu einer Vollbelegung des ROBO TX Controllers bei einem Direktanschluss an Bord des Modells. In den Details vor allem mit den neuen schlankeren und kürzeren Flachsteckern ist sicher noch das "professionell gewohnte" möglich.
Die etwas unorthodoxe Anordnung der Flachstecker der Stiftleiste ermöglicht so das An- oder Aufstecken aller Flachstecker für den Anschluss des TX wahlweise über Stiftleiste oder Buchsenfront.
Einen Schönheitspreis verdient diese universelle Anschlussmöglichkeit natürlich nicht, aber ein Behelf mit praktischen Vorteilen ist es dennoch.
Diesen Vorschlag dürfte auch jeder realisieren können, Garantiefragen treten nicht auf und Teilneuheiten oder modifizierte Teile sind nicht erforderlich.
