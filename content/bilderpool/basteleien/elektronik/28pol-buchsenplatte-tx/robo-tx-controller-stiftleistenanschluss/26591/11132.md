---
layout: "comment"
hidden: true
title: "11132"
date: "2010-03-05T12:25:16"
uploadBy:
- "Svefisch"
license: "unknown"
imported:
- "2019"
---
Kein Schönheitspreis? Wieso, da ist doch eine gewisse künstlerische Esthetik drin. Vielleicht sollten die grauen und die lila Stecker noch etwas anders sortiert werden ...
Nur Anwendungstechnisch - ich weiß nicht. Da pfriemelt man doch mehr rum, als wenn man total neu verkabelt.