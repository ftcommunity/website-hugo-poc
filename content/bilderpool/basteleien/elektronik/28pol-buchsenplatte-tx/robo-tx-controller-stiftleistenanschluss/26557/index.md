---
layout: "image"
title: "06 ROBO TX Controller, Flachstecker rechts"
date: "2010-02-26T21:03:46"
picture: "robotxcontrollerstiftleistenanschluss6.jpg"
weight: "6"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Ingo Herschel (Udo2)"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- /php/details/26557
- /details248a.html
imported:
- "2019"
_4images_image_id: "26557"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26557 -->
Eine drehbare Aufnahme der 26pol. Stiftleiste mit 2x Gelenkwürfel kpl. statt der 2x BS5 ist auch möglich. Sie ist vorteilhaft für eine fixierte Unterführung des Flachkabels und darüber eine pultartig liegende bis stehende Anordnung des ROBO TX Controllers bei einer Steuerbasis mit ein oder mehreren TX nebeneinander z.B. auf einer ft-Platte.
