---
layout: "image"
title: "Seitenansicht von der Digitalseite (oben)"
date: "2012-10-07T23:20:05"
picture: "ftArduinoMiniDigital.jpg"
weight: "4"
konstrukteure: 
- "Dirk Grebe"
fotografen:
- "Dirk Grebe"
schlagworte: ["Controller", "Elektronik", "Arduino", "Motor"]
uploadBy: "gravy"
license: "unknown"
legacy_id:
- /php/details/35840
- /detailsbb69.html
imported:
- "2019"
_4images_image_id: "35840"
_4images_cat_id: "2677"
_4images_user_id: "854"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35840 -->
Seitenansicht des Aufbaus von von der Digitalseite (oben)
Rechts ist der stehende L293 zu sehen