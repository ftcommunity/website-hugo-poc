---
layout: "overview"
title: "Arduino Controller mini"
date: 2020-02-22T07:44:08+01:00
legacy_id:
- /php/categories/2677
- /categories9eed.html
- /categoriesc3cf.html
- /categories31db.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2677 --> 
Motorcontroller basierend auf ATMega328 mit Arduino Bootloader im kleinen ft Batteriegehäuse.
- 2 Motorausgänge
- 2 PWM Ausgänge
- 6 Analog/ Digitaleingänge
- IR Erweiterung möglich