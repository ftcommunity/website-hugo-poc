---
layout: "image"
title: "Microkontrollerboard für Universal Interface"
date: "2014-09-14T12:48:39"
picture: "ft_parInt2PIC4.jpg"
weight: "1"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
schlagworte: ["Interface", "PIC", "Microkontroller"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/39358
- /details5544.html
imported:
- "2019"
_4images_image_id: "39358"
_4images_cat_id: "2949"
_4images_user_id: "427"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39358 -->
Da sich nunmehr schon 4 Interfaces angesammelt haben, war es Zeit sie sinnvoll zu nutzen. Angeregt von "uffi" ea. wurde ein kleiner Zusatzprint mit PIC Mikrokontroller gebaut um das Interface autonom zu steuern. Die Pins dienen als Programmier-/I2C/UART/SPI Schnittstelle, je nach Programm und Anwendung. Die 5V wurden vom Interface abgezapft. ( siehe Bild3 )