---
layout: "image"
title: "Interface mit IR Steuerung"
date: "2014-09-14T12:48:51"
picture: "ft_parInt2PIC6.jpg"
weight: "2"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
schlagworte: ["Interface", "PIC", "Microkontroller"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- /php/details/39359
- /details5997.html
imported:
- "2019"
_4images_image_id: "39359"
_4images_cat_id: "2949"
_4images_user_id: "427"
_4images_image_date: "2014-09-14T12:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39359 -->
Die Schnittstelle für den IR Empfänger genützt