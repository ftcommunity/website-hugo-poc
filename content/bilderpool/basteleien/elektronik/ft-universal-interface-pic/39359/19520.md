---
layout: "comment"
hidden: true
title: "19520"
date: "2014-09-16T10:28:00"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Ah, hier sieht man es besser: Du hast den originalen Stecker für das Flachbandkabel zum Anschluss an den PC ausgelötet und durch eine neue Buchsenleiste ersetzt, in die Du das neue Board mit seiner Steckerleiste einstecken kannst. Toll gemacht!!!