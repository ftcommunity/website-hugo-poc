---
layout: "image"
title: "Spule"
date: "2011-07-14T11:35:20"
picture: "bild6.jpg"
weight: "12"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31242
- /details449c-2.html
imported:
- "2019"
_4images_image_id: "31242"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31242 -->
Meine Spule aus einem alten Toaster, eignet sich auch als Magnet.