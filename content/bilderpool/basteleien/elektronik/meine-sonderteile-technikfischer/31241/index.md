---
layout: "image"
title: "Kompressor"
date: "2011-07-14T11:35:20"
picture: "bild5.jpg"
weight: "11"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31241
- /details8131.html
imported:
- "2019"
_4images_image_id: "31241"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31241 -->
Kompressor im Gehäuse, 9V Batteriefach, in das ich ein Loch für den Sclauch gefräst habe.