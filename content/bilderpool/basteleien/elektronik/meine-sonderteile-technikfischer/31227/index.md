---
layout: "image"
title: "Punktlaser"
date: "2011-07-14T11:10:48"
picture: "bild02.jpg"
weight: "2"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31227
- /detailseefd.html
imported:
- "2019"
_4images_image_id: "31227"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:10:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31227 -->
Hier der Punktlaser