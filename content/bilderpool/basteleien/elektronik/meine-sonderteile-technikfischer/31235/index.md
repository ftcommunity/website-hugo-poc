---
layout: "image"
title: "Display"
date: "2011-07-14T11:10:48"
picture: "bild10.jpg"
weight: "5"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- /php/details/31235
- /details560a.html
imported:
- "2019"
_4images_image_id: "31235"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:10:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31235 -->
Hier mein teuerstes Element (50?), ein 16 x 2 Blockdisplay, ein RoboPro -Programm kann man unter Downloads runterladen
Es ist in einem selbstgebauten Printplattengehäuse eingebaut