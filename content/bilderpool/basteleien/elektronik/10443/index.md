---
layout: "image"
title: "Komischer Sensor"
date: "2007-05-16T19:23:07"
picture: "Sensor.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/10443
- /details1c71.html
imported:
- "2019"
_4images_image_id: "10443"
_4images_cat_id: "466"
_4images_user_id: "445"
_4images_image_date: "2007-05-16T19:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10443 -->
Das ist ein Scann.
