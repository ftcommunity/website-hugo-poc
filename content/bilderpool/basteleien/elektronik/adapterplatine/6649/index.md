---
layout: "image"
title: "Adapterplatine für Robo-Interface 75151"
date: "2006-07-22T16:29:00"
picture: "Bild3_Motor_linkslauf.jpg"
weight: "3"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/6649
- /detailsf1d5.html
imported:
- "2019"
_4images_image_id: "6649"
_4images_cat_id: "572"
_4images_user_id: "426"
_4images_image_date: "2006-07-22T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6649 -->
Hier leuchten die LED´s alle Motore auf Linkslauf !!!!
