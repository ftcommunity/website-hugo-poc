---
layout: "comment"
hidden: true
title: "1625"
date: "2006-11-30T21:54:05"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Adapterplatine für Robo-Interface 75151 von Knobloch

noch kurz eine "offizielle" Info:
1)
Für die gewünschte "gemeinsame Masse" kann der "-" Anschluss der externen Stromversorgung (rechts neben der 26pol. Wanne) verwendet werden. Die Masse an diesem Anschluss ist mit der "Leistungs-" Masse an der 26pol. Stiftleiste verbunden und kann daher als "Rückleitung" für die Ausgänge O1..O8 dienen. Man sollte aber beachten, dass die Steckverbinder für das Flachbandkabel (und somit die Masse-Leitung vom Robo-Interface) nur bis ca. 600-800mA belastbar ist, da die Flachbandkabel eigentlich nur für die Datenübertragung in der EDV gedacht sind! Da sie so praktisch sind, werden sie hier "missbraucht"

2)
Die "+" Leitung an den Tastern dienen nur für die Taster. Hier sollte man keine "Lasten" anschließen. Wer also ein Modell für das Interface baut und zusätzlich "Strom" für z.B. seine Dauerbeleuchtung / Blinkelektronik benötigt, sollte diese nicht vom Flachbandkabel verwenden. Hier ist es sinnvoll, eine getrennte Zuleitung zu legen. 

3)
Der ZE - Eingang liegt auch auf der 26pol. Leiste, daher wurde er herausgeführt. Aktuell wird er aber noch nicht verwendet.

4)
Die Analogeingänge verwenden ihre eigene "Analoge-Masse".


Gruß
Ralf Knobloch