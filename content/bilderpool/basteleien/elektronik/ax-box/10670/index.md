---
layout: "image"
title: "Einzellteile"
date: "2007-06-03T19:07:00"
picture: "axbox1.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10670
- /details1c7a.html
imported:
- "2019"
_4images_image_id: "10670"
_4images_cat_id: "969"
_4images_user_id: "453"
_4images_image_date: "2007-06-03T19:07:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10670 -->
Hier sieht man die Hauptplatine auf der die, die Widerstände gelötet sind. Auf den beiden neben Platinen sind die Steckmuffen für die Anschlüsse.
