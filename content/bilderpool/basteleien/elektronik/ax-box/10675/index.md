---
layout: "image"
title: "Mit anschlüssen"
date: "2007-06-03T19:07:01"
picture: "axbox6.jpg"
weight: "6"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/10675
- /detailsde4e.html
imported:
- "2019"
_4images_image_id: "10675"
_4images_cat_id: "969"
_4images_user_id: "453"
_4images_image_date: "2007-06-03T19:07:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10675 -->
Die oberen drei Anschlüsse kommen an jeweils einen Taster, der untere Anschluss kommt an alle Taster. Das Gelb/Grüne Kabel kommt an den AX Eingang des Interfaces.
So können also drei Taster an einem AX Eingang betrieben werden. Man könnte die Box auch noch erweitern.

Ich muss dazu sagen das diese mein erstes selber gebautes Elektrisches Teil ist, deshalb ist es an manchen Stellen nicht so perfekt geworden, mit dem Platinen zu schneiden habe ich auch noch so meine Probleme.....
