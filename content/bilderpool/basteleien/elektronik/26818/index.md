---
layout: "image"
title: "Platine h4-GB für E-Tec Gehäuse"
date: "2010-03-25T20:27:35"
picture: "h4-GB-Mini.jpg"
weight: "9"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/26818
- /details3172.html
imported:
- "2019"
_4images_image_id: "26818"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-03-25T20:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26818 -->
Dies ist zunächst die fertige Platine zum Nachbau des h4-Grundbausteins im Design "E-Tec". Näheres und weitere Bilder später...
