---
layout: "image"
title: "h4-GB-Lu - Fertigmodul"
date: "2010-05-04T14:11:09"
picture: "h4-GB-KU.jpg"
weight: "14"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/27059
- /detailse365.html
imported:
- "2019"
_4images_image_id: "27059"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-05-04T14:11:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27059 -->
Dies ist das fertige Modul des h4-Grundbausteins mit umschaltbarer Logik sowie zwei zusätzlichen Buchsen für Plus und Minus. Die spätere Erweiterung auf ein einheitliches Stromversorgungssystem aller Nachbauten sowie des E-Tec Moduls (z. B. verpolungssichere Stecker) ist möglich.
