---
layout: "image"
title: "'TX-' Adapterplatine"
date: "2010-02-10T18:54:42"
picture: "TX-Adapterplatine-8.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/26320
- /details015f.html
imported:
- "2019"
_4images_image_id: "26320"
_4images_cat_id: "1870"
_4images_user_id: "22"
_4images_image_date: "2010-02-10T18:54:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26320 -->
