---
layout: "comment"
hidden: true
title: "11064"
date: "2010-02-28T12:17:25"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Pinbelegung Adapterplatine-75151 zum Robo-TX-Controller :

Die Robo-Interface-Pinbelegung der heutige FT-Adapterplatine 75175 kann man nicht ändern !
( = Pinbelegung wie auch im Bedienungsanleitung Robo-Interface )

1	=  braun = nicht verwendet weil beim TX alle (Taster-) Eingänge mit -Masse verbunden sind und  beim Robo-Interface mit +9V  !!!......

2	=  rot =  -Masse für analog Anschlusse ( jetzt für C1 bis C4 )

3	=  oranje = C3

4	=  gelb   =  C4

5	=  grun    = C1

6	=  blau    = C2

7	=  violett = +9V  ( 7+8 gemeinsam in ein Stecker )

8	= grau     = +9V  ( 7+8 gemeinsam in ein Stecker )

9    = weiss     = -Masse als Gegenpol für Leitungsausgänge O1..8  ( 9+10 gemeinsam in ein Stecker )

10  = schwarz = -Masse als Gegenpol für Leitungsausgänge O1..8  ( 9+10 gemeinsam in ein Stecker )


11 =  braun   = I1
12 =  rot     = I2
13 = oranje   = I3
14 = gelb     = I4
15 = grun     = I5
16 = blau     = I6
17 = violet   = I7
18 = grau     = I8
19 = weiss      = O1
20 = schwarz    = O2
21 = braun      = O3
22 = rot        = O4
23 = oranje     = O5
24 = gelb       = O6
25 = grun       = O7
26 = blau       = O8

Grüss, 

Peter, Poederoyen NL