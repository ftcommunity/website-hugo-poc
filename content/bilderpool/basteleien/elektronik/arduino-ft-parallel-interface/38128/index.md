---
layout: "image"
title: "Arduino und 80er Elektronik LST"
date: "2014-01-26T19:53:12"
picture: "IMG_0130.jpg"
weight: "13"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38128
- /detailsc4c5.html
imported:
- "2019"
_4images_image_id: "38128"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2014-01-26T19:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38128 -->
angebundene Leistungstsufe
