---
layout: "image"
title: "Testprogramm - alles leuchtet"
date: "2014-01-26T19:53:12"
picture: "IMG_0131.jpg"
weight: "14"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38129
- /detailsab54.html
imported:
- "2019"
_4images_image_id: "38129"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2014-01-26T19:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38129 -->
Testprogramm:
Poti wird abgefragt und je nach Stellung 1, 2, 3 oder 4 Lampen und in der 5. Stufe der Motor (über Elektronik-Leistungsstufe) eingeschaltet. Ich entschuldige mich für die Bildqualität, die Fotos wurden auf die schnelle mit nem alten IPad "geschossen"
