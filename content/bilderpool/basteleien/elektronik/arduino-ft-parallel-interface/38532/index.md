---
layout: "image"
title: "2 Interfaces am Start!"
date: "2014-04-06T14:23:19"
picture: "IMG_0005.jpg"
weight: "15"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38532
- /details8de2.html
imported:
- "2019"
_4images_image_id: "38532"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2014-04-06T14:23:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38532 -->
ich habe die beiden Interfaces durchgeschliffen und kann nu 16 Eingänge abfragen und 8 Motoren in je 2 Richtungen "fahren" :-)
