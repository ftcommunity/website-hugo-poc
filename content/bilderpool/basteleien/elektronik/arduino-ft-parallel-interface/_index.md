---
layout: "overview"
title: "Arduino FT Parallel Interface"
date: 2020-02-22T07:44:11+01:00
legacy_id:
- /php/categories/2738
- /categoriesff03.html
- /categories8407.html
- /categories8cac.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2738 --> 
Ansteuerung eines alten FT Computing Parallel-Interfaces mit einem Arduino-Board