---
layout: "image"
title: "Das Objektiv"
date: "2005-11-10T22:35:54"
picture: "Kamera_-_14.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5295
- /details6394.html
imported:
- "2019"
_4images_image_id: "5295"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:35:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5295 -->
