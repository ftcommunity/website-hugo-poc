---
layout: "image"
title: "Rückansicht"
date: "2005-11-10T22:34:12"
picture: "Kamera_-_08.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/5293
- /detailsfbf6.html
imported:
- "2019"
_4images_image_id: "5293"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:34:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5293 -->
