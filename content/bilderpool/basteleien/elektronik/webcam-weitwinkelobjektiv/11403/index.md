---
layout: "image"
title: "Screenshot"
date: "2007-08-23T18:42:44"
picture: "Bild_1.jpg"
weight: "8"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
schlagworte: ["screeenshot", "webcam", "lampenfassung"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/11403
- /details3cb2-2.html
imported:
- "2019"
_4images_image_id: "11403"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2007-08-23T18:42:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11403 -->
Dieses fürchterliche Bild zeigt die Lampenfassung bei f/16 und einer Glühbirnenspannung von 5 Volt. Die Glühwendel scharf abzubilden, war nicht drin, dafür ist das Objektiv nicht gut genug (zu harter Kontrast). Der Sensor ist erstaunlich empfindlich und überstrahlt sehr schnell.

Die 50mm Brennweite ergeben an dieser Kamera auf 35cm also ein Sichtfeld von <5 mm. Ein Gehäuse gibts immer noch nicht, die Kamera liefert also nur im Dunklen Bilder. 

Zeit, die Webcam auf den Mond zu richten.