---
layout: "image"
title: "Wandlerbaustein"
date: "2009-04-13T20:49:34"
picture: "Wandler.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/23712
- /details6984.html
imported:
- "2019"
_4images_image_id: "23712"
_4images_cat_id: "1619"
_4images_user_id: "182"
_4images_image_date: "2009-04-13T20:49:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23712 -->
Hier ist der fertige Baustein zu sehen. Als Gehäuse habe ich einen V-Stein 30x30x15 genommen. Den Deckel habe ich mir selber gemacht.
