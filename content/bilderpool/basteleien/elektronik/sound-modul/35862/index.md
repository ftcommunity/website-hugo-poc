---
layout: "image"
title: "GND isolieren 2"
date: "2012-10-09T21:02:19"
picture: "soundmodul11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35862
- /details6d69.html
imported:
- "2019"
_4images_image_id: "35862"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35862 -->
Dieses Kabel muss isoliert werden damit es auf der Rückseite der Platine im Soundmodul keine Kurzschlüsse gibt.


Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.