---
layout: "image"
title: "Platine"
date: "2012-10-09T21:02:19"
picture: "soundmodul14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- /php/details/35865
- /detailsbec8.html
imported:
- "2019"
_4images_image_id: "35865"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35865 -->
Auf der Rückseite der Platine sind die Pole laut meinen Erkenntnissen wie auf dem Bild angeordnet.
Auf der Rückseite der Platine die Kabel anlöten L und R zusammenschließen und das Kabel durch das Loch in der Platine fädeln wie beim Original.

Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.