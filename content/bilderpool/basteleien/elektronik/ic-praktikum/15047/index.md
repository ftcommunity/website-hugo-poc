---
layout: "image"
title: "Nachbau des IC14  Bausteins (2x)"
date: "2008-08-15T17:00:28"
picture: "IC14_front_s.jpg"
weight: "19"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["IC14", "IC-Digital-Praktikum"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15047
- /detailsdbbe.html
imported:
- "2019"
_4images_image_id: "15047"
_4images_cat_id: "651"
_4images_user_id: "579"
_4images_image_date: "2008-08-15T17:00:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15047 -->
Da man mit einem einzigen IC14 Baustein nicht so weit kommt, wenn man Steuerungen für Modelle damit aufbauen will, habe ich mir einen Nachbau mit weiteren IC14 Fassungen gemacht.

Wer es nachbauen will, es gibt die eagle Dateien zum Download auf meiner Homepage:

http://home.arcor.de/uffmann/Electronics.html