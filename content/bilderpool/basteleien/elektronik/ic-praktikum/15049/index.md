---
layout: "image"
title: "IC14 mit Motortreiber IC L293D"
date: "2008-08-15T17:00:28"
picture: "IC14_L293D_front_s.jpg"
weight: "21"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["IC14", "IC-Digital-Praktikum", "L293D"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15049
- /detailsf1f6-2.html
imported:
- "2019"
_4images_image_id: "15049"
_4images_cat_id: "651"
_4images_user_id: "579"
_4images_image_date: "2008-08-15T17:00:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15049 -->
Wenn man mit Digitaler Logik Motoren ansteuern will, braucht man einen Motortreiber. Alternativ zum Relais kann man das hier verwendete Motortreiber IC L293DNE einsetzen.

Eagle Dateien zum Download git es hier:

http://home.arcor.de/uffmann/Electronics.html