---
layout: "image"
title: "Einer-Relaisbaustein"
date: "2008-01-12T23:52:41"
picture: "Digitalsteine_007.jpg"
weight: "14"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/13313
- /detailsb56b.html
imported:
- "2019"
_4images_image_id: "13313"
_4images_cat_id: "651"
_4images_user_id: "473"
_4images_image_date: "2008-01-12T23:52:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13313 -->
Es ist zwar eine normale IC Steckkonsoe, allerdings von Fischer offiziell in dieser version vermarktet. Dazu gibt es von Fischer eine Anleitung