---
layout: "image"
title: "close-up of prototype board"
date: "2008-06-22T14:52:08"
picture: "protoboard800.jpg"
weight: "2"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/14751
- /details2260.html
imported:
- "2019"
_4images_image_id: "14751"
_4images_cat_id: "1350"
_4images_user_id: "716"
_4images_image_date: "2008-06-22T14:52:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14751 -->
