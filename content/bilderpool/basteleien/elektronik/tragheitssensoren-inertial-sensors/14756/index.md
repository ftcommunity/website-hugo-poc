---
layout: "image"
title: "cassette with CMP03 sensor"
date: "2008-06-22T14:52:08"
picture: "sensorpack_034_crop800.jpg"
weight: "7"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/14756
- /details218d.html
imported:
- "2019"
_4images_image_id: "14756"
_4images_cat_id: "1350"
_4images_user_id: "716"
_4images_image_date: "2008-06-22T14:52:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14756 -->
The sensor board in the cassette with compass sensor but without the transparent lid and LCD