---
layout: "image"
title: "Platine für Wandler"
date: "2011-01-27T22:49:34"
picture: "impulsewandler1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29812
- /detailsb544.html
imported:
- "2019"
_4images_image_id: "29812"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29812 -->
Hier ist die Platine zu sehen.
