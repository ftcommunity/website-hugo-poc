---
layout: "image"
title: "Versuchsaufbau zur Ermittlung der Positioniergenauigkeit"
date: "2011-01-29T21:53:06"
picture: "versuchsaufbau1.jpg"
weight: "7"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29818
- /detailsafdf.html
imported:
- "2019"
_4images_image_id: "29818"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-29T21:53:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29818 -->
Ich habe hier einen Versuchsaufbau gemacht um die Genauigkeit der Encodermotoren am TX und am Robo Interface in Verbindung mit meinem Inpulsewandler zu ermitteln.
Hier die Ergenisse:

1. Versuchsreihe:
Fahren vom Referenzpunkt ( Taster ) auf Endposition.
Weg: ca 50mm ( gefahren wurden 10 Wege )

TX mit Encoder
durchschnittliche Abweichung: 0,05mm
bester Wert: 0
schlechtester Wert: 0,13mm

Robo mit Impulswandler ( 0 und 1 Impulse ausgewertet )
durchschnittliche Abweichung: 0,15mm
bester Wert: 0,04
schlechtester Wert: 0,30mm

2. Versuchsreihe:
Wiederholgenauigkeit
Fahren vom Endpunkt um ca 25mm zurück und dann wieder auf Endposition ( gefahren wurden 10 Wege )

TX mit Encoder
durchschnittliche Abweichung: 0,05mm
bester Wert: 0
schlechtester Wert: 0,09

Robo mit Impulswandler ( 0 und 1 Impulse ausgewertet )
durchschnittliche Abweichung: 0,17mm
bester Wert: 0,06mm
schlechtester Wert: 0,35mm

Wie genau die Powermotoren in Verbindung mit dem Impulserad und Taster sind habe ich nicht mehr ermittelt, wäre sicherlich als Vergleich auch interessant gewesen.

Die Encodermotoren in Verbindung mit dem TX sind echt genau.

Die Werte von den Econdermotoren in Verbindung mit dem Impulswandler und dem Robo Interface finde ich auch schon sehr gut.

Was meint Ihr zu den ermittelten Werten?


