---
layout: "image"
title: "Gehäuse"
date: "2011-01-27T22:49:35"
picture: "impulsewandler3.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29814
- /details3ac6.html
imported:
- "2019"
_4images_image_id: "29814"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29814 -->
In das Batteriegehäuse habe ich 2 Stege eingeklebt, wo die Platine nachher aufliegt.
