---
layout: "image"
title: "Fertigbaustein"
date: "2011-01-27T22:49:35"
picture: "impulsewandler6.jpg"
weight: "6"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/29817
- /detailsacf4.html
imported:
- "2019"
_4images_image_id: "29817"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-27T22:49:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29817 -->
Hier ist der fertige Baustein zu sehen.

Dieser Impulsewandler diehnt dazu die neuen Encodermotoren 135484 an das Robo Inteface 93293 anzuschließen.
An den Baustein lassen sich 2 Encodermotoren anschließen.
Oben sind die jeweiligen Anschlüße zu sehen.
In der unteren Reihen ist dann die Spannungsversorgung sowie die Signalausgänge zu sehen.
Durch den Binärzähler wird nur jeder 16. Impuls an das Interface durchgelassen.
Somit kommt das Robo auch mit dem Encoder klar.

Der Klue ist das sich durch die Teilung der 75 Impulse durch 16 der Wert 4,69 ergibt.
Dadurch entspricht ein Impuls fast genau 1mm Wegstrecke bei Verwendung der 1,5 M Schnecke.
Dies wiederum erleichtert die Programmierung erheblich.
