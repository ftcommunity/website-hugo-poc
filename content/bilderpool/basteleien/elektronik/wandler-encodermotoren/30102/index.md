---
layout: "image"
title: "Impulsewandler Version 2"
date: "2011-02-23T21:00:31"
picture: "impulsewandler1_2.jpg"
weight: "10"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/30102
- /details0ff7.html
imported:
- "2019"
_4images_image_id: "30102"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-02-23T21:00:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30102 -->
Ich habe nun eine Kleinserie aufgelegt.
Das Design habe ich etwas verändert, ich finde der gelbe Deckel sieht schöner aus.
Es handelt sich um einen komplett fertig aufgebauten Baustein, also kein selber löten !!
Wer Interesse hat kann sich bei mir melden.

Noch mal zur Erklärung:
An den oberern Anschlüssen ( RT , SW , GR ) werden die Leitungen vom Encodermotor angeschlossen.
SG ist der Signalausgang der an die digitalen Eingänge des Robo Interface angeschlossen wird.
+ und - ist die Spannungsversorgung.

MFG

TST
