---
layout: "image"
title: "The circuit diagram"
date: "2009-01-23T08:29:18"
picture: "lcd_schema0002.jpg"
weight: "18"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/17137
- /details1364-2.html
imported:
- "2019"
_4images_image_id: "17137"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17137 -->
The LCD connects directly to the RI bus. The step-up converter is a standard application of the MC34063 which is also used in the Pollin demo kit. This kit costs €2,95 (Best. Nr. 120 487) and contains all components for the converter, the LCD connector and more. A separate connector costs €0,75 (Best. Nr. 450 975) and the LCD itself costs only €4,95 (Best.Nr. 120 345). This makes it the cheapest graphic LCD interface ever with a resolution of 240x64 pixels. All you need besides this is a piece of PCB and the two busconnectors to the RI. Total cost will be between 10 and 15 Euro.