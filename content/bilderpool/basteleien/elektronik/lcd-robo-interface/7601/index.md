---
layout: "image"
title: "Rückseite des LCD"
date: "2006-11-25T19:00:05"
picture: "robo-lcd11.jpg"
weight: "5"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["LCD", "RoboInt", "RoboInterface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7601
- /details62c8.html
imported:
- "2019"
_4images_image_id: "7601"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7601 -->
Auf der Rückseite sieht man die notwendigen elektronischen Bausteine: Zwei Latches (= Speicher) 74HCT574 (besser wäre der HC-Typ, war aber in meiner Bastelkiste nicht verfügbar), ein 6-fach Inverter 74HC04. Diese ICs werden benötigt, weil das LCD ein paar nanosekunden zu langsam ist. Mit den Latches werden Daten- und Adressbus gespeichert, mit dem 74HC04 und ein paar Kleinteilen wird das Signal für das LCD verlängert. Leider ist es durch diese Konstruktion bedingt nicht möglich, aus dem LCD zwecks Synchronisation auszulesen.
Die ICs sind alles Standardteile, es muss kein µC programmiert werden.
