---
layout: "comment"
hidden: true
title: "7675"
date: "2008-10-31T20:35:49"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
"RS232" beschreibt zunächst mal nur grob die Hardware. Das Protokoll (der Datenstrom) der dahinter steht, kommt von der Software. Und nein, es ist NICHT möglich, per RoboPro eines der Electronik-Assembly LCDs anzusteuern - trotz der schönen Doku.
RoboPro gibt grundsätzlich einen 7-Byte Frame aus, mit dem die handelsüblichen LCDs nichts anfangen können.
Unter http://www.ft-fanpage.de/roboint/RoboPro-RS232.html kannst Du Dir das RoboPro-Protokoll ansehen und selbst entscheiden, ob das EA-LCD das versteht...