---
layout: "comment"
hidden: true
title: "7655"
date: "2008-10-25T17:03:56"
uploadBy:
- "thkais"
license: "unknown"
imported:
- "2019"
---
Diese Komplettlösungen funktionieren nicht mit RoboPro, weil das ein anderes (nicht veränderbares) Protokoll verwendet, als die RS232-LCDs.
Alternative: Programmierung des Robo-Interface in Basic oder C.