---
layout: "image"
title: "LCD 132x32 (grafikfähig) extern"
date: "2008-01-26T14:42:01"
picture: "lcd132x32.jpg"
weight: "10"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["LCD"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/13433
- /details1d5d-2.html
imported:
- "2019"
_4images_image_id: "13433"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2008-01-26T14:42:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13433 -->
Eine etwas kleinere Variante eines Grafik-LCDs, das auch etwas bezahlbarer ist. Es handelt sich um die DOGM-Reihe von Electronic Assembly, die mittlerweile bei Reichelt bezogen werden kann. Das LCD kostet um die 15 Euro, zuzügl. einer optionalen Beleuchtung (etwa 4 Euro) und etwas Kleinkram drumherum. Das vorhergehende 128x64 kostet knapp 25 Euro (zuzgl. Beleuchtung)
