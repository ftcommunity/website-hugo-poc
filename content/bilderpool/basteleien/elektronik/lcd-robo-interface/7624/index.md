---
layout: "image"
title: "Externes LCD"
date: "2006-11-26T12:47:56"
picture: "robo-lcd12.jpg"
weight: "6"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7624
- /details93f7.html
imported:
- "2019"
_4images_image_id: "7624"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7624 -->
Dies ist ein grafikfähiges LCD, das per serieller Schnittstelle an das Robo-Interface angeschlossen werden kann.
