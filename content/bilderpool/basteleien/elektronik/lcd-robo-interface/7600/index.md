---
layout: "image"
title: "LCD fürs Robo-Interface"
date: "2006-11-25T19:00:05"
picture: "robo-lcd10.jpg"
weight: "4"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/7600
- /details10ca.html
imported:
- "2019"
_4images_image_id: "7600"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7600 -->
Ich habe auch ein Layout zum Display entwickelt, nun sieht das doch etwas aufgeräumter aus.
