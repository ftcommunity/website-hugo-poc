---
layout: "image"
title: "LCD showing some graphics"
date: "2009-01-23T08:29:17"
picture: "IMG_3027.jpg"
weight: "14"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- /php/details/17133
- /details4d3a.html
imported:
- "2019"
_4images_image_id: "17133"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17133 -->
