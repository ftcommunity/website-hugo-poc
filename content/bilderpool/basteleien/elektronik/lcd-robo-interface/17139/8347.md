---
layout: "comment"
hidden: true
title: "8347"
date: "2009-01-26T21:54:57"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
There are no buffers, the DC converter is the same as in the pollin demo circuit except that I didn't make the voltage adjustable. Here is the link:
http://www.pollin.de/shop/downloads/D120487B.PDF
The component values are not critical, R1 should be low enough and the ratio R3/R4 should be ok for 15V.
I'm trying to analyse what robopro does when it starts a program and which resources (memory, timers) it uses. But it's a lot of work, might just as well try it and pray for luck.