---
layout: "image"
title: "Leuchtstein mit TCST1103"
date: "2009-10-12T00:50:41"
picture: "IMG_2137.jpg"
weight: "5"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
schlagworte: ["TCST1103", "sensor", "leuchtstein"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- /php/details/25547
- /detailsb2d9.html
imported:
- "2019"
_4images_image_id: "25547"
_4images_cat_id: "1793"
_4images_user_id: "385"
_4images_image_date: "2009-10-12T00:50:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25547 -->
Leuchtstein (38216) mit TCST1103
