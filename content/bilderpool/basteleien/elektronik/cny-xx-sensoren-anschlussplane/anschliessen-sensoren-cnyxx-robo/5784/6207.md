---
layout: "comment"
hidden: true
title: "6207"
date: "2008-04-13T12:24:43"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
@Defiant: 9(Versorgungsspannung)-1,5(Diodenspannung)=7,5V
7.5*0,05A=0,375W
Also reicht ein 1/4W Widers6tand nicht mehr.