---
layout: "image"
title: "Gabellichtschranke"
date: "2008-07-26T16:23:18"
picture: "Gabellichtschranke1.jpg"
weight: "8"
konstrukteure: 
- "Nils (fitec)"
fotografen:
- "Nils (fitec)"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- /php/details/14960
- /details1709.html
imported:
- "2019"
_4images_image_id: "14960"
_4images_cat_id: "603"
_4images_user_id: "456"
_4images_image_date: "2008-07-26T16:23:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14960 -->
So sieht das dann in Echt aus. Statt Kabel hab ich Buchsen dran gemacht. Man schließt also +9V, GND und die Ausgabe für den I-Eingang an.
