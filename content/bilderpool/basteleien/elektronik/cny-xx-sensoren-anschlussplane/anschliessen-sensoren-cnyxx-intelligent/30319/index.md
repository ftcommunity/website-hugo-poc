---
layout: "image"
title: "Sensor"
date: "2011-03-26T21:21:04"
picture: "cnysensoren6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/30319
- /details13c8.html
imported:
- "2019"
_4images_image_id: "30319"
_4images_cat_id: "2255"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:21:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30319 -->
Andere Sensoren
Für die lineare Positionierung (z.B. bei Plottern) lassen sich mit dem CNY37 höhere Genauigkeiten erreichen als mit dem CNY70.
Bei der Erkennung von Objekten ist oftmals auch die Verwendung von "Hallsensoren", die auf Magnetfelder reagieren sinnvoll.