---
layout: "image"
title: "Polin Detektor eindimensional als Positions-Abstandssensor"
date: "2007-05-07T08:09:49"
picture: "Detektor_Beschreibung_Anschlu.jpg"
weight: "3"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- /php/details/10336
- /details6a6b.html
imported:
- "2019"
_4images_image_id: "10336"
_4images_cat_id: "941"
_4images_user_id: "426"
_4images_image_date: "2007-05-07T08:09:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10336 -->
Hier die Beschaltung wie ich ihn angeschlossen habe!