---
layout: "image"
title: "Montageband mit 3 Stationen - 1"
date: "2009-07-12T11:53:49"
picture: "tisch_001_resize.jpg"
weight: "67"
konstrukteure: 
- "Staudinger GmbH"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["Transportband", "Näherungssensoren", "Staudinger", "Industrie"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/24533
- /details4f80.html
imported:
- "2019"
_4images_image_id: "24533"
_4images_cat_id: "653"
_4images_user_id: "120"
_4images_image_date: "2009-07-12T11:53:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24533 -->
