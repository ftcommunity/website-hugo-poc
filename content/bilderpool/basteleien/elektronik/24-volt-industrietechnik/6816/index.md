---
layout: "image"
title: "15"
date: "2006-09-16T16:53:06"
picture: "101MSDCF_015.jpg"
weight: "19"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6816
- /details40ca.html
imported:
- "2019"
_4images_image_id: "6816"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6816 -->
