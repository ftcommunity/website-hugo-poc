---
layout: "image"
title: "11"
date: "2006-09-16T16:53:05"
picture: "101MSDCF_011.jpg"
weight: "15"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6812
- /detailsa28a.html
imported:
- "2019"
_4images_image_id: "6812"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6812 -->
