---
layout: "image"
title: "IF5"
date: "2006-09-13T22:41:22"
picture: "101MSDCF_002.jpg"
weight: "4"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6797
- /details978e.html
imported:
- "2019"
_4images_image_id: "6797"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-13T22:41:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6797 -->
