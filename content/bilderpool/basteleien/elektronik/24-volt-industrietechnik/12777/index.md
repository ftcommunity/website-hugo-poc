---
layout: "image"
title: "Bestückungsstation, ein Greifer nimmt die Werkstücke ab"
date: "2007-11-18T20:35:59"
picture: "HRL_014.jpg"
weight: "66"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12777
- /details569b.html
imported:
- "2019"
_4images_image_id: "12777"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2007-11-18T20:35:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12777 -->
Die Technik fehlt noch. Geplant war die Abnahme über pneumatische Edrücker, die das Werstück fassen. Ich bauer gerade an einer Zange. Das ganze wird auf den Ablageplatzrechts zur Aufnahme verfrachtet