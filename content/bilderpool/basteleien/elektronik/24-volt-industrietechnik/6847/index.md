---
layout: "image"
title: "24V4"
date: "2006-09-16T23:12:56"
picture: "24V_Industrieanlage_004.jpg"
weight: "50"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6847
- /details745b-2.html
imported:
- "2019"
_4images_image_id: "6847"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T23:12:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6847 -->
