---
layout: "image"
title: "jetzt wird es ein richtiges Werk"
date: "2006-10-22T18:45:58"
picture: "061022_ft_alu_002.jpg"
weight: "55"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7210
- /detailsadd6-2.html
imported:
- "2019"
_4images_image_id: "7210"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-10-22T18:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7210 -->
Jetzt bin ich schon 2-geschossig. Ich muß nur noch die Verbindung von unten nach oben einfügen ;-)