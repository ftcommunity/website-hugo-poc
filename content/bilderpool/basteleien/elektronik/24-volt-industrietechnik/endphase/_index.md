---
layout: "overview"
title: "In der Endphase"
date: 2020-02-22T07:43:29+01:00
legacy_id:
- /php/categories/2251
- /categories9be6.html
- /categoriesae8e.html
- /categories797c.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2251 --> 
Ich hab jetzt genug Material, um eine SIM-Fabrik (Simulations-Fabrik) nachzubauen. Man muß auch aufpassen, da man schnell an die Grenzen der Programmierung kommt. Zufällig wohnt ein Professor für Informatik im Nachbarhaus, der schon in den Startlöchern schabt. Ich versuche jetzt mit der Aufstellung eine gewisse Montage-, Sortier- und Ablagesystematik auszuprobieren. 

2 von den großen, doppelseitigen Hochregallagern (siehe Anfang der Bildreihe) werden je am Anfang und Ende stehen. Das erste ist das Rohmateriallager, das zweite wird das Endmateriallager zum auschecken. Je nach freier Steuerungskapzität kommt noch ein flächenübergreifender (ca 4qm) Portalkran über die Anlage.

Momentan wird die Anlage über 10qm groß werden. Mehr darf ich in der Wohnung nicht :(