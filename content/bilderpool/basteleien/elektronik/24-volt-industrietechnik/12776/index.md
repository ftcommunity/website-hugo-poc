---
layout: "image"
title: "Wagen mit Zuliefertisch fährt in das Lager ein"
date: "2007-11-18T20:35:59"
picture: "HRL_011.jpg"
weight: "65"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12776
- /details6a36.html
imported:
- "2019"
_4images_image_id: "12776"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2007-11-18T20:35:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12776 -->
