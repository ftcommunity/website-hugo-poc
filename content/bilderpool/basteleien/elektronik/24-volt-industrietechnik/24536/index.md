---
layout: "image"
title: "Trainingsrobter mit drehbarem Greifer"
date: "2009-07-12T11:53:49"
picture: "ft_resize.jpg"
weight: "70"
konstrukteure: 
- "unbekannt"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["Roboter", "Arm", "Gabellichtschranke", "Staudinger", "Industrie"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/24536
- /details6605-2.html
imported:
- "2019"
_4images_image_id: "24536"
_4images_cat_id: "653"
_4images_user_id: "120"
_4images_image_date: "2009-07-12T11:53:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24536 -->
vermutlich an TU Stuttgart gebaut, Steuerung der normalen 9V bzw 5V Technik über 24V Relais in zu den Staudinger Modellen kompatibler Beschaltung und Stecker
