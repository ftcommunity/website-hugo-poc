---
layout: "image"
title: "Zuliefertisch ohn Werkstück/Ablagetisch"
date: "2007-11-18T20:35:59"
picture: "HRL_008.jpg"
weight: "63"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/12774
- /details64a2.html
imported:
- "2019"
_4images_image_id: "12774"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2007-11-18T20:35:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12774 -->
