---
layout: "image"
title: "Infrarot"
date: "2006-10-16T20:44:34"
picture: "101MSDCF_001_2.jpg"
weight: "53"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7203
- /details3973.html
imported:
- "2019"
_4images_image_id: "7203"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-10-16T20:44:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7203 -->
32822 Infrarot strahler 409-2 kompl (auf Leuchtsteinunterteil grau)
32824 Infrarot-Fototransistor 309-4 ko. (")

222087 Reflexions-Lichttaster MHT1-P122 (von Sick, verbaut auf 15/15 Grundplatte; da ist der Anschluß 3 phasig +,- und Impuls mit Reflexionsplatte auf 15/30)
222088 Reflexions-Lichtschranke mit Polfilter PNP (")