---
layout: "image"
title: "Spannungsregler eingebaut"
date: "2013-04-19T17:54:03"
picture: "IMG_9840.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["Spannungsregler", "Laser", "Laserdiode"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/36851
- /details4294.html
imported:
- "2019"
_4images_image_id: "36851"
_4images_cat_id: "2736"
_4images_user_id: "1359"
_4images_image_date: "2013-04-19T17:54:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36851 -->
3V Spannungsregler für Laserdiodenmodul (und andere Zwecke) - die Platinen gibts als "DC DC Step Down" Wandler für unter 5€ in der "Bucht" - sind einstellbar mit Spindeltrimmer (Blaues Teil mit Schraube auf der Platine) und regeln die Spannung gemäß der Einstellung runter
80% Wirkungsgrad (also "Akkufreundlich"
