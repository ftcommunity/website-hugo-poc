---
layout: "image"
title: "Spannungsregler 01"
date: "2013-04-23T20:53:58"
picture: "voltanzeige1.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/36869
- /details13cd-2.html
imported:
- "2019"
_4images_image_id: "36869"
_4images_cat_id: "2736"
_4images_user_id: "453"
_4images_image_date: "2013-04-23T20:53:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36869 -->
Der Knopf aktiviert die Voltanzeige, zum leichteren einstellen.
http://www.ebay.de/itm/320952545753
http://www.ebay.de/itm/321070735247
