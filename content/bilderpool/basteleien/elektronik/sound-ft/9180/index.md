---
layout: "image"
title: "Lautsprecher"
date: "2007-03-01T16:13:13"
picture: "soundft06.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9180
- /details101f.html
imported:
- "2019"
_4images_image_id: "9180"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9180 -->
4.5V, eigentlich gut und gehört zum Soundmodul.