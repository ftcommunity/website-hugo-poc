---
layout: "image"
title: "Lautsprecher unten"
date: "2007-03-01T16:13:13"
picture: "soundft07.jpg"
weight: "7"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9181
- /details4516.html
imported:
- "2019"
_4images_image_id: "9181"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9181 -->
