---
layout: "image"
title: "Mikrophon"
date: "2007-03-01T16:13:13"
picture: "soundft04.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9178
- /detailsef87.html
imported:
- "2019"
_4images_image_id: "9178"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9178 -->
Ein Mikrophon das eigentlich gut ist, nimmt einfach keine tiefe Töne auf. Ich werde es auf einen Leuchtstein montieren.