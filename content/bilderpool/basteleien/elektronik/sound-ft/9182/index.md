---
layout: "image"
title: "Lautsprecher in Batteriekasten"
date: "2007-03-01T16:13:13"
picture: "soundft08.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/9182
- /details684f-2.html
imported:
- "2019"
_4images_image_id: "9182"
_4images_cat_id: "847"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:13:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9182 -->
Passt perfekt in den Kasten!