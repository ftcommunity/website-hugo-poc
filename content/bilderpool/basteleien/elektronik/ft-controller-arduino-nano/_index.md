---
layout: "overview"
title: "FT Controller mit Arduino Nano"
date: 2022-05-24T20:48:00+02:00
---

# Mini Controller auf Basis Arduino Nano #

## Gehäuse ##
  
Das Gehäuse besteht aus FT Bauteilen passend für eine Experimentierplatine mit den Maßen 80x50 mm.  
Die Platine sitzt gut fest und kann sehr einfach entnommen werden.

## Controller und Ein- / Ausgänge ##
  
Kernstück ist ein steckbarer Arduino Nano Clone mit Mini USB Buchse.  

* Leistungstreiber für bis zu zwei FT Motoren ist das steckbare IC L293D.  
* Zur Ansteuerung von bis zu drei FT LED Bausteinen dienen Transistoren BC639.  
* Es gibt bis zu vier analoge Eingänge, A0 bis A3, für Taster und Fototransistoren.  
* Ein Trimmpoti am Eingang A7 kann zu Konfigurationszwecken genutzt werden.  
* Eine Stiftleiste dient zum Anschluss eines I2C OLED Displays.

Ein- und Ausgänge sind über FT Stecker kompatible Buchsen zugänglich.  Die Buchsen sind aus dem Modellbahnbereich. Das Kunststoffgehäuse wurde entfernt. Ein 1,3 mm Messingdraht ist eingeschraubt und auf der Platine verlötet.

* Die Motoren werden an die Motorbuchsen M1 bzw. M2 angeschlossen.  
* Die Pluspole der LED-Bausteine werden direkt mit 9 V verbunden. Die Minuspole werden an die Anschlüsse L1 bis L3 gesteckt.  
* Die Eingänge werden gegen GND geschaltet.  
* Beim Taster wird der Anschluss in der Mitte mit GND verbunden. Schließer- oder Öffnerkontakt werden mit der Eingangsbuchse verbunden.  
* Beim Fototransistor wird der Minuspol mit GND verbunden und der Pluspol geht an eine Eingangsbuchse. Intern liegen dann 5 V am Fototransistor an.

## Software ##
  
Die Programmierung erfolgt mit der Arduino IDE. Ein Programmrahmen liefert Setup und Funktionen und kann für eigene Programme verwendet werden.  

Folgende Funktionen stehen zur Verfügung:  
  
int input(int port, int func)  
  port: A0 bis A3  
  func: SWITCH (Taster), PHOTO (Fototransistor)  
  Rückgabewert im Modus SWITCH: 1: Taster geschlossen, 0: Taster geöffnet  
  Rückgabewert im Modus PHOTO:  0: extrem hell, 1023: extrem dunkel  

void led(int lednum, int brightness)  
  lednum: led1, led2 oder led3, andere Werte werden ignoriert  
  brightness: 0 (aus) nis 255 (hell)  
  
void motor(int motornum, int speed, int direction)  
  motornum: MotorA (1) oder MotorB (2)  
  speed: 0 (aus) bis 255 (maximum)  
  direction: FORWARD (1), BACKWARD (2), STOP (3)  
  
Link auf github: <https://github.com/bastelklausi/bastelklausi/tree/FT-Mini-Controller>
                