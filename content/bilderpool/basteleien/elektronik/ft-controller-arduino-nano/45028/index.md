---
layout: "image"
title: "Gehäuse und Platine getrennt"
date: 2022-05-24T20:48:05+02:00
picture: "Minicon_getrennt.jpg"
weight: "3"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Elektronik"]
uploadBy: "Website-Team"
license: "unknown"
---

Gehäuse und Platine getrennt. Der Vorteil des Gehäuses besteht darin, dass es aus wenigen FT Bauteilen besteht und einfachst im Modell befestigt werden kann. Die Platine sitzt echt fest im Gehäuse und kann doch einfachst entnommen werden.