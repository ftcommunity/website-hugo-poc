---
layout: "image"
title: "Platine von der Lötseite"
date: 2022-05-24T20:48:02+02:00
picture: "Minicon_Platine_Loet.jpg"
weight: "5"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Elektronik"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Platine von der Lötseite. Beim Löten ist darauf zu achten, dass es keinen unerwünschten Kontakt mit den Nebenbahnen gibt.