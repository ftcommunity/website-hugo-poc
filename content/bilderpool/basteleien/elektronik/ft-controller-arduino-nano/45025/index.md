---
layout: "image"
title: "Schaltplan"
date: 2022-05-24T20:48:01+02:00
picture: "Minicon_Schaltplan.jpg"
weight: "6"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Fritzing"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Schaltplan wurde mit Fritzing erstellt.