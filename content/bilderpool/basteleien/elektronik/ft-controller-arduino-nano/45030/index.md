---
layout: "image"
title: "Minicontroller im Gehäuse von oben"
date: 2022-05-24T20:48:07+02:00
picture: "Minicon_von_oben.jpg"
weight: "1"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Elektronik"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Minicontroller im Gehäuse aus FT Bauteilen eingebaut (eingeschoben) in der Ansicht von oben. Das IC links ist der Motortreiber L293D, daneben der Arduino Nano. Rechts ist eine Verteilerplatte 3 x 2-polig von Gundermann. Die Stiftleiste rechts enthält die I2C Schnittstelle.