---
layout: "image"
title: "Platine von der Bestückungsseite"
date: 2022-05-24T20:48:03+02:00
picture: "Minicon_Platine_Bestueck.jpg"
weight: "4"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Elektronik"]
uploadBy: "Website-Team"
license: "unknown"
---

Die Platine von der Bestückungsseite. Die Abmaße der Platine ist 80 x 50 mm. Das IC L293D und der Nano sind auf Sockeln und können im Fall eines Defektes einfach ausgetauscht werden. Die Buchsen stammen aus dem Modellbahnbereich und sind der mit der Platine verlötet ist.