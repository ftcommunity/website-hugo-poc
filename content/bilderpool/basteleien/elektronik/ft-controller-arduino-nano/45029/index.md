---
layout: "image"
title: "Minicontroller mit geöffnetem Gehäuse"
date: 2022-05-24T20:48:06+02:00
picture: "Minicon_geoeffnet.jpg"
weight: "2"
konstrukteure: 
- "Klaus Janssen"
fotografen:
- "Klaus Janssen"
schlagworte: ["Controller", "Arduino", "Elektronik"]
uploadBy: "Website-Team"
license: "unknown"
---

Der Minicontroller im Gehäuse mit geöffneten Abdecksteinen in der Ansicht von schräg oben. Die Beschriftungen L1 bis L3 sind die Ausgänge für FT-LEDs. I1 bis I4 sind die Analogeingänge. GND und der 9V Eingang kommen von einem FT-Akku oder Batteriegehäuse mit 9 Volt. Der Trimmer liegt an A7 und kann zum Konfigurieren im Arduino Sketch verwendet werden. Die Motorausgänge M1 und M2 sind hier nicht sichtbar. 