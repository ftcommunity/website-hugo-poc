---
layout: "image"
title: "4 Pin Ventilator"
date: "2008-12-15T20:21:17"
picture: "Ventilator.jpg"
weight: "1"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
schlagworte: ["4-Pin", "Ventilator"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- /php/details/16620
- /detailsdc8c.html
imported:
- "2019"
_4images_image_id: "16620"
_4images_cat_id: "1505"
_4images_user_id: "820"
_4images_image_date: "2008-12-15T20:21:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16620 -->
Wie schließe ich einen 4Pin Ventilator an?
