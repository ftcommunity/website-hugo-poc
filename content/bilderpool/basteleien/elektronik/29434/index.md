---
layout: "image"
title: "Nachbau Blinkelektronik"
date: "2010-12-08T12:29:14"
picture: "Blinker2.jpg"
weight: "16"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "Thomas Habig (Triceratops)"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- /php/details/29434
- /details8bc8.html
imported:
- "2019"
_4images_image_id: "29434"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-12-08T12:29:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29434 -->
Mit zwei Blink-LEDs unterschiedlicher Farbe bleibt die Disharmonie der beiden Blinkfrequenzen gewährleistet. Die Platine ist mit einer Senkkopfschraube auf einer einfachen Verkleidungsplatte 15x30 fixiert und mit Heißkleber gesichert. Als Deckel habe ich hier eine zweite Platine gleicher Größe gewählt, mit der dann auch eine sonst notwendige Kabelbrücke realisiert ist.

Für den Betrieb mit LEDs an den Ausgängen darf man diese Schaltung aber nur mit 6 V betreiben: die Last an den Kollektoren der Transistoren ist dadurch verändert, was den Spannungsteiler an der Basis negativ beeinflußt. Alternativ muß für 9 V eine Z-Diode (3 V) in der Zuleitung eingefügt sein.
