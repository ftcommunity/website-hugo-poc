---
layout: "comment"
hidden: true
title: "15158"
date: "2011-09-23T02:36:59"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

PUT (programmierbarer Unijunctiontransistor?) - muß man sowas haben?

Na ja, egal. Material: 2 PNP-Transistoren (BC557), 2 Blink-LEDs (3 mm), 4 Mikrowiderstände (1 kOhm - Bauform 0204), 1 Entstörkondensator (100 nF), 4 Bundhülsen (11 mm) sowie Kabel, 2 FT-Stecker, Schaltdraht (für Leitungen unten drunter), 2 Platinen, 1 Senkkopfschraube nebst Mutter & Kunststoffunterlegscheibe (2 mm), selbstkl. Papierschild nebst selbstkl. Schutzfolie (zus. als Deckel), Heißkleber.

Ein ausführlicher Artikel für die ft:pedia ist geplant.

Gruß, Thomas