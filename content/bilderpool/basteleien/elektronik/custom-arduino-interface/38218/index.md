---
layout: "image"
title: "Close-up of the interface box"
date: "2014-02-10T22:32:19"
picture: "20140206_135254.jpg"
weight: "1"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
schlagworte: ["Arduino", "interface", "GUI", "user", "interface", "software"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38218
- /detailsa7c8.html
imported:
- "2019"
_4images_image_id: "38218"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38218 -->
I'm a big ft fan since my first two boxes (200S + 300) in 1971 (I was 11). I belong to a hacker generation that was used to build their own hardware and software. Imported parts are incredibly expensive in Brazil, so all my recent ft parts are second hand! Here is my finished working interface between an Arduino (actually an old Diecimila clone) and ft. More in the following pics!