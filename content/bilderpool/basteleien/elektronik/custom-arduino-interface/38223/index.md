---
layout: "image"
title: "Lid and upper PCB from below"
date: "2014-02-10T22:32:19"
picture: "20140201_192132.jpg"
weight: "6"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
schlagworte: ["PCB", "lib", "spaghetti", "wires"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38223
- /details7a39.html
imported:
- "2019"
_4images_image_id: "38223"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38223 -->
This image shows the lid and upper PCB as seen from below. More spaghetti.