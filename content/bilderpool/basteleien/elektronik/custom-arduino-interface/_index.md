---
layout: "overview"
title: "Custom Arduino interface"
date: 2020-02-22T07:44:13+01:00
legacy_id:
- /php/categories/2846
- /categories9ff7.html
- /categoriesb3d6.html
- /categoriese0a9.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2846 --> 
Homemade interface between Arduino and fischertechnik with custom hardware, low-level software and GUI (graphical user interface)