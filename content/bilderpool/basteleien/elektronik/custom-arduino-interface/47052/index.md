---
layout: "image"
title: "Side view with case removed"
date: "2018-01-07T14:27:05"
picture: "20160506_222701.jpg"
weight: "14"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/47052
- /details0b1f.html
imported:
- "2019"
_4images_image_id: "47052"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47052 -->
Side view of the three boards: The Arduino, the controller/connector board and the buzzer board.