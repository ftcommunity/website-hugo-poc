---
layout: "image"
title: "Main board"
date: "2014-02-10T22:32:19"
picture: "20140201_192043.jpg"
weight: "2"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
schlagworte: ["Arduino", "breadboard", "USB", "H-bridge"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38219
- /details1320.html
imported:
- "2019"
_4images_image_id: "38219"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38219 -->
This pic shows the main board. which is essentially an Arduino Diecimila soldered upon a regular breadboard. On the front you may see the four H-bridges used to drive the ft motors. The little green board is from some old hardware. It's just a holder for the mini-USB jack so I removed all the SMD components from it.