---
layout: "image"
title: "New assembly"
date: "2018-01-07T14:27:05"
picture: "20160507_104718.jpg"
weight: "10"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/47048
- /details1bca.html
imported:
- "2019"
_4images_image_id: "47048"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47048 -->
This is the new Arduino interface assembly. The enclosure is an ABS prototype case measuring 60 x 75 x 26 mm.