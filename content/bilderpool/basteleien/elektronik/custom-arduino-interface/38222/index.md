---
layout: "image"
title: "Upper PCB"
date: "2014-02-10T22:32:19"
picture: "20140201_192835.jpg"
weight: "5"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
schlagworte: ["PCB", "sockets"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- /php/details/38222
- /details081e.html
imported:
- "2019"
_4images_image_id: "38222"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38222 -->
This shows the upper PCB which is attached to the lid. Marklin sockets are very hard to find, so I ended up cutting several short segments of an old car or radio antenna!