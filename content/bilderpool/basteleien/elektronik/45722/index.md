---
layout: "image"
title: "fischertechnik Interface X2"
date: "2017-04-05T09:16:19"
picture: "ch1.jpg"
weight: "20"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- /php/details/45722
- /details00b2.html
imported:
- "2019"
_4images_image_id: "45722"
_4images_cat_id: "466"
_4images_user_id: "2374"
_4images_image_date: "2017-04-05T09:16:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45722 -->
Bild von der Platine Lötseite