---
layout: "image"
title: "02 geöffnet (5470)"
date: "2014-08-27T20:53:34"
picture: "02_geffnet_5470.jpg"
weight: "2"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- /php/details/39312
- /detailsd76f.html
imported:
- "2019"
_4images_image_id: "39312"
_4images_cat_id: "2943"
_4images_user_id: "2106"
_4images_image_date: "2014-08-27T20:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39312 -->
Hier sieht man das Ding geöffnet -- und warum es ein "quick and dirty"-Prototyp ist. Die Buchsen hatte ich einfach mit dem Dremel gebohrt und dann die Bundhülsen von FFM mit ordentlich Heißkleber festgemacht. Die bräunliche Farbe resultiert daraus, dass ich anschließend die Hülsen abgesägt hatte, was doch recht dreckig war. Die Pins vom Arduino Micro habe ich mit roher Gewalt in die Hülsen gebogen und festgelöstet. Wird beim nächsten Mal alles besser und hübscher.

Der Arduino Micro hat einen Micro-USB-Anschluss, über den man ihn wie jeden Arduino an den PC anschließen kann. Dann kann man programmieren und über ein serielles Interface testen. Der Arduino Micro kann so auch über USB mit Spannung versorgt werden, aber das ist für das Modell später natürlich nicht praktikabel -- dafür nimmt man dann die entsprechenden Buchsen GND und VIN (siehe Bild 05).