---
layout: "image"
title: "Nabenschlüssel Variante 2"
date: "2009-10-25T14:30:19"
picture: "SchlsselNEU.jpg"
weight: "13"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25573
- /details927b.html
imported:
- "2019"
_4images_image_id: "25573"
_4images_cat_id: "463"
_4images_user_id: "182"
_4images_image_date: "2009-10-25T14:30:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25573 -->
Hier die Version 2 meines Nabenschlüssel. Durch die zusätzlichen Nuten an der linken Seite läßt sich der Schlüssel wie eine Art Ratschenschlüssel verwenden. Er läßt sich je um 45° versetzen.
