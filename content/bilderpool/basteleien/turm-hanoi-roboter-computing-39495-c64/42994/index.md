---
layout: "image"
title: "Interface mit roter Ampel (1.Modell) aus dem Computing-Kasten"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter1.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/42994
- /detailsca2f.html
imported:
- "2019"
_4images_image_id: "42994"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42994 -->
als 1. Versuch zum "warmwerden" und zu testen, oab das Interface noch am c64 läuft ... da kommen einem schon fast die Tränen, wieviel HiTec man in den 80zigern schon am Start hatte :-)
