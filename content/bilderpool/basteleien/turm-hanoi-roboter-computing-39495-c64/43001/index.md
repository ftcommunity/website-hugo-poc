---
layout: "image"
title: "Detail der Scheiben & Magnet"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter8.jpg"
weight: "8"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43001
- /detailse106.html
imported:
- "2019"
_4images_image_id: "43001"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43001 -->
und löppt ... gut, etwa Krepp-Klebeband habe ich verwendet, weil doch gern die Scheiben nicht einzeln, sondern teilweise doppelt vom Magneten angezogen werden . :-)
