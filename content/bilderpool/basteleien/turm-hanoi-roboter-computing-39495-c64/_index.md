---
layout: "overview"
title: "Turm von Hanoi - Roboter Computing 39495 + c64"
date: 2020-02-22T07:46:51+01:00
legacy_id:
- /php/categories/3198
- /categories4262.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3198 --> 
Turm von Hanoi - Roboter Computing 39495 + c64 + Interface C64

Es hat uns mal wieder gepackt und wir haben den guten alten c64 angeschmissen und mal den Türme von Hanoi - Roboter aus dem Computing-Kasten gebaut ..
