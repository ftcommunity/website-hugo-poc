---
layout: "image"
title: "Stangenverbinder"
date: "2010-09-10T21:56:43"
picture: "stangenverbinder.jpg"
weight: "17"
konstrukteure: 
- "Lars Blome"
fotografen:
- "Lars Blome"
uploadBy: "lars"
license: "unknown"
legacy_id:
- /php/details/28081
- /detailsc548.html
imported:
- "2019"
_4images_image_id: "28081"
_4images_cat_id: "463"
_4images_user_id: "1177"
_4images_image_date: "2010-09-10T21:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28081 -->
hier hab ich 2  einflügelige radnaben aneinander geklebt damit man 2 stangen 
miteinander (fest) verbinden kann
