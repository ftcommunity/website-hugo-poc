---
layout: "image"
title: "ftDigiCam"
date: "2018-01-13T15:59:13"
picture: "ftDigiCam.jpg"
weight: "40"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Torsten Stuehn"
schlagworte: ["ftDigiCam", "Zahnrad", "Z30", "ftCameraZ30"]
uploadBy: "Torsten"
license: "unknown"
legacy_id:
- /php/details/47083
- /details6411.html
imported:
- "2019"
_4images_image_id: "47083"
_4images_cat_id: "463"
_4images_user_id: "2453"
_4images_image_date: "2018-01-13T15:59:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47083 -->
Das mit dem fischertechnik 3D-Drucker selbstgedruckte Zahnrad ftCameraZ30 im Einsatz mit der ftDigiCam (siehe ft:pedia 1/2016: https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-1.pdf)
