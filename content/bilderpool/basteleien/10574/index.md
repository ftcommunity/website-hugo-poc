---
layout: "image"
title: "Hubschrauber auf Tieflader"
date: "2007-05-31T09:43:07"
picture: "diverse6.jpg"
weight: "4"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Reiner Stüven"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- /php/details/10574
- /details6664.html
imported:
- "2019"
_4images_image_id: "10574"
_4images_cat_id: "463"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10574 -->
