---
layout: "image"
title: "Vierkantmitnehmer 2"
date: "2005-04-06T20:56:07"
picture: "Mitnehmer3.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/3959
- /details3b37.html
imported:
- "2019"
_4images_image_id: "3959"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2005-04-06T20:56:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3959 -->
Und so sieht die Sache montiert aus
