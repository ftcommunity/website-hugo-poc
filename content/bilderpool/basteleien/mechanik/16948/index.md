---
layout: "image"
title: "Mini Zahnstangen Hubgetriebe"
date: "2009-01-07T21:22:47"
picture: "Maxon1.jpg"
weight: "8"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16948
- /details30e1.html
imported:
- "2019"
_4images_image_id: "16948"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16948 -->
Ich habe hier ein sehr kleines Hubgetriebe entwickelt. Angetrieben wird es mit einem Maxon Getriebemotor mit einem Durchmesser von nur 10mm. Versehen mit einer Schnecke und einem Zahnrad Z14 fährt die Rundzahnstange mit einem Durchmesser von 4mm sehr kraftvoll ein und aus. Sehr geeignet für betätigungen anstatt Zylindern, oder für Lenkbewegungen oder ähnlichem.
