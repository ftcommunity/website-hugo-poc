---
layout: "image"
title: "Dubbele aandrijving door draaikrans"
date: "2012-03-28T19:29:30"
picture: "draaikrans_001.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34697
- /detailsbca9.html
imported:
- "2019"
_4images_image_id: "34697"
_4images_cat_id: "2562"
_4images_user_id: "838"
_4images_image_date: "2012-03-28T19:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34697 -->
Weet niet of het al bedacht is maar dit is mijn oplossing voor een dubbele aandrijving door een tandkrans