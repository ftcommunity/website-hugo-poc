---
layout: "image"
title: "Aandrijfing"
date: "2012-03-28T19:35:51"
picture: "draaikrans_008.jpg"
weight: "7"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- /php/details/34703
- /detailsec37.html
imported:
- "2019"
_4images_image_id: "34703"
_4images_cat_id: "2562"
_4images_user_id: "838"
_4images_image_date: "2012-03-28T19:35:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34703 -->
Iets aangepast om het klem systeem weg te nemen en het freewheelen mogelijk te maken, verder gewoon goed vast draaien