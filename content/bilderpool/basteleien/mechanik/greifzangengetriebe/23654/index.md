---
layout: "image"
title: "Greifzange Schwenkgelenk"
date: "2009-04-10T07:57:11"
picture: "kb07.jpg"
weight: "10"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23654
- /detailsc850.html
imported:
- "2019"
_4images_image_id: "23654"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23654 -->
Die Zahnstange im Aluprofil ersetzt das Parallelführungsgestänge aus Statikstreben und ermöglicht zusätzlich eine von ganz hinten angetriebene Schwenkachse.
Zwischen den Bausteinen 7,5 ist der Messingrohrbogen zu sehen, in dem der Seilzug läuft.