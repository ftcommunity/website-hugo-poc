---
layout: "image"
title: "Große Greifzange"
date: "2008-02-29T19:33:28"
picture: "Zangegromail.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
schlagworte: ["Greifzange", "TST", "Getriebe"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/13810
- /detailsc3ca.html
imported:
- "2019"
_4images_image_id: "13810"
_4images_cat_id: "1237"
_4images_user_id: "182"
_4images_image_date: "2008-02-29T19:33:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13810 -->
Hier mal eine große Greifzange mit dem von mir entwickelten Greifzangengetriebe. Die maßimale Greifbreite beträgt ca 100mm. Also hervorragent zum Stapeln der FT Kassetten geeignet.
