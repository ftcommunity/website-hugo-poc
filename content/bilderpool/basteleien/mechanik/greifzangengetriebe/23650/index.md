---
layout: "image"
title: "Greizange geschwenkt"
date: "2009-04-10T07:57:11"
picture: "kb03.jpg"
weight: "6"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23650
- /details16d1.html
imported:
- "2019"
_4images_image_id: "23650"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23650 -->
Hier sieht man, wie der Motor mitgeht.