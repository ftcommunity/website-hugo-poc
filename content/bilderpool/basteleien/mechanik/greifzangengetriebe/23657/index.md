---
layout: "image"
title: "Greifzange pneumatisch"
date: "2009-04-10T07:57:12"
picture: "kb10.jpg"
weight: "13"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23657
- /detailsd174.html
imported:
- "2019"
_4images_image_id: "23657"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:12"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23657 -->
Trotz Arbeitsdruck von 0,8 bar arbeitet sie so nicht 100% zuverlässig.