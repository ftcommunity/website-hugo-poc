---
layout: "comment"
hidden: true
title: "9019"
date: "2009-04-17T23:46:55"
uploadBy:
- "speedy68"
license: "unknown"
imported:
- "2019"
---
Hallo,
ich halte mich zwar auch bis auf wenige Ausnahmen an das „ft Reinheitsgebot“,  aber jeder hat ja das Recht, es mit Fremdteilen zu erweitern bzw. zu verbessern. Und wenn es dann noch so genial, wie in diesem Fall gemacht wird, gefällt es mir dann umso besser.
Gruß Thomas