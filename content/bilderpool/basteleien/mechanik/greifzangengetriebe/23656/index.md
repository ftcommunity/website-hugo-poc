---
layout: "image"
title: "Greifzange pneumatisch"
date: "2009-04-10T07:57:12"
picture: "kb09.jpg"
weight: "12"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23656
- /detailsc35d-2.html
imported:
- "2019"
_4images_image_id: "23656"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23656 -->
Ein leider nicht sehr befriedigender Versuch, diese Greifzangenkonstruktion mit einem obenliegenden Pneumatikzylinder anzutreiben.