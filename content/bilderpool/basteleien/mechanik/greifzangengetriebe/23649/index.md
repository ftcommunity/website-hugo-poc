---
layout: "image"
title: "Greifzange geschlossen"
date: "2009-04-10T07:57:11"
picture: "kb02.jpg"
weight: "5"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/23649
- /details99a3.html
imported:
- "2019"
_4images_image_id: "23649"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23649 -->
Die Backen sind mit jeweils einer der Zahnstangen fest verbunden.
Die Drehbewegung der Zange besorgt ein Minimotor, dieser sitzt fest auf der 4mm Hohlwelle, d.h. bei einer Schwenkbewegung geht er mit, und treibt durch diese eine Schnecke für die Drehbewegung an.
(Wäre der Motor am Arm befestigt würde eine Schwenkbewegung zu einer ungewollten Bewegung in der Drehachse führen.)