---
layout: "comment"
hidden: true
title: "4452"
date: "2007-10-28T22:56:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Beinahe...

Das Differenzialgehäuse ist nie richtig rund (Durchmesser zwischen 19,5 mm und 20 mm je nachdem, wo man misst), und zur Mitte hin ist es enger als am Ende, und erst recht, wenn eine Endkappe mit oder ohne Z20 darauf steckt. Um ein Teil draufzuschieben, muss man aber die Maße vom Ende berücksichtigen und hat im Ergebnis ordentlich viel Spiel in der Mechanik.
Zum Fixieren der großen Zahnräder ist Klemmen also nicht geeignet, da wird dann eben geklebt oder gebohrt.

Und richtig, die kleinen Zahnräder erhalten noch einen Schlitz für den Mitnehmer.

Gruß,
Harald

PS - Beim Z19 habe ich einen ordentlichen Schnitzer hingelegt (und auch noch fotografiert!). Die Fase sollte nur so ein klein bisschen beim Einfädeln aufs Diff-Gehäuse helfen, ist aber dann ordentlich tief geworden.