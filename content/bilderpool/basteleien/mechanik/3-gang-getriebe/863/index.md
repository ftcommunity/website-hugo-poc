---
layout: "image"
title: "3gang01"
date: "2003-04-27T17:15:39"
picture: "3gang01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Eigenbau", "Getriebe"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- /php/details/863
- /details861d.html
imported:
- "2019"
_4images_image_id: "863"
_4images_cat_id: "24"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=863 -->
Das Getriebe, wie es im Auto hier:
http://www.ftcommunity.de/details.php?image_id=851
sowie im Porsche:
http://www.ftcommunity.de/details.php?image_id=2048
eingebaut war. 
Das Differenzial direkt am Motor dient als Kupplung: wenn man den Achsstummel links abbremst, geht immer mehr Kraft auf das Getriebe zur rechten. Das andere Differenzial dient nur als Träger für die größeren Zahnräder und ist innen drin blockiert.
Durch Ziehen am Bildernagel (rechts) werden die drei Getriebegänge geschaltet. Wie das funktioniert, ist aus den Bildern 
http://www.ftcommunity.de/details.php?image_id=12367
http://www.ftcommunity.de/details.php?image_id=865
zu erkennen.
