---
layout: "comment"
hidden: true
title: "12662"
date: "2010-10-31T10:04:27"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Platte 180x90x2 in schwarz: 3,90 Euro
Platte 180x60x2 in schwarz: 3,50 Euro
Platte   90x90x2 in schwarz: 3,00 Euro
 
Bei Abnahme von 10 Stück einer Größe kann er 10 Prozent Rabatt geben.
Versandkosten nach Gewicht. Genaue Angaben erst nach Mengenangabe.
Zum Beispiel bis 2 Kg als Päckchen 9,50 Euro.
Einzelne Platten können eventuell noch als Brief verschickt werden.
Bitte selbst bei Harald Krafthöfer fragen für weitere Info und möglichkeiten.

Grüss,

Peter
Poederoyen NL