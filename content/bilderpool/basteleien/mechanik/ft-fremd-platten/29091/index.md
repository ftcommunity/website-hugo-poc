---
layout: "image"
title: "FT-Fremdplatten"
date: "2010-10-30T18:28:20"
picture: "FT-Platten-1.jpg"
weight: "1"
konstrukteure: 
- "Harald Krafthöfer"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/29091
- /detailse5b0.html
imported:
- "2019"
_4images_image_id: "29091"
_4images_cat_id: "2113"
_4images_user_id: "22"
_4images_image_date: "2010-10-30T18:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29091 -->
FT-Fremd-Platten gibt es bei :
Harald Krafthöfer
www.schmalspurgartenbahn.de
