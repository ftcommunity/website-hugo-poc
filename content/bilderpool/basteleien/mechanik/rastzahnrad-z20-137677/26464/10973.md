---
layout: "comment"
hidden: true
title: "10973"
date: "2010-02-19T20:50:12"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Jaaah genau! Danke! So hatte ich mir das vorgestellt. Damit kann man doch was anfangen. Fahrwerksbeine, Lukendeckel, Kurbeltriebe, Exzenter, und, und, und. 

Bei ähnlicher Gelegenheit habe ich die Ecken für die S-Riegel mit einem 1mm-Bohrer gebohrt (Winkelträger als Schablone auflegen, Achsstummel durch Träger und Objekt, dann in den Ecken bohren), dann die verbleibenden Stege mit einem scharfen Messer durchtrennt (hier z.B. http://www.ftcommunity.de/details.php?image_id=5804 ).

Schöner wär noch, wenn die zusätzlichen Bohrungen auch seitlichen Halt bieten würden, so wie die in der Mitte. Aber eine Hülse dranbohren geht halt nicht.

Gruß,
Harald