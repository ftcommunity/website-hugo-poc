---
layout: "image"
title: "Umbau Zahnrad 20"
date: "2010-02-18T17:54:21"
picture: "Pict_1633__Z20.jpg"
weight: "7"
konstrukteure: 
- "Michael Sengstschmid (Mirose)"
fotografen:
- "Michael Sengstschmid (Mirose)"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- /php/details/26464
- /details3e5a.html
imported:
- "2019"
_4images_image_id: "26464"
_4images_cat_id: "1878"
_4images_user_id: "765"
_4images_image_date: "2010-02-18T17:54:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26464 -->
Hallo Harald,
hier habe ich versucht, Deinen Vorschlag mit der Strebe zu verwirklichen.
Verwendet habe ich ein Kettenrad 31779 und zur Befestigung der Rastache eine Rastkupplung 35073.
