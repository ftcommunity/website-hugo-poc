---
layout: "comment"
hidden: true
title: "10962"
date: "2010-02-19T11:38:44"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Schnaggels,

die Strebe ist so weit im Raster, wie möglich, d.h. der Abstand vom Mittelpunkt Achse bis zum nächsten Mittelpunkt Strebenloch beträgt 7,5 mm.
Zur Herstellung: Löcher ankörnen, bohren, die Schlitze für die S-Riegel mit einer Schüsselfeile (3mm Quadrat) auffeilen (das Material ist sehr hartnäckig)

Viele Grüße

Mirose