---
layout: "image"
title: "neues Rast-Zahnrad Z20 rot (Art-Nr.137677)"
date: "2010-02-13T15:24:20"
picture: "neuesrastzahnradzrotartnr4.jpg"
weight: "4"
konstrukteure: 
- "ft"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/26363
- /details1112-2.html
imported:
- "2019"
_4images_image_id: "26363"
_4images_cat_id: "1878"
_4images_user_id: "409"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26363 -->
Anwendungsbeispiel, von der anderen Seite. Passt natürlich optimal ins Raste
