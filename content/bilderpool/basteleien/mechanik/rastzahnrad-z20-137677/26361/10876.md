---
layout: "comment"
hidden: true
title: "10876"
date: "2010-02-14T15:24:10"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich hatte da so an drei bis 6 Bohrungen reihum gedacht. Etwa, um einen Kurbeltrieb zu bauen, einen Schalter mittels Knubbel zu betätigen, oder eine S-Strebe flach drauf zu montieren und mitdrehen zu lassen. Ein Ausschnitt für einen S-Riegel wäre auch was feines gewesen.

Gruß,
Harald