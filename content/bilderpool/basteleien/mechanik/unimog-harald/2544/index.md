---
layout: "image"
title: "Unimog04.JPG"
date: "2004-07-13T13:26:02"
picture: "Unimog04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2544
- /details969b-2.html
imported:
- "2019"
_4images_image_id: "2544"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2544 -->
Das wird die linke Fahrzeugseite (Fahrtrichtung zum Betrachter hin, der Reifen sitzt falsch herum auf der Felge). Die Spurstangenbefestigung ist noch provisorisch, an der Geometrie wird noch gefeilt.