---
layout: "comment"
hidden: true
title: "20317"
date: "2015-03-08T08:55:09"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
"Das Z20 wird angetrieben ..." alles genau so, wie es ist.
Der Mitnehmerstift ist ein Messingrohr 2 mm, also hohl. Die Luft geht da durch.

Den Silikonschlauch kann man einklemmen und dann bleibt er schon sitzen. Außerdem sollte er irgendwann durch eine "richtige" Drehdurchführung ersetzt werden. Der Prototyp dazu ist just letzte Woche im Päckchen von der 3D-Druckerei drin gewesen, aber noch nicht zusammen gebaut.

Für die Kupplung war das eher nur ein Pilotprojekt. Der Witz kommt dann, wenn man z.B. einen Mähdrescher mit nur einem Motor (außer dem fürs Fahren) hat und die mechanischen Funktionen (Mähen, Fördern, Dreschen, Rütteln) einzeln zuschalten kann. 

Aber wie es immer ist: die Pläne eilen den zeitlichen Möglichkeiten weit voraus. Oder sie werden von anderen überholt, wenn sie nicht richtig vorwärts kommen.

Gruß,
Harald