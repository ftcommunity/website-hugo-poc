---
layout: "image"
title: "Unimog02.JPG"
date: "2004-07-13T13:26:02"
picture: "Unimog02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2542
- /detailsf278.html
imported:
- "2019"
_4images_image_id: "2542"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2542 -->
Die Vorderachse grob zusammengesteckt als 'Artist's Impression'.