---
layout: "image"
title: "Unimog30.JPG"
date: "2004-11-15T19:46:44"
picture: "Unimog30.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/3177
- /details975b.html
imported:
- "2019"
_4images_image_id: "3177"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T19:46:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3177 -->
Hier ist mal grob skizziert, wie das gute Stück einmal aussehen könnte.
