---
layout: "image"
title: "Unimog03.JPG"
date: "2004-07-13T13:26:02"
picture: "Unimog03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad", "Unimog", "modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2543
- /details8323.html
imported:
- "2019"
_4images_image_id: "2543"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2543 -->
Das soll die rechte Fahrzeugseite werden (der Reifen sitzt mit dem Profil falsch herum). Wenn der Achsschenkel bündig in der Felge sitzt, kämmt das Z7 im Innen-Z20
