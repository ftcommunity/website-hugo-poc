---
layout: "image"
title: "Antrieb"
date: "2006-11-12T18:26:46"
picture: "DSCN1123.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/7448
- /detailsde36.html
imported:
- "2019"
_4images_image_id: "7448"
_4images_cat_id: "702"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7448 -->
der Vorderachse der ersten Einheit
