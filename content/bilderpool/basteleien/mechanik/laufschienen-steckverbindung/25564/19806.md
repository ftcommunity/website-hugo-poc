---
layout: "comment"
hidden: true
title: "19806"
date: "2014-12-18T17:22:39"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
p.s. hat schon jemand mal versucht, eine Stromschiene und entsprechende Strom-Abnehmer für so eine Schienenbahn zu basteln?
Ich könnte die Fahrzeuge mit Batterien fahren lassen. Aber realitätsnah wäre eine Versorgung über die Schiene