---
layout: "image"
title: "Plastik04.JPG"
date: "2006-02-03T21:48:05"
picture: "Plastik04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5737
- /details9f92-2.html
imported:
- "2019"
_4images_image_id: "5737"
_4images_cat_id: "491"
_4images_user_id: "4"
_4images_image_date: "2006-02-03T21:48:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5737 -->
Im kalten Zustand kann man die Fäden noch weiter auseinander ziehen (wenn sie nicht allzu stark sind). Ich erkläre mir den Vorgang so, dass sich die Moleküle ab einer gewissen Zugkraft aus einer Reißverschlussverbindung lösen und nach und nach alle anderen reißen. Das Ergebnis ist ein dicker Faden: die Verdickung in Bildmitte ist der Rest der ursprünglichen Wurst, die (unter ziemlich hohem Kraftaufwand!) zu dem Brezel-förmigen Faden gezogen wurde. An diesen Faden kann man jedes ft-Modell dranhängen.

Wenn man einen dünnen Faden noch weiter zieht, gibt es natürlich Bruch (unten). Da musste ich aber ordentlich dran ziehen.
