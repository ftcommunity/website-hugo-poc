---
layout: "comment"
hidden: true
title: "11243"
date: "2010-03-21T20:05:40"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Es gibt eine Bezugsquelle in Deutschland!
Andreas Räde (Webmaster) hat mir (schon vor zwei Wochen -- soviel zu meinem eMail-Backlog) seine Webadresse per Mail zugeschickt:

http://www.plaast.de

und ich denke, er wird auch noch einen ftcommunity-Account zu Stande bringen.

Gruß,
Harald