---
layout: "image"
title: "Plastik02.JPG"
date: "2006-02-03T21:34:25"
picture: "Plastik02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5735
- /details87e1.html
imported:
- "2019"
_4images_image_id: "5735"
_4images_cat_id: "491"
_4images_user_id: "4"
_4images_image_date: "2006-02-03T21:34:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5735 -->
Vom linken Teil habe ich mit dem Taschenmesser zwei Schnipsel abgeschnitten, fast so wie von gut gereiftem Hartkäse, der aber noch nicht bröselig ist.

Oben sind mit der kleinen Proxxon-Maschine Löcher gebohrt worden. Die Maschine läuft zu schnell und wenn man nicht zügig arbeitet, heizt die Reibung das Material wieder auf die Schmelztemperatur auf.

Das mittlere Teil habe ich in die Drehmaschine gespannt und angefangen, es rund zu drehen. Das Material gibt aber keine gescheiten Späne, und die Schnittflächen sind eher "gerupft" als geschnitten. Die Aufbauschneiden (so würde man es bei Messing oder Kupfer nennen) verhindern präzises Arbeiten und verklemmen irgendwann das Teil am Drehstahl.

Rechts oben habe ich mit einer groben Feile (linke Kante) und einer feinen gearbeitet (rechte Kante) - das geht, nur sollte man die Feile vorher gut säubern, damit man die Brösel wiederverwertbar auffangen kann.

Schlecht zu sehen ist im rechten Teil der Abdruck, den ein aufgeheizter Teelöffel hinterlassen hat. Der Gedanke war, mit einem heißen Werkzeug ein Muster einzuritzen. Also, mit Löffeln geht es nicht so richtig. Dann doch lieber einen Lötkolben mit einem längeren Rohr (zwecks Temperatur-Reduzierung) verwenden.
