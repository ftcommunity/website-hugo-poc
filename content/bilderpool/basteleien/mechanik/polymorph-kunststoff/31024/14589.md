---
layout: "comment"
hidden: true
title: "14589"
date: "2011-07-12T07:58:15"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Tolle Idee und Umsetzung! Bitte weiter experimentieren - ich sehe viel Potenzial!

Kann es sein, dass die Gefahr groß ist, dass man eine Form baut, die nach dem Erkalten des Kunststoffs nicht mehr zu öffnen ist? Da muss man gutb aufpassen, oder?

Können es die fertigen Bausteine in Sachen Festigkeit mit echten FT-Teilen aufnehmen?

Gruß, Thomas