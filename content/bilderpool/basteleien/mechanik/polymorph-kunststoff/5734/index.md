---
layout: "image"
title: "Plastik01.JPG"
date: "2006-02-03T21:26:44"
picture: "Plastik01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5734
- /details7e61.html
imported:
- "2019"
_4images_image_id: "5734"
_4images_cat_id: "491"
_4images_user_id: "4"
_4images_image_date: "2006-02-03T21:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5734 -->
Das Material aus einer 35-Gramm-Tüte "Freeplastic", schon geschmolzen und zu Würsten geformt. Die "Warzen" auf dem rechten Teil kommen daher, dass das Granulat noch nicht richtig weich geworden war, als ich den Klumpen aus dem Wasser geholt habe.

Diskussion im alten ft-Forum (mittlerweile nur Lesen möglich):
http://www.fischertechnik.de/fanclub/forum/topic.asp?TOPIC_ID=3603&FORUM_ID=13&CAT_ID=5&Topic_Title=Polymorph%2DKunststoff&Forum_Title=Technik

Fortsetzung im neuen Forum:
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=400
