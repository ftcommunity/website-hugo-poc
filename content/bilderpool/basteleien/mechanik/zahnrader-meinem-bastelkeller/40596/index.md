---
layout: "image"
title: "Z 24"
date: "2015-02-22T21:47:09"
picture: "zahnraederausmeinembastelkeller3.jpg"
weight: "3"
konstrukteure: 
- "charly"
fotografen:
- "charly"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/40596
- /detailsd4f2.html
imported:
- "2019"
_4images_image_id: "40596"
_4images_cat_id: "3045"
_4images_user_id: "115"
_4images_image_date: "2015-02-22T21:47:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40596 -->
Das Z 24 ergibt kombiniert mit Schnecke und Encodermotor einen Antrieb für genaue Winkelpositionierung.  Die 75 Impulse ergeben mit 24 multipliziert 1800 Impulse für eine Umdrehung/360 Grad. So ergeben 5 Impulse eine Verstellung von einem Grad.