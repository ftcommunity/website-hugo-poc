---
layout: "overview"
title: "Zahnräder aus meinem Bastelkeller"
date: 2020-02-22T07:43:09+01:00
legacy_id:
- /php/categories/3045
- /categoriesa875.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3045 --> 
Seit Weihnachten bastle ich in meinem Hobbykeller an Vorrichtungen für selbstgefräste Zahnräder für mein liebstes Baukastensystem, hier zeige ich Euch meine ersten Ergebnisse.
 