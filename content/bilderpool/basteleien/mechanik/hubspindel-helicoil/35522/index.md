---
layout: "image"
title: "helicoil6.jpg"
date: "2012-09-16T20:26:41"
picture: "IMG_7997.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35522
- /details3ba3.html
imported:
- "2019"
_4images_image_id: "35522"
_4images_cat_id: "2634"
_4images_user_id: "4"
_4images_image_date: "2012-09-16T20:26:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35522 -->
Hier sind noch zwei Varianten angedeutet; eine in Funktion und die andere nur für die Fantasie. Der seltenen P-Kurbel wollte ich jetzt doch nicht das Z15 von seinem Rändelsitz herunterklopfen. Die andere mit dem Z10 eignet sich für die Lenkung von irgendwas großem.
