---
layout: "comment"
hidden: true
title: "17255"
date: "2012-09-16T20:31:27"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
es ist zweiteres: die Gewindestange darf sich nicht drehen, sondern soll mit Schmackes vor und zurück fahren. Sollte sie sich im Gewinde des(der?) helicoil verklemmen, gibt es irgendwo Bruch.

Gruß,
Harald