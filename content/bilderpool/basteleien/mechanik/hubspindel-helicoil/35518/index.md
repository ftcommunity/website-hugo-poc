---
layout: "image"
title: "helicoil1.jpg"
date: "2012-09-15T13:43:33"
picture: "IMG_7753.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35518
- /details894a.html
imported:
- "2019"
_4images_image_id: "35518"
_4images_cat_id: "2634"
_4images_user_id: "4"
_4images_image_date: "2012-09-15T13:43:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35518 -->
Wer am Auto eine Zylinderkopfschraube abgebrochen hat, kriegt das mit einer 'helicoil' wieder repariert: der verbliebene Gewindebolzen wird herausgebohrt, dann ein größeres Gewinde hineingeschnitten und dann eine Helicoil eingesetzt, die den Übergang vom großen Innengewinde aufs kleine (originalgroße) Innengewinde herstellt.
Helicoils gibt es auch in kleiner, und das hier ist ein Satz für 4 mm Innengewinde. Der Werkzeugsatz enthält einen Bohrer 4,2 mm, eine Reibahle 4 mm (fehlt im Bild), ein Gewindeschneideisen M4x0,7 und zwei Werkzeuge zum Eindrehen der Gewindemuffe und zum Reinigen des fertigen Gewindes. 
Das ganze geht so: 
1. Loch bohren, Gewinde reinschneiden
2. eine Gewindemuffe (helicoil) mittels Werkzeug eindrehen. Die helicoil ist letztlich nur ein Stück aufgewickelter Draht, aber eben in passenden Maßen. Ein Ende des Drahts ist nach innen hineingebogen. Da hinein greift ein Querschlitz am Eindrehwerkzeug, und damit nimmt man die ganze Muffe mit und dreht sie ins Innengewinde hinein.
3. Wenn man fertig ist, bricht man das eben noch zum Eindrehen benötige Drahtende mittels Linksdrehung ab.
4. Mit dem zweiten Stift entfernt man das Drahtende. Das baumelt meist noch so ein bisschen da drin herum und muss eben herausgefummelt werden.
5. Fertig. Wozu die Reibahle gut ist...?
