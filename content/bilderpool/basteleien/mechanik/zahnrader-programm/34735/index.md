---
layout: "image"
title: "Innenzahnrad Z40 und Zahnrad Z25"
date: "2012-04-01T11:29:33"
picture: "Zahnrad.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann (geometer)"
fotografen:
- "Thomas Püttmann (geometer)"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- /php/details/34735
- /details7215.html
imported:
- "2019"
_4images_image_id: "34735"
_4images_cat_id: "2566"
_4images_user_id: "1088"
_4images_image_date: "2012-04-01T11:29:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34735 -->
Beispiele für ft-kompatible Zahnradvorlagen, die sich mit meinem Postscript-Programm Zahnrad.eps zeichnen lassen. Dieses Programm findet sich im Downloadbereich der ft:c unter Software. Einzelheiten finden sich in der ft:pedia 1/2012.
