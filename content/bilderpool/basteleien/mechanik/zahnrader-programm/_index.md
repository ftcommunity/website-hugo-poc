---
layout: "overview"
title: "Zahnräder-Programm"
date: 2020-02-22T07:42:46+01:00
legacy_id:
- /php/categories/2566
- /categories26f4.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2566 --> 
Beispiele für Zahnradvorlagen, die mit dem Postscript-Programm angefertigt wurden, das sich in der ft:c unter Downloads/Software befindet.