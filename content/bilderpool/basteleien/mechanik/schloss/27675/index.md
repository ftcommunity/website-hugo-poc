---
layout: "image"
title: "Schloss 2"
date: "2010-07-05T16:39:57"
picture: "schloss2.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- /php/details/27675
- /detailsdbd7.html
imported:
- "2019"
_4images_image_id: "27675"
_4images_cat_id: "1991"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27675 -->
Auf der einen Seite habe ich ein FT-Teil angeklabt, damit man es besser in Modelle einbauen kann.