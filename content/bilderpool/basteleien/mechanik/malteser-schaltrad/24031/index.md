---
layout: "image"
title: "Malteser Schaltrad 2"
date: "2009-05-17T17:56:42"
picture: "Bild2.jpg"
weight: "12"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/24031
- /detailsb551-2.html
imported:
- "2019"
_4images_image_id: "24031"
_4images_cat_id: "542"
_4images_user_id: "182"
_4images_image_date: "2009-05-17T17:56:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24031 -->
Hier die 2. Ansicht
