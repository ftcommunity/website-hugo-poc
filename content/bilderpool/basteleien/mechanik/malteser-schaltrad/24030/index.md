---
layout: "image"
title: "Malteser Schaltrad 1"
date: "2009-05-17T17:56:42"
picture: "Bild1.jpg"
weight: "11"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/24030
- /details8432.html
imported:
- "2019"
_4images_image_id: "24030"
_4images_cat_id: "542"
_4images_user_id: "182"
_4images_image_date: "2009-05-17T17:56:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24030 -->
So ich habe meinen Prototypen nun verbessert. Jetzt schaltet es einwandfrei.
