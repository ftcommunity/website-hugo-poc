---
layout: "image"
title: "8x8-131.jpg"
date: "2004-02-20T12:22:44"
picture: "8x8-131.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/2137
- /detailsfdee.html
imported:
- "2019"
_4images_image_id: "2137"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2137 -->
Digitalkameras sehen aber auch jedes Staubkörnchen...
