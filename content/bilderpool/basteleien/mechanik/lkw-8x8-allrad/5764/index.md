---
layout: "image"
title: "8x8-120.jpg"
date: "2006-02-13T17:47:09"
picture: "8x8-120.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5764
- /details4366.html
imported:
- "2019"
_4images_image_id: "5764"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T17:47:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5764 -->
Die Innereien der Lenkung. Alles ist aus 2 mm starken Plastik-Platten ausgesägt und dann nachgeschnitten und gebohrt worden. Die Löcher für die S-Riegel erfordern 5 Bohrungen (4x 1 mm, 1x 4 mm) und etwas Schnitzerei.
