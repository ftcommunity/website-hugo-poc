---
layout: "image"
title: "Snow03.jpg"
date: "2006-02-12T15:55:04"
picture: "Snow03.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5761
- /details864f.html
imported:
- "2019"
_4images_image_id: "5761"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-12T15:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5761 -->
Die Rundumleuchte wird vom selben Motor angetrieben, der auch die Förderschnecke antreibt. Der "Auspuff" war früher mal eine Filmdose.
