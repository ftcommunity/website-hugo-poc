---
layout: "image"
title: "8x8-201.JPG"
date: "2006-03-04T16:02:43"
picture: "8x8-201.JPG"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5803
- /detailsbcb9.html
imported:
- "2019"
_4images_image_id: "5803"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-03-04T16:02:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5803 -->
Dieses und das nachfolgende Bild 202 ergeben ein hübsches "Daumenkino" (im Bildbetrachter vor- und zurück blättern!)
