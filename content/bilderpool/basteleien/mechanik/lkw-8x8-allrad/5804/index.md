---
layout: "image"
title: "8x8-202.JPG"
date: "2006-03-04T16:03:24"
picture: "8x8-202.JPG"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5804
- /details561f.html
imported:
- "2019"
_4images_image_id: "5804"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-03-04T16:03:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5804 -->
Dieses und das vorherige Bild 201 ergeben ein hübsches Daumenkino (im Bildbetrachter vor- und zurückblättern!)
