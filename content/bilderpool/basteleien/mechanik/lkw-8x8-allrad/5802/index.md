---
layout: "image"
title: "8x8-200.JPG"
date: "2006-03-04T16:01:23"
picture: "8x8-200.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5802
- /details51e4.html
imported:
- "2019"
_4images_image_id: "5802"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-03-04T16:01:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5802 -->
Mit Laubsäge, Bohrmaschine, Feile und Sekundenkleber kann man aus Plastikplatten und Messingrohren auch Teile für eine Lenkung fertigen.

Das Schmierfett zieht natürlich Staub an, deswegen sieht das hier recht schmutzig aus.
