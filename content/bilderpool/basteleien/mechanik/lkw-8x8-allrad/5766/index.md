---
layout: "image"
title: "8x8-123.jpg"
date: "2006-02-13T17:52:34"
picture: "8x8-123.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Allrad", "Eigenbau"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5766
- /detailse684.html
imported:
- "2019"
_4images_image_id: "5766"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T17:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5766 -->
"Vorne" ist hier rechts. Die beiden violetten Stecker versorgen den S-Motor für den P-Kompressor. Der P-Zylinder oben kippt das Führerhaus.

Die Radaufnahmen sind aus zwei schwarzen Kunststoff-L-Profilen zusammengeklebt, darauf kommt noch eine weiße Platte mit etwas größerer Bohrung, in die der Wulst der ft-Felge hineinpasst.
