---
layout: "image"
title: "Snow01.jpg"
date: "2006-02-12T15:50:02"
picture: "Snow01.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5759
- /detailsc03c.html
imported:
- "2019"
_4images_image_id: "5759"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-12T15:50:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5759 -->
Der 8x8 mit Schneefräse und Streuvorrichtung. Der Antriebsmotor für die Schneefräse sitzt links in Fahrzeugmitte, unter der gelben Abdeckung. Von da aus führt eine Kardanwelle unter dem Führerhaus hindurch nach vorn. Die Kardanwellen sind zugleich die unteren Haltevorrichtungen für die Fräse.
