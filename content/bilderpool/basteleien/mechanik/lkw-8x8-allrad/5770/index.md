---
layout: "image"
title: "8x8-141.jpg"
date: "2006-02-13T18:07:41"
picture: "8x8-141.jpg"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5770
- /detailsd5b6.html
imported:
- "2019"
_4images_image_id: "5770"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T18:07:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5770 -->
Das Führerhaus ist hier hochgeklappt.

Die Riemenscheibe auf dem Kompressorantrieb ist eine Propellernabe, minus Spitze.

Vorne mittendrin befindet sich die Blinkelektronik, ebenfalls ein Eigenbau.
