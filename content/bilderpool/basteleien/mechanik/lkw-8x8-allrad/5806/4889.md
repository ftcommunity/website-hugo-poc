---
layout: "comment"
hidden: true
title: "4889"
date: "2008-01-02T17:31:06"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die Kardangelenke kann man auf ft-Achsen aufkleben, aber ich verstifte sie lieber: Loch mit 1 mm quer durchs Ganze bohren1, und dann ein Drähtchen hindurchstecken. Man sieht es auch am schwarzen Differenzial: da habe ich den Achsstummel abgesägt und dann quer durchgebohrt.

Verkaufen... im Moment schlecht. Bis Mitte Februar muss noch ein dicker Flieger fertig (naja, sagen wir mal lieber "vorzeigbar") werden, der geht erstmal allem anderen vor. Danach gerne, ansonsten kann ich auch eine Bauanleitung posten.

Gruß,
Harald