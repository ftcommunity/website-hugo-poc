---
layout: "image"
title: "Kardanwellen"
date: "2005-03-05T11:26:35"
picture: "Kardanwellen.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/3708
- /details0f2c.html
imported:
- "2019"
_4images_image_id: "3708"
_4images_cat_id: "644"
_4images_user_id: "182"
_4images_image_date: "2005-03-05T11:26:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3708 -->
Nachbau der Kunstoffkardanwellen aus Messing
