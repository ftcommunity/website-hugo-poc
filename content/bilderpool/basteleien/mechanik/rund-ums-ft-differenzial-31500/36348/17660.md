---
layout: "comment"
hidden: true
title: "17660"
date: "2012-12-26T21:12:06"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
*uuups*
Da ist beim Freischalten etwas schief gegangen. Ich habe das Bild eine Ebene höher geschoben (ich sehe keine Notwendigkeit, 1 Ordner für 1 Bild anzulegen), und nun meldet der Server "Bilddatei nicht vorhanden".