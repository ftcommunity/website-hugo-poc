---
layout: "image"
title: "Kardanadapter"
date: "2005-03-05T11:26:35"
picture: "AdapterKardan.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/3706
- /details5a3b.html
imported:
- "2019"
_4images_image_id: "3706"
_4images_cat_id: "644"
_4images_user_id: "182"
_4images_image_date: "2005-03-05T11:26:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3706 -->
Adapter für das Differential 31500.Einmal in 4mm für die Standartachsen und in 5mm für den Power Motor.
