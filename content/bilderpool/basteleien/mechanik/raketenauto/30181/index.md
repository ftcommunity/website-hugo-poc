---
layout: "image"
title: "Raketenauto"
date: "2011-03-03T15:41:32"
picture: "auto1.jpg"
weight: "1"
konstrukteure: 
- "Marcel Endlich (Endlich)"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/30181
- /details3b90.html
imported:
- "2019"
_4images_image_id: "30181"
_4images_cat_id: "2241"
_4images_user_id: "1162"
_4images_image_date: "2011-03-03T15:41:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30181 -->
So, hier mein Raketenauto, das ich in der Schule gebaut habe und fischertechnik Räder und Menschen benutzt habe. 
Hier noch ein Video: http://www.youtube.com/watch?v=d5GfoAoFtIw  das erste Auto ist meins.
