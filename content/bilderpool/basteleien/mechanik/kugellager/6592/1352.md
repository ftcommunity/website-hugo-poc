---
layout: "comment"
hidden: true
title: "1352"
date: "2006-09-20T21:58:29"
uploadBy:
- "hufi"
license: "unknown"
imported:
- "2019"
---
Wenn du beim Baumarkt 4mm Messing Stangenware kaufst, und zu den gewünschten Achsenlängen verarbeitest, passen die perfekt in die 4mm Lager.
lg hufi