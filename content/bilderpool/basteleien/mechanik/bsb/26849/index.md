---
layout: "image"
title: "EMMA2"
date: "2010-03-29T21:56:05"
picture: "bsb2.jpg"
weight: "1"
konstrukteure: 
- "Mike"
fotografen:
- "Mike"
uploadBy: "mike"
license: "unknown"
legacy_id:
- /php/details/26849
- /detailsd7a8.html
imported:
- "2019"
_4images_image_id: "26849"
_4images_cat_id: "1921"
_4images_user_id: "1051"
_4images_image_date: "2010-03-29T21:56:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26849 -->
erste Ausführung mit Original
Fühl mich schon als was besonderes