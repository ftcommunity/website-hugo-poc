---
layout: "image"
title: "Drehstabfederung (2)"
date: "2015-03-07T18:08:19"
picture: "bastel4.jpg"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40621
- /detailsbb40.html
imported:
- "2019"
_4images_image_id: "40621"
_4images_cat_id: "465"
_4images_user_id: "4"
_4images_image_date: "2015-03-07T18:08:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40621 -->
Die Plastikrohre im vorher gehenden Entwurf federn nicht so gut. Deshalb folgte ein zweiter Anlauf mit Silikonschläuchen als federndem Element. Die Schläuche werden auf Torsion beansprucht und würden sich sofort zu Schraubenlinien aufwinden. Das wird verhindert, indem sie lose auf 2mm starken Messingröhrchen aufgeschoben sind. 

Am Steg in Fahrzeugmitte werden die Schläuche mittels Kabelbindern starr auf die schwarzen Röhrchen geklemmt. Diese sind in den Längsträgern drehbar und können in Schritten von 90° festgesetzt werden. Dazu dienen die kleinen Hebel zwischen den Längsträgern (Nr 1 ist überkopf eingebaut, Nr 2 geschlossen, Nr 3 und Nr 4 geöffnet). Außen werden die Schläuche, wieder mit Kabelbindern, auf die Tragarme geklemmt.

Die Federung funktioniert prima. Insgesamt ist es aber ein Fehlversuch, denn die Schwachstelle sind wieder die Tragarme (hier komplett aus Plastik, geholfen hätte nur eine Ganzmetall-Ausführung).
