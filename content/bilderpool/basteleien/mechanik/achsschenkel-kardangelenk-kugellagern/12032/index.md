---
layout: "image"
title: "Ansicht"
date: "2007-09-28T16:21:20"
picture: "DSCN1603.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/12032
- /detailse588.html
imported:
- "2019"
_4images_image_id: "12032"
_4images_cat_id: "1074"
_4images_user_id: "184"
_4images_image_date: "2007-09-28T16:21:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12032 -->
Gesamtbreite nur 45mm.
Das Kardangelenk ragt in die Schneckenmuttern hinein und wird auf jeder Seite durch ein Kugellager gestützt. Rechts wäre der Antrieb, Links das Rad.
