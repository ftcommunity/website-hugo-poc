---
layout: "image"
title: "Bowdenzug"
date: "2014-06-10T06:55:41"
picture: "bowdenzug1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38919
- /details3d1b.html
imported:
- "2019"
_4images_image_id: "38919"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38919 -->
Sowohl Hülle als auch Seele sind aus Plastik.
Die Seele hat knapp 4 mm Durchmesser und passt damit überall durch, wo eine fischertechnik Achse auch durchpassen würde.
Die Hülle misst ca. 5,5 Millimeter und passt perfekt zu manchen fischertechnik Teilen.
Die gelbe Plastikseele ist innen hohl und man kann eine M2 Schraube einschrauben und befestigen. Hier auf dem Bild habe ich noch eine M2 Belagscheibe dazwischen, die etwas größer als 4 mm ist.
Einziger Nachteil dieses Bowdenzuges: Er ist relativ steif; sehr enge Radien kann man damit nicht machen, die Reibung wird dann zu groß