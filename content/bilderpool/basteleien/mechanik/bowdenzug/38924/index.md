---
layout: "image"
title: "Bowdenzug Antrieb"
date: "2014-06-10T06:55:41"
picture: "bowdenzug6.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38924
- /detailsc0e8.html
imported:
- "2019"
_4images_image_id: "38924"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38924 -->
Hier eine Idee zum Antrieb des Bowdenzuges über ein Hubgetriebe.
Bei größeren Kräften müsste man aber noch dafür sorgen, daß der Baustein 7,5 nicht von der Zahnstange rutscht.
Eine andere Idee wäre mit einer längeren Schnecke und einer Schneckenmutter, die den Bowdenzug bewegt.
Dank des Bowdenzuges kann man ja den Antrieb da hinlegen, wo man etwas mehr Platz hat.