---
layout: "image"
title: "Aktor erste Idee"
date: "2014-06-10T06:55:41"
picture: "bowdenzug2.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38920
- /details7224.html
imported:
- "2019"
_4images_image_id: "38920"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38920 -->
Der Baustein 15 mit Ansenkung passt perfekt:
Der linke Stein wird bewegt. Die Belagscheibe hält den Bowdenzug in der Ansenkung des Bausteines fest.
In der Ansenkung des rechten Steins steckt die Hülle. Diese klemmt ein bisschen.
Auch in die Gelenkwürfel-Klaue passt die Hülle perfekt. Damit kann man über längere Strecken den Bowdenzug befestigen
Ein Bowdenzug ist ja in erster Linie zur Übertragung von Zugkräften geeignet.
In dem Bild hab ich mit der Statikstrebe die Seele daran gehindert, aus dem Baustein 15 herauszurutschen. Da die Seele insgesamt recht steif ist, kann man so auch Druckkräfte übertragen.
Für höhere Druckkräfte könnte man die Hülle mit einem Plastikkleber in die Ansenkung des Baustein 15 kleben, damit sie bei Druck nicht herausrutscht.