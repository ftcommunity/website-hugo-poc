---
layout: "image"
title: "Aktor Ansicht von oben"
date: "2014-06-10T06:55:41"
picture: "bowdenzug3.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38921
- /details284a.html
imported:
- "2019"
_4images_image_id: "38921"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38921 -->
Die zwei Metallstangen und die Bausteine 7,5 bilden eine Art Schlitten, Das Ganze ergibt einen recht leichtgängigen und stabilen Aktor