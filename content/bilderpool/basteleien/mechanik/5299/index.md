---
layout: "image"
title: "Rast-Z20.JPG"
date: "2005-11-11T12:04:06"
picture: "Rast-Z20.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Rastachse", "Clipachse", "Z20"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/5299
- /details8b7a.html
imported:
- "2019"
_4images_image_id: "5299"
_4images_cat_id: "465"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5299 -->
Mit scharfem Werkzeug und Klebstoff kommt man auch zu Rast-Zahnrädern Z20. Die Idee ist von Frickelsiggi.
