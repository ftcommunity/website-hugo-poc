---
layout: "image"
title: "Kegelzahnrad 4mm"
date: "2009-12-26T19:06:07"
picture: "kegelrad1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25981
- /details31ad.html
imported:
- "2019"
_4images_image_id: "25981"
_4images_cat_id: "1829"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25981 -->
Hier ein Modifiziertes Zahnrad 35146.
Mit einem Messingeinsatz versehen kann man es auf einer 4mm Achse mit einer Madenschraube befestigen.
