---
layout: "image"
title: "Kulissenstein für ft 3"
date: "2011-09-03T00:29:54"
picture: "kulissenstein3.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/31741
- /detailsea06.html
imported:
- "2019"
_4images_image_id: "31741"
_4images_cat_id: "2366"
_4images_user_id: "182"
_4images_image_date: "2011-09-03T00:29:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31741 -->
Er läßt sich wunderbar als Stoppen einsetzen.
Damit läßt sich verhindern das sich ein Baustein in der Nute verschiebt.
Jetzt kommen wir auch zu der ungewöhlichen Länge von 11mm.
Damit paßt der Kulissenstein, wenn man 2 Bausteine neben einander setzt, genau zwischen die Zapfen.
