---
layout: "image"
title: "P-Ventiladapter TST9084.JPG"
date: "2013-07-28T17:13:56"
picture: "P-Ventiladapter_TST9084.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37200
- /details5752.html
imported:
- "2019"
_4images_image_id: "37200"
_4images_cat_id: "2623"
_4images_user_id: "4"
_4images_image_date: "2013-07-28T17:13:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37200 -->
Ein Adapter für den inkompatiblen Handgriff des ft-P-Ventils. Das ist etwas kompakter als mit der "alten" ft-Seilwinde, hat aber keine Rutsch-Funktion bei Überlast. Entweder am Z20 drehen oder am Baustein 30x15x7,5 einen Hebel anbauen. Wenn der Baustein die Verzahnung schon hätte (man braucht ja nur einen kurzen Bogen), wäre das noch kompakter.
