---
layout: "image"
title: "Kreuzschieberkupplung"
date: "2012-09-22T22:25:15"
picture: "Kreuzschieberkupplung_8040b.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
schlagworte: ["Kreuzschieberkupplung", "Kreuzschlitzkupplung", "modding", "Oldham"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/35537
- /details88c0.html
imported:
- "2019"
_4images_image_id: "35537"
_4images_cat_id: "2623"
_4images_user_id: "4"
_4images_image_date: "2012-09-22T22:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35537 -->
Mit Säge und Fräse wird aus dem BS30x15x7,5 (Oder wie hier, aus einem BS30x22,5x7,5, der aus derselben Quelle kommt ;-) ) eine Kreuzschlitz- oder Kreuzschieberkupplung. Mit der Säge auf 15x15 kappen, mit der Fräse die Nut aufweiten, bis die Zapfen oder Federleisten anderer Bauteile leicht darin gleiten.
