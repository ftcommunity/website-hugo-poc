---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -unten -hinten"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog24.jpg"
weight: "24"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37973
- /detailsddfc-2.html
imported:
- "2019"
_4images_image_id: "37973"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37973 -->
