---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  +  'Fischertechnik-Smartbird-Earth-Flight'"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog27.jpg"
weight: "27"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37976
- /details1ed2-2.html
imported:
- "2019"
_4images_image_id: "37976"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37976 -->
Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410
