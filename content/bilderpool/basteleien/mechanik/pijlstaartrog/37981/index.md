---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  +  'Fischertechnik-Smartbird-Earth-Flight'"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog32.jpg"
weight: "32"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37981
- /detailsa569-2.html
imported:
- "2019"
_4images_image_id: "37981"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37981 -->
Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410

