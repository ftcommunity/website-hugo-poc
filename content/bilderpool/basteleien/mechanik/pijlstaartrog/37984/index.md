---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -oben"
date: "2014-01-02T20:26:18"
picture: "pijlstaartrog35.jpg"
weight: "35"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37984
- /details8e30-2.html
imported:
- "2019"
_4images_image_id: "37984"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:18"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37984 -->
Ich habe die Kinematik einer Pijlstaartrog in Fischertechnik nachgebaut.
Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + 1,5mm Polystyreen-Gold-Spiegel-Platten.
http://www.kunststofshop.nl/index.php?item=&action=page&group_id=10000046&lang=NL

http://www.kunststofshop.nl/index.php?action=home&lang=NL

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.
Für meine Pijlstaartrog-Fischertechnik nutze ich eine 20 Euro Polystyreen Platte Gold Spiegel 1000x500x1,5mm.

Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410


Meine Fischertechnik-Qualle mit Fin-Ray-Effect + pneumatik Muskel (Decke) gibt es unter :
http://www.ftcommunity.de/details.php?image_id=37251


Meine Adaptive Greifer gibt es unter :
http://www.ftcommunity.de/categories.php?cat_id=2775
Der adaptive Greifer mit Fin-Ray-Effect funktioniert auch wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Link zum Youtube: 

https://www.youtube.com/watch?v=NtDFw9uO-bs&index=4&list=UUvBlHQzqD-ISw8MaTccrfOQ = Qualle Fischertechnik 

https://www.youtube.com/watch?v=RjhEi15VK-4&index=7&list=UUvBlHQzqD-ISw8MaTccrfOQ = Smartbird Earthflight Fischertechnik 

https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1 = Libelle Fischertechnik 

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2 = Pijlstaartrog Fischertechnik
