---
layout: "image"
title: "Polystyreen-Gold-Spiegel-Platten."
date: "2014-01-02T20:26:18"
picture: "pijlstaartrog33.jpg"
weight: "33"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/37982
- /details818e.html
imported:
- "2019"
_4images_image_id: "37982"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37982 -->
Für meine Pijlstaartrog-Fischertechnik nutze ich eine 20 Euro Polystyreen Platte Gold Spiegel 1000x500x1,5mm.
Dieser und auch andere interessante Sachen gibt es bei :
Kunststofshop.nl ,   Didamseweg 150,    6902 PE  Zevenaar  NL
info@kunststofshop.nl

