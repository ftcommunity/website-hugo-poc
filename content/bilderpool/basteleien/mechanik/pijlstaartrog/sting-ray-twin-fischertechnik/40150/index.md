---
layout: "image"
title: "Sting-ray Twin Fischertechnik"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40150
- /details1ac9.html
imported:
- "2019"
_4images_image_id: "40150"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40150 -->
