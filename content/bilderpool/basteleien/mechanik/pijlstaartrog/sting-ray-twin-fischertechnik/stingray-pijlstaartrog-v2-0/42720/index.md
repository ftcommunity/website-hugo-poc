---
layout: "image"
title: "Andere Möglichkeiten mit Pololu Micro Metal Gearmotors -2"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv11.jpg"
weight: "11"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42720
- /details39d3.html
imported:
- "2019"
_4images_image_id: "42720"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42720 -->
micromotoren gibt es bei 

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1 

oder : 

https://www.pololu.com/category/60/micro-metal-gearmotors 

oder noch billiger :

http://nl.aliexpress.com/item/N20-DC12V-100RPM-Gear-Motor-High-Torque-Electric-Gear-Box-Motor/32524083373.html?spm=2114.13010608.0.65.NMlkHy


Zylinderschrauben M1.6 3 mm Schlitz DIN 84 Edelstahl 100 St. gibt es bei :
https://www.conrad.de/de/zylinderschrauben-m16-3-mm-schlitz-din-84-edelstahl-100-st-1059517.html     oder


https://www.pololu.com/product/1954

Dmv Conrad-art.nr: 297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen. 


Dreh-Zylinder Antrieb Alternativen M4, M5 + M6 + Info gibt es unter : 
http://www.ftcommunity.de/categories.php?cat_id=2940
