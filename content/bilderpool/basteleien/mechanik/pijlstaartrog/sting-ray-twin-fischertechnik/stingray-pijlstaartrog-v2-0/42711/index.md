---
layout: "image"
title: "Stingray-Pijlstaartrog-V2.0  mit neuer Schwanz"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv02.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42711
- /details975b-2.html
imported:
- "2019"
_4images_image_id: "42711"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42711 -->
