---
layout: "image"
title: "Stingray-Pijlstaartrog-V2.0  mit neuer Schwanz"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/42715
- /detailsff42.html
imported:
- "2019"
_4images_image_id: "42715"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42715 -->
Video's: 
https://www.youtube.com/watch?v=AlHQmpu9C04 

https://www.youtube.com/watch?v=vzF9cABscSw 

https://www.youtube.com/watch?v=UW5wPpoQAsY 

https://www.youtube.com/watch?v=TlITNxtRGBU
