---
layout: "image"
title: "Sting-ray Twin Fischertechnik"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik07.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40152
- /details89f8.html
imported:
- "2019"
_4images_image_id: "40152"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40152 -->
