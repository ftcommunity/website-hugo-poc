---
layout: "image"
title: "Sting-ray Twin Fischertechnik"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik06.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/40151
- /details458a.html
imported:
- "2019"
_4images_image_id: "40151"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40151 -->
