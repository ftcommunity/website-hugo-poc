---
layout: "image"
title: "Teilemodding - Bild 3"
date: "2011-11-03T18:20:02"
picture: "hy3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/33386
- /details691a.html
imported:
- "2019"
_4images_image_id: "33386"
_4images_cat_id: "2472"
_4images_user_id: "1162"
_4images_image_date: "2011-11-03T18:20:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33386 -->
Hier nun, die eingeschobene Rastachse 30 mit dem Zahnrad rechts dran, die Klemmbuchse sorgt dafür das die Achse nicht rausrutschen kann.
