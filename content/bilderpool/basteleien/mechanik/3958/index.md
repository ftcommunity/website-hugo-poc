---
layout: "image"
title: "Vierkantmitnehmer 1"
date: "2005-04-06T20:56:07"
picture: "Mitnehmer2.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/3958
- /details3723.html
imported:
- "2019"
_4images_image_id: "3958"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2005-04-06T20:56:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3958 -->
Ich habe mir Gedanken zum Ersatz der Vierkantmitnehmer die es ja nicht mehr gibt gemacht und dabei auf die Idee gekommen ein Messing T Profil zu verwenden.
