---
layout: "image"
title: "Zahnstangenfräse 1"
date: "2005-05-09T21:43:59"
picture: "Zahnstangenfrse_01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/4109
- /details8416.html
imported:
- "2019"
_4images_image_id: "4109"
_4images_cat_id: "350"
_4images_user_id: "115"
_4images_image_date: "2005-05-09T21:43:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4109 -->
Eigenbau Zahnstange