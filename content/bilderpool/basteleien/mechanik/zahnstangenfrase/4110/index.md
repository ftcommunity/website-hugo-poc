---
layout: "image"
title: "Zahnstangenfräse 2 Draufsicht"
date: "2005-05-09T21:51:20"
picture: "Zahnstangenfrse_02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "charly"
license: "unknown"
legacy_id:
- /php/details/4110
- /details83a6.html
imported:
- "2019"
_4images_image_id: "4110"
_4images_cat_id: "350"
_4images_user_id: "115"
_4images_image_date: "2005-05-09T21:51:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4110 -->
