---
layout: "comment"
hidden: true
title: "20627"
date: "2015-05-12T22:07:43"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
das sind ja spannende Entwicklungen von dir. Mikrometerarbeit! Toll, dass die Felge so flexibel einsetzbar ist. Felgen mit Planetengetriebe sind ja in verschiedener Hinsicht interessant, z.B. in Hinblick auf Drehmoment oder Bodenfreiheit. Apropos Bodenfreiheit: Man könnte vielleicht die S-Riegel andersrum einbauen oder durch Verbindungsstopfen ersetzen. Aber auch die filigranen Längs- und Querlenker sind dir gelungen. Wie hast du den Statikadapter in der Mitte befestigt?
Viele Grüße, Martin