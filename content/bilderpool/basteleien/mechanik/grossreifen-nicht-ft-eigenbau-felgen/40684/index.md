---
layout: "image"
title: "Felge Antrieb"
date: "2015-03-27T17:30:38"
picture: "Felge_Antrieb.jpg"
weight: "23"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Großreifen", "3D-Druck", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/40684
- /details7fd3.html
imported:
- "2019"
_4images_image_id: "40684"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2015-03-27T17:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40684 -->
Integriert ist ein 40er Innenzahnrad - mit einem 10er-Zahnrad lässt sich direkt ein Antrieb realisieren
