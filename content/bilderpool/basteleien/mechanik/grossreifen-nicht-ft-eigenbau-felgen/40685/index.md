---
layout: "image"
title: "Felge mit Reifen"
date: "2015-03-27T17:30:38"
picture: "Reifen_mit_Felge2.jpg"
weight: "24"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Großreifen", "Monster", "Reifen", "3D-Druck", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/40685
- /details3549.html
imported:
- "2019"
_4images_image_id: "40685"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2015-03-27T17:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40685 -->
Die Felge mit aufgezogenem Monster-Reifen
