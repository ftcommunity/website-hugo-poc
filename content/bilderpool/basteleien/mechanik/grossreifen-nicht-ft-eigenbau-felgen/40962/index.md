---
layout: "image"
title: "Felge45_3D.jpg"
date: "2015-05-12T20:26:43"
picture: "Felge03.jpg"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/40962
- /detailsb249.html
imported:
- "2019"
_4images_image_id: "40962"
_4images_cat_id: "366"
_4images_user_id: "4"
_4images_image_date: "2015-05-12T20:26:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40962 -->
Ich hab mich vom Thomas inspirieren lassen. Die Felge hier ist eine Alternative zur ft-Felge 45. Sie hat Ausnehmungen für die Noppen des ft-Raupenbands 146985, außen passen die ft-Reifen 65 und 80 drauf, und sie wird von innen mit einem Z10 im Modul 1 angetrieben.
