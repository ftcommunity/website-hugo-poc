---
layout: "image"
title: "Felge von vorn"
date: "2009-02-16T19:03:01"
picture: "IMG_7746.jpg"
weight: "12"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Großreifen", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/17428
- /details3f39.html
imported:
- "2019"
_4images_image_id: "17428"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-02-16T19:03:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17428 -->
Für die schon bekannten Großreifen von Conrad (Best.-Nr. 21 96 57-62) gibt es passende Felgen. Diese passen nicht ohne Modifikationen in das ft-Raster... Eine Drehbank hilft da aber gewaltig weiter :)
Best.-Nr. für die Felgen: 21 96 65-62
