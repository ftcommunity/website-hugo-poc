---
layout: "image"
title: "Ansicht komplett montiert"
date: "2009-03-06T17:04:35"
picture: "IMG_7757.jpg"
weight: "19"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Großreifen", "Planetengetriebe", "Reifen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/23383
- /detailsef78.html
imported:
- "2019"
_4images_image_id: "23383"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-03-06T17:04:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23383 -->
Durch das Planetengetriebe bekommt man eine recht ansehnliche Bodenfreiheit
