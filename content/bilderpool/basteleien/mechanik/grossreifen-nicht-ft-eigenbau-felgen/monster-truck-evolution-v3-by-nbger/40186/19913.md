---
layout: "comment"
hidden: true
title: "19913"
date: "2015-01-04T08:02:11"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo!

Zur möglichst späten Untersetzung gibt es auch etwas im Forum:
http://forum.ftcommunity.de/viewtopic.php?f=4&t=961&p=6360&hilit=Untersetzung+im+Endantrieb#p6360

Untersetzung im Endantrieb schön und gut, aber mit diesen Monsterreifen sehe ich den gewonnenen Vorteil gleich wieder am großen Radumfang flöten gehen.

Beim Humvee gibt es m.W. Unterschiede zwischen der mil-Version und der zivilen. Die zivile Version hat hinten Starrachse und Blattfedern, keine Einzelradaufhängung. Der Antrieb könnte auch anders aussehen.

Gruß,
Harald