---
layout: "image"
title: "Echter Monstertruck als Beispiel"
date: "2015-01-04T07:47:31"
picture: "RealesBeispiel.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "toplowridersites.com"
schlagworte: ["Monstertruck", "4WD", "4WS", "Monster", "Truck"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/40158
- /detailsda4f.html
imported:
- "2019"
_4images_image_id: "40158"
_4images_cat_id: "3016"
_4images_user_id: "1729"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40158 -->
Ein großspuriger Name für ein neues Monstertruck Prokekt ... ;-)

Nachdem ich bereits zwei Versionen eines Monstertrucks gebaut habe ( http://www.ftcommunity.de/categories.php?cat_id=2831 und http://www.ftcommunity.de/categories.php?cat_id=2840 ),
habe ich mir für 2015 einen Neubau vorgenommen.

Das alte Modell genügt inzwischen meinen Ansprüchen nicht mehr und ist bereits auseinandergenommen.

Monstertrucks eignen sich hervorragend, um Fahrzeugtechnik in Fischertechnik umzusetzen.
Die realen Vorbilder (siehe Bild) machen es vor. Oft sind es nicht nur filigrane Fahrzeuge zum Ansehen, sondern echte Heavy Duty Fahrzeuge mit extremer Fahrzeugtechnik. Siehe zum Beispiel dieses Video: https://www.youtube.com/watch?v=lLj_si8-qpg
Hier sieht man recht gut, was die Dinger können und aushalten (bzw. auch nicht ;-) ).

Um auch dem Projektnamen gerecht zu werden (..."Evolution" ), soll die neue Version einiges bieten und können. Inzwischen habe ich mehr über Fahrzeugtechnik hinzugelernt, was ich in diesem Projekt umsetzen möchte. Hier meine "Absichtsliste":

- der optische Eindruck soll einem echten Monstertruck nahe kommen, also riesige Federwege, kurzer Radstand, überdimensionierte Räder, im Vergleich dazu eine zu kleine Karosserie oben drauf 
- er soll nicht wesentlich größer werden als meine V2 Version, die Spurbreite eher wieder geringer
- 4WD, also Allrad-Antrieb
- 4WS, (Four Wheel Steering), also beide Achsen sollen gelenkt sein wie bei den echten Monstertrucks: Einschränkung: Ich werde wahrscheinlich beide Achsen immer gleichzeitig lenken, getrennte Lenkung für Vorder- und Hinterradachse ist mir zu kompliziert und zu umständlich über eine Remote Control zu bedienen
- Gangschaltung
- er soll wirklich fahren können, also ist Mechanik und Motorleistung zu verbessern. Hauptproblem bisher: Der Schlupf im Antriebsstrang bei großen Drehmomenten.Geradeaus sollte er zügig schaffen sowie kleine Steigungen erklimmen können.

Mit dem Projekt habe ich gerade erst angefangen. Ich habe ein Gesamtkonzept im Kopf und hoffe, daß ich es auch so umsetzen kann. Da mich das Projekt längere Zeit beschäftigen wird (habe nicht immer Zeit und auch nicht immer Lust für FT, außerdem fange ich dazwischen drin auch gerne mal ein anderes Modell an), poste ich ab und zu den Projekt-Fortschritt sowie interessante Details. Später bei einem kompletten Modell ist es oft schwierig, Details zu fotografieren. Von Sackgassen, Umbauten, Optimierungen werde ich leider nicht verschont werden.
Also, drückt mir die Daumen, daß das Ganze was wird.......