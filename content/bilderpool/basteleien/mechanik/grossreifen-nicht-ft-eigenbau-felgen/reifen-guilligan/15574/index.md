---
layout: "image"
title: "Teileübersicht"
date: "2008-09-24T22:21:35"
picture: "reifen02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/15574
- /details4785.html
imported:
- "2019"
_4images_image_id: "15574"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15574 -->
1 x Reifen Tamiya
2 x Drehscheibe
2 x Zahnrad
4 x Clipsplatte