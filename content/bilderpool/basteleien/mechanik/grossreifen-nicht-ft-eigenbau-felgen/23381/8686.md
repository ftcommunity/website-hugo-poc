---
layout: "comment"
hidden: true
title: "8686"
date: "2009-03-06T18:03:59"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo,
hast Du das Schneiden in Auftrag gegeben? Ich denke mal kaum, daß Du selbst so ein Gerät zu Hause hast. Wo hast Du das machen lassen? Und was hat das gekostet?
Andreas Tacke hat ja den Schlüssel für die Naben auch lasern lassen.
Viele Grüße, Andreas.