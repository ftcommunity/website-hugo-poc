---
layout: "image"
title: "Felge mit Innenzahnrad für Planetengetriebe"
date: "2009-03-06T17:04:34"
picture: "IMG_7754.jpg"
weight: "17"
konstrukteure: 
- "Thomas Kaiser (thkais)"
fotografen:
- "Thomas Kaiser (thkais)"
schlagworte: ["Großreifen", "Planetengetriebe", "Reifen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- /php/details/23381
- /details7271.html
imported:
- "2019"
_4images_image_id: "23381"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-03-06T17:04:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23381 -->
Dies ist eine Felge mit einem Innenzahnrad (30 Zähne). Das Zahnrad wurde mit einem Laser aus Plexiglas(R) ausgeschnitten.
