---
layout: "overview"
title: "Motoren - Anpassung und Modding"
date: 2020-02-22T07:42:48+01:00
legacy_id:
- /php/categories/2766
- /categoriese7cb.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2766 --> 
Wer einen fremden Motor an ft anpasst oder einen ft-Motor "anfasst", ist ein Teilemodder und wird mit Fingerschnittwunden und Plastik-Sägespänen bestraft.