---
layout: "image"
title: "XM Motor mit Excenter"
date: "2010-02-13T22:21:36"
picture: "xmmotor1_2.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26366
- /details0fbd.html
imported:
- "2019"
_4images_image_id: "26366"
_4images_cat_id: "1876"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T22:21:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26366 -->
Hier der XM Motor mit Excenter zum Bau eines Kompressors
