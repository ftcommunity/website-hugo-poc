---
layout: "image"
title: "XM Motor"
date: "2010-02-13T15:24:20"
picture: "xmmotor2.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/26354
- /details21e9.html
imported:
- "2019"
_4images_image_id: "26354"
_4images_cat_id: "1876"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26354 -->
Hier die Platine
