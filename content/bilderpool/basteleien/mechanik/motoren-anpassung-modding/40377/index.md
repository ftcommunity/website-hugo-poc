---
layout: "image"
title: "EMG30_ft_Ankopplung"
date: "2015-01-17T21:41:01"
picture: "P1170001.jpg"
weight: "4"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
schlagworte: ["EMG30", "Motor"]
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- /php/details/40377
- /details6e93.html
imported:
- "2019"
_4images_image_id: "40377"
_4images_cat_id: "2766"
_4images_user_id: "3"
_4images_image_date: "2015-01-17T21:41:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40377 -->
Hier ein Beispiel wie man einen Motor EMG30 einfach mit ft Verbinden kann:

- Die Achse ist 5mm und damit Power Motor kompatibel. Hier mit TST-Adapter.
- Die Montage am Modell kann über eine halbe V-Rohrhülse 30x58 erfolgen. Dazu muss die Rohrhülse vorher innen leicht angefeilt werden.