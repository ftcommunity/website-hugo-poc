---
layout: "image"
title: "Motor"
date: "2007-02-15T17:03:54"
picture: "Fredy002.jpg"
weight: "1"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9034
- /detailsabd3-2.html
imported:
- "2019"
_4images_image_id: "9034"
_4images_cat_id: "823"
_4images_user_id: "453"
_4images_image_date: "2007-02-15T17:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9034 -->
Mini Motor
