---
layout: "image"
title: "motor"
date: "2007-02-17T19:34:32"
picture: "mini_motor_002.jpg"
weight: "2"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/9051
- /details3540.html
imported:
- "2019"
_4images_image_id: "9051"
_4images_cat_id: "823"
_4images_user_id: "453"
_4images_image_date: "2007-02-17T19:34:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9051 -->
