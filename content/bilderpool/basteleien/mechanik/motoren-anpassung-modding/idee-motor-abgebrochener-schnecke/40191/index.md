---
layout: "image"
title: "Motor mit Pneumatikschlauch"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke3.jpg"
weight: "3"
konstrukteure: 
- "Ich"
fotografen:
- "i"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- /php/details/40191
- /detailsed14.html
imported:
- "2019"
_4images_image_id: "40191"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40191 -->
Einfach den Schlauch zuschneiden und auf den Stift des Motors setzen.