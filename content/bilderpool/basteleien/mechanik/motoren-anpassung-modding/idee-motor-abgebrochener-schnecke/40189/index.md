---
layout: "image"
title: "Bauteile"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke1.jpg"
weight: "1"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- /php/details/40189
- /detailsb926-3.html
imported:
- "2019"
_4images_image_id: "40189"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40189 -->
Hey,

Da ich mal versucht habe die Drehscheibe direkt am Motor anzubringen ist mir leider die Schnecke gebrochen danach habe ich sie ganz entfernt. Dann hab ich mir überlegt was ich aus dem Motor jetzt noch machen könnte und bin draufgekommen dass man nur ein Stück Pneumatikschlauch auf den Stift des Motors anbringen und dann die Drehscheibe festdrehen muss.

MfG
Flo