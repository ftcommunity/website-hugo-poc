---
layout: "image"
title: "Großes Fahrzeug, oben"
date: "2007-01-21T15:27:21"
picture: "f2.jpg"
weight: "2"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8589
- /details425a-2.html
imported:
- "2019"
_4images_image_id: "8589"
_4images_cat_id: "791"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8589 -->
Das ist ein Beispiel für eine Antriebsachse mit einem ft-Fremdmotor