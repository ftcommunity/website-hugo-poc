---
layout: "image"
title: "kleines Fahrzeug, Antrieb"
date: "2007-01-21T15:27:21"
picture: "f4.jpg"
weight: "4"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
uploadBy: "Wert"
license: "unknown"
legacy_id:
- /php/details/8591
- /details5c13.html
imported:
- "2019"
_4images_image_id: "8591"
_4images_cat_id: "791"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8591 -->
Das ist ein kleines Fahrzeug mit einem ft-Fremdmotor. Es ist schneller als mit einem 8:1 PM.