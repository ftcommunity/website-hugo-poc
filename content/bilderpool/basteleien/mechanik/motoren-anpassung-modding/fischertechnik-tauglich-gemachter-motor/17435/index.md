---
layout: "image"
title: "Motor von Johnson im ft-Gehäuse"
date: "2009-02-18T21:59:22"
picture: "P2130219.jpg"
weight: "8"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
schlagworte: ["Motor", "Johnson", "Pollin"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/17435
- /detailsd6a5.html
imported:
- "2019"
_4images_image_id: "17435"
_4images_cat_id: "1095"
_4images_user_id: "381"
_4images_image_date: "2009-02-18T21:59:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17435 -->
Die genaue Bezeichnung lautet:
Gleichstrommotor Johnson 1060441 3..13,5V-
Erhältlich bei Pollin unter der Bestellnr. 310398
Preis 1,95 Euro

Der Motor dreht ungefähr 5 mal langsamer als ein alter grauer ft-Motor.

Sorry für das schlechte Foto, im Moment gehts nicht besser.
