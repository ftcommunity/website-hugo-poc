---
layout: "image"
title: "Powermotorplatte 2b"
date: "2009-01-07T21:22:47"
picture: "Platte2b.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16947
- /detailse27a.html
imported:
- "2019"
_4images_image_id: "16947"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16947 -->
Winterzeit ist Bastelzeit
Hier die Rückseite der roten Platte.
