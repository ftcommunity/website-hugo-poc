---
layout: "image"
title: "Zahnrad Z 10 mit Messingnabe"
date: 2023-10-30T22:56:36+01:00
picture: "Z10_5_78238.JPG"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
uploadBy: "Website-Team"
license: "unknown"
---

Zahnrad Z 10 mit Messingnabe als Ersatz für 159577 bzw 78238