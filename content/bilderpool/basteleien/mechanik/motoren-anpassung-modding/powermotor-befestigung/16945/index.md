---
layout: "image"
title: "Powermotorplatte 1b"
date: "2009-01-07T21:22:47"
picture: "Platte1b.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16945
- /detailsff8f-3.html
imported:
- "2019"
_4images_image_id: "16945"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16945 -->
Winterzeit ist Bastelzeit
Ich habe hier mal eine stabile Powermotorplatte gemacht. Damit kann man den Motor vernünftig im Modell einbauen und er kann mit 2 Schrauben befestigt werden. Hier die Rückseite
