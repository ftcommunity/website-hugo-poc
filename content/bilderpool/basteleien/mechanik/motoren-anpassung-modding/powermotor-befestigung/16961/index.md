---
layout: "image"
title: "Powermotor mit Stufengetriebe"
date: "2009-01-09T22:16:26"
picture: "PlatteStufengetriebe1.jpg"
weight: "9"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/16961
- /detailsce69-2.html
imported:
- "2019"
_4images_image_id: "16961"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-09T22:16:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16961 -->
Und hier ist der Anbau an das Stufengetriebe zu sehen. Somit hat man mal richtig Drehmoment auf der Achse :-))
