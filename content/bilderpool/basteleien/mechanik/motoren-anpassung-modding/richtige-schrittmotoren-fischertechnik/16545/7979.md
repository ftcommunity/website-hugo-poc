---
layout: "comment"
hidden: true
title: "7979"
date: "2008-12-12T09:30:06"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@flyingcat
Hallo Christian,
kannst du mich bitte mal schlau machen, was du hier an welchen Pins der RS232 abgreifen willst? Die Ansteuerung per ROBO-Interface führst du dann im Download-Modus durch? In Online-Modus dürfte dich so fürchte ich hierbei auch das "Windows-Problem" verfolgen. Ich für meinen Teil denke da an die Schrittmotor-Steuerkarten SMC-800 oder 1500 für 3 Motoren, die für Modelle mit ft-Statik ausreichend sein sollten. Meine ft-Schrittmotoren will ich dabei auch nicht wegwerfen. Da ich gern mit Anzeigen arbeiten will, wird es wohl für mich was ohne ROBO-Interface und mit "fremder" Steuersoftware werden müssen. Meine Vorstellung zum "Abgreifen" am ROBO-Interface ging noch weiter. Ich wollte im Interface am internen Eingang zu den Motor-Leistungsstufen für eine externe Schrittmotoransteuerung abgreifen. Die Benennung der Abgriffe dazu bekam ich vom ROBO-Entwickler aber nicht beantwortet. Dieses Vorhaben habe ich aber inzwischen aus eigener Erkenntnis fallen lassen. Man munkelt auch von thermischen Problemen im Interface bei der Ansteuerung von Schrittmotoren an seinen Motorausgängen. Ob die sich dann bei einem Abgreifen der Ansteuerung ohne Anschluß seiner Motorausgänge abstellen?
Gruß, Ingo