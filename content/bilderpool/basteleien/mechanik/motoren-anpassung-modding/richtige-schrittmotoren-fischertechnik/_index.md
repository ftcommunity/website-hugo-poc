---
layout: "overview"
title: "'Richtige' Schrittmotoren und Fischertechnik"
date: 2020-02-22T07:42:55+01:00
legacy_id:
- /php/categories/1495
- /categories6037.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1495 --> 
Ich wollte mal einen Schrittmotor mit deutlich mehr Leistung als die Knobloch-Variante an Fischertechnik anbinden. Diese Bilder zeigen, wie\'s geht.