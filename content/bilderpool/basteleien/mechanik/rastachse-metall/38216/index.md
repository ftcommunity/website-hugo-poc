---
layout: "image"
title: "Achsen Ende bearbeitet"
date: "2014-02-10T17:13:42"
picture: "P1040157.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38216
- /detailsbccd.html
imported:
- "2019"
_4images_image_id: "38216"
_4images_cat_id: "2845"
_4images_user_id: "1729"
_4images_image_date: "2014-02-10T17:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38216 -->
Die Idee kam mir am Wochenende. Ich wollte was mit einem S-Motor inkl. Getriebe und Rastachsen machen.
Leider gibt es die Rast-Antriebsachse 31082 für das Getriebe nicht so, daß auf beiden Seiten des Getriebes sich ein Achs-Anschluß ergibt. (im Vergleich zur Metallachse 31063)
Dann hätte leider meine ganze Modellidee nicht funktioniert und ich hätte mir eine neue Lösung überlegen müssen.
Dann kam mir die Idee einer Metall-Rastachse...

Die Herstellung ist eigentlich einfach, man braucht nicht viel Werkzeug (Schraubstock, Flachfeile, Metallsäge, Schraubenzieher, kleiner Hammer).
Also, die Achse auf beiden Seiten etwas anfeilen, bis man das Maß der Kunststoff-Rastachsen erreicht hat. Dann ein paar Millimeter mittig einsägen, den Schraubenzieher ansetzen und mit dem Hammer einen kleinen Schlag geben.
Die Enden biegen sich dann ein bisschen auseinander und die Achse arretiert dadurch gut in einer Rastkupplung.