---
layout: "image"
title: "Kardangelenk kombinert"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen5.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40605
- /details05ce.html
imported:
- "2019"
_4images_image_id: "40605"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40605 -->
Durch die Einkerbungen kann man zwei Kardangelenke miteinander oder mit einem Mini-Steckverbinder formschlüssig verbinden. Dann ergibt sich auch ein geschlossenes rundes Profil.