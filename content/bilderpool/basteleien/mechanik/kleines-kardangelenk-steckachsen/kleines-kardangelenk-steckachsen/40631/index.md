---
layout: "image"
title: "Herstellung Kardangelenk - bohren"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40631
- /details1bcb.html
imported:
- "2019"
_4images_image_id: "40631"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40631 -->
... aber das Bohren geht super mit einer schräg angeflexten Nadel. Das ergibt einen Bohrer mit ca. 0,5mm Durchmesser.