---
layout: "image"
title: "Kardangelenk-Würfel - feilen"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40636
- /detailsd0a0-3.html
imported:
- "2019"
_4images_image_id: "40636"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40636 -->
... und danach auch.