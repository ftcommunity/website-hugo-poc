---
layout: "image"
title: "Kardangelenk - Zusammenbau 1"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40637
- /detailsee65.html
imported:
- "2019"
_4images_image_id: "40637"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40637 -->
Damit der Würfel nicht verloren geht, habe ich schon mal die eine Hälfte des Kardangelenks zusammengebaut, bevor ich ihn ganz abgesägt habe.