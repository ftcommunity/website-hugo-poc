---
layout: "image"
title: "Frontantriebs-Modul von unten"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40628
- /details34de.html
imported:
- "2019"
_4images_image_id: "40628"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40628 -->
Das Foto ist um 180° gedreht und soll eine Ansicht "von unten" sein.