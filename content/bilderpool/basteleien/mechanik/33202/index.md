---
layout: "image"
title: "Gewichte für kleine Box"
date: "2011-10-16T21:52:32"
picture: "gewichte1.jpg"
weight: "14"
konstrukteure: 
- "Markus Wolf (mawolf)"
fotografen:
- "Markus Wolf (mawolf)"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/33202
- /details2008-2.html
imported:
- "2019"
_4images_image_id: "33202"
_4images_cat_id: "465"
_4images_user_id: "968"
_4images_image_date: "2011-10-16T21:52:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33202 -->
Hier habe ich mal aus einer alten Orgelpfeife (Zinn/ Blei Legierung) ein paar Gewichte gegossen die in die Box passen.
Drei Stück in einer Box wiegen ca. 800 gr.
