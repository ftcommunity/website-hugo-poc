---
layout: "image"
title: "Schraube02.JPG"
date: "2007-04-17T21:58:49"
picture: "Schraube02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/10090
- /detailscda8.html
imported:
- "2019"
_4images_image_id: "10090"
_4images_cat_id: "913"
_4images_user_id: "4"
_4images_image_date: "2007-04-17T21:58:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10090 -->
Diese Schraube Marke "Robbe" ist auch von Conrad. Sie hat ein Sackloch mit einem Messing-Einsatz und ein Standard-Gewinde (M3,5 wenn ich richtig liege).

Die Antriebswelle ist zweiteilig: das Gewindestück stammt von einer Messingschraube, bei der am Kopfende ein Stück soweit abgedreht wurde, dass es in den 3-mm-Innenraum eines 4-mm-Messingrohrs (Baumarkt) eingeklebt werden kann. Zur Befestigung ist noch eine Kontermutter draufgesetzt.
