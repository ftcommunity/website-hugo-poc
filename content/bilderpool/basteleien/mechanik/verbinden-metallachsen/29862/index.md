---
layout: "image"
title: "Verbinden von Metallachsen"
date: "2011-02-04T15:24:29"
picture: "mmmm4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29862
- /detailse574-2.html
imported:
- "2019"
_4images_image_id: "29862"
_4images_cat_id: "2199"
_4images_user_id: "1162"
_4images_image_date: "2011-02-04T15:24:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29862 -->
Nun habt ihr eine richtig feste Verbindung.
