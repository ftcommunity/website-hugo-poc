---
layout: "image"
title: "Verbinden von Metallachsen"
date: "2011-02-04T15:24:20"
picture: "mmmm3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29861
- /detailsddce.html
imported:
- "2019"
_4images_image_id: "29861"
_4images_cat_id: "2199"
_4images_user_id: "1162"
_4images_image_date: "2011-02-04T15:24:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29861 -->
Nun macht ihr das ganze mit der anderen Achse genauso.
