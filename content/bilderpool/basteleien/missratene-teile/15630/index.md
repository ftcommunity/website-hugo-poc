---
layout: "image"
title: "Missratene Bausteine"
date: "2008-09-26T08:06:06"
picture: "sonderteile3.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15630
- /detailsb30e.html
imported:
- "2019"
_4images_image_id: "15630"
_4images_cat_id: "646"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15630 -->
Da muss das Granulat ausgegangen sein.
