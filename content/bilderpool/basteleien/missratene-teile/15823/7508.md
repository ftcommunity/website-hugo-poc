---
layout: "comment"
hidden: true
title: "7508"
date: "2008-10-07T20:42:18"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Ich dachte, dass hier auch die mißlungenen Bauteile hingehören.
Es ist ja nicht das erste, und eine entsprechende Rubrik konnte ich nicht finden.
Ich weiß natürlich, dass ich selbst Rubriken anlegen kann, aber teilweise gibt es da auch schon einen Wildwuchs, den ich nicht noch verschlimmern wollte.

In der Fotocommunity können nur die Admins neue Rubriken anlegen, und selbst zum Vorschlagen muß man schon etwas Erfahrung mitbringen.
Das System dort funktioniert sehr gut, und das obwohl es dort ca. 1000 mal so viele Bilder gibt wie hier.

http://www.fotocommunity.de