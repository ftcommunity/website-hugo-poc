---
layout: "image"
title: "Kettenglied"
date: "2008-10-06T22:44:14"
picture: "Kettenglied.jpg"
weight: "13"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "HLGR"
schlagworte: ["Kettenglied"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15823
- /details9bd8.html
imported:
- "2019"
_4images_image_id: "15823"
_4images_cat_id: "646"
_4images_user_id: "832"
_4images_image_date: "2008-10-06T22:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15823 -->
Bei diesem Kettenglied hat wohl das Granulat nicht ganz gereicht.
