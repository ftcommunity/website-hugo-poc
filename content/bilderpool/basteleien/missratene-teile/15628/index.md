---
layout: "image"
title: "Falsche Seite"
date: "2008-09-26T08:06:05"
picture: "sonderteile1.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Thomas Falkenberg (speedy68)"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/15628
- /details1306.html
imported:
- "2019"
_4images_image_id: "15628"
_4images_cat_id: "646"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15628 -->
Da wurde versucht den Zapfen in die falsche Seite zu einzupressen
