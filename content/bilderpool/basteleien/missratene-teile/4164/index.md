---
layout: "image"
title: "MoMo02.jpg"
date: "2005-05-19T21:52:53"
picture: "MoMo02.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4164
- /detailsd340.html
imported:
- "2019"
_4images_image_id: "4164"
_4images_cat_id: "646"
_4images_user_id: "4"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4164 -->
Im Gegenlicht sieht man, dass der Zapfen einen winzigen Lichtspalt freilässt. So etwa 2/10 mm hätte er noch tiefer reingehört.

Den BS15/2Zapfen/1x45° sollte ich vielleicht unter "Kuriositäten und Mutanten" sortieren und meistbietend versteigern...