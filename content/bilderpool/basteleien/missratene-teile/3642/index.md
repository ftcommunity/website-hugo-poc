---
layout: "image"
title: "Verzogene Flachsteine"
date: "2005-02-16T13:43:44"
picture: "verzogene_Flachsteine_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/3642
- /detailsb410.html
imported:
- "2019"
_4images_image_id: "3642"
_4images_cat_id: "646"
_4images_user_id: "104"
_4images_image_date: "2005-02-16T13:43:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3642 -->
Wie im ft-Forum besprochen, hier die Fotos. Die linken beiden durchsichtigen Flachsteine sind verzogen (und zwar 16 Stück alle genau gleich). Die rechten zwei sind gerade von ebay ersteigert, müssen aber ähnlich alt sein, denn die Dinger werden ja leider nicht mehr hergestellt.