---
layout: "image"
title: "Verbindungsstücke"
date: "2008-01-09T17:47:17"
picture: "Verbindungsstueck.jpg"
weight: "41"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
schlagworte: ["Verbindungsstücke", "15", "30", "45"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13299
- /details3daf.html
imported:
- "2019"
_4images_image_id: "13299"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13299 -->
Verbindungsstücke