---
layout: "comment"
hidden: true
title: "7148"
date: "2008-09-15T14:25:42"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Soviel ich weiss sind auf der anderen Seite die Kürzel für die Strebe angegeben, also X oder I. So wie hier abgebildet habe ich es aber auch noch nie gesehen.