---
layout: "comment"
hidden: true
title: "7160"
date: "2008-09-16T09:18:17"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

das falsche (-)-Vorzeichen vor der „84,8“ lässt auf eine Fehlprägung schließen!

Bei echten Sammlern, wie man sie z.B. aus Modelbahn- oder Briefmarkensammlerkreisen kennt, stellen Fehlprägungen oder auch Vorserienmodelle echte Begeisterungsstürme aus.
In der Regel sind solche Modelle (vom Erkennen des Fehlers bis zum Auslieferungsstopp) nur kurz auf dem Markt und unter Umständen nur in sehr geringen Stückzahlen vorhanden. Dies kann sich dann sehr stark auf den Sammlerpreis (=Liebhaberpreis) auswirken!

Das auf dem unteren Foto zu sehende Zeichen vor der „84,8“ ist keineswegs ein schiefes (x)-Zeichen sondern ein (-)-Zeichen das durch ein(/)-Zeichen gekreuzt wird. 
Dieses etwas unförmige Zeichen lässt auf eine kurzfristig, an der Form, durchgeführte Korrektur schließen.

In wie weit diese Art der Strebe nun als Sammerobjekt dient bleibt Spekulation.

Gruß

Lurchi