---
layout: "image"
title: "Verschiedene Kleinmotoren im Größenvergleich"
date: "2008-02-01T19:25:34"
picture: "IMG_0583.jpg"
weight: "48"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/13503
- /details3440.html
imported:
- "2019"
_4images_image_id: "13503"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2008-02-01T19:25:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13503 -->
Verschiedene Kleinmotoren im Größenvergleich zum Baustein 30
(von links nach rechts)

1.) 31062.1 Minimotor (6 V) in Bausteingröße,
Maß 30 x 15 x 15 (enthalten in mini-mot 1, 32091.1 von 1969)

2.) 31062.2 Standard-Minimotor (6 V), Maß 30 x 20 x 15 (u.a. enthalten in 32091.2, 30192, 30190, 30186)

3.) 32240 S-Motor grau (6-9 V), Maß 37,5 x 30 x 22,5 (enthalten in 'Trainigsroboter' Art.-Nr. 30572)