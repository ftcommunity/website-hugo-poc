---
layout: "comment"
hidden: true
title: "7386"
date: "2008-09-30T19:31:39"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo ft-Freunde,
bin mir sicher, das kreative Vorstellungen wie oben vorgestellt unter uns sind. Nur muss man dann das grafisch vorstellen. Dazu von mir ein Tipp, der für alle Interessierten erschwinglich ist.
Zu kaufen gibt es dazu aktuell die 2D/3D-CAD-Lösung TurboCAD 12 Deluxe mit interaktivem Lernprogramm für nur 39,90 Euro, AutoCAD-kompatibel (kam mal 129,- Euro). Wer interssiert ist, mailt mich an und erfährt die Quelle. Damit keine falschen Schlussfolgerungen aufkommen, ich kaufe es auch dort.
Wer aber bei der Einarbeitung in solche Software-Anwendungen keine Ausdauer hat, sollte die Finger davon lassen. Das ist ein professionelles Programm für vernünftig wirtschaftende  Leute.
Grüsse euch, Udo2