---
layout: "image"
title: "Fire Trucks Leiter"
date: "2012-02-12T14:48:21"
picture: "Leiter_in_zwei_Farben.jpg"
weight: "71"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/34140
- /details8bdb.html
imported:
- "2019"
_4images_image_id: "34140"
_4images_cat_id: "1119"
_4images_user_id: "184"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34140 -->
Beim auspacken eines neueren Fire Truck Baukastens viel mir auf das die Leiter nicht aus dem gleichen Material gemacht wurde wie die aus den älteren.
