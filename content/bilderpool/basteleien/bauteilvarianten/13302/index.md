---
layout: "image"
title: "Zahnrad 40 32 Vorderseite"
date: "2008-01-09T17:47:18"
picture: "Zahnrad4032_2.jpg"
weight: "44"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
schlagworte: ["Zahnrad", "Z40-32"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13302
- /details702f.html
imported:
- "2019"
_4images_image_id: "13302"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13302 -->
Zahnrad 40 32 Vorderseite, ganz andere Beschaffung, Material sieht aus wie 'nass' oder lackiert, selten
