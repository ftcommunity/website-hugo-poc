---
layout: "comment"
hidden: true
title: "7474"
date: "2008-10-05T09:20:53"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Holger,
hoffentlich kommst du jetzt nicht auf den Gedanken, ich neide dir deine Entwürfe. Das Gegenteil ist der Fall! Bei einer Argumentation "pro" im Sinne des Erfinders sollte man aber auch halbwegs exakt bleiben. Die Stirnnut der aktuellen BS 15 und 30 ist 6,5mm und die der Quernuten an den Bauteilvorschlägen durch die Kantenenentschärfung ca. 13mm lang.
Gruß, Ingo