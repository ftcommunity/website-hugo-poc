---
layout: "image"
title: "Rückseite"
date: "2009-10-11T18:13:04"
picture: "variantendeswinkelsteins1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25540
- /detailsf93d.html
imported:
- "2019"
_4images_image_id: "25540"
_4images_cat_id: "1792"
_4images_user_id: "104"
_4images_image_date: "2009-10-11T18:13:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25540 -->
Links ist der neuere, etwas größere, rechts der ältere, etwas kleinere. Es macht weniger als 1 mm aus, aber der Unterschied ist deutlich sicht- und fühlbar.
