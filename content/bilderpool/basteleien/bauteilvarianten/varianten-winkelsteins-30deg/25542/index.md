---
layout: "image"
title: "Geometrie"
date: "2009-10-11T18:13:04"
picture: "variantendeswinkelsteins3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/25542
- /details2dfe.html
imported:
- "2019"
_4images_image_id: "25542"
_4images_cat_id: "1792"
_4images_user_id: "104"
_4images_image_date: "2009-10-11T18:13:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25542 -->
Links der alte, kleinere: Der ergibt mit dieser Konstruktion genau den richtigen, rasterkonformen vertikalen Abstand (horizontal nicht, wie man sieht).

Rechts der neue, größere: Drei davon zu einem Bogen ergänzt ergeben genau 7,5 mm, also einen "halben Baustein", Versatz.
