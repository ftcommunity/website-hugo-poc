---
layout: "image"
title: "Riegelscheibe 36334 1"
date: "2012-02-13T19:19:59"
picture: "Riegelscheibe_36334_1.jpg"
weight: "72"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34180
- /details96ca.html
imported:
- "2019"
_4images_image_id: "34180"
_4images_cat_id: "1119"
_4images_user_id: "328"
_4images_image_date: "2012-02-13T19:19:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34180 -->
Ich habe genau 5 Riegelscheiben 36334 in meinem Bestand, die auf der Oberseite nicht diese 4 normalen charakteristischen Knubbel haben, hier die 3 Scheiben links im Bild. Keine Ahnung, aus welchem Kasten die sind, aber sie sind Originale und nicht modifiziert.