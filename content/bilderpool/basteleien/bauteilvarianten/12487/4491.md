---
layout: "comment"
hidden: true
title: "4491"
date: "2007-11-05T18:56:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Vom Flachstein 30x30 gab es noch eine "ganz alte" Bauform, die völlig massiv war. Ich habe davon welche von meinem allerersten ft 100-Kasten (und zwar auch hier von der ganz alten Version mit der etwas "ordentlicher" wirkenden Einsortierung als der ab ca. 1970).

Gruß,
Stefan