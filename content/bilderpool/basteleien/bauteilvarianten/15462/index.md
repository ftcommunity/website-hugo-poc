---
layout: "image"
title: "Streben mit Außenloch"
date: "2008-09-22T21:33:17"
picture: "ft-streben_004.jpg"
weight: "58"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/15462
- /details2721.html
imported:
- "2019"
_4images_image_id: "15462"
_4images_cat_id: "1119"
_4images_user_id: "473"
_4images_image_date: "2008-09-22T21:33:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15462 -->
Wer kennt diese Streben und weiß, wo es die gab ?!?

Sie lassen sich zusammenstecken und haben ein herausgehobenes Loch jeweils am Ende