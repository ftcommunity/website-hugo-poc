---
layout: "image"
title: "L-Streben-Varianten"
date: "2014-11-09T17:21:24"
picture: "P1030176.jpg"
weight: "75"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "H.A.R.R.Y."
schlagworte: ["38533", "38537", "90L", "30L", "S-Strebe", "grau", "Statikstrebe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/39788
- /details8d89.html
imported:
- "2019"
_4images_image_id: "39788"
_4images_cat_id: "1119"
_4images_user_id: "1557"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39788 -->
Ich dachte immer, die L-Streben (also die mit den Löchern alle 15mm) hätte es nur beidseitig gesenkt gegeben. Offensichtlich gab es da auch ältere Varianten (hier als Beispiel 2 Stück 90er = 38533 - die 30er = 38537 habe ich auch in den beiden Ausführungen).

Ob mir jemand verläßlich sagen könnte, in welchen Jahren die beiden Sorten gefertigt wurden? Das wäre sehr schön und nett.
