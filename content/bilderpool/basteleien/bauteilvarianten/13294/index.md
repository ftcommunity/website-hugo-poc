---
layout: "image"
title: "Grundplatte 180x90 und 90x90"
date: "2008-01-09T17:47:17"
picture: "Grundplatte18090.jpg"
weight: "36"
konstrukteure: 
- "Peter Aeberhard"
fotografen:
- "Peter Aeberhard"
schlagworte: ["Grundplatte 180x90"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- /php/details/13294
- /details8751.html
imported:
- "2019"
_4images_image_id: "13294"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13294 -->
von der Grundplatte 180 x 90 und 90 x 90 gibt es verschiedene Varianten mit jeweils unterschiedlich vielen Löcher. Die ältesten Grundplatten besitzen (gegen das Licht geschaut)...
