---
layout: "image"
title: "Flachstein 30"
date: "2008-04-14T23:17:42"
picture: "bouwsteen.jpg"
weight: "49"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- /php/details/14266
- /detailsa8cc.html
imported:
- "2019"
_4images_image_id: "14266"
_4images_cat_id: "1119"
_4images_user_id: "764"
_4images_image_date: "2008-04-14T23:17:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14266 -->
Ich habe noch eine Bauteilvariante gefunden.
Dieses ist ganz massif.