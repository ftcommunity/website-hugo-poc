---
layout: "image"
title: "Streben"
date: "2008-09-22T21:33:17"
picture: "ft-streben_003.jpg"
weight: "56"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/15460
- /details273b.html
imported:
- "2019"
_4images_image_id: "15460"
_4images_cat_id: "1119"
_4images_user_id: "473"
_4images_image_date: "2008-09-22T21:33:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15460 -->
