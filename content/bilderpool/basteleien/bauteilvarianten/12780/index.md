---
layout: "image"
title: "Schnecken 35977.JPG"
date: "2007-11-18T22:06:38"
picture: "Schnecken_35977.JPG"
weight: "22"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12780
- /detailsc351.html
imported:
- "2019"
_4images_image_id: "12780"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T22:06:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12780 -->
Von unten nach oben:
Die Schnecke gibt es in zwei Ausführungen, ohne Stummel (ganz unten) und mit Stummel (vorletzte). 
Die Version ohne Stummel lässt sich weiter in die Schneckenmutter hineindrehen (mitte, eine Klemmbuchse 5 wird fast versenkt).
Die Version mit Stummel (zweiter von oben) lässt sich durch "Teile-Modding" (hier mit Drehmaschinchen gemacht) auch soweit bringen, dass sie voll in eine Schneckenmutter eingedreht werden kann (oben).
