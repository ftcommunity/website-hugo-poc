---
layout: "image"
title: "Riegelscheibe 36334 2"
date: "2012-02-13T19:19:59"
picture: "Riegelscheibe_36334_2.jpg"
weight: "73"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- /php/details/34181
- /details3b82.html
imported:
- "2019"
_4images_image_id: "34181"
_4images_cat_id: "1119"
_4images_user_id: "328"
_4images_image_date: "2012-02-13T19:19:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34181 -->
Hier sieht man deutlich, dass die Scheiben ohne Knubbel wesentlich flacher bauen. Prima, wenn's mal eng zugeht oder eine genauer Abstand auf einer Achse dargestellt werden muss, wo die normalen Riegelscheiben zu dick sind.

Diese flachere Riegelscheibe 36334 liegt in ihrer Dicke quasi zwischen der normalen Riegelscheibe 36334 (dicker) und der Scheibe 4 105195 (dünner).