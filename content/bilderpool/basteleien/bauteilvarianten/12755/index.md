---
layout: "image"
title: "Verschiedene Bausteine mit Bohrung"
date: "2007-11-14T18:06:46"
picture: "IMG_0347.jpg"
weight: "19"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12755
- /detailsca13.html
imported:
- "2019"
_4images_image_id: "12755"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-14T18:06:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12755 -->
Verschiedene Bausteine 30 mit Bohrung (von links nach rechts)

1.) 31004.1 Baustein 30 mit Bohrung grau,
alte Bauart mit runder Bohrung

2.) 31004.2 Baustein 30 mit Bohrung grau,
neuere Bauart mit eckiger Bohrung