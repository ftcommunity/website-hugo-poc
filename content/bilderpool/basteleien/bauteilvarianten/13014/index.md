---
layout: "image"
title: "das hat mir einfach mal Spass gemacht  Die Geschichte des Baustein 30"
date: "2007-12-07T18:24:22"
picture: "Eigene_Bilder_043.jpg"
weight: "31"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/13014
- /detailsda81.html
imported:
- "2019"
_4images_image_id: "13014"
_4images_cat_id: "1119"
_4images_user_id: "473"
_4images_image_date: "2007-12-07T18:24:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13014 -->
