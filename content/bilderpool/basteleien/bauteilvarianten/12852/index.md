---
layout: "image"
title: "Reifen30 31017.JPG"
date: "2007-11-27T18:36:00"
picture: "Reifen30_31017.JPG"
weight: "27"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/12852
- /detailsc087.html
imported:
- "2019"
_4images_image_id: "12852"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:36:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12852 -->
Links: Versionen mit Innen"verzahnung". Der hintere hat 3 Rillen, der vordere nur 2. Rechts die Version "innen glatt", die in den ft-Naben rutscht wenn man sie nicht anzieht, dafür aber auf die Räder 23 draufpasst.
