---
layout: "image"
title: "Verschiedene seltene graue Statikteile"
date: "2007-11-11T08:33:13"
picture: "IMG_0335.jpg"
weight: "17"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- /php/details/12627
- /details4053.html
imported:
- "2019"
_4images_image_id: "12627"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12627 -->
Verschiedene seltene graue Statikteile

1.)hinten: 
32070 Winkelträger 7,5° grau (sehr selten!)
vorne (von links nach rechts):
2.) 38577 I-Strebe 15 grau (sehr selten!)
3.) 31576 Winkelverbinder 30 grau
4.) 31577 Eckverbinder 15 grau