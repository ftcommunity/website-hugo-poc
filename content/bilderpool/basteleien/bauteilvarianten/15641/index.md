---
layout: "image"
title: "Bauteil 15 ohne Zapfen Schnitt 3"
date: "2008-09-27T16:22:35"
picture: "B15_4.jpg"
weight: "65"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- /php/details/15641
- /details8960.html
imported:
- "2019"
_4images_image_id: "15641"
_4images_cat_id: "1119"
_4images_user_id: "832"
_4images_image_date: "2008-09-27T16:22:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15641 -->
3D-Schnitt 3