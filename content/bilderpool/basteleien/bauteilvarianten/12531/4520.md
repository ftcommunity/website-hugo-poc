---
layout: "comment"
hidden: true
title: "4520"
date: "2007-11-07T23:08:05"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Der Winkelbaustein 30° von heute ist sichtbar größer als der bis mindestens in die 70erJahre hinein hergestellte. 3 von den heutigen machen genau 7,5 mm "Offset" in jede Richtung, also 1/2 Rastermaß. Dafür passte der alte plus 1 BS 30 plus ein Winkelstein 60° ganz genau ins ft-Raster, z. B. bei einem schrägen Frontfenster eines Führerhauses oder als Verstrebung vor der Einführung der Statikteile. Ich habe von beiden Fassungen welche und muss mal daran denken, sie zu fotografieren.

Gruß,
Stefan