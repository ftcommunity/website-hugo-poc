---
layout: "image"
title: "Black is beatiful"
date: "2017-02-16T23:31:26"
picture: "bib1.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45240
- /detailsc372.html
imported:
- "2019"
_4images_image_id: "45240"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45240 -->
In einer aufgekauften Sammlung waren plötzlich ganz viele Exoten, die ich so gar nicht kannte.
In welchen Kästen kamen die denn vor?
