---
layout: "image"
title: "Mini-Mot-Schnecke"
date: "2017-02-16T23:31:26"
picture: "bib8.jpg"
weight: "8"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45247
- /details0c94.html
imported:
- "2019"
_4images_image_id: "45247"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45247 -->
Das Gehäuse ist tatsächlich schwarz.
