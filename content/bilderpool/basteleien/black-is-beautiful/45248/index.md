---
layout: "image"
title: "Der Pyramidenstein in schwarz"
date: "2017-02-16T23:31:26"
picture: "bib9.jpg"
weight: "9"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45248
- /detailse29e.html
imported:
- "2019"
_4images_image_id: "45248"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45248 -->
Die zugehörigen Flügel sind übrigens gelb.
