---
layout: "image"
title: "Schwarzes Seil"
date: "2017-02-16T23:31:26"
picture: "bib7.jpg"
weight: "7"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45246
- /details32ec.html
imported:
- "2019"
_4images_image_id: "45246"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45246 -->
Es sieht tatsächlich so aus wie ein original ft-Seil - halt in schwarz.
