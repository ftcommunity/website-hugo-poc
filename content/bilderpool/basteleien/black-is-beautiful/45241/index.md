---
layout: "image"
title: "Kippschalter in schwarz"
date: "2017-02-16T23:31:26"
picture: "bib2.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
fotografen:
- "Ralf Geerken (ThanksForTheFish)"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- /php/details/45241
- /details87b6.html
imported:
- "2019"
_4images_image_id: "45241"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45241 -->
Schwarzes Kleid mit weißer Spitze
