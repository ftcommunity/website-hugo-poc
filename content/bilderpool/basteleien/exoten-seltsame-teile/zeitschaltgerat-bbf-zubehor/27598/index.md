---
layout: "image"
title: "BBF Stromversorgung Rückansicht"
date: "2010-06-27T19:34:32"
picture: "DSCN3517.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/27598
- /detailsb79a.html
imported:
- "2019"
_4images_image_id: "27598"
_4images_cat_id: "1987"
_4images_user_id: "184"
_4images_image_date: "2010-06-27T19:34:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27598 -->
