---
layout: "image"
title: "Clip-15x30 bunt.JPG"
date: "2007-01-13T13:13:41"
picture: "Clip-15x30_bunt.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/8379
- /detailsd265.html
imported:
- "2019"
_4images_image_id: "8379"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T13:13:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8379 -->
Diese Teile sind wohl auch mal in einer BIFI-Packung mitgeschwommen?

Die rote Verkleidungsplatte ist ein Standard-Teil (gab es in Ergänzungspäckchen). In gelb und blau allerdings nicht.
