---
layout: "image"
title: "Baustein schwarz mit runden Zapfen"
date: "2009-12-26T19:06:07"
picture: "neuebauteile2.jpg"
weight: "48"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25991
- /details77b3.html
imported:
- "2019"
_4images_image_id: "25991"
_4images_cat_id: "782"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25991 -->
Gibt es den schon im aktuellen Programm??
