---
layout: "image"
title: "was ist das?"
date: "2006-10-03T02:33:08"
picture: "ft_Teile_004.jpg"
weight: "16"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7108
- /details586d.html
imported:
- "2019"
_4images_image_id: "7108"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-10-03T02:33:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7108 -->
Bezeichnung:
Scancon Encoder
2RM30
CH A green
CH B yellow
CH Z gray
VDD brown
GND white