---
layout: "image"
title: "pures Gold.jpg"
date: "2013-07-15T18:22:44"
picture: "IMG_9049.JPG"
weight: "71"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/37164
- /detailsf982.html
imported:
- "2019"
_4images_image_id: "37164"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2013-07-15T18:22:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37164 -->
Leider stand nicht "DEGUSSA" drauf. Aber in güldenem Glanze glänzen die Platten 45x15, die die Spritzmaschine beim fanClub-Tag 2013 aufs Band legte.
