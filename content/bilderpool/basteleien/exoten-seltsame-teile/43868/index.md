---
layout: "image"
title: "Nice 2"
date: "2016-07-11T10:48:52"
picture: "IMG_0358x.jpg"
weight: "75"
konstrukteure: 
- "Jochen Kaupp"
fotografen:
- "Dirk Haizmann (ft)"
uploadBy: "ft"
license: "unknown"
legacy_id:
- /php/details/43868
- /details6aa3-2.html
imported:
- "2019"
_4images_image_id: "43868"
_4images_cat_id: "782"
_4images_user_id: "560"
_4images_image_date: "2016-07-11T10:48:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43868 -->
