---
layout: "image"
title: "Federn"
date: "2011-12-17T10:50:54"
picture: "Federn.jpg"
weight: "68"
konstrukteure: 
- "ft"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/33695
- /details5738.html
imported:
- "2019"
_4images_image_id: "33695"
_4images_cat_id: "782"
_4images_user_id: "184"
_4images_image_date: "2011-12-17T10:50:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33695 -->
Ich habe da diese Federn in meinem Bestand.
Alles sind Federn 26. Wie ihr seht hat die mittlere weniger Windungen, ist aber wesentlich
stärker als die anderen beiden. Außerdem ist ihr Zapfen eckig, nicht rund wie die anderen.
Woher habe ich diese Federn? Wo wurden sie einmal verbaut?
Ich brauche noch mehr von denen.....
Gruß ludger
