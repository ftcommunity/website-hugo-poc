---
layout: "image"
title: "Kardan - Prototyp oder Modding?"
date: "2015-06-25T20:20:34"
picture: "kardanmoddingoderprototyp1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "giliprimero"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- /php/details/41277
- /detailsda63.html
imported:
- "2019"
_4images_image_id: "41277"
_4images_cat_id: "3087"
_4images_user_id: "104"
_4images_image_date: "2015-06-25T20:20:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41277 -->
Das sieht aus, wie wenn es aus zwei 31024 Klemmkupplung 20 gemacht wurde. Das ist aber dermaßen sauber gearbeitet, dass es auch ein echter ganz früher Prototyp eines Kardans sein könnte. Der müsste allerdings aus der Zeit vor dem ersten 31044 Kardangelenk stammen, und das kam gleich mit dem Ur-mot.-1 heraus (also 1968 oder noch früher). Evtl. fand ft (vielleicht Artur Fischer selber? dann heraus, dass die Achsen für den Dauerbetrieb nicht hinreichend in den Kupplungen klemmen, und erschuf daraufhin das Kardan mit den Achsklemmungen?