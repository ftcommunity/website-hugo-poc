---
layout: "comment"
hidden: true
title: "20773"
date: "2015-06-26T07:26:36"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

I think Sweany is right - ich assoziiere das auch sofort als Lego-Teil. Die Achsaufnahmen sind dafür allerdings innen mit dem typischen Kreuz ausgestattet - es sei denn die hat jemand sauber ausgebohrt. Stefan / giliprimero, machst Du uns von den Achsaufnahmen auch noch ein Foto?

Grüße
H.A.R.R.Y.