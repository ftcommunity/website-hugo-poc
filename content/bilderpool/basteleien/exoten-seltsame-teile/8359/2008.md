---
layout: "comment"
hidden: true
title: "2008"
date: "2007-01-15T00:07:39"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo,

ich vermute mal stark, daß die kurzen Lenkklauen zur Verwendung mit der Zahnspurstange (38472) vorgesehen waren. Das bedeutet, daß die Klauen um 1979/80 herum hergestellt worden sein müßten, weil es vorher noch keine Lenkklauen gab und etwa ab 1981 die heute noch bekannten Spustangen mit der roten Kunststoff-/Metall-Lenksäule eingeführt wurden, wie sie auch in den damaligen LKW-Modellen (z.B. Universalfahrzeug) Verwendung fanden. Also solltest Du deine Suche auf Kästen konzentrieren, die zwischen 1978 und 1980 angeboten wurden und entweder universell verwendbar waren oder speziell etwas mit Fahrzeugbau zu tun hatten.

Gruß,
Franz