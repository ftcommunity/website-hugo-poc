---
layout: "image"
title: "Kurze Lenkklaue"
date: "2007-01-10T21:20:46"
picture: "lenkklaue1.jpg"
weight: "21"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "-?-"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- /php/details/8359
- /details4ac4.html
imported:
- "2019"
_4images_image_id: "8359"
_4images_cat_id: "782"
_4images_user_id: "162"
_4images_image_date: "2007-01-10T21:20:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8359 -->
Linker seite ist ein normaler Lenkklaue. Die andere sind viel kurzer.
Jemand eine Ahnung ob diese Teil in einen Baukasten war??
