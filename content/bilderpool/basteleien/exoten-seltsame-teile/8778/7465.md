---
layout: "comment"
hidden: true
title: "7465"
date: "2008-10-04T13:23:46"
uploadBy:
- "charlie2700"
license: "unknown"
imported:
- "2019"
---
Das sind keine abgeschnittenen Zapfen, sondern die Reste von darauf aufgeklebte Zapfen einer Nachbarplatte.
(Abgeschnitten Zapfen haben eine Grösse von 2.5mm und nicht 4mm wie im Bild)