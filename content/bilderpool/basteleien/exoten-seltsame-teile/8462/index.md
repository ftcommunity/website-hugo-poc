---
layout: "image"
title: "Seilwinden-zahnrad sw"
date: "2007-01-14T18:06:57"
picture: "Seilwinden-Zahnrad_sw.jpg"
weight: "24"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
schlagworte: ["m05"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- /php/details/8462
- /details71a1.html
imported:
- "2019"
_4images_image_id: "8462"
_4images_cat_id: "782"
_4images_user_id: "9"
_4images_image_date: "2007-01-14T18:06:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8462 -->
Ich kenne das Zahnrad, das die Seilwinden auf der Achse fixiert, nur in rot. Dieses hier war in einer Zahnrad-Auktion auf ebay, ich habe also keine Ahnung, wo es herkommt. 

Hat übrigens mal jemand ein Getriebe aus m0,5-Zahnrädern gebaut?