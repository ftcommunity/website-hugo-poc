---
layout: "image"
title: "seltsamer Grundbaustein"
date: "2010-12-26T10:55:45"
picture: "seltsamerstein1.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Marcel Endlich (Endlich)"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- /php/details/29545
- /details8a13.html
imported:
- "2019"
_4images_image_id: "29545"
_4images_cat_id: "782"
_4images_user_id: "1162"
_4images_image_date: "2010-12-26T10:55:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29545 -->
In meiner Ebay-Bestellung war dieser seltsame Grundbaustein enthalten. Wurde der so einmal von Fischertechnik produziert oder hat den der Vorbesitzer von mir bearbeiten sprich gesägt, gefeilt?? Dieser Baustein ist auch um ca, um 1/4 kleiner als der normale Baustein 30.
Habt ihr eine Antwort.
