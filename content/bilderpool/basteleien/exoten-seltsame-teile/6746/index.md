---
layout: "image"
title: "diverse Hubgetriebe"
date: "2006-08-28T23:29:11"
picture: "hubgetriebe_.jpg"
weight: "5"
konstrukteure: 
- "ft"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6746
- /details85a9.html
imported:
- "2019"
_4images_image_id: "6746"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6746 -->
Neben dem normalen Hubgetrieb gibt es bei Plan&Simulation das Turbogetriebe mit etwas anderer Übersetzung... kann bildlich nicht aufgenommen werden