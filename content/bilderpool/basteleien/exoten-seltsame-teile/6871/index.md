---
layout: "image"
title: "Bunte Platten.JPG"
date: "2006-09-17T20:24:14"
picture: "Bunte_Platten.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/6871
- /detailsf898.html
imported:
- "2019"
_4images_image_id: "6871"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:24:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6871 -->
Diese Teile sind mir so untergekommen. Die beiden roten Teile links unten sind halbwegs Standard ("halbwegs" weil es die Clips-Platten nicht mehr gibt), aber die anderen sind sicherlich Exoten.
