---
layout: "image"
title: "Zwei Bauplatten 15 x 90 in gelb (Vorderseite)"
date: "2007-02-01T17:26:58"
picture: "DSCN1233.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
fotografen:
- "Ludger Mäsing (ludger-ftc)"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- /php/details/8777
- /detailsd20a.html
imported:
- "2019"
_4images_image_id: "8777"
_4images_cat_id: "782"
_4images_user_id: "184"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8777 -->
nix besonderes, aber die Rückseite hat es in sich...
