---
layout: "image"
title: "Hubgetriebe mit 4mm Ansatz"
date: "2006-08-28T23:29:11"
picture: "Hubgetriebe_mit_4mm_ansatz_005.jpg"
weight: "6"
konstrukteure: 
- "ft"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6747
- /details78a4.html
imported:
- "2019"
_4images_image_id: "6747"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6747 -->
