---
layout: "image"
title: "BiFi-TitelSeite 'Großpackung'"
date: "2007-01-16T21:01:30"
picture: "Bifi008.jpg"
weight: "26"
konstrukteure: 
- "ft"
fotografen:
- "ft-Bifi"
schlagworte: ["Bifi", "Kleinmodelle"]
uploadBy: "Svefisch"
license: "unknown"
legacy_id:
- /php/details/8488
- /details31ec.html
imported:
- "2019"
_4images_image_id: "8488"
_4images_cat_id: "782"
_4images_user_id: "534"
_4images_image_date: "2007-01-16T21:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8488 -->
bei diesem Bild handelt es sich um die Titelseite einer Bifi-Modellzusamenstellung. Das Blatt ist für eine ngesamten Scan leider etwas breit, der gesamte Inhalt mit den Bauanleitungen folgt etwas später.