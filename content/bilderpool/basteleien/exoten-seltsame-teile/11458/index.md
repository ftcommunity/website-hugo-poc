---
layout: "image"
title: "grünliche Statikteile"
date: "2007-09-09T23:24:15"
picture: "grnliche_Statik.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Falkenberg (speedy68)"
schlagworte: ["Fischertechnik", "Sonderteile"]
schlagworte: ["Fischertechnik", "Sonderteile"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- /php/details/11458
- /details9d5e.html
imported:
- "2019"
_4images_image_id: "11458"
_4images_cat_id: "782"
_4images_user_id: "409"
_4images_image_date: "2007-09-09T23:24:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11458 -->
Habe die Teile mal bei ebay erworben. Zum Farbvergleich noch ein roter und gelber Winkelträger 60.
