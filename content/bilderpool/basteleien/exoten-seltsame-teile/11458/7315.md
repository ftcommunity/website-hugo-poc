---
layout: "comment"
hidden: true
title: "7315"
date: "2008-09-26T18:45:21"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Beim Wechsel der Granulatfarbe wird angespritzt bis das vorangegangene Granulat aus der Maschine ist. Während dieser Phase sind Teile mit falschen oder Mischfarben möglich. Wird vor dem Granulatwechsel das Spritzgusswerkzeug für eine anderes Teil bereits gewechselt, fallen solche Flaschfarbenteile durchaus  an. Also zerbrecht euch nicht so sehr die Köpfe. Da gibt es immer Leute, die dann diese Teile nach draussen verschleppen.