---
layout: "comment"
hidden: true
title: "3954"
date: "2007-09-11T01:13:34"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Vermutlich nicht. Das Wort, das du Ma-gi-er unterstellst, schreibt sich "eklig". 

Die Farben der Statikteile sind tatsächlich ein bisschen merkwürdig ... auch wenn ich den Farben von Digitalfotos nie so richtig traue (Weißabgleich bei Mischlicht, Kalibrierung und Farbraum des Monitors).