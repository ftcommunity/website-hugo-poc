---
layout: "comment"
hidden: true
title: "4586"
date: "2007-11-13T17:58:13"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
In blau habe ich (außer den eher üblichen Teilen) eine einsame S-Strebe 75 mit Loch:
http://www.ftcommunity.de/details.php?image_id=1815
Die ist aber tief-knallblau, nicht türkis oder (Siemens-)"Petrol"farben wie die Sachen hier.