---
layout: "image"
title: "Transparente S-Riegel"
date: "2014-03-07T10:45:14"
picture: "transparentesriegel1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/38434
- /details115f.html
imported:
- "2019"
_4images_image_id: "38434"
_4images_cat_id: "2863"
_4images_user_id: "968"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38434 -->
Hier mal wieder ein Kuriosum.
Diese Woche erwarb ich mittels eines Kleinanzeigenportals einen sehr gepflegtenHobby-S Kasten.
Drinnen fand ich dann obig abgebildete transparenten S-Riegel. 

Was es nicht alles gibt :)

Gruß
Markus Wolf