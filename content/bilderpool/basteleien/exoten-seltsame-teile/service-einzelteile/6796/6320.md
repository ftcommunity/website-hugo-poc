---
layout: "comment"
hidden: true
title: "6320"
date: "2008-04-24T21:37:36"
uploadBy:
- "snugma@gmx.de"
license: "unknown"
imported:
- "2019"
---
Da ist wohl was in durcheinander gekommen .... ;-)

Natürlich waren die beiden grauen Kästen die ersten Kästen, denn sie enthielten NUR graue Bauteile und es gab sie schon in den 70er Jahren als SERVICE-1 und SERVICE-2 Kasten (1000er Kästen mit anderer Beschriftung). Da waren dann auch die Ersatzteile aus den Hobby/uT1-4 drin bzw für die EM und EC Kästen. Die Metallkästen kamen in den 80er Jahren, das sieht man schon am ft-Schriftzug auf dem Deckel, das ist der typische fischertechnik-80er-Jahre-Schriftzug.