---
layout: "comment"
hidden: true
title: "11794"
date: "2010-07-03T20:42:07"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
der Thomas hat 100%ig Recht. Wird Zeit, dass ich mich damit auch wieder mal beschäftige. Was man ab 10 Jahre aufwärts so alles "vergessen" kann? Natürlich hat der ML auch diese Stromschienen. Mikrofon bzw. Lautsprecher sind sind aber aus den nun aufgefrischten Gründen nicht an diese Schienen (hier beide nur Durchschleifer) angeschlossen. Es wundert mich nun, warum das Teil so unbekannt zu sein scheint und warum sich auch keine Art-Nr. finden lässt.
Gruss Ingo