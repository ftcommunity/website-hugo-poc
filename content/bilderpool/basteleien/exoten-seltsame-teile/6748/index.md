---
layout: "image"
title: "Impulsrad für Initiatoren"
date: "2006-08-28T23:29:11"
picture: "impulsrad_01.jpg"
weight: "7"
konstrukteure: 
- "ft"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6748
- /details4fd9.html
imported:
- "2019"
_4images_image_id: "6748"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6748 -->
