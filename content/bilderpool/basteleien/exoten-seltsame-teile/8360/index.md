---
layout: "image"
title: "Klemmbuchse 10 doppelt"
date: "2007-01-10T23:05:29"
picture: "Klemmbuchse01b.jpg"
weight: "22"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- /php/details/8360
- /detailse974.html
imported:
- "2019"
_4images_image_id: "8360"
_4images_cat_id: "782"
_4images_user_id: "488"
_4images_image_date: "2007-01-10T23:05:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8360 -->
Habe ich durch Zufall in meinem Bestand gefunden. Eine Klemmbuchse 10 mit Klemmring auf beiden Seiten (im Hintergrund das Spiegelbild der Rückseite).
Fehlproduktion oder Sonderteil?
