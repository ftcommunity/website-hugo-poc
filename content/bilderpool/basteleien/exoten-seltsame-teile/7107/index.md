---
layout: "image"
title: "was ist das ?"
date: "2006-10-03T02:33:08"
picture: "ft_Teile_003.jpg"
weight: "15"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/7107
- /detailsb68d.html
imported:
- "2019"
_4images_image_id: "7107"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-10-03T02:33:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7107 -->
Ich habe das Teil mal bei eBay erstanden. Ich weiß, daß Staudinger dieses Teil bei Bändern mitlaufen läßt. Ist das eine Art Schrittzähler, im umgekehrten Sinne Schrittmotor ?