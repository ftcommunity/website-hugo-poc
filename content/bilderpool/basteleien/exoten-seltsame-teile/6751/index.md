---
layout: "image"
title: "Taktplatten für Infrarotabtastung"
date: "2006-08-28T23:29:11"
picture: "Taktplatten_fr_Infrarot.jpg"
weight: "10"
konstrukteure: 
- "ft"
fotografen:
- "Micha Etz"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- /php/details/6751
- /detailsbdfd-2.html
imported:
- "2019"
_4images_image_id: "6751"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6751 -->
zB für die Infrarotlichtschranke von Trainingsrobot