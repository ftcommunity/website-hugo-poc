---
layout: "image"
title: "V-Bauplatte mit Klipsen 7,5 x 60"
date: "2016-09-11T20:18:37"
picture: "image.jpeg"
weight: "76"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["V-Bauplatte"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/44357
- /details443c.html
imported:
- "2019"
_4images_image_id: "44357"
_4images_cat_id: "782"
_4images_user_id: "579"
_4images_image_date: "2016-09-11T20:18:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44357 -->
Kennt jemand die Bauteil-Nr. oder weiß, in welchem Kasten die waren? 
Ich hab sie weder in der ft-Datenbank, noch bei fischerfriendsman, bei Knobloch gefunden...