---
layout: "image"
title: "Sonderproduktion"
date: "2009-02-20T14:19:07"
picture: "sonderproduktion3.jpg"
weight: "46"
konstrukteure: 
- "ft"
fotografen:
- "Dirk Kutsch (Guilligan)"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- /php/details/17468
- /details6f68-2.html
imported:
- "2019"
_4images_image_id: "17468"
_4images_cat_id: "782"
_4images_user_id: "389"
_4images_image_date: "2009-02-20T14:19:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17468 -->
Dieses Teil habe ich auf der didacta 2009 in Hannover entdeckt.
