---
layout: "image"
title: "große Kappe mit Fischertechnik-Zeichen"
date: "2008-08-21T11:08:28"
picture: "Kappe_markiert_800.jpg"
weight: "39"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Kappe", "Helm", "Hut", "groß"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/15063
- /detailsb6f3-2.html
imported:
- "2019"
_4images_image_id: "15063"
_4images_cat_id: "782"
_4images_user_id: "579"
_4images_image_date: "2008-08-21T11:08:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15063 -->
Hab zwei Kappen gefunden (aus einer ebay-Auktion), die deutlich größer sind, als meine bisherigen. Zu den Fischertechnik-Männchen passen sie daher nicht. Es ist aber ein Fischertechnik-Zeichen auf der Unterseite, das ich auf dem Foto mit einer weißen Ellipse eingekreist habe.

Frage: weiß jemand, wozu diese Kappen passen bzw. gedacht waren?

Vielleicht war es ja nur eine Fischer-Produktion für anderes Spielzeug...
