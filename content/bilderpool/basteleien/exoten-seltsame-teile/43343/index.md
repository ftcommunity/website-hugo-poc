---
layout: "image"
title: "Solar 2"
date: "2016-05-07T11:30:24"
picture: "Solar_2_kl.jpg"
weight: "74"
konstrukteure: 
- "Lothar Vogt (Pilami)"
fotografen:
- "Lothar Vogt (Pilami)"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- /php/details/43343
- /details3093-3.html
imported:
- "2019"
_4images_image_id: "43343"
_4images_cat_id: "782"
_4images_user_id: "10"
_4images_image_date: "2016-05-07T11:30:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43343 -->
Ein mir unbekanntes Teil.
Solarzelle mit Motor und Adapterbaustein
