﻿---
layout: "image"
title: "Hydraulik-Werkstattwagen, gemoddete Teile"
date: 2022-01-18T11:08:41+01:00
picture: "DSC01318_crop.JPG"
weight: "3"
konstrukteure: 
- "Stefan Hamm"
fotografen:
- "Stefan Hamm"
schlagworte: ["Rückschlagventil gemoddet", "Handventil gemoddet"]
uploadBy: "Website-Team"
license: "unknown"
---

Benötigte gemoddete Teile: 1. Rückschlagventil mit an Saugöffnung angeklebtem Schlauchanschluss weiß (35328); 2. Handventil mit am Abluftanschluss angeklebtem Schlauchanschluss grau (121640), alternativ kann auch das neue Handventil aus dem Kasten "Strong Pneumatics" verwendet werden. Beides Modding ist schon an anderer Stelle für andere Zwecke beschrieben worden und keine Idee von mir.
