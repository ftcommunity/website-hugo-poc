---
layout: "image"
title: "Hydraulik-Werkstattwagen, Übersicht"
date: 2022-01-18T11:08:43+01:00
picture: "DSC01316_crop.JPG"
weight: "1"
konstrukteure: 
- "Stefan Hamm"
fotografen:
- "Stefan Hamm"
schlagworte: ["Pneumatik", "Hydraulik"]
uploadBy: "Website-Team"
license: "unknown"
---

Pneumatik-Werkstattwagen aus FT Profi Pneumatic Power auf Hydraulik umgerüstet; zum Wassereinfüllen Pumpzylinder unter Handhebel Luftblasen-frei vollständig mit Wasser füllen (Modell dazu auf den Kopf drehen, Rückschlagventil entfernen)