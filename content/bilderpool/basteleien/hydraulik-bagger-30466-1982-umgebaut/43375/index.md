---
layout: "image"
title: "Nehmer-Zylinder"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/43375
- /detailsf23b.html
imported:
- "2019"
_4images_image_id: "43375"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43375 -->
hier ist der Zylinder mittels eines Gelenkes angeschlagen