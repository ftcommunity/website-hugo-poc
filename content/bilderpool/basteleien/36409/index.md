---
layout: "image"
title: "Exzenter mit Achsen"
date: "2013-01-05T15:41:06"
picture: "exzenter2.jpg"
weight: "31"
konstrukteure: 
- "Jan Werner (werner)"
fotografen:
- "Jan Werner (werner)"
uploadBy: "werner"
license: "unknown"
legacy_id:
- /php/details/36409
- /details7040.html
imported:
- "2019"
_4images_image_id: "36409"
_4images_cat_id: "463"
_4images_user_id: "1196"
_4images_image_date: "2013-01-05T15:41:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36409 -->
Mit den Madenschrauben lassen sich zwei Achsen einspannen
Dadurch ergibt sich eine Kurbel, die z.B. für einen Druckluftmotor sehr praktisch ist
