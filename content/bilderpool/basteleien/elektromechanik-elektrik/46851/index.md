---
layout: "image"
title: "Servo selector ball track."
date: "2017-10-29T15:22:10"
picture: "P1017675_76_800.jpg"
weight: "28"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- /php/details/46851
- /details260b-2.html
imported:
- "2019"
_4images_image_id: "46851"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-10-29T15:22:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46851 -->
Row servo selector.
Here you will see the 2 positions of the servo.  
The picture at the top:  to push the ball to the right
The picture at the bottom: to allow free passage of the ball.
This way you can control the 2 outputs and make a selection. Made for the 2-way outputs on my Rad, one at the top and one at the back side.