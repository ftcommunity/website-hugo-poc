---
layout: "image"
title: "Aderendhülsen als Steckerersatz 1"
date: "2013-09-06T01:05:26"
picture: "Aderendhlsen_als_Steckerersatz_1.jpg"
weight: "21"
konstrukteure: 
- "tim4441"
fotografen:
- "tim4441"
schlagworte: ["Aderendhülse", "Steckerersatz"]
uploadBy: "tim4441"
license: "unknown"
legacy_id:
- /php/details/37299
- /details0f84.html
imported:
- "2019"
_4images_image_id: "37299"
_4images_cat_id: "467"
_4images_user_id: "1121"
_4images_image_date: "2013-09-06T01:05:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37299 -->
Wenn  man keinen Platz für Stecker mehr hat, kann man sich anstelle von Steckern Aderendhülsen an die Enden quetschen. Ich nehme dazu 1.5er Endhülsen ohne Schutzkragen.
