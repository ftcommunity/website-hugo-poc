---
layout: "image"
title: "Klebevorrichtung"
date: "2017-01-15T12:58:29"
picture: "IMG_1901.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45036
- /details0f44.html
imported:
- "2019"
_4images_image_id: "45036"
_4images_cat_id: "3354"
_4images_user_id: "1359"
_4images_image_date: "2017-01-15T12:58:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45036 -->
