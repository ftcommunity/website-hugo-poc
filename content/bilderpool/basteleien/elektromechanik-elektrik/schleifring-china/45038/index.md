---
layout: "image"
title: "Klebevorrichtung"
date: "2017-01-15T12:58:29"
picture: "IMG_1903.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/45038
- /details0c17.html
imported:
- "2019"
_4images_image_id: "45038"
_4images_cat_id: "3354"
_4images_user_id: "1359"
_4images_image_date: "2017-01-15T12:58:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45038 -->
