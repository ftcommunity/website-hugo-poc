---
layout: "image"
title: "Schalter im Potihalter"
date: "2018-05-12T10:30:23"
picture: "schalterhalter1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /php/details/47637
- /detailsca67.html
imported:
- "2019"
_4images_image_id: "47637"
_4images_cat_id: "3512"
_4images_user_id: "1557"
_4images_image_date: "2018-05-12T10:30:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47637 -->
Was einem Poti recht ist, ist einem Schalter mit gleichem Flanschmaß billig: Der Potihalter hält sie alle. Die Deckscheibe passt mit der Arretiernase genau in die Nut des Potihalters. Die Kontermutter des Schalters wird mit einer Zange (oder einem passendem Schraubenschlüssel) angezogen.

Wer den Schaltknebel gerne 90° gedreht eingebaut hätte, nimmt stattdessen einen Potihalter 32116. Das ist übrigens der Unterschied zwischen dem hier gezeigten 136650 und dem 32116 - die Richtung der Nut für's Poti.