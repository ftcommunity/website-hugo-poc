---
layout: "image"
title: "Detailansicht"
date: "2009-10-28T09:50:20"
picture: "variante2.jpg"
weight: "4"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/25576
- /details5ad0-2.html
imported:
- "2019"
_4images_image_id: "25576"
_4images_cat_id: "1796"
_4images_user_id: "373"
_4images_image_date: "2009-10-28T09:50:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25576 -->
