---
layout: "image"
title: "Stromabnahme Variante 2"
date: "2009-10-28T09:50:20"
picture: "variante1.jpg"
weight: "3"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/25575
- /details3875-2.html
imported:
- "2019"
_4images_image_id: "25575"
_4images_cat_id: "1796"
_4images_user_id: "373"
_4images_image_date: "2009-10-28T09:50:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25575 -->
Schon deutlich stabiler und zuverlässiger - nahezu ohne Unterbrechungen. Noch etwas mehr Gewicht auf die linke Achse und schon sollte das kein Problem mehr darstellen.