---
layout: "image"
title: "TST-Spurkränze eingebaut"
date: "2009-10-27T14:02:30"
picture: "spurkranzabnahme1.jpg"
weight: "2"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/25574
- /details2fdf-2.html
imported:
- "2019"
_4images_image_id: "25574"
_4images_cat_id: "1796"
_4images_user_id: "373"
_4images_image_date: "2009-10-27T14:02:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25574 -->
Hier mal die neuen Spurkränze (Beta-Version) eingebaut und mit einer ersten aber unschönen Lösung zur Stromabnahme. Mir gefällt das so hinten und vorne nicht. Aber wenigstens funktioniert es einigermaßen. 
Hin und wieder gibt es die für Modellbahnen normalen Aussetzer, ich denke das dürfte sich mit 4 stromabnehmenden Rädern lösen lassen.
Der Draht ist einfach eingeklemmt zwischen den Bausteinen und in Form gebogen.