---
layout: "image"
title: "Joystik4"
date: "2009-12-26T19:06:07"
picture: "joystik4.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25979
- /details9394.html
imported:
- "2019"
_4images_image_id: "25979"
_4images_cat_id: "1828"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25979 -->
Hier das Innenleben ohne Verkabellung
