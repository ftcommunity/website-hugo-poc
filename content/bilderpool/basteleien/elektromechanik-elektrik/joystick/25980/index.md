---
layout: "image"
title: "Joystik5"
date: "2009-12-26T19:06:07"
picture: "joystik5.jpg"
weight: "5"
konstrukteure: 
- "apem"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25980
- /details22af.html
imported:
- "2019"
_4images_image_id: "25980"
_4images_cat_id: "1828"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25980 -->
Hier ein Bild des Joysticks.
http://www.apemswitches.be/Low-Profile-Switch-Joysticks-v1-d-117.html
