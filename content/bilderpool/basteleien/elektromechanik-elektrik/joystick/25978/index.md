---
layout: "image"
title: "Joystik3"
date: "2009-12-26T19:06:07"
picture: "joystik3.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/25978
- /detailscc51.html
imported:
- "2019"
_4images_image_id: "25978"
_4images_cat_id: "1828"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25978 -->
Hier die Grundplatte mit den Anschlüssen.
