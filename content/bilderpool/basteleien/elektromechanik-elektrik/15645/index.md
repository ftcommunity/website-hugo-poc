---
layout: "image"
title: "roter Leuchtstein 3"
date: "2008-09-27T21:15:12"
picture: "dsc01878_resize.jpg"
weight: "16"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/15645
- /detailscc44.html
imported:
- "2019"
_4images_image_id: "15645"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15645 -->
so sollte das sein ft :)
