---
layout: "image"
title: "Power-Adapter 2"
date: "2006-11-26T23:08:57"
picture: "dsc00958_resize.jpg"
weight: "6"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/7632
- /details1616.html
imported:
- "2019"
_4images_image_id: "7632"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7632 -->
