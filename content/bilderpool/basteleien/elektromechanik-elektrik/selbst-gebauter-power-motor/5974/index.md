---
layout: "image"
title: "Power-Motor"
date: "2006-03-27T19:52:07"
picture: "Fischertechnik-Bilder_019.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- /php/details/5974
- /detailsba15.html
imported:
- "2019"
_4images_image_id: "5974"
_4images_cat_id: "518"
_4images_user_id: "420"
_4images_image_date: "2006-03-27T19:52:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5974 -->
Den Motor an sich hab´ich aus einem 
alten Kasettenrekorder ausgebaut.
Dann habe ich eine ft-stange(ca.2cm lang)
in der mitte durchgesägt und mit einem 
dünnen Bohrer reingebohrt. Nun habe ich das ganze Teil
mit Heißkleber vorn am Motor angebracht und
den Motor auf eine(n)...(was weiß ich wie das 
heißt) draufgeklebt.