---
layout: "image"
title: "Adapter für den Fingermotor"
date: "2006-11-14T00:06:08"
picture: "motor1.jpg"
weight: "4"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/7455
- /details3cf8.html
imported:
- "2019"
_4images_image_id: "7455"
_4images_cat_id: "467"
_4images_user_id: "6"
_4images_image_date: "2006-11-14T00:06:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7455 -->
Ich hab mich mal drangemacht und eienen Adapter gedreht. Der Motor hat eine 3mm Welle mit abgeflachter Seite und da sitzt die Madenschraube drauf.
