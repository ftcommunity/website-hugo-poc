---
layout: "image"
title: "Kästchen am Sensor"
date: "2006-10-23T17:47:14"
picture: "Alu-folien_Sensor_003.jpg"
weight: "3"
konstrukteure: 
- "Frederik Vormann (Fredy)"
fotografen:
- "Frederik Vormann (Fredy)"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- /php/details/7214
- /detailsedd3.html
imported:
- "2019"
_4images_image_id: "7214"
_4images_cat_id: "692"
_4images_user_id: "453"
_4images_image_date: "2006-10-23T17:47:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7214 -->
Hier bekommt der Sensor ein Signal.
