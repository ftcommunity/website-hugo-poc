---
layout: "image"
title: "Trimmer / Potentiometer eingebaut"
date: "2014-06-13T14:52:54"
picture: "Trimm_1.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Trimmer", "Potentiometer", "Drehgeber"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38947
- /details47ad.html
imported:
- "2019"
_4images_image_id: "38947"
_4images_cat_id: "2914"
_4images_user_id: "1729"
_4images_image_date: "2014-06-13T14:52:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38947 -->
passt perfekt ins Fischertechnik Raster. kann man bündig einbauen; ohne Werkzeug, ohne Kleber
