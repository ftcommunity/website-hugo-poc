---
layout: "image"
title: "drei Lichter"
date: "2010-09-14T20:01:13"
picture: "flipper06.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28117
- /details56d8.html
imported:
- "2019"
_4images_image_id: "28117"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28117 -->
Trifft man eines der Ziele geht die entsprechende LED an, bei einem erneuten Treffer natürlich wieder aus. Leuchten alle 3 LEDs in einer Reihe so bekommt man einen Bonus.