---
layout: "image"
title: "Gesamtübersicht"
date: "2010-09-14T20:01:12"
picture: "flipper01.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28112
- /details1d43.html
imported:
- "2019"
_4images_image_id: "28112"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28112 -->
Insgesamt habe ich 14 LEDs und 5 Lampen verbaut. Die roten LEDs dienen dazu dem Spieler zu zeigen wo er als nächstes treffen muss um die Mission zu beenden bzw. anzufangen. Die gelbe LED bei dem Center Post zeigen dem Spieler an wann der Stöpsel wieder runter fährt indem sie immer schneller blinkt bis der Center Post schließlich im Boden verschwindet.