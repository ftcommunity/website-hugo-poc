---
layout: "image"
title: "Aktivierung Center Post"
date: "2010-09-14T20:01:13"
picture: "flipper03.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28114
- /details1913.html
imported:
- "2019"
_4images_image_id: "28114"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28114 -->
Das ist das ,,versteckte´´ Ziel das bei einem Treffer den Center Post ausfährt.