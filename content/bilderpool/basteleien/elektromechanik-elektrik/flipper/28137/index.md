---
layout: "image"
title: "Zylinder"
date: "2010-09-14T20:01:16"
picture: "flipper26.jpg"
weight: "26"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28137
- /detailsa7cd.html
imported:
- "2019"
_4images_image_id: "28137"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28137 -->
Man sieht den Zylinder der die Bälle auf die Abschussrampe legt.