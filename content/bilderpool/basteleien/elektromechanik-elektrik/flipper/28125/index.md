---
layout: "image"
title: "Kugelmagazin"
date: "2010-09-14T20:01:16"
picture: "flipper14.jpg"
weight: "14"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28125
- /detailsf12d.html
imported:
- "2019"
_4images_image_id: "28125"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28125 -->
Hier werden drei Kugeln gelagert und bei bedarf auf die Abschussrampe gelegt.