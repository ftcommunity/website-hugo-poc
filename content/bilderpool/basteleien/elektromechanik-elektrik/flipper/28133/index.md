---
layout: "image"
title: "Flipperfinger in Normalstellung"
date: "2010-09-14T20:01:16"
picture: "flipper22.jpg"
weight: "22"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28133
- /details6dd9.html
imported:
- "2019"
_4images_image_id: "28133"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28133 -->
