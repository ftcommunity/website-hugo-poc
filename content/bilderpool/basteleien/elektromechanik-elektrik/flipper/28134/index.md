---
layout: "image"
title: "Flipperfinger gedrückt"
date: "2010-09-14T20:01:16"
picture: "flipper23.jpg"
weight: "23"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28134
- /details9b96-2.html
imported:
- "2019"
_4images_image_id: "28134"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28134 -->
