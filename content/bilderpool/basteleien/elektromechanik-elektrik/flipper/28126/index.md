---
layout: "image"
title: "Kugelmagazin ohne Abdeckung"
date: "2010-09-14T20:01:16"
picture: "flipper15.jpg"
weight: "15"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28126
- /details8352.html
imported:
- "2019"
_4images_image_id: "28126"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28126 -->
Die Kugeln werden durch einen Pneumatikzylinder auf die Abschussrampe befördert.