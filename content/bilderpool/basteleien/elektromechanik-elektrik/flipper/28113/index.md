---
layout: "image"
title: "Gesamtübersicht 2"
date: "2010-09-14T20:01:13"
picture: "flipper02.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- /php/details/28113
- /details27ae.html
imported:
- "2019"
_4images_image_id: "28113"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28113 -->
Ohne Beleuchtung sieht der Flipper ziemlich tot aus.