---
layout: "image"
title: "Akkuhalter für 7 Mignonzellen"
date: "2015-01-07T19:06:05"
picture: "akkuhalter1.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- /php/details/40195
- /detailsab07-2.html
imported:
- "2019"
_4images_image_id: "40195"
_4images_cat_id: "1444"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T19:06:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40195 -->
Ich finde den Akkuhalter von Martin Giger bestechend, weil man einzelne Zellen einsetzen kann. Dadurch kann man sie auch einzeln laden und auswechseln, was die Lebenszeit deutlich erhöhen dürfte. Die hier gezeigte Variante ist nicht nur von den Abmessungen sondern wegen der Anzahl der Zellen von 7 auch von der Spannung her kompatibel zum ft-Akkupack 8,4V.