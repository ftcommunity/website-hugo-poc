---
layout: "image"
title: "Akkukasten"
date: "2008-10-05T17:50:49"
picture: "selbstgebauterakkukasten1.jpg"
weight: "1"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
fotografen:
- "Martin Giger (Ma-gi-er)"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- /php/details/15820
- /details5d21-2.html
imported:
- "2019"
_4images_image_id: "15820"
_4images_cat_id: "1444"
_4images_user_id: "445"
_4images_image_date: "2008-10-05T17:50:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15820 -->
Die Metallteile sind aus ehemaligen MickyMaus Magazin Extras. Die beiden Seitölichen Teile sind herausnehmbar (nächste Bilder). In den "Akkupack" passen acht Akkus.
