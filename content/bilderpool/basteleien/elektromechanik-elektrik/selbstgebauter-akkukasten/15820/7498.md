---
layout: "comment"
hidden: true
title: "7498"
date: "2008-10-06T17:34:00"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Die Reserve am Ende bedeutet nur, dass bereits eine Zelle umgepolt wird.
Wenn man dann nicht aufhört ist die Zelle hin.

Besonders fatal ist das beim Originalakku.
Wer versucht bis zur letzten Umdrehung weiter zu machen hat nicht lange Freude an dem Pack.