---
layout: "image"
title: "IR-Empfänger 2"
date: "2009-01-28T14:43:28"
picture: "schaltplanteile03.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17172
- /details7491.html
imported:
- "2019"
_4images_image_id: "17172"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17172 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.