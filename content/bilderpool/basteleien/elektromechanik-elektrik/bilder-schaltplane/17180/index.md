---
layout: "image"
title: "Powermotor"
date: "2009-01-28T14:43:49"
picture: "schaltplanteile11.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17180
- /details9921.html
imported:
- "2019"
_4images_image_id: "17180"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:49"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17180 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.