---
layout: "image"
title: "Akku"
date: "2009-01-28T14:43:49"
picture: "schaltplanteile12.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17181
- /details3248.html
imported:
- "2019"
_4images_image_id: "17181"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:49"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17181 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.