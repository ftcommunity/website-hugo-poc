---
layout: "comment"
hidden: true
title: "8357"
date: "2009-01-28T15:00:19"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@sebastian g.
Hallo Sebastian,
das ist allerdings mühsam, wenn du dir die einzelnen fischertechnik-Schaltelemente selbst erstellst. Diese Elemente sind aber im fischertechnik designer enthalten. Es ist aber eine 3D-Anwendung. Mit etwas Übung kannst du damit nicht nur 3D- sondern auch 2D-Pläne erstellen. Schau dazu mal unter www.fischertechnik-designer.de.
Gruss Udo2