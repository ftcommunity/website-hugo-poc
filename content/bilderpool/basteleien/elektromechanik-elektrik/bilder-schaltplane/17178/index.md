---
layout: "image"
title: "Summer"
date: "2009-01-28T14:43:28"
picture: "schaltplanteile09.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17178
- /details9803.html
imported:
- "2019"
_4images_image_id: "17178"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17178 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.