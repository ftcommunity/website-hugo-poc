---
layout: "image"
title: "Taster"
date: "2009-01-28T14:43:28"
picture: "schaltplanteile08.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- /php/details/17177
- /details6246.html
imported:
- "2019"
_4images_image_id: "17177"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17177 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.