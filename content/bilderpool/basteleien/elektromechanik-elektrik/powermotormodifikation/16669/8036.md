---
layout: "comment"
hidden: true
title: "8036"
date: "2008-12-20T12:59:46"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Jan und andere FT-Freunden,

Ist die Signal-Frequenz mit diesem schwarze 8:1-Getriebe nicht zu hoch für ein normales Betrieb mit einer FT-RoboPro-interface ?.........

9V:  ca. 850 rpm x 40 Zahne (?) = 34000x/min  = 566x/sec

Gruss,

Peter Damen
Poederoyen NL