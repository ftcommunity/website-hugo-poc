---
layout: "image"
title: "Geöffnet"
date: "2008-12-19T14:14:51"
picture: "motor2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- /php/details/16669
- /detailsfa5e.html
imported:
- "2019"
_4images_image_id: "16669"
_4images_cat_id: "1509"
_4images_user_id: "521"
_4images_image_date: "2008-12-19T14:14:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16669 -->
Hier die Innenansicht. Die Lichtschranke war mal ein CNY37 bei dem ich das Gehäuse abgenommen habe. Störlicht gibt es im Getriebe ja nicht. Die Lichtschranke wird von den Zähnen des grßen Zahnrades unterbrochen.