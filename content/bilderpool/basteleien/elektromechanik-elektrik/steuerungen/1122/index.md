---
layout: "image"
title: "fertige Steuerung,seitlich"
date: "2003-05-11T18:24:47"
picture: "seitlich.jpg"
weight: "5"
konstrukteure: 
- "Michael Orlik (Sannchen90)"
fotografen:
- "Michael Orlik (Sannchen90)"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- /php/details/1122
- /detailsd1e0.html
imported:
- "2019"
_4images_image_id: "1122"
_4images_cat_id: "114"
_4images_user_id: "6"
_4images_image_date: "2003-05-11T18:24:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1122 -->
