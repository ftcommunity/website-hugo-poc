---
layout: "image"
title: "Kleine Handsteuerung f.2 Motoren"
date: "2005-02-19T16:11:16"
picture: "Gesamt.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3645
- /details221b.html
imported:
- "2019"
_4images_image_id: "3645"
_4images_cat_id: "114"
_4images_user_id: "103"
_4images_image_date: "2005-02-19T16:11:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3645 -->
Kleine Bedieneinheit für 2 Motoren ,ermöglicht Polwendeschaltung zum Betrieb zweier FT-Mot´s mit jeweils Rechts-Linkslauf.Ich habe die Einheit absichtlich sehr klein gehalten damit man sie auch direkt in ein Modell integrieren kann.Der Rahmen aus den grauen Steinen kann dann wegfallen.Diesen verwende ich nur bei Einsatz als Kabelfernsteuerung und baue dann noch eine kleine Zugentlastung dran.Eignet sich prima zum Raupenantrieb ,Kransteuerung o.Ä.Ich habe mehrere dieser Module gebaut um sie bei Bedarf(Großkran z.B.)aneinanderreihen zu können.