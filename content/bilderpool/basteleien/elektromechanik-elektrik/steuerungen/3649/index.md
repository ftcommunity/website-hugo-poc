---
layout: "image"
title: "Schaltplan(für jeweils 1 Taster)"
date: "2005-02-19T16:11:16"
picture: "Schaltplan2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- /php/details/3649
- /details56e0.html
imported:
- "2019"
_4images_image_id: "3649"
_4images_cat_id: "114"
_4images_user_id: "103"
_4images_image_date: "2005-02-19T16:11:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3649 -->
Das Verdrahtungsschema.Die äußeren Pins  am Schalter müssen überkreuz verdrahtet werden um die Polwendefunktion zu erreichen.Dadurch daß ich die Spannungsversorgung beider Taster zusammengelegt habe brauche ich insgesamt nur 6 Buchsen:die mittleren zur Spannungsversorgung,die äußeren jeweils als Motorausgang .