---
layout: "image"
title: "Robo Control 2"
date: "2017-01-25T17:55:42"
picture: "IMG_2289_400.jpg"
weight: "14"
konstrukteure: 
- "DS"
fotografen:
- "DS"
uploadBy: "dieschra"
license: "unknown"
legacy_id:
- /php/details/45082
- /details455f-2.html
imported:
- "2019"
_4images_image_id: "45082"
_4images_cat_id: "114"
_4images_user_id: "2704"
_4images_image_date: "2017-01-25T17:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45082 -->
Smartphone steuert Robo Mobil über Bluetooth