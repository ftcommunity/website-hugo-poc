---
layout: "image"
title: "Power-Adapter 4"
date: "2006-11-26T23:08:57"
picture: "dsc00963_resize.jpg"
weight: "8"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/7634
- /details9998.html
imported:
- "2019"
_4images_image_id: "7634"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7634 -->
