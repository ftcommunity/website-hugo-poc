---
layout: "image"
title: "Schleifring Draufsicht"
date: "2014-02-08T22:06:50"
picture: "Schleifring_von_oben.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38211
- /details9ee3.html
imported:
- "2019"
_4images_image_id: "38211"
_4images_cat_id: "2843"
_4images_user_id: "1729"
_4images_image_date: "2014-02-08T22:06:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38211 -->
Der in den Drehkranz eingebaute Schleifring in der Draufsicht