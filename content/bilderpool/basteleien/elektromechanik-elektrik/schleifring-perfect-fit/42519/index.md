---
layout: "image"
title: "Drehkranz mit 6 pol Schleifring und farbliche  Steckbuchsen"
date: "2015-12-15T18:24:09"
picture: "IMG_5153.jpg"
weight: "5"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- /php/details/42519
- /details0162.html
imported:
- "2019"
_4images_image_id: "42519"
_4images_cat_id: "2843"
_4images_user_id: "2496"
_4images_image_date: "2015-12-15T18:24:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42519 -->
Der Schleifring ist von Fischerfriendsman.die Halter für die Steckbuchsen sind aus dem Kasten Electronics.