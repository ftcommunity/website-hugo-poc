---
layout: "image"
title: "Schleifring von unten"
date: "2014-02-08T22:06:50"
picture: "Schleifing_von_unten.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38212
- /detailsb0a6.html
imported:
- "2019"
_4images_image_id: "38212"
_4images_cat_id: "2843"
_4images_user_id: "1729"
_4images_image_date: "2014-02-08T22:06:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38212 -->
Der Schleifring in der Ansicht von unten