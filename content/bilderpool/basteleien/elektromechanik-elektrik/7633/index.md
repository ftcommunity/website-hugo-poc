---
layout: "image"
title: "Power-Adapter 3"
date: "2006-11-26T23:08:57"
picture: "dsc00961_resize.jpg"
weight: "7"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
schlagworte: ["akku", "adapter", "power", "controller"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- /php/details/7633
- /details818c.html
imported:
- "2019"
_4images_image_id: "7633"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7633 -->
