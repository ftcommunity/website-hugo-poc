---
layout: "comment"
hidden: true
title: "5157"
date: "2008-01-30T15:06:33"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Freunden, 

An andere möglichkeit gibt es bei Conrad: eine AV-Verbindungskabel mit 4pol. Klinkenstecker: nr. 302333-88 

Eine passende 4pol. Kupplung: 
Klinkeneinbaubuchse, 4polig gerade -90 
Conradnr. 734289-36 

http://www.ftcommunity.de/details.php?image_id=13303

Gruss, 

Peter Damen 
Poederoyen NL