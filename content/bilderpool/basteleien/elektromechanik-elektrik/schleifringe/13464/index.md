---
layout: "image"
title: "Alternativ Schleiffring: Twist-Stop Pollin"
date: "2008-01-30T14:09:24"
picture: "Twist-Stop-Pollin_004.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
fotografen:
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- /php/details/13464
- /detailsba0c-2.html
imported:
- "2019"
_4images_image_id: "13464"
_4images_cat_id: "347"
_4images_user_id: "22"
_4images_image_date: "2008-01-30T14:09:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13464 -->
