---
layout: "image"
title: "Schleifkontakt"
date: "2012-01-04T08:27:46"
picture: "drehkranzmitschleifring2.jpg"
weight: "29"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/33837
- /detailsca77.html
imported:
- "2019"
_4images_image_id: "33837"
_4images_cat_id: "347"
_4images_user_id: "182"
_4images_image_date: "2012-01-04T08:27:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33837 -->
Hier ist zu sehen wie ich die 2 Federkontakte 31306 montiert habe.
Dazu wurde das schwarze Tel des Derhkranzes mit 2 Bohrungen versehen.
Der Bausten 15 wurde eingekürzt um die Federkontakte aufnehmen zu können.
