---
layout: "image"
title: "SR_G02.JPG"
date: "2005-04-20T13:36:08"
picture: "SR_G02.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4041
- /details9c0d.html
imported:
- "2019"
_4images_image_id: "4041"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4041 -->
Modell G kann außerdem ++beidseitig++ auf den Drehkranz montiert werden: durch die Folge von 6 - 3 - 6  Millimeter starken Ringen klemmt die Trommel in beiden Richtungen.

Der Innendurchgang ist mit 10mm mehr als reichlich (da krieg ich wohl noch einen Pneumatik-Drehübertrager hinein...).