---
layout: "image"
title: "SR_E01.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_E01.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4033
- /details797c-2.html
imported:
- "2019"
_4images_image_id: "4033"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4033 -->
Modell E fing vielversprechend an, wurde aber dann zu sperrig und hat außerdem einen Kurzschluß zwischen zwei Ringen.