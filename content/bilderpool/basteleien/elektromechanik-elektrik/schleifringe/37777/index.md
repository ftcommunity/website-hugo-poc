---
layout: "image"
title: "Schleifring Unterseite"
date: "2013-10-30T22:51:38"
picture: "sriprototype2.jpg"
weight: "34"
konstrukteure: 
- "Martin Westphal (Masked)"
fotografen:
- "Martin Westphal (Masked)"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- /php/details/37777
- /details7a36.html
imported:
- "2019"
_4images_image_id: "37777"
_4images_cat_id: "347"
_4images_user_id: "373"
_4images_image_date: "2013-10-30T22:51:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37777 -->
Perfekt passend ist eine Bauplatte 60x15 mit 2 Zapfen. Der Schleifring wird mit einem Stück doppelseitigem Klebeband aus dem Bastelladen (nicht so übel klebend wie Teppichklebeband) auf der Bauplatte festgeklebt. Fertig ist das Ganze.
Wer die Befestigungsmöglichkeiten außen nicht verlieren will, kann sich natürlich eine passende Plastikplatte (PS oder so) zuschneiden, die alle Nuten und entsprechende Abstände freilässt.
