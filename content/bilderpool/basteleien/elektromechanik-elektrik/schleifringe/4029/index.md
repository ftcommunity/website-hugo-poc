---
layout: "image"
title: "SR_C01.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_C01.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4029
- /details0f31.html
imported:
- "2019"
_4images_image_id: "4029"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4029 -->
Modell C hat die Kontaktdrähte fest montiert und innen drin verläuft ein Kunststoffrohr; ansonsten ist es gleich aufgebaut wie Modell A. Das 10mm-Loch im Gelenkstein kam gerade recht :-)