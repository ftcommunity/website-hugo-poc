---
layout: "image"
title: "Schleifringe Angebot"
date: "2014-02-05T22:45:16"
picture: "Schleifringe.jpg"
weight: "38"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
schlagworte: ["Schleifring", "Slipring", "Drehkranz"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- /php/details/38208
- /detailsf0d2.html
imported:
- "2019"
_4images_image_id: "38208"
_4images_cat_id: "347"
_4images_user_id: "1729"
_4images_image_date: "2014-02-05T22:45:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38208 -->
Dies sind die Schleifringe, die ich habe.
Jeweils 6 Kontakte. Die Varianten unterscheiden sich nur in Lage und Größe des Befestigungringes.
Ich habe mehrere davon auf Vorrat. Diese kann ich bei Interesse abgeben.
Wer Bedarf hat, bitte per email melden. Ich muß dafür 14,90 ¤ zuzügl. Versandkosten verlangen, um meine Kosten zu decken.

Als weitere Variante möchte ich den Schleifring rechts so bearbeiten, daß der Befestigungsring die Form hat, die ich daneben gezeichnet habe. Dann passt er perfekt in den fischertechnik drehkranz, muß nicht zusätzlich fixiert werden und ragt nur auf einer Seite wenige Millimeter über den Drehkranz hinaus. Also perfekt platzsparend.
Ich kann auch diese Variante anbieten, muß aber dann ein paar Euro extra verlangen für die Fräserei.