---
layout: "image"
title: "Drehkranz mit Schleifring"
date: "2012-01-04T08:27:46"
picture: "drehkranzmitschleifring1.jpg"
weight: "28"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Andreas Tacke (TST)"
uploadBy: "TST"
license: "unknown"
legacy_id:
- /php/details/33836
- /detailsb09e.html
imported:
- "2019"
_4images_image_id: "33836"
_4images_cat_id: "347"
_4images_user_id: "182"
_4images_image_date: "2012-01-04T08:27:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33836 -->
Diese Idee wurde auf der Canvention gebohren.
Ich bin nun endlich dazu gekommen sie in die Tat umzusetzen.
In die Vertiefungen des roten Drehkrenzteiles habe ich 2 Messingringe mit einer Stärke von 0,5mm eingelassen.
An der Rückseite wurden dann die Litzen angelötet.
