---
layout: "image"
title: "SR_B01.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_B01.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4028
- /details5e24.html
imported:
- "2019"
_4images_image_id: "4028"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4028 -->
Das hier ist einer aus den Anfängen der Versuchsreihe. Die Ringe sind Unterlegscheiben. Die Drähte verlaufen in  Bohrungen durch den Stapel hindurch, an jedem Ring ist ein Draht angelötet, ein größeres Loch in der Nylonscheibe bietet Platz für den Lötklecks. Mittendrin bleiben 4mm Durchgang frei.