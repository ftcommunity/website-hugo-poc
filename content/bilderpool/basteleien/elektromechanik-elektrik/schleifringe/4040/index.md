---
layout: "image"
title: "SR_G01.JPG"
date: "2005-04-20T13:36:08"
picture: "SR_G01.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
schlagworte: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4040
- /details3bb4-2.html
imported:
- "2019"
_4images_image_id: "4040"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4040 -->
Modell G hat erstmalig Ringe mit einer umlaufenden Nut, d.h. der Kontaktdraht wird durch den Ring geführt, nicht durch eine Distanzscheibe. Der siebte davon ist hier gerade in Arbeit.

Beim Drehen von Ringen muss man sich an gut 50% Abfall gewöhnen, ein Drehmeißel zum Abstechen der fertigen Ringe muss schließlich eine gewisse Mindeststärke haben.