---
layout: "image"
title: "SR_G04.JPG"
date: "2005-04-20T13:36:08"
picture: "SR_G04.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- /php/details/4043
- /details5bfa.html
imported:
- "2019"
_4images_image_id: "4043"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4043 -->
Eine Stellprobe, Modell G mit der Trommel auf der schwarzen Seite des Drehkranzes.