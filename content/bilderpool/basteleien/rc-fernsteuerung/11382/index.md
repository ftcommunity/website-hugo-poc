---
layout: "image"
title: "ft-Dragster"
date: "2007-08-13T17:04:26"
picture: "PICT1954.jpg"
weight: "17"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/11382
- /details8c6f.html
imported:
- "2019"
_4images_image_id: "11382"
_4images_cat_id: "875"
_4images_user_id: "424"
_4images_image_date: "2007-08-13T17:04:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11382 -->
Im Bild die Fernsteuerung von einem Dicki Rennwagen bei dem das Getriebe kaputt war.
