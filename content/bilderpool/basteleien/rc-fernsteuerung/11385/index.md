---
layout: "image"
title: "ft-Dragster"
date: "2007-08-13T17:04:26"
picture: "PICT1959.jpg"
weight: "20"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/11385
- /details376d.html
imported:
- "2019"
_4images_image_id: "11385"
_4images_cat_id: "875"
_4images_user_id: "424"
_4images_image_date: "2007-08-13T17:04:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11385 -->
