---
layout: "image"
title: "2"
date: "2008-01-13T22:29:28"
picture: "2.jpg"
weight: "2"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Holger Howey (fishfriend)"
schlagworte: ["RC", "Servo", "verbinden", "fischertechnik"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- /php/details/13329
- /detailsde38.html
imported:
- "2019"
_4images_image_id: "13329"
_4images_cat_id: "1211"
_4images_user_id: "34"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13329 -->
Hallo...
Hier die Bauteile zusammengebaut.
Den Baustein auf das Servo stecken, Hülse rein, Schraube festziehen.
Und das ohne ft-Bauteile zu zersägen oder zu kleben.
Gruß
Holger Howey
