---
layout: "image"
title: "MB-Trac 1"
date: "2007-03-20T16:49:26"
picture: "MB-Trac1.jpg"
weight: "3"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9624
- /details21af.html
imported:
- "2019"
_4images_image_id: "9624"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9624 -->
MB-Trac aus dem Traktors Kasten mit 2-Kanal-RC-Steuerung