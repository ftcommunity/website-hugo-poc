---
layout: "image"
title: "So hält das ft-Ritzel mit Rastkupplung auf dem Servo"
date: "2007-11-14T18:06:46"
picture: "rcservozuftmotorumbauen5.jpg"
weight: "5"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12753
- /details8d1e.html
imported:
- "2019"
_4images_image_id: "12753"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-14T18:06:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12753 -->
Durch das Bohrloch eine passende Achse gesteckt (hier: eine Büroklammer), und schon ist eine sichere 1:1 Verbindung zwischen Servo und ft-Ritzel mit ft-Rastkupplung fertig. Wenn man diese Büroklammer noch abknippst und an den Enden umbiegt, damit sie sich nicht lösen kann, sieht das Endergebnis aus wie auf dem ersten Bild.

Im Detail hängt es natürlich von der Bauart des jeweiligen Servos ab, welche(s) Ritzel bearbeitet werden müssen. Das ist aber normalerweise leicht zu erkennen.