---
layout: "image"
title: "RC-Servo ohne obere Abdeckung"
date: "2007-11-14T18:06:45"
picture: "rcservozuftmotorumbauen2.jpg"
weight: "2"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- /php/details/12750
- /detailsa476.html
imported:
- "2019"
_4images_image_id: "12750"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-14T18:06:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12750 -->
Nach Demontage der Servoabdeckung ist hier das Ritzel, das bearbeitet werden muss, schon abgenommen. Die "Nase" daran dient als mechanischer Wegbegrenzer. Die muss weg! Also mit Cutter oder Kleinfräse entfernen.

Zu diesem Zeitpunkt sollte auch noch die richtige Nullposition des Servos kontolliert werden. Dazu Sender einschalten, Trimmer und Steuerknüppel in Mittenstellung bringen, Empfänger mit aufgeschraubtem Servo anschließen. Dann mit einer kleinen Zange die Achse des Potis im Servo so einstellen, dass der Servo-Motor in Ruhestellung ist.