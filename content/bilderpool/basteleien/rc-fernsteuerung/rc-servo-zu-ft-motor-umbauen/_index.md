---
layout: "overview"
title: "RC-Servo zu ft-Motor umbauen"
date: 2020-02-22T07:45:19+01:00
legacy_id:
- /php/categories/1143
- /categoriesfc31.html
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1143 --> 
Wer ein ft-Modell mit einer RC-Fernsteuerung betreiben will, möchte vielleicht die RC-Servos nicht nur zur Lenkung oder Steuerung mechanischer Fahrtregler einsetzen, sondern auch direkt als ft-Antriebsmotoren (z.B. für Seilwinden). Das ist mit einigen kleinen Umbauten am RC-Servo leicht zu bewerkstelligen! 



Dafür werden benötigt:

1 Standard RC-Servo, um die 5 EUR um Fachhandel

1 Kleinbohrmaschine (Dremel o.ä.) mit 1 mm Bohrer und kleinem Fräsaufsatz

1 ft Rastritzel Z10 (ft Nr. 35945)

1 Büroklammer

1 kleiner Kreuzschraubendreher



Wie geht das?

Indem der mechanische Stellwegbegrenzer des RC-Servos ebenso beseitigt wird wie die Kopplung des Drehwiderstandes im Servo an den aktuellen Stellzustand. Wenn das erledigt ist, dreht sich das Servo \"endlos\" und der RC-Sender bekommt keine Rückmeldung mehr über die aktuelle Stellposition.



Wie geht das genau?

1. Servo mit Ruderhorn drauf in Nullposition bringen, Ruderhorn abnehmen

2. Die 4 Schrauben an der Bodenplatte des Servos lösen

3. Obere Servoabdeckung entfernen

4. Zahnräder des Servo-Getriebes angucken und abnehmen. Ist etwas bauartbedingt, je nach Typ des Servos. Zwei Dinge müssen weg: 

5. Die Plastik\"nase\" an einem Zahnrad, die als mechanischer Wegbegrenzer dient, abclipsen oder abfräsen

6. Bei dem Zahnrad, das auf der Welle des Drehwiderstandes sitzt, die Kupplung zum Poti so rund ausfräsen, dass sich beim Drehen des Zahnrades das Drehpoti nicht mehr mitbewegt

7. Servo wieder zusammen bauen.

8. Servo an der RC-Fernsteuerung testen. Das Servo sollte sich jetzt an einem Proportional- oder Schaltkanal ohne mechanischen Widerstand endlos in die gewünschte Richtung drehen.

9. Den Antrieb \"ft-kompatibel\" machen. Dazu ein ft Rastritzel Z10 (ft Nr. 35945) an der Unterseite, wo das Zahnrad ist, zentriert so weit ausfräsen, dass es ziemlich genau auf die Achse des Servos passt.

10. ft Ritzel Z10 auf die Servo-Achse aufsetzen und mit einem 1 mm Bohrer durch Ritzel und Servoachse bohren

11. Eine 1 mm Metallachse (eine Büroklammer tut es auch) durch Ritzel und Servoachse führen und befestigen

12. Fertig!



Dieser Motor ist nicht stufenlos regelbar, dafür benötigt er aber nur einen Schaltkanal an Sender/Empfänger. Wie schnell/kräftig er ist, hängt natürlich vom verwendeten Servotyp ab.



Viele Grüße, 

Stefan