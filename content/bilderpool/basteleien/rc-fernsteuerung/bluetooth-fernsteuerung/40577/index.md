---
layout: "image"
title: "geschlossenes Gehäuse"
date: "2015-02-21T21:37:48"
picture: "GehuseZu.jpg"
weight: "1"
konstrukteure: 
- "Andreas Pockberger"
fotografen:
- "Philipp Kaufmann"
uploadBy: "phk"
license: "unknown"
legacy_id:
- /php/details/40577
- /detailscdd9.html
imported:
- "2019"
_4images_image_id: "40577"
_4images_cat_id: "3043"
_4images_user_id: "2379"
_4images_image_date: "2015-02-21T21:37:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40577 -->
