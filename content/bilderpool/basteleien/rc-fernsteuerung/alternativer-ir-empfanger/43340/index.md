---
layout: "image"
title: "Arduino und Shield"
date: "2016-05-06T10:15:54"
picture: "aire6.jpg"
weight: "6"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43340
- /details1622.html
imported:
- "2019"
_4images_image_id: "43340"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43340 -->
links der Arduino UNO SMD Edition, rechts das Shield

Bauteile auf dem Shield von oben nach unten: TSOP 4838 IR receiver, Transistor als Schalter, LED, Kondensator 1 uF, Reset Taster, L293B, Festspannungsregler 5V, DIP Schalter
