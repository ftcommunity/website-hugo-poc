---
layout: "image"
title: "Nano RC-Empfänger Shield bestückt (noch ohne NRF24)"
date: "2016-05-30T14:53:40"
picture: "RC_ohne_NRF24.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["NRF24", "Remote", "Control", "RC_Fernsteuerung", "Nano-Shield"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43446
- /details4405.html
imported:
- "2019"
_4images_image_id: "43446"
_4images_cat_id: "3219"
_4images_user_id: "579"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43446 -->
Nano RC-Empfänger Shield bestückt (noch ohne NRF24)