---
layout: "image"
title: "Arduino Nano Shield für RC Fernbedienung über NRF24 (2,4GHz Funk) mit 2 Servo-Anschlüssen"
date: "2016-05-13T13:21:52"
picture: "Nano_Servo_Shield_801.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
schlagworte: ["Fernbedienung", "Funkempfänger", "RC", "NRF24"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- /php/details/43345
- /detailsb665.html
imported:
- "2019"
_4images_image_id: "43345"
_4images_cat_id: "3219"
_4images_user_id: "579"
_4images_image_date: "2016-05-13T13:21:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43345 -->
Arduino Nano Shield für RC Fernbedienung über NRF24 (2,4GHz Funk) mit 2 Servo-Anschlüssen

Ganz links der Arduino Nano von oben
Mitte links das RC Funkempfänger Servo-Shield von oben betrachtet (wird unter den Nano montiert)
Mitte rechts der Arduino Nano von unten
Ganz rechts das RC Funkempfänger Servo-Shield von unten betrachtet (wird unter den Nano montiert)

Abmessungen der Platine 18 x 44 mm.