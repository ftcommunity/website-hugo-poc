---
layout: "image"
title: "'unboxing'"
date: "2016-05-06T10:15:54"
picture: "aire3.jpg"
weight: "3"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43337
- /details79cc.html
imported:
- "2019"
_4images_image_id: "43337"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43337 -->
IR Empfänger mit abgenommener Verdeckung, darunter befindet sich das Shield und der Arduino Uno
