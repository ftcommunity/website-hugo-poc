---
layout: "image"
title: "Oberseite"
date: "2016-05-06T10:15:54"
picture: "aire2.jpg"
weight: "2"
konstrukteure: 
- "David Holtz (davidrpf)"
fotografen:
- "David Holtz (davidrpf)"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- /php/details/43336
- /details294b.html
imported:
- "2019"
_4images_image_id: "43336"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43336 -->
Die Steckverbindung der Servos ist im 2.54 mm Raster gestaltet. Die Motoranschlüsse, der OUTPUT 1 und die Stromversorgung werden über die kombinierte Fischertechnik / Experimentiersteckverbindung angebracht. Diese besteht aus einem einzelnen Pin einer Stiftleiste (2.54 mm Raster) und einer Messinghülse mit Innendurchmesser von 3mm
