---
layout: "image"
title: "ft-Dragster"
date: "2007-08-13T17:04:26"
picture: "PICT1951.jpg"
weight: "16"
konstrukteure: 
- "Walter-Mario Graf (bumpf)"
fotografen:
- "Walter-Mario Graf (bumpf)"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- /php/details/11381
- /details7d55.html
imported:
- "2019"
_4images_image_id: "11381"
_4images_cat_id: "875"
_4images_user_id: "424"
_4images_image_date: "2007-08-13T17:04:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11381 -->
ft-Dragster mit 2 Geschwindigkeiten. Der Akku liefert 6 und 9 Volt.
