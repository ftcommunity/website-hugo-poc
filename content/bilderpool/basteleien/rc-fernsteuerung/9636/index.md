---
layout: "image"
title: "Beispiel Servoeinbau mit Clipachse 2"
date: "2007-03-20T18:02:59"
picture: "Servoeinbau_mit_Clipachse_2.jpg"
weight: "15"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9636
- /details4a37.html
imported:
- "2019"
_4images_image_id: "9636"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T18:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9636 -->
