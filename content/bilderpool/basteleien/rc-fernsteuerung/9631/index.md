---
layout: "image"
title: "Ruderhorn Rund mit 4,5mm-Loch"
date: "2007-03-20T16:49:26"
picture: "Ruderhorn_Rund.jpg"
weight: "10"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9631
- /detailsdca3.html
imported:
- "2019"
_4images_image_id: "9631"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9631 -->
