---
layout: "image"
title: "Fahrregler auf Bauplatte 30x15 mit ft-Steckern"
date: "2007-03-20T16:49:26"
picture: "Fahrregler_mit_ft-Steckern.jpg"
weight: "6"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9627
- /details8ef3.html
imported:
- "2019"
_4images_image_id: "9627"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9627 -->
