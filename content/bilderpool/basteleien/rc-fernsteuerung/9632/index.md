---
layout: "image"
title: "Alu-Ruderhörner Original und mit 4,5mm-Loch"
date: "2007-03-20T18:02:59"
picture: "Alu-Ruderhrner_Original_und_aufgebohrt.jpg"
weight: "11"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9632
- /details1b97.html
imported:
- "2019"
_4images_image_id: "9632"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T18:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9632 -->
