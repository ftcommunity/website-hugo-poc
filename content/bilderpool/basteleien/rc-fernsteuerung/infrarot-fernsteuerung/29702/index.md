---
layout: "image"
title: "Infrarot Fernsteuerung"
date: "2011-01-16T16:52:15"
picture: "infrarotfernsteuerung1.jpg"
weight: "1"
konstrukteure: 
- "ft"
fotografen:
- "-?-"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- /php/details/29702
- /details013f.html
imported:
- "2019"
_4images_image_id: "29702"
_4images_cat_id: "2180"
_4images_user_id: "968"
_4images_image_date: "2011-01-16T16:52:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29702 -->
Vor einiger Zeit konnte ich eine größere  Menge gebrauchtes FT erwerben.Dabei war auch eine Fernbedienung. Jetzt habe ich auch den passenden Empfänger . Ich suche allerdings noch die Bedienungsanleitung. Kann mir da wer weiterhelfen ?

Danke und Gruß