---
layout: "image"
title: "Ruderhorn mit S-Riegel-Gelenk 2"
date: "2007-03-20T16:49:26"
picture: "Ruderhorn_mit_Langloch_und_Strebe_2.jpg"
weight: "9"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9630
- /details4c9e.html
imported:
- "2019"
_4images_image_id: "9630"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9630 -->
