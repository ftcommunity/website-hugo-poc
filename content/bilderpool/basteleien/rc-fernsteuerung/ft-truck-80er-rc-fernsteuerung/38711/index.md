---
layout: "image"
title: "Inhalt Powerblockgehäuse, komplett"
date: "2014-04-27T16:09:00"
picture: "IMG_0001_2.jpg"
weight: "19"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38711
- /detailse41f.html
imported:
- "2019"
_4images_image_id: "38711"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38711 -->
Oben der Empfänger, darunter der Fahrregler, rechts danebein im Isolierband die Servoelektronik..
Der Fahrregler ist von Conrad, inzwischen habe ich einen Kleineren eines anderen Herstellers, in Kürze kommt dazu ein Treckermodell online..
