---
layout: "image"
title: "Seite Schräg rechts"
date: "2014-04-13T18:18:16"
picture: "IMG_0003.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38545
- /detailsc46f.html
imported:
- "2019"
_4images_image_id: "38545"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38545 -->
inzwischenn hat sich die Stromversorgung verlagert und es ist nur noch eine Energiequelle notwendig
