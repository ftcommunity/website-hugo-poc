---
layout: "image"
title: "Gesamtansicht"
date: "2014-04-13T18:18:16"
picture: "IMG_0001.jpg"
weight: "1"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
schlagworte: ["RC", "Truck", "Fischertechnik", "80er", "Fernsteuerung"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38543
- /detailse8ff.html
imported:
- "2019"
_4images_image_id: "38543"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38543 -->
