---
layout: "image"
title: "Befestigung des Akkus"
date: "2014-04-27T16:09:00"
picture: "IMG_0110.jpg"
weight: "10"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38702
- /details12d1.html
imported:
- "2019"
_4images_image_id: "38702"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38702 -->
