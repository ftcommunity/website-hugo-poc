---
layout: "image"
title: "Empfänger, Servoelektronik, Fahrtregler"
date: "2014-04-13T18:18:16"
picture: "IMG_0006.jpg"
weight: "6"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38548
- /details5f80.html
imported:
- "2019"
_4images_image_id: "38548"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38548 -->
Hier das Innenleben des Powerblock-Leerghäuses - ist eng und unschön - später mal in Aufgeräumt
vorn links sieht man die servoelektronik mit zusätzlichem Poti, um das original ft servo hier anschliessen zu können
