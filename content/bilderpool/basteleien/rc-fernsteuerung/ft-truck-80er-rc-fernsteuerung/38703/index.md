---
layout: "image"
title: "Kabelauslass"
date: "2014-04-27T16:09:00"
picture: "IMG_0002_2.jpg"
weight: "11"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- /php/details/38703
- /detailsc0e5.html
imported:
- "2019"
_4images_image_id: "38703"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38703 -->
