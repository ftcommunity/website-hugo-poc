---
layout: "image"
title: "Ruderhorn mit S-Riegel kompatiblem Langloch"
date: "2007-03-20T16:49:26"
picture: "Ruderhorn_mit_Langloch.jpg"
weight: "7"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- /php/details/9628
- /detailscfad.html
imported:
- "2019"
_4images_image_id: "9628"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9628 -->
