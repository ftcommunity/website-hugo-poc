---
layout: "wiki"
title: "Daten von ft-Motoren"
hidden: true
date: 2016-03-22T13:33:24
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
uploadBy:
- "Ma-gi-er"
license: "unknown"
legacy_id:
- /wikicec6.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wikicec6.html?action=show&topic_id=39 -->
<!--
Wiki


Thema: Daten von ft-Motoren

Aktuelle Version

von: Ma-gi-er

am: 22.01.2016, 13:33:24 Uhr

   Historie:
Version 22 von H.A.R.R.Y. am 28.10.2013, 08:04:06 Uhr
Version 21 von H.A.R.R.Y. am 28.10.2013, 08:01:24 Uhr
Version 20 von H.A.R.R.Y. am 28.09.2013, 06:26:21 Uhr

...

Version 3 von H.A.R.R.Y. am 17.09.2013, 07:36:27 Uhr
Version 2 von H.A.R.R.Y. am 17.09.2013, 07:35:41 Uhr
Version 1 von H.A.R.R.Y. am 17.09.2013, 07:35:13 Uhr
-->


"Was eigentlich immer fehlt und jeder braucht, aber von offizieller Seite nicht zu bekommen ist:


Vollständige Daten von ft-Motoren

Die Idee ist, mit Eurer Hilfe eine Übersicht über die verschiedenen Motortypen zu bekommen. Hier als Starthilfe mal die Ergebnisse einiger Messungen. Wie man's macht und noch viel mehr findet sich im gleichnamigen Artikel in der [ft:pedia, Heft 3/2013](/ftpedia/2013/2013-3) (ca. 5,5MByte). Im Forum gibt es auch einen [Thread](http://forum.ftcommunity.de/viewtopic.php?f=4&t=2096) zum Thema."


So ambitioniert war das mal geplant, aber das Echo aus der Community ist bislang minimal. Falls sich doch noch jemand dafür interessiert: Die Daten einiger von H.A.R.R.Y. ausgemessener Exemplare sind als pdf-Dateien im Download-Bereich "Technische Informationen" zu finden.

Anmerkung: Die Dateien finden sich jetzt [hier](../).


