---
layout: "file"
hidden: true
title: "Motordaten Power-Motor 20:1"
date: "2013-12-09T00:00:00"
file: "ft104589_neu.pdf"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/ft104589_neu.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/ft104589_neu.pdf -->
Kennlinien und -werte
für Getriebemotor 20:1
(104589) an 9V. Version ohne Tipfehler.