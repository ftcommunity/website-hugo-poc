---
layout: "file"
hidden: true
title: "Lineal"
date: 2020-08-14T15:38:04+02:00
file: "lineal72aFinalPDFCommunity2.pdf"
konstrukteure: 
- "Holger Howey (fishfriend)"
uploadBy:
- "website-team"
license: "unknown"
---
Hallo...

Besser bauen mit fischertechnik. Hierzu habe ich ein ft-Lineal entworfen.
Es ist fast wie Schweizer Messer. Man findet immer neue Anwendungen.
Ein paar sind am Rand des Papiers abgedruckt.
Man kann es als Lineal benutzen und sofort sehen wie viele Bausteine oder welche Achse man braucht.

Sonst zählt man die Kettenglieder, nun kann man sie einfach ablesen.
Oder: „Welcher Winkelbaustein kann passen und wie viele Bausteine 30/15 brauch ich?“
Und, und, und...
Auch zum schnellen Sortieren von Achsen, einfach das Lineal auf den Boden einer Box legen und die Achsen etwas rütteln.
Oder wenn man in einer älteren Anleitung/Ausdruck eine Achse 90 braucht, kann man die passende auf dem Lineal ablesen...

![Vorschau](../lineal72aFinalPDFCommunity2.jpg)

### Tipp für das Ausdrucken

Wenn vorhanden nimm dickeres 100g Papier. Mit normalen 80g Papier geht es genau so.
Z.B. Firefox: 
* Die PDF-Datei öffnen.
* Wichtig! Auf „**Menü öffnen**“ (rechts oben) und dann „**Drucken**“ klicken.
* Hier auf Skalierung: 100% stellen.
* Nun „Querformat“
* Auf „Drucken“ und den Drucker auswählen.
* Je nach Drucker: 100% (normale Größe); DIN A4; Querformat;
* Wenn vorhanden: „beste Qualität“ oder höhere DPI Zahl = hohe Auflösung; Photopapier...

Schwarz-Weiß geht auch, aber in Farbe sieht es schöner aus.
So sollte die richtige Größe in bester Qualität gedruckt werden.

### Tipp für das Zusammenkleben

Nimm die Zeit - sauber ausschneiden.
Die zwei Kanten falten und mit dem Fingernagel noch mal über die Faltkanten gehen um eine „scharfe“ Kanten zu bekommen.
Mit einem Klebestift die Innenseiten (beide Seiten von A) einstreichen.
Zusammendrücken und glattstreichen.

Wenn vorhanden:
Mit einer Lochzange 4mm Löcher bei den Streben lochen.
Die 105er Strebe braucht man nicht unbedingt, weil sie noch recht selten ist.
Die höchste Festigkeit erreicht man, nach einem Tag pressen, unter 2-3 schweren Büchern.

Viel Spaß wünscht
Holger Howey (fishfriend)

