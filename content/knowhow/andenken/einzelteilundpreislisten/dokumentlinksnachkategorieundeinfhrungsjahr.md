---
layout: "file"
hidden: true
title: "Dokument-Links nach Kategorie und Einführungsjahr"
date: "2014-10-31T00:00:00"
file: "dokumentlinksnachkategorieundeinfhrungsjahr.xls"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/dokumentlinksnachkategorieundeinfhrungsjahr.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/dokumentlinksnachkategorieundeinfhrungsjahr.xls -->
Dies ist das Ergebnis einer Abfrage an die ft:db mit Kategorie, Artikelbezeichnung, Jahr des Dokuments, Link zum Dokument und (soweit erfasst) das Jahr, ab dem der Artikel nicht mehr produziert wurde. Sortiert ist nach Kategorie und Einführungsjahr.
