---
layout: "file"
hidden: true
title: "Einzelteiltabelle"
date: "2005-05-20T00:00:00"
file: "fischertechnik_3_7_3.zip"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/fischertechnik_3_7_3.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/fischertechnik_3_7_3.zip -->
In der Tabelle finden Sie fast alle Fischertechnik-Bauteile (Ab 1989, farbig) mit: Abbildung, Artikelnummer, Bezeichnung, Farbe, Preisen (Stand: Preisliste '01) und wie oft ein bestimmtes Teil in den einzelnen Baukästen vorkommt."

Mehr Informationen
