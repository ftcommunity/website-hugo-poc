---
layout: "file"
hidden: true
title: "Material-Aufzug"
date: "2008-03-08T00:00:00"
file: "materialaufzug.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/software/materialaufzug.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/materialaufzug.zip -->
Das Programm enthält eine Routine zur Überprüfung der Drehrichtung ==> Ist der Aufzug z.B. im dritten Stock, und es drückt jemand im Erdgeschoß den Rufknopf, dann muß der Aufzug "runter" fahren. Klingt erst mal logisch. Aber die Umsetzung ist gar nicht so einfach...
