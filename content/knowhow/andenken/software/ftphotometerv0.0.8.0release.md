---
layout: "file"
hidden: true
title: "ftPhotometer"
date: "2017-08-15T00:00:00"
file: "ftphotometerv0.0.8.0release.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/ftphotometerv0.0.8.0release.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftphotometerv0.0.8.0release.zip -->
ftPhotometer

On the basis of ftrobopy and ftRoboRemote.TXT the full working application ftPhotometer is provided. Together with a fischertechnik model it is possible to measure the concentration in a solution with a 650nm laser. The software was developed on a WinXP SP3 system.

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail 