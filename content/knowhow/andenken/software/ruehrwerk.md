---
layout: "file"
hidden: true
title: "Rührwerk"
date: "2013-04-25T00:00:00"
file: "ruehrwerk.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/software/ruehrwerk.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ruehrwerk.zip -->
Kleines Modell von einem Rührwerk. Kann man mit den Kästen "Fischertechnik in der Schule" bauen (Grundschüler) inklusive PDF Anleitung zum Ausdrucken