---
layout: "file"
hidden: true
title: "Plotter Treiber"
date: "2009-10-26T00:00:00"
file: "plottertreiber_download.rpp"
konstrukteure: 
- "Severin"
uploadBy:
- "Severin"
license: "unknown"
legacy_id:
- /data/downloads/software/plottertreiber_download.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/plottertreiber_download.rpp -->
X und Y Werte haben ihre eigenen Listen. 
Das Programm gibt nimmt den kürzesten Weg zwischen der aktuellen und der gewünschten Position.
Die Konstante am Unterprogramm "Rechner" gibt die maximale Schrittgeschwindigkeit an.

Das Programm kann, über die Variablen X-Schub und Y-Schub, die Figur, verschoben, beliebig oft zeichnen. Sieht bei einfachen Figuren gut aus.
Über die Maximalgröße der Listen kann eigestellt werden wie oft die Figur verschoben wird. 
Hat die Figur 4 Koordinaten und soll 4 mal verschoben gezeichnet werden, muss der Maximalwert einer Liste der X oder Y Achse 16 sein.
Wenn die Figur nur einmal gezeichnet werden soll 4.

Mit 0 ist der Stift oben, mit 1 auf dem Blatt. Im Unterprogramm "Stift" kann die Mechanik der Stiftehalterung eingestellt werden. 
Es muss für jede Koordinate Angegeben werden in welche Position sich der Stift befindet.

Über X-Max und Y-Max muss die Auflösung vom Plotter angegeben werden. z.B.: 1000x570  
Sollte der Plotter einen Befehl bekommen der über dem der Auflösung ist, wird dieser nicht ausgeführt und das Programm beendet.