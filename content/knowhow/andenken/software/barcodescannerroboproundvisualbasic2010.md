---
layout: "file"
hidden: true
title: "BarCodeScanner RoboPro und VisualBasic 2010"
date: "2014-06-09T00:00:00"
file: "barcodescannerroboproundvisualbasic2010.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/barcodescannerroboproundvisualbasic2010.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/barcodescannerroboproundvisualbasic2010.zip -->
Hallo Fischertechnik-Freunde von Modellbau und Automatisierung!

Die Dateien die hier gezeigt werden sind Beispiele wie der Fischertechnik RoboTX Controller programmiert werden kann, mit der Fischertechnik RoboPro Software oder mit Microsoft VisualBasic 2010 Express. Beide Arten der Programmierung machen ihren Sinn auf die eine oder andere Art. Probiert es aus. Das Projekt darf kopiert und weiterverteilt werden, immer unter Berücksichtigung der Rechte von Microsoft, Fischertechnik und anderen.

Es würde von der Allgemeinheit sehr geschätzt werden, wenn die Ergebnisse der weiteren Arbeiten veröffentlicht würden, z.B. unter www.ftcommunity.de

Es gibt keine Garantie, alles auf eigenes Risiko.

Besonderer Dank gilt Carel van Leeuwen für grundlegende Informationen und Unterstützung. Besucht seine Seiten:
http://nl.linkedin.com/pub/carel-van-leeuwen/11/ba0/504
http://web.inter.nl.net/users/Ussel-IntDev/





Dear friends of fischertechnik models and automation!

The files shown here are an example to program the RoboTX Controller with Fischertechnik RoboPro software or with Microsoft VisualBasic 2010 Express. Both types of programming are making sense in the one or other way. Feel free to check it out. You can copy or distribute the project, but you must pay attention to the rights of Microsoft, Fischertechnik and others.

To publish your results would be appreciated by the community, e.g. under www.ftcommunity.de

No guarantee, all on your own risk.

Special thanks to Carel van Leeuwen for basic information and support. Watch his pages:
http://nl.linkedin.com/pub/carel-van-leeuwen/11/ba0/504
http://web.inter.nl.net/users/Ussel-IntDev/



Andreas Gail