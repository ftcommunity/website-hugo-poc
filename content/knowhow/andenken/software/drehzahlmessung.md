---
layout: "file"
hidden: true
title: "Drehzahlmessung für Getriebemotore"
date: "2007-11-26T00:00:00"
file: "drehzahlmessung.rpp"
konstrukteure: 
- "Werwex"
uploadBy:
- "Werwex"
license: "unknown"
legacy_id:
- /data/downloads/software/drehzahlmessung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/drehzahlmessung.rpp -->
Hier meine Lösung der Drehzahlmessung.
Er misst alle 2,5 s und aktualisiert die Anzeige. Zugegeben es sind durch den kurzen Zeittakt Ungenauigkeiten möglich.

Gruß werwex

