---
layout: "file"
hidden: true
title: "ftrobopy"
date: "2015-07-12T00:00:00"
file: "ftrobopy_v0_5.zip"
konstrukteure: 
- "Torsten Stuehn"
uploadBy:
- "Torsten"
license: "unknown"
legacy_id:
- /data/downloads/software/ftrobopy_v0_5.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftrobopy_v0_5.zip -->
Python Modul zur Ansteuerung des fischertechnik TXT Controllers ueber ein Python-Programm.

Das Modul kann mit allen Betriebssystemen genutzt werden, die Python2.7 und POSIX Sockets unterstuetzen. Dazu gehoeren z.B. Linux und Mac OS X Systeme. Entweder wird die Datei ftrobopy.py in das site-packages Verzeichnis des Systems kopiert, damit sie automatisch gefunden wird oder man kopiert die Datei in das gleiche Verzeichnis in dem man auch seine Python-Programme zur Steuerung des TXT hat. 
