---
layout: "file"
hidden: true
title: "Joysticksteuerung in Freebasic"
date: "2007-09-21T00:00:00"
file: "freebasic.zip"
konstrukteure: 
- "Werner Hobelbrecht"
uploadBy:
- "Werner Hobelbrecht"
license: "unknown"
legacy_id:
- /data/downloads/software/freebasic.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/freebasic.zip -->
Mit dem PC-Joystick kann man seine Modelle steuern und die Eingänge beobachten.
Die Sourcen und die Anbindung an Freebasic sind mit dabei.