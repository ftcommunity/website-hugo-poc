---
layout: "file"
hidden: true
title: "FreeSpeed"
date: "2005-05-20T00:00:00"
file: "FreeSpeed.exe"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/software/FreeSpeed.exe
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/FreeSpeed.exe -->
Programmieren Sie Abläufe, in denen die Motoren in beliebigen Geschwindigkeiten bewegt und beschleunigt werden können.

#### Programm zum Ansteuern des ft-Interfaces mit variablen Motordrehzahlen




FreeSpeed Version 3.1



Das alles kann FreeSpeed



* Motorgeschwindigkeiten von 0.00% bis 100.00%

* bis zu vier Motoren, alle unabhängig voneinander

* Beschleunigungen mit frei wählbaren Start- und Endwerten und beliebiger Dauer

* Erstellen von Programmabläufen durch Aneinanderreihen von Einzelbewegungen

* Abspeichern und Laden von Bewegungsabläufen

* Automatische Abschaltung einzelner Motoren bei Erkennung eines Endtasters

* Impulszählung an beliebigen Eingängen

* Sowohl Software- als auch Hardwareseitiger Notaus - Taster

* Nach Wunsch abspielen des Ablaufs als Endlosschleife

* Benutzerfreundliche, alles-auf-einen-Blick Benutzeroberfläche

* Neu: Unterstützt jetzt auch das parallele fischertechnik-Interface

     



Lesen Sie auch auch das Kurz-Handbuch! 
