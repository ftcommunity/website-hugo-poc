---
layout: "file"
hidden: true
title: "JoyTx"
date: "2010-02-10T00:00:00"
file: "joytest.rpp"
konstrukteure: 
- "Laurens Wagner"
uploadBy:
- "Laurens Wagner"
license: "unknown"
legacy_id:
- /data/downloads/software/joytest.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/joytest.rpp -->
Programm zur Steuerung eins Fischert. >TX Controllers mit ein aus Fischertechnik gebauten Joystick am ROBOIterface