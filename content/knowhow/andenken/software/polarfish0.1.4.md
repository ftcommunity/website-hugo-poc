---
layout: "file"
hidden: true
title: "Software für den Plotter / Scanner"
date: "2009-12-30T00:00:00"
file: "polarfish0.1.4.zip"
konstrukteure: 
- "Marcus Schulz"
uploadBy:
- "Marcus Schulz"
license: "unknown"
legacy_id:
- /data/downloads/software/polarfish0.1.4.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/polarfish0.1.4.zip -->
Zeichnet bisher 4 vordefinierte Plots.
