---
layout: "file"
hidden: true
title: "Zwei Versuche mit FT und POV-RAY"
date: "2011-03-02T00:00:00"
file: "ft_pov.zip"
konstrukteure: 
- "con.barriga"
uploadBy:
- "con.barriga"
license: "unknown"
legacy_id:
- /data/downloads/software/ft_pov.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ft_pov.zip -->
