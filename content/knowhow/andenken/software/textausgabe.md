---
layout: "file"
hidden: true
title: "Texte via RS232"
date: "2006-04-09T00:00:00"
file: "textausgabe.zip"
konstrukteure: 
- "Thomas Kaiser (thkais)"
uploadBy:
- "thkais"
license: "unknown"
legacy_id:
- /data/downloads/software/textausgabe.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/textausgabe.zip -->
Dieses C-Programm für das RoboInt sendet beliebige Texte über die serielle Schnittstelle zum PC. Auf der PC-Seite müssen die Daten allerdings noch aufbereitet werden, siehe hierzu die Kommentare in den Subroutinen.
