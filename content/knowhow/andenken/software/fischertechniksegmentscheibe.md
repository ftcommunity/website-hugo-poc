---
layout: "file"
hidden: true
title: "Segmentscheibe für Gabellichtschranke"
date: "2008-10-03T00:00:00"
file: "fischertechniksegmentscheibe.xls"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/software/fischertechniksegmentscheibe.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/fischertechniksegmentscheibe.xls -->
Mit diesem Excel-Programm kann man Segmentscheiben erstellen mit 2 bis 120 Segmenten. Einfach die entsprechende Anzahl Segmente in den Rahmen eintragen.
   Die Segmentscheibe kann man dann in beliebiger Größe ausdrucken. Hierzu in das Register Druckansicht gehen und auf den Button drücken, um die Größe zu verändern. Wenn man das Voreingestellte Maß von 20 mm beibehalten will, braucht man nur in diesem Register (Druckansicht) auf Drucken zu gehen.
