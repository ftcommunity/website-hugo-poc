---
layout: "file"
hidden: true
title: "FreeSpeedSpeech"
date: "2005-05-20T00:00:00"
file: "FreeSpeedSpeech.exe"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/software/FreeSpeedSpeech.exe
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/FreeSpeedSpeech.exe -->
Mithilfe dieser Software und einem Spracherkennungsprogramm können Sie Modelle sprachsteuern!
(Kann auch als Tastatursteuerungsprogramm dienen)

#### Programm zum Ansteuern des ft-Interfaces mit variablen Motordrehzahlen mit Sprachsteuerung



FreeSpeedSpeech Version 1.2



FreeSpeedSpeech dient wie FreeSpeedJoy zur Echtzeitsteuerung, ist allerdings für Tastatureingabe bestimmt.

Mit einer Spracherkennungssoftware können dann "künstlich" Tastendrücke erzeugt werden, die dann von FreeSpeedSpeech erkannt werden.



Sie können also mithilfe dieser Software fischertechnik-Modelle sprachsteuern!



#### Das alles kann FreeSpeedSpeech



* Ansteuerung des ft-Interfaces per Tastatur (und damit auch per Sprache)

* Für jeden Motor können Startwerte eingestellt werden, die dann bis zu der 1. Änderung gelten:

    * Startrichtung und Startgeschwindigkeit

    * Per Tastendruck können zur Laufzeit Motorwerte geändert werden:

        * Drehrichtung und

        * Geschwindigkeit der Motoren sind unabhängig von einander änderbar

    * Es lassen sich für jeden Motor und jede Drehrichtung Endtaster festlegen

* Es gibt drei verschiedene Notaus-Möglichkeiten

    * Auf der Benutzeroberfläche

    * beim Druck einer Taste und

    * bei Betätigung eines Tasters am Interface.

* Speicherung aller Einstellungen (wie in FreeSpeedJoy) in einer Datei: settings.fss

* Bei jedem Start des Programms werden die Daten geladen.



Zum Einrichten der Sprachsteuerung und zur Verwendung des Programms lesen Sie bitte das Tutorial.

Die richtige Einrichtung ist keineswegs einfach und kann durchaus einige Zeit in Anspruch nehmen!





#### Tipp



Zum dauerhaften Speichern von Einstellungen können Sie (nach dem Beenden von FreeSpeedSpeech) eine Kopie der settings.fss-Datei machen und dieser einen aussagekräftigen Namen geben.

Zum Wiederherstellen sichern/löschen Sie die aktuelle settings.fss-datei und benennen die Kopie wieder in diesen Namen um.



Bei entsprechender Nachfrage (oder wenn ich's selber brauche) kann ich ggf. auch noch eine Impulszählung an Eingängen dazu programmieren. Damit ließen sich dann auch Roboter sinnvoller Sprachsteuern.



Vielleicht finde ich auch eine brauchbare Möglichkeit, dass man Geschwindigkeitswerte als Zahlen bei der Sprachsteuerung sprechen kann - das ist mit dem bisherigen Programm leider nur dann möglich, wenn man jede Zahl einzeln einprogrammiert.
