---
layout: "file"
hidden: true
title: "Konvertieren .PLT in .RPP Datei (Für Plotter von Dirk Fox)"
date: "2012-04-03T00:00:00"
file: "konvertierenpltinrpp.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/software/konvertierenpltinrpp.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/konvertierenpltinrpp.zip -->
Ich habe ein paar kleine Tools geschrieben für den Plotter von Dirk Fox.
1.) In die Textdatei schreibt man die aktuelle Plotdatei rein (z.B. Afrika). So weiß man immer, welche Datei man gerade plottet.
2.) Man muß ja die entsprechende Datei immer umbenennen in "HPGL.csv".
Davor sollte man die Original-Datei kopieren (Strg+C / Strg+V) und nur die Kopie umbenennen).
3.) Mein Excel Makro konvertiert die .PLT Datei (z.B. Corel Draw) in die .RPP Datei (RoboPro). Makros müssen natürlich in Excel aktiviert sein, damit es funktioniert. Wenn es Fragen gibt, oder etwas nicht funktioniert, bitte mir bescheid sagen.

