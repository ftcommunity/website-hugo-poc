---
layout: "file"
hidden: true
title: "Normal -->Binärsystem Rechner"
date: "2008-01-29T00:00:00"
file: "binr.xls"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/software/binr.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/binr.xls -->
Programm rechnet natürlcihe Zahlen bis zu 2^26 in Binärsystem um.
System: Zahl wird durch aktuelle 2er Potenz geteilt, das Ergebnis abgerundet und der Rest ausgerechnet, mit dem dann auch so verfahren wird. Außerdem wird der Verlauf in einem Diagramm mit Logarithmischer Skalierung angezeigt