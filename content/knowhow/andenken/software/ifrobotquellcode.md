---
layout: "file"
hidden: true
title: "Programm zu meinem IR Roboter"
date: "2011-05-28T00:00:00"
file: "ifrobotquellcode.zip"
konstrukteure: 
- "Martin"
uploadBy:
- "Martin"
license: "unknown"
legacy_id:
- /data/downloads/software/ifrobotquellcode.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ifrobotquellcode.zip -->
Bilder gibŽs hier : http://www.ftcommunity.de/categories.php?cat_id=2284 
Bevor man das Programm startet muss man die Wiimote mit dem PC koppeln.
Dazu muss man während der PC nach neuen Geräten sucht die 1 und die 2 Taste auf der Wiimote drücken.
Bei weiteren Fragen könnt ihr eine E-mail an mich schreiben ( ft.fan@gmx.de ) 
mfg Martin