---
layout: "file"
hidden: true
title: "DOS xBase-Clipper drivers"
date: "2005-09-06T00:00:00"
file: "ftclip.zip"
konstrukteure: 
- "R.Nooteboom"
uploadBy:
- "R.Nooteboom"
license: "unknown"
legacy_id:
- /data/downloads/software/ftclip.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftclip.zip -->
Treiber für die DOS-xBase-Clipper5-Umgebung für die fischertechnik-Interfaces (parallel, seriell, ROBO-Interface im Kompatibilitätsmodus sowie das Knobloch Multiface)

#### Hier die Beschreibung des Autors für die "Fischer Technik DOS xBase-Clipper drivers":

Dear Fischer Technik user

In my opinion is the possibility to control the Fischer Technik interfaces and the knobloch Multiface from DOS(especially the xBase-environment Clipper5) is still a very interesting and effective option.
In contrast with the heavy, complex and slow GUI oriented windows like XP are applications under DOS extremely fast, stable, robust, straightforward and have no overhead.

For a dedicated application in digital interfacing for example controlling ft robots is DOS very efficient and relevant.

That is why I have developed a complete set of DOS (xBase) Clipper drivers for all the Fischer Technik Interfaces and the MultiFace from Knobloch.

I make all the software available so the whole Fischer Technik community can benefit from this driver collection, I consider all the clipper ft driver software developed by me as freeware. I hope it will then soon be available from ft websites.

There are drivers for the ftlpt (30520), ftcom (intelligent interface, 30402), the robopro (in II-mode) and the Multiface-interface from knobloch. All drivers directly address the IO-hardware registers of the COM-and LPT-port these addresses are implemented as parameters so all possible IO-configurations suited. All interfaces can be used simultaneously for example the Intelligent Interface at COM2 and the LPT (30520) at LPT1.

All software including source code is in the zip-file: ftclip.zzz , so rename to ftclip.zip for use. Read readme.txt first.

greetings: <r.nooteboom2@chello.nl>


#### readme.txt:

inhoud zip bijlage ftclip.zzz -> (is ftclip.zip !)

file list for DOS-xBase Clipper 5.2 ft interface drivers for the Intelligent Interface(30402), LPT(30520) and robopro interface and the MultiFace from Knobloch

CLIPXMS1 OBJ let the clipper VM use XMS instead of EMS
CLIPXMS2 OBJ let the clipper VM use XMS instead of EMS
CT LIB required : CA-Tools clipper library
FT CH #include file with fischer techniek constants
FT LIB all interfaces *.obj in one lib-file
FT NG norton guide documentation ft interface functions
FTCOM INI optional ini file with com port IO-adresses & IRQs
FTCOM OBJ compiled ftcom.prg
FTCOM PRG source code 30402-driver
FTCOMTST EXE ftcom 30402 test program
FTCOMTST LNK blinker link script for ftcomtst.exe
FTCOMTST OBJ compiled ftcomtst.prg
FTCOMTST PRG source code
FTLPT CH #include file for ftlpt.prg
FTLPT INI optional ini file with lpt IO-adresses
FTLPT OBJ compiled ftlpt.prg
FTLPT PRG source code 30520-driver
FTLPTTST EXE ftlpt 30520 test program
FTLPTTST LNK blinker link script for ftlpttst.exe
FTLPTTST OBJ compiled ftlpttst.prg
FTLPTTST PRG source code
GENERIC CH required: #include file for all appl
MFACE INI optional: ini file with multiface ioadresses
MFACE OBJ compiled mface.prg
MFACE PRG source code multiface driver
MFACETST EXE test program for the multiface
MFACETST LNK blinker link script for mfacetst.exe
MFACETST OBJ compiled mfacetst.prg
MFACETST PRG source code
NOOT CH #include file for use of noot.lib
NOOT LIB required : my own user defined functions library
NOOT NG norton guide doc for noot.lib
SIM_CTUS OBJ required replacement object for catools ctus.obj
__WAIT_B OBJ required patch for present recent (too) fast cpu's
