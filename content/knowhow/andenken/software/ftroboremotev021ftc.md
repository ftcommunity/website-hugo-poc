---
layout: "file"
hidden: true
title: "ftRoboRemote VisualBasic 2010"
date: "2017-04-18T00:00:00"
file: "ftroboremotev021ftc.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/ftroboremotev021ftc.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftroboremotev021ftc.zip -->
ftRoboRemote

This software is an example how to link one or more fischertechnik RoboTXT controllers to MS Visual Basic (VB) 2010 or higher. So VB gets access to the I/O connectors of the controller(s) (digital and analog) and the camera. On the basis of ftrobobpy nearly all features of the controller can be used.  

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail