---
layout: "file"
hidden: true
title: "Schrittberechnung für Viertelkreisbogen"
date: "2006-08-29T00:00:00"
file: "schrittberechnung.xls"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/software/schrittberechnung.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/schrittberechnung.xls -->
Erstellt von Thomas Brestrich nach Erläuterungen von Martin Roman im Fischertechnik-Forum im Artikel http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=863	

Ursprünglich zur XY-Ansteuerung eines Werkzeugs für einen Halbkreis gedacht, kann das Excelsheet die Schritte der Motoren in XY-Richtung für eine Ansteuerung über ein Schneckengetriebe universal berechnen.
