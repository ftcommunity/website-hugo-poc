---
layout: "file"
hidden: true
title: "Berechnung von Drehzahl und möglichen Impulsen"
date: "2008-10-10T00:00:00"
file: "drehzahlundmglicheflanken.xls"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/software/drehzahlundmglicheflanken.xls
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/drehzahlundmglicheflanken.xls -->
Mal ein Versuch das rechnerisch anzugehen. Die gelben Felder sind für Eingaben gedacht, die Drehzahlen der Motoren sind ja Spannungsabhängig und ich habe einen Wert für etwa ein volles Akkupack genommen. Mit Netzteil halt mehr. Fehler und Amerkungen bitte im Forum unter z.B. http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3364 posten
