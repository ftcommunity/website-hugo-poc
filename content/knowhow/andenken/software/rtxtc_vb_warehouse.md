---
layout: "file"
hidden: true
title: "RoboTXTC full automatic warehouse VisualBasic 2010 "
date: "2017-02-20T00:00:00"
file: "rtxtc_vb_warehouse.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/software/rtxtc_vb_warehouse.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/rtxtc_vb_warehouse.zip -->
This software is an application with a link of fischertechnik RoboTXT controller to MS Visual Basic (VB) 2010 or higher. With access to the digital I/O connectors of the controller and the video device it is possible to built a full automatic warehouse and a bar code reader for the goods to check in. All warehouse items are registered in a simple database.
"https://youtu.be/ysP5dczClOk"

Feel free to check it out. You can copy or distribute the programm together with this information, but you must pay attention to the rights of Microsoft and others.

No guarantee, all on your own risk.

Andreas Gail