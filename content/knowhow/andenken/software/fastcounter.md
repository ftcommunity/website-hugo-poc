---
layout: "file"
hidden: true
title: "Fast counter"
date: "2009-07-12T00:00:00"
file: "fastcounter.zip"
konstrukteure: 
- "Ad van der Weiden"
uploadBy:
- "Ad van der Weiden"
license: "unknown"
legacy_id:
- /data/downloads/software/fastcounter.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/fastcounter.zip -->
See the readme.txt and the wiki on the fast counter
