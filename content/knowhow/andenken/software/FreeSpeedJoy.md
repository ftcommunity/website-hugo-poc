---
layout: "file"
hidden: true
title: "FreeSpeedJoy"
date: "2005-05-20T00:00:00"
file: "FreeSpeedJoy.exe"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/software/FreeSpeedJoy.exe
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/FreeSpeedJoy.exe -->
Die beliebte Software zur Joysticksteuerung - leicht zum Einstellen und flexible Funktionsweise



Programm zum Ansteuern des ft-Interfaces mit variablen Motordrehzahlen mittels Joystick

 



#### FreeSpeedJoy Version 1.4



FreeSpeedJoy verwendet zwar den gleichen Steuerungskern und eine ähnliche Benutzeroberfläche wie FreeSpeed, ist aber ein komplett anderes Programm.



Das alles kann FreeSpeedJoy



* Ansteuerung des ft-Interfaces mit einem Joystick oder Gamepad (theoretisch auch Lenkrad etc *g*)

* Für jeden Motor kann eingestellt werden, wie er angesteuert werden soll:

    * Mit einer Joystickachse

    * Joystickbuttons oder

    * einer konstanten Geschwindigkeit

* Bei normalen Joystickachsen wird die Geschwindigkeit direkt durch die Achse bestimmt, bei Buttons ggf. durch einen Schubregler

* Drehrichtung und Geschwindigkeit der Motoren sind unabhängig von einander, d.h. man kann z.B. auch für eine Achse eine konstante Geschwindigkeit wählen

* Es lassen sich für jeden Motor und jede Drehrichtung Endtaster festlegen

* Es gibt drei verschiedene Notaus-Möglichkeiten:

    * Auf der Benutzeroberfläche

    * beim Druck eines Joystickbuttons und

    * bei Betätigung eines Tasters am Interface



Neu in Version 1.1



* FreeSpeedJoy speichert die Einstellungen beim Beenden und stellt sie beim nächsten Start wieder her.





Version 1.2 wegen Bugproblemen übersprungen





Neu in Version 1.3



* Bessere Unterstützung des Extension-Moduls, jetzt kein separates Programm mehr

* Automatische Verbindung zu Interface & Joystick beim Programmstart

(Beim 1. Start, bei Hardwareänderungen und auf Wunsch werden die Verbindungsdialoge selbstverständlich angezeigt)

* Bugfixes am Schubregler: Jetzt ist ein sanftes Beschleunigen/Abbremsen damit möglich

* Aufgrund der höheren Komplexität des Programms ist leider eine direkte Übernahme der Einstellunegn zur Laufzeit nicht mehr
 möglich, es gibt dafür aber "als Trost" einen Übernehmen-Button gleich unter dem LOS-Knopf.



Neu in Version 1.4



* Unterstützung des parallelen Interfaces durch Verwendung der umfish-Treiber



Eine ROBO-Version wird demnächst freigegeben, diese wird auch den RF Data Link unterstützten. (wer Lust hat zum Testen dieser Version kann sich bei MarMac melden)
