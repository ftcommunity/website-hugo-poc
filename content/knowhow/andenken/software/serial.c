/***********************************************************************/
/*                                                                     */
/*  FILE        :serial.c                                              */
/*  DATE        :Fri, Jan 23, 2009                                     */
/*  DESCRIPTION :main program file.                                    */
/*  CPU GROUP   :24                                                    */
/*                                                                     */
/*  This file is generated by Renesas Project Generator (Ver.4.13).    */
/*                                                                     */
/***********************************************************************/

#define U3TB 	(*(unsigned*)0x32A) //transmit register
#define U3RB 	(*(unsigned*)0x32E) //receive register
#define U3BRG 	(*(unsigned char*)0x329) //bit rate
#define U3MR 	(*(unsigned char*)0x328) //mode
#define U3C0 	(*(unsigned char*)0x32C) //control 0
#define U3C1 	(*(unsigned char*)0x32D) //control 1
#define U3SMR 	(*(unsigned char*)0x327) //special mode 0
#define U3SMR2 	(*(unsigned char*)0x326) //special mode 0
#define U3SMR3 	(*(unsigned char*)0x325) //special mode 0
#define U3SMR4 	(*(unsigned char*)0x324) //special mode 0
#define FCLK1	16000000L


void init_uart3(long br)
{ long n = FCLK1/16/br-1;
  char clksel =0;
  if (n>255)
  { n/=8;
    clksel++;
  }
  if (n>255)
  { n/=4;
    clksel++;
  }
  U3BRG = n;
  U3C0 = 0x10+clksel; // no rts/cts
  U3MR = 0x05; //8 bit, no parity, 1 stop
  U3C1 = 0x05; //en rx, tx
}

void uart3_tx(int d)
{ while (!(U3C1 & 0x02)) /*wait till tx empty*/;
  U3TB = d;
}

int uart3_rx()
{ if (U3C1 & 0x08) //rx full
    return U3RB;
  return -1;
}

void hex_nibble(char b)
{ if (b<0xa)
    uart3_tx('0'+b);
  else
    uart3_tx('A'+b-0xa);
}

void hex_byte(char b)
{ hex_nibble(b>>4);
  hex_nibble(b&0xF);
}

void hex_addr(long a)
{ hex_byte(a>>16);
  hex_byte(a>>8);
  hex_byte(a);
}

void hex_line(long a)
{ int i;
  hex_addr(a);
  uart3_tx(' ');
  for (i = 0; i<16; i++)
  { hex_byte(*(char far *)(a+i));
    uart3_tx(i==15?'\n':' ');
  }
}

//Write a motorola S0 record to the terminal
void S0_line(long a, char n)
{ int i, chk = n+3;
  uart3_tx('S');
  uart3_tx('0'); //S0 record
  hex_byte(n+3); //2 byte address + 1 byte checksum
  hex_byte(a>>8);//hi byte
  hex_byte(a);//lo byte
  chk += a>>8;
  chk += a;
  for (i = 0; i<n; i++)
  { char b = *(char far *)(a+i);
	hex_byte(b);
    chk += b;
  }
  hex_byte(~chk);
  uart3_tx('\r');
  uart3_tx('\n');
}

//Write a motorola S2 record to the terminal
void S2_line(long a, char n)
{ int i, chk = n+4;
  uart3_tx('S');
  uart3_tx('2'); //S2 record
  hex_byte(n+4); //3 byte address + 1 byte checksum
  hex_addr(a);//3 bytes for S2 record
  chk += a>>16;
  chk += a>>8;
  chk += a;
  for (i = 0; i<n; i++)
  { char b = *(char far *)(a+i);
	hex_byte(b);
    chk += b;
  }
  hex_byte(~chk);
  uart3_tx('\r');
  uart3_tx('\n');
}

//Write a motorola S8 record to the terminal
void S8_line(long a)
{ int i, chk = 4;
  uart3_tx('S');
  uart3_tx('8'); //S8 record
  hex_byte(4); //3 byte address + 1 byte checksum
  hex_addr(a);//3 bytes for S2 record
  chk += a>>16;
  chk += a>>8;
  chk += a;
  hex_byte(~chk);
  uart3_tx('\r');
  uart3_tx('\n');
}

/* This example program shows how to use the serial interface
   on the RoboInterface in the simplest way. That means no interrups,
   no buffering, nothing. By way of example we output a memory range
   as a motorola S file to a the PC. If you have a terminal emulator like 
   Putty (freeware) you can capture the output and save it to a .mot file.
   This file you can inspect in a text editor or, when you remove the 
   header line from putty, even load it as a module in the Renesas debugger!
   This will open a whole new world of possibilities.
*/
   
void main(void)
{ const char n = 32;
  long addr = 0xa0000;
  long end  = 0xa1000;
  addr = n*(addr/n);
  end  = n*(end/n+1);
  init_uart3(38400);
  S0_line(0,0);
  for (;addr<end;addr+=n)
    S2_line(addr,n);
  S8_line(0);	
}
