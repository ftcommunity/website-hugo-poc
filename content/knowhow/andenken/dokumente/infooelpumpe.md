---
layout: "file"
hidden: true
title: "Variables Getriebe bei der Ölpumpe"
date: "2007-12-15T00:00:00"
file: "infooelpumpe.pdf"
konstrukteure: 
- "Wilhelm Klopmeier"
uploadBy:
- "Wilhelm Klopmeier"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/infooelpumpe.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/infooelpumpe.pdf -->
Mit dem Einsatz des variablen Getriebes bei der Ölpumpe kann entweder die
aktuelle Förderkapazität aus der Quelle erhöht werden oder eine schwächer
werdende Quelle über einen längeren Zeitraum genutzt werden. Die
gleichmäßige Hubgeschwindigkeit ermöglicht das Füllen des Pumpenraumes - die
Pumpe darf niemals "Vakuum" saugen.
