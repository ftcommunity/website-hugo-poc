---
layout: "file"
hidden: true
title: "Etiketten Stand 2015-03-16"
date: "2015-03-16T00:00:00"
file: "etiketten_einzelteile_20150316.zip"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/einzelteilundpreislisten/etiketten_einzelteile_20150316.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/einzelteilundpreislisten/etiketten_einzelteile_20150316.zip -->

Diese Etikettenversion hat viele Neuigkeiten:
- Die Bilder wurden von unnötigem weißen Rand beschnitten, sodass die Teile selbst größer erscheinen.
- Sie stehen neben den Rohdaten als .xlsx und .csv auch als PDF, XLSX (Excel 2013) und DOCX (Word 2013) in den Formaten 40 mm x 18 mm und 45 mm x 15 mm bereit.
- Der Artikeltext ist ein Hyperlink direkt zur Artikelvariante auf der ft:db-Website (auch wenn er nicht blau formatiert ist). Man kann ihn also in der PDF, in der Excel sowie (mit Strg) in Word anklicken und landet auf der ft:db-Website.
- Jede Kategorie ist mit einem grauen Zwischenetikett mit der vollständigen Kategoriebezeichnung versehen.
- Diese Kategoriebezeichnung findet sich in den PDF und in den XLSX auch als anklickbares Inhaltsverzeichnis wieder (PDF bitte aufklappen, XLSX auf Tabellenblatt 1).
- Die Kategorien in der XLSX sind dort auch als Gliederungsebenen verankert und können also zu- und aufgeklappt werden.
- In der ersten Version der Etiketten hatten die Bilder einen unerwünschten hellgrauen 1-Pixel-Rand; der ist weg.
