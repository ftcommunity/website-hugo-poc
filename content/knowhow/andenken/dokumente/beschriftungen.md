---
layout: "file"
hidden: true
title: "Beschriftungen Sortimentsboxen"
date: "2007-01-14T00:00:00"
file: "beschriftungen.zip"
konstrukteure: 
- "Sven Engelke (sven)"
uploadBy:
- "Sven Engelke"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/beschriftungen.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/beschriftungen.zip -->
Vor einiger Zeit habe ich für meine ft-Sammlung Beschriftungskärtchen erstellt.

Die Größe ist unterschiedlich und auf meine Sortimentsboxen abgestimmt.

Also 500er Box, 1000er Box, Raaco, Schubladensortimentskästen, Foxboxen in verschiedenen Größen.

Erstellt habe ich die Sachen in Word.
Hier im Zip-File sind pdf's.
Bei Bedarf kann ich die Worddateien auch gerne rausgeben. Format Word 2003!

Euer Sven
