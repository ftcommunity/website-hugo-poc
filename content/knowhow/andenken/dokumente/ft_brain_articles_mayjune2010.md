---
layout: "file"
hidden: true
title: "Robot Magazine Articles"
date: "2010-04-06T00:00:00"
file: "ft_brain_articles_mayjune2010.pdf"
konstrukteure: 
- "Richard Mussler-Wright"
uploadBy:
- "Richard Mussler-Wright"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/ft_brain_articles_mayjune2010.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/ft_brain_articles_mayjune2010.zip -->
This is a zipped file of two articles in Robot Magazine. One is about the ft fan convention. The other is a review of the PCS Brain and ft manipulatives. (Please order a subscription - they are located on the web at http://www.botmag.com/ -these guys are awesome!)
