---
layout: "file"
hidden: true
title: "Schmiedepresse"
date: "2007-12-15T00:00:00"
file: "infovarpressenantriebe.pdf"
konstrukteure: 
- "Wilhelm Klopmeier"
uploadBy:
- "Wilhelm Klopmeier"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/infovarpressenantriebe.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/infovarpressenantriebe.pdf -->
Exzenterantrieb mit zyklisch variablem
Getriebe
