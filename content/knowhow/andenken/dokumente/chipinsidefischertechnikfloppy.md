---
layout: "file"
hidden: true
title: "ChipInside-Fischertechnik-Floppy.zip"
date: "2013-10-20T00:00:00"
file: "chipinsidefischertechnikfloppy.zip"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
uploadBy:
- "Volker-James Münchhof"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/chipinsidefischertechnikfloppy.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/chipinsidefischertechnikfloppy.zip -->
Software
Floppy     :  ChipInside-Fischertechnik-Floppy.zip
installiert:  ChipInside-Fischertechnik-CHIP1292.zip
