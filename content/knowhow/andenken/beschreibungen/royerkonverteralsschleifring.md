---
layout: "file"
hidden: true
title: "Royer Converter"
date: "2010-10-27T00:00:00"
file: "royerkonverteralsschleifring.pdf"
konstrukteure: 
- "ErwinG"
uploadBy:
- "ErwinG"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/royerkonverteralsschleifring.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/royerkonverteralsschleifring.pdf -->
Drahtlose Energieübertragung mittels Royer Converter z.B. als Ersatz für Schleifringe