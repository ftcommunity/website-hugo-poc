---
layout: "file"
hidden: true
title: "Programm + Beschreibung eines ft-Wolf-Roboters"
date: "2008-05-29T00:00:00"
file: "fischertechnikbordewolfroboprobeschrijving.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/fischertechnikbordewolfroboprobeschrijving.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/fischertechnikbordewolfroboprobeschrijving.doc -->
fischertechnik-Wolf-Roboter, inspiriert von unserem Urlaub im Mai 2008 im Sauerland (= Rotkäppchenland) und früheren Besuchen in Efteling.  
Ich kann ihn so programmieren, dass er folgendes tut: 

* Auf- und abwärts gehen (Eisbären), 
* Drehen des Körpers, 
* Körper und Hals nach vorne und hinten, 
* Kiefer öffnen und schließen (fest zubeißen), 
* zurück nach oben mit Kompressor, Magnetventil und Druckluftspeicher, 
* Sprechen, Schreien und Weinen mittels Conrad-Sprachmodul.


### Toelichting programma + opbouw FT-model
Fischertechnik-Wolf-robot, geïnspireerd door onze vakantie in mei 2008 in het Sauerland (=Rotkäppchenland) en eerdere bezoeken aan de Efteling.

Ik kan hem geprogrammeerd de volgende zaken laten uitvoeren: 

- op en neer lopen (ijsberen) 
- draaien met lijf 
- lijf/hals naar voren en achteren 
- bek open en dicht (stevig bijten) 
- rug opzetten d.m.v. compressor, magneetventiel, en drukzak 
- praten, roepen en huilen d.m.v. Conrad-voice-module 
