---
layout: "file"
hidden: true
title: "Die Ballwurfmaschine"
date: "2005-05-20T00:00:00"
file: "Die_Ballwurfmaschine.pdf"
konstrukteure: 
- "Martin Romann (Remadus)"
uploadBy:
- "Martin Romann"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/Die_Ballwurfmaschine.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/Die_Ballwurfmaschine.pdf -->
Martin Romann hat eine Maschine gebaut, die Metallkugeln gezielt eine vorher eingestellte Distanz wirft.
Hier hat er anhand vieler Bilder seine "Maschine" genau beschrieben und erklärt.
