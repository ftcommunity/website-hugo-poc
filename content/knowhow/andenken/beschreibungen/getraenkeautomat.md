---
layout: "file"
hidden: true
title: "Getränkeautomat-Steuerung"
date: "2006-11-28T00:00:00"
file: "getraenkeautomat.pdf"
konstrukteure: 
- "Jan Knobbe"
uploadBy:
- "Jan Knobbe"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/getraenkeautomat.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/getraenkeautomat.pdf -->
Modell siehe [Bilderpool](http://www.ftcommunity.de/categories.php?cat_id=539)
