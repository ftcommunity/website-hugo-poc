---
layout: "file"
hidden: true
title: "Interaktiver Fin-Ray-Zerrspiegel"
date: "2014-03-06T00:00:00"
file: "interactievefinraylachspiegel.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/interactievefinraylachspiegel.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/interactievefinraylachspiegel.pdf -->
Der Zerrspiegel besteht aus 2 Spiegelplatten von 0,5 x 1m mit Fischertechnik I-Streben in regelmäßigen Abständen für das Fin-Ray-Prinzip.  
Je nach Einstellposition bzw. gewähltem RoboPro-Programm des Fin-Ray Zerrspiegels wird auf der einen Seite die Wölbung und auf der anderen Seite der Hohlraum des Spiegels größer, kleiner oder umgekehrt.  
Es werden verschiedene pneumatische und elektrische Antriebe mit Vor- und Nachteilen beschrieben.

### Interactieve Fin-Ray Lachspiegel

Lachspiegel bestaande uit 2 spiegelplaten van 0,5 x 1m met Fischertechnik I-spanten op regelmatige afstanden ten behoeve van het Fin-Ray-principe. 
Afhankelijk van de instel-positie en/of het gekozen RoboPro-Programma van de Fin-Ray Lachtspiegel wordt aan de ene zijde de bolling en aan de andere zijde de holling van de spiegel groter, kleiner of omgekeerd.  
Verschillende pneumatische en elektrische aandrijvingen met voor- en nadelen worden beschreven.
