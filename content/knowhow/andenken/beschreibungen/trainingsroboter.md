---
layout: "file"
hidden: true
title: "Trainingsroboter Koordinatenrückrechnung"
date: "2010-02-10T00:00:00"
file: "trainingsroboter.doc"
konstrukteure: 
- "Christian Lachmann"
uploadBy:
- "Christian Lachmann"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/trainingsroboter.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/trainingsroboter.doc -->
Berechnung der Knickwinkel (Motor 2&3)des Trainingsrobots durch Vorgabe vom Werkzeugpunkt (x,y)
Nicht vollständig aber die größte Nuss die geknackt werden muss