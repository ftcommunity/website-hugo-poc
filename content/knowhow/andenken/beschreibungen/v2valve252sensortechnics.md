---
layout: "file"
hidden: true
title: "5 und 12V Pneumatik Magnetventiele, passend für Fischertechnik."
date: "2007-01-29T00:00:00"
file: "v2valve252sensortechnics.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/v2valve252sensortechnics.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/v2valve252sensortechnics.pdf -->
Bei SensorTechnics ( www.sensortechnics.com ) gibt es 5 und 12V Pneumatik Magnetventiele, passend für Fischertechnik.

Sensortechnics GmbH
Boschstr. 10
82178 Puchheim 
Deutschland 
Telefon: +49 (89) 80083-0 
Fax: +49 (89) 80083-33 


V2-valves: V2-13-3-PV-12-F-8-8 á 22,30 Euro/st eignen sich sehr gut für Fischertechnik. 

Weil es 3 Anschlusse gibt, statt nur 2 wie beim öriginal FT-Magnetventilen, gibt es mehrere möglichkeiten. Positionierung der Zylinder ist leichter.
