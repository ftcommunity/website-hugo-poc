---
layout: "file"
hidden: true
title: "fischertechnik WALL-E Nachbau"
date: "2017-02-20T00:00:00"
file: "ftwallepraktikum.mp4"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
uploadBy:
- "Ralf Geerken"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/walle/ftwallepraktikum.mp4
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/walle/ftwallepraktikum.mp4 -->
Dieses ist ein Nachbau des WALL-E von Dirk Wölffel.
Er ist entstanden mit seiner Bauanleitung, unter meiner Anleitung und mit den fischertechnik Neulingen Alexander und Julian während eines 2-wöchigen Betriebspraktikums im Januar 2017. Die Bauzeit pro Tag lag etwa bei 90min (mehr nicht). Die Software wurde uns freundlicherweise von Dirk Wölffel zur Verfügung gestellt.

Fairerweise sei hier erwähnt, dass der WALL-E nicht den kompletten Funktionsumfang, wie er in der Bauanleitung beschrieben ist, hat. Er kann lediglich den Ball mit dem Kopf und den Raupenketten verfolgen. Mehr war aber von Anfang an nicht in dem zeitlich sehr knapp bemessenen Projektumfang vorgesehen.
