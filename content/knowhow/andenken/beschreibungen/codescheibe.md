---
layout: "file"
hidden: true
title: "Codierscheibe CNY37_CNY70 als JPG"
date: "2006-02-28T00:00:00"
file: "codescheibe.jpg"
konstrukteure: 
- "Reiner"
uploadBy:
- "Reiner"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/codescheibe.jpg
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/codescheibe.jpg -->
