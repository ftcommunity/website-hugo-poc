---
layout: "file"
hidden: true
title: "Wall-E_V1.1"
date: "2017-11-25T00:00:00"
file: "walle_v1.1.zip"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/walle/walle_v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/walle/walle_v1.1.zip -->
neue verbesserte Version für den Wall-E.
