---
layout: "file"
hidden: true
title: "fischertechnik Waaierschleuse"
date: "2009-08-15T00:00:00"
file: "ftinundatiewaaiersluis.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/ftinundatiewaaiersluis.doc
- /knowhow/umzugsgut/beschreibungen/ftinundatiewaaiersluis"
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/ftinundatiewaaiersluis.doc -->
"Eine Waaierschleuse ist eine spezielle Schleuse, die gegen den Wasserdruck geöffnet und geschlossen werden kann. Dieser Schleusentyp wurde von Jan Blanken (NL, 1755-1838) erfunden."

Het bijzondere van een (inundatie-) waaiersluizen is gelegen in het feit dat de sluisdeuren door omloopriolen met schuiven, ook tegen hoog water in, kunnen  worden geopend of gesloten, zelfs tegen de stroom in. 
