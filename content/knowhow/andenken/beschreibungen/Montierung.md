---
layout: "file"
hidden: true
title: "Teleskopmontierung"
date: "2005-05-20T00:00:00"
file: "Montierung.pdf"
konstrukteure: 
- "Martin Romann (Remadus)"
uploadBy:
- "Martin Romann"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/Montierung.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/Montierung.pdf -->
Bau einer schrittmotorgetriebenen alt-azimutalen Montierung für ein Teleobjektiv.
