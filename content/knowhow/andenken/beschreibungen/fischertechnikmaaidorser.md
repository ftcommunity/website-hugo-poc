---
layout: "file"
hidden: true
title: "fischertechnik Mähdrescher"
date: "2008-05-06T00:00:00"
file: "fischertechnikmaaidorser.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poedeoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/fischertechnikmaaidorser.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/fischertechnikmaaidorser.doc -->

Einen "New Holland", einen "Case-International" oder einen "Massey Ferguson"-Mähdrescher zu bauen, war nicht mein Hauptziel. Schließlich ist es ein "fischertechnik-Mähdrescher".  
Für den Straßentransport habe ich hinter dem Mähdrescher einen Schleppwagen gebaut, auf dem das sogenannte Mähbrett transportiert werden kann.  
Der Mähdrescher ist selbsttragend, bestehend aus fischertechnik U-Profilen, Eckstützen und vielen Aluminiumprofilen, an denen alle beweglichen Teile befestigt sind.


### fischertechnik maaidorser
Om specifiek een "New Holland", een "Case-International", of een "Massey Ferguson" maaidorser te bouwen was niet mijn hoofddoel. Het is ten slotte een "Fischertechnik maaidorser".  
Voor het wegtransport heb ik een achter de maaidorser getrokken wagen gemaakt, waarop het zogenaamde maaibord vervoerd kan worden.  
De maaidorser is zelfdragend, opgebouwd uit Fischertechnik U-profielen, hoekdragers en veel aluminiumprofielen, waaraan alle bewegende delen zijn bevestigd. 
