---
layout: "file"
hidden: true
title: "RoboPro Bumberbot"
date: "2009-02-08T00:00:00"
file: "robopro_bumberbot_description.pdf"
konstrukteure: 
- "Guido van der Harst"
uploadBy:
- "Guido van der Harst"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/robopro_bumberbot_description.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/robopro_bumberbot_description.pdf -->

Grundsätzliche Überlegungen für ein fischertechnik-Wendefahrzeug mit Roboterfunktionen.
Schwerpunkt ist die Programmierung in ROBO Pro.
