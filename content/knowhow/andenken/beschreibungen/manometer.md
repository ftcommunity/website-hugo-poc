---
layout: "file"
hidden: true
title: "Manometerkatalog"
date: "2006-05-10T00:00:00"
file: "manometer.pdf"
konstrukteure: 
- "Martin Romann (Remadus)"
uploadBy:
- "Remadus"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/manometer.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/manometer.pdf -->
Die blauen Bestellnummern bezeichnen Lagerware, die ohne Mindestmenge bestellt werden kann.
Die anderen Typen gibt es ab 100 Stück.

Meine Bestellung zielt auf Seite 43.
