---
layout: "file"
hidden: true
title: "Codewheels und Codestrips"
date: "2006-09-05T00:00:00"
file: "codewheel.ps"
konstrukteure: 
- "Martin Romann (Remadus)"
uploadBy:
- "Remadus"
license: "unknown"
legacy_id:
- /data/downloads/beschreibungen/codewheel.ps
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/beschreibungen/codewheel.ps -->
Diese Postscript-Datei erstellt Codewheels und Codestrips für Postscript-Drucker.

Die Anzahl der Linien, Durchmesser, Liniendichte usw. muß man in der Datei selbst eingeben. 
