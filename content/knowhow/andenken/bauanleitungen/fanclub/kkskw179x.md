---
layout: "file"
hidden: true
title: "ft-Club Bauanleitung 1 von 1979 "
date: "2007-02-17T00:00:00"
file: "kkskw179x.pdf"
konstrukteure: 
- "Walter Golz"
uploadBy:
- "Walter Golz"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/kkskw179x.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/kkskw179x.pdf -->
Dies ist eine "Weckmaschine" die sozusagen Knistert, knallt, kracht, spritzt und weckt

diese Bauanleitungen konnte man damals als ft-Club-Mitglied anfordern, nachdem diese im ft-Club-Magazin angekündigt wurden