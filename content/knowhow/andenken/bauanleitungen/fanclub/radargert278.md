---
layout: "file"
hidden: true
title: "ft-Club Bauanleitung 2 von 1978 "
date: "2007-02-17T00:00:00"
file: "radargert278.pdf"
konstrukteure: 
- "Walter Golz"
uploadBy:
- "Walter Golz"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/radargert278.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/radargert278.pdf -->

Die Funktion des Radars wird beschrieben und eine Radaranlage mit Turm und Schirm,
Schreiber und Elektronik wird gebaut.
