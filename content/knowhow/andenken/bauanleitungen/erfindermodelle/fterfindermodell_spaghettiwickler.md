---
layout: "file"
hidden: true
title: "Erfindermodell Spaghetti-Wickler"
date: "2017-02-20T00:00:00"
file: "fterfindermodell_spaghettiwickler.pdf"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
uploadBy:
- "Ralf Geerken"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/erfindermodelle/fterfindermodell_spaghettiwickler.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/erfindermodelle/fterfindermodell_spaghettiwickler.pdf -->
