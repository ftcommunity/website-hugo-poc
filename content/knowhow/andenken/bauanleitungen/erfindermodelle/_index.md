---
title: "Erfindermodelle"
weight: 1
legacy_id:
- /downloadsa0d4.html
- /php/downloads/Erfindermodelle
---
Neben den "Clubmodellen" und den "fischertechnik Modellen" gab es die Serie "Erfindermodelle". 
Drei Anleitungen aus der Serie sind verschollen (Nasenbohrer<sup>1</sup>, Mondkalb und Seifenblasenmachine), aber sechs Perlen zeigen wir euch hier.

<sup>1</sup> Vom Nasenbohrer gibt es immerhin ["Beweisfotos"](https://www.ftcommunity.de/bilderpool/modelle/sonstiges/ft-nasenbohrer/).
