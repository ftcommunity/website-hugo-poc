---
layout: "file"
hidden: true
title: "Erfindermodell Blumengießmaschine"
date: "2017-02-20T00:00:00"
file: "blumengiessmaschine.pdf"
konstrukteure: 
- "Ralf Geerken (ThanksForTheFish)"
uploadBy:
- "Ralf Geerken"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/erfindermodelle/blumengiessmaschine.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/erfindermodelle/blumengiessmaschine.pdf -->
