---
title: "Bauanleitungen"
weight: 10
legacy_id:
- /downloadscaa8.html
- /php/downloads/Bauanleitungen
---
Hier werden Bauanleitungen in verschiedenen Formaten gesammelt,
z.B. Anleitungen zu eigenen Modellen.
Anleitungen aus den nicht mehr im Handel erhältlichen Baukästen
finden sich in der [ft-Datenbank](https://ft-datenbank.de).
Die [Fan-Club-News](/fans/andenken/fanclubnews) enthalten auch interessante Bauanleitungen.

