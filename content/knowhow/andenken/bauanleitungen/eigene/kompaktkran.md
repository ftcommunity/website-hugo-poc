---
layout: "file"
title: "Bauanleitung Kompaktkran"
hidden: "true"
date: "2014-09-12T00:00:00"
file: "kompaktkran.pdf"
konstrukteure: 
- "David Holtz (davidrpf)"
uploadBy:
- "david-ftc"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/kompaktkran.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/kompaktkran.pdf -->
Bauanleitung für den Kompaktkran aus meinem Modell "Unimog U400" (http://ftcommunity.de/categories.php?cat_id=2928)
