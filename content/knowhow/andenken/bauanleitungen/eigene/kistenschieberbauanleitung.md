---
layout: "file"
hidden: true
title: "Kistenschieber Bauanleitung"
date: "2014-11-16T00:00:00"
file: "kistenschieberbauanleitung.pdf"
konstrukteure: 
- "M. Bäter"
uploadBy:
- "M. Bäter"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/kistenschieberbauanleitung.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/kistenschieberbauanleitung.pdf -->
Eine Maschine die Kisten aus einem Magazin auf ein Förderband schiebt.