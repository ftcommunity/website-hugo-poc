---
layout: "file"
title: "Bogenbrücke"
hidden: "true"
date: "2008-12-24T00:00:00"
file: "bogenbrcke.pdf"
konstrukteure: 
- "Thomas Habig (Triceratops)"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/bogenbrcke.doc
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/bogenbrcke.doc -->
Nachbau der Bogenbrücke einer historischen Anleitung in
neuem Design mit diversen Erweiterungen und Ergänzungen.
