---
layout: "file"
hidden: true
title: "Hubschrauber"
date: "2013-04-25T00:00:00"
file: "hubschrauber.zip"
konstrukteure: 
- "Johannes Visser"
uploadBy:
- "Johannes Visser"
license: "unknown"
legacy_id:
- /data/downloads/software/hubschrauber.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/hubschrauber.zip -->
Einfaches Hubschraubermodell angelehnt an ein ähnliches Modell aus den original Anleitungen aus den 70ern. Kann mit den Kästen "Fischertechnik in der Schule" gebaut werden (Grundschule) mit PDF-Anleitung zum ausdrucken  