---
layout: "file"
title: "Buggy"
hidden: "true"
date: "2012-03-22T00:00:00"
file: "buggyanleitung.zip"
konstrukteure: 
- "con.barriga"
uploadBy:
- "con.barriga"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/eigene/buggyanleitungteil1.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/eigene/buggyanleitungteil1.pdf -->
Bauanleitung eines motorisierten und beleuchteten Buggys.
