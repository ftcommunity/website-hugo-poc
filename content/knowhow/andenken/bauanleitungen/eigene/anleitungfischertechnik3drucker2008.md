---
layout: "file"
hidden: true
title: "3D-Drucker 2008 "
date: "2017-02-20T00:00:00"
file: "anleitungfischertechnik3drucker2008.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/anleitungfischertechnik3drucker2008.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/anleitungfischertechnik3drucker2008.pdf -->
Der Weblink zur Anleitung des fischertechnik 3D-Druckers 2008 wurde leider entfernt,
deshalb findet ihr hier ein entsprechendes PDF von Peter Dahmen.
