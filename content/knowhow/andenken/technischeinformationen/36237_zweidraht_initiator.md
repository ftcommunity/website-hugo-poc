---
layout: "file"
hidden: true
title: "ft Näherungsschalter 36237"
date: "2007-05-20T00:00:00"
file: "36237_zweidraht_initiator.pdf"
konstrukteure: 
- "xbach"
uploadBy:
- "xbach"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/36237_zweidraht_initiator.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/36237_zweidraht_initiator.pdf -->
NAMUR Sensor 