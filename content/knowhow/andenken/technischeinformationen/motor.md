---
layout: "file"
hidden: true
title: "Maxon Motor"
date: "2009-03-13T00:00:00"
file: "motor.pdf"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/motor.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/motor.pdf -->
