---
layout: "file"
hidden: true
title: "Datenblatt Fototransistor"
date: "2008-10-10T00:00:00"
file: "32946fototransistor_3094.pdf"
konstrukteure: 
- "pinkpanter"
uploadBy:
- "pinkpanter"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/32946fototransistor_3094.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/32946fototransistor_3094.pdf -->
Hierbei handelt es sich um das Datenblatt das equester mir zugeschickt hat