---
title: "Hobby-Bücher"
weight: 15
---
In den 1970er Jahren sind insgesamt 18 Bände der Hobby-Bücher erschienen. Diese Bücher erklären die Grundlagen von einfachen und komplizierteren Modellen, die sich (nicht nur) mit den Hobby-Kästen bauen lassen.

Hier liegen diese Dokumente jetzt gescannt und als Volltext-PDFs vor. Vielen Dank an Dirk Fox.

Eine Inhaltsübersicht findet sich hier: https://ftcommunity.de/knowhow/andenken/bauanleitungen/fischertechnikinhaltbauplne/

