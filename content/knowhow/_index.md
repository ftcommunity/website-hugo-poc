---
title: "Know-how"
weight: 20
ordersectionsby: "weight"
---
Das "Gewusst Wie" ist oft unbezahlbar.
Hier findest Du es sogar kostenlos.

Für neuere Modelle und Teile finden sich Informationen oft direkt bei [fischertechnik](https://www.fischertechnik.de).

Über ältere Teile wissen oft die Fans Bescheid und teilen ihr Wissen hier mit
der restlichen Welt.
Die Rubriken sollten Dich auf den richtigen Pfad leiten, falls die Suche nichts
passendes findet.

Hier liegen auch die Dateien, die auf unserer alten Website (bis 2018) unter "Download" abgelegt waren. 
Dateien, die zu einer bestimmten ft:pedia-Ausgabe gehören, liegen [dort](https://ftcommunity.de/ftpedia/ftp-extras/).

Viel Spass beim Stöbern.
