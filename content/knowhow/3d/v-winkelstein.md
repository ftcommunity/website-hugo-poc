---
layout: "file"
title: "V-Winkelstein"
date: 2021-01-26T16:00:19+02:00
file: "v-winkelstein.zip"
hidden: true
konstrukteure: 
- "Wolfgang"
uploadBy:
- "website-Team"
license: "unknown"
---
Ich habe vor vielen Jahren den Modellbaukasten Schwerlast-Kran [https://ft-datenbank.de/ft-article/3028](https://ft-datenbank.de/ft-article/3028) als Ausstellungsstück eines Kaufhauses in Wien bekommen habe. Aber die V-Winkelplatten [https://ft-datenbank.de/ft-article/4717](https://ft-datenbank.de/ft-article/4717) waren damals nicht dabei. 
Nun habe ich diese mit dem 3D-Builder nachgezeichnet.

