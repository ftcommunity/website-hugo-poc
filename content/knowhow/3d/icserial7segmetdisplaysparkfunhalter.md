---
layout: "file"
title: "I²C-Serial 7-Segmet Display Sparkfun Halter"
date: 2017-06-01T15:00:19+02:00
file: "icserial7segmetdisplaysparkfunhalter.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/icserial7segmetdisplaysparkfunhalter.zip
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/icserial7segmetdisplaysparkfunhalter.zip -->
*.stl und *.ipt Inventor Datei  
I²C-Serial 7-Segmet Display Sparkfun Halter
