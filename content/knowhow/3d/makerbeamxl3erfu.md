---
layout: "file"
title: "Maker Beam XL 3er Fuß"
date: 2017-06-01T15:00:19+02:00
file: "makerbeamxl3erfu.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/makerbeamxl3erfu.zip
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/makerbeamxl3erfu.zip -->
\*.stl und \*.ipt Inventor Datei  
Maker Beam XL 3er Fuß für Flipper "Pirates of the Caribbian"
