---
layout: "file"
hidden: true
title: "Trimmer-Adapter - ft-Block für einen Mehrgang-Trimmer"
date: 2022-04-08T16:34:40+02:00
file: "PolarPattern.stl"
konstrukteure: 
- "Florian Bauer"
uploadBy:
- "website-team"
license: "unknown"
---

Dateien zum Forumsbeitrag https://forum.ftcommunity.de/viewtopic.php?f=15&t=7421

![Trimmer-Adapter im Einsatz](../Trimmer-Adapter.jpg)

![FreeCAD - Workspace Screenshot](../TBlock.png)

[FreeCAD-Datei](../Trimmer-Block.FCStd)

