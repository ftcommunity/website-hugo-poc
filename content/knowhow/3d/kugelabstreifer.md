---
layout: "file"
title: "Kugelabstreifer"
date: 2017-06-01T15:00:19+02:00
file: "kugelabstreifer.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/kugelabstreifer.zip
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/kugelabstreifer.zip -->
*.stl und *.ipt Inventor Datei  
Kugelabstreifer für Flipper "Pirates of the Caribbian"
