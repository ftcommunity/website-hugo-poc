---
layout: "file"
title: "I²C-Bicolor 8 x 8 LED Matrix Adafruit Halter"
date: 2017-06-01T15:00:19+02:00
file: "icbicolor8x8ledmatrixadafruithalter.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/3ddruckdateien/icbicolor8x8ledmatrixadafruithalter.zip
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/3ddruckdateien/icbicolor8x8ledmatrixadafruithalter.zip -->
*.stl und *.ipt Inventor Datei  
I²C-Bicolor 8 x 8 LED Matrix Adafruit Halter
