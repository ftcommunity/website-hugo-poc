---
layout: "file"
title: "Stromverteiler 9 Volt"
date: 2018-06-13T11:06:54+02:00
file: "stromverteiler.zip"
hidden: true
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- "/data/downloads/3ddruckdateien/stromverteiler.zip"
imported:
- "2019"
---
<!--https://ftcommunity.de/data/downloads/3ddruckdateien/stromverteiler.zip -->

\*.stl und \*.ipt Inventor Datei  
Stromverteiler 9 Volt
