---
layout: "wiki"
title: "RAL-Farben für 3D-Druck"
date: 2019-10-13T08:24:19+02:00
hidden: true
konstrukteure: 
- "Resteverwerter"
uploadBy:
- "EstherM"
license: "unknown"
---
 
Nicht nur zum 3D-Druck (und Kauf von Filament) ist es interessant zu
wissen, welche Farben fischertechnik eigentlich verwendet. 

Für die Farbkennzeichnung gibt es mit den RAL-Farben ein normiertes
Farbsystem.[^1] Leider gibt es keine Informationen von fischertechnik
direkt, sodass dies wiederholt zu Nachfragen im Forum gab. Erschwert wird
dies durch wechselnde Rottöne im Laufe der Zeit. Hier sind laut Dirk
Haizmann von fischertechnik "*notwendige Rezepturänderungen aufgrund neuer
EU-Bestimmungen*" verantwortlich, da z.B. Cadmium in Spielzeug heute nicht
mehr verwendet werden darf.[^2]

- **Rot**
     - Neurot [RAL 3031
Orientrot](https://www.ralfarbpalette.de/ral-classic/ral-3031-orientrot)
     - klassisches Rot RAL 3020 Verkehrsrot
     - dunkelrot

- **Grau**
    - ???

- **Gelb**
     - [RAL 1037
sonnengelb](https://www.ralfarbpalette.de/ral-classic/ral-1037-sonnengelb)

- **Grün**
    - [RAL 6018
Gelbgrün](https://www.ralfarbpalette.de/ral-classic/ral-6018-gelbgrun)

- **Blau**
     - ???

Die aufgeführten Farbbezeichnungen sind verschiedenen Postings aus dem
Forum entnommen worden. Fehlende Farbwerte können gerne ergänzt werden!

[^1]: Wikipedia.de: [RAL-Farbe](https://de.wikipedia.org/wiki/RAL-Farbe)
[^2]:  ft-community-Forum: [Re: Verschiedene Rottöne in den Baukästen](https://forum.ftcommunity.de/viewtopic.php?f=21&t=984&p=6535&hilit=RAL#p6562) vom 22.11.2011
