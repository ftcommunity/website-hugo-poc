---
title: "Links"
weight: 80
legacy_id:
- /wiki204c.html
- /php/wiki/38
imported:
- "2019"
---

### Weiterführende Links

* [PolarFish](https://sourceforge.net/projects/polarfish/)
    (software to keep computing experimental, computing, plotter / scanner and the famous robot kit from the 80s alive)
* [Elektronik und Mikrocontroller von Burkhard Kainka](http://www.b-kainka.de/index.htm)
* [Elektronik-Kompendium](http://www.elektronik-kompendium.de/)
* [Doku zu RoboPro von Axel Chobe](http://chobe.info/dokus/RoboProBefehle.pdf)
* [Neue Doku zu ROBO Pro Coding von Axel Chobe](http://chobe.info/dokus/RoboProCodingBefehle.pdf)

### Zubehör

* Kugellager 4mm: [Oppermann, Typ KLM 12](http://www.oppermann-electronic.de/html/body_mechanikteile.html)
* Magnet 4x2 mm: [Pollin](https://www.pollin.de/p/magnet-4x2-mm-441596)
* Felgenmitnehmer-Set Schwarz (V21071L): [Conrad](https://www.conrad.de/de/p/reely-9-mm-1-10-kunststoff-felgenmitnehmer-12-mm-6-kant-9-mm-schwarz-1-set-232728.html)
* Getriebemotor EMG30 mit eingebautem Drehgeber: [Blog](https://www.mikrocontroller-elektronik.de/tag/motor/)


### Bezugsquellen für nützliche Technik

* [Conrad Electronic Shop](https://www.conrad.de/)
* [GHW](http://www.ghw-modellbau.de)
* [LEMO-SOLAR](https://lemo-solar.de)
* [OPITEC Bastelshop](https://de.opitec.com/opitec-web/st/Home)
* [Pollin Electronic](https://www.pollin.de/)
* [reichelt elektronik](https://www.reichelt.de/)

Diese Listen sind nicht vollständig und können es nie werden. Sie stellen eine mehr oder weniger zufällige Auswahl dar und bedeuten keine Empfehlung für den Kauf bestimmter Produkte oder bei den genannten Anbietern.

