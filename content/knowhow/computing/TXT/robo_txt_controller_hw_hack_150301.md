---
layout: "file"
hidden: true
title: "TXT - Hardware"
date: "2015-03-06T00:00:00"
file: "robo_txt_controller_hw_hack_150301.pdf"
konstrukteure: 
- "Wolfi"
uploadBy:
- "Wolfi"
license: "unknown"
legacy_id:
- /data/downloads/dokumente/technischeinformationen/robo_txt_controller_hw_hack_150301.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/robo_txt_controller_hw_hack_150301.pdf -->
Für die "Hardcore"-Freunde des TXT eine erste Zusammenstellung der Hardware mit Bildern und Angabe der wesentlichsten ICs.