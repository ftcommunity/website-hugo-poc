---
title: "TXT-Controller (2014)"
weight: 60
---
Der ROBOTICS TXT Controller von fischertechnik ist das Herz unserer „Computerisierung“. 

![TXT](522429_TXT_Controller-Bildquelle-fischertechnik.jpg) (Bildquelle: [fischertechnik](https://www.fischertechnik.de/de-de/produkte/spielen/robotics/522429-robotics-txt-controller))

Ein paar Daten dazu: 

* Maße und Gewicht 90 x 90 x25 mm, 150 g
* 128 MB DDR 3 RAM, 64 MB Flash, Micro SD Kartenslot
* 32-bit ARM Cortex A8 Prozessor (600 MHz) + 32-bit Coprozessor Cortex M3; programmierbar mit ROBO Pro Software oder C-Compiler (nicht enthalten)
* Betriebssystem Linux basiert, Open Source, incl. Linux Kameratreiber, Bildverarbeitungssoftware in ROBOPro integriert 
* Schnittstellen: USB 2.0 Device: Mini USB-Buchse, USB 2.0 Host: USB-A-Buchse
* Funkschnittstelle Bluetooth: BT 2.1 EDR+4.0 oder WLAN 802.11 b / g / n
* IR-Empfangsdiode für den fischertechnik Control Set 500881
* Erweiterungsanschluss EXT Stiftleiste 10-polig; I2C 

Als Eigenentwicklung innerhalb der Community gibt es die [community firmware (cfw)](https://cfw.ftcommunity.de/ftcommunity-TXT/de/).
