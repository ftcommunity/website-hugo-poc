---
layout: "file"
hidden: true
title: "Hochregallager I2C OLED u Tastatur"
date: "2018-10-31T00:00:00"
file: "hochregallageri2c.7z"
konstrukteure: 
- "Helmes"
uploadBy:
- "Helmes"
license: "unknown"
legacy_id:
- /data/downloads/gemeinschaftsprojekte/hochregallageri2c.7z
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/gemeinschaftsprojekte/hochregallageri2c.7z -->
Das Hochregallager von Fischertechnik wird unabhängig vom PC gesteuert und bedient. Beides wird von einem - mit etwas Hardware erweiterten - ftDuino erledigt. Infos für den Benutzer werden mittels OLED Display ausgegeben, Werte werden mit einer Matrixtatstatur eingegeben.
Die Hardware besteht neben dem Modell selbst, aus den ftcommunity-Komponenten ftDuino, ftExtender und ftOLED sowie einem IO Expansion Board und einer 4x4 Matrix-Tastatur.
Das Programm selbst ist eng angelehnt an das von der ftc veröffentlichte Beispiel 'HighLevelRack'. 