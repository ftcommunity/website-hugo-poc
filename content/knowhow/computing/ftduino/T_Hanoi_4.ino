
// 27.01.2024

// STEM Pneumatic, PROjektmodell, Turm von Hanoi mit 3 oder 4 Scheiben
// Aenderungen: Drehung mit Encoder_Motor, Federzylinder, E-Magnet

// I1	        Endschalter	
// I3         Start-Taster 3 Scheiben
// I4         Start-Taster 4 Scheiben
// O1         Kompressor
// O2         Magnetventiel, Arm hoch
// O3         E-Magnet
// M4, C4     Encoder-Motor, drehen

#include <Ftduino.h>
int P{};   // Position

void setup() {        
  ftduino.init();
  ftduino.input_set_mode(Ftduino::I1, Ftduino::SWITCH);
  ftduino.input_set_mode(Ftduino::I3, Ftduino::SWITCH);
  ftduino.input_set_mode(Ftduino::I4, Ftduino::SWITCH);
  ftduino.motor_counter_set_brake(Ftduino::M4, true);

  P = 4; // unbestimmte Position
  // drehe zur Start-Position (2)
  ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX); // Kompressor an
  delay(1000);                                  
  if (!ftduino . input_get ( Ftduino :: I1))  Position(1);
     else P = 1;
  Position(2);
  ftduino.output_set(Ftduino::O1, Ftduino::LO, Ftduino::MAX); // Kompressor au  
}  

void loop() {
  if((ftduino.input_get(Ftduino::I3))||(ftduino.input_get(Ftduino::I4))) {
    ftduino.output_set(Ftduino::O1, Ftduino::HI, Ftduino::MAX); // Kompressor an
    
    if(ftduino.input_get(Ftduino::I3)) {   // Start-Taster, 3 Scheiben 
      delay(1000);                    
      mS(1,3); mS(1,2); mS(3,2); mS(1,3); mS(2,1); mS(2,3); mS(1,3);}

    if(ftduino.input_get(Ftduino::I4)) {   // Start-Taster, 4 Scheiben 
      delay(1000);
      mS(1,2); mS(1,3); mS(2,3); mS(1,2); mS(3,1); mS(3,2); mS(1,2);
      mS(1,3); mS(2,3); mS(2,1); mS(3,1); mS(2,3); mS(1,2); mS(1,3); 
      mS(2,3); }
    
    Position(2);
    ftduino.output_set(Ftduino::O1, Ftduino::LO, Ftduino::MAX); // Kompressor aus
  }
}

void mS(int von, int nach) {        // Prozedur: bewege Scheibe von, nach
  Position(von);
  ftduino.output_set(Ftduino::O3, Ftduino::HI, Ftduino::MAX/2);  // Magnet an 
  delay(500); 
  Position(nach);
  ftduino.output_set(Ftduino::O3, Ftduino::LO, Ftduino::MAX/2);  // Magnet aus 
  delay(500); 
}
 
void Position(int Pn)  {        // Prozedur: Position anlaufen 
 ftduino.output_set(Ftduino::O2, Ftduino::HI, Ftduino::MAX);  // Arm hoch
 delay(1000);   
 
 if (P==1) if (Pn==2) {
       ftduino.motor_counter(Ftduino::M4, Ftduino::LEFT, Ftduino::MAX, 412); 
       while(ftduino.motor_counter_active(Ftduino::M4));  }
       else {	
         ftduino.motor_counter(Ftduino::M4, Ftduino::LEFT, Ftduino::MAX, 824); 
         while(ftduino.motor_counter_active(Ftduino::M4)); }

  if (P==2) if (Pn==1) {
       Endschalter();}
       else {
         ftduino.motor_counter(Ftduino::M4, Ftduino::LEFT, Ftduino::MAX, 412); 
         while(ftduino.motor_counter_active(Ftduino::M4));  }

  if (P==3) if (Pn==2) {
         ftduino.motor_counter(Ftduino::M4, Ftduino::RIGHT, Ftduino::MAX, 412); 
         while(ftduino.motor_counter_active(Ftduino::M4));  }
         else Endschalter();

  if (P==4 && Pn==1) Endschalter();
  
  P = Pn;     
  ftduino.output_set(Ftduino::O2, Ftduino::LO, Ftduino::MAX);  // Arm runter
  delay(1000);
}

void Endschalter()  {		// Prozedur: fahre zum Endschalter
   ftduino.motor_set(Ftduino::M4, Ftduino::RIGHT, Ftduino::MAX);
   while(!ftduino.input_get(Ftduino::I1));    
   ftduino.motor_set(Ftduino::M4, Ftduino::BRAKE, Ftduino::MAX);
   P = 1;	
}
