---
layout: "file"
hidden: true
title: "Turm von Hanoi mit Schneckengetriebe"
date: 2024-07-12T09:49:18+02:00
file: "T_Hanoi_4.ino"
konstrukteure: 
- "Jürgen Vollmer"
uploadBy:
- "Website-Team"
license: "unknown"
---

Beide Programme zum Turm von Hanoi sind bis auf die Impulsvorgaben für den Encodermotor identisch. 
Im 'setup' wird unter anderem der Drehkranz zum Endschalter gefahren, um den Bezugspunkt festzulegen und dann wird der Arm zur Startposition bewegt.
Im Hauptprogramm wird auf die Eingabe für 3 oder 4 Scheiben  gewartet.
Die Zugfolgen sind fest vorgegeben. 
Die Prozedur 'ms' mit den Parametern (von, nach) vollzieht einen Zug mit zur Hilfenahme der Prozeduren 'position' oder 'Endschalter'.

In Wikipedia wird die klassische rekursive Methode und eine iterative Methode 
beschrieben.

Ein Bild des Modells findet sich unter https://ftcommunity.de/bilderpool/modelle/roboter-industrieanlagen-computing/turm-hanoi-ftduino/50411/
