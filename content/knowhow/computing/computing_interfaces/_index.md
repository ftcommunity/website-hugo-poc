---
title: "Computing Interfaces (1985)"
weight: 20
---
![CBM Interface 30561](./30561.png "30561 - Interface für CBM 4xxx/8xxx - Mit freundlicher Erlaubnis von fischerfriendsman")
Mit diesen Interfaces fing alles an!
Die erste Interface-Generation zum Anschluss an seinerzeit gängige Computer.
Wer von den etwas älteren Damen und Herren kennt sie nicht noch aus seiner
Jugend?
C64, Apple II und Schneider CPC erfreuten sich grosser Beliebtheit.
Und Dank der Innovation aus Waldachtal-Tumlingen konnten sich Programmierer
schon damals mit ihrem Homecomputer und fischertechnik Bausteinen in der
realen Welt "austoben".
Selbst heuzutage können die alten Interfaces an modernen
Experimentiercomputern (z. B. RaspberryPi, Arduino) weiter betrieben werden.

Fans haben die alten Interfaces mittlerweile detailgenau dokumentiert.
Ein MUSS für jeden der einen Blick in die Technik werfen möchte.
Und hilfreich falls, trotz aller Vorsicht, mal was kaputt geht.

Schaltpläne:  
<!-- Upload von Holger Howey, Mai 2005-->
[Historisch](./ParallelInterface.zip) - Für alle Interfaces, leider noch mit Lücken  
<!-- Uploads von H.A.R.R.Y., April 2017 => war mal alles in der alten ftdb,
ist jetzt aber in der neuen ftdb wieder weg! *Ganz grosses* :( -->
[30520](./ft30520_schematic.pdf) - Universal  
[30561](./ft30561_schematic.pdf) - CBM4xxx / CBM8xxx  
[30562](./ft30562_schematic.pdf) - VC20 / C64 / C128  
[30563](./ft30563_schematic.pdf) - Apple II  
[30564](./ft30564_schematic.pdf) - Acorn  
[30565](./ft30565_schematic.pdf) - CPC464 / CPC 664 / CPC6128  
[30566](./ft30566_schematic.pdf) - Centronics  
[30567](./ft30567_schematic.pdf) - IBM-PC  
[39319](./ft39319_schematic.pdf) - Centronics Universal  
[66843](./ft66843_schematic.pdf) - Universal, CVK-Version

Schaltungsbeschreibungen:  
[ft:pedia 2 / 2017](../../../ftpedia/2017/2017-2/ftpedia-2017-2.pdf),
S. 63, V. I. P. - Teil 1 (Einführung und jede Menge Steckerbelegungen)  
[ft:pedia 3 / 2017](../../../ftpedia/2017/2017-3/ftpedia-2017-3.pdf),
S. 57, V. I. P. - Teil 2 (Stromversorgung und Motortreiber)  
[ft:pedia 4 / 2017](../../../ftpedia/2017/2017-4/ftpedia-2017-4.pdf),
S. 36, V. I. P. - Teil 3 (Eingänge und Watchdog)  

