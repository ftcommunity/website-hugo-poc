---
title: "TX-Controller (2009)"
weight: 50
---

![TX-Controller](TXController.jpg)

Dies ist der Vorgänger des aktuellen Robo TXT Controllers, vorgesehen insbesondere zur Steuerung von fischertechnik-Modellen der Computing-Reihe. An ihn können zahlreiche Aktoren (Motoren, Lampen, Magnetventile, etc.) und Sensoren (Farbsensor, Taster, Lichtschranken, usw.) angeschlossen werden.
Ein paar Daten dazu:

- Größe 90 × 90 × 15 mm
- Prozessor: Atmel AT91SAM9260, 32-bit, ARM9E, 200 MHz
- Speicher: 8MB Ram, 2MB Flash
- Display: 128×64 Pixel, monochrom, 4,3 cm
- Ein- und Ausgänge: 
    - 8 Universaleingänge: Digital/Analog 0–9 V, Analog 0–5 kΩ
    - 4 schnelle Zähleingänge: Digital, Frequenz bis 1 kHz
    - 4 Motorausgänge 9 V/250 mA: Geschwindigkeit stufenlos regelbar, kurzschlussfest, alternativ 8 Einzelausgänge
- Schnittstellen: Bluetooth 2.0, I²C, RS-485
