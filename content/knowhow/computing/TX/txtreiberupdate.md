---
layout: "file"
hidden: true
title: "TX-Treiber"
date: "2017-02-23T00:00:00"
file: "txtreiberupdate.rar"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/txtreiberupdate.rar
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/txtreiberupdate.rar -->
Dies ist ein Update der TX-Treiber vom 17.1.2016. 
Alle Dateien sollten nach system32 kopiert werden. 
Eine Installation von Visual Studio ist nicht mehr nötig. 
Der TX-Controller (v1.30) kann damit z.B. mit c++, Terrapin Logo oder Java angesteuert werden. 
TX.dll ist eine Erweiterung von umfish50.dll (1.1.2010) von Ulrich Müller. [Hier](https://www.ftcommunity.de/ftComputingFinis/index.html) ist die zugehörige Seite zu finden.
