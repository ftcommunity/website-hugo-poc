---
layout: "wiki"
title: "Beispiel für Ampelprogrammierung"
hidden: true
date: 2020-03-26T15:44:36+01:00
konstrukteure: 
- "EstherM"
uploadBy:
- "EstherM"
license: "unknown"
---

Dies ist ein Beispiel für eine einfache Fußgängerampel.

![Ampel1](../Ampel01.jpg)

Im nächsten Beispiel ist die Fußgängerampel immer aktiv, weil unten am Ende des Programmes eine Verbindung nach oben geht, so dass der Ablauf wieder von oben nach unten läuft. 

![Ampel1a](../Ampel1a.jpg)
