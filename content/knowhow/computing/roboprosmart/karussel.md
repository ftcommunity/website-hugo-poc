---
layout: "wiki"
title: "Karusell"
hidden: true
date: 2020-03-26T15:56:56+01:00
konstrukteure: 
- "EstherM"
uploadBy:
- "EstherM"
license: "unknown"
---

Der Motor für ein Karusell soll sich 8 Sekunden lang in eine Richtung drehen. Dann wird er für 4 Sekunden ausgeschaltet und dann wieder für 8 Sekunden laufen, aber in die andere Richtung, also mit negativer Geschwindigkeit.

![Karussel1](../Karussel1.jpg)

Im nächsten Beispiel wiederholt das Karussel den Ablauf aus dem vorigen Beispiel 3 mal.

![Karusell1a](../Karussel1a.jpg)
