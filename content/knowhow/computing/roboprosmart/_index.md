---
title: "ROBO Pro Smart"
weight: 90
---
ROBO Pro Smart ist eine App. Sie gibt es für Android und iOS kostenlos zum Download. Mit dieser App können Modelle des BT Smart Beginner Sets programmiert werden.

Der BT Smart Controller hat 2 Ausgänge für Motoren oder Lampen (sie heißen M1 und M2) und 4 Eingänge für Taster oder Lichtschranken (sie heißen I1, I2, I3 und I4).

Hier gibt es einige Beispiele für Programme.
Diese ersetzen nicht die Informationen im didaktischen Begleitheft, das es bei fischertechnik zum [Download](https://www.fischertechnik.de/-/media/fischertechnik/fite/service/elearning/spielen/bt-smart-beginner-set/smart-beginner-set_de.ashx) gibt.
