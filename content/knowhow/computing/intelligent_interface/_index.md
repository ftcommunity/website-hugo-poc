---
title: "Intelligent Interface (1997)"
weight: 30
---
Nach den Computing Interfaces folgt der nächste evolutionäre Schritt.
Ein Interface mit eigenem Mikrocomputer.
Weil der Computer jetzt eingebaut ist, lassen sich erstmals mobile Fahrzeuge
konstruieren und programieren ohne dass Kabel hinterherschleifen.

Siehe auch [ft-Datenbank](https://ft-datenbank.de/tickets?fulltext=%22intelligent+interface%22&format=grid).
