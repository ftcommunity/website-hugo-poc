---
layout: "file"
hidden: true
title: "I²C-Treiber für VCNL4010 Entfernungs- und Lichtsensor"
date: "2017-02-03T00:00:00"
file: "i2cvcnl4010entfernungundlichtsensorversion1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cvcnl4010entfernungundlichtsensorversion1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cvcnl4010entfernungundlichtsensorversion1.0.rpp -->
Der VCNL4010 Sensor ist ideal, wenn sie Ihrem fischertechnik-Projekt einen Näherungssensor für kurze Distanzen von 1 bis 200 mm hinzufügen wollen. Auserdem verfügt er auch über einen eingebauten Lichsensor.

Programmiert in ROBO Pro V4.2.3