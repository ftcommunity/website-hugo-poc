---
layout: "file"
hidden: true
title: "I²C-Treiber v1.0 für 7-Segment LED-Display mit ATMega328"
date: "2017-01-12T00:00:00"
file: "ledatmega328_v10.zip"
konstrukteure: 
- "Georg Stiegler (fantogerch)"
uploadBy:
- "Georg Stiegler (fantogerch)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ledatmega328_v10.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ledatmega328_v10.zip -->
RoboPro-Treiber für 7-Segment LED-Displays mit ATMega328 <a target="_blank" 
href="https://github.com/sparkfun/Serial7SegmentDisplay/wiki/Hardware%20Specifications">Serial 7-Segment display</a>. Zusätzlich ein 
Programm zum ändern der I²C-Adresse, sowie ein Programm zum identifiziern der I²C-Adresse falls vergessen oder verstellt.