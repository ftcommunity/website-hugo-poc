---
layout: "file"
hidden: true
title: "Flipper mit 2x I2C-Bus LED Display (Conrad # 198344) + 1x LCD Anzeige (Conrad # 198330) "
date: "2017-02-03T00:00:00"
file: "flipperpd2.1.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/flipperpd2.1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/flipperpd2.1.rpp -->
Flipper funtioniert jetzt mit 2x I2C-Bus LED Display (Conrad # 198344) + 1x LCD Anzeige (Conrad # 198330) . 

LED Display- nr2 mit Jumper 2 wird im Robo Pro Programm mit Adresse 0x39 ansprochen.
