---
layout: "file"
hidden: true
title: "Hochregallager Version 3.5 von nula"
date: "2017-02-03T00:00:00"
file: "hrl3.5nula.rpp"
konstrukteure: 
- "nula"
uploadBy:
- "nula"
license: "unknown"
legacy_id:
- /data/downloads/robopro/hrl3.5nula.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/hrl3.5nula.rpp -->
Dies ist die neue, überarbeitete und letzte Version meines Hochregallagers, wie es auf der Convention 2008 zu sehen war.
