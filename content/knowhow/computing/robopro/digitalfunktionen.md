---
layout: "file"
hidden: true
title: "Ein paar Unterprogramme mit digital Versionen V1.0 :)"
date: "2007-06-07T00:00:00"
file: "digitalfunktionen.rpp"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/digitalfunktionen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/digitalfunktionen.rpp -->
HA, VA, NOR, NAND, XOR, XNOR, RS-FF können so als Bausteine direkt genutzt werden. Am Besten in das Verzeichnis "Bibliothek" von RoboPro kopieren, dann erscheinen die Symbole automatisch in RoboPro...
