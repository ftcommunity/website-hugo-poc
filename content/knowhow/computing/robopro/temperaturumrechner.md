---
layout: "file"
hidden: true
title: "Umrechnung IF-Wert -> Temperatur"
date: "2017-02-03T00:00:00"
file: "temperaturumrechner.rpp"
konstrukteure: 
- "equester"
uploadBy:
- "equester"
license: "unknown"
legacy_id:
- /data/downloads/robopro/temperaturumrechner.rpp
imported:
- "2019"
---

<!-- https://ftcommunity.de/data/downloads/robopro/robopro/temperaturumrechner.html -->
Das Programm wandelt den analogen Wert des NTC-Widerstands in die passende Temperatur um. Dabei verwendet es Listen, die sich auf meine Messwerten zum NTC berufen (siehe Downloads "NTC-Wärmesensor ft").
