---
layout: "file"
hidden: true
title: "PneuHoogte Cosinus"
date: "2014-02-07T00:00:00"
file: "pneuhoogtecosinus.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pneuhoogtecosinus.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pneuhoogtecosinus.rpp -->
Ein Robopro-Programm für eine Positionierung mit Potmeter zum Cosinus-Bewegung wie im Natur. 
Pneumatisch weil es mit eine -Kolben-Zylinder oder eine &#8211;Muskel grossere und schnellere modellen möglich sein. 
