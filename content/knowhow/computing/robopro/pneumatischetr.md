---
layout: "file"
hidden: true
title: "pneumatische Tür"
date: "2017-09-29T00:00:00"
file: "pneumatischetr.rpp"
konstrukteure: 
- "Alwin (fischertechniker)"
uploadBy:
- "fischertechniker (Alwin)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pneumatischetr.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pneumatischetr.rpp -->
https://ftcommunity.de/categories.php?cat_id=3430
