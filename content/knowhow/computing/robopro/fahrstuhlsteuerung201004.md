---
layout: "file"
hidden: true
title: "ROBO Pro Programm Fahrstuhlsteuerung; siehe: Bilder Aufzug (Dirk Fox)"
date: "2017-02-03T00:00:00"
file: "fahrstuhlsteuerung201004.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/fahrstuhlsteuerung201004.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/fahrstuhlsteuerung201004.rpp -->
Das ROBO Pro Programm steuert mit einem Robo TX Controller einen Aufzug (drei Stockwerke) mit automatischen Schiebetüren, Lichtschranken und Ruftasten. 
Anschlussbelegung des Robo TX Controllers: siehe Beschreibung Bild "Gesamtansicht Aufzug mit Steuerung" unter "Aufzug (Dirk Fox)", Kategorie "Aufzüge".