---
layout: "file"
hidden: true
title: "Codekarten-Leser mit automatischem Einzug"
date: "2017-02-03T00:00:00"
file: "codekartenlesermitautomatischemeinzug.rpp"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/robopro/codekartenlesermitautomatischemeinzug.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/codekartenlesermitautomatischemeinzug.rpp -->
Das Programm für den Codekarten-Leser. Kann auch einfahcer gelöst werden. Aber so kann man sich mit dem Programm vertraut machen.
