---
layout: "file"
hidden: true
title: "I²C-Treiber für einen intelligenten 9-achsen absolut orientierten Sensor von Bosch (BNO055) V1"
date: "2018-02-25T00:00:00"
file: "i2cbno055version1.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cbno055version1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cbno055version1.rpp -->
Dank dem verbauten 32-bit cortex M0+ Microcontroller stehen folgende Sensor fusion daten von den 3 Sensoren (Accelerometer, Magnetometer, Gyroscope) zur Verfügung:
Quaternion, Euler Winkel, Beschleunigung, Winkelgeschwindigkeit, Heading, Kompass
