---
layout: "file"
hidden: true
title: "Spiel"
date: "2017-02-03T00:00:00"
file: "zhler1000.rpp"
konstrukteure: 
- "Johannes Deppert"
uploadBy:
- "Johannes Deppert"
license: "unknown"
legacy_id:
- /data/downloads/robopro/zhler1000.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/zhler1000.rpp -->
Dass ist ein Spiel bei dem man schnell reagieren muss. Man spielt es im Bedienfeld. Dort ist auch eine Anleitung zu dem Spiel.