---
layout: "file"
hidden: true
title: "Mein Codeschloss mit Alarmcode"
date: "2017-02-03T00:00:00"
file: "codeschloss.rpp"
konstrukteure: 
- "Hobbyprogrammer"
uploadBy:
- "Hobbyprogrammer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/codeschloss.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/codeschloss.rpp -->
Codeschloss mit Alarmcode einfach Änderbar Beschreibung in der RoboPro Datei  

Beschreibung im Programm(Auch mit Tastern nutzbar um z.B. einen Tresor zu öffnen)  

Bilder hier 
http://www.ftcommunity.de/categories.php?cat_id=1598. 
Beschreibung unter Regestrierkarte Beschreibung im Programm.
