---
layout: "file"
hidden: true
title: "ABS-Antiblockersystem"
date: "2009-06-20T00:00:00"
file: "abs2.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/abs2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/abs2.rpp -->
Beim heutigen ABS-Programm FTC-UM:
http://www.ftcomputing.de/abs.htm 
gibt es nur Scheibe Halt | Dreht | Halt | Dreht | Halt | Dreht ....(=Stotterbrems), ...usw. , ohne das die Reife nach Z.b. 8 sec wirklich  halt wie es normal bei ABS in wirklichkeit gibt.

Mit eine Auslauf-Schleife (lokale Variable) über Geschwindigkeit M2 is das Modell etwas realistischer.

In Praxis ist alles doch nicht so einfach, da auch der Brems(Luft)druck dosiert wird. 
Gruss, Peter 
