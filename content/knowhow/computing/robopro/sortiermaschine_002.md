---
layout: "file"
hidden: true
title: "Programm für angehende Sortiemaschine"
date: "2008-02-03T00:00:00"
file: "sortiermaschine_002.rpp"
konstrukteure: 
- "Frank Hanke (franky)"
uploadBy:
- "Frank Hanke (franky)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sortiermaschine_002.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sortiermaschine_002.rpp -->
Im Augenblick lässt sich das Programm nur im Onlinemodus betreiben. Beim Download und Start blinkt die Fehler LED und die COM LED.
Lt Liste : "Power On Error: Interner Ram Test2". Dabei ist das Programm längst nicht fertig. Vielleicht weiss jemad Rat
