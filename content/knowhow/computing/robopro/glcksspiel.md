---
layout: "file"
hidden: true
title: "Spiel mit Glückszahlen, kein IF benötigt"
date: "2017-02-03T00:00:00"
file: "glcksspiel.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/robopro/glcksspiel.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/glcksspiel.rpp -->
Nach Drücken der "Insert Coin" Taste wird das Spiel gestartet.
Drücken sie die "Stopp" Taste, um die erste, zweite und dann die dritte Zufallszahl zu generieren. Stimmen zwei oder drei Zahlen überein, wird dies kommentiert.