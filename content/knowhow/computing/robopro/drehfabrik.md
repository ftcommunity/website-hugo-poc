---
layout: "file"
hidden: true
title: "Programm zur pneumatischen Fabrik"
date: "2017-02-03T00:00:00"
file: "drehfabrik.rpp"
konstrukteure: 
- "Johannes Deppert"
uploadBy:
- "Johannes Deppert"
license: "unknown"
legacy_id:
- /data/downloads/robopro/drehfabrik.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/drehfabrik.rpp -->
Dass ist das Programm zu meiner pneumatischen Fabrik.