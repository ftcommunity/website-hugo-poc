---
layout: "file"
hidden: true
title: "Druckregulierung Pneumatik Muskel  stabil  machen"
date: "2017-02-03T00:00:00"
file: "pneumatikmuskel.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pneumatikmuskel.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pneumatikmuskel.rpp -->
Ich versuche mit Robo-TX eine stabiele Druckregulierung für eine Pneumatik Muskel zu programieren.
Ziel ist mit ein Pot-Meter eine stabile Luftdruck zu realisieren, damit die Pneumatik Muskel eine feste einstellbare Verkurzung hat. 
Es gibt eine feste Zusammenhang schwischen den Druck und die Länge einer Pneumatik Muskel.
 
Problem is aber :
Fast immer gibt es eine Abweichung zwischen der Potmeter-einstellung und der Drucksensor.
Das Programm wird schell instabil. In die Niederlände sagt man:  das Programm "Jo-Jo-t".
 
Beim Pneumatik Muskel  gibt es dann immer Pressluft-Zufür via O1  oder  Pressluf-Abfür via O2.
Eine Druck-range (etwa 200 mBar = 0,2 Bar) wobei gar nichts passiert wäre o.k.  
Wenn das Unterschied (Differenz) zwischen die Drucksensor-druck und Pot-meter grösser ist als 200 mBar, nur dann muss Pressluft zu- oder abgeführt werden.
 
Wie ist solch eine stabiele Regulierung realisierbar ?      ........es gibt in Robo-Pro kein standard -Vergleichung für dieses Problem.
