---
layout: "file"
hidden: true
title: "I2C Treiber für den Ultraschall Entfernungsmesser SRF10, neue Version 1.1"
date: "2017-02-03T00:00:00"
file: "usrangefindersrf10_v11.rpp"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/usrangefindersrf10_v11.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/usrangefindersrf10_v11.rpp -->
Dies ist die Version 1.1 des I2C-Treibers für den Ultraschall Entfernungsmesser SRF10 von Devantech. Bitte die erste Version nicht mehr nutzen!
Das Modul kann z.B. hier: http://www.shop.robotikhardware.de
 ... gekauft werden.