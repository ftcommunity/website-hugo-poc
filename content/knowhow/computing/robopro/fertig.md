---
layout: "file"
hidden: true
title: "Sparschwein-Überblick"
date: "2011-06-07T00:00:00"
file: "fertig.rpp"
konstrukteure: 
- "Fischer fan"
uploadBy:
- "Fischer fan"
license: "unknown"
legacy_id:
- /data/downloads/robopro/fertig.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/fertig.rpp -->
Ein Programm mit dem man das Geld im Sparschwein besser überblicken oder zählen kann.
Wenn man den selben Quell- und Speicherort für die Liste angibt, bleiben die Werte gespeichert. Einfach im Bedienfeld im Hauptprogramm die Taster anklicken; kein Interface / TX benötigt. Neben den Knöpfen sind zwei Anzeigen, die obere zeigt die Anzahl der Münzen (oder Scheine), die untere den Wert in Euro. Wer Fehler findet, darf sie behalten oder mir bescheidgeben :-)

Viel Spaß,
Fischer fan