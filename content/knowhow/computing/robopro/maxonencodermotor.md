---
layout: "file"
hidden: true
title: "Maxon-Encoder-Getriebemotoren mit heutigen Robo Interface"
date: "2017-02-03T00:00:00"
file: "maxonencodermotor.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/maxonencodermotor.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/maxonencodermotor.rpp -->
Mit dem heutigen Robo Interface funktionieren die Encoder-Getriebemotoren (schon) gut mit "Channel A" oder "Channel B" als analogen A1 Spannungs-eingang und ein Impulszähler.
