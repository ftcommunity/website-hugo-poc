---
layout: "file"
hidden: true
title: "Tempautomat Steuerung"
date: "2007-12-13T00:00:00"
file: "tempoautomat_2.2.rpp"
konstrukteure: 
- "Frank Hanke (franky)"
uploadBy:
- "Franky (Frank Hanke)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/tempoautomat_2.2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/tempoautomat_2.2.rpp -->
Damit soll der Temptaschentuchautomat (Clubmodell aus 2004) gesteuert werden. Leider funktioniert das Blinklicht nicht im Offlinemods. Ist das Interface mit dem PC verbunden, klappt alles wunderbar. Nach dem runterladen, blinken die Lampen nicht. 
