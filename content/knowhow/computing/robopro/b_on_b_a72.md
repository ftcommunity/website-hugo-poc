---
layout: "file"
hidden: true
title: "Ball on a beam - Ballbalancierer eindimensional"
date: "2017-02-03T00:00:00"
file: "b_on_b_a72.rpp"
konstrukteure: 
- "techum - FTmodels"
uploadBy:
- "techum / FTmodels"
license: "unknown"
legacy_id:
- /data/downloads/robopro/b_on_b_a72.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/b_on_b_a72.rpp -->
Programm zu Youtube Video 
https://www.youtube.com/watch?v=hqqrcrJ-XgY

für TXT Controller mit Kamera

Es ist sicher noch mehr drin, wenn man sich etwas mit PID auskennt, was bei mir nicht der Fall ist.
