---
layout: "file"
hidden: true
title: "I2C-Output 5- 24 V 8-bit 0x20"
date: "2017-07-16T00:00:00"
file: "i2coutput524v8bit0x20.rpp"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2coutput524v8bit0x20.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2coutput524v8bit0x20.rpp -->
Mit der I2C-Ausgabekarte können 8 digitale Verbraucher geschaltet werden. 
Der Ausgangstreiber UDN2981 schaltet eine positive Spannung bis zu 30V. 
Bausatz Bezugsquelle: http://www.horter.de/shop/index.html
