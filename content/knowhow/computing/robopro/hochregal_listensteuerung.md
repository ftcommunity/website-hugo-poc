---
layout: "file"
hidden: true
title: "Listensteuerung Hochregallager"
date: "2017-02-03T00:00:00"
file: "hochregal_listensteuerung.rpp"
konstrukteure: 
- "Martin W."
uploadBy:
- "Martin W."
license: "unknown"
legacy_id:
- /data/downloads/robopro/hochregal_listensteuerung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/hochregal_listensteuerung.rpp -->
Eine Erweiterung des ft-Beispielprogramms "Hochregal" aus Automation Robots. Die belegten Fächer werden in einer Liste (UP Liste_Faecher) gespeichert (0 oder 1). Entsprechend des gedrückten Tasters wird ein belegtes oder unbelegtes Fach gesucht (UP Fachsuche) und an das UP Platz_anfahren ausgegeben.
