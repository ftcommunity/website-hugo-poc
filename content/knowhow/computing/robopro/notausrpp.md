---
layout: "file"
hidden: true
title: "Not-Aus Programm"
date: "2013-09-14T00:00:00"
file: "notaus.rpp"
konstrukteure: 
- "Deleted User"
uploadBy:
- "Deleted User"
license: "unknown"
legacy_id:
- /data/downloads/robopro/notaus.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/notaus.rpp -->
Das Not-Aus Programm stoppt alle Ausgänge wo das UP zwischen Befehl und Ausgang gesetzt ist. Dies ist sehr nützlich wenn z.B. ein Kran etwas an einem E-Magnet hochheben soll und bei Not-Aus nicht fallen lassen soll. Es ist egal wie schnell und wann eine Komponente ein- oder ausgeschaltet wird.
