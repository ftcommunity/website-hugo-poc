---
layout: "file"
hidden: true
title: "Krooshek-reiniger-programma (verbeterd) / Grobrechen"
date: "2017-02-03T00:00:00"
file: "krooshekreiniger.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen"
license: "unknown"
legacy_id:
- /data/downloads/robopro/krooshekreiniger.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/krooshekreiniger.rpp -->
Krooshekreiniger:

Ich arbeite beim Wasserverband "Rivierenland" in die Niederlände. 
Beim unseren Wasserverband gibt es verschiedene automatischen Grobrechen; grosse und kleinere, und auch Portalreiniger. 
Die sorgen dafür, dass allerlei Treibsel, Pflanzen, Holz, Äste, Kunststoff usw. nicht in die teueren Pumpanlagen kommt. 
Auch "Trocken-pumpen" durch zu viel angeschwemmtes Material, wirt so nicht passieren.

Von Zeit zu Zeit wirt automatisch das angeschwemmtes Material vom Grobrechen entfernt. 
Auch wenn beim pumpen die Wasserständ-differenz zwischen vor und hinter das Grobrech zu gross ist, 
wirt automatisch das angeschwemmtes Material vom Grobrechen entfernt. 
Der Rechen wirt in 3 Schritten über die volle Länge gereinigt.

Verschiedene Bilder meiner "krooshekreiniger" gibt es beim FT_Community:  

ftCommunity / Bilderpool / Modelle / Sonstiges / Grobrechen  

(oder schau mal unter:
http://www.ftcommunity.de/categories.php?cat_id=358&page=2)

Peter Damen
Poederoyen (entlang die Maas und Waal/Rhein)
die Niederlande
