---
layout: "file"
hidden: true
title: "Gewächshausprogramm"
date: "2017-02-03T00:00:00"
file: "gewchshausohnealarm.rpp"
konstrukteure: 
- "Nils (fitec)"
uploadBy:
- "fitec"
license: "unknown"
legacy_id:
- /data/downloads/robopro/gewchshausohnealarm.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/gewchshausohnealarm.rpp -->
Das ist das Programm für mein Gewächshaus wie ich es gerade benutze. Die Alarmfunktion wurde weggemacht. Außerdem wurde die nicht funktionierende Extragießenfunktion so verändert, dass nun jede einzelne Pflanze belibig gegossen werden kann.
