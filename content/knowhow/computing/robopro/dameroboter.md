---
layout: "file"
hidden: true
title: "Minimax-Algorithmus"
date: "2017-02-03T00:00:00"
file: "dameroboter.rpp"
konstrukteure: 
- "Jan (kehrblech)"
uploadBy:
- "Jan (kehrblech)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dameroboter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dameroboter.rpp -->
Hier jetzt die funktionierende Variante des Algorithmus   

Der Versuch den Minimax-Algorithmus mit RoboPro zu programmieren.
