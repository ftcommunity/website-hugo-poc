---
layout: "file"
hidden: true
title: "ball on a plate - Ballbalancierer zweidimensional"
date: "2017-02-03T00:00:00"
file: "b_on_p_2.rpp"
konstrukteure: 
- "techum - FTmodels"
uploadBy:
- "techum / FTmodels"
license: "unknown"
legacy_id:
- /data/downloads/robopro/b_on_p_2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/b_on_p_2.rpp -->
Robo Pro Programm zu Youtube Video
https://www.youtube.com/watch?v=1ObdFOjeSV4

Das Programm nutzt in zwei Dimensionen den Algorithmus des ball on a beam.
Leichte Unterschiede bei der Auswertung der Ballgeschwindigkeit und insbesondere der Richtung..

Meine Programmiererfahrung mit Robopro hält noch sich sehr in Grenzen, auch habe ich mich mit der PID Controller Theorie nicht auseinandergesetzt. Es geht also sicher viel besser.
Der Aufbau ist simpel, vielleicht hat ja jemand Lust das ganze zu verbessern, so dass es dann so funktioniert:
https://www.youtube.com/watch?v=uERF6D37E_o
