---
layout: "file"
hidden: true
title: "Förderband für Bastel-Freak mit Geldeinwurf und Timer (Andere Möglichkeit)"
date: "2017-02-03T00:00:00"
file: "frderbandmittimer.rpl"
konstrukteure: 
- "McDoofi"
uploadBy:
- "McDoofi"
license: "unknown"
legacy_id:
- /data/downloads/robopro/frderbandmittimer.rpl
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/frderbandmittimer.rpl -->
Das Förderbandprogramm, das mit RoboPro entworfen wurde, aber als RoboPro Light Datei für Bastel-Freak gespeichert wurde)