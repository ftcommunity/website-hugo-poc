---
layout: "file"
hidden: true
title: "ProgrammHochregallager"
date: "2018-01-24T00:00:00"
file: "hochregallager.rpp"
konstrukteure: 
- "Techniker"
uploadBy:
- "Techniker"
license: "unknown"
legacy_id:
- /data/downloads/robopro/hochregallager.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/hochregallager.rpp -->
Dieses Programm bringt mich zur Weißglut! Im UP Einlagern wird der Motor M3 
nicht gestartet ( laut Interfacetest sollte er laufen), das Programm wartet dann am Taster,
der allerdings nicht kommt, da sich ja nichts bewegt. An der Hardware kanns nicht liegen,
der Roboter fährt einwandfrei in die Grundposition. Kann mir wer von euch da helfen?

Grüße

Techniker