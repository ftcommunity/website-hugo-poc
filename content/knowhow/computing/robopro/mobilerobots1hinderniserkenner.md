---
layout: "file"
hidden: true
title: "Mobile Robots I (Hinderniserkenner)"
date: "2017-02-03T00:00:00"
file: "mobilerobots1hinderniserkenner.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/mobilerobots1hinderniserkenner.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/mobilerobots1hinderniserkenner.rpp -->
Das ist ein Programm für die die nur Robo Pro haben. Ich habe das gemacht weil ich auch nur Robo Pro habe und das Programm gab es nur für LLwin. Das fahnde ich schade und ich schrieb das Programm