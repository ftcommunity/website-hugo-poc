---
layout: "file"
hidden: true
title: "Logischer Binär-Volladdierer"
date: "2015-12-22T00:00:00"
file: "logischervolladdierer.zip"
konstrukteure: 
- "Marc"
uploadBy:
- "Marc"
license: "unknown"
legacy_id:
- /data/downloads/robopro/logischervolladdierer.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/logischervolladdierer.zip -->
Der Volladdierer verrechnet nur mithilfe der AND und OR Logikgatter 2 4-Bit Binärzahlen.
Für das Programm wird keine Hardware benötigt, es kann also im Simulationsmodus ausgeführt werden.