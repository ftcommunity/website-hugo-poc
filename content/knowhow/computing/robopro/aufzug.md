---
layout: "file"
hidden: true
title: "Aufzugssteuerung"
date: "2017-02-03T00:00:00"
file: "aufzug.rpp"
konstrukteure: 
- "Frank Jakob"
uploadBy:
- "Frank Jakob"
license: "unknown"
legacy_id:
- /data/downloads/robopro/aufzug.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/aufzug.rpp -->
Steuerung für meinen Aufzug mit 4 Haltestellen. Langsames Anfahren und-Anhalten mit Impulszähler an der Motorwelle.