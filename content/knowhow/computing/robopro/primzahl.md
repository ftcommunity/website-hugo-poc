---
layout: "file"
hidden: true
title: "Primzahl Rechner"
date: "2011-02-05T00:00:00"
file: "primzahl.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/primzahl.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/primzahl.rpp -->
Dieses Programm kann Primzahlen ausrechnen.