---
layout: "file"
hidden: true
title: "Kaputte Programmierung zur Verknüpfung zweier Werte"
date: "2009-06-20T00:00:00"
file: "temperaturzuordnung.jpg"
konstrukteure: 
- "Patrick P."
uploadBy:
- "Patrick P."
license: "unknown"
legacy_id:
- /data/downloads/robopro/temperaturzuordnung.jpg
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/temperaturzuordnung.jpg -->
Das ist eine nicht funktionstüchtige Programmierung zur verknüpfung von zwei Werten.