---
layout: "file"
hidden: true
title: "StackingPro 1D"
date: "2017-12-19T00:00:00"
file: "stackingpro1d.zip"
konstrukteure: 
- "Andreas Gail"
uploadBy:
- "Andreas Gail"
license: "unknown"
legacy_id:
- /data/downloads/robopro/stackingpro1d.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/stackingpro1d.zip -->
Mit dieser Software ist es möglich, Macro-Stacks mit Hilfe einer Spiegelreflexkamera aufzunehmen. Diesem Paket liegen 2 Software-Versionen bei, einmal mit Spiegelvorauslösung (SVA) oder auch ohne. Die Software ist in fischertechnik RoboPro 4.2.4 erstellt.