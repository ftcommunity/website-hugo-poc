---
layout: "file"
hidden: true
title: "löscht das erste Element einer Liste"
date: "2007-11-07T00:00:00"
file: "listen_element_loeschen.rpp"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/listen_element_loeschen.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/listen_element_loeschen.rpp -->
Realisierung als UP mit Ein- und Ausgang für eine Liste zur einfachen Verwendung in einem eigenen Programm
