---
layout: "file"
hidden: true
title: "Farbsortierer 2.0"
date: "2017-02-03T00:00:00"
file: "farbsortierer2.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/farbsortierer2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/farbsortierer2.rpp -->
Nach Vier Jahren habe ich eine neue Version von meinem Farbsortierer gebaut. Neu ist, dass er jetzt Blau und Schwarz in zwei Größen erkennen kann. Am Anfang werden die Werte der Farben ermittelt.
YouTube Link: http://youtu.be/jVbV_SHyt_M