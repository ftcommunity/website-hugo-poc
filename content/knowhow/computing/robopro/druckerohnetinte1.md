---
layout: "file"
hidden: true
title: "Drucker ohne Tinte (Funzt nur mit zB. Sift)"
date: "2009-04-29T00:00:00"
file: "druckerohnetinte1.rpp"
konstrukteure: 
- "Jonas Fischer"
uploadBy:
- "Jonas Fischer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/druckerohnetinte1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/druckerohnetinte1.rpp -->
-> ließt daten aus Exel Tabelle aus.
   (1.Spalte x-achse 2. Spalte y-achse 3. Spalte farbintensitet (wie dunkel))
-> funzt mit lötzinn (getested)
-> powered and Copyrighted by Fijo © 2009