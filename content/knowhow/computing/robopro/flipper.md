---
layout: "file"
hidden: true
title: "Flipper Version5"
date: "2017-02-03T00:00:00"
file: "flipper.rpp"
konstrukteure: 
- "Niklas Frühauf"
uploadBy:
- "Niklas Frühauf"
license: "unknown"
legacy_id:
- /data/downloads/robopro/flipper.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/flipper.rpp -->
Hardware siehe Bilderpool(11.03.07)
Mit 3 Leben muss man in 5 Leveln möglichst viele Punkte erzielen.
4Mini-Taster sind Ziele
Lampe und Fototransistor detektieren verlorene Bälle

Neu:Anzeigeverbesserung, keine Programmierfehler mehr
