---
layout: "file"
hidden: true
title: "Taktstraße mit 2 Stationen"
date: "2017-02-03T00:00:00"
file: "irtaktstrae.zip"
konstrukteure: 
- "Sebastian Geisler"
uploadBy:
- "Sebastian Geisler"
license: "unknown"
legacy_id:
- /data/downloads/robopro/irtaktstrae.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/irtaktstrae.zip -->
