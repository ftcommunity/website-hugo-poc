---
layout: "file"
hidden: true
title: "Gesamterrechner"
date: "2011-05-16T00:00:00"
file: "gesamterrechner.rpp"
konstrukteure: 
- "Fuzzi 52"
uploadBy:
- "Fuzzi 52"
license: "unknown"
legacy_id:
- /data/downloads/robopro/gesamterrechner.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/gesamterrechner.rpp -->
Mit diesem Programm kann man die Zahlen 1-x zusammenrechnen
X darf maximal 225 sein weil ab 226 32767 überschritten wird
Beispiel
x=4
1+2+3+4=10 
10 wäre in diesem Fall das Ergebnis