---
layout: "file"
hidden: true
title: "Problem"
date: "2017-02-03T00:00:00"
file: "problem.zip"
konstrukteure: 
- "Holger Howey (fishfriend)"
uploadBy:
- "Holger Howey (fishfriend)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/problem.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/problem.zip -->
Das Programm Problem ist ein Gag-Programm, das auch mit der Demo von RoboPro läuft.
Man kann aber auch das Ja-Nein-Unterprogramm in eigene Programme kopieren und nutzen.
