---
layout: "file"
hidden: true
title: "Poti-Lenkung"
date: "2007-05-20T00:00:00"
file: "lenkungpoti.rpp"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
uploadBy:
- "Stefan Lehnerer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/lenkungpoti.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/lenkungpoti.rpp -->
Programm von meinem Unimog mit Poti-Lenkung und Geschwindigkeitssteuerung.
