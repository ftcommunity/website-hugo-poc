---
layout: "file"
hidden: true
title: "Spurverfolger mit Gleichlaufgetriebe v1.0"
date: "2013-03-27T00:00:00"
file: "spurverfolgermitgleichlaufgetriebev1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/spurverfolgermitgleichlaufgetriebev1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/spurverfolgermitgleichlaufgetriebev1.0.zip -->
Spurverfolger-Programm für einen Explorer mit Gleichlaufgetriebe (siehe Modell "Spurverfolger mit Gleichlaufgetriebe" im Bliderpool).