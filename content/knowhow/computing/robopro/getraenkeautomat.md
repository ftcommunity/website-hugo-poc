---
layout: "file"
hidden: true
title: "Getränkeautomatprogramm"
date: "2017-02-03T00:00:00"
file: "getraenkeautomat.rpp"
konstrukteure: 
- "technikfischer"
uploadBy:
- "technikfischer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/getraenkeautomat.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/getraenkeautomat.rpp -->
Dies ist das Programm zu meinem Automaten.
Die Belegung der Tasten Findet sich in der Beschreibung des Hauptprogramms.
Bei Unklarheiten bitte einfach die Frage zum Model schreiben
[Bilder](http://www.ftcommunity.de/categories.php?cat_id=2373).
