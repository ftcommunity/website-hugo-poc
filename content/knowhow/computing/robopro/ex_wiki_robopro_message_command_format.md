---
layout: "wiki"
title: "RoboPro message command format for serial communication"
date: 2008-01-16T22:23:15
konstrukteure: 
- "Ad"
uploadBy:
- "Ad"
license: "unknown"
legacy_id:
- /wikic2a6.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wikic2a6.html?action=show&topic_id=32 -->
<!--
Wiki

Thema: RoboPro message command format for serial communication

Aktuelle Version

von: Ad

am: 16.01.2008, 22:23:15 Uhr

   Historie:
Version 1 von Ad am 13.01.2008, 19:47:52 Uhr
-->


When sending messages over the COM interface from a RoboPro program (running on an RI) and receiving them on a PC (using ftlib or any other program), you will notice that what you send is not what you receive. The other direction is even more difficult because in RoboPro you have to know which command to expect.

First make sure that you have the hardwareId (00) and the SubId (09) correct, otherwise you won't receive anything.

RoboPro sends the message value (which you enter as a binary value) directly in binary form (a 16 bit integer) but for the command a translation is used.

For user defined message commands RoboPro accepts strings of which only the first three characters are used. These three characters are encoded in base 40, using the following symbol set:

    a-zA-Z -> 1-26
    0-9 -> 27-36
    + -> 37
    - -> 38
    ! -> 39

anything else maps to 0.

This means that the command found on the line is 1600 times the value of the first character plus 40 times the value of the second character plus the value of the third character. When there are less than three character, leading null value characters are assumed, so 'A' is taken as '__A' (the underscore has value 0 just like space, period, hash etc.

For example the string 'Ent' will be encoded as 1600*5 + 40*14 + 20 = 8580, in hex this is 2184 the reserved code for 'delete'. My guess is that the programmer made a mistake here because he moved all the other predefined commands outside the codespace of 0-63999.
