---
layout: "file"
hidden: true
title: "Digitaluhr in RoboPro"
date: "2017-02-03T00:00:00"
file: "digitaluhr.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Laserman"
license: "unknown"
legacy_id:
- /data/downloads/robopro/digitaluhr.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/digitaluhr.zip -->
Die Anzeige befindet sich im Bedienfeld 
des Hauptprogramms. Wenn man nicht alle 
6 Ziffern sieht, muß man mit der kleinen 
Lupe etwas herauszoomen.
Nach dem Start des Programms dauert es 
etwa 20 Sekunden, bis die Uhr loslegt 
(warum auch immer).

Dann kann die Uhr mit den Knöpfen 
gestellt werden. Allerdings müssen diese 
etwa 1-2 Sekunden gedrückt, und 
anschließend wieder losgelassen werden.
Im Hauptprogramm befinden sich Routinen 
zur Zeit-Erstellung, Stellen der Uhrzeit 
und Aufruf von diversen Unterprogrammen.
Die Uhr läuft nicht ganz exakt. Aber man 
kann schön sehen, was so alles dahinter 
steckt...
