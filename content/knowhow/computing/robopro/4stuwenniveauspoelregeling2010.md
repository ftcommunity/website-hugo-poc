---
layout: "file"
hidden: true
title: "Niveau + Spoel -regeling voor 4 stuwen (2010)  "
date: "2017-02-03T00:00:00"
file: "4stuwenniveauspoelregeling2010.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/4stuwenniveauspoelregeling2010.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/4stuwenniveauspoelregeling2010.rpp -->
Niveau + Spoel -regeling voor 4 stuwen:
-Schuif-stuw-1: Ultrasound spanningsuitgang A1,
-Segment-stuw-2: US-FT-D1,
-Segment-stuw-3: US-FT-D2,
-Schuif-stuw-4: Ultrasound spanningsuitgang A2,

Mit kurze Motorbetriebzeiten ist eine stabile Wasserstand möglich: [+/-1 cm] 
Statt die Impuls-schalter nutze ich 0,02-1,0 sec für eine kleine Wehr-Motor-Bewegungen. 
Jede 0,3 - 0,5 Sekunde wirt die Wasserstand mit einem US-Sensor gemessen.
Wenn es eine niedrige Wasserstand gibt als mit dem Pot-meter eingefuhrt und berechnet, bewegt die Wehre ein wenig nach Unten damit eine hohere Wasserstand erreicht wird.
