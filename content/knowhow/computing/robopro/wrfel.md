---
layout: "file"
hidden: true
title: "Würfel"
date: "2009-06-20T00:00:00"
file: "wrfel.rpp"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
uploadBy:
- "Stefan Lehnerer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/wrfel.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/wrfel.rpp -->
Ein kleines Programm indem man Würfeln kann. Mit Lichteffekten kombiniert.<!-- https://www.ftcommunity.de/data/downloads/robopro/wrfel.rpp -->
Das ist ein Würfel mit 7 Lampen und die Zahlen werden durch "Zufall" gewählt.
