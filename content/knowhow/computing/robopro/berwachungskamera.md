---
layout: "file"
hidden: true
title: "Überwachungs-Kamera"
date: "2017-02-03T00:00:00"
file: "berwachungskamera.rpp"
konstrukteure: 
- "ba-ft-ler"
uploadBy:
- "ba-ft-ler"
license: "unknown"
legacy_id:
- /data/downloads/robopro/berwachungskamera.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/berwachungskamera.rpp -->
Dies ist ein Programm für eine Überwachungs-Kamera!
Anleitung siehe Beschreibung