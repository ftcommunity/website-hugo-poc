---
layout: "file"
hidden: true
title: "Speedtest V1.0"
date: "2016-01-10T00:00:00"
file: "speedtest.zip"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/speedtest.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/speedtest.zip -->
Das Programm Speedtest V1.0 versucht zu ermitteln, wie schnell ein ROBOPro Programm ausgeführt wird.

Die Geschwindigkeit der Programm-Ausführung ist in erster Linie vom verwendeten Controller abhängig:

ROBOTICS TXT Controller -> USB:   401  Download:    35 (mit angeschlossener USB-Kamera: 39)
ROBO TX Controller ----------> USB: 1990  Download: 3142 (im Flash: 3207)
ROBO Interface (93293) ----> USB: ....?  RS232: ....?  Download: ....?
Intelligent Interface ------------> RS232: ....?  Download: ....?

Zusammenfassung der aktuellen Ergebnisse:
1. Der TXT führt Programme online fast 5x schneller aus als der TXC
2. Der TXT führt Programme im Download-Modus fast 90x schneller aus als der TXC
3. Der TXT führt Programme im Download-Modus über 11x schneller aus als online
4. Der TXT führt Programme im Download-Modus mit angeschlossener USB-Kamera etwas langsamer
    aus als ohne USB-Kamera
5. Der TXC führt Programme im Download-Modus nur mit 2/3 der Online-Geschwindigkeit aus (a)
6. Der TXC fürhrt Programme im RAM etwas schneller aus als im Flash

Anmerkungen:
(a) Sehr stark abhängig von den ausgeführten Befehlen, d.h. nicht allgemeingültig!


Die Werte können dazu benutzt werden, um in einem ROBOPro Programm festzustellen, auf welchem
Controller das Programm läuft. Dies kann für die Auslegung zeitkritischer Funktionen benutzt werden,
damit diese auf unterschiedlichen Controllern identisch arbeiten.

Beispiel (einfache Verzweigung):
Z > 1200 --->  J: ROBO TX Controller
               ---> N: ROBOTICS TXT Controller
