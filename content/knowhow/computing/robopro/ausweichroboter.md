---
layout: "file"
hidden: true
title: "Ausweichroboter"
date: "2017-02-03T00:00:00"
file: "ausweichroboter.rpp"
konstrukteure: 
- "Fuzzi 52"
uploadBy:
- "Fuzzi 52"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ausweichroboter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ausweichroboter.rpp -->
Dieser Roboter weicht Hindernissen  ( Ultraschallsensor )
und Spuren ( ( Schwarze Stellen ) Spursensor ) aus.