---
layout: "file"
hidden: true
title: "Kleiner automatisierter Briefkasten"
date: "2018-09-24T00:00:00"
file: "kleiner_automatisierter_briefkasten.zip"
konstrukteure: 
- "Robin Seidl"
uploadBy:
- "Robin Seidl"
license: "unknown"
legacy_id:
- /data/downloads/robopro/kleiner_automatisierter_briefkasten.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/kleiner_automatisierter_briefkasten.zip -->
Zwei Programme für TxT und Tx, Codekartendatei, Video