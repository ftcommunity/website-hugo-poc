---
layout: "file"
hidden: true
title: "Teilen + Runden"
date: "2017-02-03T00:00:00"
file: "uprunden.rpp"
konstrukteure: 
- "nula"
uploadBy:
- "nula"
license: "unknown"
legacy_id:
- /data/downloads/robopro/uprunden.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/uprunden.rpp -->
Ein UP welches eine Zahl durch eine andere teilt und rundet auf eine gerade Zahl. Der Operator Teilen bei RoboPro rundet nur ab, dieses UP ab ,5 auf.