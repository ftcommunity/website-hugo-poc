---
layout: "file"
hidden: true
title: "IO_Check für TX-Controller"
date: "2017-10-24T00:00:00"
file: "io_check.rpp"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/robopro/io_check.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/io_check.rpp -->
Es werden die 8 "normalen" Eingänge auf dem Display angezeigt (ohne Zähleingänge).
So hat man bequem und schnelle alle (End-) Taster im Blick.

Und man kann 4 Motoren an den Ausgängen rechts oder links laufen lassen.
Mit den 2 Knöpfen am TX kann man den Motor und die Drehrichtung auf dem Display auswählen.
Dann den rechten Knopf lange drücken und der Motor läuft, bis der Taster losgelassen wird.
