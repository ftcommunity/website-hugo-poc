---
layout: "file"
hidden: true
title: "I²C-Treiber für Real Time Clock (RTC) DS3231"
date: "2017-02-03T00:00:00"
file: "rtctreiberds3231v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/rtctreiberds3231v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/rtctreiberds3231v1.0.zip -->
I²C-Treiber für RTC DS3231 inklusive Beispielprogramm - Stellen der RTC mit dem DCF77-Decoder (siehe ft:pedia 3/2012) und Ausgabe auf TX-Display sowie (falls angeschlossen) bis zu drei 7-Segment-LED-Anzeigen (SAA1064, siehe ft:pedia 4/2012). Synchronisation der RTC mit DCF77-Signal erfolgt (sofern erreichbar) einmal pro Stunde. Näheres im entsprechenden Beitrag der ft:pedia 4/2013.