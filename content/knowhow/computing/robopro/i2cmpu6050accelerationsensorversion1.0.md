---
layout: "file"
hidden: true
title: "I²C-Treiber für 6-achsen MotionTracking Sensor"
date: "2017-02-03T00:00:00"
file: "i2cmpu6050accelerationsensorversion1.0.rpp"
konstrukteure: 
- "Christian Hehr"
uploadBy:
- "Christian Hehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cmpu6050accelerationsensorversion1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cmpu6050accelerationsensorversion1.0.rpp -->
Erster verfügbarer I²C Treiber für den MPU-6050 mit folgenden Eigenschaften:
- 3-Achsen digitale Beschleunigungswerte AX,AY und AZ bis maximal +/- 16g
- 3-Achsen digitale Drehgeschwindigkeiten GX, GY, und GZ bis maximal +/- 2000 deg/s
- Temperaturanzeige T im bereich von -40°C bis +85°C
Alle Werte werden mit einer Samplingrate von 10Hz in einer Liste gespeichert für die weitere Verwendung in Excel.

Programmiert in ROBO Pro V4.1.5