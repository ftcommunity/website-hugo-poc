---
layout: "file"
hidden: true
title: "Sorting color ball"
date: "2018-09-07T00:00:00"
file: "sortingcolorball.rpp"
konstrukteure: 
- "fotoopa"
uploadBy:
- "fotoopa"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sortingcolorball.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sortingcolorball.rpp -->
Sorting color ball Hubelino 24.5 mm. There are 4 colors. The sorting module use the color sensor I2C TCS34725.