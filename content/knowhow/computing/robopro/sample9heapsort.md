---
layout: "file"
hidden: true
title: "HeapSort"
date: "2017-02-03T00:00:00"
file: "sample9heapsort.zip"
konstrukteure: 
- "RoboProEntwickler"
uploadBy:
- "RoboProEntwickler"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sample9heapsort.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sample9heapsort.zip -->
Unterprogramm zum Sortieren von Listen. Es können sogar recht elegant mehrere Listen parallel sortiert werden.