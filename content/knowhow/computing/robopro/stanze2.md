---
layout: "file"
hidden: true
title: "Stanze 2"
date: "2011-05-16T00:00:00"
file: "stanze2.rpp"
konstrukteure: 
- "Fuzzi 52"
uploadBy:
- "Fuzzi 52"
license: "unknown"
legacy_id:
- /data/downloads/robopro/stanze2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/stanze2.rpp -->
Mein Stanzenprogramm habe ich noch mal überarbeitet