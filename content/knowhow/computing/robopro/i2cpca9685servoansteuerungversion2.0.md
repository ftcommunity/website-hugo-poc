---
layout: "file"
hidden: true
title: "I²C-Treiber LED und Servoansteuerung Version 2.0"
date: "2017-01-12T00:00:00"
file: "i2cpca9685servoansteuerungversion2.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cpca9685servoansteuerungversion2.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cpca9685servoansteuerungversion2.0.rpp -->
RoboPro I2C Treiber für Servo oder  LEDs die dann über RoboPro gesteuert werden können.
Programmiert in ROBO Pro V4.2.3

Unbedingt Hinweis und Beschreibung im Forum beachten!
Siehe http://forum.ftcommunity.de/viewtopic.php?f=8&t=3407