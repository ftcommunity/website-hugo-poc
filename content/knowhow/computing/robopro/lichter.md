---
layout: "file"
hidden: true
title: "Lichtorgel"
date: "2017-02-03T00:00:00"
file: "lichter.rpp"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
uploadBy:
- "Stefan Lehnerer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/lichter.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/lichter.rpp -->
Für dieses Programm sind keine Lampen nötig und keine IF. Die Lichter werden im Bedienfeld simuliert.
