---
layout: "file"
hidden: true
title: "ROBO Pro Fernsteuerung"
date: "2006-09-24T00:00:00"
file: "fernsteuerrung.rpp"
konstrukteure: 
- "Paul Nehlich"
uploadBy:
- "Paul Nehlich"
license: "unknown"
legacy_id:
- /data/downloads/robopro/fernsteuerrung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/fernsteuerrung.rpp -->
Software für Raupenfunktion ROBO Interface 
(Handsender)
Ausgelegt für "einfacher Roboter"/ "Basismodell"(beides aus beschreibung ROBO Mobile Set)
Oder jeden Fahrroboter mit zwei Einzelansteuerbaren Rädern.