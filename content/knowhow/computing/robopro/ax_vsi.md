---
layout: "file"
hidden: true
title: "AX input test program"
date: "2006-02-08T00:00:00"
file: "ax_vsi.rpp"
konstrukteure: 
- "Tone Jurca"
uploadBy:
- "Tone Jurca"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ax_vsi.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ax_vsi.rpp -->
