---
layout: "file"
hidden: true
title: "Programm zum messen der  maximum Beschleunigung beim Freefall-turm "
date: "2011-01-09T00:00:00"
file: "gkrachtmeting.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Poederoyen NL"
license: "unknown"
legacy_id:
- /data/downloads/robopro/gkrachtmeting.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/gkrachtmeting.rpp -->
Programm zum messen der  maximum Beschleunigung beim Freefall-turm mit Wirbelstrom-bremsen.  Die mit TX und Beschleunigungs-Sensor-Modul N5170 gemessen maximum Beschleunigung   2,8g  gibt es beim anfang der Wirbelstrom-bremsen nach unten.
