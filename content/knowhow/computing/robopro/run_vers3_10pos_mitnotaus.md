---
layout: "file"
hidden: true
title: "Industrieroboter mit 10 Lagerplätze und Förderband"
date: "2017-02-03T00:00:00"
file: "run_vers3_10pos_mitnotaus.rpp"
konstrukteure: 
- "Erwin Kneubuehl"
uploadBy:
- "Erwin Kneubuehl"
license: "unknown"
legacy_id:
- /data/downloads/robopro/run_vers3_10pos_mitnotaus.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/run_vers3_10pos_mitnotaus.rpp -->
Programm für 10 Lagerplätze und aus-eingabe Förderband.
