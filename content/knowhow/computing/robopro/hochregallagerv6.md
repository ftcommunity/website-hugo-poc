---
layout: "file"
hidden: true
title: "Hochregallager Positionen tauschen"
date: "2018-09-07T00:00:00"
file: "hochregallagerv6.7z"
konstrukteure: 
- "Helmut Räuchle"
uploadBy:
- "Helmut Räuchle"
license: "unknown"
legacy_id:
- /data/downloads/robopro/hochregallagerv6.7z
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/hochregallagerv6.7z -->
Das Programm bietet zusätzlich zu den sonst üblichen - Modi Hand, Einlagern und Auslagern - noch den Modus Umsetzen. Ist dieser Ausgewählt können zwei Lagerplätze bestimmt werden deren Inhalt nach betätigen der Start-Taste getauscht wird. Also z.B. Inhalt Lagerplatz 1 tauschen mit dem Inhalt von Lagerplatz 5. Der PROGRAMMABLAUF basiert dabei auf einer Excel-Tabelle die in das CSV-Format exportiert und in drei ROBOPro Listen geladen wurde.