---
layout: "file"
hidden: true
title: "Radar SOftware"
date: "2017-02-03T00:00:00"
file: "radar.rpp"
konstrukteure: 
- "T-bear"
uploadBy:
- "T-bear"
license: "unknown"
legacy_id:
- /data/downloads/robopro/radar.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/radar.rpp -->
Dieses Programm ist dafür da, ein Radar zu drehen.
Das Radar erkennt vier Richtungen durch ein Impulsrad und zeigt die zuletzt gemessenen Werte im Bedienfeld. 