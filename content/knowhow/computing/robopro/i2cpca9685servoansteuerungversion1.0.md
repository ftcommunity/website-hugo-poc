---
layout: "file"
hidden: true
title: "I²C-Treiber LED und Servoansteuerung Version 1.0"
date: "2017-01-12T00:00:00"
file: "i2cpca9685servoansteuerungversion1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cpca9685servoansteuerungversion1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cpca9685servoansteuerungversion1.0.rpp -->
RoboPro I2C Treiber für 16 Servo- oder 16 LEDs die dann über RoboPro gesteuert werden können.
Programmiert in ROBO Pro V4.2.3

Unbedingt Hinweise im Forum beachten!