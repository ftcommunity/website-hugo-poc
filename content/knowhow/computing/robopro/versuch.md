---
layout: "file"
hidden: true
title: "Versuch IF und TX zusammen zu programmieren"
date: "2009-12-30T00:00:00"
file: "versuch.rpp"
konstrukteure: 
- "Patrick P."
uploadBy:
- "Patrick P."
license: "unknown"
legacy_id:
- /data/downloads/robopro/versuch.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/versuch.rpp -->
Ich habe das Programm so eingestellt, dass man theoretisch Robo IF und TX Controller zusammen in einem Programm programmieren könnte, aber leider klappt das nicht.

Wäre nett, wenn jmd. eine Idee hat.

Patrick
