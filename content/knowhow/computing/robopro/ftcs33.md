---
layout: "file"
hidden: true
title: "Cube Solver"
date: "2017-02-03T00:00:00"
file: "ftcs33.zip"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/robopro/ftcs33.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/ftcs33.zip -->
Steuerungsprogramm des Cube Solver-Roboters, mit Algorithmus und Farberkennung.

Zum größten Teil grob dokumentiert, eine "Anleitung" zum evl. Verwenden des Algorithmus mit einem anderen Roboter liegt bei.

Genaueres zum Projekt siehe Forum.

Benötigt aktuellste RoboPro-Version!
