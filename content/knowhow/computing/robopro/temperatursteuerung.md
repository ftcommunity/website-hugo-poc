---
layout: "file"
hidden: true
title: "Temperatursteuerung"
date: "2011-11-03T00:00:00"
file: "temperatursteuerung.rpp"
konstrukteure: 
- "lord_Unbekannt"
uploadBy:
- "lord_Unbekannt"
license: "unknown"
legacy_id:
- /data/downloads/robopro/temperatursteuerung.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/temperatursteuerung.rpp -->
Hier eine wirkungsvollere und größere Variante der Temperatursteuerung aus dem ROBO Starter. Ich bin bei den Lüftern auf Axaiallüftern umgestiegen, da diese leiser und besser zu befestigen sind. 

Bilder werden folgen.