---
layout: "file"
hidden: true
title: "ASCII Umwandler"
date: "2017-02-03T00:00:00"
file: "buchstabea.rpp"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
uploadBy:
- "Martin Giger"
license: "unknown"
legacy_id:
- /data/downloads/robopro/buchstabea.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/buchstabea.rpp -->
Dieses Programm wandelt ASCII Codierte Zahlen in Buchstaben um. Es kommt im Sekundentakt immer noch derselber dazu.
