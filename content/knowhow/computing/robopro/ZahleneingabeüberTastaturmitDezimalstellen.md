---
layout: "file"
hidden: true
title: "Zahleneingabe über Tastatur mit Dezimalstellen"
date: 2021-08-04T14:31:56+02:00
file: "ZahleneingabeüberTastaturmitDezimalstellen.rpp"
konstrukteure: 
- "Thomas-815"
uploadBy:
- "Website-Team"
license: "unknown"
---
Das Programm realisiert die Zahleneingabe über eine 4x4-Tastatur, welche am I²C-Port angeschlossen ist.

Grundlage für die Ansteuerung ist der angepasste Treiber für den PCF8574 von Dirk Fox.

Das Programm gibt eine 7-stellige Zahl mit 2 Dezimalstellen aus. Weitere Hinweise über Tastenbelegung sind im Programm enthalten und können angepasst werden.
