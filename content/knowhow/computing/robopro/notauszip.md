---
layout: "file"
hidden: true
title: "Not-Aus"
date: "2006-05-31T00:00:00"
file: "notaus.zip"
konstrukteure: 
- "Markus Mack (MarMac)"
uploadBy:
- "Markus Mack"
license: "unknown"
legacy_id:
- /data/downloads/robopro/notaus.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/notaus.zip -->
Primitives Notaus-Unterprogramm, das die Motoren in einem eigenen Prozess in einer Endlosschleife ausschaltet.

Achtung: Funktioniert nicht, wenn irgendwo anderes ein Motor in einer (schnellen) Endlosschleife eingeschaltet wird.

Ein Reset ist möglich, indem man das Unterprogramm so erweitert, dass die Endlosschleife beim Druck eines anderen Tasters abbricht und wieder auf den Notaus wartet. Die Motoren, die vorher liefen, wieder automatisch einzuschalten geht nicht ohne weiteres.

Wenn mehr Ausgänge als M1-M4 verwendet werden, muss das Unterprogramm entsprchend erweitert werden.
