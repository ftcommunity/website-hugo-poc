---
layout: "file"
hidden: true
title: "DCF77-Testprogramme 1 und 2"
date: "2017-02-03T00:00:00"
file: "dcf77tests.zip"
konstrukteure: 
- "ftDirk"
uploadBy:
- "ftDirk"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dcf77tests.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dcf77tests.zip -->
Dies sind die ersten 2 Testprogramme für die Entwicklung eines DCF77-Decoders mit RoboPro und dem TXC.
Hier ist der Thread dazu: http://forum.ftcommunity.de/viewtopic.php?f=8&t=1421#p9207
