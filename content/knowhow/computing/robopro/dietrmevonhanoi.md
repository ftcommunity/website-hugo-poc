---
layout: "file"
hidden: true
title: "Die Türme von Hanoi (qincym)"
date: "2017-02-03T00:00:00"
file: "dietrmevonhanoi.zip"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
uploadBy:
- "Volker-James Münchhof (qincym)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dietrmevonhanoi.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dietrmevonhanoi.zip -->
RPP-Programme und PDF-Bauanleitung für "Die Türme von Hanoi" mit Impulszählung zur Positionierung und rekursiven Programmaufrufen.

Anmerkung des Website-Teams: Bilder sind hier: https://www.ftcommunity.de/bilderpool/modelle/kleinmodelle-zum-nachbauen/turme-hanoi/
