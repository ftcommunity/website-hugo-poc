---
layout: "file"
hidden: true
title: "Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen"
date: "2017-02-03T00:00:00"
file: "5stuwenniveauspoelregeling.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/5stuwenniveauspoelregeling.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/5stuwenniveauspoelregeling.rpp -->
Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen

Ich versuche mit einer Pot-Meter und RoboPro in 5 Wasserbecken eine stabile verstelbare Wasserstand zu erreichen.

Niveau + Spoel -regeling voor 5 stuwen:
-Schuif-stuw-1: Ultrasound spanningsuitgang A1,
-Segment-stuw-2: US-FT-D1,
-Segment-stuw-3: US-FT-D2,
-Schuif-stuw-4: Potmeter-vlotter weerstand: AY
-Klep-stuw-5: Ultrasound spanningsuitgang A21

Mit kurze Motorbetriebzeiten  ist eine stabile Wasserstand möglich:  [+/-1 cm] 
Statt die Impuls-schalter nutze ich 0,02-1,0 sec für eine kleine Wehr-Motor-Bewegungen. 
Jede 0,3 - 0,5 Sekunde wirt die Wasserstand mit einem US-Sensor (oder Potmeter) gemessen.
Wenn es eine niedrige Wasserstand gibt als mit dem Pot-meter eingefuhrt und berechnet, bewegt die Wehre ein wenig nach Unten damit eine hohere Wasserstand erreicht wird. 
