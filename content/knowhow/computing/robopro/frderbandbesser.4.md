---
layout: "file"
hidden: true
title: "Farben erkenner"
date: "2009-08-15T00:00:00"
file: "frderbandbesser.4.rpp"
konstrukteure: 
- "Manuel Neumann"
uploadBy:
- "Manuel Neumann"
license: "unknown"
legacy_id:
- /data/downloads/robopro/frderbandbesser.4.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/frderbandbesser.4.rpp -->
Kennt ihr das Problem vom Farbsensor? Man programmier es in einen zimmer und in dem anderen wo man es vorführen will ist die Helligkeit anders. Dieses Programm fragt erst mal ab welche Farbe der stein hat und erst dann kann er sie sortieren. Eine bessere Beschreibung: bei Hauptprogramm / Beschreibung.
