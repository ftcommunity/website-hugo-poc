---
layout: "file"
hidden: true
title: "Motoren synchronisieren"
date: "2007-01-05T00:00:00"
file: "synchron_new.rpp"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/synchron_new.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/synchron_new.rpp -->
2 Motoren (z.B. Antrieb Mobile Robot) sollen synchron laufen ohne mechanisches Gleichlaufgetriebe
