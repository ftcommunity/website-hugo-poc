---
layout: "file"
hidden: true
title: "I²C-Treiber für 6-achsen MotionTracking Sensor"
date: "2017-02-03T00:00:00"
file: "i2cmpu60509150motiondetectionversion1.0.rpp"
konstrukteure: 
- "chehr"
uploadBy:
- "chehr"
license: "unknown"
legacy_id:
- /data/downloads/robopro/i2cmpu60509150motiondetectionversion1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/i2cmpu60509150motiondetectionversion1.0.rpp -->
Erweiterung für den I²C Treiber MPU-6050 / 9150 mit folgenden Eigenschaften:
1. Motion detection (Bewegungsmelder) +X, -X ,+Y, -Y, +Z und -Z Achse.
2. Einstellung der Sensitivität und Dauer.
Ideal als Kollisionszähler, z.B. für den Fischertechnik Flipper

Programmiert in ROBO Pro V4.1.6