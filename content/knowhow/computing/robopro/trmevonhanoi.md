---
layout: "file"
hidden: true
title: "Türme von Hanoi mit Altem Kasten 30554"
date: "2017-02-03T00:00:00"
file: "trmevonhanoi.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/robopro/trmevonhanoi.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/trmevonhanoi.zip -->
Roboterpositionierung über Poti!
Den Poti für Hoch und Runter braucht man nicht.
Es gibt ein Programm für Handbetrieb.

Und ein Programm für Automatikbetrieb. Man startet das Programm. Der Roboter Initialisiert. Dann schiebt man 2 bis 5 Scheiben in Position. Stellt am Regler ein, wieviel Scheiben es sind und drückt den Start-Knopf.

Evtl. müssen die Werte angepaßt werden. Poti und Roboterarm in Mittelstellung bringen und dann mit Seiltrommel fixieren.
