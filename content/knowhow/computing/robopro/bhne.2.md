---
layout: "file"
hidden: true
title: "Ansteuerungsprogramm für Lampen am IF"
date: "2011-11-03T00:00:00"
file: "bhne.2.rpp"
konstrukteure: 
- "Lukas (Kieseleck)"
uploadBy:
- "Lukas (Kieseleck)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/bhne.2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/bhne.2.rpp -->
Ein Programm zur Ansteuerung von den Lampenausgängen 01-07 per Bedienfeld (helligkeitsregelung möglich). Das Programm ist langsam, wenn es darum geht die Lampen schnell ein- und auszuschalten. Die beschriftungen und IDs sind von der Bühne.

