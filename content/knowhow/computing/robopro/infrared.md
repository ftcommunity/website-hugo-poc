---
layout: "file"
hidden: true
title: "Use Control set with RoboPro on Robo Interface"
date: "2009-07-12T00:00:00"
file: "infrared.zip"
konstrukteure: 
- "Ad van der Weiden"
uploadBy:
- "Ad van der Weiden"
license: "unknown"
legacy_id:
- /data/downloads/robopro/infrared.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/infrared.zip -->
contains the .hex file with the handler and an example .rpp file as well as all the sources.
See the readme file for installation instructions.