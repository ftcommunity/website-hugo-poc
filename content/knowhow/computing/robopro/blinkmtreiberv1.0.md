---
layout: "file"
hidden: true
title: "I²C-Treiber für BlinkM"
date: "2017-02-03T00:00:00"
file: "blinkmtreiberv1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/blinkmtreiberv1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/blinkmtreiberv1.0.zip -->
Das LED-Modul BlinkM kann beliebige RGB-Farben anzeigen und bietet die Möglichkeit, Lichteffekte über Skripte ablaufen zu lassen; 18 Skripte werden ab Werk mitgeliefert. Mit dem nebenstehenden I²C-Treiber kann es aus RoboPro angesteuert werden. ft:pedia-Beitrag folgt.