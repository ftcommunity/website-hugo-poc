---
layout: "file"
hidden: true
title: "Schrittmotor-Treiber V2"
date: "2017-02-03T00:00:00"
file: "stepper2.rpp"
konstrukteure: 
- "Alfred Svabenicky"
uploadBy:
- "Alfred Svabenicky"
license: "unknown"
legacy_id:
- /data/downloads/robopro/stepper2.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/stepper2.rpp -->
Universelle Unterprogramme zum Ansteuern von Schrittmotoren im Voll- oder Halbschritt-Betrieb. Das Hautprogramm enthält ein Anwendungsbeispiele zu den Unterprogrammen.

Die Version 2 ist einfacher zu handhaben, die Verschiedenen Modi des Unterprogrammes koennen jetzt ueber unterschiedliche Befehle direkt angesteuert werden (auch gemischt). Es wird RoboPro Version 1.2 oder hoeher benoetigt.