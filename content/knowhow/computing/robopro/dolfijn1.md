---
layout: "file"
hidden: true
title: "3-Dolfijnen,  -wave,  dolfijn 1 of 2 leidend (Pneumatik)"
date: "2017-02-03T00:00:00"
file: "dolfijn1.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/dolfijn1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/dolfijn1.rpp -->
3-Dolfijnen,  -wave,  dolfijn 1 of 2 leidend (Pneumatik)
