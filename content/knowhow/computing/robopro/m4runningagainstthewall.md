---
layout: "file"
hidden: true
title: "Using the distance sensor on a 'car'"
date: "2017-02-03T00:00:00"
file: "m4runningagainstthewall.rpp"
konstrukteure: 
- "Richard R. Budding"
uploadBy:
- "Richard R. Budding"
license: "unknown"
legacy_id:
- /data/downloads/robopro/m4runningagainstthewall.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/m4runningagainstthewall.rpp -->
This program is using
I1 for activating the Distance sensor
I2 to stop the distance sensor
I3 to start the 'car' using M4

if the 'car' robot is running close to a wall (50cm), use 3 seconds to stop. with the sequence Speed=8, 6, 4 and 2 followed by STOP M4.

