---
layout: "file"
hidden: true
title: "Matrix robot"
date: "2018-07-09T00:00:00"
file: "matrixrobotfinal2018_06_30.rpp"
konstrukteure: 
- "fotoopa"
uploadBy:
- "fotoopa"
license: "unknown"
legacy_id:
- /data/downloads/robopro/matrixrobotfinal2018_06_30.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/matrixrobotfinal2018_06_30.rpp -->
Full program of the matrix robot. The program is written for the TXT Controller. The TXT Controller is connected online to the PC via USB. This means there is enough space on a large screen. Only the I2C bus of the TXT controller is used. Everything is done in the external hardware via a few very simple commands. The robot uses an 8x8 matrix to load steel balls according to a drawing. The drawing can be selected from a character rom or from a hand-drawn pattern.

After selecting the drawing, the balls can be placed in the matrix by the robot. You can also remove a drawing by the robot. If there are balls in the matrix of which you no longer have a drawing, the robot itself can remove them by means of a scan function to remove the balls.

Without a correct I2C connection that supports all addresses, the program will stop. But you can still see the screen layout to have an idea of the program. I've added some explanation to describe the external hardware.

I've designed new rotary encoders. Made with hall sensors they are much more stable and accurate. To solve the backslash I always do the position in the same direction. If the direction is negative I go a little further back in order to get to the right position. On each motor, an external hardware control is made to reduce the speed at the end point. Distance, max speed, minimum speed and brake curve are available. The TXT controller only has to transmit the new position value. This makes the task much easier.
