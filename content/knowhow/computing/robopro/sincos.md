---
layout: "file"
hidden: true
title: "SinCos UP"
date: "2017-02-03T00:00:00"
file: "sincos.rpp"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
uploadBy:
- "Thomas Brestrich (schnaggels)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/sincos.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/sincos.rpp -->
Sinus und Cosinus Routinen für RoboPro 

(überarbeitet Version, Original von Jens Mewes lief mit aktueller RoboPro Version nicht mehr...)

Integer Werte werden je nach Wertebereich aus Tabelle geladen!
