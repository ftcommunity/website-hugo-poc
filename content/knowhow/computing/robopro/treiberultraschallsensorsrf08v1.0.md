---
layout: "file"
hidden: true
title: "I²C-Treiber für Ultraschall-Sensor SRF08 (Devantech) v1.0"
date: "2017-02-03T00:00:00"
file: "treiberultraschallsensorsrf08v1.0.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberultraschallsensorsrf08v1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberultraschallsensorsrf08v1.0.rpp -->
RoboPro-I²C-Treiber für den Ultraschall-Sensor SRF08 von Devantech inklusive Beispielprogramm (Vergleichsmessung von SRF08 mit fischertechnik-Abstandssensor). Näheres im entsprechenden Beitrag der ft:pedia 4/2013.