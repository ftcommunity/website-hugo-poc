---
layout: "file"
hidden: true
title: "Robo Pro Programm für das Lcd"
date: "2017-02-03T00:00:00"
file: "serielltestlcd.rpp"
konstrukteure: 
- "Frederik Vormann (Fredy)"
- "Thomas Kaiser (thkais)"
uploadBy:
- "thkais und Fredy"
license: "unknown"
legacy_id:
- /data/downloads/robopro/serielltestlcd.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/serielltestlcd.rpp -->
Hier ist das Programm was alle haben wollten. Ich habe es von thkais bekommen, und nutze es für mein Display.

