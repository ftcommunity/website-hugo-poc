---
layout: "file"
hidden: true
title: "Lichtsteuerung"
date: "2017-02-03T00:00:00"
file: "lightshow.rpp"
konstrukteure: 
- "Pascal Jan (bauFischertechnik)"
uploadBy:
- "Pascal Jan (bauFischertechnik)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/lightshow.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/lightshow.rpp -->
Mit den Programm kann man mein Modell einer kleinen "Bühne" steuern