---
layout: "file"
hidden: true
title: "Treinpendel-programma met Ultrasoon-afstandsmeter"
date: "2017-02-03T00:00:00"
file: "treinpendelprogramma.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen, Holland)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treinpendelprogramma.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treinpendelprogramma.rpp -->
Treinpendel-programma met Ultrasoon-afstandsmeter
