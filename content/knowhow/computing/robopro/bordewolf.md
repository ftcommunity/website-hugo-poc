---
layout: "file"
hidden: true
title: "Maske-Robot  "
date: "2017-02-03T00:00:00"
file: "bordewolf.rpp"
konstrukteure: 
- "Peter Damen (peterholland, Poederoyen NL)"
uploadBy:
- "Peter Damen (Poederoyen NL)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/bordewolf.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/bordewolf.rpp -->
"Bor de Wolf" heeft o.a. de volgende aantrekkelijke zeer compakte bloeddrukmeter-onderdelen (<15 Euro) : compressor, magneetventiel, en drukzak met instelbaar overdrukventiel. Een Conrad-voice-module trigger ik via de RoboPro-interface om de Bor de Wolf naar Roodkapje en de 7 geitjes te laten vragen................. 

Ik laat hem het volgende uitvoeren: 

-op en neer lopen (ijsberen) 
-draaien met lijf 
-lijf/hals naar voren en achteren 
-bek open en dicht (stevig bijten) 
-rug opzetten d.m.v. compressor, magneetventiel, en drukzak 
-praten, roepen en huilen d.m.v. Conrad-voice-module   

Meiner Maske-Robot "Bor de Wolf" gleicht immer mehr als "Mensch" wenn mehrere Prozessen (Bewegungen) parallel funktionieren wie auch in wirklichkeit. 

Im Downloadmodus gibt es leider schnell eine entsprechende Fehlermeldung, wenn die Anzahl an Prozessen zu klein ist oder es zu wenig Dataspeicher gibt. 
Die Größe des Mindestspeicher pro Prozess im Downloadmodus kann man verkleinern bis zum 1281 Bytes statt die 4096-Default-werte. 
Meine Robopro-Programm mit viele parallel-Prozessen (Bewegungen) funktioniert mit diesem Rezept auch im Downloadmodus.

