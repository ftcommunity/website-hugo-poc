---
layout: "file"
hidden: true
title: "AutoJoy"
date: "2008-07-16T00:00:00"
file: "autojoy.rpp"
konstrukteure: 
- "nula"
uploadBy:
- "nula"
license: "unknown"
legacy_id:
- /data/downloads/robopro/autojoy.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/autojoy.rpp -->
Damit kann man z.B. das Basismodell aus ROBO Mobile Set mit einem Joystick und evtl. RF Data Link fernsteuern. Dazu muss man den folgenden Link ansehen und die dort angesprochenen Dateien installieren.