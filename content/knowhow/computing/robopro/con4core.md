---
layout: "file"
hidden: true
title: "Connect Four / Vier Gewinnt"
date: "2017-02-03T00:00:00"
file: "con4core.zip"
konstrukteure: 
- "Helmut (hamlet)"
uploadBy:
- "Helmut Schmücker"
license: "unknown"
legacy_id:
- /data/downloads/robopro/con4core.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/con4core.zip -->
"Vier Gewinnt" spielbar über das TX Controller Display oder als Basis für einen "Vier Gewinnt" Roboter.
