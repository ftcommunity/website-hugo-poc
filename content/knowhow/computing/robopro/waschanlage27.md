---
layout: "file"
hidden: true
title: "Programm Portalwaschanlage"
date: "2017-05-22T00:00:00"
file: "waschanlage27.rpp"
konstrukteure: 
- "David Holtz (davidrpf)"
uploadBy:
- "david-ftc"
license: "unknown"
legacy_id:
- /data/downloads/robopro/waschanlage27.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/waschanlage27.rpp -->
Robo Pro Programm der Portalwaschanlage:

Bilder: https://ftcommunity.de/categories.php?cat_id=3409
Video: https://youtu.be/Lc_4gvUTjd8
