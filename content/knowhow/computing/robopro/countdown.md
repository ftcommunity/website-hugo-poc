---
layout: "file"
hidden: true
title: "Countdown"
date: "2007-05-28T00:00:00"
file: "countdown.rpp"
konstrukteure: 
- "nula"
uploadBy:
- "nula"
license: "unknown"
legacy_id:
- /data/downloads/robopro/countdown.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/countdown.rpp -->
Ein Countdown-Programm. Im Bedienfeld des Hauptprogrammes kann man die Dauer eingeben. Auf "Los!" gehts los. Ist der Zähler bei Null angekommen, Blinkt eine Lampe, welche man mit OK ausschalten kann. Man kann den Countdobn auch vorher auf "Stopp!" ausschalten.
Es funzt noch nicht ganz, da es nicht wirklich Hundertstelsekunden zählt (es zählt langsamer).
Kein Interface benötigt.