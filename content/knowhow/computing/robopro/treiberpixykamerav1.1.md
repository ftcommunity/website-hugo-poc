---
layout: "file"
hidden: true
title: "Treiber Pixy-Kamera v1.1"
date: "2018-04-13T00:00:00"
file: "treiberpixykamerav1.1.rpp"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberpixykamerav1.1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberpixykamerav1.1.rpp -->
Bug behoben: 
Bei I2C-Lesen und I2C-Schreiben Element Häkchen "Offen lassen" entfernt.
