---
layout: "file"
hidden: true
title: "Zeitmessanlage 2.1"
date: "2007-04-11T00:00:00"
file: "zeitmessung.zip"
konstrukteure: 
- "Martin Giger (Ma-gi-er)"
uploadBy:
- "Martin Giger"
license: "unknown"
legacy_id:
- /data/downloads/robopro/zeitmessung.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/zeitmessung.zip -->
Zeitmessung-jetzt auch mit Word-Anleitung.  

Das htm wurde überarbeitet und neu ist eine PDF Datei inbegriffen!  

Jetzt habe ich die Anleitung als doc und als htm Datei in den Zip integriert. Natürlich ist auch das Programm dabei.
