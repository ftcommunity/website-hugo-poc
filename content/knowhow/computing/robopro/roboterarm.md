---
layout: "file"
hidden: true
title: "Das Programm von meinem Roboterarm."
date: "2017-02-03T00:00:00"
file: "roboterarm.rpp"
konstrukteure: 
- "Stefan Lehnerer (StefanL)"
uploadBy:
- "Stefan Lehnerer"
license: "unknown"
legacy_id:
- /data/downloads/robopro/roboterarm.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/roboterarm.rpp -->
