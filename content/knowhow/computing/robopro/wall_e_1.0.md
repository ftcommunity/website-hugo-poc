---
layout: "file"
hidden: true
title: "Dateien für den Wall-E Version 1.0"
date: "2017-03-25T00:00:00"
file: "wall_e_1.0.zip"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/robopro/wall_e_1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/wall_e_1.0.zip -->
gepackte Datei:

- RoboPro Programm Wall-E V1.0
- 3x Sound Dateien
- Schriftzug
- Schaltschema
- Wall-E_Menue
