---
layout: "file"
hidden: true
title: "Using the distance sensor"
date: "2006-09-28T00:00:00"
file: "usingthedistancesensor.rpp"
konstrukteure: 
- "Richard R. Budding"
uploadBy:
- "Richard R. Budding"
license: "unknown"
legacy_id:
- /data/downloads/robopro/usingthedistancesensor.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/usingthedistancesensor.rpp -->
Connect the sensor to A1, blue wire.
Connect the red wire to O6, green wire to massa. 

Now you can use this program in order to measure the distance in cm.
