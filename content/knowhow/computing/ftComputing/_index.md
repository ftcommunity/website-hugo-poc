---
title: "ftComputing (1998 - 2012)"
weight: 10
---
Es war einmal ein fischertechik Fan namens Ulrich Müller.
Er betrieb von Juli 1998 bis Januar 2012 eine eigene Seite rund um das Thema
und (ver)sammelte dort jede Menge Infos zu Interfaces, Baukästen,
Programmpaketen und vieles mehr.

Er stellt uns über seine Seite auch Treiber zur Verfügung, mit denen
altehrwürdige Interfaces an modernen Computern betrieben werden können.
Leider musste Ulrich Anfang 2012 die Pflege seiner Seite aufgeben.

Wir danken Ulrich, dass er uns sein Werk, nicht nur zur Aufbewahrung,
überlassen hat.
Leider können wir ftComputing nicht mehr weiter hosten.