---
layout: "file"
hidden: true
title: "Buch: Einstieg in Robo Pro Coding"
date: 2024-06-24T15:40:21+02:00
file: "BuchEinstiegRoboProCodingKlein.pdf"
konstrukteure: 
- "Holger Howey (fishfriend)"
uploadBy:
- "Website-Team"
license: "unknown"
---

Im Buch gibt es eine Beschreibung aller Befehle und für die meisten auch noch Beispielprogramme. Es gibt eine ausführliche Einführung in die Sprache, der Handhabung und es gibt auch Beispiel zu Themen wie MQTT und den Datenaustausch mit anderen Geräten, Controllern und dem Internet. 

Von älteren Kästen und Modellen, gibt es Gegenüberstellungen der Programme anderer Sprachen, zu den Robo Pro Coding Programmen. Hilfreiche Befehlsübersichten und ein Lesezeichen, kann man sich ausdrucken und diese für das eigene Programmieren nutzen.

Es gibt ausführliche Beschreibungen zu den Controllern: TXT 4.0 , RX-Controller und dem BT-Smart Controller und (auch älteren) fischertechnik-Sensoren, die man an die Controller anschließen kann. Man kann in der PDF mit [CTRL]+F nach Stichworten suchen, um so für ein Problem, leichter eine Lösung zu finden.
