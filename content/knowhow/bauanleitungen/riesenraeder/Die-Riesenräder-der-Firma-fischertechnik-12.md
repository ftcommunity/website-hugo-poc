---
layout: "file"
hidden: true
title: "Die Riesenräder der Firma fischertechnik"
date: 2021-10-03T18:05:41+02:00
file: "Die-Riesenräder-der-Firma-fischertechnik-12.pdf"
konstrukteure: 
- "Holger Howey (fishfriend)"
uploadBy:
- "website-team"
license: "unknown"
---

### Übersicht über alle jemals von fischertechnik veröffentlichten Riesenräder

In diesem Dokument ist die Geschichte der fischertechnik-Riesenräder ausführlich dargestellt.
Informationen und Diskussionen zu diesem Projekt finden sich [in unserem Forum](https://forum.ftcommunity.de/viewtopic.php?f=6&t=6318).
