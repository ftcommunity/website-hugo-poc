---
layout: "file"
hidden: true
title: "Deckblatt für die Box"
date: 2020-04-06T11:43:20+02:00
file: "Cover_Box125-1.pdf"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Das Deckblatt für die Box, Auszudrucken in 129x98 mm
