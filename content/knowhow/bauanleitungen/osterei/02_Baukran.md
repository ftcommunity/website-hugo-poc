---
layout: "file"
hidden: true
title: "ft-Designer-Datei Baukran"
date: 2020-04-06T11:43:38+02:00
file: "02_Baukran.ftm"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Die Baukran-Datei für den ft-Designer
