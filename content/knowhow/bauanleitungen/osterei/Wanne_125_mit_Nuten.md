---
layout: "file"
hidden: true
title: "stl-Datei Sortierwanne 125"
date: 2020-04-06T11:43:51+02:00
file: "Wanne_125_mit_Nuten.stl"
konstrukteure: 
- "Peter Habermehl (PHabermehl)"
uploadBy:
- "Website-Team"
license: "by-nc-sa"
---
Die [Sortierwanne 125](https://forum.ftcommunity.de/viewtopic.php?f=38&t=5904) (Box 125)  
