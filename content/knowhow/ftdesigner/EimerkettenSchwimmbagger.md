---
layout: "file"
hidden: true
title: "Eimerketten-Schwimmbagger"
date: 2022-02-28T16:48:22+01:00
file: "EimerkettenSchwimmbagger.ftm"
konstrukteure: 
- "Ludger Mäsing (ludger-ftc)"
uploadBy:
- "website-team"
license: "unknown"
---
Aus ca. 1200 Bauteilen habe ich ihn anhand eines Bildes nachgebaut.


Es stellt einen Eimerketten-Schwimmbagger dar.
Mit dem Original wurde ein Graben der Wassergenossenschaft Marienthal in Österreich gereinigt.
Erstmals wurde der Bagger mit einem kleinen Motor für den Antrieb ausgerüstet.
Der Bagger erreichte eine theoretische Tagesleistung von 120 qm.


![Eimerketten-Schwimmbagger](../Schwimmbagger088.jpeg)

Ich versuche mal die Funktionen zu beschreiben.

Der Mann links hinter der Seilwinde ist für die Höhe der Eimerkette zuständig. Er kann sie (nur vorne) nach belieben heben oder senken.
Hinten ist sie auf einer Achse fest.

Der Mann im Vordergrund steuert den Motor (Antrieb der Kette)

Einer der drei rechts kümmert sich um die Seilwinde die das Boot nach vorne ziehen.
Am Ende der Ketten sind Anker.

Einer der beiden hinter den Eimern kann eine Rutsche heben und senken.
Hierüber fällt das ausgebaggerte Schüttgut in einen Lastkahn.

Ohne die Hilfe vom Stefan Kallauch hätte das Modell keine Seile.

