---
layout: "file"
hidden: true
title: "Ellipsenschablone.zip"
date: "2012-05-21T00:00:00"
file: "ellipsenschablone.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/ellipsenschablone.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/ellipsenschablone.zip -->
Ellipsenschablone. Wenn man einen Stift einspannt, können Ellipsen ezeichnet werden.
