---
layout: "file"
hidden: true
title: "Dokumentation Dreigang-Schaltgetriebe (ft designer)"
date: "2012-06-30T00:00:00"
file: "dokumentationdreigangschaltgetriebeftdesigner.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/dokumentationdreigangschaltgetriebeftdesigner.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/dokumentationdreigangschaltgetriebeftdesigner.zip -->
Modell des Dreigang-Schaltgetriebes (siehe http://www.ftcommunity.de/categories.php?cat_id=2106) im fischertechnik designer (mit Simulation)