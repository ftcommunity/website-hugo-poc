---
layout: "file"
hidden: true
title: "Plotter 85.zip"
date: "2012-05-21T00:00:00"
file: "plotter85.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/plotter85.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/plotter85.zip -->
Der Plotter 85 mit Schrittmotoren.
