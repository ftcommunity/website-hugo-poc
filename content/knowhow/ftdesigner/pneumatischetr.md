---
layout: "file"
hidden: true
title: "Pneumatische Tür.zip"
date: "2012-05-21T00:00:00"
file: "pneumatischetr.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatischetr.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatischetr.zip -->
Pneumatische Tür. Original von Fischertechnik Pneumatik Robots.
