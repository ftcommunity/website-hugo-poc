---
layout: "file"
hidden: true
title: "Zylinder mit Kinematik - Beschreibung"
date: "2015-03-20T00:00:00"
file: "zylindermitkinematik.pdf"
konstrukteure: 
- "Joachim Häberlein"
uploadBy:
- "Joachim Häberlein"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/zylindermitkinematik.docx
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/zylindermitkinematik.docx -->
Pneumatikzylinders 39638 mit Kinematik Funktion im ft-Designer.
Zylinder mit einer Kollisionskugel für Umschaltung in beide Richtungen.
