---
layout: "file"
hidden: true
title: "Konstruktion des kleinen Wall-e"
date: "2012-11-26T00:00:00"
file: "dreiradraupengetriebemitpowermotoren03.ftm"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/dreiradraupengetriebemitpowermotoren03.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/dreiradraupengetriebemitpowermotoren03.ftm -->
ft-Designer-Konstruktion des kleinen Wall-e (vorgestellt auf der Convention 2011; [Modellfotos](http://www.ftcommunity.de/categories.php?cat_id=2148)); inklusive einer Kinematik-Animation.
