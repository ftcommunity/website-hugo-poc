---
layout: "file"
hidden: true
title: "ft-designer Bauteile Teil 1"
date: "2016-01-30T00:00:00"
file: "ftdesigner_bauteile_teil_1.zip"
konstrukteure: 
- "Dirk Wölffel (DirkW)"
uploadBy:
- "Dirk Wölffel"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip -->
Neue und modifizierte ft-designer Bauteile.
Wichtig: Installationsanweisung lesen.
