---
layout: "file"
hidden: true
title: "Zylinder mit Kinematik"
date: "2015-03-27T00:00:00"
file: "zylindermitkinematik2.ftm"
konstrukteure: 
- "Joachim Häberlein"
uploadBy:
- "Joachim Häberlein"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/zylindermitkinematik2.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/zylindermitkinematik2.ftm -->
Pneumatikzylinders 39638 mit Kinematik Funktion im ft-Designer.
 Zylinder mit einer Kollisionskugel für Umschaltung in beide Richtungen.  