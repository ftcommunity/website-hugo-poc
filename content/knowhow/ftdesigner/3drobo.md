---
layout: "file"
hidden: true
title: "3D Robo"
date: "2016-03-20T00:00:00"
file: "3drobo.ftm"
konstrukteure: 
- "M.Bäter"
uploadBy:
- "M.Bäter"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/3drobo.rar
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/3drobo.rar -->
Modell eines 3D Robo mit großer Greifzange für Fischertechnik Kisten
