---
layout: "file"
hidden: true
title: "Pneumatik Roboter 5.zip"
date: "2012-05-21T00:00:00"
file: "pneumatikroboter5.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatikroboter5.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatikroboter5.zip -->
Pneumatik Roboter 5 aus dem Experimenta Schulprogramm.
