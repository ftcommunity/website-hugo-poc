---
layout: "file"
hidden: true
title: "Baukran.zip"
date: "2012-05-21T00:00:00"
file: "baukran.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/baukran.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/baukran.zip -->
Baukran. Original von Fischertechnik Starlifters Master Plus
