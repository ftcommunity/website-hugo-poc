---
layout: "file"
hidden: true
title: "Kistenschieber "
date: "2014-11-16T00:00:00"
file: "kistenschieber.ftm"
konstrukteure: 
- "M. Bäter"
uploadBy:
- "M. Bäter"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/kistenschieber.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/kistenschieber.ftm -->
Eine Maschine die Kisten aus einem Magazin auf ein Förderband schiebt.