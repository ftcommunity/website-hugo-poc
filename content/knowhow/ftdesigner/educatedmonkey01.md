---
layout: "file"
hidden: true
title: "Consul the Educated Monkey"
date: "2015-01-11T00:00:00"
file: "educatedmonkey01.ftm"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/educatedmonkey01.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/educatedmonkey01.ftm -->
Konstruktion der Multiplikationshilfe "Consul, The Educated Monkey" aus dem Jahr 1916 - ein kleiner mechanischer "Multiplikationsrechner" (Fotos siehe Bilderpool).