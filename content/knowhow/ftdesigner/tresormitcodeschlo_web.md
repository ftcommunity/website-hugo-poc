---
layout: "file"
hidden: true
title: "Tresor mit Codeschloß.zip"
date: "2012-05-21T00:00:00"
file: "tresormitcodeschlo_web.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/tresormitcodeschlo_web.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/tresormitcodeschlo_web.zip -->
Tresor mit Codeschloß. Original von FischerTechnik Profi-Computing.
