---
layout: "file"
hidden: true
title: "PneumaCube.zip"
date: "2012-05-21T00:00:00"
file: "pneumacube.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumacube.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumacube.zip -->
Mein PneumaCube. Original aus Lego. Peter Damen hat die Version ohne Elektronik entwickelt.
