---
layout: "file"
hidden: true
title: "Pneumatische Schiebetür.zip"
date: "2012-05-21T00:00:00"
file: "pneumatischeschiebetr.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/pneumatischeschiebetr.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/pneumatischeschiebetr.zip -->
Pneumatische Schiebetür. Original von Fischertechnik Pneumatik.
