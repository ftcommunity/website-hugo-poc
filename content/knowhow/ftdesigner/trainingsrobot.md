---
layout: "file"
hidden: true
title: "TrainingsRobot.zip"
date: "2012-05-21T00:00:00"
file: "trainingsrobot.zip"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/trainingsrobot.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/trainingsrobot.zip -->
TrainingsRobot von 1985. Ein sehr faszinierendes Roboter-Modell. Schon modifiziert auf Impulstaster. Original von FT mit Gabellichtschranke. Leider gibt es kein Steuerprogramm mehr dafür. Vielleicht bekommt es ja jemand hin...
