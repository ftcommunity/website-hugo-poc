---
layout: "file"
hidden: true
title: "LTSPICE-Dateien für Silberlinge"
date: 2022-06-28T15:10:29+02:00
file: "SilberlingeLTSPICE.zip"
konstrukteure: 
- "Joachim (jona2004)"
uploadBy:
- "Website-Team"
license: "unknown"
---
<!--more-->

#### Simulation der Silberlinge mit LTSPICE

Zu einigen Bausteinen (z.B. dem Grundbaustein) gibt es den Schaltplan, eine Testsimulationsumgebung und ein Symbol. Man kann also beliebig komplexe Silberlingschaltungen simulieren.

LTSPICE (https://de.wikipedia.org/wiki/LTspice) muss sich der Interessent runterladen und installieren (dauert, sind ca. 50MByte).
Gibt es bei Heise oder direkt bei Analogdevices. Ist sehr beliebt und ziemlich gut.

Im zip file gibt es ein README, wie man die erste Simulation hinkriegt.
