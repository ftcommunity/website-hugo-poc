---
title: "Elektronik"
weight: 4
---
fischertechnik und Elektronik?
Ja, das geht gut zusammen - schon seit 1971!
Hier findest Du hoffentlich all die Infos, nach denen Du anderswo vergeblich
gesucht hast.

Projekte, die einen Mikrocontroller (und Software) verwenden, gehören allerdings
eher in die Rubrik [Computing](../computing).
