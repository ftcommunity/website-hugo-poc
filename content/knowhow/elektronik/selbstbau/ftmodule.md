---
layout: "file"
hidden: true
title: "24 Silberlinge"
date: 2012-05-03T00:00:00
file: "ftmodule.zip"
konstrukteure: 
- "Thomas Habig (Triceratops)"
uploadBy:
- "Thomas Habig"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/ftmodule.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/ftmodule.zip -->
Frontansichten (fast) aller Silberlinge, diversen Nachbauten und Modifikationen,
des E-Tec Moduls sowie der IC-Bausteine (Nagelbretter) in zwei Auflösungen.

Alle Module sind als Einzeldatei sowohl in BMP als auch in GIF erstellt.
Eine Gesamtübersicht und eine Kurzbeschreibung (txt) liegt ebenfalls bei.
