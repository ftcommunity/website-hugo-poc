---
layout: "wiki"
hidden: true
title: "RC5 Codes für IR Contol Set 30344 (für den schwarzen Empfänger)"
date: 2016-08-24T00:00:00
konstrukteure: 
- "Dirk Uffmann"
uploadBy:
- "uffi"
license: "unknown"
legacy_id:
- /wiki1b8f.html
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/wiki1b8f.html?action=show&topic_id=40 -->
<!--
   Wiki

   Thema: RC5 Codes für IR Contol Set 30344 (für den schwarzen Empfänger)

   Aktuelle Version

   von: uffi

   am: 24.08.2016, 19:04:16 Uhr

   Historie:
   Version 3 von uffi am 24.08.2016, 18:02:57 Uhr
   Version 2 von uffi am 24.08.2016, 15:23:26 Uhr
   Version 1 von uffi am 24.08.2016, 14:56:29 Uhr

Das hier wäre dann Version 4, die auf .md umgestellt und etwas aufgehübscht
wurde. Uffi wird es mir wohl verzeihen.
-->

Falls jemand wie ich einen schwarzen Fernbedienungsempfänger aus 30344 IR
Control Set hat, aber nicht den passenden Fernbedienungssender dazu, dann
helfen ihm vielleicht folgende Infos, um sich ggf. selbst einen Sender zu
bauen.

Ich habe mir einen RC5-Code Infrarot-Scanner gebastelt, mit dem automatisiert
alle Codes durchprobiert werden und habe folgendes herausgefunden:

Der Empfänger arbeitet mit RC5 Code exakt nach Spezifikation, siehe z.B.  
[OpenDCC.de RC5](http://www.opendcc.de/info/rc5/rc5.html)  
[sprut.de RC5](http://www.sprut.de/electronic/ir/rc5.htm)

Das 6.-te Command Bit, das laut Spec in die Adresse gemappt ist, wird hier
nicht genutzt (das Bit daher als zweites Startbit auf 1 stehen lassen).

Der schwarze Empfänger aus fischertechnik (30344) wird mit folgender
Systemadresse angesprochen (ich vermute, dass der blaue Zweitempfänger
entweder eine andere Systemadresse oder zumindest andere Kommandos verwendet,
einen solchen habe ich aber nicht und kann es nicht überprüfen):

0x1B (hex) bzw. 27 (dec)

Das folgende sind die Command-Codes der Tasten (die sind genau entsprechend
der Nummerierung der Tasten in der Bedienungsanleitung codiert):

#### Motor 1
<table border = "1">
   <tr>
      <th>Befehl</th>
      <th>Code</th>
   </tr>
   <tr>
      <td>Rückwärts</td>
      <td>0x07 / 7</td>
   </tr>
   <tr>
      <td>Vorwärts</td>
      <td>0x08 / 8</td>
   </tr>
   <tr>
      <td>Leistungsstufe umschalten</td>
      <td>0x03 / 3</td>
   </tr>
</table>

#### Motor 2
<table border = "1">
   <tr>
      <th>Befehl</th>
      <th>Code</th>
   </tr>
   <tr>
      <td>Links</td>
      <td>0x09 / 9</td>
   </tr>
   <tr>
      <td>Rechts</td>
      <td>0x0A / 10</td>
   </tr>
   <tr>
      <td>Leistungsstufe umschalten</td>
      <td>0x04 / 4</td>
   </tr>
</table>

#### Motor 3
<table border = "1">
   <tr>
      <th>Befehl</th>
      <th>Code</th>
   </tr>
   <tr>
      <td>Rechtslauf</td>
      <td>0x01 / 1</td>
   </tr>
   <tr>
      <td>Linkslauf</td>
      <td>0x02 / 2</td>
   </tr>
   <tr>
      <td>Leistungsstufe umschalten</td>
      <td>0x05 / 5</td>
   </tr>
</table>

"Leistungsstufe umschalten" bedeutet, zwischen langsam und schnell hin- und
her zu schalten.

Die Bedienungsanleitung zu 30344 findet ihr hier:
https://ft-datenbank.de/binary/7024
<!-- https://ft-datenbank.de/web_document.php?id=c69b5037-8ba0-48d7-aa2c-f6bb44ef7284
der funzt aber aus irgendwelchen Gründen nicht mehr. Ist nicht der Einzige! -->
