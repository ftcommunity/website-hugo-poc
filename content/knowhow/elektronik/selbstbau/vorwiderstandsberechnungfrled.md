---
layout: "file"
hidden: true
title: "Vorwiderstands Berechnung für LED"
date: 2008-03-08T00:00:00
file: "vorwiderstandsberechnungfrled.xls"
konstrukteure: 
- "Quirin Schweigert"
uploadBy:
- "Quirin Schweigert"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/vorwiderstandsberechnungfrled.xls
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/vorwiderstandsberechnungfrled.xls -->
Berechnet den erfordelichen Vorwiderstand für LEDs bei einstelbarer Spannung.
(ist noch nicht ganz fertig)
PS: Nur in die roten Felder schreiben!
In eine Spalte immer die gleichen Werte schreiben!
