---
layout: "file"
hidden: true
title: "MiniBots vorläufige Schalterstellungen"
date: 2016-01-20T00:00:00
file: "minibotsvorlufigeschalterstellungen.pdf"
konstrukteure: 
- "fischertechnik GmbH"
- "Stefan Falk"
uploadBy:
- "Stefan Falk"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/minibotsvorlufigeschalterstellungen.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/minibotsvorlufigeschalterstellungen.pdf -->
Aus dem im PDF verlinkten Forumsthread habe ich die Antwort von fischertechnik
in eine PDF gebracht.
Vielleicht ist das ja für jemanden nützlich, der sich das gerne ausdrucken
möchte.
Eine offizielle PDF von fischertechnik soll ja noch kommen.
