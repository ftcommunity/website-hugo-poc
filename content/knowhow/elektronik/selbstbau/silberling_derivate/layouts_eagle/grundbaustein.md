---
layout: "file"
hidden: true
title: "Grundbaustein GB-H4"
date: 2005-12-31T00:00:00
file: "grundbaustein.brd"
konstrukteure: 
- "Thomas Kaiser (thkais)"
uploadBy:
- "Thomas Kaiser"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/platinenlayoutsbrd/grundbaustein.brd
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/platinenlayoutsbrd/grundbaustein.brd -->
