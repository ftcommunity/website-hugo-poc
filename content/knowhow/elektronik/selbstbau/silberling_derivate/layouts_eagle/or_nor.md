---
layout: "file"
hidden: true
title: "OR NOR"
date: 2005-12-31T00:00:00
file: "or_nor.brd"
konstrukteure: 
- "Thomas Kaiser (thkais)"
uploadBy:
- "Thomas Kaiser"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/platinenlayoutsbrd/or_nor.brd
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/platinenlayoutsbrd/or_nor.brd -->
