---
layout: "file"
hidden: true
title: "AND NAND Baustein"
date: 2005-06-06T00:00:00
file: "and_nand.pdf"
konstrukteure: 
- "Michael Becker"
uploadBy:
- "Michael Becker"
license: "unknown"
legacy_id:
- /data/downloads/robopro/and_nand.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/robopro/and_nand.pdf -->
<!-- Ja, der war total falsch "abgebogen". Datum rekonstruiert. -->
Schaltplan "AND NAND Baustein" (36482) - Bestückungsvariante vom
["OR NOR Baustein"](../ornor)!
