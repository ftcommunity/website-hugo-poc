---
layout: "file"
hidden: true
title: "Voltmeter Hobbylabor 1"
date: 2005-06-06T00:00:00
file: "voltmeter_hobbylabor_1.pdf"
konstrukteure: 
- "Michael Becker"
uploadBy:
- "Michael Becker"
license: "unknown"
legacy_id:
- /data/downloads/ebausteine/schaltplne/voltmeter_hobbylabor_1.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ebausteine/schaltplne/voltmeter_hobbylabor_1.pdf -->
Schaltplan "Voltmeter Hobbylabor 1" (31185)
