---
title: "Silberlinge (1971)"
weight: 10
legacy_id:
- /downloads6ef5.html
- /php/downloads/Schaltpl%E4ne
- /downloads.php?kategorie=Schaltpl%E4ne
---
<!-- https://www.ftcommunity.de/downloads6ef5.html?kategorie=Schaltpl%E4ne -->
![Grundbaustein](./7507.jpg "36391 - Grundbaustein - Mit freundlicher Erlaubnis von fischerfriendsman")
Bei fischertechnik gab es schon recht früh einen Bezug zur Elektronik.
Die alten Elektronik-Bausteine sind zwar schon seit Jahrzehnten nur noch
antiquarisch erhältlich, aber bei den "alten Hasen" immer noch im Einsatz.

In dieser Rubrik gibt es die Schaltpläne der Originale.

<!--
Uploads von Michael Becker, 6.6.2005
[31185](./voltmeter_hobbylabor_1.pdf) - Voltmeter "Hobbylabor 1"  
[31360](./schaltstab_le1.pdf) - Schaltstab le1  
[36391](./grundbaustein.pdf) - Grundbaustein  
[36392](./relais_baustein_h4rb.pdf) - Relais I (h4rb)  
[36393](./gleichrichter.pdf) - Gleichrichter  
[36479](./flipflop.pdf) - Flipflop  
[36480](./monoflop.pdf) - Monoflop  
[36481](./ornor.pdf) - OR / NOR  
 36482 - AND / NAND  
[36483](./dynamischer_and.pdf) - Dynamisches AND  
[36733](./verstaerker_baustein_ut4vb.pdf) - Verstärker ut4vb  
[36734](./relais_baustein_ut4rb.pdf) - Relais III (ut4rb)  
[36735](./transistor_poti_baustein_ut4tp.pdf) - Transistor + Potentiometer ut4tp  
[37140](./experimentierfeld_hobbylabor_1.pdf) - Experimentierfeld "Hobbylabor 1"  
[37158](./potentiometer_baustein_h1pb.pdf) - Potentiometer h1pb  
[37683](./relais_baustein_rb2.pdf) - Relais II (rb2)  
 -->
 
