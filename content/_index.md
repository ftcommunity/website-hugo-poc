---
Title: "Start"
ordersectionsby: "weight"
---

# ![fischertechnik community](images/logo-full.png)

Du baust gerne mit fischertechnik? Hier gibt es für dich ....

* eine [Bildersammlung](./bilderpool) (schau dir Fotos anderer fischertechnik-Freunde an und nutze sie als Vorbild für eigene Modelle)
* einen [Servicebereich](./knowhow) (lade Bauanleitungen und andere Dokumente herunter)
* ein [Forum](https://forum.ftcommunity.de/) (tritt in Kontakt mit Gleichgesinnten)
* die kostenlose Zeitschrift [ft:pedia](./ftpedia) (mit Artikeln und Modellvorstellungen)
* [Veranstaltungen](fans/veranstaltungen), bei denen du vor Ort erlebst, was mit fischertechnik alles möglich ist.
  Weitere aktuelle Infos dazu auch im [Forum](https://forum.ftcommunity.de/viewforum.php?f=39).
    * 12.-13. Oktober 2024: [ftc:süd:con:24](./fans/veranstaltungen/suedconvention2024/) im Fördertechnik Museum Sinsheim
* eine [Teile-Datenbank](./ftdatenbank) (nutze die umfangreichen Informationen über aktuelle oder frühere Bauteile und Baukästen sowie Bauanleitungen und andere Dokumente)

