---
hidden: true
layout: "issue"
title: "4 / 2018"
file: "ftpedia-2018-4.pdf"
publishDate: 2018-12-24T00:00:00
date: 2018-12-24T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2018-4.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-4.pdf -->
