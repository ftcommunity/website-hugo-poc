---
layout: "file"
title: "Staubschutz-Stückliste für den Urlaubskasten V2"
date: 2018-09-25T00:00:00
file: "staubschutzstcklistefrdenurlaubskasten201809.pdf"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/staubschutzstcklistefrdenurlaubskasten201809.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/staubschutzstcklistefrdenurlaubskasten201809.pdf -->
Im Wohnzimmer-Dienstreisen-Urlaubs-Notfallkasten aus der ft:pedia 1/2016 wird
eine Bauplatte 500 als Deckel benutzt.
Durch deren Nuten fällt bei längerem Herumstehen gerne Staub ins Innere des
Kastens.
Deshalb gibt es hier ein Staubschutz-Einlegeblatt, das gleichzeitig eine
Stückliste darstellt und einige Modellanregungen zeigt.
Zur Beschreibung siehe ft:pedia 2/2017 und das Update in ft:pedia 3/2018.
