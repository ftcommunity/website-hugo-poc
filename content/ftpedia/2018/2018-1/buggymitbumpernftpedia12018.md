---
hidden: true
layout: "file"
title: "Buggy mit Bumpern"
date: 2018-03-30T00:00:00
file: "buggymitbumpernftpedia12018.ftm"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/buggymitbumpernftpedia12018.ftm
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/buggymitbumpernftpedia12018.ftm -->
Designer-Datei zum Buggy mit Bumpern aus ft:pedia 1 / 2018 (Scratch mit fischertechnik)
