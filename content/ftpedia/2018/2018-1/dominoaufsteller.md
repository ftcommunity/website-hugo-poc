---
hidden: true
layout: "file"
title: "Dominosteine-Aufsteller"
date: 2018-03-30T00:00:00
file: "dominoaufsteller.txt"
konstrukteure:
- "Thomas Püttmann (geometer)"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/dominoaufsteller.txt
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/dominoaufsteller.txt -->
Liste der Einzelteile zum Bau des Dominosteine-Aufstellers aus ft:pedia 1 / 2018.
