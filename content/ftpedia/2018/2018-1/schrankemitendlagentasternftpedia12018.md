---
hidden: true
layout: "file"
title: "Schranke mit Endlagentastern"
date: 2018-03-30T00:00:00
file: "schrankemitendlagentasternftpedia12018.ftm"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/schrankemitendlagentasternftpedia12018.ftm
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/schrankemitendlagentasternftpedia12018.ftm -->
Designer-Datei zum Schranken-Modell mit Endlagentastern und Lichtschranke aus ft:pedia 1 / 2018 (Scratch mit fischertechnik)
