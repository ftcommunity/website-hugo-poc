---
hidden: true
layout: "file"
title: "Neopixelcontroller"
date: 2018-03-22T00:00:00
file: "neopixelcontroller.zip"
konstrukteure:
- "Christian Bergschneider"
- "Stefan Fuss"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/neopixelcontroller.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/neopixelcontroller.zip -->
STLs und Sourcen zum Neopixel-Controller aus ft:pedia 1 / 2018, S. 53
