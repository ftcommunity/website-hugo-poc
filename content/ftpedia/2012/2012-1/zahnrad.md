---
layout: "file"
hidden: true
title: "Zahnrad"
date: "2012-03-31T00:00:00"
file: "zahnrad.zip"
konstrukteure: 
- "Thomas Püttmann (geometer)"
uploadBy:
- "Thomas Püttmann"
license: "unknown"
legacy_id:
- /data/downloads/software/zahnrad.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/zahnrad.zip -->
Dateien zu meinem Artikel in der ft:pedia 1/2012. Enthalten sind ein Postscript-Programm zum Zeichnen von fischertechnik-kompatiblen Zahnrädern mit beliebig vielen Zähnen und eine Druckvorlage für ein Modell zum Verständnis der Evolventenverzahnung.
