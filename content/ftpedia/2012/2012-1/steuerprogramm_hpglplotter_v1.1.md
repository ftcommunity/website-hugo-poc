---
layout: "file"
hidden: true
title: "Steuerprogramm HP-GL-Plotter v1.1"
date: "2012-03-31T00:00:00"
file: "steuerprogramm_hpglplotter_v1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/steuerprogramm_hpglplotter_v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/steuerprogramm_hpglplotter_v1.1.zip -->
Steuerprogramm (siehe ft:pedia 1/2012) für den in ft:pedia 4/2011vorgestellten HP-GL-Plotter (siehe auch [minimalistischer Präzisionsplotter](http://www.ftcommunity.de/categories.php?cat_id=2456). Änderung zu Version 1.0: Nur noch eine HPGL-Datei erforderlich.
