---
hidden: true
layout: "file"
title: "ftUNIpi Platinenlayout-Dateien"
date: 2016-12-21T00:00:00
file: "ftunipi_layout.zip"
konstrukteure:
- "Christian Bergschneider"
- "Stefan Fuss"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftunipi_layout.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftunipi_layout.zip -->
Das ZIP-File enthält die Layout-Dateien im Eagle-Format um die Platine bei
einem Platinenbelichtungsservice herstellen zu lassen.

Vor einer Bestellung unbedingt das Readme und den Artikel lesen!
