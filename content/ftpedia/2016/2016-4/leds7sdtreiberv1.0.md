---
layout: "file"
hidden: true
title: "I²C-Treiber für 7-Segment-LED-Display von Sparkfun"
date: "2017-03-25T00:00:00"
file: "leds7sdtreiberv1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/leds7sdtreiberv1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/leds7sdtreiberv1.0.zip -->
RoboPro-I²C-Teiber für das 7-Segment-LED-Display von Sparkfun am TX(T) Controller. Nähere Erläuterungen in ft:pedia 4/2016.