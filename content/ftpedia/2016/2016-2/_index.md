---
hidden: true
layout: "issue"
title: "2 / 2016"
file: "ftpedia-2016-2.pdf"
publishDate: 2016-06-25T00:00:00
date: 2016-06-25T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2016-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2016-2.pdf -->
