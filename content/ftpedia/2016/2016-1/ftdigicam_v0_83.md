---
layout: "file"
hidden: true
title: "Digitalkamera ftDigiCam (v0.83)"
date: "2016-03-22T00:00:00"
file: "ftdigicam_v0_83.zip"
konstrukteure: 
- "Torsten Stuehn"
uploadBy:
- "Torsten"
license: "unknown"
legacy_id:
- /data/downloads/software/ftdigicam_v0_83.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/ftdigicam_v0_83.zip -->
Softwarepaket zum Artikel "Digitalkamera mit Autofokus und Live-Video-Vorschau" aus der ft:pedia 1/2016. Enthält den Python2.7 Interpreter für den TXT, die Module ftrobopy.py und ftrobopytools.so und das Python-Programm ftDigiCam.py zur Steuerung des Modells. Der ft:pedia-Artikel enthält eine ausführliche Installationsanleitung.
