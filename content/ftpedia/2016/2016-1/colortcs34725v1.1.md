---
layout: "file"
hidden: true
title: "I²C-Treiber für Farbsensor TCS34725 v1.1"
date: "2017-02-03T00:00:00"
file: "colortcs34725v1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/colortcs34725v1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/colortcs34725v1.1.zip -->
RoboPro-I²C-Teiber für den Farbsensor TCS34752 am TX Controller. Nähere Erläuterungen in ft:pedia 1/2016, S. 79-89.