---
layout: "file"
hidden: true
title: "I²C-Treiber für Temperatursensor MCP9808 v1.0"
date: "2017-02-03T00:00:00"
file: "thermometermcp9808v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/thermometermcp9808v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/thermometermcp9808v1.0.zip -->
RoboPro-I²C-Teiber für den Temperatursensor MCP9808 am TX(T) Controller. Auflösung 0,0625°, Genauigkeit 0,25°. Nähere Erläuterungen in ft:pedia 4/2015, S. 44-48.
