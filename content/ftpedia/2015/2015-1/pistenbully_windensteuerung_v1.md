---
layout: "file"
hidden: true
title: "Pistenbully-Windensteuerung"
date: "2017-02-03T00:00:00"
file: "pistenbully_windensteuerung_v1.rpp"
konstrukteure: 
- "Jörg Busch (ft-familie)"
uploadBy:
- "Jörg Busch (ft-familie)"
license: "unknown"
legacy_id:
- /data/downloads/robopro/pistenbully_windensteuerung_v1.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/pistenbully_windensteuerung_v1.rpp -->
Regelung der Windendrehung, der Seiltrommel und des Spillkopfes einer Pistenbully-Winde.
Winde reagiert nur auf die Seilspannung
Pistenbully von Jörg aus ft-pedia 1/2015
