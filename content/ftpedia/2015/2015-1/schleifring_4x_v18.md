---
hidden: true
layout: "file"
title: "Schleifring_4x_v18.zip"
date: 2015-03-27T00:00:00
file: "schleifring_4x_v18.zip"
konstrukteure:
- "Harald Steinhaus"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/schleifring_4x_v18.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/schleifring_4x_v18.zip -->
Zu ftpedia Nr. 17 (1/2015): 3D-Drucker-Daten für den Schleifring mit 4 Bahnen
