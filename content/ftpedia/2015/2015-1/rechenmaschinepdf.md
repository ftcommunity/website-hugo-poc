---
layout: "file"
hidden: true
title: "Bauanleitung Rechenmaschine"
date: "2015-04-05T00:00:00"
file: "rechenmaschine.pdf"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/rechenmaschine.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/rechenmaschine.pdf -->
Ergänzung zum FT-Pedia Artikel 01/2015 von Thomas "Geometer" Püttmann

Bauanleitung Rechenmaschine
