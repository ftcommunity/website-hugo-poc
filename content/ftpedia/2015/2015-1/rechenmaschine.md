---
layout: "file"
hidden: true
title: "Rechenmaschine von Thomas "
date: "2015-03-29T00:00:00"
file: "rechenmaschine.ftm"
konstrukteure: 
- "Andreas Gürten (Laserman)"
uploadBy:
- "Andreas Gürten"
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/rechenmaschine.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/rechenmaschine.ftm -->
Als Ergänzung zum FT-Pedia Artikel 01 / 2015
