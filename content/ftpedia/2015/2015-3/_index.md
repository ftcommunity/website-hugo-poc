---
hidden: true
layout: "issue"
title: "3 / 2015"
file: "ftpedia-2015-3.pdf"
publishDate: 2015-09-25T00:00:00
date: 2015-09-25T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2015-3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2015-3.pdf -->
