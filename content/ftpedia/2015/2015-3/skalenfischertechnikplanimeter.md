---
hidden: true
layout: "file"
title: "Skalen fischertechnik-Planimeter"
date: 2015-09-27T00:00:00
file: "skalenfischertechnikplanimeter.pdf"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/skalenfischertechnikplanimeter.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/skalenfischertechnikplanimeter.pdf -->
Skalen für die Messräder des fischertechnik-Planimeters aus ft:pedia 3/2015
(S. 25-30).
Ausschneiden und mit Tesafilm/doppelseitigem Klebeband auf Spurkranz/Flachnabe
aufkleben.
