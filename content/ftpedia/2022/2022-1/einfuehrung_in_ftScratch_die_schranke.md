---
layout: "file"
title:  "Einführung in ftScratch - Die Schranke"
date: 2022-03-26T00:00:00+0100
publishDate: 2022-03-26T00:00:00+0100
file: "einfuehrung_in_ftScratch_die_schranke.zip"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
