---
layout: "file"
title:  "ROBO Pro-Programme zum Beitrag Sensoren am TXT - Die Kamera"
date: 2022-03-26T00:00:00+0100
publishDate: 2022-03-26T00:00:00+0100
file: "robopro_Programme_sensoren_am_txt_kamera.zip"
konstrukteure:
- "Kurt Mexner"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
