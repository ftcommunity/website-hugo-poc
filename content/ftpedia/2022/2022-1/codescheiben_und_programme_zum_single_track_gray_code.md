---
layout: "file"
title:  "Codescheiben und Programme zum Single Track Gray Code"
date: 2022-03-26T00:00:00+0100
publishDate: 2022-03-26T00:00:00+0100
file: "codescheiben_und_programme_zum_single_track_gray_code.tar.zip"
konstrukteure:
- "Florian Bauer"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
