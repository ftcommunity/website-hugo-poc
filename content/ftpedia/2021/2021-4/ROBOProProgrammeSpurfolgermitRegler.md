---
layout: "file"
hidden: true
title: "ROBO Pro-Programme Spurfolger mit Regler"
date: 2021-12-23T19:40:59+01:00
file: "ROBOProProgrammeSpurfolgermitRegler.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Website-Team"
license: "unknown"
---
ROBO Pro-Programme für den analogen Spurfolger mit P-, PD- und 
PID-Regler 
