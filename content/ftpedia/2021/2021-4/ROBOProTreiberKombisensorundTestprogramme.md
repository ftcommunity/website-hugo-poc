---
layout: "file"
hidden: true
title: "ROBOPro-Treiber Kombisensor und  Testprogramme"
date: 2021-12-23T19:38:17+01:00
file: "ROBOProTreiberKombisensorundTestprogramme.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Website-Team"
license: "unknown"
---
Verbesserter ROBO Pro-Treiber für den fischertechnik-Kombisensor 
BMX055, Testprogramme
