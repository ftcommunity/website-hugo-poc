---
layout: "file"
hidden: true
title: "fischertechnik-Kreis-Parcours A0 (ohneFarbflächen)"
date: 2021-12-23T19:41:50+01:00
file: "fischertechnikKreis-ParcoursA0ohneFarbflächen.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Website-Team"
license: "unknown"
---
Kreisförmiger fischertechnik-Parcours ohne Farbflächen in A0 
