---
layout: "file"
hidden: true
title: "ftDuino-Sketche für den Buggy"
date: 2021-12-23T19:39:57+01:00
file: "ftDuino-SketchefürdenBuggy.zip"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Website-Team"
license: "unknown"
---
ftDuino-Sketche für den Buggy aus "fischertechnik-Roboter mit 
Arduino" 
