---
layout: "file"
hidden: true
title: "ROBOPro-Programm IR-Sender und Empfänger"
date: 2021-12-23T19:40:42+01:00
file: "ROBOProProgrammIRSenderundEmpfänger.zip"
konstrukteure: 
- "Kurt Mexner"
uploadBy:
- "Website-Team"
license: "unknown"
---
ROBO Pro-Programm "IR Sender und Empfänger"
