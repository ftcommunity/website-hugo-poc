---
layout: "file"
hidden: true
title: "Designerdatei Synchronuhr mit Stirnradgetriebe"
date: 2021-12-23T19:39:37+01:00
file: "DesignerdateiSynchronuhrmitStirnradgetriebe.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Website-Team"
license: "unknown"
---
3D-Bauanleitung für die Synchronuhr mit Stirnradgetrieben 
(fischertechnik-Designer) 
