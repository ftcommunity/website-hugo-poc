---
layout: "file"
hidden: true
title: "Designerdatei Analoger Spurfolger mit Kamera"
date: 2021-12-23T19:39:03+01:00
file: "DesignerdateiAnalogerSpurfolgermitKamera.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "ebsite-Team"
license: "unknown"
---

3D-Bauanleitung für den analogen Spurfolger mit Kamera 
(fischertechnik-Designer)
