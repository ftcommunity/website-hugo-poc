---
layout: "file"
hidden: true
title: "FT 4fach Eingang"
date: 2020-12-23T11:37:24+01:00
publishDate: 2020-12-24T00:00:00+0100
file: "FT4fachEingang.rpp"
konstrukteure: 
- "Kurt Mexner"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---

Die acht Eingänge des TXT sind schnell belegt. Wenn Endlagenschalter,
Steuerungsschalter, Fototransitor oder weitere Module angeschlossen werden
müssen, gelangt der Controller schnell an seine Grenzen. Ein weiterer TXT
oder eine Erweiterung über den EXT-Anschluss geht ins Geld. Hier kommt eine
einfache und kostengünstige Eigenbaulösung, wie ein Eingang als
Mehrfach-Eingang benutzt werden kann.
