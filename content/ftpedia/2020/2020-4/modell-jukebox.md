---
layout: "file"
title:  "Programm zum Artikel 'Elektronik-Module (5)' - Jukebox"
date: 2020-12-30T00:00:00+0100
publishDate: 2020-12-30T00:00:00+0100
file: "modell-jukebox.zip"
konstrukteure:
- "Hans-Christian Funke"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---

