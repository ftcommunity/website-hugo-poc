---
layout: "file"
title:  "ftpedia-Sonderbeilage Mini-Modelle der ersten 39 Ausgaben"
date: 2020-12-24T00:00:00+0100
publishDate: 2020-12-24T00:00:00+0100
file: "ftpediaMiniModelle.pdf"
konstrukteure:
- "Rüdiger Riedel"
uploadBy: 
- "ft:pedia-Redaktion"
license: "unknown"
---
Viele, viele kleine Modelle sind in den letzten 10 Jahren von fleißigen und
kreativen Fans der fischertechnik-Welt für die ft:pedia erfunden worden.
Jetzt werden sie zusammenfassend präsentiert.
