---
layout: "file"
hidden: true
title: "ROBOPro-Programm für eigene Experimente zur Messung der gesteigerten Energieausbeute mit dem Solarzellen-Nachführer"
date: 2020-03-28T09:16:20+01:00
file: "Solarzellennachführer-Experiment202004.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---

