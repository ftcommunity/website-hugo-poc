---
hidden: true
layout: "issue"
title: "1 / 2013"
file: "ftpedia-2013-1.pdf"
publishDate: 2013-04-04T00:00:00
date: 2013-04-04T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2013-1.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2013-1.pdf -->
