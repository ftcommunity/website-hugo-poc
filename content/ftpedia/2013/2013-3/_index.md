---
hidden: true
layout: "issue"
title: "3 / 2013"
file: "ftpedia-2013-3.pdf"
publishDate: 2013-09-27T00:00:00
date: 2013-09-27T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2013-3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2013-3.pdf -->
