---
layout: "file"
hidden: true
title: "I²C-Treiber für Ultraschall-Sensor SRF02 (Devantech) v1.0"
date: "2017-02-03T00:00:00"
file: "treiberultraschallsensorsrf02v1.0.rpp"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/treiberultraschallsensorsrf02v1.0.rpp
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/treiberultraschallsensorsrf02v1.0.rpp -->
I²C-Treiber für den Ultraschallsensor SRF02 von Devantech inklusive Testprogramm (Vergleichsmessung mit fischertechnik-Abstandssensor). Näheres im entsprechenden Beitrag der ft:pedia 4/2013.