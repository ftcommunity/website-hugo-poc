---
layout: "file"
hidden: true
title: "C-Code zur Ziffernerkennung mit einer CMOS-Kamera am AVR-Controller"
date: 2020-11-25T17:30:14+01:00
file: "ziffern.zip"
konstrukteure: 
- "Dirk Uffmann"
uploadBy:
- "website-team"
license: "unknown"
---

Download-Paket mit C-Code zum Artikel: Ziffernerkennung über eine CMOS-Kamera am AVR-Controller. Komponenten: Arduino-Board mit ATMEGA2560 und Alientek Kameramodul V2.2 mit einer Omnivision OV7670 CMOS-VGA-Kamera inkl. Averlogic FIFO AL422B sowie LCD-2408(V1.1)-Farbdisplay mit Display-Controller ILI9325D. Damit lassen sich Ziffern identifizieren, die von einer Vorlage abgelesen werden. Die Software zur Bildverarbeitung enthält: Schwellwertfilter, Kantenfilter, Segmentierung & Ziffernerkennung.
