---
hidden: true
layout: "file"
title: "TX-Fernsteuerung mit dem RPi"
date: 2014-12-23T00:00:00
file: "txfernsteuerungmitdemrpi.zip"
konstrukteure:
- "Raphael Jacob"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/txfernsteuerungmitdemrpi.zip
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/txfernsteuerungmitdemrpi.zip -->
Dies ist das Package für den ft:pedia Beitrag. Enthalten sind folgende Daten:

- Alle Befehle als Text-Datei
- Die eigentliche Software für den RPi
- Die ROBOPro Software
- Die Software Putty (zur SSH-Verbindung)