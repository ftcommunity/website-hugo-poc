---
hidden: true
layout: "file"
title: "C-Code zum Ansteuern des ft-Universal-Interface"
date: 2018-09-25T00:00:00
file: "ftui_pwm.zip"
konstrukteure:
- "Dirk Uffmann"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/ftui_pwm.zip
- /ftpedia/2015/2015-1/ftui_pwm
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/ftui_pwm.zip -->
Siehe den Artikel in der ft_pedia 2015/1: "PWM-Motorsteuerung am fischertechnik-Universal-Interface"

Edit des website-teams: 24.11.20202: Der zu dieser Datei gehörende Artikel ist in der ft:pedia [2014-2](https://ftcommunity.de/ftpedia/2014/2014-2/ftpedia-2014-2.pdf#page=30) erschienen, die Fortsetzung in der ft:pedia [2015-4](https://ftcommunity.de/ftpedia/2015/2015-4/ftpedia-2015-4.pdf#page=49).
