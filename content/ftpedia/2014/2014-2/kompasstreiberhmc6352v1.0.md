---
layout: "file"
hidden: true
title: "I²C-Treiber für Kompass-Sensor HMC6352 v1.0"
date: "2017-02-03T00:00:00"
file: "kompasstreiberhmc6352v1.0.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/kompasstreiberhmc6352v1.0.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/kompasstreiberhmc6352v1.0.zip -->
RoboPro-Treiber für I²C-Kompass-Sensor HMC6352 von Honeywell. Näheres im entsprechenden Beitrag in der ft:pedia 2/2014.