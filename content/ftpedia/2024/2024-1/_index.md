---
hidden: true
layout: "issue"
title: "1 / 2024"
file: "ftpedia-2024-1.pdf"
publishDate: 2024-03-30T00:00:00+01:00
date: 2024-03-30T00:00:00+01:00
uploadBy:
- "ft:pedia-Redaktion"
---
