---
hidden: true
layout: "issue"
title: "3 / 2024"
file: "ftpedia-2024-3.pdf"
publishDate: 2024-09-27T00:00:00+01:00
date: 2024-09-28T00:00:00+01:00
uploadBy:
- "ft:pedia-Redaktion"
---