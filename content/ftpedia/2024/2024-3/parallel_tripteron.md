---
layout: "file"
title:  "Parallel Tripteron"
date: 2024-09-24T00:00:00+0100
publishDate: 2024-09-28T00:00:00+0100
file: "parallel_tripteron.zip"
konstrukteure:
- "Florian Bauer" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
