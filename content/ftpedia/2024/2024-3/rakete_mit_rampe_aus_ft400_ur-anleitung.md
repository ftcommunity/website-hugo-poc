---
layout: "file"
title:  "Rakete mit Rampe aus ft 400 Ur-Anleitung"
date: 2024-09-24T00:00:00+0100
publishDate: 2024-09-28T00:00:00+0100
file: "rakete_mit_rampe_aus_ft400_ur-anleitung.zip"
konstrukteure:
- "Stefan Falk" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
