---
layout: "file"
title:  "Download zum Artikel 'Der adaptive Greifer'"
date: 2024-12-23T00:00:00+0100
publishDate: 2024-12-24T00:00:00+0100
file: "fin-ray-gripper-adapter.zip"
konstrukteure:
- "Stephan Kallauch" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
