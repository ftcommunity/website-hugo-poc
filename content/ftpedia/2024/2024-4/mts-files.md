---
layout: "file"
title:  "Download zum Artikel 'Abstandsmessung mit Magnetostriktion'"
date: 2024-12-18T00:00:00+0100
publishDate: 2024-12-24T00:00:00+0100
file: "mts-files.zip"
konstrukteure:
- "Florian Bauer" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
