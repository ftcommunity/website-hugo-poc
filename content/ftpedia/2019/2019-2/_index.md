---
hidden: true
layout: "issue"
title: "2 / 2019"
file: "ftpedia-2019-2.pdf"
publishDate: 2019-06-29T00:00:00+0200
date: 2019-06-29T00:00:00+0200
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2019-2.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2019-2.pdf -->
