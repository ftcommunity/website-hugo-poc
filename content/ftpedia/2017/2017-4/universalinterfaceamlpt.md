---
layout: "file"
hidden: true
title: "Universal-Interface-Treiber"
date: "2017-12-19T00:00:00"
file: "universalinterfaceamlpt.zip"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/universalinterfaceamlpt.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/universalinterfaceamlpt.zip -->
Hier sind die Treiber und Beispielprogramme zum Artikel "Das Universal-Interface am LPT-Port" in ft:pedia 4/2017. Ich habe umfish.dll zu umfishcount.dll erweitert und Counterbefehle sowie Befehle für Schrittmotoren hinzugefügt. Das Packet enthält ein Diagnose-Programm in Terrapin-Logo und 2 Formular-Programme in Java. Dazu gehören die Bibliotheken uiplotter.jar, Parser.jar und aplu5.jar,  mit denen man beliebige Funktionsgraphen mit dem alten Fischertechnik-Plotter malen kann. Mit jd-gui.exe kann man bequem die Methoden einer jar-Datei einsehen. Alles läuft unter winxp, win7 und win10 32Bit. 