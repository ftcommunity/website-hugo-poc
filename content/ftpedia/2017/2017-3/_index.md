---
hidden: true
layout: "issue"
title: "3 / 2017"
file: "ftpedia-2017-3.pdf"
publishDate: 2017-09-30T00:00:00
date: 2017-09-30T00:00:00
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2017-3.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2017-3.pdf -->
