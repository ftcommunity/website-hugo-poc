---
layout: "file"
hidden: true
title: "Treiber für das Robo Interface"
date: "2017-09-29T00:00:00"
file: "robotreiber.zip"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/robotreiber.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/robotreiber.zip -->
In dem ft:pedia Heft 3/2017 erscheint mein Artikel "Der alte fischertechnik-Plotter mit neuen
Treibern". Hier sind die dazugehörigen Windows-Treiber. Damit lässt sich das Robo Interface mit den Sprachen c++, c#, Terrapin Logo und Java online ansteuern. Beispielprogramme in Terrapin Logo und Java zeigen, wie das geht. Interessant sind die neuen Stepper-Befehle, mit denen die alten Schrittmotoren des Fischertechnik-Plotters gesteuert werden können. Alles wurde unter win7 sowie win10 erprobt. Basis ist ein Update der FT-Bibliothek umfish40.dll (v 4.3.77.0 ) von Ulrich Müller aus 2010.