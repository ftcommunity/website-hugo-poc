---
layout: "file"
hidden: true
title: "I²C-Treiber für das vierstellige 7-Segment-LED-Display von Adafruit v1.1"
date: "2017-03-25T00:00:00"
file: "leda7sdv1.1.zip"
konstrukteure: 
- "Dirk Fox"
uploadBy:
- "Dirk Fox"
license: "unknown"
legacy_id:
- /data/downloads/robopro/leda7sdv1.1.zip
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/robopro/leda7sdv1.1.zip -->
ROBO Pro-Treiber für das I²C-7-Segment-LED-Display von Adafruit (HT16K33). Nähere Erläuterungen in ft:pedia 1/2017, S. 86-91.