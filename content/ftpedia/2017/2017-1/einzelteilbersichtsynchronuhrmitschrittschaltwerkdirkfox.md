---
hidden: true
layout: "file"
title: "Einzelteilübersicht der Synchronuhr mit Schrittschaltwerk"
date: 2017-06-12T00:00:00
file: "einzelteilbersichtsynchronuhrmitschrittschaltwerkdirkfox.pdf"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/einzelteilbersichtsynchronuhrmitschrittschaltwerkdirkfox.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/einzelteilbersichtsynchronuhrmitschrittschaltwerkdirkfox.pdf -->
Einzelteilübersicht der Synchronuhr mit Schrittschaltwerk, vorgestellt in
Ausgabe 1/2017 der ft:pedia.
