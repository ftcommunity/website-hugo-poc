---
hidden: true
layout: "file"
title: "Film vom Wankelmotor"
date: 2017-10-24T00:00:00
file: "wankelmotor.mp4"
konstrukteure:
- "Andreas Gürten (Laserman)"
uploadBy:
- "-LegacyAdmin-"
license: "unknown"
legacy_id:
- /data/downloads/ftpediadateien/wankelmotor.mp4
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/data/downloads/ftpediadateien/wankelmotor.mp4 -->
Zu Heft 1-2017

Hier sieht man schön, daß und wie der Motor funktioniert
