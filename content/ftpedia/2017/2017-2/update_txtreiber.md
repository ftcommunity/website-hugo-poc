---
layout: "file"
hidden: true
title: "TreiberUpdate für den TX"
date: "2017-07-01T00:00:00"
file: "update_txtreiber.rar"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/software/update_txtreiber.rar
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/software/update_txtreiber.rar -->
In der neusten Ausgabe 2/2017 von ft:pedia beschreibe ich die Programmierung des TX-Controllers mit klassischen Programmiersprachen. Da gelegentlich Treiberprobleme aufgetreten sind, habe ich das Treiberpacket nochmal überarbeitet. Ich verwende nun aus VS 2012 andere msvc..-Treiber. Außerdem habe ich in Java die Methode getKeyState(..) ergänzt, damit auch bei Konsolen-Anwendungen eine direkte Tastatursteuerung möglich ist. Ein Programmbeispiel für Bluej ist hier enthalten. Erfahrungen könne z.B. im Forum besprochen werden.