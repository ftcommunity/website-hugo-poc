---
layout: "file"
hidden: true
title: "TX-Beispiel-Programme"
date: "2017-04-18T00:00:00"
file: "txbeispielprogramme.rar"
konstrukteure: 
- "Helmut Jawtusch"
uploadBy:
- "Helmut Jawtusch"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/computing/txbeispielprogramme.rar
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/computing/txbeispielprogramme.rar -->
Ich habe auf der Basis der TX-Treiber (23.2.2017) einige Programm für den TX-Controller (V1.30) erstellt für die Sprachen: c++, c#, Java, Terrapin Logo und c.
Sie zeigen den grundlegenden Aufbau einfacher Robotik-Programme. In der nächste Ausgabe von ft:pedia gebe ich zu jedem Beispiel noch einige Hinweise. In c geschriebenen Programme können (als binär-code) in den Speicher des TX kopiert und dann von der firmware ausgeführt werden. 