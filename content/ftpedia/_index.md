---
title: "ft:pedia"
weight: 70
layout: "ftpediaAll"
legacy_id:
- ftcomm2409.html
---
<!-- https://www.ftcommunity.de/ftcomm2409.html?file=ftpedia -->
#### ft:pedia - das fischertechnik-Kompendium

Viermal jährlich (immer am letzten Samstag im Quartal bzw. am 24.12.) erscheint mit 
ft:pedia eine fischertechnik-Fachzeitschrift von und für fischertechnik-Fans (und
solche, die es werden wollen) - als PDF-Datei zum kostenlosen Herunterladen
(einfach auf die jeweilige Titelseite klicken). Mit Beiträgen zu technischen
Grundlagen, Computing, Elektronik sowie der Vorstellung von Groß- und
Kleinmodellen richtet sich ft:pedia ebenso an Einsteiger wie an “alte Hasen”.

* [Übersicht über alle bisher erschienenen Artikel](overview)  
Hier werden die veröffentlichten Artikel über alle Ausgaben hinweg aufgeführt.
Auch die älteren Beiträge enthalten viele wichtige und nützliche Informationen.
Die Links in der Übersicht führen zur jeweiligen Ausgabe und auch gleich auf
die richtige Seite.

* [Volltextsuche über alle Ausgaben](../search/ftpedia-search/)  
Hiermit könnt ihr ft:pedia-Ausgaben finden, in denen beliebige Suchbegriffe im Text vorkommen.

* [Ergänzende Downloads](ftp-extras)  
Zu einigen ft:pedia-Artikeln gibt es ergänzende Dateien. Diese “Extras” sind im
Jahrgang und beim jeweiligen Heft aufgeführt. Unter obigem Link findet sich
Gesamtübersicht über diese Dateien über alle Ausgaben hinweg.

* [Gedruckte Ausgaben der ft:pedia](https://forum.ftcommunity.de/viewtopic.php?f=31&t=5568#p40610)  
Die ft:pedia ist auch gedruckt erhältlich. Nähere Informationen dazu finden
sich unter diesem Link in unserem Forum.

  * [Inhaltsverzeichnis des Doppeljahrgangs 2013 & 2014](./Inhalt20132014.pdf)
  * [Inhaltsverzeichnis des Doppeljahrgangs 2011 & 2012](./Inhalt20112012.pdf)

* [ft:pedia in der Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm?method=simpleSearch&query=ftpedia)  
Die ft:pedia hat die offizielle ISSN 2192-5879 und wird in der zentralen
Archivbibliothek Deutschlands in Leipzig archiviert.

* [Einzelne Artikel oder Serien selbst zusammenstellen](https://pedia.faler.ch/)  
Der "ft:pedia-Mixer" von unserem Fan olagino kann PDFs mit ganz nach
Bedarf selbst ausgewählten ft:pedia-Artikel erstellen. Danke, olagino!

* [Kommentare und Anregungen posten](https://forum.ftcommunity.de/viewforum.php?f=31)  
Dafür gibt es den hier verlinkten Forums-Bereich. Die Herausgeber freuen sich
über jede Rückmeldung zur ft:pedia, und die Autoren freuen sich über
Feedback zu ihren Beiträgen. Schreibt uns!

* Beiträge einreichen an ftpedia@ftcommunity.de  
Die ft:pedia lebt von euch als Beitrags-Autoren! Die Herausgeber und erst
recht die Leser freuen sich also über Beiträge, die ihr einreicht. Auch wer noch
nie „geschrieben“ hat, aber ein technisches Thema oder ein Modell mit
fischertechnik darstellen möchte, ist herzlich im Kreis der ft:pedia-Autoren
willkommen! Mailt uns einfach unter dieser Adresse, und schon sind wir in
Kontakt. Wir können fast alle Dateiformate von unformatiertem Text bis zu
PDFs verarbeiten. Bitte achtet auf gut ausgeleuchtete, scharfe Fotos. Wir
redigieren, fertigen auf Wunsch Reinzeichnungen an und machen in
Abstimmung mit euch den Beitrag druckfertig. Und wenn ihr zufrieden seid –
dann geht euer ft:pedia-Artikel in die ganze Welt hinaus!

Jede unentgeltliche Verbreitung der unveränderten und vollständigen Ausgabe sowie
einzelner Beiträge (mit vollständiger Quellenangabe: Autor, Ausgabe, Seitenangabe
ft:pedia) ist nicht nur zulässig, sondern ausdrücklich erwünscht. Die
Verwertungsrechte aller in ft:pedia veröffentlichten Beiträge liegen bei den jeweiligen
Autoren.

