---
layout: "file"
hidden: true
title: "Großer Raupenkran 1969"
date: 2023-03-13T17:44:33+01:00
file: "großer-raupenkran-1969.ftm"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Website-Team"
license: "unknown"
---

Zum Artikel „Ein Bild von einem Modell (Teil 2): Großer Raupenkran 1969“
