---
layout: "file"
hidden: true
title: "Uhrwerk"
date: 2023-03-13T17:37:20+01:00
file: "uhrwerk.ftm"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Website-Team"
license: "unknown"
---
Zum Artikel „Ein kompaktes Uhrengetriebe mit Sekundenzeiger“ von Stefan Falk
