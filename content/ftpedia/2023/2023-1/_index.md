---
hidden: true
layout: "issue"
title: "1 / 2023"
file: "ftpedia-2023-1.pdf"
publishDate: 2023-03-25T00:00:00+01:00
date: 2023-03-25T00:00:00+01:00
uploadBy:
- "ft:pedia-Redaktion"
---
