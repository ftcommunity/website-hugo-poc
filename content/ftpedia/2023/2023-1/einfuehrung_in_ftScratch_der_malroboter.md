---
layout: "file"
title:  "Einführung in ftScratch (5) - Der Malroboter"
date: 2023-03-23T00:00:00+0100
publishDate: 2023-03-25T00:00:00+0100
file: "einfuehrung_in_ftScratch_der_malroboter.zip"
konstrukteure:
- "Dirk Fox"
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
