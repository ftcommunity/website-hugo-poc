---
layout: "file"
hidden: true
title: "Großer Portalkran 1967"
date: 2023-03-13T17:41:15+01:00
file: "großer-portalkran-1967.ftm"
konstrukteure: 
- "Stefan Falk"
uploadBy:
- "Website-Team"
license: "unknown"
---

Zum Artikel „Ein Bild von einem Modell (Teil 1): Großer Portalkran 1967"
