---
layout: "file"
title:  "Materialien zur Schickard-Rechenmaschine"
date: 2023-09-30T00:00:00+0100
publishDate: 2023-09-29T00:00:00+0100
file: "schickard_rechenmaschine_materialien.zip"
konstrukteure:
- "Thomas Püttmann" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
