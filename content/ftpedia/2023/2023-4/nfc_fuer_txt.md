﻿---
layout: "file"
title:  "NFC für TXT"
date: 2023-12-18T00:00:00+0100
publishDate: 2023-12-24T00:00:00+0100
file: "nfc_fuer_txt.zip"
konstrukteure:
- "Axel Chobe" 
uploadBy:
- "ft:pedia-Redaktion"
license: "unknown"
---
